<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.TimeZone"%>
<jsp:useBean id="task" scope="session" class="com.debisys.utils.CarrierTaskBean"></jsp:useBean>
<%
String strReport = request.getParameter("report");
int section=14;
int section_page=52;

boolean downloadbar  = false;
boolean maxdownloadbar  = false;
int maxdownloadset=-1 ;
String products = "";

if (strReport !=null && strReport.equals("y"))
{
  section=14;
  section_page=14;
}
else
{
  strReport = "";
}
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CarrierSearch" class="com.debisys.transactions.CarrierSearch" scope="request"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="CarrierSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;

//DBsy-905
int imaxdownloadbar = -1;
String positionMarkers = null;
int intSectionPage = 0;
int recordcount = 0;
boolean isDownload = false;

products = request.getParameter("products");
if (products != null && !products.trim().equals(""))
{
    CarrierSearch.setProductIds(products);
}
    
if (request.getParameter("download") != null)
{
  if (request.getParameter("section_page") != null)
  {
    try
    {
      intSectionPage=Integer.parseInt(request.getParameter("section_page"));
    }
    catch(NumberFormatException ex)
    {
     response.sendRedirect(request.getHeader("referer"));
     return;
    }
  }  
    if (request.getParameter("markers") != null) {
        positionMarkers = request.getParameter("markers"); 
    } 
     if (request.getParameter("invoiceID") != null)
    {
    	CarrierSearch.setInvoiceNo(request.getParameter("invoiceID").toString());
    }
    
      //DBSY-908 
    if (request.getParameter("recordcount") != null) {
        recordcount = Integer.valueOf(request.getParameter("recordcount")); 
    } 
      //DBSY-908 
    if (request.getParameter("downloadbar") != null) {
        downloadbar = Boolean.valueOf(request.getParameter("downloadbar")); 
    } 
      //DBSY-908 
    if (request.getParameter("maxdownloadbar") != null) {
        imaxdownloadbar = Integer.valueOf(request.getParameter("maxdownloadbar")); 
    } 
    
    if (request.getParameter("merchantIds") != null) {
        CarrierSearch.setMerchantIds(request.getParameter("merchantIds"));
    }

  if (CarrierSearch.validateDateRange(SessionData))
  {
    if ( request.getParameter("millennium_no") != null )
    {
    	CarrierSearch.setMillennium_No(request.getParameter("millennium_no"));
    }
    String strUrlLocation = "";
	String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
			    
	
      	if( downloadbar ){
     	task.setRunning(true);
     	task.setrecordcount(recordcount);
	    if(imaxdownloadbar!=-1  && recordcount>imaxdownloadbar){
	     	task.setmax(imaxdownloadbar);
     		task.setrecordcount(imaxdownloadbar);
     	}
     	task.setvar(false,SessionData, intSectionPage, application, CarrierSearch);
		isDownload = true;
		}
		else{
	   		 if(imaxdownloadbar!=-1)
	     			CarrierSearch.setmax(imaxdownloadbar);
     	 	strUrlLocation = CarrierSearch.download(SessionData, intSectionPage, application);
     	 	 response.sendRedirect(strUrlLocation);
     	 	}
    
   
  }
  else
  {
     response.sendRedirect(request.getHeader("referer"));
     return;
  }
}

//////////////////////////////
if (request.getParameter("search") != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  if (CarrierSearch.validateDateRange(SessionData))
  {
  
    if (request.getParameter("invoiceID") != null)
    {
    	CarrierSearch.setInvoiceNo(request.getParameter("invoiceID").toString());
    }
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    vecSearchResults = CarrierSearch.search(intPage, intPageSize, SessionData, section_page, application, (request.getParameter("chkUseTaxValue") != null));
    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    	// DBSY 908
   	// checking download bar settings if Enabled and if feature is needed.
		String download = DebisysConfigListener.getdownloadbar(application);
		if(download!=null && !download.equals("")){
		try{
			int downloadset = Integer.parseInt(download);
			if( downloadset <= intRecordCount ){
				 downloadbar = true;
			}
		}
		catch(NumberFormatException e){
		 	downloadbar = false;
		}
		}
		
		
		String maxdownload = DebisysConfigListener.getmaxdownload(application);
		if(maxdownload!=null && !maxdownload.equals("")){
		try{
			 maxdownloadset = Integer.parseInt(maxdownload);
			if( maxdownloadset < intRecordCount && maxdownloadset!=0){
				maxdownloadbar = true; 
			}
		}
		catch(NumberFormatException e){
		 	maxdownloadbar = false;
		}
		}
		vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize);
      if ((intPageCount * intPageSize) < intRecordCount)
      {
        intPageCount++;
      }
    }
  }
  else
  {
   searchErrors = CarrierSearch.getErrors();
  }


}
%>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
function check(){
	
 if(!IsNumeric(document.mainform.invoiceID.value)){
 	document.mainform.invoiceID.value  = "";
 	alert ("Invoice must be numberic");
	return false; 
 }
 if(!IsNumeric(document.mainform.transactionID.value)){
 	document.mainform.transactionID.value  = "";
 	alert ("Transaction ID must be numberic");
	return false; 
 }
   scroll();
   return true;
}         
function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
   }

var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2(istrue)
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2("false")', 150);
}

function openViewPIN(sTransId)
{
  var sURL = "/support/admin/reports/carrier_user/transactions/view_pin.jsp?trans_id=" + sTransId;
  var sOptions = "left=" + (screen.width - (screen.width * 0.4))/2 + ",top=" + (screen.height - (screen.height * 0.3))/2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
  var w = window.open(sURL, "ViewPIN", sOptions, true);
  w.focus();
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
  <tr>
	  <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
	  <td class="formAreaTitle" align=left width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.merchants.transactions.title",SessionData.getLanguage()).toUpperCase()%></td>
	  <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="get" action="admin/reports/carrier_user/transactions/merchants_transactions.jsp" onSubmit="return check();">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
      <%if (strReport == null || strReport.equals(""))
      {%>
     	<tr>
	        <td class="formArea2">
	          <table width="300">
<%
	Vector vTimeZoneData = null;
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
              <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
              <tr class="main">
               <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
         <%}else{ %>
         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
         <%}%><td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td><td><input class="plain" id="startDate" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td> <input class="plain" id="endDate" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>

		<tr class="main">
		    <td nowrap><%=Languages.getString("jsp.admin.reports.transaction_id",SessionData.getLanguage())%>: </td><td><input name="transactionID" id="transactionID" value="<%=CarrierSearch.getTransactionID()%>"></td> 
		</tr>
		<tr class="main">
		    <td nowrap><%=Languages.getString("jsp.admin.reports.pinreturn_search.pin",SessionData.getLanguage())%>: </td><td><input name="PINNumber" id="PINNumber" value="<%=CarrierSearch.getPINNumber()%>"></td> 
		</tr>
		
		<% if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{%>
		<tr class="main">
		    <td nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>: </td><td><input name="invoiceID" id="invoiceID" value="<%=CarrierSearch.getInvoiceNo()%>"></td> 
		</tr>
		<%} 
		else
		{ %>
		<tr style="display:none;" class="main">
		    <td style="display:none;" nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>: </td><td><input name="invoiceID" id="invoiceID" style="display:none;"  value="<%=CarrierSearch.getInvoiceNo()%>"></td> 
		</tr>
		<%} %>
		

<tr> 
	<td class=main>
      <input type="hidden" name="search" value="y">
      <input type="hidden" name="merchantId" value="<%=CarrierSearch.getMerchantId()%>">
      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.transactions.show_transactions",SessionData.getLanguage())%>">
    </td>
</tr>
<tr>
<td colspan=3 class="main">
* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
</td>
</tr>
<%}%>
</table>
               </td>
              </tr>
              </form>
            </table>
 <% if(isDownload){%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>
<div><iframe width=350 height=200 name="iframe" id="iframe" src="admin/reports/carrier_user/transactions/status.jsp" frameborder="0" style="visibility:visible;">
		</iframe></div></td></tr></table><%
new Thread(task).start();
}
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
	if ( strReport.equals("y") )
	{
		Vector vTimeZoneData = null;
		if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
		{
			vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
		}
		else
		{
			vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
		}
%>
	        <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
<%
	}
%>
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
              if (!CarrierSearch.getStartDate().equals("") && !CarrierSearch.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(CarrierSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(CarrierSearch.getEndDateFormatted()));
                }
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%></td></tr>
            <tr><td class="main">
            <%=Languages.getString("jsp.admin.iso_name",SessionData.getLanguage())%>:&nbsp;<%=SessionData.getUser().getCompanyName()%>
            </td></tr>            
            <tr>
              <td align=left class="main" nowrap>
            <table>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>
<% if(!isDownload){%> 
			  <form name="downloadform" method=post action="admin/reports/carrier_user/transactions/merchants_transactions.jsp" onSubmit="scroll2();" ><input type="hidden" name="startDate" value="<%=CarrierSearch.getStartDate()%>">
              <input type="hidden" name="endDate" value="<%=CarrierSearch.getEndDate()%>">
              <input type="hidden" name="page" value="<%=intPage%>">
              <input type="hidden" name="section_page" value="<%=section_page%>">
              <input type="hidden" name="merchantId" value="<%=CarrierSearch.getMerchantId()%>">
              <input type="hidden" name="invoiceID" value="<%=CarrierSearch.getInvoiceNo()%>">
              <input type="hidden" name="transactionID" value="<%=CarrierSearch.getTransactionID()%>">
              <input type="hidden" name="PINNumber" value="<%=CarrierSearch.getPINNumber()%>">
              <input type="hidden" id="downloadbar" name="downloadbar" value="<%=downloadbar%>">
              <input type="hidden" id="recordcount" name="recordcount" value="<%=intRecordCount%>">
              <input type="hidden" name="download" value="y">
<%
    if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
         (request.getParameter("chkUseTaxValue") != null)
       )
    {
%>
              <input type="hidden" name="chkUseTaxValue" value="<%=request.getParameter("chkUseTaxValue")%>">
<%
    }
%>
              <% if(maxdownloadbar){ %><input type="hidden" id="maxdownloadbar" name="maxdownloadbar" value="<%=maxdownloadset%>"><% } %>
              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
              </form>
              <br/>
              <% if(maxdownloadbar){ %>
              <FONT COLOR="red"> <%=Languages.getString("jsp.admin.warning_maxdownload",SessionData.getLanguage())%><%=maxdownloadset%><%=Languages.getString("jsp.admin.warning_secmaxdownload",SessionData.getLanguage())%>
              </font><% } } %>
              </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
<%
	if ( request.getParameter("printbtn") != null )
	{
%>
                <FORM ACTION="admin/reports/carrier_user/transactions/print_merchants_transactions.jsp" METHOD=post TARGET=blank>
                	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                	<INPUT TYPE=hidden NAME=merchantId VALUE="<%=request.getParameter("merchantId")%>">
                	<INPUT TYPE=hidden NAME=search VALUE="<%=request.getParameter("search")%>">
                	<INPUT TYPE=hidden NAME=report VALUE="<%=request.getParameter("report")%>">
                	<INPUT TYPE=hidden NAME=page VALUE="<%=com.debisys.utils.StringUtil.toString(request.getParameter("page"))%>">
                	<INPUT ID=btnPrint TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.print",SessionData.getLanguage())%>">
                </FORM>
<%
	}
%>
                </td>
              </tr>
            </table>
              </td>
            </tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"admin/reports/carrier_user/transactions/merchants_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&merchantId=" + CarrierSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(CarrierSearch.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(CarrierSearch.getEndDate(), "UTF-8") +"&page=1&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null)?"&chkUseTaxValue=" + request.getParameter("chkUseTaxValue"):"") + ((request.getParameter("printbtn") != null)?"&printbtn=" + request.getParameter("printbtn"):"") + "\">First</a>&nbsp;");
                out.println("<a href=\"admin/reports/carrier_user/transactions/merchants_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&merchantId=" + CarrierSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(CarrierSearch.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(CarrierSearch.getEndDate(), "UTF-8") +"&page=" + (intPage-1) + "&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null)?"&chkUseTaxValue=" + request.getParameter("chkUseTaxValue"):"") + ((request.getParameter("printbtn") != null)?"&printbtn=" + request.getParameter("printbtn"):"") + "\">&lt;&lt;Prev</a>&nbsp;");
              }

              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"admin/reports/carrier_user/transactions/merchants_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&merchantId=" + CarrierSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(CarrierSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(CarrierSearch.getEndDate(), "UTF-8") + "&page=" + i + "&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null)?"&chkUseTaxValue=" + request.getParameter("chkUseTaxValue"):"") + ((request.getParameter("printbtn") != null)?"&printbtn=" + request.getParameter("printbtn"):"") + "\">" +i+ "</a>&nbsp;");
                }
              }

              if (intPage <= (intPageCount-1))
              {
                out.println("<a href=\"admin/reports/carrier_user/transactions/merchants_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&merchantId=" + CarrierSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(CarrierSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(CarrierSearch.getEndDate(), "UTF-8") + "&page=" + (intPage+1) + "&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null)?"&chkUseTaxValue=" + request.getParameter("chkUseTaxValue"):"") + ((request.getParameter("printbtn") != null)?"&printbtn=" + request.getParameter("printbtn"):"") + "\">Next&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"admin/reports/carrier_user/transactions/merchants_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&merchantId=" + CarrierSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(CarrierSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(CarrierSearch.getEndDate(), "UTF-8") + "&page=" + (intPageCount) + "&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null)?"&chkUseTaxValue=" + request.getParameter("chkUseTaxValue"):"") + ((request.getParameter("printbtn") != null)?"&printbtn=" + request.getParameter("printbtn"):"") + "\">Last</a>");
              }

              %>
              </td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
              <td class=rowhead2>#</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
<%
		if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
		{
%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.termtypedesc",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
<%
				if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
%>
  			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.phys_state",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
%>                
			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.city",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.county",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.reference",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%></td>   
<%
/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                	
                	if ( strAccessLevel.equals(DebisysConstants.CARRIER ) )
                	{
                	%>
                		<td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;</td>
			            <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></td>
			            <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
			            <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
			            <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
                	
                	<%
                	}
                	if(strAccessLevel.equals(DebisysConstants.ISO))
                	{
               	 		if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
						%>               	  
							<td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;</td>
			                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
			                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%               	 	}
                	 	else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
	%>					  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;</td>
			              <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></td>
			              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
			              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
			              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                	 	}
                   	}
                   	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
%>                   	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
                   	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                 	}
                   	else if (strAccessLevel.equals(DebisysConstants.REP)){
%>					  <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
                   	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
%>                    <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
%>				


<%
				}
/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/				
%>              
                         
				<%//DBSY-905
				if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
				%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%></td>
					  
			        <% if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
			    	  {%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></td>
			  		  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxpercentage",SessionData.getLanguage()).toUpperCase()%></td>
			  		  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxtype",SessionData.getLanguage()).toUpperCase()%></td>
					<%}
				}%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.description",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()).toUpperCase()%></td>
<% 
		if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
%>
				<td class=rowhead2><%=Languages.getString("jsp.admin.reports.pin_number",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
 %>              
<% 
		if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
%>
				<td class=rowhead2><%=Languages.getString("jsp.admin.reports.ach_date",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
 %>              
<%
				if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
				{
%>
  			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_number",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
%>                 
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_type",SessionData.getLanguage()).toUpperCase()%></td>
               <%  if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage()).toUpperCase()%></td>
              <%} %>
            </tr>
            <%
                  int intCounter = 1;
                  //intCounter = intCounter - ((intPage-1)*intPageSize);

                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  String type = "";
                  String row = "";
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    // check if type is "Return" of "Adjust", if so, display the whole row in red
                    type = (String)vecTemp.get(14);
                    if (type.equals("Return") || type.equals("Adjustment") || type.equals("Credit")||type.equals("Write Off"))
                    {
                    	row = "rowred";
                    }
                    else
                    {
                    	row = "row";
                    }

                    String sViewPIN = "", sPhysState = "", sAuthorizationNo = "";
                    String sPinNumber = "N/A";
                    if ( SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) )
                    {
                        // LOCALIZATION - Section modified to accept just a result string with the currency amount
                        // From the DB.
                      //if the trans_type=2 which is a pin sale
                      if ( vecTemp.get(13).toString().equals("2") && Double.parseDouble(vecTemp.get(9).toString().replaceAll(",", "")) > 0)
                      {
                        sViewPIN = "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + vecTemp.get(0).toString() + "');\"><span class=\"showLink\">" + vecTemp.get(12).toString() + "</span></a>";
                      	sPinNumber = vecTemp.get(19).toString();
                      }
                      else
                      {
                        sViewPIN = vecTemp.get(12).toString();
                      }
                    }
                    else
                    {
                      sViewPIN = vecTemp.get(12).toString();
                    }

                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                    {
						sPhysState = "<td nowrap>" + vecTemp.get(16).toString() + "</td>";
                    }
                    else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                    {
						sAuthorizationNo = "<td nowrap>" + vecTemp.get(15).toString() + "</td>";
                    }
                    String sTerminalDescription = "";
                    if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
                    {
                    	sTerminalDescription = "<td nowrap>" + vecTemp.get(21) + "</td>";                    	
                    }
                    
					
					String[] productDescription = vecTemp.get(11).toString().split("\\/");
					
                    out.println("<tr class=" + row + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                sTerminalDescription +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString())  + "</td>" +
                                "<td>" + vecTemp.get(3) + "</td>" +
                                "<td nowrap>" + vecTemp.get(4) + "</td>" +
                                sPhysState +                                
                                 //city
                                "<td nowrap>" +  vecTemp.get(5).toString() + "</td>" +
                                //county
                                "<td nowrap>" +  vecTemp.get(28).toString() + "</td>" +
                                //clerk
                                "<td nowrap>" + vecTemp.get(6) + "</td>" +
                                "<td>" + vecTemp.get(7) + "</td>" +
                                "<td>" + vecTemp.get(8) + "</td>" +
                                //DBSY-905 Tax Calculation
                    		    //amount
                    		    "<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>");
								/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) 
			                    {
			                     	if ( strAccessLevel.equals(DebisysConstants.CARRIER) )
			                     	{
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
			                     	}
			                     	if(strAccessLevel.equals(DebisysConstants.ISO))
			                     	{
			                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //iso
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
			                     	 }
			                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			                     	 	out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
			                     	 }
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
		                         	}
		                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
		                         	}
		                        }  
		                    	/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/                       //bonus                    		    
                    		    
			                    if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
			                    
			                       //bonus
			                                out.println("<td nowrap>" + vecTemp.get(17) + "</td>" +
			                      //total Recharge
			                                "<td nowrap>" + vecTemp.get(18) + "</td>");
			                        if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
				    	 					{
			                     //net amount 
			                         	       out.println("<td nowrap>" + vecTemp.get(24) + "</td>" +
			                     //tax amount
			                                 "<td nowrap>" + vecTemp.get(25) + "</td>" + 
			                                 "<td nowrap>" + vecTemp.get(26) + "%</td>" + 
			                                 "<td nowrap>" + vecTemp.get(27) + "</td>");
			                                }
			                    }
                                //bal
                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(10).toString()) + "</td>" +
                                //product id
                                "<td nowrap>" + productDescription[0] + "</td>" +
                                //description product
                                "<td nowrap>" + productDescription[1] + "</td>" +
                                "<td>" + sViewPIN + "</td>" );
					if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
						&& strAccessLevel.equals(DebisysConstants.ISO)
						&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
						out.println("<td>" + sPinNumber + "</td>");
					}
					if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
						&& strAccessLevel.equals(DebisysConstants.ISO)
						&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
						out.println("<td>" + vecTemp.get(20) + "</td>");
					}
                    out.println(sAuthorizationNo + "<td>" + type + "</td>");
                    if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{
                    //DBSY-919 Invoice Number Field
						out.println("<td>" + vecTemp.get(23) + "</td>" + "</tr>");
					//////  
					}
					else
						out.println("</tr>");
						
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>

<%
}
else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>