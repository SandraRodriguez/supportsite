<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder" %>
<%
	int section=14;
	int section_page=30;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector vecSearchResults = new Vector();
	Hashtable searchErrors = null;
	
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;

	int DATE_TIME = 1;
	int MILLENNIUM_NO = 2;
	int TYPE = 3;
	int HOST_ID = 4;
	int PORT = 5;
	int DURATION = 6;
	int INFO1 = 7;
	int RESULT_CODE = 8;
	int TRANSACTION_ID = 9;
	int MOBILE = 10;
	int PROVIDER_RESPONSE=12;

	int ASC = 1;
	int DESC = 2;

	String queryType = null;
	String appendToDownloadURL = "";

  	if (request.getParameter("page") != null)
  	{
    	try
    	{
      		intPage=Integer.parseInt(request.getParameter("page"));
    	}
    	catch(NumberFormatException ex)
    	{
      		intPage = 1;
    	}
  	}
  
  	if (request.getParameter("queryType") != null) 
  	{
    	queryType = request.getParameter("queryType");
  	}

  	if (intPage < 1)
  	{
	    intPage=1;
  	}

	if ( request.getParameter("download") != null )
	{
	  	String sURL = "";
      	SessionData.setProperty("start_date", request.getParameter("startDate"));
      	SessionData.setProperty("end_date", request.getParameter("endDate"));
      	TransactionReport.setMerchantIds(request.getParameter("merchant_ids"));
      	TransactionReport.setCol(request.getParameter("col"));
      	TransactionReport.setSort(request.getParameter("sort"));
      	TransactionReport.setResultCode(request.getParameter("resultCode"));      	
              
    	sURL = TransactionReport.downloadCarrierUserTransactionErrorDetailReport(application, SessionData, false, queryType);
    	response.sendRedirect(sURL);
    	return;
	}
	
  	if (TransactionReport.validateDateRange(SessionData))
  	{
    	SessionData.setProperty("start_date", request.getParameter("startDate"));
    	SessionData.setProperty("end_date", request.getParameter("endDate"));        
    	vecSearchResults = TransactionReport.getCarrierUserTransactionErrorDetailByTypes(intPage, intPageSize, SessionData, true, queryType);
    	intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    	vecSearchResults.removeElementAt(0);
    	
    	if (intRecordCount>0)
    	{
    	  	intPageCount = (intRecordCount / intPageSize);
      
      		if ((intPageCount * intPageSize) < intRecordCount)
      		{
        		intPageCount++;
      		}
    	}	
  	}
  	else
  	{
    	searchErrors = TransactionReport.getErrors();
  	}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="2000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.transaction_error_details.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
			            	<tr>
			            		<td class="main"><%=intRecordCount%> result(s) found<%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
			            			<table>
			              				<tr>
			                				<td>
							                  	<form method=post action="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp<%=(queryType != null) ? "queryType=voidErrors" : ""%>" onsubmit="document.getElementById('btnSubmit').disabled=true;">
							                    	<input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
							                    	<input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
							                    	<input type=hidden name="merchant_ids" value="<%=TransactionReport.getMerchantIds()%>">
							                    	<input type=hidden name="col" value="<%=TransactionReport.getCol()%>">
							                    	<input type=hidden name="sort" value="<%=TransactionReport.getSort()%>">
							                    	<input type=hidden name="page" value="<%=intPage%>">
							                    	<input type=hidden name="resultCode" value="<%=TransactionReport.getResultCode()%>">
						              				<input type="hidden" name="download" value="y">
							                    	<input id=btnSubmit type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
							                  	</form>
			                				</td>
			                				<td>
							                  	<form method=post target=blank action="admin/reports/carrier_user/transactions/print_transaction_error_detail_by_types.jsp<%=(queryType != null) ? "queryType=voidErrors" : ""%>">
							                    	<input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
							                    	<input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
							                    	<input type=hidden name="merchant_ids" value="<%=TransactionReport.getMerchantIds()%>">
							                    	<input type=hidden name="col" value="<%=TransactionReport.getCol()%>">
							                    	<input type=hidden name="sort" value="<%=TransactionReport.getSort()%>">
							                    	<input type=hidden name="page" value="<%=intPage%>">
							                    	<input type=hidden name="resultCode" value="<%=TransactionReport.getResultCode()%>">
							                    	<input type=submit value="<%=Languages.getString("jsp.admin.reports.carrier_user.print",SessionData.getLanguage())%>">
							                  	</form>
			                				</td>
			              				</tr>
			            			</table>
			            		</td>
			            	</tr>
			            	<tr>
								<td align=right class="main" nowrap>
<%
		if (intPage > 1)
		{
			out.println("<a href=\"admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?merchantIds=" + TransactionReport.getMerchantIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=1\">First</a>&nbsp;");
			out.println("<a href=\"admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?merchantIds=" + TransactionReport.getMerchantIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPage-1) + "\">&lt;&lt;Prev</a>&nbsp;");
		}

		int intLowerLimit = intPage - 12;
		int intUpperLimit = intPage + 12;

		if (intLowerLimit < 1)
		{
			intLowerLimit=1;
            intUpperLimit = 25;
		}

		for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
		{
			if (i==intPage)
			{
				out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
			}
            else
            {
            	out.println("<a href=\"admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?merchantIds=" + TransactionReport.getMerchantIds() + "&resultCode=" + TransactionReport.getResultCode() +  "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + i + "\">" +i+ "</a>&nbsp;");
			}
		}

		if (intPage <= (intPageCount-1))
		{
			out.println("<a href=\"admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?merchantIds=" + TransactionReport.getMerchantIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPage+1) + "\">Next&gt;&gt;</a>&nbsp;");
			out.println("<a href=\"admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?merchantIds=" + TransactionReport.getMerchantIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPageCount) + "\">Last</a>");
		}
%>
								</td>
			            	</tr>
			            </table>
			            <table width="100%" cellspacing="1" cellpadding="2">
			            	<thead>
			      	      	<tr>
			              		<td class=rowhead2>#</td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=DATE_TIME%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=DATE_TIME%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=TRANSACTION_ID%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=TRANSACTION_ID%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.amount",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk_id",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.product_id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=INFO1%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=INFO1%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.phone",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=MOBILE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=MOBILE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.type",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=TYPE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=TYPE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.host",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=HOST_ID%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=HOST_ID%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.port",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=PORT%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=PORT%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.duration",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=DURATION%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=DURATION%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.result",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=RESULT_CODE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=RESULT_CODE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>              
			              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.provider_response",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=PROVIDER_RESPONSE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?page=<%=intPage%>&col=<%=PROVIDER_RESPONSE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
			            	</tr>
							</thead>
<%
		int intCounter = 1;
		Iterator it = vecSearchResults.iterator();
		int intEvenOdd = 1;
                  
		while (it.hasNext())
		{
			Vector vecTemp = null;
			vecTemp = (Vector) it.next();
			out.println("<tr class=row" + intEvenOdd +">" +
				"<td nowrap>" + intCounter++ + "</td>" +
				"<td nowrap>" + vecTemp.get(0)+ "</td>" +
				"<td nowrap>" + vecTemp.get(1) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(2) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(3) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(4) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(5) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(7) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(8) + "</td>" +
                "<td nowrap align=\"right\">" + vecTemp.get(9) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(11) + "</td>" +
                "<td nowrap align=\"right\">" + vecTemp.get(12) + "</td>" +
				"</tr>");
                    
			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}
		vecSearchResults.clear();
%>
						</table>
<%
	}
	else
	{
%>
						<table width="100%">
			               	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>