<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.languages.*,
                 com.debisys.utils.NumberUtil" %>
<%
  	int section      = 14;
  	int section_page = 22;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
  	Vector    vecSearchResults = new Vector();
  	Hashtable searchErrors     = null;
  
  	int intErrorsSum = 0;
  
    if (TransactionReport.validateDateRange(SessionData))
    {
      	SessionData.setProperty("start_date", request.getParameter("startDate"));
      	SessionData.setProperty("end_date", request.getParameter("endDate"));
      	vecSearchResults = TransactionReport.getCarrierUserTransactionErrorSummaryByTypes(SessionData);
    }
    else
    {
      	searchErrors = TransactionReport.getErrors();
    }
%>
<html>
<head>
    <link href="../../../../default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="400">
	<tr>
      	<td width="18" height="20" align="left"><img src="../../../../images/top_left_blue.gif"></td>
      	<td background="../../../../images/top_blue.gif" align=left width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title31",SessionData.getLanguage()).toUpperCase()%></td>
      	<td width="12" height="20" align="right"><img src="../../../../images/top_right_blue.gif"></td>
    </tr>
    <tr>
      	<td colspan="3" bgcolor="#FFFFFF" class="formArea">
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td class="main">
						<%= Languages.getString("jsp.admin.error_summary_report",SessionData.getLanguage()) %>
<%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
				" " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
%>
                		<br>               
              		</td>
            	</tr>
          	</table>
          	<table width="100%" cellspacing="1" cellpadding="1" border="0">
            	<thead>
              	<tr>
                	<td class=rowhead2 width="5%">#</td>
                	<td class=rowhead2 nowrap width="40%">
                  		<%= Languages.getString("jsp.admin.reports.error_type",SessionData.getLanguage()).toUpperCase() %>
                	</td>
                	<td class=rowhead2 nowrap width="17%">
                  		<%= Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase() %>
                	</td>
                	<td class=rowhead2 nowrap width="20%">
                  		<%= Languages.getString("jsp.admin.reports.total_errors",SessionData.getLanguage()).toUpperCase() %>
                	</td>
                	<td class=rowhead2 nowrap width="18%">
                  		<%= Languages.getString("jsp.admin.reports.error_percent",SessionData.getLanguage()).toUpperCase() %>
                	</td>
              	</tr>
				</thead>
<%
		Iterator it                       = vecSearchResults.iterator();
		int      intEvenOdd               = 1;
		int      intCounter               = 1;

		if (it.hasNext())
		{
			Vector vecTemp = null;
			vecTemp = (Vector)it.next();
			intErrorsSum = Integer.parseInt(vecTemp.get(0).toString());
		}
              
		while (it.hasNext())
		{
			Vector vecTemp = null;
			vecTemp = (Vector)it.next();
			int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
                
			int errors = Integer.parseInt(vecTemp.get(2).toString());
			double errors_percent = 0;
              
			if (intErrorsSum == 0)
			{
				errors_percent = 100;
			}
			else
			{
				errors_percent = ((double)errors/intErrorsSum) * 100;
			} 
			
			out.print("<tr class=row" + intEvenOdd + ">" + 
              	"<td nowrap>" + intCounter++ + "</td>" + 
              	"<td nowrap>" + vecTemp.get(1) + "</td>" + 
              	"<td nowrap>" + vecTemp.get(2) + "</td>" +
              	"<td nowrap>" + intErrorsSum + "</td>" + 
              	"<td nowrap align=\"right\">" + NumberUtil.formatAmount(Double.toString(errors_percent)) + "%</td></tr>");
                    
			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}            
%>            
			</table>
            <table id="tblClose" style="display:block;width:100%;">
              	<tr>
              		<td align=center>
              			<input type=button value="<%=Languages.getString("jsp.admin.reports.closewindow",SessionData.getLanguage())%>" onclick="window.close();/*window.setTimeout('', 1000);*/">
              		</td>
              	</tr>
			</table>
<%
	}
	else {
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
	}
%>
      	</td>
    </tr>
</table>
<script>
document.getElementById("tblClose").style.display = "none";
window.print();
document.getElementById("tblClose").style.display = "block";
</script>
</body>
</html>