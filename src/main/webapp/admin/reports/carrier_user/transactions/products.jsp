<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport" %>
<%
	int section=14;
	int section_page=23;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title5",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<form name="mainform" method="post" action="admin/reports/carrier_user/transactions/products_summary.jsp" onSubmit="scroll();">
						<table width="300">
<%
	if(SessionData.getProperty("carrierUserProds") == null || SessionData.getProperty("carrierUserProds").equals(""))
	{
%>
							<tr class=main>
								<td colspan=2><font color="red">NOTE: This Carrier User does not have any products allowed for report viewing.</font></td>
							</tr>
<%		
	}
%>
							<tr class="main">
								<td nowrap valign="top">
									<%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>
		         <%}%>:</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td valign="top" nowrap>
									<table width=400>
										<tr>
											<td class="main" colspan=2>
												* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
											</td>
										</tr>
										<tr class="main">
											<td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
											<td><input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
										</tr>
										<tr class="main">
											<td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td>
											<td><input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
										</tr>
										<tr>
											<td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.products.option",SessionData.getLanguage())%></td>
							    			<td class=main valign=top>
												<select name="pids" size="10" multiple>
													
<%
	// For this, since the carrier user products are in SessionData, just pull them out and split on the commas
	// Then filter by whichever ones the carrier user can report on.
	Vector<Vector<String>> vecProductList = TransactionReport.getCarrierUserProductList(SessionData);
	
	if(vecProductList.size() > 0)
	{
		out.println("<option value=\"\" selected>" + Languages.getString("jsp.admin.reports.all",SessionData.getLanguage()) + "</option>");
	} else {
		out.println("<option value=\"\" selected>N/A</option>");
	}

	Iterator<Vector<String>> it = vecProductList.iterator();
  
	while (it.hasNext())
	{
		Vector<String> vecTemp = null;
		vecTemp = (Vector<String>) it.next();
		out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "("+vecTemp.get(0)+")</option>");
	}
%>
												</select>
												<br>
												*<%=Languages.getString("jsp.admin.reports.transactions.products.instructions",SessionData.getLanguage())%>
											</td>
										</tr>
<%
	if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
	{
		//If when deploying in Mexico
%>
										<TR CLASS="main">
										    <TD></TD><TD NOWRAP><INPUT TYPE="checkbox" ID="chkUseTaxValue" NAME="chkUseTaxValue"><LABEL FOR="chkUseTaxValue"><%=Languages.getString("jsp.admin.reports.transactions.products.mx_valueaddedtax",SessionData.getLanguage())%></LABEL></TD>
										</TR>
										<TR CLASS="main">
											<TD></TD>
										</TR>
<%
	}//End of if when deploying in Mexico
%>
										<tr>
									    	<td class=main colspan=2>
									      		<input type="hidden" name="search" value="y">
									      		<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
									      		<input type="hidden" id="merchantIds" name="merchantIds" value="">
									   	 	</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>