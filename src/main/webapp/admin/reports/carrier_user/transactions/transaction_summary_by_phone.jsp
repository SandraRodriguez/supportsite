<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
	int section = 14;
	int section_page = 46;
    boolean NoCarrierProductAttached = false;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CarrierReport" class="com.debisys.reports.CarrierReport" scope="request"/>
<jsp:setProperty name="CarrierReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector vecCurrent = new Vector();
	Vector vecPast = new Vector();
	Hashtable searchErrors = null;
	String message = ""; 
	
	if (request.getParameter("search") != null)
	{
  if (CarrierReport.validateDateRange(SessionData) && request.getParameter("phoneNumber") != null && !(request.getParameter("phoneNumber")).equals(""))
  {
    //pull last months
    CarrierReport.setStartDate(DateUtil.addSubtractMonths(request.getParameter("startDate"),-1));
    CarrierReport.setEndDate(DateUtil.addSubtractMonths(request.getParameter("endDate"),-1));
    vecPast = CarrierReport.getPhoneSummary(SessionData,application);

    CarrierReport.setStartDate(request.getParameter("startDate"));
    CarrierReport.setEndDate(request.getParameter("endDate"));
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    String carrierUserProds = SessionData.getProperty("carrierUserProds");
	  if(carrierUserProds!=null && !"".equals(carrierUserProds))
			vecCurrent = CarrierReport.getPhoneSummary(SessionData,application);	
	  else
      	 	NoCarrierProductAttached = true;
			
  }
  else
  {
    if(!CarrierReport.validateDateRange(SessionData))
    {
    	searchErrors = CarrierReport.getErrors();
    }
    if(request.getParameter("phoneNumber") == null || (request.getParameter("phoneNumber")).equals(""))
    {
        if (searchErrors == null)
        {
        	searchErrors = new Hashtable();
        }
        searchErrors.put("zphoneNumber",Languages.getString("com.debisys.reports.error6",SessionData.getLanguage()));	
    }
  }
	}
%>
<%@ include file="/includes/header.jsp" %>
<%
	boolean bool_custom_config_mexico = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
	boolean bool_custom_config_default = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	boolean bool_deploy_type_intl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);

%>
<table border="0" cellpadding="0" cellspacing="0" width="500" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title34",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
												<table width=400>
<%
	if (searchErrors != null)
	{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1 = searchErrors.keys();

		while(enum1.hasMoreElements())
		{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
		}

  out.println("</font></td></tr>");
	}
%>
												</table>
               </td>
              </tr>
            </table>
<%
	if (vecCurrent != null && vecCurrent.size() > 0)
	{
%>
            <table width="100%" cellspacing="1" cellpadding="2">
<%
	Vector vTimeZoneData = null;
		
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
            <tr class="main"><td nowrap colspan="4"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
			<tr class="errorText">
			<td colspan="4"><%= message %></td>
			</tr>

            <tr>
              <td colspan=4 class=rowhead2><%=Languages.getString("jsp.admin.reports.title34",SessionData.getLanguage()).toUpperCase()%> <%=HTMLEncoder.encode(CarrierReport.getPhoneNumber())%></td>
            </tr>
            <tr>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.description",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.current_period",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.previous_period",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.percentage_change",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
<%
		String topUpCountCurrent = vecCurrent.get(2).toString();
            double dbTopUpCountCurrent = Double.parseDouble(topUpCountCurrent);

		String topUpCountPast = vecPast.get(2).toString();
            double dbTopUpCountPast = Double.parseDouble(topUpCountPast);
            
		double dbTotalAmountCurrent = Double.parseDouble(vecCurrent.get(3).toString());
		double dbTotalAmountPast = Double.parseDouble(vecPast.get(3).toString());
            
		// DBSY-905 
            double dbBonusAmountPast = 0; 
            double dbTotalRechargesAmountPast = 0; 
            double dbBonusAmountCurrent = 0; 
            double dbTotalRechargesAmountCurrent  = 0; 
            
            if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
			dbBonusAmountPast = Double.parseDouble(vecPast.get(5).toString()); 
			dbTotalRechargesAmountPast = Double.parseDouble(vecPast.get(6).toString()); 
			dbBonusAmountCurrent = Double.parseDouble(vecCurrent.get(5).toString()); 
			dbTotalRechargesAmountCurrent = Double.parseDouble(vecCurrent.get(6).toString()); 
            }
        ///////    
            
		String locationCountCurrent = vecCurrent.get(4).toString();
            double dbLocationCountCurrent = Double.parseDouble(locationCountCurrent);
            
		String locationCountPast = vecPast.get(4).toString();
            double dbLocationCountPast = Double.parseDouble(locationCountPast);
            
		double dbl_percentTopUpCount = 0.0;
		double dbl_percentTotalAmount= 0.0;
		double dbl_percentBonusAmount= 0.0;
		double dbl_percentTotalRechargesAmount= 0.0;
		double dbl_percentNetAmount= 0.0;
		double dbl_percentTaxAmount= 0.0;
		double dbl_percentLocationCount = 0.0;
            
		// make sure you don't divide by zero
        if (dbTopUpCountPast != 0) {
        	dbl_percentTopUpCount = (dbTopUpCountCurrent - dbTopUpCountPast) / dbTopUpCountPast * 100;
        } else if(dbTopUpCountCurrent != 0){
        	dbl_percentTopUpCount = 100.0;
            }
        if (dbTotalAmountPast != 0) {
        	dbl_percentTotalAmount = (dbTotalAmountCurrent - dbTotalAmountPast) / dbTotalAmountPast * 100;
        } else if(dbTopUpCountCurrent != 0){
        	dbl_percentTotalAmount = 100.0;
            }
        if (dbBonusAmountPast != 0) {
        	dbl_percentBonusAmount = (dbBonusAmountCurrent - dbBonusAmountPast) / dbBonusAmountPast * 100;
        } else if(dbBonusAmountCurrent != 0){
        	dbl_percentBonusAmount = 100.0;
            }
        if (dbTotalRechargesAmountPast != 0) {
        	dbl_percentTotalRechargesAmount = (dbTotalRechargesAmountCurrent - dbTotalRechargesAmountPast) / dbTotalRechargesAmountPast * 100;
        } else if(dbTotalRechargesAmountCurrent != 0){
        	dbl_percentTotalRechargesAmount = 100.0;
            }
        if (dbLocationCountPast != 0) {
        	dbl_percentLocationCount = (dbLocationCountCurrent - dbLocationCountPast) / dbLocationCountPast * 100;
        } else if(dbLocationCountCurrent != 0){
        	dbl_percentLocationCount = 100.0;
            }
            
      	out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.date_range",SessionData.getLanguage())
      		+"</td><td  align=right nowrap>" + HTMLEncoder.encode(vecCurrent.get(1).toString()) + "</td><td  align=right nowrap>" 
      		+ HTMLEncoder.encode(vecPast.get(1).toString()) + "</td><td></td></tr>");
            
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.number_of_top_ups",SessionData.getLanguage())
      +"</td><td  align=right nowrap><a href=\"admin/reports/carrier_user/transactions/transaction_detail_by_phone.jsp?startDate="
      + URLEncoder.encode(CarrierReport.getStartDate(), "UTF-8") + "&endDate=" 
      + URLEncoder.encode(CarrierReport.getEndDate(), "UTF-8") + "&search=y&phoneNumber=" 
      + URLEncoder.encode(CarrierReport.getPhoneNumber(), "UTF-8") + "\" target=\"_blank\">" 
      + topUpCountCurrent+ "</a></td><td  align=right nowrap><a href=\"admin/reports/carrier_user/transactions/transaction_detail_by_phone.jsp?startDate="
      + URLEncoder.encode(DateUtil.addSubtractMonths(CarrierReport.getStartDate(),-1), "UTF-8") + "&endDate=" 
      + URLEncoder.encode(DateUtil.addSubtractMonths(CarrierReport.getEndDate(),-1), "UTF-8") + "&search=y&phoneNumber=" 
      + URLEncoder.encode(CarrierReport.getPhoneNumber(), "UTF-8") + "\" target=\"_blank\">" + topUpCountPast 
      		+ "</td><td nowrap align=\"right\">" + NumberUtil.formatAmount(String.valueOf(dbl_percentTopUpCount)) + "%</td></tr>");
      
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage())+
      "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTotalAmountCurrent))
      + "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTotalAmountPast))
      		+ "</td><td nowrap  align=right>" + NumberUtil.formatAmount(String.valueOf(dbl_percentTotalAmount)) + "%</td></tr>");
      
      if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
      	{
      
      //DBSY-905 bonus
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage())+
      "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbBonusAmountCurrent))
      + "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbBonusAmountPast) )
	      		+ "</td><td nowrap  align=right>" + dbl_percentBonusAmount + "%</td></tr>");
      
      //DBSY-905 total recharge
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage())+
      "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTotalRechargesAmountCurrent) )
      + "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTotalRechargesAmountPast) )
	      		+ "</td><td nowrap  align=right>" + dbl_percentTotalRechargesAmount + "%</td></tr>");
	 
      }
      
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.number_of_locations",SessionData.getLanguage())
      +"</td><td  align=right nowrap>" + locationCountCurrent+ "</td><td  align=right nowrap>" + locationCountPast 
      		+ "</td><td nowrap  align=right>" + dbl_percentLocationCount + "%</td></tr>");
      
      out.println("</table>");
      
      vecCurrent.clear();
      vecPast.clear();
	}
	else if (vecCurrent.size()==0 && request.getParameter("search") != null && searchErrors == null)
	{
		if(NoCarrierProductAttached)
        	out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_carriersattached_found",SessionData.getLanguage()) + "</font>");
      	else 
 			out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
	}
%>
          </td>
      </tr>
    </table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>