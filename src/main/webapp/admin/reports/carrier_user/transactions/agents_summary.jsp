<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 14;
  int section_page = 39;
  boolean NoCarrierProductAttached = false;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="CarrierReport" class="com.debisys.reports.CarrierReport" scope="request" />
  <jsp:setProperty name="CarrierReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;

  if (request.getParameter("search") != null)
  {
    if (CarrierReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));

      String strRepIds[] = request.getParameterValues("rids");

      if (strRepIds != null)
      {
        CarrierReport.setRepIds(strRepIds);
      }
	  
	  String carrierUserProds = SessionData.getProperty("carrierUserProds");
	  if(carrierUserProds!=null && !"".equals(carrierUserProds))
			vecSearchResults = CarrierReport.getAgentSummary(SessionData,application);
      else
      	 	NoCarrierProductAttached = true;
    }
    else
    {
      searchErrors = CarrierReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
        &nbsp;
        <%= Languages.getString("jsp.admin.reports.title1",SessionData.getLanguage()).toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">

<%
                            if (searchErrors != null)
                            {
                              out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString
                                      ("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

                              Enumeration enum1 = searchErrors.keys();

                              while (enum1.hasMoreElements())
                              {
                                String strKey   = enum1.nextElement().toString();
                                String strError = (String)searchErrors.get(strKey);

                                out.println("<li>" + strError);
                              }

                              out.println("</font></td></tr></table>");
                            }

                    if (vecSearchResults != null && vecSearchResults.size() > 0)
                    {
%>
                      <table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
%>
				        <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
				<tr>
                          <td class="main">

<%                          out.println(Languages.getString("jsp.admin.reports.transaction.agent_summary",SessionData.getLanguage()));
                            if (!CarrierReport.getStartDate().equals("") && !CarrierReport.getEndDate().equals(""))
                            {
                              out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(
                                      CarrierReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + 
                                      HTMLEncoder.encode(CarrierReport.getEndDateFormatted()));
                            }
%>
                            <br>
                            <font color="#ff0000">
                              <%= Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage()) %>
                            </font>
                          </td>
                        </tr>
                      </table>
                     <table>
                            <tr>
                                <td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
                            </tr>
                     </table>
                      <table width="100%" cellspacing="1" cellpadding="2">
                        <tr>
<%
                          String strSortURL = "admin/reports/carrier_user/transactions/agents_summary.jsp?search=y&startDate=" + 
                                  CarrierReport.getStartDate() + "&endDate=" + CarrierReport.getEndDate() + "&rids=" + 
                                  CarrierReport.getRepIds();
%>
                          <td class=rowhead2 valign=bottom>
                            #
                          </td>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.reports.business_name",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=1&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=1&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
<%
                if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
                {
%>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.customers.merchants_info.address",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=11&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=11&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=12&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=12&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=13&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=13&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=14&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=14&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
<%
                }
%>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=2&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=2&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=3&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=3&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=4&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=4&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
<%
						if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
						&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)){
%>
					<td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=15&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=15&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
                    <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=16&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=16&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
<% 
 }
                          if (strAccessLevel.equals(DebisysConstants.ISO))
                          {
%>
                            <td class=rowhead2 valign=bottom>
                              <%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                              <a href="<%= strSortURL %>&col=9&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                              <a href="<%= strSortURL %>&col=9&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                            </td>
<% 
                          }
                          if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) 
                                  && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                          {
%>
                            <td class=rowhead2 valign=bottom>
                              <%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                              <a href="<%= strSortURL %>&col=8&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                              <a href="<%= strSortURL %>&col=8&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                            </td>
<%
                          }
                          if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(
                                  DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                          {
%>
                            <td class=rowhead2 valign=bottom>
                              <%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                              <a href="<%= strSortURL %>&col=7&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                              <a href="<%= strSortURL %>&col=7&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                            </td>
<%
                          }
                          if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                          {
%>
                            <td class=rowhead2 valign=bottom>
                              <%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                              <a href="<%= strSortURL %>&col=6&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                              <a href="<%= strSortURL %>&col=6&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                            </td>
<% 
                          }
%>
                          <td class=rowhead2 valign=bottom>
                            <%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                            <a href="<%= strSortURL %>&col=5&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                            <a href="<%= strSortURL %>&col=5&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                          </td>
<%
                          if (strAccessLevel.equals(DebisysConstants.ISO))
                          {
%>
                            <td class=rowhead2 valign=bottom>
                              <%= Languages.getString("jsp.admin.reports.adjustment",SessionData.getLanguage()).toUpperCase() %>&nbsp;
                              <a href="<%= strSortURL %>&col=10&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                              <a href="<%= strSortURL %>&col=10&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
                            </td>
<%
                          }
                          out.println("</tr>");

                          double   dblTotalSalesSum         = 0;
                          double   dblMerchantCommissionSum = 0;
                          double   dblRepCommissionSum      = 0;
                          double   dblSubAgentCommissionSum = 0;
                          double   dblAgentCommissionSum    = 0;
                          double   dblISOCommissionSum      = 0;
                          double   dblAdjAmountSum          = 0;
                          
                  
                          //DBSY-905
		                  double dblbonus = 0;
		                  double dbltotalrecharge = 0;
		                  double dblnetamount = 0;
		                  
		                  
                          int      intTotalQtySum           = 0;
                          Iterator it                       = vecSearchResults.iterator();
                          int      intEvenOdd               = 1;
                          int      intCounter               = 1;

                          while (it.hasNext())
                          {
                            Vector vecTemp = null;

                            vecTemp = (Vector)it.next();

                            int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
                            double dblTotalSales         = Double.parseDouble(vecTemp.get(3).toString());
                            double dblMerchantCommission = Double.parseDouble(vecTemp.get(4).toString());
                            double dblRepCommission      = Double.parseDouble(vecTemp.get(5).toString());
                            double dblSubAgentCommission = Double.parseDouble(vecTemp.get(6).toString());
                            double dblAgentCommission    = Double.parseDouble(vecTemp.get(7).toString());
                            double dblISOCommission      = Double.parseDouble(vecTemp.get(8).toString());
                            double dblAdjAmount          = Double.parseDouble(vecTemp.get(9).toString());
                            
							
		                    
                            dblTotalSalesSum = dblTotalSalesSum + dblTotalSales;
                            dblMerchantCommissionSum = dblMerchantCommissionSum + dblMerchantCommission;
                            dblRepCommissionSum = dblRepCommissionSum + dblRepCommission;
                            dblAgentCommissionSum = dblAgentCommissionSum + dblAgentCommission;
                            dblSubAgentCommissionSum = dblSubAgentCommissionSum + dblSubAgentCommission;
                            dblISOCommissionSum = dblISOCommissionSum + dblISOCommission;
                            dblAdjAmountSum = dblAdjAmountSum + dblAdjAmount;
                            
		                    //DBSY-905
		                    if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
							&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
								dblbonus +=  Double.parseDouble(vecTemp.get(14).toString());
								dbltotalrecharge +=  Double.parseDouble(vecTemp.get(15).toString()); 
								
							}
                    
                            intTotalQtySum = intTotalQtySum + intTotalQty;

                            String sLocation = "";
                            if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
                            {
                              sLocation = "<td>" + vecTemp.get(10).toString() + "</td><td>" + vecTemp.get(11).toString() + "</td><td>" + vecTemp.get(12).toString() + "</td><td>" + vecTemp.get(13).toString() + "</td>";
                            }

                            out.println("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + "<td nowrap>" 
                                    + vecTemp.get(0) + "</td>" + sLocation + "<td>" + vecTemp.get(1) + "</td>" + "<td>" + intTotalQty + "</td>" 
                                    + "<td align=right><a href=\"admin/reports/carrier_user/transactions/agents_transactions.jsp?startDate=" + 
                                    URLEncoder.encode(CarrierReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(
                                    CarrierReport.getEndDate(), "UTF-8") + "&search=y&repId=" + vecTemp.get(1) + "&report=y\">" 
                                    + NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
 					//DBSY-905
                  if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
						out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(14).toString())  + "</td>");
						out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(15).toString())  + "</td>");
						
				  }
                            if (strAccessLevel.equals(DebisysConstants.ISO))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommission)) 
                                      + "</td>");
                             
                            }

                            if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
                                    ) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommission))
                                       + "</td>");
                              
                            }

                            if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(
                                    DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(
                                      dblSubAgentCommission)) + "</td>");
                             
                            }

                            if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommission)) 
                                      + "</td>");
                            }

                            out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommission)
                                    ) + "</td>");
								
								
                            if (strAccessLevel.equals(DebisysConstants.ISO))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmount)) + 
                                      "</td>");
                            }

                            out.println("</tr>");

                            if (intEvenOdd == 1)
                            {
                              intEvenOdd = 2;
                            }
                            else
                            {
                              intEvenOdd = 1;
                            }
                          }

                          vecSearchResults.clear();
%>
                          <tr class=row<%= intEvenOdd %>>
<%
                if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
                {
%>
                <td colspan=7 align=right>
<%
                }
                else
                {
%>
                <td colspan=3 align=right>
<%
                }
%>
                              <%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                            </td>
                            <td align=left>
                              <%= intTotalQtySum %>
                            </td>
                            <td align=right>
                              <%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %>
                            </td>
                            
                            <%//DBSY-905
 			 if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
 			 %>
                 <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblbonus))%></td>
				 <td align=right><%=NumberUtil.formatCurrency(Double.toString(dbltotalrecharge))%></td>
				 
						<%
			}
                            if (strAccessLevel.equals(DebisysConstants.ISO))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommissionSum)
                                      ) + "</td>");
                            }

                            if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
                                    ) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(
                                      dblAgentCommissionSum)) + "</td>");
                                    
                            }

                            if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(
                                    DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(
                                      dblSubAgentCommissionSum)) + "</td>");
                                
                            }

                            if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommissionSum)
                                      ) + "</td>");
                            
                            }

                            out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(
                                    dblMerchantCommissionSum)) + "</td>");
                                   
                            if (strAccessLevel.equals(DebisysConstants.ISO))
                            {
                              out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmountSum)) + 
                                      "</td>");
                            }

                            out.println("</tr></table>");
                          }
                          else
                          if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
                          {
                          if(NoCarrierProductAttached)
                          		out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_carriersattached_found",SessionData.getLanguage()) + 
                                    "</font>");
                          else
                            	out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + 
                                    "</font>");
                          }
%>
                        </td>
                      </tr>
                    </table>
        <%@ include file="/includes/footer.jsp" %>