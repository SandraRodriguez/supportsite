<%@page import="com.debisys.utils.HTMLEncoder, com.debisys.presentation.CarrierReportGroup,com.debisys.languages.Languages"%>
<%
	int section=14;
	int section_page=7;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%

CarrierReportGroup a = CarrierReportGroup.createAnalysisGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType,customConfigType,0);
CarrierReportGroup t = CarrierReportGroup.createTransactionsGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType,0);
CarrierReportGroup aa = CarrierReportGroup.createCarrierGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType,customConfigType,0);
CarrierReportGroup tt = CarrierReportGroup.createRoamingGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType,0);

%>

          <TABLE cellSpacing=0 cellPadding=0 width="750" 
            background=images/top_blue.gif border=0>
              <TBODY>
              <TR>
                <TD width=23 height=20><IMG height=20 
                  src="images/top_left_blue.gif" 
width=18></TD>
                <TD width="3000" class=formAreaTitle><%=Languages.getString("jsp.admin.reports.carrier_user.reports",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD height=20><IMG 
                  src="images/top_right_blue.gif"></TD></TR>
              <TR>
                <TD colSpan=3>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" 
                  bgColor=#7B9EBD border=0>
                    <TBODY>
                    <TR>
                      <TD width=1 bgColor=#003082><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top align=left bgColor=#ffffff>
                        <TABLE width="100%" border=0 
                        align=left cellPadding=2 cellSpacing=0 class="fondoceldas">
                          <TBODY>
                          <TR>
	     				    	<td>
	     				    	<%
	if ( t.hasItems() )
	{
%>
                              <TABLE class="reportGroupTable"  id="reportGroupTable">
                                <TBODY>
                                <TR>
                                <TH align=left><IMG height=22 
                                src="images/transaction_cube.png" 
                                width=22 border=0><span class="main">
                                <%=Languages.getString("jsp.admin.reports.transactions",SessionData.getLanguage()).toUpperCase()%>
                                </SPAN>
                                </TH></TR>
                                <TR>
                                <TD>
                                <UL class="sublevel" id="sublevel" name="main">
                                <%t.showItem(out);%>
                                </UL></TD></TR>
                                </TBODY></TABLE>
                                
<%
	}
	
	if(!SessionData.checkPermission(DebisysConstants.PERM_CS_REP_CARRIER_VIEW)) // The new permission should only show the transaction summary report for the carrier view
	{
%>
                                <table class="reportGroupTable" id="reportGroupTable">
                                <TBODY>
                                <TR>
                                <TH align=left><IMG height=22 
                                src="images/analysis_cube.png" 
                                width=22 border=0> <span class="main">
                                <%=Languages.getString("jsp.admin.reports.analysis",SessionData.getLanguage()).toUpperCase() %></TH></TR>
                                <TR>
                                <TD>
                                <UL class="sublevel" id="sublevel" name="main">
                                <%a.showItem(out); %>
                                </span>
                                </LI></UL></TD></TR>
                                </TBODY></TABLE>
                                
	     				    	
                                <TABLE class=reportGroupTable>
                                <TBODY>
                                <TR>
                                <TH align=left><IMG height=22 
                                src="images/carrier.png" 
                                width=22 border=0> <span class="main">
                                <%=Languages.getString("jsp.admin.reports.carrier_user.carrier",SessionData.getLanguage()).toUpperCase() %></TH></TR>
                                <TR>
                                <TD>
                                <UL class=sublevel name="main">
                                <% aa.showItem(out); %>
                                </SPAN>
                                </LI></UL></TD></TR>
                                </TBODY></TABLE>
                                
									
                                <TABLE class=reportGroupTable>
                                <TBODY>
                                <TR>
                                <TH align=left><IMG height=22 
                                src="images/roaming.png" 
                                width=22 border=0> <span class="main">
                                <%=Languages.getString("jsp.includes.menu.roaming_etopups",SessionData.getLanguage()).toUpperCase() %></TH></TR>
                                <TR>
                                <TD>
                                <UL class=sublevel name="main">
                                <% tt.showItem(out); %>
                                </SPAN>
                                </LI></UL></TD></TR>
                                </TBODY></TABLE>
<%
	}
%>

                                </td>
	     				    	</tr>
		       			</table>
	    			</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
		  		</tr>
		  		<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
		  		</tr>
	 		</table>
	 	</td>
	</tr>
</table>


<%@ include file="/includes/footer.jsp" %>