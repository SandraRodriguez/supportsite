<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.DateUtil,
                 com.debisys.reports.jfreechart.Chart,
                 java.io.PrintWriter,
                 com.debisys.utils.NumberUtil" %>
<%
	int section=14;
	int section_page=9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
	Vector<Vector<String>> vecSearchResults = new Vector<Vector<String>>();
	Hashtable searchErrors = new Hashtable();
	String isGraph = request.getParameter("graph");
	
	if (isGraph == null || (!isGraph.equalsIgnoreCase("y") && !isGraph.equalsIgnoreCase("n")))
	{
		isGraph = "";
	}
	
	if ( request.getParameter("download") != null )
	{
	  	String sURL = "";
    	SessionData.setProperty("start_date", request.getParameter("startDate"));
    	SessionData.setProperty("end_date", request.getParameter("endDate"));	
  	  	sURL = TransactionReport.downloadDailyVolume(application, SessionData);
    	response.sendRedirect(sURL);
	  	return;
	}
	
	if (request.getParameter("search") != null)
	{
		if (DateUtil.isValid(request.getParameter("startDate")))
		{
			SessionData.setProperty("start_date", request.getParameter("startDate"));
			TransactionReport.setEndDate(DateUtil.addSubtractMonths(request.getParameter("startDate"),1));
			vecSearchResults = TransactionReport.getCarrierUserDailyVolume(SessionData);
		}
		else
		{
			searchErrors.put("startDate",Languages.getString("com.debisys.reports.error2",SessionData.getLanguage()));
		}
	}
%>
<%@ include file="/includes/header.jsp" %>
<script>
function PrintReport()
{
	var divBanner = document.getElementById("divPrintBanner");
	var divMenu = document.getElementById("divPrintMenu");
	var divButton = document.getElementById("divPrintButton");
	
	if ( divBanner != null )
	{
		divBanner.style.display = "none";
	}
	
	if ( divMenu != null )
	{
		divMenu.style.display = "none";
	}
	
	//divButton.style.display = "none";
	window.print();
	//divButton.style.display = "inline";
	
	if ( divMenu != null )
	{
		divMenu.style.display = "inline";
	}
	
	if ( divBanner != null )
	{
		divBanner.style.display = "inline";
	}
}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title9",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<form name="mainform" method="get" action="admin/reports/carrier_user/analysis/volume_daily.jsp" onSubmit="scroll()">
		          		<table width="300">
<%
	if(SessionData.getProperty("carrierUserProds") == null || SessionData.getProperty("carrierUserProds").equals(""))
	{
%>
							<tr class=main>
								<td colspan=2><font color="red">NOTE: This Carrier User does not have any products allowed for report viewing.</font></td>
							</tr>
<%		
	}
%>
			               	<tr>
				               	<td nowrap>
									<table width=400>
<%
	if (searchErrors.size() > 0)
	{
  		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1=searchErrors.keys();

		while(enum1.hasMoreElements())
		{
  			String strKey = enum1.nextElement().toString();
  			String strError = (String) searchErrors.get(strKey);
  			out.println("<li>" + strError);
		}

  		out.println("</font></td></tr>");
	}
%>
										<tr>
											<td colspan=2 class="main" nowrap>
												* <%=Languages.getString("jsp.admin.reports.analysis.volume_daily.instructions",SessionData.getLanguage())%>
											</td>
										</tr>
										<tr>
									    	<td class="main" nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>: <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.mainform.startDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
									    	<td class="main">
									      		<input type="hidden" name="search" value="y">
									    	</td>
										</tr>
										<tr>
											<td colspan=2 class=main>
												<input type=radio name="graph" value="y" <%if (isGraph.equalsIgnoreCase("y")||isGraph.equals("")) {out.println("checked");} %>><%=Languages.getString("jsp.admin.reports.analysis.graph",SessionData.getLanguage())%>&nbsp;&nbsp;<input type=radio name="graph" value="n" <%if (isGraph.equalsIgnoreCase("n")) {out.println("checked");}%>><%=Languages.getString("jsp.admin.reports.analysis.table",SessionData.getLanguage())%>
											</td>
										</tr>
										<tr>
											<td>
												<table>
													<tr>
														<td>
															<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.analysis.volume_daily.submit",SessionData.getLanguage())%>">
														</td>
		              									</form>
<%
	if (request.getParameter("search") != null && vecSearchResults != null && vecSearchResults.size() > 0 && isGraph.equals("y"))
	{
		// Print the page since it is a graph
%>
														<td>
															<input type=button value="<%=Languages.getString("jsp.admin.reports.carrier_user.analysis.tot_vol_dly.print",SessionData.getLanguage())%>" onclick="PrintReport();">
														</td>
<%
	} else if (request.getParameter("search") != null && vecSearchResults != null && vecSearchResults.size() > 0 && isGraph.equals("n")) 
	{
		// Download to Excel
%>
														<td>
															<form method=post action="admin/reports/carrier_user/analysis/volume_daily.jsp">
								                				<input type="hidden" name=startDate value="<%=request.getParameter("startDate")%>">
								                				<input type="hidden" name=endDate value="<%=request.getParameter("endDate")%>">  
	       														<input type="hidden" name="download" value="y">
	       														<input type=submit value="<%=Languages.getString("jsp.admin.reports.carrier_user.analysis.tot_vol_dly.download",SessionData.getLanguage())%>">
															</form>
														</td>
<%
	} 
%>										
													</tr>
												</table>
											</td>
										</tr>
									</table>
			               		</td>
		              		</tr>
		            	</table>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
    	if (isGraph.equals("") || isGraph.equalsIgnoreCase("y"))
    	{
    		Hashtable hashSettings = new Hashtable();
    		hashSettings.put("data", vecSearchResults);
    		hashSettings.put("title", Languages.getString("jsp.admin.reports.title23", new Object[]{SessionData.getProperty("company_name").trim(),TransactionReport.getStartDateFormatted(),TransactionReport.getEndDateFormatted()},SessionData.getLanguage()));
    		hashSettings.put("category_label",Languages.getString("jsp.admin.reports.days",SessionData.getLanguage()));
    		hashSettings.put("value_label",Languages.getString("jsp.admin.reports.transaction_amounts",SessionData.getLanguage()));
    		hashSettings.put("width", "745");
    		hashSettings.put("height", "600");
	  		String filename = Chart.generateBarChart(hashSettings, session,  new PrintWriter(out));

  			String graphURL = request.getContextPath() + "/servlets/DisplayChart?filename=" + filename;
%>
						<table width="100%" cellspacing="0" cellpadding="0" align=center border=0 height=400>
				            <tr>
				            	<td align=center><img src="<%= graphURL %>" width=745 height=600 border=0 usemap="#<%= filename %>"></td>
				            </tr>
						</table>
<%
    	}
    	else
    	{
%>
				      	<table width="300" cellspacing="1" cellpadding="1" border="0" align=center>
				      		<tr>
				      			<td class=rowhead2 width="100"><%=Languages.getString("jsp.admin.reports.analysis.volume_daily.date",SessionData.getLanguage()).toUpperCase()%></td>
				      			<td class=rowhead2><%=Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase()%></td>
				      			<td class=rowhead2><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%></td>
							</tr>
<%
	      	int intEvenOdd = 1;
	      	double dblTotalSales = 0;
	      	int intTotalQty = 0;
	      	Iterator<Vector<String>> it = vecSearchResults.iterator();
	      
	      	while (it.hasNext())
	     	{
	        	Vector<String> vecTemp = null;
	        	vecTemp = (Vector<String>) it.next();
	        	dblTotalSales = dblTotalSales + Double.parseDouble(vecTemp.get(1));
	        	intTotalQty = intTotalQty + Integer.parseInt(vecTemp.get(3));
	
	        	out.println("<tr class=row" + intEvenOdd + ">" +
					"<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2) + "/" + vecTemp.get(0)) + "</td>" +
					"<td nowrap align=right>" + vecTemp.get(3) + "</td>" +
					"<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(1)) + "</td></tr>");
	
				if (intEvenOdd == 1)
					intEvenOdd = 2;
				else
                    intEvenOdd = 1;
      		}
      		
	      	out.println("<tr class=row" + intEvenOdd + "><td><b>"+Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())+":</td><td align=right><b>"+intTotalQty+"</td><td align=right><b>" +NumberUtil.formatCurrency(Double.toString(dblTotalSales))+ "</td></tr>");
                  vecSearchResults.clear();
%>
						</table>
<%
    	}
	}
	else if (vecSearchResults.size()==0 && request.getParameter("search") != null)
	{
%>
						<table width="100%">
			               	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.reports.error6",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>