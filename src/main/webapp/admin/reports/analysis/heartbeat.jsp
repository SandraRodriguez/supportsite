<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.DateUtil,
                 com.debisys.reports.jfreechart.Chart,
                 java.io.PrintWriter,
                 com.debisys.utils.NumberUtil" %>
<%
int section= 4;
int section_page=41; 
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%
Vector vecSearchResults = new Vector();
Vector vecTerminalsList= new Vector();
String sDateOptions="1";
String[] strTerminalTypes={""};
	
if (request.getParameter("search") != null)
{
	if (request.getParameter("dateOptions") != null ) {
  		sDateOptions = request.getParameter("dateOptions");
  		//sTerminals= request.getParameter("terminals");
  	}
  	strTerminalTypes = request.getParameterValues("terminals");
  	  if (strTerminalTypes != null)
      {
        TransactionReport.setTerminalTypes(strTerminalTypes);
	 	//TransactionReport.setTerminalTypes(request.getParameter("terminalTypes"));
	 	}
	 vecSearchResults = TransactionReport.getHeartBeatData(request.getParameter("dateOptions"),request.getParameterValues("terminals"),SessionData);
	 vecTerminalsList.clear();
	   
    

}
 %>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}

</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.heartbeat_title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>	
      <table border="0" width="100%" cellpadding="0" cellspacing="0">      
     	<tr>
	        <td class="formArea2">
	        <form name="mainform" method="post" action="admin/reports/analysis/heartbeat.jsp" onSubmit="scroll();">
	        <input type="hidden" name="terminalTypes" value="<%=TransactionReport.getTerminalTypes()%>">
	          <table width="300">
               <tr class="main">
               <td valign="top" nowrap>
					<table width=400>
					<tr class="main">
                        <td nowrap valign="top">
                          <%=Languages.getString("jsp.admin.reports.heartbeat.date",SessionData.getLanguage())%>
                        </td>
                         <td>
                          &nbsp;
                        </td>
                         </tr>
                        <tr>
                         <tr class="main">
                              <td nowrap>
                           <%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>
                                </td>
                       			 <td>
                          	 <select name="dateOptions" size="10" multiple>
                            <option value="1" <%=sDateOptions.equals("1")?"selected":""%>><%=Languages.getString("jsp.admin.reports.heartbeat.last5min",SessionData.getLanguage())%></option>
                             <option value="2" <%=sDateOptions.equals("2")?"selected":""%>><%=Languages.getString("jsp.admin.reports.heartbeat.last1Hr",SessionData.getLanguage())%></option>
                              <option value="3" <%=sDateOptions.equals("3")?"selected":""%>><%=Languages.getString("jsp.admin.reports.heartbeat.last24Hr",SessionData.getLanguage())%></option>
                               <option value="4" <%=sDateOptions.equals("4")?"selected":""%>><%=Languages.getString("jsp.admin.reports.heartbeat.lastMonth",SessionData.getLanguage())%></option>
                                <option value="5" <%=sDateOptions.equals("5")?"selected":""%>><%=Languages.getString("jsp.admin.reports.heartbeat.last3Months",SessionData.getLanguage())%></option>
                        </td>
                      </tr>
					
					<tr>
				    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.terminals.option",SessionData.getLanguage())%></td>
				    <td class=main valign=top>
				        <select name="terminals" size="10" multiple>
				          <option value="" <%=strTerminalTypes[0].equals("")?"selected":""%>><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%
				  vecTerminalsList = TransactionReport.getTerminalTypesList(SessionData);
				  Iterator it = vecTerminalsList.iterator();
				  while (it.hasNext())
				  {
				    Vector vecTemp = null;
				    vecTemp = (Vector) it.next();
				    for(int i=0;i<strTerminalTypes.length;i++)
				    {
					    if(strTerminalTypes[i].equals(vecTemp.get(0)))
					    {
					    	 out.println("<option value=" + vecTemp.get(0) +" 'selected'>" + vecTemp.get(1) + "</option>");
					    }else
					    {
					      out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
					    }
				    }
				    
				   
				  }
				%>
				        </select>
				        <br>
				        *<%=Languages.getString("jsp.admin.reports.transactions.terminals.instructions",SessionData.getLanguage())%>
				</td>
				</tr>
				<%
				
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
    
    Hashtable hashSettings = new Hashtable();
    hashSettings.put("data", vecSearchResults);
     hashSettings.put("title","Heartbeat Transctions for " + SessionData.getProperty("company_name").trim() + " from " + TransactionReport.getStartDate());
    hashSettings.put("category_label","DateTime");
    hashSettings.put("value_label","Number of HeartBeat Transactions");
     hashSettings.put("width", "745");
    hashSettings.put("height", "600");
    hashSettings.put("showlabels", "Y");
    String filename = Chart.generateHeartbeatBarChart(hashSettings, session,  new PrintWriter(out));

    String graphURL = request.getContextPath() + "/servlets/DisplayChart?filename=" + filename;
%>
      <table width="100%" cellspacing="0" cellpadding="0" align="center" border="0" height="400">
        <tr>
          <td align="center">
            <img src="<%= graphURL %>" width="745" height="600" border="0" usemap="#<%= filename %>">
          </td>
        </tr>
      </table>
<%

}
else if (vecSearchResults.size()==0 && request.getParameter("search") != null)
{%>
<table align="center" border="0">
        <tr>
          <td align="center">
          <%
 				out.println("<br><br><font color=ff0000>No data found.</font>"); %>
 			 </td>
 			 </tr>
 			 </table>
<%}
%>
				
					
					<tr>
					    <td class=main colspan=2 align=center>
					      <input type="hidden" name="search" value="y">
					       <input type="hidden" name="result" value="y">
					      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
					    </td>
					</tr>
					<tr>
					<td class="main" colspan=2>

					</td>
					</tr>
					</table>
               </td>
              </tr>              
            </table>
			</form>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>