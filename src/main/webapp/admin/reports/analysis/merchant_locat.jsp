<%-- 
    Document   : merchant_locator
    Created on : Apr 9, 2015, 9:35:58 AM
    Author     : nmartinez
--%>

<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 4;
    int section_page = 61;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="/support/js/dragUtil.js"></script>

<%    String strRefId = SessionData.getProperty("ref_id");
    String sessionLanguage = SessionData.getUser().getLanguageCode();
    String reportCode = "MerchantMapLocator";

    ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport(reportCode, sessionLanguage, null);

    String statusLabel                 = "status";
    String titleReportLabel            = "merchantMapLocatorTitle";
    String activeLabel                 = "active";
    String cancelledLabel              = "cancelled";
    String disabledLabel               = "disabled";
    String activeTrxLabel              = "activeTrx";
    String periodDaysLabel             = "days";
    String periodWeekLabel             = "week";
    String periodMonthLabel            = "month";
    String numTrxToConsiderActiveLabel = "numTrxToConsiderActiveLabel";
    String howDaysBack                 = "howDaysBack";
    String howWeeksBack                = "howWeeksBack";
    String howMonthsBack               = "howMonthsBack";
    String drag                        = "drag";
    String dragTitle                   = "dragTitle";
    String close                       = "close";
    String closeTitle                  = "closeTitle";
    String decrement                   = "decrement";
    String increment                   = "increment";
    String transparency                = "transparency";
    String goCenter                    = "goCenter";
    String goCenterTitle               = "goCenterTitle";
    
    /*DEFAULT VALUES*/
    HashMap parametersName = new HashMap();
    parametersName.put(titleReportLabel, "Merchant Map Locator");
    parametersName.put(statusLabel, "Status");
    parametersName.put(activeLabel, "Active");
    parametersName.put(cancelledLabel, "Cancelled");
    parametersName.put(disabledLabel, "Disabled");
    parametersName.put(activeTrxLabel, "Active Trx");
    parametersName.put(periodDaysLabel, "Days");
    parametersName.put(periodWeekLabel, "Weeks");
    parametersName.put(periodMonthLabel, "Months");
    parametersName.put(numTrxToConsiderActiveLabel, "#Trxs to consider merchant active1");
    parametersName.put(howDaysBack, "howDaysBack");
    parametersName.put(howWeeksBack, "howWeeksBack");
    parametersName.put(howMonthsBack, "howMonthsBack");
    parametersName.put(drag, "Drag here");
    parametersName.put(dragTitle, "To move this window");
    parametersName.put(close, "Close Map");
    parametersName.put(closeTitle, "Click to close the map");
    parametersName.put(decrement, "To decrement transparency");
    parametersName.put(increment, "To increment transparency");
    parametersName.put(transparency, "Transparency");
    parametersName.put(goCenter, "Go center");
    parametersName.put(goCenterTitle, "Click to go the main center");
    
    /*DEFAULT VALUES*/

    for (ResourceReport resource : resources) {
        if (parametersName.containsKey(resource.getKeyMessage())) {
            parametersName.put(resource.getKeyMessage(), resource.getMessage());
        }
    }

    ArrayList<PropertiesByFeature> propertiesByFeature = PropertiesByFeature.findPropertiesByFeature(reportCode);

    String latitudeValue       = "latitude";
    String longitudeValue      = "longitude";
    String activeTrxColorValue = "activeTrxColor";
    String disabledColorValue  = "disabledColor";
    String activeColorValue    = "activeColor";
    String cancelledColorValue = "cancelledColor";
    String numActTrxValue      = "numActTrx";
    String zoomValue      = "zoom";
    
    /*DEFAULT VALUES*/
    HashMap propertiesValues = new HashMap();
    propertiesValues.put(activeTrxColorValue, "4DFFF9");
    propertiesValues.put(disabledColorValue, "FFA7A1");
    propertiesValues.put(activeColorValue, "C2FE68");
    propertiesValues.put(cancelledColorValue, "FF3300");
    propertiesValues.put(numActTrxValue, "500");
    propertiesValues.put(latitudeValue, "25.05");
    propertiesValues.put(longitudeValue, "-77.33");
    propertiesValues.put(zoomValue, "12");
    /*DEFAULT VALUES*/

    for (PropertiesByFeature property : propertiesByFeature) {
        if (propertiesValues.containsKey(property.getProperty())) {
            propertiesValues.put(property.getProperty(), property.getValue());
        }
    }

    Integer unitValue = DbUtil.getReportIntervalLimitDays(reportCode, application);

%>

<script LANGUAGE="JavaScript">
    var count = 0;
    
    var zoomDefault   = <%=propertiesValues.get(zoomValue)%>;;
    var drag          = "<%=parametersName.get(drag)%>";
    var dragTitle     = "<%=parametersName.get(dragTitle)%>";
    var close         = "<%=parametersName.get(close)%>";
    var closeTitle    = "<%=parametersName.get(closeTitle)%>";
    var decrement     = "<%=parametersName.get(decrement)%>";
    var increment     = "<%=parametersName.get(increment)%>";
    var transparency  = "<%=parametersName.get(transparency)%>";
    var goCenter      = "<%=parametersName.get(goCenter)%>";
    var goCenterTitle = "<%=parametersName.get(goCenterTitle)%>";
    
    var map = null;
    var markers = [];
    var centerControlDiv;
    //                    0    1    2      3      4     5     6     7     8     9    10   
    var opacityValues = ["0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1"];
    var opacityPointer = 10;
    var statusWindowMap = 0;
    
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ", "< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");

    function scroll()
    {
        document.mainform.submit.disabled = true;
        document.mainform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll()', 150);
    }

    function scroll2()
    {
        document.downloadform.submit.disabled = true;
        document.downloadform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll2()', 150);
    }

    var markers = [];

    function entityLocation(merchantId, latitude, longitude)
    {
        this.merchantId = merchantId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    function doReport()
    {
        initialize();
        var waitingControl = "<img src='images/global-wait.gif'>";
        var currentIds = $("#currentSelectedIds").val();
        var url = "/support/admin/customers/merchants/data_entity.jsp";

        var statusMerchants = $("#status").val().toString();
        var period = $("#period").val();
        var unitNumber = $("#unitNumber").val();
        var trxNumber = $("#trxNumber").val();

        var w = $(window).width();
        var h = $(window).height();
        $("#map-canvas").width(w);
        $("#map-canvas").height(h);
        $("#map-canvas").css({"top": "1px", "left": "1px", "position": "absolute"});

        setAllMap();
        $("#statusImages").html(waitingControl);
        $.post(
                url,
                {
                    ids: currentIds,
                    type: <%=DebisysConstants.MERCHANT%>,
                    responseType: "json",
                    rnd: Math.random(),
                    statusmerchants: statusMerchants,
                    period: period,
                    unitNumber: unitNumber,
                    trxNumber: trxNumber,
                },
                function (data)
                {
                    $("#statusImages").empty();
                    var images = [];
                    var imagesInfo = "";
                    var dataTrim = data;
                    $.each(dataTrim.items, function (i, item) {
                        //var merchantId = item.merchantId;
                        var latitude = item.latitude;
                        var longitude = item.longitude;
                        var cancelled = item.cancelled;
                        var datecancelled = item.datecancelled;
                        var dba = unescape(item.dba);
                        var trx = item.trx;
                        var businessname = unescape(item.businessname);
                        var additional = "";

                        var pinColor = "<%=propertiesValues.get(activeColorValue)%>";
                        var statusName = "<%=parametersName.get(activeLabel)%>";

                        if (cancelled === "1")
                        {
                            pinColor = "<%=propertiesValues.get(cancelledColorValue)%>";
                            statusName = "<%=parametersName.get(cancelledLabel)%>";
                        }
                        else if (datecancelled.length > 0 && cancelled === "0")
                        {
                            pinColor = "<%=propertiesValues.get(disabledColorValue)%>";
                            statusName = "<%=parametersName.get(disabledLabel)%>";
                        }
                        else if (trx > 0)
                        {
                            pinColor = "<%=propertiesValues.get(activeTrxColorValue)%>";
                            additional = " Trx=[" + trx + "]";
                            statusName = "<%=parametersName.get(activeTrxLabel)%>";
                        }

                        var title = dba + " Rep=[" + businessname + "] " + additional;
                        var urlImage = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor;

                        var pinImage = new google.maps.MarkerImage(urlImage,
                                new google.maps.Size(20, 30),
                                new google.maps.Point(0, 0),
                                new google.maps.Point(10, 34));
                        if (images.indexOf(urlImage) === -1) {
                            imagesInfo = imagesInfo + statusName + ":<img  src='" + urlImage + "' height='25px'/>";
                        }
                        images.push(urlImage);
                        var latLng = new google.maps.LatLng(latitude, longitude);
                        addMarker(latLng, pinImage, title);
                    });
                    addTable(imagesInfo);
                },
                "json"
                );
        $("#map-canvas").show();
    }

    function addTable(imagesHtml) {
        if ($("#mainTable") != null) {
            $("#mainTable").remove();
        }        
        var table = document.createElement('TABLE');
        table.id = 'mainTable';
        table.style.width = '1150px';
        var tableBody = document.createElement('TBODY');
        table.appendChild(tableBody);
        var tr = document.createElement('TR');
        tableBody.appendChild(tr);

        createTD(tr, 'dragControlLocator', drag, dragTitle, '');
        createTD(tr, 'imagesPins', '', '', imagesHtml);
        createTD(tr, 'opacity', transparency, '', '');
        createTD(tr, 'goToCenter', goCenter, goCenterTitle, '');
        createTD(tr, 'closeMap', close, closeTitle, '');
        
        if ( centerControlDiv!=null )
            centerControlDiv.appendChild(table);   
        else 
            alert("fail adding table/div");
    }


    function createTD(tr, id, text, title, innerHTML) {
        var td = document.createElement('TD');        
        var controlUI = document.createElement('div');
        controlUI.id = id;        
        controlUI.title = title;
        controlUI.innerHTML = text;
        applyStyleControl(controlUI);
        
        if (id === "dragControlLocator") {            
            controlUI.style.cursor = 'pointer';
            controlUI.onmousedown = function () {
                _drag_init(document.getElementById('map-canvas'));
                return false;
            };
        } else if (id === "closeMap") {            
            controlUI.style.cursor = 'pointer';
            statusWindowMap = 1;
            controlUI.addEventListener('click', function () {
                $("#map-canvas").hide();
            });
        } else if (id === "imagesPins") {            
            controlUI.innerHTML = innerHTML;
        } else if (id === "opacity") {            
            var controlUI_Plus = document.createElement('button');
            applyStyleControl(controlUI_Plus);
            controlUI_Plus.title = increment; 
            controlUI_Plus.innerHTML = '-';
            controlUI.appendChild(controlUI_Plus);
            controlUI_Plus.addEventListener('click', function () {
                opacityControl("+");
            });
            var controlUI_Minus = document.createElement('button');
            applyStyleControl(controlUI_Minus);
            controlUI_Minus.title = decrement;
            controlUI_Minus.innerHTML = '+';
            controlUI.appendChild(controlUI_Minus);
            controlUI_Minus.addEventListener('click', function () {
                opacityControl("-");
            });
        } else if (id === "goToCenter") {            
            controlUI.style.cursor = 'pointer';
            controlUI.addEventListener('click', function () {
                setDefaultCenter();
            });
        } 
        td.appendChild(controlUI);
        tr.appendChild(td);
    }  

    function applyStyleControl(controlUI){
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.height = '30px';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';        
    }
    
    function opacityControl(action) {
        if (action === "+") {
            if (opacityPointer < 10) {
                opacityPointer = opacityPointer + 1;
            }
        } else {
            if (opacityPointer <= 10 && opacityPointer > 2) {
                opacityPointer = opacityPointer - 1;
            } else {
                opacityPointer = 2;
            }
        }
        $("#map-canvas").css({"opacity": opacityValues[opacityPointer]});
    }

    function setMapZoomValue(){
        map.setZoom(zoomDefault);
    }
    
    function setDefaultCenter(){
        map.setCenter(new google.maps.LatLng(<%=propertiesValues.get(latitudeValue)%>,<%=propertiesValues.get(longitudeValue)%>));   
        setMapZoomValue();
    }
    
    function initialize()
    {
        var mapOptions = {
            center: new google.maps.LatLng(<%=propertiesValues.get(latitudeValue)%>,<%=propertiesValues.get(longitudeValue)%>),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        if (map === null) {
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            centerControlDiv = document.createElement('div');
            centerControlDiv.id = "centerControlDiv";
            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.LEFT_TOP].push(centerControlDiv);
        }
        setDefaultCenter();                
    }

    function addMarker(location, pinImage, title)
    {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: pinImage,
            title: title
        });
        markers.push(marker);
    }

    function setAllMap(map)
    {
        for (var i = 0; i < markers.length; i++)
        {
            markers[i].setMap(map);
        }
    }

    function statusEvent()
    {
        var status = $("#status").val();
        if (status === "AT" || status.indexOf("AT") >= 0)
        {
            $("#tdActiveControls1").show();
            $("#tdActiveControls2").show();
            $("#tdActiveControls3").show();
        }
        else
        {
            $("#tdActiveControls1").hide();
            $("#tdActiveControls2").hide();
            $("#tdActiveControls3").hide();
        }
    }

    var currentMax;

    function calculateByPeriod()
    {
        var period = $("#period").val();
        var unitNumberHidden = $("#unitNumberHidden").val();

        if (period === "day")
        {
            $("#unitNumber").val(unitNumberHidden);
            currentMax = unitNumberHidden;
            $("#spanHow").text("<%=parametersName.get(howDaysBack)%>");
        }
        else if (period === "week")
        {
            var remainder = unitNumberHidden % 7;
            var weeks = (unitNumberHidden - remainder) / 7;
            currentMax = weeks;
            $("#spanHow").text("<%=parametersName.get(howWeeksBack)%>");
        }
        else if (period === "month")
        {
            var remainder = unitNumberHidden % 30;
            var months = (unitNumberHidden - remainder) / 30;
            currentMax = months;
            $("#spanHow").text("<%=parametersName.get(howMonthsBack)%>");
        }
        $("#unitNumber").val(currentMax);
    }

    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    $(function () {
        $(".numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            $(".error").css("display", ret ? "none" : "inline");
            return ret;
        });
        $(".numeric").bind("keyup", function (e) {
            if ($(this).val() > currentMax)
            {
                $(this).val(1);
                $(this).focus();
            }

            //alert( $(this).val() );
        });
        $(".numeric").bind("paste", function (e) {
            return false;
        });
        $(".numeric").bind("drop", function (e) {
            return false;
        });
        $("#trxNumber").bind("keyup", function (e) {
            if ($(this).val() > $("#trxNumberHidden").val() || $(this).val() == 0)
            {
                $(this).val($("#trxNumberHidden").val());
            }
        });
    });

    $(document).ready(function () {
        latitudeRegion = <%=propertiesValues.get(latitudeValue)%>;
        longitudeeRegion = <%=propertiesValues.get(longitudeValue)%>;
        $("#tdActiveControls1").hide();
        $("#tdActiveControls2").hide();
        $("#tdActiveControls3").hide();
    });

</script>
<link rel="stylesheet" type="text/css" href="css/maps.css" title="style">



<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=parametersName.get(titleReportLabel).toString().toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
</tr>
<tr>
<td colspan="3"  bgcolor="#FFFFFF">            
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
        <td class="formArea2">                                        
            <table width="1500px">                                                        
                <tr>
                <td>
                    <jsp:include page="/admin/customers/merchants/entity_combo_selector.jsp" >
                        <jsp:param value="<%=strRefId%>" name="strRefId" />
                        <jsp:param value="<%=strAccessLevel%>" name="strAccessLevel" />
                        <jsp:param value="<%=strDistChainType%>" name="strDistChainType" />                                            
                    </jsp:include>                                                
                </td>                                                            
                </tr>
                <tr>
                <td>
                    <table style="border: 0px solid #ccc; border-collapse: separate; border-spacing: 20px;">
                        <tr style="vertical-align: top">
                        <td id="tdActiveControls0">
                            <%=parametersName.get(statusLabel).toString()%> <br/>
                            <select id="status" name="status" size="4" onchange="statusEvent();" multiple >                                                         
                                <option value="A" selected ><%=parametersName.get(activeLabel).toString()%></option>
                                <option value="C"><%=parametersName.get(cancelledLabel).toString()%></option>
                                <option value="D" ><%=parametersName.get(disabledLabel).toString()%></option>
                                <option value="AT" ><%=parametersName.get(activeTrxLabel).toString()%></option>
                            </select>                                             
                        </td>                                                                                                                       
                        <td id="tdActiveControls1"><span id="spanHow"><%=parametersName.get(howDaysBack)%></span>
                        <br/> 
                        <select id="period" name="status" size="3" onchange="calculateByPeriod();" >                                                                       
                            <option value="day" selected><%=parametersName.get(periodDaysLabel).toString()%></option>
                            <option value="week"><%=parametersName.get(periodWeekLabel).toString()%></option>
                            <option value="month"><%=parametersName.get(periodMonthLabel).toString()%></option>
                        </select>     
                </td>
                <td id="tdActiveControls2">                                                            
                    <input id="unitNumber" type="text" value="<%=unitValue%>" size="3" class="numeric" > 
                    <input id="unitNumberHidden" type="hidden" value="<%=unitValue%>" />
                </td>
                <td id="tdActiveControls3">
                    <%=parametersName.get(numTrxToConsiderActiveLabel).toString()%>:
                    <input id="trxNumber" type="text" value="<%=propertiesValues.get(numActTrxValue)%>" size="4">                                              
                    <input id="trxNumberHidden" type="hidden" value="<%=propertiesValues.get(numActTrxValue)%>" />
                </td>
                <td align="right">                                                                
                </td>
                </tr>
            </table>                                  
        </td> 
        </tr>
        <tr>
        <td class=main align="center" >
            <input type="hidden" name="search" value="y">
            <input type="button" id="button" name="button" value="<%=Languages.getString("jsp.admin.reports.show_report", SessionData.getLanguage())%>" onclick="doReport();">
        </td>
        </tr>
    </table>
</td>
</tr>
</table>                                                
</td>
</tr>
</table>
<div id="map-canvas"></div>   

<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>
