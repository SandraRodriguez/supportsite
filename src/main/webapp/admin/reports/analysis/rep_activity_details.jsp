<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport,
                 com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.TimeZone"%>
<jsp:useBean id="task" scope="session" class="com.debisys.utils.TaskBean"></jsp:useBean>
<%
String strReport = request.getParameter("report");
String requestType=request.getParameter("requestType");
String repId=request.getParameter("repId"); 
String userLogon=request.getParameter("userLogon");
int section=3;

boolean downloadbar  = false;
boolean maxdownloadbar  = false;
int maxdownloadset=-1 ;

if (strReport !=null && strReport.equals("y"))
{
  section=4;
}
else
{
  strReport = "";
}

int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:useBean id="Currency" class="com.debisys.currency.Currency" scope="page"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;

//DBsy-905
int imaxdownloadbar = -1;
String positionMarkers = null;
int intSectionPage = 0;
int recordcount = 0;
boolean isDownload = false;


if (request.getParameter("download") != null)
{
  if (request.getParameter("section_page") != null)
  {
    try
    {
      intSectionPage=Integer.parseInt(request.getParameter("section_page"));
    }
    catch(NumberFormatException ex)
    {
     response.sendRedirect(request.getHeader("referer"));
     return;
    }
  }  
    if (request.getParameter("markers") != null) {
        positionMarkers = request.getParameter("markers"); 
    } 
    
      //DBSY-908 
    if (request.getParameter("recordcount") != null) {
        recordcount = Integer.valueOf(request.getParameter("recordcount")); 
    } 
      //DBSY-908 
    if (request.getParameter("downloadbar") != null) {
        downloadbar = Boolean.valueOf(request.getParameter("downloadbar")); 
    } 
      //DBSY-908 
    if (request.getParameter("maxdownloadbar") != null) {
        imaxdownloadbar = Integer.valueOf(request.getParameter("maxdownloadbar")); 
    } 
    
 	requestType=request.getParameter("requestType");
	repId=request.getParameter("repId");
	userLogon=request.getParameter("userLogon");

  if (TransactionSearch.validateDateRange(SessionData))
  {
   
    String strUrlLocation = "";
	String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
	
      	if( downloadbar ){
     	task.setRunning(true);
     	task.setrecordcount(recordcount);
	    if(imaxdownloadbar!=-1  && recordcount>imaxdownloadbar){
	     	task.setmax(imaxdownloadbar);
     		task.setrecordcount(imaxdownloadbar);
     	}
     	task.setvar(false,SessionData, intSectionPage, application, TransactionSearch);
		isDownload = true;
		}
		else{
	   		 if(imaxdownloadbar!=-1)
	     			TransactionSearch.setmax(imaxdownloadbar);
     	 	strUrlLocation = TransactionSearch.downloadRepActivityDetails(SessionData, intSectionPage, application,repId,requestType,userLogon);
     	 	 response.sendRedirect(strUrlLocation);
     	 	}  
   
  }
  else
  {
     response.sendRedirect(request.getHeader("referer"));
     return;
  }
}

//////////////////////////////
if (request.getParameter("search") != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  if (TransactionSearch.validateDateRange(SessionData))
  {
     SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));    
   vecSearchResults = TransactionSearch.getRepActivityTransactions(intPage, intPageSize, SessionData, section_page, application, request.getParameter("repId"),request.getParameter("requestType"),request.getParameter("userLogon"));
    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
     	// DBSY 908
   	// checking download bar settings if Enabled and if feature is needed.
		String download = DebisysConfigListener.getdownloadbar(application);
		if(download!=null && !download.equals("")){
		try{
			int downloadset = Integer.parseInt(download);
			if( downloadset <= intRecordCount ){
				 downloadbar = true;
			}
		}
		catch(NumberFormatException e){
		 	downloadbar = false;
		}
		}
		
		
		String maxdownload = DebisysConfigListener.getmaxdownload(application);
		if(maxdownload!=null && !maxdownload.equals("")){
		try{
			 maxdownloadset = Integer.parseInt(maxdownload);
			if( maxdownloadset < intRecordCount && maxdownloadset!=0){
				maxdownloadbar = true; 
			}
		}
		catch(NumberFormatException e){
		 	maxdownloadbar = false;
		}
		}
	vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize) ;
      if ((intPageCount * intPageSize) < intRecordCount)
      {
        intPageCount++;
      }
    }
  }
  else
  {
   searchErrors = TransactionSearch.getErrors();
  }

}
%>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2("false")', 150);
}
}

</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
  <tr>
	  <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
	  <td class="formAreaTitle" align=left width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.rep_activity_detail_report.title",SessionData.getLanguage()).toUpperCase()%></td>
	  <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}
 out.println("</font></td></tr>");
}

if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
	if ( strReport.equals("y") )
	{
		Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
%>
	        <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
<%
	}
%>
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
              if (!TransactionSearch.getStartDate().equals("") && !TransactionSearch.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(TransactionSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getEndDateFormatted()));
                }
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%></td></tr>
            <tr><td class="main">
            <%=Languages.getString("jsp.admin.iso_name",SessionData.getLanguage())%>:&nbsp;<%=SessionData.getUser().getCompanyName()%>
            </td></tr>
            <tr>
              <td align=left class="main" nowrap>
               <form  name="mainform" method="post" action="admin/reports/analysis/rep_activity_details.jsp" onSubmit="scroll();">
              <input type="hidden" name="startDate" value="<%=TransactionSearch.getStartDate()%>">
              <input type="hidden" name="endDate" value="<%=TransactionSearch.getEndDate()%>">
              <input type="hidden" name="page" value="<%=intPage%>">
              <input type="hidden" name="section_page" value="<%=section_page%>">
              <input type="hidden" name="repId" value="<%=repId%>">
               <input type="hidden" name="requestType" value="<%=requestType%>">
               <input type="hidden" id="downloadbar" name="downloadbar" value="<%=downloadbar%>">
               <input type="hidden" id="recordcount" name="recordcount" value="<%=intRecordCount%>">
                <input type="hidden" id="userLogon" name="userLogon" value="<%=userLogon%>">
              <input type="hidden" name="download" value="y">
             <input type="submit" name="submit" value="Download to CSV">
             
              <% if(maxdownloadbar){ %><input type="hidden" id="maxdownloadbar" name="maxdownloadbar" value="<%=maxdownloadset%>"><% } %>
              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
              </form>
              <br/>
              <% if(maxdownloadbar){ %>
              <FONT COLOR="red"> <%=Languages.getString("jsp.admin.warning_maxdownload",SessionData.getLanguage())%><%=maxdownloadset%><%=Languages.getString("jsp.admin.warning_secmaxdownload",SessionData.getLanguage())%>
              </font><% } %>
              </td>
            </tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"admin/reports/analysis/rep_activity_details.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId="+repId + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") +"&page=1&report=" + strReport +"&type="+requestType + "\">First</a>&nbsp;");
                out.println("<a href=\"admin/reports/analysis/rep_activity_details.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + repId+ "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") +"&page=" + (intPage-1) + "&report=" + strReport  +"&type="+requestType + "\">&lt;&lt;Prev</a>&nbsp;");
              }

              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"admin/reports/analysis/rep_activity_details.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + repId + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + i + "&report=" + strReport + "&type="+requestType + "\">" +i+ "</a>&nbsp;");
                }
              }

              if (intPage <= (intPageCount-1))
              {
                out.println("<a href=\"admin/reports/analysis/rep_activity_details.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + repId + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + (intPage+1) + "&report=" + strReport + "&type="+requestType + "\">Next&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"admin/reports/analysis/rep_activity_details.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + repId + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + (intPageCount) + "&report=" + strReport +"&type="+requestType + "\">Last</a>");
              }

              %>
              </td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
              <td class=rowhead2>#</td>
			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.analysis.rep_activity_report.RepID",SessionData.getLanguage()).toUpperCase()%></td>
             <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_credit_history.date",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_credit_history.user",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_credit_history.entity",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_credit_history.desc",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.amount",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_credit_history.new_avail_credit",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_credit_history.prior_credit_limit",SessionData.getLanguage()).toUpperCase()%></td>
              			 
            </tr>
            <%
                  int intCounter = 1;
                  //intCounter = intCounter - ((intPage-1)*intPageSize);

                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  String type = "";
                  String row = "";
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    // check if type is "Return" of "Adjust", if so, display the whole row in red
                   out.print("<tr class=row" + intEvenOdd +">" +
                      "<td>" + intCounter++ + "</td>" +
                        "<td nowrap>" + vecTemp.get(0).toString() + "</td>" +
                      "<td nowrap>" + vecTemp.get(3).toString() + "</td>" +
                      "<td nowrap>" + vecTemp.get(1).toString() + "</td>" +
                       "<td nowrap>" + vecTemp.get(6).toString() + "</td>" +
                      "<td nowrap>" + vecTemp.get(5).toString() + "</td>" +
                      "<td nowrap align=right>" + vecTemp.get(2).toString() + "</td>" +
                      "<td nowrap align=right>" + vecTemp.get(4).toString() + "</td>"+
                       "<td nowrap>" + vecTemp.get(7).toString() + "</td>" );
				   	 
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>

<%
}
else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>