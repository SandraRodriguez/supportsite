<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 java.io.PrintWriter,
                 com.debisys.reports.jfreechart.Chart,
                 com.debisys.utils.*" %>
<%
int section=4;
int section_page=24;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
vecSearchResults = TransactionReport.getMerchantNotificationList(SessionData);
%>
<%@ include file="/includes/header.jsp" %>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet"/>
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%}%>
<table border="0" cellpadding="0" cellspacing="0" width="700">
  <tr>
    <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" class="formAreaTitle" width="5000">&nbsp;<%= Languages.getString("jsp.admin.reports.analysis.merchant_notification_list.title",SessionData.getLanguage()).toUpperCase() %></td>
    <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
	<tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" class=main>

<%
if (searchErrors != null)
{
  out.println("<table width=\"300\"><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr></table>");
}
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
<form name="mainform" method="post" action="admin/reports/analysis/merchant_notification_download.jsp"><input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.analysis.merchant_notification_list.download",SessionData.getLanguage())%>"></form>
            <br><%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
      	      <tr class="SectionTopBorder">
                <td class=rowhead2>#</td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase()%></td>
				<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.status",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.contact",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.phone",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.email",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.legal_name",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.street",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.city",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.state",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.zip",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.fax",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.rep_name",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.signup_date",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.notification_list.last_transaction_date",SessionData.getLanguage()).toUpperCase()%></td>
				<%
					if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
					{
				%>
						<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.route",SessionData.getLanguage()).toUpperCase()%></td>
				<% 
					}
				%>                  
              </tr>
            </thead>

            <%
            Iterator it = vecSearchResults.iterator();
            int intEvenOdd = 1;
            int intCounter = 1;

                while (it.hasNext())
                {
                  Vector vecTemp = null;
                  vecTemp = (Vector) it.next();
                  /*
                  0 dba
                  1 merchant_id
                  2 contact
                  3 contact_phone
                  4 email
                  5 datecancelled
                  */
                  String dateCancelled = vecTemp.get(5).toString();
                  out.print("<tr class=row" + intEvenOdd +">" +
                      "<td>" + intCounter++ + "</td>" +
                      "<td nowrap>" + vecTemp.get(0).toString() + "</td>" +
                      "<td>" + vecTemp.get(1).toString() + "</td>");
                      //determine status
                      out.print("<td>");
                      if (!dateCancelled.equals(""))
                      {
                        out.print(Languages.getString("jsp.admin.reports.disabled",SessionData.getLanguage()));
                      }
                      else
                      {
                        out.print(Languages.getString("jsp.admin.reports.active",SessionData.getLanguage()));
                      }
                      String strRouteName = "";
                      if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
					  {
					      strRouteName =  "<td>" + vecTemp.get(15).toString() + "</td>";		 
					  }
                      out.print("</td>");
                      out.println("<td>" + vecTemp.get(2).toString() + "</td>" +
                      "<td>" + vecTemp.get(3).toString() + "</td>" +
                      "<td>" + vecTemp.get(4).toString() + "</td>" +
                      "<td>" + vecTemp.get(6).toString() + "</td>" +
                      "<td>" + vecTemp.get(7).toString() + "</td>" +
                      "<td>" + vecTemp.get(8).toString() + "</td>" +
                      "<td>" + vecTemp.get(9).toString() + "</td>" +
                      "<td>" + vecTemp.get(10).toString() + "</td>" +
                      "<td>" + vecTemp.get(11).toString() + "</td>" +
                      "<td>" + vecTemp.get(12).toString() + "</td>" +
                      "<td>" + vecTemp.get(13).toString() + "</td>" +
                                          "<td>" + vecTemp.get(14).toString() + "</td>" +                      
                      strRouteName);
                        if (intEvenOdd == 1)
                        {
                          intEvenOdd = 2;
                        }
                        else
                        {
                          intEvenOdd = 1;
                        }

                      }

            %>
            </td>
            </tr>
            </table><%
}
else if (vecSearchResults.size()==0 && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.error6",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
<SCRIPT type="text/javascript">
  <!--
	<%
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
		{
	%>
			var stT1 = new SortROC(document.getElementById("t1"), ["None", "CaseInsensitiveString", "Number","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
	<% 
		}
		else
		{
	%>
	     	var stT1 = new SortROC(document.getElementById("t1"), ["None", "CaseInsensitiveString", "Number","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
	<% 
		}
	%>				
  -->
</SCRIPT>
<%}%>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>