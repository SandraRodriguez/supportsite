<%@ page import="java.util.*"%>
<%
  int section=4;
  int section_page=5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"	scope="session" />
<jsp:useBean id="TransactionReport"	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:setProperty name="Terminal" property="*" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp"%>

<%
	Vector vecSearchResults = new Vector();
	String strRuta = "";

	String strFormatedSerialNumber = Terminal.transformSerialNumber(SessionData);
		
	String report = request.getParameter("report");
	String start = request.getParameter("start");
	String end = request.getParameter("end");
	
	if (report!=null)
	{
		TransactionReport.setStartDate(start);
		TransactionReport.setEndDate(end);
		if (report.equals("Top10Merchants"))
		{
			vecSearchResults = TransactionReport.getTop10Merchants(SessionData);
			strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,6,SessionData);
		}
		else if ( report.equals("Top10Products") )
		{
		    vecSearchResults = TransactionReport.getTop10Products(SessionData);
			strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,5,SessionData);
		}
		else if ( report.equals("WeeklyVolume") )
		{
		    vecSearchResults = TransactionReport.getWeeklyVolumeDash(SessionData);
			strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,8,SessionData);
		}
		else if ( report.equals("MonthlyVolume") )
		{
		    vecSearchResults = TransactionReport.getMonthlyVolume(SessionData);
			strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,7,SessionData);
		}
		
	}	
	
	if (strRuta.length() > 0)
	{
		response.sendRedirect(strRuta);
	}
%>
