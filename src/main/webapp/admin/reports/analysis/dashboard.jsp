<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.DateUtil,
                 com.debisys.reports.jfreechart.Chart,
                 java.io.PrintWriter" %>
<%
int section=4;
int section_page=5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Date dtTodaysDate = new Date();
GregorianCalendar cal = new GregorianCalendar();
cal.setTime(dtTodaysDate);
cal.add(Calendar.MONTH, -1);
Date dtStartDate = cal.getTime();

TransactionReport.setStartDate(DateUtil.formatDate(dtStartDate));
TransactionReport.setEndDate(DateUtil.formatDate(dtTodaysDate));
%>
<%@ include file="/includes/header.jsp" %>
	<div id="divPrintButton" style="display:inline;"><table width=100%><tr><td>
		<script>
		function PrintReport()
		{
			var divBanner = document.getElementById("divPrintBanner");
			var divMenu = document.getElementById("divPrintMenu");
			var divButton = document.getElementById("divPrintButton");
			if ( divBanner != null )
			{
				divBanner.style.display = "none";
			}
			if ( divMenu != null )
			{
				divMenu.style.display = "none";
			}
			divButton.style.display = "none";
			window.print();
			divButton.style.display = "inline";
			if ( divMenu != null )
			{
				divMenu.style.display = "inline";
			}
			if ( divBanner != null )
			{
				divBanner.style.display = "inline";
			}
		}
		</script>
		<input type=button value="<%=Languages.getString("jsp.admin.reports.Print_This_Page",SessionData.getLanguage())%>" onclick="PrintReport();">
	</td></tr></table></div><br>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.title8",SessionData.getLanguage()).toUpperCase()%></font></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" height=800>
<table align=center cellpadding="0" cellspacing="0" border=0>
<tr>
<td align=center height=400><%
vecSearchResults = TransactionReport.getTop10Merchants(SessionData);
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
  /**
   * ******************************************************
   * hashSettings
   * key              value
   * data             Vector of data shere item 0 is desc, 1 is number value
   * title            Title of chart
   * category_label   Label name for the description
   * value_label      Label name for the values
   * width            Integer width
   * height           Integer height
   * horizontal       If exists then horizontal bar graph
   * showlabels       If exists then labels are shown on the chart
   * rgb              Color string i.e. "0,0,0"
   * ******************************************************
   */

    Hashtable hashSettings = new Hashtable();
    hashSettings.put("data", vecSearchResults);
    hashSettings.put("title", Languages.getString("jsp.admin.reports.title21", new Object[]{SessionData.getProperty("company_name").trim(),TransactionReport.getStartDateFormatted(),TransactionReport.getEndDateFormatted()},SessionData.getLanguage()));
    hashSettings.put("category_label",Languages.getString("jsp.admin.reports.merchants",SessionData.getLanguage()));
    hashSettings.put("value_label",Languages.getString("jsp.admin.reports.transaction_amounts",SessionData.getLanguage()));
    hashSettings.put("width", "475");
    hashSettings.put("height", "400");
    hashSettings.put("rgb", "99,125,165");
    hashSettings.put("horizontal", "Y");
    hashSettings.put("showlabels", "Y");

	  String filename = Chart.generateBarChart(hashSettings, session,  new PrintWriter(out));
  	String graphURL = request.getContextPath() + "/servlets/DisplayChart?filename=" + filename;
	%>
		<table width="100%" cellspacing="0" cellpadding="0" align=center border=0 height=400>
            <tr>
            	<td class=main align=center>
            		<form action="admin/reports/analysis/analisys_download.jsp" method="post">
            			<input ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.analisys.download",SessionData.getLanguage())%>">
            			<input type="hidden" id="start" name="start" value="<%=TransactionReport.getStartDate()%>"/>
            			<input type="hidden" id="end" name="end" value="<%=TransactionReport.getEndDate()%>"/>
            			<input type="hidden" id="report" name="report" value="Top10Merchants"/>
            		</form>            		
            	</td>
            </tr>
            <tr>
            <td align=center><img src="<%= graphURL %>" width=475 height=400 border=0 usemap="#<%= filename %>"></td>
            </tr>
        </table>
       <%
}
else if (vecSearchResults.size()==0)
{
	if(strAccessLevel.equals(DebisysConstants.MERCHANT)) {
		out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.error7",SessionData.getLanguage())+"</font>");
	} else {
		out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.error4",SessionData.getLanguage())+"</font>");		
	}
 
}

out.println("</td><td align=center height=400>");
vecSearchResults.clear();
vecSearchResults = TransactionReport.getTop10Products(SessionData);
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
    Hashtable hashSettings = new Hashtable();
    hashSettings.put("data", vecSearchResults);
    hashSettings.put("title", Languages.getString("jsp.admin.reports.title22", new Object[]{SessionData.getProperty("company_name").trim(),TransactionReport.getStartDateFormatted(),TransactionReport.getEndDateFormatted()},SessionData.getLanguage()));
    hashSettings.put("category_label",Languages.getString("jsp.admin.reports.products",SessionData.getLanguage()));
    hashSettings.put("value_label",Languages.getString("jsp.admin.reports.transaction_amounts",SessionData.getLanguage()));
    hashSettings.put("width", "475");
    hashSettings.put("height", "400");
    hashSettings.put("rgb", "222,121,115");
    hashSettings.put("horizontal", "Y");
    hashSettings.put("showlabels", "Y");

    String filename = Chart.generateBarChart(hashSettings, session,  new PrintWriter(out));
  	String graphURL = request.getContextPath() + "/servlets/DisplayChart?filename=" + filename;
	%>
		<table width="100%" cellspacing="0" cellpadding="0" align=center border=0 height=400>
            <tr>
            	<form action="admin/reports/analysis/analisys_download.jsp" method="post">
            			<input ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
            			<input type="hidden" id="start" name="start" value="<%=TransactionReport.getStartDate()%>"/>
            			<input type="hidden" id="end" name="end" value="<%=TransactionReport.getEndDate()%>"/>
            			<input type="hidden" id="report" name="report" value="Top10Products"/>
            	</form>            
            </tr>	
             <tr>
            <td align=center><img src="<%= graphURL %>" width=475 height=400 border=0 usemap="#<%= filename %>"></td>
            </tr>
        </table>
    <%
}
else if (vecSearchResults.size()==0)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.error5",SessionData.getLanguage())+"</font>");
}

  out.println("</td></tr><tr><td align=center height=400 align=top>");
  vecSearchResults.clear();
  cal.setTime(dtTodaysDate);
  cal.add(Calendar.DATE, -42);
  dtStartDate = cal.getTime();
  TransactionReport.setStartDate(DateUtil.formatDate(dtStartDate));
  TransactionReport.setEndDate(DateUtil.formatDate(dtTodaysDate));
  vecSearchResults = TransactionReport.getWeeklyVolumeDash(SessionData);

if (vecSearchResults != null && vecSearchResults.size() > 0)
{
    Hashtable hashSettings = new Hashtable();
    hashSettings.put("data", vecSearchResults);
    hashSettings.put("title", Languages.getString("jsp.admin.reports.title24", new Object[]{SessionData.getProperty("company_name").trim(),TransactionReport.getStartDateFormatted(),TransactionReport.getEndDateFormatted()},SessionData.getLanguage()));
    hashSettings.put("category_label",Languages.getString("jsp.admin.reports.weeks",SessionData.getLanguage()));
    hashSettings.put("value_label",Languages.getString("jsp.admin.reports.transaction_amounts",SessionData.getLanguage()));
    hashSettings.put("width", "475");
    hashSettings.put("height", "400");
    hashSettings.put("rgb", "123,178,115");
    hashSettings.put("showlabels", "Y");

	  String filename = Chart.generateBarChart(hashSettings, session, new PrintWriter(out));
  	String graphURL = request.getContextPath() + "/servlets/DisplayChart?filename=" + filename;
	%>
		<table width="100%" cellspacing="0" cellpadding="0" align=center border=0>
            <tr>
            <td align=center height=400><img src="<%= graphURL %>" width=475 height=400 border=0 usemap="#<%= filename %>"></td>
            </tr>
            <tr>
            	<td class=main align=center>
            		<form action="admin/reports/analysis/analisys_download.jsp" method="post">
            			<input ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.analisys.download",SessionData.getLanguage())%>">
            			<input type="hidden" id="start" name="start" value="<%=TransactionReport.getStartDate()%>"/>
            			<input type="hidden" id="end" name="end" value="<%=TransactionReport.getEndDate()%>"/>
            			<input type="hidden" id="report" name="report" value="WeeklyVolume"/>
            		</form>            		
            	</td>
            </tr>
        </table>
    <%
}
else if (vecSearchResults.size()==0 && request.getParameter("search") != null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.error3",SessionData.getLanguage())+"</font>");
}
  out.println("</td><td valign=top align=center height=400>");
  vecSearchResults.clear();
  cal.setTime(dtTodaysDate);
  cal.add(Calendar.MONTH, -6);
  cal.set(Calendar.DAY_OF_MONTH,1);
  dtStartDate = cal.getTime();
  TransactionReport.setStartDate(DateUtil.formatDate(dtStartDate));
  TransactionReport.setEndDate(DateUtil.formatDate(dtTodaysDate));
  vecSearchResults = TransactionReport.getMonthlyVolume(SessionData);

  if (vecSearchResults != null && vecSearchResults.size() > 0)
{
    Hashtable hashSettings = new Hashtable();
    hashSettings.put("data", vecSearchResults);
    hashSettings.put("title", Languages.getString("jsp.admin.reports.title25", new Object[]{SessionData.getProperty("company_name").trim(),TransactionReport.getStartDateFormatted(),TransactionReport.getEndDateFormatted()},SessionData.getLanguage()));
    hashSettings.put("category_label",Languages.getString("jsp.admin.reports.months",SessionData.getLanguage()));
    hashSettings.put("value_label",Languages.getString("jsp.admin.reports.transaction_amounts",SessionData.getLanguage()));
    hashSettings.put("width", "475");
    hashSettings.put("height", "400");
    hashSettings.put("rgb", "254,166,74");
    hashSettings.put("showlabels", "Y");
	  String filename = Chart.generateBarChart(hashSettings, session,  new PrintWriter(out));

  	String graphURL = request.getContextPath() + "/servlets/DisplayChart?filename=" + filename;
	%>
		<table width="100%" cellspacing="0" cellpadding="0" align=center border=0 height=400>
            <tr>
            <td align=center><img src="<%= graphURL %>" width=475 height=400 border=0 usemap="#<%= filename %>"></td>
            </tr>
            <tr>
            	<td class=main align=center>
            		<form action="admin/reports/analysis/analisys_download.jsp" method="post">
            			<input ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.analisys.download",SessionData.getLanguage())%>">
            			<input type="hidden" id="start" name="start" value="<%=TransactionReport.getStartDate()%>"/>
            			<input type="hidden" id="end" name="end" value="<%=TransactionReport.getEndDate()%>"/>
            			<input type="hidden" id="report" name="report" value="MonthlyVolume"/>
            		</form>
            	</td>
            </tr>
        </table>
    <%
}
else if (vecSearchResults.size()==0 && request.getParameter("search") != null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.error2",SessionData.getLanguage())+"</font>");
}
%></td>
</tr>
</table>
        </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>