<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page import="java.net.URLEncoder,com.debisys.utils.HTMLEncoder,com.debisys.terminals.Terminal,java.util.*,com.debisys.customers.Merchant,com.debisys.reports.pojo.*" buffer="65kb"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="generalReports" class="com.debisys.reports.GeneralReports" scope="request" />
<%
int section = 4;
int section_page = 44;
boolean downloadRows = false;
String message = "";
%>
<%@ include file="/includes/security.jsp"%>
<%
String language_ = Languages.getLanguage();
String first = "";
String prev = "";
String next = "";
String last = "";

if (!language_.equals("english")){
	first = "Primero";
	prev = "Anterior";
	next = "Siguiente";
	last = "�ltimo";
}

String strMerchantIds[] = request.getParameterValues("mids");
String strTerminalsIds[] = request.getParameterValues("tmids");

if (request.getParameter("searchMerchants") != null){
	String strTempinstance = com.debisys.dateformat.DateFormat.getDateFormat();
	generalReports.setMerchantIds(strMerchantIds);
	generalReports.setTerminalTypesIds(strTerminalsIds);
	if (request.getParameter("ratePlanId") != null){
		generalReports.setRatePlanId(request.getParameter("ratePlanId"));
	}
	if (strTerminalsIds != null && strMerchantIds != null){
		downloadRows = generalReports.getSitesIdByMerchantList(SessionData, application, request);
		if (!downloadRows){
			//jsp.admin.transactions.manageinventory.not_found
			//jsp.admin.transactions.manageinventory.inventory_assignment.select_error
			message = Languages.getString("jsp.admin.transactions.manageinventory.not_found", SessionData.getLanguage());
		}
	}
}
if (request.getParameter("download") != null){
	generalReports.setMerchantIds(strMerchantIds);
	generalReports.setTerminalTypesIds(strTerminalsIds);
	if (request.getParameter("ratePlanId") != null){
		generalReports.setRatePlanId(request.getParameter("ratePlanId"));
	}
	String sURL = "";
	sURL = generalReports.downloadSitesIdByMerchantList(application, SessionData,
			request);
	response.sendRedirect(sURL);
	return;
}
%>
<%@ include file="/includes/header.jsp"%>
<link rel="stylesheet" type="text/css" href="/support/css/displaytagex.css">
<script LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");

function scroll(){
	//document.mainform.submit.disabled = true;
	//document.mainform.submit.value = iProcessMsg[count];
	//count++
	//if (count = iProcessMsg.length) count = 0
	//setTimeout('scroll()', 150);
}

function scroll2(){
	document.downloadform.submit.disabled = true;
	document.downloadform.submit.value = iProcessMsg[count];
	count++
	if (count = iProcessMsg.length) count = 0
	setTimeout('scroll2()', 150);
}

function deselectAll(control){
	var items=false;
	for (var i=0; i<control.length; i++){
		if (control.options[i].selected && control.options[i].value=="ALL"){ // text+" "+selectobject.options[i].value)
			items=true;
			break;
		}
	}
	if (items){
		for (var i=0; i<control.length; i++){
			if (control.options[i].selected && control.options[i].value!="ALL"){ 
				control.options[i].selected=false;
			}
		}
	}
}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="950">
	<tr>
		<td width="18" height="20">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" width="3000"
			class="formAreaTitle"><%=Languages.getString(
					"jsp.admin.reports.analisys.merchant_detail_siteid.title",
					SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20">
			<img src="images/top_right_blue.gif">
		</td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				align=center>
				<tr>
					<td>
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="formArea2">
									<form name="mainform" method="post" action="admin/reports/analysis/merchant_detail_siteId.jsp" onSubmit="scroll();">
										<table width="300">
											<tr>
												<td valign="top" nowrap>
													<table width=500>
<%
if (request.getParameter("ratePlanId") == null){
%>
														<tr>
															<td>
																<table>
																	<tr>
																		<td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.merchants.option", SessionData.getLanguage())%></td>
																	</tr>
																	<tr>
																		<td class=main valign=top>
																			<select name="mids" id="mids" size="10" multiple onclick="deselectAll(this);">
																				<option value="ALL"><%=Languages.getString("jsp.admin.reports.all", SessionData.getLanguage())%></option>
<%
	boolean findChoosen = false;
		Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
		Iterator it = vecMerchantList.iterator();
		while (it.hasNext()){
			Vector vecTemp = null;
			vecTemp = (Vector) it.next();
			if (generalReports.getMerchant_ids().indexOf(vecTemp.get(0).toString()) >= 0){
				%>				
				<option selected value="<%=vecTemp.get(0)%>"><%=vecTemp.get(1)%></option>
<%
				//System.out.println("---> "+generalReports.getMerchant_ids());
				findChoosen = true;
			}else{
%>
				<option value="<%=vecTemp.get(0)%>"><%=vecTemp.get(1)%></option>
<%
			}
		}
		if (!findChoosen){
%>
																				<script type="text/javascript">
																					var combo1 = document.getElementById("mids");
																					combo1.options[0].selected = true;
																				</script>
<%
		}
%>
																			</select>

																		</td>
																	</tr>
																</table>
															</td>
															<td>
																<table>
																	<tr>
																		<td class=main valign=top>
																			<%=Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.terminals_types", SessionData.getLanguage())%>
																		</td>
																	</tr>
																	<tr>
																		<td class=main valign=top nowrap>
																			<select name="tmids" id="tmids" size="10" multiple
																				onclick="deselectAll(this);">
																				<option value="ALL"><%=Languages.getString("jsp.admin.reports.all", SessionData.getLanguage())%></option>
<%
		findChoosen = false;
		Vector vecTerminalTypes = Terminal.getTerminalTypes(SessionData);
		Iterator itT = vecTerminalTypes.iterator();
		//System.out.println("-------> "+generalReports.getTerminalTypes_ids());
		while (itT.hasNext()){
			Vector vecTemp = null;
			vecTemp = (Vector) itT.next();
			if (generalReports.containsTerminalSelected(vecTemp.get(0).toString()))	{
				out.println("<option selected value=" + vecTemp.get(0) + ">"
						+ vecTemp.get(1) + "</option>");
				//System.out.println("---> "+vecTemp.get(0).toString());
				findChoosen = true;
			}else{
				out.println("<option value=" + vecTemp.get(0) + ">" + vecTemp.get(1)
						+ "</option>");
			}
		}
		if (!findChoosen){
%>
																				<script type="text/javascript">
																					var combo2 = document.getElementById("tmids");
																					combo2.options[0].selected = true;
																				</script>
<%
		}
%>
																			</select>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class=main valign=top nowrap colspan="2">
																*<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions", SessionData.getLanguage())%>
															</td>
														</tr>
<%
	}else{
%>
														<tr>
															<td>
																<input type="hidden" name="mids" value="ALL">
																<input type="hidden" name="tmids" value="ALL">
																<input type="hidden" name="ratePlanId"
																	value="<%=request.getParameter("ratePlanId")%>">
															</td>
														</tr>
<%
	}
%>
														<tr>
															<td style="font-color: red;" colspan="2">
																<%=message%>
															</td>
														</tr>

														<tr>
<%
	if (request.getParameter("ratePlanId") == null){
%>
															<td class=main colspan=1 align=center>
																<input type="submit" name="searchMerchants" id="searchMerchants" value="<%=Languages.getString("jsp.admin.reports.show_report", SessionData.getLanguage())%>">
															</td>
<%
	}
	if (downloadRows){
%>
															<td>
																<INPUT type="submit" name="download" id="download" value="<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.download", SessionData.getLanguage())%>">
															</td>
<%
	}
%>
														</tr>
														<tr>
															<td class="main" colspan=2>

															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</form>
								</td>
							</tr>
<%
	String merchantId = Languages.getString("jsp.admin.customers.merchants.merchant_id", SessionData.getLanguage()).toUpperCase();
	String merchantName = Languages.getString("jsp.admin.reports.merchant_name", SessionData.getLanguage()).toUpperCase();
	String phys_address = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_address", SessionData.getLanguage()).toUpperCase();
	String phys_city = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_city", SessionData.getLanguage()).toUpperCase();
	String phys_state = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_state", SessionData.getLanguage()).toUpperCase();
	String phys_zip = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_zip", SessionData.getLanguage()).toUpperCase();
	String phys_country = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_country", SessionData.getLanguage()).toUpperCase();
	String contact_phone = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.contact_phone", SessionData.getLanguage()).toUpperCase();
	String contact_fax = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.contact_fax", SessionData.getLanguage()).toUpperCase();
        String accountExecutiveHeader = Languages.getString("jsp.admin.reports.tools.accountexecutives.label", SessionData.getLanguage()).toUpperCase();
	String millennium_no = Languages.getString("jsp.merchant.requestrebuil.text5", SessionData.getLanguage()).toUpperCase();
	String ISO_Rateplan_id = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.ISO_Rateplan_id", SessionData.getLanguage()).toUpperCase();
	String description = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.description", SessionData.getLanguage()).toUpperCase();
	String rateplan = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.rateplan", SessionData.getLanguage()).toUpperCase();
	String terminal_version = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.terminal_version", SessionData.getLanguage()).toUpperCase();
	String rebuildType = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.rebuildType", SessionData.getLanguage()).toUpperCase();
	String rebuildDate = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.rebuildDate", SessionData.getLanguage()).toUpperCase();
	String repName = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.repName", SessionData.getLanguage()).toUpperCase();
	String terminalCreationDate = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.terminalCreationDate", SessionData.getLanguage()).toUpperCase();
	//String providerName = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.providerName",SessionData.getLanguage());
%>
							<tr style="width: 690px">
								<td>
									<display:table export="false" pagesize="20" name="merchantDetails" id="row" class="dataTable" cellspacing="1">
										<display:column property="rep_Name" title="<%=repName%>" sortable="true" />
										<display:column property="merchant_id" title="<%=merchantId%>" sortable="true" />
										<display:column property="dba" title="<%=merchantName%>" sortable="true" />
										<display:column property="phys_address" title="<%=phys_address%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="phys_city" title="<%=phys_city%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="phys_state" title="<%=phys_state%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="phys_zip" title="<%=phys_zip%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="phys_country" title="<%=phys_country%>" sortable="true" class="customer" headerClass="customer" /> 
										<display:column property="contact_phone" title="<%=contact_phone%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="contact_fax" title="<%=contact_fax%>" sortable="true" class="customer" headerClass="customer" />
                                                                                <display:column property="accountExecutive" title="<%=accountExecutiveHeader%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="millennium_no" title="<%=millennium_no%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="ISO_Rateplan_id" title="<%=ISO_Rateplan_id%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="ratePlan" title="<%=rateplan%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="description" title="<%=description%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="terminal_version" title="<%=terminal_version%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="rebuildType" title="<%=rebuildType%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="rebuildDate" title="<%=rebuildDate%>" sortable="true" class="customer" headerClass="customer" />
										<display:column property="terminalCreationDate" title="<%=terminalCreationDate%>" sortable="true" class="customer" headerClass="customer" />
									</display:table>
								</td>
							</tr>
<%
	if (!language_.equals("english")){
%>
							<script type="text/javascript">
								var valueI8E = "";
								var pagingbannerfirst = document.getElementById("pagingbannerfirst");
								if (pagingbannerfirst != null) {
									valueI8E = pagingbannerfirst.innerHTML;
									pagingbannerfirst.innerHTML = replaceChains(valueI8E);
								}
								
								var pagingbannerfull = document.getElementById("pagingbannerfull");
								if (pagingbannerfull != null) {
									valueI8E = pagingbannerfull.innerHTML;
								
									pagingbannerfull.innerHTML = replaceChains(valueI8E);
								}
								
								var pagingbannerlast = document.getElementById("pagingbannerlast");
								if (pagingbannerlast != null) {
									valueI8E = pagingbannerlast.innerHTML;
									pagingbannerlast.innerHTML = replaceChains(valueI8E);
								}
								
								function replaceChains(valueI8E) {
									var chainReplace = "";
									chainReplace = valueI8E.replace("First", "<%=first%>");
									chainReplace = chainReplace.replace("Prev", "<%=prev%>");
									chainReplace = chainReplace.replace("Next", "<%=next%>");
									chainReplace = chainReplace.replace("Last", "<%=last%>");
									return chainReplace;
								}
							</script>
<%
	}
%>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;"></iframe>
<%@ include file="/includes/footer.jsp"%>