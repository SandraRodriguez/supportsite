<%@ page import="java.util.*"%>
<%
  int section=4;
  int section_page=5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"	scope="session" />
<jsp:useBean id="TransactionReport"	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:setProperty name="Terminal" property="*" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp"%>

<%
	Vector vecSearchResults = new Vector();
	String strRuta = "";

	String strFormatedSerialNumber = Terminal.transformSerialNumber(SessionData);
		
	String report = request.getParameter("report");
	String start = request.getParameter("start");
	String end = request.getParameter("end");
	int reportId=-1;
	
	if (report!=null)
	{
		TransactionReport.setStartDate(start);
		TransactionReport.setEndDate(end);
		if (report.equals("Top10Merchants"))
		{
			vecSearchResults = TransactionReport.getTop10Merchants(SessionData);
			reportId=6;
		}
		else if ( report.equals("Top10Products") )
		{
		    vecSearchResults = TransactionReport.getTop10Products(SessionData);
			reportId=5;
		}
		else if ( report.equals("WeeklyVolume") )
		{
		    vecSearchResults = TransactionReport.getWeeklyVolume(SessionData);		    
			reportId=8;
		}
		else if ( report.equals("MonthlyVolume") )
		{
		    vecSearchResults = TransactionReport.getMonthlyVolume(SessionData);
			reportId=7;
		}
		else if ( report.equals("VolumeDaily") )
		{
		    vecSearchResults = TransactionReport.getDailyVolume(SessionData);
			reportId=21;
		}
		else if ( report.equals("VolumeWeekly") )
		{
		    vecSearchResults = TransactionReport.getWeeklyVolume(SessionData);
			reportId=22;
		}
		else if ( report.equals("VolumeMonthly") )
		{
		    vecSearchResults = TransactionReport.getMonthlyVolume(SessionData);
			reportId=23;
		}
		else if ( report.equals("Top10Agents") )
		{
		    vecSearchResults = TransactionReport.getTop10Agents(SessionData);
			reportId=24;
		}
		else if ( report.equals("Top10SubAgents") )
		{
		   vecSearchResults = TransactionReport.getTop10SubAgents(SessionData);
		   reportId=25;
		}
		else if ( report.equals("Top10Rep") )
		{
		   vecSearchResults = TransactionReport.getTop10Reps(SessionData);
		   reportId=26;
		}
		
		strRuta = com.debisys.reports.TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,reportId,SessionData);
		if (strRuta.length() > 0)
		{
			response.sendRedirect(strRuta);
		}
						
	}	
	
	
%>
