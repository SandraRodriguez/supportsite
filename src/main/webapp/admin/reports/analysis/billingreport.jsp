<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 java.io.PrintWriter,
                 com.debisys.reports.jfreechart.Chart,
                 com.debisys.utils.*" %>
<%
int section=4;
int section_page=37;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="BillingReport" class="com.debisys.reports.BillingReport" scope="request"/>
<jsp:setProperty name="BillingReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;

if (request.getParameter("search") != null)
{

  if (BillingReport.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
	SessionData.setProperty("currencyid", request.getParameter("currencyid"));
    SessionData.setProperty("conversionr", request.getParameter("conversionR"));
    
    vecSearchResults = BillingReport.getBillingSummary(SessionData);
  }
  else
  {
   searchErrors = BillingReport.getErrors();
  }
}
else
{
	SessionData.setProperty("conversionr", "1");
}

%>
<%@ include file="/includes/header.jsp" %>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet"/>
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%}%>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll() /*??*/
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="800">
  <tr>
    <td background="images/top_blue.gif" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle" align=left>&nbsp;<%=Languages.getString("jsp.admin.reports.title43",SessionData.getLanguage()).toUpperCase()%></td>
    <td background="images/top_blue.gif" align=right><img src="images/top_right_blue.gif"></td>
  </tr>
	<tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="get" action="admin/reports/analysis/billingreport.jsp" onSubmit="scroll();">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" class=main>
          <%=Languages.getString("jsp.admin.reports.desc18",SessionData.getLanguage())%>
	          <table width="300">
              <tr class="main">
               <td nowrap valign="top">
               <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>
		         <%}%>:</td>
               <td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
<tr class="main">
    <td nowrap widht="5%"><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>: <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
    <td nowrap widht="95%"><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.reports.analysis.billingreport.currency",SessionData.getLanguage())%>:&nbsp;&nbsp; 
    <select id="currencyid" class="plain" name="currencyid" enabled="true" value="<%=SessionData.getProperty("currency")%>">
        	<option value="0" ><%=Languages.getString("jsp.admin.reports.analysis.billingreport.currency.type1",SessionData.getLanguage())%></option>
         	<option value="1" <% if(SessionData.getProperty("currencyid").equals("1"))out.print("selected"); %>><%=Languages.getString("jsp.admin.reports.analysis.billingreport.currency.type2",SessionData.getLanguage())%></option>
    </select></a></td>
    <td nowrap><%=Languages.getString("jsp.admin.reports.analysis.billingreport.conversionrate",SessionData.getLanguage())%>: <input class="plain" name="conversionR" value="<%=SessionData.getProperty("conversionr")%>" size="12"></a></td>
    <td class=main>
      <input type="hidden" name="search" value="y">
      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.submit18",SessionData.getLanguage())%>">
    </td>
</tr>
</table>
               </td>
              </tr>
              </form>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%><table width="100%" cellspacing="0" cellpadding="0" align=center border=0>
            <tr>
            <td class=main>
            <%
              out.print(Languages.getString("jsp.admin.reports.title43",SessionData.getLanguage()) + " ");
              if (!BillingReport.getStartDate().equals("") && !BillingReport.getEndDate().equals(""))
                {
                   out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(BillingReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(BillingReport.getEndDateFormatted()));
                }
  %><br>
  <table>
    <tr>
      <td align=left class="main" nowrap>
      <form method=post action="admin/reports/analysis/billingreport.jsp">
       <input type="hidden" name="startDate" value="<%=BillingReport.getStartDate()%>">
       <input type="hidden" name="endDate" value="<%=BillingReport.getEndDate()%>">
       <input type="hidden" name="currencyID" value="<%=BillingReport.getCurrencyID() %>">
       <input type="hidden" name="conversionR" value="<%=BillingReport.getConversionR() %>">
     </form>
      </td>
    </tr>
  </table>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
 	 	      <tr class="SectionTopBorder">
                <td class="rowhead2" nowrap> </td>
              	<%
             		Iterator it1 = vecSearchResults.iterator();
                    while (it1.hasNext())
             		{
                  		Vector vecTemp = null;
                  		vecTemp = (Vector) it1.next();
                  		
                  		out.print(	"<td class=\"rowhead2\" align=\"center\" nowrap>" + vecTemp.get(0).toString() + "</td>");
                  	}
               	%>
				</tr>               
            </thead>

            <%
	        int intEvenOdd = 1;
             
			for (int i=1; i<=12;i++)
			{
	            Iterator it = vecSearchResults.iterator();
	            
                  switch (i)
                  {
		               	case 1:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.totalsales",SessionData.getLanguage()) + "</td>");
	                  		break;
	            		case 2:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.transactions",SessionData.getLanguage()) + "</td>");
                  			break;
                  		case 3:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.numberofdays",SessionData.getLanguage()) + "</td>");
                  			break;
	            		case 4:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.activeterminals",SessionData.getLanguage()) + "</td>");
                  			break;
	            		case 5:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.revenue",SessionData.getLanguage()) + "</td>");
                  			break;
	            		case 6:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.avgtransactionamount",SessionData.getLanguage()) + "</td>");
                  			break;
	            		case 7:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.avgtransperdayperterm",SessionData.getLanguage()) + "</td>");
                  			break;
	            		case 8:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.totalaverageperday",SessionData.getLanguage()) + "</td>");
                  			break;
	            		case 9:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.amountaverage",SessionData.getLanguage()) + "</td>");
                  			break;
	            		case 10:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.monthlyamountaverage",SessionData.getLanguage()) + "</td>");
                  			break;
	            		case 11:
                  			break;
	            		case 12:
        		      		out.print("<tr class=row"  + intEvenOdd + ">" + "<td nowrap>" + 
	                  		Languages.getString("jsp.admin.reports.analysis.billingreport.arpu",SessionData.getLanguage()) + "</td>");
                  			break;                  			                  			                  			                  			                  			
                  	}                  			                  			                  			                  			                  			
	            
	 
                while (it.hasNext())
                {
                  Vector vecTemp = null;
                  vecTemp = (Vector) it.next();
                  /*
                  0 Trans_Month_Year
                  1 Trans_Amount
                  2 Number_Days
                  3 Trans_Quantity
                  4 Live_POS
                  5 Emida_Rev
                  6 Avg_Amount_Trans
                  7 Avg_Trans_Term_Day
                  8 Total_Avg_day
                  9 Avg_Amount_Day_Term
                  10 Avg_Amount_Mo_Term
                  //11 Num_Recharge_Mo_Sub
                  12 ARPU
                  */
    	      	  if ((i==1) || (i==5) || (i==6) || (i==8) || (i==9) || (i==10) || (i==12))
    	      	  {
    	      	   	  out.print("<td nowrap align=\"right\">" + "$" + vecTemp.get(i).toString() + "</td>");
    	      	  }
    	      	  else if (i!=11)
    	      	  {
    	      	   	  out.print("<td nowrap align=\"right\">" + vecTemp.get(i).toString() + "</td>");    	      	  
    	      	  }
	                                			                  			                  			                  			                  			
             
                 }
                 out.print("</tr>");
                 if ((i % 2 == 1)||(i==12))
                 {
                     intEvenOdd = 2;
                 }
                 else if (i!=11)
                 {
                     intEvenOdd = 1;
                 }
                 
		}
            %>
            </td>
            </tr>
            </table><%
}
else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.error6",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%
String bonusOrder = "";
if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
	bonusOrder = "\"Number\", \"Number\","; 
}
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
	String sSMSNumber = "";
    if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
    {
  	  sSMSNumber = ", \"CaseInsensitiveString\"";
    }
    
    String streetAndState = "";
    if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) 
    {
    	streetAndState = "\"CaseInsensitiveString\", \"CaseInsensitiveString\",";
    }
    
	String sRouteName = "";
    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
    {
  	  sRouteName = ", \"CaseInsensitiveString\"";
    }    
    
%>
<SCRIPT type="text/javascript">
  <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "CaseInsensitiveString", <%=streetAndState%> "Number","Number","Number", <%=bonusOrder%> "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"<%=sSMSNumber%>, "Date", "Date", "CaseInsensitiveString"<%=sRouteName%>],0,false,false);
  -->
</SCRIPT>
<%}%>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>
