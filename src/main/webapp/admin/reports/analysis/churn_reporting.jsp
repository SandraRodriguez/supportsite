<%@ page import="java.util.*, java.text.DecimalFormat" %>
<%
int section=4;
int section_page=9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ChurnReport" class="com.debisys.reports.ChurnReport" scope="request"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request" />
<%@ include file="/includes/security.jsp" %>
<%!
String stripLeadingAndTrailingQuotes(String str){
	if (str.startsWith("\"")){
		str = str.substring(1, str.length());
	}
	if (str.endsWith("\"")){
		str = str.substring(0, str.length() - 1);
	}
	return str;
}
%>
<%
String downReport = request.getParameter("download");
String showReport = request.getParameter("showReport");
String isosList = request.getParameter("isosList");

boolean merchOnly = ((""+request.getParameter("merchOnly")).equals("Y")?true:false);
int merchdisabled = ((""+request.getParameter("merchdisabled")).equals("Y")?0:1);
int frequency = 1;
if(request.getParameter("frequency") != null){
	frequency = Integer.parseInt(request.getParameter("frequency"));
}
double threshold = 50;
if(request.getParameter("threshold") != null){
	threshold =  Double.parseDouble(request.getParameter("threshold"));
}

	StringBuilder strComboIsos = new StringBuilder();
	Vector vecSearchResults = null;
		
	if ( strAccessLevel.equals(DebisysConstants.CARRIER) )
	{		
		if ( session.getAttribute("isosCache")!=null)
		{
			StringBuilder strComboIsosAux = new StringBuilder();
			if ( isosList!=null)
			{
				strComboIsosAux = (StringBuilder)session.getAttribute("isosCache");
				String straux = strComboIsosAux.toString().replaceAll("value='"+isosList+"'"," selected value='"+isosList+"' ");
				strComboIsos.append(straux);
			}
			else
			{
				strComboIsos = (StringBuilder)session.getAttribute("isosCache");
			}						
		}
		else
		{
			vecSearchResults = CustomerSearch.searchRep(1, -1, SessionData,DebisysConstants.REP_TYPE_ISO_5_LEVEL);
			vecSearchResults.removeElementAt(0);
			Iterator it = vecSearchResults.iterator();
			
			strComboIsos.append("<tr><td class='main'><b>"+Languages.getString("jsp.productapproval.selectISO",SessionData.getLanguage())+"</b></td><td align='left'><select id='isosList' name='isosList'>");
												
			while (it.hasNext())
			{
				Vector vecTemp = null;
				vecTemp = (Vector) it.next();
				String businessname = (String)vecTemp.get(0);
				String rec_id = (String)vecTemp.get(1);
				
				strComboIsos.append("<option value='"+rec_id+"'>"+businessname+"</option>");
				
			}	
			strComboIsos.append("</select></td></tr>");	
		
			session.setAttribute("isosCache",strComboIsos);
		}		
	 }
		
		
if(downReport != null){
	String strPath = ChurnReport.downloadChurnReportCVS(SessionData, getServletContext(), merchdisabled, frequency, threshold/100, merchOnly,isosList);
	if (strPath.length() > 0){
		response.sendRedirect(strPath);
	}
}else{
%>
<%@ include file="/includes/header.jsp" %>
<style>
	.reportForm table{border:0;border-collapse:collapse;width:100%;font-family:Tahoma;color:#000000;font-size:9pt;}
	.reportForm td{padding:4px; height:28;vertical-align:text-top;}
</style>
<script>
	function trim (str){
		return str.replace(/^\s+/g,'').replace(/\s+$/g,'')
	}
	function validateForm(){
		return validateThreshold(document.getElementById("threshold").value, 
				'<%=Languages.getString("jsp.admin.reports.churn.errorthreshold",SessionData.getLanguage())%>');
	}
	function validateThreshold(val, msg){
		val = trim(val);
		if(!validThreshold(val)){
			alert(msg);
			return false;
		}
		return true;
	}
	function validThreshold(num){
		var pattern = /^\d{1,3}(\.\d{1,2})?$/i;
		if(pattern.test(num)){
			num = num * 1;
			return (num >= 0.0 && num <= 100.0)
		}
		return false;
	}	
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle"><%=Languages.getString("jsp.admin.reports.churn.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3" class="formArea2">
			<form class="reportForm" name="mainform" method="get" action="admin/reports/analysis/churn_reporting.jsp" onsubmit="return validateForm();">
				<table>
					<tr>
						<td colspan="2" class="main"><%=Languages.getString("jsp.admin.reports.churn.prolog",SessionData.getLanguage())%></td>
					</tr>
					<tr>
						<td class="main"><b><%=Languages.getString("jsp.admin.reports.churn.frequency",SessionData.getLanguage())%>:</b></td>
						<td>
							<input class="main" name="frequency" type="radio" value="1" <%=frequency==1?"checked":"" %>><%=Languages.getString("jsp.admin.reports.churn.weekly",SessionData.getLanguage())%> 
							<input class="main" name="frequency" type="radio" value="2" <%=frequency==2?"checked":"" %>><%=Languages.getString("jsp.admin.reports.churn.monthly",SessionData.getLanguage())%>
						</td>
					</tr>
					<tr>
						<td class="main"><b><%=Languages.getString("jsp.admin.reports.churn.merchonly",SessionData.getLanguage())%></b></td>
						<td><input class="main" name="merchOnly" type="checkbox" value="Y" <%=merchOnly?"checked":"" %>></td>
					</tr>
					<tr>
						<td class="main"><b><%=Languages.getString("jsp.admin.reports.churn.merchdisabled",SessionData.getLanguage())%></b></td>
						<td><input class="main" name="merchdisabled" type="checkbox" value="Y" <%=merchdisabled==0?"checked":"" %>></td>
					</tr>
					<tr>
						<td class="main">
							<b><%=Languages.getString("jsp.admin.reports.churn.threshold",SessionData.getLanguage())%></b><br>
							<small><%=Languages.getString("jsp.admin.reports.churn.note1",SessionData.getLanguage())%></small>
						</td>
						<td><input class="main" id="threshold" name="threshold"  type="text" value="<%= (new DecimalFormat("#0.00").format(threshold)) %>" maxlength="5" size="5" style="text-align: right;"></td>
					</tr>
					<%=strComboIsos.toString()%>
					<tr>
						<td colspan="2" align="center" class="main">
							<input type="submit" name="showReport" value="<%=Languages.getString("jsp.admin.reports.churn.showreport",SessionData.getLanguage())%>">
							<input type="submit" name="download" value="<%=Languages.getString("jsp.admin.reports.churn.download",SessionData.getLanguage())%>">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<small><%=Languages.getString("jsp.admin.reports.churn.note",SessionData.getLanguage())%>:
							<br>* <%=Languages.getString("jsp.admin.reports.churn.note2",SessionData.getLanguage())%>
							<br>* <%=Languages.getString("jsp.admin.reports.churn.note4",SessionData.getLanguage())%></small>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
<%
if(showReport != null){
	Vector<Vector<String>> merchs = ChurnReport.getChurnReport(SessionData, merchdisabled, frequency, threshold/100, merchOnly,isosList);
	if(merchs.size() <= 1){
%>
	<div class="formArea"><big style="color:red;"><%=Languages.getString("jsp.admin.reports.error6",SessionData.getLanguage()) %></big></div>
<%
	}else{
	%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<script type="text/javascript">
	$(function(){
		var TableSorter1 = new TSorter;
		if (document.getElementById('t1') != null){
				TableSorter1.init('t1');
		}
	});	
</script>
<div class="formArea">
	<table width="300" cellspacing="1" cellpadding="1" border="0" align="center" class="sort-table" id="t1">
<%
		String ord="\"None\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"Number\",\"Number\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\"";
		int nrow = 0;
		for(Vector<String> vectTemp: merchs){
			if(nrow == 0){
				%><thead><tr class="rowhead2"><%
				for(String val: vectTemp){
					ord+=",\"Number\"";
					val = stripLeadingAndTrailingQuotes(val);
					%><td style="text-align:center;" <%=val.equals("#")?" abbr=\"number\"":"" %>><%=val.toUpperCase() %></td><%
				}
				%></tr></thead><%="\n" %><%
			}else{
				%><tr class="<%="row"+(nrow % 2 + 1) %>"><%
				for(String val: vectTemp){
					%><td width="100%" <%=((val.matches("^-?[0-9]+(.[0-9]+)?%?$")?"align=\"right\"":""))%>><%=val.equals("!!!")?"<img src=\"images/risk-icon.png\"\">":val %>&nbsp;</td><%
				}
				%></tr><%="\n" %><%
			}
			nrow++;
		}
%>
	</table>
          <SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  [<%= ord %>],0,false,false);
  -->
          </SCRIPT>	
</div>
	<%
	}
}
%>
<%@ include file="/includes/footer.jsp" %>
<%
}
%>