<%@ page import="java.util.*"%>
<%
  int section      = 4;
  int section_page = 41;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"	scope="session" />
<jsp:useBean id="TransactionReport"	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:setProperty name="Terminal" property="*" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp"%>

<%
	Vector vecSearchResults = new Vector();
	String strRuta = "";

	String strFormatedSerialNumber = Terminal.transformSerialNumber(SessionData);
		
	if(!"".equals(strFormatedSerialNumber))
		vecSearchResults = Terminal.getTerminalsBySerialNumbers(SessionData, strFormatedSerialNumber,null,false, 0, 1);

	vecSearchResults.remove(0);
	
	Vector vecTransformedResults = new Vector();
	for(int i=0;i<vecSearchResults.size();i++)
	{
		Vector vecTemp = (Vector)vecSearchResults.get(i);
		Vector vecTempNew = new Vector();
		vecTempNew.addElement(vecTemp.get(0));
		vecTempNew.addElement(vecTemp.get(1));
		vecTempNew.addElement(vecTemp.get(2));
		vecTempNew.addElement(vecTemp.get(8));
		vecTempNew.addElement(vecTemp.get(4));
		vecTempNew.addElement(vecTemp.get(5));	
		vecTransformedResults.add(vecTempNew); 
	}
	
	strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecTransformedResults,9,SessionData);
	
	if (strRuta.length() > 0)
	{
		response.sendRedirect(strRuta);
	}
%>
