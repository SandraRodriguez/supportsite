<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*,
                 com.debisys.dateformat.DateFormat" %>
<%
	int section=4;
	int section_page=12;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector<Vector<String>> vecSearchResults = new Vector<Vector<String>>();
	
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;
	String statuses;
	String sSortClient = "";

	Hashtable searchErrors = null;
	if ( request.getParameter("download") != null )
	{
	  	String sURL = "";
    	SessionData.setProperty("start_date", request.getParameter("startDate"));
    	SessionData.setProperty("end_date", request.getParameter("endDate"));	
		String strStatuses[] = request.getParameterValues("statuses");
    	TransactionReport.setStatuses(strStatuses);	
  	  	sURL = TransactionReport.downloadMerchantActivity(application, SessionData);
    	response.sendRedirect(sURL);
	  	return;
	}
  	if (request.getParameter("search") != null)
	{
		// Do all of the paging stuff here.
		if (request.getParameter("page") != null)
  		{
    		try
    		{
      			intPage=Integer.parseInt(request.getParameter("page"));
    		}
    		catch(NumberFormatException ex)
    		{
      			intPage = 1;
    		}
  		}
  		if (intPage < 1)
  		{
    		intPage=1;
  		}
	
		if (TransactionReport.validateDateRange(SessionData))
  		{
			String strStatuses[] = request.getParameterValues("statuses");
			
    		SessionData.setProperty("start_date", request.getParameter("startDate"));
    		SessionData.setProperty("end_date", request.getParameter("endDate"));
    		TransactionReport.setStatuses(strStatuses);
    		intRecordCount = TransactionReport.getCrazyMerchantActivityCount(SessionData);
    		SessionData.setProperty("recordCount", "" + intRecordCount);
    		if (intRecordCount>0)
    		{
      			intPageCount = (intRecordCount / intPageSize);
      			if ((intPageCount * intPageSize) < intRecordCount)
      			{
        			intPageCount++;
      			}
    		}
    		
    		vecSearchResults = TransactionReport.getCrazyMerchantActivity(SessionData, intPage);
  		}
  		else
  		{
   			searchErrors = TransactionReport.getErrors();
  		}
	}
%>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

</SCRIPT>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="800">
  <tr>
    <td background="images/top_blue.gif" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle" align="left">&nbsp;<%=Languages.getString("jsp.admin.reports.title17",SessionData.getLanguage()).toUpperCase()%></td>
    <td background="images/top_blue.gif" align="right"><img src="images/top_right_blue.gif"></td>
  </tr>
	<tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
  <tr>
    <td>
	    <form name="mainform" method="get" action="admin/reports/analysis/merchant_activity.jsp" onSubmit="scroll();">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        					<td class="formArea2">
          <%=Languages.getString("jsp.admin.reports.desc17",SessionData.getLanguage())%>
	          <table width="300">
              <tr class="main">
               <td nowrap="nowrap" valign="top">
               									<%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>
		         <%}%>:
               								</td>
               <td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap="nowrap">
												<table width="400">
<%
	if (searchErrors != null)
	{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1=searchErrors.keys();
		while(enum1.hasMoreElements())
		{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
		}

  out.println("</font></td></tr>");
	}
%>
													<tr class="main">
														<td nowrap="nowrap"><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>: </td><td><input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
													</tr>
													<tr class="main">
													    <td nowrap="nowrap"><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td><input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
													</tr>
													<tr class="main">
														<td nowrap="nowrap" valign="top"><%=Languages.getString("jsp.admin.reports.analysis.merchant_activity_report.status",SessionData.getLanguage())%>: </td>
														<td valign="top">
															<SELECT NAME="statuses" SIZE="5">
<%
																statuses = TransactionReport.getStatuses();
%>
																<OPTION VALUE="ALL" <%if(statuses.contains("ALL") || statuses.length() == 0){out.print("SELECTED"); } %>><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></OPTION>
																<OPTION VALUE="ACTIVE" <%if(statuses.contains("ACTIVE")){out.print("SELECTED"); } %>><%=Languages.getString("jsp.admin.reports.analysis.merchant_activity_report.criteria.active",SessionData.getLanguage())%></OPTION>      															
      															<OPTION VALUE="NOT_ACTIVE" <%if(statuses.contains("NOT_ACTIVE")){out.print("SELECTED"); } %>><%=Languages.getString("jsp.admin.reports.analysis.merchant_activity_report.criteria.notActive",SessionData.getLanguage())%></OPTION>
      															<OPTION VALUE="CANCELLED" <%if(statuses.contains("CANCELLED")){out.print("SELECTED"); } %>><%=Languages.getString("jsp.admin.reports.analysis.merchant_activity_report.criteria.cancelled",SessionData.getLanguage())%></OPTION>
      															<OPTION VALUE="DISABLED" <%if(statuses.contains("DISABLED")){out.print("SELECTED"); } %>><%=Languages.getString("jsp.admin.reports.analysis.merchant_activity_report.criteria.disabled",SessionData.getLanguage())%></OPTION>
	    													</SELECT>
														</td>
													</tr>
													<tr>
													    <td class="main" colspan="2">
															<input type="hidden" name="search" value="y" />
															<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.submit17",SessionData.getLanguage())%>" />
    </td>
													</tr>
												</table>
               </td>
              </tr>
				            		</table>
              </form>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
									<table width="100%" cellspacing="0" cellpadding="0" align="center" border="0">
            <tr>
            <td class="main">
												<%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
<%
              if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                   out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
%>
												<br>
<%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%>
  <table>
    <tr>
      <td align="left" class="main" nowrap="nowrap">
      <form method="post" action="admin/reports/analysis/merchant_activity.jsp">
       <input type="hidden" name="startDate" value="<%=TransactionReport.getStartDate()%>">
       <input type="hidden" name="endDate" value="<%=TransactionReport.getEndDate()%>">
       <input type="hidden" name="download" value="y">
															<input type="hidden" name="statuses" value="<%=request.getParameter("statuses")%>">
       <input type="submit" value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
      </form>
      </td>
    </tr>
            										<tr>
              											<td align="left" class="main" nowrap="nowrap">
<%
		if (intPage > 1)
		{
			out.println("<a href=\"admin/reports/analysis/merchant_activity.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +"&statuses=" + request.getParameter("statuses") + "&page=1\">First</a>&nbsp;");
			out.println("<a href=\"admin/reports/analysis/merchant_activity.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +"&statuses=" + request.getParameter("statuses") + "&page=" + (intPage-1) + "\">&lt;&lt;Prev</a>&nbsp;");
		}

		int intLowerLimit = intPage - 12;
		int intUpperLimit = intPage + 12;

		if (intLowerLimit<1)
		{
			intLowerLimit=1;
            intUpperLimit = 25;
		}

		for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
		{
			if (i==intPage)
			{
				out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
			}
			else
			{
				out.println("<a href=\"admin/reports/analysis/merchant_activity.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&statuses=" + request.getParameter("statuses") + "&page=" + i + "\">" +i+ "</a>&nbsp;");
			}
		}

		if (intPage <= (intPageCount-1))
		{
			out.println("<a href=\"admin/reports/analysis/merchant_activity.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&statuses=" + request.getParameter("statuses") + "&page=" + (intPage+1) + "\">Next&gt;&gt;</a>&nbsp;");
			out.println("<a href=\"admin/reports/analysis/merchant_activity.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&statuses=" + request.getParameter("statuses") + "&page=" + (intPageCount) + "\">Last</a>");
		}
%>
              											</td>
              										</tr>
  </table>
									            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
									      	      	<tr>
									                	<td abbr="number" class="rowhead2">#</td>
									                	<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.dba").toUpperCase()%></td>
<%
		sSortClient += "\"None\",\"CaseInsensitiveString\"";
		if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) 
		{
%>
														<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.street").toUpperCase()%></td>
														<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.cityStateZip").toUpperCase()%></td>
<%
			sSortClient += ",\"CaseInsensitiveString\",\"CaseInsensitiveString\"";
		}
%>
														<td abbr="number" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.siteID").toUpperCase()%></td>
														<td abbr="number" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.merchantID").toUpperCase()%></td>
														<td abbr="currency" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.merchantAccountBalance").toUpperCase()%></td>
														<td abbr="currency" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.repCurrentBalance").toUpperCase()%></td>
														<% 
															sSortClient += ",\"Number\",\"Number\",\"Number\",\"Number\"";
														 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_SUBAGENT_DETAILS_MERCHANT_ACTIVITY_REPORT)){ %>
															<td abbr="currency" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.subagentCurrentBalance").toUpperCase()%></td>
														<%	sSortClient += ",\"Number\"";
														  }%>
														<td abbr="number" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.transactionQty").toUpperCase()%></td>
														<td abbr="currency" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.amount").toUpperCase()%></td>
<%
		sSortClient += ",\"Number\",\"Number\"";
		if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
			DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
		{
%>
														<td abbr="currency" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.bonusAmount").toUpperCase()%></td>
														<td abbr="currency" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.totalRecharge").toUpperCase()%></td>
<%
			sSortClient += ",\"Number\",\"Number\"";
		}
%>
														<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.status").toUpperCase()%></td>
														<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.contact").toUpperCase()%></td>
														<td abbr="number" class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.contactPhone").toUpperCase()%></td>
<%
	sSortClient += ",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\"";
	if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
			customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
	{
%>
														<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.sms").toUpperCase()%></td>
<%
		sSortClient += ",\"CaseInsensitiveString\"";
	}
%>
														<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.terminalType").toUpperCase()%></td>
														<td class="rowhead2" abbr="date" axis="<%=DateFormat.getDateFormat()%>"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.signupDate").toUpperCase()%></td>
														<td class="rowhead2" abbr="date" axis="<%=DateFormat.getDateFormat()%>"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.transactionDate").toUpperCase()%></td>
														<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.rep").toUpperCase()%></td>
													
<%
		sSortClient += ",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\"";
	if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_SUBAGENT_DETAILS_MERCHANT_ACTIVITY_REPORT)){ %>
			<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.subagent").toUpperCase()%></td>
			
		<%
				sSortClient += ",\"CaseInsensitiveString\"";	
			}
					if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
					{
%>
														<td class="rowhead2"><%=SessionData.getString("jsp.admin.reports.analysis.merchant_activity_report.route").toUpperCase()%></td>
<%
						sSortClient += ",\"CaseInsensitiveString\"";
					}
%>
              </tr>
            </thead>
<%
            Iterator it = vecSearchResults.iterator();
            int intEvenOdd = 1;
		int iterator = (intPage - 1)*50 + 1;

                while (it.hasNext())
                {
                  Vector vecTemp = null;
                  vecTemp = (Vector) it.next();
			
			// Row number
			// Dba
			out.println(							"<tr class=row" + intEvenOdd +">" +
								                    	"<td nowrap>" + iterator++ + "</td>" +
										            	"<td nowrap>" + vecTemp.get(4).toString() + "</td>"
			);
			
			if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) 
                  {
				// Physical address
				// Location
				out.println(	        			   	"<td nowrap>" + vecTemp.get(5).toString() + "</td>" +  
										            	"<td nowrap>" + vecTemp.get(6).toString() + "</td>"
				);
                  }

			// Millennium #
			// Merchant ID
			// Merchant credit
			// Rep credit
			// Qty
			// Total recharge amt
			out.println(					           	"<td nowrap>" + vecTemp.get(3).toString() + "</td>" +  
										            	"<td nowrap>" + vecTemp.get(0).toString() + "</td>" +
										            	"<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +  
										            	"<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(11).toString()) + "</td>");
										            	if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_SUBAGENT_DETAILS_MERCHANT_ACTIVITY_REPORT)){	
										            		out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); 
										            	}
										            	out.println(
										            	"<td nowrap>" + vecTemp.get(8).toString() + "</td>" +
										            	"<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>"
			);
			
			if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
			{
			// 
				out.println(				           	"<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(10).toString()) + "</td>" +  
										            	"<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(12).toString()) + "</td>"
                              );
                  }
                  
			// Status			
			if ( vecTemp.get(13).toString().equals("1") && !vecTemp.get(14).toString().equals(""))
			{
				// Vector 13 is the cancelled column in merchants
				out.println("<td nowrap class=cancelledStatus>"+Languages.getString("jsp.admin.reports.cancelled",SessionData.getLanguage())+"</td>");
                      }
			else if (vecTemp.get(13).toString().equals("0") && !vecTemp.get(14).toString().equals(""))
                      {
				// Vector 14 is the datecancelled column
				out.println("<td nowrap class=disabledStatus>"+Languages.getString("jsp.admin.reports.disabled",SessionData.getLanguage())+"</td>");
                      }
           	else if (Double.parseDouble(vecTemp.get(9).toString()) == 0)
                      {
           		// Vector 9 is the total recharge amount
             	out.println("<td nowrap class=notActiveStatus>"+Languages.getString("jsp.admin.reports.not_active",SessionData.getLanguage())+"</td>");
                      }
           	else
                      {
           		// If it's all good then no worries bro
             	out.println("<td nowrap class=activeStatus>"+Languages.getString("jsp.admin.reports.active",SessionData.getLanguage())+"</td>");
                      }
			
			out.println(										            	
										            	"<td nowrap>" + vecTemp.get(15).toString() + "</td>" +
										            	"<td nowrap>" + vecTemp.get(16).toString() + "</td>"
			);
			
			if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
                      {
				out.println(				           	"<td nowrap>" + vecTemp.get(17).toString() + "</td>");
                      }
                      
			out.println(
										            	"<td nowrap>" + vecTemp.get(2).toString() + "</td>" + 
										            	"<td nowrap>" + vecTemp.get(18).toString() + "</td>" + 
										            	"<td nowrap>" + vecTemp.get(21).toString() + "</td>");
										            	out.println("<td nowrap>" + vecTemp.get(19).toString() + "</td>" );
										      			if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_SUBAGENT_DETAILS_MERCHANT_ACTIVITY_REPORT)){
										            	out.println("<td nowrap>" + vecTemp.get(22).toString() + "</td>");
										            	}
			
                      if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
					  {
				out.println(				           	"<td nowrap>" + vecTemp.get(20).toString() + "</td>");
					  }                      
                      
			out.println(
								                    "</tr>"
			);
	              
                        if (intEvenOdd == 1)
                        {
                          intEvenOdd = 2;
                        }
                        else
                        {
                          intEvenOdd = 1;
                        }


                      }
%>
												</table>
          </td>
      </tr>
    </table>
<%
    }
	else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null)
    {
 		out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.error6",SessionData.getLanguage())+"</font>");
    }    
%>
          						</td>
      						</tr>
    					</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<SCRIPT type="text/javascript">
  var stT1 = new SortROC(document.getElementById("t1"),
  [<%= sSortClient %>],0,false,false);
</SCRIPT>

<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>