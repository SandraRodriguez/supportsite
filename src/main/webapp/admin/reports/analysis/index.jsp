<%@ page import="java.util.*,java.util.ArrayList, com.debisys.presentation.ReportGroup"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
    // Warning! If section or section_page has been modified,
    // Please adjust header.jsp accordingly
	int section=4;
	int section_page=4;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
ReportGroup a = ReportGroup.createAnalysisGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType,customConfigType);
%>
          <TABLE cellSpacing=0 cellPadding=0 width=750 
            background=images/top_blue.gif border=0>
              <TBODY>
              <TR>
                <TD width=23 height=20><IMG height=20 
                  src="images/top_left_blue.gif" 
width=18></TD>
                <TD width="2000" class=main><B class="formAreaTitle"><%=Languages.getString("jsp.admin.reports.transactions",SessionData.getLanguage()).toUpperCase()%></B></TD>
                <TD width=28 height=20><IMG 
                  src="images/top_right_blue.gif"></TD></TR>
              <TR>
                <TD colSpan=3>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" 
                  bgColor=#7B9EBD border=0>
                    <TBODY>
                    <TR>
                      <TD width=1 bgColor=#7B9EBD><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top align=left bgColor=#ffffff>
                        <TABLE width="100%" border=0 
                        align="left" cellPadding=2 cellSpacing=0 class="fondoceldas">
                          <TBODY>
                          <TR>
                            <TD class=main>
<%@ include file="../qcommReportsFragment.jsp" %>
								<TABLE class=reportGroupTable>
                                <TBODY>
                                <TR>
                                <TH align=left><IMG height=22 
                                src="images/analysis_cube.png" 
                                width=22 border=0><span class="main">
                                <%=Languages.getString("jsp.admin.reports.analysis",SessionData.getLanguage()).toUpperCase() %>
                                </SPAN>
                                </TH></TR>
                                <TR>
                                <TD>
                                <UL class=sublevel name="main">
                                <% a.showItem(out); %>
                                </UL></TD></TR>
                                </TH></TR>
                                </TBODY></TABLE>
                                
									
							            	</td>
								<td width="18">
									<div id="divGroup"></div>
							            	</td>
							   		 	</tr>
									</table>
     				    		</td>
    				<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	  			</tr>
	  			<tr>
		  			<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	  			</tr>
 			</table>
 		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>