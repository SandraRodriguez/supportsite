<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.DateUtil"%>
<%@page import="com.debisys.utils.StringUtil"%>
<%
int section=4;
int section_page=32;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
boolean bUseTaxValue = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns

if ( request.getParameter("Download") != null )
{
	  String sURL = "";
	  TransactionReport.setProductIds(request.getParameter("_productIds"));
	  TransactionReport.setMerchantIds(request.getParameter("_merchantIds"));
      
      sURL = TransactionReport.downloadRechargeDataByPhoneNumber(application, SessionData);
      
      response.sendRedirect(sURL);
	  return;
}
if (request.getParameter("search") != null)
{
  if (TransactionReport.validateDateRange(SessionData) )
  {
     if (DateUtil.getDaysBetween(request.getParameter("startDate"),request.getParameter("endDate")) <= 90)
     { 
	    SessionData.setProperty("start_date", request.getParameter("startDate"));
	    SessionData.setProperty("end_date", request.getParameter("endDate"));
	    String strProductIds[] = request.getParameterValues("pids");
	    if (strProductIds != null)
	    {
	      TransactionReport.setProductIds(strProductIds);
	    }
	    String strMerchantIds[] = request.getParameterValues("merchantIds");
	    if (strMerchantIds != null)
	    {
	      TransactionReport.setMerchantIds(strMerchantIds);
	    }
	
	    if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
	    {
	      SessionData.setProperty("deploymentType", DebisysConfigListener.getDeploymentType(application));
	    }
	    
	    if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
	    {
	      SessionData.setProperty("customConfigType", DebisysConfigListener.getCustomConfigType(application));
	    }    
	    vecSearchResults = TransactionReport.getRechargeDataByPhoneNumber(SessionData,application,false);
	 }
	 else
	 {
	    TransactionReport.addFieldError("star_end_date",Languages.getString("jsp.admin.reports.analysis.error_date",SessionData.getLanguage()));
	    searchErrors = TransactionReport.getErrors();
	 }    
  }
  else
  {
   searchErrors = TransactionReport.getErrors();
  }
}
%>
<%@ include file="/includes/header.jsp" %>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td background="images/top_blue.gif" width="18"  align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" class="formAreaTitle" width="5000"><%=Languages.getString("jsp.admin.reports.analysis.topedupphones",SessionData.getLanguage()).toUpperCase()%></td>
    <td background="images/top_blue.gif" width="12" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF" class="formArea2">

<%
if (searchErrors != null)
{
  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr></table>");
}
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main">
              <%
              if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
            %><br><font color="#ff0000"><%=Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage())%></font>
            </td>
             <td class=main align=right>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%>
             </td>
            </tr>            
            <tr>
				<td class="main" colspan="2">
				 <%=Languages.getString("jsp.admin.reports.analysis.cvs",SessionData.getLanguage())%>:

				<FORM ACTION="admin/reports/analysis/topedupphones_summary.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
					<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
					<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
					<INPUT TYPE=hidden NAME=_productIds VALUE="<%=TransactionReport.getProductIds()%>">
					<INPUT TYPE=hidden NAME=_merchantIds VALUE="<%=TransactionReport.getMerchantIds()%>">
					<INPUT TYPE=hidden NAME=Download VALUE="Y">
					<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
				</FORM>
				</td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2" border="0" class="sort-table" id="t1">
            <thead>
	            <tr class="SectionTopBorder">            
	              <td class=rowhead2 valign=bottom align="center">#</td>
	              <td class=rowhead2 valign=bottom align="center"><%=Languages.getString("jsp.admin.reports.analysis.topedupphones.ani",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
	              <td class=rowhead2 valign=bottom align="center"><%=Languages.getString("jsp.admin.reports.analysis.topedupphones.frecuency",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
	              <td class=rowhead2 valign=bottom align="center"><%=Languages.getString("jsp.admin.reports.analysis.topedupphones.totalamountrecharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
	              <td class=rowhead2 valign=bottom align="center"><%=Languages.getString("jsp.admin.reports.analysis.topedupphones.avg",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
	            </tr>
            </thead>
<%                  
                  int intTotalQtySum = 0;
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  int intCounter = 1;
                  while (it.hasNext())
                  {
	                    Vector vecTemp = null;
	                    vecTemp = (Vector) it.next();
	                    String ani = vecTemp.get(0).toString();
	                    String frecuency = vecTemp.get(1).toString();
	                    String totalamountrecharge = NumberUtil.formatCurrency( vecTemp.get(2).toString());
	                    String avg = NumberUtil.formatCurrency(vecTemp.get(3).toString());	 	                   
						%>
						  <tr class="row<%=intEvenOdd%>">
						     <td align="center"><%=intCounter%>&nbsp;</td>
						     <td align="right"><%=ani%>&nbsp;</td>
						     <td align="center"><%=frecuency%>&nbsp;</td>
						     <td align="right"><%=totalamountrecharge%>&nbsp;</td>
						     <td align="right"><%=avg%>&nbsp;</td>
						  </tr>
						
						<% 
	                    if (intEvenOdd == 1)
	                    {
	                      intEvenOdd = 2;
	                    }
	                    else
	                    {
	                      intEvenOdd = 1;
	                    }
	                    intCounter++;

                  }
                  vecSearchResults.clear();
            %>            
			</table>
<SCRIPT type="text/javascript">
<!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None","CaseInsensitiveString", "Number", "Number", "Number"],0,false,false);
-->            
</SCRIPT>			
		<%
		}
		else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null)
		{
		 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
		}
		%>
</td>
</tr>
<tr>
 <td>
              
     <INPUT ID=btnSubmitback TYPE=button VALUE="<%=Languages.getString("jsp.admin.reports.analysis.back",SessionData.getLanguage())%>" onclick="history.back();">
              
 </td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>
