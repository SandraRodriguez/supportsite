<%--
    Document   : merchant_stock_levels
    Created on : Oct 13, 2015, 10:43:39 AM
    Author     : dgarzon
--%>

<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.customers.CustomerSearch"%>
<%@page import="com.debisys.tools.stockLevels.MerchantStockLevels"%>
<%@page import="com.debisys.tools.stockLevels.MerchantsStockLevelsPojo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 4;
    int section_page = 64;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="/support/js/dragUtil.js"></script>

<%
    String strRefId = SessionData.getProperty("ref_id");
    String sessionLanguage = SessionData.getUser().getLanguageCode();
    String reportCode = "MerchantMapLocator";
    String iso_id = SessionData.getProperty("iso_id");

    String reportTitle = Languages.getString("jsp.admin.reports.titleMStockLevel", SessionData.getLanguage());
    MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
    List<MerchantsStockLevelsPojo> stockLevelsList = merchantStockLevels.getMerchantStockLevelsByIso(null, iso_id);
    ArrayList<String> headers = new ArrayList<String>();

    ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport(reportCode, sessionLanguage, null);
    String drag = "drag";
    String dragTitle = "dragTitle";
    String close = "close";
    String closeTitle = "closeTitle";
    String decrement = "decrement";
    String increment = "increment";
    String transparency = "transparency";
    String goCenter = "goCenter";
    String goCenterTitle = "goCenterTitle";
    String titleReportLabel = "merchantMapLocatorTitle";

    /*DEFAULT VALUES*/
    HashMap parametersName = new HashMap();
    parametersName.put(titleReportLabel, "Merchant Map Locator");
    parametersName.put(drag, "Drag here");
    parametersName.put(dragTitle, "To move this window");
    parametersName.put(close, "Close Map");
    parametersName.put(closeTitle, "Click to close the map");
    parametersName.put(decrement, "To decrement transparency");
    parametersName.put(increment, "To increment transparency");
    parametersName.put(transparency, "Transparency");
    parametersName.put(goCenter, "Go center");
    parametersName.put(goCenterTitle, "Click to go the main center");
    /*DEFAULT VALUES*/

    for (ResourceReport resource : resources) {
        if (parametersName.containsKey(resource.getKeyMessage())) {
            parametersName.put(resource.getKeyMessage(), resource.getMessage());
        }
    }
    String noMerchantsFound = "noMerchantsFound";
    HashMap headersDownload = new HashMap();
    headersDownload.put("merchantName", "Merchant Name!");
    headersDownload.put("merchantId", "Merchant Id!");
    headersDownload.put("merchantPhone", "Phone #!");
    headersDownload.put("balance", "Balance!");
    headersDownload.put("RepName", "Rep Name!");
    headersDownload.put(noMerchantsFound, "No merchants found!!!");

    reportCode = "MerchantStockLevels";
    resources = ResourceReport.findResourcesByReport(reportCode, sessionLanguage, null);

    for (ResourceReport resource : resources) {
        if (headersDownload.containsKey(resource.getKeyMessage())) {
            headersDownload.put(resource.getKeyMessage(), resource.getMessage());
        }
    }

    headers.add(headersDownload.get("merchantName").toString());
    headers.add(headersDownload.get("merchantId").toString());
    headers.add(headersDownload.get("merchantPhone").toString());
    headers.add(headersDownload.get("balance").toString());
    headers.add(headersDownload.get("RepName").toString());

    ArrayList<PropertiesByFeature> propertiesByFeature = PropertiesByFeature.findPropertiesByFeature(reportCode);

    String latitudeValue = "latitude";
    String longitudeValue = "longitude";
    String zoomValue = "zoom";
    String showLowLink = "showLowLink";
    String showMedLink = "showMedLink";
    String showHigLink = "showHigLink";
    String showOutLink = "showOutLink";

    /*DEFAULT VALUES*/
    HashMap propertiesValues = new HashMap();
    propertiesValues.put(latitudeValue, "25.05");
    propertiesValues.put(longitudeValue, "-77.33");
    propertiesValues.put(zoomValue, "12");
    propertiesValues.put(showLowLink, "1");
    propertiesValues.put(showMedLink, "0");
    propertiesValues.put(showHigLink, "0");
    propertiesValues.put(showOutLink, "1");
    /*DEFAULT VALUES*/

    for (PropertiesByFeature property : propertiesByFeature) {
        propertiesValues.put(property.getProperty(), property.getValue());
    }
    String action = request.getParameter("action");
    StringBuilder javascriptActions = new StringBuilder();
    if (action != null && action.equals("downLows")) {
        String currentId = request.getParameter("currentId");
        String low = request.getParameter("low");
        String max = request.getParameter("max");

        ArrayList<String> fieldToRead = new ArrayList<String>();
        fieldToRead.add("m.dba");
        fieldToRead.add("m.merchant_id");
        fieldToRead.add("m.contact_phone");
        fieldToRead.add("m.LiabilityLimit");
        fieldToRead.add("m.RunningLiability");
        fieldToRead.add("r.businessname");

        {
            MerchantStockLevels merchantStockLevels1 = new MerchantStockLevels();
            String sqlQuery = merchantStockLevels1.getReportQuery(currentId, fieldToRead, strAccessLevel, strRefId, strDistChainType, low, max);
            if (sqlQuery != null && sqlQuery.length() > 0) {

                Vector vecEntitiesList = CustomerSearch.executeReport(sqlQuery, fieldToRead);

                if (vecEntitiesList.size() > 0) {
                    String strFileName = merchantStockLevels1.generateFileName(application);
                    ArrayList<String> titles = new ArrayList<String>();
                    titles.add(reportTitle);
                    String finalDownloadFilName = merchantStockLevels1.download(vecEntitiesList, SessionData, application, headers, strFileName, titles);
                    response.sendRedirect(finalDownloadFilName);
                }
            }
        }
        javascriptActions.append("$('#merchantsStockLevelsList').val('" + currentId + "');" + '\n');
        javascriptActions.append("setTimeout(function () " + '\n');
        javascriptActions.append("{ " + '\n');
        javascriptActions.append("    doReport(); " + '\n');
        javascriptActions.append("}, 500); " + '\n');

    }
%>

<script LANGUAGE="JavaScript">
    var count = 0;
    var mainLatitude = <%=propertiesValues.get(latitudeValue)%>;
    var mainLongitude = <%=propertiesValues.get(longitudeValue)%>;
    var zoomDefault = <%=propertiesValues.get(zoomValue)%>;
    var drag = "<%=parametersName.get(drag)%>";
    var dragTitle = "<%=parametersName.get(dragTitle)%>";
    var close = "<%=parametersName.get(close)%>";
    var closeTitle = "<%=parametersName.get(closeTitle)%>";
    var decrement = "<%=parametersName.get(decrement)%>";
    var increment = "<%=parametersName.get(increment)%>";
    var transparency = "<%=parametersName.get(transparency)%>";
    var goCenter = "<%=parametersName.get(goCenter)%>";
    var goCenterTitle = "<%=parametersName.get(goCenterTitle)%>";
    var showLowLink = "<%=propertiesValues.get(showLowLink)%>";
    var showMedLink = "<%=propertiesValues.get(showMedLink)%>";
    var showHigLink = "<%=propertiesValues.get(showHigLink)%>";
    var showOutLink = "<%=propertiesValues.get(showOutLink)%>";

    var markers = [];
    var centerControlDiv;
    var map = null;
    var markers = [];
    //                    0    1    2      3      4     5     6     7     8     9    10   
    var opacityValues = ["0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1"];
    var opacityPointer = 10;
    var statusWindowMap = 0;
    var urlThresholdValues = "admin/tools/stockLevelsThreshold/stockLevelsThresholdVerifyDB.jsp";
    var stockLevelsReport = "/support/admin/reports/analysis/merchantStockLevelsReport.jsp";

    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ", "< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");

    function scroll() {
        document.mainform.submit.disabled = true;
        document.mainform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll()', 150);
    }

    function scroll2() {
        document.downloadform.submit.disabled = true;
        document.downloadform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll2()', 150);
    }

    function entityLocation(merchantId, latitude, longitude) {
        this.merchantId = merchantId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    function complete() {
        $("#messagesStockLevels").text("");
    }

    function doReport() {
        initialize();
        //var waitingControl = "<img src='images/global-wait.gif'>";
        var currentId = $("#merchantsStockLevelsList").val();
        var url = urlThresholdValues;
        var w = $(window).width();
        var h = $(window).height();
        $("#map-canvas").width(w);
        $("#map-canvas").height(h);
        $("#map-canvas").css({"top": "1px", "left": "1px", "position": "absolute"});
        setAllMap();
        var countLow = 0;
        var countMed = 0;
        var countHei = 0;
        var countOut = 0;
        $("#map-canvas").hide();
        $.post(
                url,
                {
                    merchantStockLevelId: currentId,
                    responseType: "json",
                    action: "stockLevelsReport",
                },
                function (data)
                {
                    var images = [];
                    var dataTrim = data;
                    var length = Object.keys(dataTrim.items).length;
                    if (length === 0) {                        
                        var imagesInfo = setThresholdValues(currentId, countLow, countMed, countHei, countOut);
                        addTable(imagesInfo);                        
                        $("#messagesStockLevels").text("<%=headersDownload.get(noMerchantsFound)%>");
                        $("#messagesStockLevels").show();
                        $("#messagesStockLevels").fadeOut(2500, "linear", complete);                        
                    } else {
                        $.each(dataTrim.items, function (i, item) {
                            var latitude = item.latitude;
                            var longitude = item.longitude;
                            var dba = unescape(item.dba);
                            var balance = item.LiabilityLimit - item.RunningLiability;

                            var pinColor;

                            if (balance >= item.lowMinValue && balance <= item.lowMaxValue) {
                                pinColor = item.lowColor.substr(1);
                                countLow++;
                            } else if (balance >= item.mediumMinValue && balance <= item.mediumMaxValue) {
                                pinColor = item.mediumColor.substr(1);
                                countMed++;
                            } else if (balance >= item.highMinValue && balance <= item.highMaxValue) {
                                pinColor = item.highColor.substr(1);
                                countHei++;
                            } else {
                                pinColor = "FFFFFF";
                                countOut++;
                            }

                            var title = dba + " Balance=[" + balance.toFixed(2) + "] ";
                            var urlImage = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor;

                            var pinImage = new google.maps.MarkerImage(urlImage,
                                    new google.maps.Size(21, 34),
                                    new google.maps.Point(0, 0),
                                    new google.maps.Point(10, 34));

                            images.push(urlImage);

                            var latLng = new google.maps.LatLng(latitude, longitude);
                            addMarker(latLng, pinImage, title);
                            if (i === length - 1) {
                                var imagesInfo = setThresholdValues(currentId, countLow, countMed, countHei, countOut);
                                addTable(imagesInfo);
                            }
                        });
                    }
                },
                "json"
                );
            $("#map-canvas").show();
            setMapZoomValue();
    }

    function setThresholdValues(currentId, countLow, countMed, countHei, countOut) {
        var imagesInfo = "";
        $.ajax({
            type: "POST",
            async: false,
            data: {IdMerchantsStockLevels: currentId, action: 'getMerchantsStockLevels'},
            url: urlThresholdValues,
            success: function (data) {
                var array_data = String($.trim(data)).split(",");
                $("#description").val(array_data[0]);
                var lowMinValue = array_data[1];
                var lowMaxValue = array_data[2];
                var mediumMinValue = array_data[3];
                var mediumMaxValue = array_data[4];
                var highMinValue = array_data[5];
                var highMaxValue = array_data[6];
                var lowColor = array_data[7];
                var mediumColor = array_data[8];
                var highColor = array_data[9];

                var infoLow = "<%=Languages.getString("jsp.admin.tools.stockLevels.low", SessionData.getLanguage())%>  = [" + lowMinValue + " , " + lowMaxValue + "]";
                var infoMedium = "<%=Languages.getString("jsp.admin.tools.stockLevels.medium", SessionData.getLanguage())%> = [" + mediumMinValue + " , " + mediumMaxValue + "]";
                var infoHigh = "<%=Languages.getString("jsp.admin.tools.stockLevels.high", SessionData.getLanguage())%>  = [" + highMinValue + " , " + highMaxValue + "]";
                var infoOutSideRange = "<%=Languages.getString("jsp.admin.tools.stockLevels.outsideRange", SessionData.getLanguage())%> ";

                var urlImageLow = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + lowColor.substr(1);
                var urlImageMedium = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + mediumColor.substr(1);
                var urlImageHigh = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + highColor.substr(1);
                var urlImageOutSide = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + "FFFFFF";

                var heightPin = '20';

                var supportDownload = "";
                //stockLevelsReport + "?action=downLows&currentId="+currentId + "&low="+lowMinValue+"&max="+lowMaxValue;

                var lowHtml = infoLow + ":";
                if (showLowLink === "1" && countLow > 0) {
                    supportDownload = stockLevelsReport + "?action=downLows&currentId=" + currentId + "&low=" + lowMinValue + "&max=" + lowMaxValue;
                    lowHtml = "<a style='text-decoration:underline;' href='" + supportDownload + "' >" + infoLow + "</a>:";
                    lowHtml = lowHtml + "<img  src='" + urlImageLow + "' height='" + heightPin + "px'/>";
                }

                var mediumHtml = infoMedium + ":";
                if (showMedLink === "1" && countMed > 0) {
                    supportDownload = stockLevelsReport + "?action=downLows&currentId=" + currentId + "&low=" + mediumMinValue + "&max=" + mediumMaxValue;
                    mediumHtml = "<a style='text-decoration:underline;' href='" + supportDownload + "' >" + infoMedium + "</a>:";
                    mediumHtml = mediumHtml + "<img  src='" + urlImageMedium + "' height='" + heightPin + "px'/>";
                }

                var highHtml = infoHigh + ":";
                if (showHigLink === "1" && countHei > 0) {
                    supportDownload = stockLevelsReport + "?action=downLows&currentId=" + currentId + "&low=" + highMinValue + "&max=" + highMaxValue;
                    highHtml = "<a style='text-decoration:underline;' href='" + supportDownload + "' >" + infoHigh + "</a>:";
                    highHtml = highHtml + "<img  src='" + urlImageHigh + "' height='" + heightPin + "px'/>";
                }

                var outHtml = infoOutSideRange + ":";
                if (showOutLink === "1" && countOut > 0) {
                    supportDownload = stockLevelsReport + "?action=downLows&currentId=" + currentId + "&low=-1&max=-1";
                    outHtml = "<a style='text-decoration:underline;' href='" + supportDownload + "' >" + infoOutSideRange + "</a>:";
                    outHtml = outHtml + "<img  src='" + urlImageOutSide + "' height='" + heightPin + "px'/>";
                }

                var tableInfo = "<table border='1' style='width: 1050px;border-collapse: collapse;'>";
                tableInfo = tableInfo + "<tr>";
                tableInfo = tableInfo + " <td>" + lowHtml + " </td>";
                tableInfo = tableInfo + " <td>" + mediumHtml + " </td>";
                tableInfo = tableInfo + " <td>" + highHtml + " </td>";
                tableInfo = tableInfo + " <td>" + outHtml + " </td>";
                tableInfo = tableInfo + "</tr>";

                tableInfo = tableInfo + "<tr><td>" + countLow + " Merchants</td><td>" + countMed + " Merchants</td><td>" + countHei + " Merchants </td><td>" + countOut + " Merchants </td></tr>";
                tableInfo = tableInfo + "</table>";

                imagesInfo = tableInfo;

                //imagesInfo = "<a id='lowLinkStockLevel' style='text-decoration:underline;' href='"+supportDownload+"' >"+infoLow+"</a>:<img  src='"+urlImageLow+"' height='"+heightPin+"px'/>";
                //imagesInfo = imagesInfo + infoMedium+":<img  src='"+urlImageMedium+"' height='"+heightPin+"px'/>";
                //imagesInfo = imagesInfo + infoHigh+":<img  src='"+urlImageHigh+"' height='"+heightPin+"px'/>";
                //imagesInfo = imagesInfo + infoOutSideRange+":<img  src='"+urlImageOutSide+"' height='"+heightPin+"px'/>";                

            }
        });
        return imagesInfo;
    }

    function addTable(imagesHtml) {
        if ($("#mainTable") != null) {
            $("#mainTable").remove();
        }
        //var myTableDiv = document.getElementById(divParent);
        var table = document.createElement('TABLE');
        table.id = 'mainTable';
        table.style.width = '1450px';
        var tableBody = document.createElement('TBODY');
        table.appendChild(tableBody);
        var tr = document.createElement('TR');
        tableBody.appendChild(tr);

        createTD(tr, 'dragControlLocator', drag, dragTitle, '');
        createTD(tr, 'imagesPins', '', '', imagesHtml);
        createTD(tr, 'opacity', transparency, '', '');
        createTD(tr, 'goToCenter', goCenter, goCenterTitle, '');
        createTD(tr, 'closeMap', close, closeTitle, '');

        if (centerControlDiv != null)
            centerControlDiv.appendChild(table);
        else
            alert("fail adding table/div");
    }


    function createTD(tr, id, text, title, innerHTML) {
        var td = document.createElement('TD');

        var controlUI = document.createElement('div');
        controlUI.id = id;
        applyStyleControl(controlUI);
        controlUI.title = title;
        controlUI.innerHTML = text;

        if (id === "dragControlLocator") {
            controlUI.style.cursor = 'pointer';
            controlUI.onmousedown = function () {
                _drag_init(document.getElementById('map-canvas'));
                return false;
            };
        } else if (id === "closeMap") {
            controlUI.style.cursor = 'pointer';
            statusWindowMap = 1;
            controlUI.addEventListener('click', function () {
                $("#map-canvas").hide();
            });
        } else if (id === "imagesPins") {
            controlUI.innerHTML = innerHTML;
            controlUI.style.height = '40px';
        } else if (id === "opacity") {
            var controlUI_Plus = document.createElement('button');
            controlUI_Plus.style.backgroundColor = '#fff';
            applyStyleControl(controlUI_Plus);
            controlUI_Plus.title = increment;
            controlUI_Plus.innerHTML = '-';
            controlUI.appendChild(controlUI_Plus);
            controlUI_Plus.addEventListener('click', function () {
                opacityControl("+");
            });

            var controlUI_Minus = document.createElement('button');
            applyStyleControl(controlUI_Minus);
            controlUI_Minus.title = decrement;
            controlUI_Minus.innerHTML = '+';
            controlUI.appendChild(controlUI_Minus);
            controlUI_Minus.addEventListener('click', function () {
                opacityControl("-");
            });
        } else if (id === "goToCenter") {
            controlUI.style.cursor = 'pointer';
            controlUI.addEventListener('click', function () {
                setDefaultCenter();
            });
        }
        td.appendChild(controlUI);
        tr.appendChild(td);
    }

    function applyStyleControl(controlUI) {
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.height = '35px';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
    }

    function opacityControl(action) {
        if (action === "+") {
            if (opacityPointer < 10) {
                opacityPointer = opacityPointer + 1;
            }
        } else {
            if (opacityPointer <= 10 && opacityPointer > 2) {
                opacityPointer = opacityPointer - 1;
            } else {
                opacityPointer = 2;
            }
        }
        $("#map-canvas").css({"opacity": opacityValues[opacityPointer]});
    }

    function initialize() {
        var mapOptions = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        if (map === null) {
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            centerControlDiv = document.createElement('div');
            centerControlDiv.id = "centerControlDiv";
            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.LEFT_TOP].push(centerControlDiv);
        }
        setDefaultCenter();
    }

    function setDefaultCenter() {
        map.setCenter(new google.maps.LatLng(mainLatitude, mainLongitude));
        setMapZoomValue();
    }

    function setMapZoomValue() {
        map.setZoom(zoomDefault);
    }

    function addMarker(location, pinImage, title) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: pinImage,
            title: title
        });
        markers.push(marker);
    }

    function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    $(document).ready(function () {
    <%= javascriptActions.toString()%>
    });

</script>

<link rel="stylesheet" type="text/css" href="css/maps.css" title="style">



<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td style="background-image:url(images/top_blue.gif)" width="2000" class="formAreaTitle">&nbsp;<%=reportTitle.toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
</tr>
<tr>
<td colspan="3"  bgcolor="#FFFFFF">
    <table style="width:'100%';" cellpadding="0" cellspacing="0">
        <tr>
        <td class="formArea2">
            <table style="width:'1500px';" >
                <tr>
                <td align="center" style="border-spacing: 20px;">
                    <%=Languages.getString("jsp.admin.tools.stockLevels.merchantsStockLevelList", SessionData.getLanguage())%>
                    <select name="merchantsStockLevelsList" id = "merchantsStockLevelsList">
                        <option value="-1"></option>
                        <%
                            for (MerchantsStockLevelsPojo stockLevels : stockLevelsList) {%>  
                        <option value="<%=stockLevels.getId()%>"><%=stockLevels.getDescription()%></option>
                        <%}%>
                    </select>
                </td>
                <td align="center" style="border-spacing: 20px;">
                <span id="messagesStockLevels" style='color:red;'></span>
        </td>
        </tr>                            
        <tr>
        <td class=main align="center" >
            <input type="hidden" name="search" value="y">

            <input type="button" name="button" value="<%=Languages.getString("jsp.admin.reports.show_report", SessionData.getLanguage())%>" onclick="doReport();">

        </td>
        </tr>
        <tr align="center">
        <td colspan="3" align="center">

        </td>
        </tr>
    </table>
</td>
</tr>
</table>

</td>
</tr>

</table>

<div id="map-canvas"></div>  

<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>