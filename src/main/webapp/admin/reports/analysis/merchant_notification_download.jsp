<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 java.io.PrintWriter,
                 com.debisys.reports.jfreechart.Chart,
                 com.debisys.utils.*,
                 com.debisys.languages.Languages"%><%
int section=4;
int section_page=24;
//had to remove all spaces or else it creates white space at the top of the csv.
%><jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/><jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/><jsp:setProperty name="TransactionReport" property="*"/><%@ include file="/includes/security.jsp"%><%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
vecSearchResults = TransactionReport.getMerchantNotificationList(SessionData);
String deploymentType = DebisysConfigListener.getDeploymentType(application);
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
  response.setContentType("application/text");
  response.setHeader("Content-Disposition", "attachment;filename=\"merchant_list.csv\"");
	if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
	{
%>"#","<%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.id",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.status",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.contact",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.phone",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.email",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.legal_name",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.street",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.city",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.state",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.zip",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.fax",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.rep_name",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.signup_date",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.last_transaction_date",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.customers.merchants_edit.route",SessionData.getLanguage())%>"
<%}else{
%>"#","<%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.id",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.status",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.contact",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.phone",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.email",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.legal_name",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.street",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.city",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.state",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.zip",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.fax",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.rep_name",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.signup_date",SessionData.getLanguage())%>","<%=Languages.getString("jsp.admin.reports.notification_list.last_transaction_date",SessionData.getLanguage())%>"
<%}
Iterator it = vecSearchResults.iterator();
int intCounter = 1;

    while (it.hasNext())
    {
      Vector vecTemp = null;
      vecTemp = (Vector) it.next();
      /*
      0 dba
      1 merchant_id
      2 contact
      3 contact_phone
      4 email
      5 datecancelled
      */
      String dateCancelled = vecTemp.get(5).toString();
      out.print("\"" + intCounter++ + "\"" +
          ",\"" + vecTemp.get(0).toString().replaceAll("\"","\"\"") + "\"" +
          ",\"" + vecTemp.get(1).toString().replaceAll("\"","\"\"") + "\"");
          //determine status
          out.print(",\"");
          if (!dateCancelled.equals(""))
          {
            out.print(Languages.getString("jsp.admin.reports.disabled",SessionData.getLanguage()));
          }
          else
          {
            out.print(Languages.getString("jsp.admin.reports.active",SessionData.getLanguage()));
          }
          String strRouteName = "";
          if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
		  {
		      strRouteName = ",\"" + vecTemp.get(15).toString().replaceAll("\"","\"\"") + "\"";		 
		  }          
          out.print("\"");
          out.println(",\"" + vecTemp.get(2).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(3).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(4).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(6).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(7).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(8).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(9).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(10).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(11).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(12).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(13).toString().replaceAll("\"","\"\"") + "\"" +
                      ",\"" + vecTemp.get(14).toString().replaceAll("\"","\"\"") + "\"" +
                      strRouteName);

          }

}
else if (vecSearchResults.size()==0)
{
 out.println(Languages.getString("jsp.admin.reports.error6",SessionData.getLanguage()));
}
%>