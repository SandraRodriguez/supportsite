<%@ page import="java.util.*"%>
<%
  int section=4;
  int section_page=5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"	scope="session" />
<jsp:useBean id="TransactionReport"	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:setProperty name="Terminal" property="*" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp"%>

<%
	Vector vecSearchResults = new Vector();
	String strRuta = "";

	String strFormatedSerialNumber = Terminal.transformSerialNumber(SessionData);
		
	String report = request.getParameter("report");
	String start = request.getParameter("start");
	String end = request.getParameter("end");
	
	if (report!=null)
	{
		TransactionReport.setStartDate(start);
		TransactionReport.setEndDate(end);
		if (report.equals("Daily"))
		{
			vecSearchResults = TransactionReport.getDailyVolume(SessionData);
			//strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,6,);
		}
		else if ( report.equals("Monthly") )
		{
		    vecSearchResults = TransactionReport.getTop10Products(SessionData);
			//strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,5);
		}
		else if ( report.equals("Weekly") )
		{
		    vecSearchResults = TransactionReport.getWeeklyVolume(SessionData);
			//strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,8);
		}		
	}	
	
	if (strRuta.length() > 0)
	{
		response.sendRedirect(strRuta);
	}
%>
