<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.terminals.Terminal"
                  %>
<%
  int section      = 4;
  int section_page = 41;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
  <jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
  <jsp:setProperty name="Terminal" property="*" />
  <jsp:setProperty name="TransactionReport" property="*" />
  
  <%@ include file="/includes/security.jsp" %>
  <%@ include file="/includes/header.jsp" %>
  
  
<%
Vector<Vector<Object>>    vecSearchResults = new Vector<Vector<Object>>();
Hashtable searchErrors     = null;
String    sortString       = "";

int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;

  if (request.getParameter("search") != null)
  {
  	if (request.getParameter("page") != null)
	{
	  try
	  {
	    intPage=Integer.parseInt(request.getParameter("page"));
	  }
	  catch(NumberFormatException ex)
	  {
	    intPage = 1;
	  }
	}
	if (intPage < 1)
	{
	  intPage=1;
	}
    if (Terminal.validateSerialNumber(SessionData) )
    {		
		String strFormatedSerialNumber = Terminal.transformSerialNumber(SessionData);
		if(!"".equals(strFormatedSerialNumber))
			vecSearchResults = Terminal.getTerminalsBySerialNumbers(SessionData, strFormatedSerialNumber, null,false,intPageSize, intPage);
		else		
			searchErrors = Terminal.getErrors();					
    }
    else
    {
      searchErrors = Terminal.getErrors();
    }
  }

  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
	Iterator it  = vecSearchResults.iterator();
	Vector vecCounts = (Vector)it.next();
	Integer intNumRecords = (Integer)vecCounts.get(0); 
	intRecordCount = intNumRecords.intValue();
	if (intRecordCount>0)
	{
	  intPageCount = (intRecordCount / intPageSize);
	  if ((intPageCount * intPageSize) < intRecordCount)
	  {
	    intPageCount++;
	  }
	}	
  }
%>
	<SCRIPT LANGUAGE="JavaScript">
		var count = 0
	    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");
	
	    function scroll2()
	    {
	    document.downloadform.submit.disabled = true;  
	    document.downloadform.submit.value = iProcessMsg[count];
	    count++
	    if (count = iProcessMsg.length) count = 0
	    setTimeout('scroll2()', 150);
	    }
	    
	    function submitPage(thePage)
	    {
	        document.submitPageForm.page.value = thePage;  
	    	document.submitPageForm.submit();
	    }
	</SCRIPT>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.analisys.terminal_search.title",SessionData.getLanguage()).toUpperCase()%></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }
		
        if (intRecordCount > 0)
        {
        	int      intEvenOdd               = 1;
            int      intCounter               = 1;
       		Iterator it  = vecSearchResults.iterator();
			Vector vecCounts = (Vector)it.next();

%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td class="main">
<%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
<%
                out.println(Languages.getString("jsp.admin.reports.analisys.terminal_search.terminalsFound",SessionData.getLanguage()));
//                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
//               {
//                  out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
//                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
//                }
%>.  <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
              </td>
              
	  <tr>
	    <td align=left class="main" nowrap>
	    <form name="downloadform" method=post action="admin/reports/analysis/terminals_by_serialNumber_export.jsp" onSubmit="scroll2();">
		    <input type="hidden" name="page" value="<%=intPage%>">
		    <input type="hidden" name="section_page" value="<%=section_page%>">
		    <input type="hidden" name="download" value="y">
		    <input type="hidden" id="serialNumber" name="serialNumber" value="<%=Terminal.getSerialNumber()%>">
		    <input type="hidden" id="serialNumberFormat" name="serialNumberFormat" value="<%=Terminal.getSerialNumberFormat()%>"> 
		    <input type=submit name=submit value="Download Report">
	    </form>
	    </td>
	  </tr>
	  <tr>
	    <td align=right class="main" nowrap>
	    <form name="submitPageForm" id="submitPageForm" method="post" action="admin/reports/analysis/terminals_by_serialNumber_summary.jsp">
	    	<input type="hidden" name="search" value="<%=URLEncoder.encode(request.getParameter("search"), "UTF-8")%>" />
		    <input type="hidden" id="serialNumber" name="serialNumber" value="<%=Terminal.getSerialNumber()%>">
		    <input type="hidden" id="serialNumberFormat" name="serialNumberFormat" value="<%=Terminal.getSerialNumberFormat()%>"> 
	    	<input type="hidden" name="page" value="1" />	    	
		    <%
		    if (intPage > 1)
		    {
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage(1);return false;\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+ (intPage-1) +");return false;\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
		    }
		    int intLowerLimit = intPage - 12;
		    int intUpperLimit = intPage + 12;
		
		    if (intLowerLimit<1)
		    {
		      intLowerLimit=1;
		      intUpperLimit = 25;
		    }
		
		    for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
		    {
		      if (i==intPage)
		      {
		        out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
		      }
		      else
		      {
		        out.println("<a href=\"javascript:;\" onClick=\"submitPage("+ i +");return false;\">" + i + "</a>&nbsp;");
		      }
		    }
		
		    if (intPage <= (intPageCount-1))
		    {
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+(intPage+1)+");return false;\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+(intPageCount)+");return false;\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
		    }		
		    %>
	    </form>
	    </td>
	  </tr>              
  
         <tr>
         <td class="main">
         <%=Languages.getString("jsp.admin.iso_name",SessionData.getLanguage())%>:&nbsp;<%=SessionData.getUser().getCompanyName()%>
         </td>
         </tr>
              <tr>
              <td class=main align=right valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
          </table>
          <table>
              <tr>
                  <td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
              </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>#</td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.analisys.terminal_search.repName",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.analisys.terminal_search.DBA",SessionData.getLanguage()).toUpperCase()%></td>                                 
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.analisys.terminal_search.MerchantID",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.analisys.terminal_search.SiteID",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.analisys.terminal_search.TerminalType",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.analisys.terminal_search.TerminalStatus",SessionData.getLanguage()).toUpperCase()%></td>
              </tr>
            </thead>
<%

			
            while(it.hasNext())
            {
				Vector vecTemp = null;
				vecTemp = (Vector)it.next();
				
				String strMerchantID = (String)vecTemp.get(2);
				String strRepId = (String)vecTemp.get(7);
				String strSiteId = (String)vecTemp.get(8);
				String strLinkTerminal = "admin/customers/terminal.jsp?merchantId="+strMerchantID+"&repId="+strRepId+"&siteId="+strSiteId;
				out.println("<tr class=row" + intEvenOdd +">");
				out.println("	<td>" + intCounter++ + "</td>");
              	out.println("	<td>" + vecTemp.get(0) + "</td>");
              	out.println("	<td>" + vecTemp.get(1) + "</td>");
              	out.println("	<td>" + vecTemp.get(2) + "</td>");
              	out.println("	<td><a href=\""+strLinkTerminal+"\" target=\"_blank\">"+strSiteId+"</a></td>");
              	out.println("	<td>" + vecTemp.get(4) + "</td>");
              	out.println("	<td>" + vecTemp.get(5) + "</td>");
              	out.println("</tr>");
                if (intEvenOdd == 1)
                	intEvenOdd = 2;
                else
                	intEvenOdd = 1;
                           	
            }
%>
            <tfoot>
              <tr class=row<%= intEvenOdd %>>

              </tr>
            </tfoot>
          </table>
<%
        }
        else
        if (intRecordCount == 0 && request.getParameter("search") != null && searchErrors == null)
        {        
			String strFormatedSerialNumber = Terminal.transformSerialNumber(SessionData);
			if(!"".equals(strFormatedSerialNumber)
			&& Terminal.existsTerminalsBySerialNumbers(SessionData, strFormatedSerialNumber))
				out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.reports.analisys.error.serialnumber_exists",SessionData.getLanguage()) + "</font><br><br>"); 
        	else          
        		out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
        	//System.out.println("Order: " + sortString);
%>
          <SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "String","String","Number","Number","String","String"],0,false,false);
                    
  -->
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
