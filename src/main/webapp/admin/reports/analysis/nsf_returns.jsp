<%@ page import="java.util.*, java.text.DecimalFormat,com.debisys.reports.NsfReport,com.debisys.customers.Merchant" %>
<%
	int section=4;
int section_page=10;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="NsfReport" class="com.debisys.reports.NsfReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%!
String stripLeadingAndTrailingQuotes(String str){
	if (str.startsWith("\"")){
		str = str.substring(1, str.length());
	}
	if (str.endsWith("\"")){
		str = str.substring(0, str.length() - 1);
	}
	return str;
}
%>
<%@ include file="/includes/header.jsp" %>
<%
String nsfhistory = request.getParameter("nsfhistory");
%>
<script src="includes/tableSort.js" type="text/javascript"></script>
<link href="includes/tableSort.css" type="text/css" rel="StyleSheet" />
<script type="text/javascript">
	$(function(){
		var TableSorter1 = new TSorter;
		if (document.getElementById('t1') != null){
				TableSorter1.init('t1');
		}
	});
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.nsf.returns",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3" class="formArea">
<%
if(nsfhistory != null && nsfhistory.length() > 0){
%>
			<div class="formArea">
				<table width="300" cellspacing="1" cellpadding="1" border="0" align="center" class="sortable" id="t1">
<%
	int nrow = 0;
	Vector<Vector<String>> returns = NsfReport.getReturnsByMerchant(Long.valueOf(nsfhistory)); 
	for(Vector<String> vectTemp: returns){
		if(nrow == 0){
					%><thead><tr class="<%="row"+(nrow % 2 + 1) %>"><%
			for(String val: vectTemp){
				val = stripLeadingAndTrailingQuotes(val);
						%><th nowrap="nowrap" style="text-align:center;" <%=val.equals("#")?" abbr=\"number\"":"" %>><%=val %></th><%
			}
					%></tr></thead><%="\n" %><%
		}else{
			%><tr class="<%="row"+(nrow % 2 + 1) %>"><%
			int i = 0;
			String mid = "";
			for(String val: vectTemp){
				if(i == 2){
					mid = val;
				}
				if(i == 11 && (Integer.valueOf(val) > 0)){
					%><td class="nsfbutton" align="center" merchantid="<%=mid%>"><%=val%>&nbsp;</td><%
				}else{
					%><td nowrap="nowrap" <%=((val.matches("^-?[0-9]+(.[0-9]+)?%?$")?"align=\"right\"":""))%>><%=val%>&nbsp;</td><%
				}
				i++;
			}
					%></tr><%="\n" %><%
		}
		nrow++;
	}
%>
				</table>
			</div>
<%
}
%>
		</td>
	</tr>	
</table>
<%@ include file="/includes/footer.jsp" %>
