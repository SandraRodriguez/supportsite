<%-- 
    Document   : imtu_providers_by_product
    Created on : Apr 22, 2015, 2:55:34 PM
    Author     : nmartinez
--%>

<%@page import="com.debisys.utils.NumberUtil"%>
<%@page import="com.debisys.reports.pojo.ImtuMerchantPojo"%>
<%@page import="com.debisys.reports.pojo.ImtuIsoPojo"%>
<%@page import="com.debisys.reports.pojo.ImtuPojo"%>
<%@page import="com.debisys.reports.IMTUReport"%>
<%@page import="com.debisys.reports.pojo.WorkMasterPojo"%>
<%@page import="com.debisys.web.servlet.SecurityLogin"%>
<%@page import="com.debisys.reports.pojo.CrossborderSkuPojo"%>
<%@page import="com.debisys.reports.pojo.ProviderPojo"%>
<%@page import="com.debisys.utils.Countries"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=62;
String path = request.getContextPath(); 
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>

<style type="text/css" title="currentStyle">
    @import "<%=path%>/includes/media/demo_page.css";
    @import "<%=path%>/includes/media/demo_table.css";
</style>
    
<%
    String idWork = request.getParameter("idWork");
    String type   = request.getParameter("type");
    
    String urlFormat = path+"/admin/reports/analysis/intranet/imtu_result_report.jsp?idWork=%s&type=%s";
    String url = "";
    
    if ( type.equals("1"))
    {        
        url = String.format(urlFormat, idWork, 2);
    }
    else if ( type.equals("2") )
    {        
        url = String.format(urlFormat, idWork, 3);        
    }
    
    String strRefId = SessionData.getProperty("ref_id");
    String sessionLanguage = SessionData.getUser().getLanguageCode(); 
    if ( sessionLanguage.equals("0")){
        sessionLanguage="1";
    }
    String reportCode = ResourceReport.RESOURCE_IMTU_FEATURE;
    
    
    ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport( reportCode, sessionLanguage, null);
    String titleReportLabel = "title";
    String productIdLabel   = "productId"; 
    String productDescLabel   = "productDesc"; 
    String selectAllLabel = "selectAll";
    
    String yesLabel = "yesLabel";
    String noLabel  = "noLabel";
    String subTitleReport = "";
    
    HashMap parametersName = new HashMap();
    parametersName.put(titleReportLabel, reportCode); 
    parametersName.put(productIdLabel, "Product Id"); 
    parametersName.put(productDescLabel, "Product Descripion");
    parametersName.put(selectAllLabel, "Select All");
    parametersName.put(yesLabel, "Yes");
    parametersName.put(noLabel, "No");
    
    for( ResourceReport resource : resources)
    {
        if ( parametersName.containsKey( resource.getKeyMessage() ) )
        {
            parametersName.put( resource.getKeyMessage(), resource.getMessage());
        }
    }      
%>

<%
    String labelProcessing      = Languages.getString("jsp.tools.datatable.processing",SessionData.getLanguage());
    String labelShowRecords     = Languages.getString("jsp.tools.datatable.showRecords",SessionData.getLanguage());
    String labelNoRecords       = Languages.getString("jsp.tools.datatable.norecords",SessionData.getLanguage());
    String labelNoDataAvailable = Languages.getString("jsp.tools.datatable.noDataAvailable",SessionData.getLanguage());
    String labelInfo            = Languages.getString("jsp.tools.datatable.info",SessionData.getLanguage());
    String labelInfoEmpty       = Languages.getString("jsp.tools.datatable.infoEmpty",SessionData.getLanguage());
    String labelFilter          = Languages.getString("jsp.tools.datatable.filter",SessionData.getLanguage());
    String labelSearch          = Languages.getString("jsp.tools.datatable.search",SessionData.getLanguage());    
    String labelLoading         = Languages.getString("jsp.tools.datatable.loading",SessionData.getLanguage());
    String labelFirst           = Languages.getString("jsp.tools.datatable.first",SessionData.getLanguage());
    String labelLast            = Languages.getString("jsp.tools.datatable.last",SessionData.getLanguage());
    String labelNext            = Languages.getString("jsp.tools.datatable.next",SessionData.getLanguage());
    String labelPrevious        = Languages.getString("jsp.tools.datatable.previous",SessionData.getLanguage());
    String labelShow            = Languages.getString("jsp.tools.datatable.show",SessionData.getLanguage());
    String labelRecords         = Languages.getString("jsp.tools.datatable.records",SessionData.getLanguage());
%>
            
<script type="text/javascript" charset="utf-8">
    function setTableProducts(table)
    {
        $('#'+table).dataTable( {
                "iDisplayLength": 10,
                "bLengthChange": true,
                "bFilter": true,
                "sPaginationType": "full_numbers",

                "oLanguage": {
                   "sProcessing":      "<%=labelProcessing%>",
                            "sLengthMenu":     "<%=labelShowRecords%>",
                            "sZeroRecords":    "<%=labelNoRecords%>",
                            "sEmptyTable":     "<%=labelNoDataAvailable%>",
                            "sInfo":           "<%=labelInfo%>",
                            "sInfoEmpty":      "<%=labelInfoEmpty%>",
                            "sInfoFiltered":   "<%=labelFilter%>",
                            "sInfoPostFix":    "",
                            "sSearch":         "<%=labelSearch%>",
                            "sUrl":            "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "<%=labelLoading%>",
                            "oPaginate": {
                                "sFirst":    "<%=labelFirst%>",
                                "sLast":     "<%=labelLast%>",
                                "sNext":     "<%=labelNext%>",
                                "sPrevious": "<%=labelPrevious%>"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            },
                            "sLengthMenu": ' <%=labelShow%> <select id="pagesNumbers" >'+
                                                        '<option value="10">10</option>'+
                                                        '<option value="20">20</option>'+
                                                        '<option value="30">30</option>'+
                                                        '<option value="40">40</option>'+
                                                        '<option value="50">50</option>'+
                                                        '<option value="-1">All</option>'+
                                                        '</select> <%=labelRecords%>'
                 },
             "aaSorting": [[ 0, "asc" ]]     
        }				
        );
    }
</script> 
                     
<link rel="stylesheet" type="text/css" href="css/maps.css" title="style">

    

<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=parametersName.get(titleReportLabel).toString().toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">            
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="formArea2">                                        
                        <table width="1000px">                                                                                 
                            <tr>
                                <td id="tdSubTitle"></td>
                            <tr>    
                            <tr style="width: 50%">                                
                                <td class="main" align="left" >
                                    <%
                                    if ( type.equals("1") )
                                    {
                                        ArrayList<ImtuPojo> imtuPojos = IMTUReport.findIMTUReportByID(idWork);
                                    %>  
                                        <table cellpadding="0"  cellspacing="0" border="0" class="display" id="imtuReportTable" name="imtuReportTable">
                                            <thead>
                                                <tr class="rowhead2">
                                                    <th>Provider Id</th>
                                                    <th>Provider Name</th>
                                                    <th>Product Description</th>
                                                    <th>Product Id</th>
                                                    <th>Buy Rate</th>
                                                    <th>Conversion</th>
                                                    <th>Enabled</th>
                                                    <th>SKU category</th>
                                                    <th>ISO's Assigned Product</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             <%			 
                                             for(ImtuPojo imtu : imtuPojos )
                                             {   
                                                String productId  = imtu.getProduct().getId();
                                                String providerId = imtu.getId();
                                                String prodDesc   = HTMLEncoder.encode(imtu.getProduct().getDescripton());
                                                String provName   = HTMLEncoder.encode(imtu.getDescripton());
                                                String parameters = "&productId="+productId+"&prodDesc="+URLEncoder.encode(prodDesc, "UTF-8")+"&provName="+URLEncoder.encode(provName, "UTF-8")+"&providerId="+providerId;
                                                
                                                String linkToViewReport="<a style='text-decoration: underline' href='"+url+parameters+"'>"+imtu.getCountIsosProducts()+"</a>";
                                                String disabledLbl="";
                                                if ( imtu.isDisabled() )
                                                {
                                                    disabledLbl = parametersName.get(noLabel).toString();
                                                }
                                                else
                                                {
                                                   disabledLbl = parametersName.get(yesLabel).toString();
                                                }   
                                                String conversionMark="";
                                                if (imtu.isConversion())
                                                    conversionMark="X";
                                             %>
                                             <tr>                                             
                                                <td><%=providerId%></td>
                                                <td><%=provName%></td>                                                                                
                                                <td><%=prodDesc%></td>                                                                                
                                                <td><%=productId%></td>  
                                                <td><%=NumberUtil.formatAmount(imtu.getBuyRate())%></td>  
                                                <td><%=conversionMark%></td>  
                                                <td><%=disabledLbl%></td>  
                                                <td><%=imtu.getSkuCategory()%></td>
                                                <td><%=linkToViewReport%></td>                                           
                                             </tr>
                                             <%				 				 		
                                             }
                                             %>
                                            </tbody>                                        
                                        </table>
                                        
                                    <%
                                    }
                                    else if ( type.equals("2") )
                                    {
                                        String productId = request.getParameter("productId");
                                        String prodDesc  = request.getParameter("prodDesc");
                                        String provName  = request.getParameter("provName");
                                        String providerId= request.getParameter("providerId");
                                        subTitleReport   = provName+ "("+providerId+") <br/> "+ prodDesc+ " ("+productId+")";
                                        
                                        ArrayList<ImtuIsoPojo> imtuIsoPojos = IMTUReport.findIMTUReportByIDAndProduct(idWork, productId);
                                    %>
                                    
                                    <table cellpadding="0" width="100%"  cellspacing="0" border="0" class="display" id="imtuReportTable" name="imtuReportTable">
                                            <thead>
                                                <tr class="rowhead2">
                                                    <th>ISO Id</th>
                                                    <th style="width:400px">ISO Name</th>                                                                                                       
                                                    <th># Rateplan with Product</th>
                                                    <th># Terminals with Product</th> 
                                                    <th># Active Terminals with Product</th>                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                             <%	
                                             
                                             for(ImtuIsoPojo imtuIso : imtuIsoPojos )
                                             {                                                        
                                                String isoId   = imtuIso.getId();
                                                String isoDesc = imtuIso.getDescripton();
                                                String parameters = "&productId="+productId+"&prodDesc="+URLEncoder.encode(prodDesc, "UTF-8")+"&provName="+URLEncoder.encode(provName, "UTF-8")+"&providerId="+providerId+"&isoId="+isoId+"&isoDesc="+URLEncoder.encode(isoDesc, "UTF-8");
                                                String linkToViewReport="<a style='text-decoration: underline' href='" + url + parameters + "'>"+imtuIso.getNumTerminals()+"</a>";
                                                
                                             %>
                                             <tr>                                             
                                                <td><%=isoId%></td>
                                                <td><%=isoDesc%></td>                                                                                               
                                                <td><%=imtuIso.getNumRatePlans()%></td>  
                                                <td><%=linkToViewReport%></td>  
                                                <td><%=imtuIso.getNumActiveTerminals()%></td>                                                  
                                             </tr>
                                             <%				 				 		
                                             }
                                             %>
                                            </tbody>                                        
                                        </table>
                                    
                                    <%
                                    }
                                    else if ( type.equals("3") )
                                    {
                                        String productId = request.getParameter("productId");                                        
                                        String prodDesc  = request.getParameter("prodDesc");
                                        String provName  = request.getParameter("provName");
                                        String providerId= request.getParameter("providerId");
                                        String isoId     = request.getParameter("isoId");
                                        String isoDesc   = request.getParameter("isoDesc");
                                        subTitleReport = provName+ "("+providerId+") <br/> "+ prodDesc+ " ("+productId+") <br/> "+isoDesc+" ("+isoId+")";
                                        
                                        ArrayList<ImtuMerchantPojo> imtuMerchantsPojos = IMTUReport.findIMTUReportByIDAndProductAndIsoId(idWork, productId, isoId);
                                        
                                    %>
                                    
                                        <table cellpadding="0"  cellspacing="0" border="0" class="display" id="imtuReportTable" name="imtuReportTable">
                                            <thead>
                                                <tr class="rowhead2">
                                                    
                                                    <th>Merchant Id</th>
                                                    <th>Merchant DBA</th>
                                                    <th>Number of Terminals</th>                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                             <%			 
                                             for(ImtuMerchantPojo imtuMerchant : imtuMerchantsPojos)
                                             {                                                    
                                             %>
                                             <tr> 
                                                <td><%=imtuMerchant.getId()%></td>
                                                <td><%=imtuMerchant.getDescripton()%></td>
                                                <td><%=imtuMerchant.getNumberOfTerminals()%></td>                                                                   
                                             </tr>
                                             <%				 				 		
                                             }
                                             %>
                                            </tbody>                                        
                                        </table>
                                            
                                    <%
                                    }
                                    %>
                                    
                                    <script LANGUAGE="JavaScript">
                                    $(document).ready(function() {
                                        setTableProducts("imtuReportTable"); 
                                        $("#tdSubTitle").html("<%=subTitleReport%>");
                                    });
                                    </script>
                                    
                                </td>
                            </tr>                                                    
                        </table>
                    </td>
                </tr>
            </table>                                                
         </td>
    </tr>    
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>

