<%-- 
    Document   : work
    Created on : Apr 23, 2015, 10:15:59 AM
    Author     : nmartinez
--%>
<%@page import="java.util.HashMap"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.pojo.WorkJob"%>
<%@page import="com.debisys.reports.IMTUReport"%>
<%@page import="com.debisys.reports.pojo.WorkMasterPojo"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<%
    String path = request.getContextPath(); 
    String userIdUnique =    SessionData.getUser().getIdNew();
    String sessionLanguage = SessionData.getUser().getLanguageCode();
    String reportCode = request.getParameter("reportCode");
    String typeAction = request.getParameter("type");
    String title   = request.getParameter("title");
    String country = request.getParameter("country");
    String categories = request.getParameter("categories");
    
    String messages="";
    Integer numWorksActive = new Integer(2);
    int workingJobs = 0;
    
    ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport( reportCode, sessionLanguage, null);
    String titleReportLabel = "title";
    String productIdLabel   = "productId"; 
    String productDescLabel   = "productDesc"; 
    String selectAllLabel = "selectAll";
    String exceedScheduleJobs = "exceedScheduleJobs";
    
    HashMap parametersName = new HashMap();
    parametersName.put(titleReportLabel, reportCode); 
    parametersName.put(productIdLabel, "Product Id"); 
    parametersName.put(productDescLabel, "Product Descripion");
    parametersName.put(selectAllLabel, "Select All");
    parametersName.put(exceedScheduleJobs, "You have exceed the number of jobs. Max("+numWorksActive+")");
    
    for( ResourceReport resource : resources)
    {
        if ( parametersName.containsKey( resource.getKeyMessage() ) )
        {
            parametersName.put( resource.getKeyMessage(), resource.getMessage());
        }
    }  
    
    WorkJob workJobParameters = new WorkJob();
    workJobParameters.setCategoryJob("SS-REPORTS");
    workJobParameters.setCountryId(country);
    workJobParameters.setReportCode(reportCode);
    workJobParameters.setTitle(title);
    workJobParameters.setUserId(userIdUnique);
    workJobParameters.setSkuCategories(categories);
    
    if ( typeAction.equals("saveByProvider") || typeAction.equals("saveByProduct") )
    {        
        if ( typeAction.equals("saveByProvider") ){
            String providers = request.getParameter("providers");
            workJobParameters.setProviders(providers);
        }
        else{
            String products = request.getParameter("products");
            workJobParameters.setProducts(products);
        }        
        ArrayList<PropertiesByFeature> propertiesFeature = PropertiesByFeature.findPropertiesByFeature(reportCode);
       
        for(PropertiesByFeature propertyFeatureRecord : propertiesFeature){
            if (propertyFeatureRecord.getProperty().equals("numWorksActive"))
            {
                try{
                    numWorksActive = Integer.parseInt(propertyFeatureRecord.getValue());                   
                }
                catch(Exception e)
                {
                    numWorksActive = 1;
                }
            }
        }
        ArrayList<WorkMasterPojo> currentWorks = IMTUReport.getWorksByUserId(userIdUnique, reportCode);
        
        for( WorkMasterPojo scheduleWork : currentWorks)
        {
            if ( scheduleWork.getAtEndDate() == null || scheduleWork.getEndDate() == null )
            {
                workingJobs++;
            }
        }
        if ( workingJobs < numWorksActive )
        {
            IMTUReport.createNewWork(workJobParameters);
        } 
        else
        {
            messages = parametersName.get(exceedScheduleJobs).toString();
        }        
    }    
    else if ( typeAction.equals("delete") ){
        String idWork = request.getParameter("idWork");
        IMTUReport.deleteByWorkId(idWork);
    }
    else if ( typeAction.equals("find") ){
        //do nothing
    }
    ArrayList<WorkMasterPojo> works = IMTUReport.getWorksByUserId(userIdUnique, reportCode);
%>


<%
    if ( works != null && works.size()>0 )
    {
%>
        <table width="100%"> 
            <tr class="rowhead2">
                <td>Id</td>    
                <td>Title</td>
                <td>Creation Date</td>
                <td>End date finding RatePlans/Terminals</td>
                <td>End date finding Active Transactions terminals</td>
                <td colspan="2"></td>                                            
            </tr> 
            <%
            String waitingControl = "<img src='images/global-wait.gif'>";
            for( WorkMasterPojo workPojo : works )
            {
                String idWork = workPojo.getId();
                String creationDate = workPojo.getCreationDate();
                //String wParameters  = workPojo.getWorkParameters();
                String endDate = workPojo.getEndDate();
                String atEndDate = workPojo.getAtEndDate();
                
                String linkToViewReport = "";
                if ( endDate != null && atEndDate!=null )
                {
                    //type  = 1 means the first grouping for this report
                    //idWork this is the unique id of the [WorkMaster] table when the user schedule this job
                    linkToViewReport="<a style='text-decoration: underline'  href='"+path+"/admin/reports/analysis/intranet/imtu_result_report.jsp?idWork="+ idWork +"&type=1'>View Report</a>";
                }
                if ( endDate == null )
                {
                    endDate = waitingControl;                    
                }
                if ( atEndDate == null )
                {
                    atEndDate = waitingControl;                    
                }
                %>
            <tr id="tr_<%=idWork%>" >
                <td><%=idWork%></td>
                <td><%=workPojo.getTitleJob()%></td>
                <td><%=creationDate%></td>
                <td><%=endDate%></td>
                <td><%=atEndDate%></td>
                <td><%=linkToViewReport%></td>
                
                <td align="right"><input type='button' onclick="doWorkByAction('delete','<%=idWork%>');" value='Delete' ></input></td>
            </tr> 
            <%
            }
            %>
        </table>
        <table>
            <tr>
            <td><%=messages%></td>
            </tr>
        </table>
            <%
    }
    else{
    %>
        
    <%    
    }
    %>
    
