<%-- 
    Document   : imtu_providers_by_product
    Created on : Apr 22, 2015, 2:55:34 PM
    Author     : nmartinez
--%>

<%@page import="com.debisys.reports.pojo.BasicPojo"%>
<%@page import="com.debisys.reports.IMTUReport"%>
<%@page import="com.debisys.reports.pojo.WorkMasterPojo"%>
<%@page import="com.debisys.web.servlet.SecurityLogin"%>
<%@page import="com.debisys.reports.pojo.CrossborderSkuPojo"%>
<%@page import="com.debisys.reports.pojo.ProviderPojo"%>
<%@page import="com.debisys.utils.Countries"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=62;
String path = request.getContextPath(); 
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>

<style type="text/css" title="currentStyle">
    @import "<%=path%>/includes/media/demo_page.css";
    @import "<%=path%>/includes/media/demo_table.css";
</style>
    
<%
    String strRefId = SessionData.getProperty("ref_id");
    String sessionLanguage = SessionData.getUser().getLanguageCode(); 
    String getJobsOnLoadPage = "doWorkByAction('find',0);";
    if ( sessionLanguage.equals("0")){
        sessionLanguage="1";
    }
    String reportCode = ResourceReport.RESOURCE_IMTU_FEATURE;
    
    
    ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport( reportCode, sessionLanguage, null);
    String titleReportLabel = "title";
    String productIdLabel   = "productId"; 
    String productDescLabel   = "productDesc"; 
    String selectAllLabel = "selectAll";
    
    HashMap parametersName = new HashMap();
    parametersName.put(titleReportLabel, reportCode); 
    parametersName.put(productIdLabel, "Product Id"); 
    parametersName.put(productDescLabel, "Product Descripion");
    parametersName.put(selectAllLabel, "Select All");
    
    for( ResourceReport resource : resources)
    {
        if ( parametersName.containsKey( resource.getKeyMessage() ) )
        {
            parametersName.put( resource.getKeyMessage(), resource.getMessage());
        }
    }  
    
    
    ArrayList<Countries> countries = IMTUReport.getCountries();
    StringBuilder comboCountries = new StringBuilder();
    comboCountries.append("<option value=-1 selected ></option>");
    for( Countries country : countries)
    {
        comboCountries.append("<option value="+country.getCountryId()+">"+ country.getCountryName() +" ("+country.getInternationalCode()+") </option>");
    }

    StringBuilder skuCategory = new StringBuilder();
    skuCategory.append("<option value=-1 selected ></option>");
    ArrayList<BasicPojo> skusPojos = IMTUReport.getSkuCategories();
    for(BasicPojo category : skusPojos)
    {
        skuCategory.append("<option value='"+category.getId()+"'>"+category.getDescripton()+"</option>");
    }
        
    ArrayList<ProviderPojo> providers = IMTUReport.getProviders();
    StringBuilder comboProviders = new StringBuilder();
    comboProviders.append("<option value=-1 selected ></option>");
    for( ProviderPojo provider : providers)
    {
        comboProviders.append("<option value=" + provider.getId() + ">" + provider.getDescripton() + "</option>");
    }
    
    ArrayList<CrossborderSkuPojo> crossBorderSkus = CrossborderSkuPojo.getCrossborderSkuPojo();
    
    String labelProcessing      = Languages.getString("jsp.tools.datatable.processing",SessionData.getLanguage());
    String labelShowRecords     = Languages.getString("jsp.tools.datatable.showRecords",SessionData.getLanguage());
    String labelNoRecords       = Languages.getString("jsp.tools.datatable.norecords",SessionData.getLanguage());
    String labelNoDataAvailable = Languages.getString("jsp.tools.datatable.noDataAvailable",SessionData.getLanguage());
    String labelInfo            = Languages.getString("jsp.tools.datatable.info",SessionData.getLanguage());
    String labelInfoEmpty       = Languages.getString("jsp.tools.datatable.infoEmpty",SessionData.getLanguage());
    String labelFilter          = Languages.getString("jsp.tools.datatable.filter",SessionData.getLanguage());
    String labelSearch          = Languages.getString("jsp.tools.datatable.search",SessionData.getLanguage());
    labelSearch = "Filter here by Product ID or descripion:";
    String labelLoading         = Languages.getString("jsp.tools.datatable.loading",SessionData.getLanguage());
    String labelFirst           = Languages.getString("jsp.tools.datatable.first",SessionData.getLanguage());
    String labelLast            = Languages.getString("jsp.tools.datatable.last",SessionData.getLanguage());
    String labelNext            = Languages.getString("jsp.tools.datatable.next",SessionData.getLanguage());
    String labelPrevious        = Languages.getString("jsp.tools.datatable.previous",SessionData.getLanguage());
    String labelShow            = Languages.getString("jsp.tools.datatable.show",SessionData.getLanguage());
    String labelRecords         = Languages.getString("jsp.tools.datatable.records",SessionData.getLanguage());
%>

<script LANGUAGE="JavaScript">

function calculateByPeriod()
{
    var period = $("#period").val();   
    var unitNumberHidden = $("#unitNumberHidden").val();
    
    if ( period == "day" )
    {
        $("#unitNumber").val( unitNumberHidden); 
    }
    else if ( period == "week" )
    {
        var remainder = unitNumberHidden % 7;
        var dd = (unitNumberHidden - remainder) / 7;
        $("#unitNumber").val( (dd) ); 
    }
    else if ( period == "month" )
    {
        var remainder = unitNumberHidden % 30;
        var dd = (unitNumberHidden - remainder) / 30;
        $("#unitNumber").val( dd ); 
    }
}

var url = "/support/admin/reports/analysis/intranet/work.jsp";

var specialKeys = new Array();
specialKeys.push(8); //Backspace
$(function () {
    $(".title").bind("keyup", function (e) {
        var ret=false;
        var string = $(this).val(); //$this'test- _ 0Test';
        if (string.match(/^[-_ a-zA-Z0-9]+$/)){
            ret = true;
        }
        else
        {
            $(this).val("");
        }
        return ret;
    });
    $(".title").bind("keyup", function (e) {
               
    });
    $(".title").bind("paste", function (e) {
        return false;
    });
    $(".title").bind("drop", function (e) {
        return false;
    });

    $(".searchType").bind("click", function (e) {
        //alert($(this).val());
        if ( $(this).val() == "byprovider" )
        {            
            $("#td_byprovider").show();
            $("#td_byproduct").hide();
        }
        else
        { //byproduct
            $("#td_byprovider").hide();
            $("#td_byproduct").show();
        }
    });
    
});
    

function doWorkByAction(action,idWork)
{   
    
    if ( action === "save" )
    {
        var countryValue  = $("#country option:selected").val();
        if ( countryValue === "-1")
        {
            alert("Country is required!");
            $("#country").focus();
            return;
        }
        var titleValue    = $("#titleJob").val();
        if ( titleValue.length === 0 )
        {     
            alert("The title of job is required!");
            $("#titleJob").focus();
            return;
        }
        
        var categoryValue = $("#category option:selected").val();
        var searchType    = $('input:radio[name=searchType]:checked').val();
        if ( categoryValue ==="-1" )
        {
            categoryValue="";
        }
        if ( searchType === "byprovider" )
        {
            var provider  = $("#provider option:selected").val();             
            saveByProvider(provider, titleValue, countryValue, categoryValue);
        }
        else
        {
            var products = loopTableProducts();
            saveByProduct(products, titleValue, countryValue, categoryValue);
        }
    }
    else
    {
        $.post( 
            url, 
            {             
                type: action,            
                rnd : Math.random(),  
                reportCode : "<%=ResourceReport.RESOURCE_IMTU_FEATURE%>",
                idWork: idWork
            },
            function( data ) 
            {
                var dataTrim = data;
                $("#tdWorkInformation").html(dataTrim);            
            }        
        ); 
        
    }    
}

function saveByProvider(providers, title, country, categories)
{ 
    $.post( 
        url, 
        {             
            type: "saveByProvider",            
            rnd : Math.random(),  
            reportCode : "<%=ResourceReport.RESOURCE_IMTU_FEATURE%>",
            title: title,
            providers: providers,
            country: country,
            categories: categories
        },
        function( data ) 
        {
            var dataTrim = data;
            $("#tdWorkInformation").html(dataTrim);            
        }        
    );            

}

function saveByProduct(products, title, country, categories)
{

    $.post( 
        url, 
        {             
            type: "saveByProduct",            
            rnd : Math.random(),  
            reportCode : "<%=ResourceReport.RESOURCE_IMTU_FEATURE%>",
            title: title,
            products: products,
            country: country,
            categories: categories
        },
        function( data ) 
        {
            var dataTrim = data;
            $("#tdWorkInformation").html(dataTrim);            
        }        
    );
}

$(document).ready(function() {
    setTableProducts("crossborderProductsTable"); 
    setInterval("doWorkByAction('find',0);",9000);
    $('input:radio[name=searchType][value=byprovider]').click();
    <%=getJobsOnLoadPage%>
});

</script>
            
<script type="text/javascript" charset="utf-8">
    function setTableProducts(table)
    {
        $('#'+table).dataTable( {
                "iDisplayLength": 10,
                "bLengthChange": true,
                "bFilter": true,
                "sPaginationType": "full_numbers",

                "oLanguage": {
                   "sProcessing":      "<%=labelProcessing%>",
                            "sLengthMenu":     "<%=labelShowRecords%>",
                            "sZeroRecords":    "<%=labelNoRecords%>",
                            "sEmptyTable":     "<%=labelNoDataAvailable%>",
                            "sInfo":           "<%=labelInfo%>",
                            "sInfoEmpty":      "<%=labelInfoEmpty%>",
                            "sInfoFiltered":   "<%=labelFilter%>",
                            "sInfoPostFix":    "",
                            "sSearch":         "<%=labelSearch%>",
                            "sUrl":            "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "<%=labelLoading%>",
                            "oPaginate": {
                                "sFirst":    "<%=labelFirst%>",
                                "sLast":     "<%=labelLast%>",
                                "sNext":     "<%=labelNext%>",
                                "sPrevious": "<%=labelPrevious%>"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            },
                            "sLengthMenu": ' <%=labelShow%> <select id="pagesNumbers" >'+
                                                        '<option value="10">10</option>'+
                                                        '<option value="20">20</option>'+
                                                        '<option value="30">30</option>'+
                                                        '<option value="40">40</option>'+
                                                        '<option value="50">50</option>'+
                                                        '<option value="-1">All</option>'+
                                                        '</select> <%=labelRecords%>'
                 },
             "aaSorting": [[ 0, "asc" ]]     
        }				
        );
    }
    function unselectAll()
    {
        var actionSelect = false;
        if ($('#unselect').is(':checked'))
        {
            actionSelect = true            
        }
        else
        {
            actionSelect = false;            
        }	
        $('#pagesNumbers').val("-1");
        $('#pagesNumbers').change();
        $('#merchantsTable tbody input[type=checkbox]').attr('checked',actionSelect);
        $('#pagesNumbers').val("10");
        $('#pagesNumbers').change();					
    }
    
    function loopTableProducts()
    {   
        var oTable = $('#crossborderProductsTable').dataTable();
        var data_length = $('#pagesNumbers').val();
        var nNodes = oTable.fnGetNodes( );
        var pages = nNodes.length / data_length;
        var idSelected = "";
        oTable.fnPageChange( 0 );        
        var selectedSku = "";
        for ( var j = 0; j < pages; j++ ) 
        {
            $('#crossborderProductsTable tbody tr td input[type="checkbox"]').each(function()
            {
                if ($(this).is(':checked'))
                {
                    var nameControl = $(this).attr( 'id' );
                    //merchantSelected = merchantSelected + "-"+nameControl;
                    var strs = nameControl.split("_");	//chkProduct_	                
                    var sku = strs[1]; 
                    if ( selectedSku == "" )
                        selectedSku = sku;
                    else
                        selectedSku = selectedSku + "," + sku;
                }
            });
            oTable.fnPageChange( 'next' ); 
        }
        oTable.fnPageChange( 0 );
        return selectedSku;
    } 
</script> 
                     
<link rel="stylesheet" type="text/css" href="css/maps.css" title="style">

    

<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=parametersName.get(titleReportLabel).toString().toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">            
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="formArea2">                                        
                        <table width="1000px">                                                                                    
                            <tr>
                                <td colspan="2">
                                    <table>
                                       <tr style="vertical-align: top">
                                            <td>
                                                International Country Code:
                                                <select id="country" name="country" >                                                         
                                                    <%=comboCountries.toString()%>
                                                </select>                                             
                                            </td> 
                                            <td>
                                                Sku Category:
                                                <select id="category" name="category" >                                                         
                                                    <%=skuCategory%>
                                                </select>                                             
                                            </td>                                             
                                        </tr>                                       
                                    </table>                                  
                                </td> 
                            </tr>
                            
                            <tr>
                                <td>Search Type</td> 
                                <td></td>                              
                            </tr>
                            
                            <tr>
                                <td>
                                    By Provider: <input type="radio" id="searchType" name="searchType" value="byprovider" class="searchType" />                                            
                                </td> 
                                <td id="td_byprovider">
                                    Providers:
                                    <select id="provider" name="provider" >                                                         
                                        <%=comboProviders%>
                                    </select>                                             
                                </td>                              
                            </tr>
                            
                            <tr style="width: 50%"> 
                                <td>
                                    By Product: <input type="radio" id="searchType" name="searchType" value="byproduct" class="searchType" />                                            
                                </td>
                                <td id="td_byproduct" class="main" align="left" colspan="2" >
                                        <table cellpadding="0"  cellspacing="0" border="0" class="display" id="crossborderProductsTable" name="crossborderProductsTable">
                                            <thead>
                                                <tr class="rowhead2">
                                                    <th><%=parametersName.get(productIdLabel).toString() %></th>
                                                    <th><%=parametersName.get(productDescLabel).toString() %></th>
                                                    <th><span id="spanSelect"><%=parametersName.get(selectAllLabel).toString()%></span>
                                                        <input type="checkbox" id="unselect" name="unselect" onclick="unselectAll();">
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             <%			 
                                             for(CrossborderSkuPojo crossborderPojo : crossBorderSkus )
                                             {   
                                                 String idPojo = crossborderPojo.getId();
                                             %>
                                             <tr>      
                                                <td><%=crossborderPojo.getId()%></td>
                                                <td><%=crossborderPojo.getDescripton()%></td>                                                                                
                                                <td><input type="checkbox" id="chkProduct_<%=idPojo%>" /></td>                                                
                                             </tr>
                                             <%				 				 		
                                             }
                                             %>
                                            </tbody>                                        
                                        </table>
                                    </td>
                            </tr>
                            <tr>
                                <td colspan="2" class=main align="center" >
                                  Title of job:  
                                  <input type="text" name="titleJob" id="titleJob" class="title" >
                                  <input type="button"  name="id" name="save" value="Save work" onclick="doWorkByAction('save',0);">
                                </td>                                
                            </tr>
                            <tr>
                            <td  colspan="2" class=main align="center" id="tdWorkInformation" ></td>
                            </tr>                            
                        </table>
                    </td>
                </tr>
            </table>                                                
         </td>
    </tr>
    <tr align="center">
        <td colspan="3" align="center">
            
        </td>
    </tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>

