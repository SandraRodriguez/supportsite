<%@ page import="java.util.*, java.text.DecimalFormat,com.debisys.reports.NsfReport,com.debisys.customers.Merchant" %>
<%
int section=4;
int section_page=10;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="NsfReport" class="com.debisys.reports.NsfReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%!
String stripLeadingAndTrailingQuotes(String str){
	if (str.startsWith("\"")){
		str = str.substring(1, str.length());
	}
	if (str.endsWith("\"")){
		str = str.substring(0, str.length() - 1);
	}
	return str;
}

String checkSelected(String merchid, String[] mids){
	String selected = "";
	if(mids != null){
		for(int j=0;j<mids.length;j++){
			if(mids[j].equals(merchid)){
				selected = "selected";
				break;
			}	
		}
	}
	return selected;
}

long [] getMerchList(SessionData sessionData, boolean print, JspWriter out, String[] mids){
	long []merchList = null;
	try{
		Vector<Vector<String>> vecMerchantList = Merchant.getMerchantListReports(sessionData);
		if(vecMerchantList.size() > 0){
			Iterator<Vector<String>> it = vecMerchantList.iterator();
			merchList = new long [vecMerchantList.size()];
			int i = 0;
			if(print){
				out.println("<option value=\"ALL\" "+ (mids == null?"selected":checkSelected("ALL", mids)) +">"+Languages.getString("jsp.admin.reports.all",sessionData.getLanguage())+"</option>");
			}
			while (it.hasNext()){
				Vector<String> vecTemp = null;
				vecTemp = it.next();
				if(print){
					try{
						out.println("<option value=" + vecTemp.get(0) +" " + checkSelected(vecTemp.get(0), mids) + ">" + vecTemp.get(1) + "</option>");
					}catch(Exception ioe){}
				}
				if(!vecTemp.get(0).equals("") && vecTemp.get(0) != null){
					try{
						merchList[i] = Long.parseLong(vecTemp.get(0));
						i++;
					}catch(NumberFormatException nfe) {}
				}
			}
		}
	}catch(Exception ioe){}
	return merchList;
}%>
<%
String downReport = request.getParameter("download");
String showReport = request.getParameter("showReport");
long []allMerchIds = null;
String[] mids= request.getParameterValues("mids");
if(downReport != null){
	allMerchIds = getMerchList(SessionData, false, null, mids);
	if(mids.length > 0){
		long []merchIds = new long[mids.length];
		for(int i=0;i<mids.length;i++){
			if(mids[i].equals("ALL")){
				merchIds = allMerchIds;
				break;
			}else{
				try {
					merchIds[i] = Long.parseLong(mids[i]);
				}catch (NumberFormatException nfe) {}
			}
		}
		String strPath = NsfReport.downloadNFSReportCVS(getServletContext(), merchIds);
		if (strPath.length() > 0){
			response.sendRedirect(strPath);
		}
	}
}else{
%>
<%@ include file="/includes/header.jsp" %>
<script language="JavaScript">
	var count = 0
	var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

	function scroll(){
		document.mainform.submit.disabled = true;
		document.mainform.submit.value = iProcessMsg[count];
		count++
		if (count = iProcessMsg.length) count = 0
		setTimeout('scroll()', 150);
	}

	function scroll2(){
		document.downloadform.submit.disabled = true;
		document.downloadform.submit.value = iProcessMsg[count];
		count++
		if (count = iProcessMsg.length) count = 0
		setTimeout('scroll2()', 150);
	}
</script>
<style>
	.nsfbutton{background-color:silver;font-weight:bold;}
</style>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.nsf.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3" class="formArea2">
			<form class="reportForm" name="mainform" method="get" action="admin/reports/analysis/nsf.jsp" onsubmit="return validateForm();">
				<table>
					<tr>
						<td class="main" valign="top" nowrap="nowrap"><%=Languages.getString("jsp.admin.reports.payments.index.select_merchants",SessionData.getLanguage())%></td>
						<td class="main" valign="top">
							<select name="mids" size="10" multiple="multiple">
								<% allMerchIds = getMerchList(SessionData,  true, out, mids); %>
							</select>
							<br>
							*<%=Languages.getString("jsp.admin.reports.payments.index.instructions",SessionData.getLanguage())%>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<input type="submit" name="showReport" value="<%=Languages.getString("jsp.admin.reports.nsf.showbutton",SessionData.getLanguage())%>">
							<input type="submit" name="download" value="<%=Languages.getString("jsp.admin.reports.churn.download",SessionData.getLanguage())%>">
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
<%
	if(showReport != null){
	if(mids.length > 0){
		long []merchIds = new long[mids.length];
		for(int i=0;i<mids.length;i++){
	if(mids[i].equals("ALL")){
		merchIds = allMerchIds;
		break;
	}else{
		try {
			merchIds[i] = Long.parseLong(mids[i]);
		}catch (NumberFormatException nfe) {}
	}
		}
		Vector<Vector<String>> merchs = NsfReport.getNsfReport(merchIds);
		if(merchs.size() <= 1){
%>
	<div class="formArea2"><big style="color:red;"><%=Languages.getString("jsp.admin.reports.error6",SessionData.getLanguage()) %></big></div>
<%
			}else{
	%>
<script src="includes/tableSort.js" type="text/javascript"></script>
<link href="includes/tableSort.css" type="text/css" rel="StyleSheet" />
<script type="text/javascript">
	$(function(){
		var TableSorter1 = new TSorter;
		if (document.getElementById('t1') != null){
				TableSorter1.init('t1');
		}
	});
</script>
<div class="formArea2">
	<table width="300" cellspacing="1" cellpadding="1" border="0" align="center" class="sortable" id="t1">
<%
		int nrow = 0;
		for(Vector<String> vectTemp: merchs){
			if(nrow == 0){
				%><thead><tr class="<%="row"+(nrow % 2 + 1) %>"><%
				for(String val: vectTemp){
					val = stripLeadingAndTrailingQuotes(val);
					%><th nowrap="nowrap" style="text-align:center;" <%=val.equals("#")?" abbr=\"number\"":"" %>><%=val %></th><%
				}
				%></tr></thead><%="\n" %><%
			}else{
				%><tr class="<%="row"+(nrow % 2 + 1) %>"><%
				int i = 0;
				String mid = "";
				for(String val: vectTemp){
					if(i == 2){
						mid = val;
					}
					if(i == 12 && (Integer.valueOf(val) > 0)){
						%><td class="nsfbutton" align="center"><a href="admin/reports/analysis/nsf_returns.jsp?nsfhistory=<%=mid%>"><%=val%></a>&nbsp;</td><%
					}else{
						%><td nowrap="nowrap" <%=((val.matches("^-?[0-9]+(.[0-9]+)?%?$")?"align=\"right\"":""))%>><%=val%>&nbsp;</td><%
					}
					i++;
				}
				%></tr><%="\n" %><%
			}
			nrow++;
		}
%>
	</table>
</div>
<%
			}
		}
	}
%>
<%@ include file="/includes/footer.jsp" %>
<%
}
%>
