<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.terminals.Terminal,
                 java.util.*,
                 com.debisys.reports.TransactionReport" %>
<%
int section= 4;
int section_page=41; 
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}

</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.analisys.terminal_search.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>	
      <table border="0" width="100%" cellpadding="0" cellspacing="0">      
     	<tr>
	        <td class="formArea2">
	        <form name="mainform" method="post" action="admin/reports/analysis/terminals_by_serialNumber_summary.jsp" onSubmit="scroll();">
	          <table width="300">
              <tr class="main">
	               <td nowrap valign="top">
	               <%=Languages.getString("jsp.admin.reports.analisys.terminal_search.summary",SessionData.getLanguage())%>
	               </td>
	               <td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
					<table width=400>
					<tr>
					    <td class=main nowrap><%=Languages.getString("jsp.admin.reports.analisys.terminal_search.SerialNumberFormat",SessionData.getLanguage())%>:</td>
					    <td class=main>
					        <select name="serialNumberFormat">        	
					<%
					  Vector vecFormatsList = com.debisys.terminals.Terminal.getSerialNumberFormats();
					  Iterator it = vecFormatsList.iterator();
					  while (it.hasNext())
					  {
					    Vector vecTemp = null;
					    vecTemp = (Vector) it.next();
					    out.println("<option value=\"" + vecTemp.get(0) +"\">" + vecTemp.get(1) + "</option>");
					  }
					
					%>
					        </select>
					</td>
					</tr>
					
					<tr>
					    <td class=main nowrap><%=Languages.getString("jsp.admin.reports.analisys.terminal_search.SerialNumber",SessionData.getLanguage())%>:</td>
					    <td class=main>
					    	<input type="text" name="serialNumber" value="" size="30">
						</td>
					</tr>
					
					<tr>
					    <td class=main colspan=2 align=center>
					      <input type="hidden" name="search" value="y">
					      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
					    </td>
					</tr>
					<tr>
					<td class="main" colspan=2>

					</td>
					</tr>
					</table>
               </td>
              </tr>              
            </table>
			</form>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>