<%@ page language="java" import="java.util.*,com.debisys.utils.*" pageEncoding="ISO-8859-1"%>
<SCRIPT LANGUAGE="JavaScript">
function validateSchedule(shedule)
{  
  if ( shedule == "1" )
  	$('#sheduleReport').val("y");
  else	
  	$('#sheduleReport').val("");
  return false;	  
}

</SCRIPT>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	String permmisionTrue = request.getParameter("permissionEnableScheduleReports");
	String language =  request.getParameter("language");
	boolean isInternational = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
									DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

    String alignTD = "center";    
    String align = request.getParameter("align");
    if ( align != null)
    	alignTD = align;    
    
	if ( permmisionTrue!= null && permmisionTrue.equals("true") && isInternational )  
	{	
	%>
	    <td align="<%=alignTD%>">
	    	<input type="submit" value="<%=com.debisys.languages.Languages.getString("jsp.admin.reports.scheduleReport.programmingReport", language )%>" onclick="validateSchedule(1);">
	    	<input type="hidden" id="sheduleReport" name="sheduleReport" value="">
	    </td>
	<%}%> 