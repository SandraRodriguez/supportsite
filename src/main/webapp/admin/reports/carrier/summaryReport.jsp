<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*,
                 com.debisys.reports.pojo.*,
                 com.debisys.utils.ColumnReport,
                 com.debisys.utils.NumberUtil, 
                 com.debisys.schedulereports.ScheduleReport" %>
<%
int section=4;
int section_page=11;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CarrierReport" class="com.debisys.reports.CarrierReport" scope="request"/>
<jsp:setProperty name="CarrierReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
 Vector vecCurrent = new Vector();
 Vector vecPast = new Vector();
 Hashtable searchErrors = null;
 SummaryReport carrierSummary = new SummaryReport();
  ArrayList<String> titles = new ArrayList<String>();
 String titleReport = "jsp.admin.reports.title26";
 String sc = request.getParameter("sheduleReport");
  
 if (request.getParameter("search") != null)
 {
	  if (CarrierReport.validateDateRange(SessionData))
	  {  
	     SessionData.setProperty("start_date", request.getParameter("startDate"));
         SessionData.setProperty("end_date", request.getParameter("endDate"));	     
	     titles.add( Languages.getString( titleReport ,SessionData.getLanguage()).toUpperCase() );
	     if ( sc != null && sc.equals("y") )
		 {
		    //TO SCHEDULE REPORT		    
			CarrierReport.getCarrierSummaryReport(SessionData, application, DebisysConstants.SCHEDULE_REPORT, titles);
			ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
			if (  scheduleReport != null  )
			{				
				scheduleReport.setStartDateFixedQuery( CarrierReport.getStartDate() );
				scheduleReport.setEndDateFixedQuery( CarrierReport.getEndDate() );
				scheduleReport.setTitleName( titleReport );   
			}	
			response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );			
		 }
		 else if ( request.getParameter("downloadReport") != null ) 
	     {
		    CarrierReport.getCarrierSummaryReport(SessionData, application, DebisysConstants.DOWNLOAD_REPORT, titles);
		    response.sendRedirect( CarrierReport.getStrUrlLocation() );	   
		 }
		 else 
	     {
		    carrierSummary = CarrierReport.getCarrierSummaryReport(SessionData, application, DebisysConstants.EXECUTE_REPORT, titles);  
		 }
	  }
	  else
	  {
	   searchErrors = CarrierReport.getErrors();
	  }
	  
 }
 
%>
<%@ include file="/includes/header.jsp" %>

<table border="0" cellpadding="0" cellspacing="0" width="500" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString( titleReport ,SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
<table width=400>
<%
if (searchErrors != null)
{
	out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
	Enumeration enum1=searchErrors.keys();
	while(enum1.hasMoreElements())
	{
	  String strKey = enum1.nextElement().toString();
	  String strError = (String) searchErrors.get(strKey);
	  out.println("<li>" + strError);
	}
	
	  out.println("</font></td></tr>");
}
%>
</tr>
</table>
               </td>
              </tr>
            </table>
<%
if ( carrierSummary.getRecords().size() > 0  )
{
%>
		<form name="mainform" id="mainform" method="post" action="admin/reports/carrier/summaryReport.jsp" onSubmit="scroll();">
		
		 	<input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
			<input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
			<input type="hidden" name="search" value="y">
			<input type="hidden" name="downloadReport" value="y">
			<input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
			              
            <table width="100%" cellspacing="1" cellpadding="2">
                        	 
            <% 
            int rowClass = 1;
            for( SummaryReportPojo carrierPojo : carrierSummary.getRecords())
            {
                String description = carrierPojo.getDescription();
                String current     = carrierPojo.getCurrentPeriod();
                String previous    = carrierPojo.getPreviuosPeriod();
                String change    = carrierPojo.getChange();
            	if ( carrierPojo.getRecordType().equals("T"))
            	{
            	%>
            	 <tr>
              		<td class=rowhead2 colspan="4" valign=bottom><%=description%></td>
              	 </tr>            
            	<%
                }
                else if ( carrierPojo.getRecordType().equals("R"))
                {
                %>
            	 <tr class=row<%=rowClass%> >
              		<td valign=bottom><%=description%></td>
              		<td valign=bottom><%=current%></td>
              		<td valign=bottom><%=previous%></td>
              		<td valign=bottom><%=change%></td>
              	 </tr>            
            	<%
            		if ( rowClass == 1 )
            		  rowClass = 2;
            		else  
            		  rowClass = 1;
                }  
                else if ( carrierPojo.getRecordType().equals("C"))
                {
                %>
            	 <tr>
              		<td class=rowhead2 valign=bottom><%=description%></td>
              		<td class=rowhead2 valign=bottom><%=current%></td>
              		<td class=rowhead2 valign=bottom><%=previous%></td>
              		<td class=rowhead2 valign=bottom><%=change%></td>
              	 </tr>            
            	<%            		
                }
            }
            %>        
            </table>
            </form>  
<%
}
else if (vecCurrent.size()==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>
