<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*" %>
<%
int section=4;
int section_page=11;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CarrierReport" class="com.debisys.reports.CarrierReport" scope="request"/>
<jsp:setProperty name="CarrierReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecCurrent = new Vector();
Vector vecPast = new Vector();
Hashtable searchErrors = null;

if (request.getParameter("search") != null)
{
  if (CarrierReport.validateDateRange(SessionData))
  {
    //pull last months
    CarrierReport.setStartDate(DateUtil.addSubtractMonths(request.getParameter("startDate"),-1));
    CarrierReport.setEndDate(DateUtil.addSubtractMonths(request.getParameter("endDate"),-1));
    vecPast = CarrierReport.getCarrierSummary(SessionData);

    CarrierReport.setStartDate(request.getParameter("startDate"));
    CarrierReport.setEndDate(request.getParameter("endDate"));
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    vecCurrent = CarrierReport.getCarrierSummary(SessionData);
  }
  else
  {
   searchErrors = CarrierReport.getErrors();
  }


}
%>
<%@ include file="/includes/header.jsp" %>

<table border="0" cellpadding="0" cellspacing="0" width="500" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title26",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
</tr>
</table>
               </td>
              </tr>
            </table>
<%
if (vecCurrent != null && vecCurrent.size() > 0)
{
%>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
              <td colspan=4 class=rowhead2><%=Languages.getString("jsp.admin.reports.title26",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <tr>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.description",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.current_period",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.previous_period",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.percentage_change",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <%
            double totalTranAmountCurr = Double.parseDouble(vecCurrent.get(2).toString());
            double numOfTransCurr = Double.parseDouble(vecCurrent.get(3).toString());
            double liveTerminalCountCurr = Double.parseDouble(vecCurrent.get(5).toString());
            double activeLocationCountCurr = Double.parseDouble(vecCurrent.get(6).toString());
            double avgPerTranCurr = Double.parseDouble(vecCurrent.get(7).toString());
            double transPerTerminalDayCurr = Double.parseDouble(vecCurrent.get(8).toString());
            double avgPerDayCurr = Double.parseDouble(vecCurrent.get(9).toString());
            double avgPerDayTerminalCurr = Double.parseDouble(vecCurrent.get(10).toString());
            double avgPerMonthTerminalCurr = Double.parseDouble(vecCurrent.get(11).toString());
            double avgRevenuePerUserCurr = Double.parseDouble(vecCurrent.get(13).toString());
            double avgRechargesPerSubscriberCurr = Double.parseDouble(vecCurrent.get(14).toString());
            double totalSubscribersCurr = Double.parseDouble(vecCurrent.get(15).toString());
            double errorCountCurr = Double.parseDouble(vecCurrent.get(16).toString());
            double errorRateCurr = Double.parseDouble(vecCurrent.get(17).toString());
            
            double totalTranAmountPrev = Double.parseDouble(vecPast.get(2).toString());
            double numOfTransPrev = Double.parseDouble(vecPast.get(3).toString());
            double liveTerminalCountPrev = Double.parseDouble(vecPast.get(5).toString());
            double activeLocationCountPrev = Double.parseDouble(vecPast.get(6).toString());
            double avgPerTranPrev = Double.parseDouble(vecPast.get(7).toString());
            double transPerTerminalDayPrev = Double.parseDouble(vecPast.get(8).toString());
            double avgPerDayPrev = Double.parseDouble(vecPast.get(9).toString());
            double avgPerDayTerminalPrev = Double.parseDouble(vecPast.get(10).toString());
            double avgPerMonthTerminalPrev = Double.parseDouble(vecPast.get(11).toString());
            double avgRevenuePerUserPrev = Double.parseDouble(vecPast.get(13).toString());
            double avgRechargesPerSubscriberPrev = Double.parseDouble(vecPast.get(14).toString());
            double totalSubscribersPrev = Double.parseDouble(vecPast.get(15).toString());
            double errorCountPrev = Double.parseDouble(vecPast.get(16).toString());
            double errorRatePrev = Double.parseDouble(vecPast.get(17).toString());
            
            String totalTranAmountChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String numOfTransChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String liveTerminalCountChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String activeLocationCountChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String avgPerTranChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String transPerTerminalDayChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String avgPerDayChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String avgPerDayTerminalChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String avgPerMonthTerminalChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String avgRevenuePerUserChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String avgRechargesPerSubscriberChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String totalSubscribersChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String errorCountChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            String errorRateChange = Languages.getString("jsp.admin.reports.not_available",SessionData.getLanguage());
            
            if (totalTranAmountPrev != 0)
            {
            	totalTranAmountChange = NumberUtil.formatAmount(Double.toString((totalTranAmountCurr - totalTranAmountPrev) / totalTranAmountPrev * 100)) + "%";
            }
            if (numOfTransPrev != 0)
            {
            	numOfTransChange = NumberUtil.formatAmount(Double.toString((numOfTransCurr - numOfTransPrev) / numOfTransPrev * 100)) + "%";
            }
            if (liveTerminalCountPrev != 0)
            {
            	liveTerminalCountChange = NumberUtil.formatAmount(Double.toString((liveTerminalCountCurr - liveTerminalCountPrev) / liveTerminalCountPrev * 100)) + "%";
            }
            if (activeLocationCountPrev != 0)
            {
            	activeLocationCountChange = NumberUtil.formatAmount(Double.toString((activeLocationCountCurr - activeLocationCountPrev) / activeLocationCountPrev * 100)) + "%";
            }
            if (avgPerTranPrev != 0)
            {
            	avgPerTranChange = NumberUtil.formatAmount(Double.toString((avgPerTranCurr - avgPerTranPrev) / avgPerTranPrev * 100)) + "%";
            }
            if (transPerTerminalDayPrev != 0)
            {
            	transPerTerminalDayChange = NumberUtil.formatAmount(Double.toString((transPerTerminalDayCurr - transPerTerminalDayPrev) / transPerTerminalDayPrev * 100)) + "%";
            }
            if (avgPerDayPrev != 0)
            {
            	avgPerDayChange = NumberUtil.formatAmount(Double.toString((avgPerDayCurr - avgPerDayPrev) / avgPerDayPrev * 100)) + "%";
            }
            if (avgPerDayTerminalPrev != 0)
            {
            	avgPerDayTerminalChange = NumberUtil.formatAmount(Double.toString((avgPerDayTerminalCurr - avgPerDayTerminalPrev) / avgPerDayTerminalPrev * 100)) + "%";
            }
            if (avgPerMonthTerminalPrev != 0)
            {
            	avgPerMonthTerminalChange = NumberUtil.formatAmount(Double.toString((avgPerMonthTerminalCurr - avgPerMonthTerminalPrev) / avgPerMonthTerminalPrev * 100)) + "%";
            }
            if (avgRevenuePerUserPrev != 0)
            {
            	avgRevenuePerUserChange = NumberUtil.formatAmount(Double.toString((avgRevenuePerUserCurr - avgRevenuePerUserPrev) / avgRevenuePerUserPrev * 100)) + "%";
            }
            if (avgRechargesPerSubscriberPrev != 0)
            {
            	avgRechargesPerSubscriberChange = NumberUtil.formatAmount(Double.toString((avgRechargesPerSubscriberCurr - avgRechargesPerSubscriberPrev) / avgRechargesPerSubscriberPrev * 100)) + "%";
            }
            if (totalSubscribersPrev != 0)
            {
            	totalSubscribersChange = NumberUtil.formatAmount(Double.toString((totalSubscribersCurr - totalSubscribersPrev) / totalSubscribersPrev * 100)) + "%";
            }
            if (errorCountPrev != 0)
            {
            	errorCountChange = NumberUtil.formatAmount(Double.toString((errorCountCurr - errorCountPrev) / errorCountPrev * 100)) + "%";
            }
            if (errorRatePrev != 0)
            {
            	errorRateChange = NumberUtil.formatAmount(Double.toString((errorRateCurr - errorRatePrev) / errorRatePrev * 100)) + "%";
            }
            

      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.number_of_days_month",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(0)+ "</td><td>" + vecPast.get(0) + "</td><td></td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.date_range",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(1)+ "</td><td>" + vecPast.get(1) + "</td><td></td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.number_of_days",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(4)+ "</td><td>" + vecPast.get(4) + "</td><td></td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.total_trans_amount",SessionData.getLanguage())+"</td><td>" + NumberUtil.formatCurrency(vecCurrent.get(2).toString())+ "</td><td>" + NumberUtil.formatCurrency(vecPast.get(2).toString()) + "</td><td align=right>" + totalTranAmountChange + "</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.number_of_trans",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(3)+ "</td><td>" + vecPast.get(3) + "</td><td align=right>" + numOfTransChange + "</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.live_term_count",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(5)+ "</td><td>" + vecPast.get(5) + "</td><td align=right>" + liveTerminalCountChange + "</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.active_location_count",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(6)+ "</td><td>" + vecPast.get(6) + "</td><td align=right>" + activeLocationCountChange + "</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.number_of_errors",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(16) + "</td><td>" + vecPast.get(16) + "</td><td align=right>" + errorCountChange + "</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.error_rate",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(17) + "%</td><td>" + vecPast.get(17) + "%</td><td align=right>" + errorRateChange + "</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.avg_per_trans",SessionData.getLanguage())+"</td><td>" + NumberUtil.formatCurrency(vecCurrent.get(7).toString()) + "</td><td>" + NumberUtil.formatCurrency(vecPast.get(7).toString()) + "</td><td align=right>" + avgPerTranChange + "</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.trans_per_term_day",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(8)+ "</td><td>" + vecPast.get(8) + "</td><td align=right>" + transPerTerminalDayChange + "</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.avg_per_day",SessionData.getLanguage())+"</td><td>" + NumberUtil.formatCurrency(vecCurrent.get(9).toString()) + "</td><td>" + NumberUtil.formatCurrency(vecPast.get(9).toString()) + "</td><td align=right>" + avgPerDayChange + "</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.avg_amount_per_day_term",SessionData.getLanguage())+"</td><td>" + NumberUtil.formatCurrency(vecCurrent.get(10).toString()) + "</td><td>" + NumberUtil.formatCurrency(vecPast.get(10).toString()) + "</td><td align=right>" + avgPerDayTerminalChange + "</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.avg_amount_per_month_term",SessionData.getLanguage())+"</td><td>" + NumberUtil.formatCurrency(vecCurrent.get(11).toString()) + "</td><td>" + NumberUtil.formatCurrency(vecPast.get(11).toString()) + "</td><td align=right>" + avgPerMonthTerminalChange + "</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.projected_amt",SessionData.getLanguage())+"</td><td>" + NumberUtil.formatCurrency(vecCurrent.get(12).toString()) + "</td><td>&nbsp;</td><td></td></tr>");
      
      out.println("<tr><td colspan=4 class=rowhead2>"+Languages.getString("jsp.admin.reports.carrier.summary.recharge_report",SessionData.getLanguage())+"</td></tr>");
      out.println("<tr><td class=rowhead2 valign=bottom>" + Languages.getString("jsp.admin.reports.description",SessionData.getLanguage()) +"</td><td class=rowhead2 valign=bottom>"+Languages.getString("jsp.admin.reports.current_period",SessionData.getLanguage())+"</td><td class=rowhead2 valign=bottom>"+Languages.getString("jsp.admin.reports.previous_period",SessionData.getLanguage())+"</td><td class=rowhead2 valign=bottom>"+Languages.getString("jsp.admin.reports.percentage_change",SessionData.getLanguage())+"</td></tr>");

      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.arpu",SessionData.getLanguage())+"</td><td>" + NumberUtil.formatCurrency(vecCurrent.get(13).toString()) + "</td><td>" + NumberUtil.formatCurrency(vecPast.get(13).toString()) + "</td><td align=right>" + avgRevenuePerUserChange + "</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.arps",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(14)+ "</td><td>" + vecPast.get(14) + "</td><td align=right>" + avgRechargesPerSubscriberChange + "</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.carrier.summary.total",SessionData.getLanguage())+"</td><td>" + vecCurrent.get(15)+ "</td><td>" + vecPast.get(15) + "</td><td align=right>" + totalSubscribersChange + "</td></tr>");


      out.println("</table>");




      vecCurrent.clear();
      vecPast.clear();

}
else if (vecCurrent.size()==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>