<%@ page import="java.util.Formatter" %>
<%@ page import="java.util.Date" %>

<% if (SessionData.getUser().isQcommBusiness()) { %>
   <table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
   <tr>
       <td VALIGN="middle" WIDTH="376" height="28">
         <span style="font-weight: 700; padding-right:2px;" ><font face="Arial, Helvetica, sans-serif" style="font-size: 13px" color="#2C0973">
         <img border="0" src="images/QCommMiniLogo.png" width="63" height="25"> <%=Languages.getString("jsp.admin.reports.qcommreports",SessionData.getLanguage())%> </font></span></td>
     </tr>
     <tr>
     	<td>
     	  <font color="#2C0973" face="Arial, Helvetica, sans-serif" style="font-size: 13px">
         <A HREF="https://reports.qcomm.com/" target="_blank"><%=new Formatter().format(Languages.getString("jsp.admin.reports.qcommreports.instructions",SessionData.getLanguage()),SessionData.getUser().getQcommBusinessTransitionDate())%></A></font>
       </td>
     </tr>
    </table>
<%} %>