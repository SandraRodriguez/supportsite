<%@ page import="com.debisys.customers.Merchant" %>
<%@ page import="com.debisys.reports.SpiffReport" %>
<%@ page import="com.debisys.utils.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    int section=4;
    int section_page=14;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="spiffReport" class="com.debisys.reports.SpiffReport" scope="request"/>
<jsp:setProperty name="spiffReport" property="*"/>

<%@ include file="/includes/security.jsp" %>
<%
    //Some code to execute

    long intRecordCount = 0;
    long currentPage = 1;
    long intPageSize = 25;
    long intPageCount = 1;
    spiffReport.setPageSize(intPageSize);
    Collection<SpiffReport.SpiffInfo> searchResults = new ArrayList();
    Hashtable searchErrors = null;
    String[] merchantsId = null;
    String strCol = request.getParameter("col") != null ? request.getParameter("col") : "0";
    String strSort = request.getParameter("sort") != null ? request.getParameter("sort") : "1";

    String titleReport = Languages.getString("jsp.admin.reports.spiffReports.spiffDetailReport",SessionData.getLanguage()).toUpperCase();
    ArrayList<ColumnReport> reportHeaders = null;
    spiffReport.setCol(strCol);
    spiffReport.setSort(strSort);


    if (request.getParameter("search") != null) {
        if (spiffReport.validateDateRange(SessionData)){
            SessionData.setProperty("start_date", request.getParameter("startDate"));
            SessionData.setProperty("end_date", request.getParameter("endDate"));
            merchantsId = request.getParameterValues("mids");
            if (merchantsId != null) {
                spiffReport.setMerchantIds(merchantsId);
            }
            if (!spiffReport.getStartDate().equals("") && !spiffReport.getEndDate().equals("")) {
                titleReport += " " +  Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( spiffReport.getStartDateFormatted() );
                titleReport += " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+HTMLEncoder.encode( spiffReport.getEndDateFormatted() );
            }

            if (request.getParameter("downloadReport") != null && request.getParameter("downloadReport").equalsIgnoreCase("y")){

                //response.sendRedirect(downLoadUrl);
                return;
            }

            reportHeaders = spiffReport.getReportHeaders(SessionData);
            searchResults = spiffReport.getSpiffReportResult(SessionData);
            intRecordCount = spiffReport.getRecordCount();
            System.out.println("******** " + intRecordCount);
            currentPage = spiffReport.getPageIndex();
            if(intRecordCount > 0){
                intPageCount = intRecordCount / intPageSize;
                if(intPageCount * intPageSize < intRecordCount){
                    intPageCount++;
                }
            }

        } else {
            searchErrors = spiffReport.getErrors();
        }
    }else if(request.getParameter("downloadReport") != null){
        merchantsId = request.getParameterValues("mids");
        if (merchantsId != null) {
            spiffReport.setMerchantIds(merchantsId);
        }
        String csvReportUrl = spiffReport.generateCsvReport(application, SessionData);
        response.sendRedirect(csvReportUrl);
        return;
    }



%>


<%@ include file="/includes/header.jsp" %>



<table id="tb1" border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">

    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="formAreaTitle"  height="20" style="border-left: 1px solid #62891E; border-right: 1px solid #62891E;">
                        <%=Languages.getString("jsp.admin.reports.spiffReports.spiffDetailReport",SessionData.getLanguage()).toUpperCase()%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td bgcolor="#FFFFFF" style="height: 25px;"></td>
    </tr>
<%
    if (searchErrors != null) {
%>
    <tr>
        <td bgcolor="#FFFFFF">
            <table>
                <tr class=main>
                    <td align=left colspan=3>
                        <font color=ff0000><%=Languages.getString("jsp.admin.error1",SessionData.getLanguage())%><br/>
<%
        Enumeration enum1=searchErrors.keys();
        while(enum1.hasMoreElements()) {
            String strKey = enum1.nextElement().toString();
            String strError = (String) searchErrors.get(strKey);
            out.println("<li>" + strError);
        }
%>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
<%
    }
%>

<%
    if (searchResults != null && searchResults.size() > 0) {
%>


    <tr>
        <td bgcolor="#FFFFFF">
            <form name="downloadData" method=post action="/support/admin/reports/spiff/spiffDetailReportResult.jsp">
                <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
                <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
                <input type="hidden" name="sim" value="<%=request.getParameter("sim")%>">
                <input type="hidden" name="pnumber" value="<%=request.getParameter("pnumber")%>">
                <input type="hidden" name="col" value="<%=strCol%>">
                <input type="hidden" name="sort" value="<%=strSort%>">
                <%--<input type="hidden" name="search" value="y">--%>
                <input type="hidden" name="downloadReport" value="y">
                <%for(int i=0;i<merchantsId.length;i++){%>
                <input type="hidden" name="mids" value="<%=merchantsId[i]%>">
                <%}%>

                <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.downloadToCsv",SessionData.getLanguage())%>">
            </form>
        </td>
    </tr>

    <tr>
        <td bgcolor="#FFFFFF" style="height: 25px;"></td>
    </tr>

<%--    <tr>
        <td bgcolor="#FFFFFF" class="main">
            <%=titleReport%>
        </td>
    </tr>--%>

    <tr>
        <td bgcolor="#FFFFFF" class="main">
            <%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
            <%=" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( spiffReport.getStartDateFormatted())%>
            <%=" " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+HTMLEncoder.encode( spiffReport.getEndDateFormatted())%>
            <%=Languages.getString("jsp.admin.displaying", new Object[]{ Long.toString(currentPage),  Long.toString(intPageCount)}, SessionData.getLanguage())%>
        </td>
    </tr>

    <tr>
        <td align="right" bgcolor="#FFFFFF">
            <%
                if (currentPage > 1) {
                    out.println("<a href=\"admin/reports/spiff/spiffDetailReportResult.jsp?search=y" +  "&startDate=" + spiffReport.getStartDate() + "&endDate=" + spiffReport.getEndDate() + "&sim=" + spiffReport.getSim() + "&pnumber=" + spiffReport.getPnumber() + "&col=" + strCol + "&sort=" + strSort + "&mids=" + spiffReport.getMerchantIds() + "&pageIndex=1" + "\">" + Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                    out.println("<a href=\"admin/reports/spiff/spiffDetailReportResult.jsp?search=y" +  "&startDate=" + spiffReport.getStartDate() + "&endDate=" + spiffReport.getEndDate() + "&sim=" + spiffReport.getSim() + "&pnumber=" + spiffReport.getPnumber() + "&col=" + strCol + "&sort=" + strSort + "&mids=" + spiffReport.getMerchantIds() + "&pageIndex="  + (currentPage -1) +  "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
                }

                long lowerLimit = currentPage - 10;
                long upperLimit = currentPage + 10;

                if (lowerLimit<1) {
                    lowerLimit=1;
                    upperLimit = 20;
                }

                for(long i = lowerLimit; i <= upperLimit && i <= intPageCount; i++) {
                    if (i == currentPage) {
                        out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                    }else {
                        out.println("<a href=\"admin/reports/spiff/spiffDetailReportResult.jsp?search=y" +  "&startDate=" + spiffReport.getStartDate() + "&endDate=" + spiffReport.getEndDate() + "&sim=" + spiffReport.getSim() + "&pnumber=" + spiffReport.getPnumber() + "&col=" + strCol + "&sort=" + strSort + "&mids=" + spiffReport.getMerchantIds()+ "&pageIndex=" + i  + "\">" + i + "</a>&nbsp;");
                    }
                }

                if (currentPage <= (intPageCount-1)) {
                    out.println("<a href=\"admin/reports/spiff/spiffDetailReportResult.jsp?search=y" +  "&startDate=" + spiffReport.getStartDate() + "&endDate=" + spiffReport.getEndDate() + "&sim=" + spiffReport.getSim() + "&pnumber=" + spiffReport.getPnumber() + "&col=" + strCol + "&sort=" + strSort + "&mids=" + spiffReport.getMerchantIds() + "&pageIndex=" + (currentPage + 1) +  "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
                    out.println("<a href=\"admin/reports/spiff/spiffDetailReportResult.jsp?search=y" +  "&startDate=" + spiffReport.getStartDate() + "&endDate=" + spiffReport.getEndDate() + "&sim=" + spiffReport.getSim() + "&pnumber=" + spiffReport.getPnumber() + "&col=" + strCol + "&sort=" + strSort + "&mids=" + spiffReport.getMerchantIds() + "&pageIndex=" + (intPageCount) +  "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
                }

            %>



        </td>
    </tr>

    <tr>
        <td bgcolor="#FFFFFF">

            <table width="100%" cellspacing="1" cellpadding="2">
                <tr>
                    <%
                        String strSortURL = "admin/reports/spiff/spiffDetailReportResult.jsp?search=y&startDate=" + spiffReport.getStartDate() + "&endDate=" + spiffReport.getEndDate() + "&sim=" + spiffReport.getSim() + "&pnumber=" + spiffReport.getPnumber() + "&mids=" + spiffReport.getMerchantIds();
                    %>
                    <td class=rowhead2 valign=bottom>#</td>
                    <%
                        int col =  0;
                        for( ColumnReport header : reportHeaders ) {

                    %>
                    <td class=rowhead2 valign=bottom><%=header.getLanguageDescription().toUpperCase()%>&nbsp;
                        <a href="<%=strSortURL%>&col=<%=col%>&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
                        <a href="<%=strSortURL%>&col=<%=col%>&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>

                    <%
                            col++;

                        }
                    %>
                </tr>
                <%
                    System.out.println("generando resulatados...");
                    int intEvenOdd = 1;
                    int intCounter = 1;
                    System.out.println("SIZE=" +searchResults.size());
                    for (SpiffReport.SpiffInfo sInfo : searchResults) {

                        String redColorStyle ="";
                        if(sInfo.getMerchantCommissionAmount().doubleValue() < 0){
                            redColorStyle = "style=color:red;";
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.append("<tr class=\"row").append(intEvenOdd).append("\" ").append(redColorStyle).append(">");

                        sb.append("<td>").append(intCounter++).append("</td>");
                        sb.append("<td nowrap>").append(sInfo.getRecId()).append("</td>");
                        sb.append("<td nowrap>").append(sInfo.getActivationTrxIdStr()).append("</td>");
                        sb.append("<td nowrap>").append(sInfo.getMerchantId()).append("</td>");
                        sb.append("<td>").append(sInfo.getDba()).append("</td>");
                        sb.append("<td>").append(sInfo.getProductName()).append("</td>");
                        sb.append("<td>").append(sInfo.getSpiffType()).append("</td>");
                        sb.append("<td>").append(sInfo.getCommissionPaymentDateStr()).append("</td>");
                        sb.append("<td>").append(sInfo.getActivationDateStr()).append("</td>");
                        sb.append("<td>").append(sInfo.getActivationProduct()).append("</td>");
                        sb.append("<td>").append(sInfo.getPin()).append("</td>");
                        sb.append("<td>").append(sInfo.getEsn()).append("</td>");
                        sb.append("<td>").append(sInfo.getSim()).append("</td>");
                        sb.append("<td>").append(sInfo.getCommissionType()).append("</td>");
                        sb.append("<td>").append(NumberUtil.formatCurrency(sInfo.getMerchantCommissionAmount().toString())).append("</td>");

                        if (strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO)) {
                            sb.append("<td>").append(NumberUtil.formatCurrency(sInfo.getRepCommissionAmount().toString())).append("</td>");
                        }
                        if (strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT)
                                || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                            sb.append("<td>").append(NumberUtil.formatCurrency(sInfo.getSubAgentCommissionAmount().toString())).append("</td>");
                        }
                        if (strAccessLevel.equals(DebisysConstants.AGENT)
                                || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                            sb.append("<td>").append(NumberUtil.formatCurrency(sInfo.getAgentCommissionAmount().toString())).append("</td>");
                        }
                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                            sb.append("<td>").append(NumberUtil.formatCurrency(sInfo.getIsoCommissionAmount().toString())).append("</td>");
                        }

                        sb.append("<td>").append(sInfo.getPaymentType()).append("</td>");
                        sb.append("<td>").append(sInfo.getPaymentReferenceNumber()).append("</td>");

                        if (strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO)) {
                            sb.append("<td>").append(sInfo.getRepName()).append("</td>");
                        }
                        if (strAccessLevel.equals(DebisysConstants.AGENT)
                                || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                            sb.append("<td>").append(sInfo.getSubAgentName()).append("</td>");
                        }
                        if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                            sb.append("<td>").append(sInfo.getAgentName()).append("</td>");
                        }

                        sb.append("</tr>");
                        out.println(sb.toString());
                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        }else {
                            intEvenOdd = 1;
                        }
                    }


                %>
            </table>

        </td>
    </tr>

    <tr>
        <td bgcolor="#FFFFFF">
            <%
                }else if (searchResults.size()==0 && request.getParameter("search") != null && searchErrors == null) {
                    out.println("<br><br><font color=000000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
                }
            %>
        </td>
    </tr>

    <tr>
        <td bgcolor="#FFFFFF" style="height: 25px;"></td>
    </tr>

</table>


<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>