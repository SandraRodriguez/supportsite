<%@ page import="com.debisys.customers.Merchant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    int section=4;
    int section_page=14;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%@ include file="/includes/security.jsp" %>
<%
    //Some code to execute

%>


<%@ include file="/includes/header.jsp" %>

<form name="mainForm" id="mainForm" method="post" action="/support/admin/reports/spiff/activationDetailReportResult.jsp">
    <input type="hidden" name="search" value="y">
<table id="tb1" border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">

    <tr>
        <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="formAreaTitle" align="center" height="20" style="border-left: 1px solid #62891E; border-right: 1px solid #62891E;">
                        <%=Languages.getString("jsp.admin.reports.spiffReports.activationDetailReport",SessionData.getLanguage()).toUpperCase()%>
                    </td>
                </tr>
            </table>
        </td>

    </tr>

    <tr>
        <td colspan="2" bgcolor="#FFFFFF" style="height: 25px;"></td>
    </tr>

    <tr>
        <td colspan="2"  bgcolor="#FFFFFF"><%=Languages.getString("jsp.admin.reports.spiffReports.spiffDetailReport.selectDateRange",SessionData.getLanguage())%></td>
    </tr>

    <tr>
        <td nowrap bgcolor="#FFFFFF">
            <%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:
        </td>
        <td bgcolor="#FFFFFF">
            <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12">
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainForm.startDate,document.mainForm.endDate);return false;" HIDEFOCUS>
                <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
            </a>
        </td>
    </tr>

    <tr>
        <td nowrap bgcolor="#FFFFFF">
            <%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:
        </td>
        <td bgcolor="#FFFFFF" style="padding-bottom: 10px">
            <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12">
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainForm.startDate,document.mainForm.endDate);return false;" HIDEFOCUS>
                <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
            </a>
        </td>
    </tr>

    <tr>
        <td nowrap bgcolor="#FFFFFF">
            <%=Languages.getString("jsp.admin.reports.spiffReports.spiffDetailReport.sim",SessionData.getLanguage())%>:
        </td>
        <td bgcolor="#FFFFFF" style="padding-bottom: 10px">
            <input class="plain" name="sim" value="" size="12"><br/>
        </td>
    </tr>

    <tr>
        <td nowrap bgcolor="#FFFFFF">
            <%=Languages.getString("jsp.admin.reports.spiffReports.spiffDetailReport.paymentNumber",SessionData.getLanguage())%>:
        </td>
        <td bgcolor="#FFFFFF" style="padding-bottom: 10px">
            <input class="plain" name="pnumber" value="" size="12" >
        </td>
    </tr>

    <tr>
        <td nowrap bgcolor="#FFFFFF">
            <%=Languages.getString("jsp.admin.reports.spiffReports.spiffDetailReport.merchants.option",SessionData.getLanguage())%>:
        </td>
        <td class=main valign=top bgcolor="#FFFFFF" style="padding-bottom: 10px">
            <select name="mids" size="10" multiple>
                <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
                <%
                    Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
                    Iterator it = vecMerchantList.iterator();
                    while (it.hasNext())
                    {
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                    }

                %>
            </select>
            <br>
            *<%=Languages.getString("jsp.admin.reports.spiffReports.merchants.instructions",SessionData.getLanguage())%>
        </td>
    </tr>


    <tr>
        <td colspan="2"  bgcolor="#FFFFFF">
            <input id="btnSubmit" type="submit" class="main" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>"/>
        </td>
    </tr>
    <tr>
        <td colspan="2"  bgcolor="#FFFFFF" style="height: 25px">
            * <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
        </td>
    </tr>
</table>
</form>

<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>