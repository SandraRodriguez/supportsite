<%@ page import="java.net.URLEncoder,com.debisys.users.User,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.reports.ach.*, 
                 com.debisys.utils.DateUtil, 
                 java.text.SimpleDateFormat,
                 com.debisys.utils.NumberUtil" %>

<%
	int section=4;	
	int section_page=55;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
<jsp:useBean id="AchReports" class="com.debisys.reports.ach.AchReports" scope="request"/>
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
 ArrayList<FmrCommissionPaymentVo> arraySearchResults = new ArrayList<FmrCommissionPaymentVo>();
 String strIsoID = "";
 String strAgentID = "";
 String strSubAgentID = "";
 String strRepID = "";
 String strMerchantID = "";
 String strStartDate = "";
 String strEndDate = "";
 String strErrorDate = "";
 boolean bShowLabelLimitDays = false;
 //for response only
 
 int nRecordCount = 0;
 int nPage = 1;
 int nPageSize = 8;
 int nPageCount = 1;

 String strStatementDate = "";
 int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays("summaryFMRCommissionPayout", application);
 
 if (request.getParameter("page") != null)
 {
	try
	{
		nPage = Integer.parseInt(request.getParameter("page"));
		if (nPage < 1) { nPage = 1; }
	}
	catch (Exception ex)
	{
		nPage = 1;
	}
 }

 try
 {
    	if (request.getParameter("StartDate") != null)
    	{
    		strStartDate = request.getParameter("StartDate");
    	}
    	if (request.getParameter("EndDate") != null)
    	{
    		strEndDate = request.getParameter("EndDate");
    	}
    	strErrorDate = TransactionSearch.validateDateRange(strStartDate,strEndDate,SessionData);
    			
    	if(strErrorDate.equals(""))
    	{
	    	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	    	int daysBetween = DateUtil.getDaysBetween(strEndDate, strStartDate);
			if ( daysBetween > limitDays)
			{
			   strStartDate = DateUtil.addSubtractDays(strEndDate,-limitDays);
			   bShowLabelLimitDays = true;
			}
			Date date1 = format.parse(strStartDate);
			Date date2 = format.parse(strEndDate);
			format.applyPattern("yyyy-MM-dd");
			strStartDate = format.format(date1);
			strEndDate = format.format(date2);
    	    arraySearchResults = AchReports.FRMCommisionPayoutSummary(SessionData,strStartDate,strEndDate,nPageSize, nPage);
    	    nRecordCount = AchReports.recordCountLastQuery();
    	    if (nRecordCount > 0)
			{
				nPageCount = (nRecordCount / nPageSize);
				if ((nPageCount * nPageSize) < nRecordCount)
				{
					nPageCount++;
				}
			}			
    	}
 }
 catch (Exception e){}  
	    
 String masterTitle = Languages.getString("jsp.admin.reports.ach.title.fmr_commission_payout",SessionData.getLanguage())+" "+Languages.getString("jsp.admin.reports.ach.title.fmr_commission_payout_summary",SessionData.getLanguage());	    
%>
<SCRIPT LANGUAGE="JavaScript">
	
	function openViewDetail(detailDivId, batchId, entityId, statementId, paymentTypeId)
	{
	    var hiddenField = $("#hidden"+detailDivId).val();
	    		
		var sURL = "/support/admin/reports/ach/detailFMRCommissionPayout.jsp?Entity="+entityId+"&statementId="+statementId+"&batchId="+batchId+"&paymentTypeId="+paymentTypeId;
	  	var sOptions = "left=" + (screen.width - (screen.width * 0.2))/2 + ",top=" + (screen.height - (screen.height * 0.1))/2 + ",width=" + (screen.width * 0.5) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes,scrollbars=yes";
  		var w = window.open(sURL, "FMR"+detailDivId, sOptions, true);
  		w.focus();
  		  
  		/*  		
  		var htmlTest = "<table width='70%' cellspacing='1' cellpadding='1' border='0' >";
  		htmlTest = htmlTest + " <thead> <tr class='SectionTopBorder'><td class='rowhead2'>#</td>";
        htmlTest = htmlTest + " <td class='rowhead2'>TRANSACTION TYPE</td>";
        htmlTest = htmlTest + " <td class='rowhead2'>NO. TRANSACTIONS</td>";
        htmlTest = htmlTest + " <td class='rowhead2'>COMMISSION AMOUNT</td>";
        htmlTest = htmlTest + " </tr>";
        htmlTest = htmlTest + "</thead>";
		htmlTest = htmlTest + " <tr class=row1>";
		htmlTest = htmlTest + "	<td>1</td>";
		htmlTest = htmlTest + "	<td>Long Distance</td>";
		htmlTest = htmlTest + "	<td>2</td>";
	    htmlTest = htmlTest + "	<td align='right' >($3.30)</td>";
		htmlTest = htmlTest + "</tr>"; 		
		htmlTest = htmlTest + "</table>";
  		  	
  		if (hiddenField=="0")
	    {
	    	$("#hidden"+detailDivId).val("1");
	    	$("#divDetail"+detailDivId).html(htmlTest);
	    }
	    else
	    {
	    	$("#hidden"+detailDivId).val("0");
	    	$("#divDetail"+detailDivId).html("");
	    }
	    */  		
  		
  		
	}
</SCRIPT>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

            <TABLE cellSpacing=0 cellPadding=0 width=750 border=0 id="tb1">
              <TBODY>
              <TR>
                <TD align=left width="1%"  background=images/top_blue.gif><IMG height=18 src="images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle    background=images/top_blue.gif width="3000">&nbsp;
                	<B>
                	<%=masterTitle.toUpperCase()%>
                	</B>
                </TD>
                <TD align=right width="1%" background=images/top_blue.gif><IMG height=18 src="images/top_right_blue.gif" width=18></TD>
               </TR>
               
               <TR>
                <TD colSpan=3>
                  <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0  id="tb2">
                    <TR>
                      <TD width=1 bgColor=#003082><IMG src="images/trans.gif" width=1></TD>
                      <TD vAlign=top align=middle >
                        <TABLE width="100%" border=0 align=center cellPadding=2 cellSpacing=0 class="backceldas" id="tb3">
                          <TR>
                            <TD width=18>&nbsp;</TD>
                            <TD>
								<%
								if (arraySearchResults != null && arraySearchResults.size() > 0)
								{
								 //
								 String titleReporWithData = masterTitle + " "+ Languages.getString("jsp.admin.reports.ach.fmr.RangeTitle",
								 							 					 new String[] { 
								 							 									strStartDate,
								 							 									strEndDate 
								 							 								 }, 
								 							 					 SessionData.getLanguage()
								 							 					);
								%>					
								    <form id="frmSubmit" method="post" action="admin/reports/ach/summaryFMRCommissionPayout.jsp">
										<input type="hidden" name="StartDate" value="<%=request.getParameter("StartDate")%>">
										<input type="hidden" name="EndDate" value="<%=request.getParameter("EndDate")%>">
										<input type="hidden" id="varPage" name="page" value="1">				
									</form>
									<script>
										function GoToPage(nPage)
										{
											document.getElementById('varPage').value = nPage;
											document.getElementById('frmSubmit').submit();
										}
									</script>
									
									<table width="100%" border="0" cellspacing="0" cellpadding="1">	
										<%
							            if (bShowLabelLimitDays && arraySearchResults != null && arraySearchResults.size() > 0)
							            {
							            %>
							               <TR>
							                <TD colspan="3" align=center>
								               <Font style="color: red">
								                	* <%=Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage())%>
								                                                <%=" "+limitDays+" "%>
								                                                <%=Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage())%>
								               </Font>
							                </TD>                
							               </TR>
							            <%
							            }
							            %>   			
										<tr>
										    <td width="18" align="left">&nbsp;
						                       	<a href="admin/reports/ach/fmrCommissionPayout.jsp">
						                       		<%=Languages.getString("jsp.admin.ach.summary.continue",SessionData.getLanguage())%>
						                       	</a>
							                </td>
										    <td colspan="1" align=center class="main" nowrap>
										    <%=titleReporWithData%>
										    </td>				    	
											<td colspan="1" align=right class="main" nowrap>
											<%
											if (nPage > 1)
											{
											%>
												<a href="javascript:GoToPage(1)"><%=Languages.getString("jsp.admin.first",SessionData.getLanguage())%></a>
												<a href="javascript:GoToPage(<%=nPage - 1%>)">&lt;&lt;<%=Languages.getString("jsp.admin.previous",SessionData.getLanguage())%></a>
											<%
											}
						
											int nLowerLimit = nPage - 12;
											int nUpperLimit = nPage + 12;
									
											if (nLowerLimit < 1)
											{
												nLowerLimit = 1;
												nUpperLimit = 25;
											}
						
											for (int i = nLowerLimit; i <= nUpperLimit && i <= nPageCount; i++)
											{
												if (i == nPage)
												{
													out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
												}
												else
												{
									%>
													<a href="javascript:GoToPage(<%=i%>)"><%=i%></a>
									<%
												}
											}
						
											if (nPage <= (nPageCount - 1))
											{
									%>
												<a href="javascript:GoToPage(<%=nPage + 1%>)"><%=Languages.getString("jsp.admin.next",SessionData.getLanguage())%>&gt;&gt;</a>
												<a href="javascript:GoToPage(<%=nPageCount%>)"><%=Languages.getString("jsp.admin.last",SessionData.getLanguage())%></a>
									<%
											}
						
									%>
											</td>
										</tr>
									</table>
			 				
	        <table width="100%" cellspacing="1" cellpadding="1" id="t1">
   	      	 
   	      		 <tr class="SectionTopBorder">
           		  <td class="rowhead2">#</td>
           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.ach.fmr.datetime",SessionData.getLanguage()).toUpperCase()%></td>
           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.ach.fmr.descr",SessionData.getLanguage()).toUpperCase()%></td>
           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.ach.fmr.NoTrx",SessionData.getLanguage()).toUpperCase()%></td>
           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.ach.fmr.CommissionAmt",SessionData.getLanguage()).toUpperCase()%></td>
           		 </tr>
           	  
           	<%
           		int intCounter = 1;		               		
              	int intEvenOdd = 1;
              	for (FmrCommissionPaymentVo commissionVo : arraySearchResults)
              	{	                    	   		
                	String strLink = "";
                	String formattedAmt = NumberUtil.formatCurrency(commissionVo.getCommissionAmount(), true);
                 	String dateTime = DateUtil.formatDateNoTime(commissionVo.getDateTime());
                 	   		
			        strLink = "<a href=\"javascript:void(0);\" onclick=\"openViewDetail('"+commissionVo.getId().trim()+"','"+commissionVo.getBillingBatchId().trim()+"','"+commissionVo.getEntityId().trim()+"','"+commissionVo.getStatementTypeID().trim()+"','"+commissionVo.getPaymentTypeId().trim()+"');\"><span class=\"showLink\"><span class=\"showLink\">"+formattedAmt+"</span></span></a>";
               		out.println("<tr class=row" + intEvenOdd +">");
               		out.println("<td>" + intCounter++ + "</td>");
               		out.println("<td>" + dateTime +"</td>");
               		out.println("<td>" + commissionVo.getDescription() + "</td>");
               		out.println("<td>" + commissionVo.getNoTransactions() + "</td>");
               		out.println("<td align=\"right\" >" + strLink + "</td>");
               		out.println("</tr>");
                    //out.println("<tr>");
                    //out.println("<td colspan='5'  align=\"right\">");
                    //out.println("  <input type='hidden' id='hidden"+commissionVo.getId()+"' name='hidden"+commissionVo.getId()+"' value='0' />");
                    //out.println("  <div id='divDetail"+ commissionVo.getId() +"' >");
                    //out.println("  </div>");
                    //out.println("</td>");
                    //out.println("</tr>");
                	if (intEvenOdd == 1)
                	{
           	      	  intEvenOdd = 2;
             	   	}	
                    else
                   	{
                      intEvenOdd = 1;
                   	}
                }                  	  		
				out.println("</table>");
}
else
{
	%>
		<tr>
		    <td width="18" align="left">&nbsp;
                       	<a href="admin/reports/ach/fmrCommissionPayout.jsp">
                       		<%=Languages.getString("jsp.admin.ach.summary.continue",SessionData.getLanguage())%>
                       	</a>
            </td>
        </tr>       
	<%	
	if(strErrorDate.equals(""))
	{
	 	out.println("<td class=main align=left><br><br><font color=ff0000>"  + Languages.getString("jsp.admin.ach.summary.not_found",SessionData.getLanguage()) + "</font><br></td>");
	}
	else
	{
		out.println("<td class=main align=left><br><br><font color=ff0000>"  + strErrorDate + "</font><br><br></td>");
	}
}
%>	
                            
                            
                      </td>
                    </tr>
                   </table>
                </td>
                <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	         </tr>
	         <tr>
		        <td height="1" bgcolor="#003082" colspan="3">
		        	<img src="images/trans.gif" height="1">
		        </td>
            </tr>
            </table>
        </td>
    </tr>
</table>
                            

<%@ include file="/includes/footer.jsp"%>
