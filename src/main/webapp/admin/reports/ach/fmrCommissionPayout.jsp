<%@ 
	page import="java.util.*,com.debisys.utils.DebisysConfigListener" 
%>
<%
	int section=4;	
	int section_page=55;
%>
	<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
	<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
	<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ 
	include file="/includes/security.jsp" 
%>
<%
    /////////////////////////////////////////////////////////////////////////////// 
    //in the future can be usefull, by now just aplies to the current user login.
	boolean showCombosChainLeves = false;
	///////////////////////////////////////////////////////////////////////////////
	
	String strIsoID = "-2";
	String strAgentID = "-2";
	String strSubAgentID = "-2";
	String strRepID = "-2";
	String strMerchantID = "-2";
	String strStartDate = "";
	String strEndDate = "";	
	int intACHRange = DebisysConfigListener.getACHRange(application) - 1;
	
	if (request.getParameter("submitted") != null)
	{
		try
	    {
		    if (request.getParameter("submitted").equals("y"))
    		{
		    	if (request.getParameter("IsoID") != null)
		    	{
		    		strIsoID = request.getParameter("IsoID");
		    	}
		    	if (request.getParameter("AgentID") != null)
		    	{
		    		strAgentID = request.getParameter("AgentID");
		    	}
		    	if (request.getParameter("SubAgentID") != null)
		    	{
		    		strSubAgentID = request.getParameter("SubAgentID");
		    	}
		    	if (request.getParameter("RepID") != null)
		    	{
		    		strRepID = request.getParameter("RepID");
		    	}
		    	if (request.getParameter("MerchantID") != null)
		    	{
		    		strMerchantID = request.getParameter("MerchantID");
		    	}
		    	if (request.getParameter("StartDate") != null)
		    	{
		    		strStartDate = request.getParameter("StartDate");
		    	}
		    	if (request.getParameter("EndDate") != null)
		    	{
		    		strEndDate = request.getParameter("EndDate");
		    	}		    	
		    }
  		}
		catch (Exception e){}  
	}		    		
%>
<%@ 
	include file="/includes/header.jsp" 
%>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.frmMain.showRep.disabled = true;
  document.frmMain.showRep.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    	<td width="18" height="20" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	    <td class="formAreaTitle" align="left" width="3000"><%=Languages.getString("jsp.admin.reports.ach.title.fmr_commission_payout",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20" align="right"><img src="images/top_right_blue.gif"></td>
		</tr>
	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
			  <tr>
			    <td>
	    			<form name="frmMain" id="frmMain" method="post" action="admin/reports/ach/summaryFMRCommissionPayout.jsp" onSubmit="scroll();">
	    				<table border="0" width="100%" cellpadding="0" cellspacing="0">
     						<tr>
	        					<td class="formArea2">
	          						<table width="300">
               							<tr>
               								<td valign="top" nowrap="nowrap">
												<table width="400">
	                            		            	                	        			        	                	        			      	                	        			        	                	        			        	                	        			      	                	        			        	                	        			      	                	        			        	                	        			
			              							<tr class="main">
            			   								<td nowrap="nowrap">
										               		<%=Languages.getString("jsp.admin.reports.ach.title.fmr_commission_payout.rangeDates",SessionData.getLanguage())%>: 
							        			       	</td>
							        			       	<td colspan="3">&nbsp;</td>
			               							</tr>    	    	                	        		
    	    	                	        		<tr class="main">													
			    										<td nowrap="nowrap">
			    											<%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>: 
			    										</td>
			    										<td>
			    											<input class="plain" id="txtStartDate" name="txtStartDate" value="<%=strStartDate%>" size="12">
			    												<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frmMain.txtStartDate,document.frmMain.txtEndDate);return false;" HIDEFOCUS="HIDEFOCUS">
			    													<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
			    												</a>
			    										</td>
    													<td nowrap="nowrap">
    														<%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: 
    													</td>
    													<td>
    														<input class="plain" id="txtEndDate" name="txtEndDate" value="<%=strEndDate%>" size="12">
    															<a href="javascript:void(0)" onclick="setDate();" HIDEFOCUS="HIDEFOCUS">
    																<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
    															</a>
			    											<SCRIPT type="text/javascript">
																function PadLeft(sValue, sPad, nLength) 
																{
																	sValue = sValue.toString();
																  	while ( sValue.length < nLength )
																    sValue = sPad + sValue;
																	return sValue;
																}			    											
			    												function setDate()
                                        	            		{
  																	if(!document.getElementById("txtStartDate").value.length == 0) 
  																	{
  																		var dtStart = Date.parse(document.getElementById("txtStartDate").value);
  																		dtStart = new Date(dtStart);
  																		var sStartYear = dtStart.getFullYear();
  																		var sStartMonth = PadLeft(dtStart.getMonth() + 1, "0", 2);
  																		var sStartDay = PadLeft(dtStart.getDate(), "0", 2);
  																		dtStart.setDate(dtStart.getDate() + <%=intACHRange%>);
  																		var sEndYear = dtStart.getFullYear();
  																		var sEndMonth = PadLeft(dtStart.getMonth() + 1, "0", 2);
  																		var sEndDay = PadLeft(dtStart.getDate(), "0", 2);
 																		if(self.gfPop)
																			gfPop.fPopCalendar(document.frmMain.txtEndDate,[[sStartYear,sStartMonth,sStartDay],[sEndYear,sEndMonth,sEndDay],[sStartYear,sStartMonth]]);
																	}
																	else
																	{
																		if(self.gfPop)
																			gfPop.fEndPop(document.frmMain.txtStartDate,document.frmMain.txtEndDate);
																	}	                                        	            		
	                                        	            		return false;
                                        	            		}
			    											</SCRIPT>    														
    													</td>
													</tr>													
													<tr class="main">
    													<td colspan="4">      														
									         				<input type="Button" name="showRep" id="showRep" 
									         					   value="<%=Languages.getString("jsp.admin.reports.ach.buttonShowReport",SessionData.getLanguage())%>" 
									         					   onclick="showReport();">
			            	    		                	<SCRIPT type="text/javascript" >            	    		    	            				
            	    		        	        					function showReport()
                                        	            			{
                                        	            			    var StartDate = document.getElementById('txtStartDate').value;
																		var EndDate = document.getElementById('txtEndDate').value;
																		//alert(StartDate+"  "+EndDate);        
                                        	            			    document.frmMain.action = '/support/admin/reports/ach/summaryFMRCommissionPayout.jsp?StartDate='+StartDate+'&EndDate='+EndDate;
                                                    	    			document.frmMain.submit();
                                                     				}
            	    		    	            					
                                                			</SCRIPT>
    													</td>
													</tr>
													
												</table>
		               						</td>
              							</tr>
            						</table>
            					</td>
            				</tr>	
            			</table>
            		 </form>
            	  </td>
               </tr>	
           </table>
		</td>
	</tr>
</table>
	<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
	</iframe>
<%@ 
	include file="/includes/footer.jsp" 
%>