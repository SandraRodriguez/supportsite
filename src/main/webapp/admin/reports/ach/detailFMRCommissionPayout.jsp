<%@ page import="java.net.URLEncoder,com.debisys.users.User,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.reports.ach.*, 
                 com.debisys.utils.DateUtil, 
                 com.debisys.utils.NumberUtil,
                 com.debisys.languages.Languages" %>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="AchReports" class="com.debisys.reports.ach.AchReports" scope="request"/>
                 
<%
 String path = request.getContextPath();
 String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
 ArrayList<FmrCommissionPaymentVo> arraySearchResults = new ArrayList<FmrCommissionPaymentVo>();
  
 String entityId = request.getParameter("Entity");
 String billingBatchId = request.getParameter("batchId");
 String statementTypeID = request.getParameter("statementId");
 String paymentTypeId = request.getParameter("paymentTypeId");
 
 arraySearchResults = AchReports.FRMCommisionPayoutDetail(entityId,billingBatchId,statementTypeID,paymentTypeId);
 
 String masterTitle = Languages.getString("jsp.admin.reports.ach.title.fmr_commission_payout",SessionData.getLanguage())+" "+Languages.getString("jsp.admin.reports.ach.title.fmr_commission_payout_detail",SessionData.getLanguage());	    
 	
 
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title><%=masterTitle%></title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="default.css" type="text/css" rel="stylesheet">
	<script type="text/javascript">        
        setTimeout("window.close()",180*1000);							        
    </script>
  </head>
  
  <body>
    <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
	   <thead>
	    <tr class="SectionTopBorder">
         <td class="rowhead2">#</td>
         <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.ach.fmr.TrxType",SessionData.getLanguage()).toUpperCase()%></td>
         <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.ach.fmr.NoTrx",SessionData.getLanguage()).toUpperCase()%></td>
         <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.ach.fmr.CommissionAmt",SessionData.getLanguage()).toUpperCase()%></td>
        </tr>
        </thead>
		      <%  	
         		int intCounter = 1;		               		
            		int intEvenOdd = 1;
            		for (FmrCommissionPaymentVo commissionVo : arraySearchResults)
            		{	                    	   		
               	   		String strLink = "";
               	   		String formattedAmt = NumberUtil.formatCurrency(commissionVo.getCommissionAmount(), true);
               	   		String dateTime = DateUtil.formatDateNoTime_DB(commissionVo.getDateTime());
               	   		
	         			out.println("<tr class=row" + intEvenOdd +">");
                      	out.println("<td>" + intCounter++ + "</td>");
                      	out.println("<td>" + commissionVo.getTransactionType() +"</td>");
                      	out.println("<td>" + commissionVo.getNoTransactions() + "</td>");
                      	out.println("<td align=\"right\" >" + formattedAmt + "</td>");                           		
                        out.println("</tr>");
                           
                      	if (intEvenOdd == 1)
                      	{
                 	    	intEvenOdd = 2;
               	   		}	
                      	else
                     	{
                        	intEvenOdd = 1;
                     	}
              		} 		              	  		
		      %> 
      </table>
  </body>
</html>
