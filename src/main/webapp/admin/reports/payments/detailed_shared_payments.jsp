<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Rep,
                 com.debisys.customers.Merchant" %>
                 
<%@page import="com.debisys.utils.TimeZone"%>
<%@page import="com.debisys.ach.TransactionSearch"%>
<%
int section = 4;
int section_page = 50;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script>
function onChangeActor(ctlLevel)
{
  var actorVal = $("#actors").val();
  var repList =  $("#reps");

  repList.empty();

  if ( actorVal == "" )
  {
    ctlLevel = "<%=strAccessLevel%>";
    actorVal = <%=SessionData.getProperty("ref_id")%>;
  }
	
  //$.getJSON("/support/includes/ajax.jsp?class=com.debisys.customers.Merchant&method=getMerchantsByActorAjax&value=" + ctlLevel + "_" + actorVal + "_<%=strDistChainType%>&rnd=" + Math.random(),
    $.getJSON("/support/includes/ajax.jsp?class=com.debisys.customers.Rep&method=getRepsByActorAjax&value=" + ctlLevel + "_" + actorVal + "_<%=strDistChainType%>&rnd=" + Math.random(),
          function(data)
          {
            var option = $("<option>");
            option.val("");
            option.text("<%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%>");
            option.appendTo(repList);
            
            $.each(data.items, function(i, item)
            {
              var option = $("<option>");
              option.val(item.rep_id);
              item.businessname = unescape(URLDecode(item.businessname));
              item.businessname = item.businessname.replace(/\+/g, " ");
              option.text(item.businessname);             
              option.appendTo(repList);
              
            });
            repList.val("<%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%>");
          });
  //terminalType_onclick(document.getElementById("terminalType"));
}
function URLDecode(utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;
 
    while ( i < utftext.length ) {
 
      c = utftext.charCodeAt(i);
 
      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      }
      else if((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i+1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }
      else {
        c2 = utftext.charCodeAt(i+1);
        c3 = utftext.charCodeAt(i+2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }
 
    }
 
    return string;
  }
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
  <tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.payments.detailedrep_payments.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
    <td colspan="3"  bgcolor="#FFFFFF">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
        <tr>
          <td>
            <form name="mainform" method="post" action="admin/reports/payments/detailed_shared_payments_summary.jsp" onSubmit="document.getElementById('btnSubmit').disabled = true;">
              <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="formArea2">
                    <table width="300">
                    <%
	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
%>
              <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
              
                      <tr class="main">
                        <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>
		         <%}%>:</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td valign="top" nowrap>
                          <table width=400>
                            <tr class="main">
                              <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
                              <td>
                                <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12">
                                <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
                              </td>
                            </tr>
                            <tr class="main">
                              <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:</td>
                              <td>
                                <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12">
                                <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
                              </td>
                            </tr>
<%
  if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
%>
                            <tr>
                              <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.payments.index.selectrep_agents",SessionData.getLanguage())%></td>
                              <td class=main valign=top>
                                <select id="actors" name="agents" onchange="onChangeActor('<%=DebisysConstants.AGENT%>');">
	                                  <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
									<%
									    Vector v = TransactionSearch.getAgentList(SessionData, "");
									    Iterator it = v.iterator();
									    while (it.hasNext())
									    {
									      Vector vTemp = null;
									      vTemp = (Vector) it.next();
									      out.println("<option value=" + vTemp.get(0) +">" + vTemp.get(1) + "</option>");
									    }
									%>
									</select>
								</td>
							</tr>
						<%
						 }
  else if (strAccessLevel.equals(DebisysConstants.AGENT))
  {
%>
                            <tr>
                              <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.payments.index.selectrep_subagents",SessionData.getLanguage())%></td>
                              <td class=main valign=top>
                                <select id="actors" name="subagents" onchange="onChangeActor('<%=DebisysConstants.SUBAGENT%>');">
                                  <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
    Vector v = TransactionSearch.getSubAgentList(SessionData, "");
    Iterator it = v.iterator();
    while (it.hasNext())
    {
      Vector vTemp = null;
      vTemp = (Vector) it.next();
      out.println("<option value=" + vTemp.get(0) +">" + vTemp.get(1) + "</option>");
    }
%>
                                </select>
                              </td>
                            </tr>
<%
  }
  else if (strAccessLevel.equals(DebisysConstants.SUBAGENT) || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))
  {
%>
                            <tr>													   
                              <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.payments.index.selectrep_reps",SessionData.getLanguage())%></td>
                              <td class=main valign=top>
                              <select id="reps" name="rids" size="10" multiple>
                                  <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
    Vector v = TransactionSearch.getRepList(SessionData, "");
     //Vector v = Rep.getRepListReports(SessionData);
    Iterator it = v.iterator();
    while (it.hasNext())
    {
      Vector vTemp = null;
      vTemp = (Vector) it.next();
      out.println("<option value=" + vTemp.get(0) +">" + vTemp.get(1) + "</option>");
    }
%>
                                </select>
                              </td>
                            </tr>
<%
  }

  //if ( strAccessLevel.equals(DebisysConstants.SUBAGENT) )
  if ( strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.SUBAGENT) )
  {
%>
                            <tr><td class=main valign=top nowrap><input type=hidden name=risds value="<%=SessionData.getProperty("ref_id")%>"></td></tr>
<%
  }
  else
  {
  
   if ( ! strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL ) ){
%>
                            <tr>
                              <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.payments.index.selectrep_reps",SessionData.getLanguage())%></td>
                              <td class=main valign=top>
                                <select id="reps" name="rids" size="10" multiple>
                                  <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
									<%
									    //Vector v = Merchant.getMerchantListReports(SessionData);
									    Vector v = Rep.getRepListReports(SessionData);
									    Iterator it = v.iterator();
									    while (it.hasNext())
									    {
									      Vector vTemp = null;
									      vTemp = (Vector) it.next();
									      out.println("<option value=" + vTemp.get(0) +">" + vTemp.get(1) + "</option>");
									    }
									%>
                                </select>
                                <br>*<%=Languages.getString("jsp.admin.reports.payments.index.instructions",SessionData.getLanguage())%>
                              </td>
                            </tr>
<%
		}
  }
%>
                            <tr>
                              <td class=main colspan=2 align=center>
                                <input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
                              </td>
                            </tr>
                            <tr><td class="main" colspan=2>* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%></td></tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </form>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>