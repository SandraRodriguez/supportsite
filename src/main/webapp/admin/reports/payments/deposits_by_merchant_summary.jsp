<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.customers.UserPayments"
                  %>
<%@page import="com.debisys.utils.DateUtil"%>
<%
  int section      = 4;
  int section_page = 34;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:useBean id="User" class="com.debisys.customers.UserPayments" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<%
Hashtable searchErrors = null;
Vector    vecSearchResults = new Vector();

if (request.getParameter("search") != null)
{
	if (TransactionReport.validateDateRange(SessionData))
	{
		SessionData.setProperty("start_date", request.getParameter("startDate"));
		SessionData.setProperty("end_date", request.getParameter("endDate"));

		//user.setStrUserNames(request.getParameterValues("mids"));
		User.setStrRepIds(request.getParameterValues("mids"));
		User.setStart_date(request.getParameter("startDate"));
		User.setEnd_date(request.getParameter("endDate"));
		vecSearchResults = User.getMerchantsPayments(SessionData);

	}
	else
	{
		searchErrors = TransactionReport.getErrors();
	}
}
%>

<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= Languages.getString("jsp.admin.reports.title42",SessionData.getLanguage()).toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
    	<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
if (searchErrors != null)
{
    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
    Enumeration enum1=searchErrors.keys();
    while(enum1.hasMoreElements())
    {
      String strKey = enum1.nextElement().toString();
      String strError = (String) searchErrors.get(strKey);
      out.println("<li>" + strError);
    }
    out.println("</font></td></tr></table>");
}
else
{
%>
<% if (vecSearchResults.size() <= 0) {%>

    <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
    <tr>
        <td>
             <%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.not_found",SessionData.getLanguage())%>
        </td>
    </tr> 
    </table>
<%}
else{
	Iterator itTemp = vecSearchResults.iterator();
	double dblTotalPayment = 0;	
	if(itTemp.hasNext()) {
	    Vector vecTemp = null;
	    while (itTemp.hasNext())                    
	    {                                       
	        vecTemp = null;              
	        vecTemp = (Vector)itTemp.next(); 
	        dblTotalPayment += Double.parseDouble(vecTemp.get(4).toString());  
		}
	}

%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr><td class="main">
		<%out.println(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.deposits_by_merchant_summary",SessionData.getLanguage()));
		if (!User.getStart_date().equals("") && !User.getEnd_date().equals(""))
		{
		   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(User.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(User.getEndDateFormatted()));
		}
		%>
		</td>
        <td class=main align=right valign=bottom>
              <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
        </td>		
		</tr>
	</table>
	<table> 
		<tr>
			<td class="main"><font color=ff0000><%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.warning",SessionData.getLanguage())%>
			</font></td>
		</tr>    
		<tr>
			<td class="main"><%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.totalPayments",SessionData.getLanguage())%>: <%=NumberUtil.formatCurrency(Double.toString(dblTotalPayment))%></td>
		</tr>         
	  <tr>
	    <td>
	    <FORM ACTION="admin/reports/payments/deposits_by_merchant_export.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
	    	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
	    	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
	    	<INPUT TYPE=hidden NAME=merchantIds VALUE="<%=User.getStrRepIds()%>">
	    	<INPUT TYPE=hidden NAME=Download VALUE="Y">
	    	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.download",SessionData.getLanguage())%>">
	    </FORM>
	    </td>
	    <td>&nbsp;&nbsp;&nbsp;</td>
	    <td>
	    </td>
	  </tr>
	</table>
		
		<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
          <thead>
              <tr class="SectionTopBorder"> 
                  <% 
                  out.println("<td class=rowhead2>#</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.merchant_id",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.dba",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.logon_id",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.description",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.balance",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.prior_available_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.Deposit_date",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.log_datetime",SessionData.getLanguage()).toUpperCase() + "</td>");
                  %>
              </tr>
          </thead>
<%//Iteration over results
    int intEvenOdd = 1;
    int intCounter = 1; 
      
	Iterator it = vecSearchResults.iterator();

	if(it.hasNext()) {
	    Vector vecTemp = null;
	    while (it.hasNext())                    
	    {                                       
	        vecTemp = null;              
	        vecTemp = (Vector)it.next();       
	        String depositDate = (vecTemp.get(7) != null)? vecTemp.get(7).toString():"N/A";
	        out.println("<tr class=row" + intEvenOdd +">" +
	     		   "<td>" + intCounter+ "</td>" +
	                "<td nowrap>" + vecTemp.get(0)+ "</td>" +
	                "<td>" + vecTemp.get(1) + "</td>" +
	                "<td>" + vecTemp.get(2) + "</td>" +
	                "<td>" + vecTemp.get(3) + "</td>" +
	                "<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(4).toString()) + "</td>" +
	                "<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
	                "<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
	                "<td align=right>" + depositDate + "</td>" +
	                "<td align=right>" + vecTemp.get(8) + "</td>" +
	        "</tr>");
		    if (intEvenOdd == 1)
		    {
		      intEvenOdd = 2;
		    }
		    else
		    {
		      intEvenOdd = 1;
		    }
		    intCounter++;	    
	    }	    
	}
%>
		</table>
<SCRIPT type="text/javascript">
<!--
  var stT1 = new SortROC(document.getElementById("t1"), ["None","Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","Number", "Number","Number","DateTime","DateTime"],0,false,false);
-->
</SCRIPT> 

<%}%>
			
  		</td>
</table>  
<%} %>
<%@ include file="/includes/footer.jsp" %>
