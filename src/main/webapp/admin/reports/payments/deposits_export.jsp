<%@ page import="java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.customers.UserPayments"%>
<%
	int section = 4;
	int section_page = 34;
%>
<jsp:useBean id="SessionData" class = "com.debisys.users.SessionData"	scope="session" />
<jsp:useBean id="User" class="com.debisys.customers.UserPayments" scope="request" />
<jsp:useBean id= "TransactionReport"	class="com.debisys.reports.TransactionReport" scope="request" />
<%@ include file="/includes/security.jsp"%>

<%
String strRuta = "";
Vector vecSearchResults = new Vector();
UserPayments user = new UserPayments();
String strMerchantIds[] = null;
String extraFilter = User.getPermissionFaceyJAMGrpPaymentDetailsReport(SessionData);
if(!extraFilter.equals("1")){
	user.setStrRepIds(request.getParameter("merchantIds"));
}
else if(extraFilter.equals("1")){
	strMerchantIds = request.getParameterValues("mids");
}

user.setStart_date(request.getParameter("startDate"));
user.setEnd_date(request.getParameter("endDate"));
String entityType=request.getParameter("entityType");
if(!extraFilter.equals("1")){
	vecSearchResults = user.getPaymentsDownload(SessionData,application,entityType,null,null);
}
else{
	String itemSelected = request.getParameter("itemSelected");
	if(itemSelected != null && (itemSelected.equals("0"))){
		itemSelected = "-1";
	}
	if(itemSelected != null && (itemSelected.equals("1"))){
	  	itemSelected = "0";
	}
	vecSearchResults = user.getPaymentsDownload(SessionData,application,entityType,itemSelected,strMerchantIds);
}


strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,3,SessionData);
if (strRuta.length() > 0)
{
	response.sendRedirect(strRuta);
}
%>
