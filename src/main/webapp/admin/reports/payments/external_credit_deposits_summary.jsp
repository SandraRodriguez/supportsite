<%@ page
	import="java.net.URLEncoder,com.debisys.utils.HTMLEncoder,java.util.*,com.debisys.utils.NumberUtil,com.debisys.reports.schedule.ExternalCreditOrPaymentReport,com.debisys.schedulereports.ScheduleReport,com.debisys.utils.ColumnReport,com.debisys.utils.StringUtil"%>
<%
	int section = 4;
	int section_page = 10;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
	<jsp:useBean id="PaymentReport" class="com.debisys.reports.PaymentReport" scope="request"/>
	<jsp:setProperty name="PaymentReport" property="*"/>
	<jsp:useBean id="ExternalCreditOrPaymentReport"
	class="com.debisys.reports.schedule.ExternalCreditOrPaymentReport"
	scope="request" />
	<jsp:setProperty name="ExternalCreditOrPaymentReport" property="*" />
<%@ include file="/includes/security.jsp"%>

<%

SessionData.setProperty("reportId", DebisysConstants.SC_EXTERNAL_CREDIT_OR_PAYMENT);
ExternalCreditOrPaymentReport report = null;
Vector<Vector<Object>> vecSearchResults = null;
Hashtable searchErrors = null;
boolean allMerchantsSelected = false;
String reportTitleKey = "jsp.admin.reports.extern.title1";

String reportTitle = Languages.getString(reportTitleKey,SessionData.getLanguage()).toUpperCase();
String[] merchantIds = null;
String merchantList = null; // Coma separated string

merchantIds = request.getParameterValues("merchantIds");

if(merchantIds != null)
{
	merchantList = com.debisys.utils.StringUtil.arrayToString(merchantIds, ",");
	if(merchantList.equals(""))
	{
		allMerchantsSelected = true;
	}
}



String trReportStartDate = PaymentReport.getStartDate();
if((trReportStartDate == null) || ((trReportStartDate != null) && (trReportStartDate.trim().equals(""))))
{
	trReportStartDate = request.getParameter("startDate");
	PaymentReport.setStartDate(trReportStartDate);	
}
String trReportEndDate = PaymentReport.getEndDate();
if((trReportEndDate == null) || ((trReportEndDate != null) && (trReportEndDate.trim().equals(""))))
{
	trReportEndDate = request.getParameter("endDate");
	PaymentReport.setEndDate(trReportEndDate);	
}
SessionData.setProperty("start_date", trReportStartDate);
SessionData.setProperty("end_date", trReportEndDate);


if (PaymentReport.validateDateRange(SessionData))
{
	report = new ExternalCreditOrPaymentReport(SessionData, application, false, trReportStartDate, trReportEndDate, merchantList,
			DebisysConfigListener.getDeploymentType(application), allMerchantsSelected);
	
	if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	{
		//TO SCHEDULE REPORT
		report.setScheduling(true);
		if(report.scheduleReport())
		{
			ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
			if (  scheduleReport != null  )
			{
				scheduleReport.setStartDateFixedQuery( report.getStart_date());
				scheduleReport.setEndDateFixedQuery( report.getEnd_date());
				scheduleReport.setTitleName( reportTitleKey );   
			}	
			response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
		}
	}
	else if ( request.getParameter("downloadReport") != null )
	{
		//TO DOWNLOAD ZIP REPORT
		report.setScheduling(false);
		String zippedFilePath = report.downloadReport();
		if((zippedFilePath != null) &&(!zippedFilePath.trim().equals("")))
		{
			response.sendRedirect( zippedFilePath );
		}
	}
	else
	{
		//TO SHOW REPORT
		report.setScheduling(false);
		vecSearchResults = report.getResults();
	}
}
else
{
	searchErrors = PaymentReport.getErrors();
}


%>
	<%@ include file="/includes/header.jsp"%>
	<table border="0" cellpadding="0" cellspacing="0" width="750"
	background="images/top_blue.gif">
	<tr>
		<td width="18" height="20">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td width="2000" class="formAreaTitle">
			&nbsp;<%=Languages.getString("jsp.admin.reports.title.externalCreditOrPayment",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20">
			<img src="images/top_right_blue.gif">
		</td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				align=center>
				<tr>
					<td>
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="formArea2">
									<table width="300">
										<tr>
											<td valign="top" nowrap>
												<table width=400>
													<%
if (searchErrors != null)
{
  	out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
	Enumeration enum1=searchErrors.keys();
	while(enum1.hasMoreElements())
	{
		String strKey = enum1.nextElement().toString();
		String strError = (String) searchErrors.get(strKey);
		out.println("<li>" + strError);
	}

  out.println("</font></td></tr>");
}
%>
													</tr>
												</table>
	</td>
	</tr>
	</table>
	<%
		if ((vecSearchResults != null) && (vecSearchResults.size() > 0))
			{
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr>
			<td class="main"><%=Languages.getString("jsp.admin.reports.extern.title1", SessionData.getLanguage())%>
				<%
					if ((!PaymentReport.getStartDate().equals("")) && (!PaymentReport.getEndDate().equals("")))
							{
								out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " "
										+ HTMLEncoder.encode(PaymentReport.getStartDateFormatted()) + " "
										+ Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " "
										+ HTMLEncoder.encode(PaymentReport.getEndDateFormatted()));
							}
				%>
			</td>
		</tr>
		<tr>
			<td class="main">
				<form name="downloadData" method=post
					action="admin/reports/payments/external_credit_deposits_summary.jsp">
					<input type="hidden" name="startDate"
						value="<%=request.getParameter("startDate")%>">
					<input type="hidden" name="endDate"
						value="<%=request.getParameter("endDate")%>">
					<input type="hidden" name="downloadReport" value="y">
<%
					if((merchantIds != null) && (merchantIds.length > 0))
					{
						for(int i = 0; i < merchantIds.length;i++)
						{
%>
					<input type="hidden" value="<%=merchantIds[i]%>" name="merchantIds">
<%
						}
					}
%>					
					<label>
						<%=Languages.getString("jsp.admin.reports.transaction.rep_actions_summary.DownloadLabel",
							SessionData.getLanguage())%>
					</label>
					<input type=submit name=submit
						value="<%=Languages.getString("jsp.admin.reports.transaction.rep_actions_summary.Download",
							SessionData.getLanguage())%>">
				</form>
			</td>

		</tr>

	</table>
	<table width="100%" cellspacing="1" cellpadding="2">
		<tr>
			<%
				String strSortURL = "admin/reports/payments/external_credit_deposits_summary.jsp?search=y&startDate="
								+ PaymentReport.getStartDate()
								+ "&endDate="
								+ PaymentReport.getEndDate()
								+ "&merchantIds="
								+ PaymentReport.getMerchantIds();
			%>
			<td class=rowhead2 valign=bottom>
				#
			</td>
			<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.dba", SessionData.getLanguage()).toUpperCase()%>&nbsp;
				<a href="<%=strSortURL%>&col=1&sort=1"><img
						src="images/down.png" height=11 width=11 border=0> </a><a
					href="<%=strSortURL%>&col=1&sort=2"><img src="images/up.png"
						height=11 width=11 border=0> </a>
			</td>
			<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.id", SessionData.getLanguage()).toUpperCase()%>&nbsp;
				<a href="<%=strSortURL%>&col=2&sort=1"><img
						src="images/down.png" height=11 width=11 border=0> </a><a
					href="<%=strSortURL%>&col=2&sort=2"><img src="images/up.png"
						height=11 width=11 border=0> </a>
			</td>
			<%
				if (!DebisysConfigListener.getDeploymentType(application).equals(
								DebisysConstants.DEPLOYMENT_DOMESTIC))
						{
			%>
			<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.customers.merchants_edit.route",
								SessionData.getLanguage()).toUpperCase()%>&nbsp;
				<a href="<%=strSortURL%>&col=3&sort=1"><img
						src="images/down.png" height=11 width=11 border=0> </a><a
					href="<%=strSortURL%>&col=3&sort=2"><img src="images/up.png"
						height=11 width=11 border=0> </a>
			</td>
			<%
				}
			%>
			<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.qty", SessionData.getLanguage()).toUpperCase()%>&nbsp;
				<a href="<%=strSortURL%>&col=4&sort=1"><img
						src="images/down.png" height=11 width=11 border=0> </a><a
					href="<%=strSortURL%>&col=4&sort=2"><img src="images/up.png"
						height=11 width=11 border=0> </a>
			</td>
			<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.total", SessionData.getLanguage()).toUpperCase()%>&nbsp;
				<a href="<%=strSortURL%>&col=5&sort=1"><img
						src="images/down.png" height=11 width=11 border=0> </a><a
					href="<%=strSortURL%>&col=5&sort=2"><img src="images/up.png"
						height=11 width=11 border=0> </a>
			</td>
		</tr>
		<%
					double dblTotalPaymentsSum = 0;
					int intTotalQtySum = 0;
					Vector vecTemp = null;

					Iterator it = vecSearchResults.iterator();
					int intEvenOdd = 1;
					int intCounter = 1;
					
					while (it.hasNext())
					{
						vecTemp = null;
						vecTemp = (Vector) it.next();
						int intTotalQty = Integer.parseInt(vecTemp.get(3).toString());
						double dblTotalSales = Double.parseDouble(vecTemp.get(4).toString());

						out.println("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>"
								+ "<td nowrap>" + vecTemp.get(0) + "</td>" + "<td>" + vecTemp.get(1) + "</td>");
						if (!DebisysConfigListener.getDeploymentType(application).equals(
								DebisysConstants.DEPLOYMENT_DOMESTIC))
						{
							out.println("<td>" + vecTemp.get(2) + "</td>");
						}
						out
								.println("<td>"
										+ intTotalQty
										+ "</td>"
										+ "<td align=right><a href=\"javascript:void(0)\" onClick=\"window.open('/support/admin/customers/merchants_external_credit_history.jsp?startDate="
										+ URLEncoder.encode(PaymentReport.getStartDate(), "UTF-8")
										+ "&intTotalQty="
										+ intTotalQty
										+ "&endDate="
										+ URLEncoder.encode(PaymentReport.getEndDate(), "UTF-8")
										+ "&merchantId="
										+ vecTemp.get(1)
										+ "','merchantHistory','width=850,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');\">"
										+ NumberUtil.formatCurrency(Double.toString(dblTotalSales))
										+ "</a></td></tr>");
						intTotalQtySum += intTotalQty; 
						dblTotalPaymentsSum += dblTotalSales;

						if (intEvenOdd == 1)
						{
							intEvenOdd = 2;
						}
						else
						{
							intEvenOdd = 1;
						}

					}
					vecSearchResults.clear();
		%>
		<tr class=row <%=intEvenOdd%>>
			<%
				if (!DebisysConfigListener.getDeploymentType(application).equals(
								DebisysConstants.DEPLOYMENT_DOMESTIC))
						{
			%>
			<td colspan=4 align=right>
				<%
					}
							else
							{
				%>
				<td colspan=3 align=right>
					<%
						}
					%>
					<%=Languages.getString("jsp.admin.reports.totals", SessionData.getLanguage())%>:
				</td>
				<td align=left><%=intTotalQtySum%></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalPaymentsSum))%>
		</tr>
	</table>
	</td>
	<%
		}
			else if ((vecSearchResults == null) ||((vecSearchResults != null) && (vecSearchResults.size() == 0)
					&& (request.getParameter("search") != null) && (searchErrors == null)))
			{
				out.println("<br><br><font color=ff0000>"
						+ Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
			}
	%>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<%@ include file="/includes/footer.jsp"%>