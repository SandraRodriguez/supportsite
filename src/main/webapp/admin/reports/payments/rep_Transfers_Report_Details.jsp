


<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
          com.debisys.reports.ReportsUtil,
                 com.debisys.utils.ColumnReport,
                 com.debisys.utils.NumberUtil,com.debisys.utils.TimeZone,java.text.SimpleDateFormat, com.debisys.schedulereports.ScheduleReport,com.debisys.customers.Rep" %>
<%
  int section      = 4;
  int section_page = 60;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
    Hashtable searchErrors     = null;
  String strRepIds[] = null;
  String strOutRepIds[]=null;
  
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();
  String noteTimeZone = "";
  String testTrx = "";  
  String titleReport = "";
     
  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
    
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));

      strRepIds = request.getParameterValues("rids");
      strOutRepIds= request.getParameterValues("outrids");

      if (strRepIds != null)
      {
        TransactionReport.setRepIds(strRepIds);
      }

      if(strOutRepIds!=null)
      {
          TransactionReport.setDestRepIds(strOutRepIds);
      }
	  //////////////////////////////////////////////////////////////////
	  //HERE WE DEFINE THE REPORT'S HEADERS 
	  headers = com.debisys.reports.ReportsUtil.getRepTransferSummaryTransactions( SessionData, application, DebisysConstants.ISO );
	  //////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////
	  
	  Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  	  noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
  	  String keyLanguage = "jsp.admin.reports.rep.transfers.title";
	  //titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
	  testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());
	  
      titles.add(noteTimeZone);
     // titles.add(testTrx);
      
      if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
	  {
		titleReport = Languages.getString(keyLanguage,SessionData.getLanguage()) + " "+Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
		titleReport = titleReport + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
 	  }
		                          
	  if ( request.getParameter("downloadReport") != null )
	  {
	  		//TO DOWNLOAD ZIP REPORT	  		
	  		titles.add(titleReport); 
			TransactionReport.getRepTransferSummary( SessionData, application, DebisysConstants.DOWNLOAD_REPORT, headers, titles);
			response.sendRedirect( TransactionReport.getStrUrlLocation() );	
	  }
	  else
	  {
	  		//TO SHOW REPORT
	  		vecSearchResults = TransactionReport.getRepTransferSummary( SessionData, application, DebisysConstants.EXECUTE_REPORT, headers, titles);
	  }
		      
	  
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
  
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
        &nbsp;
        <%= Languages.getString("jsp.admin.reports.rep.transfers.title",SessionData.getLanguage()).toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">

<%
                            if (searchErrors != null)
                            {
                              out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString
                                      ("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

                              Enumeration enum1 = searchErrors.keys();

                              while (enum1.hasMoreElements())
                              {
                                String strKey   = enum1.nextElement().toString();
                                String strError = (String)searchErrors.get(strKey);

                                out.println("<li>" + strError);
                              }

                              out.println("</font></td></tr></table>");
                            }

                        if (vecSearchResults != null && vecSearchResults.size() > 0)
                    {
%>
                      <table width="100%" border="0" cellspacing="0" cellpadding="2">
				        <tr class="main"><td nowrap colspan="2"><%= noteTimeZone %><br/><br/></td></tr>
				<tr>
                          <td class="main">
							<%= titleReport %>
                            <br>
                          </td>
                        </tr>
                      </table>
                      
                      
		              
		              <form name="downloadData" method=post action="admin/reports/payments/rep_Transfers_Report_Details.jsp">
			              <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
			              <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
			              <input type="hidden" name="search" value="y">
			              <input type="hidden" name="downloadReport" value="y">
			            	<%for(int i=0;i<strRepIds.length;i++){%>
							<input type="hidden" name="rids" value="<%=strRepIds[i]%>">
							<%}%>
                                          <%if(strOutRepIds!=null){
                                         for(int i=0;i<strOutRepIds.length;i++){%>
							<input type="hidden" name="outrids" value="<%=strOutRepIds[i]%>">
							<%}
                                          }%>
			              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		              </form>
		              
		              
                     <table>
                            <tr>
                                <td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
                            </tr>
                     </table>
                      <table width="100%" cellspacing="1" cellpadding="2" class="sort-table">
                        <tr>
<%
                          String strSortURL = "admin/reports/payments/rep_Transfers_Report_Details.jsp?search=y&startDate=" + 
                                  TransactionReport.getStartDate() + "&endDate=" + TransactionReport.getEndDate() + "&rids=" + 
                                  TransactionReport.getRepIds()+"&outrids=" + 
                                  TransactionReport.getDestRepIds();
%>
                          <td class=rowhead2 valign=bottom>
                            #
                          </td>
                           <%
                           int colNumber=1;
                           for (ColumnReport column : headers)
                           {
                           	 String columnDescription = column.getLanguageDescription();
                           	%>
                           			<td class=rowhead2 valign=bottom>
		                            <%= columnDescription %>&nbsp;
		                            <a href="<%= strSortURL %>&col=<%= colNumber %>&sort=1"><img src="images/down.png" height=11 width=11 border=0></a>
		                            <a href="<%= strSortURL %>&col=<%= colNumber %>&sort=2"><img src="images/up.png" height=11 width=11 border=0></a>
		                          </td>
                           <% 
                           	colNumber++;
                           }                           
                          
                          out.println("</tr>");

                          double   dblTotalSalesSum         = 0;

                          Iterator it                       = vecSearchResults.iterator();
                          int      intEvenOdd               = 1;
                          int      intCounter               = 1;

                          while (it.hasNext())
                          {
                            
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        SimpleDateFormat fDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String username=(String)vecTemp.get(0);
                        long senderId=0;
                        String senderName="";
                       // if(strAccessLevel.equals(DebisysConstants.ISO))
                      //  {
                            senderId=Long.valueOf(vecTemp.get(1).toString());
                             senderName=(String)vecTemp.get(2);
                       // }
                        long receiverId=Long.valueOf(vecTemp.get(3).toString());
                        String receiverName=(String)vecTemp.get(4);
                        long senderPaymentId=Long.valueOf(vecTemp.get(5).toString());
                        long reeiverPaymentId=Long.valueOf(vecTemp.get(6).toString());
                        double sales = Double.parseDouble(vecTemp.get(7).toString());
                        String ip = (String) vecTemp.get(8);
                        String strDate = (String)vecTemp.get(9);
                        dblTotalSalesSum += sales;

                        out.print("<tr class=row" + intEvenOdd + ">"
                                + "<td>" + intCounter++ + "</td>"
                                 + "<td nowrap>" +  username + "</td>"
                                + "<td nowrap>" + senderId + "</td>"
                                     + "<td nowrap>" + senderName + "</td>"
                                     + "<td nowrap>" + receiverId + "</td>"
                                     + "<td nowrap>" + receiverName + "</td>"
                                     + "<td nowrap>" + senderPaymentId + "</td>"
                                 + "<td nowrap>" + reeiverPaymentId + "</td>"
                                + "<td nowrap align=\"right\">" + NumberUtil.formatCurrency(Double.toString(sales)) + "</td>"
                                + "<td nowrap align=\"right\">" + ip + "</td>"
                                + "<td nowrap align=\"right\">" + strDate + "</td></tr>");

                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        } else {
                            intEvenOdd = 1;
                        }
                    }

                %> 
           
                    <tr class=row<%= intEvenOdd%>>
                        <td colspan="8"><strong><%=Languages.getString("jsp.admin.reports.total_payments", SessionData.getLanguage())%></strong></td>
                        <td align="right"><strong><%=NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum))%></strong></td>
                         <td align="right"><strong></strong></td>
                          <td align="right"><strong></strong></td>
                        <td> </td>
                    </tr>
            </table>
            <%

                } else if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null) {
                    out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                }
               
            %>
                              </td>
                      </tr>
                    </table>
        <%@ include file="/includes/footer.jsp" %>