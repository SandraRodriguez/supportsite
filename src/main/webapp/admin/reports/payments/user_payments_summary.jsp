<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,com.debisys.utils.NumberUtil,
                 com.debisys.customers.Merchant,
                 org.apache.torque.Torque,java.sql.*,java.util.Date,com.debisys.utils.DateUtil,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" %>
<%
int section=4;
int section_page=3;
%>

<%@page import="com.debisys.customers.UserPayments"%><jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<jsp:setProperty name="TransactionReport" property="*"/>


<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
    

<%
int reg=15;
Hashtable searchErrors = null;
Vector    vecSearchResults = new Vector();

boolean bReport=false;
boolean bShowLabelLimitDays=false;

   ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
   ArrayList<String> titles = new ArrayList<String>();
   String noteTimeZone = "";   
   String keyLanguage = "jsp.admin.reports.title40";
   String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
   
   UserPayments user = new UserPayments();
   user.setStrUserIds(request.getParameterValues("mids"));
   user.setStart_date(request.getParameter("startDate"));
   user.setEnd_date(request.getParameter("endDate"));
   
   int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays("user_payments_summary", application);
   int daysBetween = DateUtil.getDaysBetween(user.getEnd_date().toString(), user.getStart_date().toString());
   if ( daysBetween > limitDays){
		user.setStart_date(DateUtil.addSubtractDays(user.getEnd_date().toString(),-limitDays));
		bShowLabelLimitDays = true;
   }
   
   //////////////////////////////////////////////////////////////////
   //HERE WE DEFINE THE REPORT'S HEADERS 
   headers = getHeadersSummaryCreditOrPaymnetByUser( SessionData );
   //////////////////////////////////////////////////////////////////
   //////////////////////////////////////////////////////////////////
	      
   titles.add(noteTimeZone);								
   titles.add(titleReport);
   if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
   {
   		SessionData.setProperty("start_date", request.getParameter("startDate"));
		SessionData.setProperty("end_date", request.getParameter("endDate"));
		//TO SCHEDULE REPORT
		user.getSummaryReport( SessionData, application, DebisysConstants.SCHEDULE_REPORT, headers, titles );
		ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
		if (  scheduleReport != null  )
		{
			scheduleReport.setStartDateFixedQuery( user.getStart_date() );
			scheduleReport.setEndDateFixedQuery( user.getEnd_date() );
			scheduleReport.setTitleName( keyLanguage );
		}
		response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
   }
   else if ( request.getParameter("downloadReport") != null )
   {
		
		user.getSummaryReport( SessionData, application, DebisysConstants.DOWNLOAD_REPORT, headers, titles );
		response.sendRedirect( user.getUrlLocation() );
   }	  
   else
   {
	 vecSearchResults = user.getSummaryReport( SessionData, application, DebisysConstants.EXECUTE_REPORT, null, null );
   }        
		  
   
   
if (searchErrors != null)
{
    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
    Enumeration enum1=searchErrors.keys();
    while(enum1.hasMoreElements())
    {
      String strKey = enum1.nextElement().toString();
      String strError = (String) searchErrors.get(strKey);
      out.println("<li>" + strError);
    }
    out.println("</font></td></tr></table>");
}
%>

<%!public static ArrayList<ColumnReport> getHeadersSummaryCreditOrPaymnetByUser( SessionData sessionData )
   {    
	 ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
	 headers.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.reports.user.user", sessionData.getLanguage()).toUpperCase(), String.class, false));
	 headers.add(new ColumnReport("qty", Languages.getString("jsp.admin.reports.user.qty", sessionData.getLanguage()).toUpperCase(), Integer.class, true));
	 headers.add(new ColumnReport("total_payments", Languages.getString("jsp.admin.reports.user.total", sessionData.getLanguage()).toUpperCase(), Double.class, true));
	 return headers;
   }  
%>
  
<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= titleReport.toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
		if (vecSearchResults.size() > 0)
		{
				out.println("<span class=main>"+Languages.getString("jsp.admin.reports.title40",SessionData.getLanguage())+"</span>");
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                  out.println(" <span class=main>" + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(user.getStartDateFormatted()) + 
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(user.getEndDateFormatted()) + "</span>");
                
                	if (bShowLabelLimitDays){
                	%>
                	
                	<br/>
                	<Font style="color: red">
                	* <%=Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage())%>
                                                <%=" "+limitDays+" "%>
                                                <%=Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage())%>
                    </Font>                            
                	<%                 	
					}                                                
                }
                %>
				<br/>
				<form name="downloadData" method=post action="admin/reports/payments/user_payments_summary.jsp">
		          <input type="hidden" name="startDate" value="<%=user.getStart_date()%>">
		          <input type="hidden" name="endDate" value="<%=user.getEnd_date()%>">
		          <input type="hidden" name="search" value="y">
		          <input type="hidden" name="downloadReport" value="y">		          
		          <%
		          String mids[] = request.getParameterValues("mids");
		          for(int i=0;i< mids.length;i++)
		          {%>
					<input type="hidden" name="mids" value="<%=mids[i]%>">
				<%}%>
				  <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.write_off_request_report.download",SessionData.getLanguage())%>">
				</form>

				<%
        }        
 %>


<% if (vecSearchResults.size() <= 0) {%>

    <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
    <tr>
        <td class="main">
             <%=Languages.getString("jsp.admin.reports.GM5.not_found",SessionData.getLanguage())%>
        </td>
    </tr> 
    <table>
<%}
else{%>
	
		  
 <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
                            <thead>
                                <tr class="SectionTopBorder"> 
                                    <td class=rowhead2 nowrap>
                                      #
                                    </td>
                                    <% 
                                    for(ColumnReport column : headers)
                                    {
                                    %>
                                    	<td class=rowhead2 nowrap>
                                      	<%=column.getLanguageDescription()%>
                                    	</td>
                                    <% 
                                    }
                                    %>
                                </tr>
                            </thead>

                         <%
                         double paymentsSum = 0;
                         int qtSum = 0;
                         int      intEvenOdd               = 1;
                         int      intCounter               = 1; 
                         
                           Iterator it                       = vecSearchResults.iterator();
                           if(it.hasNext()) {
                        	             
                               Vector vecTemp = null;
                               vecTemp = (Vector) it.next();
                               paymentsSum = Double.parseDouble(vecTemp.get(1).toString());
                               qtSum = Integer.parseInt(vecTemp.get(0).toString());
                               
                               
                               while (it.hasNext())                    
                               {                                       
                                   vecTemp = null;              
                                   vecTemp = (Vector)it.next();        
                                   java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
                                   formatter.setLenient(false);        
                                   //Date dtDaterequest = formatter.parse(vecTemp.get(2).toString());
                                   //Date dtDaterepaid = formatter.parse(vecTemp.get(5).toString());   %>                                    
                               <tr class=row<%=intEvenOdd%>>                                 
                                   <td><%=intCounter++%></td>
                                   <td><%=vecTemp.get(0).toString()%></td>
                                   <td align="right"><%=vecTemp.get(1).toString()%></td> 
                                   
<%="<td align=right><a href=\"javascript:void(0)\" onClick=\"window.open('/support/admin/customers/merchants_credit_history.jsp?startDate=" + URLEncoder.encode(user.getStart_date().toString(), "UTF-8") +  
																															   "&endDate=" +URLEncoder.encode(user.getEnd_date().toString(), "UTF-8") + 
																															   "&merchantId=" + vecTemp.get(0) + "&user=YES','merchantHistory','width=1400,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');\">" + NumberUtil.formatCurrency(vecTemp.get(2).toString())  + "</a></td></tr>" %>
			
</td>

     
                               </tr>                              
                              <%
                               if (intEvenOdd == 1)
                               {
                                 intEvenOdd = 2;
                               }
                               else
                               {
                                 intEvenOdd = 1;
                               }
                               
                               }
                           }
                           
                           %>                  
<tfoot>        
                            <tr class=row<%=intEvenOdd%>>
            <td colspan=2 align=right><%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
            <td align=right><%=qtSum%></td>
            <td align=right><%=NumberUtil.formatCurrency(Double.toString(paymentsSum))%></tr>
               </tfoot>            
                        </table>
</td>
</tr>  
                       </table>
                       
<SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None","CaseInsensitiveString","Number", "Number"],0,false,false);
  -->
          </SCRIPT> 
                        
<%}%>
 
</td>
</tr>  
</table>   

          
                         
<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>