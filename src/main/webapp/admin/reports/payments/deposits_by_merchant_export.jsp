<%@ page import="java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.customers.UserPayments"%>
<%
	int section = 4;
	int section_page = 34;
%>
<jsp:useBean id="SessionData" class = "com.debisys.users.SessionData"	scope="session" />
<jsp:useBean id="User" class="com.debisys.customers.UserPayments" scope="request" />
<jsp:useBean id= "TransactionReport"	class="com.debisys.reports.TransactionReport" scope="request" />
<%@ include file="/includes/security.jsp"%>

<%
String strRuta = "";
Vector vecSearchResults = new Vector();
UserPayments user = new UserPayments();
user.setStrRepIds(request.getParameter("merchantIds"));
user.setStart_date(request.getParameter("startDate"));
user.setEnd_date(request.getParameter("endDate"));

vecSearchResults = user.getMerchantsPaymentsDownload(SessionData);

strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,4,SessionData);
if (strRuta.length() > 0)
{
	response.sendRedirect(strRuta);
}
%>
