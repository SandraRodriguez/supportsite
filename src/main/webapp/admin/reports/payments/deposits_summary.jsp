<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.UserPayments,
                 com.debisys.reports.ReportsUtil,
                 com.debisys.utils.ColumnReport,
                 com.debisys.utils.NumberUtil, com.debisys.schedulereports.ScheduleReport"
                  %>
<%@page import="com.debisys.utils.DateUtil"%>
<%@page import="com.debisys.users.User"%>
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 4;
  int section_page = 34;
  
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:useBean id="User" class="com.debisys.customers.UserPayments" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<%
 boolean bExternalRep=com.debisys.users.User.isExternalRepAllowedEnabled(SessionData);
 Hashtable searchErrors = null;
 Vector    vecSearchResults = new Vector();
 String entityType="";
 String strMerchantIds[] = null;
 String extraFilter = User.getPermissionFaceyJAMGrpPaymentDetailsReport(SessionData);
 
 String warnningDeposit = Languages.getString("jsp.admin.reports.payments.depositsByMerchant.warning",SessionData.getLanguage());
 ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
 ArrayList<String> titles = new ArrayList<String>();
 String noteTimeZone = "";
 String testTrx = "";  
 String keyLanguage = "jsp.admin.reports.payments.deposits.deposits_summary";
 String titleReport = titleReport = Languages.getString( keyLanguage , SessionData.getLanguage() );;
 
 Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
 String subTitle = "";
 noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
 String entityNametextSelected = "";
    
if (request.getParameter("search") != null)
{
	if ( request.getParameter("midSelected") != null && request.getParameter("midSelected").length()>0 )
	 	entityNametextSelected = request.getParameter("midSelected");
	else
		entityNametextSelected = request.getParameter("textSelected");
		
	if (TransactionReport.validateDateRange(SessionData))
	{
		SessionData.setProperty("start_date", request.getParameter("startDate"));
		SessionData.setProperty("end_date", request.getParameter("endDate"));
		User.setStart_date(request.getParameter("startDate"));
		User.setEnd_date(request.getParameter("endDate"));
		subTitle = titleReport+" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(User.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(User.getEndDateFormatted());
				
		String itemSelected = null;			
		if(!extraFilter.equals("1"))
		{
			entityType = request.getParameter("entities");
			strMerchantIds = null;
			//vecSearchResults = User.getPayments(SessionData,application, entityType, null, null, DebisysConstants.EXECUTE_REPORT, headers, titles);
		}
		else if(extraFilter.equals("1"))
		{
			strMerchantIds = request.getParameterValues("mids");
		    if (strMerchantIds != null)
		    {
		      	if(strMerchantIds[0] != "")
		      	{
		        	TransactionReport.setMerchantIds(strMerchantIds);
		        }
		        else
		        {
		        	strMerchantIds = null;
		      	}
		    }		      
		    entityType = request.getParameter("entitySelected");
		    itemSelected = request.getParameter("itemSelected");
		    if(itemSelected != null && (itemSelected.equals("0")))
		    {
		     	itemSelected = "-1";
		    }
		    if(itemSelected != null && (itemSelected.equals("1")))
		    {
		      	itemSelected = "0";
		    }
		    	     	
	      }
	      //////////////////////////////////////////////////////////////////
		  //HERE WE DEFINE THE REPORT'S HEADERS 
		  headers = getHeadersPaymentDetailReport( SessionData, entityType, strDistChainType, extraFilter, bExternalRep );
		  //////////////////////////////////////////////////////////////////
		  //////////////////////////////////////////////////////////////////
	      
	      if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
		  {
				//TO SCHEDULE REPORT
				titles.add(noteTimeZone);
				titles.add(warnningDeposit);				
				titles.add(titleReport);		
		        titles.add(testTrx);
				User.getPayments(SessionData,application,entityType, itemSelected, strMerchantIds, DebisysConstants.SCHEDULE_REPORT, headers, titles);
				ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
				if (  scheduleReport != null  )
				{
					scheduleReport.setStartDateFixedQuery( User.getStart_date() );
					scheduleReport.setEndDateFixedQuery( User.getEnd_date() );
					scheduleReport.setAdditionalData(entityNametextSelected);
					scheduleReport.setTitleName( keyLanguage );   
				}	
				response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS  );
		  }
		  else
		  {
		   	vecSearchResults = User.getPayments(SessionData, application, entityType, itemSelected, strMerchantIds, DebisysConstants.EXECUTE_REPORT, headers, titles);
		  } 

	}
	else
	{
		searchErrors = TransactionReport.getErrors();
	}
	
	//String strDistChainType = sessionData.getProperty("dist_chain_type");
}
%>

<%!public static ArrayList<ColumnReport> getHeadersPaymentDetailReport( SessionData sessionData, String entityType, String strDistChainType, 
																		String extraFilter,
																		boolean bExternalRep  )
	{
		 //m.merchant_id, m.dba, mc.logon_id, ISNULL(mc.description, 'Not Available') description, mc.payment, mc.balance, mc.prior_available_credit,
		 //mc.commission, mc.PaymentOrigin, dbo.GetLocalTimeByAccessLevel(1, 4042379240316, mc.Deposit_date, 1) AS Deposit_date, 
		 //dbo.GetLocalTimeByAccessLevel(1, 4042379240316, mc.log_datetime, 1) AS log_datetime, mc.ExternalDBA, 
		 //r.businessname AS repName, s.businessname AS subAgentName, a.businessname AS agentName 

		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();				          
        if(entityType.equalsIgnoreCase("Merchant"))
        {
          headers.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.merchant_id", sessionData.getLanguage()).toUpperCase(), String.class, false));	
          if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1"))
          {
             headers.add(new ColumnReport("agentName", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent", sessionData.getLanguage()).toUpperCase(), String.class, false));
             headers.add(new ColumnReport("subAgentName", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent", sessionData.getLanguage()).toUpperCase(), String.class, false));
             headers.add(new ColumnReport("repName", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep", sessionData.getLanguage()).toUpperCase(), String.class, false));
             
          }
          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1"))
          {
          	headers.add(new ColumnReport("rep_id", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep", sessionData.getLanguage()).toUpperCase(), String.class, false));           	
          }
          
          headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.dba", sessionData.getLanguage()).toUpperCase(), String.class, false));
          headers.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.logon_id", sessionData.getLanguage()).toUpperCase(), String.class, false));
          headers.add(new ColumnReport("description", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.description", sessionData.getLanguage()).toUpperCase(), String.class, false));
          headers.add(new ColumnReport("payment", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.payment", sessionData.getLanguage()).toUpperCase(), Double.class, true));
          headers.add(new ColumnReport("balance", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.balance", sessionData.getLanguage()).toUpperCase(), Double.class, false));
          headers.add(new ColumnReport("prior_available_credit", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.prior_available_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
          headers.add(new ColumnReport("Deposit_date", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.Deposit_date", sessionData.getLanguage()).toUpperCase(), String.class, false));
          headers.add(new ColumnReport("log_datetime", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.log_datetime", sessionData.getLanguage()).toUpperCase(), String.class, false));
          
          if(bExternalRep)
          {
          	headers.add(new ColumnReport("ExternalDBA", Languages.getString("jsp.admin.reports.extern.RepresentativeType", sessionData.getLanguage()).toUpperCase(), String.class, false));            	
          }
        }
        else
        {
        	String entityId = "ENTITY " + Languages.getString("jsp.admin.reports.payments.deposits.entityid",sessionData.getLanguage()).toUpperCase();
        	headers.add( new ColumnReport( "rep_id", entityId , String.class, false) );
        	//, rc., rc., 
			//dbo.GetLocalTimeByAccessLevel(1, 3866331800940, rc.datetime, 1) as datetime    
        	
        	
         	boolean wasValidated = false;
          	if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1"))
          	{
			    if (entityType.equalsIgnoreCase("SubAgent"))
			    {
			    	wasValidated = true;
			    	entityId = entityId.replace("ENTITY", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",sessionData.getLanguage()).toUpperCase());
			    	headers.add(new ColumnReport("businessname", entityId, String.class, false));
			    	headers.add(new ColumnReport("agentName", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",sessionData.getLanguage()).toUpperCase(), String.class, false));
			    }
			    else if (entityType.equalsIgnoreCase("Rep"))
			    {
			    	wasValidated = true;
			    	
			    	entityId = entityId.replace("ENTITY", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",sessionData.getLanguage()).toUpperCase());
			    	//headers.add(new ColumnReport("", entityId, String.class, false));
			    	//headers.add(new ColumnReport("", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",sessionData.getLanguage()).toUpperCase(), String.class, false));
			     	//headers.add(new ColumnReport("", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",sessionData.getLanguage()).toUpperCase(), String.class, false));
			     	
			    }
           }
           if(!wasValidated)
           {
           		entityId = entityId.replace("ENTITY", "");
    			//out.println(entityId);
           }
           headers.add(new ColumnReport("businessname", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.dba",sessionData.getLanguage()).toUpperCase(), String.class, false));
           headers.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.logon_id",sessionData.getLanguage()).toUpperCase(), String.class, false));
           headers.add(new ColumnReport("description", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.description",sessionData.getLanguage()).toUpperCase(), String.class, false));
           headers.add(new ColumnReport("amount", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.payment",sessionData.getLanguage()).toUpperCase(), Double.class, true));
           headers.add(new ColumnReport("prior_available_credit", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.prior_available_credit",sessionData.getLanguage()).toUpperCase(), Double.class, false));
           headers.add(new ColumnReport("datetime", Languages.getString("jsp.admin.reports.payments.deposits.datetime",sessionData.getLanguage()).toUpperCase(), String.class, false));
           
       }
       return headers;
	}  
  %>
  
<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%= Languages.getString("jsp.admin.reports.title45",SessionData.getLanguage()).toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
    	<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
if (searchErrors != null)
{
    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
    Enumeration enum1=searchErrors.keys();
    while(enum1.hasMoreElements())
    {
      String strKey = enum1.nextElement().toString();
      String strError = (String) searchErrors.get(strKey);
      out.println("<li>" + strError);
    }
    out.println("</font></td></tr></table>");
}
else
{
%>
<% if (vecSearchResults.size() <= 0) {%>

    <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
    <tr>
        <td>
             <%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.not_found",SessionData.getLanguage())%>
        </td>
    </tr> 
    </table>
<%}
else{
	Iterator itTemp = vecSearchResults.iterator();
	double dblTotalPayment = 0;	
	if(itTemp.hasNext()) {
	    Vector vecTemp = null;
	    while (itTemp.hasNext())                    
	    {                                       
	        vecTemp = null;              
	        vecTemp = (Vector)itTemp.next(); 
	        dblTotalPayment += Double.parseDouble(vecTemp.get(4).toString());  
		}
	}

%>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr><td class="main"><%=noteTimeZone%></td></tr>
		<tr><td class="main"><%=subTitle%> - <%=entityNametextSelected%></td></tr>
		<tr><td class="main"><font color=ff0000><%=warnningDeposit%></font></td></tr>		
	</table>
	<table> 
		<tr>
			<td class="main"><%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.totalPayments",SessionData.getLanguage())%>: <%=NumberUtil.formatCurrency(Double.toString(dblTotalPayment))%></td>
		</tr>         
	  <tr>
	    <td>
	    <FORM ACTION="admin/reports/payments/deposits_export.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
	    	<input type="hidden" name="entitySelected" id="entitySelected" VALUE="<%=request.getParameter("entitySelected")%>" >
	  		<input type="hidden" name="itemSelected" id="itemSelected" VALUE="<%=request.getParameter("itemSelected")%>" >
	  		
	  		<%
	  			if(strMerchantIds != null){
		      		for(int i=0; i < strMerchantIds.length; i++){%>						
						<input type="hidden" name="mids" VALUE="<%=strMerchantIds[i]%>" >	      
		      		<%}
		      	}
	  		 %>
	    
	    	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
	    	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
	    	<INPUT TYPE=hidden NAME=entityType VALUE="<%=entityType%>">
	    	<INPUT TYPE=hidden NAME=merchantIds VALUE="<%=User.getStrRepIds()%>">
	    	<INPUT TYPE=hidden NAME=Download VALUE="Y">
	    	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.download",SessionData.getLanguage())%>">
	    </FORM>
	    </td>
	    <td>&nbsp;&nbsp;&nbsp;</td>
	    <td>
	    </td>
	  </tr>
	</table>
		
		<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
          <thead>
              <tr class="SectionTopBorder"> 
                  <% 
                  out.println("<td class=rowhead2>#</td>");
                  
                 if(entityType.equalsIgnoreCase("Merchant"))
                 {
	                 out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.merchant_id",SessionData.getLanguage()).toUpperCase() + "</td>");
	                 if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
	                   		out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   		out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   		out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   }
	                   else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
	                   		out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   }
	                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.dba",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.logon_id",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.description",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.balance",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.prior_available_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.Deposit_date",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.log_datetime",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   if(bExternalRep) {
	                   		out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.extern.RepresentativeType",SessionData.getLanguage()).toUpperCase() + "</td>");
	                   }
                 }
                 else
                 {
                 	String entityId =   "<td class=rowhead2 nowrap> ENTITY " + Languages.getString("jsp.admin.reports.payments.deposits.entityid",SessionData.getLanguage()).toUpperCase() + "</td>";
                  	boolean wasValidated = false;
                  	if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
				        if (entityType.equalsIgnoreCase("SubAgent")){
				        	wasValidated = true;
				        	entityId = entityId.replace("ENTITY", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",SessionData.getLanguage()).toUpperCase());
				        	out.println(entityId);
					        out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",SessionData.getLanguage()).toUpperCase() + "</td>");
				        }
				        else if (entityType.equalsIgnoreCase("Rep")){
				        	wasValidated = true;
				        	entityId = entityId.replace("ENTITY", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",SessionData.getLanguage()).toUpperCase());
				        	out.println(entityId);
					        out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",SessionData.getLanguage()).toUpperCase() + "</td>");
					        out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",SessionData.getLanguage()).toUpperCase() + "</td>");
				        }
                   }
                   if(!wasValidated){
                   		entityId = entityId.replace("ENTITY", "");
				        out.println(entityId);
                   }
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.deposits.dba",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.logon_id",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.description",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                  out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.depositsByMerchant.prior_available_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
                   out.println("<td class=rowhead2 nowrap>" + Languages.getString("jsp.admin.reports.payments.deposits.datetime",SessionData.getLanguage()).toUpperCase() + "</td>");
                   
                 }
                  %>
              </tr>
          </thead>
<%//Iteration over results
    int intEvenOdd = 1;
    int intCounter = 1; 
      
	Iterator it = vecSearchResults.iterator();

	if(it.hasNext()) {
	    Vector vecTemp = null;
	    while (it.hasNext())                    
	    {                                       
	        vecTemp = null;              
	        vecTemp = (Vector)it.next(); 
	         String depositDate="";
	         if(entityType.equalsIgnoreCase("Merchant")){
	       		  depositDate = (vecTemp.get(7) != null)? vecTemp.get(7).toString():"N/A";    
	       	 }  
	        out.println("<tr class=row" + intEvenOdd +">" +
	     		   "<td>" + intCounter+ "</td>" +
	                "<td nowrap>" + vecTemp.get(0)+ "</td>");
	                
	                if(entityType.equalsIgnoreCase("Merchant") && !strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
	                	out.println("<td align=right>" + ((bExternalRep)?vecTemp.get(12):vecTemp.get(11)) + "</td>");
	                	out.println("<td align=right>" + ((bExternalRep)?vecTemp.get(11):vecTemp.get(10)) + "</td>");
	                	out.println("<td align=right>" + ((bExternalRep)?vecTemp.get(10):vecTemp.get(9)) + "</td>");
	                }
	                else if(entityType.equalsIgnoreCase("Merchant") && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
	                	out.println("<td align=right>" + ((bExternalRep)?vecTemp.get(10):vecTemp.get(9)) + "</td>");
	                }
	                
	                else{
	                	if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && entityType.equalsIgnoreCase("Rep") && extraFilter.equals("1")){
					        out.println("<td align=right>" + vecTemp.get(8) + "</td>");
					        out.println("<td align=right>" + vecTemp.get(7) + "</td>");
				        }
				        else if(entityType.equalsIgnoreCase("SubAgent") && extraFilter.equals("1")){
				        	out.println("<td align=right>" + vecTemp.get(7) + "</td>");
				        }
	                }
	                
	                
	                out.println("<td>" + vecTemp.get(1) + "</td>" +
	                "<td>" + vecTemp.get(2) + "</td>" +
	                "<td>" + vecTemp.get(3) + "</td>" +
	                "<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(4).toString()) + "</td>" );
	                if(entityType.equalsIgnoreCase("Merchant")){
	                  out.println( "<td>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
	                 "<td>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>"+
	                  "<td align=right>" + depositDate + "</td>" +
	                  "<td align=right>" + vecTemp.get(8) + "</td>");
	                  if(bExternalRep) {
	                  	out.println("<td align=right>" + vecTemp.get(9) + "</td>");
	                  }
	                }else{
	                 out.println( "<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
	                 "<td align=right>" + vecTemp.get(6) + "</td>");
	                 
	                 
	                }
	                
	         out.println("</tr>");
		    if (intEvenOdd == 1)
		    {
		      intEvenOdd = 2;
		    }
		    else
		    {
		      intEvenOdd = 1;
		    }
		    intCounter++;	    
	    }	    
	}
%>
		</table>
<SCRIPT type="text/javascript">
<!--
<%if(entityType.equalsIgnoreCase("Merchant")){ %>
  
<%
	if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){%>
		var stT1 = new SortROC(document.getElementById("t1"), ["None","Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","Number", "Number","Number","DateTime","DateTime"],0,false,false);
	<%}
	else if(entityType.equalsIgnoreCase("Merchant") && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){%>
		var stT1 = new SortROC(document.getElementById("t1"), ["None","Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","Number", "Number","Number","DateTime","DateTime"],0,false,false);
	
	<%}
	else{%>
		var stT1 = new SortROC(document.getElementById("t1"), ["None","Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","Number", "Number","Number","DateTime","DateTime"],0,false,false);
	<%}
	
}
else{
	if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
		if (entityType.equalsIgnoreCase("SubAgent")){ %>
			var stT1 = new SortROC(document.getElementById("t1"), ["None","Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number","Number","DateTime"],0,false,false);
		<%}
		else if (entityType.equalsIgnoreCase("Rep")){%>
			var stT1 = new SortROC(document.getElementById("t1"), ["None","Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number","Number","DateTime"],0,false,false);
		<%}
		else{%>
		var stT1 = new SortROC(document.getElementById("t1"), ["None","Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","Number","Number","DateTime"],0,false,false);
	
	<%}
		
	}
	else{%>
		var stT1 = new SortROC(document.getElementById("t1"), ["None","Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","Number","Number","DateTime"],0,false,false);
	
	<%}
}
%>


-->
</SCRIPT> 

<%}%>
			
  		</td>
</table>  
<%} %>
<%@ include file="/includes/footer.jsp" %>
