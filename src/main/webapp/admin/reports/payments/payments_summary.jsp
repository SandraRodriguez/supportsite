<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" %>
<%
int section=4;
int section_page=10;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="PaymentReport" class="com.debisys.reports.PaymentReport" scope="request"/>
<jsp:setProperty name="PaymentReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
  Vector vecSearchResults = new Vector();
  Hashtable searchErrors = null;
  String strMerchantIds[] = null;
  String strSalesIds[]= null;
  
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();
  String titleReport = Languages.getString("jsp.admin.reports.title6",SessionData.getLanguage());
  String titleReportSchedule = Languages.getString("jsp.admin.reports.title6",SessionData.getLanguage());
  
if (request.getParameter("search") != null)
{
  if (PaymentReport.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    strMerchantIds = request.getParameterValues("mids");
    strSalesIds= request.getParameterValues("salesIds");
    if (strMerchantIds != null)
    {
      PaymentReport.setMerchantIds(strMerchantIds);
    }
    if (strSalesIds != null)
    {
      PaymentReport.setSalesIds(strSalesIds);
    }
    if (!PaymentReport.getStartDate().equals("") && !PaymentReport.getEndDate().equals(""))
	{
	   titleReport +=  Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( PaymentReport.getStartDateFormatted() );
	   titleReport +=  Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+HTMLEncoder.encode( PaymentReport.getEndDateFormatted() );
	}
	
	//////////////////////////////////////////////////////////////////
	//HERE WE DEFINE THE REPORT'S HEADERS 
	headers = com.debisys.reports.ReportsUtil.getHeadersCreditOrPaymentSummaryByMerchant( SessionData,application );
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	  
	if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	{
		 //TO SCHEDULE REPORT
		 titles.add(titleReportSchedule);		
         
	     PaymentReport.getPaymentSummary( SessionData , application, DebisysConstants.SCHEDULE_REPORT, headers, titles );
	     
	     ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
		 if (  scheduleReport != null  )
		 {
		 	scheduleReport.setStartDateFixedQuery( PaymentReport.getStartDate() );
			scheduleReport.setEndDateFixedQuery( PaymentReport.getEndDate() );
			//scheduleReport.setAdditionalData("");
			scheduleReport.setTitleName( "jsp.admin.reports.title6" );   
		 }	
	     response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
    }  
    else if ( request.getParameter("downloadReport") != null )
	{
	  	 //TO DOWNLOAD ZIP REPORT		 
		 titles.add(titleReport);
	     vecSearchResults = PaymentReport.getPaymentSummary( SessionData , application, DebisysConstants.DOWNLOAD_REPORT, headers, titles );
	     response.sendRedirect( PaymentReport.getStrUrlLocation() );	
	}
	else
	{
		vecSearchResults = PaymentReport.getPaymentSummary( SessionData , application, DebisysConstants.EXECUTE_REPORT, headers, titles );
    }
     
  }
  else
  {
   searchErrors = PaymentReport.getErrors();
  }


}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td width="2000" class="formAreaTitle">&nbsp;<%= Languages.getString("jsp.admin.reports.title6",SessionData.getLanguage()).toUpperCase() %></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
</tr>
</table>
               </td>
              </tr>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
			<form name="downloadData" method=post action="admin/reports/payments/payments_summary.jsp">
              <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
              <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
              <input type="hidden" name="search" value="y">
              <input type="hidden" name="downloadReport" value="y">
            	<%for(int i=0;i<strMerchantIds.length;i++){%>
				<input type="hidden" name="mids" value="<%=strMerchantIds[i]%>">
				<%}%>
             <%if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
			 {%>
              <%for(int i=0;i<strSalesIds.length;i++){%>
				<input type="hidden" name="salesIds" value="<%=strSalesIds[i]%>">
				<%}%>
               <%}%>
              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		    </form>
		    
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=Languages.getString("jsp.admin.reports.title6",SessionData.getLanguage())%><%
              if (!PaymentReport.getStartDate().equals("") && !PaymentReport.getEndDate().equals(""))
                {
                   out.println(" "  + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(PaymentReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(PaymentReport.getEndDateFormatted()));
                }
            %></td></tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
            <%
                  String strSortURL="";
             if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)){
                strSortURL= "admin/reports/payments/payments_summary.jsp?search=y&startDate=" + PaymentReport.getStartDate() + "&endDate=" + PaymentReport.getEndDate() + "&mids=" + PaymentReport.getMerchantIds() + "&salesIds=" + PaymentReport.getSalesIds();
             }else{
              strSortURL = "admin/reports/payments/payments_summary.jsp?search=y&startDate=" + PaymentReport.getStartDate() + "&endDate=" + PaymentReport.getEndDate() + "&mids=" + PaymentReport.getMerchantIds() ;
                
             }
            %>
           	  <td class=rowhead2 valign=bottom>#</td>
           	  <%
              	for( ColumnReport header : headers )
		    	{	
		    	%>
		    		<td class=rowhead2 valign=bottom><%=header.getLanguageDescription().toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=1&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=1&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
              
		    	<%
		    	} 		   
	 		  %>              		 		              
            </tr>
            <%
                  double dblTotalPaymentsSum = 0;
                  int intTotalQtySum = 0;

                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  int intCounter = 1;
                  if (it.hasNext())
                  {
                      Vector vecTemp = null;
                      vecTemp = (Vector) it.next();
                      dblTotalPaymentsSum = Double.parseDouble(vecTemp.get(1).toString());
                      intTotalQtySum = Integer.parseInt(vecTemp.get(0).toString());

                      while (it.hasNext())
                      {
                        vecTemp = null;
                        vecTemp = (Vector) it.next();
                        int intTotalQty = Integer.parseInt(vecTemp.get(2).toString());
                        double dblTotalSales = Double.parseDouble(vecTemp.get(3).toString());

                        out.println("<tr class=row" + intEvenOdd +">" +
                                    "<td>" + intCounter++ + "</td>" +
                                    "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                    "<td>" + vecTemp.get(1) + "</td>");
                         if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
			 {
					    	out.println("<td>" + vecTemp.get(5) + "</td>");
                                                out.println("<td>" + vecTemp.get(6) + "</td>");
                                                out.println("<td>" + vecTemp.get(7) + "</td>");
                        }     
                        if (!DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					    {
					    	out.println("<td>" + vecTemp.get(4) + "</td>");
                        }            
                         out.println("<td>" + intTotalQty + "</td>" +
                                    "<td align=right><a href=\"javascript:void(0)\" onClick=\"window.open('/support/admin/customers/merchants_credit_history.jsp?startDate=" + URLEncoder.encode(PaymentReport.getStartDate(), "UTF-8") +  "&endDate=" +URLEncoder.encode(PaymentReport.getEndDate(), "UTF-8") + "&merchantId=" + vecTemp.get(1) + "','merchantHistory','width=1400,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');\">" + NumberUtil.formatCurrency(Double.toString(dblTotalSales))  + "</a></td></tr>");

                        if (intEvenOdd == 1)
                        {
                          intEvenOdd = 2;
                        }
                        else
                        {
                          intEvenOdd = 1;
                        }

                      }
                  }
                  vecSearchResults.clear();
            %>
            <tr class=row<%=intEvenOdd%>>
<%
               if (!DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                {
                  if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
%>
                <td colspan=7 align=right>
<%
                }else {%>

		      <td colspan=4 align=right>
 <%              } } else
                {
		if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
%>
               <td colspan=6 align=right>
<%
                } else {%>
 		<td colspan=3 align=right>

            <%    }} %>   
            <%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
            <td align=left><%=intTotalQtySum%></td>
            <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalPaymentsSum))%></tr></table></td>
<%

}
else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=000000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>