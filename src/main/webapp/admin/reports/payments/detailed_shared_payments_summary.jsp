<%@ page import="com.debisys.languages.Languages,
				 com.debisys.customers.Merchant,
				 com.debisys.reports.TransactionReport,
				 com.debisys.utils.StringUtil,
				 com.debisys.utils.DateUtil,
				 java.util.*,
				 java.util.Enumeration" %>
<%
int section = 4;
int section_page = 50;
%> 
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>

<%@ include file="/includes/security.jsp" %>

<%
  int intPage = 1;
  int intPageSize = 50;
  int intPageCount = 1;
  int intRecordCount = 0;
  Vector vResults = new Vector();
  Hashtable searchErrors = null;

  try
  {
      if ( request.getParameter("download") != null )
      {
        String sURL = "";
        TransactionReport.setRepIds(request.getParameterValues("rids"));
        TransactionReport.setSubAgentsIds(request.getParameter("subagents"));
        TransactionReport.setAgentsIds(request.getParameter("agents"));
        sURL = TransactionReport.downloadSharedDetailedPayment(application, SessionData);
        response.sendRedirect(sURL);
        return;
      }

      if (request.getParameter("page") != null)
      {
        try
        {
            intPage=Integer.parseInt(request.getParameter("page"));
        }
        catch(NumberFormatException ex)
        {
            intPage = 1;
        }
      }

      if (intPage < 1)
      {
        intPage = 1;
      }

    if (TransactionReport.validateDateRange(SessionData) || 
    	TransactionReport.validateRepId( request.getParameterValues("rids"), SessionData  ) )
    {
	    
	    
		    SessionData.setProperty("start_date", request.getParameter("startDate"));
		    SessionData.setProperty("end_date", request.getParameter("endDate"));
		    TransactionReport.setRepIds(request.getParameterValues("rids"));
		    
		 // if (TransactionReport.validateRepId( request.getParameterValues("rids")  ))
	      //{
		    TransactionReport.setSubAgentsIds(request.getParameter("subagents"));
		    TransactionReport.setAgentsIds(request.getParameter("agents"));
		    vResults = TransactionReport.getSharedDetailedPayment(intPage, intPageSize, SessionData);
		    intRecordCount = Integer.parseInt(vResults.get(0).toString());
		    vResults.removeElementAt(0);
		    if (intRecordCount>0)
		    {
		      intPageCount = (intRecordCount / intPageSize) + 1;
		      if ((intPageCount * intPageSize) + 1 >= intRecordCount)
		      {
		        intPageCount++;
		      }
		    }
		/*}
		else
		  {
		   searchErrors = TransactionReport.getErrors();
		  }
      }*/
      }else
	  {
	   searchErrors = TransactionReport.getErrors();
	  }
	}
	catch (Exception e)
	{
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b>ERROR IN PAGE</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class=main><%=Languages.getString("jsp.admin.reports.payments.detailedrep_payments.checklog",SessionData.getLanguage()) + e.toString()%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}
%>
<%@ include file="/includes/header.jsp" %>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script src="includes/sortROC.js" type="text/javascript"></script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.reports.payments.detailedrep_payments.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
<%
	if ( vResults.size() > 0 )
	{//If there are results
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class=main><%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%></td></tr>
        <tr>
          <td colspan=2>
        <table>
          <tr>
            <td>
            <FORM ACTION="admin/reports/payments/detailed_shared_payments_summary.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
              <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
              <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
              <INPUT TYPE=hidden NAME=mids VALUE="<%=TransactionReport.getMerchantIds()%>">
              <INPUT TYPE=hidden NAME=reps VALUE="<%=request.getParameter("reps")%>">
              <INPUT TYPE=hidden NAME=subagents VALUE="<%=request.getParameter("subagents")%>">
              <INPUT TYPE=hidden NAME=agents VALUE="<%=request.getParameter("agents")%>">
              <INPUT TYPE=hidden NAME=download VALUE="Y">
              <INPUT TYPE=hidden NAME=rids VALUE="<%=TransactionReport.getRepIds()%>">
              <INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>" >
            </FORM>
            </td>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td>
            </td>
          </tr>
        </table>
          </td>
        </tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
							String baseURL = "admin/reports/payments/detailed_shared_payments_summary.jsp?search=" 
							+ "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")
							+ "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")
							+ "&mids=" + URLEncoder.encode(TransactionReport.getMerchantIds(), "UTF-8")
              + "&reps=" + URLEncoder.encode(TransactionReport.getRepIds(), "UTF-8")
              + "&rids=" + URLEncoder.encode(TransactionReport.getRepIds(), "UTF-8")
              + "&subagents=" + URLEncoder.encode(TransactionReport.getSubAgentsIds(), "UTF-8")
              + "&agents=" + URLEncoder.encode(TransactionReport.getAgentsIds(), "UTF-8");

              if (intPage > 1)
              {
                out.println("<a href=\"" + baseURL + "&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"" + baseURL + "&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }

              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"" + baseURL + "&page=" + i + "\">" +i+ "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"" + baseURL + "&page=" + (intPage+1) + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"" + baseURL + "&page=" + (intPageCount-1) + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
              }

              %>
              </td>
            </tr>
			</table>
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class=rowhead2></td>
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.user",SessionData.getLanguage()))%></td>
<%
          if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
          {
%>																											
            <td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.agent",SessionData.getLanguage()).toUpperCase())%></td>
            <td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.subagent",SessionData.getLanguage()).toUpperCase())%></td>
            <td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.rep",SessionData.getLanguage()).toUpperCase())%></td>
<%
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
%>
            <td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.subagent",SessionData.getLanguage()).toUpperCase())%></td>
            <td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.rep",SessionData.getLanguage()).toUpperCase())%></td>
<%
          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)
              || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))
          {
%>
            <td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.rep",SessionData.getLanguage()).toUpperCase())%></td>
<%
          }
%>

						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.sales_date",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.payment_date",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedshared_rep_payments.description",SessionData.getLanguage()).toUpperCase())%></td>
						
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.transactions.pinreturn_request.merchant_dba",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.analisys.dashboard.quantity",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.product_name",SessionData.getLanguage()).toUpperCase())%></td>
						
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedshared_rep_payments.amount",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedshared_rep_payments.available_credit",SessionData.getLanguage()).toUpperCase())%></td>
						
					</tr>
				</thead>
<%
		int nRow = 1;
		for ( int i = 0; i < vResults.size(); i++ )
		{
			Vector vTemp = (Vector)vResults.get(i);
			int nPos = 0;
%>
				<tr class="row<%=nRow%>">
					<td><%=(i + 1)%></td>
          <td align=center><%=vTemp.get(nPos++).toString()%></td>
<%
          if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
          {
%>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
<%
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
%>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
<%
          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)
              || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))
          {
%>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
<%
          }
%>
			<td align=left><%=vTemp.get(nPos++).toString()%></td>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
            <td align=right><%=vTemp.get(nPos++).toString()%></td>
            <td align=right><%=vTemp.get(nPos++).toString()%></td>
            <td align=left><%=vTemp.get(nPos++).toString()%></td>
            <td align=right><%=vTemp.get(nPos++).toString()%></td>
            <td align=right><%=vTemp.get(nPos++).toString()%></td>

				</tr>
<%
			nRow = (nRow == 1)?2:1;
		}//End of for
%>
			</table>
			<script type="text/javascript">
<%
          String sActors = "";
          if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
          {
            sActors = "\"CaseInsensitiveString\", \"CaseInsensitiveString\", \"CaseInsensitiveString\", ";
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
            sActors = "\"CaseInsensitiveString\", \"CaseInsensitiveString\", ";
          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)
              || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))
          {
            sActors = "\"CaseInsensitiveString\", ";
          }
%>
				var stT1 = new SortROC(document.getElementById("t1"), ["None", "CaseInsensitiveString", <%=sActors%>"CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Date", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "Number"], 0, false, false);
			</script>
		</td>
<%
	}//End of if there are results
	else
	{//Else show the message of no results
%>
    <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
		if (searchErrors != null)
		{
		  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		  Enumeration enum1=searchErrors.keys();
		  while(enum1.hasMoreElements())
		  {
		    String strKey = enum1.nextElement().toString();
		    String strError = (String) searchErrors.get(strKey);
		    out.println("<li>" + strError + "</li>");
		  }
		
		  out.println("</font></td></tr></table>");
		}
		else
		{
%>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class=main><%=Languages.getString("jsp.admin.reports.pinreturn_search.noresults",SessionData.getLanguage())%></td></tr>
			</table>
<%
    }
%>
    </td>
<%
	}//End of else show the message of no results
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan=3 align=center>
			<form method="post" action="admin/reports/payments/detailed_shared_payments.jsp">
				<input type=submit class="plain" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.backtosearch",SessionData.getLanguage())%>">
			</form>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>