<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,com.debisys.tools.CreditReportTaskHandler,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 4;
  int section_page = 22;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Taskhandler" class="com.debisys.tools.CreditReportTaskHandler" scope="request"></jsp:useBean>
<jsp:setProperty name="Taskhandler" property="*"/>
  <%@ include file="/includes/security.jsp" %>
<%
String agentsID="";
 	 String subsID="";
  	String entity="";
  	String jobstartDate="";
  	String email="";
  	int deleteresponse = -1;
  	boolean deletefailed = false;
  	String strRefId = SessionData.getProperty("ref_id");
  	String activity="";
  	String jobid="";
  	int dbvalue =0;
  	Hashtable searchErrors = null;
  if ( request.getParameter("activity") != null )
{
activity = request.getParameter("activity");
}
if(activity.equals("save")){
  	
		agentsID=request.getParameter("agentsID");
  		subsID=request.getParameter("subsID");
  		entity=request.getParameter("entity");
  		jobstartDate=request.getParameter("jobstartDate");
  		email=request.getParameter("email");
        String insertresult  = Taskhandler.inserttask(strRefId,agentsID,subsID,entity,jobstartDate,email,0,strAccessLevel);
        if(insertresult=="-4"){
        Taskhandler.addFieldError("insert task", Languages.getString("jsp.admin.tools.s2k.error12",SessionData.getLanguage()));
		    	searchErrors = Taskhandler.getErrors();
        }
        else if(insertresult=="-1"){
        	Taskhandler.addFieldError("insert task", Languages.getString("jsp.admin.tools.s2k.error13",SessionData.getLanguage()));
		    	searchErrors = Taskhandler.getErrors();
        }
        
      }
      
else if (activity.equals("edit"))
{
		agentsID=request.getParameter("agentsID");
  		subsID=request.getParameter("subsID");
  		entity=request.getParameter("entity");
  		jobstartDate=request.getParameter("jobstartDate");
  		email=request.getParameter("email");
  		jobid=request.getParameter("jobid");
        int resp  = Taskhandler.edittask(strRefId,agentsID,subsID,entity,jobstartDate,email,0,strAccessLevel,jobid);
        if(resp == 0){
		
		}
		else if(resp == 2){
			Taskhandler.clearvalidation();
			Taskhandler.addFieldError("edit task", Languages.getString("jsp.admin.tools.s2k.error9",SessionData.getLanguage()));
	    	searchErrors = Taskhandler.getErrors();
	    	
		}
		else if(resp == -4){
			Taskhandler.clearvalidation();
			Taskhandler.addFieldError("edit task", Languages.getString("jsp.admin.tools.s2k.error11",SessionData.getLanguage()));
	    	searchErrors = Taskhandler.getErrors();    	
	    	
		}
}
      if(request.getParameter("type") != null)
	{
		if(request.getParameter("type").equals("DELETE"))
		{
		 deleteresponse = new CreditReportTaskHandler().deletetask(request.getParameter("delId"));	
	 	}
	 
    }
     	Vector vecSearchResults = new CreditReportTaskHandler().getCreditJoblist(SessionData.getProperty("ref_id"),strAccessLevel);	
  %>
 <%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="400"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.credit.balance.scheduler",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width=100% align=center>
			<tr><td class="formAreaTitle"><a href="javascript:void(0)" onclick="openReport();"><%=Languages.getString("jsp.admin.reports.credit.balance.back",SessionData.getLanguage()).toUpperCase()%></a></td></tr>
				<tr>
					<td class=formArea2>
      		<tr><td><font color=red>
		<% if (searchErrors != null) 
		{ 
		  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br><ul>"); 
		Enumeration enum1=searchErrors.keys(); 
		while(enum1.hasMoreElements()) 
		{ 
		  String strKey = enum1.nextElement().toString(); 
		  String strError = (String) searchErrors.get(strKey); 
		  out.println("<li>" + strError+"</li>"); 
		} 
		 
		  out.println("</ul></font></td></tr></table>"); 
		} 
      
	
%></font></td></tr>
	<table>
							<tr>
								<td colspan="4">					
									<table border="0" cellpadding="2" cellspacing="0" width=100% align="center">
										<tr>
											<td class="main"><br>
												<table cellspacing="1" cellpadding="2" width=100%>
													<tr>							
													    <th class=rowhead2 width="40%"><%=Languages.getString("jsp.admin.tools.s2k.task.title4",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   
														<th class=rowhead2 width="40%"><%=Languages.getString("jsp.admin.tools.s2k.task.title6",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>     
														<th class=rowhead2 width="20%"><%=Languages.getString("jsp.admin.tools.s2k.task.title5",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th> 
														<th class=rowhead2><%=Languages.getString("jsp.admin.tools.bonusPromoTools.edit",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2 style="text-align:center;"><%=Languages.getString("jsp.admin.tools.s2k.delete",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>  
													</tr>

<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();%>
		<tr class=rowwrap<%=intEvenOdd%>>
		<td id ="id" value ="<%=vecTemp.get(0).toString()%>" valign=top><%=vecTemp.get(0).toString().trim()%></td>
															<td id ="date" valign=top><%=vecTemp.get(1)%></td>
															<% dbvalue = Integer.parseInt(vecTemp.get(2).toString());
															String dvbalue2 ="";
															if(dbvalue==0){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response0",SessionData.getLanguage());
															}
															else if(dbvalue==1){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response1",SessionData.getLanguage());
															} 
															else if(dbvalue==2){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response2",SessionData.getLanguage());
															} 
															else if(dbvalue==3){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response3",SessionData.getLanguage());
															} 
															else if(dbvalue==4){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response4",SessionData.getLanguage());
															}
															else
																dvbalue2 = Languages.getString("jsp.admin.tools.s2k.default",SessionData.getLanguage());%>
															
															   <td id ="status" value="<%=vecTemp.get(2).toString()%>" valign=top><%=dvbalue2%></td>
															 <td valign=top><a href="javascript:void(0);" <%if(dbvalue==0){%>onclick="return UpdateJob(<%=vecTemp.get(0).toString()%>,'<%=vecTemp.get(3).toString()%>');" <%}else{%> onclick="return false"<%}%>><img src="images/icon_edit.gif" border=0 alt="Edit this Route." title="Edit this Job." /></a></td>
															<td valign=top><a href="javascript:void(0);" onclick="return DeleteJob(<%=vecTemp.get(0).toString()%>);"><img src="images/icon_delete.gif" border=0 alt="Delete this route." title="Delete this Job." /></a></td>
															
															</tr>
	<%          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            				
									
 			</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
   <tr>
								<td>
    	<FORM ID="frmDelete" action="admin/reports/payments/credit_balance_scheduler.jsp" METHOD="POST">
									<INPUT TYPE="hidden" NAME="type" VALUE="DELETE">
									<INPUT TYPE="hidden" ID="delId" NAME="delId" VALUE="">
								</FORM>
								<SCRIPT>
								function DeleteJob(nID)
								{
									var agree=confirm("Are you sure you want to delete?");
									if (agree)
									{
										document.getElementById("delId").value = nID;
										document.getElementById("frmDelete").submit();
									}
									else
									{
										return false ;
									}
																		
									
								}
								</SCRIPT>
								<FORM ID="frmEdit" action="admin/reports/payments/credit_balance.jsp" METHOD="POST">
									<INPUT TYPE="hidden" NAME="type" VALUE="UPDATE">
									<INPUT TYPE="hidden" ID="editId" NAME="editId" VALUE="">
									<input type="hidden" id="activity" name="activity" value=""/>
									<input type="hidden" id="entityR" name="entityR" value=""/>
									<input type="hidden" id="op" name="op" value=""/>
								</FORM>
								<SCRIPT>
								function UpdateJob(nID,entityId)
								{
									document.getElementById("editId").value = nID;
									document.getElementById("activity").value = "edit";
									document.getElementById("op").value = "2";
									document.getElementById("entityR").value=entityId;
									document.getElementById("frmEdit").submit();
								}
								function openReport()
								{
									document.getElementById("frmEdit").submit();
								}
								</SCRIPT>
   </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>