<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 4;
  int section_page = 22;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%

 Vector    vecSearchResults = new Vector();
  	 int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;
     Hashtable searchErrors     = null;
     String    sortString       = "";
   	String agentsID="";
 	 String subsID="";
  	String entity="";
if (request.getParameter("search") != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }
  
  }
	
  if ( request.getParameter("download") != null )
  {
  	  String sURL = "";
  	   sURL = TransactionReport.downloadEntityCreditDetail(application, SessionData,request.getParameter("agentsID"),request.getParameter("subsID"),request.getParameter("entity"));
  	  response.sendRedirect(sURL);
  	  return;
  }
   if (request.getParameter("search") != null)
  {
  		 agentsID=request.getParameter("agentsID");
  		 subsID=request.getParameter("subsID");
  		 entity=request.getParameter("entity");
        vecSearchResults = TransactionReport.getEntityCreditDetail(intPage, intPageSize, request.getParameter("agentsID"),request.getParameter("subsID"),request.getParameter("entity"),SessionData);
    	intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
	  vecSearchResults.removeElementAt(0);
	  if (intRecordCount>0)
	  {
	    intPageCount = (intRecordCount / intPageSize) + 1;
	    if ((intPageCount * intPageSize) + 1 >= intRecordCount)
	    {
	      intPageCount++;
	    }
	    if ( intPage > intPageCount )
	    {
			intPage = intPageCount;
		}
	  }
  }
  %>
 <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
        &nbsp;
        <%= Languages.getString("jsp.admin.reports.credit.balance.title",SessionData.getLanguage()).toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td class="main">
                <%= Languages.getString("jsp.admin.reports.credit.balance.notes",SessionData.getLanguage()) %>
                </font>
              </td>
             </tr>
             <tr>
              <td class=main align=left valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>                
              </td>
            </tr>
            <tr>
            	<td colspan=2>
            <table>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>
                <FORM ACTION="admin/reports/payments/credit_balance_report.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	<INPUT TYPE=hidden NAME="search" VALUE="<%=request.getParameter("search")%>">
                	<INPUT TYPE=hidden NAME="download" VALUE="Y">
                	<INPUT TYPE=hidden NAME="entity" id="entity" value="<%=entity%>">
                	<INPUT TYPE=hidden NAME="agentsID" id="agentsID" VALUE="<%=agentsID%>">
                	<INPUT TYPE=hidden NAME="subsID" id="subsID" VALUE="<%=subsID%>">
                	<INPUT name="btnSubmit" ID="btnSubmit" TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                	
                </FORM>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                </td>
              </tr>
            </table>
            	</td>
            </tr>
             <tr>
              <td align=right class="main" nowrap colspan=7>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"admin/reports/payments/credit_balance_report.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=1&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") +"&agentsID="+ agentsID +"&subsID="+ subsID +"&entity="+ entity +"&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" + Languages.getString("jsp.admin.first",SessionData.getLanguage()) + "</a>&nbsp;");
                out.println("<a href=\"admin/reports/payments/credit_balance_report.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") +  "&page=" + (intPage-1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&agentsID="+ agentsID +"&subsID="+ subsID +"&entity="+ entity + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"admin/reports/payments/credit_balance_report.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + i + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&agentsID="+ agentsID +"&subsID="+ subsID +"&entity="+ entity + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" + i + "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"admin/reports/payments/credit_balance_report.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + (intPage+1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&agentsID="+ agentsID +"&subsID="+ subsID +"&entity="+ entity + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"admin/reports/payments/credit_balance_report.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + (intPageCount-1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&agentsID="+ agentsID +"&subsID="+ subsID +"&entity="+ entity + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
              }

              %>
              </td>
            </tr>
          </table>
          
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>
                <td class=rowhead2 nowrap>
                <%if(request.getParameter("entity").equals("rep")){%>   
                   <%= Languages.getString("jsp.admin.reports.credit.balance.rep_id",SessionData.getLanguage()) %>  
                  <%}else{%>
                  <%= Languages.getString("jsp.admin.reports.credit.balance.merchant_id",SessionData.getLanguage()) %>  
                  <%}%>
                </td>
                <td class=rowhead2 nowrap>
                 <%if(request.getParameter("entity").equals("rep")){%>  
                	<%= Languages.getString("jsp.admin.reports.credit.balance.rep",SessionData.getLanguage()) %>  
                  <%}else{%>
                  <%= Languages.getString("jsp.admin.reports.credit.balance.merchant",SessionData.getLanguage()) %>  
                  <%}%>
                </td>
                <td class=rowhead2 nowrap>
                 <%= Languages.getString("jsp.admin.reports.credit.balance.available_credit",SessionData.getLanguage()) %>
                </td>
                </tr>
            </thead>
<%
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;
            
  while (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector)it.next();
                 out.print("<tr class=row" + intEvenOdd + ">" + 
            		  "<td>" + intCounter++ + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(0) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(1) + "</td>" + 
            		  "<td align=right nowrap>" + (NumberUtil.formatCurrency(vecTemp.get(2).toString()))+  "</td>");
              out.println("</tr>");

              if (intEvenOdd == 1)
              {
                intEvenOdd = 2;
              }
              else
              {
                intEvenOdd = 1;
              }          	
            	
            }
%> 
					<tfoot>
                    </tfoot>
			   </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
            
                    //<!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "Number", "CaseInsensitiveString", "Number", "Number"],0,false,false);
 
  //-->
            
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
