<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.customers.Merchant,
                 java.util.*" %>
<%
int section=4;
int section_page=34;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="User" class="com.debisys.customers.UserPayments" scope="request" />
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

    <SCRIPT LANGUAGE="JavaScript">
	var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

	var textAgent ="";
	var textSubAgent ="";
	var textRep ="";
	
	var agentParam = null;
	var subAgentParam = null;
	var repParam = null;

function scroll(){
	var midSelect = document.getElementById("mids");
	var midText = midSelect.options[midSelect.selectedIndex].text;
	document.getElementById("midSelected").value = midText;
	
    document.mainform.submit.disabled = true;
    document.mainform.submit.value = iProcessMsg[count];
    count++
    if (count = iProcessMsg.length) count = 0
    setTimeout('scroll()', 150);
}
function scroll2(){
  	document.downloadform.submit.disabled = true;
  	document.downloadform.submit.value = iProcessMsg[count];
  	count++
  	if (count = iProcessMsg.length) count = 0
  	setTimeout('scroll2()', 150);
}

/*
 * Alfred A./DBSY-1082 - Generic xmlDoc loading depending on the active browser
 */
function createXMLDoc(){
	if(window.ActiveXObject){
		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
	}else if(document.implementation && document.implementation.createDocument){
		xmlDoc = document.implementation.createDocument("", "", null);
	} 
	else{
		alert('Form is not supported in your browser');
		return;
	}
}

//Alfred A./DBSY-1082 - Generic function to generate an array of values from a form.
function returnOptionsValues(formOptions){
	var paramArray = new Array();
	arrayCounter = 0;
	for(var i = 0;i<formOptions.length;i++){
		if(formOptions.options[i].value != 0 && formOptions.options[i].value != -1){
			paramArray[arrayCounter] = formOptions.options[i].value;
			arrayCounter++;
		}
	}
	return paramArray;
}

function generateURL(mainURL, additionalStringParam, paramArray, singleParam){
	var url = null;
	if(paramArray.length != 0){
		url = mainURL+paramArray[0];
		for(var i = 1; i < paramArray.length; i++){
			if(i < 100)
				url = url + additionalStringParam + paramArray[i];
		}
	}else{
		url = mainURL+singleParam;
	}
	return url;
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the ISO version.
 */
function importISOXML(){
	createXMLDoc();
	xmlDoc.async = false;
	var isoParam = <%=SessionData.getProperty("ref_id")%>;
	var url = "admin/reports/transactions/entitiesData.jsp?isoParam="+isoParam;
	var loaded = xmlDoc.load(url);
	if(loaded){
		createAgentDropDown(xmlDoc);
	}
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the Agent version.
 */
function importAgentXML(){
	document.getElementById("midSelected").value = "";
	createXMLDoc();
	xmlDoc.async = false;
	agentParam = null;
	var tempIndex = 0;
	var agentParamArray = new Array();
	//Makes sure we take our own id if we're an agent.
	if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){
		agentParam = <%=SessionData.getProperty("ref_id")%>;
		textAgent = agentParam;
		tempIndex = 1;
	}else{
		agentParam = document.mainform.AgentTypes.value;
		tempIndex = 3;
		var el = document.mainform.AgentTypes;
		textAgent = el.options[el.selectedIndex].innerHTML;
		if(agentParam == 0){
			agentParamArray = returnOptionsValues(document.mainform.AgentTypes);
		}
	}
	if(agentParamArray.length > 100){
		var url = "admin/reports/transactions/entitiesData.jsp?a="+"-1"+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
	}else{
		var url = generateURL("admin/reports/transactions/entitiesData.jsp?a=", "&a=", agentParamArray, agentParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createSubAgentDropDown(xmlDoc);
	}
	if(tempIndex == 3){ 
		if((agentParam == 0 || agentParam == 1 ) && <%=strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)%>){
			textAgent = "All";
			agentParam = 1;
			document.mainform.AgentTypes.selectedIndex="1";
		}
		
		var extraInfo = '';
		if(agentParam == 1 ){
			extraInfo = ". <%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.infoEntity",SessionData.getLanguage())%>";
		}
		document.getElementById("itemSelected").value = agentParam;
		document.getElementById("entitySelected").value = "Agent";
		document.getElementById("textSelected").value = textAgent;
		document.getElementById("paragraphInfo").innerHTML = "[<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",SessionData.getLanguage())%> ] > " + textAgent+ " - " + agentParam+ " "+extraInfo;
	}
}


/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the SubAgent version.
 */
function importSubAgentXML(){
	document.getElementById("midSelected").value = "";
	createXMLDoc();
	xmlDoc.async = false;
	subAgentParam = null;
	var tempIndex = 0;
	var subAgentParamArray = new Array();
	if(<%=strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){
		subAgentParam = <%=SessionData.getProperty("ref_id")%>;
		textSubAgent = subAgentParam;
		tempIndex = 1;
	}else if(<%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>){
		subAgentParam = <%=SessionData.getProperty("ref_id")%>;
		textSubAgent = subAgentParam;
		tempIndex = 2;
	}
	else{
		subAgentParam = document.mainform.SubAgentTypes.value;
		tempIndex = 3;
		var el = document.mainform.SubAgentTypes;
		textSubAgent = el.options[el.selectedIndex].innerHTML;
		if(subAgentParam == 0){
			subAgentParamArray = returnOptionsValues(document.mainform.SubAgentTypes);
		}
	}
	if(subAgentParamArray.length > 100){
		if(<%=strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){//You're an iso, and you've got a lot of subagents.  Agent value needs to be passed as well
			var url = "admin/reports/transactions/entitiesData.jsp?a="+"-1"+"&subAgentTooLong="+document.mainform.AgentTypes.value+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){//Basically means you're an agent at this point, and you had tons of subagents underneath that are going to error when creating the reps dropdown.
			var url = "admin/reports/transactions/entitiesData.jsp?a="+"-1"+"&subAgentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}
	}else{
		var url = generateURL("admin/reports/transactions/entitiesData.jsp?a=", "&a=", subAgentParamArray, subAgentParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createRepDropDown(xmlDoc);
	} else {
                    // display error message
                var errorMsg = null;
                if (xmlDoc.parseError && xmlDoc.parseError.errorCode != 0) {
                    errorMsg = "XML Parsing Error: " + xmlDoc.parseError.reason
                              + " at line " + xmlDoc.parseError.line
                              + " at position " + xmlDoc.parseError.linepos;
                }
                else {
                    if (xmlDoc.documentElement) {
                        if (xmlDoc.documentElement.nodeName == "parsererror") {
                            errorMsg = xmlDoc.documentElement.childNodes[0].nodeValue;
                        }
                    }
                }
                if (errorMsg) {
                    alert (errorMsg);
                }
                else {
                    alert ("There was an error while loading XML file!");
                }
            }
            
     if(tempIndex == 3){
     			
     	if((subAgentParam == 0 || subAgentParam == 1) && <%=(strAccessLevel.equals(DebisysConstants.AGENT)) && !strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){
			textSubAgent = "All";
			subAgentParam = 1;
			document.mainform.SubAgentTypes.selectedIndex="1";
		}
		var extraInfo = '';
		if(subAgentParam == 1 ){
			extraInfo = ". <%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.infoEntity",SessionData.getLanguage())%>";
		}
		
		if(document.mainform.SubAgentTypes.selectedIndex == 0){
			selectLastEntity("Subagent");
		}
		else{
	     document.getElementById("entitySelected").value = "Subagent";
	     document.getElementById("itemSelected").value = subAgentParam;
	     document.getElementById("textSelected").value = textSubAgent;
	     document.getElementById("paragraphInfo").innerHTML = "[<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",SessionData.getLanguage())%> ] > " + textSubAgent+ " - " + subAgentParam+ " "+extraInfo;
     	}
     }
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the Rep version.
 */
function importRepXML(){
	document.getElementById("midSelected").value = "";
	createXMLDoc();
	xmlDoc.async = false;
	var tempIndex = 0;
	repParam = null;
	var repParamArray = new Array();
	//Makes sure we take our own id if we're a rep.
	if(<%=strAccessLevel.equals(DebisysConstants.REP)%>){
		repParam = <%=SessionData.getProperty("ref_id")%>;
		textRep = repParam;
		tempIndex = 1;
	}else{
		repParam = document.mainform.RepTypes.value;
		tempIndex = 3;
		var el = document.mainform.RepTypes;
		textRep = el.options[el.selectedIndex].innerHTML;
		if(repParam == 0){
			repParamArray = returnOptionsValues(document.mainform.RepTypes);
		}
	}
	//Needs to handle iso, agent, and subagent here.
	if(repParamArray.length > 100){
		if(<%=strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){//You're an iso, and you've got a lot of subagents.  Agent value needs to be passed as well
			var url = "admin/reports/transactions/entitiesData.jsp?a="+"-1"+"&repTooLong="+document.mainform.SubAgentTypes.value+"&subAgentTooLong="+document.mainform.AgentTypes.value+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){//Basically means you're an agent at this point, and you had tons of subagents underneath that are going to error when creating the reps dropdown.
			var url = "admin/reports/transactions/entitiesData.jsp?a="+"-1"+"&repTooLong="+document.mainform.SubAgentTypes.value+"&subAgentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>){
			var url = "admin/reports/transactions/entitiesData.jsp?a="+"-1"+"&repTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}
	}else{
		var url = generateURL("admin/reports/transactions/entitiesData.jsp?r=", "&r=", repParamArray, repParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createMerchantList(xmlDoc);
	}
	
	if(tempIndex == 3 ){		
		if((repParam == 0 || repParam == 1 ) && <%=((strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) || 
						(strAccessLevel.equals(DebisysConstants.ISO) && !strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))%>){						
			if(<%= strAccessLevel.equals(DebisysConstants.ISO)%> && repParam == 1 ){
				document.mainform.AgentTypes.selectedIndex="1"; 
				document.mainform.SubAgentTypes.selectedIndex=repParam;
			}
			if(<%= strAccessLevel.equals(DebisysConstants.AGENT)%>  && repParam == 1 ){
				document.mainform.SubAgentTypes.selectedIndex=repParam;
				document.mainform.RepTypes.selectedIndex=repParam;
			}
			else if(<%= strAccessLevel.equals(DebisysConstants.SUBAGENT)%>  && repParam == 1 ){
				document.mainform.RepTypes.selectedIndex=repParam;				
			}
			else if(<%= strAccessLevel.equals(DebisysConstants.REP)%>  && repParam == 1 ){
				document.mainform.RepTypes.selectedIndex=repParam;
			}
		}
		
		if((repParam == 0 || repParam == 1 )&& <%=(strAccessLevel.equals(DebisysConstants.SUBAGENT) || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))%>){
			textRep = "All";
			document.mainform.RepTypes.selectedIndex="1";		
			repParam = 1;	
		}
		
		var extraInfo = '';
		if(repParam == 1 ){
			extraInfo = ". <%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.infoEntity",SessionData.getLanguage())%>";
		}
		if(document.mainform.RepTypes.selectedIndex == 0){
			selectLastEntity("Rep");
		}
		else{
		document.getElementById("entitySelected").value = "Rep";
		document.getElementById("itemSelected").value = repParam;
		document.getElementById("textSelected").value = textRep;
		document.getElementById("paragraphInfo").innerHTML = "[<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",SessionData.getLanguage())%>] > " + textRep+ " - " + repParam + " "+extraInfo;
		}
	}
	else if(tempIndex == 1){
		document.mainform.mids.selectedIndex=1;
    	document.getElementById("entitySelected").value = "Merchant";
		document.getElementById("itemSelected").value = "";
		document.getElementById("paragraphInfo").innerHTML = "[<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.merchant",SessionData.getLanguage())%>] ";
    }
}

/*
 * Alfred A./DBSY-1082 - Function that dynamically clear out form locations that
 * no longer need to be filled in anymore.
 */
function clearOptions(levelToClear){
	if(levelToClear == 0){
		var agentDropDown = document.mainform.SubAgentTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
	}else if(levelToClear == 1){
		var agentDropDown2 = document.mainform.RepTypes;
		for(var a = agentDropDown2.options.length;a>=0;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 2){
		var agentDropDown2 = document.mainform.mids;
		for(var a = agentDropDown2.options.length;a>=2;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 3){
		var agentDropDown = document.mainform.RepTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
		var agentDropDown2 = document.mainform.mids;
		for(var a = agentDropDown2.options.length;a>=2;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 4){
		var agentDropDown = document.mainform.SubAgentTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
		var agentDropDown2 = document.mainform.RepTypes;
		for(var a = agentDropDown2.options.length;a>=0;a--){
			agentDropDown2.options[a] = null;
		}
		var agentDropDown3 = document.mainform.mids;
		for(var a = agentDropDown3.options.length;a>=2;a--){
			agentDropDown3.options[a] = null;
		}
	}
}

/*
 * Alfred A./DBSY-1082 - Generic function that takes in a form and XML data,
 * which will then populate that form with the data.
 */
function genericFillIn(formType, x){
	//formType.options[0] = new Option("Select Option", -1);
	formType.options[0] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.none",SessionData.getLanguage())%>", 0);
	formType.options[1] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage())%>", 1);
	//formType.options[1] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage())%>", 0);
	var dropDownCounter = 1;
	for (i=0;i<x.length;i++)
	{
		if(<%=SessionData.getProperty("ref_id")%> == x[i].getAttribute("val")){
  			continue;
  		}
		var name = x[i].firstChild.nodeValue;
		var value = x[i].getAttribute("val");
  		
  		formType.options[dropDownCounter+1] = new Option(name, value);
  		dropDownCounter++;
	}
}

// Alfred A./DBSY-1082 - These 3 functions merely set up the generic fill-in above.
function createAgentDropDown(xmlDoc){
	genericFillIn(document.mainform.AgentTypes, xmlDoc.getElementsByTagName("display"));
	importAgentXML();
}

function createSubAgentDropDown(xmlDoc){
	clearOptions(4);
	genericFillIn(document.mainform.SubAgentTypes, xmlDoc.getElementsByTagName("display"));
	importSubAgentXML();
}

function createRepDropDown(xmlDoc){
	clearOptions(3);
	genericFillIn(document.mainform.RepTypes, xmlDoc.getElementsByTagName("display"));
	importRepXML();
}

/*
 * Alfred A./DBSY-1082 - Slightly different function that functions like the generic fill in above, 
 * but also gives a hidden attribute a value in case the user has selected the ALL option in Merchants.
 */
function createMerchantList(xmlDoc){
	clearOptions(2);
	var agentDropDown = document.mainform.mids;
	var x = xmlDoc.getElementsByTagName("display");
	var allValues = new Array();
	var dropDownCounter = 1;
	for (i=0;i<x.length;i++)
	{
		if(<%=SessionData.getProperty("ref_id")%> == x[i].getAttribute("val")){
  			continue;
  		}
		var name = x[i].firstChild.nodeValue;
		var value = x[i].getAttribute("val");
		allValues[i] = value;
  		
  		agentDropDown.options[dropDownCounter+1] = new Option(name, value);
  		dropDownCounter++;
	}
	
	document.mainform.AllValues.value = allValues.join(":");
}

function changeModeMerchant(){
	//
	if(document.mainform.mids.selectedIndex == 0){
		selectLastEntity("Merchant");
	}
	else{
		document.getElementById("entitySelected").value = "Merchant";
		document.getElementById("itemSelected").value = "";
		document.getElementById("paragraphInfo").innerHTML = "[<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.merchant",SessionData.getLanguage())%>] ";
	}
}

function selectLastEntity(entity){
	
	var extraInfo = "";
	var label = "";
	if(entity == "Subagent"){
		if(document.mainform.AgentTypes != null){
			document.mainform.AgentTypes.value = agentParam;
			label = "[<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",SessionData.getLanguage())%>]";
			document.getElementById("entitySelected").value = "Agent";
			document.getElementById("itemSelected").value = agentParam;
			document.getElementById("textSelected").value = textAgent;
			if(agentParam == 1){
				extraInfo = ". <%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.infoEntity",SessionData.getLanguage())%>";
			}
		}
	}
	else if(entity == "Rep"){
		if(subAgentParam == 0){
			selectLastEntity("Subagent");
			return;
		}
		else{
		
			document.mainform.SubAgentTypes.value = subAgentParam;
			label = "[<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",SessionData.getLanguage())%>]";
			document.getElementById("entitySelected").value = "Subagent";
			document.getElementById("itemSelected").value = subAgentParam;
			document.getElementById("textSelected").value = textSubAgent;
			if(subAgentParam == 1){
				extraInfo = ". <%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.infoEntity",SessionData.getLanguage())%>";
			}
		}
	}
	else if(entity == "Merchant"){
		if(repParam == 0){
			selectLastEntity("Rep");
			return;
		}
		else{
			document.mainform.RepTypes.value = repParam;
			label = "[<%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",SessionData.getLanguage())%>]";
			document.getElementById("entitySelected").value = "Rep";
			document.getElementById("itemSelected").value = repParam;
			document.getElementById("textSelected").value = textRep;
						
			if(repParam == 1){
				extraInfo = ". <%=Languages.getString("jsp.admin.reports.payments.depositsByMerchant.infoEntity",SessionData.getLanguage())%>";
			}
		}
	}
	document.getElementById("paragraphInfo").innerHTML = label+" > " + document.getElementById("textSelected").value+ " - " + document.getElementById("itemSelected").value + " "+extraInfo;
}
</SCRIPT>
<%	
String extraFilter = User.getPermissionFaceyJAMGrpPaymentDetailsReport(SessionData);

if(extraFilter.equals("1")){
	if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
	%>
	<body onload="importSubAgentXML()">
	<%
	}else if(strAccessLevel.equals(DebisysConstants.ISO)) {
	%>
	<body onload="importISOXML()">
	<%
	}else if(strAccessLevel.equals(DebisysConstants.AGENT)) {
	%>
	<body onload="importAgentXML()">
	<%
	}else if(strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
	%>
	<body onload="importSubAgentXML()">
	<%
	}else if(strAccessLevel.equals(DebisysConstants.REP)) {
	%>
	<body onload="importRepXML()">
	<%
	}
}
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title45",SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
       
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form name="mainform" method="post" action="admin/reports/payments/deposits_summary.jsp" onSubmit="scroll();">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="formArea2">              
                                        <table width="300">
                                            <tr class="main">
                                                <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>
		         <%}%>:</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" nowrap>
                                                    <table width=400>
                                                        <tr class="main">
                                                            <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td><td><input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                                        </tr>
                                                        <tr class="main">
                                                            <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td> <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                                        </tr>
<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.payments.deposits.entities.option",SessionData.getLanguage())%></td>
    <td class=main valign=top>
    <%if(!extraFilter.equals("1")){ %>
        <select name="entities">
       <%  if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) )
           { %>
          <option value="Agent" selected>Agent</option>
          <option value="SubAgent">SubAgent</option>
          <option value="Rep">Rep</option>
          <option value="Merchant">Merchant</option>
          <%
          }
          else if  ( strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) )
          {
            %>
            <option value="Rep">Rep</option>
            <option value="Merchant">Merchant</option>
            <% 
          }
          
           if (strAccessLevel.equals(DebisysConstants.AGENT)) { %>
          <option value="SubAgent" selected>SubAgent</option>
          <option value="Rep">Rep</option>
          <option value="Merchant">Merchant</option>
          <%}if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){ %>
          <option value="Rep" selected>Rep</option>
          <option value="Merchant">Merchant</option>
          <%} if (strAccessLevel.equals(DebisysConstants.REP)){ %>
          <option value="Merchant" selected>Merchant</option>
          <%} %>
          </select>
        <br>
        <%} %>
                <%
                if(extraFilter.equals("1")){
					if(!strAccessLevel.equals(DebisysConstants.MERCHANT)) {
					%>
						<%
						if(strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
						%>
						    	 <tr class="main">
									 <td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.agent.select",SessionData.getLanguage())%></td>
								     <td><SELECT name="AgentTypes" onchange="importAgentXML()"  style="width: 250px"></SELECT></td>
								 </tr>	
								 
							   <tr></tr><tr></tr>
						
						<%
						}if((strAccessLevel.equals(DebisysConstants.ISO)|| strAccessLevel.equals(DebisysConstants.AGENT)) && !strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
						%>
								<tr class="main">
									<td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.sub_agent.select",SessionData.getLanguage())%></td>
									<td><SELECT name="SubAgentTypes" onchange ="importSubAgentXML()" style="width: 250px"> </SELECT></td>
								</tr>
								
								<tr></tr><tr></tr>
						<%
						}if((strAccessLevel.equals(DebisysConstants.ISO)|| strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) || 
						(strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))) {
						%>		 
								<tr class="main">
									<td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.rep.select",SessionData.getLanguage())%></td>
									<td><SELECT name="RepTypes" onchange="importRepXML()" style="width: 250px"> </SELECT></td>
								</tr>
								
								<tr></tr><tr></tr>
						<%
						}
						%>
								 
						<tr>
								<td class=main valign=top nowrap> <%=Languages.getString("jsp.admin.reports.transactions.daily_liability.merchant.select",SessionData.getLanguage())%></td>
						        <td class=main valign=top>
									<select name="mids" size="10" style="width:250px;" multiple onclick="changeModeMerchant()">
									  <option value="-1"><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.none",SessionData.getLanguage())%></option>	
									  <option value="" ><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
									  <%
									  if(strAccessLevel.equals(DebisysConstants.REP)){
										Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
										Iterator it = vecMerchantList.iterator();
										while (it.hasNext())
										{
											Vector vecTemp = null;
											vecTemp = (Vector) it.next();
											out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
										}
									  }
									   %>
									</select>	
						         <br /><br />
							</td>
						</tr>
						
						<%
					} else {
						%>
					<input type="hidden" name="search" value="y">
						<script language="javascript" type="text/javascript" >
						var form = document.getElementById('deposits_form');
						form.submit();
						</script>
					<%
					}
					}
					%>
        
        <br>
        
</td>
</tr>
<%if(extraFilter.equals("1")){ %>
<tr><td nowrap="nowrap" colspan="1">
	<p><%=Languages.getString("jsp.admin.reports.payments.deposits.entitySeleted",SessionData.getLanguage())%>:</p>
	</td>
	<td nowrap="nowrap" colspan="1">
		<p id="paragraphInfo" style="color:green;"></p>
 	</td>
</tr>
<%} %>
<tr>
    <td class=main align=center>
      <input type="hidden" name="entitySelected" id="entitySelected" >
	  <input type="hidden" name="itemSelected" id="itemSelected" >
	  <input type="hidden" name="textSelected" id="textSelected" >
	  <input type="hidden" name="midSelected" id="midSelected" >
      <input type="hidden" name="search" value="y">
      <input type="hidden" name="AllValues">
      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="validateSchedule(0);" >
    </td>
    <jsp:include page="/admin/reports/schreportoption.jsp">
	  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
	 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
	</jsp:include>
    
</tr>
<tr>
<td class="main" colspan=2>
* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
</td>
</tr>                                              
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                      
                                    </td>
                                </tr>
                            </table>          
                        </form>
                    </td>
                </tr>                                
            </table>
        </td>
    </tr>  
</table>

<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe> 
<%@ include file="/includes/footer.jsp" %>                        