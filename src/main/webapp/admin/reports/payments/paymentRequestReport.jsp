<%@page import="com.debisys.reports.schedule.ResultField"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.debisys.utils.DateUtil"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="com.debisys.reports.pojo.BankPojo"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="com.debisys.reports.schedule.PaymentRequestReport"%>
<%@page import="com.debisys.utils.StringUtil"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 4;
    int section_page = 10;
%>


<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>



<%@ page import = "java.util.Map" %>
<%    /*
     Map<String, String[]> parameters = request.getParameterMap();
     for (String parameter : parameters.keySet()) {
     String[] values = parameters.get(parameter);
     out.println(String.format("%s = %s <br>", parameter, values[0]));
     }
     out.println("Fin Parameters");
     */
    String strRefId = SessionData.getProperty("ref_id");
    SessionData.setProperty("show_schedule_btn", "y");
    String reportTitleKey = "jsp.admin.reports.paymentRequestReport.title";
    String reportTitle = Languages.getString(reportTitleKey, SessionData.getLanguage()).toUpperCase();

    if (((request.getParameter("sheduleReport") == null) || (request.getParameter("sheduleReport").equals("")))
            && ((request.getParameter("search") == null) || (request.getParameter("search").equals("")))
            && ((request.getParameter("download") == null) || (request.getParameter("download").equals("")))) {
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <form name="mainform" method="post" action="admin/reports/payments/paymentRequestReport.jsp">
                <%@ include file="/WEB-INF/jspf/admin/reports/payments/paymentRequestSearchFilters.jspf" %>
            </form>
        </td>
    </tr>    
</table>

<%    } else {

    ArrayList<ArrayList<ResultField>> vec_results = new ArrayList<ArrayList<ResultField>>();
    PaymentRequestReport report = null;
    Hashtable searchErrors = null;

%>
<%@ include file="/WEB-INF/jspf/admin/reports/payments/paymentRequestSearchParamProcess.jspf" %>

<%    if (request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y")) {
        //TO SCHEDULE REPORT
        scheduling = true;
        report = new PaymentRequestReport(SessionData, application, startDate,
                endDate, agentList, allAgentsSelected, subAgentList, allSubAgentsSelected,
                repList, allRepsSelected, merchantList, allMerchantsSelected,
                paymentStatusList, allPaymentStatusSelected, bankListParam,
                allBanksSelected, paymentRequestId, documentId, paymentAmount,
                siteId, scheduling);
        if (report.scheduleReport()) {
            ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
            if (scheduleReport != null) {
                scheduleReport.setStartDateFixedQuery(report.getStartDate());
                scheduleReport.setEndDateFixedQuery(report.getEndDate());
                scheduleReport.setTitleName(reportTitleKey);
            }
            response.sendRedirect(DebisysConstants.PAGE_TO_SCHEDULE_REPORTS);
        }
    } else if (request.getParameter("search") != null && request.getParameter("search").equals("y")) {
        report = new PaymentRequestReport(SessionData, application, startDate,
                endDate, agentList, allAgentsSelected, subAgentList, allSubAgentsSelected,
                repList, allRepsSelected, merchantList, allMerchantsSelected,
                paymentStatusList, allPaymentStatusSelected, bankListParam,
                allBanksSelected, paymentRequestId, documentId, paymentAmount,
                siteId, scheduling);
        vec_results = report.getResults();
    } else if (request.getParameter("download") != null && request.getParameter("download").equals("y")) {
        //TO DOWNLOAD ZIP REPORT
        report = new PaymentRequestReport(SessionData, application, startDate,
                endDate, agentList, allAgentsSelected, subAgentList, allSubAgentsSelected,
                repList, allRepsSelected, merchantList, allMerchantsSelected,
                paymentStatusList, allPaymentStatusSelected, bankListParam,
                allBanksSelected, paymentRequestId, documentId, paymentAmount,
                siteId, scheduling);
        vec_results = report.getResults();
        report.setScheduling(false);
        String zippedFilePath = report.downloadReport();
        if ((zippedFilePath != null) && (!zippedFilePath.trim().equals(""))) {
            response.sendRedirect(zippedFilePath);
        }
    }

%>

<script src="js/admin/reports/payments/PaymentRequestSearchFilters.js" type="text/javascript"></script>
<link href="css/admin/reports/payments/paymentRequestSearchFilters.css" type="text/css" rel="StyleSheet" />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <th class="formAreaTitle title" colspan="100%" width="100%">&nbsp;<%=reportTitle.toUpperCase()%></th>
    </tr>
    <%
        if ((searchErrors != null) && (searchErrors.size() > 0)) {
    %>
    <tr>
        <td>
            <div id="errorListDiv" class="warnMessage">
                <ul>
                    <%
                        Enumeration enum1 = searchErrors.keys();

                        while (enum1.hasMoreElements()) {
                            String strKey = enum1.nextElement().toString();
                            String strError = (String) searchErrors.get(strKey);
                            out.println("<li>" + strError);
                        }
                    %>
                </ul>
            </div>
        </td>
    </tr>
    <%
    } else if ((vec_results == null) || (((vec_results != null) && (vec_results.size() == 0))
            && (request.getParameter("search") != null) && (request.getParameter("search").equals("y"))
            && (searchErrors == null))) {
    %>
    <tr>
        <td>
            <div id="emptyResults" class="warnMessage">
                <br>
                <p><%=Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage())%></p>
            </div>
        </td>
    </tr>
    <%
    } else if ((vec_results != null) && (!vec_results.isEmpty())) {
    %>
    <tr>
        <td>
            <%
                out.println(reportTitle);
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                    out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
            %>                    
            <br>
            <span class="warnMessage"><%=Languages.getString("jsp.admin.reports.test_trans", SessionData.getLanguage())%></span>
        </td>
    </tr>
    <tr>
        <td>
            <form id="downloadForm" method="post" action="admin/reports/payments/paymentRequestReport.jsp">
                <%@ include file="/WEB-INF/jspf/admin/reports/payments/paymentRequestSearchSetFilters.jspf" %>
                <input type=hidden name=download value="y">
                <input type="hidden" id="sheduleReport" name="sheduleReport" value="">
                <input type="hidden" name='search' id='search' value=''>
                <button id="btnSubmit" type="submit">
                    <%=Languages.getString("jsp.admin.reports.downloadToCsv", SessionData.getLanguage())%>
                </button>        
            </form>
        </td>
    </tr>
    <tr>
    <table class="resultsTable sort-table">
        <thead>
            <tr class="SectionTopBorder">
                <td class=rowhead2 nowrap>#</td>      
                <%
                    // Set the column headers
                    ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
                    if ((reportColumns != null) && (reportColumns.size() > 0)) {
                        for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                %>                    
                <th class=rowhead2 nowrap align=center><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
                </th>
                <%
                        }
                    }
                %>
            </tr>
        </thead>   
        <%
            int intEvenOdd = 1;
            int count = 1;

            for (ArrayList<ResultField> currentRecord : vec_results) {
                StringBuilder tableRecord = new StringBuilder();
                tableRecord.append(String.format("<tr class=row%d>", intEvenOdd));
                tableRecord.append(String.format("<td>%d</td>", (count++)));
                // Fill the columns
                for (ResultField currentField : currentRecord) {
                    String defaultFieldFormat = "<td %s>%s</td>";
                    String style = "";

                    if (currentField.getName().equals(PaymentRequestReport.REJECT_REASON_ID)) {
                        if (!String.valueOf(currentField.getValue()).isEmpty()) {
                            String resourceKey = "jsp.admin.reports.paymentrequest_search.rejectReason_" + currentField.getValue();
                            String rejectReazon = Languages.getString(resourceKey, SessionData.getLanguage());
                            if ((rejectReazon != null) && (!rejectReazon.isEmpty())) {
                                tableRecord.append(String.format(defaultFieldFormat, style, String.valueOf(currentField.getValue())));//rejectReazon
                            } else {
                                tableRecord.append(String.format(defaultFieldFormat, style, String.valueOf(currentField.getValue())));
                            }
                        } else {
                            tableRecord.append(String.format(defaultFieldFormat, style, ""));
                        }
                    } else if (currentField.getName().equals(PaymentRequestReport.STATUS_CODE_ID)) {
                        String resourceKey = "jsp.admin.reports.paymentrequest_search.status_" + currentField.getValue();
                        String statusString = Languages.getString(resourceKey, SessionData.getLanguage());
                        if ((statusString != null) && (!statusString.isEmpty())) {
                            tableRecord.append(String.format(defaultFieldFormat, style, statusString));
                        } else {
                            tableRecord.append(String.format(defaultFieldFormat, style, String.valueOf(currentField.getValue())));
                        }
                    } else if (currentField.getName().equals(PaymentRequestReport.DOCUMENT_DATE_ID)) {
                        style = " style=\"text-align: right;\"";
                        tableRecord.append(String.format(defaultFieldFormat, style,
                                PaymentRequestReport.documentDateFormatter.format(currentField.getValue())));

                    } else if (currentField.getName().equals(PaymentRequestReport.AMOUNT_ID)) {
                        style = " style=\"text-align: right;\"";
                        tableRecord.append(String.format(defaultFieldFormat, style,
                                NumberUtil.formatCurrency(String.valueOf(currentField.getValue()))));
                    } else if (currentField.getName().equals(PaymentRequestReport.REQUEST_DATE_ID)) {
                        tableRecord.append(String.format(defaultFieldFormat, style,
                                PaymentRequestReport.requestDateFormatter.format(currentField.getValue())));
                    } else if (currentField.getName().equals(PaymentRequestReport.BANK_ID)
                            || currentField.getName().equals(PaymentRequestReport.MERCHANT_ID)
                            || currentField.getName().equals(PaymentRequestReport.CLERK_ID)
                            || currentField.getName().equals(PaymentRequestReport.CLERK_NAME_ID)) {
                        continue;
                    } else {
                        if (currentField.getName().equals(PaymentRequestReport.DOCUMENT_NUMBER_ID)) {
                            style = " style=\"text-align: right;\"";
                        }
                        tableRecord.append(String.format(defaultFieldFormat, style, String.valueOf(currentField.getValue())));
                    }
                }
                tableRecord.append("</tr>");
                out.println(tableRecord.toString());
                if (intEvenOdd == 1) {
                    intEvenOdd = 2;
                } else {
                    intEvenOdd = 1;
                }
            }
            vec_results.clear();
        %>        
        <td colspan="100%" align=center>
            <form method="post" action="admin/reports/payments/paymentRequestReport.jsp">
                <input type=submit class="plain" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.backtosearch", SessionData.getLanguage())%>">
                <input type=hidden name=download value="">
                <input type="hidden" name='search' id='search' value=''>
                <input type="hidden" id="sheduleReport" name="sheduleReport" value="">
            </form>
        </td>

    </table>
</tr>
<%
        }
    }
%>
</table>    

<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>