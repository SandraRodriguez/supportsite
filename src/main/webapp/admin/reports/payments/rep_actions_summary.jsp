<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,com.debisys.utils.NumberUtil,
                 com.debisys.customers.Merchant,
                 org.apache.torque.Torque,java.sql.*,java.util.Date,
                 com.debisys.reports.schedule.IsoActionsOnRepAccountsReport,
                 com.debisys.schedulereports.ScheduleReport,
                 com.debisys.utils.ColumnReport,
                 com.debisys.utils.StringUtil" %>
<%
	int section=4;
	int section_page=3;
%>

<%@page import="com.debisys.customers.UserPayments"%><jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="request"/>

<jsp:useBean id="IsoActionsOnRepAccountsReport"  
	class="com.debisys.reports.schedule.IsoActionsOnRepAccountsReport" 
	scope="request"/>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<jsp:setProperty name="TransactionReport" property="*"/>


<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
    

<%
	int reg=15;
	SessionData.setProperty("reportId", DebisysConstants.SC_REP_ACT_SUMMARY);
	boolean allUsersSelected = false;
	boolean allRepsSelected = false;
	Vector<String> vecUserList = new Vector<String>(); 

	Hashtable searchErrors = null;
	IsoActionsOnRepAccountsReport report = null;
	
	Vector<Vector<Object>> vecSearchResults = null;
	boolean bReport=false;
	String reportTitleKey = "jsp.admin.reports.title41";
	String reportTitle = Languages.getString(reportTitleKey,SessionData.getLanguage()).toUpperCase();
	String[] userArray = null;
	String[] repsArray = null;

	String users = null;
	String reps = null;
	
    if(request.getParameterValues("users") != null)
    {
    	userArray = request.getParameterValues("users");
    	users = com.debisys.utils.StringUtil.arrayToString(userArray, ",");
    	if(users.equals(""))
    	{
    		allUsersSelected = true;
    	}
    }

    if(request.getParameterValues("reps") != null)
    {
    	repsArray = request.getParameterValues("reps");
    	reps =  com.debisys.utils.StringUtil.arrayToString(repsArray, ",");
    	if(reps.equals(""))
    	{
    		allRepsSelected = true;
    	}
    	
    }
	
	String trReportStartDate = TransactionReport.getStartDate();
	if((trReportStartDate == null) || ((trReportStartDate != null) && (trReportStartDate.trim().equals(""))))
	{
		trReportStartDate = request.getParameter("startDate");
		TransactionReport.setStartDate(trReportStartDate);	
	}
	String trReportEndDate = TransactionReport.getEndDate();
	if((trReportEndDate == null) || ((trReportEndDate != null) && (trReportEndDate.trim().equals(""))))
	{
		trReportEndDate = request.getParameter("endDate");
		TransactionReport.setEndDate(trReportEndDate);	
	}
	SessionData.setProperty("start_date", trReportStartDate);
	SessionData.setProperty("end_date", trReportEndDate);

	
	if (TransactionReport.validateDateRange(SessionData))
    {
		report = new IsoActionsOnRepAccountsReport(SessionData, application, false, users,
				reps, trReportStartDate, trReportEndDate,allRepsSelected, allUsersSelected);
		
		if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
		{
			//TO SCHEDULE REPORT
			report.setScheduling(true);
			if(report.scheduleReport())
			{
				ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
				if (  scheduleReport != null  )
				{
					scheduleReport.setStartDateFixedQuery( report.getStart_date());
					scheduleReport.setEndDateFixedQuery( report.getEnd_date());
					scheduleReport.setTitleName( reportTitleKey );   
				}	
				response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
			}
		}
		else if ( request.getParameter("downloadReport") != null )
		{
			//TO DOWNLOAD ZIP REPORT
			report.setScheduling(false);
			String zippedFilePath = report.downloadReport();
			if((zippedFilePath != null) &&(!zippedFilePath.trim().equals("")))
			{
				response.sendRedirect( zippedFilePath );
			}
		}
		else
		{
			//TO SHOW REPORT
			report.setScheduling(false);
			vecSearchResults = report.getResults();
		}
    }
	else
	{
		searchErrors = TransactionReport.getErrors();
	}
	
	
	if (searchErrors != null)
	{
	    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
	    Enumeration enum1=searchErrors.keys();
	    while(enum1.hasMoreElements())
	    {
	      String strKey = enum1.nextElement().toString();
	      String strError = (String) searchErrors.get(strKey);
	      out.println("<li>" + strError);
	    }
	    out.println("</font></td></tr></table>");
	}
	else
	{
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= Languages.getString("jsp.admin.reports.title41",SessionData.getLanguage()).toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<% 
		if ((vecSearchResults != null) && (vecSearchResults.size() > 0))
		{
%>		
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr><td class="main">
		<%out.println(Languages.getString("jsp.admin.reports.transaction.rep_actions_summary",SessionData.getLanguage()));
		if ((report != null) && (!report.getStart_date().equals("")) && (!report.getEnd_date().equals("")))
		{
			if(report.isUsingDateLimitInterval())
			{
				out.println("<br/>");
				out.println("<Font style=\"color: red\">*");
				out.println(Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage()));
				out.println(" " +  report.getLimitDays() + " ");
				out.println(Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage()));
				
			}
		     out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(report.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(report.getEndDateFormatted()));
		}
		%>
		
		<br><font color="#ff0000"><%=Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage())%></font>
		</td>
        <td class=main align=right valign=bottom>
              <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
        </td>        	
		</tr>		
		<tr>
			<td class="main">
				<form name="downloadData" method=post action="admin/reports/payments/rep_actions_summary.jsp">
					<input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
					<input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
					<input type="hidden" name="downloadReport" value="y">
					<INPUT TYPE=hidden NAME=allUsersSelected VALUE="<%=allUsersSelected%>">
					<INPUT TYPE=hidden NAME=allRepsSelected VALUE="<%=allRepsSelected%>">
<%
		if((userArray != null) && (userArray.length > 0))
		{
			for(int i = 0;i < userArray.length;i++)
			{
%>
       				<input type="hidden" name="users" value="<%=userArray[i]%>">
<%
          	}
		}
%>
<%
		if((repsArray != null) && (repsArray.length > 0))
		{
			for(int i = 0;i < repsArray.length;i++)
			{
%>
       				<input type="hidden" name="reps" value="<%=repsArray[i]%>">
<%
          	}
		}
%>
					<label>
						<%=Languages.getString("jsp.admin.reports.transaction.rep_actions_summary.DownloadLabel",SessionData.getLanguage())%>
         			</label>
				<input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transaction.rep_actions_summary.Download",SessionData.getLanguage())%>">
				</form>
			</td>             
		
		</tr>
		</table>	
<%
		}
%>		

<% 
	if ((vecSearchResults == null)  || ((vecSearchResults != null) && (vecSearchResults.size() <= 0))) 
	{
		if ((report != null) && (!report.getStart_date().equals("")) && (!report.getEnd_date().equals("")))
		{
			if(report.isUsingDateLimitInterval())
			{
				out.println("<br/>");
				out.println("<Font style=\"color: red\">*");
				out.println(Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage()));
				out.println(" " +  report.getLimitDays() + " ");
				out.println(Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage()));		
			}
			out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(report.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(report.getEndDateFormatted()));
		}
%>

    <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
    <tr>
        <td>
             <%=Languages.getString("jsp.admin.reports.GM5.not_found",SessionData.getLanguage())%>
        </td>
    </tr> 
    <table>
<%
	}
	else
	{
	%>

 	<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
                            <thead>
                                <tr class="SectionTopBorder"> 
	                 <td class=rowhead2 nowrap>#</td>      
<% 
		// Set the column headers
		ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
		if((reportColumns != null) && (reportColumns.size() > 0))
		{
			for(int columnIndex = 0;columnIndex < reportColumns.size();columnIndex++)
			{
%>                    
						<td class=rowhead2 nowrap align=center><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
						</td>
<%
			}
		}
%>
                                </tr>
                            </thead>

                         <%
                        
                         int      intEvenOdd               = 1;
                         int      intCounter               = 1; 
                         
                           Iterator<Vector<Object>> it                       = vecSearchResults.iterator();
                           if(it.hasNext()) {
                        	             
                               Vector<Object> vecTemp = null;
                               
                               while (it.hasNext())                    
                               {                                       
                                   vecTemp = null;              
                                   vecTemp = it.next();        
                                   out.println("<tr class=row" + intEvenOdd +">" +
                                		   "<td>" + intCounter+ "</td>" +
                                           "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                           "<td>" + vecTemp.get(1) + "</td>" +
                                           "<td>" + vecTemp.get(2) + "</td>" +
                                           "<td>" + vecTemp.get(3) + "</td>" +
                                           "<td align=right>" + vecTemp.get(4) + "</td>" +
                                           "<td align=right>" + vecTemp.get(5) + "</td>" +
                                           "<td align=right>" + vecTemp.get(6) + "%</td>" +
                                           "<td align=right>" + vecTemp.get(7) + "</td>" +
                                   "</tr>");
                               if (intEvenOdd == 1)
                               {
                                 intEvenOdd = 2;
                               }
                               else
                               {
                                 intEvenOdd = 1;
                               }
                               intCounter++;
                               
                               }
                           }
                           
                           %>                  
         
                        </table>
</td>
</tr>  
                       </table>
<SCRIPT type="text/javascript">
<!--
  var stT1 = new SortROC(document.getElementById("t1"), ["None","Date","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","Number", "Number","Number","Number"],0,false,false);
-->
</SCRIPT> 
                        
<%
			vecSearchResults.clear();
		}
	}
%>
 
</td>
</tr>  
</table>   

          
                         
<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>