<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.DebisysConfigListener,
                 com.debisys.languages.Languages" %>
<%
int section=2;
int section_page=4;
String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 25;
int intPageCount = 1;
String merchantId = "";
if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
{
  merchantId = request.getParameter("merchantId");
}
 else
{
  merchantId = SessionData.getProperty("ref_id");
  Merchant.setMerchantId(merchantId);
}


String strUrlParams = "?merchantId=" + merchantId + "&startDate=" + Merchant.getStartDate() + "&endDate=" + Merchant.getEndDate();

if (merchantId != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }
  
  SessionData.setProperty("reportId", DebisysConstants.SC_USR_PAYMENT_TOTALS);
  
    if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {  
        vecSearchResults = Merchant.getMerchantCreditsMx(intPage, intPageSize, SessionData, application, false);
    }else{
        vecSearchResults = Merchant.getMerchantCredits(intPage, intPageSize, SessionData, application, false, DebisysConstants.EXECUTE_REPORT, null, null);
    }
    
    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize) + 1;
      if ((intPageCount * intPageSize) + 1 >= intRecordCount)
      {
        intPageCount++;
      }
    }
  }
  else
  {
   searchErrors = Merchant.getErrors();
  }



%>
<html>
  <head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.customers.merchants_credit_history.title",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="700" background="../../images/top_blue.gif">
	<tr>
    <td width="18" height="20" align="left"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" align="left" width="2000"><%=Languages.getString("jsp.admin.customers.merchants_credit_history.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20" align="right"><img src="../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td class="main">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" class="main">
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
</table>
               </td>
              </tr>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage())+"." + Languages.getString("jsp.admin.displaying", new Object []{Integer.toString(intPage), Integer.toString(intPageCount-1)},SessionData.getLanguage())%></td></tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams +"&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams +"&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams + "&page=" + i + "\">" + i + "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams + "&page=" + (intPage+1) + "\">"+Languages.getString("jsp.admin.next",SessionData.getLanguage())+"&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams + "&page=" + (intPageCount-1) + "\">"+Languages.getString("jsp.admin.last",SessionData.getLanguage())+"</a>");
              }

              %>
              </td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
            <%
               if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {  
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.user",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.desc",SessionData.getLanguage()).toUpperCase() + "</td>");
                    
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.bank",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.reference_number",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.deposit_date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.concept",SessionData.getLanguage()).toUpperCase() + "</td>");
                    
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.new_avail_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.commission",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.net_payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + "Representative Type" + "</td>"); 
               }else{
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.user",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.desc",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.new_avail_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.commission",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.net_payment",SessionData.getLanguage()).toUpperCase() + "</td>");            
                    out.println("<td class=rowhead2>" + "Representative Type" + "</td>");
               }
            %> 
            </tr>
            <%
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {  
                        out.println("<tr class=row" + intEvenOdd +">" +
                                    "<td>" + vecTemp.get(0)+ "</td>" +
                                    "<td>" + vecTemp.get(1) + "</td>" +
                                    "<td>" + vecTemp.get(2) + "</td>" +
                                    "<td>" + vecTemp.get(3) + "</td>" +
                                    "<td>" + vecTemp.get(4) + "</td>" +
                                    "<td>" + vecTemp.get(5) + "</td>" +
                                    "<td>" + vecTemp.get(6) + "</td>" +
                                    "<td>" + vecTemp.get(7) + "</td>" +
                                    "<td>" + vecTemp.get(8) + "</td>" +
                                    "<td>" + vecTemp.get(9) + "</td>" +
                                    "<td>" + vecTemp.get(10) + "</td>" +
                                    "<td>" + vecTemp.get(11) + "</td>" +
                            "</tr>");
                    }else{
                        out.println("<tr class=row" + intEvenOdd +">" +
                                    "<td>" + vecTemp.get(0)+ "</td>" +
                                    "<td>" + vecTemp.get(1) + "</td>" +
                                    "<td>" + vecTemp.get(2) + "</td>" +
                                    "<td>" + vecTemp.get(3) + "</td>" +
                                    "<td>" + vecTemp.get(4) + "</td>" +
                                    "<td>" + vecTemp.get(5) + "%</td>" +
                                    "<td>" + vecTemp.get(6) + "</td>" +
                                    "<td>" + vecTemp.get(8) + "</td>" +
                            "</tr>");                        
                    }
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>
            <input type="button" value="<%=Languages.getString("jsp.admin.reports.Print_This_Page",SessionData.getLanguage())%>" onclick="window.print()">

<%
}
else if (intRecordCount==0 && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.customers.merchants_credit_history.not_found",SessionData.getLanguage())+"</font>");
}
%>

</td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
