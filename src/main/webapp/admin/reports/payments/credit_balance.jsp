<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                  com.debisys.utils.*,
                  com.debisys.promotions.*,
                 com.debisys.customers.Merchant" %>
<%
int section=4;
int section_page=10;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Taskhandler" class="com.debisys.tools.CreditReportTaskHandler" scope="request"></jsp:useBean>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
ValidateEmail validateEmail = new ValidateEmail();
  	Vector vecReps = new Vector();
  	Vector vecSubagents = new Vector();
  	String sAgentID = "";
   String sSubagentID = "";
   String jobid ="";
   String opselected="";
   String jobstartDate="";
	String email="";
	String entity="";
	String activity = "";
	boolean firstTime=false;
	String showDisable="";
	String pageSubmittedVal="";
	String job="";
	if (request.getParameter("jobid") != null ) {
  		job = request.getParameter("jobid");
  	}
	if (request.getParameter("PageSubmitted") != null ) {
  		pageSubmittedVal = request.getParameter("PageSubmitted");
  	}
  	
  	if (request.getParameter("showDisable") != null ) {
  		showDisable = request.getParameter("showDisable");
  	}
  	if (request.getParameter("entityR") != null ) {
  		entity= request.getParameter("entityR");
  		}
	if ( request.getParameter("activity") != null )
{
activity = request.getParameter("activity");
if( activity.equals("edit") ){
	//load data
	if( request.getParameter("editId") != null ){
		jobid = request.getParameter("editId");
		opselected= request.getParameter("op");
		Taskhandler.getCreditjob(jobid,strAccessLevel,SessionData.getProperty("ref_id"));
	
	}
}
}
  		Vector vecAgents = new Vector();
  	
  	    vecAgents = Promotion.GetCompanies(Long.valueOf(SessionData.getProperty("ref_id")),4);
  		if (request.getParameter("agentsID") != null ) {
	  		sAgentID = request.getParameter("agentsID");
	  		if (NumberUtil.isNumeric(sAgentID)) {
	  			vecSubagents = Promotion.GetCompanies(Long.parseLong(sAgentID),5);
	  		}		
  		}
  		if (request.getParameter("subsID") != null ) {
  			sSubagentID = request.getParameter("subsID");
  		}
  		if (request.getParameter("op") != null ) {
  		opselected= request.getParameter("op");
  		}
  		
  		if (request.getParameter("email") != null ) {
  		email= request.getParameter("email");
  	
  		}
  		
  	
  		
  		if(!activity.equals("edit"))
  		{
	  		if (request.getParameter("jobstartDate") != null )
	  		 {
	  		jobstartDate= request.getParameter("jobstartDate");
	  		}
	  			Taskhandler.setjobstartDate(jobstartDate);
  	}

  %>
  <script type="text/javascript" src="includes/_lib/jquery.min.js"></script>    
<SCRIPT LANGUAGE="JavaScript"><!--
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function checkEmail(str){
		var regx = /^([\w\-]+\.?)+[\w\-]+@([\w\-]+\.?)+[\w\-]+\.\w{2,4}$/i;
		//var regx =/^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
		 return regx.test(str);
	}
	
function validateEmail1()
{
		var ok= true;
			// email validation
		if(!checkEmail($('#email').val())){
				$('#lblEmailError').show();
			ok = false;
			alert(ok);
		
		}		
		return ok;	

}


function validateEmail(value) {
    var result = value.split(",");
    var ok= true;
    for(var i = 0;i < result.length;i++)
    {
    if(!checkEmail(result[i])) 
    	{	
    		$('#lblEmailError').show();
            return false; 
         }   
       }
    return true;
}


function validateForm()
{
if(document.getElementById('op').options[document.getElementById('op').selectedIndex].value== "1")
	{
	        document.getElementById('show').style.display='inline';
			document.getElementById('show').style.visibility='visible';
			document.getElementById('dateDiv').style.display='none';
			document.getElementById('schedule').style.display='none';
			document.getElementById('emailDiv').style.display='none';
			document.getElementById('emailNotes').style.display='none';
			document.getElementById('schedule').style.display='none';
			
	}else
	{
			document.getElementById('dateDiv').style.display='inline';
			document.getElementById('schedule').style.display='inline';
			document.getElementById('emailDiv').style.display='inline';
			document.getElementById('emailNotes').style.display='inline';
			document.getElementById('show').style.display='none';
			document.getElementById('show').style.visibility='hidden';	
	}
			
	//$('#lblEmailError').hide();
	
}

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}


function showHide()
{
var operation=getoperationvalue();
	if(document.getElementById('op').options[document.getElementById('op').selectedIndex].value== "1")
	{
			document.getElementById('show').style.display='inline';
			document.getElementById('show').style.visibility='visible';
			document.getElementById('dateDiv').style.display='none';
			document.getElementById('schedule').style.display='none';
			document.getElementById('emailDiv').style.display='none';
			document.getElementById('emailNotes').style.display='none';
			document.getElementById('schedule').style.display='none';
			
	}else
	{
			document.getElementById('dateDiv').style.display='inline';
			document.getElementById('schedule').style.display='inline';
			document.getElementById('emailDiv').style.display='inline';
			document.getElementById('emailNotes').style.display='inline';
			document.getElementById('show').style.display='none';
			document.getElementById('show').style.visibility='hidden';	
	}
	

	
}
function submitForm(activity)
{	
			document.getElementById('PageSubmitted').value='1';
            document.getElementById('operation').value=document.getElementById('op').options[document.getElementById('op').selectedIndex].value;
			if(activity=='edit' )
			 {
			 	document.getElementById('op').disabled=true;	
			 	var job=document.getElementById('jobid').value;		 	
				document.mainform.action = 'admin/reports/payments/credit_balance.jsp?op=2&showDisable=1&jobid='+job;
				//document.mainform.action = 'admin/reports/payments/credit_balance.jsp?op=2&showDisable=1';	
				document.mainform.submit();
			 }else if(document.getElementById('op').disabled==true)
			 {
			 	<%if(!job.equals("")){%>
			 	document.mainform.action = 'admin/reports/payments/credit_balance.jsp?op=2&showDisable=1&jobid='+<%=job%>;	
			 	<%}%>		
			 	//document.mainform.action = 'admin/reports/payments/credit_balance.jsp?op=2&showDisable=1';
			 	document.mainform.submit();
			 }
			 else
			 {
			 document.mainform.submit();
			 }

}

function getentityvalue() {
            var inputs = document.getElementsByName("entityR");
            for (var i = 0; i < inputs.length; i++) {
              if (inputs[i].checked) {
                return inputs[i].value;
              }
            }
          }
          
          
          function getoperationvalue() {
            var inputs = document.getElementsByName("op");
            for (var i = 0; i < inputs.length; i++) {
              if (inputs[i].checked) {
                return inputs[i].value;
              }
            }
          }


function generateReport()
{
    var entityId = getentityvalue();
    if(entityId==undefined)
    {
      alert("Please choose an Entity Type");
      return;
    }
  	document.getElementById('entity').value=entityId;
	document.mainform.action = 'admin/reports/payments/credit_balance_report.jsp';
	document.mainform.submit();
}

function reportSchedule()
{
    var entityId = getentityvalue();
    
    if(document.getElementById('jobstartDate').value=='')
    {
      alert("Please choose the job start date");
      return;
    }
    
      if(entityId==undefined)
    {
      alert("Please choose an Entity Type");
      return;
    }
    
    if(document.getElementById('email').value=='')
    {
      alert("Please enter an email id");
      return;
    }
	document.getElementById('entity').value=entityId;
  	document.getElementById('activity').value="save";
  	document.getElementById('operation').value=document.getElementById('op').options[document.getElementById('op').selectedIndex].value;
  	if(validateEmail(document.getElementById('email').value))
	{	
		document.mainform.action = 'admin/reports/payments/credit_balance_scheduler.jsp';
		document.mainform.submit();
	}
}


function editSchedule()
{
    var entityId = getentityvalue();
  	document.getElementById('entity').value=entityId;
  	document.getElementById('activity').value="edit";
  	<%if(!job.equals("")){%>
  	document.getElementById('jobid').value=<%=job%>
  	<%}%>
  	document.getElementById('operation').value=document.getElementById('op').options[document.getElementById('op').selectedIndex].value;
	if(validateEmail(document.getElementById('email').value))
	{
	document.mainform.action = 'admin/reports/payments/credit_balance_scheduler.jsp';
	
	document.mainform.submit();
	}
}

--></SCRIPT>
<body onload="validateForm()">
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.credit.balance.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
		
		<%@ include file="../qcommReportsFragment.jsp" %>
		
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" id="mainform" method="post" action="admin/reports/payments/credit_balance.jsp">
	       <input type="hidden" id="entity" name="entity" value="">	
	      <input type="hidden" id="operation" name="operation" value="">
	      <input type="hidden" id="operationEdit" name="operationEdit" value="">	
	      <input type="hidden" id="activity" name="activity" value="">
	      <input type="hidden" id="showDisable" name="showDisable" value="">
	      <input name="PageSubmitted" id="PageSubmitted" type="hidden" value="0"/>
	       <input type="hidden" id="jobid" name="jobid" value="<%=jobid%>">
	      
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="500">
	          <tr><td class="rowHead2"><a href="/support/admin/reports/payments/credit_balance_scheduler.jsp?activity=view"><%=Languages.getString("jsp.admin.reports.credit.balance.view.jobs",SessionData.getLanguage())%></a></td>
	          <td></td></tr>
	          <tr class="main" style='display:inline'>
								 <td><%=Languages.getString("jsp.admin.reports.credit.balance.action",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
									 <td><select id="op" name="op" onchange="showHide();" <% if(activity.equals("edit") || showDisable.equals("1"))out.print("disabled"); %> >
									  <option value="1"<% if(opselected.equals("1"))out.print("selected");%>><%=Languages.getString("jsp.admin.display.credit.balance.report",SessionData.getLanguage())%> </option>
									  <option value="2"<% if(opselected.equals("2"))out.print("selected");%>><%=Languages.getString("jsp.admin.schedule.credit.balance.report",SessionData.getLanguage())%></option>
									  </select>
									</td>
								 </tr>
				
				 <tr id='dateDiv' name='dateDiv' style='display:none' class="main" >
    	<td nowrap><%=Languages.getString("jsp.admin.tools.s2k.tools.task1",SessionData.getLanguage())+ ":&nbsp;&nbsp;"%></td>
    	<td nowrap><input class="plain" id="jobstartDate" name="jobstartDate" value="<%=Taskhandler.getjobstartDate()%>" size="18"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fJobPop(document.mainform.jobstartDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
   </td>
   </tr>
			 	<%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
	           Iterator it=null;%>
								<tr id='agententities' name='agententities' class="main" style='display:inline'>
									<td><%=Languages.getString("jsp.admin.tools.editpromotions.agentname",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
									<%if (activity.equals("edit")) {%>
									<td><select id="agentsID" name="agentsID" onchange="submitForm('edit');">
									<option value="1"><%=Languages.getString("jsp.admin.tools.editpromotions.view_all_agents",SessionData.getLanguage())%></option>
									<%
										it = vecAgents.iterator();
										while (it.hasNext()) {
											Company compTemp = null;
											compTemp = (Company) it.next();
											if (!Taskhandler.getselectedagent().equals(String.valueOf(compTemp.getCompanyID()))) {
												out.println("<option value=\""+compTemp.getCompanyID()+"\">" +
													HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
											}
											else {
												out.println("<option value=\""+compTemp.getCompanyID()+"\" selected>" +
													HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
											}
										}
										vecAgents.clear();
									%>
									</select></td>
									<%}else{%>
									<td><select id="agentsID" name="agentsID" onchange="submitForm('load');">
									<option value="1"><%=Languages.getString("jsp.admin.tools.editpromotions.view_all_agents",SessionData.getLanguage())%></option>
									<%
										it = vecAgents.iterator();
										while (it.hasNext()) {
											Company compTemp = null;
											compTemp = (Company) it.next();
											if (!sAgentID.equals(String.valueOf(compTemp.getCompanyID()))) {
													out.println("<option value=\""+compTemp.getCompanyID()+"\">" +
														HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
												}
												else {
													out.println("<option value=\""+compTemp.getCompanyID()+"\" selected>" +
														HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
												}

										}
										vecAgents.clear();
									%>
									</select></td>
									<%}%>
									</tr>
									<tr id='subentities' name='subentities' class="main" style='display:block'>
									<td><%=Languages.getString("jsp.admin.tools.editpromotions.subagentname",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
										<%if (activity.equals("edit")) {%>
										<td><select id="subsID" name="subsID" onchange="submitForm('edit');">
										<%
										if(Taskhandler.getselectedsub().equals("1")){%>
										   <option value="1" selected><%=Languages.getString("jsp.admin.tools.editpromotions.view_all_subagents",SessionData.getLanguage())%></option>
										<%}else
										{
										out.println("<option value=\""+Taskhandler.getselectedsub()+"\" selected>" +
														HTMLEncoder.encode(Taskhandler.getselectedsubName(Taskhandler.getselectedsub())) + "</option>");
										}
										
										%>
										</select>
									</td>	
									<%}else{ %>	
									<td><select id="subsID" name="subsID" onchange="submitForm('load');">
										<option value="1"><%=Languages.getString("jsp.admin.tools.editpromotions.view_all_subagents",SessionData.getLanguage())%></option>
										<%
											it = vecSubagents.iterator();
											while (it.hasNext()) {
												Company compTemp = null;
												compTemp = (Company) it.next();
												if (!sSubagentID.equals(String.valueOf(compTemp.getCompanyID()))) {
													out.println("<option value=\""+compTemp.getCompanyID()+"\">" +
														HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
												}
												else {
													out.println("<option value=\""+compTemp.getCompanyID()+"\" selected>" +
														HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
												}
											}
											vecSubagents.clear();
										%>
										</select>
									</td>					
									<%}%>				
								</tr 
								<%} // End if (iIsoType == 3) %>
		       		<tr id="entityVal" name="entityVal" style="display:inline" class="main">
		       		<td nowrap><%=Languages.getString("jsp.admin.reports.credit.balance.entityType",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
		       		<td nowrap><input type="radio" name="entityR" id="entityR"  value="rep" <%if(pageSubmittedVal.equalsIgnoreCase("1")){ if(entity.equals("rep")) { out.print("checked");}} else { if (Taskhandler.getEntity().equals("rep")){ out.print("checked");}}%>><%=Languages.getString("jsp.admin.reports.credit.balance.rep",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="entityR" id="entityR" value="mer" <%if (pageSubmittedVal.equalsIgnoreCase("1")){ if(entity.equals("mer")) { out.print("checked");}} else { if (Taskhandler.getEntity().equals("mer")){ out.print("checked");}}%>><%=Languages.getString("jsp.admin.reports.credit.balance.merchant",SessionData.getLanguage())%>
		       		</td>
		       		</tr>
					 <tr id='emailDiv' name='emailDiv' style="display:none" class="main" >
					   <td><%=Languages.getString("jsp.admin.reports.credit.email",SessionData.getLanguage())+":&nbsp;&nbsp;"%> </td>
					   <td><input class="plain" name="email" id="email" <%if (!pageSubmittedVal.equalsIgnoreCase("1")){ %> value="<%=email.equals("")?Taskhandler.getEmail():email%>" <% } else {%> value="<%= email%>" <% }%>  maxlength="100"  size="60" onchange="validateEmail(<%= email%>)"></td>
   						</tr>	
   						 <tr id='emailNotes' name='emailNotes' style='display:none' class="main" >
              <td colspan="2" class="main">
                <%= Languages.getString("jsp.admin.reports.credit.balance.email.notes",SessionData.getLanguage()) %>
                </font>
              </td>
             </tr>
   			<tr><td><FONT COLOR="#ff0000"><B><span id="lblEmailError" style="display:none;"><%=Languages.getString("jsp.admin.contact.index.error2",SessionData.getLanguage())%><br></span></font></td></tr>
							
    		<tr><td></td></tr>
    		<tr><td></td></tr>
    		
		<tr>
    	<td class='main' colspan='3' align='center'  id='show' name='show' style='display:inline'>
    	 <input type="hidden" name="search" value="y">
	       <input type="submit" name="showReport" id="showReport"  value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>"  onclick="generateReport();">
    	</td>
		</tr>
		<tr>
    	<td class='main' colspan='3' align='center' id='schedule' name='schedule' style='display:none'>
    	<%if(activity.equals("edit") || showDisable.equals("1")) {%>
    	   <input type="submit" name="saveSchedule" id="saveSchedule"  value="<%=Languages.getString("jsp.admin.saveschedule.credit.balance.report",SessionData.getLanguage())%>" onclick="editSchedule();">
    	 <%}else{%>
    	<input type="submit" name="scheduleReport" id="scheduleReport"  value="<%=Languages.getString("jsp.admin.schedule.credit.balance.report",SessionData.getLanguage())%>"  onclick="reportSchedule();">
    	 <%}%>
	      
    	</td>
		</tr>
		
		</table>
		</td></tr>

	</table>
</form>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_time.js" id="gToday:datetime:agenda.js:gfPop:plugins_time.js" src="admin/CalenderXP/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>