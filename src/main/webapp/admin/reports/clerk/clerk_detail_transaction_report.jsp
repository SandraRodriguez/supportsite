<%@ page import="java.util.*,
				 java.text.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.Hashtable,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.DateUtil,
                 com.debisys.dateformat.DateFormat,
                 com.debisys.languages.Languages" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ClerkReport" class="com.debisys.reports.ClerkReport" scope="request"/>
<jsp:setProperty name="ClerkReport" property="*"/>
	<%
		if (SessionData.getProperty("access_level") == null ||
    		SessionData.getProperty("access_level").equals(""))
		{
			session.invalidate();
  			response.sendRedirect("clerk_logout.jsp?siteId=" + ClerkReport.getSiteId());
	  		return;
		}
	%>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage())%></title>
    <LINK href="../../../includes/sortROC.css" type="text/css" rel="StyleSheet"/>
    <SCRIPT SRC="../../../includes/sortROC.js" type="text/javascript"></SCRIPT>
</head>
<body bgcolor="#ffffff">
	<%
		Vector vecSearchResults = new Vector();
		Hashtable clerkErrors = new Hashtable();
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		ClerkReport.setStartDateTime(startDate);
		ClerkReport.setEndDateTime(endDate);
		vecSearchResults = ClerkReport.getClerkDetailTransactionReport(SessionData);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormat.getDateFormat());
        Calendar currentDate = Calendar.getInstance();
        String strStartDateNow = dateFormat.format(currentDate.getTime());
		
        String strMerchantname = "";
        String strMerchantAddress = "";
        Vector vecMerchantInfo = new Vector();
        vecMerchantInfo = ClerkReport.getMerchantInfo(SessionData);
		if (vecMerchantInfo != null && vecMerchantInfo.size() > 0)
		{
			Iterator itMerchant = vecMerchantInfo.iterator();
			while(itMerchant.hasNext())
  			{
    			Vector vecTempMerchant = null;
    			vecTempMerchant = (Vector) itMerchant.next();
    			strMerchantname = vecTempMerchant.get(0).toString();
    			strMerchantAddress = vecTempMerchant.get(1).toString() + "," + vecTempMerchant.get(2).toString() + "," + vecTempMerchant.get(3).toString() + "," + vecTempMerchant.get(4).toString();
  			}
			vecMerchantInfo.clear();
		}
	%>
	<table border="0" cellpadding="0" cellspacing="0" width="700">
		<tr>
    		<td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    		<td background="../../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.clerk.index.report3",SessionData.getLanguage()).toUpperCase()%></td>
    		<td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  		</tr>
				<%
					if (vecSearchResults != null && vecSearchResults.size() > 0)
					{
				%> 
		<tr>
			<td  class=main colspan=3>
				<br><b><%=Languages.getString("jsp.admin.reports.clerk.index.report3",SessionData.getLanguage())%></b>
			</td>
		</tr> 	
		<tr>
			<td  class=main colspan=3>
				<br><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.clerk_id",SessionData.getLanguage())%>:&nbsp;<%=ClerkReport.getClerkId()%> 
			</td>
		</tr> 
		<tr>
			<td  class=main colspan=3>
                            <%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.sales_for",SessionData.getLanguage())%>&nbsp;<%=DateUtil.formatDateTime(ClerkReport.getStartDateTime())%>&nbsp;<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.to",SessionData.getLanguage())%>&nbsp;<%=DateUtil.formatDateTime(ClerkReport.getEndDateTime())%>
			</td>
		</tr> 
		<tr>
			<td  class=main colspan=3>
				<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.merchant_name",SessionData.getLanguage())%>:&nbsp;<%=strMerchantname%>
			</td>
		</tr> 
		<tr>
			<td  class=main colspan=3>
				<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.merchant_address",SessionData.getLanguage())%>:&nbsp;<%=strMerchantAddress%> 
			</td>
		</tr> 
		<tr>
			<td  class=main colspan=3>
				<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.site_id",SessionData.getLanguage())%>:&nbsp;&nbsp;<%=ClerkReport.getSiteId()%> 
			</td>
		</tr> 
		<tr>
			<td  class=main colspan=3>
				<br><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.report_generated",SessionData.getLanguage())%>&nbsp;<%=strStartDateNow%> 
			</td>
		</tr> 
  		<tr>
  			<td class=main colspan=3>
  				<br><br><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.reports_15",SessionData.getLanguage())%>
  			</td>
  		</tr>
				<%
					}
				%>  		
  		<tr>
				<%
					if (vecSearchResults != null && vecSearchResults.size() > 0)
					{
				%>
  			<td colspan="3"  bgcolor="#FFFFFF">				
            			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            				<thead>
            					<tr class="SectionTopBorder">
              						<td class=rowhead2>#</td>
            						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.datetime",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.clerk_name",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.product_name",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.retail",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.profit",SessionData.getLanguage()).toUpperCase()%></td>
            					</tr>
            				</thead>
            				<%
                  				int i = 1;
                  				Iterator it = vecSearchResults.iterator();
                  				int intEvenOdd = 1;
                                double dblTotalRetail = 0;
                                double dblTotalProfit = 0;
                                double dblTotalCost   = 0;                  				
                 				while (it.hasNext())
                  				{
                    				Vector vecTemp = null;
                    				vecTemp = (Vector) it.next();
                    				out.println("<tr class=row" + intEvenOdd +">" +
		                                			"<td>" + i++ + "</td>" +
		                                			"<td nowrap align=right>" + vecTemp.get(0) + "</td>" +
                                                                        "<td nowrap align=right>" + vecTemp.get(1).toString() + "</td>" +
		                                			"<td nowrap align=right>" + vecTemp.get(2).toString() + "</td>" +
                                                                        "<td nowrap align=right>" + vecTemp.get(3).toString() + "</td>" +
                                					"<td nowrap align=right>" + vecTemp.get(4).toString() + "</td>");
                                	out.println("</tr>");
				                    if (intEvenOdd == 1)
                    				{
                      					intEvenOdd = 2;
                    				}
                    				else
                    				{
                      					intEvenOdd = 1;
                    				}
				                    dblTotalRetail += Double.parseDouble(vecTemp.get(5).toString());
				                    dblTotalProfit += Double.parseDouble(vecTemp.get(6).toString());
                  				}
                 				dblTotalCost = dblTotalRetail - dblTotalProfit;
                  				vecSearchResults.clear();
            				%>
            			</table>
          				<SCRIPT type="text/javascript">
          					<!--
          					var stT1 = new SortROC(document.getElementById("t1"),
          					["None","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
         					-->
        				</SCRIPT>
       		</td>
   		</tr>     
				<%
						out.println("<tr><td class=main colspan=3><br><b>" + Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.total_retail",SessionData.getLanguage()) + " " + NumberUtil.formatCurrency(Double.toString(dblTotalRetail)) + "</b></td></tr>");
						out.println("<tr><td class=main colspan=3><br><b>" + Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.total_profit",SessionData.getLanguage()) + " " + NumberUtil.formatCurrency(Double.toString(dblTotalProfit)) + "</b></td></tr>");
						out.println("<tr><td class=main colspan=3><br><b>" + Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.total_cost",SessionData.getLanguage()) + " " + NumberUtil.formatCurrency(Double.toString(dblTotalCost)) + "</b></td></tr>");
					}
					else if (clerkErrors == null || clerkErrors.size()==0)
					{
						out.println("<td class=main colspan=3><br>" + Languages.getString("jsp.admin.reports.clerk.clerk_report.error2",SessionData.getLanguage()));
						out.println("</td>");
						out.println("</tr>");
					}
				%>

   		<tr>
       		<td class=main colspan=3>
				<DIV ID="divButtons" STYLE="display:inline;">
					<br>				
					<br>
				    <input type=button value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.back",SessionData.getLanguage())%>" onClick="history.go(-1)">
 	      			<input type=button value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.print",SessionData.getLanguage())%>" onclick="document.getElementById('divButtons').style.display='none';window.print();document.getElementById('divButtons').style.display='inline';">
      			</DIV>       		
       		</td>   		
   		</tr>
   	</table>
	<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="../../calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
	</iframe>
</body>
</html>