<%@ page import="com.debisys.languages.Languages"%><html>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
    <td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    <td background="../../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea">
            <div class="main">
            <%=Languages.getString("jsp.admin.reports.clerk.index.instructions",SessionData.getLanguage())%><br><br>
            <%
            String siteId = request.getParameter("siteId");
            if (siteId == null) siteId = "";
            String userId = request.getParameter("userId");
            if (userId == null) userId = "";

            String paymentTypeInd = request.getParameter("paymentTypeInd");
            if (paymentTypeInd == null) paymentTypeInd = "";
            String urlString = "";
            if (paymentTypeInd.equalsIgnoreCase("true"))
            {
              urlString = "&paymentTypeInd=True";
            }
            



            %>
            <li><a href="clerk_login.jsp?siteId=<%=siteId + urlString%>"><%=Languages.getString("jsp.admin.reports.clerk.index.report1",SessionData.getLanguage())%></a><br><br>
            <li><a href="clerk_manager_login.jsp?userId=<%=userId+ urlString%>"><%=Languages.getString("jsp.admin.reports.clerk.index.report2",SessionData.getLanguage())%></a><br><br>
            <li><a href="clerk_login.jsp?siteId=<%=siteId%>&transactionReport=true"><%=Languages.getString("jsp.admin.reports.clerk.index.report3",SessionData.getLanguage())%></a><br><br>
			<li><a href="clerk_login.jsp?siteId=<%=siteId%>&transactionReportMerchant=true"><%=Languages.getString("jsp.admin.reports.clerk.index.report5",SessionData.getLanguage())%></a><br><br>
			<li><a href="clerk_login.jsp?siteId=<%=siteId%>&bulkReport=YES"><%=Languages.getString("jsp.admin.reports.clerk.index.report4",SessionData.getLanguage())%></a><br><br>
			</div>


<br>
<br>
<br><br>






          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
