<%@ page import="java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.languages.Languages" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ClerkReport" class="com.debisys.reports.ClerkReport" scope="request"/>
  <jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="ClerkReport" property="*"/>
<%
String strSort = "";
String urlString = "";
if (request.getParameter("paymentTypeInd") != null && request.getParameter("paymentTypeInd").equalsIgnoreCase("true"))
{
  urlString = "&paymentTypeInd=True";
}

if (SessionData.getProperty("access_level") == null ||
    SessionData.getProperty("access_level").equals(""))
{
  session.invalidate();
  response.sendRedirect("clerk_logout.jsp?siteId=" + ClerkReport.getSiteId() + urlString);
  return;
}
%>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage())%></title>
    <LINK href="../../../includes/sortROC.css" type="text/css" rel="StyleSheet"/>
    <SCRIPT SRC="../../../includes/sortROC.js" type="text/javascript"></SCRIPT>
</head>
<body bgcolor="#ffffff">
<%
  Vector<Vector<String>> vecSearchResults = new Vector<Vector<String>>();
  if (ClerkReport.validate(SessionData))
  {
    vecSearchResults = ClerkReport.getClerkDetail(SessionData);
  }
%>
<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
    <td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    <td background="../../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
          <form>
            <DIV ID="divButtons" STYLE="display:inline;">
              <input type="button" value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.back",SessionData.getLanguage())%>" onclick="history.go(-1)">&nbsp;&nbsp;&nbsp;
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
              <INPUT TYPE=button value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.print",SessionData.getLanguage())%>" onclick="document.getElementById('divButtons').style.display='none';window.print();document.getElementById('divButtons').style.display='inline';">
<%
}
%>
            </DIV>
          </form>
          <BR><BR>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
          <%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.reports_15",SessionData.getLanguage())%>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
          <thead>
      	  <tr class="SectionTopBorder">
              <td class=rowhead2>#</td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.tran_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.term_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.date",SessionData.getLanguage()).toUpperCase()%></td>
              
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.clerk_name",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.reference",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.amt",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.product",SessionData.getLanguage()).toUpperCase()%></td>

              <%

              if (SessionData.getProperty("paymentTypeInd").equalsIgnoreCase("true"))
              {
                strSort = ",\"CaseInsensitiveString\"";
              %>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.payment_type",SessionData.getLanguage()).toUpperCase()%></td>
              <%}%>
            </tr>
            </thead>
            <%
                  
                  int intEvenOdd = 1;
                  int i = 1;
                  String deploymentType = com.debisys.utils.DebisysConfigListener.getDeploymentType(application);
                  String customConfigType = com.debisys.utils.DebisysConfigListener.getCustomConfigType(application);
                  String sClerkPassword = "";
                  String sClerkName = "";

                  for (Vector<String> vecTemp: vecSearchResults)
                  {
					sClerkPassword = "****"; 
               	    if(deploymentType.equals(com.debisys.utils.DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(com.debisys.utils.DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
               	    {           	    		
           	    		if ( vecTemp.get(3).length() < 3 )
           	    		{
           	    			sClerkPassword += vecTemp.get(3);
           	    		}
           	    		else
           	    		{
           	    			sClerkPassword += vecTemp.get(3).substring(vecTemp.get(3).length() - 2);
           	    		}
               	    	if ( vecTemp.get(4).equals("") )
               	    	{
               	    		sClerkName = vecTemp.get(3);
               	    	}
               	    	else
               	    	{
               	    		sClerkName = vecTemp.get(4);
               	    	}
               	    }
               	    else
               	    {
             	    	sClerkPassword = vecTemp.get(3);
           	    		sClerkName = vecTemp.get(4);
               	    }
                    out.print("<tr class=row" + intEvenOdd +">" +
                                "<td>" + i++ + "</td>" +
                                "<td nowrap>" + vecTemp.get(0) + "</td>" +
                                "<td nowrap>" + vecTemp.get(1) + "</td>" +
                                "<td nowrap>" + vecTemp.get(2) + "</td>" +
                                
                                "<td nowrap>" + sClerkName + "</td>" +
                                "<td nowrap>" + vecTemp.get(5) + "</td>" +
                                "<td nowrap>" + vecTemp.get(6) + "</td>" +
                                "<td nowrap>" + vecTemp.get(7) + "</td>");

                                  if (SessionData.getProperty("paymentTypeInd").equalsIgnoreCase("true"))
                                  {
                                    out.print("<td nowrap>" + vecTemp.get(8) + "</td>");
                                  }
                    out.println("</tr>");
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>

<%
}
else if (vecSearchResults == null || vecSearchResults.size() == 0)
{
 out.println(Languages.getString("jsp.admin.reports.clerk.clerk_report.error2",SessionData.getLanguage()));
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
          <SCRIPT type="text/javascript">
          <!--
          var stT1 = new SortROC(document.getElementById("t1"),
          ["None", "Number", "Number", "Date", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString"<%=strSort%>],0,false,false);
         -->
        </SCRIPT>
</body>
</html>
