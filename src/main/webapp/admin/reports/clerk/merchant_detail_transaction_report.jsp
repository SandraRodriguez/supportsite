<%@ page import="java.util.*,
			     java.util.ArrayList,
				 java.text.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.Hashtable,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.DateUtil,
                 com.debisys.dateformat.DateFormat,
                 com.debisys.languages.Languages,
                 com.debisys.reports.pojo.MerchantDetailPojo" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="MerchantReport" class="com.debisys.reports.MerchantReport" scope="request"/>
<jsp:setProperty name="MerchantReport" property="*"/>
	<%
		if (SessionData.getProperty("access_level") == null ||
    		SessionData.getProperty("access_level").equals(""))
		{
			session.invalidate();
  			response.sendRedirect("clerk_logout.jsp?siteId=" + MerchantReport.getSiteId());
	  		return;
		}
	%>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage())%></title>
    <LINK href="../../../includes/sortROC.css" type="text/css" rel="StyleSheet"/>
    <SCRIPT SRC="../../../includes/sortROC.js" type="text/javascript"></SCRIPT>
</head>
<body bgcolor="#ffffff">
	<%
		Hashtable clerkErrors = new Hashtable();
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		MerchantReport.setStartDateTime(startDate);
		MerchantReport.setEndDateTime(endDate);
		
		ArrayList<MerchantDetailPojo> transactionsMerchant = MerchantReport.getMerchantDetailTransactionReport(SessionData);
		
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormat.getDateFormat());
        Calendar currentDate = Calendar.getInstance();
        String strStartDateNow = dateFormat.format(currentDate.getTime());
		int sizeTransactions = transactionsMerchant.size();        
	%>
	<table border="0" cellpadding="0" cellspacing="0" width="700">
		<tr>
    		<td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    		<td background="../../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.clerk.index.report5",SessionData.getLanguage()).toUpperCase()%></td>
    		<td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  		</tr>
		<%
			if (transactionsMerchant != null && transactionsMerchant.size() > 0)
			{
		%> 
		<tr>
			<td class="main" colspan="3">
				<br><b><%=Languages.getString("jsp.admin.reports.clerk.index.report3",SessionData.getLanguage())%></b>
			</td>
		</tr> 	
		<!--  
		<tr>
			<td  class="main" colspan="3">
				<br><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.clerk_id",SessionData.getLanguage())%>:&nbsp;<%=MerchantReport.getClerkId()%> 
			</td>
		</tr>
		-->
		<tr>
			<td class="main" colspan="3">
                 <%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.sales_for",SessionData.getLanguage())%>&nbsp;<%=DateUtil.formatDateTime(MerchantReport.getStartDateTime())%>&nbsp;<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.to",SessionData.getLanguage())%>&nbsp;<%=DateUtil.formatDateTime(MerchantReport.getEndDateTime())%>
			</td>
		</tr> 
		<tr>
			<td class="main" colspan="3">
				<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.merchant_name",SessionData.getLanguage())%>:&nbsp;<%=MerchantReport.getMerchantName()%>
			</td>
		</tr> 
		<tr>
			<td  class="main" colspan="3">
				<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.merchant_address",SessionData.getLanguage())%>:&nbsp;<%=MerchantReport.getMerchantAddress()%> 
			</td>
		</tr> 
		<tr>
			<td class="main" colspan="3">
				<br><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.report_generated",SessionData.getLanguage())%>&nbsp;<%=strStartDateNow%> 
			</td>
		</tr> 
  		<tr>
  			<td class="main" colspan="3">
  				<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.reports_15",SessionData.getLanguage())%>
  			</td>
  		</tr>
  		<tr>
  			<td class="main" colspan="3">
  				<%=Languages.getString("jsp.includes.menu.transactions",SessionData.getLanguage())%>&nbsp;<%=sizeTransactions%> 
  			</td>
  		</tr>					
  		<tr>				
  			<td colspan="3"  bgcolor="#FFFFFF">		
  			            <br>		
            			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            				<thead>
            					<tr class="SectionTopBorder">
              						<td class="rowhead2">#</td>
              						<td class="rowhead2"><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.site_id",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class="rowhead2"><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.datetime",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class="rowhead2"><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.clerk_name",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class="rowhead2"><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.product_name",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class="rowhead2"><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.retail",SessionData.getLanguage()).toUpperCase()%></td>
            						<td class="rowhead2"><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.profit",SessionData.getLanguage()).toUpperCase()%></td>
            					</tr>
            				</thead>
            				<%
                  				int i = 0;
                  				
                  				int intEvenOdd = 1;
                                double dblTotalRetailFinal = 0;
                                double dblTotalProfitFinal = 0;
                                
                                double dblTotalRetailSite = 0;
                                double dblTotalProfitSite = 0;
                                double dblTotalCostSite   = 0;
                                
                                double dblTotalCost   = 0;  
                                String siteId = ((MerchantDetailPojo)transactionsMerchant.get(i)).getSiteId();
                                boolean changeSiteId=false;
                                
                                System.out.println("sizeTransactions: "+sizeTransactions);
                                i=1;
                                for(int j=0; j <=sizeTransactions-1;j++ )
                                {    
                                   MerchantDetailPojo transactionMerchant = transactionsMerchant.get(j);
                                   
                                   if (j<sizeTransactions-1)
                                   {     
	                                     MerchantDetailPojo transactionMerchantAux = transactionsMerchant.get(j+1);
	                                     if (!transactionMerchant.getSiteId().equals(transactionMerchantAux.getSiteId()))
	                                     {
	                                        //if the next site is diferent
	                                       changeSiteId = true;
	                                       
	                                     }                                
                                    }
                                    else if (j == sizeTransactions-1)
                                    {
                                      changeSiteId = true;
                                      
                                    }
                                    dblTotalRetailFinal += Double.parseDouble(transactionMerchant.getAmount());
				                    dblTotalProfitFinal += Double.parseDouble(transactionMerchant.getComission());
				                    dblTotalRetailSite += Double.parseDouble(transactionMerchant.getAmount());
				                    dblTotalProfitSite += Double.parseDouble(transactionMerchant.getComission());
				                    
                                   %>
                                   <tr class="row<%=intEvenOdd%>">
                                     <td><%=i%></td>
                                     <td><%=transactionMerchant.getSiteId()%></td>
                                     <td><%=transactionMerchant.getDatetime()%></td>
                                     <td><%=transactionMerchant.getClerk_name()%></td>
                                     <td><%=transactionMerchant.getDescription()%></td>
                                     <td><%=NumberUtil.formatCurrency(transactionMerchant.getAmount())%></td>
                                     <td><%=NumberUtil.formatCurrency(transactionMerchant.getComission())%></td>
                                   </tr>
                                  <%   
                                                                    
                                   siteId = transactionMerchant.getSiteId();  
                                   
                                   if (changeSiteId)
                                   {
                                     dblTotalCostSite = dblTotalRetailSite - dblTotalProfitSite;
                                   %> 
                                      <tr class="rowTotals">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td align="right"><b><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.total",SessionData.getLanguage())%></b></td>
		                                <td align="center"><b><%=NumberUtil.formatCurrency(Double.toString(dblTotalRetailSite))%></b></td>
		                                <td align="center"><b><%=NumberUtil.formatCurrency(Double.toString(dblTotalProfitSite))%></b></td>
                                      </tr>
	                                  <tr class="rowTotals" style="font-size: 12px;">
	                                    <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
	                                    <td align="right" ><b><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.total_cost",SessionData.getLanguage())%></b></td>
	                                    <td  align="center" colspan="2"><b><%=NumberUtil.formatCurrency(Double.toString(dblTotalCostSite))%></b></td>
		                               </tr>
	                                   <tr>
	                                     <td colspan="7" ></td>
	                                   </tr>
                                   <%
                                     dblTotalRetailSite=0;
                                     dblTotalProfitSite=0;
                                     dblTotalCostSite=0;
                                     changeSiteId=false;
                                     i=1;
                                   }
                                   else{
                                     i++;
                                   }
                                   
                                   if (intEvenOdd == 1)
                    			   {	
                      					intEvenOdd = 2;
                    			   }
                    			   else
                    			   {
                      					intEvenOdd = 1;
                    			   }
                    			   
				                   
                                }
                                dblTotalCost = dblTotalRetailFinal - dblTotalProfitFinal;  
                                
                            %>
            			</table>
          				<SCRIPT type="text/javascript">
          					<!--
          					var stT1 = new SortROC(document.getElementById("t1"),
          					["None","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
         					-->
        				</SCRIPT>
       		</td>
   		</tr>     
		<%
		  	  out.println("<tr><td class=main colspan=3><br><b>" + Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.total_retail",SessionData.getLanguage()) + " " + NumberUtil.formatCurrency(Double.toString(dblTotalRetailFinal)) + "</b></td></tr>");
			  out.println("<tr><td class=main colspan=3><br><b>" + Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.total_profit",SessionData.getLanguage()) + " " + NumberUtil.formatCurrency(Double.toString(dblTotalProfitFinal)) + "</b></td></tr>");
			  out.println("<tr><td class=main colspan=3><br><b>" + Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.total_cost",SessionData.getLanguage()) + " " + NumberUtil.formatCurrency(Double.toString(dblTotalCost)) + "</b></td></tr>");
			}
			else if (clerkErrors == null || clerkErrors.size()==0)
			{
				out.println("<td class=main colspan=3><br>" + Languages.getString("jsp.admin.reports.clerk.clerk_report.error2",SessionData.getLanguage()));
				out.println("</td>");
				out.println("</tr>");
			}
		%>

   		<tr>
       		<td class="main" colspan="3">
				<DIV ID="divButtons" STYLE="display:inline;">
					<br>				
					<br>
				    <input type="button" value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_transaction.back",SessionData.getLanguage())%>" onClick="history.go(-1)">
 	      			<input type="button" value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.print",SessionData.getLanguage())%>" onclick="document.getElementById('divButtons').style.display='none';window.print();document.getElementById('divButtons').style.display='inline';">
      			</DIV>       		
       		</td>   		
   		</tr>
   	</table>
	<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="../../calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
	</iframe>
</body>
</html>