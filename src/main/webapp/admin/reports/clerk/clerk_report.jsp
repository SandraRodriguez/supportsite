<%@ page import="java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.Hashtable,
                 com.debisys.utils.NumberUtil, 
                 com.debisys.utils.DebisysConstants, 
                 com.debisys.utils.DebisysConfigListener,                 
                 com.debisys.languages.Languages" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ClerkReport" class="com.debisys.reports.ClerkReport" scope="request"/>
<jsp:setProperty name="ClerkReport" property="*"/>
<%
String urlString = "";
if (request.getParameter("paymentTypeInd") != null && request.getParameter("paymentTypeInd").equalsIgnoreCase("true"))
{
  urlString = "&paymentTypeInd=True";
}
if (SessionData.getProperty("access_level") == null ||
    SessionData.getProperty("access_level").equals(""))
{
  session.invalidate();
  response.sendRedirect("clerk_logout.jsp?siteId=" + ClerkReport.getSiteId() + urlString);
  return;
}
%>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage())%></title>
    <LINK href="../../../includes/sortROC.css" type="text/css" rel="StyleSheet"/>
    <SCRIPT SRC="../../../includes/sortROC.js" type="text/javascript"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

</SCRIPT>

</head>
<body bgcolor="#ffffff">
<%
Vector vecSearchResults = new Vector();
 Hashtable clerkErrors = new Hashtable();

if (request.getParameter("submitted") != null)
{
    if (request.getParameter("submitted").equals("y"))
    {
  		if (ClerkReport.validate(SessionData))
  		{
            String transactionReport = request.getParameter("transactionReport");
            String transactionReportMerchant = request.getParameter("transactionReportMerchant");
            
            if (transactionReport != null && transactionReport.equalsIgnoreCase("true"))
            {
                String urlReport = "clerk_detail_transaction_report.jsp?startDate=" + URLEncoder.encode( ClerkReport.getStartDateTime(), "UTF-8") + "&endDate=" + URLEncoder.encode( ClerkReport.getEndDateTime(), "UTF-8");
                
            	response.sendRedirect(urlReport);
            } 
            else if (transactionReportMerchant != null && transactionReportMerchant.equalsIgnoreCase("true"))
            {
                String urlReport = "merchant_detail_transaction_report.jsp?startDate=" + URLEncoder.encode(ClerkReport.getStartDateTime(), "UTF-8") + "&endDate=" + URLEncoder.encode( ClerkReport.getEndDateTime(), "UTF-8");
                response.sendRedirect(urlReport);
            }
            else
            {
            	vecSearchResults = ClerkReport.getClerkSummary(SessionData);
            }
  		}
  		else
  		{
    		clerkErrors = ClerkReport.getErrors();
  		}
	}
}
%>
<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
    <td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    <td background="../../../images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;
              <%if (request.getParameter("transactionReport") !=null && request.getParameter("transactionReport").equalsIgnoreCase("true")){%>
	               <%=Languages.getString("jsp.admin.reports.clerk.index.report3",SessionData.getLanguage()).toUpperCase()%>
              <%}
              else if ( request.getParameter("transactionReportMerchant") !=null && request.getParameter("transactionReportMerchant").equalsIgnoreCase("true") ){
              %>     
    				<%=Languages.getString("jsp.admin.reports.clerk.index.report5",SessionData.getLanguage()).toUpperCase()%>
              <%
              }
              else
              {
              %>     
    				<%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage()).toUpperCase()%>
              <%}%>         
    
    </td>
    <td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <form name="mainform" method="post" action="clerk_report.jsp" onSubmit="scroll();">
      <input type="hidden" name="siteId" value="<%=ClerkReport.getSiteId()%>">
      <input type="hidden" name="submitted" value="y">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
<%
if (clerkErrors != null)
{
 if (clerkErrors.containsKey("enddate") || clerkErrors.containsKey("startdate") || clerkErrors.containsKey("date"))
 {
    out.println("<font color=ff0000>"+Languages.getString("jsp.admin.reports.clerk.clerk_report.error1",SessionData.getLanguage())+"</font>");
 }

}
%>

	          <table width="300">
              <tr class="main">
               <td nowrap valign="top">
               <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>
		         <%}%>:</td>
               <td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
<table width=400>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.start_date_time",SessionData.getLanguage())%>:</td>
    <td nowrap><input class="plain" name="startDate" value="<%=ClerkReport.getStartDate()%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="../../calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
    <td nowrap>
    <select name=startHour>
    <%
      int startHour = 12;
      int endHour = 11;

      int startMin = 0;
      int endMin = 59;

      String startAmPm = "am";
      String endAmPm = "pm";

      if (NumberUtil.isNumeric(ClerkReport.getStartHour()))
      {
        startHour = Integer.parseInt(ClerkReport.getStartHour());
      }

      for (int i=1;i < 13; i++)
      {
          if (i==startHour)
          {
            out.println("<option value=\"" +i+ "\" selected>" +i+ "</option>");
          }
          else
          {
            out.println("<option value=\"" +i+ "\">" +i+ "</option>");
          }
      }
    %>
    </select>:<select name=startMin>
    <%
      if (NumberUtil.isNumeric(ClerkReport.getStartMin()))
      {
        startMin = Integer.parseInt(ClerkReport.getStartMin());
      }

      for (int i=0;i < 60; i++)
      {
        if (i < 10)
        {
          if (startMin != i)
          {
            out.println("<option value=\"0" +i+ "\">0" +i+ "</option>");            
          }
          else
          {
            out.println("<option value=\"0" +i+ "\" selected>0" +i+ "</option>");  
          }
        }
        else
        {
          if (startMin != i)
          {
            out.println("<option value=\"" +i+ "\">" +i+ "</option>");            
          }
          else
          {
            out.println("<option value=\"" +i+ "\" selected>" +i+ "</option>");  
          }
        }
      }
    %>
    </select>&nbsp;<select name=startAmPm>
    <%
      if (ClerkReport.getStartAmPm() != null && 
          !ClerkReport.getStartAmPm().equals(""))
      {
        startAmPm = ClerkReport.getStartAmPm();
      }

      if (startAmPm.equalsIgnoreCase("am"))
      {
        out.println("<option value=\"am\" selected>AM</option>");
      }
      else
      {
        out.println("<option value=\"am\">AM</option>");
      }
      if (startAmPm.equalsIgnoreCase("pm"))
      {
        out.println("<option value=\"pm\" selected>PM</option>");
      }
      else
      {
        out.println("<option value=\"pm\">PM</option>");
      }

    %>
    </select>
    </td>
</tr>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.end_date_time",SessionData.getLanguage())%>:</td>
    <td nowrap><input class="plain" name="endDate" value="<%=ClerkReport.getEndDate()%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="../../calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
    <td nowrap>
    <select name=endHour>
    <%
      if (NumberUtil.isNumeric(ClerkReport.getEndHour()))
      {
        endHour = Integer.parseInt(ClerkReport.getEndHour());
      }

      for (int i=1;i < 13; i++)
      {
          if (i==endHour)
          {
            out.println("<option value=\"" +i+ "\" selected>" +i+ "</option>");
          }
          else
          {
            out.println("<option value=\"" +i+ "\">" +i+ "</option>");
          }
      }
    %>
    </select>:<select name=endMin>
    <%
      if (NumberUtil.isNumeric(ClerkReport.getEndMin()))
      {
        endMin = Integer.parseInt(ClerkReport.getEndMin());
      }

      for (int i=0;i < 60; i++)
      {
        if (i < 10)
        {
          if (endMin != i)
          {
            out.println("<option value=\"0" +i+ "\">0" +i+ "</option>");
          }
          else
          {
            out.println("<option value=\"0" +i+ "\" selected>0" +i+ "</option>");
          }
        }
        else
        {
          if (endMin != i)
          {
            out.println("<option value=\"" +i+ "\">" +i+ "</option>");
          }
          else
          {
            out.println("<option value=\"" +i+ "\" selected>" +i+ "</option>");
          }
        }
      }
    %>
    </select>&nbsp;<select name=endAmPm>
    <%
      if (ClerkReport.getEndAmPm() != null && 
          !ClerkReport.getEndAmPm().equals(""))
      {
        endAmPm = ClerkReport.getEndAmPm();
      }

      if (endAmPm.equalsIgnoreCase("am"))
      {
        out.println("<option value=\"am\" selected>AM</option>");
      }
      else
      {
        out.println("<option value=\"am\">AM</option>");
      }
      
      if (endAmPm.equalsIgnoreCase("pm"))
      {
        out.println("<option value=\"pm\" selected>PM</option>");
      }
      else
      {
        out.println("<option value=\"pm\">PM</option>");
      }

    %>
    </select>
    </td>
</tr>
<tr class=main>
<td colspan=3>
<%
  String strSort = ",\"CaseInsensitiveString\"";
  if (SessionData.getProperty("paymentTypeInd").equalsIgnoreCase("true"))
  {


  String clerkSelected = "";
  String paymentSelected = "";
  if (ClerkReport.getGroupBy() != null && ClerkReport.getGroupBy().equals("payment"))
  {
    paymentSelected = "checked";
    strSort = "";
  }
  else
  {
    clerkSelected = "checked";
  }
  %>
    <input type="radio" name="groupBy" value="clerk" <%=clerkSelected%>><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.option1",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;<input type="radio" name="groupBy" value="payment" <%=paymentSelected%>><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.option2",SessionData.getLanguage())%>
<%}%>
<br>
</td>
</tr>

<tr>
    <td class=main colspan=3 align=center>
      <input type="hidden" name="search" value="y">
              <%if (request.getParameter("paymentTypeInd") !=null && request.getParameter("paymentTypeInd").equalsIgnoreCase("true"))
                {%>
               <input type="hidden" name="paymentTypeInd" value="true">
              <%}%>
              <%if (request.getParameter("transactionReport") !=null && request.getParameter("transactionReport").equalsIgnoreCase("true"))
                {%>
               <input type="hidden" name="transactionReport" value="true">
              <%}%> 
              <%if (request.getParameter("transactionReportMerchant") !=null && request.getParameter("transactionReportMerchant").equalsIgnoreCase("true"))
                {%>
               <input type="hidden" name="transactionReportMerchant" value="true">
              <%}%> 
              
                           
		<DIV ID="divButtons" STYLE="display:inline;">
              <%if (request.getParameter("transactionReport") !=null && request.getParameter("transactionReport").equalsIgnoreCase("true")){%>
	               <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.submit_transaction_report",SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
              <%}
              else if ( request.getParameter("transactionReportMerchant") !=null && request.getParameter("transactionReportMerchant").equalsIgnoreCase("true") ){
                %>
                  <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.clerk.index.report5",SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
                <% 											   	
              }
              else
              {
              %>     
				<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.submit",SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
              <%}%>  		
<%
				if (vecSearchResults != null && vecSearchResults.size() > 0)
				{
%>
	      <input type=button value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.print",SessionData.getLanguage())%>" onclick="document.getElementById('divButtons').style.display='none';window.print();document.getElementById('divButtons').style.display='inline';">
<%
				}
%>
      	</DIV>
    </td>
</tr>
</table>
               </td>
              </tr>
              </form>
            </table>
          <br>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <font class="main"><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.reports_15",SessionData.getLanguage())%></font>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
            <tr class="SectionTopBorder">
              <td class=rowhead2>#</td>
              <%if (ClerkReport.getGroupBy().equals("payment"))
              {%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.payment_type",SessionData.getLanguage()).toUpperCase()%></td>
              <%}
              else
              {              
              %> 
                <!-- <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.clerk_id",SessionData.getLanguage()).toUpperCase()%></td> -->
                 <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.clerk_name",SessionData.getLanguage()).toUpperCase()%></td>
              <%}%>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.qty",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.total",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            </thead>
            <%
                  int i = 1;
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    if (ClerkReport.getGroupBy().equals("payment"))
                    {
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + i++ + "</td>" +
                                "<td nowrap>" + vecTemp.get(1).toString() + "</td>");
                    out.println("<td nowrap><a href=\"clerk_report_details.jsp?paymentType=" + vecTemp.get(0).toString().trim() + "&groupBy=payment&startDate=" + URLEncoder.encode(ClerkReport.getStartDate(),"UTF-8") + "&startHour=" + ClerkReport.getStartHour() + "&startMin=" + ClerkReport.getStartMin() + "&startAmPm=" + ClerkReport.getStartAmPm() + "&endDate=" + URLEncoder.encode(ClerkReport.getEndDate(), "UTF-8") + "&endHour=" + ClerkReport.getEndHour() + "&endMin=" + ClerkReport.getEndMin() + "&endAmPm=" + ClerkReport.getEndAmPm() + "&siteId=" + ClerkReport.getSiteId() +urlString + "\">" + vecTemp.get(2) + "</a></td>" +
                                "<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(3).toString()) + "</td>" +
                        "</tr>");
                    }
                    else
                    {
                        String deploymentType = com.debisys.utils.DebisysConfigListener.getDeploymentType(application);
                        String customConfigType = com.debisys.utils.DebisysConfigListener.getCustomConfigType(application);
                    	if(deploymentType.equals(com.debisys.utils.DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(com.debisys.utils.DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                    	{
                  			String sClerkPassword = "****";
                  			if ( com.debisys.utils.StringUtil.toString(vecTemp.get(0).toString()).length() < 3 )
                  			{
                  				//sClerkPassword += com.debisys.utils.StringUtil.toString(vecTemp.get(0).toString());
                  			}
                  			else
                  			{
                  				//sClerkPassword += com.debisys.utils.StringUtil.toString(vecTemp.get(0).toString()).substring(com.debisys.utils.StringUtil.toString(vecTemp.get(0).toString()).length() - 2);
                  			}
                    		if(com.debisys.utils.StringUtil.toString(vecTemp.get(1).toString()) == "")
                    		{
	    		                out.println("<tr class=row" + intEvenOdd +">" +
            	                    "<td>" + i++ + "</td>" +
                	                //"<td nowrap>" + sClerkPassword + "</td>" +
                    	            "<td nowrap>" + com.debisys.utils.StringUtil.toString(vecTemp.get(0).toString()) + "</td>");
                    		}
                    		else
                    		{
	    		                out.println("<tr class=row" + intEvenOdd +">" +
	            	                    "<td>" + i++ + "</td>" +
	                	                //"<td nowrap>" + sClerkPassword + "</td>" +
	                    	            "<td nowrap>" + com.debisys.utils.StringUtil.toString(vecTemp.get(1).toString()) + "</td>");                    			
                    		}
                    	}
                    	else
                    	{
    		                out.println("<tr class=row" + intEvenOdd +">" +
            	                    "<td>" + i++ + "</td>" +
                	                //"<td nowrap>" + com.debisys.utils.StringUtil.toString(vecTemp.get(0).toString()) + "</td>"+
                    	            "<td nowrap>" + com.debisys.utils.StringUtil.toString(vecTemp.get(1).toString()) + "</td>");                    	
                    	}
                    	
	                    out.println("<td nowrap><a href=\"clerk_report_details.jsp?clerkId=" + vecTemp.get(0).toString().trim() + "&startDate=" + URLEncoder.encode(ClerkReport.getStartDate(),"UTF-8") + "&startHour=" + ClerkReport.getStartHour() + "&startMin=" + ClerkReport.getStartMin() + "&startAmPm=" + ClerkReport.getStartAmPm() + "&endDate=" + URLEncoder.encode(ClerkReport.getEndDate(), "UTF-8") + "&endHour=" + ClerkReport.getEndHour() + "&endMin=" + ClerkReport.getEndMin() + "&endAmPm=" + ClerkReport.getEndAmPm() + "&siteId=" + ClerkReport.getSiteId() +urlString+ "\">" + vecTemp.get(2) + "</a></td>" +
                                "<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(3).toString()) + "</td>" +
                        "</tr>");
                    }
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>
          <SCRIPT type="text/javascript">
          <!--
          var stT1 = new SortROC(document.getElementById("t1"),
          ["None"<%=strSort%>,"CaseInsensitiveString", "Number", "Number"],0,false,false);
         -->
        </SCRIPT>

<%
}
else if (request.getParameter("search") != null
    && (clerkErrors == null || clerkErrors.size()==0))
{
 out.println("<font class=main>"+ Languages.getString("jsp.admin.reports.clerk.clerk_report.error2",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="../../calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

</body>
</html>
