<%@ page import="java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.Hashtable,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.DateUtil,
                 com.debisys.utils.DebisysConfigListener,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.languages.Languages" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ClerkReport" class="com.debisys.reports.ClerkReport" scope="request"/>

<jsp:setProperty name="ClerkReport" property="*"/>

<%
String urlString = "";
if (request.getParameter("paymentTypeInd") != null && request.getParameter("paymentTypeInd").equalsIgnoreCase("true"))
{
  urlString = "&paymentTypeInd=True";
}
if (SessionData.getProperty("access_level") == null ||
    SessionData.getProperty("access_level").equals(""))
{
  session.invalidate();
  response.sendRedirect("clerk_logout.jsp?siteId=" + ClerkReport.getSiteId() + urlString);
  return;
}
%>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title39",SessionData.getLanguage())%></title>
    <LINK href="../../../includes/sortROC.css" type="text/css" rel="StyleSheet"/>
    <SCRIPT SRC="../../../includes/sortROC.js" type="text/javascript"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}


<%//Following checks defaults odd user input to ''. Application layer interprets empty '' as a wildcard for bulkId.%>
function validateBulkId() {   
    var val = document.mainform.bulkId.value;
    if (isEmpty(val)) {
	   <%//alert('<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.validationError_EmptyBulkId",SessionData.getLanguage())!');return false;%>
	   document.mainform.bulkId.value = '';
	}
	if (isNaN(val)) {
	   document.mainform.bulkId.value = '';
       alert('<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.validationError_NanBulkId",SessionData.getLanguage())%>!');
       document.mainform.bulkId.focus();
       return false;        
    }
    return true;
}

</SCRIPT>

</head>
<body bgcolor="#ffffff">
<%
Vector vecSearchResults = new Vector();
 Hashtable clerkErrors = new Hashtable();

if (request.getParameter("submitted") != null)
{
    if (request.getParameter("submitted").equals("y"))
    {
  			ClerkReport.setBulkId(request.getParameter("bulkId"));
            vecSearchResults = ClerkReport.getBulkTotal(SessionData);
	}
}
%>
<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
    <td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    <td background="../../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;     
    				<%=Languages.getString("jsp.admin.reports.title39",SessionData.getLanguage()).toUpperCase()%>
    </td>
    <td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <form name="mainform" method="post" action="clerk_bulk_report.jsp">
      <input type="hidden" name="siteId" value="<%=ClerkReport.getSiteId()%>">
      <input type="hidden" name="submitted" value="y">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea">
<%
if (clerkErrors != null)
{
 if (clerkErrors.containsKey("enddate") || clerkErrors.containsKey("startdate") || clerkErrors.containsKey("date"))
 {
    out.println("<font color=ff0000>"+Languages.getString("jsp.admin.reports.clerk.clerk_report.error1",SessionData.getLanguage())+"</font>");
 }

}
%>

	          <table width="300">
              <tr class="main">
               <td nowrap valign="top">
               <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>
		         <%}%>:
								</td>
               
               </tr>
               <tr>
               <td valign="top" nowrap>
								<table width=400>
									<tr class="main">
										<td nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.bulkNo",SessionData.getLanguage())%>:
										</td>
										<td nowrap><input type="text" name="bulkId" value="<%=ClerkReport.getBulkId()%>"></td>

									</tr>
									<tr class="main">
										<td nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.start_date_time",SessionData.getLanguage())%>:</td>
										<td nowrap><input class="plain" name="startDate"
											value="<%=ClerkReport.getStartDate()%>" size="12"><a
											href="javascript:void(0)"
											onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;"
											HIDEFOCUS><img name="popcal" align="absmiddle"
											src="../../calendar/calbtn.png" width="34" height="22"
											border="0" alt=""></a></td>

									</tr>
									<tr class="main">
										<td nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.end_date_time",SessionData.getLanguage())%>:</td>
										<td nowrap><input class="plain" name="endDate"
											value="<%=ClerkReport.getEndDate()%>" size="12"><a
											href="javascript:void(0)"
											onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;"
											HIDEFOCUS><img name="popcal" align="absmiddle"
											src="../../calendar/calbtn.png" width="34" height="22"
											border="0" alt=""></a></td>

									</tr>



									<tr>
										<td colspan="2" align="center"><input type="submit" onClick="return validateBulkId();" value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report.view",SessionData.getLanguage()) %>"><input type="hidden" name="clerkId" value="<%=request.getParameter("clerkId")%>" /> </td>
										
									</tr>
								</table>
								</td>
              </tr>
              
              </form>
            </table>
          <br>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
            <tr class="SectionTopBorder">
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.bulkNo",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.status",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.date",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            </thead>
<%
Iterator it = vecSearchResults.iterator();
int intEvenOdd = 1;
while(it.hasNext()) {
	Vector temp = (Vector) it.next();
	String bulkId = (String) temp.get(0);
	String status = (String) temp.get(1);
	
	out.print("<tr class=row" + intEvenOdd +">");
	out.print("<td nowrap><a href='clerk_bulk_detail_report.jsp?bulkId="+bulkId+"&status="+status+"' >"+bulkId+"</a></td>");
	out.print("<td nowrap>"+temp.get(1)+"</td>");
        out.print("<td nowrap>"+temp.get(2)+"</td>");
	out.print("</tr>");
	if (intEvenOdd == 1)
    {
      intEvenOdd = 2;
    }
    else
    {
      intEvenOdd = 1;
    }
}
%>
<tr>

</tr>
</table>
			
 <%}
 else 
{
if(vecSearchResults.size() == 0 && request.getParameter("submitted") != null && request.getParameter("submitted").equals("y"))
	out.println(Languages.getString("jsp.admin.reports.clerk.clerk_report.errorBulk",SessionData.getLanguage()));
}%>
    
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="../../calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

</body>
</html>