<%@ page import="com.debisys.customers.CustomerSearch,
                 java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.Hashtable,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.languages.Languages" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ClerkReport" class="com.debisys.reports.ClerkReport" scope="request"/>
<jsp:setProperty name="ClerkReport" property="*"/>
<%
            String paymentTypeInd = request.getParameter("paymentTypeInd");
            if (paymentTypeInd == null) paymentTypeInd = "";
            String urlString = "";
            if (paymentTypeInd.equalsIgnoreCase("true"))
            {
              urlString = "paymentTypeInd=True";
            }

%>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
    <td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    <td background="../../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;CLERK LOGOUT</td>
    <td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
            <div class="main">
            <br>
            <%=Languages.getString("jsp.admin.reports.clerk.clerk_logout.instructions",SessionData.getLanguage())%>
            <br>
            <a href="clerk_login.jsp?<%=urlString%>&siteId=<%=request.getParameter("siteId")%>"><%=Languages.getString("jsp.admin.reports.clerk.clerk_logout.clerk_login",SessionData.getLanguage())%></a><br>
            <a href="clerk_manager_login.jsp?<%=urlString%>"><%=Languages.getString("jsp.admin.reports.clerk.clerk_logout.clerk_manager_login",SessionData.getLanguage())%></a><br>
            </div>


<br>
<br>
<br><br>






          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
