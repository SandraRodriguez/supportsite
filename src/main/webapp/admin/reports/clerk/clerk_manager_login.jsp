<%@ page import="com.debisys.customers.CustomerSearch,
                 java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.Hashtable,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.languages.Languages" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ClerkReport" class="com.debisys.reports.ClerkReport" scope="request"/>
<jsp:setProperty name="ClerkReport" property="*"/>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title27",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<%
Hashtable clerkErrors = null;
String urlString = "";
if (request.getParameter("submitted") != null)
{
  try
  {
    if (request.getParameter("submitted").equals("y"))
    {
      if (ClerkReport.validateClerkManager())
        {
            SessionData.clear();
            SessionData.setProperty("siteId", ClerkReport.getSiteId());
            SessionData.setProperty("access_level", DebisysConstants.CLERK_MANAGER);
            String paymentTypeInd = request.getParameter("paymentTypeInd");
            if (paymentTypeInd != null && paymentTypeInd.equalsIgnoreCase("true"))
            {
              SessionData.setProperty("paymentTypeInd", "true");
              urlString = "&paymentTypeInd=True";
            }
            else
            {
              SessionData.setProperty("paymentTypeInd", "");

            }
            response.sendRedirect("clerk_report.jsp?siteId=" + ClerkReport.getSiteId() + urlString );
            return;
        }
      else
      {
        clerkErrors = ClerkReport.getErrors();
      }
    }

  }
  catch (Exception e)
  {
  }

}

%>
<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
    <td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    <td background="../../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.clerk.clerk_manager_login.clerk_manager_login",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="login" method="post" action="clerk_manager_login.jsp" >
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
            <div class="main"><br><%=Languages.getString("jsp.admin.reports.clerk.clerk_manager_login.instructions",SessionData.getLanguage())%></div><br>
<%
if (clerkErrors != null)
{
 if (clerkErrors.containsKey("userId"))
 {
    out.println("<font color=ff0000>"+Languages.getString("jsp.admin.reports.clerk.clerk_manager_login.error1",SessionData.getLanguage())+"</font>");
 }
 else if (clerkErrors.containsKey("password"))
 {
    out.println("<font color=ff0000>"+Languages.getString("jsp.admin.reports.clerk.clerk_manager_login.error2",SessionData.getLanguage())+"</font>");
 }
 else if (clerkErrors.containsKey("login"))
 {
    out.println("<font color=ff0000>"+Languages.getString("jsp.admin.reports.clerk.clerk_manager_login.error3",SessionData.getLanguage())+"</font>");
 }
 else
 {
  out.println("<font color=ff0000>"+Languages.getString("jsp.index.error_input",SessionData.getLanguage())+"</font>");
 }

}
%>

	          <table width="100%">
              <tr class="main">
               <td width="100" nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_manager_login.user_id",SessionData.getLanguage())%>:</td><td><input type="text" name="userId" value="<%=ClerkReport.getUserId()%>" size="20" maxlength="15"><%if (clerkErrors != null && clerkErrors.containsKey("userId")) out.print("<font color=ff0000>*</font>");%>&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr class="main">
               <td width="100" nowrap><%=Languages.getString("jsp.admin.reports.clerk.clerk_manager_login.password",SessionData.getLanguage())%>:</td><td><input type="password" name="password" value="<%=ClerkReport.getPassword()%>" size="20" maxlength="15"><%if (clerkErrors != null && clerkErrors.containsKey("password")) out.print("<font color=ff0000>*</font>");%>&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <%if (request.getParameter("paymentTypeInd") !=null && request.getParameter("paymentTypeInd").equalsIgnoreCase("true"))
                {%>
               <input type="hidden" name="paymentTypeInd" value="true">
              <%}%>
              
              <input type="hidden" name="submitted" value="y">
            </table>
            <table width="100%">
             <tr class="main">
              <td><center><input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_manager_login.login",SessionData.getLanguage())%>"></center></td>
             </tr>
            </table>
            </form>
        <br><br>




          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
