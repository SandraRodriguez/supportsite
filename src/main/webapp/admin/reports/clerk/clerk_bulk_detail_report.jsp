<%@ page import="java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.Hashtable,
                 com.debisys.utils.NumberUtil,
                 com.debisys.languages.Languages" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<jsp:useBean id="ClerkReport" class="com.debisys.reports.ClerkReport" scope="request"/>
<jsp:setProperty name="ClerkReport" property="*"/>
<%
String urlString = "";
if (request.getParameter("paymentTypeInd") != null && request.getParameter("paymentTypeInd").equalsIgnoreCase("true"))
{
  urlString = "&paymentTypeInd=True";
}
if (SessionData.getProperty("access_level") == null ||
    SessionData.getProperty("access_level").equals(""))
{
  session.invalidate();
  response.sendRedirect("clerk_logout.jsp?siteId=" + ClerkReport.getSiteId() + urlString);
  return;
}
%>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.title39",SessionData.getLanguage())%></title>
    <LINK href="../../../includes/sortROC.css" type="text/css" rel="StyleSheet"/>
    <SCRIPT SRC="../../../includes/sortROC.js" type="text/javascript"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

</SCRIPT>

</head>
<body bgcolor="#ffffff">
<%
Vector vecSearchResults = new Vector();
 Hashtable clerkErrors = new Hashtable();


ClerkReport.setBulkId(request.getParameter("bulkId"));
vecSearchResults = ClerkReport.getBulkDetail(SessionData);

%>
<table border="0" cellpadding="0" cellspacing="0" width="700">
	<tr>
    <td width="18" height="20"><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
    <td background="../../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;     
    				<%=Languages.getString("jsp.admin.reports.title39",SessionData.getLanguage()).toUpperCase()%>
    </td>
    <td width="12" height="20"><img src="../../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
      <input type="hidden" name="siteId" value="<%=ClerkReport.getSiteId()%>">
      <input type="hidden" name="submitted" value="y">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
      
     	<tr>
     	
	        <td class="formArea2" colspan="2">
<%
if (clerkErrors != null)
{
 if (clerkErrors.containsKey("enddate") || clerkErrors.containsKey("startdate") || clerkErrors.containsKey("date"))
 {
    out.println("<font color=ff0000>"+Languages.getString("jsp.admin.reports.clerk.clerk_report.error1",SessionData.getLanguage())+"</font>");
 }

}
%>
<table>
<tr class="main">
    <td>
   <%=Languages.getString("jsp.admin.reports.clerk.clerk_report.bulkNo",SessionData.getLanguage())%>:
    </td>
    <td><%=ClerkReport.getBulkId() %></td>
    </tr>
    <tr class="main">
    <td><%=Languages.getString("jsp.admin.reports.clerk.clerk_report.status",SessionData.getLanguage())%>:</td>
    <td><%=request.getParameter("status") %></td>
    </tr>
</table>
	          
          <br>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
            <tr class="SectionTopBorder">
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.phonenumber",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.amt",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.tran_id",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.date",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.reference",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.response",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            </thead>
<%
Iterator it = vecSearchResults.iterator();
int intEvenOdd = 1;
while(it.hasNext()) {
	Vector temp = (Vector) it.next();
	out.print("<tr class=row" + intEvenOdd +">");
	out.print("<td nowrap>"+temp.get(0)+"</td>");
	out.print("<td nowrap>"+temp.get(1)+"</td>");
	out.print("<td nowrap>"+temp.get(2)+"</td>");
	out.print("<td nowrap>"+temp.get(3)+"</td>");
	out.print("<td nowrap>"+temp.get(4)+"</td>");
	out.print("<td nowrap>"+temp.get(5)+"</td>");
	out.print("</tr>");
	if (intEvenOdd == 1)
    {
      intEvenOdd = 2;
    }
    else
    {
      intEvenOdd = 1;
    }
}
%>
</table>
			
 <%}
 else 
{
if(vecSearchResults.size() == 0)
	out.println(Languages.getString("jsp.admin.reports.clerk.clerk_report.errorBulk",SessionData.getLanguage()));
}%>
    
</td>
</tr>
<tr>
<td><input type="button" value="<%=Languages.getString("jsp.admin.reports.clerk.clerk_report_details.return",SessionData.getLanguage()) %>" onclick="history.back() "  /></td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="../../calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

</body>
</html>