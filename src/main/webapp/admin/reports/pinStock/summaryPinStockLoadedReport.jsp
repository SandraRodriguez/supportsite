<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.*,
                 com.debisys.reports.PinReports,
                 com.debisys.reports.pojo.ReportPinStock,
                 com.debisys.reports.pojo.PinStockPojo,
                 com.debisys.schedulereports.ScheduleReport,
                 com.debisys.reports.TransactionReport,java.text.SimpleDateFormat" %>
      
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 4;
  int section_page = 38;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  
<%@ include file="/includes/security.jsp" %>

<%
  String keyLanguage = "jsp.admin.reports.pinstock.title.pinStockLoaded";
  ReportPinStock report = new ReportPinStock();  
  Hashtable searchErrors     = null;
  String    sortString       = "";
  int noOfColumns=0;
  boolean bShowLabelLimitDays=false;	  
  int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays( DebisysConstants.SC_PIN_STOCK_LOADED, application );
   
  String strProductIds[] = request.getParameterValues("pids");	       
  if (strProductIds != null)
  {
  	String strTemp = "";
	if (strProductIds != null && strProductIds.length > 0)
	{
		boolean isFirst = true;
		for (int i = 0; i < strProductIds.length; i++)
		{
			if (strProductIds[i] != null && !strProductIds[i].equals(""))
			{
				if (!isFirst)
				{
					strTemp = strTemp + "," + strProductIds[i];
				}
				else
				{
					strTemp = strProductIds[i];
					isFirst = false;
				}
			}
		}
		SessionData.setProperty("pids", strTemp);
	}	  
  }
  	
  if ( request.getParameter("Download") != null )
  { 
	 String sURL = ""; 	 
	 report = PinReports.getPinStockReport(SessionData, application, false, DebisysConstants.EXECUTE_REPORT);
	 Vector vecSearchResults = new Vector(); 
	 vecSearchResults.add(report);
	 sURL = TransactionReport.downloadReportCVS(application, vecSearchResults, 27, SessionData);
     response.sendRedirect(sURL);
	 return;
  }
  
  if (request.getParameter("search") != null)
  {
  		String startDate = request.getParameter("startDate");
  		String endDate = request.getParameter("endDate");	
  				
  		String strFormat="MM/dd/yyyy";
  		SimpleDateFormat format = new SimpleDateFormat(strFormat);
  		int daysBetween = DateUtil.getDaysBetween(endDate, startDate);		
		if (daysBetween > limitDays)
		{
			startDate = DateUtil.addSubtractDays(endDate, -limitDays);
			bShowLabelLimitDays = true;
		}	
		Date date1 = format.parse(startDate);
		Date date2 = format.parse(endDate);		
		startDate = format.format(date1);
		endDate = format.format(date2);
				    
	    SessionData.setProperty("start_date", startDate);
	    SessionData.setProperty("end_date", endDate);
	    SessionData.setProperty("order_no",request.getParameter("orderNo"));
	    
	    if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	    {
	  		//TO SCHEDULE REPORT	  		
			PinReports.getPinStockReport(SessionData, application, false, DebisysConstants.SCHEDULE_REPORT );
			ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
			if (  scheduleReport != null  )
			{
				scheduleReport.setTitleName( keyLanguage );
			}	
			response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
	    }
	    else
	    {
	  		report = PinReports.getPinStockReport(SessionData, application, false, DebisysConstants.EXECUTE_REPORT );
	    }	  		
  }
  
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if ( report != null  )
  {
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="550">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="3000">
        &nbsp;
        <%= Languages.getString(keyLanguage,SessionData.getLanguage()).toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        if ( report != null  )
        {
        	StringBuilder messages = new StringBuilder();
        	for( String warn : report.getWarnings() )
        	{
        		messages.append(warn + "<br/>");
        	}
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
              <td class="main">
                 
                 <%if (bShowLabelLimitDays){ %>
                 <Font style="color: red">
                	* <%=Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage())%>
                                                <%=" "+limitDays+" "%>
                                                <%=Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage())%>
                 </Font>
                 <%} %>   
                <br>
            <table>
              <tr><td><%= messages.toString() %></td></tr>
              <tr>
                <td>
                <FORM ACTION="admin/reports/pinStock/summaryPinStockLoadedReport.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	<INPUT TYPE=hidden NAME=orderNo VALUE="<%=request.getParameter("orderNo")%>">
                	<INPUT TYPE=hidden NAME=Download VALUE="Y">
                	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.pinstock.download",SessionData.getLanguage())%>">
                </FORM>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
              </tr>
            </table>
                <br>
                <br>
                <%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
           		<thead>
		              <tr class="SectionTopBorder">
		              <%
		           		for(String title : report.getTitles() )
		           		{
		           		   %>
		           		   <td class=rowhead2 nowrap width="35%">
			                  <%= title.toUpperCase() %>
			               </td>
		           	   <% 
		           		}
		           	   %>
		           	   </tr>		               
               </thead>
	           <tbody>
		              
               <%
               int intEvenOdd=1;
               for(PinStockPojo data : report.getData() )
           	   {
           		   %>
           		   <tr class="row<%=intEvenOdd%>">	
           		     <% 
           			 for(String valueReport : data.getData() )
           			 {
           	   		 %>
           		      <td align="center" nowrap width="35%" >
		                  <%= valueReport %>
		               </td>
		             <% 
           			  }
           	   		 %>  
	               </tr>
	            <%
	            	if (intEvenOdd == 1)
	                {
	                  intEvenOdd = 2;
	                }
	                else
	                {
	                  intEvenOdd = 1;
	                }		           	    
           		}
           	    %>		               
               </tbody>		
          </table>
<%
        }
        else
        if ( report == null && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (report != null )
        {
%>
  <SCRIPT type="text/javascript">   
    var test=["None", "CaseInsensitiveString", "CaseInsensitiveString"];
  	for (var i = 0; i<<%=(noOfColumns-2)%>; i++) 
    {
    	test.push("Number"); 
    }	 
    var stT1 = new SortROC(document.getElementById("t1"),test,0,false,false);
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
