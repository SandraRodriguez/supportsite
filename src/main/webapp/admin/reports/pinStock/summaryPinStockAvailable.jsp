<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone,
				java.text.SimpleDateFormat"%>
<%
  int section      = 4;
  int section_page = 38;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <jsp:useBean id="Properties" class="com.debisys.properties.Properties" scope="request" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";
  int noOfColumns=0;
  
  String instance = DebisysConfigListener.getInstance(request.getSession().getServletContext());
  
	int daysBackward;
	try{
		daysBackward = Integer.parseInt(Properties.getPropertyValue(instance, "global","PinStockReportDaysBackward"));
	}catch(Exception ex){daysBackward = 90;}
   
  Calendar dateTo = Calendar.getInstance();
  Calendar dateFrom;
    
	dateTo.set(Calendar.HOUR_OF_DAY, 0);
	dateTo.set(Calendar.MINUTE, 0);
	dateTo.set(Calendar.SECOND, 0);
	dateTo.set(Calendar.MILLISECOND, 0);
	dateTo.add(Calendar.DATE, 1);
	
	String strFormat="MM/dd/yyyy";
	SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
  	String endDate = sdf.format(dateTo.getTime());
  	
	
	dateTo.add(Calendar.DATE, daysBackward * -1);
	dateFrom=dateTo;
  	String startDate = sdf.format(dateFrom.getTime());
  	
  
  
  TransactionReport.setStartDate(startDate);
  TransactionReport.setEndDate(endDate);
  
  if ( request.getParameter("Download") != null )
  {
  		SessionData.setProperty("start_date", startDate);
	      SessionData.setProperty("end_date", endDate);
	       String strProductIds[] = request.getParameterValues("pids");
	       
		    if (strProductIds != null)
		    {
		      TransactionReport.setProductIds(strProductIds);
		    }
		      SessionData.setProperty("order_no",request.getParameter("orderNo"));
	String sURL = TransactionReport.downloadPinStockSummary(application, SessionData,true);
	 response.sendRedirect(sURL);
	  return;
  }
  
  if (request.getParameter("search") != null)
  {
	      SessionData.setProperty("start_date", startDate);
	      SessionData.setProperty("end_date", endDate);
	       String strProductIds[] = request.getParameterValues("pids");
	       
		    if (strProductIds != null)
		    {
		      TransactionReport.setProductIds(strProductIds);
		    }
		      SessionData.setProperty("order_no",request.getParameter("orderNo"));
	       vecSearchResults = TransactionReport.getPinStockReport(SessionData,true);
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="550">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="3000">
        &nbsp;
        <%= Languages.getString("jsp.admin.reports.pinstock.title.pinStockAvailable",SessionData.getLanguage()).toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
     <tr>
              <td class="main">
                <%= Languages.getString("jsp.admin.reports.pinstock.title.pinStockAvailableReport",SessionData.getLanguage()) %>
<%
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                	
                  out.println(Languages.getString("jsp.admin.at",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getCurrentDateFormatted()) );

                }
%>
                <br>
            <table>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>
                <FORM ACTION="admin/reports/pinStock/summaryPinStockAvailable.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	<INPUT TYPE=hidden NAME=startDate VALUE="<%=startDate%>">
                	<INPUT TYPE=hidden NAME=endDate VALUE="<%=endDate%>">
                	<INPUT TYPE=hidden NAME=orderNo VALUE="<%=request.getParameter("orderNo")%>">
                	<INPUT TYPE=hidden NAME=pids VALUE="<%=request.getParameter("pids")%>">
                	<INPUT TYPE=hidden NAME=Download VALUE="Y">
                	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.pinstock.download",SessionData.getLanguage())%>">
                </FORM>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
              </tr>
            </table>
                <br>
                <br>
                <%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
               <td class=rowhead2 width="4%">
                  #
                </td>
                 <td class=rowhead2 nowrap width="35%">
                  <%= Languages.getString("jsp.admin.reports.pinstock.loadDate",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap width="20%">
                  <%= Languages.getString("jsp.admin.reports.pinstock.orderNo",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <% Iterator it  = vecSearchResults.iterator();
                 while(it.hasNext())
                 {
                 	 Vector vecTemp=null;
                    vecTemp = (Vector)it.next();
                    for(int i=2;i<vecTemp.size();i++)
                    {
                    out.println("<td class=rowhead2 nowrap width='16%'>"+vecTemp.get(i).toString());
                    }
                 break;
                 }
                 %>
              </tr>
            </thead>
<%
          it  = vecSearchResults.iterator();
            
            int      intEvenOdd               = 1;
            int      intCounter               = 1;
			if (it.hasNext())
    	it.next();
    	int[] totalArray= new int[100];
    	noOfColumns=0;
    	int firstRow=0;
	while (it.hasNext())
            {
               Vector vecTemp1 = null;
	              vecTemp1 = (Vector)it.next();
	              	out.print("<tr class=row" + intEvenOdd + ">");
	              	 	out.print("<td>" + intCounter++ + "</td>");
	           for(int i=0;i<vecTemp1.size();i++)
				{		
						if(firstRow==0)
						{
						 noOfColumns++;
						}	
						out.print("<td nowrap>" + vecTemp1.get(i) + "</td>");
						if(i>1)
						{
						 	totalArray[i-2]=totalArray[i-2]+Integer.parseInt(vecTemp1.get(i).toString());
						}
						
               			
				}
				firstRow=1;
				if (intEvenOdd == 1)
                {
                  intEvenOdd = 2;
                }
                else
                {
                  intEvenOdd = 1;
                }
                out.print("</tr>");
              }
%>
			<tfoot>
              <tr class=row<%= intEvenOdd %>>
                <td colspan=2 align=right>
                  <%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                </td>
                <td align=left>&nbsp;</td>
                <%for (int j=0;j<(noOfColumns-2);j++) {%>
                <td>
                  <%= totalArray[j]%>
                </td>
                <%}%>
              </tr>
            </tfoot>
	      
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">   
          var test=["None", "CaseInsensitiveString", "CaseInsensitiveString"];
		 for (var i = 0; i<<%=(noOfColumns-2)%>; i++) 
             {
             	test.push("Number"); 
             }	 
		 var stT1 = new SortROC(document.getElementById("t1"),
  test,0,false,false);
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
