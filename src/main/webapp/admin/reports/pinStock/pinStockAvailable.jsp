<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.customers.Merchant,
                  com.debisys.reports.TransactionReport,
                 java.util.*" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=38;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

</SCRIPT>

<%
	String urlSummaryReport = "admin/reports/pinStock/summaryPinStockAvailable.jsp";
	urlSummaryReport = "admin/reports/pinStock/summaryPinStockAvailableReport.jsp";
	
%>

	<table border="0" cellpadding="0" cellspacing="0" width="750">
		  <tr>
		    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.pinstock.title.pinStockAvailableReport",SessionData.getLanguage()).toUpperCase()%></td>
		    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
		  </tr>
		  <tr>
		  	<td colspan="3"  bgcolor="#FFFFFF">
		  	   <form name="mainform" method="post" action="<%= urlSummaryReport %>" onSubmit="scroll();">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
				  <tr>
				    <td>
						<tr class="main">
						    <td nowrap><%=Languages.getString("jsp.admin.reports.pinstock.orderNo",SessionData.getLanguage())%>: </td><td> <input class="plain" name="orderNo" value="" size="12"></td>
						</tr>
						<tr>
						    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.products.option",SessionData.getLanguage())%></td>
						    <td class=main valign=top>
						        <select name="pids" size="10" multiple>
						          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
									<%
									  SessionData.setProperty("pins_only", Boolean.TRUE.toString()); 
									  Vector vecProductList = TransactionReport.getProductList(SessionData, null);
									  SessionData.setProperty("pins_only", null);
									  Iterator it = vecProductList.iterator();
									  while (it.hasNext())
									  {
									    Vector vecTemp = null;
									    vecTemp = (Vector) it.next();
									    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "("+vecTemp.get(0)+")</option>");
									  }
									
									%>
						        </select>
						        <br>
						        *<%=Languages.getString("jsp.admin.reports.transactions.products.instructions",SessionData.getLanguage())%>
							</td>
						</tr>
				
							<tr>
							    <td class=main align=center>
							      <input type="hidden" name="search" value="y">
							      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="validateSchedule(0);">
							    </td>
							    <jsp:include page="/admin/reports/schreportoption.jsp">
								  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
								 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>								 	  								 	  
								</jsp:include>
							</tr>
							<tr>
							<td class="main" colspan=2>
							* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
							</td>
							</tr>
				</table>
				</form>
		      </td>
		     </tr>    
  	</table>

          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>