<%-- 
    Document   : streetlightSnapshotFrom.jsp
    Created on : Apr 24, 2017, 2:17:03 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.reports.streetlightSnapshot.StreetlightSnapshotModelGray"%>
<%@page import="net.emida.supportsite.mvc.UrlPaths"%>
<%@page import="com.debisys.reports.streetlightSnapshot.StreetlightSnapshotModel"%>
<%@page import="com.debisys.reports.streetlightSnapshot.StreetlightSnapshotFactory"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<style type="text/css">
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }
    #spanInfo{
        font-size: 20px;
        color: #088A08;
    }

</style>

<%
    String strRefId = SessionData.getProperty("ref_id");
    List<StreetlightSnapshotModel> modelList = StreetlightSnapshotFactory.getReport(SessionData);
    List<StreetlightSnapshotModelGray> modelGray = StreetlightSnapshotFactory.getInactiveMerchants("", "", strRefId, false);
%>


<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp; <%=Languages.getString("jsp.admin.reports.flow.streetlightSnapshotReport", SessionData.getLanguage())%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
            <form id="mainform" name="mainform" class="reportForm" name="mainform" method="post" action="" >
                <table border="0" width="100%" cellpadding="0" cellspacing="0">

                    <tr>
                        <td>
                            <%=Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.infoGray", SessionData.getLanguage())%>
                            <br>
                            <%=Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.infoColors", SessionData.getLanguage())%>
                        </td>
                        <td align="center">
                            <img src="images/flowSemaphore/semaphore.png" alt="Smiley face" height="50">
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3" class="formArea2" align="center">


                            <table width="100%" border="0" class="main"  id="terminalsTable" cellspacing="5" cellpadding="5">

                                <tr>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.light", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.active", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.definition", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.totalMerchants", SessionData.getLanguage())%></td>
                                </tr>

                                <tr>
                                    <td align="center" bgcolor="#6B6E6B"></td>
                                    <td align="center"><%=(SessionData.getLanguage().equals("spanish")) ? "NO" : "NOT"%></td>
                                    <td align="center">N/A</td>
                                    <td align="center"><a onclick="showInactiveMerchants();" href="javascript:void(0);"><%=modelGray.size()%></a></td>
                                </tr>

                                <%
                                    int countTotal = 0;
                                    for (int i = 0; i < modelList.size(); i++) {
                                        StreetlightSnapshotModel info = modelList.get(i);
                                        countTotal += info.getTotalMerchants();
                                %>
                                <tr>
                                    <%
                                        String color = "#FFFFFF";
                                        if (info.getLight().equalsIgnoreCase("GRAY")) {
                                            color = "#6B6E6B";
                                        }
                                        else if (info.getLight().equalsIgnoreCase("RED")) {
                                            color = "#E62424";
                                        }
                                        else if (info.getLight().equalsIgnoreCase("YELLOW")) {
                                            color = "#FFBF00";
                                        }
                                        else if (info.getLight().equalsIgnoreCase("GREEN")) {
                                            color = "#55BE45";
                                        }
                                    %>
                                    <td align="center" bgcolor="<%=color%>"></td>


                                    <%if (info.isActive()) {%>
                                    <td align="center"><%=(SessionData.getLanguage().equals("spanish")) ? "SI" : "YES"%></td>
                                    <%}
                                    else {%>
                                    <td align="center"><%=(SessionData.getLanguage().equals("spanish")) ? "NO" : "NOT"%></td>
                                    <%}%>
                                    <td align="center"><%=info.getDefinition()%></td>
                                    <td align="center"><a onclick="showMerchantsInfo('<%=info.getLight()%>');" href="javascript:void(0);"><%=info.getTotalMerchants()%></a></td>

                                </tr>
                                <%
                                    }
                                %>
                                <tr>
                                    <td align="center"></td>
                                    <td align="center"></td>
                                    <td align="center"></td>
                                    <td align="center"><%=(countTotal+modelGray.size())%></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
    <tr>
        <td>
            <div id="dialog-message" title="SIMs" >
                <p>
                    <span class="spanTextBatchId" style="float:left; margin:0 7px 50px 0;"></span>
                </p>
                <p id="pDialogMessage"></p>
            </div>
        </td>
    </tr>

</table>                                                
</td>
</tr>
</table>

<script>
    
    function isInternetExplorer() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {      // If Internet Explorer, return version number
            return true;
        }
        else {                 // If another browser, return 0
            return false;
        }
    }

    function showMerchantsInfo(light) {     
        if (isInternetExplorer()) {
            window.location.href = "/support/reports/streetlightSnapshot/historical?light=" + light;
        }
        else {
            window.location.href = "reports/streetlightSnapshot/historical?light=" + light;
        }
    }

    function showInactiveMerchants() {

        $.ajax({
            type: "POST",
            async: false,
            data: {action: 'InactiveMerchants'},
            url: "admin/reports/streetlightSnapshot/streetlightSnapshotFormDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data)).split("\n");
                if (array_data[0] !== 'NOTHING') {

                    var text = "";
                    text += "<tr><td class=\"rowhead2\" align=\"center\">Merchant DBA</td><td class=\"rowhead2\" align=\"center\" >Merchant Id</td><td class=\"rowhead2\" align=\"center\">Remaining Days</td></tr>";
                    
                    
                    if (array_data.length >= 1 && array_data[0].indexOf('undefined') !== -1) {
                        var countRow = 1;
                        var arrTemp;
                        for (var i = 0; i < array_data.length; i++) {

                            arrTemp = array_data[i].split(",");
                            text += "<tr>";
                            text += "<td>" + arrTemp[0] + "</td>";
                            text += "<td>" + arrTemp[1] + "</td>";
                            text += "<td>" + arrTemp[2] + "</td>";
                            text += "</tr>";
                            countRow++;
                        }
                    }
                    if (text.length == 0 || (array_data.length == 1 && array_data[0].indexOf('undefined') === -1)) {
                        text = "<tr><td>There are not Data.</td></tr>";
                    }

                    text = "<table border=\"1\" cellspacing=\"4\" cellpadding=\"4\" class=\"sort-table\"> " + text + "</table>";
                    $("#pDialogMessage").html(text);

                    var opt = {
                        autoOpen: false,
                        modal: true,
                        width: 500,
                        height: 650,
                        title: 'Data ',
                        html: text
                    };

                    $(document).ready(function () {
                        $("#dialog-message").dialog(opt).dialog("open");
                    });
                }
                else {
                    alert("There are not data");
                }
            }
        });
    }

</script>

<%@ include file="/includes/footer.jsp" %>
