<%-- 
    Document   : newjspstreetlightSnapshotDB
    Created on : May 8, 2017, 11:31:39 AM
    Author     : dgarzon
--%>

<%@page import="com.debisys.reports.streetlightSnapshot.StreetlightSnapshotModelGray"%>
<%@page import="com.debisys.reports.streetlightSnapshot.StreetlightSnapshotFactory"%>
<%@page import="java.util.List"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    String action = request.getParameter("action");
    String strRefId = SessionData.getProperty("ref_id"); 
    
    if(action.equals("InactiveMerchants")){
        List<StreetlightSnapshotModelGray> modelGray = StreetlightSnapshotFactory.getInactiveMerchants("","",strRefId, false);
        String dba="";
        for (StreetlightSnapshotModelGray data : modelGray) {
            dba = data.getMerchantDBA().replace(",", " ");
            out.println(dba+","+data.getMerchantId()+","+data.getRemainingDays());
        }
    }
    if(action.equals("InactiveMerchantsTrend")){
        
        String filterBy = request.getParameter("filterBy");
        String filterValues = request.getParameter("filterValues");
        
        List<StreetlightSnapshotModelGray> modelGray = StreetlightSnapshotFactory.getInactiveMerchants(filterBy,filterValues,strRefId, true);
        String dba="";
        for (StreetlightSnapshotModelGray data : modelGray) {
            dba = data.getMerchantDBA().replace(",", " ");
            out.println(dba+","+data.getMerchantId()+","+data.getRemainingDays());
        }
    }
    
%>
