<%-- 
    Document   : streetLightTrendForm
    Created on : Apr 26, 2017, 5:39:06 PM
    Author     : dgarzon
--%>
<%@page import="java.math.RoundingMode"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.debisys.reports.streetlightSnapshot.StreetlightSnapshotModelGray"%>
<%@page import="com.debisys.reports.streetlightSnapshot.StreetLightTrendModel"%>
<%@page import="net.emida.supportsite.mvc.UrlPaths"%>
<%@page import="com.debisys.reports.streetlightSnapshot.StreetlightSnapshotModel"%>
<%@page import="com.debisys.reports.streetlightSnapshot.StreetlightSnapshotFactory"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<style type="text/css">
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }
    #spanInfo{
        font-size: 20px;
        color: #088A08;
    }

</style>

<%
    String strRefId = SessionData.getProperty("ref_id");
    String action = request.getParameter("action");
    
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String filterBy = request.getParameter("filterBy");
    String filterValues = request.getParameter("filterValues");
    
    double totalMerchants = 0;
    BigDecimal merchantsPercentGray = BigDecimal.ZERO;
    List<StreetLightTrendModel> modelList = new ArrayList<StreetLightTrendModel>();
    List<StreetlightSnapshotModelGray> modelGray = new ArrayList<StreetlightSnapshotModelGray>();
    if(action != null && action.equalsIgnoreCase("execReport")){
        startDate = (startDate != null) ? startDate : SessionData.getProperty("start_date");
        endDate = (endDate != null) ? endDate : SessionData.getProperty("end_date");
        modelList = StreetlightSnapshotFactory.getReportMainTrend(filterBy, filterValues, strRefId, startDate, endDate);
        modelGray = StreetlightSnapshotFactory.getInactiveMerchants(filterBy, filterValues, strRefId, true);
        totalMerchants = StreetlightSnapshotFactory.getCountMerchants(strRefId);
        if (totalMerchants != 0) {
            merchantsPercentGray = new BigDecimal(modelGray.size()*100).divide(BigDecimal.valueOf(totalMerchants), 2, RoundingMode.HALF_UP);    
        }        
    }
    
    SessionData.setProperty("start_date", startDate);
    SessionData.setProperty("end_date", endDate);

%>

<table border="0" cellpadding="0" cellspacing="0" width="750" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle"> 
            <%=Languages.getString("jsp.admin.reports.flow.streetlightTrend.title", SessionData.getLanguage())%>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
                <form id="mainform" name="mainform" class="reportForm2" name="mainform2" method="post" action="admin/reports/streetlightSnapshot/streetLightTrendForm.jsp" onsubmit="return validateMerchants();">
                <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                    
                    <tr>
                        <td>
                            <table border = "0" width="40%">
                                <tr >
                                    <td ><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td><td><input class="plain" name="startDate" id="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                    <td rowspan="2" align="center">
                                        <img src="images/flowSemaphore/semaphore.png" alt="Smiley face" height="50">
                                    </td>
                                </tr>
                                <tr >
                                    <td ><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td> <input class="plain" name="endDate" id="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                </tr>
                                
                                
                                    
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td colspan="3">

                                        <div id="entityCombos">
                                            <jsp:include page="/admin/customers/merchants/entity_combo_selector.jsp" >
                                                <jsp:param value="<%=strRefId%>" name="strRefId" />
                                                <jsp:param value="<%=strAccessLevel%>" name="strAccessLevel" />
                                                <jsp:param value="<%=strDistChainType%>" name="strDistChainType" />                                            
                                            </jsp:include>                                                
                                        </div>
                                    </td>
                                </tr>
                                <tr>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br/>
                            <input type="hidden" name="filterBy" id="filterBy" value="<%=filterBy%>">
                            <input type="hidden" name="filterValues" id="filterValues" value="<%=filterValues%>">
                            <input type="hidden" name="action" id="action" value="execReport">
                            <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.flow.streetlightTrend.submit", SessionData.getLanguage())%>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            
                        </td>
                    </tr>
                    <%if(modelList != null && !modelList.isEmpty()){%>
                    
                    <tr>
                        <td>
                            <%=Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.infoGray", SessionData.getLanguage())%>
                            <br>
                            <%=Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.infoColors", SessionData.getLanguage())%>
                        </td>
                    </tr>
                    <tr>
                        <table width="100%" border="0" class="main"  id="terminalsTable" cellspacing="5" cellpadding="5">

                                <tr>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.light", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.active", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.definition", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightTrend.totalOccurences", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightTrend.uniqueMerchants", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightTrend.merchantPercent", SessionData.getLanguage())%></td>
                                    <td align="center" class="rowhead2"><%=Languages.getString("jsp.admin.reports.flow.streetlightTrend.occurrencesPercent", SessionData.getLanguage())%></td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#6B6E6B"></td>
                                    <td align="center"><%=(SessionData.getLanguage().equals("spanish")) ? "NO" : "NOT"%></td>
                                    <td align="center">N/A</td>
                                    <td align="center">-</td>
                                    <td align="center"><a onclick="showInactiveMerchantsTrend('<%=filterBy%>','<%=filterValues%>');" href="javascript:void(0);"><%=modelGray.size()%></a></td>
                                    <td align="center"><%=merchantsPercentGray%></td>
                                </tr>

                                <%
                                    int countTotalOccurrences = 0;
                                    int countTotalMerchants = 0;
                                    for (int i = 0; i < modelList.size(); i++) {
                                        StreetLightTrendModel info = modelList.get(i);
                                        countTotalOccurrences += info.getTotalOccurences();
                                        countTotalMerchants += info.getCountMerchants();
                                %>
                                <tr>
                                    <%
                                        String color = "#FFFFFF";
                                        if(info.getLight().equalsIgnoreCase("GRAY")){
                                        color = "#6B6E6B";
                                        }
                                        else if(info.getLight().equalsIgnoreCase("RED")){
                                            color = "#E62424";
                                        }
                                        else if(info.getLight().equalsIgnoreCase("YELLOW")){
                                            color = "#FFBF00";
                                        }
                                        else if(info.getLight().equalsIgnoreCase("GREEN")){
                                            color = "#55BE45";
                                        }
                                    %>
                                    <td align="center" bgcolor="<%=color%>"></td>
                                    
                                    
                                    <%if (info.isActive()) {%>
                                    <td align="center"><%=(SessionData.getLanguage().equals("spanish")) ? "SI" : "YES"%></td>
                                    <%}
                                else {%>
                                    <td align="center"><%=(SessionData.getLanguage().equals("spanish")) ? "NO" : "NOT"%></td>
                                    <%}%>
                                    
                                    <td align="center"><%=info.getDefinition()%></td>
                                    <td align="center"><a onclick="showTrendElements('<%=info.getLight()%>','<%=filterBy%>','<%=filterValues%>','<%=startDate%>','<%=endDate%>');" 
                                                          href="javascript:void(0);"><%=info.getTotalOccurences()%></a></td>
                                    <td align="center"><%= info.getCountMerchants() %></td>
                                    <td align="center"><%= info.getMerchantsPercent()%></td>
                                    <td align="center"><%= info.getOccurencesPercent()%></td>
                                    

                                </tr>
                                <%
                                    }
                                %>
                                <tr>
                                    <td align="center"></td>
                                    <td align="center"></td>
                                    <td align="center">TOTAL</td>
                                    <td align="center"><%=countTotalOccurrences%></td>
                                    <td align="center"><%=countTotalMerchants%></td>
                                    <td align="center"></td>
                                    <td align="center"></td>
                                </tr>
                            </table>
                    </tr>
                    <%}%>
                </table>
                </form>
        </td>
    </tr>
    <tr>
        <td>
            <div id="dialog-message" title="SIMs" >
                <p>
                    <span class="spanTextBatchId" style="float:left; margin:0 7px 50px 0;"></span>
                </p>
                <p id="pDialogMessage"></p>
            </div>
        </td>
    </tr>
</table>

<script>

    function validateMerchants() {
        
        var filterBy = "iso";
        var filterValues = "";
        var selectedMerchants = $('#options_5').val();
        
        if(selectedMerchants != null){
            filterBy = "merchants";
            filterValues = selectedMerchants;
        }
        else{
            // REPS
            var selectedReps = $('#options_4').val();
            if(selectedReps != null){
                filterBy = "reps";
                filterValues = selectedReps;
            }
            else{
                // SUBAGENTS
                var selectedSubAgents = $('#options_3').val();
                if(selectedSubAgents != null){
                    filterBy = "subAgents";
                    filterValues = selectedSubAgents;
                }
                else{
                    // AGENTS
                    var selectedAgents = $('#options_2').val();
                    if(selectedAgents != null){
                        filterBy = "agents";
                        filterValues = selectedAgents;
                    }
                }
            }
        }
        
        $("#filterBy").val(filterBy);
        $("#filterValues").val(filterValues);
        
        $("#startDate").css('border', '');
        $("#endDate").css('border', '');
        
        var startDate = $('#startDate').val();
        var endDate = $('#endDate').val();
        var dateBad = false;
        if(startDate === ''){
            dateBad = true;
            $("#startDate").css("border", "1px solid red"); 
        }
        if(endDate === ''){
            dateBad = true;
            $("#endDate").css("border", "1px solid red");
        }
        
        if(dateBad === true){
            return false;
        }

        return true;
    }
    
    function isInternetExplorer() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {      // If Internet Explorer, return version number
            return true;
        }
        else {                 // If another browser, return 0
            return false;
        }
    }
    
    function showTrendElements(light, filterBy, filterValues, startDate, endDate) {
        
        if (isInternetExplorer()) {
            window.location.href = "/support/reports/streetLightTrendElements/historical?light="+
                light+"&filterBy="+filterBy+"&filterValues="+filterValues+"&startDate="+startDate+"&endDate="+endDate;
        }
        else {
            window.location.href = "reports/streetLightTrendElements/historical?light="+
                light+"&filterBy="+filterBy+"&filterValues="+filterValues+"&startDate="+startDate+"&endDate="+endDate;
        }
        
    }
    
    
    function showInactiveMerchantsTrend(filterBy, filterValues) {

        $.ajax({
            type: "POST",
            async: false,
            data: {action: 'InactiveMerchantsTrend', filterBy: filterBy, filterValues: filterValues},
            url: "admin/reports/streetlightSnapshot/streetlightSnapshotFormDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data)).split("\n");
                if (array_data[0] !== 'NOTHING') {

                    var text = "";
                    text += "<tr><td class=\"rowhead2\" align=\"center\">Merchant DBA</td><td class=\"rowhead2\" align=\"center\" >Id</td><td class=\"rowhead2\" align=\"center\">Remaining Days</td></tr>";
                    if (array_data.length >= 1) {
                        var countRow = 1;
                        var arrTemp;
                        for (var i = 0; i < array_data.length; i++) {

                            arrTemp = array_data[i].split(",");
                            text += "<tr>";
                            text += "<td>" + arrTemp[0] + "</td>";
                            text += "<td>" + arrTemp[1] + "</td>";
                            text += "<td>" + arrTemp[2] + "</td>";
                            text += "</tr>";
                            countRow++;
                        }
                    }
                    if (text.length == 0) {
                        text = "<tr><td>There are not Data.</td></tr>";
                    }

                    text = "<table border=\"1\" cellspacing=\"4\" cellpadding=\"4\"> " + text + "</table>";
                    $("#pDialogMessage").html(text);

                    var opt = {
                        autoOpen: false,
                        modal: true,
                        width: 500,
                        height: 650,
                        title: 'Data: ',
                        html: text
                    };

                    $(document).ready(function () {
                        $("#dialog-message").dialog(opt).dialog("open");
                    });
                }
                else {
                    alert("There are not data");
                }
            }
        });
    }

</script>

<iframe width=132 height=142 name="gToday:contrast:agenda.js"
        id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm"
        scrolling="no" frameborder="0"
        style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;">
</iframe>


<c:import url="/WEB-INF/pages/footer.jsp" />