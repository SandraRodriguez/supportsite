<%@ page
	import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.reports.schedule.TopUpConvenienceCardSummaryReport,
                 com.debisys.schedulereports.ScheduleReport,
                 com.debisys.customers.Merchant"
                 %>
<%
 	int section      = 4;
	int section_page = 68;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="TransactionReport"
	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />

<jsp:useBean id="TopUpConvenienceCardSummaryReport"  
	class="com.debisys.reports.schedule.TopUpConvenienceCardSummaryReport" 
	scope="request"/>

<%@ include file="/includes/security.jsp"%>


<%
	SessionData.setProperty("reportId", DebisysConstants.SC_TOPUP_CONVENIENCE_CARD_SUM);
	Vector<Vector<Object>> vecSearchResults = null;
	Hashtable searchErrors     = null;
	String sortString = "";
	String merchantList = "";
	String carrierList = "";
	boolean allCarriersSelected = false;
	boolean allMerchantsSelected = false;
	boolean bUseTaxValue = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns
	boolean isMexico = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
	boolean permissionInterCustomConfig = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
    										DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
    											DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	String reportTitleKey = "jsp.admin.reports.convenienceCard.carrier_summary.title";
	String reportTitle = Languages.getString(reportTitleKey,SessionData.getLanguage()).toUpperCase();
	String[] merchantIds = null;
	String[] cids = null;
	
	TopUpConvenienceCardSummaryReport report = new TopUpConvenienceCardSummaryReport();
	String trReportStartDate = TransactionReport.getStartDate();
	if((trReportStartDate == null) || ((trReportStartDate != null) && (trReportStartDate.trim().equals(""))))
	{
		trReportStartDate = request.getParameter("startDate");
		TransactionReport.setStartDate(trReportStartDate);	
	}
	String trReportEndDate = TransactionReport.getEndDate();
	if((trReportEndDate == null) || ((trReportEndDate != null) && (trReportEndDate.trim().equals(""))))
	{
		trReportEndDate = request.getParameter("endDate");
		TransactionReport.setEndDate(trReportEndDate);	
	}
	SessionData.setProperty("start_date", trReportStartDate);
	SessionData.setProperty("end_date", trReportEndDate);
	
	
 	if (request.getParameter("search") != null)
  	{
    	if (TransactionReport.validateDateRange(SessionData))
    	{
    		allCarriersSelected =  Boolean.valueOf(request.getParameter("allCarriersSelected"));
    		allMerchantsSelected = Boolean.valueOf(request.getParameter("allMerchantsSelected"));

     		if ( isMexico && (request.getParameter("chkUseTaxValue") != null ) )
    		{
     			bUseTaxValue = true;
     			report.setTaxEnabled(bUseTaxValue);
     			report.setDeployment(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
   	 		}
     		else
   			{
     			report.setTaxEnabled(false);   
     			report.setDeployment(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
   			}
    	    report.setContext(application);
    	    cids = request.getParameterValues("cids");
    	    if(cids != null)
    	    {
    	    	carrierList = com.debisys.utils.StringUtil.arrayToString(cids, ",");
    	    	if(carrierList.equals(""))
    	    	{
    	    		allCarriersSelected = true;
    	    	}
    	    }
    	    report.setAllCarriersSelected(allCarriersSelected);
    		report.setConvenienceCardCarrier_ids(carrierList);
    		report.setStart_date(trReportStartDate);
    		report.setEnd_date(trReportEndDate);
    		report.setPermissionInterCustomConfig(permissionInterCustomConfig);
    		merchantIds = request.getParameterValues("merchantIds");
    		if(merchantIds != null)
    		{
    			merchantList = com.debisys.utils.StringUtil.arrayToString(merchantIds, ",");
    			if(merchantList.equals(""))
    			{
    				allMerchantsSelected = true;
    			}
    		}
    		report.setAllMerchantsSelected(allMerchantsSelected);
    		report.setSelectedMerchantIDs(merchantList);
    		report.setSessionData(SessionData);
    		report.setScheduling(false);
    		
	if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	{
		//TO SCHEDULE REPORT
		report.setScheduling(true);
		if(report.scheduleReport())
		{
			ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
			if (  scheduleReport != null  )
			{
				scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
				scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
				scheduleReport.setTitleName( reportTitleKey );   
			}	
			response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
		}
	}
	else if ( request.getParameter("downloadReport") != null )
	{
		//TO DOWNLOAD ZIP REPORT
		report.setScheduling(false);
		String zippedFilePath = report.downloadReport();
		if((zippedFilePath != null) &&(!zippedFilePath.trim().equals("")))
		{
			response.sendRedirect( zippedFilePath );
		}
	}
	else
	{
		//TO SHOW REPORT
		report.setScheduling(false);
		vecSearchResults = report.getResults();
	}
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
<%@ include file="/includes/header.jsp"%>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
		<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
		<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
	}
%>
	<table border="0" cellpadding="0" cellspacing="0" width="750">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left">
				<img src="images/top_left_blue.gif" width="18" height="20">
			</td>
			<td background="images/top_blue.gif" class="formAreaTitle"
				width="2000">
				&nbsp;<%= reportTitle %>
			</td>
			<td background="images/top_blue.gif" width="1%" align="right">
				<img src="images/top_right_blue.gif" width="18" height="20">
			</td>
		</tr>
		<tr>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
	if (searchErrors != null)
	{
		out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
		
		Enumeration enum1 = searchErrors.keys();
		
		while (enum1.hasMoreElements())
		{
			String strKey   = enum1.nextElement().toString();
			String strError = (String)searchErrors.get(strKey);			
			out.println("<li>" + strError);
		}
		out.println("</font></td></tr></table>");
	}

	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td class="main">

<%
		out.println(Languages.getString("jsp.admin.reports.convenienceCard.carrier_summary.title",SessionData.getLanguage()));
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) +
			        " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
%>
					</td>
					<td class=main align=right valign=bottom>
						<%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
					</td>
				</tr>
			</table>
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
<%
		sortString = "\"None\", \"CaseInsensitiveString\", \"Number\", \"Number\", \"Number\",";
%>
					<tr>
						<td class=rowhead2>
							#
						</td>
						<td class=rowhead2 nowrap>
							<%= Languages.getString("jsp.admin.reports.convenienceCard.carrier_name",SessionData.getLanguage()).toUpperCase() %>
						</td>
						<td class=rowhead2 nowrap>
							<%= Languages.getString("jsp.admin.reports.convenienceCard.activation",SessionData.getLanguage()).toUpperCase() %>
						</td>
						<td class=rowhead2 nowrap>
							<%= Languages.getString("jsp.admin.reports.convenienceCard.swipe",SessionData.getLanguage()).toUpperCase() %>
						</td>
						<td class=rowhead2 nowrap>
							<%= Languages.getString("jsp.admin.reports.analysis.merchant_activity_report.amount",SessionData.getLanguage()).toUpperCase() %>
						</td>
<% 
		if(bUseTaxValue)
		{
        	sortString += "\"Number\", ";
%>
						<td class=rowhead2 nowrap>
							<%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>
						</td>
<%
		}
        if(permissionInterCustomConfig)
        {
			sortString += "\"Number\", \"Number\"";
%>
						<td class=rowhead2 nowrap>
							<%= Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase() %>
						</td>
						<td class=rowhead2 nowrap>
							<%= Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase() %>
						</td>
<% 		
		}
%>
					</tr>
				</thead>
				<tbody>
<%

		Iterator it = vecSearchResults.iterator();
		int intEvenOdd = 1;
		int intCounter = 1;
		double dblTotalRechargeSum = 0;
		double dblTotalRechargeMinusVatSum=0;
		double dblTotalTax=0;
		double dblTotalTaxSum=0;
		int intTotalAssocSum=0;
		int intTotalSwipeSum = 0;
		double dblTotalRechargeMinusVat=0;
		int columnIndex = 0;
        while (it.hasNext())
        {
			Vector vecTemp = null;
			vecTemp = (Vector) it.next();
			int intTotalAssoc = Integer.parseInt(vecTemp.get(1).toString());
			int intTotalSwipe=Integer.parseInt(vecTemp.get(2).toString());
  			double dblTotalRecharge = Double.parseDouble(vecTemp.get(3).toString()); 
			if ( bUseTaxValue || permissionInterCustomConfig ) 
  			{
				dblTotalRechargeMinusVat=Double.parseDouble(vecTemp.get(4).toString()); 
  			}
			if(permissionInterCustomConfig)
			{
				columnIndex = 5;
				if(columnIndex <  vecTemp.size())
				{
					dblTotalTax = Double.parseDouble(vecTemp.get(columnIndex).toString());
				}
			}
			intTotalAssocSum = intTotalAssocSum + intTotalAssoc;
			intTotalSwipeSum= intTotalSwipeSum + intTotalSwipe;
			dblTotalRechargeSum += dblTotalRecharge;
			if ( bUseTaxValue || permissionInterCustomConfig ) 
  			{
				dblTotalRechargeMinusVatSum+=dblTotalRechargeMinusVat;
			}
			if(permissionInterCustomConfig)
			{
				dblTotalTaxSum+=dblTotalTax;
			}
					
			if(bUseTaxValue)
			{
				out.print("<tr class=row" + intEvenOdd + ">" +
					"<td>" + intCounter++ + "</td>" +
					"<td align=\"left\" nowrap>" + vecTemp.get(0).toString() + "</td>" +
					"<td align=\"left\">" + "<a href=\"admin/reports/transactions/convenience_card_carriers_transactions.jsp?" +
				    "startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +
				    "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +
				    "&carrierName=" + vecTemp.get(0).toString() +
				    "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") +
				    "&search=y&type=assoc&report=y&printbtn=\" target=\"_blank\">" + Integer.parseInt(vecTemp.get(1).toString()) + "</a></td>" +
					"<td align=\"left\">" + "<a href=\"admin/reports/transactions/convenience_card_carriers_transactions.jsp?" +
    				"startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +
    				"&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +
    				"&carrierName=" + vecTemp.get(0).toString()+
     				"&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") +
					"&search=y&type=swipe&report=y&printbtn=\" target=\"_blank\">" + Integer.parseInt(vecTemp.get(2).toString()) + "</a></td>"+
      				"<td align=\"right\" nowrap>" + Double.parseDouble(vecTemp.get(3).toString()) + "</td>"+
      				"<td align=\"right\" nowrap>" + Double.parseDouble(vecTemp.get(4).toString()) + "</td>");                         	
			}
			else
			{	
				out.print("<tr class=row" + intEvenOdd + ">" +
					"<td>" + intCounter++ + "</td>" +
					"<td align=\"left\" nowrap>" + vecTemp.get(0).toString() + "</td>" +
					"<td align=\"left\">" + "<a href=\"admin/reports/transactions/convenience_card_carriers_transactions.jsp?" +
					"startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +
					"&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +
					"&carrierName="+vecTemp.get(0).toString()+
					"&search=y&type=assoc&report=y&printbtn=\" target=\"_blank\">"+Integer.parseInt(vecTemp.get(1).toString()) + "</a></td>" +
					"<td align=\"left\">" + "<a href=\"admin/reports/transactions/convenience_card_carriers_transactions.jsp?" +
					"startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +
					"&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +
					"&carrierName="+vecTemp.get(0).toString()+
					"&search=y&type=swipe&report=y&printbtn=\" target=\"_blank\">" + Integer.parseInt(vecTemp.get(2).toString()) + "</a></td>"+
					"<td align=\"right\" nowrap>" + Double.parseDouble(vecTemp.get(3).toString()) + "</td>");
				if(permissionInterCustomConfig)
				{
					out.print	("<td align=\"right\" nowrap>" + Double.parseDouble(vecTemp.get(4).toString()) + "</td>"+
					"<td align=\"right\" nowrap>" + Double.parseDouble(vecTemp.get(5).toString()) + "</td>");
				}
			}

			out.print("</tr>");

            if (intEvenOdd == 1)
            {
            	intEvenOdd = 2;
            }
            else
            {
            	intEvenOdd = 1;
            }
		}

%>
				</tbody>
				<tfoot>
					<tr class="row<%=intEvenOdd%>">
						<td colspan=2 align=right>
							<%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
						</td>
						<td align=left>
							<%= intTotalAssocSum %>
						</td>
						<td align=left>
							<%= intTotalSwipeSum %>
						</td>
						<td align=right>
							<%= NumberUtil.formatCurrency(Double.toString(dblTotalRechargeSum)) %>
						</td>
<% 
		if(bUseTaxValue || permissionInterCustomConfig)
        { 
%>
						<td align=right>
							<%= NumberUtil.formatCurrency(Double.toString(dblTotalRechargeMinusVatSum)) %>
						</td>
<%
		} 
		if(permissionInterCustomConfig)
        {
%>
						<td align=right>
							<%= NumberUtil.formatCurrency(Double.toString(dblTotalTaxSum)) %>
						</td>
<%
		}
%>
					</tr>
				</tfoot>
			</table>
			
            <form name="downloadData" method=post action="admin/reports/transactions/convenience_card_carriers_summary.jsp">
				<input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
				<input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
				<input type="hidden" name="search" value="y">
				<input type="hidden" name="downloadReport" value="y">
				<input type="hidden" name="allCarriersSelected" value="<%=allCarriersSelected%>">
				<input type="hidden" name="allMerchantsSelected" value="<%=allMerchantsSelected%>">
<%
		if((merchantIds != null) && (merchantIds.length > 0))
		{
			for(int i = 0;i < merchantIds.length;i++)
			{
%>
       			<input type="hidden" name="merchantIds" value="<%=merchantIds[i]%>">
<%
          	}
		}
%>
<%
		if((cids != null) && (cids.length > 0))
		{
			for(int i = 0;i < cids.length;i++)
			{
%>
       			<input type="hidden" name="cids" value="<%=cids[i]%>">
<%
          	}
		}
%>
				<input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.convenienceCard.Download",SessionData.getLanguage())%>">
            </form>
			
<%
        }
        else
        {
        	if ((vecSearchResults == null) || 
        		((vecSearchResults.size() == 0) && (request.getParameter("search") != null) && (searchErrors == null)))
        	{
          		out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        	}
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
			<SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
 [<%=sortString%>],0,false,false);
  -->
          </SCRIPT>
<%
        }       
%>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>
