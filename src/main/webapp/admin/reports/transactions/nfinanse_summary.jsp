<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.reports.NFinanseReport"%>
<%@page import="com.debisys.users.User"%>
<%
int section=4;
int section_page=43;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="NFinanseReport" class="com.debisys.reports.NFinanseReport" scope="request" />
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page" />
<jsp:setProperty name="NFinanseReport" property="*" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script src="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
int intRecordCount = 0;
Hashtable searchErrors = null;
Vector vecSearchResults = new Vector();
String report_link = "admin/reports/transactions/nfinanse_summary.jsp?";
String down_link = "admin/reports/transactions/nfinanse_summary_download.jsp?";
String link_params = "search=y&";
String paginator = "";

if (request.getParameter("search") != null){
	if (request.getParameter("page") != null){
		try{
			intPage=Integer.parseInt(request.getParameter("page"));
		}catch(NumberFormatException ex){
			intPage = 1;
		}
	}
	if (intPage < 1){
		intPage=1;
	}
	if (request.getParameter("rowsperpage") != null){
		try{
			intPageSize=Integer.parseInt(request.getParameter("rowsperpage"));
		}catch(NumberFormatException ex){
			intPageSize = 1;
		}
	}
	if (intPageSize < 1){
		intPageSize=1;
	}
	try{
	  	if (NFinanseReport.validateDateRange(SessionData)){
	  		SessionData.setProperty("start_date", request.getParameter("startDate"));
			SessionData.setProperty("end_date", request.getParameter("endDate"));
		  	SessionData.setProperty("sort", request.getParameter("sort"));
			SessionData.setProperty("col", request.getParameter("col"));
			String[] strCategoryIds = request.getParameterValues("categoryIds");
			String strMerchantIds[] = request.getParameterValues("merchantIds");
			NFinanseReport.setCategoryIds(strCategoryIds);
			NFinanseReport.setMerchantIds(strMerchantIds);
			vecSearchResults = NFinanseReport.getTransactions(SessionData, intPage, intPageSize);
			intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
			vecSearchResults.removeElementAt(0);
			
		    if (intRecordCount>0){
		      intPageCount = (intRecordCount / intPageSize);
		      if ((intPageCount * intPageSize) < intRecordCount){
		        intPageCount++;
		      }
		    }
		    link_params += "startDate=" + NFinanseReport.getStartDate(); 
		    link_params += "&endDate=" + NFinanseReport.getEndDate(); 
		    link_params += "&categoryIds=" + NFinanseReport.getCategoryIds();
		    link_params += "&merchantIds=" + NFinanseReport.getMerchantIds();
		    link_params += "&rowsperpage=" + intPageSize + "&";
	
		    report_link += link_params;
		    down_link +=  link_params;
			paginator = "Results found: " + intRecordCount;
			paginator += "| Page: ";
			for(int i=1;i<=intPageCount;i++){
				if(i > 1){paginator += " | ";}
				if(i==intPage){
					paginator += "<b>" + i + "</b>";
				}else{
					paginator += "<a href=\""+ report_link + "col=" + request.getParameter("col") + "&sort=" + request.getParameter("sort") +"&page=" + i +"\">" + i + "</a>";
				}
			}
		}else{
			searchErrors = NFinanseReport.getErrors();
		}
	}catch(Exception ex){
		searchErrors = new Hashtable();
		searchErrors.put("Report Error", "the report throwed an exception: " + ex.getMessage());
	}
}
%>
<script language="JavaScript">
	var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");
    function scroll2(){
    	document.downloadform.submit.disabled = true;  
    	document.downloadform.submit.value = iProcessMsg[count];
    	count++
    	if (count = iProcessMsg.length) count = 0
    	setTimeout('scroll2()', 150);
    }
    function submitPage(thePage){
        document.submitPageForm.page.value = thePage;  
    	document.submitPageForm.submit();
    }
</script>
<table border="0" cellpadding="0" cellspacing="0" width="980">
	<tr>
		<td width="18" height="20">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">
			&nbsp;<%=Languages.getString("jsp.admin.reports.nfinanse.title",SessionData.getLanguage()).toUpperCase()%>
		</td>
		<td width="12" height="20">
			<img src="images/top_right_blue.gif">
		</td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
				<tr>
					<td>
						<br><a href="<%=down_link + "&sort=" + request.getParameter("sort")+"&col=" + request.getParameter("col")%>" ><%=Languages.getString("jsp.admin.reports.nfinanse.download",SessionData.getLanguage())%></a><br><br>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="formArea2">
        						    <table width="100%" border="0" cellspacing="0" cellpadding="2">
		            					<tr>
		            						<td class="main">
		            							<%=Languages.getString("jsp.admin.reports.nfinanse.summarytitle",SessionData.getLanguage())%>
												<%=request.getParameter("startDate")%>
												<%=Languages.getString("jsp.admin.reports.nfinanse.summaryto",SessionData.getLanguage())%>
												<%=request.getParameter("endDate")%><br>
 												<font color="#ff0000"><%=Languages.getString("jsp.admin.reports.nfinanse.summarywarning",SessionData.getLanguage())%></font>
											</td>
										</tr>
						            </table>
						            <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
									<thead>
										<tr>
											<td colspan="13" align="center"><%=paginator %></td>
										</tr>
										<tr class="SectionTopBorder">
											<!-- <td class="BoldClickableBody" align="center" >< %=Languages.getString("jsp.admin.reports.money_transfers.PrincipalAmount",SessionData.getLanguage())% >&nbsp;</td > -->				
											<td class="rowhead2" align="center">#</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.sku",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=0&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=0&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>											
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.loadamount",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=1&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=1&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.loadfee",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=2&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=2&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<%=Languages.getString("jsp.admin.reports.nfinanse.transtype",SessionData.getLanguage()).toUpperCase()%>&nbsp;
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.datetime",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=4&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=4&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.transid",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=5&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=5&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.mername",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=6&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=6&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.meraddress",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=7&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=7&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.mercity",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=8&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=8&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.merstate",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=9&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=9&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.serialnumber",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=10&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=10&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
											<td class="rowhead2" align="center">
												<nobr>
												<%=Languages.getString("jsp.admin.reports.nfinanse.terminalid",SessionData.getLanguage()).toUpperCase()%>&nbsp;
												<a href="<%=report_link%>col=11&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
												<a href="<%=report_link%>col=11&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
												</nobr>
											</td>
										</tr>
									</thead>
									<tbody>
									<%
									Iterator itResults = vecSearchResults.iterator();
									int intEvenOdd = 1;
									while (itResults.hasNext()){
										Vector vecTemp = null;
										vecTemp = (Vector) itResults.next();
										if(vecTemp.get(0).equals("TOTAL")){
											out.println("<tr><td>&nbsp;</td>");
											out.println("<td align=\"right\" colspan=\"2\">TOTAL:&nbsp;&nbsp;&nbsp;&nbsp;" + vecTemp.get(1) + "</td>");
										}else{
											String amountValue = "";
							                if ( vecTemp.get(5).toString().contains("-")){
							                	amountValue = "<font color=#ff0000>"+vecTemp.get(5)+"</font>";
							                }else{
							                	amountValue = vecTemp.get(5).toString();
							                }				
							                out.println("<tr class=\"row" + intEvenOdd + "\">");
							                out.println("<td>" + vecTemp.get(0) + "</td>");
							                out.println("<td align=\"right\">" + vecTemp.get(1) + "</td>");
											out.println("<td align=\"right\">" + vecTemp.get(2) + "</td>");
											out.println("<td align=\"right\">" + vecTemp.get(3) + "</td>");
											out.println("<td>" + vecTemp.get(4) + "</td>");
											out.println("<td>" + vecTemp.get(5) + "</td>");
											out.println("<td align=\"right\">" + vecTemp.get(6) + "</td>");
											out.println("<td>" + vecTemp.get(7) + "</td>");
											out.println("<td>" + vecTemp.get(8) + "</td>");
											out.println("<td>" + vecTemp.get(9) + "</td>");
											out.println("<td>" + vecTemp.get(10) + "</td>");
											out.println("<td>" + vecTemp.get(11) + "</td>");
											out.println("<td>" + vecTemp.get(12) + "</td>");
											//out.println("<td align=\"center\">" + vecTemp.get(6) + "</td>");
										}
										out.println("</tr>");
										if (intEvenOdd == 1){
											intEvenOdd = 2;
										}else{
											intEvenOdd = 1;
										}
									}
									vecSearchResults.clear();
									%>
									</tbody>
									</table>						
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>