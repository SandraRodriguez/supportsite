<%@page import="com.debisys.utils.DbUtil"%>
<%@ page import="java.net.URLEncoder,
				 com.debisys.utils.HTMLEncoder,
				 java.util.*,
				 com.debisys.utils.NumberUtil,
				 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
	Vector vecSearchResults = new Vector();
 Vector warningSearchResults = new Vector();
 Hashtable searchErrors = null;
 String strRepIds[] = null;
 ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
 ArrayList<String> titles = new ArrayList<String>();

 String noteTimeZone = "";
 String testTrx = "";  
 String titleReport = "";
 
 boolean bUseTaxValue = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns
 
 if (request.getParameter("search") != null)
 {
	if (TransactionReport.validateDateRange(SessionData))
	{
		SessionData.setProperty("start_date", request.getParameter("startDate"));
		SessionData.setProperty("end_date", request.getParameter("endDate"));
		strRepIds = request.getParameterValues("rids");
		if (strRepIds != null) {
			TransactionReport.setRepIds(strRepIds);
		} else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
			// Try to fill reps IDs by agent id
			String agentId = request.getParameter("agentId");
			if (agentId != null) {
				List<String> repIds = DbUtil.getAgentRepIds(application, Long.valueOf(agentId));
				if (!repIds.isEmpty()) {
					strRepIds = new String[repIds.size()];
					strRepIds = repIds.toArray(strRepIds);
					TransactionReport.setRepIds(strRepIds);
				}
			} else {
				String subAgentId = request.getParameter("subAgentId");
				if (subAgentId != null) {
					List<String> repIds = DbUtil.getRepEntityChildIds(application, Long.valueOf(subAgentId));
					if (!repIds.isEmpty()) {
						strRepIds = new String[repIds.size()];
						strRepIds = repIds.toArray(strRepIds);
						TransactionReport.setRepIds(strRepIds);
					}
				}

			}
		}

			//////////////////////////////////////////////////////////////////
			//HERE WE DEFINE THE REPORT'S HEADERS 
			headers = com.debisys.reports.ReportsUtil.getHeadersSummaryTransactions(SessionData, application, DebisysConstants.REP);
			//////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////

			Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
			noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage()) + ":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
			String keyLanguage = "jsp.admin.reports.transaction.rep_summary";
			titleReport = Languages.getString(keyLanguage, SessionData.getLanguage());
			testTrx = Languages.getString("jsp.admin.reports.test_trans", SessionData.getLanguage());

			if (request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y")) {
				//TO SCHEDULE REPORT
				titles.add(noteTimeZone);
				titles.add(titleReport);
				titles.add(testTrx);
				if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
						&& (request.getParameter("chkUseTaxValue") != null)) {
					bUseTaxValue = true;
					TransactionReport.getRepSummaryMx(SessionData, application, DebisysConstants.SCHEDULE_REPORT, headers);
				} else {
					vecSearchResults = TransactionReport.getRepSummary(SessionData, application, DebisysConstants.SCHEDULE_REPORT, headers, titles);
				}
				ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
				if (scheduleReport != null) {
					scheduleReport.setStartDateFixedQuery(TransactionReport.getStartDate());
					scheduleReport.setEndDateFixedQuery(TransactionReport.getEndDate());
					scheduleReport.setTitleName(keyLanguage);
				}
				response.sendRedirect(DebisysConstants.PAGE_TO_SCHEDULE_REPORTS);
			} else if (request.getParameter("downloadReport") != null) {
				//TO DOWNLOAD ZIP REPORT
				if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
					titleReport = titleReport + Languages.getString("jsp.admin.from", SessionData.getLanguage())
							+ " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted());
					titleReport = titleReport + " "
							+ Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " "
							+ HTMLEncoder.encode(TransactionReport.getEndDateFormatted());
				}
				titles.add(noteTimeZone);
				titles.add(titleReport);
				titles.add(testTrx);

				if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
						&& (request.getParameter("chkUseTaxValue") != null)) {
					TransactionReport.getRepSummaryMx(SessionData, application,	DebisysConstants.DOWNLOAD_REPORT, headers);
				} else {
					TransactionReport.getRepSummary(SessionData, application, DebisysConstants.DOWNLOAD_REPORT, headers, titles);
				}
				response.sendRedirect(TransactionReport.getStrUrlLocation());
			} else {
				//TO SHOW REPORT
				if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
					titleReport = titleReport + Languages.getString("jsp.admin.from", SessionData.getLanguage())
							+ " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted());
					titleReport = titleReport + " "
							+ Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " "
							+ HTMLEncoder.encode(TransactionReport.getEndDateFormatted());
				}
				titles.add(noteTimeZone);
				titles.add(titleReport);
				titles.add(testTrx);
				if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
						&& (request.getParameter("chkUseTaxValue") != null)) {
					vecSearchResults = TransactionReport.getRepSummaryMx(SessionData, application, DebisysConstants.EXECUTE_REPORT, headers);
				} else {
					vecSearchResults = TransactionReport.getRepSummary(SessionData, application, DebisysConstants.EXECUTE_REPORT, headers, titles);
				}
			}
		} else {
			searchErrors = TransactionReport.getErrors();
		}
	}
%>
<%@include file="/includes/header.jsp"%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img width="18" height="20" src="images/top_left_blue.gif"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.title3",SessionData.getLanguage()).toUpperCase()%></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img width="18" height="20" src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF" class="formArea2">
<% 
if (searchErrors != null){ 
	out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>"); 
	Enumeration enum1=searchErrors.keys(); 
	while(enum1.hasMoreElements()){ 
		String strKey = enum1.nextElement().toString(); 
		String strError = (String) searchErrors.get(strKey); 
		out.println("<li>" + strError); 
	} 
	out.println("</font></td></tr></table>"); 
} 
if (vecSearchResults != null && vecSearchResults.size() > 0){ 
%>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
%>
            <tr class="main">
            	<td nowrap colspan="2"><%= noteTimeZone %><br/><br/>
            	</td>
            </tr>
				
				<% if ( warningSearchResults.size()>0){ %>
				<tr>
				<td class="main" style="color:red">
				<% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1)
								||  (!TransactionReport.checkfortaxtype(SessionData))){
				%>
					<%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
					<%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
				<% } %>
					<% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
						<%= warningSearchResults.get(i).toString()%> <br/>
					<% } %>
				</td>
				</tr>
				<%} %>
				<tr>
					<td class="main">
					<%= titleReport %> 
						<br><font color="#ff0000"><%= testTrx %></font>
					</td>
				</tr>
			</table>
			
			<form name="downloadData" method=post action="admin/reports/transactions/reps_summary.jsp">
              <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
              <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
              <input type="hidden" name="search" value="y">
              <input type="hidden" name="downloadReport" value="y">
            	<%for(int i=0;i<strRepIds.length;i++){%>
				<input type="hidden" name="rids" value="<%=strRepIds[i]%>">
				<%}%>              
              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		    </form>
		    
			<table>
				<tr>
					<td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
				</tr>
			</table>
			<table width="100%" cellspacing="1" cellpadding="2" class="sort-table">
				<tr>
<% 
	String strSortURL = "admin/reports/transactions/reps_summary.jsp?search=y&startDate=" + TransactionReport.getStartDate() + "&endDate=" + TransactionReport.getEndDate() + "&rids=" + TransactionReport.getRepIds() + ((bUseTaxValue)?"&chkUseTaxValue=" + request.getParameter("chkUseTaxValue"):""); 
%>
					<td class="rowhead2" valign="bottom">#</td>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.business_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=1&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=1&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
	if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) ){ 
%>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.customers.merchants_info.address",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=11&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=11&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=12&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=12&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=13&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=13&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=14&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=14&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
	} 
%>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=2&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=2&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=3&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=3&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=4&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=4&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<%  
	if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
		DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))  { 
%>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=17&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=17&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=18&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=18&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
	} 
	if ( bUseTaxValue ){ 
%>
					<td CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&amp;col=11&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=11&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
	} 
%><%  
if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  
		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
				DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
{ 
%>

              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=17&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=17&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
			  <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=18&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=18&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<%} 
%><% 
	if (strAccessLevel.equals(DebisysConstants.ISO)){ 
%>  

					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=9&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=9&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
		if ( bUseTaxValue ){ 
%>
					<td class="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&amp;col=16&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=16&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
		} 
 
    if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
    { 
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_iso_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=16&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=16&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
		} 
	} 
	if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){ 
%><% 
	} 
	if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){ 
%>

					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=8&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=8&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
		if ( bUseTaxValue ){ 
%>
					<td class="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&amp;col=15&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=15&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
		} 
 
     if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
    { 
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_agent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=15&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=15&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
    } 
 
	} 
	if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){ 
%>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=7&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=7&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
		if ( bUseTaxValue ){ 
%>
					<td class="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&amp;col=14&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=14&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
		} 
     if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
    { 
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_subagent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=15&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=15&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
    } 
%><% 
	} 
	if (!strAccessLevel.equals(DebisysConstants.MERCHANT)){ 
%>

					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=6&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=6&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
		if ( bUseTaxValue ){ 
%>
					<TD class="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&amp;col=13&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=13&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
		} 
		  if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
	    { 
	%>
	  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_rep_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=15&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=15&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
	<% 
	    } 
		 
	} 
%>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=5&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=5&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
	if ( bUseTaxValue ){ 
%>
					<TD class="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&amp;col=12&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=12&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
	} 
	if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)){ 
	 
%>
      <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_merchant_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=15&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=15&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>     
<% 
	} 
	if (strAccessLevel.equals(DebisysConstants.ISO)){ 
%>
					<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.adjustment",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=10&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=10&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<% 
	} 
        if ((SessionData.checkPermission((DebisysConstants.PERM_ALLOW_PROMO_DATA_COLUMN_IN_TRANSACTIONS_REPORT))) && (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) && (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) {
%>
            <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.reports.reports.transactions.form.table.column.name.promoData", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=10&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=10&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></td>
<%
        }

	out.println("</tr>"); 
 
	Iterator it = vecSearchResults.iterator(); 
	int intEvenOdd = 1; 
	int intCounter = 1; 
	 
	String link_params = "startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") 
		+ "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") 
		+ "&search=y"; 
 
	int totalspancols = 3; 
	if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) ){ 
		totalspancols = 7; 
	} 
	while (it.hasNext()){ 
		Vector vecTemp = null; 
		vecTemp = (Vector) it.next(); 
		if(vecTemp.get(0).toString().equals("TOTALS")){ 
			%>
			<tr class="row<%=intEvenOdd%>">
			<td colspan="<%=totalspancols%>" align="right"><%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
			<td align="right"><%=vecTemp.get(1).toString()%><br></td>
			<td align="right"><%=vecTemp.get(2).toString()%><br></td>
			<%if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
					DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)){%>
				<td align="right"><%=vecTemp.get(9).toString()%><br></td>
				<td align="right"><%=vecTemp.get(10).toString()%><br></td>
			<%}%><%  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  
							DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
									DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){%>
			<td align="right"><%=vecTemp.get(11).toString()%><br></td><%}%><%  if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)){%>
			<td align="right"><%=vecTemp.get(17).toString()%><br></td><%}%><%if (strAccessLevel.equals(DebisysConstants.ISO)){%>
			
				<td align="right"><%=vecTemp.get(7).toString()%><br></td>
				<%  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){%><td align="right"><%=vecTemp.get(16).toString()%><br></td><%}%><%}%><%if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){%>
			
			
			
				<td align="right"><%=vecTemp.get(6).toString()%><br></td>
				<%  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){%><td align="right"><%=vecTemp.get(15).toString()%><br></td><%}%><%}%><%if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){%>
			
			
				<td align="right"><%=vecTemp.get(5).toString()%><br></td>
				<%  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){%><td align="right"><%=vecTemp.get(14).toString()%><br></td><%}%><%}%><%if (!strAccessLevel.equals(DebisysConstants.MERCHANT)){%>
			
			
				<td align="right"><%=vecTemp.get(4).toString()%><br></td>
				<%  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){%><td align="right"><%=vecTemp.get(13).toString()%><br></td><%}%><%}%>
			
			<td align="right"><%=vecTemp.get(3).toString()%><br></td>
			<%if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){%><td align="right"><%=vecTemp.get(12).toString()%><br></td><%}%><%if (strAccessLevel.equals(DebisysConstants.ISO)){%>
			<td align="right"><%=vecTemp.get(8).toString()%><%}%>
			<br></tr> <% 
		}else{ 
			String usetaxparam = ""; 
			String sLocation = ""; 
			  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){ 
				usetaxparam = "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue"); 
			}else{ 
				if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) ){ 
			    	sLocation = "<td>" + vecTemp.get(10).toString() + "</td><td>" + vecTemp.get(11).toString() + "</td><td>" + vecTemp.get(12).toString() + "</td><td>" + vecTemp.get(13).toString() + "</td>"; 
				} 
			}
                          
                          String useS2kRatePlans = "";
                          if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS)) {
                              useS2kRatePlans = "&s2kRatePlans=" + request.getParameter("s2kRatePlans");
                          }
			%>
				<tr class=row<%=intEvenOdd%>>
					<td><%=intCounter++%><br></td>
					<td nowrap="nowrap">
						<a href="admin/reports/transactions/merchants_summary.jsp?<%=link_params%>&amp;repIds=<%=vecTemp.get(1) + usetaxparam %>"> 
							<%=vecTemp.get(0)%> 
						</a>
					<br></td>
					<%=sLocation%>
					<td><%=vecTemp.get(1).toString()%><br></td>
					<td align="right"><%=vecTemp.get(2).toString()%><br></td>
					<td align="right">
						<a href="admin/transactions/reps_transactions.jsp?<%=link_params%>&amp;repId=<%=vecTemp.get(1)%>&amp;report=y<%=usetaxparam%><%=useS2kRatePlans%>"> 
							<%=vecTemp.get(3).toString()%> 
						</a>
					<br></td>
			<% 
			if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) { 
				  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){ 
					%><td align="right"><%=vecTemp.get(16).toString()%><br></td><% 
					%><td align="right"><%=vecTemp.get(17).toString()%><br></td><% 
				} else { 
					%><td align="right"><%=vecTemp.get(14).toString()%><br></td><% 
					%><td align="right"><%=vecTemp.get(15).toString()%><br></td><% 
				} 
			} 
			  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){ 
				%><td align="right"><%=vecTemp.get(10).toString()%><br></td><% 
			}	 
			 if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)){ 
				%><td align="right"><%=vecTemp.get(18).toString()%><br></td><% 
			}	 
			if (strAccessLevel.equals(DebisysConstants.ISO)){ 
				%><td align="right"><%=vecTemp.get(8).toString()%><br></td><% 
				  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){ 
					%><td align="right"><%=vecTemp.get(15).toString()%><br></td><% 
				} 
			} 
			if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){ 
				%><td align="right"><%=vecTemp.get(7).toString()%><br></td><% 
				  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){ 
					%><td align="right"><%=vecTemp.get(14).toString()%><br></td><% 
				} 
			} 
			if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){ 
				%><td align="right"><%=vecTemp.get(6).toString()%><br></td><% 
				  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){ 
					%><td align="right"><%=vecTemp.get(13).toString()%><br></td><% 
				} 
			} 
			if (!strAccessLevel.equals(DebisysConstants.MERCHANT)){ 
				%><td align="right"><%=vecTemp.get(5).toString()%><br></td><% 
				  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){ 
					%><td align="right"><%=vecTemp.get(12).toString()%><br></td><% 
				} 
			} 

                        if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                            out.println("<td align=right><a href=\"admin/reports/transactions/merchants_summary.jsp?startDate="
                                    + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(
                                    TransactionReport.getEndDate(), "UTF-8") + "&search=y&repId=" + vecTemp.get(1) + "&report=y\">"
                                    + vecTemp.get(4).toString() + "</a></td>");
                        } else {
                            out.println("<td align=\"right\">" + vecTemp.get(4).toString() + "<br></td>");
                        }

			%>
                                        
                        
                        
                        
                        <% 
			  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))){ 
			%>
                        <td align="right"><%=vecTemp.get(11).toString()%><br></td><% 
			} 
			if (strAccessLevel.equals(DebisysConstants.ISO)){ 
				%><td align="right"><%=vecTemp.get(9).toString()%><br></td><% 
			} 
                        // Statement for Data Promo Column
                        if ((SessionData.checkPermission((DebisysConstants.PERM_ALLOW_PROMO_DATA_COLUMN_IN_TRANSACTIONS_REPORT))) && (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) && (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) {
                            if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) { 
%>
                                <td align="right"><%=vecTemp.get(19) %><br></td>
<% 
                            } else {
%>
                                <td align="right"><%=vecTemp.get(16) %><br></td>
<% 
                            }
                        } 
			%></tr><% 
			if (intEvenOdd == 1){ 
				intEvenOdd = 2; 
			}else{ 
				intEvenOdd = 1; 
			} 
		} 
	} 
	vecSearchResults.clear(); 
	out.println("</table>"); 
}else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null){ 
 	out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>"); 
} 
%>
			</td>
		</tr>
	</table>
<%@ include file="/includes/footer.jsp" %>
