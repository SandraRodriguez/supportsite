<%@ page import="java.util.*,
         java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    String strReport = request.getParameter("report");
    int section = 3;
    int section_page = 3;

    if (strReport != null && strReport.equals("y")) {
        section = 4;
        section_page = 14;
    }
    else {
        strReport = "";
    }
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%    Vector vecSearchResults = new Vector();
    Hashtable searchErrors = null;
    int intRecordCount = 0;
    int intPage = 1;
    int intPageSize = 50;
    int intPageCount = 1;

//this flag means if the user is in International Default
    boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

    boolean viewReferenceCard = false;
    if (isInternationalDefault) {
        viewReferenceCard = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
    }

    boolean showAccountIdQRCode = false;
    showAccountIdQRCode = SessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);

    if (request.getParameter("search") != null) {

        if (request.getParameter("page") != null) {
            try {
                intPage = Integer.parseInt(request.getParameter("page"));
            } catch (NumberFormatException ex) {
                intPage = 1;
            }
        }

        if (intPage < 1) {
            intPage = 1;
        }

        if (TransactionSearch.validateDateRange(SessionData)) {
            SessionData.setProperty("start_date", request.getParameter("startDate"));
            SessionData.setProperty("end_date", request.getParameter("endDate"));
            if (request.getParameter("millennium_no") != null) {
                TransactionSearch.setMillennium_No(request.getParameter("millennium_no"));
            }

            vecSearchResults = TransactionSearch.search(intPage, intPageSize, SessionData, section_page, application, (request.getParameter("chkUseTaxValue") != null));
            intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
            vecSearchResults.removeElementAt(0);
            if (intRecordCount > 0) {
                intPageCount = (intRecordCount / intPageSize);
                if ((intPageCount * intPageSize) < intRecordCount) {
                    intPageCount++;
                }
            }
        }
        else {
            searchErrors = TransactionSearch.getErrors();
        }

    }
%>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
    var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ", "< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");

    function scroll()
    {
        document.merchantform.submit.disabled = true;
        document.merchantform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll()', 150);
    }

    function scroll2()
    {
        document.downloadform.submit.disabled = true;
        document.downloadform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll2()', 150);
    }

    function openViewPIN(sTransId)
    {
        var sURL = "/support/admin/transactions/view_pin.jsp?trans_id=" + sTransId;
        var sOptions = "left=" + (screen.width - (screen.width * 0.4)) / 2 + ",top=" + (screen.height - (screen.height * 0.3)) / 2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
        var w = window.open(sURL, "ViewPIN", sOptions, true);
        w.focus();
    }
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
    <tr>
        <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td class="formAreaTitle" align=left width="2000"><%=Languages.getString("jsp.admin.reports.transactions.merchants.transactions.title", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form name="merchantform" method="get" action="admin/transactions/merchants_transactions.jsp" onSubmit="scroll();">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <%if (strReport == null || strReport.equals("")) {%>
                                <tr>
                                    <td class="formArea2">
                                        <table width="300">
                                            <tr class="main">
                                                <td nowrap valign="top"><%if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {%>
                                                    <%=Languages.getString("jsp.admin.select_date_range", SessionData.getLanguage())%>:</td>
                                                    <%}
                         else {%>
                                                    <%=Languages.getString("jsp.admin.select_date_range_international", SessionData.getLanguage())%>:</td>
                                                <%}%><td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" nowrap>
                                                    <table width=400>
                                                        <%
                                                            if (searchErrors != null) {
                                                                out.println("<tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");
                                                                Enumeration enum1 = searchErrors.keys();
                                                                while (enum1.hasMoreElements()) {
                                                                    String strKey = enum1.nextElement().toString();
                                                                    String strError = (String) searchErrors.get(strKey);
                                                                    out.println("<li>" + strError);
                                                                }

                                                                out.println("</font></td></tr>");
                                                            }
                                                        %>
                                                        <tr class="main">
                                                            <td nowrap><%=Languages.getString("jsp.admin.start_date", SessionData.getLanguage())%>: <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if (self.gfPop)
                gfPop.fStartPop(document.merchantform.startDate, document.merchantform.endDate);
            return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a></td>
                                                            <td nowrap><%=Languages.getString("jsp.admin.end_date", SessionData.getLanguage())%>: <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if (self.gfPop)
                gfPop.fEndPop(document.merchantform.startDate, document.merchantform.endDate);
            return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a></td>
                                                            <td class=main>
                                                                <input type="hidden" name="search" value="y">
                                                                <input type="hidden" name="merchantId" value="<%=TransactionSearch.getMerchantId()%>">
                                                                <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.transactions.show_transactions", SessionData.getLanguage())%>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan=3 class="main">
                                                                * <%=Languages.getString("jsp.admin.reports.general", SessionData.getLanguage())%>
                                                            </td>
                                                        </tr>
                                                        <%}%>
                                                    </table>
                                                </td>
                                            </tr>
                                            </form>
                                        </table>
                                        <%
                                            if (vecSearchResults != null && vecSearchResults.size() > 0) {
                                        %>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                            <%
                                                Vector vTimeZoneData = null;
                                                if (SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT)) {
                                                    vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
                                                }
                                                else {
                                                    vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
                                                }
                                            %>
                                            <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
                                            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found", SessionData.getLanguage()) + " "%><%
                                                if (!TransactionSearch.getStartDate().equals("") && !TransactionSearch.getEndDate().equals("")) {
                                                    out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getEndDateFormatted()));
                                                }
                                                    %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{Integer.toString(intPage), Integer.toString(intPageCount)}, SessionData.getLanguage())%></td></tr>
                                            <tr>
                                                <td align=left class="main" nowrap>
                                                    <form name="downloadform" method=post action="admin/transactions/download_transactions.jsp" onSubmit="scroll2();">
                                                        <input type="hidden" name="startDate" value="<%=TransactionSearch.getStartDate()%>">
                                                        <input type="hidden" name="endDate" value="<%=TransactionSearch.getEndDate()%>">
                                                        <input type="hidden" name="page" value="<%=intPage%>">
                                                        <input type="hidden" name="section_page" value="<%=section_page%>">
                                                        <input type="hidden" name="merchantId" value="<%=TransactionSearch.getMerchantId()%>">
                                                        <input type="hidden" name="millennium_no" value="<%=TransactionSearch.getMillennium_No()%>">
                                                        <input type="hidden" name="download" value="y">

                                                        <%
                                                            if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (request.getParameter("chkUseTaxValue") != null)) {
                                                        %>
                                                        <input type="hidden" name="chkUseTaxValue" value="<%=request.getParameter("chkUseTaxValue")%>">
                                                        <%
                                                            }
                                                        %>
                                                        <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download", SessionData.getLanguage())%>">
                                                    </form>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align=right class="main" nowrap>
                                                    <%
                                                        if (intPage > 1) {
                                                            out.println("<a href=\"admin/reports/transactions/transaction_mercsumbytrm_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&millennium_no=" + TransactionSearch.getMillennium_No() + "&merchantId=" + TransactionSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=1&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null) ? "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") : "") + "\">First</a>&nbsp;");
                                                            out.println("<a href=\"admin/reports/transactions/transaction_mercsumbytrm_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&millennium_no=" + TransactionSearch.getMillennium_No() + "&merchantId=" + TransactionSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + (intPage - 1) + "&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null) ? "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") : "") + "\">&lt;&lt;Prev</a>&nbsp;");
                                                        }

                                                        int intLowerLimit = intPage - 12;
                                                        int intUpperLimit = intPage + 12;

                                                        if (intLowerLimit < 1) {
                                                            intLowerLimit = 1;
                                                            intUpperLimit = 25;
                                                        }

                                                        for (int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) {
                                                            if (i == intPage) {
                                                                out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                                                            }
                                                            else {
                                                                out.println("<a href=\"admin/reports/transactions/transaction_mercsumbytrm_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&millennium_no=" + TransactionSearch.getMillennium_No() + "&merchantId=" + TransactionSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + i + "&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null) ? "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") : "") + "\">" + i + "</a>&nbsp;");
                                                            }
                                                        }

                                                        if (intPage <= (intPageCount - 1)) {
                                                            out.println("<a href=\"admin/reports/transactions/transaction_mercsumbytrm_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&millennium_no=" + TransactionSearch.getMillennium_No() + "&merchantId=" + TransactionSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + (intPage + 1) + "&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null) ? "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") : "") + "\">Next&gt;&gt;</a>&nbsp;");
                                                            out.println("<a href=\"admin/reports/transactions/transaction_mercsumbytrm_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&millennium_no=" + TransactionSearch.getMillennium_No() + "&merchantId=" + TransactionSearch.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + (intPageCount) + "&report=" + strReport + ((request.getParameter("chkUseTaxValue") != null) ? "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") : "") + "\">Last</a>");
                                                        }

                                                    %>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" cellspacing="1" cellpadding="2">
                                            <tr>
                                                <td class=rowhead2>#</td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.termtypedesc", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }
                                                %>             

                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba", SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.phys_state", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }
                                                %>                
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.city", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.county", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.reference", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.recharge", SessionData.getLanguage()).toUpperCase()%></td>     
                                                <%
                                                    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                                %>               	  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%               	 	}
                                                else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                                %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                	 }
                                                }
                                                else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                                %>                   	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                  }
                                                else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                                %>		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                 	}
                                                else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                                %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                  }
                                                else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                                %>                    <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                  }
                                                %>				


                                                <%
                                                    }
                                                    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                                %>              




                                                <%//DBSY-905
                                                    if (isInternationalDefault) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.bonus", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.total_recharge", SessionData.getLanguage()).toUpperCase()%></td>

                                                <% if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.netAmount", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxAmount", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxpercentage", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxtype", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%}
                                            }%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.balance", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.product", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.description", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_number", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }

                                                    boolean viewPinNumber = false;
                                                    boolean viewAchDate = false;

                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
                                                            && strAccessLevel.equals(DebisysConstants.ISO)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                        viewPinNumber = true;
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.pin_number", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }
                                                %>              
                                                <%
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
                                                            && strAccessLevel.equals(DebisysConstants.ISO)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                        viewAchDate = true;
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ach_date", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }
                                                %>   

                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_type", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%

                                                    if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                                            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.invoiceno", SessionData.getLanguage()).toUpperCase()%></td>
                                                <% } %>

                                                <%if (viewReferenceCard) {%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.DTU1204", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%}%>
                                                
                                                <%if (showAccountIdQRCode) {%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.reference", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%}%>
                                                
                                                

                                            </tr>
                                            <%
                                                int intCounter = 1;
                                                //intCounter = intCounter - ((intPage-1)*intPageSize);

                                                Iterator it = vecSearchResults.iterator();
                                                int intEvenOdd = 1;
                                                String type = "";
                                                String row = "";

                                                String PinNumberInfo = "";
                                                String achDateInfo = "";

                                                while (it.hasNext()) {
                                                    Vector vecTemp = null;
                                                    vecTemp = (Vector) it.next();
                                                    // check if type is "Return" of "Adjust", if so, display the whole row in red
                                                    type = (String) vecTemp.get(14);
                                                    if (type.equals("Return") || type.equals("Adjustment") || type.equals("Credit") || type.equals("Write Off")) {
                                                        row = "rowred";
                                                    }
                                                    else {
                                                        row = "row";
                                                    }

                                                    String sViewPIN = "", sPhysState = "", sAuthorizationNo = "";
                                                    if (viewPinNumber/*SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)*/) {
                                                        // LOCALIZATION - Section modified to accept just a result string with the currency amount
                                                        // From the DB.
                                                        //if the trans_type=2 which is a pin sale
                                                        if (vecTemp.get(13).toString().equals("2") && Double.parseDouble(vecTemp.get(9).toString().replaceAll(",", "")) > 0) {
                                                            sViewPIN = "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + vecTemp.get(0).toString() + "');\"><span class=\"showLink\">" + vecTemp.get(12).toString() + "</span></a>";
                                                            if (viewPinNumber) {
                                                                PinNumberInfo = "<td nowrap>" + vecTemp.get(19).toString() + "</td>";
                                                            }
                                                            else {
                                                                PinNumberInfo = "<td nowrap>N/A</td>";
                                                            }
                                                        }
                                                        else {
                                                            sViewPIN = vecTemp.get(12).toString();
                                                            PinNumberInfo = "<td nowrap>N/A</td>";
                                                        }
                                                    }
                                                    else {
                                                        sViewPIN = vecTemp.get(12).toString();
                                                    }

                                                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                        sPhysState = "<td nowrap>" + vecTemp.get(16).toString() + "</td>";
                                                    }
                                                    else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                        sAuthorizationNo = "<td nowrap>" + vecTemp.get(15).toString() + "</td>";
                                                    }

                                                    if (viewAchDate) {
                                                        achDateInfo = "<td nowrap>" + vecTemp.get(20).toString() + "</td>";
                                                    }
                                                    String terminalDescription = "";
                                                    if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                        terminalDescription = "<td>" + vecTemp.get(21) + "</td>";
                                                    }
                                                    String[] productDescription = vecTemp.get(11).toString().split("\\/");

                                                    out.println("<tr class=" + row + intEvenOdd + ">"
                                                            + "<td>" + intCounter++ + "</td>"
                                                            + "<td>" + vecTemp.get(0) + "</td>"
                                                            + "<td>" + vecTemp.get(1) + "</td>"
                                                            + terminalDescription
                                                            + "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString()) + "</td>"
                                                            + "<td>" + vecTemp.get(3) + "</td>"
                                                            + "<td nowrap>" + vecTemp.get(4) + "</td>"
                                                            + sPhysState
                                                            + //city
                                                            "<td nowrap>" + vecTemp.get(5).toString() + "</td>"
                                                            + //county
                                                            "<td nowrap>" + vecTemp.get(28).toString() + "</td>"
                                                            + //clerk
                                                            "<td nowrap>" + vecTemp.get(6) + "</td>"
                                                            + "<td>" + vecTemp.get(7) + "</td>"
                                                            + "<td>" + vecTemp.get(8) + "</td>"
                                                            + //DBSY-905 Tax Calculation
                                                            //amount
                                                            "<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>");

                                                    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(31).toString()) + "</td>"); //iso
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //rep
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(32).toString()) + "</td>"); //merchant
                                                            }
                                                            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(31).toString()) + "</td>"); //iso
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(29).toString()) + "</td>"); //agente
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(30).toString()) + "</td>"); //subagente
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //rep
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(32).toString()) + "</td>"); //merchant
                                                            }
                                                        }
                                                        else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(29).toString()) + "</td>"); //agente
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(30).toString()) + "</td>"); //subagente
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //rep
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(32).toString()) + "</td>"); //merchant
                                                        }
                                                        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(30).toString()) + "</td>"); //subagente
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //rep
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(32).toString()) + "</td>"); //merchant
                                                        }
                                                        else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //rep
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(32).toString()) + "</td>"); //merchant
                                                        }
                                                        else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(32).toString()) + "</td>"); //merchant
                                                        }
                                                    }
                                                    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/

                                                    //DBSY-905 Tax Calculation
                                                    if (isInternationalDefault) {
                                                        //bonus
                                                        out.println("<td nowrap>" + vecTemp.get(17) + "</td>"
                                                                + //total Recharge
                                                                "<td nowrap>" + vecTemp.get(18) + "</td>");
                                                        if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                                                            //net amount 
                                                            out.println("<td nowrap>" + vecTemp.get(24) + "</td>"
                                                                    + //tax amount
                                                                    "<td nowrap>" + vecTemp.get(25) + "</td>");

                                                            //tax percentage
                                                            out.println("<td nowrap>" + vecTemp.get(26) + "%" + "</td>"
                                                                    + //tax type
                                                                    "<td nowrap>" + vecTemp.get(27) + "</td>");

                                                        }
                                                    }

                                                    //bal
                                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(10).toString()) + "</td>"
                                                            + //product id
                                                            "<td nowrap>" + productDescription[0] + "</td>"
                                                            + //description product
                                                            "<td nowrap>" + productDescription[1] + "</td>"
                                                            + "<td>" + sViewPIN + "</td>"
                                                            + PinNumberInfo + achDateInfo + sAuthorizationNo
                                                            + "<td>" + type + "</td>");
                                                    if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application) && isInternationalDefault) {
                                                        // DBSY-919 Invoice Number
                                                        out.println("<td>" + vecTemp.get(23));
                                                    }

                                                    if (viewReferenceCard) {
                                                        out.println("<td>" + vecTemp.get(34) + "</td>");
                                                    }

                                                    if (vecTemp.size() >= 36 && showAccountIdQRCode) {
                                                        out.println("<td>" + vecTemp.get(35) + "</td>");
                                                    }
                                                    else if (showAccountIdQRCode) {
                                                        out.println("<td></td>");
                                                    }
                                                    out.println("</tr>");

                                                    if (intEvenOdd == 1) {
                                                        intEvenOdd = 2;
                                                    }
                                                    else {
                                                        intEvenOdd = 1;
                                                    }

                                                }
                                                vecSearchResults.clear();
                                            %>
                                        </table>

                                        <%
                                            }
                                            else if (intRecordCount == 0 && request.getParameter("search") != null && searchErrors == null) {
                                                out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                                            }
                                        %>
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>