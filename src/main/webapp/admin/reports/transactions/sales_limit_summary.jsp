<%@ page import="
				java.util.*,
				com.debisys.customers.Merchant,
				com.debisys.utils.NumberUtil,
				com.debisys.utils.DateUtil"
%>
<%
int section=4;
int section_page=45;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
	int nRecordCount = 0;
	int nPage = 1;
	int nPageSize = 50;
	int nPageCount = 1;
	Vector<Vector<Object>> vecSearchResults = new Vector<Vector<Object>>();
	String sortString = "";

	if ( request.getParameter("download") != null )
	{
		String sURL = "";
		TransactionReport.setMerchantIds(request.getParameterValues("mids"));
		sURL = TransactionReport.downloadSalesLimitSummary(application, SessionData, request.getParameter("salesLimitType"));
		response.sendRedirect(sURL);
		return;
	}

	if (request.getParameter("page") != null)
	{
		try
		{
			nPage = Integer.parseInt(request.getParameter("page"));
			if (nPage < 1) { nPage = 1; }
		}
		catch (Exception ex)
		{
			nPage = 1;
		}
	}

	if (request.getParameter("mids") != null)
	{
		TransactionReport.setMerchantIds(request.getParameterValues("mids"));
		vecSearchResults = TransactionReport.getSalesLimitSummary(nPageSize, nPage, SessionData, application, request.getParameter("salesLimitType"));
		nRecordCount = Integer.parseInt(((Vector<Object>)vecSearchResults.get(0)).get(0).toString());
		vecSearchResults.removeElementAt(0);
		if (nRecordCount > 0)
		{
			nPageCount = (nRecordCount / nPageSize);
			if ((nPageCount * nPageSize) < nRecordCount)
			{
				nPageCount++;
			}
		}
	}
%>
<%@ include file="/includes/header.jsp" %>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= Languages.getString("jsp.admin.saleslimit.reportTitle",SessionData.getLanguage()).toUpperCase() %></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
			<form id="frmSubmit" method="post" action="admin/reports/transactions/sales_limit_summary.jsp">
				<input type="hidden" name="mids" value="<%=TransactionReport.getMerchantIds()%>">
				<input type="hidden" name="salesLimitType" value="<%=request.getParameter("salesLimitType")%>">
				<input type="hidden" id="varPage" name="page" value="1">
			</form>
			<script>
				function GoToPage(nPage)
				{
					document.getElementById('varPage').value = nPage;
					document.getElementById('frmSubmit').submit();
				}
			</script>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td class=main align=left valign=bottom>
						<form method="post" action="admin/reports/transactions/sales_limit_summary.jsp?download=">
							<input type="hidden" name="mids" value="<%=TransactionReport.getMerchantIds()%>">
							<input type="hidden" name="salesLimitType" value="<%=request.getParameter("salesLimitType")%>">
							<input type="submit" value="<%=Languages.getString("jsp.admin.saleslimit.download",SessionData.getLanguage())%>">
						</form>
					</td>
					<td class=main align=right valign=bottom><%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%><br><br></td>
				</tr>
				<tr>
					<td colspan="2" align=right class="main" nowrap>
<%
		if (nPage > 1)
		{
%>
						<a href="javascript:GoToPage(1)"><%=Languages.getString("jsp.admin.first",SessionData.getLanguage())%></a>
						<a href="javascript:GoToPage(<%=nPage - 1%>)">&lt;&lt;<%=Languages.getString("jsp.admin.previous",SessionData.getLanguage())%></a>
<%
		}

		int nLowerLimit = nPage - 12;
		int nUpperLimit = nPage + 12;

		if (nLowerLimit < 1)
		{
			nLowerLimit = 1;
			nUpperLimit = 25;
		}

		for (int i = nLowerLimit; i <= nUpperLimit && i <= nPageCount; i++)
		{
			if (i == nPage)
			{
				out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
			}
			else
			{
%>
						<a href="javascript:GoToPage(<%=i%>)"><%=i%></a>
<%
			}
		}

		if (nPage <= (nPageCount - 1))
		{
%>
						<a href="javascript:GoToPage(<%=nPage + 1%>)"><%=Languages.getString("jsp.admin.next",SessionData.getLanguage())%>&gt;&gt;</a>
						<a href="javascript:GoToPage(<%=nPageCount%>)"><%=Languages.getString("jsp.admin.last",SessionData.getLanguage())%></a>
<%
		}

%>
					</td>
				</tr>
			</table>
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2" align="center">#</td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.salesLimitType",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.salesLimitMonday",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.salesLimitTuesday",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.salesLimitWednesday",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.salesLimitThursday",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.salesLimitFriday",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.salesLimitSaturday",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.salesLimitSunday",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.maximumSalesLimit",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.currentSales",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.currentAvailableSales",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.currentCreditLimit",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.currentRunningLiability",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.availableCredit",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.lastExtensionDate",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						<td class="rowhead2" align="center" nowrap>&nbsp;<%=Languages.getString("jsp.admin.saleslimit.numberExtensions90Days",SessionData.getLanguage()).toUpperCase()%>&nbsp;<br></td>
						
					</tr>
				</thead>
<%
		Iterator<Vector<Object>> it = vecSearchResults.iterator();
		int intEvenOdd = 1;
		int intCounter = 1;

		if (it.hasNext())
		{
			Vector<Object> vecTemp = null;
			while (it.hasNext())
			{
				vecTemp = it.next();
%>
					<tr class="row<%=intEvenOdd%>">
						<td><%=intCounter++%></td>
						<td><%=vecTemp.get(0)%></td>
						<td><%=vecTemp.get(1)%></td>
						<td><%=Languages.getString("jsp.admin.saleslimit.salesLimitType" + vecTemp.get(2),SessionData.getLanguage())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(3).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(4).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(5).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(6).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(7).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(8).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(9).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(10).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(11).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(12).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(13).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(14).toString())%></td>
						<td align="right"><%=NumberUtil.formatAmount(vecTemp.get(15).toString())%></td>
						<td align="right"><%=DateUtil.formatDate((Date)vecTemp.get(16))%></td>
						<td align="right"><%=vecTemp.get(17)%></td>
					</tr>
<%
				vecTemp = null;
			}
		}
%>
			</table>
<%
	}
	else
	{
		out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
	}

	if (vecSearchResults.size() > 0)
	{
%>
			<SCRIPT type="text/javascript"><!--
				var stT1 = new SortROC(document.getElementById("t1"),
				["None","CaseInsensitiveString","CaseInsensitiveString","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Date","Number"],0,false,false);
			--></SCRIPT>
<%
	}
%>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>
