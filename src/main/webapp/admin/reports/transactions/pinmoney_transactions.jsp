<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=54;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;

if ( request.getParameter("Download") != null )
{
	  String sURL = "";
      sURL = TransactionReport.downloadPINMoneyTransactions(application, SessionData, request.getParameter("transactionID"), request.getParameter("PINNumber"));
	  response.sendRedirect(sURL);
	  return;
}
if (request.getParameter("search") != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  if (TransactionReport.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    String strMerchantIds[] = request.getParameterValues("merchantIds");
    if (strMerchantIds != null)
    {
      TransactionReport.setMerchantIds(strMerchantIds);
    }

    vecSearchResults = TransactionReport.getPINMoneyTransactions(intPage, intPageSize, SessionData, application, request.getParameter("transactionID"), request.getParameter("PINNumber"), false);

    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize) + 1;
      if ((intPageCount * intPageSize) + 1 >= intRecordCount)
      {
        intPageCount++;
      }
    }
  }
  else
  {
   searchErrors = TransactionReport.getErrors();
  }


}
%>
<%@ include file="/includes/header.jsp" %>
<SCRIPT>

function openViewPIN(sTransId)
{
  var sURL = "/support/admin/transactions/view_pin.jsp?trans_id=" + sTransId;
  var sOptions = "left=" + (screen.width - (screen.width * 0.4))/2 + ",top=" + (screen.height - (screen.height * 0.3))/2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
  var w = window.open(sURL, "ViewPIN", sOptions, true);
  w.focus();
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
  <tr>
	  <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	  <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.PINMoney_Transactions_Report.title",SessionData.getLanguage()).toUpperCase()%></td>
	  <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#FFFFFF" class="formArea2">

<%
if (searchErrors != null)
{
  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr></table>");
}

  if (vecSearchResults != null && vecSearchResults.size() > 0)
{

String baseURL = "admin/reports/transactions/pinmoney_transactions.jsp?search=" 
+ URLEncoder.encode(request.getParameter("search"), "UTF-8") 
+ "&productId=" + TransactionReport.getProductId() 
+ "&transactionID=" + request.getParameter("transactionID") 
+ "&PINNumber=" + request.getParameter("PINNumber") 
+ "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") 
+  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")
+  "&merchantIds=" + URLEncoder.encode(TransactionReport.getMerchantIds(), "UTF-8");
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
	Vector vTimeZoneData = null;
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
	        <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
              if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                   out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount-1)},SessionData.getLanguage())%></td></tr>
            <tr><td class="main">
            	<%=Languages.getString("jsp.admin.iso_name",SessionData.getLanguage())%>:&nbsp;<%=SessionData.getUser().getCompanyName()%>
            </td></tr>
            <tr>
            	<td colspan=2>
            <table>
              <tr>
                <td>
                <FORM ACTION="admin/reports/transactions/pinmoney_transactions.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                	<INPUT TYPE=hidden NAME=search VALUE="<%=request.getParameter("search")%>">
                	<INPUT TYPE=hidden NAME=productId VALUE="<%=request.getParameter("productId")%>">
                	<INPUT TYPE=hidden NAME=merchantIds VALUE="<%=request.getParameter("merchantIds")%>">
                	<INPUT TYPE=hidden NAME=transactionID VALUE="<%=request.getParameter("transactionID")%>">
                	<INPUT TYPE=hidden NAME=PINNumber VALUE="<%=request.getParameter("PINNumber")%>">
                	<INPUT TYPE=hidden NAME=Download VALUE="Y">
                	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                </FORM>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                </td>
              </tr>
            </table>
            	</td>
            </tr>            
            <tr>
              <td align=right class="main" nowrap>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"" + baseURL + "&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"" + baseURL + "&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }

              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"" + baseURL + "&page=" + i + "\">" +i+ "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"" + baseURL + "&page=" + (intPage+1) + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"" + baseURL + "&page=" + (intPageCount-1) + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
              }

              %>
              </td>
            </tr>

            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
              <td class=rowhead2>#</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
<%
				if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
%>
  			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.phys_state",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.city",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.county",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.reference",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()).toUpperCase()%></td>
               <td class=rowhead2><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%></td>   
<%
/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                	if(strAccessLevel.equals(DebisysConstants.ISO)){
               	 		if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
%>               	  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;</td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%               	 	}
                	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>					  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;</td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                	 }
                   	}
                   	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
%>                   	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
                   	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                 	}
                   	else if (strAccessLevel.equals(DebisysConstants.REP)){
%>					  <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
                   	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
%>                    <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
%>				


<%
				}
/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/				
%>                             
                          
				<%//DBSY-905
				if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
				%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%></td>
					  
			        <% if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
			    	  {%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxpercentage",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxtype",SessionData.getLanguage()).toUpperCase()%></td>
					<%}
				}%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.description",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()).toUpperCase()%></td>
<%
				if (false && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
%>
  			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.vendor_id",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
%>
<%
				if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
					&& strAccessLevel.equals(DebisysConstants.ISO)
					&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
%>
				<td class=rowhead2><%=Languages.getString("jsp.admin.reports.pin_number",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
%>
<% 
				if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
					&& strAccessLevel.equals(DebisysConstants.ISO)
					&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
%>
				<td class=rowhead2><%=Languages.getString("jsp.admin.reports.ach_date",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
%>                
<%
				if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
				{
%>
  			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_number",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
				if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{
%>			<td class=rowhead2><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage()).toUpperCase()%></td>
            <%} %>
			<td class=rowhead2><%=Languages.getString("jsp.admin.reports.pinstatus",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <%
                  int intCounter = 1;
                  //intCounter = intCounter - ((intPage-1)*intPageSize);

                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();

                    String sViewPIN = "", sPhysState = "", sAuthorizationNo = "", sVendorID = "";
                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) )
                    {
                      //if the trans_type=2 which is a pin sale
                      String strCurrencySymbol = NumberUtil.getCurrencySymbol();
                      if ( vecTemp.get(11).toString().split("\\/")[0].equals("3322457") ) //Debit PIN
                      {
                      	Vector<Vector<String>> vp = TransactionReport.getPINMoneyPinsFromDebitPIN(Long.valueOf(vecTemp.get(0).toString()));
                      	sViewPIN += "<div";
                      	if ( vp.size() > 3 )
                      	{
                      		sViewPIN += " style=\"overflow:auto;height:100px;\">";
                      	}
                      	else if ( vp.size() == 0 )
                      	{
                      		sViewPIN += "N/A";
                      	}
                      	else
                      	{
                      		sViewPIN += ">";
                      	}

                      	Iterator<Vector<String>> itp = vp.iterator();
                      	while ( itp.hasNext() )
                      	{
                      		Vector<String> vpItem = itp.next();
	                        sViewPIN += "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + vpItem.get(0) + "');\"><span class=\"showLink\">" + vpItem.get(1) + "</span></a><br/>";
                      	}
                      	sViewPIN += "</div>";
                      }
                      else
                      {
                        sViewPIN = vecTemp.get(12).toString();
                      }
                    }
                    else
                    {
                      sViewPIN = vecTemp.get(12).toString();
                    }
                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                    {
						sPhysState = "<td nowrap>" + vecTemp.get(15).toString() + "</td>";
						
						if ( vecTemp.get(19).toString().equals("154"))
						{
							sVendorID = "<td nowrap>" + vecTemp.get(18).toString() + "</td>";
						}
						else
						{
							sVendorID = "<td nowrap>N/A</td>";
						}
                    }
                    else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                    {
						sAuthorizationNo = "<td nowrap>" + vecTemp.get(14).toString() + "</td>";
                    }
					String[] productDescription = vecTemp.get(11).toString().split("\\/");
					
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString())  + "</td>" +
                                "<td>" + vecTemp.get(3) + "</td>" +
                                "<td nowrap>" + vecTemp.get(4) + "</td>" +
                                
                                sPhysState +
                                 //city
                                "<td nowrap>" +  vecTemp.get(5) + "</td>" +
                                //county
                                "<td nowrap>" +  vecTemp.get(27)+ "</td>" +
                                //clerk
                                "<td nowrap>" + vecTemp.get(6) + "</td>" +
                                "<td>" + vecTemp.get(7) + "</td>" +
                                "<td>" + vecTemp.get(8) + "</td>" +
                                "<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>");
                                
								/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
								
							//if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
							 if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
			                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
			                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
			                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
			                     	 }
			                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			                     	 	out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
			                     	 }
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
                         				out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                        }
							
							}	
					         else if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
			                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
			                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
			                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
			                     	 }
			                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			                     	 	out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
			                     	 }
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
                         				out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                        }
		                        
		                        
		                        }
		                         else if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
									 if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
				                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
				                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
				                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //iso
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
				                     	 }
				                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
				                     	 	out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //iso
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //agente
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //subagente
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
				                     	 }
			                         	}
			                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //agente
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //subagente
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
			                         	}
			                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //subagente
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
			                         	}
			                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
			                         	}
			                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
			                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
			                         	}
			                        }			                         
		                         }
		                          
		                        else if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
							         (request.getParameter("chkUseTaxValue") != null)
							       ) {

		                        	if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
			                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
			                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
			                     	 }
			                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			                     	 	out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
			                     	 }
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
		                      			out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                        }		                        
		                        }
		                        else if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) 
							        {
		                        if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT))
		                        {
			                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
			                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
			                     	 }
			                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			                     	 	out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
			                     	 }
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
		                         	}
		                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //merchant
		                         	}		                        
		                          }
		                        }
		                        else {
		                        	if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
			                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
			                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
			                     	 }
			                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			                     	 	out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
			                     	 }
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
		                      			out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(28).toString())+ "</td>"); //merchant
		                         	}
		                        }
		                        
		                        }  
		                    	/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/                                
                                //DBSY-905 Tax Calculation
			                    //amount
			                    
			                    if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
			                       //bonus
			                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>" +
			                      //total Recharge
			                                "<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>");
			                        if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
				    	 					{
			                     //net amount 
			                         	       out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>" +
			                     //tax amount
			                                 "<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>");
			                     //tax percentage
			                         	       out.println("<td nowrap>" + vecTemp.get(25).toString() + "%" + "</td>" +
			                     //tax type
			                                 "<td nowrap>" + vecTemp.get(26).toString() + "</td>");
			                                }
			                    }
                                //bal
                                 out.println("<td nowrap>" + vecTemp.get(10) + "</td>" +
                                "<td nowrap>" + productDescription[0] + "</td>" +
                                "<td nowrap>" + productDescription[1] + "</td>" +
                                "<td>" + sViewPIN + "</td>"/* +
                                sVendorID*/
                                );
					if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
						&& strAccessLevel.equals(DebisysConstants.ISO)
						&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{					
					out.println("<td>" + vecTemp.get(16) + "</td>");
					}
					if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
						&& strAccessLevel.equals(DebisysConstants.ISO)
						&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{					
					out.println("<td>" + vecTemp.get(17) + "</td>");
					} 
                    out.println(sAuthorizationNo);
                    if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{
	                    //DBSY-919 Invoice Number Field
						out.println("<td>" + vecTemp.get(20) + "</td>");
						//////     
					}
					out.println("<td>" + Languages.getString("jsp.admin.reports.pinmoneystatus_" + vecTemp.get(35), SessionData.getLanguage()) + "</td>");
			   		out.println("</tr>");
				   		
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>

<%
}
else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.reports.transactions.products_transactions.error",SessionData.getLanguage())+"</font>");
}
%>

</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>