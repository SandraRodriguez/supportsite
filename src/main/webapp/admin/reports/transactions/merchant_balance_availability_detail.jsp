<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.NumberUtil"%>
<%@page import="com.debisys.reports.schedule.MerchantAvailableBalanceReport" %>

<%
    int section = 4;
    int section_page = 21;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*" />
<jsp:useBean id="MerchantAvailableBalanceReport" class="com.debisys.reports.schedule.MerchantAvailableBalanceReport" scope="request"/>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<%
	String path = request.getContextPath(); 
%>
<style type="text/css" title="currentStyle">
    @import "<%=path%>/includes/media/demo_page.css";
    @import "<%=path%>/includes/media/demo_table.css";
</style>
  
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>

<% 
	Vector<Vector<Object>> searchResults = new Vector<Vector<Object>>();
	Hashtable<?, ?> pageErrors = null;
	Hashtable<String, String> searchErrors = new Hashtable<String, String>();
	String reportTitle = Languages.getString("jsp.admin.reports.titleABR.detail", SessionData.getLanguage()).toUpperCase();
	MerchantAvailableBalanceReport report = new MerchantAvailableBalanceReport(application);
    report.setSessionData(SessionData);
    report.setStartDate(request.getParameter("startDate"));
    report.setEndDate(request.getParameter("endDate"));
    report.setStrRefId(SessionData.getProperty("ref_id"));
    report.setReportTitle(reportTitle);
    String[] merchantId = request.getParameterValues("merchantIdTxt");
    String merchantType = request.getParameter("merchantType");
    if(merchantId == null){
    	merchantId = request.getParameterValues("merchantIdTxt[]");
    }
    report.setMerchantIdList(merchantId);
    String sharedBalance = request.getParameter("sharedBalance");
    boolean isSharedBalance = Boolean.parseBoolean(sharedBalance);
    report.setSharedBalance(Boolean.parseBoolean(sharedBalance));
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    
    if ((request.getParameter("search") != null) && (request.getParameter("search").toLowerCase().equals("y"))) {
    	String field = (merchantId != null && merchantId.length > 0) ? merchantId[0] : null;
	    if (TransactionReport.validateDateRange(SessionData) && TransactionReport.validateManadatoryField(field, "jsp.admin.reports.paymentrequest_search.checkMerchantId", SessionData)) {
	    	boolean validMerchant = true;
	    	boolean validRep = true;
	    	if(merchantType.equals("1") && !isSharedBalance){
	    		validMerchant = report.validateMerchant(field);
	    	} else if(merchantType.equals("1") && isSharedBalance){
	    		validRep = report.validateRep(field);
	    	}
	    	if(validMerchant && validRep){
	    		searchResults = report.getResults();
	    	} else {
	    		String error = "";
	    		if(!validMerchant){
	    			error = Languages.getString("jsp.admin.reports.abr.error.invalid_merchant", SessionData.getLanguage());
	    		} else if(!validRep){
	    			error = Languages.getString("jsp.admin.customers.reps_add.error_rep_id", SessionData.getLanguage());
	    		}
	    		searchErrors.put("merchantId", error);
	    	}
	    } else {
	    	searchErrors = TransactionReport.getErrors();
	    }
    } else if (request.getParameter("downloadReport") != null && request.getParameter("downloadReport").equals("y")) {
        report.setScheduling(false);
        String zippedFilePath = report.downloadReport();
        if ((zippedFilePath != null) && (!zippedFilePath.trim().equals(""))) {
            response.sendRedirect(zippedFilePath);
        }
    }
    
    ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
    String titleMerchantId = reportColumns.get(0).getLanguageDescription();
    String titleMerchantName = reportColumns.get(1).getLanguageDescription();
    String titleRepName = reportColumns.get(2).getLanguageDescription();
    String titleInitialBal = reportColumns.get(3).getLanguageDescription();
    String titleSales = reportColumns.get(4).getLanguageDescription();
    String titlePayments = reportColumns.get(5).getLanguageDescription();
    String titleCommissions = reportColumns.get(6).getLanguageDescription();
    String titleFinalBal = reportColumns.get(7).getLanguageDescription();
    
    String pageTitle            = Languages.getString("jsp.tools.searchmerchant.title",SessionData.getLanguage()).toUpperCase();                  
    String labelProcessing      = Languages.getString("jsp.tools.datatable.processing",SessionData.getLanguage());
    String labelShowRecords     = Languages.getString("jsp.tools.datatable.showRecords",SessionData.getLanguage());
    String labelNoRecords       = Languages.getString("jsp.tools.datatable.norecords",SessionData.getLanguage());
    String labelNoDataAvailable = Languages.getString("jsp.tools.datatable.noDataAvailable",SessionData.getLanguage());
    String labelInfo            = Languages.getString("jsp.tools.datatable.info",SessionData.getLanguage());
    String labelInfoEmpty       = Languages.getString("jsp.tools.datatable.infoEmpty",SessionData.getLanguage());
    String labelFilter          = Languages.getString("jsp.tools.datatable.filter",SessionData.getLanguage());
    String labelSearch          = Languages.getString("jsp.tools.datatable.search",SessionData.getLanguage());
    String labelLoading         = Languages.getString("jsp.tools.datatable.loading",SessionData.getLanguage());
    String labelFirst           = Languages.getString("jsp.tools.datatable.first",SessionData.getLanguage());
    String labelLast            = Languages.getString("jsp.tools.datatable.last",SessionData.getLanguage());
    String labelNext            = Languages.getString("jsp.tools.datatable.next",SessionData.getLanguage());
    String labelPrevious        = Languages.getString("jsp.tools.datatable.previous",SessionData.getLanguage());
    String labelShow            = Languages.getString("jsp.tools.datatable.show",SessionData.getLanguage());
    String labelRecords         = Languages.getString("jsp.tools.datatable.records",SessionData.getLanguage());
    
    String pagereturn         = request.getParameter("pagereturn");
    String code               = request.getParameter("code");
%>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() 
        {	                                
            setTableMerchants();                                
        }
    );
</script>
    
<script type="text/javascript" charset="utf-8">
    function setTableMerchants() {
            $('#merchantsTable').dataTable( 
            	{
                    "iDisplayLength": 50,
                    "bLengthChange": true,
                    "bFilter": true,
                    "sPaginationType": "full_numbers",

                    "oLanguage": {
                       "sProcessing":      "<%=labelProcessing%>",
                                "sLengthMenu":     "<%=labelShowRecords%>",
                                "sZeroRecords":    "<%=labelNoRecords%>",
                                "sEmptyTable":     "<%=labelNoDataAvailable%>",
                                "sInfo":           "<%=labelInfo%>",
                                "sInfoEmpty":      "<%=labelInfoEmpty%>",
                                "sInfoFiltered":   "<%=labelFilter%>",
                                "sInfoPostFix":    "",
                                "sSearch":         "<%=labelSearch%>",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "<%=labelLoading%>",
                                "oPaginate": {
                                    "sFirst":    "<%=labelFirst%>",
                                    "sLast":     "<%=labelLast%>",
                                    "sNext":     "<%=labelNext%>",
                                    "sPrevious": "<%=labelPrevious%>"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                },
                                "sLengthMenu": ' <%=labelShow%> <select id="pagesNumbers" >'+
                                                            '<option value="10">10</option>'+
                                                            '<option value="20">20</option>'+
                                                            '<option value="30">30</option>'+
                                                            '<option value="40">40</option>'+
                                                            '<option value="50">50</option>'+
                                                            '<option value="-1">All</option>'+
                                                            '</select> <%=labelRecords%>'
                     },
                 	"aaSorting": [[ 0, "asc" ]]     
            	}				
            );
    }
</script>

<%
	if (searchResults != null && searchResults.size() > 0) {
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
    }
%>

<table border="0" cellpadding="0" cellspacing="0" width="420">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
            &nbsp;
            <%= reportTitle%>
        </td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
        	<%
	        	if (!searchErrors.isEmpty()) {
	                out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");
	
	                Enumeration<?> enum1 = searchErrors.keys();
	
	                while (enum1.hasMoreElements()) {
	                    String strKey = enum1.nextElement().toString();
	                    String strError = (String) searchErrors.get(strKey);
	
	                    out.println("<li>" + strError);
	                }
	                out.println("</font></td></tr></table>");
	            }
        	
        		if (searchResults != null && searchResults.size() > 0) {
        	%>
		     <table width="100%" border="0" cellspacing="0" cellpadding="2">
		        <tr>
		            <td class="main">
		            	<%=reportTitle%>
		            	<%
                            if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                                out.println(Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted())
                                        + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                            }
                        %>
                        <br/>
		        	</td>
		        	<td class=main align=right valign=bottom>
                        <%= Languages.getString("jsp.admin.reports.click_to_sort", SessionData.getLanguage())%>
                    </td>
		        </tr>
		        <tr>
		        	<td>
			            <FORM ACTION="admin/reports/transactions/merchant_balance_availability_detail.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
			                <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
			                <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
			                <input type="hidden" name="downloadReport" value="y">
			                <input type="hidden" name="search" value="n">
			                <input type="hidden" name="sheduleReport" value="n">
			                <input type="hidden" name="sharedBalance" value="<%=sharedBalance%>">
			                <INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.downloadToCsv", SessionData.getLanguage())%>">
			                <%
			                	for(String id : merchantId){
			                %>
			                		<INPUT TYPE=hidden NAME="merchantIdTxt" VALUE="<%=id%>">
			                <%	} %>
			            </FORM>
			            <form action="admin/reports/transactions/merchant_balance_availability.jsp">
			            	<input type="submit" name="submit" onclick="validateSchedule(0);" value="<%=Languages.getString("jsp.admin.tools.smsInventory.goBack", SessionData.getLanguage())%>">
			            </form>
			        </td>
		        </tr>
		    </table>
		    <table width="100%" cellspacing="1" cellpadding="1" border="0" class="display" id="merchantsTable" name="merchantsTable">
			    <thead>
			        <tr class="rowhead2">
			            <th>#</th>
		                <th><%=titleMerchantId%></th>
    					<th><%=titleMerchantName%></th>
    					<th><%=titleRepName%></th>
					    <th><%=titleInitialBal%></th>
					    <th><%=titleSales%></th>
					    <th><%=titlePayments%></th>
					    <th><%=titleCommissions%></th>
					    <th><%=titleFinalBal%></th>
			        </tr>
			    </thead>
			    <%
			    	int intEvenOdd = 1;
			    	int intCounter = 1;
				    BigDecimal totalSales = new BigDecimal(0d);
			        BigDecimal totalPayments = new BigDecimal(0d);
			        BigDecimal totalCommissions = new BigDecimal(0d);
			        BigDecimal totalFinalBalance = new BigDecimal(0d);
			        BigDecimal totalInitialBalance = new BigDecimal(0d);
			        
			        for(int i = 0; i < searchResults.size(); i++){
			        	Vector<Object> row = searchResults.get(i);
			        	double iniBalance = (Double)row.get(3);
			        	double sales = (Double)row.get(4);
			        	double payments = (Double)row.get(5);
			        	double commissions = (Double)row.get(6);
			        	double finalBalance = (Double)row.get(7);
			        	StringBuilder sb = new StringBuilder();
			        	sb.append("<tr class=\"row").append(intEvenOdd).append("\">");
			        	sb.append("<td>").append(intCounter++).append("</td>");
			        	sb.append("<td>").append(row.get(0)).append("</td>");
			        	sb.append("<td>").append(row.get(1)).append("</td>");
			        	sb.append("<td>").append(row.get(2)).append("</td>");
			        	sb.append("<td nowrap align=\"right\">").append(NumberUtil.formatCurrency(Double.toString(iniBalance))).append("</td>");
			        	sb.append("<td nowrap align=\"right\">").append(NumberUtil.formatCurrency(Double.toString(sales))).append("</td>");
			        	sb.append("<td nowrap align=\"right\">").append(NumberUtil.formatCurrency(Double.toString(payments))).append("</td>");
			        	sb.append("<td nowrap align=\"right\">").append(NumberUtil.formatCurrency(Double.toString(commissions))).append("</td>");
			        	sb.append("<td nowrap align=\"right\">").append(NumberUtil.formatCurrency(Double.toString(finalBalance))).append("</td>");
			        	out.print(sb.toString());
			        	totalInitialBalance = totalInitialBalance.add(new BigDecimal(iniBalance));
			        	totalSales = totalSales.add(new BigDecimal(sales));
			        	totalPayments = totalPayments.add(new BigDecimal(payments));
			        	totalCommissions = totalCommissions.add(new BigDecimal(commissions));
			        	totalFinalBalance = totalFinalBalance.add(new BigDecimal(finalBalance));
			        	
			        	if (intEvenOdd == 1) {
		                    intEvenOdd = 2;
		                } else {
		                    intEvenOdd = 1;
		                }
			        }
			    %>
			    <tfoot>
		            <tr class=row<%= intEvenOdd%>>
		                <td colspan="4"><strong><%=Languages.getString("jsp.admin.reports.total_sales", SessionData.getLanguage())%></strong></td>
		                <td align="right"><strong><%=NumberUtil.formatCurrency(totalInitialBalance.toString())%></strong></td>
		                <td align="right"><strong><%=NumberUtil.formatCurrency(totalSales.toString())%></strong></td>
		                <td align="right"><strong><%=NumberUtil.formatCurrency(totalPayments.toString())%></strong></td>
		                <td align="right"><strong><%=NumberUtil.formatCurrency(totalCommissions.toString())%></strong></td>
		                <td align="right"><strong><%=NumberUtil.formatCurrency(totalFinalBalance.toString())%></strong></td>
		            </tr>
		        </tfoot>
			 </table>
			<%
                } else if (searchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null) {
                    out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                }
                if (searchResults != null && searchResults.size() > 0) {
            %>
			<%
                }
            %>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>