<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
	int section = 4;
	int section_page = 16;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
 Vector warningSearchResults = new  Vector();
	Vector<String> vecCurrent = new Vector<String>();
	Vector<String> vecPast = new Vector<String>();
	Hashtable searchErrors = null;
	String message = ""; 

	TransactionReport.setWarningmessage(new Vector<String>());
	if (request.getParameter("search") != null)
	{
  if (TransactionReport.validateDateRange(SessionData) && TransactionReport.validateMinMaxAmount(SessionData))
  {
		    //pull last months report
	    	TransactionReport.setStartDate(DateUtil.addSubtractMonths(request.getParameter("startDate"), -1));
	    	TransactionReport.setEndDate(DateUtil.addSubtractMonths(request.getParameter("endDate"), -1));
		    vecPast = TransactionReport.getPromoSummary(SessionData, application);
	
		    // update the date and then reset date on the page
    TransactionReport.setStartDate(request.getParameter("startDate"));
    TransactionReport.setEndDate(request.getParameter("endDate"));
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
	
		    // pull the current months report
		    vecCurrent = TransactionReport.getPromoSummary(SessionData, application);
  }
  else
  {
   searchErrors = TransactionReport.getErrors();
  }
	}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="500">
	<tr>
    <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.title7",SessionData.getLanguage()).toUpperCase()%></td>
    <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF" class="formArea2">
<%
	boolean bool_custom_config_mexico = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
	boolean bool_custom_config_default = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	
	boolean bool_deploy_type_intl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
	boolean bool_perm_enable_tax_calc = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);

	if (searchErrors != null)
	{
  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1 = searchErrors.keys();
		
		while(enum1.hasMoreElements())
		{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
		}

  out.println("</font></td></tr></table>");
  	  }

	if (vecCurrent == null || vecPast == null)
      {
 		out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
	}	
	else if (vecCurrent.size() > 0 && vecPast.size() > 0)
      {
  		// Row 3) Number of Transactions
  		int currentSelectedQty = Integer.parseInt(vecCurrent.get(1));
  		String sCurrentSelectedQty = "<a href='admin/reports/transactions/promo_transactions.jsp?search=y&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() + "&startDate=" + request.getParameter("startDate") + "&endDate=" + request.getParameter("endDate") + "' target=_blank>" + currentSelectedQty + "</a>";
  		int pastSelectedQty = Integer.parseInt(vecPast.get(1));		
  		String sPastSelectedQty = "<a href='admin/reports/transactions/promo_transactions.jsp?search=y&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() + "&startDate=" + DateUtil.addSubtractMonths(request.getParameter("startDate"), -1) + "&endDate=" + DateUtil.addSubtractMonths(request.getParameter("endDate"), -1) + "' target=_blank>" + pastSelectedQty + "</a>";
		double qtyPercent = 100.0;
		if (pastSelectedQty != 0.0) {
  			//TransactionReport.cat.debug("current qty: " + currentSelectedQty);
  			//TransactionReport.cat.debug("past qty: " + pastSelectedQty);
  			qtyPercent = ((double)(currentSelectedQty - pastSelectedQty) / pastSelectedQty) * 100;
  			//TransactionReport.cat.debug("% HERE: " + qtyPercent);
  }
		
		// Row 4) Recharge Value
  		double currentSelectedSales = Double.parseDouble(vecCurrent.get(2)); 
  		double pastSelectedSales = Double.parseDouble(vecPast.get(2));
  		double salesPercent = 100.0;
		if (pastSelectedSales != 0.0) {
	  		salesPercent = ((currentSelectedSales - pastSelectedSales) / pastSelectedSales) * 100;
  }
		
  		// Row 5) Bonus Amount
  		double currentSelectedBonus = 0.0;
  		double pastSelectedBonus = 0.0;
  		if(bool_deploy_type_intl && bool_custom_config_default) {
  	 		currentSelectedBonus = Double.parseDouble(vecCurrent.get(3));
    		pastSelectedBonus = Double.parseDouble(vecPast.get(3));
  }
		double bonusPercent = 100.0;
		if (pastSelectedBonus != 0.0) {	
	  		bonusPercent = ((currentSelectedBonus - pastSelectedBonus) / pastSelectedBonus) * 100;
		}
  		
		// Row 6) Total Recharge
  		double currentSelectedTotalRecharges = 0.0;
  		double pastSelectedTotalRecharges = 0.0;
  		if(bool_deploy_type_intl && bool_custom_config_default) {
  	 		currentSelectedTotalRecharges = Double.parseDouble(vecCurrent.get(4));
    		pastSelectedTotalRecharges = Double.parseDouble(vecPast.get(4));
  		}
		double totalPercent = 100.0;
		if (pastSelectedTotalRecharges != 0.0) {
	  		totalPercent = ((currentSelectedTotalRecharges - pastSelectedTotalRecharges) / pastSelectedTotalRecharges) * 100;
		}
  		
  		// Row 7) Net Amount
  		double currentSelectedNetAmount = Double.parseDouble(vecCurrent.get(5));
  		double pastSelectedNetAmount = Double.parseDouble(vecPast.get(5));
  		double netAmountPercent = 100.0;
		if (pastSelectedNetAmount != 0.0) {
			netAmountPercent = ((currentSelectedNetAmount - pastSelectedNetAmount) / pastSelectedNetAmount) * 100;
		}
  		
		// Row 8) Tax Amount
  		double currentSelectedTaxAmount = Double.parseDouble(vecCurrent.get(6));
  		double pastSelectedTaxAmount = Double.parseDouble(vecPast.get(6));
  		double taxAmountPercent = 100.0;
		if (pastSelectedTaxAmount != 0.0) {
			taxAmountPercent = ((currentSelectedTaxAmount - pastSelectedTaxAmount) / pastSelectedTaxAmount) * 100;
		}
  		
		// Row 9) % of Total for period
  		double currentPercentOfTotal = Double.parseDouble(vecCurrent.get(7));
  		double pastPercentOfTotal = Double.parseDouble(vecPast.get(7));
		double periodPercent = 100.0;
		if (pastPercentOfTotal != 0.0) {
  periodPercent = ((currentPercentOfTotal - pastPercentOfTotal)/pastPercentOfTotal) * 100;
		}
		
		Vector<String> vTimeZoneData = new Vector<String>();
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr class="main"><td nowrap colspan="4"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
<%
		String taxtype = "";
 		if ((bool_custom_config_mexico && (request.getParameter("chkUseTaxValue") != null))
 				|| (bool_perm_enable_tax_calc 
 				&& bool_deploy_type_intl 
 				&& bool_custom_config_default))
      {
      		if(bool_perm_enable_tax_calc &&  bool_deploy_type_intl && bool_custom_config_default) {   
      			// The default tax was used in the calculation
		    	if(vecCurrent.get(0).equals("1") || vecPast.get(0).equals(("1"))) {
		    		if( TransactionReport.getWarningMessage().size()>0)
			    		taxtype = TransactionReport.getWarningMessage().get(0);
	      			
		      		TransactionReport.setWarningmessage(new  Vector());
      		TransactionReport.setWarningmessage(TransactionReport.getWarningMessage(SessionData,SessionData.getProperty("end_date"),SessionData.getProperty("start_date")));
					warningSearchResults = TransactionReport.getWarningMessage(); 
      }
      }
      		
      		if (warningSearchResults.size() > 0) {
					message = message + Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage()) + "<br />";
					message = message + Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage()) + "<br />";

				for (int i = 0; i < warningSearchResults.size(); i++){  
						message = message + warningSearchResults.get(i).toString() + "<br />";
					 } 
%>
				<tr class="errorText">
            		<td colspan="4"><%= message %></td>
            	</tr>
<%
			}
%>
				<tr class="errorText">
            		<td colspan="4"><%= taxtype %></td>
            	</tr>
			  <tr>
              		<td colspan=4 class=rowhead2><%=Languages.getString("jsp.admin.reports.title7",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <tr>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.description",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.current_period",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.previous_period",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.transactions.promo_summary.percent_change",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
<%
	      	out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.promo_summary.date_range",SessionData.getLanguage()).toUpperCase()+"</td><td align=right>" + DateUtil.formatDateTime(request.getParameter("startDate")) + "-" + DateUtil.formatDateTime(request.getParameter("endDate")) + "</td><td align=right>" + DateUtil.formatDateTime(DateUtil.addSubtractMonths(request.getParameter("startDate"),-1)) + "-" +DateUtil.formatDateTime(DateUtil.addSubtractMonths(request.getParameter("endDate"),-1)) + "</td><td align=right>&nbsp;</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.promo_summary.denomination_range",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + NumberUtil.formatCurrency(request.getParameter("minAmount"))+"-"+NumberUtil.formatCurrency(request.getParameter("maxAmount"))+ "</td><td  align=right>" + NumberUtil.formatCurrency(request.getParameter("minAmount"))+"-"+NumberUtil.formatCurrency(request.getParameter("maxAmount")) + "</td><td  align=right>&nbsp;</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.promo_summary.num_of_trans",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + sCurrentSelectedQty + "</td><td  align=right>" + sPastSelectedQty + "</td><td  align=right>"+NumberUtil.formatAmount(Double.toString(qtyPercent))+"%</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(currentSelectedSales))+ "</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(pastSelectedSales)) + "</td><td  align=right>"+NumberUtil.formatAmount(Double.toString(salesPercent))+"%</td></tr>");
      
	      	if(bool_deploy_type_intl && bool_custom_config_default) {
			out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(currentSelectedBonus))+ "</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(pastSelectedBonus)) + "</td><td  align=right>"+NumberUtil.formatAmount(Double.toString(bonusPercent))+"%</td></tr>");
     		out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(currentSelectedTotalRecharges))+ "</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(pastSelectedTotalRecharges)) + "</td><td  align=right>"+NumberUtil.formatAmount(Double.toString(totalPercent))+"%</td></tr>");
     	}
	      	
        	out.println("<tr class=row1><td nowrap>" + Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase() + "</td><td align=right>" + NumberUtil.formatCurrency(Double.toString(currentSelectedNetAmount))+ "</td><td align=right>" + NumberUtil.formatCurrency(Double.toString(pastSelectedNetAmount)) + "</td><td align=right>" + NumberUtil.formatAmount(Double.toString(netAmountPercent)) + "%</td></tr>");
        	out.println("<tr class=row2><td nowrap>" + Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase() + "</td><td align=right>" + NumberUtil.formatCurrency(Double.toString(currentSelectedTaxAmount))+ "</td><td align=right>" + NumberUtil.formatCurrency(Double.toString(pastSelectedTaxAmount)) + "</td><td align=right>" + NumberUtil.formatAmount(Double.toString(taxAmountPercent)) + "%</td></tr>");
      	} else {
%>
				<tr>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.description",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.current_period",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.previous_period",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.transactions.promo_summary.percent_change",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
<%
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.promo_summary.date_range",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + DateUtil.formatDateTime(request.getParameter("startDate"))+"-"+DateUtil.formatDateTime(request.getParameter("endDate")) + "</td><td  align=right>" + DateUtil.formatDateTime(DateUtil.addSubtractMonths(request.getParameter("startDate"),-1)) + "-" +DateUtil.formatDateTime(DateUtil.addSubtractMonths(request.getParameter("endDate"),-1)) + "</td><td  align=right>&nbsp;</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.promo_summary.denomination_range",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + NumberUtil.formatCurrency(request.getParameter("minAmount"))+"-"+NumberUtil.formatCurrency(request.getParameter("maxAmount"))+ "</td><td  align=right>" + NumberUtil.formatCurrency(request.getParameter("minAmount"))+"-"+NumberUtil.formatCurrency(request.getParameter("maxAmount")) + "</td><td  align=right>&nbsp;</td></tr>");
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.promo_summary.num_of_trans",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + sCurrentSelectedQty + "</td><td  align=right>" + sPastSelectedQty + "</td><td  align=right>"+NumberUtil.formatAmount(Double.toString(qtyPercent))+"%</td></tr>");
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(currentSelectedSales))+ "</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(pastSelectedSales)) + "</td><td  align=right>"+NumberUtil.formatAmount(Double.toString(salesPercent))+"%</td></tr>");
      		
      		if(bool_deploy_type_intl && bool_custom_config_default)
		{
			out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(currentSelectedBonus))+ "</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(pastSelectedBonus)) + "</td><td  align=right>"+NumberUtil.formatAmount(Double.toString(bonusPercent))+"%</td></tr>");
     		out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()+"</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(currentSelectedTotalRecharges))+ "</td><td  align=right>" + NumberUtil.formatCurrency(Double.toString(pastSelectedTotalRecharges)) + "</td><td  align=right>"+NumberUtil.formatAmount(Double.toString(totalPercent))+"%</td></tr>");
     	}
      }
      	out.println("<tr class=row1><td nowrap>" + Languages.getString("jsp.admin.reports.transactions.promo_summary.percent_total",SessionData.getLanguage()).toUpperCase() + "</td><td align=right>" + NumberUtil.formatAmount(Double.toString(currentPercentOfTotal)) + "%</td><td align=right>" + NumberUtil.formatAmount(Double.toString(pastPercentOfTotal)) + "%</td><td align=right>"+NumberUtil.formatAmount(Double.toString(periodPercent))+"%</td></tr>");
      out.println("</table>");

      vecCurrent.clear();
      vecPast.clear();
	}
	else if (vecCurrent.size()==0 && request.getParameter("search") != null && searchErrors == null)
	{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
	}	
%> 
          </td>
      </tr>  
</table>
<%@ include file="/includes/footer.jsp" %>
