<%@ page import="java.util.*"%>
<%
	int section = 3;
	int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="TransactionReport"
	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp"%>

<%
	Vector vecSearchResults = new Vector();	
	String start_date = request.getParameter("startdate");
	String end_date = request.getParameter("enddate");
	String status = request.getParameter("status");
	boolean blnIso = new Boolean(request.getParameter("blnIso"));
    String merchant = request.getParameter("merchant");
    String pincase = request.getParameter("pincase");
    String strPath = ""; 
     
    vecSearchResults = TransactionReport.getVoidTopupRequest(start_date, end_date,status, merchant, pincase, blnIso);
	strPath = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,13,SessionData);
	if (strPath.length() > 0)
	{
		response.sendRedirect(strPath);
	}
%>
