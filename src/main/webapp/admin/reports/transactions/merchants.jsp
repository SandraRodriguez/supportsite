<%@ page import="java.net.URLEncoder,com.debisys.customers.Merchant,com.debisys.tools.s2k,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script type="text/javascript" src="includes/s2k/s2ktaskscheduler.js" ></script>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title4",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="post" action="admin/reports/transactions/merchants_summary.jsp" onSubmit="scroll();">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
<%
	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
%>
	        <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
              <tr class="main">
               <td nowrap valign="top">
               	 <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
		         <%}%><td>&nbsp;</td>
               <td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
<table width=400>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td><td><input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td> <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>

<%if(!strAccessLevel.equals(DebisysConstants.MERCHANT)) {	
	%>

<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%></td>
    <td class=main valign=top>
        <select name="mids" id="mids" size="10" multiple onchange="getRatePlansForMerchants();">
          <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
  Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
  Iterator it = vecMerchantList.iterator();
  while (it.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
  }

%>
        </select>
        <br>
        *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%>
</td>
</tr>
<%
}%>

<%if(strAccessLevel.equals(DebisysConstants.ISO)&& SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
{
%>
<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.payments.index.select_saleReps",SessionData.getLanguage())%></td>
  <td class=main valign=top>
        <select name="salesIds" size="10" multiple>
          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
  Vector vecSalesIdList = Merchant.getMerchants2ksalesman(SessionData.getProperty("ref_id"));
  Iterator it1 = vecSalesIdList.iterator();
  while (it1.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it1.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
  }

%>
        </select>
         <br>
        *<%=Languages.getString("jsp.admin.reports.salesRep.instructions",SessionData.getLanguage())%>
</td>
</tr>


<%if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS)) {%>
<tr>
    <td class="main" valign="top" nowrap><%=Languages.getString("jsp.admin.tools.s2k.rateplan", SessionData.getLanguage())%></td>
    <td class="main" valign="top">
        <select name="s2kRatePlans" id="s2kRatePlans" size="10" multiple>
            <option value="" selected><%=Languages.getString("jsp.admin.reports.all", SessionData.getLanguage())%></option>
        </select>
        <br>
    </td>
</tr>


<%}
}%>

<%
 if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && SessionData.checkPermission(DebisysConstants.GENERATE_REPORT_WITH_TAXES_MX) )
 {//If when deploying in Mexico
%>
<TR CLASS="main">
    <TD></TD><TD NOWRAP>
    <!-- <INPUT TYPE="checkbox" ID="chkUseTaxValue" NAME="chkUseTaxValue"><LABEL FOR="chkUseTaxValue"><%=Languages.getString("jsp.admin.reports.transactions.products.mx_valueaddedtax",SessionData.getLanguage())%></LABEL>-->
    <input type="hidden" name="chkUseTaxValue" id="chkUseTaxValue" value="on">
    </TD>
</TR>
<TR CLASS="main"><TD></TD></TR>
<%
 }//End of if when deploying in Mexico
%>

<tr>
    <td class=main align=center>
      <input type="hidden" name="search" value="y">
      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="validateSchedule(0);">
    </td>
    <jsp:include page="/admin/reports/schreportoption.jsp">
	  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
	 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
	</jsp:include>
</tr>
<tr>
<td class="main" colspan=2>
* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
</td>
</tr>
</table>
               </td>
              </tr>
              </form>
            </table>

          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<script LANGUAGE="JavaScript">
    $(document).ready(function() {
        getRatePlansForMerchants();
    });
</script>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>