<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder" %>
<%
String strReport = request.getParameter("report");
int section=4;
int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;

  if (TransactionReport.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    vecSearchResults = TransactionReport.getMerchantErrorDetail(1, Integer.MAX_VALUE, SessionData, true);
  }
  else
  {
   searchErrors = TransactionReport.getErrors();
  }
%>
<%@ include file="/includes/header.jsp" %>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet"/>
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
  <tr>
	  <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	  <td class="formAreaTitle" align="left" width="3000">&nbsp;<%=Languages.getString("jsp.admin.reports.transaction.error_summary",SessionData.getLanguage()).toUpperCase()%></td>
	  <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#fFFfFF" class="formArea2">
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=vecSearchResults.size()%> result(s) found<%
              if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                   out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
            %>
            <br><%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%></td></tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
            <thead>
      	      <tr class="SectionTopBorder">
              <td class="rowhead2">#</td>
              <td class="rowhead2">DATE</td>
              <td class="rowhead2" nowrap="nowrap">TERM NO.</td>
              <td class="rowhead2">TYPE</td>
              <td class="rowhead2" nowrap="nowrap">TYPE DESC</td>
              <td class="rowhead2">HOST</td>
              <td class="rowhead2">PORT</td>
              <td class="rowhead2">DURATION</td>
              <td class="rowhead2" nowrap="nowrap">OTHER INFO</td>
              <td class="rowhead2" nowrap="nowrap">RESULT CODE</td>
              <td class="rowhead2" nowrap="nowrap">RESULT DESC</td>
            </tr>
            </thead>
            <%
                  int intCounter = 1;
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td>" + vecTemp.get(2) + "</td>" +
                                "<td>" + vecTemp.get(3) + "</td>" +
                                "<td>" + vecTemp.get(4) + "</td>" +
                                "<td>" + vecTemp.get(5) + "</td>" +
                                "<td>" + vecTemp.get(6) + "</td>" +
                                "<td>" + vecTemp.get(7) + "</td>" +
                                "<td>" + vecTemp.get(8) + "</td>" +
                                "<td>" + vecTemp.get(9) + "</td>" +
                        "</tr>");
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>

<%
}
else
{
 out.println("<br><br><font color=ff0000>No errors found.</font>");
}
%>
</td>
</tr>
</table>
<SCRIPT type="text/javascript">
  <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "Date", "Number", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "Number", "CaseInsensitiveString", "Number", "CaseInsensitiveString"],0,false,false);
  -->
</SCRIPT>

<%@ include file="/includes/footer.jsp" %>