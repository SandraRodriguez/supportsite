<%@page import="com.debisys.reports.ReportsUtil"%>
<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="com.debisys.utils.ColumnReport"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
    Vector vecSearchResults = new Vector();
    Hashtable searchErrors = null;
    Vector warningSearchResults = new Vector();
    boolean bUseTaxValue = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns

    boolean isMexico = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
    
    //this flag indicates if the user has:
    //The Tax Persmission in SS reports.
    //The deployment type be International
    //The custom config type be default
    boolean permissionInterCustomConfig = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                                                DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                                                        DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
    boolean isIntlAndHasDataPromoPermission = SessionData.checkPermission(DebisysConstants.PERM_ALLOW_PROMO_DATA_COLUMN_IN_TRANSACTIONS_REPORT) && 
                                              DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                                              DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
 
 
 	//this flag means if the user is in International Default
    boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                                                DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
    
    
    ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
    ArrayList<String> titles = new ArrayList<String>();
    
    String testTrx = "";  
    String keyLanguage = "jsp.admin.reports.title5";
    String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
    
       
    Vector vTimeZoneData = null;
    if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
    {
        vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
    }
    else
    {
        vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
    }
    String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+" "+vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
    
    
    if ( request.getParameter("Download") != null )
    {
        String sURL = "";
        TransactionReport.setProductIds(request.getParameter("_productIds"));
        TransactionReport.setMerchantIds(request.getParameter("_merchantIds"));
        String strUseTaxValue = request.getParameter("chkUseTaxValue");

        if ( isMexico && strUseTaxValue != null && strUseTaxValue.equals("on") )
        {
            bUseTaxValue = true;
            sURL = TransactionReport.downloadProductSummary(application, SessionData, true,permissionInterCustomConfig,isInternationalDefault);
        }
        else
        {
            sURL = TransactionReport.downloadProductSummary(application, SessionData, false,permissionInterCustomConfig,isInternationalDefault);
        }
        response.sendRedirect(sURL);
        return;
    }
	
    if (request.getParameter("search") != null)
    {
        boolean isScheduleReportRequest = request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y");
        if (TransactionReport.validateDateRange(SessionData))
        {
            SessionData.setProperty("start_date", request.getParameter("startDate"));
            SessionData.setProperty("end_date", request.getParameter("endDate"));
            String strProductIds[] = request.getParameterValues("pids");
            if (strProductIds != null)
            {
              TransactionReport.setProductIds(strProductIds);
            }
            String strMerchantIds[] = request.getParameterValues("merchantIds");
            if (strMerchantIds != null)
            {
              TransactionReport.setMerchantIds(strMerchantIds);
            }
            if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
            {
              SessionData.setProperty("deploymentType", DebisysConfigListener.getDeploymentType(application));
            }
            if ( isMexico )
            {
              SessionData.setProperty("customConfigType", DebisysConfigListener.getCustomConfigType(application));
            }
            
            if ( isMexico && (request.getParameter("chkUseTaxValue") != null ) )
            {
                bUseTaxValue = true;
                vecSearchResults = TransactionReport.getProductSummaryMx(SessionData, application);
            }
            else if ( !isScheduleReportRequest )
            {
                vecSearchResults = TransactionReport.getProductSummary(SessionData, application,permissionInterCustomConfig, DebisysConstants.EXECUTE_REPORT, null, null);
                warningSearchResults = TransactionReport.getWarningMessage();
            }
             
            //////////////////////////////////////////////////////////////////
            //HERE WE DEFINE THE REPORT'S HEADERS 
            headers = com.debisys.reports.ReportsUtil.getHeadersSummaryTrxByProduct( SessionData, application, bUseTaxValue);
            //////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////
                
            if ( isScheduleReportRequest )
            {
                titles.add(noteTimeZone);
                titles.add(testTrx);
                titles.add( Languages.getString(keyLanguage,SessionData.getLanguage()) );
                TransactionReport.getProductSummary(SessionData,application, permissionInterCustomConfig, DebisysConstants.SCHEDULE_REPORT, headers, titles);
                ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
                if (  scheduleReport != null  )
                {
                    scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
                    scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
                    scheduleReport.setTitleName( keyLanguage );   
                }	
                response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );            
            }
    
        }
        else
        {
            searchErrors = TransactionReport.getErrors();
        }
    }
%>
<%@ include file="/includes/header.jsp" %>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=titleReport.toUpperCase()%></td>
    <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF" class="formArea2">

<%
    if (searchErrors != null)
    {
        out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
        Enumeration enum1=searchErrors.keys();
        while(enum1.hasMoreElements())
        {
            String strKey = enum1.nextElement().toString();
            String strError = (String) searchErrors.get(strKey);
            out.println("<li>" + strError);
        }
        out.println("</font></td></tr></table>");
    }
    if (vecSearchResults != null && vecSearchResults.size() > 0)
    {
%>
        <table width="100%" border="0" cellspacing="0" cellpadding="2">            
            <%
            boolean showTaxWarning = false;    
            if ( warningSearchResults.size()>0 && showTaxWarning){ 
            %>
            <tr>
            <td class="main" style="color:red">
                            <% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1)
                                            ||  (!TransactionReport.checkfortaxtype(SessionData))){
            %>
                    <%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
                    <%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
            <% } %>
                    <% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
                            <%= warningSearchResults.get(i).toString()%> <br/>
                    <% } %>
            </td>
            </tr>
            <%} %>

            <tr class="main"><td nowrap="nowrap" colspan="2"><%=noteTimeZone%><br/><br/></td></tr>
            <tr><td class="main">
                  <%
                    out.println(Languages.getString("jsp.admin.reports.transaction.product_summary",SessionData.getLanguage()));
                  if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                    {
                       out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                    }
                %><br><font color="#ff0000"><%=Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage())%></font></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                      <tr><td>&nbsp;</td></tr>
                      <tr>
                        <td>
                        <FORM ACTION="admin/reports/transactions/products_summary.jsp" METHOD="post" ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                                <INPUT TYPE="hidden" NAME="startDate" VALUE="<%=request.getParameter("startDate")%>">
                                <INPUT TYPE="hidden" NAME="endDate" VALUE="<%=request.getParameter("endDate")%>">
                                <INPUT TYPE="hidden" NAME="chkUseTaxValue" VALUE="<%=request.getParameter("chkUseTaxValue")%>">
                                <INPUT TYPE="hidden" NAME="_productIds" VALUE="<%=TransactionReport.getProductIds()%>">
                                <INPUT TYPE="hidden" NAME="_merchantIds" VALUE="<%=TransactionReport.getMerchantIds()%>">
                                <INPUT TYPE="hidden" NAME="Download" VALUE="Y">
                                <INPUT ID="btnSubmit" TYPE="submit" VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                        </FORM>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                        </td>
                      </tr>
                    </table>
                </td>
            </tr>
        </table>
            <table>
                <tr>
                    <td align="left" width="3000"><%=SessionData.getString("jsp.admin.index.company_name")%>: <%=SessionData.getProperty("company_name")%></td>
                </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2"  class="sort-table">
            <tr>
            <%
            String strSortURL = "admin/reports/transactions/products_summary.jsp?search=y&startDate=" + TransactionReport.getStartDate() + "&endDate=" + TransactionReport.getEndDate() + "&pids=" + TransactionReport.getProductIds() + ((bUseTaxValue)?"&chkUseTaxValue=" + request.getParameter("chkUseTaxValue"):"") + ((request.getParameterValues("merchantIds") != null)?"&merchantIds=" + TransactionReport.getMerchantIds():"");
            
            boolean showCol = false;
            if ( showCol )
            {
            %>
              <td class="rowhead2" valign="bottom">#</td>
              <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.transactions.products_summary.product_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=1&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=1&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
              <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=2&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=2&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
              <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=3&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=3&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
              <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=4&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=4&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
			  
<%
            if ( isInternationalDefault )
            {
%>
            <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=17&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=17&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
            <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=18&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=18&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
            }
                if ( bUseTaxValue )
                {
%>
            <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&col=11&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=11&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
                }
%>
<%
		if ( permissionInterCustomConfig) 
		{
 %>
            <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=11&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=11&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
            <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=11&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=11&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
         }
		if (strAccessLevel.equals(DebisysConstants.ISO))
  		{
%>
            <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=9&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=9&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
                if ( bUseTaxValue )
                {
            %>
            <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&col=16&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=16&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
            <%
                }

            if ( permissionInterCustomConfig) 
            {
%>
            <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_iso_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=16&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=16&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
            }
	    }
		if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  		{
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=8&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=8&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
    if ( bUseTaxValue )
    {
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&col=15&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=15&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
    }
	        if(permissionInterCustomConfig)
    {
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_agent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=15&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=15&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
    }
		}

        if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  	    {
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=7&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=7&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
    if ( bUseTaxValue )
    {
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&col=14&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=14&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
    }
	
	     	if ( permissionInterCustomConfig ) 
    {
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_subagent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=15&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=15&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
    }
   		}
if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
  {
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=6&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=6&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
    if ( bUseTaxValue )
    {
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&col=13&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=13&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
    }

			    if ( permissionInterCustomConfig )
    {
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_rep_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=15&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=15&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
    }
%>
<%
  }

%>
<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=5&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=5&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
  if ( bUseTaxValue )
  {
%>
<TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&col=12&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=12&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
  }
		
     	if ( permissionInterCustomConfig )
    {
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_merchant_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=15&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=15&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></TD>
<%
    }
if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
{
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.channel_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=10&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=10&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
}

if (strAccessLevel.equals(DebisysConstants.ISO))
{%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.adjustment",SessionData.getLanguage()).toUpperCase()%>&nbsp;
        <a href="<%=strSortURL%>&col=10&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
        <a href="<%=strSortURL%>&col=10&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
  </td>
<%}
}
    else{%>
  
            <td class="rowhead2" valign="bottom">#</td>
            
            <%  int i=1;
                for ( ColumnReport columnReport : headers ){%>

                <td class=rowhead2>
                  <%=columnReport.getLanguageDescription()%>
                  &nbsp;
                    <a href="<%=strSortURL%>&col=<%=i%>&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a>
                    <a href="<%=strSortURL%>&col=<%=i%>&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a>
                </td>

            <%  i++;
                }%>
                    
    <%}
%>
  </tr>
<%
  


                  double dblTotalSalesSum = 0;
                  double dblTotalBonus = 0;
                  double dblTotalRechargeSum = 0;
                  double dblMerchantCommissionSum = 0;
                  double dblRepCommissionSum = 0;
                  double dblSubAgentCommissionSum = 0;
                  double dblAgentCommissionSum = 0;
                  double dblISOCommissionSum = 0;
                  double dblVATTotalSalesSum = 0;
                  double dblVATMerchantCommissionSum = 0;
                  double dblVATRepCommissionSum = 0;
                  double dblVATSubAgentCommissionSum = 0;
                  double dblVATAgentCommissionSum = 0;
                  double dblVATISOCommissionSum = 0;
                  double dblAdjAmountSum = 0;
                  double dblChannelPercentageSum = 0; // 2008-11-12.FB. New column added
                  double dblTotalTaxAmountSum=0;
                  int intTotalQtySum = 0;
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  int intCounter = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    int intTotalQty = Integer.parseInt(vecTemp.get(2).toString());
                    double dblTotalSales = Double.parseDouble(vecTemp.get(3).toString());
                    double dblMerchantCommission = Double.parseDouble(vecTemp.get(4).toString());
                    double dblRepCommission = Double.parseDouble(vecTemp.get(5).toString());
                    double dblSubAgentCommission = Double.parseDouble(vecTemp.get(6).toString());
                    double dblAgentCommission = Double.parseDouble(vecTemp.get(7).toString());
                    double dblISOCommission = Double.parseDouble(vecTemp.get(8).toString());
                    double dblAdjAmount = Double.parseDouble(vecTemp.get(9).toString());
                    double dblVATTotalSales = 0;
                    double dblVATMerchantCommission = 0;
                    double dblVATRepCommission = 0;
                    double dblVATSubAgentCommission = 0;
                    double dblVATAgentCommission = 0;
                    double dblVATISOCommission = 0;
                    double dblBonus = 0;
                    double dblTotalRecharge = 0;
                    double dblTaxAmount=0;
                    
                    // 2008-11-11 FB. New column added for Jim
                    double dblChannelPercentage = 0;
                    
                    if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS)) //iso only and with permission
	              	{
	              		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	              		{
	              			dblChannelPercentage = (dblISOCommission + dblRepCommission + dblMerchantCommission) /  dblTotalSales * 100;
	              		}
	              		else if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	              		{
		                	dblChannelPercentage = (dblISOCommission + dblAgentCommission + dblSubAgentCommission + dblRepCommission + dblMerchantCommission) / dblTotalSales * 100;
		                }
	              	}

                    if ( bUseTaxValue || permissionInterCustomConfig ) 
                    {
                      dblVATTotalSales = Double.parseDouble(vecTemp.get(10).toString());
                      dblVATMerchantCommission = Double.parseDouble(vecTemp.get(11).toString());
                      dblVATRepCommission = Double.parseDouble(vecTemp.get(12).toString());
                      dblVATSubAgentCommission = Double.parseDouble(vecTemp.get(13).toString());
                      dblVATAgentCommission = Double.parseDouble(vecTemp.get(14).toString());
                      dblVATISOCommission = Double.parseDouble(vecTemp.get(15).toString());
                      dblBonus = Double.parseDouble(vecTemp.get(16).toString());
                      dblTotalRecharge = Double.parseDouble(vecTemp.get(17).toString());
                    } 
                    else 
                    {
                    	dblBonus = Double.parseDouble(vecTemp.get(10).toString());
                    	dblTotalRecharge = Double.parseDouble(vecTemp.get(11).toString()); 
                    }
                    
                    if ( permissionInterCustomConfig )
                    {
                      dblTaxAmount=dblTotalSales-dblVATTotalSales;
                    }

                    intTotalQtySum = intTotalQtySum + intTotalQty;
                    dblTotalSalesSum = dblTotalSalesSum + dblTotalSales;
                    dblTotalBonus += dblBonus;
                    dblTotalRechargeSum += dblTotalRecharge;
                    dblMerchantCommissionSum=dblMerchantCommissionSum + dblMerchantCommission;
                    dblRepCommissionSum = dblRepCommissionSum + dblRepCommission;
                    dblSubAgentCommissionSum = dblSubAgentCommissionSum + dblSubAgentCommission;
                    dblAgentCommissionSum = dblAgentCommissionSum + dblAgentCommission;
                    dblISOCommissionSum = dblISOCommissionSum + dblISOCommission;
                    dblAdjAmountSum = dblAdjAmountSum + dblAdjAmount;

                    String sMerchantIds = "";
                    if ( request.getParameterValues("merchantIds") != null )
                    {
                      sMerchantIds = "&merchantIds=" + TransactionReport.getMerchantIds();
                    }

                    if ( bUseTaxValue || permissionInterCustomConfig )
                    {
		                    if ( permissionInterCustomConfig )
		                    {
                     	dblTotalTaxAmountSum+=dblTaxAmount;
                     }
                      dblVATTotalSalesSum += dblVATTotalSales;
                      dblVATMerchantCommissionSum += dblVATMerchantCommission;
                      dblVATRepCommissionSum += dblVATRepCommission;
                      dblVATSubAgentCommissionSum += dblVATSubAgentCommission;
                      dblVATAgentCommissionSum += dblVATAgentCommission;
                      dblVATISOCommissionSum += dblVATISOCommission;
                      out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td align=\"right\">" + intTotalQty + "</td>" +
                                "<td align=\"right\">" +
                                "<a href=\"admin/reports/transactions/products_transactions.jsp?" +
                                "startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +
                                "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +
                                "&productId=" + vecTemp.get(1) +
                                "&search=y&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") +
                                sMerchantIds + "\" target=\"_blank\">" +
                                NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
                    	 
		                      if( isInternationalDefault )
		                      {
		                      	out.println("<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>" + "<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblTotalRecharge)) + "</td>");
                      }
		                      
                                out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATTotalSales)) + "</td>");
		                      
		                      if ( permissionInterCustomConfig )
                                {
                                 out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAmount)) + "</td>");
                                 }
                    }
                    else 
                    {
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td align=\"right\">" + intTotalQty + "</td>" +
                                "<td align=right>" +
                                "<a href=\"admin/reports/transactions/products_transactions.jsp?" +
                                "startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +
                                "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +
                                "&productId=" + vecTemp.get(1) +
                                "&search=y" + sMerchantIds + "\" target=\"_blank\">" +
                                NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
		                                
		                    if ( isInternationalDefault )
		                    {
		                        out.println("<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>" + "<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblTotalRecharge)) + "</td>");
                    }
                    }

if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommission))  + "</td>");
					  if ( bUseTaxValue || permissionInterCustomConfig )
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATISOCommission)) + "</td>");
  }
  }

				    if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
{
	  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommission))  + "</td>");
					  if ( bUseTaxValue || permissionInterCustomConfig )
	  {
	    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATAgentCommission)) + "</td>");
	  }
}

					if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommission))  + "</td>");
					   if ( bUseTaxValue || permissionInterCustomConfig )
    {
      out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATSubAgentCommission)) + "</td>");
    }
  }

if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommission))  + "</td>");
					    if ( bUseTaxValue || permissionInterCustomConfig )
    {
      out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATRepCommission)) + "</td>");
    }
  }

out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommission))  + "</td>");
					
					if ( bUseTaxValue || permissionInterCustomConfig )
{
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATMerchantCommission)) + "</td>");
}

// 2008-11-11 FB. New column added for Jim
if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
{
	out.println("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblChannelPercentage))  + "</td>");
}

if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmount))  + "</td>");
  }

    if (isIntlAndHasDataPromoPermission) {
        if (permissionInterCustomConfig) {
            out.println("<td align=right>" + vecTemp.get(18)  + "</td>");
        } else {
            out.println("<td align=right>" + vecTemp.get(12)  + "</td>");
        }
    }

out.println("</tr>");

                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
                  
                  
                  
            //********************************************************************************//      
            //***************************SUMMARY REGION**************************************//      
            %>
            <tr class="row<%=intEvenOdd%>">
            <td colspan="3" align="right"><%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
            <td align="right"><%=intTotalQtySum%></td>
            <td align="right"><%=NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum))%></td>
<%
			if ( isInternationalDefault )
			{
	%>
			<td align="right"><%=NumberUtil.formatCurrency(Double.toString(dblTotalBonus))%></td>
			<td align="right"><%=NumberUtil.formatCurrency(Double.toString(dblTotalRechargeSum))%></td>
<%
}
            if ( bUseTaxValue || permissionInterCustomConfig )
                {
%>
            <td align="right">
                  <%= NumberUtil.formatCurrency(Double.toString(dblVATTotalSalesSum)) %>
            </td>
<%
                }
            if ( permissionInterCustomConfig )
                {
%>
<td align="right">
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalTaxAmountSum)) %>
            </td>
<%
                }
if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommissionSum))  + "</td>");
				if ( bUseTaxValue || permissionInterCustomConfig )
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATISOCommissionSum)) + "</td>");
  }
  }

			if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommissionSum))  + "</td>");
			    if ( bUseTaxValue || permissionInterCustomConfig )
    {
      out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATAgentCommissionSum)) + "</td>");
    }
  }
  			
			if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommissionSum))  + "</td>");
			    if ( bUseTaxValue || permissionInterCustomConfig )
      {
        out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATSubAgentCommissionSum)) + "</td>");
      }
  }

if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommissionSum))  + "</td>");
			    if ( bUseTaxValue || permissionInterCustomConfig )
      {
        out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATRepCommissionSum)) + "</td>");
      }
  }
			
out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommissionSum))+ "</td>");
			
			if ( bUseTaxValue || permissionInterCustomConfig )
{
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATMerchantCommissionSum)) + "</td>");
}

if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
{	
	if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	{
		dblChannelPercentageSum = (dblISOCommissionSum + dblRepCommissionSum + dblMerchantCommissionSum) /  dblTotalSalesSum * 100;
	}
	else if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	{
	 	dblChannelPercentageSum = (dblISOCommissionSum + dblAgentCommissionSum + dblSubAgentCommissionSum + dblRepCommissionSum + dblMerchantCommissionSum) / dblTotalSalesSum * 100;
	}

   out.println("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblChannelPercentageSum))  + "</td>");
}

if (strAccessLevel.equals(DebisysConstants.ISO))
  {
   out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmountSum))  + "</td>");
  }
  
out.println("</tr></table>");
			
			//********************************************************************************//      
            //***************************END SUMMARY REGION**************************************//   
}
else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>
