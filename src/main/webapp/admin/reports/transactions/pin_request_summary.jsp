<%@ page import="java.net.URLEncoder,com.debisys.utils.HTMLEncoder,java.util.*,
         com.debisys.customers.Merchant,
         com.debisys.utils.NumberUtil,
         com.debisys.reports.schedule.PinReturnRequestReport,
         com.debisys.schedulereports.ScheduleReport,
         com.debisys.utils.ColumnReport,
         com.debisys.utils.StringUtil" %>
<%
    int section = 4;
    int section_page = 25;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<jsp:useBean id="PinReturnRequestReport" 
             class="com.debisys.reports.schedule.PinReturnRequestReport" 
             scope="request"/>
<%@ include file="/includes/security.jsp" %>


<%    SessionData.setProperty("reportId", DebisysConstants.SC_PIN_RETURN_REQUESTS);
    Vector<Vector<Object>> vecSearchResults = null;
    Hashtable searchErrors = null;
    String start_date = "";
    String end_date = "";
    String status = "";
    String pathUrl = "";
    String merchant = "";
    String pincase = "";
    boolean allStatusSelected = false;
    boolean allMerchantsSelected = false;

    String reportTitleKey = "jsp.admin.reports.pin_request_report";
    String reportTitle = Languages.getString(reportTitleKey, SessionData.getLanguage()).toUpperCase();
    PinReturnRequestReport report = null;

    if (request.getParameter("search") != null) {
        String trReportStartDate = TransactionReport.getStartDate();
        if ((trReportStartDate == null) || ((trReportStartDate != null) && (trReportStartDate.trim().equals("")))) {
            trReportStartDate = request.getParameter("startDate");
            TransactionReport.setStartDate(trReportStartDate);
        }
        String trReportEndDate = TransactionReport.getEndDate();
        if ((trReportEndDate == null) || ((trReportEndDate != null) && (trReportEndDate.trim().equals("")))) {
            trReportEndDate = request.getParameter("endDate");
            TransactionReport.setEndDate(trReportEndDate);
        }
        SessionData.setProperty("start_date", trReportStartDate);
        SessionData.setProperty("end_date", trReportEndDate);

        if (TransactionReport.validateDateRange(SessionData)) {
            allStatusSelected = Boolean.valueOf(request.getParameter("allStatusSelected"));
            allMerchantsSelected = Boolean.valueOf(request.getParameter("allMerchantsSelected"));
            start_date = trReportStartDate;
            end_date = trReportEndDate;

            status = request.getParameter("statusList");
            if (status != null) {
                if (status.equals("")) {
                    allStatusSelected = true;
                }
            }

            String merchantTemp = "";

            merchantTemp = request.getParameter("merchantList");
            if (merchantTemp != null) {
                if (merchantTemp.equals("")) {
                    allMerchantsSelected = true;
                } else {
                    merchant = merchantTemp;
                }
            } else {
                Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
                merchant = StringUtil.vector2CsvString(vecMerchantList);
                allMerchantsSelected = true;
            }
            pincase = request.getParameter("PINCase");

            report = new PinReturnRequestReport(SessionData, application, false, pincase, TransactionReport.getStartDate(), TransactionReport.getEndDate(), status, merchant, allMerchantsSelected, allStatusSelected);

            if (request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y")) {
                //TO SCHEDULE REPORT
                report.setScheduling(true);
                if (report.scheduleReport()) {
                    ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
                    if (scheduleReport != null) {
                        scheduleReport.setStartDateFixedQuery(report.getStart_date());
                        scheduleReport.setEndDateFixedQuery(report.getEnd_date());
                        scheduleReport.setTitleName(reportTitleKey);
                    }
                    response.sendRedirect(DebisysConstants.PAGE_TO_SCHEDULE_REPORTS);
                }
            } else if (request.getParameter("downloadReport") != null) {
                //TO DOWNLOAD ZIP REPORT
                report.setScheduling(false);
                String zippedFilePath = report.downloadReport();
                if ((zippedFilePath != null) && (!zippedFilePath.trim().equals(""))) {
                    response.sendRedirect(zippedFilePath);
                }
            } else {
                //TO SHOW REPORT
                report.setScheduling(false);
                vecSearchResults = report.getResults();
            }
        } else {
            searchErrors = TransactionReport.getErrors();
        }
    }
%>

<%@ include file="/includes/header.jsp" %>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.pin_request_report", SessionData.getLanguage()).toUpperCase()%></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" class="formArea2">
            <%
                if (searchErrors != null) {
                    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");
                    Enumeration enum1 = searchErrors.keys();
                    while (enum1.hasMoreElements()) {
                        String strKey = enum1.nextElement().toString();
                        String strError = (String) searchErrors.get(strKey);
                        out.println("<li>" + strError);
                    }
                    out.println("</font></td></tr></table>");
                }
                if (vecSearchResults != null && vecSearchResults.size() > 0) {
            %>           
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td class="main">
                        <%
                            out.println(reportTitle);
                            if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                                out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                            }
                        %>                    
                        <br><font color="#ff0000"><%=Languages.getString("jsp.admin.reports.test_trans", SessionData.getLanguage())%></font>
                    </td>
                    <td class=main align=right valign=bottom>
                        <%=Languages.getString("jsp.admin.reports.click_to_sort", SessionData.getLanguage())%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <FORM ACTION="admin/reports/transactions/pin_request_summary.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                            <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                            <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                            <INPUT TYPE=hidden NAME=merchantList VALUE="<%=merchant%>">
                            <INPUT TYPE=hidden NAME=PINCase VALUE="<%=pincase%>">
                            <INPUT TYPE=hidden NAME=statusList VALUE="<%=status%>">
                            <INPUT TYPE=hidden NAME=allStatusSelected VALUE="<%=allStatusSelected%>">
                            <INPUT TYPE=hidden NAME=allMerchantsSelected VALUE="<%=allMerchantsSelected%>">
                            <input type="hidden" name="downloadReport" value="y">
                            <input type="hidden" name="search" value="y">
                            <INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.downloadToCsv", SessionData.getLanguage())%>">
                        </FORM>
                    </td>

                </tr>

            </table>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1" >
                <thead>
                    <tr class="SectionTopBorder">
                        <td class=rowhead2 nowrap>#</td>      
                        <%
                            // Set the column headers
                            ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
                            if ((reportColumns != null) && (reportColumns.size() > 0)) {
                                for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                        %>                    
                        <td class=rowhead2 nowrap align=center><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
                        </td>
                        <%
                                }
                            }
                        %>

                    </tr>
                </thead>                        
                <%
                    int intEvenOdd = 1;
                    int intCounter = 1;
                    Iterator it = vecSearchResults.iterator();
                    while (it.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        out.println("<tr class=row" + intEvenOdd + ">"
                                + "<td>" + intCounter++ + "</td>"
                                + "<td nowrap>"
                                + "<a href=\"admin/reports/transactions/pin_request_case_summary.jsp?"
                                + "pincase=" + vecTemp.get(0)
                                + "&reason=" + vecTemp.get(9)
                                + "&status=" + vecTemp.get(8) + "\">" + vecTemp.get(0) + "</a>"
                                + "</td>"
                                + "<td nowrap>" + vecTemp.get(1) + "</td>"
                                + "<td>" + vecTemp.get(2) + "</td>"
                                + "<td>" + vecTemp.get(3) + "</td>"
                                + "<td>" + vecTemp.get(4) + "</td>"
                                + "<td nowrap>"
                                + "<a href=\"admin/reports/transactions/pin_request_controlno_summary.jsp?"
                                + "controlNo=" + vecTemp.get(5)
                                + "&siteId=" + vecTemp.get(3)
                                + "\">" + vecTemp.get(5) + "</a>"
                                + "</td>"
                                + "<td>" + vecTemp.get(6) + "</td>"
                                + "<td align=\"right\">" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>"
                                + "<td>" + vecTemp.get(8) + "</td>");
                        out.println("</tr>");
                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        } else {
                            intEvenOdd = 1;
                        }
                    }
                    vecSearchResults.clear();
                %>

            </table>

            <SCRIPT type="text/javascript">

                <!--
    var stT1 = new SortROC(document.getElementById("t1"),
                        ["None", "Number", "Date", "String", "Number", "Number", "Number", "String", "Number", "String"], 0, false, false);
  -->
            
            </SCRIPT>            
            <%
                } else if ((vecSearchResults == null) || ((vecSearchResults != null) && (vecSearchResults.size() == 0) && (request.getParameter("search") != null) && (searchErrors == null))) {
                    out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                }
            %>
        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>