<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="com.debisys.reports.schedule.MerchantCreditAccountStatusDetail"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.utils.NumberUtil" %>
<%
    int section = 4;
    int section_page = 35;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%    Vector vecSearchResults = new Vector();
    Hashtable searchErrors = null;
    int intPage = 1;
    int intPageSize = 50;
    int intPageCount = 1;
    int intRecordCount = 0;
    String reportTitleKey = "jsp.admin.reports.merchant_credit_accountstatus";
    String reportTitle = Languages.getString(reportTitleKey, SessionData.getLanguage());
    String merchantDba = TransactionReport.getMerchantDBA(request.getParameter("merchantId"));
    Date currentDate = Calendar.getInstance().getTime();
    String strCurrentDate = com.debisys.utils.DateUtil.formatSqlDateTime(currentDate);
    if (request.getParameter("page") != null) {
        try {
            intPage = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException ex) {
            intPage = 1;
        }
    }
    if (intPage < 1) {
        intPage = 1;
    }
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    MerchantCreditAccountStatusDetail report = new MerchantCreditAccountStatusDetail(SessionData,
            application, false, DebisysConfigListener.getDeploymentType(application),
            DebisysConfigListener.getCustomConfigType(application),
            request.getParameter("startDate"), TransactionReport.getStartDateFormatted(),
            request.getParameter("endDate"), TransactionReport.getEndDateFormatted(),
            request.getParameter("merchantId"), merchantDba, reportTitle, strCurrentDate,
            true, intPage, intPageSize);

    if (request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y")) {
        report.setScheduling(true);
        if (report.scheduleReport()) {
            ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
            if (scheduleReport != null) {
                scheduleReport.setStartDateFixedQuery(strCurrentDate);
                scheduleReport.setEndDateFixedQuery(strCurrentDate);
                scheduleReport.setTitleName(reportTitleKey);
            }
            response.sendRedirect(DebisysConstants.PAGE_TO_SCHEDULE_REPORTS);
        }
    } else if ((request.getParameter("search") != null) && (request.getParameter("search").toLowerCase().equals("y"))) {

        if (TransactionReport.validateDateRange(SessionData)) {
            report.setPaging(true);
            report.setScheduling(false);
            vecSearchResults = report.getResults();
            intRecordCount = report.getRecordCount();
            if (intRecordCount > 0) {
                intPageCount = (intRecordCount / intPageSize);
                if ((intPageCount * intPageSize) < intRecordCount) {
                    intPageCount++;
                }
            }
        } else {
            searchErrors = TransactionReport.getErrors();
        }

    } else if (request.getParameter("downloadReport") != null && request.getParameter("downloadReport").equals("y")) {
        report.setPaging(false);
        report.setScheduling(false);
        String zippedFilePath = report.downloadReport();
        if ((zippedFilePath != null) && (!zippedFilePath.trim().equals(""))) {
            response.sendRedirect(zippedFilePath);
        }
    }
%>
<%@ include file="/includes/header.jsp" %>
<%    if (vecSearchResults != null && vecSearchResults.size() > 0) {
%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script src="includes/sortROC.js" type="text/javascript"></script>
<%
    }
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
            &nbsp;
            <%= reportTitle.toUpperCase()%>
        </td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <%
                if (searchErrors != null) {
                    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");
                    Enumeration enum1 = searchErrors.keys();
                    while (enum1.hasMoreElements()) {
                        String strKey = enum1.nextElement().toString();
                        String strError = (String) searchErrors.get(strKey);
                        out.println("<li>" + strError + "</li>");
                    }
                    out.println("</font></td></tr></table>");
                }
                if (vecSearchResults != null && vecSearchResults.size() > 0) {
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td class="main">
                        <strong>
                            <%
                                out.println(merchantDba);
                            %>
                        </strong>
                        <br>
                        <%=reportTitle%>
                        <%
                            if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                                out.println(Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted())
                                        + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                            }
                        %>
                        <br>
                        <font color="ff0000"><%= Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.limited_to_90_days", SessionData.getLanguage())%></font>
                    </td>
                    <td class=main align=right valign=bottom>
                        <%= Languages.getString("jsp.admin.reports.click_to_sort", SessionData.getLanguage())%>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <FORM ACTION="admin/reports/transactions/merchant_credit_accountstatus_detail.jsp" METHOD=post ONSUBMIT="document.getElementById('btnDownload').disabled = true;" target="_blank">
                            <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                            <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                            <INPUT TYPE=hidden NAME=merchantId VALUE="<%=TransactionReport.getMerchantId()%>">
                            <INPUT TYPE=hidden NAME=download VALUE="Y">
                            <INPUT type="hidden" NAME="downloadReport" value="y">
                            <INPUT type="hidden" NAME="search" value="n">
                            <INPUT type="hidden" NAME="sheduleReport" value="n"> 
                            <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                            <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">                            
                            <INPUT ID=btnDownload TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.downloadToCsv", SessionData.getLanguage())%>">
                        </FORM>
                    </td>
                </tr>
                <tr>
                    <td align=right class="main" nowrap colspan=2>
                        <%
                            if (intPage > 1) {
                                out.println("<a href=\"admin/reports/transactions/merchant_credit_accountstatus_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&merchantId=" + URLEncoder.encode(TransactionReport.getMerchantId(), "UTF-8") + "&page=1\">" + Languages.getString("jsp.admin.first", SessionData.getLanguage()) + "</a>&nbsp;");
                                out.println("<a href=\"admin/reports/transactions/merchant_credit_accountstatus_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&merchantId=" + URLEncoder.encode(TransactionReport.getMerchantId(), "UTF-8") + "&page=" + (intPage - 1) + "\">&lt;&lt;" + Languages.getString("jsp.admin.previous", SessionData.getLanguage()) + "</a>&nbsp;");
                            }
                            int intLowerLimit = intPage - 12;
                            int intUpperLimit = intPage + 12;
                            if (intLowerLimit < 1) {
                                intLowerLimit = 1;
                                intUpperLimit = 25;
                            }
                            for (int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) {
                                if (i == intPage) {
                                    out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                                } else {
                                    out.println("<a href=\"admin/reports/transactions/merchant_credit_accountstatus_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&merchantId=" + URLEncoder.encode(TransactionReport.getMerchantId(), "UTF-8") + "&page=" + i + "\">" + i + "</a>&nbsp;");
                                }
                            }
                            if (intPage <= (intPageCount - 1)) {
                                out.println("<a href=\"admin/reports/transactions/merchant_credit_accountstatus_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&merchantId=" + URLEncoder.encode(TransactionReport.getMerchantId(), "UTF-8") + "&page=" + (intPage + 1) + "\">" + Languages.getString("jsp.admin.next", SessionData.getLanguage()) + "&gt;&gt;</a>&nbsp;");
                                out.println("<a href=\"admin/reports/transactions/merchant_credit_accountstatus_detail.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&merchantId=" + URLEncoder.encode(TransactionReport.getMerchantId(), "UTF-8") + "&page=" + (intPageCount) + "\">" + Languages.getString("jsp.admin.last", SessionData.getLanguage()) + "</a>");
                            }
                        %>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
                <thead>
                    <tr class="SectionTopBorder">
                        <td class=rowhead2>#</td>
                        <%                                                    // Set the column headers
                            ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
                            if ((reportColumns != null) && (reportColumns.size() > 0)) {
                                for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                        %>                    
                        <td class=rowhead2 nowrap align=center><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
                        </td>
                        <%
                                }
                            }
                        %>
                    </tr>
                </thead>
                <tbody>
                    <%
                        Iterator itResults = vecSearchResults.iterator();
                        int intEvenOdd = 1;
                        int count = 1;
                        while (itResults.hasNext()) {
                            Vector vecTemp = null;
                            vecTemp = (Vector) itResults.next();
                            out.println("<tr class=row" + intEvenOdd + ">");
                            out.println("<td>" + (count++) + "</td>");
                            out.println("<td nowrap>" + vecTemp.get(0) + "</td>");
                            out.println("<td nowrap align=\"left\">" + vecTemp.get(1) + "</td>");
                            out.println("<td nowrap align=\"right\">" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(2))) + "</td>");
                            out.println("<td nowrap align=\"left\">" + vecTemp.get(3) + "</td>");
                            out.println("<td nowrap align=\"center\">" + vecTemp.get(4) + "</td>");
                            out.println("<td nowrap align=\"right\">" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(5))) + "</td>");
                            out.println("<td nowrap align=\"right\">" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(6))) + "</td>");
                            out.println("<td nowrap align=\"center\">" + vecTemp.get(7) + "</td>");
                            if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                                    && (vecTemp.size() == 9)) {
                                out.println("<td  nowrap align=\"center\">" + vecTemp.get(8) + "</td>");
                            }
                            out.println("</tr>");

                            if (intEvenOdd == 1) {
                                intEvenOdd = 2;
                            } else {
                                intEvenOdd = 1;
                            }

                        }
                    %>
                </tbody>
            </table>
            <%
                } else if ((vecSearchResults == null) || (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)) {
                    out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                }
                if (vecSearchResults != null && vecSearchResults.size() > 0) {
            %>
            <script type="text/javascript"><!--
                    var stT1 = new SortROC(document.getElementById("t1"),
                        ["None", "Date", "Number", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "Number", "Number", "Number"], 0, false, false);
--></script>
                <%
                    }
                %>
        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>
