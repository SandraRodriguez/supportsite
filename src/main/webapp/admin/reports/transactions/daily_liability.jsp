<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,com.debisys.tools.s2k,
                 com.debisys.users.SessionData" %>
<%
int section=4;
int section_page=22;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}

/*
 * Alfred A./DBSY-1082 - Generic xmlDoc loading depending on the active browser
 */
function createXMLDoc(url){
    try {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET', url, false);
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        xmlhttp.send('');
        xmlDoc = xmlhttp.responseXML;
    } catch (e) {
        try {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        } catch (e) {
            console.error(e.message);
        }
    }
}

//Alfred A./DBSY-1082 - Generic function to generate an array of values from a form.
function returnOptionsValues(formOptions){
	var paramArray = new Array();
	arrayCounter = 0;
	for(var i = 0;i<formOptions.length;i++){
		if(formOptions.options[i].value != 0 && formOptions.options[i].value != -1){
			paramArray[arrayCounter] = formOptions.options[i].value;
			arrayCounter++;
		}
	}
	return paramArray;
}

function generateURL(mainURL, additionalStringParam, paramArray, singleParam){
	var url = null;
	if(paramArray.length != 0){
		url = mainURL+paramArray[0];
		for(var i = 1; i < paramArray.length; i++){
			if(i < 100)
				url = url + additionalStringParam + paramArray[i];
		}
	}else{
		url = mainURL+singleParam;
	}
	return url;
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the ISO version.
 */
function importISOXML(){
	var isoParam = <%=SessionData.getProperty("ref_id")%>;
	var url = "admin/reports/transactions/data_daily_liability.jsp?isoParam="+isoParam;
        createXMLDoc(url);
        createAgentDropDown(xmlDoc);
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the Agent version.
 */
function importAgentXML(){
	
	var agentParam = null;
	var agentParamArray = new Array();
	//Makes sure we take our own id if we're an agent.
	if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){
		agentParam = <%=SessionData.getProperty("ref_id")%>;
	}else{
		agentParam = document.mainform.AgentTypes.value;
		if(agentParam == 0){
			agentParamArray = returnOptionsValues(document.mainform.AgentTypes);
		}
	}
        var url = '';
	if(agentParamArray.length > 100){
		url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
	}else{
		url = generateURL("admin/reports/transactions/data_daily_liability.jsp?a=", "&a=", agentParamArray, agentParam);
	}
        createXMLDoc(url);
        createSubAgentDropDown(xmlDoc);
}


/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the SubAgent version.
 */
function importSubAgentXML(){
	
	var subAgentParam = null;
	var subAgentParamArray = new Array();
	if(<%=strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){
		subAgentParam = <%=SessionData.getProperty("ref_id")%>;
	}else if(<%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>){
		subAgentParam = <%=SessionData.getProperty("ref_id")%>;
	}
	else{
		subAgentParam = document.mainform.SubAgentTypes.value;
		if(subAgentParam == 0){
			subAgentParamArray = returnOptionsValues(document.mainform.SubAgentTypes);
		}
	}
        var url = '';
	if(subAgentParamArray.length > 100){
		if(<%=strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){//You're an iso, and you've got a lot of subagents.  Agent value needs to be passed as well
			url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&subAgentTooLong="+document.mainform.AgentTypes.value+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){//Basically means you're an agent at this point, and you had tons of subagents underneath that are going to error when creating the reps dropdown.
			url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&subAgentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}
	}else{
		url = generateURL("admin/reports/transactions/data_daily_liability.jsp?a=", "&a=", subAgentParamArray, subAgentParam);
	}
        
        createXMLDoc(url);
        createRepDropDown(xmlDoc);
	
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the Rep version.
 */
function importRepXML(){
	var repParam = null;
	var repParamArray = new Array();
	//Makes sure we take our own id if we're a rep.
	if(<%=strAccessLevel.equals(DebisysConstants.REP)%>){
		repParam = <%=SessionData.getProperty("ref_id")%>;
	}else{
		repParam = document.mainform.RepTypes.value;
		if(repParam == 0){
			repParamArray = returnOptionsValues(document.mainform.RepTypes);
		}
	}
        var url = '';
	//Needs to handle iso, agent, and subagent here.
	if(repParamArray.length > 100){
		if(<%=strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){//You're an iso, and you've got a lot of subagents.  Agent value needs to be passed as well
			url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+document.mainform.SubAgentTypes.value+"&subAgentTooLong="+document.mainform.AgentTypes.value+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){//Basically means you're an agent at this point, and you had tons of subagents underneath that are going to error when creating the reps dropdown.
			url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+document.mainform.SubAgentTypes.value+"&subAgentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>){
			url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}
	}else{
		url = generateURL("admin/reports/transactions/data_daily_liability.jsp?r=", "&r=", repParamArray, repParam);
	}
        
        createXMLDoc(url);
        createMerchantList(xmlDoc);
}

/*
 * Alfred A./DBSY-1082 - Function that dynamically clear out form locations that
 * no longer need to be filled in anymore.
 */
function clearOptions(levelToClear){
	if(levelToClear == 0){
		var agentDropDown = document.mainform.SubAgentTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
	}else if(levelToClear == 1){
		var agentDropDown2 = document.mainform.RepTypes;
		for(var a = agentDropDown2.options.length;a>=0;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 2){
		var agentDropDown2 = document.mainform.mids;
		for(var a = agentDropDown2.options.length;a>=1;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 3){
		var agentDropDown = document.mainform.RepTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
		var agentDropDown2 = document.mainform.mids;
		for(var a = agentDropDown2.options.length;a>=1;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 4){
		var agentDropDown = document.mainform.SubAgentTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
		var agentDropDown2 = document.mainform.RepTypes;
		for(var a = agentDropDown2.options.length;a>=0;a--){
			agentDropDown2.options[a] = null;
		}
		var agentDropDown3 = document.mainform.mids;
		for(var a = agentDropDown3.options.length;a>=1;a--){
			agentDropDown3.options[a] = null;
		}
	}
}

/*
 * Alfred A./DBSY-1082 - Generic function that takes in a form and XML data,
 * which will then populate that form with the data.
 */
function genericFillIn(formType, x){
	//formType.options[0] = new Option("Select Option", -1);
	//formType.options[0] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_one",SessionData.getLanguage())%>", -1);
	formType.options[0] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage())%>", 0);
	//formType.options[1] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage())%>", 0);
	var dropDownCounter = 0;
	for (i=0;i<x.length;i++)
	{
		if(<%=SessionData.getProperty("ref_id")%> == x[i].getAttribute("val")){
  			continue;
  		}
		var name = x[i].firstChild.nodeValue;
		var value = x[i].getAttribute("val");
  		
  		formType.options[dropDownCounter+1] = new Option(name, value);
  		dropDownCounter++;
	}
}

// Alfred A./DBSY-1082 - These 3 functions merely set up the generic fill-in above.
function createAgentDropDown(xmlDoc){
	genericFillIn(document.mainform.AgentTypes, xmlDoc.getElementsByTagName("display"));
	importAgentXML();
}

function createSubAgentDropDown(xmlDoc){
	clearOptions(4);
	genericFillIn(document.mainform.SubAgentTypes, xmlDoc.getElementsByTagName("display"));
	importSubAgentXML();
}

function createRepDropDown(xmlDoc){
	clearOptions(3);
	genericFillIn(document.mainform.RepTypes, xmlDoc.getElementsByTagName("display"));
	importRepXML();
}

/*
 * Alfred A./DBSY-1082 - Slightly different function that functions like the generic fill in above, 
 * but also gives a hidden attribute a value in case the user has selected the ALL option in Merchants.
 */
function createMerchantList(xmlDoc){
	clearOptions(2);
	var agentDropDown = document.mainform.mids;
	var x = xmlDoc.getElementsByTagName("display");
	var allValues = new Array();
	var dropDownCounter = 0;
	for (i=0;i<x.length;i++)
	{
		if(<%=SessionData.getProperty("ref_id")%> == x[i].getAttribute("val")){
  			continue;
  		}
		var name = x[i].firstChild.nodeValue;
		var value = x[i].getAttribute("val");
		allValues[i] = value;
  		
  		agentDropDown.options[dropDownCounter+1] = new Option(name, value);
  		dropDownCounter++;
	}
	
	document.mainform.AllValues.value = allValues.join(":");
}
</SCRIPT>
<%
if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
%>
<body onload="importSubAgentXML()">
<%
}else if(strAccessLevel.equals(DebisysConstants.ISO)) {
%>
<body onload="importISOXML()">
<%
}else if(strAccessLevel.equals(DebisysConstants.AGENT)) {
%>
<body onload="importAgentXML()">
<%
}else if(strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
%>
<body onload="importSubAgentXML()">
<%
}else if(strAccessLevel.equals(DebisysConstants.REP)) {
%>
<body onload="importRepXML()">
<%
}
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title38",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="post" action="admin/reports/transactions/daily_liability_report.jsp" onSubmit="scroll();" id="daily_form">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
					<table width=400>
					<%
					if(!strAccessLevel.equals(DebisysConstants.MERCHANT)) {
					%>
					<!--<tr class="main"
						<td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%></td>
					    <td class=main nowrap></td>
					    <td class=main valign=top>-->
					    
					<%
					if(strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
					%>
					    	 <tr class="main">
								 <td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.agent.select",SessionData.getLanguage())%></td>
							     <td><SELECT NAME="AgentTypes" onchange="importAgentXML()"></SELECT></td>
							 </tr>	
							 
						   <tr></tr><tr></tr>
					<%
					}if((strAccessLevel.equals(DebisysConstants.ISO)|| strAccessLevel.equals(DebisysConstants.AGENT)) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
					%>
							<tr class="main">
								<td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.sub_agent.select",SessionData.getLanguage())%></td>
								<td><SELECT NAME="SubAgentTypes" onchange ="importSubAgentXML()"> </SELECT></td>
							</tr>
							
							<tr></tr><tr></tr>
					<%
					}if(strAccessLevel.equals(DebisysConstants.ISO)|| strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
					%>		 
							<tr class="main">
								<td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.rep.select",SessionData.getLanguage())%></td>
								<td><SELECT NAME="RepTypes" onchange="importRepXML()"> </SELECT></td>
							</tr>
							
							<tr></tr><tr></tr>
					<%
					}
					%>
                                        
                                       
							 
					<tr>
							<td class=main valign=top nowrap> <%=Languages.getString("jsp.admin.reports.transactions.daily_liability.merchant.select",SessionData.getLanguage())%></td>
					        <td class=main valign=top>
								<select name="mids" size="10" style='width:250px;' multiple>
								  <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
								  <%
								  if(strAccessLevel.equals(DebisysConstants.REP)){
									Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
									Iterator it = vecMerchantList.iterator();
									while (it.hasNext())
									{
										Vector vecTemp = null;
										vecTemp = (Vector) it.next();
										out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
									}
								  }
								   %>
								</select>	
					         <br /><br />
					        *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%><br/>
					        <%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions2",SessionData.getLanguage())%><br/>
					        <%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions3",SessionData.getLanguage())%>
						</td>
					</tr>
                                        
                                         <%
                                        if(strAccessLevel.equals(DebisysConstants.ISO)&& SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
                                        {
                                        %>
                                        <tr>
                                            <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.payments.index.select_saleReps",SessionData.getLanguage())%></td>
                                          <td class=main valign=top>
                                                <select name="salesIds" size="10" multiple>
                                                  <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
                                        <%
                                          Vector vecSalesIdList = Merchant.getMerchants2ksalesman(SessionData.getProperty("ref_id"));
                                          Iterator it1 = vecSalesIdList.iterator();
                                          while (it1.hasNext())
                                          {
                                            Vector vecTemp = null;
                                            vecTemp = (Vector) it1.next();
                                            out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                                          }

                                        %>
                                                </select>
                                                     <br>
        *<%=Languages.getString("jsp.admin.reports.salesRep.instructions",SessionData.getLanguage())%>
</td>
                                        </tr>
                                        <%}%>	
					
					<%
					} else {
						%>
					<input type="hidden" name="search" value="y">
						<script language="javascript" type="text/javascript" >
						var form = document.getElementById('daily_form');
						form.submit();
						</script>
					<%
					}
					%>
					<tr>
					    <td class=main colspan=2 align=center>
					      <input type="hidden" name="search" value="y">
					      <input type="hidden" name="AllValues">
					      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="validateSchedule(0);">
					    </td>
					    
					    <jsp:include page="/admin/reports/schreportoption.jsp">
					  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
					 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
						</jsp:include>
					
					</tr>
					
					</table>
               </td>
              </tr>
              </form>
            </table>

          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>