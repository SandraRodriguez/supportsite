<%@ page import="java.util.*"%>
<%
	int section = 3;
	int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="TransactionReport"
	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp"%>
<%@ page import="com.debisys.languages.Languages"%>

<%
	Vector vecSearchResults = new Vector();	
	Vector vecSearchResultsWithHeaders = new Vector();	
	String start_date = request.getParameter("startdate");
	String end_date = request.getParameter("enddate");
	boolean blnIso = new Boolean(request.getParameter("blnIso"));
    String merchant = request.getParameter("merchant");
    String strRuta = ""; 
     
    vecSearchResults = TransactionReport.getWriteOffTransactions(start_date, end_date,merchant, blnIso);
    vecSearchResultsWithHeaders = new Vector ();
    Vector vecTemp = new Vector();
    
    vecTemp.add(0, "#");
	vecTemp.add(1, Languages.getString("jsp.admin.reports.write_off_request_report.grid.RecId",SessionData.getLanguage()));
	vecTemp.add(2, Languages.getString("jsp.admin.reports.write_off_request_report.grid.Datetime",SessionData.getLanguage()));
	vecTemp.add(3, Languages.getString("jsp.admin.reports.write_off_request_report.grid.BusinessName",SessionData.getLanguage()));
	vecTemp.add(4, Languages.getString("jsp.admin.reports.write_off_request_report.grid.Site",SessionData.getLanguage()));
	vecTemp.add(5, Languages.getString("jsp.admin.reports.write_off_request_report.grid.SKU",SessionData.getLanguage()));
	vecTemp.add(6, Languages.getString("jsp.admin.reports.write_off_request_report.grid.ProdDescription",SessionData.getLanguage()));
	vecTemp.add(7, Languages.getString("jsp.admin.reports.write_off_request_report.grid.amount",SessionData.getLanguage()));
				vecSearchResultsWithHeaders.add(vecTemp);
				vecSearchResultsWithHeaders.addAll(vecSearchResults);
				
	strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResultsWithHeaders,16,SessionData);
	if (strRuta.length() > 0)
	{
		response.sendRedirect(strRuta);
	}
%>
