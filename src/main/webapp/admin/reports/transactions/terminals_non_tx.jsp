<%@ page import="java.net.URLEncoder,com.debisys.utils.*,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant" %>
<%
int section=4;
int section_page=27;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request" />
<jsp:setProperty name="TransactionSearch" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.terminals_non_tx_title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
     <form name="mainform" method="post" action="admin/reports/transactions/terminals_non_tx_detail.jsp" onSubmit="scroll();">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	   <td class="formArea2">
	    <table width="400">             
               <tr>
               <td valign="top" nowrap>
                    <table width=100>
                                <tr class=main>                
                                        <td nowrap><%=Languages.getString("jsp.admin.reports.terminals_non_tx_date",SessionData.getLanguage())%>:</td> 
                                        <td>
                                            <%
                                              java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
                                              formatter.setLenient(false);
                                              String date_now="";
                                              Date date_d =java.util.Calendar.getInstance().getTime();
                                              date_now = formatter.format(date_d);
                                              //date_now = DateUtil.formatDate(date_d);
                                            %>
                                        <input class="plain" name="startDate" id="startDate" value="<%=date_now%>" size="12" onfocus="linkdate.focus();">
                                        <a name="linkdate" href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.mainform.startDate);return false;" HIDEFOCUS>
                                            <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
                                        </a>
                                        <br>
                                        <%=Languages.getString("jsp.admin.reports.terminals_non_tx_type_title_combo_date",SessionData.getLanguage())%>
                                    </td>                               
                                </tr>
                                <tr class=main valign=top nowrap>                                                                           
                                        <td><%=Languages.getString("jsp.admin.reports.terminals_non_tx_options_terminals",SessionData.getLanguage())%></td>
                                        <td VALIGN="top">
                                            <select name="pids" size="10" multiple>
                                            <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
                                                <%
                                                Vector vecProductList = TransactionReport.getTerminalTypesList(SessionData);
                                                Iterator it = vecProductList.iterator();
                                                while (it.hasNext())
                                                {
                                                Vector vecTemp = null;
                                                vecTemp = (Vector) it.next();
                                                out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "("+vecTemp.get(0)+")</option>");
                                                }
                                                //TransactionSearch.getRepList(SessionData,"");
                                                %>
                                            </select>
                                            <BR>
                                            *<%=Languages.getString("jsp.admin.reports.terminals_non_tx_instructions_terminals",SessionData.getLanguage())%>
                                        </td> 
                                </tr>
                                 <%
                                  if ( !strAccessLevel.equals(DebisysConstants.MERCHANT) && !strAccessLevel.equals(DebisysConstants.REP)  )
                                  {%>
                                <tr class=main valign=top nowrap>
                                        <td><%=Languages.getString("jsp.admin.reports.terminals_non_tx_options_reps",SessionData.getLanguage())%></td>
                                        <td VALIGN="top">
                                             <select name="repsids" size="10" multiple>
                                              <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
                                                        <%
                                                          Vector vecRepList = TransactionSearch.getRepList(SessionData,"");
                                                          Iterator itReps = vecRepList.iterator();
                                                          while (itReps.hasNext())
                                                          {
                                                            Vector vecTemp = null;
                                                            vecTemp = (Vector) itReps.next();
                                                            out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "("+vecTemp.get(0)+")</option>");
                                                          }  %>
                                             </select>
                                            <BR>
                                            *<%=Languages.getString("jsp.admin.reports.terminals_non_tx_instructions_reps",SessionData.getLanguage())%>
                                        </td> 
                                </tr>                
                                <%}%>
                            </tr>
               <tr>
                    <td class=main align=center>
                      <input type="hidden" name="search" value="y">
                      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="validateSchedule(0);">
                    </td>
                     
				  	<jsp:include page="/admin/reports/schreportoption.jsp">
				  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
				  	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
				    </jsp:include>
				  
               </tr>
                   
               </td>
              </tr>
              </form>
            </table>

          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>