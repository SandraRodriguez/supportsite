<%@ page import="java.util.*,
         java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 4;
    int section_page = 18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%    Vector vecSearchResults = new Vector();
    Hashtable searchErrors = null;
    int intRecordCount = 0;
    int intPage = 1;
    int intPageSize = 50;
    int intPageCount = 1;
    boolean isInternal = Boolean.FALSE;

    int DATE_TIME = 1;
    int MILLENNIUM_NO = 2;
    int TYPE = 3;
    int HOST_ID = 4;
    int PORT = 5;
    int DURATION = 6;
    int INFO1 = 7;
    int RESULT_CODE = 8;
    int TRANSACTION_ID = 9;
    int MOBILE = 10;
    int PROVIDER_RESPONSE = 12;
    int INVOICE_NUMBER = 13;
    int DBA = 14;
    int PROVIDER = 15;
    int MULTISOURCE = 16;

    int ASC = 1;
    int DESC = 2;

    String products = "";
    String providersIds = "";

    String queryType = null;
    String appendToDownloadURL = "";
    boolean showInfoMultiSource = (SessionData.checkPermission(DebisysConstants.PERM_SHOW_MULTISOURCE_INFO) ? true : false);

    boolean showAccountIdQRCode = false;
    showAccountIdQRCode = SessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);

    if (request.getParameter("page") != null) {
        try {
            intPage = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException ex) {
            intPage = 1;
        }
    }

    if (request.getParameter("queryType") != null) {
        queryType = request.getParameter("queryType");
    }

    if (intPage < 1) {
        intPage = 1;
    }

    if (TransactionReport.validateDateRange(SessionData)) {
        SessionData.setProperty("start_date", request.getParameter("startDate"));
        SessionData.setProperty("end_date", request.getParameter("endDate"));
        products = request.getParameter("products");
        if (products != null && !products.trim().equals("")) {
            TransactionReport.setProductIds(products);
        }
        providersIds = request.getParameter("providersIds");
        if (providersIds != null && !providersIds.trim().equals("")) {
            TransactionReport.setProviderIDs(providersIds);
        }        
        if(request.getParameter("isInternal") != null) {            
            if(request.getParameter("isInternal") == "true" || request.getParameter("isInternal").equalsIgnoreCase("true")) {
                isInternal = Boolean.TRUE;
            }
        }        
        vecSearchResults = TransactionReport.getTransactionErrorDetailByTypes(intPage, intPageSize, SessionData, true, queryType, isInternal);
        intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
        vecSearchResults.removeElementAt(0);
        if (intRecordCount > 0) {
            intPageCount = (intRecordCount / intPageSize);
            if ((intPageCount * intPageSize) < intRecordCount) {
                intPageCount++;
            }
        }
    }
    else {
        searchErrors = TransactionReport.getErrors();
    }
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
    <tr>
        <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td class="formAreaTitle" align=left width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.transaction_error_details.title", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF">
            <%
                if (vecSearchResults != null && vecSearchResults.size() > 0) {
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <%
                    Vector vTimeZoneData = null;
                    if (SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT)) {
                        vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
                    }
                    else {
                        vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
                    }
                %>
                <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
                <tr><td class="main"><%=intRecordCount%> result(s) found<%
                    if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                        out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                    }
                        %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{Integer.toString(intPage), Integer.toString(intPageCount)}, SessionData.getLanguage())%>
                        <br><%=Languages.getString("jsp.admin.reports.click_to_sort", SessionData.getLanguage())%>
                        <table>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td><%if (queryType != null) {
                                        appendToDownloadURL = "?queryType=" + queryType + "";
                                        System.out.println("download::with::" + appendToDownloadURL);
                                    }%>
                                    <form target="blank" method=post action="admin/reports/transactions/export_transaction_error_detail_by_types.jsp<%=appendToDownloadURL%>" onsubmit="document.getElementById('btnSubmit').disabled = true;">
                                        <input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
                                        <input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
                                        <input type=hidden name="merchant_ids" value="<%=TransactionReport.getMerchantIds()%>">
                                        <input type=hidden name="col" value="<%=TransactionReport.getCol()%>">
                                        <input type=hidden name="sort" value="<%=TransactionReport.getSort()%>">
                                        <input type=hidden name="page" value="<%=intPage%>">
                                        <input type=hidden name="isInternal" value="<%=isInternal%>">
                                        <input type=hidden name="resultCode" value="<%=TransactionReport.getResultCode()%>">
                                        <input type=hidden name="providersIds" value="<%=TransactionReport.getProviderIDs()%>">
                                        <%
                                        if (products != null && !products.trim().equals("")) {%>
                                        <input type=hidden name="products" value="<%=TransactionReport.getProductIds()%>">
                                        <%}
                                        %>
                                        <input id=btnSubmit type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download", SessionData.getLanguage())%>">
                                    </form>
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <form target="blank" method=post action="admin/reports/transactions/print_transaction_error_detail_by_types.jsp<%=appendToDownloadURL%>">
                                        <input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
                                        <input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
                                        <input type=hidden name="merchant_ids" value="<%=TransactionReport.getMerchantIds()%>">
                                        <input type=hidden name="col" value="<%=TransactionReport.getCol()%>">
                                        <input type=hidden name="sort" value="<%=TransactionReport.getSort()%>">
                                        <input type=hidden name="page" value="<%=intPage%>">
                                        <input type=hidden name="isInternal" value="<%=isInternal%>">
                                        <input type=hidden name="resultCode" value="<%=TransactionReport.getResultCode()%>">
                                        <input type=hidden name="providersIds" value="<%=TransactionReport.getProviderIDs()%>">
                                        <%
                        if (products != null && !products.trim().equals("")) {%>
                                        <input type=hidden name="products" value="<%=TransactionReport.getProductIds()%>">
                                        <%}
                                        %>
                                        <input type=submit value="<%=Languages.getString("jsp.admin.reports.print", SessionData.getLanguage())%>">
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </td></tr>
                <tr>
                    <td align=right class="main" nowrap>
                        <%                            
                            if(request.getParameter("isInternal") != null) {            
                                if(request.getParameter("isInternal") == "true" || request.getParameter("isInternal").equalsIgnoreCase("true")) {
                                    isInternal = Boolean.TRUE;
                                }
                            }   
                            if (intPage > 1) {
                                out.println("<a href=\"admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=" + isInternal + "&merchantIds=" + TransactionReport.getMerchantIds() + "&products=" + TransactionReport.getProductIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=1\">First</a>&nbsp;");
                                out.println("<a href=\"admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=" + isInternal + "&merchantIds=" + TransactionReport.getMerchantIds() + "&products=" + TransactionReport.getProductIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPage - 1) + "\">&lt;&lt;Prev</a>&nbsp;");
                            }                                         

                            int intLowerLimit = intPage - 12;
                            int intUpperLimit = intPage + 12;

                            if (intLowerLimit < 1) {
                                intLowerLimit = 1;
                                intUpperLimit = 25;
                            }

                            for (int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) {
                                if (i == intPage) {
                                    out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                                }
                                else {
                                    out.println("<a href=\"admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=" + isInternal + "&merchantIds=" + TransactionReport.getMerchantIds() + "&products=" + TransactionReport.getProductIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + i + "\">" + i + "</a>&nbsp;");
                                }
                            }

                            if (intPage <= (intPageCount - 1)) {
                                out.println("<a href=\"admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=" + isInternal + "&merchantIds=" + TransactionReport.getMerchantIds() + "&products=" + TransactionReport.getProductIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPage + 1) + "\">Next&gt;&gt;</a>&nbsp;");
                                out.println("<a href=\"admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=" + isInternal + "&merchantIds=" + TransactionReport.getMerchantIds() + "&products=" + TransactionReport.getProductIds() + "&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPageCount) + "\">Last</a>");
                            }

                        %>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
                <thead>
                    <tr>
                        <td class=rowhead2>#</td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=DATE_TIME%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=DATE_TIME%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_id", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=TRANSACTION_ID%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=TRANSACTION_ID%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.amount", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk_id", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.product_id", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=INFO1%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=INFO1%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.phone", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=MOBILE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=MOBILE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.dba", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=DBA%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=DBA%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.term_no", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=MILLENNIUM_NO%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=MILLENNIUM_NO%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.type", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=TYPE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=TYPE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.host", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=HOST_ID%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=HOST_ID%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.port", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=PORT%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=PORT%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.duration", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=DURATION%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=DURATION%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.result", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=RESULT_CODE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=RESULT_CODE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.provider_response", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=PROVIDER_RESPONSE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=PROVIDER_RESPONSE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                                <%
                                    if (showInfoMultiSource) {
                                %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.tools.dtu2536.ProvBackup", SessionData.getLanguage()).toUpperCase()%>&nbsp;
                            <a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=PROVIDER%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>">
                                <img src="images/down.png" height=11 width=11 border=0>
                            </a>
                            <a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=PROVIDER%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>">
                                <img src="images/up.png" height=11 width=11 border=0>
                            </a>
                        </td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.tools.dtu2536.MultiSource", SessionData.getLanguage()).toUpperCase()%>&nbsp;
                            <a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=MULTISOURCE%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>">
                                <img src="images/down.png" height=11 width=11 border=0>
                            </a>
                            <a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=MULTISOURCE%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>">
                                <img src="images/up.png" height=11 width=11 border=0>
                            </a>
                        </td>
                        <%
                            }
                            if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
                                    && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {%>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.invoiceno", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=INVOICE_NUMBER%>&sort=<%=ASC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_detail_by_types.jsp?isInternal=<%=isInternal%>&page=<%=intPage%>&col=<%=INVOICE_NUMBER%>&sort=<%=DESC%>&merchantIds=<%=TransactionReport.getMerchantIds()%>&resultCode=<%=TransactionReport.getResultCode()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                                <%} %>

                        <%if (showAccountIdQRCode) {%>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.reference", SessionData.getLanguage()).toUpperCase()%></td>
                        <%}%>
                    </tr>
                </thead>
                <%
                    int intCounter = 1;
                    Iterator it = vecSearchResults.iterator();
                    int intEvenOdd = 1;
                    while (it.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        out.println("<tr class=row" + intEvenOdd + ">"
                                + "<td nowrap>" + intCounter++ + "</td>"
                                + "<td nowrap>" + vecTemp.get(0) + "</td>"
                                + "<td nowrap>" + vecTemp.get(1) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(2) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(3) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(4) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(5) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(6) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(7) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(8) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(9) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(11) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(12) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(13) + "</td>");
                        if (showInfoMultiSource) {
                            out.println("<td nowrap align=\"right\">" + vecTemp.get(14) + "</td>");
                            String markMultiSource = "";
                            if (vecTemp.get(15) == "1") {
                                markMultiSource = Languages.getString("jsp.tools.dtu2536.MultiSourceFlag", SessionData.getLanguage()).toUpperCase();
                            }
                            out.println("<td nowrap align=\"center\">" + markMultiSource + "</td>");
                        }
                        if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
                                && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                            //DBSY-919 Invoice Number Field
                            out.println("<td>" + vecTemp.get(16) + "</td>");
                            ///
                        }

                        if (vecTemp.size() >= 18 && showAccountIdQRCode) {
                            out.println("<td>" + vecTemp.get(17) + "</td>");
                        }
                        else if (showAccountIdQRCode) {
                            out.println("<td></td>");
                        }

                        out.println("</tr>");

                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        }
                        else {
                            intEvenOdd = 1;
                        }

                    }
                    vecSearchResults.clear();
                %>
            </table>
            <%
                }
                else {
                    out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                }
            %>
        </td>
    </tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>


<%@ include file="/includes/footer.jsp" %>