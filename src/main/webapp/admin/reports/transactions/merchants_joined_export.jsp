<%@ page import="java.util.*"%>
<%
	int section = 4;
	int section_page = 39; 
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="TransactionReport"
	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>	
<jsp:setProperty name="TransactionReport" property="*" />
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp"%>

<%
	Vector vecSearchResults = new Vector();
	String strRuta = "";
	String strAgentIds = request.getParameter("AgentIds");
	String strSubAgentIds = request.getParameter("SubAgentIds");
	
	if (strAgentIds != null)
	{	    
		CustomerSearch.setAgentIds(strAgentIds);
	}
	if(strSubAgentIds != null)
	{
		CustomerSearch.setSubAgentIds(strSubAgentIds);
	}
	
	vecSearchResults = CustomerSearch.listJoinedMerchants(1,0,SessionData,application);
	vecSearchResults.remove(0);	
	strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,14,SessionData);
	if (strRuta.length() > 0)
	{
		response.sendRedirect(strRuta);
	}
%>