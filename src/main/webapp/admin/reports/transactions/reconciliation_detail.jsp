<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="com.debisys.reports.schedule.ReconciliationDetailReport"%>
<%@page import="com.debisys.utils.StringUtil"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 4;
    int section_page = 52;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>

<%@ include file="/includes/security.jsp" %>
<%    Vector<Vector<Object>> vec_results = new Vector<Vector<Object>>();
    ReconciliationDetailReport report = null;
    Hashtable searchErrors = null;
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String reportTitleKey = "jsp.admin.reports.transactions.reconcile_detail.title";
    String reportTitle = Languages.getString(reportTitleKey, SessionData.getLanguage()).toUpperCase();

    SessionData.setProperty("start_date", startDate);
    SessionData.setProperty("end_date", endDate);

    if (request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y")) {
        //TO SCHEDULE REPORT
        report = new ReconciliationDetailReport(SessionData, application, startDate, endDate, true);
        if (report.scheduleReport()) {
            ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
            if (scheduleReport != null) {
                scheduleReport.setStartDateFixedQuery(report.getStartDate());
                scheduleReport.setEndDateFixedQuery(report.getEndDate());
                scheduleReport.setTitleName(reportTitleKey);
            }
            response.sendRedirect(DebisysConstants.PAGE_TO_SCHEDULE_REPORTS);
        }
    } else if (request.getParameter("search") != null && request.getParameter("search").equals("y")) {
        if ((startDate != null) && (endDate != null) && (TransactionReport.validateDateRange(SessionData))) {
            report = new ReconciliationDetailReport(SessionData, application, startDate, endDate, false);
            vec_results = report.getResults();
        } else {
            searchErrors = TransactionReport.getErrors();
        }
    } else if (request.getParameter("download") != null && request.getParameter("download").equals("y")) {
        //TO DOWNLOAD ZIP REPORT
        if ((startDate != null) && (endDate != null) && (TransactionReport.validateDateRange(SessionData))) {
            report = new ReconciliationDetailReport(SessionData, application, startDate, endDate, false);
            vec_results = report.getResults();
            report.setScheduling(false);
            String zippedFilePath = report.downloadReport();
            if ((zippedFilePath != null) && (!zippedFilePath.trim().equals(""))) {
                response.sendRedirect(zippedFilePath);
            }
        } else {
            searchErrors = TransactionReport.getErrors();
        }
    }

%>
<%@ include file="/includes/header.jsp" %>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= reportTitle.toUpperCase()%></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <%
                if (request.getParameter("search") == null || searchErrors != null) {
                    if ((searchErrors != null) && (searchErrors.size() > 0)) {
                        out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");

                        Enumeration enum1 = searchErrors.keys();

                        while (enum1.hasMoreElements()) {
                            String strKey = enum1.nextElement().toString();
                            String strError = (String) searchErrors.get(strKey);
                            out.println("<li>" + strError);
                        }

                        out.println("</font></td></tr></table>");
                    }
            %>
            <table width=400>
                <form name="mainform" action="admin/reports/transactions/reconciliation_detail.jsp" method=post>
                    <tr class="main">
                        <td nowrap>
                            <%=Languages.getString("jsp.admin.start_date", SessionData.getLanguage())%>:
                        </td>
                        <td>
                            <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if (self.gfPop)
                                        gfPop.fStartPop(document.mainform.startDate, document.mainform.endDate);
                                    return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
                        </td>
                    </tr>
                    <tr class="main">
                        <td nowrap>
                            <%=Languages.getString("jsp.admin.end_date", SessionData.getLanguage())%>: 
                        </td>
                        <td>
                            <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if (self.gfPop)
                                        gfPop.fEndPop(document.mainform.startDate, document.mainform.endDate);
                                    return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" name="search" value="y">
                            <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report", SessionData.getLanguage())%>">
                        </td>
                        <jsp:include page="/admin/reports/schreportoption.jsp">
                            <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
                            <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
                        </jsp:include>
                    </tr>
                </form>
            </table>
            <%
            } else if (vec_results != null && vec_results.size() > 0) {
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <%
                    Vector vTimeZoneData = null;
                    if (SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT)) {
                        vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
                    } else {
                        vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
                    }
                %>
                <tr>
                    <td class="main">
                        <%=(vec_results.size() - 1) + " " + Languages.getString("jsp.admin.results_found", SessionData.getLanguage()) + " "%>
                        <%
                            if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                                out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                            }
                        %>
                    </td>
                </tr>
                <tr class="main">
                    <td nowrap colspan="2">
                        <%=Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%>
                    </td>
                </tr>
                <tr>          
                    <td class="main">
                        <font color="#ff0000"><%= Languages.getString("jsp.admin.reports.test_trans", SessionData.getLanguage())%></font>
                    </td>
                </tr>
                <tr>
                    <td class="main">
                        <%=Languages.getString("jsp.admin.reports.transactions.reconcile_summary.report_generated", SessionData.getLanguage()) + " " + TransactionReport.currentdate(SessionData)%> 
                        <br><%=TransactionReport.getHeaderUserInfo(SessionData, application)%>
                    </td>
                </tr>
            </table>
            <%
                if (request.getParameter("search") != null) {
            %>
            <table>
                <tr>
                    <td>	
                        <FORM ACTION="admin/reports/transactions/reconciliation_detail.jsp" METHOD=post">
                            <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                            <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                            <INPUT TYPE=hidden NAME=download VALUE="y">
                            <INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download", SessionData.getLanguage())%>">
                        </FORM>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <%
                }
            %>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
                <thead>
                    <tr class="SectionTopBorder">
                        <td class=rowhead2 nowrap>#</td>

                        <%
                            // Set the column headers
                            ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
                            if ((reportColumns != null) && (reportColumns.size() > 0)) {
                                for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                        %>                    
                        <td class=rowhead2 align=center><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
                        </td>
                        <%
                                }
                            }
                        %>                        

                    </tr>
                </thead>
                <%
                    Iterator<Vector<Object>> iter = vec_results.iterator();
                    int intEvenOdd = 1;
                    int index = 1;
                    while (iter.hasNext()) {
                        Vector<Object> vecTemp = iter.next();

                        String s_boldSetting = "";

                        if (vecTemp.get(0).equals(Languages.getString("jsp.admin.reports.transactions.reconcile_detail.grandTotal", SessionData.getLanguage()))) {
                            s_boldSetting = "class=\"mainBold\"";
                        }

                        out.println("<tr class=row" + intEvenOdd + ">");
                        out.println("<td " + s_boldSetting + ">" + index++ + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + String.valueOf(vecTemp.get(0)) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + String.valueOf(vecTemp.get(1)) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + String.valueOf(vecTemp.get(2)) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + String.valueOf(vecTemp.get(3)) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(4))) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(5))) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(6))) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(7))) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(8))) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(9))) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(10))) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(11))) + "</td>");
                        out.println("<td " + s_boldSetting + " nowrap>" + NumberUtil.formatCurrency(String.valueOf(vecTemp.get(12))) + "</td>");
                %>
    </tr>
    <%
            if (intEvenOdd == 1) {
                intEvenOdd = 2;
            } else {
                intEvenOdd = 1;
            }
        }
    %>
</table>
<%
    } else {
        if ((vec_results == null) || (((vec_results != null) && (vec_results.size() == 0)) && (request.getParameter("search") != null) && (searchErrors == null))) {
            out.println("<br><br><font color=ff0000>"
                    + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
        }

    }

    if (vec_results != null) {
%>
<SCRIPT type="text/javascript">
    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
            [<%="\"Number\", \"CaseInsensitiveString\", \"CaseInsensitiveString\", \"CaseInsensitiveString\", \"Number\", \"Number\", \"Number\", \"Number\", \"Number\", \"Number\", \"Number\", \"Number\", \"Number\", \"Number\""%>], 0, false, false);
-->
    	            
</SCRIPT>
<%
    }
%>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>