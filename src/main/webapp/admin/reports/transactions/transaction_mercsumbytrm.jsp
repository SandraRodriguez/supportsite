<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=1;
String sMerchantID = "";
String sAux = "";
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%	
	if ( request.getParameter("mids") != null )
	{
		if ( request.getParameter("lblShowReport").equals("y") || request.getParameter("sheduleReport").equals("y") )
		{
			String sURL = "";
			String sMIDs = "";
			String sMILs = "";
			sURL += "startDate=" + request.getParameter("startDate");
			sURL += "&endDate=" + request.getParameter("endDate");
			sURL += "&mids=";
			if ( request.getParameter("rbtnView").equals("Multiple") )
			{
				String sMerchantIDs[] = request.getParameterValues("mids");
				for ( int i = 0; i < sMerchantIDs.length; i++ )
				{
					if ( (sMerchantIDs[i] != null) && !sMerchantIDs[i].equals("") )
					{
						if ( sMIDs.length() > 0 )
						{
							sMIDs += ",";
						}
						sMIDs += sMerchantIDs[i];
					}
				}
			}
			else
			{
				sMIDs = request.getParameter("mids2");
			}
			sURL += sMIDs;
			sURL += "&merchantId=" + sMIDs;
			sURL += "&millennium_no=";
			String sTerminals[] = request.getParameterValues("millennium_no");
			for ( int i = 0; i < sTerminals.length; i++ )
			{
				if ( (sTerminals[i] != null) && !sTerminals[i].equals("") )
				{
					if ( sMILs.length() > 0 )
					{
						sMILs += ",";
					}
					sMILs += sTerminals[i];
				}
			}
			sURL += sMILs;
			if ( request.getParameter("chkUseTaxValue") != null )
			{
				sURL += "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue");
			}
			String schedule = request.getParameter("sheduleReport");
			if ( request.getParameter("rbtnView").equals("Multiple") )
			{
				response.sendRedirect("transaction_mercsumbytrm_merchants.jsp?search=y&" + sURL + "&sheduleReport="+schedule);
			}
			else
			{
				String merchantName = URLEncoder.encode(request.getParameter("merchantName"));
				response.sendRedirect("transaction_mercsumbytrm_terminals.jsp?search=y&" + sURL + "&sheduleReport="+schedule+"&merchantName="+merchantName);
			}
			return;
		}
		sMerchantID = request.getParameter("mids2");
	}
%>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.getElementById("btnSubmit").disabled = true;
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}
function validateSchedule(shedule)
{   
  $('#merchantName').val( $("#mids2").find('option:selected').text() );
  if ( shedule == "1" )
  	$('#sheduleReport').val("y");
  else	
  	$('#sheduleReport').val("");
  return false;	  
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.TransactionMerchantSummaryByTerminal",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form id="mainform" name="mainform" method="post" action="admin/reports/transactions/transaction_mercsumbytrm.jsp" onSubmit="scroll();">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
<%
	Vector vTimeZoneData = null;
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
              <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
              <tr class="main">
               <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
		         <%}%><td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
<SCRIPT>
function EnableSingle()
{
	document.getElementById("lblSingle").style.display = "inline";
	document.getElementById("lblMultiple").style.display = "none";
	document.getElementById("divSingle").style.display = "inline";
	document.getElementById("divMultiple").style.display = "none";
}
function EnableMultiple()
{
	document.getElementById("lblSingle").style.display = "none";
	document.getElementById("lblMultiple").style.display = "inline";
	document.getElementById("divSingle").style.display = "none";
	document.getElementById("divMultiple").style.display = "inline";
}
</SCRIPT>
<table width=400>
<tr class="main">
<%
	sAux = SessionData.getProperty("start_date");
	if ( request.getParameter("startDate") != null )
	{
		sAux = request.getParameter("startDate");
	}
%>
    <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td><td>
    <input class="plain" name="startDate" value="<%=sAux%>" size="12">
    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<tr class="main">
<%
	sAux = SessionData.getProperty("end_date");
	if ( request.getParameter("endDate") != null )
	{
		sAux = request.getParameter("endDate");
	}
%>
    <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td>
    <input class="plain" name="endDate" value="<%=sAux%>" size="12">
    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<tr>
<%
String sSingle = "";
String sMultiple = "";
if ( request.getParameter("rbtnView") != null && request.getParameter("rbtnView").equals("Multiple") )
{
	sMultiple = " CHECKED ";
}
else if ( request.getParameter("rbtnView") != null && request.getParameter("rbtnView").equals("Single") )
{
	sSingle = " CHECKED ";
}
else
{
	if ( strAccessLevel.equals(DebisysConstants.MERCHANT) )
	{
		sSingle = " CHECKED ";
	}
	else
	{
		sMultiple = " CHECKED ";
	}
}
%>
<TR CLASS="main">
    <TD></TD><TD NOWRAP><INPUT TYPE="radio" ID="rbtnSingle" NAME="rbtnView" VALUE="Single" ONCLICK="EnableSingle();" <%=sSingle%>><LABEL FOR="rbtnSingle"><%=Languages.getString("jsp.admin.reports.transaction.merchant_summarybyterminal.singlemerchant",SessionData.getLanguage())%></LABEL></TD>
</TR>
<%
if ( !strAccessLevel.equals(DebisysConstants.MERCHANT) )
{
%>
<TR CLASS="main">
    <TD></TD><TD NOWRAP><INPUT TYPE="radio" ID="rbtnMultiple" NAME="rbtnView" VALUE="Multiple" ONCLICK="EnableMultiple();" <%=sMultiple%>><LABEL FOR="rbtnMultiple"><%=Languages.getString("jsp.admin.reports.transaction.merchant_summarybyterminal.multiplemerchant",SessionData.getLanguage())%></LABEL></TD>
</TR>
<%
}
%>
<tr>
    <td class=main valign=top nowrap>
    	<span id="lblSingle" style="display:none;"><%=Languages.getString("jsp.admin.reports.transactions.merchants.optionsingle",SessionData.getLanguage())%></span>
    	<span id="lblMultiple" style="display:inline;"><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%></span>
    </td>
    <td class=main valign=top>
    	<div id="divSingle" style="display:none;">
        <select id="mids2" name="mids2" onchange="document.getElementById('lblShowReport').value='';document.getElementById('mainform').submit();">
<%
  Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
  Iterator it = vecMerchantList.iterator();
  while (it.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it.next();
    if ( sMerchantID.equals(vecTemp.get(0).toString()) )
    {
    	out.println("<option value=" + vecTemp.get(0) +" selected>" + vecTemp.get(1) + "</option>");
    }
    else
    {
    	out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
    }
  }

%>
        </select>
        <br>
        <br>
        <select name="millennium_no" size="10" multiple>
          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%

  if ( sMerchantID.equals("") && vecMerchantList.size()>0)
  {
	  sMerchantID = ((Vector)vecMerchantList.get(0)).get(0).toString();
  }
  if(!sMerchantID.equals("")){
  it = com.debisys.customers.Merchant.getTerminals(sMerchantID).iterator();
  while (it.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(0) + " (" + vecTemp.get(1) + ")" + "</option>");
  }
  }

%>
        </select>
    	</div>
    	<div id="divMultiple" style="display:inline;">
        <select name="mids" size="10" multiple>
          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
  it = vecMerchantList.iterator();
  while (it.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
  }

%>
        </select>
        <br>
        *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%>
        </div>
</td>
</tr>
<%
 if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && SessionData.checkPermission(DebisysConstants.GENERATE_REPORT_WITH_TAXES_MX))
 {//If when deploying in Mexico
%>
<TR CLASS="main">
    <TD></TD>
    <TD NOWRAP>
    	<!--<INPUT TYPE="checkbox" ID="chkUseTaxValue" NAME="chkUseTaxValue"><LABEL FOR="chkUseTaxValue"><%=Languages.getString("jsp.admin.reports.transactions.products.mx_valueaddedtax",SessionData.getLanguage())%></LABEL> -->
    	<input type="hidden" name="chkUseTaxValue" id="chkUseTaxValue" value="on">
    </TD>
</TR>
<TR CLASS="main"><TD></TD></TR>
<%
 }//End of if when deploying in Mexico

if ( sSingle.length() > 0 )
{
	out.println("<SCRIPT>EnableSingle();</SCRIPT>");
}
else
{
	out.println("<SCRIPT>EnableMultiple();</SCRIPT>");
}
%>

<tr>
    <td class=main align=center>
      <input type="hidden" id="sheduleReport" name="sheduleReport" value="">	 
      <input type="hidden" id="merchantName" name="merchantName" value="">	
      <input type="hidden" name="search" value="y">
      <input id="lblShowReport" type="hidden" name="lblShowReport" value="">
      <input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" 
      	ONCLICK="document.getElementById('lblShowReport').value='y'; $('#merchantName').val( $('#mids2').find('option:selected').text() ); $('#sheduleReport').val(''); ">
    </td>
    <jsp:include page="/admin/reports/schreportoption.jsp">
	  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
	 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
	</jsp:include>   
</tr>
<tr>
<td class="main" colspan=2>
* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
</td>
</tr>
</table>
               </td>
              </tr>
              </form>
            </table>

          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>