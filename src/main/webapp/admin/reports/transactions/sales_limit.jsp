<%@ page import="
				java.util.*,
				com.debisys.customers.Merchant"
%>
<%@page import="com.debisys.reports.TransactionReport"%>
<%
int section=4;
int section_page=45;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.saleslimit.reportTitle",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
				<tr>
					<td>
						<form name="mainform" method="post" action="admin/reports/transactions/sales_limit_summary.jsp">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="formArea2">
										<table width="300">
											<tr class="main">
												<td nowrap>
													<%=Languages.getString("jsp.admin.saleslimit.salesLimitType",SessionData.getLanguage())%>
												</td>
												<td nowrap>
													<select name="salesLimitType">
														<option value="DAILY"><%=Languages.getString("jsp.admin.saleslimit.salesLimitTypeDAILY",SessionData.getLanguage())%></option>
													</select>
												</td>
											</tr>
											<tr>
												<td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%>&nbsp;&nbsp;</td>
												<td class=main valign=top>
													<select name="mids" size="10" multiple>
														<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
	Vector vecMerchantList = Merchant.getMerchantListSalesLimitReport(SessionData);
	Iterator it = vecMerchantList.iterator();
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
	}
%>
													</select>
												</td>
											</tr>
											<tr>
												<td class=main colspan=2 align=center>
													<br>*<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%>
												</td>
											</tr>
											<tr>
												<td class=main colspan=2 align=center>
													<input type="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>