<%@page import="com.debisys.reports.ReportsUtil"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%@page import="java.net.URLDecoder"%>
<%
  int section      = 4;
  int section_page = 1;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%

  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";
  Vector warningSearchResults = new Vector();
  boolean   bUseTaxValue     = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns
 
  boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(SessionData, application);

  String merchantId = "";
  String millennium_no = "";
  String merchantName = "";
  
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();

  String noteTimeZone = "";
  String testTrx = "";  
  String titleReport = "";

  Vector vTimeZoneData = null;
  if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
  {
	vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id"))); 
  }
  else
  {
	vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  }			

  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));
			  
	  if ( request.getParameter("merchantName") != null)	
	  	merchantName = request.getParameter("merchantName").toUpperCase();
	  merchantId = request.getParameter("merchantId");	
	   
      TransactionReport.setMerchantIds( merchantId );
      if ( request.getParameter("millennium_no") != null )
      {
      	  millennium_no = request.getParameter("millennium_no");
    	  TransactionReport.setMillennium_No( millennium_no );
      }

	  //////////////////////////////////////////////////////////////////
	  //HERE WE DEFINE THE REPORT'S HEADERS 
	  headers = com.debisys.reports.ReportsUtil.getHeadersSummaryTransactionsTerminals( SessionData,application, DebisysConstants.MERCHANT );
	  //////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////
		  
  	  noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
	  String keyLanguage = "jsp.admin.reports.transaction.merchant_summarybyterminal";
	  titleReport = Languages.getString( keyLanguage ,SessionData.getLanguage());
	  testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());

	  boolean IsMexAndChkTax = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
           							(request.getParameter("chkUseTaxValue") != null); 		

	  if (  IsMexAndChkTax )
      {
        bUseTaxValue = true;
      }
       	
	  if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	  {
		 //TO SCHEDULE REPORT
		 titles.add(noteTimeZone);
		 titles.add(titleReport + " - "+merchantName);		
         titles.add(testTrx);
         TransactionReport.getMerchantTransactionsByTerminal( SessionData, application , DebisysConstants.SCHEDULE_REPORT , headers, titles );
         ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
         if (  scheduleReport != null  )
		 {
		 	scheduleReport.setName( DebisysConstants.SC_TRX_SUMM_BY_TERMINAL );
			scheduleReport.setAdditionalData( merchantName );
			scheduleReport.setTitleName( keyLanguage );
			scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
			scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
			response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS + "?title="+titleReport + " - "+ URLEncoder.encode( merchantName ) );   
		 }		     
      }
      else if ( request.getParameter("downloadReport") != null )
	  {
	  	 //TO DOWNLOAD ZIP REPORT
		 if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		 {
			   titleReport = titleReport + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
			   titleReport = titleReport + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+  HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
		 }
		 titles.add(noteTimeZone);
		 titles.add(titleReport);
	     titles.add(testTrx);
	     TransactionReport.getMerchantTransactionsByTerminal( SessionData, application , DebisysConstants.DOWNLOAD_REPORT , headers, titles );
	     response.sendRedirect( TransactionReport.getStrUrlLocation() );	
	  }
	  else 
	  {
	     //TO SHOW REPORT
		 if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		 {
			   titleReport = titleReport + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
			   titleReport = titleReport + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+  HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
		 }
		 titles.add(noteTimeZone);
		 titles.add(titleReport);
	     titles.add(testTrx);
	     
	     vecSearchResults = TransactionReport.getMerchantTransactionsByTerminal( SessionData, application , DebisysConstants.EXECUTE_REPORT , headers, titles );
	     
	  }
	  warningSearchResults = TransactionReport.getWarningMessage();
	 
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
	  //sAux = ((Vector)vecSearchResults.get(1)).get(0).toString();
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
<%

%>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.transaction.merchant_summarybyterminalfor",SessionData.getLanguage()).toUpperCase() + " - " + merchantName %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    <% 
                    boolean showWarnings = false;
                    if ( showWarnings && warningSearchResults.size()>0){
                    %>
				<tr>
				<td class="main" style="color:red">
					<% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1)
								||  (!TransactionReport.checkfortaxtype(SessionData))){
				%>
								<%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
								<%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
				<% } %>
					<% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
						<%= warningSearchResults.get(i).toString()%> <br/>
					<% } %>
				</td>
				</tr>
                    <%
                    }
                    %>
<%
			
%>
            <tr class="main"><td nowrap colspan="2"><%= noteTimeZone %><br/><br/></td></tr>
            <tr>
              <td class="main">
				<%= titleReport %>
                <br>
                <font color="#ff0000">
                  <%= testTrx %>
                </font>
              </td>
              <!-- 
              <td class=main align=right valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
               -->              
            </tr>
          </table>
          
          <form name="downloadData" method=post action="admin/reports/transactions/transaction_mercsumbytrm_terminals.jsp">
              <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
              <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
              <input type="hidden" name="search" value="y">
              <input type="hidden" name="downloadReport" value="y">            
              <input type="hidden" name="merchantId" value="<%= merchantId %>">
              <input type="hidden" name="millennium_no" value="<%= millennium_no %>">
              <input type="hidden" name="merchantName" value="<%= merchantName %>">              
              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		  </form>
          <table>
                <tr>
                    <td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
                </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.TerminalDescription",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
                sortString = "\"None\",\"CaseInsensitiveString\",\"Number\",\"Number\",\"Number\"";
%>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
 				if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
                {
                %>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase() %>
                </td><td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <%
                }
                if ( bUseTaxValue )
                {
                  sortString = sortString + ",\"Number\"";
%>
                <TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%></TD>
<%
                }
                if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
                {
                sortString = sortString + ",\"Number\"";
                sortString = sortString + ",\"Number\"";
%>
 			<TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></TD>
 			<TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                }
%>

<%

                if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%>
                  </td>
<%
                  if ( bUseTaxValue )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
 	if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
                {
                sortString = sortString + ",\"Number\"";
%>
 			<TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%></TD>
 <%
                }
%>
<%
                }
                if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%>
                  </td>
<%
                   if ( bUseTaxValue )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage())%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                  if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                }
                if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%>
                  </td>
<%
                   if ( bUseTaxValue )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage())%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                    if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                }
                if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%>
                  </td>
<%
                   if ( bUseTaxValue )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage())%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                    if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                }
                //mer %
                sortString = sortString + ",\"Number\"";
%>
                <td class=rowhead2 valign=bottom nowrap>
                  <%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%>
                </td>
<%
                 if ( bUseTaxValue )
                {
                  sortString = sortString + ",\"Number\"";
%>
                <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage())%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                }
%>
<%
    if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
                {
                  sortString = sortString + ",\"Number\"";
%>
                <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                }
%>
<%
                if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%= Languages.getString("jsp.admin.reports.adjustment",SessionData.getLanguage()).toUpperCase() %>
                  </td>
<%
                }
                if (isIntlAndHasDataPromoPermission) {
%>
                    <td class="rowhead2" valign="bottom" nowrap><%=Languages.getString("jsp.reports.reports.transactions.form.table.column.name.promoData", SessionData.getLanguage()).toUpperCase()%></td>
<%
                }
%>
              </tr>
            </thead>
<%
            double   dblTotalSalesSum         = 0;
			double dblTotalBonus = 0;
			double dblTotalRecharge = 0;
            double   dblMerchantCommissionSum = 0;
            double   dblRepCommissionSum      = 0;
            double   dblSubAgentCommissionSum = 0;
            double   dblAgentCommissionSum    = 0;
            double   dblISOCommissionSum      = 0;
            double   dblVATTotalSalesSum         = 0;
            double   dblVATMerchantCommissionSum = 0;
            double   dblVATRepCommissionSum      = 0;
            double   dblVATSubAgentCommissionSum = 0;
            double   dblVATAgentCommissionSum    = 0;
            double   dblVATISOCommissionSum      = 0;
            double   dblAdjAmountSum          = 0;
            int      intTotalQtySum           = 0;
             double dblTotalTaxAmountSum=0;
             
                double dblBonus = 0;
                double dblTotalRe = 0;
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;

            if (it.hasNext())
            {
              Vector vecTemp = null;

              vecTemp = (Vector)it.next();
              dblTotalSalesSum = Double.parseDouble(vecTemp.get(1).toString());
              dblMerchantCommissionSum = Double.parseDouble(vecTemp.get(2).toString());
              dblRepCommissionSum = Double.parseDouble(vecTemp.get(3).toString());
              dblSubAgentCommissionSum = Double.parseDouble(vecTemp.get(4).toString());
              dblAgentCommissionSum = Double.parseDouble(vecTemp.get(5).toString());
              dblISOCommissionSum = Double.parseDouble(vecTemp.get(6).toString());
              intTotalQtySum = Integer.parseInt(vecTemp.get(0).toString());
              dblAdjAmountSum = Double.parseDouble(vecTemp.get(7).toString());
              dblTotalBonus = Double.parseDouble(vecTemp.get(14).toString());
              dblTotalRecharge = Double.parseDouble(vecTemp.get(15).toString());
               if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
              {
                dblVATTotalSalesSum = Double.parseDouble(vecTemp.get(8).toString());
                dblVATMerchantCommissionSum = Double.parseDouble(vecTemp.get(9).toString());
                dblVATRepCommissionSum = Double.parseDouble(vecTemp.get(10).toString());
                dblVATSubAgentCommissionSum = Double.parseDouble(vecTemp.get(11).toString());
                dblVATAgentCommissionSum = Double.parseDouble(vecTemp.get(12).toString());
                dblVATISOCommissionSum = Double.parseDouble(vecTemp.get(13).toString());
                 if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                    {
                      dblTotalTaxAmountSum=dblTotalSalesSum-dblVATTotalSalesSum;
                    }
              }
              
                				
              while (it.hasNext())
              {
                vecTemp = null;
                vecTemp = (Vector)it.next();

                int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
                double dblTotalSales         = Double.parseDouble(vecTemp.get(3).toString());
                double dblMerchantCommission = Double.parseDouble(vecTemp.get(4).toString());
                double dblRepCommission      = Double.parseDouble(vecTemp.get(5).toString());
                double dblSubAgentCommission = Double.parseDouble(vecTemp.get(6).toString());
                double dblAgentCommission    = Double.parseDouble(vecTemp.get(7).toString());
                double dblISOCommission      = Double.parseDouble(vecTemp.get(8).toString());
                double dblAdjAmount          = Double.parseDouble(vecTemp.get(12).toString());
               					 
                  dblBonus    = Double.parseDouble(vecTemp.get(25).toString());
                  dblTotalRe = Double.parseDouble(vecTemp.get(26).toString());
                  
                double dblVATTotalSales         = 0;
                double dblVATMerchantCommission = 0;
                double dblVATRepCommission      = 0;
                double dblVATSubAgentCommission = 0;
                double dblVATAgentCommission    = 0;
                double dblVATISOCommission      = 0;
                double dblTaxAmount=0;

               if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
                  dblVATTotalSales = Double.parseDouble(vecTemp.get(19).toString());
                  dblVATMerchantCommission = Double.parseDouble(vecTemp.get(20).toString());
                  dblVATRepCommission = Double.parseDouble(vecTemp.get(21).toString());
                  dblVATSubAgentCommission = Double.parseDouble(vecTemp.get(22).toString());
                  dblVATAgentCommission = Double.parseDouble(vecTemp.get(23).toString());
                  dblVATISOCommission = Double.parseDouble(vecTemp.get(24).toString());
                  
                  if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                    {
                      dblTaxAmount=dblTotalSales-dblVATTotalSales;
                    }
                  
                  out.print("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + "<td nowrap>" + vecTemp.get(
                        17) + "</td>" + "<td>" + vecTemp.get(18) + "</td>" + "<td>" + intTotalQty + "</td>" + 
                        "<td align=right><a href=\"admin/reports/transactions/transaction_mercsumbytrm_detail.jsp?startDate=" + URLEncoder.encode(
                        TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), 
                        "UTF-8") + "&search=y&millennium_no=" + vecTemp.get(18) + "&merchantId=" + vecTemp.get(1) + "&report=y&chkUseTaxValue=" + request.getParameter("chkUseTaxValue") + "\" target=\"_blank\">" +
                        NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
                  if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                  DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
                  {
                	  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>" 
                	  				+ "<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTotalRe)) + "</td>");
                  }
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATTotalSales)) + "</td>");
                        
                        
                       if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                        {
                        out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAmount)) + "</td>");
                        }
                }
                else
                {
                out.print("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + "<td nowrap>" + vecTemp.get(
                        17) + "</td>" + "<td>" + vecTemp.get(18) + "</td>" + "<td>" + intTotalQty + "</td>" + 
                        "<td align=right><a href=\"admin/reports/transactions/transaction_mercsumbytrm_detail.jsp?startDate=" + URLEncoder.encode(
                        TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), 
                        "UTF-8") + "&search=y&millennium_no=" + vecTemp.get(18) + "&merchantId=" + vecTemp.get(1) + "&report=y\" target=\"_blank\">" +
                        NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
                        if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                  DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
                  {
                	  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>" 
                	  				+ "<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTotalRe)) + "</td>");
                  }
                }

                if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommission)) + "</td>");
                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATISOCommission)) + "</td>");
                  }
                }

                if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommission)) + "</td>");
                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATAgentCommission)) + "</td>");
                  }
                }

                if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommission)) + 
                          "</td>");
                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATSubAgentCommission)) + "</td>");
                  }
                }

                if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommission)) + "</td>");
                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) 
                 {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATRepCommission)) + "</td>");
                  }
                }

                out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommission)) + "</td>");

                if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATMerchantCommission)) + "</td>");
                }


                if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmount)) + "</td>");
                }
                
                if (isIntlAndHasDataPromoPermission) {
                    try {
                        out.println("<td align=right>" + vecTemp.get(28) + "</td>");
                    } catch (java.lang.ArrayIndexOutOfBoundsException arrayError) {}
                }

                out.println("</tr>");

                if (intEvenOdd == 1)
                {
                  intEvenOdd = 2;
                }
                else
                {
                  intEvenOdd = 1;
                }
              }
            }
%>
            <tfoot>
              <tr class=row<%= intEvenOdd %>>
                <td colspan=3 align=right>
                  <%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                </td>
                <td align=left>
                  <%= intTotalQtySum %>
                </td>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %>
                </td>
                <% if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
                { %>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalBonus )) %>
                </td>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalRecharge )) %>
                </td>
<%				}
                if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
%>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblVATTotalSalesSum)) %>
                </td>
<%
                }
                 if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                  {
%>
				<td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalTaxAmountSum)) %>
                </td>
                
                
                
<%
                }


                if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommissionSum)) + "</td>");
                   if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATISOCommissionSum)) + "</td>");
                  }
                }

                if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommissionSum)) + 
                          "</td>");
                   if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATAgentCommissionSum)) + "</td>");
                  }
                }

                if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommissionSum)) + 
                          "</td>");
                   if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATSubAgentCommissionSum)) + "</td>");
                  }
                }

                if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommissionSum)) + "</td>")
                          ;
                   if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATRepCommissionSum)) + "</td>");
                  }
                }
%>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblMerchantCommissionSum)) %>
                </td>
<%
                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATMerchantCommissionSum)) + "</td>");
                }
                if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmountSum)) + "</td>");
                }
%>
              </tr>
            </tfoot>
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  [<%= sortString %>],0,false,false);
  -->
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>

  