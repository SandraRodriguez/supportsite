<%@ page import="java.util.*,
         java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 4;
    int section_page = 15;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%    Vector warningSearchResults = new Vector();
    Vector vecSearchResults = new Vector();
    Hashtable searchErrors = null;
    int intRecordCount = 0;
    int intPage = 1;
    int intPageSize = 50;
    int intPageCount = 1;
    String sortString = "";
    String message = "";

    //this flag means if the user is in International Default
    boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

    boolean showAccountIdQRCode = false;
    showAccountIdQRCode = SessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);

    boolean viewReferenceCard = false;
    if (isInternationalDefault) {
        viewReferenceCard = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
    }

    if (request.getParameter("Download") != null) {
        String sURL = "";

        if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
            sURL = TransactionReport.downloadCityDetails(application, SessionData, true);
        }
        else {
            sURL = TransactionReport.downloadCityDetails(application, SessionData, false);
        }
        response.sendRedirect(sURL);
        return;
    }

    if (request.getParameter("search") != null) {
        if (request.getParameter("page") != null) {
            try {
                intPage = Integer.parseInt(request.getParameter("page"));
            } catch (NumberFormatException ex) {
                intPage = 1;
            }
        }

        if (intPage < 1) {
            intPage = 1;
        }

        if (TransactionReport.validateDateRange(SessionData)) {
            SessionData.setProperty("start_date", request.getParameter("startDate"));
            SessionData.setProperty("end_date", request.getParameter("endDate"));

            if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                SessionData.setProperty("deploymentType", DebisysConfigListener.getDeploymentType(application));
            }

            if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                SessionData.setProperty("customConfigType", DebisysConfigListener.getCustomConfigType(application));
            }

            if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                vecSearchResults = TransactionReport.getCityTransactionsMx(intPage, intPageSize, SessionData, application, true);
            }
            else {
                vecSearchResults = TransactionReport.getCityTransactions(intPage, intPageSize, SessionData, application, true);
            }

            intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
            vecSearchResults.removeElementAt(0);

            if (intRecordCount > 0) {
                intPageCount = (intRecordCount / intPageSize) + 1;
                if ((intPageCount * intPageSize) + 1 >= intRecordCount) {
                    intPageCount++;
                }
            }
        }
        else {
            searchErrors = TransactionReport.getErrors();
        }
    }
%>
<%@ include file="/includes/header.jsp" %>
<%
    boolean bool_custom_config_mexico = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
    boolean bool_custom_config_default = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

    boolean bool_deploy_type_intl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
    boolean bool_perm_enable_tax_calc = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);

    if (vecSearchResults != null && vecSearchResults.size() > 0) {
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
    <%
        }
    %>
<SCRIPT>
    function openViewPIN(sTransId)
    {
        var sURL = "/support/admin/transactions/view_pin.jsp?trans_id=" + sTransId;
        var sOptions = "left=" + (screen.width - (screen.width * 0.4)) / 2 + ",top=" + (screen.height - (screen.height * 0.3)) / 2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
        var w = window.open(sURL, "ViewPIN", sOptions, true);
        w.focus();
    }
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.title47", SessionData.getLanguage()).toUpperCase()%></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <%
                if (searchErrors != null) {
                    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");
                    Enumeration enum1 = searchErrors.keys();

                    while (enum1.hasMoreElements()) {
                        String strKey = enum1.nextElement().toString();
                        String strError = (String) searchErrors.get(strKey);
                        out.println("<li>" + strError);
                    }

                    out.println("</font></td></tr></table>");
                }

                if (vecSearchResults != null && vecSearchResults.size() > 0) {

                    String baseURL = "admin/reports/transactions/city_transactions.jsp?search="
                            + URLEncoder.encode(request.getParameter("search"), "UTF-8")
                            + "&cityId=" + TransactionReport.getCityId()
                            + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")
                            + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8");
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <%
                    Vector vTimeZoneData = null;

                    if (SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT)) {
                        vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
                    }
                    else {
                        vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
                    }
                %>
                <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
                <tr>
                    <td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found", SessionData.getLanguage()) + " "%>
                        <%
                            if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                                out.println(Languages.getString("jsp.admin.from", SessionData.getLanguage()) + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                            }
                        %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{Integer.toString(intPage), Integer.toString(intPageCount - 1)}, SessionData.getLanguage())%>
                    </td>
                </tr>
                <tr>
                    <td class="main">
                        <%=Languages.getString("jsp.admin.iso_name", SessionData.getLanguage())%>:&nbsp;<%=SessionData.getUser().getCompanyName()%>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <table>
                            <tr>
                                <td>
                                    <FORM ACTION="admin/reports/transactions/city_transactions.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                                        <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                                        <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                                        <INPUT TYPE=hidden NAME=search VALUE="<%=request.getParameter("search")%>">
                                        <INPUT TYPE=hidden NAME=cityId VALUE="<%=request.getParameter("cityId")%>">
                                        <INPUT TYPE=hidden NAME=Download VALUE="Y">
                                        <INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download", SessionData.getLanguage())%>">
                                    </FORM>
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align=right class="main" nowrap>
                        <%
                            if (intPage > 1) {
                                out.println("<a href=\"" + baseURL + "&page=1\">" + Languages.getString("jsp.admin.first", SessionData.getLanguage()) + "</a>&nbsp;");
                                out.println("<a href=\"" + baseURL + "&page=" + (intPage - 1) + "\">&lt;&lt;" + Languages.getString("jsp.admin.previous", SessionData.getLanguage()) + "</a>&nbsp;");
                            }

                            int intLowerLimit = intPage - 12;
                            int intUpperLimit = intPage + 12;

                            if (intLowerLimit < 1) {
                                intLowerLimit = 1;
                                intUpperLimit = 25;
                            }

                            for (int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++) {
                                if (i == intPage) {
                                    out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                                }
                                else {
                                    out.println("<a href=\"" + baseURL + "&page=" + i + "\">" + i + "</a>&nbsp;");
                                }
                            }

                            if (intPage < (intPageCount - 1)) {
                                out.println("<a href=\"" + baseURL + "&page=" + (intPage + 1) + "\">" + Languages.getString("jsp.admin.next", SessionData.getLanguage()) + "&gt;&gt;</a>&nbsp;");
                                out.println("<a href=\"" + baseURL + "&page=" + (intPageCount - 1) + "\">" + Languages.getString("jsp.admin.last", SessionData.getLanguage()) + "</a>");
                            }
                        %>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
                <thead>
                    <%
                        sortString = "\"None\", \"Number\", \"Number\", \"CaseInsensitiveString\", \"Number\", \"Date\", ";
                    %>
                    <tr class="SectionTopBorder">
                        <td class=rowhead2 nowrap>#</td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.tran_no", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.term_no", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.dba", SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.merchant_id", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.date", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                sortString += "\"CaseInsensitiveString\", ";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.phys_state", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            }

                            sortString += "\"CaseInsensitiveString\", \"CaseInsensitiveString\", \"CaseInsensitiveString\", \"Number\", \"Number\", \"Number\", ";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.city", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.county", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.reference", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.ref_no", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.recharge", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/

                            if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && isInternationalDefault) {
                                if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                    if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                        %>               	  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%               	 	}
                        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                	 }
                        }
                        else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                        %>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                        %>		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                 	}
                        else if (strAccessLevel.equals(DebisysConstants.REP)) {
                        %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                        else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                        %>                    <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                        %>


                        <%
                            }
                        }
                        else if (isInternationalDefault) {
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                        %>               	  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%               	 	}
                        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                	 }
                        }
                        else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                        %>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                        %>		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                 	}
                        else if (strAccessLevel.equals(DebisysConstants.REP)) {
                        %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                        else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                        %>                    <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                        %>


                        <%
                            }
                        }
                        else {
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                        %>               	  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%               	 	}
                        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                	 }
                        }
                        else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                        %>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                        %>		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                 	}
                        else if (strAccessLevel.equals(DebisysConstants.REP)) {
                        %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                        else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                        %>                    <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                        <%                  }
                                }
                            }

                            /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                        %>

                        <%    //DBSY-905
                            if (isInternationalDefault) {
                                sortString += "\"Number\", \"Number\", ";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.bonus", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.total_recharge", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                                sortString += "\"Number\", \"Number\", \"Number\", \"Number\", ";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.netAmount", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxAmount", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxpercentage", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxtype", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                                }
                            }

                            sortString += "\"Number\", \"Number\", \"CaseInsensitiveString\", \"Number\", ";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.balance", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.product", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.description", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.control_no", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
                                    && strAccessLevel.equals(DebisysConstants.ISO)
                                    && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                sortString += "\"Number\", ";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.pin_number", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            }

                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
                                    && strAccessLevel.equals(DebisysConstants.ISO)
                                    && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                sortString += "\"Date\", ";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.ach_date", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            }

                            if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                sortString += "\"Number\", ";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.authorization_number", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            }

                            sortString += "\"Number\"";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.transaction_type", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application) && isInternationalDefault) {
                                sortString += ",\"Number\"";
                        %>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.invoiceno", SessionData.getLanguage()).toUpperCase()%></td>
                        <%
                            }
                        %>
                        <%if (viewReferenceCard) {%>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.DTU1204", SessionData.getLanguage()).toUpperCase()%></td>
                        <%}%>
                        <%if (showAccountIdQRCode) {%>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.reference", SessionData.getLanguage()).toUpperCase()%></td>
                        <%}%>
                    </tr>
                </thead>
                <%
                    int intCounter = 1;
                    String type = "";
                    Iterator it = vecSearchResults.iterator();
                    int intEvenOdd = 1;

                    while (it.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        type = (String) vecTemp.get(13);
                        String sViewPIN = "", sPhysState = "", sAuthorizationNo = "";

                        if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)) {
                            //if the trans_type=2 which is a pin sale
                            String strCurrencySymbol = NumberUtil.getCurrencySymbol();

                            if (vecTemp.get(13).toString().equals("2") && Double.parseDouble(vecTemp.get(9).toString().replaceAll(strCurrencySymbol, "")) > 0) {
                                sViewPIN = "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + vecTemp.get(0).toString() + "');\"><span class=\"showLink\">" + vecTemp.get(12).toString() + "</span></a>";
                            }
                            else {
                                sViewPIN = vecTemp.get(12).toString();
                            }
                        }
                        else {
                            sViewPIN = vecTemp.get(12).toString();
                        }

                        if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                            sPhysState = "<td nowrap>" + vecTemp.get(15).toString() + "</td>";
                        }
                        else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                            sAuthorizationNo = "<td nowrap>" + vecTemp.get(14).toString() + "</td>";
                        }

                        String city = "";
                        String county = "";

                        if (vecTemp.get(5) != null) {
                            String[] cityCounty = vecTemp.get(5).toString().split("\\,");

                            if (cityCounty.length == 1) {
                                city = cityCounty[0];
                            }
                            else if (cityCounty.length == 2) {
                                city = cityCounty[0];
                                county = cityCounty[1];
                            }
                        }

                        String[] productDescription = vecTemp.get(11).toString().split("\\/");

                        out.println("<tr class=row" + intEvenOdd + ">"
                                + "<td>" + intCounter++ + "</td>"
                                + "<td>" + vecTemp.get(0) + "</td>"
                                + "<td>" + vecTemp.get(1) + "</td>"
                                + "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString()) + "</td>"
                                + "<td>" + vecTemp.get(3) + "</td>"
                                + "<td nowrap>" + vecTemp.get(4) + "</td>"
                                + sPhysState
                                + "<td nowrap>" + city + "</td>"
                                + "<td nowrap>" + county + "</td>"
                                + //clerk
                                "<td nowrap>" + vecTemp.get(6) + "</td>"
                                + "<td>" + vecTemp.get(7) + "</td>"
                                + "<td>" + vecTemp.get(8) + "</td>"
                                + //DBSY-905 Tax Calculation
                                //amount
                                "<td nowrap>" + vecTemp.get(9) + "</td>");

                        /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                        if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && isInternationalDefault) {
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(29).toString()) + "</td>"); //iso
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //rep
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //merchant
                                    }
                                    else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(29).toString()) + "</td>"); //iso
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(27).toString()) + "</td>"); //agente
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(28).toString()) + "</td>"); //subagente
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //rep
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //merchant
                                    }
                                }
                                else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(27).toString()) + "</td>"); //agente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(28).toString()) + "</td>"); //subagente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(28).toString()) + "</td>"); //subagente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //merchant
                                }
                            }
                        }
                        else if (isInternationalDefault) {
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //iso
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>"); //rep
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //merchant
                                    }
                                    else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //iso
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //agente
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //subagente
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>"); //rep
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //merchant
                                    }
                                }
                                else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //agente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //subagente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //subagente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //merchant
                                }
                            }
                        }
                        else if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //iso
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>"); //rep
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>"); //merchant
                                    }
                                    else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //iso
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //agente
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>"); //subagente
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>"); //rep
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>"); //merchant
                                    }
                                }
                                else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //agente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>"); //subagente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>"); //subagente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>"); //merchant
                                }
                            }
                        }
                        else {
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //iso
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(18).toString()) + "</td>"); //rep
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(17).toString()) + "</td>"); //merchant
                                    }
                                    else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>"); //iso
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>"); //agente
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>"); //subagente
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(18).toString()) + "</td>"); //rep
                                        out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(17).toString()) + "</td>"); //merchant
                                    }
                                }
                                else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>"); //agente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>"); //subagente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(18).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(17).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>"); //subagente
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(18).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(17).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(18).toString()) + "</td>"); //rep
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(17).toString()) + "</td>"); //merchant
                                }
                                else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(17).toString()) + "</td>"); //merchant
                                }
                            }
                        }
                        /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/

                        if (isInternationalDefault) {
                            //bonus
                            out.println("<td nowrap>" + vecTemp.get(19) + "</td>"
                                    + //total Recharge
                                    "<td nowrap>" + vecTemp.get(20) + "</td>");

                            if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                                //net amount
                                out.println("<td nowrap>" + vecTemp.get(21) + "</td>"
                                        + //tax amount
                                        "<td nowrap>" + vecTemp.get(22) + "</td>"
                                        + // tax percentage
                                        "<td nowrap>" + vecTemp.get(23) + "%</td>"
                                        + // tax type
                                        "<td nowrap>" + vecTemp.get(24) + "</td>");
                            }
                        }

                        //bal
                        out.println("<td nowrap>" + vecTemp.get(10) + "</td>"
                                + //product
                                "<td nowrap>" + productDescription[0] + "</td>"
                                + //description
                                "<td nowrap>" + productDescription[1] + "</td>"
                                + "<td>" + sViewPIN + "</td>");

                        if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
                                && strAccessLevel.equals(DebisysConstants.ISO)
                                && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                            out.println("<td>" + vecTemp.get(16) + "</td>");
                        }

                        if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
                                && strAccessLevel.equals(DebisysConstants.ISO)
                                && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                            out.println("<td>" + vecTemp.get(17) + "</td>");
                        }

                        out.println(sAuthorizationNo
                                + "<td>" + type + "</td>");

                        if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application) && isInternationalDefault) {
                            //DBSY-919 Invoice Number Field
                            out.println("<td>" + vecTemp.get(18) + "</td>");
                        }

                        if (viewReferenceCard) {
                            out.println("<td>" + vecTemp.get(30) + "</td>");
                        }

                        if (showAccountIdQRCode) {
                            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {

                                }
                                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                    if (vecTemp.size() == 32 && showAccountIdQRCode) {
                                        out.println("<td>" + vecTemp.get(31) + "</td>");
                                    }
                                    else if (showAccountIdQRCode) {
                                        out.println("<td></td>");
                                    }
                                }

                            }
                            else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                if (vecTemp.size() == 28 && showAccountIdQRCode) {
                                    out.println("<td>" + vecTemp.get(27) + "</td>");
                                }
                                else if (showAccountIdQRCode) {
                                    out.println("<td></td>");
                                }
                            }
                            else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                if (vecTemp.size() == 28 && showAccountIdQRCode) {
                                    out.println("<td>" + vecTemp.get(27) + "</td>");
                                }
                                else if (showAccountIdQRCode) {
                                    out.println("<td></td>");
                                }
                            }
                            else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                if (vecTemp.size() == 28 && showAccountIdQRCode) {
                                    out.println("<td>" + vecTemp.get(27) + "</td>");
                                }
                                else if (showAccountIdQRCode) {
                                    out.println("<td></td>");
                                }
                            }
                            else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                if (vecTemp.size() == 32 && showAccountIdQRCode) {
                                    out.println("<td>" + vecTemp.get(31) + "</td>");
                                }
                                else if (showAccountIdQRCode) {
                                    out.println("<td></td>");
                                }
                            }
                        }

                        out.println("</tr>");

                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        }
                        else {
                            intEvenOdd = 1;
                        }
                    }

                    vecSearchResults.clear();
                %>
            </table>
            <%
                }
                else if (intRecordCount == 0 && request.getParameter("search") != null && searchErrors == null) {
                    out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.reports.transactions.cities_transactions.error", SessionData.getLanguage()) + "</font>");
                }

                if (vecSearchResults != null) {
            %>
            <SCRIPT type="text/javascript">
                <!--
              var stT1 = new SortROC(document.getElementById("t1"),
                        [<%=sortString%>], 0, false, false);
-->

            </SCRIPT>
            <%
                }
            %>
        </td>
    </tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>