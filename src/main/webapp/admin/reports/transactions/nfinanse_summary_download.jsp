<%@page import="java.net.URLEncoder, com.debisys.utils.HTMLEncoder, java.util.*, java.io.*, javax.servlet.*, java.text.*"%>
<%@page import="com.debisys.users.User, com.debisys.reports.TransactionReport"%>
<%
int section=4;
int section_page=43;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="NFinanseReport" class="com.debisys.reports.NFinanseReport" scope="request" />
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page" />
<jsp:setProperty name="NFinanseReport" property="*" />
<%@include file="/includes/security.jsp"%>
<%
int ALL = -1;
int intRecordCount = 0;
Hashtable searchErrors = null;
try{
  	if (NFinanseReport.validateDateRange(SessionData)){
  		SessionData.setProperty("start_date", request.getParameter("startDate"));
		SessionData.setProperty("end_date", request.getParameter("endDate"));
	  	SessionData.setProperty("sort", request.getParameter("sort"));
		SessionData.setProperty("col", request.getParameter("col"));
		NFinanseReport.setCategoryIds((String[])request.getParameterValues("categoryIds"));
		NFinanseReport.setMerchantIds((String[])request.getParameterValues("merchantIds"));
		StringBuilder results = NFinanseReport.getTransactionsCSV(SessionData);		
		response.setContentType("application/text");
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		Date date = new Date();
		response.addHeader("Content-Disposition", "attachment; filename=nFinanse_summary"+ dateFormat.format(date) +".csv");
		PrintWriter pw = response.getWriter();
		pw.write(results.toString());
		pw.flush();
		pw.close();
	}else{
		searchErrors = NFinanseReport.getErrors();
	}
}catch(Exception ex){
	searchErrors = new Hashtable();
	searchErrors.put("Report Error", "the report throwed an exception: " + ex.getMessage());
}
%>