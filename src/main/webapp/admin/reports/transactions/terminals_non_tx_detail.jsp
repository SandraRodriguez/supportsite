<%@ page import="java.net.URLEncoder,com.debisys.utils.*,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,java.text.SimpleDateFormat,
                 com.debisys.utils.NumberUtil,
                 com.debisys.schedulereports.ScheduleReport,
                 com.debisys.utils.ColumnReport" %>
<%
  int section      = 4;
  int section_page = 27;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";
  String startdate           = "";
  java.lang.StringBuffer strIds= new java.lang.StringBuffer(2000);
  String days ="-5";
  java.lang.StringBuffer strRepsIds= new java.lang.StringBuffer(2000);  
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();
  String keyLanguage = "jsp.admin.reports.terminals_non_tx_title";
  
  String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
  
  if (request.getParameter("search") != null)
  {
	    startdate=request.getParameter("startDate");
	    String[] pids = request.getParameterValues("pids");
	    int longi=pids.length;
	    for(int i=0; i <= longi - 1 ; i++)
	    {                       
	        if ( longi == (i+1) )
	        {
	         strIds.append(pids[i]);
	        }               
	        else
	        {
	          strIds.append(pids[i]+",");
	        }               
	    }
	    if (request.getParameter("repsids") != null)
	    {      
	      String[] repsids = request.getParameterValues("repsids");
	      longi=repsids.length;
	      for(int i=0; i <= longi - 1 ; i++)
	      {                       
	            if ( longi == (i+1) )
	            {
	             strRepsIds.append(repsids[i]);
	            }               
	            else if (repsids[i].length() > 0)
	            {
	              strRepsIds.append(repsids[i]+",");
	            }               
	      }      
	   }
       //////////////////////////////////////////////////////////////////
	   //HERE WE DEFINE THE REPORT'S HEADERS 
	   headers = getHeadersTerminalsNon_TrxReport( SessionData, application );
	   //////////////////////////////////////////////////////////////////
	   //////////////////////////////////////////////////////////////////
	   if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	   {
	   		titles.add(titleReport);
			TransactionReport.getTerminalsNonTX(SessionData,days,startdate,strRepsIds.toString(),strIds.toString(), DebisysConstants.SCHEDULE_REPORT, headers, titles);
			ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
			if (  scheduleReport != null  )
			{
				scheduleReport.setTitleName( keyLanguage );   
			}	
			response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );		
	   }
	   else
	   {
			vecSearchResults = TransactionReport.getTerminalsNonTX(SessionData,days,startdate,strRepsIds.toString(),strIds.toString(), DebisysConstants.EXECUTE_REPORT, headers, titles);
	   } 		
    
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>

	<%!public static ArrayList<ColumnReport> getHeadersTerminalsNon_TrxReport(SessionData sessionData, ServletContext application )
	{
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add(new ColumnReport("rep", Languages.getString("jsp.admin.reports.terminals_non_tx_rep", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.terminals_non_tx_dba", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("contact", Languages.getString("jsp.admin.reports.terminals_non_tx_contact", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("contact_phone", Languages.getString("jsp.admin.reports.terminals_non_tx_phone", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("last_tx", Languages.getString("jsp.admin.reports.terminals_non_tx_date_last_tx", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("type_credit", Languages.getString("jsp.admin.reports.terminals_non_tx_type_credit", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("description", Languages.getString("jsp.admin.reports.terminals_non_tx_type_terminal", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("millennium_no", Languages.getString("jsp.admin.reports.terminals_non_tx_type_terminal_id", sessionData.getLanguage()).toUpperCase(), Integer.class, false));
			
		return headers;
	}  
  %>
  
  <table border="0" cellpadding="0" cellspacing="0" width="420">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
        &nbsp;
        <%= titleReport.toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%      if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
          Enumeration enum1 = searchErrors.keys();
          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);
            out.println("<li>" + strError);
          }
          out.println("</font></td></tr></table>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        { %>
                  <%
                   java.util.Calendar cale= java.util.Calendar.getInstance();
                   cale.setTime(new java.util.Date(startdate));
                   cale.add(cale.DATE,Integer.parseInt(days));                   
                  %>
                  
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td class="main">
                <%=Languages.getString("jsp.admin.reports.terminals_non_tx_type_title_date",SessionData.getLanguage())%> <%=DateUtil.formatDate(cale.getTime())%> - <%=DateUtil.formatDateTime(startdate)%>
              </td>
			</tr>
			<tr>              
              <td class=main align=left valign=bottom><%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %></td>
            </tr>
             <tr>
              <td class="main">
                  <form method="post" action="admin/reports/transactions/terminals_non_tx_export.jsp">
                      <input type="submit" value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                      <input type="hidden" value="<%=startdate%>" name="startdate"> 
                      <input type="hidden" value="<%=strIds.toString()%>" name="strIds"> 
                      <input type="hidden" value="<%=days%>" name="days">
                      <input type="hidden" value="<%=strRepsIds.toString()%>" name="strRepsIds">
                  </form>                   
              </td>             
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
           
            <thead>
               
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>                 
                <td class=rowhead2 align="center">
                  <%= Languages.getString("jsp.admin.reports.terminals_non_tx_rep",SessionData.getLanguage()).toUpperCase() %> <br>
                </td>
                <td class=rowhead2 align="center">
                  <%= Languages.getString("jsp.admin.reports.terminals_non_tx_dba",SessionData.getLanguage()).toUpperCase() %> <br>
                </td>                                
                <td class=rowhead2 align="center">
                  <%= Languages.getString("jsp.admin.reports.terminals_non_tx_contact",SessionData.getLanguage()).toUpperCase() %> <br>
                </td>                
                <td class=rowhead2 align="center">
                  <%= Languages.getString("jsp.admin.reports.terminals_non_tx_phone",SessionData.getLanguage()).toUpperCase() %> <br>
                </td>
                <td class=rowhead2 align="center">
                  <%= Languages.getString("jsp.admin.reports.terminals_non_tx_date_last_tx",SessionData.getLanguage()).toUpperCase() %> <br>
                </td>
                <td class=rowhead2 align="center">
                  <%= Languages.getString("jsp.admin.reports.terminals_non_tx_type_credit",SessionData.getLanguage()).toUpperCase() %> <br>
                </td>
                <td class=rowhead2 align="center" >
                  <%= Languages.getString("jsp.admin.reports.terminals_non_tx_type_terminal",SessionData.getLanguage()).toUpperCase() %> <br>
                </td> 
                <td class=rowhead2 align="center" >
                  <%= Languages.getString("jsp.admin.reports.terminals_non_tx_type_terminal_id",SessionData.getLanguage()).toUpperCase() %> <br>
                </td>
              </tr>
            </thead>            
<%
        try
        {
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;                        
            while (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector)it.next();
              java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
              formatter.setLenient(false);
              
              // LOCALIZATION - SW
              String date_last="";
              if ( vecTemp.get(4) != null)
              {
                date_last = DateUtil.formatDateTime(vecTemp.get(4).toString());
              }  
              else
              {
              	date_last="N/A";
              }  
              // END LOCALIZATION REMOVAL
                  out.print("<tr class=row" + intEvenOdd + ">" + 
                  "<td>" + intCounter++ + "</td>" + 
                  "<td nowrap>" + vecTemp.get(0) + "</td>" + 
                  "<td nowrap align=\"left\">" + vecTemp.get(1) + "</td>" + 
                  "<td nowrap align=\"left\">" + vecTemp.get(2) + "</td>" + 
                  "<td nowrap align=\"left\">" + vecTemp.get(3) + "</td>" + 
              // Date modified for localization - SW
                  "<td nowrap align=\"left\">" + date_last + "</td>" + 
                  "<td nowrap align=\"right\">"+ vecTemp.get(5) + "</td>" + 
                  "<td nowrap align=\"right\">"+ vecTemp.get(6) + "</td>" + 
                  "<td nowrap align=\"right\">"+ vecTemp.get(7) + "</td></tr>");  
                  
              if (intEvenOdd == 1)
              {
                intEvenOdd = 2;
              }
              else
              {
                intEvenOdd = 1;
              }
            }
          }
          catch(Exception ex)
          {
                out.print(ex);
          }
%> 
			
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
  				var stT1 = new SortROC(document.getElementById("t1"),["None","String", "String", "String", "String", "String", "String","String","String"],0,false,false);
          </SCRIPT>
<%
       }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
