<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport" %>
<%
    int section=4;
    int section_page=28;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
    <SCRIPT LANGUAGE="JavaScript">
var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

    function scroll()
    {
    document.mainform.submit.disabled = true;
    document.mainform.submit.value = iProcessMsg[count];
    count++
    if (count = iProcessMsg.length) count = 0
    setTimeout('scroll()', 150);
    }

    function scroll2()
    {
    document.downloadform.submit.disabled = true;  
    document.downloadform.submit.value = iProcessMsg[count];
    count++
    if (count = iProcessMsg.length) count = 0
    setTimeout('scroll2()', 150);
    }

</SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.write_off_request_report",SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form name="mainform" method="post" action="admin/reports/transactions/write_off_request_summary.jsp" onSubmit="scroll();">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">                                
                                <tr>
                                    <td class="formArea2">
                                        <table width="300">
                                            <tr class="main">
			                                    <td valign="top" align="left" nowrap>
			                                    <b><%=Languages.getString("jsp.admin.choose_query_params",SessionData.getLanguage())%></b>
			                                    </td>
			                                </tr>
                                            <tr class="main">
                                                <td nowrap valign="top">&nbsp;<%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
		         <%}%><td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" nowrap>
                                                    <table width=400>
                                                        <tr class="main">
                                                            <td nowrap>&nbsp;<%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td><td><input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                                        </tr>
                                                        <tr class="main">
                                                            <td nowrap>&nbsp;<%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td> <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                                        </tr>
                                                        <%
                                                            //String sStyle = "display:none;";
                                                            if (strAccessLevel.equals(DebisysConstants.ISO))
                                                            {%>                                                            
                                                                <tr class="main">
                                                                    <td class=main valign=top nowrap>&nbsp;<%=Languages.getString("jsp.admin.reports.write_off_request_report.merchants",SessionData.getLanguage())%></td>
                                                                    <td class=main valign=top>
                                                                        <select name="merchantList">
                                                                            <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
                                                                            <%
                                                                                Iterator itM = Merchant.getMerchantListReports(SessionData).iterator();
                                                                                while (itM.hasNext())
                                                                                {
                                                                                    Vector vecTemp = (Vector)itM.next();
                                                                                    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                                                                                }
                                                                            %>
                                                                        </select>
                                                                    </td>
                                                                </tr>    
                                                        <% 
                                                          //sStyle = "display:inline;";
                                                            }
                                                        %>
                                                        <tr class="main">                                                     
                                                            <td>
                                                                <input type="hidden" name="search" value="y"><br><br>
                                                                <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.write_off_request_report.search",SessionData.getLanguage())%>">
                                                            </td>
                                                        </tr>        
                                                        <tr>
                                                            <td colspan=3 class="main"><br><br>* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%></td>
                                                        </tr>                                                
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>          
                        </form>
                    </td>
                </tr>                                
            </table>
        </td>
    </tr>  
</table>                                          
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>                        