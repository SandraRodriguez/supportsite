<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport"%>
<%@page import="com.debisys.utils.TimeZone"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />



<%
int section=4;
int section_page=18;

String strProviderId = "";
if (request.getParameter("ProviderId")!= null)
{
	strProviderId = request.getParameter("ProviderId");
}

String destination = "";
if (request.getParameter("filter")!= null && (request.getParameter("filter")).equals("merchants"))
{
	destination="admin/reports/transactions/transaction_error_summary_by_merchants.jsp";	
}
else if (request.getParameter("filter")!= null && (request.getParameter("filter")).equals("errors"))
{
	destination="admin/reports/transactions/transaction_error_summary_by_types.jsp";
}
else if (request.getParameter("filter")!= null && (request.getParameter("filter")).equals("providererrors"))
{
	destination="admin/reports/transactions/transaction_error_summary_by_provider_errors.jsp";
}


SessionData.setProperty("start_date", request.getParameter("startDate"));
SessionData.setProperty("end_date", request.getParameter("endDate"));
%>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>



<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}



function filterByErrors()
{
	window.location="/support/admin/reports/transactions/transaction_error.jsp?filter=errors&startDate=" + document.getElementById("startDate").value + "&endDate=" + document.getElementById("endDate").value;
}

function filterByMerchants()
{
	window.location="/support/admin/reports/transactions/transaction_error.jsp?filter=merchants&startDate=" + document.getElementById("startDate").value + "&endDate=" + document.getElementById("endDate").value;
}

function filterByProviderErrors()
{
	window.location="/support/admin/reports/transactions/transaction_error.jsp?filter=providererrors&startDate=" + document.getElementById("startDate").value + "&endDate=" + document.getElementById("endDate").value;
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title32",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="post" action="<%=destination%>" onSubmit="scroll();">
	    <INPUT TYPE="hidden" ID="ProviderId" NAME="ProviderId" VALUE="<%=strProviderId%>">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
<%
	Vector vTimeZoneData = null;
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
              <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
              <tr class="main">
               <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
		         <%}%><td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
<table width=400>

<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td><td><input class="plain" name="startDate" id="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>

<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td> <input class="plain" name="endDate" id="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<%

if (request.getParameter("filter")!= null && (request.getParameter("filter")).equals("merchants"))
{

%>
<tr>
    <td class=main nowrap><%=Languages.getString("jsp.admin.reports.transactions.show_errors_by",SessionData.getLanguage())%>:</td><td class=main nowrap><input type=radio name="filter" value=merchants checked><%=Languages.getString("jsp.admin.reports.transactions.errors.merchants",SessionData.getLanguage())%>&nbsp;&nbsp;<input type=radio name="filter" value=errors onclick="filterByErrors();"><%=Languages.getString("jsp.admin.reports.transactions.errors.error_types",SessionData.getLanguage())%>&nbsp;&nbsp;<input type=radio name="filter" value=providererrors onclick="filterByProviderErrors();"><%=Languages.getString("jsp.admin.reports.transactions.errors.provider_error_types",SessionData.getLanguage())%></td>
</tr>
<%
if(strAccessLevel.equals(DebisysConstants.MERCHANT)) {
	%>
<input type="hidden" value="<%=SessionData.getProperty("ref_id") %>" name="mids" />
<%
} else {
%>
<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%></td>
    <td class=main valign=top>
        <select name="mids" size="10" multiple>
          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
  Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
  Iterator it = vecMerchantList.iterator();
  while (it.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
  }

%>
        </select>
        <br>
        *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%>
</td>
</tr>
<%
	}
}
else if (request.getParameter("filter")!= null && (request.getParameter("filter")).equals("errors"))
{
%>
<tr>
    <td class=main nowrap><%=Languages.getString("jsp.admin.reports.transactions.show_errors_by",SessionData.getLanguage())%>:</td><td class=main nowrap><input type=radio name="filter" value=merchants onclick="filterByMerchants();"><%=Languages.getString("jsp.admin.reports.transactions.errors.merchants",SessionData.getLanguage())%>&nbsp;&nbsp;<input type=radio name="filter" value=errors checked><%=Languages.getString("jsp.admin.reports.transactions.errors.error_types",SessionData.getLanguage())%>&nbsp;&nbsp;<input type=radio name="filter" value=providererrors onclick="filterByProviderErrors();"><%=Languages.getString("jsp.admin.reports.transactions.errors.provider_error_types",SessionData.getLanguage())%></td><td>&nbsp;</td>
</tr>
<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.errors.option",SessionData.getLanguage())%></td>
    <td class=main valign=top>
        <select name="errors" size="10" multiple>
          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
  Vector vecErrorTypeList = TransactionReport.getErrorTypesList();
  Iterator it2 = vecErrorTypeList.iterator();
  while (it2.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it2.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
  }

%>
        </select>
        <br>
        *<%=Languages.getString("jsp.admin.reports.transactions.errors.instructions",SessionData.getLanguage())%>
</td>
</tr>
<%
}
else if (request.getParameter("filter")!= null && (request.getParameter("filter")).equals("providererrors"))
{
%>
<tr>
    <td class=main nowrap><%=Languages.getString("jsp.admin.reports.transactions.show_errors_by",SessionData.getLanguage())%>:</td><td class=main nowrap><input type=radio name="filter" value=merchants onclick="filterByMerchants();"><%=Languages.getString("jsp.admin.reports.transactions.errors.merchants",SessionData.getLanguage())%>&nbsp;&nbsp;<input type=radio name="filter" value=errors onclick="filterByErrors();"><%=Languages.getString("jsp.admin.reports.transactions.errors.error_types",SessionData.getLanguage())%>&nbsp;&nbsp;<input type=radio name="filter" value=providererrors checked><%=Languages.getString("jsp.admin.reports.transactions.errors.provider_error_types",SessionData.getLanguage())%></td><td>&nbsp;</td>
</tr>
<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.provider.option_providers",SessionData.getLanguage())%></td>
    <td class=main valign=top>
<%
	Vector vecIsoProviderMapping = TransactionReport.getISOProviderMapping(SessionData);
	if (vecIsoProviderMapping.isEmpty())
	{
%>
		<br>
		<font color=ff0000><b><%=Languages.getString("jsp.admin.reports.transactions.errors.provider_error",SessionData.getLanguage())%></b></font>
		<br><br>
<%
	}
	else
	{
%>
        <select name="ProvidersList" id="ProvidersList" onchange="getErrorsByProvider()">
<%
		  String strSelectedP = "";
		  Iterator it2 = vecIsoProviderMapping.iterator();
		  while (it2.hasNext())
		  {
		    Vector vecTemp = null;
		    vecTemp = (Vector) it2.next();
          	if(!strProviderId.equals(""))
  	    	{
  	    		if(strProviderId.equals(vecTemp.get(0)))
     	    	{
         			strSelectedP = "selected";
      		    }
          		else
	          	{
          			strSelectedP = "";
	          	}
  	    	}
          	else
          	{
          		strSelectedP = "";
          		strProviderId = vecTemp.get(0).toString();
          	}
		    out.println("<option " + strSelectedP + " value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
		  }
%>
        </select>
        <SCRIPt>
			function getErrorsByProvider()
			{
				document.getElementById('ProviderId').value = document.getElementById('ProvidersList').value;
				window.location="/support/admin/reports/transactions/transaction_error.jsp?filter=providererrors&startDate=" + document.getElementById("startDate").value + "&endDate=" + document.getElementById("endDate").value + "&ProviderId=" + document.getElementById("ProvidersList").value;
			}
        </SCRIPt>

        <br>
</td>
</tr>
		<tr class="main">
			<td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.provider.option_errors",SessionData.getLanguage())%></td>
			<td class=main valign=top>
<%
	Vector vecErrorsList = TransactionReport.getErrorsByProvider(SessionData, strProviderId);
	if (vecErrorsList.isEmpty())
	{
%>
		<br>
		<font color=ff0000><b><%=Languages.getString("jsp.admin.reports.transactions.errors.provider_error_codes",SessionData.getLanguage())%></b></font>
		<br><br>
<%
	}
	else
	{
%>
	<select name="ErrorsList" id="ErrorsList" size="10" multiple>
		<option value="-1" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
		  String strSelectedPE = "";
		  Iterator itEL = vecErrorsList.iterator();
		  while (itEL.hasNext())
		  {
				Vector vecTempEL = null;
				vecTempEL = (Vector) itEL.next();
				out.println("<option value=" + vecTempEL.get(0) +">" + vecTempEL.get(1) + "</option>");
		  }
%>
    </select>
<%
	}
%>
			</td>
		</tr>
<%
	}
%>
<%
}
%>
<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.products.option",SessionData.getLanguage())%></td>
    <td class=main valign=top>
        <select name="pids" size="10" multiple>
          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
	//Adding a filter by product to this report
  Vector vecProductList = TransactionReport.getProductList(SessionData, null);
  Iterator it = vecProductList.iterator();
  while (it.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "("+vecTemp.get(0)+")</option>");
  }

%>
        </select>
        <br>
        *<%=Languages.getString("jsp.admin.reports.transactions.products.instructions",SessionData.getLanguage())%>
</td>
</tr>


<tr>
    <td class=main align=center>
      <input type="hidden" name="search" value="y">
      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="validateSchedule(0);">
    </td>
    <jsp:include page="/admin/reports/schreportoption.jsp">
	  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
	 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
	</jsp:include>
</tr>
<tr>
<td class="main" colspan=2>
* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
</td>
</tr>
</table>
               </td>
              </tr>
              </form>
            </table>

          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>