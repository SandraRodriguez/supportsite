<%@page import="com.debisys.reports.ResourceReport"%>
<%@page import="com.debisys.reports.pojo.BasicPojo"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.debisys.reports.schedule.ResultField"%>
<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.reports.pojo.BankPojo"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.debisys.reports.schedule.PaymentRequestReport"%>
<%@ page import="com.debisys.languages.Languages,
         com.debisys.customers.Merchant,
         com.debisys.reports.TransactionReport,
         com.debisys.utils.StringUtil,
         com.debisys.utils.DateUtil,
         com.debisys.utils.NumberUtil,
         java.util.Vector,
         java.util.Iterator,
         java.util.Enumeration" %>
<%
    int section = 9;
    int section_page = 2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%    PaymentRequestReport report = null;
    ArrayList<ArrayList<ResultField>> vResults = new ArrayList<ArrayList<ResultField>>();
    String strRefId = SessionData.getProperty("ref_id");
    SessionData.setProperty("show_schedule_btn", "n");
%>    
<%@ include file="/WEB-INF/jspf/admin/reports/payments/paymentRequestSearchParamProcess.jspf" %>  
<%    if (request.getParameter("frmApply") != null) {//If a change in credit/recycle/reject was made
        try {
            report = new PaymentRequestReport(SessionData, application, startDate,
                    endDate, agentList, allAgentsSelected, subAgentList, allSubAgentsSelected,
                    repList, allRepsSelected, merchantList, allMerchantsSelected,
                    paymentStatusList, allPaymentStatusSelected, bankListParam,
                    allBanksSelected, paymentRequestId, documentId, paymentAmount,
                    siteId, scheduling);
            report.setScheduling(false);
            vResults = report.getResults();
            boolean bResultsProcessed = false;
            Enumeration enParams = request.getParameterNames();
%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b><%=Languages.getString("jsp.admin.reports.paymentrequest_search.title", SessionData.getLanguage()).toUpperCase()%></b></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <%
            int nRow = 1;
            int nAppliedId = 0;
            int nNotAppliedId = 0;

            //Retrieve IDs for Applied and NotApplied status
            Vector vecStatusList = TransactionReport.getPaymentRequestStatus();
            for (int i = 0; i < vecStatusList.size(); i++) {
                Vector vTemp = (Vector) vecStatusList.get(i);
                if (vTemp.get(1).toString().equals("APPLIED")) {
                    nAppliedId = Integer.parseInt(vTemp.get(0).toString());
                } else if (vTemp.get(1).toString().equals("NOT_APPLIED")) {
                    nNotAppliedId = Integer.parseInt(vTemp.get(0).toString());
                }
            }

            while (enParams.hasMoreElements()) {//Thru request params
                String sNextId = enParams.nextElement().toString();
                if (sNextId.startsWith("chk_")) {//If user made a change in results
                    boolean bHold = false;
                    boolean bApply = false;
                    boolean bReject = false;

                    if (!bResultsProcessed) {
        %>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
                <thead>
                    <tr class="SectionTopBorder">

                        <%
                            // Set the column headers
                            ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
                            if ((reportColumns != null) && (reportColumns.size() > 0) ) {
                                for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                                    %>                    
                                    <th class="rowhead2" nowrap><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
                                    </th>
                                    <%
                                }
                            }
                        %>
                        <th class="rowhead2" title='<%=Languages.getString("jsp.admin.reports.paymentrequest_search.hold", SessionData.getLanguage())%>'><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.hold1", SessionData.getLanguage()).toUpperCase())%></th>
                        <th class="rowhead2" title='<%=Languages.getString("jsp.admin.reports.paymentrequest_search.apply", SessionData.getLanguage())%>'><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.apply1", SessionData.getLanguage()).toUpperCase())%></th>
                        <th class="rowhead2" title='<%=Languages.getString("jsp.admin.reports.paymentrequest_search.reject", SessionData.getLanguage())%>'><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.reject1", SessionData.getLanguage()).toUpperCase())%></th>
                        
                        <th class="rowhead2" ><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.result", SessionData.getLanguage()).toUpperCase())%></td>
                    </tr>
                </thead>
                <%
                        bResultsProcessed = true;
                    }

                    int nRequestNumber = Integer.parseInt(sNextId.replaceAll("chk_", ""));
                    if (request.getParameter(sNextId).equals("h")) {
                        bHold = true;
                    } else if (request.getParameter(sNextId).equals("a")) {
                        bApply = true;
                    } else if (request.getParameter(sNextId).equals("r")) {
                        bReject = true;
                    }
                    
                %>
                <tr class="row<%=nRow%>">
                    <td align=right><%=nRequestNumber%></td>
                    <td><%=request.getParameter("sta_" + nRequestNumber)%></td>
                    <td><%=request.getParameter("reason_hidden_desc_" + nRequestNumber)%></td>
                    <td><%=SessionData.getUser().getUsername()%></td>
                    <td><%=request.getParameter("dba_" + nRequestNumber)%></td>
                    <td align=right><%=request.getParameter("mil_" + nRequestNumber)%></td> 
                    <td><%=request.getParameter("clerk_" + nRequestNumber)%></td>
                    <td align=right><%=request.getParameter("docnum_" + nRequestNumber)%></td>
                    <td><%=request.getParameter("date_" + nRequestNumber)%></td>
                    <td nowrap><%=request.getParameter("bankname_" + nRequestNumber)%></td>
                    <td nowrap><%=request.getParameter("depositTypeName_" + nRequestNumber)%></td>
                    <td align=right><%=NumberUtil.formatAmount(request.getParameter("amount_" + nRequestNumber))%></td>  
                    <td nowrap><%=request.getParameter("date_" + nRequestNumber)%></td>
                    <%
                        if (SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    %>
                    <td><%=request.getParameter("age_" + nRequestNumber)%></td>
                    <td><%=request.getParameter("sag_" + nRequestNumber)%></td>
                    <td><%=request.getParameter("rep_" + nRequestNumber)%></td>
                    
                    
                    <%
                    } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                    %>
                    <td><%=request.getParameter("sag_" + nRequestNumber)%></td>
                    <td><%=request.getParameter("rep_" + nRequestNumber)%></td>
                    <td><%=request.getParameter("sta_" + nRequestNumber)%></td>
                    <%
                    } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                    %>    
                    <td><%=request.getParameter("rep_" + nRequestNumber)%></td>
                    <td><%=request.getParameter("sta_" + nRequestNumber)%></td>
                    <%
                        }
                    } else {
                    %>    
                    <td><%=request.getParameter("rep_" + nRequestNumber)%></td>
                    <td><%=request.getParameter("sta_" + nRequestNumber)%></td>
                    <%

                        }

                    %>    

                    <td align=center></td>
                    <td align=center></td>

                    <td align=center></td>
                    <td align=center></td>
                    
                    <td align=center><%=(bHold) ? Languages.getString("jsp.admin.reports.paymentrequest_search.yes", SessionData.getLanguage()) : Languages.getString("jsp.admin.reports.paymentrequest_search.no", SessionData.getLanguage())%></td>
                    <td align=center><%=(bApply) ? Languages.getString("jsp.admin.reports.paymentrequest_search.yes", SessionData.getLanguage()) : Languages.getString("jsp.admin.reports.paymentrequest_search.no", SessionData.getLanguage())%></td>
                    <td align=center><%=(bReject) ? Languages.getString("jsp.admin.reports.paymentrequest_search.yes", SessionData.getLanguage()) : Languages.getString("jsp.admin.reports.paymentrequest_search.no", SessionData.getLanguage())%></td>                                        
                    
                    <%
                        if (bHold) {
                    %>
                    <td align=center><%=Languages.getString("jsp.admin.reports.paymentrequest_search.pending", SessionData.getLanguage())%></td>
                    <%
                    } else if (bApply) {//Else if the payment should be applied
                        Vector vErrors = new Vector();
                        try {
                            vErrors = TransactionReport.applyPaymentRequest(application,
                                    Integer.toString(nRequestNumber),
                                    request.getParameter("mil_" + nRequestNumber),
                                    Double.parseDouble(request.getParameter("amount_" + nRequestNumber)),
                                    request.getParameter("docdate_" + nRequestNumber),
                                    0/*Commission*/,
                                    1/*True - commission by value*/,
                                    Languages.getString("jsp.admin.reports.paymentrequest_search.paymentdescription", SessionData.getLanguage()).replaceAll("_req_", Integer.toString(nRequestNumber)),
                                    request.getParameter("bankname_" + nRequestNumber), //"HSBC" 
                                    request.getParameter("bankID_" + nRequestNumber),
                                    request.getParameter("docnum_" + nRequestNumber),
                                    SessionData.getProperty("username"));
                        } catch (Exception ex) {
                            vErrors.add(999);
                            vErrors.add(""/*ex.toString()*/);
                        }
                        if (vErrors.get(0).toString().equals("0")) {//If the payment was successful
%>
                    <td align=center><%=Languages.getString("jsp.admin.reports.paymentrequest_search.successful", SessionData.getLanguage())%></td>
                    <%
                    } else {//Else the payment failed
%>					

                    <td>
                        <%=Languages.getString("jsp.admin.reports.pinreturn_search.failed", SessionData.getLanguage())%><br>
                        <div style="height:50px;overflow:auto;"><ul>
                                <li>
                                    <%=Languages.getString("jsp.admin.customers.merchants_info.paymenterror", SessionData.getLanguage())%>&nbsp;
                                    (
                                    <%
                                        if (vErrors.get(0).toString().equals("3")) {
                                    %>
                                    <%=Languages.getString("jsp.admin.customers.merchants_info.paymentAlreadyProcessed", SessionData.getLanguage())%>
                                    <%
                                    } else {
                                    %>
                                    <%=vErrors.get(0)%>
                                    <%
                                        }
                                    %>
                                    )
                                </li>
                            </ul></div>						
                    </td>
                    <%
                        }
                    }//End of else if the payment should be applied
                    else if (bReject) {//Else if the payment should be rejected
                        String reasonId = request.getParameter("reason_hidden_"+nRequestNumber);
                        if (DbUtil.updatePaymentRequestStatus(application, nRequestNumber, nNotAppliedId, (String) SessionData.getProperty("username"), reasonId)) {
                    %>
                    <td align=center><%=Languages.getString("jsp.admin.reports.paymentrequest_search.successful", SessionData.getLanguage())%></td>
                    <%
                    } else {
                    %>
                    <td align=center><%=Languages.getString("jsp.admin.reports.paymentrequest_search.failedstatus", SessionData.getLanguage())%></td>
                    <%
                            }
                        }//End of else if the payment should be rejected
                    %>
                    
                </tr>
                <%
                            nRow = (nRow == 1) ? 2 : 1;
                        }//End of if user made a change in results
                    }//End of thru request params
                    if (bResultsProcessed) {
                %>
            </table>
        </td>
        <%
        } else {
        %>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr><td class=main><%=Languages.getString("jsp.admin.reports.paymentrequest_search.noitemschanged", SessionData.getLanguage())%></td></tr>
            </table>
        </td>
        <%
            }
        %>
    </tr>
    <tr><td><br><br></td></tr>
    <tr>
        <td colspan=3 align=center>
            <form method="post" action="admin/reports/transactions/paymentrequest_search.jsp">
                <input type=submit class="plain" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.backtosearch", SessionData.getLanguage())%>">
            </form>
        </td>
    </tr>
</table>
<%
} catch (Exception e) {
%>
<table border="0" cellpadding="0" cellspacing="0" width="60%">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b>ERROR IN PAGE</b></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr><td class=main><%=Languages.getString("jsp.admin.reports.paymentrequest_search.checklog", SessionData.getLanguage())%></td></tr>
            </table>
        </td>
    </tr>
    <tr><td><br><br></td></tr>
</table>
<%
    }
}//End of if a change in credit/recycle/reject was made
else if (request.getParameter("search") != null) {//If a search was made
    StringBuilder comboRejectReasons = new StringBuilder(); 
    
    String COL_REASON                    = "COL_REASON";
    String COL_TITLE_REASON              = "COL_TITLE_REASON";
    String LABEL_REJECTED_WINDOW         = "LABEL_REJECTED_WINDOW";
    String WARNNING_REJECTED_WINDOW      = "WARNNING_REJECTED_WINDOW";
    String OK_BUTTON_REJECTED_WINDOW     = "OK_BUTTON_REJECTED_WINDOW";
    String CANCEL_BUTTON_REJECTED_WINDOW = "CANCEL_BUTTON_REJECTED_WINDOW";
            
    String value_LABEL_REJECTED_WINDOW         = "";
    String value_WARNNING_REJECTED_WINDOW      = "";
    String value_OK_BUTTON_REJECTED_WINDOW     = "";
    String value_CANCEL_BUTTON_REJECTED_WINDOW = "";
    
    try {        
        report = new PaymentRequestReport(SessionData, application, startDate,
                endDate, agentList, allAgentsSelected, subAgentList, allSubAgentsSelected,
                repList, allRepsSelected, merchantList, allMerchantsSelected,
                paymentStatusList, allPaymentStatusSelected, bankListParam,
                allBanksSelected, paymentRequestId, documentId, paymentAmount,
                siteId, scheduling);
        report.setScheduling(false);
        vResults = report.getResults();
        
        ArrayList<BasicPojo> paymentRequestsRejectReasons = DbUtil.getPaymentRequestsRejectReasons(application);
        String sessionLanguage = SessionData.getUser().getLanguageCode(); 
        HashMap<String, ResourceReport> resources = ResourceReport.findResourcesByReportHash( ResourceReport.RESOURCE_PAYMENT_REQUEST_MANAGEMENT, sessionLanguage);
        comboRejectReasons.append("<select id='comboRejectReason' >");
        for(BasicPojo basic : paymentRequestsRejectReasons){
            ResourceReport res = resources.get(basic.getDescripton()); 
            if ( res!=null){
                comboRejectReasons.append("<option value='"+basic.getId()+"'>"+res.getMessage()+"</option>");
            }
        }
        comboRejectReasons.append("</select>");
        
        if ( resources.get(LABEL_REJECTED_WINDOW) != null )
            value_LABEL_REJECTED_WINDOW = resources.get(LABEL_REJECTED_WINDOW).getMessage();
        
        if ( resources.get(WARNNING_REJECTED_WINDOW) != null )
            value_WARNNING_REJECTED_WINDOW = resources.get(WARNNING_REJECTED_WINDOW).getMessage();
        
        if ( resources.get(OK_BUTTON_REJECTED_WINDOW) != null )
            value_OK_BUTTON_REJECTED_WINDOW = resources.get(OK_BUTTON_REJECTED_WINDOW).getMessage();
        
        if ( resources.get(CANCEL_BUTTON_REJECTED_WINDOW) != null )
            value_CANCEL_BUTTON_REJECTED_WINDOW = resources.get(CANCEL_BUTTON_REJECTED_WINDOW).getMessage();
        
    } catch (Exception e) {
%>
<table border="0" cellpadding="0" cellspacing="0" width="60%">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b>ERROR IN PAGE</b></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr><td class=main><%=Languages.getString("jsp.admin.reports.paymentrequest_search.checklog", SessionData.getLanguage())%></td></tr>
            </table>
        </td>
    </tr>
    <tr><td><br><br></td></tr>
</table>
<%
    }
%>
<script>
    function ToggleConfirmLabel(nControlID, sText)
    {
        document.getElementById("span_h_" + nControlID).style.display = 'none';
        document.getElementById("span_a_" + nControlID).style.display = 'none';
        document.getElementById("span_r_" + nControlID).style.display = 'none';
        document.getElementById("span_" + sText + "_" + nControlID).style.display = 'inline';
    }

    function btnApply_OnClick()
    {
        document.getElementById('t1').style.display = 'none';
        document.getElementById('t2').style.display = 'inline';
        document.getElementById('btnApply').style.display = 'none';
        document.getElementById('btnReset').style.display = 'none';
        document.getElementById('btnSubmit').style.display = 'inline';
        document.getElementById('btnCancel').style.display = 'inline';
    }

    function btnCancel_OnClick()
    {
        document.getElementById('t1').style.display = 'inline';
        document.getElementById('t2').style.display = 'none';
        document.getElementById('btnApply').style.display = 'inline';
        document.getElementById('btnReset').style.display = 'inline';
        document.getElementById('btnSubmit').style.display = 'none';
        document.getElementById('btnCancel').style.display = 'none';
    }
</script>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script src="includes/sortROC.js" type="text/javascript"></script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b><%=Languages.getString("jsp.admin.reports.paymentrequest_search.title", SessionData.getLanguage()).toUpperCase()%></b></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <%
            if ((vResults != null) && (vResults.size() > 0)) {//If there are results
                boolean bShowOptions = false;
                for (ResultField currentField : vResults.get(0)) {
                    if (currentField.getName().equals(PaymentRequestReport.STATUS_CODE_ID)) {
                        bShowOptions = String.valueOf(currentField.getValue()).equals("SUBMITTED");
                        break;
                    }
                }
                if (bShowOptions) {
        %>
    <form method="post" action="admin/reports/transactions/paymentrequest_search.jsp" onsubmit="document.getElementById('btnSubmit').disabled = true;">
        
        
        <div id="dialog-message" title="<%=value_WARNNING_REJECTED_WINDOW%>" >
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                <%=value_LABEL_REJECTED_WINDOW%><br/>        
            </p>
            <p id="pDialogMessage">
                <%=comboRejectReasons.toString()%>
            </p>		  
        </div>
        <script LANGUAGE="JavaScript">
            var id_to_reason="";    
            function cleanReason(requestId){        
                
                $("#reason_"+requestId).html("");
                $("#reason_hidden_"+requestId).val("");
                $("#reason_t2_"+requestId).html("");
            }
            $(function() 
            {				
                $(document).ready(function(){	
                    $("#dialog-message").hide();
                });
                $( "#dialog-message" ).dialog({
                    modal: true,
                    height: 150,
                    width: 805,
                    open: function(event, ui) { $(".ui-dialog-titlebar-close").bind("click", function (e) {
                        $("#hold_"+id_to_reason).prop('checked',true);               
                        }); 
                    },           
                    buttons: {
                          <%=value_OK_BUTTON_REJECTED_WINDOW%>: function() {
                            var rejectReasonId = $("#comboRejectReason option:selected").val();                    
                            var rejectReasonDesc = $("#comboRejectReason option:selected").text();
                            $("#reason_"+id_to_reason).html(rejectReasonDesc);
                            $("#reason_t2_"+id_to_reason).html(rejectReasonDesc);                            
                            $("#reason_hidden_"+id_to_reason).val(rejectReasonId);
                            $("#reason_hidden_desc_"+id_to_reason).val(rejectReasonDesc);
                            $( this ).dialog( "close" );                                        
                          },
                          <%=value_CANCEL_BUTTON_REJECTED_WINDOW%>: function(){
                            $("#hold_"+id_to_reason).prop('checked',true); 
                            $( this ).dialog( "close" );
                          }                  
                    }
                });
                $( "#dialog-message" ).dialog( "close" );
                $(".rejectReason").bind("click", function (e) {
                    var idValue = this.name.split("_");            
                    id_to_reason = idValue[1];             
                    $("#dialog-message").dialog( "open" );
                    $("#dialog-message").width( 2000 );
                    $("#dialog-message").height( 150 );            
                });
            });

        </script>
        
        <%
            }
        %>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1" style="display:inline;">
                <thead>
                    <tr  class="SectionTopBorder">
                        <th class="rowhead2"></th>
                            <%
                                // This section prints the column names for the table
                                if (bShowOptions) {
                                    // Print the column titles for the H (Hold), A (Apply), R (Reject) options
                                    String optionFormat = "<th class='rowhead2' title='%s'>%s</th>";

                                    String optionTitle = Languages.getString("jsp.admin.reports.paymentrequest_search.hold", SessionData.getLanguage());
                                    String optionValue = StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.hold1", SessionData.getLanguage()).toUpperCase());
                                    out.println(String.format(optionFormat, optionTitle, optionValue));

                                    optionTitle = Languages.getString("jsp.admin.reports.paymentrequest_search.apply", SessionData.getLanguage());
                                    optionValue = StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.apply1", SessionData.getLanguage()).toUpperCase());
                                    out.println(String.format(optionFormat, optionTitle, optionValue));

                                    optionTitle = Languages.getString("jsp.admin.reports.paymentrequest_search.reject", SessionData.getLanguage());
                                    optionValue = StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.reject1", SessionData.getLanguage()).toUpperCase());
                                    out.println(String.format(optionFormat, optionTitle, optionValue));                                   
                                }
                                // Set the rest of the column headers
                                ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
                                String columnTitleFormat = "<th class='rowhead2' nowrap>%s&nbsp;</th>";
                                if ((reportColumns != null) && (reportColumns.size() > 0)) {
                                    for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                                        out.println(String.format(columnTitleFormat, reportColumns.get(columnIndex).getLanguageDescription()));
                                    }
                                }
                            %>
                    </tr>
                </thead>
                <%
                    int intEvenOdd = 1;
                    int count = 1;
                    String requestId = "";
                    for (ArrayList<ResultField> currentRecord : vResults) {
                        StringBuilder tableRecord = new StringBuilder();
                        tableRecord.append(String.format("<tr class=row%d>", intEvenOdd));
                        tableRecord.append(String.format("<td>%d</td>", (count++)));
                        if (bShowOptions) {
                            tableRecord.append("<td align='center'>");

                            // From Array list to hash map, so specific values are easier to retrieve in order to set them in hiden fields
                            HashMap<String, Object> mapRecord = new HashMap();
                            for (ResultField currentField : currentRecord) {
                                mapRecord.put(currentField.getName(), currentField.getValue());
                            }
                            requestId = String.valueOf(mapRecord.get(PaymentRequestReport.REQUEST_ID));
                            String hidenFieldFormat = "<input type='hidden' name='%s_%s' value='%s'>";
                            tableRecord.append(String.format(hidenFieldFormat, "usr", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.PROCESS_USER_ID))));
                            tableRecord.append(String.format(hidenFieldFormat, "dba", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.DBA_ID))));
                            tableRecord.append(String.format(hidenFieldFormat, "mil", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.MILLENIUM_NO_ID))));
                            tableRecord.append(String.format(hidenFieldFormat, "clerk", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.CLERK_ID_NAME_ID))));
                            tableRecord.append(String.format(hidenFieldFormat, "docnum", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.DOCUMENT_NUMBER_ID))));
                            tableRecord.append(String.format(hidenFieldFormat, "docdate", requestId, DateUtil.formatDate((java.sql.Timestamp) mapRecord.get(PaymentRequestReport.DOCUMENT_DATE_ID))));
                            tableRecord.append(String.format(hidenFieldFormat, "bankname", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.BANK_NAME_ID))));
                            if (mapRecord.containsKey(PaymentRequestReport.DEPOSIT_TYPE_DESCRIPTION_ENG)) {
                                tableRecord.append(String.format(hidenFieldFormat, "depositTypeName", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.DEPOSIT_TYPE_DESCRIPTION_ENG))));
                            }
                            if (mapRecord.containsKey(PaymentRequestReport.DEPOSIT_TYPE_DESCRIPTION_SPA)) {
                                tableRecord.append(String.format(hidenFieldFormat, "depositTypeName", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.DEPOSIT_TYPE_DESCRIPTION_SPA))));
                            }
                            tableRecord.append(String.format(hidenFieldFormat, "amount", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.AMOUNT_ID))));
                            tableRecord.append(String.format(hidenFieldFormat, "date", requestId, DateUtil.formatDateTime((java.sql.Timestamp) mapRecord.get(PaymentRequestReport.REQUEST_DATE_ID))));
                            
                            tableRecord.append("<input type='hidden' id='reason_hidden_"+requestId+"' name='reason_hidden_"+requestId+"' value=''>");
                            tableRecord.append("<input type='hidden' id='reason_hidden_desc_"+requestId+"' name='reason_hidden_desc_"+requestId+"' value=''>");
                            
                            if (mapRecord.containsKey(PaymentRequestReport.AGENT_ID)) {
                                tableRecord.append(String.format(hidenFieldFormat, "age", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.AGENT_ID))));
                            }
                            if (mapRecord.containsKey(PaymentRequestReport.SUB_AGENT_ID)) {
                                tableRecord.append(String.format(hidenFieldFormat, "sag", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.SUB_AGENT_ID))));
                            }
                            if (mapRecord.containsKey(PaymentRequestReport.REP_ID)) {
                                tableRecord.append(String.format(hidenFieldFormat, "rep", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.REP_ID))));
                            }
                            if (mapRecord.containsKey(PaymentRequestReport.STATUS_CODE_ID)) {
                                tableRecord.append(String.format(hidenFieldFormat, "sta", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.STATUS_CODE_ID))));
                            }
                            if (mapRecord.containsKey(PaymentRequestReport.PROCESS_USER_ID)) {
                                tableRecord.append(String.format(hidenFieldFormat, "usr", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.PROCESS_USER_ID))));
                            }
                            if (mapRecord.containsKey(PaymentRequestReport.MERCHANT_ID)) {
                                tableRecord.append(String.format(hidenFieldFormat, "mer", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.MERCHANT_ID))));
                            }
                            if (mapRecord.containsKey(PaymentRequestReport.BANK_ID)) {
                                tableRecord.append(String.format(hidenFieldFormat, "bankID", requestId, String.valueOf(mapRecord.get(PaymentRequestReport.BANK_ID))));
                            }
                            
                            // The first radio is the only control shown in this column
                            tableRecord.append(String.format("<input type='radio' onclick='cleanReason(" + requestId + " );' id='hold_%s' name='chk_%s' value='h' checked onpropertychange=\"if (this.checked) {ToggleConfirmLabel(%s, 'h');}\">",
                                    requestId, requestId, requestId));
                            tableRecord.append("</td>"); // Closing hiden fields column
                            tableRecord.append(String.format("<td align=center><input type='radio' onclick='cleanReason(" + requestId + ");' name='chk_%s' value='a' onpropertychange=\"if (this.checked) {ToggleConfirmLabel(%s, 'a');}\"></td>",
                                    requestId, requestId));
                            tableRecord.append(String.format("<td align=center><input type='radio' class='rejectReason' name='chk_%s' value='r' onpropertychange=\"if (this.checked) {ToggleConfirmLabel(%s, 'r');}\"></td>",
                                    requestId, requestId));                            
                        }

                        // After the combos has been set the rest of the columns are populated
                        for (ResultField currentField : currentRecord) {
                            String defaultFieldFormat = "<td %s %s>%s</td>";
                            String style = "";
                            String elementId = "";

                            if (currentField.getName().equals(PaymentRequestReport.REQUEST_ID)) {
                                requestId = String.valueOf(currentField.getValue());
                            }
                            if (currentField.getName().equals(PaymentRequestReport.REJECT_REASON_ID)) {
                                if (!String.valueOf(currentField.getValue()).isEmpty()) {
                                    String resourceKey = "jsp.admin.reports.paymentrequest_search.rejectReason_" + currentField.getValue();
                                    String rejectReazon = Languages.getString(resourceKey, SessionData.getLanguage());
                                    if ((rejectReazon != null) && (!rejectReazon.isEmpty())) {                                        
                                        tableRecord.append(String.format(defaultFieldFormat, elementId, style, String.valueOf(currentField.getValue())));
                                    } else {
                                        tableRecord.append(String.format(defaultFieldFormat, elementId, style, String.valueOf(currentField.getValue())));
                                    }
                                } else {
                                    String idTdReason = "id='reason_"+requestId+"'";
                                    tableRecord.append(String.format(defaultFieldFormat, idTdReason, style, ""));
                                }
                            } else if (currentField.getName().equals(PaymentRequestReport.STATUS_CODE_ID)) {
                                String resourceKey = "jsp.admin.reports.paymentrequest_search.status_" + currentField.getValue();
                                String statusString = Languages.getString(resourceKey, SessionData.getLanguage());
                                if ((statusString != null) && (!statusString.isEmpty())) {
                                    tableRecord.append(String.format(defaultFieldFormat, elementId, style, statusString));
                                } else {
                                    tableRecord.append(String.format(defaultFieldFormat, elementId, style, String.valueOf(currentField.getValue())));
                                }
                            } else if (currentField.getName().equals(PaymentRequestReport.DOCUMENT_DATE_ID)) {
                                style = " style=\"text-align: right;\"";
                                tableRecord.append(String.format(defaultFieldFormat, elementId, style,
                                        PaymentRequestReport.documentDateFormatter.format(currentField.getValue())));

                            } else if (currentField.getName().equals(PaymentRequestReport.AMOUNT_ID)) {
                                style = " style=\"text-align: right;\"";
                                tableRecord.append(String.format(defaultFieldFormat, elementId, style,
                                        NumberUtil.formatCurrency(String.valueOf(currentField.getValue()))));
                            } else if (currentField.getName().equals(PaymentRequestReport.REQUEST_DATE_ID)) {
                                tableRecord.append(String.format(defaultFieldFormat, elementId, style,
                                        PaymentRequestReport.requestDateFormatter.format(currentField.getValue())));
                            } else if (currentField.getName().equals(PaymentRequestReport.BANK_ID)
                                    || currentField.getName().equals(PaymentRequestReport.MERCHANT_ID)
                                    || currentField.getName().equals(PaymentRequestReport.CLERK_ID)
                                    || currentField.getName().equals(PaymentRequestReport.CLERK_NAME_ID)) {
                                continue;
                            } else {
                                if (currentField.getName().equals(PaymentRequestReport.DOCUMENT_NUMBER_ID)) {
                                    style = " style=\"text-align: right;\"";
                                }
                                if (currentField.getName().equals(PaymentRequestReport.REJECT_REASON_ID)) {
                                    elementId = String.format("id='resonCol_%s'", requestId);
                                }
                                tableRecord.append(String.format(defaultFieldFormat, elementId, style, String.valueOf(currentField.getValue())));
                            }

                        } //End for columns                                                
                        tableRecord.append("</tr>");
                        out.println(tableRecord.toString());
                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        } else {
                            intEvenOdd = 1;
                        }
                    }// End for records
                %> 
            </table>
            <%
                if (bShowOptions) {
            %>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t2" style="display:none;">
                <thead>
                    <tr class="SectionTopBorder">
                        <th class="rowhead2"></th>
                        <%
                            // Set the column headers
                            reportColumns = report.getReportRecordHeaders();
                            if ((reportColumns != null) && (reportColumns.size() > 0)) {
                                for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                        %>                    
                        <th class="rowhead2" nowrap align=center valign="top"><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
                        </th>
                        <%
                                }
                            }
                        %>
                        <th class="rowhead2" align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.action", SessionData.getLanguage()).toUpperCase())%></th>
                    </tr>
                </thead>


                <%
                    intEvenOdd = 1;
                    count = 1;
                    String spanFormat = "<span id=\"span_h_%s\" style=\"display:%s;\"><b>%s&nbsp;?<br></b></span>";

                    for (ArrayList<ResultField> currentRecord : vResults) {
                        StringBuilder tableRecord = new StringBuilder();
                        tableRecord.append(String.format("<tr class=row%d>", intEvenOdd));
                        tableRecord.append(String.format("<td>%d</td>", (count++)));
                        requestId = "";
                        // Fill the columns
                        for (ResultField currentField : currentRecord) {
                            String defaultFieldFormat = "<td %s %s>%s</td>";
                            String style = "";
                            String elementId = "";

                            if (currentField.getName().equals(PaymentRequestReport.REQUEST_ID)) {
                                requestId = String.valueOf(currentField.getValue());
                            }
                            if (currentField.getName().equals(PaymentRequestReport.REJECT_REASON_ID)) {
                                if (!String.valueOf(currentField.getValue()).isEmpty()) {
                                    String resourceKey = "jsp.admin.reports.paymentrequest_search.rejectReason_" + currentField.getValue();
                                    String rejectReazon = Languages.getString(resourceKey, SessionData.getLanguage());
                                    if ((rejectReazon != null) && (!rejectReazon.isEmpty())) {
                                        tableRecord.append(String.format(defaultFieldFormat, elementId, style, rejectReazon));
                                    } else {
                                        tableRecord.append(String.format(defaultFieldFormat, elementId, style, String.valueOf(currentField.getValue())));
                                    }
                                } else {
                                    String idTdReason = "id='reason_t2_"+requestId+"'";
                                    tableRecord.append(String.format(defaultFieldFormat, idTdReason, style, ""));
                                }
                            } else if (currentField.getName().equals(PaymentRequestReport.STATUS_CODE_ID)) {
                                String resourceKey = "jsp.admin.reports.paymentrequest_search.status_" + currentField.getValue();
                                String statusString = Languages.getString(resourceKey, SessionData.getLanguage());
                                if ((statusString != null) && (!statusString.isEmpty())) {
                                    tableRecord.append(String.format(defaultFieldFormat, elementId, style, statusString));
                                } else {
                                    tableRecord.append(String.format(defaultFieldFormat, elementId, style, String.valueOf(currentField.getValue())));
                                }
                            } else if (currentField.getName().equals(PaymentRequestReport.DOCUMENT_DATE_ID)) {
                                style = " style=\"text-align: right;\"";
                                tableRecord.append(String.format(defaultFieldFormat, elementId, style,
                                        PaymentRequestReport.documentDateFormatter.format(currentField.getValue())));

                            } else if (currentField.getName().equals(PaymentRequestReport.AMOUNT_ID)) {
                                style = " style=\"text-align: right;\"";
                                tableRecord.append(String.format(defaultFieldFormat, elementId, style,
                                        NumberUtil.formatCurrency(String.valueOf(currentField.getValue()))));
                            } else if (currentField.getName().equals(PaymentRequestReport.REQUEST_DATE_ID)) {
                                tableRecord.append(String.format(defaultFieldFormat, elementId, style,
                                        PaymentRequestReport.requestDateFormatter.format(currentField.getValue())));
                            } else if (currentField.getName().equals(PaymentRequestReport.BANK_ID)
                                    || currentField.getName().equals(PaymentRequestReport.MERCHANT_ID)
                                    || currentField.getName().equals(PaymentRequestReport.CLERK_ID)
                                    || currentField.getName().equals(PaymentRequestReport.CLERK_NAME_ID)) {
                                continue;
                            } else {
                                if (currentField.getName().equals(PaymentRequestReport.DOCUMENT_NUMBER_ID)) {
                                    style = " style=\"text-align: right;\"";
                                }
                                if (currentField.getName().equals(PaymentRequestReport.REJECT_REASON_ID)) {
                                    elementId = String.format("id='resonCol_%s'", requestId);
                                }                                
                                tableRecord.append(String.format(defaultFieldFormat, elementId, style, String.valueOf(currentField.getValue())));
                            }

                        } //End for columns
                        tableRecord.append("<td nowrap>");
                        tableRecord.append(String.format(spanFormat, requestId, "inline",
                                Languages.getString("jsp.admin.reports.paymentrequest_search.hold", SessionData.getLanguage())));
                        tableRecord.append(String.format(spanFormat, requestId, "none",
                                Languages.getString("jsp.admin.reports.paymentrequest_search.apply", SessionData.getLanguage())));
                        tableRecord.append(String.format(spanFormat, requestId, "none",
                                Languages.getString("jsp.admin.reports.paymentrequest_search.reject", SessionData.getLanguage())));
                        tableRecord.append("</td>");
                        tableRecord.append("</tr>");
                        out.println(tableRecord.toString());
                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        } else {
                            intEvenOdd = 1;
                        }
                    }// End for records
                    vResults.clear();
                %>        

            </table>
            <%
                }//End of render of confirmation table
            %>
            <script type="text/javascript">
                <%
                    if (bShowOptions) {
                %>
                //var stT1 = new SortROC(document.getElementById("t1"), ["None", "None", "None", "None", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString"], 0, false, false);
                var stT1 = new SortROC(document.getElementById("t1"), ["None", "None", "None", "None", "Number", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"], 0, false, false);
                <%
                } else {
                %>
                //var stT1 = new SortROC(document.getElementById("t1"), ["None", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString"], 0, false, false);
                var stT1 = new SortROC(document.getElementById("t1"), ["None", "Number", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"], 0, false, false);
                <%
                    }
                %>
            </script>
        </td>
        <%
            if (bShowOptions) {
        %>
        </tr>
        <tr><td><br><br></td></tr>
        <tr>
            <td colspan=3 align=center>
                <table>
                    <tr>
                        <td>
                            <input type=button id="btnApply" style="display:inline;" onclick="btnApply_OnClick();" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.applychanges", SessionData.getLanguage())%>">
                            <input type=submit id="btnSubmit" style="display:none;" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.confirmchanges", SessionData.getLanguage())%>">
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <input type=reset id="btnReset" style="display:inline;" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.resetchanges", SessionData.getLanguage())%>">
                            <input type=button id="btnCancel" style="display:none;" onclick="btnCancel_OnClick();" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.cancelchanges", SessionData.getLanguage())%>">
                        </td>
                    </tr>
                </table>
            </td>
        <input type=hidden name=frmApply value=y>
        <input type=hidden name=download value="">
        <%@ include file="/WEB-INF/jspf/admin/reports/payments/paymentRequestSearchSetFilters.jspf" %>  
    </form>
    <%        }
    }//End of if there are results
    else {//Else show the message of no results
%>
    <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class=main><%=Languages.getString("jsp.admin.reports.paymentrequest_search.noresults", SessionData.getLanguage())%></td></tr>
        </table>
    </td>

    <%
        }//End of else show the message of no results
%>
</tr>
<tr><td><br><br></td></tr>
<tr>
    <td colspan=3 align=center>
        <table>
            <tr>
                <td>
                    <form method="post" action="admin/reports/transactions/paymentrequest_search.jsp">
                        <input type=submit class="plain" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.backtosearch", SessionData.getLanguage())%>">
                    </form>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                    <form id="downloadForm" method="post" action="admin/reports/transactions/paymentrequest_search.jsp">
                        <%@ include file="/WEB-INF/jspf/admin/reports/payments/paymentRequestSearchSetFilters.jspf" %>  
                        <input type=hidden name=download value="y">
                        <button id="btnSubmit" type="submit">
                            <%=Languages.getString("jsp.admin.reports.downloadToCsv", SessionData.getLanguage())%>
                        </button>        
                    </form>
                </td>
            </tr>
        </table>    
    </td>


</tr>
</table>
<%
}//End of if a search was made
else if (request.getParameter("download") != null && request.getParameter("download").equals("y")) {
    //TO DOWNLOAD ZIP REPORT
    report = new PaymentRequestReport(SessionData, application, startDate,
            endDate, agentList, allAgentsSelected, subAgentList, allSubAgentsSelected,
            repList, allRepsSelected, merchantList, allMerchantsSelected,
            paymentStatusList, allPaymentStatusSelected, bankListParam,
            allBanksSelected, paymentRequestId, documentId, paymentAmount,
            siteId, scheduling);
    report.setScheduling(false);
    String zippedFilePath = report.downloadReport();
    if ((zippedFilePath != null) && (!zippedFilePath.trim().equals(""))) {
        response.sendRedirect(zippedFilePath);
    }
    vResults = report.getResults();
} else {//Else we need to show the search page
%>
<script>
    var nSUBMITTEDID = 0;
    function ToggleDateFilters(chkAllOpen)
    {
        if (chkAllOpen.checked)
        {
            document.getElementById('txtStartDate').readOnly = true;
            document.getElementById('txtEndDate').readOnly = true;
            document.getElementById('txtStartDate').value = '';
            document.getElementById('txtEndDate').value = '';
            document.getElementById('linkStartDate').onclick = new Function('return true;');
            document.getElementById('linkEndDate').onclick = new Function('return true;');
            document.getElementById('ddlStatus').disabled = true;
            document.getElementById('ddlStatus').value = nSUBMITTEDID;
        }
        else
        {
            document.getElementById('txtStartDate').readOnly = false;
            document.getElementById('txtEndDate').readOnly = false;
            document.getElementById('linkStartDate').onclick = new Function(document.getElementById('linkStartDate').getAttribute("_onclick"));//_onclick
            document.getElementById('linkEndDate').onclick = new Function(document.getElementById('linkEndDate').getAttribute("_onclick"));
            document.getElementById('ddlStatus').disabled = false;
        }
    }//End of function ToggleDateFilters


    function FilterRequestNumber(txtRequestNumber)
    {
        var sText = txtRequestNumber.value;
        var sResult = "";
        for (i = 0; i < sText.length; i++)
        {
            if ((sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57))
            {
                sResult += sText.charAt(i);
            }
        }
        if (sText != sResult)
        {
            txtRequestNumber.value = sResult;
        }
    }//End of function FilterRequestNumber

</script>
<form name="mainform" method="post" action="admin/reports/transactions/paymentrequest_search.jsp">
    <%@ include file="/WEB-INF/jspf/admin/reports/payments/paymentRequestSearchFilters.jspf" %>
</form>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%    }//End of else we need to show the search page
%>
<%@ include file="/includes/footer.jsp" %>