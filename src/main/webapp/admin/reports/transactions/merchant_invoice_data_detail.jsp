<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 4;
  int section_page = 1;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%

  Vector<Vector<String>>    vecSearchResults = new Vector<Vector<String>>();
  Hashtable searchErrors     = null;
  String    sortString       = "";
  
    if ( request.getParameter("download") != null ){
		SessionData.setProperty("start_date", request.getParameter("startDate"));
		SessionData.setProperty("end_date", request.getParameter("endDate"));
		
		String strMerchantIds[] = request.getParameterValues("mids");
		String strAgentIds[] = request.getParameterValues("rids");
		String strInvoice = request.getParameter("invoiceTypeList");
		
		if (strMerchantIds != null)
		{
      		TransactionReport.setMerchantIds(strMerchantIds);
    	}
    
    	if (strAgentIds != null)
    	{
      		TransactionReport.setAgentsIds(strAgentIds);
    	}
    
	    if (strInvoice != null)
	    {
	      TransactionReport.setInvoiceType(strInvoice);
	    }
		String sURL = TransactionReport.downloadMerchantInvoiceDataDetail(application, SessionData);
		response.sendRedirect(sURL);
		return;
	}
  
  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));

      String strMerchantIds[] = request.getParameterValues("mids");
      String strAgentIds[] = request.getParameterValues("rids");
      String strInvoice = request.getParameter("invoiceTypeList");

      if (strMerchantIds != null)
      {
        TransactionReport.setMerchantIds(strMerchantIds);
      }
      
      if (strAgentIds != null)
      {
        TransactionReport.setAgentsIds(strAgentIds);
      }
      
      if (strInvoice != null)
      {
        TransactionReport.setInvoiceType(strInvoice);
      }

      vecSearchResults = TransactionReport.getMerchantInvoiceDataDetail(SessionData, application);
      
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%= Languages.getString("jsp.admin.reports.invoice_merchants.title",SessionData.getLanguage()).toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td class="main">

<%
                out.println(Languages.getString("jsp.admin.reports.invoice_merchants.report_description",SessionData.getLanguage()));
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                  out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
%>
				<br>
                <br>
                <%= Languages.getString("jsp.admin.reports.invoice_merchants.shared_note",SessionData.getLanguage()) %>  
                <br>
                <font color="#ff0000">
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.limited_to_90_days",SessionData.getLanguage()) %>
                </font>
             	<br>
             	<br>
              </td>
              <td class=main align=right valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
            <tr>
              <td colspan=2>
                <FORM ACTION="admin/reports/transactions/merchant_invoice_data_detail.jsp" METHOD=post ONSUBMIT="document.getElementById('btnDownload').disabled = true;" target="_blank">
                  <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                  <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                  <INPUT TYPE=hidden NAME=mids VALUE="<%=TransactionReport.getMerchantIds()%>">
                  <INPUT TYPE=hidden NAME=rids VALUE="<%=TransactionReport.getAgentsIds()%>">
                  <INPUT TYPE=hidden NAME=invoiceTypeList VALUE="<%=TransactionReport.getInvoiceType()%>">
                  <INPUT TYPE=hidden NAME=download VALUE="Y">
                  <INPUT ID=btnDownload TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                </FORM>
              </td>
            </tr>
          </table>
          <table>
              <tr>
                  <td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
              </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.invoice_type",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.merchant_id",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.business_name",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.address",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.colony",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.delegation",SessionData.getLanguage()).toUpperCase() %>
                </td>
                
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.city",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.state",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.country",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.zip",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.rfc",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.dba",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.rep_name",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.rep_id",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.rep_credit_type",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.credit_limit_type",SessionData.getLanguage()).toUpperCase()%>
                </td>
                
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.customers.merchants_edit.bankname",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                	<%= Languages.getString("jsp.admin.reports.invoice_merchants.payment_total",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.quantity",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.amount",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.merchant_commission",SessionData.getLanguage()).toUpperCase() %>
                </td>
                
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.invoice_amount",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.before_tax_amount",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.invoice_merchants.tax_amount",SessionData.getLanguage()).toUpperCase() %>
                </td>
          </thead>
				<%
					int intEvenOdd = 1;
					int count = 1;
					double dblTotalPayment = 0;
					long lngQuantityTotal = 0;
					double dblTotalAmount = 0;
					double dblMerchantRateTotal = 0;
					double dblInvoiceAmountTotal = 0;
					double dblNoTaxAmountTotal = 0;
					double dblTaxAmountTotal = 0;
					 
					for (Vector vecTemp : vecSearchResults) {
						out.println("<tr class=row" + intEvenOdd + ">");
						out.println("<td>" + (count++) + "</td>");
						out.println("<td>" + vecTemp.get(0) + "</td>");
						out.println("<td>" + vecTemp.get(1) + "</td>");
						out.println("<td>" + vecTemp.get(2) + "</td>");
						out.println("<td>" + vecTemp.get(3) + "</td>");
						out.println("<td>" + vecTemp.get(4) + "</td>");
						out.println("<td>" + vecTemp.get(5) + "</td>");
						out.println("<td>" + vecTemp.get(6) + "</td>");
						out.println("<td>" + vecTemp.get(7) + "</td>");
						out.println("<td>" + vecTemp.get(8) + "</td>");
						out.println("<td>" + vecTemp.get(9) + "</td>");
						out.println("<td>" + vecTemp.get(10) + "</td>");
						out.println("<td>" + vecTemp.get(11) + "</td>");
						out.println("<td>" + vecTemp.get(12) + "</td>");
						out.println("<td>" + vecTemp.get(13) + "</td>");
						out.println("<td>" + vecTemp.get(14) + "</td>");
						out.println("<td>" + vecTemp.get(15) + "</td>");
						out.println("<td>" + vecTemp.get(16) + "</td>");
						out.println("<td align=\"right\">" + NumberUtil.formatCurrency(vecTemp.get(17).toString()) + "</td>");
						out.println("<td align=\"right\">" + vecTemp.get(18) + "</td>");
						out.println("<td align=\"right\">" + NumberUtil.formatCurrency(vecTemp.get(19).toString()) + "</td>");
						out.println("<td align=\"right\">" + NumberUtil.formatCurrency(vecTemp.get(20).toString()) + "</td>");
						out.println("<td align=\"right\">" + NumberUtil.formatCurrency(vecTemp.get(21).toString()) + "</td>");
						out.println("<td align=\"right\">" + NumberUtil.formatCurrency(vecTemp.get(22).toString()) + "</td>");
						out.println("<td align=\"right\">" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>");
                        out.println("</tr>");
						
						intEvenOdd = intEvenOdd == 1 ? 2 : 1;	
						//totals
						dblTotalPayment += Double.parseDouble(vecTemp.get(17).toString());
						lngQuantityTotal += Long.parseLong(vecTemp.get(18).toString());
						dblTotalAmount += Double.parseDouble(vecTemp.get(19).toString());
						dblMerchantRateTotal += Double.parseDouble(vecTemp.get(20).toString());
						dblInvoiceAmountTotal += Double.parseDouble(vecTemp.get(21).toString());
						dblNoTaxAmountTotal += Double.parseDouble(vecTemp.get(22).toString());
						dblTaxAmountTotal += Double.parseDouble(vecTemp.get(23).toString());
					}
					vecSearchResults.clear();
				%>   
			  <tfoot>
			  	<tr class=row<%=intEvenOdd%> >
			  		<td colspan="18" align="right"><%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
			  		<td align="right"><%=NumberUtil.formatCurrency(Double.toString(dblTotalPayment))%></td>
			  		<td align="right"><%=lngQuantityTotal%></td>
			  		<td align="right"><%=NumberUtil.formatAmount(Double.toString(dblTotalAmount))%></td>
			  		<td align="right"><%=NumberUtil.formatAmount(Double.toString(dblMerchantRateTotal))%></td>
			  		<td align="right"><%=NumberUtil.formatAmount(Double.toString(dblInvoiceAmountTotal))%></td>
			  		<td align="right"><%=NumberUtil.formatAmount(Double.toString(dblNoTaxAmountTotal))%></td>
			  		<td align="right"><%=NumberUtil.formatAmount(Double.toString(dblTaxAmountTotal))%></td>
			  	</tr>
			  </tfoot>
          </table>
          <SCRIPT type="text/javascript">
          <!--
			  var stT1 = new SortROC(document.getElementById("t1"), ["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString","Number", "Number", "Number","Number", "Number", "Number", "Number"],0,false,false);   
           -->
           </SCRIPT>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
