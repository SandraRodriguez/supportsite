<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 4;
  int section_page = 1;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%

  Vector<Vector<String>>    vecSearchResults = new Vector<Vector<String>>();
  Vector warningSearchResults = new  Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";
  boolean   bUseTaxValue     = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns

  String strMerchantIds[] = null;
  String strSalesIds[]= null;
  	
  Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();

  String noteTimeZone = "";
  String testTrx = "";  
  String titleReport = "";
  String reqRepId = request.getParameter("repIds");
 	
  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));

      strMerchantIds = request.getParameterValues("mids");
      strSalesIds = request.getParameterValues("salesIds");

      if (strMerchantIds != null)
      {
        TransactionReport.setMerchantIds(strMerchantIds);
      } else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
            // Try to fill subagent IDs by agent id
            String repId = request.getParameter("repId");
            if (repId != null) {
                List<String> merchantIds = DbUtil.getRepMerchantIds(application, Long.valueOf(repId));
                if (!merchantIds.isEmpty()) {
                    strMerchantIds = new String[merchantIds.size()];
                    strMerchantIds = merchantIds.toArray(strMerchantIds);
                    TransactionReport.setMerchantIds(strMerchantIds);
                }
            }
    }
	if (strSalesIds != null)
    {
      TransactionReport.setSalesIds(strSalesIds);
    }
	  //////////////////////////////////////////////////////////////////
	  //HERE WE DEFINE THE REPORT'S HEADERS 
	  headers = com.debisys.reports.ReportsUtil.getHeadersSummaryTransactionsMerchant(SessionData,application, DebisysConstants.MERCHANT );
	  //////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////
	
	  vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  	  noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
	  String keyLanguage = "jsp.admin.reports.transaction.merchant_summary";
	  titleReport = Languages.getString( keyLanguage , SessionData.getLanguage() );
	  testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());
	
	  boolean IsMexAndChkTax = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
           							(request.getParameter("chkUseTaxValue") != null); 	
      
      if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	  {
		 //TO SCHEDULE REPORT
		 titles.add(noteTimeZone);
		 titles.add(titleReport);		
         titles.add(testTrx);
         if ( IsMexAndChkTax )
	     {
	        bUseTaxValue = true;
	        TransactionReport.getMerchantSummaryMx( SessionData, application );
	     }
	     else
	     {
	      	TransactionReport.getMerchantSummary( SessionData, application , DebisysConstants.SCHEDULE_REPORT, headers, titles );   
		    TransactionReport.getWarningMessage();
	     }
	     ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
		 if (  scheduleReport != null  )
		 {
		 	scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
			scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
			scheduleReport.setTitleName( keyLanguage );   
		 }	
	     response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
      }
      else if ( request.getParameter("downloadReport") != null )
	  {
	  	 //TO DOWNLOAD ZIP REPORT
	  	 TransactionReport.setRepIds(reqRepId);
		 if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		 {
			   titleReport = titleReport + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
			   titleReport = titleReport + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
		 }
		 titles.add(noteTimeZone);
		 titles.add(titleReport);
	     titles.add(testTrx);
	     if ( IsMexAndChkTax )
	     {
	        bUseTaxValue = true;
	        TransactionReport.getMerchantSummaryMx(SessionData, application);
	     }
	     else
	     {
	      	TransactionReport.getMerchantSummary( SessionData, application , DebisysConstants.DOWNLOAD_REPORT, headers, titles );   
		    warningSearchResults = TransactionReport.getWarningMessage();
	     }
	     response.sendRedirect( TransactionReport.getStrUrlLocation() );	
	  }
	  else 
	  {
	     //TO SHOW REPORT
		 if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		 {
			   titleReport = titleReport + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
			   titleReport = titleReport + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
		 }
		 titles.add(noteTimeZone);
		 titles.add(titleReport);
	     titles.add(testTrx);
	  	 if ( IsMexAndChkTax )
	     {
	       bUseTaxValue = true;
	       vecSearchResults = TransactionReport.getMerchantSummaryMx(SessionData, application);
	     }
	     else
	     {
	      	vecSearchResults = TransactionReport.getMerchantSummary( SessionData, application , DebisysConstants.EXECUTE_REPORT , headers, titles );   
		    warningSearchResults = TransactionReport.getWarningMessage();
	     }
	  }
      
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= Languages.getString("jsp.admin.reports.title4",SessionData.getLanguage()).toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
          
          
          <% if ( warningSearchResults.size()>0){ %>
				<tr>
				<td class="main" style="color:red">
					<% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1)
								||  (!TransactionReport.checkfortaxtype(SessionData))){
								%>
					<%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
					<%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
								<% } %>
					<% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
						<%= warningSearchResults.get(i).toString()%> <br/>
					<% } %>
				</td>
				</tr>
				<%} %>
            
            <tr>            
              <td class="main"><%= noteTimeZone %> <br>
                <font color="#ff0000"><%= testTrx %></font>
              </td>
              <!--  
              <td class=main align=right valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
              -->
            </tr>
            <tr>
            	 <td class="main"><%= titleReport %> <br>
            </tr>
          </table>
          
          <form name="downloadData" method=post action="admin/reports/transactions/merchants_summary.jsp">
              <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
              <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
              <input type="hidden" name="search" value="y">
              <input type="hidden" name="downloadReport" value="y">
              <input type="hidden" name="repIds" value="<%=reqRepId%>">
            	<%if(strMerchantIds!=null){
            	for(int i=0;i<strMerchantIds.length;i++){%>
				<input type="hidden" name="mids" value="<%=strMerchantIds[i]%>">
				<%} }%>
               <%if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
			 {
                  if(strSalesIds!=null){
              for(int i=0;i<strSalesIds.length;i++){%>
				<input type="hidden" name="salesIds" value="<%=strSalesIds[i]%>">
				<%}}%>
               <%}%>
              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		  </form>
		  
          <table>
              <tr>
                  <td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
              </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
                if ( (strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                	  || strAccessLevel.equals(DebisysConstants.CARRIER) )
                {
                	
                  sortString = "\"None\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"Number\",\"Number\",\"Number\", \"Number\", \"Number\"";
%>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.customers.merchants_info.address",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
                }
                else
                {
                	
                  sortString = "\"None\",\"CaseInsensitiveString\",\"Number\",\"Number\",\"Number\", \"Number\", \"Number\"";
                }
%>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()) %>
                </td>
                  <%if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
                        {
                            sortString = "\"None\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"Number\",\"Number\",\"Number\", \"Number\", \"Number\""; %>
                         <td class=rowhead2 nowrap>
                	<%= Languages.getString("jsp.admin.reports.SalesRepId", SessionData.getLanguage()) %>
                        </td>
                        <td class=rowhead2 nowrap>
                	<%= Languages.getString("jsp.admin.reports.SalesRepName", SessionData.getLanguage()) %>
                        </td>
                        <td class=rowhead2 nowrap>
                	<%= Languages.getString("jsp.admin.reports.AccountNo", SessionData.getLanguage()) %>
                        </td>
                         <%
                         }else
                  {
                      sortString = "\"None\",\"CaseInsensitiveString\",\"Number\",\"Number\",\"Number\", \"Number\", \"Number\"";
                  }
%>
				<% if(strAccessLevel.equals(DebisysConstants.ISO)){ %>                
                <td class=rowhead2 nowrap>
                	<%= Languages.getString("jsp.admin.reports.transactions.commerce.invoice_type", SessionData.getLanguage()) %>
                </td>
                <%} %>
				<%
					if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
					{
                                            if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
                                         {
                                        sortString = "\"None\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"CaseInsensitiveString\",\"Number\",\"CaseInsensitiveString\",\"Number\",\"Number\", \"Number\", \"Number\""; 
                                            }else{
						sortString = "\"None\",\"CaseInsensitiveString\",\"Number\",\"CaseInsensitiveString\",\"Number\",\"Number\", \"Number\", \"Number\"";
                                }
				%>
						<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.route",SessionData.getLanguage()).toUpperCase()%></td>
						 <td class=rowhead2 nowrap> <%= Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()).toUpperCase() %> </td>
				<% 
					}
				%>                  
				<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase() %></td>
				<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase() %></td>
<%
					if( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
						 DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
					{
%>
				<td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase() %>
                </td>
				<td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%	
}
%>

<%
                if ( bUseTaxValue )
                {
                  sortString = sortString + ",\"Number\"";
%>
                <TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%></TD>
<%
                }
%>
<%
               if( SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) && 
                    DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                     DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                {
                  sortString = sortString + ",\"Number\"";
%>
                 <TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></TD>
                 <TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                }
%>
<%

                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>
                  </td>
<%
                  if ( bUseTaxValue )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  
                	DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                		DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.net_iso_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                }
                if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.CARRIER)) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %>
                  </td>
<%
                   if ( bUseTaxValue )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) && 
                	 DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                	 	DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.net_agent_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                }
                if ( (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %>
                  </td>
<%
                   if ( bUseTaxValue )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                if( SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) && 
                	  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                	   DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.net_subagent_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                }
%>
<%
                }
                if (!strAccessLevel.equals(DebisysConstants.MERCHANT) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %>
                  </td>
<%
                   if ( bUseTaxValue )
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) && 
                	DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                		DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.net_rep_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                }
                //mer %
                sortString = sortString + ",\"Number\"";
%>
                <td class=rowhead2 valign=bottom nowrap>
                  <%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
                 if ( bUseTaxValue )
                {
                  sortString = sortString + ",\"Number\"";
%>
                <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP>Mer %<%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                }
%>
<%
                if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  
                	DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                		DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                  {
                    sortString = sortString + ",\"Number\"";
%>
                  <TD CLASS="rowhead2" VALIGN="bottom" NOWRAP><%=Languages.getString("jsp.admin.net_merchant_percent",SessionData.getLanguage()).toUpperCase()%></TD>
<%
                  }
%>
<%
                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  sortString = sortString + ",\"Number\"";
%>
                  <td class=rowhead2 valign=bottom nowrap>
                    <%= Languages.getString("jsp.admin.reports.adjustment",SessionData.getLanguage()).toUpperCase() %>
                  </td>
<%
                }
%>
<%
						sortString +=  ",\"Number\"";
%>
                <td class=rowhead2 valign=bottom nowrap>
                  <%= Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
						sortString +=  ",\"Number\"";
%>
                <td class=rowhead2 valign=bottom nowrap>
                  <%= Languages.getString("jsp.admin.reports.limit",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
						sortString +=  ",\"Number\"";
%>
                <td class=rowhead2 valign=bottom nowrap>
                  <%= Languages.getString("jsp.admin.reports.available",SessionData.getLanguage()).toUpperCase() %>
                </td>
              </tr>
            </thead>
<%
            double   dblTotalSalesSum         = 0;
			double dblTotalBonus = 0;
			double dblTotalRecharge = 0;
            double   dblMerchantCommissionSum = 0;
            double   dblRepCommissionSum      = 0;
            double   dblSubAgentCommissionSum = 0;
            double   dblAgentCommissionSum    = 0;
            double   dblISOCommissionSum      = 0;
            double   dblVATTotalSalesSum         = 0;
            double   dblVATMerchantCommissionSum = 0;
            double   dblVATRepCommissionSum      = 0;
            double   dblVATSubAgentCommissionSum = 0;
            double   dblVATAgentCommissionSum    = 0;
            double   dblVATISOCommissionSum      = 0;
            double   dblAdjAmountSum          = 0;
            double   dblTotalQtySum           = 0;
            double dblTotalTaxAmountSum      = 0;
            Iterator<Vector<String>> it       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;

            if (it.hasNext())
            {
              Vector<String> vecTemp = null;

              vecTemp = it.next();
              dblTotalSalesSum = Double.parseDouble(vecTemp.get(1));
              dblMerchantCommissionSum = Double.parseDouble(vecTemp.get(2));
              dblRepCommissionSum = Double.parseDouble(vecTemp.get(3));
              dblSubAgentCommissionSum = Double.parseDouble(vecTemp.get(4));
              dblAgentCommissionSum = Double.parseDouble(vecTemp.get(5));
              dblISOCommissionSum = Double.parseDouble(vecTemp.get(6));
              dblTotalQtySum = Double.parseDouble(vecTemp.get(0));
              dblAdjAmountSum = Double.parseDouble(vecTemp.get(7));
            if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
              {
                dblVATTotalSalesSum = Double.parseDouble(vecTemp.get(8));
                dblVATMerchantCommissionSum = Double.parseDouble(vecTemp.get(9));
                dblVATRepCommissionSum = Double.parseDouble(vecTemp.get(10));
                dblVATSubAgentCommissionSum = Double.parseDouble(vecTemp.get(11));
                dblVATAgentCommissionSum = Double.parseDouble(vecTemp.get(12));
                dblVATISOCommissionSum = Double.parseDouble(vecTemp.get(13));
                dblTotalBonus = Double.parseDouble(vecTemp.get(14));
                dblTotalRecharge = Double.parseDouble(vecTemp.get(15));
                if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                    {
                      dblTotalTaxAmountSum=dblTotalSalesSum-dblVATTotalSalesSum;
                    }
              } else {
            	  dblTotalBonus = Double.parseDouble(vecTemp.get(8));
                  dblTotalRecharge = Double.parseDouble(vecTemp.get(9));
              }

              while (it.hasNext())
              {
                vecTemp = null; 
                vecTemp = it.next();
                double dblTotalQty           = Double.parseDouble(vecTemp.get(2));
                double dblTotalSales         = Double.parseDouble(vecTemp.get(3));
                double dblMerchantCommission = Double.parseDouble(vecTemp.get(4));
                double dblRepCommission      = Double.parseDouble(vecTemp.get(5));
                double dblSubAgentCommission = Double.parseDouble(vecTemp.get(6));
                double dblAgentCommission    = Double.parseDouble(vecTemp.get(7));
                double dblISOCommission      = Double.parseDouble(vecTemp.get(8));
                double dblAdjAmount          = Double.parseDouble(vecTemp.get(12));
                double dblVATTotalSales         = 0;
                double dblVATMerchantCommission = 0;
                double dblVATRepCommission      = 0;
                double dblVATSubAgentCommission = 0;
                double dblVATAgentCommission    = 0;
                double dblVATISOCommission      = 0;
                double dblTaxAmount=0;
                double dblBonus = 0;
                double dblTotalRe = 0;
                String strRouteName = "";
                String strCity="";
                if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                	DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                		DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
                  dblVATTotalSales = Double.parseDouble(vecTemp.get(13));
                  dblVATMerchantCommission = Double.parseDouble(vecTemp.get(14));
                  dblVATRepCommission = Double.parseDouble(vecTemp.get(15));
                  dblVATSubAgentCommission = Double.parseDouble(vecTemp.get(16));
                  dblVATAgentCommission = Double.parseDouble(vecTemp.get(17));
                  dblVATISOCommission = Double.parseDouble(vecTemp.get(18));
                  if((SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  
                  		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                  			DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
	                  dblBonus = Double.parseDouble(vecTemp.get(23));
	                  dblTotalRe = Double.parseDouble(vecTemp.get(24));
                  }
                  else
                  {
	                  dblBonus = Double.parseDouble(vecTemp.get(19));
	                  dblTotalRe = Double.parseDouble(vecTemp.get(20));
                  }
                  if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                    {
                      dblTaxAmount = Double.parseDouble(vecTemp.get(26));
                    }
                    
                 
                  	if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
				  	{
				  	   if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				  	   {
				  	        strRouteName =  "<td>" + vecTemp.get(25) + "</td>";	
					      	strCity =  "<td>" + vecTemp.get(20) + "</td>";	
				  	   
				  	   }else
				  	   {
					      	strRouteName =  "<td>" + vecTemp.get(21) + "</td>";	
					      	strCity =  "<td>" + vecTemp.get(22) + "</td>";	
				      	}	 
				  	}                 
                  	
                  	StringBuilder sb = new StringBuilder();
                  	sb.append("<tr class=row" + intEvenOdd + ">");
                  	sb.append("<td>" + intCounter++ + "</td>");
                  	sb.append("<td nowrap>" + vecTemp.get(0) + "</td>");
                  	sb.append("<td>" + vecTemp.get(1) + "</td>");
                        if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
			 {
                             sb.append("<td>" + vecTemp.get(28) + "</td>");
                             sb.append("<td>" + vecTemp.get(29) + "</td>");
                             sb.append("<td>" + vecTemp.get(30) + "</td>");
                         }
                  	if(strAccessLevel.equals(DebisysConstants.ISO)){
                  		if(deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
                                        !DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ){
                  			sb.append("<td>" + vecTemp.get(27) + "</td>");
                  		} else {
                  			sb.append("<td>" + vecTemp.get(23) + "</td>");
                  		}
                  	}
                        
                        String useS2kRatePlans = "";
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS)) {
                                useS2kRatePlans = "&s2kRatePlans=" + request.getParameter("s2kRatePlans");
                            }
                        
                  	sb.append(strRouteName + strCity);
                  	sb.append("<td align=\"right\">" + (int)dblTotalQty + "</td>");
                  	sb.append("<td align=right><a href=\"admin/transactions/merchants_transactions.jsp?startDate="); 
                  	sb.append(URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate="); 
                  	sb.append(URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&search=y&merchantId=");
                  	sb.append(vecTemp.get(1) + "&report=y&chkUseTaxValue=");
                  	sb.append(request.getParameter("chkUseTaxValue") + useS2kRatePlans+"\" target=\"_blank\">");
                        
                        
                        
                  	sb.append(NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
                  	out.print(sb.toString());
                        		
                  if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                  DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
                  {
                	  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>" 
                	  				+ "<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTotalRe)) + "</td>");
                  }
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATTotalSales)) + "</td>");
                   if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                    {
                     out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAmount)) + "</td>");
                    }
                  
                }
                else
                {
                	dblBonus = Double.parseDouble(vecTemp.get(17));
                    dblTotalRe = Double.parseDouble(vecTemp.get(18));
                  	String sLocation = "";
                  	//
                  	if ( (strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) || strAccessLevel.equals(DebisysConstants.CARRIER) )
                  	{
                    	sLocation = "<td>" + vecTemp.get(13) + "</td><td>" + vecTemp.get(14) + "</td><td>" + vecTemp.get(15) + "</td><td>" + vecTemp.get(16) + "</td>";
                  	}

                  	if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
				  	{
				      	strRouteName =  "<td>" + vecTemp.get(19) + "</td>";	
				      	strCity =  "<td>" + vecTemp.get(14) + "</td>";		 
				  	}                    

                  	StringBuilder sb = new StringBuilder();
                	sb.append("<tr class=row" + intEvenOdd + ">");
                	sb.append("<td>" + intCounter++ + "</td>");
                	sb.append("<td nowrap>" + vecTemp.get(0) + "</td>");
                	sb.append(sLocation); 
                	sb.append("<td>" + vecTemp.get(1) + "</td>");
                         if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
			 {
                             sb.append("<td>" + vecTemp.get(21) + "</td>");
                             sb.append("<td>" + vecTemp.get(22) + "</td>");
                             sb.append("<td>" + vecTemp.get(23) + "</td>");
                         }
                	if(strAccessLevel.equals(DebisysConstants.ISO)){
                		sb.append("<td>" + vecTemp.get(20) + "</td>");
                	}
                        
                        String useS2kRatePlans = "";
                            if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS)) {
                                useS2kRatePlans = "&s2kRatePlans=" + request.getParameter("s2kRatePlans");
                            }
                            
                	sb.append(strRouteName + strCity);
                	sb.append("<td align=\"right\">" + (int)dblTotalQty + "</td>");
                	sb.append("<td align=right><a href=\"admin/transactions/merchants_transactions.jsp?startDate=");
                	sb.append(URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8"));
                	sb.append("&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(),"UTF-8"));
                	sb.append("&search=y&merchantId=" + vecTemp.get(1));
                	sb.append("&report=y"+useS2kRatePlans+"\" target=\"_blank\">");
                	sb.append(NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
                	out.print(sb.toString());
                	
                	if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                	DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
                	{
                		out.println("<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>" +
                    		"<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblTotalRe)) + "</td>");
                	}
                
                }
                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommission)) + "</td>");
                  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATISOCommission)) + "</td>");
                  }
                }

                if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || 
                		strAccessLevel.equals(DebisysConstants.CARRIER)) && 
                         strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommission)) + "</td>");
                   if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                   		 DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                   		   DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATAgentCommission)) + "</td>");
                  }
                }

                if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommission)) + 
                          "</td>");
                   if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                   			DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                   				DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATSubAgentCommission)) + "</td>");
                  }
                }

                if (!strAccessLevel.equals(DebisysConstants.MERCHANT) || strAccessLevel.equals(DebisysConstants.CARRIER) )
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommission)) + "</td>");
                   if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                   			DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                   				DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATRepCommission)) + "</td>");
                  }
                }

                out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommission)) + "</td>");

                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  
                 	   DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
                 	   	DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATMerchantCommission)) + "</td>");
                }


                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmount)) + "</td>");
                }

                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(9)) + "</td>" + 
                        "<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(10)) + "</td>" + "<td align=right>" + 
                        NumberUtil.formatCurrency(vecTemp.get(11)) + "</td>" + "</tr>");

                if (intEvenOdd == 1)
                {
                  intEvenOdd = 2;
                }
                else
                {
                  intEvenOdd = 1;
                }
              }
            }
%>
            <tfoot>
              <tr class=row<%= intEvenOdd %>>
<%
                if ( (strAccessLevel.equals(DebisysConstants.ISO) && 
                	   DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) || strAccessLevel.equals(DebisysConstants.CARRIER) )
                {
                    if(SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)){
%>
                <td colspan=10 align=right>
<%            }else{%>
                    <td colspan=7 align=right>
               <% }}
                else
                {
                	if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
			{ 
                                            int colspan=0;
                                   if(SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)){
                		 colspan = strAccessLevel.equals(DebisysConstants.ISO) ? 9 : 8;
                                     }else{
                                       colspan = strAccessLevel.equals(DebisysConstants.ISO) ? 6 : 5;
                                     }
%>
                		<td colspan=<%=colspan%> align=right>
<%
                	 }
                	else
                	{
                		     int colspan=0;
                                   if(SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)){
                		  colspan = strAccessLevel.equals(DebisysConstants.ISO) ? 7 : 6;
                                   }else{
                                        colspan = strAccessLevel.equals(DebisysConstants.ISO) ? 4 : 3;
                                     }

%>
                		<td colspan=<%=colspan%> align=right>
<% 
                	}
                }
%>
                  <%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                </td>
				<td align="right"><%= (int)dblTotalQtySum %></td>
				<td align=right><%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %></td>
<%
				if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
					 DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
				{
					
%>
				<td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalBonus)) %>
                </td>
				<td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalRecharge)) %>
                </td>
<%
				}
                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  
                 		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                 			DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
%>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblVATTotalSalesSum)) %>
                </td>
<%
                }
%>
<%
		        if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  
		      		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
		      			 DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		       {
 %>
		<td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalTaxAmountSum)) %>
                </td>
<%
                }
%>
<%
                if ( strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommissionSum)) + "</td>");
                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                 	DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                 		DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATISOCommissionSum)) + "</td>");
                  }
                }

                if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) || strAccessLevel.equals(DebisysConstants.CARRIER) )
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommissionSum)) + 
                          "</td>");
                  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                  		 DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                  		 	DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATAgentCommissionSum)) + "</td>");
                  }
                }

                if ( (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommissionSum)) + 
                          "</td>");
                 if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATSubAgentCommissionSum)) + "</td>");
                  }
                }

                if ( !strAccessLevel.equals(DebisysConstants.MERCHANT) || strAccessLevel.equals(DebisysConstants.CARRIER) )
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommissionSum)) + "</td>")
                          ;
                  if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATRepCommissionSum)) + "</td>");
                  }
                }
%>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblMerchantCommissionSum)) %>
                </td>
<%
                if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&  
                		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
                			 DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATMerchantCommissionSum)) + "</td>");
                }
                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmountSum)) + "</td>");
                }
%>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
              </tr>
            </tfoot>
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
        	System.out.println("Order: " + sortString);
%>
          <SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  [<%= sortString %>, "Number", "Number", "Number"],0,false,false);
                    
  -->
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
