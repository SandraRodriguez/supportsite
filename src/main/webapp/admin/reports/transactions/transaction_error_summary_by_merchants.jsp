<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.reports.ReportsUtil,
                 com.debisys.utils.ColumnReport,
                 com.debisys.utils.NumberUtil, com.debisys.schedulereports.ScheduleReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 4;
  int section_page = 18;
  String products = "";
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";

  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();
  String noteTimeZone = "";
  String testTrx = "";  
  String keyLanguage = "jsp.admin.reports.title33";
  String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
  
  //jsp.admin.error_summary_report
  
  Vector vTimeZoneData = null;
  if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
  {
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
  }
  else
  {
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  }
  noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
  String titleReportResults = "";
  if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
  { 
     titleReportResults = Languages.getString(keyLanguage,SessionData.getLanguage()) + " "+Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
	 titleReportResults = titleReportResults + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );                     
  }
                	    	  	
  if ( request.getParameter("Download") != null )
  {
	  TransactionReport.setMerchantIds(request.getParameter("MerchantIDs"));
	  TransactionReport.setProductIds(request.getParameter("products"));	  
	  String sURL = TransactionReport.downloadTransactionErrorSummaryByMerchant(application, SessionData);
	  response.sendRedirect(sURL);
	  return;
  }
  
  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));

      String strMerchantIds[] = request.getParameterValues("mids");

      if (strMerchantIds != null)
      {
        TransactionReport.setMerchantIds(strMerchantIds);
      }

	  String strProductIds[] = request.getParameterValues("pids");

      if (strProductIds != null)
      {
        TransactionReport.setProductIds(strProductIds);
        products = String.valueOf(strProductIds);
      }
      
      //////////////////////////////////////////////////////////////////
	  //HERE WE DEFINE THE REPORT'S HEADERS 
	  headers = getHeadersTrxSummErrorMerchant( SessionData );
	  //////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////
	  
      if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	  {
	  		//TO SCHEDULE REPORT
	  		titles.add(noteTimeZone);
      		titles.add(testTrx);
	  		titles.add(titleReport);
			TransactionReport.getTransactionErrorSummaryByMerchants(SessionData, DebisysConstants.SCHEDULE_REPORT, headers, titles);
			ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
			if (  scheduleReport != null  )
			{
				scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
				scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
				scheduleReport.setTitleName( keyLanguage );   
			}	
			response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
	  }
	  else
	  {
      	//vecSearchResults = TransactionReport.getTransactionErrorSummaryByMerchants(SessionData);
      	vecSearchResults = TransactionReport.getTransactionErrorSummaryByMerchants(SessionData, DebisysConstants.EXECUTE_REPORT, headers, titles);
      }
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>

<%!public static ArrayList<ColumnReport> getHeadersTrxSummErrorMerchant(SessionData sessionData )
	{
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.dba", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("qty", Languages.getString("jsp.admin.reports.success", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		headers.add(new ColumnReport("total_sales", Languages.getString("jsp.admin.reports.total", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		headers.add(new ColumnReport("error_count", Languages.getString("jsp.admin.reports.errors", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		headers.add(new ColumnReport("errorPercent", Languages.getString("jsp.admin.reports.error_percent", sessionData.getLanguage()).toUpperCase(), Double.class, false));
						
		return headers;
	}  
  %>
  <table border="0" cellpadding="0" cellspacing="0" width="550">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="3000">
        &nbsp;
        <%= titleReport.toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        if (vecSearchResults != null && vecSearchResults.size() > 1)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr class="main"><td nowrap colspan="2"><%=noteTimeZone%><br/><br/></td>
            </tr>
            <tr><td class="main"><%= titleReportResults %><br>
            <table>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>
                <FORM ACTION="admin/reports/transactions/transaction_error_summary_by_merchants.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                	<INPUT TYPE=hidden NAME=MerchantIDs VALUE="<%=TransactionReport.getMerchantIds()%>">                	
                	<INPUT TYPE=hidden NAME=Download VALUE="Y">
                	<% 
                    if (products != null && !products.trim().equals(""))
    				{%>
    					<input type=hidden name="products" id="products" value="<%=TransactionReport.getProductIds()%>">
    				<%}
    				%>
                	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                </FORM>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                <FORM ACTION="admin/reports/transactions/print_transaction_error_summary_by_merchants.jsp" METHOD=post TARGET=blank>
                	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                	<INPUT TYPE=hidden NAME=MerchantIDs VALUE="<%=TransactionReport.getMerchantIds()%>">
                	<% 
                    if (products != null && !products.trim().equals(""))
    				{%>
    					<input type=hidden name="products" value="<%=TransactionReport.getProductIds()%>">
    				<%}
    				%>
                	<INPUT ID=btnPrint TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.print",SessionData.getLanguage())%>">
                </FORM>
                </td>
              </tr>
            </table>
                <br>
                <font color="#ff0000">
                  <%= Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage()) %>
                </font>
                <br>
                <%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2 width="4%">
                  #
                </td>
                <% 
                for(ColumnReport column : headers)
                {
                	String columnDescription = column.getLanguageDescription();
                %>
                	<td class=rowhead2 nowrap width="35%"><%= columnDescription %></td>
                <% 
                }
    			%>                
              </tr>
            </thead>
<%
            double   dblTotalSalesSum         = 0;
            double   dblErrorCountSum         = 0;
            double   dblErrorPercentageSum    = 0;
            int      intTotalQtySum           = 0;
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;

            if (it.hasNext())
            {
              Vector vecTemp = null;

              vecTemp = (Vector)it.next();
              dblTotalSalesSum = Double.parseDouble(vecTemp.get(1).toString());
              intTotalQtySum = Integer.parseInt(vecTemp.get(0).toString());
              dblErrorCountSum = Double.parseDouble(vecTemp.get(2).toString());
              dblErrorPercentageSum = Double.parseDouble(vecTemp.get(3).toString());

              while (it.hasNext())
              {
                vecTemp = null;
                vecTemp = (Vector)it.next();

                int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
                double dblTotalSales         = Double.parseDouble(vecTemp.get(3).toString());
                double dblTotalErrorCountSum         = Double.parseDouble(vecTemp.get(4).toString());
                
                out.print("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + "<td nowrap>" + vecTemp.get(
                        0) + "</td>" + "<td>" + vecTemp.get(1) + "</td>" + "<td>" + intTotalQty + "</td>" + 
                        "<td align=right><a href=\"admin/transactions/merchants_transactions.jsp?startDate=" + URLEncoder.encode(
                        TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), 
                        "UTF-8") + "&search=y&merchantId=" + vecTemp.get(1) +"&products="+TransactionReport.getProductIds()+ "&report=y&printbtn=\" target=\"_blank\">" + 
                        NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");

              
                out.print("<td align=right><a href=\"admin/reports/transactions/transaction_error_details.jsp?startDate=" 
                          + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(
                          TransactionReport.getEndDate(), "UTF-8") + "&merchantId=" + vecTemp.get(1) +"&products="+TransactionReport.getProductIds()+ 
                          "&printbtn=&resultCodes=" + TransactionReport.getResultCodes() + "\" target=\"_blank\">" + 
                          vecTemp.get(4).toString() + "</a></td>" + "<td align=right>" + vecTemp.get(5).toString() + "%</td></tr>");
                
                if (intEvenOdd == 1)
                {
                  intEvenOdd = 2;
                }
                else
                {
                  intEvenOdd = 1;
                }
              }              
            }
%>
            <tfoot>
              <tr class=row<%= intEvenOdd %>>
                <td colspan=3 align=right>
                  <%= Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()) %>:
                </td>
                <td align=left>
                  <%= intTotalQtySum %>
                </td>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %>
                </td>
<%
                out.println("<td align=right><a href=\"admin/reports/transactions/transaction_error_summary_by_types.jsp?startDate=" + URLEncoder.encode(
                        TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + 
                        "&merchantIds=" + TransactionReport.getMerchantIds() + 
                        "&resultCodes=" + TransactionReport.getResultCodes() +
                        "&pids="+TransactionReport.getProductIds()+ 
                        "&search=y\" target=\"_blank\">" + 
                        Integer.toString((int)dblErrorCountSum) + "</a>" +
                  "<td align=right>" +
                    NumberUtil.formatAmount(Double.toString(dblErrorPercentageSum))+ 
                  "%</td>");                
%>
              </tr>
            </tfoot>
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 1 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
            
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number"],0,false,false);
  -->
            
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
