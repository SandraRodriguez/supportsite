<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.languages.*,
                 com.debisys.utils.HTMLEncoder" %>
<%
int section=4;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;

String queryType = null;

  boolean isInternal = Boolean.FALSE;
  
    if(request.getParameter("isInternal") != null) {            
         if(request.getParameter("isInternal") == "true" || request.getParameter("isInternal").equalsIgnoreCase("true")) {
             isInternal = Boolean.TRUE;
         }
    }

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }
  if (request.getParameter("queryType") != null)
  {
    queryType = request.getParameter("queryType");
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  if (TransactionReport.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));

    String products = request.getParameter("products");
    if (products != null && !products.trim().equals(""))
    {
        TransactionReport.setProductIds(products);
    }
    String providersIds = request.getParameter("providersIds");
    if (providersIds != null && !providersIds.trim().equals(""))
    {
        TransactionReport.setProviderIDs(providersIds);
    }
    vecSearchResults = TransactionReport.getTransactionErrorDetailByTypes(1, 1000000, SessionData, false, queryType, isInternal);
    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize);
      if ((intPageCount * intPageSize) < intRecordCount)
      {
        intPageCount++;
      }
    }
    String sURL = com.debisys.transactions.TransactionSearch.downloadErrorDetailReport(application, SessionData, vecSearchResults);
    response.sendRedirect(sURL);
    return;
  }
  else
  {
    searchErrors = TransactionReport.getErrors();
  }
%>
