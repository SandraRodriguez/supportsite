<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="com.debisys.reports.schedule.PinRemainingInventoryReport"%>
<%@ page import="java.net.URLEncoder,  
         com.debisys.utils.HTMLEncoder,
         java.util.*, 
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.users.User"%>
<%
    int section = 4;
    int section_page = 33;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<%    int intPage = 1;
    int intPageSize = 50;
    int intPageCount = 1;
    int intRecordCount = 0;
    Hashtable searchErrors = null;
    Vector vecSearchResults = new Vector();
    String reportTitleKey = "jsp.admin.reports.pin_remaining_inventory_report";
    String reportTitle = Languages.getString(reportTitleKey, SessionData.getLanguage()).toUpperCase();
    Date currentDate = Calendar.getInstance().getTime();
    String strCurrentDate = com.debisys.utils.DateUtil.formatSqlDateTime(currentDate);
    String strProductID = request.getParameter("pid");
    long productId = ((strProductID != null) && (!strProductID.equals(""))) ? Long.valueOf(strProductID) : 0L;
    Vector vecProduct = TransactionReport.getProductData(SessionData, strProductID);
    String strProductSelected = (vecProduct.size() > 0) ? vecProduct.get(1) + " (" + vecProduct.get(0) + ")" : "";
    if (request.getParameter("page") != null) {
        try {
            intPage = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException ex) {
            intPage = 1;
        }
    }
    if (intPage < 1) {
        intPage = 1;
    }

    PinRemainingInventoryReport report = new PinRemainingInventoryReport(SessionData, application, false,
            reportTitle, strCurrentDate, SessionData.getUser().getCompanyName(), productId, strProductSelected,
            true, intPage, intPageSize);

    if (request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y")) {
        report.setScheduling(true);
        if (report.scheduleReport()) {
            ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
            if (scheduleReport != null) {
                scheduleReport.setStartDateFixedQuery(strCurrentDate);
                scheduleReport.setEndDateFixedQuery(strCurrentDate);
                scheduleReport.setTitleName(reportTitleKey);
            }
            response.sendRedirect(DebisysConstants.PAGE_TO_SCHEDULE_REPORTS);
        }
    } else if ((request.getParameter("search") != null) && (request.getParameter("search").toLowerCase().equals("y"))) {

        if (TransactionReport.validateProductId(SessionData, strProductID)) {
            report.setPaging(true);
            report.setScheduling(false);
            vecSearchResults = report.getResults();
            intRecordCount = report.getRecordCount();
            if (intRecordCount > 0) {
                intPageCount = (intRecordCount / intPageSize);
                if ((intPageCount * intPageSize) < intRecordCount) {
                    intPageCount++;
                }
            }
        } else {
            searchErrors = TransactionReport.getErrors();
        }

    } else if (request.getParameter("downloadReport") != null && request.getParameter("downloadReport").equals("y")) {
        report.setPaging(false);
        report.setScheduling(false);
        String zippedFilePath = report.downloadReport();
        if ((zippedFilePath != null) && (!zippedFilePath.trim().equals(""))) {
            response.sendRedirect(zippedFilePath);
        }
    }


%>

<SCRIPT LANGUAGE="JavaScript">
    var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ", "< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");

    function scroll2()
    {
        document.downloadform.submit.disabled = true;
        document.downloadform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll2()', 150);
    }
</SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=reportTitle.toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">

            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>                        
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="formArea2">
                                    <%
                                        if (searchErrors != null) {
                                            out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"
                                                    + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");
                                            Enumeration enum1 = searchErrors.keys();
                                            while (enum1.hasMoreElements()) {
                                                String strKey = enum1.nextElement().toString();
                                                String strError = (String) searchErrors.get(strKey);
                                                out.println("<li>" + strError);
                                            }
                                            out.println("</font></td></tr></table>");
                                        } else {
                                    %>                 
                                    <table width="100%">
                                        <tr class="main">
                                            <td width="50%" valign="top">
                                                <%=Languages.getString("jsp.admin.reports.pin_remaining_inventory.report_type_title_date", SessionData.getLanguage())%> 
                                                <BR>
                                                <%=new java.text.SimpleDateFormat(DateUtil.getDateFormatString() + " HH:mm:ss.SSS").format(new java.util.Date())%>					                                            
                                            </td>
                                            <td><%=Languages.getString("jsp.admin.iso_name", SessionData.getLanguage())%>:&nbsp;<%=SessionData.getUser().getCompanyName()%></td>
                                        </tr>
                                        <tr class="main">
                                            <td >Product:
                                                <BR>
                                                <%=strProductSelected%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%} %>

                                    <%
                                        if (strProductID != null) {

                                            if (vecSearchResults != null && vecSearchResults.size() > 0) {
                                    %>
                                    <br>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                        <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found", SessionData.getLanguage()) + " "%>. 
                                                <%=Languages.getString("jsp.admin.displaying", new Object[]{Integer.toString(intPage), Integer.toString(intPageCount)}, SessionData.getLanguage())%></td></tr>
                                        <tr>
                                            <td align=left class="main" nowrap>
                                                <form name="downloadform" method=post action="admin/reports/transactions/pin_remaining_inventory_summary.jsp" onSubmit="document.getElementById('btnSubmit').disabled = true;">
                                                    <input type="hidden" name="page" value="<%=intPage%>">
                                                    <input type="hidden" name="section_page" value="<%=section_page%>">
                                                    <input type="hidden" name="downloadReport" value="y">
                                                    <input type="hidden" name="search" value="n">
                                                    <input type="hidden" id="pid" name="pid" value="<%=strProductID%>"> 
                                                    <input type=submit id=btnSubmit value="<%=Languages.getString("jsp.admin.reports.downloadToCsv", SessionData.getLanguage())%>">
                                                </form>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align=right class="main" nowrap>
                                                <%
                                                    if (intPage > 1) {
                                                        //out.println("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&pid=" + URLEncoder.encode(strProductID, "UTF-8") + "&page=1\">" + Languages.getString("jsp.admin.first", SessionData.getLanguage()) + "</a>&nbsp;");
                                                        out.println(String.format("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=%s&pid=%s&page=1\">%s</a>&nbsp;",
                                                                URLEncoder.encode(request.getParameter("search"), "UTF-8"),
                                                                URLEncoder.encode(strProductID, "UTF-8"),
                                                                Languages.getString("jsp.admin.first", SessionData.getLanguage())));

                                                        //out.println("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&pid=" + URLEncoder.encode(strProductID, "UTF-8") + "&page=" + (intPage - 1) + "\">&lt;&lt;" + Languages.getString("jsp.admin.previous", SessionData.getLanguage()) + "</a>&nbsp;");
                                                        out.println(String.format("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=%s&pid=%s&page=%d\">&lt;&lt;%s</a>&nbsp;",
                                                                URLEncoder.encode(request.getParameter("search"), "UTF-8"),
                                                                URLEncoder.encode(strProductID, "UTF-8"),
                                                                (intPage - 1),
                                                                Languages.getString("jsp.admin.previous", SessionData.getLanguage())));
                                                    }
                                                    int intLowerLimit = intPage - 12;
                                                    int intUpperLimit = intPage + 12;

                                                    if (intLowerLimit < 1) {
                                                        intLowerLimit = 1;
                                                        intUpperLimit = 25;
                                                    }

                                                    for (int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) {
                                                        if (i == intPage) {
                                                            out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                                                        } else {
                                                            //out.println("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&pid=" + URLEncoder.encode(strProductID, "UTF-8") + "&page=" + i + "\">" + i + "</a>&nbsp;");
                                                            out.println(String.format("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=%s&pid=%s&page=%d\">%d</a>&nbsp;",
                                                                    URLEncoder.encode(request.getParameter("search"), "UTF-8"),
                                                                    URLEncoder.encode(strProductID, "UTF-8"),
                                                                    i,
                                                                    i));
                                                        }
                                                    }

                                                    if (intPage <= (intPageCount - 1)) {
                                                        //out.println("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&pid=" + URLEncoder.encode(strProductID, "UTF-8") + "&page=" + (intPage + 1) + "\">" + Languages.getString("jsp.admin.next", SessionData.getLanguage()) + "&gt;&gt;</a>&nbsp;");
                                                        out.println(String.format("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=%s&pid=%s&page=%d\">%s&gt;&gt;</a>&nbsp;",
                                                                URLEncoder.encode(request.getParameter("search"), "UTF-8"),
                                                                URLEncoder.encode(strProductID, "UTF-8"),
                                                                (intPage + 1),
                                                                Languages.getString("jsp.admin.next", SessionData.getLanguage())));

                                                        //out.println("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&pid=" + URLEncoder.encode(strProductID, "UTF-8") + "&page=" + (intPageCount) + "\">" + Languages.getString("jsp.admin.last", SessionData.getLanguage()) + "</a>");
                                                        out.println(String.format("<a href=\"admin/reports/transactions/pin_remaining_inventory_summary.jsp?search=%s&pid=%s&page=%d\">%s</a>",
                                                                URLEncoder.encode(request.getParameter("search"), "UTF-8"),
                                                                URLEncoder.encode(strProductID, "UTF-8"),
                                                                (intPageCount),
                                                                Languages.getString("jsp.admin.last", SessionData.getLanguage())));
                                                    }

                                                %>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
                                        <thead>
                                            <tr class="SectionTopBorder">
                                                <td class=rowhead2 align=center>#
                                                </td>

                                                <%                                                    // Set the column headers
                                                    ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
                                                    if ((reportColumns != null) && (reportColumns.size() > 0)) {
                                                        for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                                                %>                    
                                                <td class=rowhead2 nowrap align=center><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
                                                </td>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </tr>
                                        </thead>	
                                        <%
                                            Iterator itResults = vecSearchResults.iterator();
                                            int intEvenOdd = 1;
                                            int count = 1;
                                            while (itResults.hasNext()) {
                                                Vector vecTemp = null;
                                                vecTemp = (Vector) itResults.next();
                                                out.println("<tr class=row" + intEvenOdd + ">");
                                                out.println("<td>" + (count++) + "</td>");
                                                out.println("<td nowrap>" + vecTemp.get(0) + "</td>");
                                                out.println("<td>" + vecTemp.get(1) + "</td>");
                                                out.println("</tr>");

                                                if (intEvenOdd == 1) {
                                                    intEvenOdd = 2;
                                                } else {
                                                    intEvenOdd = 1;
                                                }
                                            }
                                            vecSearchResults.clear();
                                        %>
                                        <SCRIPT type="text/javascript">
                                            <!--
                                                var stT1 = new SortROC(document.getElementById("t1"),
                                                    ["None", "Integer", "DateTime"],
                                                    0, false, false);
-->            
                                        </SCRIPT>
                                    </table>
                                    <%
                                        } else if ((vecSearchResults == null) || ((vecSearchResults.size() == 0) && (request.getParameter("search") != null))) {
                                            out.println("<br><br><font color=ff0000>"
                                                    + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                                        }
                                    %>
                                    <%
                                        }
                                    %>    

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>                                
            </table>
        </td>
    </tr>  
</table>

<%@ include file="/includes/footer.jsp" %>                        