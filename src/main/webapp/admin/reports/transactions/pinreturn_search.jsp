<%@ page import="com.debisys.languages.Languages,
				 com.debisys.customers.Merchant,
				 com.debisys.reports.TransactionReport,
				 com.debisys.utils.StringUtil,
				 com.debisys.utils.DateUtil,
				 java.util.Arrays,
				 java.util.Vector,
				 java.util.Iterator,
				 java.util.Enumeration,
				 com.debisys.pinreturn.*,
				 java.util.ArrayList,
				 com.debisys.pinreturn.VoidList" %>
<%
int section = 9;
int section_page = 1;
%> 
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script>
function openDetails(nCaseNumber)
{
  var sURL = "/support/admin/reports/transactions/pinreturn_searchdetails.jsp?caseNumber=" + nCaseNumber;
  var sOptions = "left=" + (screen.width - (screen.width * 0.6))/2 + ",top=" + (screen.height - (screen.height * 0.7))/2 + ",width=" + (screen.width * 0.6) + ",height=" + (screen.height * 0.7) + ",location=no,menubar=no,resizable=yes";
  var w = window.open(sURL, "_blank", sOptions, true);
  w.focus();
}
</script>
<!-- 
<script language="JavaScript1.2">
var isNN4up = (window.Event)? true : false;

function key_press_event_handler(e) {	
	if (isNN4up) {
	   var whichKey = e.which;
	} else {
	   var whichKey = window.event.keyCode;
	}
	var realKey = String.fromCharCode(whichKey);
	window.status = 'Pressed ' + realKey + ' (Key code: ' + whichKey + ')';
	//alert('You pressed ' + realKey + ' (Key code: ' + whichKey + ')');
    if (whichKey == 0 || whichKey == 114) {
        return false;
    }
}
// Register event handler for NNav
if (isNN4up) {
    document.captureEvents(Event.KEYPRESS);
}
// Register event handler for MSIE
document.onkeypress = key_press_event_handler;
</script> -->
<%
Vector vResults = new Vector();
if ( request.getParameter("frmApply") != null )
{//If a change in credit/recycle/reject was made
	try
	{
		boolean bResultsProcessed = false;		
		Enumeration enParams = request.getParameterNames();
		Vector vecDuplicates = new Vector();
		int nCounter = 0;
%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
<%
		int nRow = 1;
		int nCreditedId = 0;
		int nNotCreditedId = 0;
		int nCreditedNotRecycledId = 0;
		int nRejectedByACED = 3;

		//Retrieve IDs for Credited and NotCredited status
		Vector vecStatusList = TransactionReport.getPINRequestStatus(true);
		for ( int i = 0; i < vecStatusList.size(); i++ )
		{
			Vector vTemp = (Vector)vecStatusList.get(i);
			if ( vTemp.get(2).toString().equals("ISO_CREDITED") )
			{
				nCreditedId = Integer.parseInt(vTemp.get(0).toString());
			}
			else if ( vTemp.get(2).toString().equals("ISO_NOTCREDITED") )
			{
				nNotCreditedId = Integer.parseInt(vTemp.get(0).toString());
			}
			else if ( vTemp.get(2).toString().equals("ISO_CREDITED_NOT_RECYCLED") )
			{
				nCreditedNotRecycledId = Integer.parseInt(vTemp.get(0).toString());
			}			
		}

		//First loop to count how many items has this enum
		while ( enParams.hasMoreElements() )
		{
			if ( enParams.nextElement().toString().startsWith("chk_") )
			{
				nCounter++;
			}
		}
		String sParams[] = new String[nCounter];
		enParams = request.getParameterNames();
		nCounter = 0;

		//Second loop to store tokens
		while ( enParams.hasMoreElements() )
		{
			String sToken = enParams.nextElement().toString();
			if ( sToken.startsWith("chk_") )
			{
				sParams[nCounter++] = sToken;
			}
		}

		Arrays.sort(sParams);//Now sort the tokens

		for ( nCounter = 0; nCounter < sParams.length; nCounter++ )
		{//Thru request params
			String sNextId = sParams[nCounter];
			if ( sNextId.startsWith("chk_") )
			{//If user made a change in results
				boolean bCredit = false;
				boolean bRecycle = false;

				if ( !bResultsProcessed )
				{
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea">
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.casenumber",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.sku",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.pin",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.credit",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.recycle",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.result",SessionData.getLanguage()).toUpperCase()%></td>
					</tr>
				</thead>
<%
					bResultsProcessed = true;
				}
				
				int nCaseNumber = Integer.parseInt(sNextId.replaceAll("chk_", ""));
				int nProductId = Integer.parseInt(request.getParameter("sku_" + Integer.toString(nCaseNumber)));
				String sMillennium_no = request.getParameter("mil_" + Integer.toString(nCaseNumber));
				String sPIN = request.getParameter("pin_" + Integer.toString(nCaseNumber));
				String sControlNo = request.getParameter("con_" + Integer.toString(nCaseNumber));
				String sRecID = request.getParameter("rec_" + Integer.toString(nCaseNumber));
				String srec_ACED = request.getParameter("rec_ACED_" + Integer.toString(nCaseNumber));

				if ( request.getParameter(sNextId).equals("cr") )
				{
					bCredit = true;
					bRecycle = true;
				}
				else if ( request.getParameter(sNextId).equals("cdr") )
				{
					bCredit = true;
					bRecycle = false;
				}
%>
				<tr class="row<%=nRow%>">
					<td align="right"><a href="javascript:void(0);" onclick="openDetails(<%=nCaseNumber%>);"><%=nCaseNumber%></a></td>
					<td align="right"><%=nProductId%></td>
					<td align="right"><%=sPIN%></td>
					
<%
				if ( srec_ACED == null )
				{
					%>
					<td align="center"><%=(bCredit)?Languages.getString("jsp.admin.reports.pinreturn_search.yes",SessionData.getLanguage()):Languages.getString("jsp.admin.reports.pinreturn_search.no",SessionData.getLanguage())%></td>
					<td align="center"><%=(bRecycle)?Languages.getString("jsp.admin.reports.pinreturn_search.yes",SessionData.getLanguage()):Languages.getString("jsp.admin.reports.pinreturn_search.no",SessionData.getLanguage())%></td>
					<% 				
					//Check if the current item to process has been already marked as duplicate
					if ( !TransactionReport.isPINRequestInDuplicates(nCaseNumber, vecDuplicates) )
					{
						Vector vTemp = TransactionReport.checkDuplicatePINRequests(nCaseNumber, sMillennium_no, sControlNo,SessionData);
						for ( int i = 0; i < vTemp.size(); i++ )
						{
							vecDuplicates.add(vTemp.get(i).toString());
						}                    
	
						//Check if the last transaction of this item is a sale
						if ( TransactionReport.checkLastTrxIsSale(sControlNo) )
						{
							if ( TransactionReport.doCreditRecycleForPINRequest(nProductId, sPIN, bCredit, bRecycle, sMillennium_no) )
							{
								boolean bUpdateResult = false;
								if ( bCredit && bRecycle )
								{
									bUpdateResult = TransactionReport.updatePINReturnRequest(nCaseNumber, nCreditedId);
								}
								else if ( bCredit && !bRecycle )
								{
									bUpdateResult = TransactionReport.updatePINReturnRequest(nCaseNumber, nCreditedNotRecycledId);
								}
								else
								{
									bUpdateResult = TransactionReport.updatePINReturnRequest(nCaseNumber, nNotCreditedId);
									String clerk = TransactionReport.getTransactionClerk(sControlNo, sRecID);
									bUpdateResult &= TransactionReport.activatePIN(Integer.parseInt(sMillennium_no), Integer.parseInt(sControlNo), 13, 0, clerk);
								}
								if ( bUpdateResult )
								{
	%>
						<td align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.successful",SessionData.getLanguage())%></td>
	<%
								}
								else
								{
	%>
						<td align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failedstatus",SessionData.getLanguage())%></td>
	<%
								}
							}
							else
							{
	%>
						<td align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failed",SessionData.getLanguage())%></td>
	<%
							}
						}
						else
						{
							TransactionReport.updatePINReturnRequest(nCaseNumber, nNotCreditedId);
	%>
						<td align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failednosale",SessionData.getLanguage())%></td>
	<%
						}
					}
					else
					{
	%>
						<td align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failedduplicate",SessionData.getLanguage())%></td>
	<%
					}
				}
				else if ( srec_ACED!=null && srec_ACED.length()>0 )
				{
					%>
					<td align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.no",SessionData.getLanguage())%></td>
					<td align="center"><%=Languages.getString("jsp.admin.reports.pinreturn_search.no",SessionData.getLanguage())%></td>
					<% 
					if ( VoidQueue.insertPINReturnRequestCommentACED(nCaseNumber, nRejectedByACED, sRecID, SessionData ) )
					{					
					%>
						<td align="center"><%=Languages.getString("jsp.admin.pin.return.messageSuccessFullyACED",SessionData.getLanguage())%></td>
					<%
					}
					else
					{
					%>
						<td align="center"><%=Languages.getString("jsp.admin.pin.return.messageFailACED",SessionData.getLanguage())%> <%=nCaseNumber%></td>
					<%
					}
				}	
%>
				</tr>
<%
				nRow = (nRow == 1)?2:1;
			}//End of if user made a change in results
		}//End of thru request params
		if ( bResultsProcessed )
		{
%>
			</table>
<%
			if ( vecDuplicates.size() > 0 )
			{
%>
			<br><br><%=Languages.getString("jsp.admin.reports.pinreturn_search.itemsduplicated",SessionData.getLanguage())%><br>
<%
				for ( int i = 0; i < vecDuplicates.size(); i++ )
				{
%>
			<a href="javascript:void(0);" onclick="openDetails(<%=vecDuplicates.get(i)%>);"><%=vecDuplicates.get(i)%></a>&nbsp;&nbsp;&nbsp;
<%
				}
			}
%>
		</td>
<%
		}
		else
		{
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class="main"><%=Languages.getString("jsp.admin.reports.pinreturn_search.noitemschanged",SessionData.getLanguage())%></td></tr>
			</table>
		</td>
<%
		}
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan="3" align="center">
			<form method="post" action="admin/reports/transactions/pinreturn_search.jsp">
				<input type="submit" class="plain" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.backtosearch",SessionData.getLanguage())%>">
			</form>
		</td>
	</tr>
</table>
<%
	}
	catch (Exception e)
	{
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b>ERROR IN PAGE</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class="main"><%=Languages.getString("jsp.admin.reports.pinreturn_search.checklog",SessionData.getLanguage())%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}
}//End of if a change in credit/recycle/reject was made
else if ( request.getParameter("defStatusId") != null )
{//If a search was made
	try
	{
		long lCaseNumber = 0;
		int nStatusId = Integer.parseInt(request.getParameter("defStatusId"));
		String[] sMerchants = new String[]{""};
		String[] sProducts = new String[]{""};

		if ( request.getParameter("caseNumber").length() > 0 )
		{
			lCaseNumber = Long.parseLong(request.getParameter("caseNumber"));
		}
		if ( request.getParameter("statusId") != null )
		{
			nStatusId = Integer.parseInt(request.getParameter("statusId"));
		}
		if ( request.getParameterValues("merchantIds") != null )
		{
			sMerchants = request.getParameterValues("merchantIds");
		}
		if ( request.getParameterValues("productIds") != null )
		{
			sProducts = request.getParameterValues("productIds");
		}
		vResults = TransactionReport.getPINReturnRequests(application, request.getParameter("startDate"), request.getParameter("endDate"), nStatusId, sMerchants, sProducts, lCaseNumber, SessionData, request.getParameter("searchByOption"));
	}
	catch (Exception e)
	{
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b>ERROR IN PAGE</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class="main"><%=Languages.getString("jsp.admin.reports.pinreturn_search.checklog",SessionData.getLanguage())%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}
%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script src="includes/sortROC.js" type="text/javascript"></script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
<%
	if ( vResults.size() > 0 )
	{//If there are results
			
		
		boolean bShowOptions = false;
		if ( vResults.size() == 1 )
		{
			bShowOptions = ((Vector)vResults.get(0)).get(9).toString().equals("ISO_FOLLOWUP");
		}
		else
		{
			bShowOptions = ( request.getParameter("defStatusId").equals(request.getParameter("statusId")) || (request.getParameter("statusId") == null) );
		}

		if ( bShowOptions )
		{
%>
		<form method="post" action="admin/reports/transactions/pinreturn_search.jsp" onsubmit="document.getElementById('btnApplyChanges').disabled=true;">
<%
		}
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class="main"><%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%></td></tr>
			</table>
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2"></td>
<%
		if ( bShowOptions )
		{
%>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.admin.reports.pinreturn_search.credit_recycle").toUpperCase().replaceAll("&NBSP;", "&nbsp;"))%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.admin.reports.pinreturn_search.credit_dontrecycle").toUpperCase().replaceAll("&NBSP;", "&nbsp;"))%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.admin.reports.pinreturn_search.reject").toUpperCase())%><br/></td>
<%
		}
%>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.casenumber",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.datetime",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.lastactiondate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.pinloaddate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.pinsaledate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.trxcount",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.businessname",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.sitenumber",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.sku",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.controlnumber",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.productdescription",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.pin",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.pinreturn_search.status",SessionData.getLanguage()).toUpperCase())%></td>
					</tr>
				</thead>
<%
		VoidList trxWithACEDVoid = VoidQueue.findLastTrxToVoid(application);
		int nRow = 1;		
		for ( int i = 0; i < vResults.size(); i++ )
		{
			Vector vTemp = (Vector)vResults.get(i);
			boolean isACED = false;
			VoidQueue voidQ = new VoidQueue();
			String rec_id = (String)vTemp.get(13);
			if ( vTemp.get(13) != null && rec_id.length()>0 )
			{				
				voidQ.setVoidTrxNumber(rec_id);
				if ( trxWithACEDVoid.contains(voidQ) )
				{
					System.out.println("ACED rec_id "+rec_id);
					isACED = true;
				}				
			}
%>
				<tr class="row<%=nRow%>" >
					<td><%=(i + 1)%></td>
<%
		if ( bShowOptions )
		{
%>
					<td align="center">
						<input type="hidden" name="mil_<%=vTemp.get(0)%>" value="<%=vTemp.get(4)%>">
						<input type="hidden" name="sku_<%=vTemp.get(0)%>" value="<%=vTemp.get(5)%>">
						<input type="hidden" name="pin_<%=vTemp.get(0)%>" value="<%=vTemp.get(8)%>">
						<input type="hidden" name="con_<%=vTemp.get(0)%>" value="<%=vTemp.get(6)%>">
						<input type="hidden" name="rec_<%=vTemp.get(0)%>" value="<%=rec_id%>">
						<input type="radio" name="chk_<%=vTemp.get(0)%>" value="cr">
						<%
						if ( isACED )
						{
						%>
							 <input type="hidden" name="rec_ACED_<%=vTemp.get(0)%>" value="<%=rec_id%>">
						<%
						}												
						%>						
					</td>
					<td align="center"><input type="radio" name="chk_<%=vTemp.get(0)%>" value="cdr"></td>					
					<td align="center"><input type="radio" name="chk_<%=vTemp.get(0)%>" value="r"></td>
<%
		}
%>
					<td align="right"><a href="javascript:void(0);" onclick="openDetails(<%=vTemp.get(0).toString()%>);"><%=vTemp.get(0).toString()%></a></td>
					<td align="right" nowrap="nowrap"><%=DateUtil.formatDateTime((java.sql.Timestamp)vTemp.get(1))%></td>
					<td align="right" nowrap="nowrap"><%=DateUtil.formatDateTime((java.sql.Timestamp)vTemp.get(14))%></td>
					<td align="right" nowrap="nowrap"><%=DateUtil.formatDateTime((java.sql.Timestamp)vTemp.get(15))%></td>
					<td align="right" nowrap="nowrap"><%=DateUtil.formatDateTime((java.sql.Timestamp)vTemp.get(16))%></td>
					<td align="right"><%=vTemp.get(2).toString()%></td>
					<td nowrap="nowrap"><%=vTemp.get(3).toString()%></td>
					<td align="right"><%=vTemp.get(4).toString()%></td>
					<td align="right"><%=vTemp.get(5).toString()%></td>
					<td align="right"><%=vTemp.get(6).toString()%></td>
					<td nowrap="nowrap"><%=vTemp.get(7).toString()%></td>
					<td align="right"><%=vTemp.get(8).toString()%></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.reports.pinreturn_search.status_" + vTemp.get(9).toString(),SessionData.getLanguage())%></td>
				</tr>
<%
			nRow = (nRow == 1)?2:1;
		}//End of for
%>
			</table>
			<script type="text/javascript">
<%
		if ( bShowOptions )
		{
%>
				var stT1 = new SortROC(document.getElementById("t1"), ["None", "None", "None", "None", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "CaseInsensitiveString", "Number", "CaseInsensitiveString"], 0, false, false);
<%
		}
		else
		{
%>
				var stT1 = new SortROC(document.getElementById("t1"), ["None", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "CaseInsensitiveString", "Number", "CaseInsensitiveString"], 0, false, false);
<%
		}
%>
			</script>
		</td>
<%
		if ( bShowOptions )
		{
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan="3" align="center">
			<table>
				<tr><td><input id="btnApplyChanges" type="submit" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.applychanges",SessionData.getLanguage())%>"></td><td>&nbsp;&nbsp;&nbsp;</td><td><input type=reset value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.resetchanges",SessionData.getLanguage())%>"></td></tr>
			</table>
		</td>
		<input type="hidden" name="frmApply" value="y">
		</form>
<%
		}
	}//End of if there are results
	else
	{//Else show the message of no results
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class="main"><%=Languages.getString("jsp.admin.reports.pinreturn_search.noresults",SessionData.getLanguage())%></td></tr>
			</table>
		</td>

<%
	}//End of else show the message of no results
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan="3" align="center">
			<form method="post" action="admin/reports/transactions/pinreturn_search.jsp">
				<input type="submit" class="plain" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.backtosearch",SessionData.getLanguage())%>">
			</form>
		</td>
	</tr>
</table>
<%
}//End of if a search was made
else
{//Else we need to show the search page
%>
<script>
  var nISO_FOLLOWUP_ID = 0;
  function ToggleDateFilters(chkAllOpen)
  {
    if ( chkAllOpen.checked )
    {
      document.getElementById('txtStartDate').readOnly = true;
      document.getElementById('txtEndDate').readOnly = true;
      document.getElementById('txtStartDate').value = '';
      document.getElementById('txtEndDate').value = '';
      document.getElementById('linkStartDate').onclick = new Function('return true;');
      document.getElementById('linkEndDate').onclick = new Function('return true;');
      document.getElementById('ddlStatus').disabled = true;
      document.getElementById('ddlStatus').value = nISO_FOLLOWUP_ID;
      document.getElementById('chkSearchByRequest').disabled = true;
      document.getElementById('chkSearchByLoad').disabled = true;
      document.getElementById('chkSearchBySale').disabled = true;
    }
    else
    {
      document.getElementById('txtStartDate').readOnly = false;
      document.getElementById('txtEndDate').readOnly = false;
      document.getElementById('linkStartDate').onclick = new Function(document.getElementById('linkStartDate')._onclick);
      document.getElementById('linkEndDate').onclick = new Function(document.getElementById('linkEndDate')._onclick);
      document.getElementById('ddlStatus').disabled = false;
      document.getElementById('chkSearchByRequest').disabled = false;
      document.getElementById('chkSearchByLoad').disabled = false;
      document.getElementById('chkSearchBySale').disabled = false;
    }
  }//End of function ToggleDateFilters
  
  function FilterCaseNumber(txtCaseNumber)
  {
    var sText = txtCaseNumber.value;
    var sResult = "";
    for ( i = 0; i < sText.length; i++ )
    {
      if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) )
      {
        sResult += sText.charAt(i);
      }
    }
    if ( sText != sResult )
    {
      txtCaseNumber.value = sResult;
    }
  }//End of function FilterCaseNumber
  
  function ValidateForm()
  {
    var sMessage = '';
    if ( !document.getElementById('chkAllOpen').checked )
    {
      if ( document.getElementById('txtStartDate').value.length == 0 )
      {
        sMessage += '<%=Languages.getString("jsp.admin.reports.pinreturn_search.emptystartdate",SessionData.getLanguage())%>\n';
      }
      else if ( !document.getElementById('txtStartDate').value.match('^(0?[1-9]|1[012])\/(0?[1-9]|1[0-9]|2[0-9]|3[01])\/(19|20)[0-9][0-9]$') )
      {
        sMessage += '<%=Languages.getString("jsp.admin.reports.pinreturn_search.badstartdate",SessionData.getLanguage())%>\n';
      }

      if ( document.getElementById('txtEndDate').value.length == 0 )
      {
        sMessage += '<%=Languages.getString("jsp.admin.reports.pinreturn_search.emptyenddate",SessionData.getLanguage())%>\n';
      }
      else if ( !document.getElementById('txtEndDate').value.match('^(0?[1-9]|1[012])\/(0?[1-9]|1[0-9]|2[0-9]|3[01])\/(19|20)[0-9][0-9]$') )
      {
        sMessage += '<%=Languages.getString("jsp.admin.reports.pinreturn_search.badenddate",SessionData.getLanguage())%>\n';
      }

      if ( sMessage.length > 0 )
      {
        alert(sMessage);
        return false;
      }
    }
    document.getElementById('btnSearch').disabled = true;
    return true;
  }//End of function ValidateForm
</script>
<form name="mainform" method="post" action="admin/reports/transactions/pinreturn_search.jsp" onsubmit="return ValidateForm();">
<table border="0" cellpadding="0" cellspacing="0" width="80%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="left" style="text-align:left;">
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td>&nbsp;</td>
							    <td class="main" nowrap="nowrap"><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
								<td>&nbsp;</td>
							    <td><input type="text" readonly="true" maxlength="10" class="plain" name="startDate" id="txtStartDate" value="" size="12"><a id="linkStartDate" href="javascript:void(0)" _onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
								<td>&nbsp;</td>
								<td rowspan="2">
									<table>
										<tr><td class="main" nowrap="nowrap"><label for="chkSearchByRequest"><input id="chkSearchByRequest" type="radio" class="plain" name="searchByOption" value="0" disabled="true" checked="checked"><%=Languages.getString("jsp.admin.reports.pinreturn_search.searchbyrequest",SessionData.getLanguage())%></label></td></tr>
										<tr><td class="main" nowrap="nowrap"><label for="chkSearchByLoad"><input id="chkSearchByLoad" type="radio" class="plain" name="searchByOption" value="1" disabled="true"><%=Languages.getString("jsp.admin.reports.pinreturn_search.searchbyload",SessionData.getLanguage())%></label></td></tr>
										<tr><td class="main" nowrap="nowrap"><label for="chkSearchBySale"><input id="chkSearchBySale" type="radio" class="plain" name="searchByOption" value="2" disabled="true"><%=Languages.getString("jsp.admin.reports.pinreturn_search.searchbysale",SessionData.getLanguage())%></label></td></tr>
									</table>
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td><input type="text" readonly="true" maxlength="10" class="plain" name="endDate" id="txtEndDate" value="" size="12"><a id="linkEndDate" href="javascript:void(0)" _onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=Languages.getString("jsp.admin.reports.pinreturn_search.allopenrequests",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td colspan="3"><input type="checkbox" class="plain" name="allopen" id="chkAllOpen" checked="checked" onclick="ToggleDateFilters(this);"></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=Languages.getString("jsp.admin.reports.pinreturn_search.status",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td colspan="3">
							    	<select name="statusId" id="ddlStatus" disabled="disabled">
						<%
						    Vector vecStatusList = TransactionReport.getPINRequestStatus(true);
						    Iterator itStatus = vecStatusList.iterator();
						    int nISO_FOLLOWUP_ID = 0;
						    while (itStatus.hasNext())
						    {
						      Vector vecTemp = null;
						      vecTemp = (Vector) itStatus.next();
						      if ( vecTemp.get(2).toString().equals("ISO_FOLLOWUP") )
						      {
						      	out.println("<option selected value=" + vecTemp.get(0) +">" + Languages.getString("jsp.admin.reports.pinreturn_search.status_" + vecTemp.get(2),SessionData.getLanguage()) + "</option>");
						        nISO_FOLLOWUP_ID = Integer.parseInt(vecTemp.get(0).toString());
						      }
						      else
						      {
						      	out.println("<option value=" + vecTemp.get(0) +">" + Languages.getString("jsp.admin.reports.pinreturn_search.status_" + vecTemp.get(2),SessionData.getLanguage()) + "</option>");
						      }
						    }
						%>
									</select>
									<input type="hidden" name="defStatusId" value="<%=nISO_FOLLOWUP_ID%>">
									<script>nISO_FOLLOWUP_ID = <%=nISO_FOLLOWUP_ID%>;</script>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=Languages.getString("jsp.admin.reports.pinreturn_search.selectmerchants",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td colspan="3">
							    	<select name="merchantIds" size="10" multiple="multiple">
										<option value="" selected="selected"><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
						<%
						    Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
						    Iterator itMerchant = vecMerchantList.iterator();
						    while (itMerchant.hasNext())
						    {
						      Vector vecTemp = null;
						      vecTemp = (Vector) itMerchant.next();
						      out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
						    }
						%>
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=Languages.getString("jsp.admin.reports.pinreturn_search.selectproducts",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td colspan="3">
							    	<select name="productIds" size="10" multiple="multiple">
										<option value="" selected="selected"><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
						<%
						    Vector vecProductList = TransactionReport.getProductList(SessionData, null);
						    Iterator itProduct = vecProductList.iterator();
						    while (itProduct.hasNext())
						    {
						      Vector vecTemp = null;
						      vecTemp = (Vector) itProduct.next();
						      out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
						    }
						%>
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=Languages.getString("jsp.admin.reports.pinreturn_search.casenumber",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td colspan="3"><input type="text" class="plain" name="caseNumber" maxlength="9" onpropertychange="FilterCaseNumber(this);"></td>
								<td></td>
							</tr>
							<tr><td><br></td></tr>
							<tr>
								<td colspan="7" align="center"><input type="submit" id="btnSearch" class="plain" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.search",SessionData.getLanguage())%>"></td>
							</tr>
							<tr><td><br></td></tr>
						</table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	            </tr>
			</table>
		</td>
	</tr>
</table>
</form>
<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%
}//End of else we need to show the search page
%>
<%@ include file="/includes/footer.jsp" %>