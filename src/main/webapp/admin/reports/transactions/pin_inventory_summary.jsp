<%@ page language="java" import="java.util.*,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" pageEncoding="ISO-8859-1"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":"
			+ request.getServerPort() + path + "/";
%>
<%@ page
	import="java.net.URLEncoder,com.debisys.utils.HTMLEncoder,java.util.*,com.debisys.customers.Merchant,com.debisys.utils.NumberUtil"%>
<%
	int section = 4;
	int section_page = 25;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@include file="/includes/security.jsp" %>
    
<%
	Vector<Vector> vecSearchResults = new Vector<Vector>();
	Hashtable searchErrors = null;
	String[] strProductIds = request.getParameterValues("pids");
	
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();
  String noteTimeZone = "";
  
  String keyLanguage = "jsp.admin.reports.pininventory.title";
  
  String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
  
  Vector vTimeZoneData = null;
  String subTitleReportFixed = "";
  if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
  {
	subTitleReportFixed = Languages.getString(keyLanguage,SessionData.getLanguage()) + " "+Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
	subTitleReportFixed = subTitleReportFixed + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
  }
  
  boolean isInternational = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
								&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
	
	
	if (request.getParameter("search") != null) {    
	
	if (strProductIds != null) {
			TransactionReport.setProductIds(strProductIds);  
		//////////////////////////////////////////////////////////////////
	  	  //HERE WE DEFINE THE REPORT'S HEADERS 
	      headers = getHeadersPinInventory( SessionData, isInternational );
	      //////////////////////////////////////////////////////////////////
	      //////////////////////////////////////////////////////////////////
	      
	      if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
		  {
				//TO SCHEDULE REPORT
				titles.add(titleReport);		
		        TransactionReport.getPinInventory( SessionData, DebisysConstants.SCHEDULE_REPORT, headers, titles );
				ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
				if (  scheduleReport != null  )
				{
					scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
					scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
					scheduleReport.setTitleName( keyLanguage );
					response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );   
				}	
				else
	  	      		vecSearchResults = TransactionReport.getPinInventory(SessionData, DebisysConstants.EXECUTE_REPORT, headers, titles);
				
		  }else	 				
            vecSearchResults = TransactionReport.getPinInventory(SessionData, DebisysConstants.EXECUTE_REPORT, headers, titles);                
		} else {
			searchErrors = TransactionReport.getErrors();
		}
	}
%>
<%@ include file="/includes/header.jsp" %>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
    
    <%!public static ArrayList<ColumnReport> getHeadersPinInventory( SessionData sessionData, boolean isInternational )
   {
	 ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
	 headers.add(new ColumnReport("carrier", Languages.getString("jsp.admin.reports.pin_inventory_report.grid.Carrier", sessionData.getLanguage()).toUpperCase(), String.class, false));
	 headers.add(new ColumnReport("sku", Languages.getString("jsp.admin.reports.pin_inventory_report.grid.SKU", sessionData.getLanguage()).toUpperCase(), String.class, false));
	 headers.add(new ColumnReport("description", Languages.getString("jsp.admin.reports.pin_inventory_report.grid.ProductName", sessionData.getLanguage()).toUpperCase(), String.class, false));
	 headers.add(new ColumnReport("amount", Languages.getString("jsp.admin.reports.pin_inventory_report.grid.Denomination", sessionData.getLanguage()).toUpperCase(), Double.class, true));
	 headers.add(new ColumnReport("stock", Languages.getString("jsp.admin.reports.pin_inventory_report.grid.InStock", sessionData.getLanguage()).toUpperCase(), Double.class, true));
	 headers.add(new ColumnReport("avgSoldUnits", Languages.getString("jsp.admin.reports.pin_inventory_report.grid.AvgSales", sessionData.getLanguage()).toUpperCase(), Double.class, true));
	 headers.add(new ColumnReport("stimatedDaysOfStock", Languages.getString("jsp.admin.reports.pin_inventory_report.grid.InventoryDays", sessionData.getLanguage()).toUpperCase(), Double.class, true));
	 return headers;
	}  
  %>
    
    
<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.pin_inventory_report",SessionData.getLanguage()).toUpperCase()%></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" class="formArea2">
       <%
       	if (searchErrors != null) {
       		out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"
       						+ Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
       		Enumeration enum1 = searchErrors.keys();
       		while (enum1.hasMoreElements()) {
       			String strKey = enum1.nextElement().toString();
       			String strError = (String) searchErrors.get(strKey);
       			out.println("<li>" + strError);
       		}
       		out.println("</font></td></tr></table>");
       	}
       	if (vecSearchResults != null && vecSearchResults.size() > 0) {
       %>
         <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <td class="main">
                <%=Languages.getString("jsp.admin.reports.pin_inventory_report_type_title_date",SessionData.getLanguage())%> 
                <BR>
                 <%=new java.text.SimpleDateFormat(DateUtil.getDateFormatString()).format(new java.util.Date())%>
              </td>
            <tr>              
              <td class=main align=right valign=bottom>
                <%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%>
              </td>
            </tr>
            <tr>
              <td class="main">
                  <form method="post" action="admin/reports/transactions/pin_inventory_export.jsp">
                     <label><%=Languages.getString("jsp.admin.reports.transactions.pin_inventory_report.downloadLabel",SessionData.getLanguage())%>:</label>
					 <input type="submit" value="<%=Languages.getString("jsp.admin.reports.transactions.pin_inventory_report.download",SessionData.getLanguage())%>">
					  <% String strpids = Arrays.toString(strProductIds).substring(1);
					     strpids = strpids.substring(0, strpids.length()-1);  %>
                     <input type="hidden" value="<%=strpids%>" name="pids">                    
                  </form>                   
              </td>             
            </tr>          
            <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
               <thead>
                <tr class="SectionTopBorder">
                    <td class=rowhead2 align=center>#
                    </td>
					<td class=rowhead2 align=center>
					   <%=Languages.getString("jsp.admin.reports.pin_inventory_report.grid.Carrier",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center>
					   <%=Languages.getString("jsp.admin.reports.pin_inventory_report.grid.SKU",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center>
					   <%=Languages.getString("jsp.admin.reports.pin_inventory_report.grid.ProductName",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center>
					   <%=Languages.getString("jsp.admin.reports.pin_inventory_report.grid.Denomination",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center>
					   <%=Languages.getString("jsp.admin.reports.pin_inventory_report.grid.InStock",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center>
					   <%=Languages.getString("jsp.admin.reports.pin_inventory_report.grid.AvgSales",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center>
					   <%=Languages.getString("jsp.admin.reports.pin_inventory_report.grid.InventoryDays",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>					
				</tr>
			   </thead>	
			   <tbody>
				<%
					int intEvenOdd = 1;
					int count = 1;
					double dblAmountTotals = 0;
					long lngPinsAvailableTotals = 0;
					double dblAvgDailySalesTotals = 0;
					int intNumberDaysInventory = 0;
					 
					for (Vector vecTemp : vecSearchResults) {
						out.println("<tr class=row" + intEvenOdd + ">");
						out.println("<td>" + (count++) + "</td>");
						out.println("<td nowrap>" + vecTemp.get(0) + "</td>");
						out.println("<td>" + vecTemp.get(1) + "</td>");
						out.println("<td>" + vecTemp.get(2) + "</td>");
                        out.println("<td align=\"right\">" + NumberUtil.formatCurrency(vecTemp.get(3).toString()) + "</td>");
						out.println("<td align=\"right\">" + vecTemp.get(4) + "</td>");
						out.println("<td align=\"right\">" + NumberUtil.formatAmount(vecTemp.get(5).toString()) + "</td>");
						out.println("<td align=\"right\">" + vecTemp.get(6) + "</td>");
						out.println("</tr>");
						
						intEvenOdd = intEvenOdd == 1 ? 2 : 1;	
						//totals
						dblAmountTotals += Double.parseDouble(vecTemp.get(3).toString());
						lngPinsAvailableTotals += Long.parseLong(vecTemp.get(4).toString());
						dblAvgDailySalesTotals += Double.parseDouble(vecTemp.get(5).toString());
						intNumberDaysInventory += Integer.parseInt(vecTemp.get(6).toString());
					}
					vecSearchResults.clear();
				%>   
			  </tbody>         
			  <tfoot>
			  	<tr class=row<%=intEvenOdd%> >
			  		<td colspan="4" align="right"><%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
			  		<td align="right"><%=NumberUtil.formatCurrency(Double.toString(dblAmountTotals))%></td>
			  		<td align="right"><%=lngPinsAvailableTotals%></td>
			  		<td align="right"><%=NumberUtil.formatAmount(Double.toString(dblAvgDailySalesTotals))%></td>
			  		<td align="right"><%=intNumberDaysInventory%></td>
			  	</tr>
			  </tfoot>
          </table>
            <%
            	} else if (vecSearchResults.isEmpty() && request.getParameter("search") != null && searchErrors == null) {
           			out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
            	}
            %>
            <SCRIPT type="text/javascript">
	            <!--
			  new SortROC(document.getElementById("t1"),
			  		["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number"],
			  		0, false, false);
	             -->            
            </SCRIPT>
        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>
