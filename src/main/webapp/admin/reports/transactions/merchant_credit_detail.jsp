<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.reports.schedule.MerchantCreditReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.utils.NumberUtil" %>
<%
    int section = 4;
    int section_page = 21;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*" />

<jsp:useBean id="MerchantCreditReport" 
             class="com.debisys.reports.schedule.MerchantCreditReport" 
             scope="request"/>

<%@ include file="/includes/security.jsp" %>
<%  Vector vecSearchResults = new Vector();
    Hashtable searchErrors = null;
//    String sortString = "";
    String reportTitleKey = "jsp.admin.reports.transactions.merchant_credit_detail.merchant_credit_report";
    String reportTitle = Languages.getString(reportTitleKey, SessionData.getLanguage()).toUpperCase();
    String merchantDba = TransactionReport.getMerchantDBA(request.getParameter("merchantId"));
    MerchantCreditReport report = new MerchantCreditReport(SessionData, application, false, request.getParameter("startDate"),
            TransactionReport.getStartDateFormatted(), request.getParameter("endDate"), TransactionReport.getEndDateFormatted(),
            request.getParameter("merchantId"), merchantDba, reportTitle);
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));

    if (request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y")) {
        report.setScheduling(true);
        
        if (report.scheduleReport()) {
            ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
            if (scheduleReport != null) {
                scheduleReport.setStartDateFixedQuery(report.getStartDate());
                scheduleReport.setEndDateFixedQuery(report.getEndDate());
                scheduleReport.setTitleName(reportTitleKey);
            }

            response.sendRedirect(DebisysConstants.PAGE_TO_SCHEDULE_REPORTS);
        }
    }
    else if ((request.getParameter("search") != null) && (request.getParameter("search").toLowerCase().equals("y"))) {
        if (TransactionReport.validateDateRange(SessionData)) {
            vecSearchResults = report.getResults();
        } else {
            searchErrors = TransactionReport.getErrors();
        }
    } else if (request.getParameter("downloadReport") != null && request.getParameter("downloadReport").equals("y")) {
        report.setScheduling(false);
        String zippedFilePath = report.downloadReport();
        if ((zippedFilePath != null) && (!zippedFilePath.trim().equals(""))) {
            response.sendRedirect(zippedFilePath);
        }
    }


%>
<%@ include file="/includes/header.jsp" %>
<%    if (vecSearchResults != null && vecSearchResults.size() > 0) {
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
    <%
        }
    %>
<table border="0" cellpadding="0" cellspacing="0" width="420">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
            &nbsp;
            <%= Languages.getString("jsp.admin.reports.title37", SessionData.getLanguage()).toUpperCase()%>
        </td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <%
                if (searchErrors != null) {
                    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                            "jsp.admin.error1", SessionData.getLanguage()) + ":<br>");

                    Enumeration enum1 = searchErrors.keys();

                    while (enum1.hasMoreElements()) {
                        String strKey = enum1.nextElement().toString();
                        String strError = (String) searchErrors.get(strKey);

                        out.println("<li>" + strError);
                    }

                    out.println("</font></td></tr></table>");
                }

                if (vecSearchResults != null && vecSearchResults.size() > 0) {
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td class="main">
                        <strong>
                            <%
                                out.println(merchantDba);
                            %>
                        </strong>
                        <br>
                        <%=reportTitle%>
                        <%
                            if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                                out.println(Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted())
                                        + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                            }
                        %>
                        <br>
                        <font color="ff0000"><%= Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.limited_to_90_days", SessionData.getLanguage())%></font>    
                    </td>
                    <td class=main align=right valign=bottom>
                        <%= Languages.getString("jsp.admin.reports.click_to_sort", SessionData.getLanguage())%>
                    </td>
                </tr>
                <TR>
                    <td>
                        <FORM ACTION="admin/reports/transactions/merchant_credit_detail.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                            <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                            <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                            <INPUT TYPE=hidden NAME=merchantId VALUE="<%=request.getParameter("merchantId")%>">
                            <input type="hidden" name="downloadReport" value="y">
                            <input type="hidden" name="search" value="n">
                            <input type="hidden" name="sheduleReport" value="n">
                            <INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.downloadToCsv", SessionData.getLanguage())%>">
                        </FORM>
                    </td>
                </TR>
            </table>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
                <thead>
                    <tr class="SectionTopBorder">
                        <td class=rowhead2>
                            #
                        </td>
                        <%
                            // Set the column headers
                            ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
                            if ((reportColumns != null) && (reportColumns.size() > 0)) {
                                for (int columnIndex = 0; columnIndex < reportColumns.size(); columnIndex++) {
                        %>                    
                        <td class=rowhead2 nowrap align=center><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
                        </td>
                        <%
                                }
                            }
                        %>
                    </tr>
                </thead>
                <%
                    Iterator it = vecSearchResults.iterator();
                    int intEvenOdd = 1;
                    int intCounter = 1;

                    double totalsales = 0;
                    double totalpayments = 0;

                    while (it.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        SimpleDateFormat fDate = new SimpleDateFormat("dd MMMMMM yyyy");
                        String strDate = fDate.format((Date) vecTemp.get(0));
                        double openningCredit = (Double) vecTemp.get(1);
                        double sales = (Double) vecTemp.get(2);
                        double payments = (Double) vecTemp.get(3);
                        double closingCredit = (Double) vecTemp.get(4);
                        totalsales += sales;
                        totalpayments += payments;

                        out.print("<tr class=row" + intEvenOdd + ">"
                                + "<td>" + intCounter++ + "</td>"
                                + "<td nowrap>" + strDate + "</td>"
                                + "<td nowrap align=\"right\">" + NumberUtil.formatCurrency(Double.toString(openningCredit)) + "</td>"
                                + "<td nowrap align=\"right\">" + NumberUtil.formatCurrency(Double.toString(sales)) + "</td>"
                                + "<td nowrap align=\"right\">" + NumberUtil.formatCurrency(Double.toString(payments)) + "</td>"
                                + "<td nowrap align=\"right\">" + NumberUtil.formatCurrency(Double.toString(closingCredit)) + "</td></tr>");
                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        } else {
                            intEvenOdd = 1;
                        }
                    }

                %> 
                <tfoot>
                    <tr class=row<%= intEvenOdd%>>
                        <td colspan="3"><strong><%=Languages.getString("jsp.admin.reports.total_sales", SessionData.getLanguage())%></strong></td>
                        <td align="right"><strong><%=NumberUtil.formatCurrency(Double.toString(totalsales))%></strong></td>
                        <td align="right"><strong><%=NumberUtil.formatCurrency(Double.toString(totalpayments))%></strong></td>
                        <td> </td>
                    </tr>
                </tfoot>
            </table>
            <%

                } else if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null) {
                    out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                }
                if (vecSearchResults != null && vecSearchResults.size() > 0) {
            %>
            <SCRIPT type="text/javascript">

                <!--
    var stT1 = new SortROC(document.getElementById("t1"),
                        ["None", "Date", "Number", "Number", "Number", "Number"], 0, false, false);
-->
            
            </SCRIPT>
            <%
                }
            %>
        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>
