<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.reports.pojo.ProviderPojo,
                 com.debisys.reports.pojo.ProductPojo" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=60;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
String path = request.getContextPath(); 
%>
<style type="text/css" title="currentStyle">
    @import "<%=path%>/includes/media/demo_table.css";
</style>
  
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>
    
<jsp:useBean id="MultiSource" class="com.debisys.reports.multisource.MultiSource" scope="request"/>
<jsp:setProperty name="MultiSource" property="*"/>
<%@ include file="/includes/security.jsp" %>



<%
    ArrayList<ProviderPojo> providersList = new ArrayList<ProviderPojo>();
    ArrayList<ProductPojo> productList    = new ArrayList<ProductPojo>();
    StringBuilder providerCombo = new StringBuilder();
    StringBuilder productsCombo = new StringBuilder();
    String[] providerValues = null;
    String[] productValues = null;
    //String strRefId = SessionData.getProperty("ref_id");
    String pageReport = "admin/reports/transactions/multisource_summary.jsp";
    
    String reportType = request.getParameter("reportType");
    if ( request.getMethod().equals("POST"))
    {
        SessionData.setProperty("start_date", request.getParameter("startDate"));
        SessionData.setProperty("end_date", request.getParameter("endDate"));
                
            
        providerValues = request.getParameterValues("providersCombo");
        productValues  = request.getParameterValues("productsCombo");
                    
        if ( providerValues != null )
        {    
           System.out.println("post findProductProviders");
           productList = com.debisys.reports.multisource.MultiSource.getProductsByproviders(providerValues);
        }
        
        System.out.println("post findProductProviders END ");
    }
    
    String mainTitle = Languages.getString("jsp.tools.dtu2536.report.main",SessionData.getLanguage()).toUpperCase();
    if ( reportType.equals("2"))
    {
        mainTitle = Languages.getString("jsp.tools.dtu2536.report.mainProvider",SessionData.getLanguage()).toUpperCase();
        pageReport = "admin/reports/transactions/multisource_summary_prov.jsp";
    }    
        
    
    String startDate = Languages.getString("jsp.admin.start_date",SessionData.getLanguage());
    String endDate   = Languages.getString("jsp.admin.end_date",SessionData.getLanguage());
    String types     = Languages.getString("jsp.tools.dtu2536.report.types",SessionData.getLanguage());
    String providers = Languages.getString("jsp.tools.dtu2536.report.providers",SessionData.getLanguage());
    String trxId     = Languages.getString("jsp.tools.dtu2536.report.trxId",SessionData.getLanguage());
    String prodLbl   = Languages.getString("jsp.tools.dtu2536.report.products",SessionData.getLanguage());
    String searchSkus= Languages.getString("jsp.tools.dtu2536.searchproduct",SessionData.getLanguage());
  
    String labelTypeAll         = Languages.getString("jsp.tools.dtu2536.report.type.all",SessionData.getLanguage());
    String labelTypeMultiSource = Languages.getString("jsp.tools.dtu2536.report.type.multisource",SessionData.getLanguage());
    String labelTypeDirect      = Languages.getString("jsp.tools.dtu2536.report.type.direct",SessionData.getLanguage());

    String labelshowReport      = Languages.getString("jsp.tools.dtu2536.show_report",SessionData.getLanguage());
    
    providersList = com.debisys.reports.multisource.MultiSource.getProviders();
    
    for(ProviderPojo prov : providersList)
    {
        String id = prov.getId();
        String desc = prov.getDescripton();
        boolean isProviderSelected = false;
        if ( providerValues != null )
        {
            for(String provSelected : providerValues )
            {
                if ( provSelected.equals(id) )
                {
                    isProviderSelected=true;
                    break;
                }
            }
        }
        if ( isProviderSelected )
            providerCombo.append(" <option selected value=\""+id+"\">"+desc+"("+id+")</option> ");
        else
            providerCombo.append(" <option value=\""+id+"\">"+desc+"("+id+")</option> ");
    }
  
  for(ProductPojo product : productList)
  {
    String id = product.getId();
    String desc = product.getDescripton();
    String providerId   = product.getProvider().getId();
    String providerName = product.getProvider().getDescripton();
        
    productsCombo.append(" <option value=\""+id+"\">"+desc+"("+id+") ["+providerName+"("+providerId+")]</option> ");
  }    
  
  
%>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" charset="utf-8">
    function setUrlForm(ControlId) 
    {   
        if ( ControlId == "executeReporte" )
        {
            document.getElementById("mainform").action = "<%=pageReport%>";
        }   
        else
        {
            document.getElementById("mainform").action = "admin/reports/transactions/multisource.jsp";
        }        
        document.getElementById("mainform").submit();
    }
</script>

<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
  <tr>
        <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td class="formAreaTitle" align=left width="1000">&nbsp;<%=mainTitle%></td>
        <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr class="formArea2">
  <td align=center bgcolor="#FFFFFF" colspan="3">
      <form name="mainform" id="mainform" method="post" >
        <input type="hidden" id="reportType" name="reportType" value="<%=reportType%>" />  
        <table width="100%" align=center>                  
           <tr class="main">                
                <td nowrap><%=startDate%>:</td>
                <td>
                    <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12">
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS>
                            <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""/>
                        </a>                        
                </td>               
            </tr>            
            <tr class="main">
                <td nowrap><%=endDate%>:</td>
                <td>
                    <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12">
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS>
                            <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
                        </a>                        
                </td>                
            </tr>
            <%
            if ( reportType.equals("1"))
            {
            %>
            <tr class="main">
                <td nowrap><%=trxId%>: </td>
                <td>
                    <input id="trxId" name="trxId" value="" size="12">                        
                    </input>    
                </td>               
            </tr>
            <%
            }
            %>
            <tr class="main">
                <td><%=types%></td>
                <td>
                    <select id="typeMultiSource" name="typeMultiSource">
                        <option value="0"><%=labelTypeAll%></option>
                        <option value="1"><%=labelTypeMultiSource%></option>
                        <option value="2"><%=labelTypeDirect%></option>
                    </select>
                </td>               
            </tr>            
            <tr class="main">
                <td align="left" colspan="2">
                    <table width="100%" align=center cellspacing="15" cellpadding="10">  
                         <tr class="main">
                            <td><%=providers%></td>
                            <td>
                                <select id="providersCombo" name="providersCombo" size="20" multiple="true" >
                                    <option value="-1"><%=labelTypeAll%></option>
                                    <%=providerCombo%>
                                </select>
                                <%
                                if ( reportType.equals("1") )
                                {
                                %>
                                <input type="button" onclick="setUrlForm('findProductProviders');" id="findProductProviders" name="findProductProviders" value="<%=searchSkus%>" />
                                <%
                                }
                                %>
                            </td>
                            <%
                            if ( reportType.equals("1"))
                            {
                            %>
                            <td><%=prodLbl%></td>
                            <td>
                                <select size="20" id="productsCombo" name="productsCombo" multiple="true">
                                    <option value="-1"><%=labelTypeAll%></option><%=productsCombo%>
                                </select>
                            </td>
                            <%
                            }
                            %>
                        </tr>
                    </table>
                </td>               
            </tr>                     
        </table>
        </form>
    </td>	 
  </tr>
  
  <tr align="center">
        <td colspan="5" bgcolor="#FFFFFF">                                                         
            <input type="button" id="executeReporte" name="executeReporte" value="<%=labelshowReport%>" onclick="setUrlForm('executeReporte');" />
        </td>
    </tr>
   </table>

<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>