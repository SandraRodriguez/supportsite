<%@ page import="java.util.*" %>
<%
int section=3;
int section_page=4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>

<%
   Vector vecSearchResults = new Vector();
   String strPath="";
   String strRepsIds ="";
   String strIds ="";
   String startdate = request.getParameter("startdate");
   if ( request.getParameter("strRepsIds") != null)
     strRepsIds = request.getParameter("strRepsIds");
                                                                
   if ( request.getParameter("strIds") != null)                 
      strIds = request.getParameter("strIds");            
                                                                
   String days = request.getParameter("days");                  
   vecSearchResults = TransactionReport.getTerminalsNonTX(SessionData,days,startdate,strRepsIds,strIds, DebisysConstants.DOWNLOAD_REPORT, null, null);
   strPath = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,1,SessionData);
   if ( strPath.length() > 0)
   {
     response.sendRedirect(strPath);    
   }
%>
