<%@ page import="java.util.*"%>
<%
	int section = 4;
	int section_page = 33;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="TransactionReport"
	class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp"%>

<%
	Vector vecSearchResults = new Vector();
	String strRuta = "";
	String strProductId = request.getParameter("pid");
	if (strProductId != null)
	{	    
		TransactionReport.setProductId(strProductId);
	}
	
	vecSearchResults = TransactionReport.getPinRemainingInventory(1,0,SessionData);
	vecSearchResults.remove(0);
	//this.getServletContext().setAttribute("ISO_Name", "");
	strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,10,SessionData);
	if (strRuta.length() > 0)
	{
		response.sendRedirect(strRuta);
	}
%>
