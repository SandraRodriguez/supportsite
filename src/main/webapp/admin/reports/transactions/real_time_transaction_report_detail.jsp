<%@ page
	import="com.debisys.languages.Languages,
				 com.debisys.customers.Merchant,
				 com.debisys.reports.TransactionReport,
				 com.debisys.utils.StringUtil,
				 com.debisys.utils.DateUtil,
				 java.util.*,
				 com.debisys.utils.TimeZone,
				 java.util.Enumeration,
				 java.text.SimpleDateFormat"%>
<%
	int section = 4;
	int section_page = 58;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />

<%@ include file="/includes/security.jsp"%>

<%
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;
	int intRecordCount = 0;
	String dateStr = "";
	Vector<Vector<String>> vResults1Min = new Vector<Vector<String>>();
	Vector<Vector<String>> vResults5Min = new Vector<Vector<String>>();
	Vector<Vector<String>> vResults15Min = new Vector<Vector<String>>();
	Vector<Vector<String>> vResults60Min = new Vector<Vector<String>>();
	Vector<String> vTimeZoneFilterDates = new Vector<String>();

	try {
		if (request.getParameter("download") != null) {
			String sURL = "";
			String[] terminal_types = new String[] { "" };
			terminal_types = request.getParameterValues("terminal_types");
			dateStr = request.getParameter("currentDate");
			sURL = TransactionReport.downloadRealTimeTransactions(SessionData,application, terminal_types,dateStr);
			response.sendRedirect(sURL);
			return;
		}
		else{
			String[] terminal_types = new String[] { "" };
			terminal_types = request.getParameterValues("terminal_types");
			 
			Date dateEvaluated = new Date(); 
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dateStr = formatter.format(dateEvaluated);
			String strRefId = SessionData.getProperty("ref_id");
			vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, dateStr, dateStr, false);
			
			vResults1Min = TransactionReport.getRealTimeTransactions(SessionData, terminal_types,dateEvaluated, -1);
			vResults5Min = TransactionReport.getRealTimeTransactions(SessionData, terminal_types,dateEvaluated, -5);
			vResults15Min = TransactionReport.getRealTimeTransactions(SessionData, terminal_types,dateEvaluated,-15);
			vResults60Min = TransactionReport.getRealTimeTransactions(SessionData, terminal_types,dateEvaluated,-60);
		}
		

	} catch (Exception e) {
		%>
		<table border="0" cellpadding="0" cellspacing="0" width="60%">
			<tr>
				<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
				<td background="images/top_blue.gif" class="formAreaTitle"
					width="3000"><b>ERROR IN PAGE</b></td>
				<td background="images/top_blue.gif" width="1%" align="right"><img
					src="images/top_right_blue.gif" width="18" height="20"></td>
			</tr>
			<tr>
				<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class=main><%=Languages.getString("jsp.admin.reports.payments.detailedrep_payments.checklog", SessionData.getLanguage()) + e.toString()%></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><br>
				<br></td>
			</tr>
		</table>
	<%}%>
<%@ include file="/includes/header.jsp"%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script src="includes/sortROC.js" type="text/javascript"></script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img
			src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle"
			width="3000"><b><%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.title", SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img
			src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	
	<tr> 
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td colspan=2>
						<table>
							<tr>
								<td>
									<FORM ACTION="admin/reports/transactions/real_time_transaction_report_detail.jsp" METHOD="post" ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
										<%
											String[] terminal_types = new String[] { "" };
											terminal_types = request.getParameterValues("terminal_types");
											for (int i = 0; i < terminal_types.length; i++) {
												%>
												<input type="hidden" name="terminal_types"
													value="<%=terminal_types[i]%>">
												<%
											}
										%>

										<INPUT TYPE="hidden" NAME="download" VALUE="Y"> 
										<INPUT TYPE="hidden" NAME="currentDate" VALUE="<%=dateStr%>"> 
										<INPUT ID="btnSubmit" TYPE="submit" VALUE="<%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.download", SessionData.getLanguage())%>">
									</FORM>
								</td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td></td>
							</tr>
							
							<tr>
								
							</tr>
						</table>
					</td>
				</tr>
			</table>
	
	</tr>
	<tr>
		<td align=center><%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.baseDate",SessionData.getLanguage())%>:<%=vTimeZoneFilterDates.get(0)%></td>
	</tr>
	<tr>
		<td></td>
	</tr>
	
	<!-- ############################################## TABLE FOR 1 MINUTE AGO #################################################################-->
	<tr><td align=center><%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.1MinAgo",SessionData.getLanguage())%></td></tr>
	<tr>
		<%if (vResults1Min.size() > 0) {%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2"></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.terminalDesc",SessionData.getLanguage()))%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successQuantity",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successRate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.errorRate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successQuantity2WeeksAgo",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successRate2WeeksAgo",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.errorRate2WeeksAgo", SessionData.getLanguage()).toUpperCase())%></td>
					</tr>
				</thead>
				<%
				int nRow = 1;
				for (int i = 0; i < vResults1Min.size(); i++) {
					Vector vTemp = (Vector) vResults1Min.get(i);%>
					<tr class="row<%=nRow%>">
						<td><%=(i + 1)%></td>
						<td align="left"><%=vTemp.get(0).toString()%></td>
						<td align="right"><%=vTemp.get(1).toString()%></td>
						<td align="right"><%=vTemp.get(2).toString()%></td>
						<td align="right"><%=vTemp.get(3).toString()%></td>
						<td align="right"><%=vTemp.get(4).toString()%></td>
						<td align="right"><%=vTemp.get(5).toString()%></td>
						<td align="right"><%=vTemp.get(6).toString()%></td>
					</tr>
				<%
					nRow = (nRow == 1) ? 2 : 1;
				}//End of for
				%>
			</table>
		</td>
		<%}else {%>
				<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class=main><%=Languages.getString("jsp.admin.reports.pinreturn_search.noresults", SessionData.getLanguage())%></td>
						</tr>
					</table>
				</td>
		<%}%>
	</tr>
	
		<!-- ############################################## TABLE FOR 5 MINUTEs AGO #################################################################-->
	<tr><td align=center><%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.5MinAgo",SessionData.getLanguage())%></td></tr>
	<tr>
		<%if (vResults5Min.size() > 0) {%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2"></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.terminalDesc",SessionData.getLanguage()))%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successQuantity",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successRate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.errorRate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successQuantity2WeeksAgo",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successRate2WeeksAgo",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.errorRate2WeeksAgo", SessionData.getLanguage()).toUpperCase())%></td>
					</tr>
				</thead>
				<%
				int nRow = 1;
				for (int i = 0; i < vResults5Min.size(); i++) {
					Vector vTemp = (Vector) vResults5Min.get(i);%>
					<tr class="row<%=nRow%>">
						<td><%=(i + 1)%></td>
						<td align="left"><%=vTemp.get(0).toString()%></td>
						<td align="right"><%=vTemp.get(1).toString()%></td>
						<td align="right"><%=vTemp.get(2).toString()%></td>
						<td align="right"><%=vTemp.get(3).toString()%></td>
						<td align="right"><%=vTemp.get(4).toString()%></td>
						<td align="right"><%=vTemp.get(5).toString()%></td>
						<td align="right"><%=vTemp.get(6).toString()%></td>
					</tr>
				<%
					nRow = (nRow == 1) ? 2 : 1;
				}//End of for
				%>
			</table>
		</td>
		<%}else {%>
				<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class=main><%=Languages.getString("jsp.admin.reports.pinreturn_search.noresults", SessionData.getLanguage())%></td>
						</tr>
					</table>
				</td>
		<%}%>
	</tr>
	
			<!-- ############################################## TABLE FOR 15 MINUTEs AGO #################################################################-->
	<tr><td align=center><%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.15MinAgo",SessionData.getLanguage())%></td></tr>
	<tr>
		<%if (vResults15Min.size() > 0) {%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2"></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.terminalDesc",SessionData.getLanguage()))%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successQuantity",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successRate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.errorRate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successQuantity2WeeksAgo",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successRate2WeeksAgo",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.errorRate2WeeksAgo", SessionData.getLanguage()).toUpperCase())%></td>
					</tr>
				</thead>
				<%
				int nRow = 1;
				for (int i = 0; i < vResults15Min.size(); i++) {
					Vector vTemp = (Vector) vResults15Min.get(i);%>
					<tr class="row<%=nRow%>">
						<td><%=(i + 1)%></td>
						<td align="left"><%=vTemp.get(0).toString()%></td>
						<td align="right"><%=vTemp.get(1).toString()%></td>
						<td align="right"><%=vTemp.get(2).toString()%></td>
						<td align="right"><%=vTemp.get(3).toString()%></td>
						<td align="right"><%=vTemp.get(4).toString()%></td>
						<td align="right"><%=vTemp.get(5).toString()%></td>
						<td align="right"><%=vTemp.get(6).toString()%></td>
					</tr>
				<%
					nRow = (nRow == 1) ? 2 : 1;
				}//End of for
				%>
			</table>
		</td>
		<%}else {%>
				<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class=main><%=Languages.getString("jsp.admin.reports.pinreturn_search.noresults", SessionData.getLanguage())%></td>
						</tr>
					</table>
				</td>
		<%}%>
	</tr>
	
			<!-- ############################################## TABLE FOR 60 MINUTEs AGO #################################################################-->
	<tr><td align=center><%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.60MinAgo",SessionData.getLanguage())%></td></tr>
	<tr>
		<%if (vResults60Min.size() > 0) {%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2"></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.terminalDesc",SessionData.getLanguage()))%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successQuantity",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successRate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.errorRate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successQuantity2WeeksAgo",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.successRate2WeeksAgo",SessionData.getLanguage()).toUpperCase())%></td>
						<td class="rowhead2" align="center"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.realTimeTransactionReport.errorRate2WeeksAgo", SessionData.getLanguage()).toUpperCase())%></td>
					</tr>
				</thead>
				<%
				int nRow = 1;
				for (int i = 0; i < vResults60Min.size(); i++) {
					Vector vTemp = (Vector) vResults60Min.get(i);%>
					<tr class="row<%=nRow%>">
						<td><%=(i + 1)%></td>
						<td align="left"><%=vTemp.get(0).toString()%></td>
						<td align="right"><%=vTemp.get(1).toString()%></td>
						<td align="right"><%=vTemp.get(2).toString()%></td>
						<td align="right"><%=vTemp.get(3).toString()%></td>
						<td align="right"><%=vTemp.get(4).toString()%></td>
						<td align="right"><%=vTemp.get(5).toString()%></td>
						<td align="right"><%=vTemp.get(6).toString()%></td>
					</tr>
				<%
					nRow = (nRow == 1) ? 2 : 1;
				}//End of for
				%>
			</table>
		</td>
		<%}else {%>
				<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class=main><%=Languages.getString("jsp.admin.reports.pinreturn_search.noresults", SessionData.getLanguage())%></td>
						</tr>
					</table>
				</td>
		<%}%>
	</tr>
	
	<tr>
		<td><br>
		<br></td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<form method="post"
				action="admin/reports/transactions/real_time_transaction_report.jsp" onSubmit="document.getElementById('btnSubmitBack').disabled = true; document.getElementById('btnSubmit').disabled = true;">
				<input type=submit class="plain" id="btnSubmitBack"
					value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.backtosearch", SessionData.getLanguage())%>">
			</form>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>