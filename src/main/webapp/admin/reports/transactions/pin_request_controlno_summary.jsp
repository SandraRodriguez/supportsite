<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.utils.NumberUtil" %>
<%
    int section=4;
    int section_page=25;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
  <%@ include file="/includes/header.jsp" %>
<%
	Vector vecSearchResults = new Vector();
	long controlNo = Long.parseLong(request.getParameter("controlNo"));
	long siteId = Long.parseLong(request.getParameter("siteId"));
	vecSearchResults = TransactionReport.getControlNoHistoryByMerchantId(siteId, controlNo);

%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%= Languages.getString("jsp.admin.reports.pin_request_report",SessionData.getLanguage()) %></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea">
           <table width="100%" cellspacing="1" cellpadding="2">
                <tr>
                    <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.pin_request_report.control.grid.TransId",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                    <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.pin_request_report.control.grid.DateTime",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                    <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.pin_request_report.control.grid.BusinessName",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                    <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.pin_request_report.control.grid.SiteNo",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                    <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.pin_request_report.control.grid.Clerk",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                    <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.pin_request_report.control.grid.Amount",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                    <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.pin_request_report.control.grid.Location",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                    <%  
                        Iterator it = vecSearchResults.iterator();
                        while (it.hasNext())
                        {
                            Vector vecTemp = null;
                            vecTemp = (Vector) it.next();                            
                            out.println("<tr class=main>" +
                                        "<td>" + vecTemp.get(0) + "</td>" +
                                        "<td>" + vecTemp.get(1) + "</td>" +
                                        "<td>" + vecTemp.get(2) + "</td>" +
                                        "<td>" + vecTemp.get(3) + "</td>" +
                                        "<td>****</td>" +
                                        "<td>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
                                        "<td>" + vecTemp.get(6) + "</td>");
                            out.println("</tr>");
                        }
                        vecSearchResults.clear();
                    %>
                </tr>
            </table>     
        </td>
    </tr>
</table>
  <%@ include file="/includes/footer.jsp" %>
