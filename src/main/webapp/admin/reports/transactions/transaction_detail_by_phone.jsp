<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 4;
  int section_page = 19;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
  Vector warningSearchResults = new Vector();
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";
  String message = "";
  
  //this flag means if the user is in International Default
  boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
	   									DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	   
  boolean viewReferenceCard = false;
  if ( isInternationalDefault ){
	viewReferenceCard = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);							
  } 

  if (request.getParameter("download") != null)
  {
	TransactionReport.setStartDate(request.getParameter("startDate"));
	TransactionReport.setEndDate(request.getParameter("endDate"));
	TransactionReport.setPhoneNumber(request.getParameter("phoneNumber"));
  	String sURL = TransactionReport.downloadPhoneDetails(application, SessionData);
  	response.sendRedirect(sURL);
  	return;
  }

  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {        
    		vecSearchResults = TransactionReport.getPhoneDetail(SessionData, application);
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
<%@ include file="/includes/header.jsp" %>
<%
	boolean bool_custom_config_mexico = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
	boolean bool_custom_config_default = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	
	boolean bool_deploy_type_intl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
	boolean bool_perm_enable_tax_calc = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
  	
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
<table border="0" cellpadding="0" cellspacing="0" width="420">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td nowrap background="images/top_blue.gif" class="formAreaTitle" width="2000">
        &nbsp;
        <%= Languages.getString("jsp.admin.reports.title35",SessionData.getLanguage()).toUpperCase() %> <%=HTMLEncoder.encode(TransactionReport.getPhoneNumber())%>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
		out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);
            out.println("<li>" + strError);
          }
		
          out.println("</font></td></tr></table>");
        }

        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
	Vector vTimeZoneData = null;
	
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
				<tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>			
            <tr>
            		<td colspan=2>
            			<table>
              				<tr>
                				<td>
				                	<FORM ACTION="admin/reports/transactions/transaction_detail_by_phone.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
				                		<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
				                		<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
				                		<INPUT TYPE=hidden NAME=phoneNumber VALUE="<%=request.getParameter("phoneNumber")%>">
				                		<INPUT TYPE=hidden NAME=download VALUE="y">
				                		<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
				                	</FORM>
                				</td>
              				</tr>
            			</table>
            		</td>
            	</tr>      
            	<tr>
              <td class="main">
                <%= Languages.getString("jsp.admin.reports.transactions.phone_transaction_report",SessionData.getLanguage()) %>
<%
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                  out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
%>
              </td>
              <td class=main align=right valign=bottom>
                		<%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%>
              </td>
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
<%
			sortString = "\"None\", \"CaseInsensitiveString\", \"Date\", \"Number\", ";
%>
              <tr class="SectionTopBorder">
                	<td class=rowhead2>#</td>
                	<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase() %></td>
                	<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase() %></td>               
                	<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase() %></td>
<%
		// DBSY-905
				if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
		{
			sortString += "\"CaseInsensitiveString\", \"CaseInsensitiveString\", ";
%>
					  <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%></td>
<% 
			if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
			{
				sortString += "\"CaseInsensitiveString\", \"CaseInsensitiveString\", \"Number\", \"CaseInsensitiveString\", ";
%>
					  <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></td>
			  		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxpercentage",SessionData.getLanguage()).toUpperCase()%></td>
			  		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxtype",SessionData.getLanguage()).toUpperCase()%></td>
<%
			}
		}

		sortString += "\"Number\"";
%>
					<td class=rowhead2 nowrap width="20%"><%= Languages.getString("jsp.admin.reports.clerk_id",SessionData.getLanguage()).toUpperCase() %></td>
<%  
		if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
			sortString += ", \"Number\"";
%>
					<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage()).toUpperCase()%></td> 
<%
		} 
%>
		<%if ( viewReferenceCard ){%>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.DTU1204",SessionData.getLanguage()).toUpperCase()%></td>
        <%}%>
        </tr>
      </thead>
<%
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;
            
            while (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector)it.next();
                
              out.print("<tr class=row" + intEvenOdd + ">" + 
              "<td>" + intCounter++ + "</td>" + 
              "<td nowrap>" + vecTemp.get(0) + "</td>" + 
              "<td nowrap>" + vecTemp.get(1) + "</td>" +
              "<td nowrap align=\"right\">" + vecTemp.get(2) + "</td>" ); 
              
			// DBSY-905 Tax Calculation
			                    if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
				&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
			{
				// bonus
			                                out.println("<td nowrap align=\"right\">" + vecTemp.get(5) + "</td>" +
					// total Recharge
			                                "<td nowrap align=\"right\">" + vecTemp.get(6) + "</td>");
		
			                        if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
				    	 					{
					// net amount 
			                         	       out.println("<td nowrap align=\"right\">" + vecTemp.get(8) + "</td>" +
						// tax amount
						"<td nowrap align=\"right\">" + vecTemp.get(7) + "</td>" +
						// tax percentage
						"<td nowrap align=\"right\">" + vecTemp.get(9) + "%</td>" +
						// tax type
						"<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>");
			                                }
			                    }
	
			   out.println("<td nowrap align=\"right\">" + vecTemp.get(3) + "</td>");
              
              if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{
              		//DBSY-919 Invoice Number Field
			  		out.println("<td>" + vecTemp.get(4) + "</td>");
		      		////// 
		      	}
				    
			  if ( viewReferenceCard ){
			  	out.println("<td>" + vecTemp.get(11) + "</td>");
			  }	    
              if (intEvenOdd == 1)
              {
                intEvenOdd = 2;
              }
              else
              {
                intEvenOdd = 1;
              }
            }
%>
          </table>
<%
        }
	else if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
<SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  [<%=sortString%>],0,false,false);
  -->
            
</SCRIPT>
<%
        }
%>
      </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>