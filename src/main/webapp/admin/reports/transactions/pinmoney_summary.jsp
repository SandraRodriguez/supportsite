<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=54;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;

if ( request.getParameter("Download") != null )
{
	  String sURL = "";
	  TransactionReport.setProductIds(request.getParameter("_productIds"));
	  TransactionReport.setMerchantIds(request.getParameter("_merchantIds"));

	  sURL = TransactionReport.downloadPINMoneySummary(application, SessionData, request.getParameter("transactionID"), request.getParameter("PINNumber"));
	  response.sendRedirect(sURL);
	  return;
}
	
if (request.getParameter("search") != null)
{
  if (TransactionReport.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    String strProductIds[] = request.getParameterValues("pids");
    if (strProductIds != null)
    {
      TransactionReport.setProductIds(strProductIds);
    }
    String strMerchantIds[] = request.getParameterValues("merchantIds");
    if (strMerchantIds != null)
    {
      TransactionReport.setMerchantIds(strMerchantIds);
    }

	vecSearchResults = TransactionReport.getPINMoneySummary(SessionData, application, request.getParameter("transactionID"), request.getParameter("PINNumber"));
  }
  else
  {
   searchErrors = TransactionReport.getErrors();
  }
}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=SessionData.getString("jsp.admin.reports.PINMoney_Transactions_Report.title").toUpperCase()%></td>
    <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF" class="formArea2">

<%
if (searchErrors != null)
{
  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br/>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError + "</li>");
}

  out.println("</font></td></tr></table>");
}
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
	Vector vTimeZoneData = null;
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
            <tr class="main"><td nowrap="nowrap" colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
            <tr><td class="main">
              <%
                out.println(Languages.getString("jsp.admin.reports.transaction.product_summary",SessionData.getLanguage()));
              if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
            %><br><font color="#ff0000"><%=Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage())%></font></td></tr>
            <tr>
            	<td colspan="2">
            <table>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>
                <FORM ACTION="admin/reports/transactions/pinmoney_summary.jsp" METHOD="post" ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	<INPUT TYPE="hidden" NAME="startDate" VALUE="<%=request.getParameter("startDate")%>">
                	<INPUT TYPE="hidden" NAME="endDate" VALUE="<%=request.getParameter("endDate")%>">
                	<INPUT TYPE="hidden" NAME="_productIds" VALUE="<%=TransactionReport.getProductIds()%>">
                	<INPUT TYPE="hidden" NAME="_merchantIds" VALUE="<%=TransactionReport.getMerchantIds()%>">
                	<INPUT TYPE="hidden" NAME="transactionID" VALUE="<%=request.getParameter("transactionID")%>">
                	<INPUT TYPE="hidden" NAME="PINNumber" VALUE="<%=request.getParameter("PINNumber")%>">
                	<INPUT TYPE="hidden" NAME="Download" VALUE="Y">
                	<INPUT ID="btnSubmit" TYPE="submit" VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                </FORM>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                </td>
              </tr>
            </table>
            	</td>
            </tr>
            </table>
            <table>
                <tr>
                    <td align="left" width="3000"><%=SessionData.getString("jsp.admin.index.company_name")%>: <%=SessionData.getProperty("company_name")%></td>
                </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
            <%
            String strSortURL = "admin/reports/transactions/pinmoney_summary.jsp?search=y&startDate=" + TransactionReport.getStartDate() + "&endDate=" + TransactionReport.getEndDate() + "&pids=" + TransactionReport.getProductIds() + ((request.getParameterValues("merchantIds") != null)?"&merchantIds=" + TransactionReport.getMerchantIds():"") + "&transactionID=" + request.getParameter("transactionID") + "&PINNumber=" + request.getParameter("PINNumber");
            %>
              <td class="rowhead2" valign="bottom">#</td>
              <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.transactions.products_summary.product_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=1&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=1&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
              <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=2&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=2&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
              <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=3&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=3&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
              <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=4&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=4&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
			  
<%
		if (strAccessLevel.equals(DebisysConstants.ISO))
  		{
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=9&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=9&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
	    }
		if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  		{
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=8&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=8&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
		}

        if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  	    {
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=7&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=7&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
   		}
if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
  {
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=6&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=6&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
  }

%>
<td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=5&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=5&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
{
%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.channel_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=10&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=10&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
}

if (strAccessLevel.equals(DebisysConstants.ISO))
{%>
  <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.adjustment",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=10&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="<%=strSortURL%>&col=10&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%}
  out.println("</tr>");


                  double dblTotalSalesSum = 0;
                  double dblTotalBonus = 0;
                  double dblTotalRechargeSum = 0;
                  double dblMerchantCommissionSum = 0;
                  double dblRepCommissionSum = 0;
                  double dblSubAgentCommissionSum = 0;
                  double dblAgentCommissionSum = 0;
                  double dblISOCommissionSum = 0;
                  double dblAdjAmountSum = 0;
                  double dblChannelPercentageSum = 0; // 2008-11-12.FB. New column added
                  double dblTotalTaxAmountSum=0;
                  int intTotalQtySum = 0;
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  int intCounter = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    int intTotalQty = Integer.parseInt(vecTemp.get(2).toString());
                    double dblTotalSales = Double.parseDouble(vecTemp.get(3).toString());
                    double dblMerchantCommission = Double.parseDouble(vecTemp.get(4).toString());
                    double dblRepCommission = Double.parseDouble(vecTemp.get(5).toString());
                    double dblSubAgentCommission = Double.parseDouble(vecTemp.get(6).toString());
                    double dblAgentCommission = Double.parseDouble(vecTemp.get(7).toString());
                    double dblISOCommission = Double.parseDouble(vecTemp.get(8).toString());
                    double dblAdjAmount = Double.parseDouble(vecTemp.get(9).toString());
                    double dblBonus = 0;
                    double dblTotalRecharge = 0;
                    double dblTaxAmount=0;
                    
                    // 2008-11-11 FB. New column added for Jim
                    double dblChannelPercentage = 0;
                    
                    if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS)) //iso only and with permission
	              	{
	              		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	              		{
	              			dblChannelPercentage = (dblISOCommission + dblRepCommission + dblMerchantCommission) /  dblTotalSales * 100;
	              		}
	              		else if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	              		{
		                	dblChannelPercentage = (dblISOCommission + dblAgentCommission + dblSubAgentCommission + dblRepCommission + dblMerchantCommission) / dblTotalSales * 100;
		                }
	              	}

                    	dblBonus = Double.parseDouble(vecTemp.get(10).toString());
                    	dblTotalRecharge = Double.parseDouble(vecTemp.get(11).toString()); 
                    
                    intTotalQtySum = intTotalQtySum + intTotalQty;
                    dblTotalSalesSum = dblTotalSalesSum + dblTotalSales;
                    dblTotalBonus += dblBonus;
                    dblTotalRechargeSum += dblTotalRecharge;
                    dblMerchantCommissionSum=dblMerchantCommissionSum + dblMerchantCommission;
                    dblRepCommissionSum = dblRepCommissionSum + dblRepCommission;
                    dblSubAgentCommissionSum = dblSubAgentCommissionSum + dblSubAgentCommission;
                    dblAgentCommissionSum = dblAgentCommissionSum + dblAgentCommission;
                    dblISOCommissionSum = dblISOCommissionSum + dblISOCommission;
                    dblAdjAmountSum = dblAdjAmountSum + dblAdjAmount;

                    String sMerchantIds = "";
                    if ( request.getParameterValues("merchantIds") != null )
                    {
                      sMerchantIds = "&merchantIds=" + TransactionReport.getMerchantIds();
                    }

                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td>" + intTotalQty + "</td>" +
                                "<td align=right>" +
                                "<a href=\"admin/reports/transactions/pinmoney_transactions.jsp?" +
                                "startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +
                                "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +
                                "&productId=" + vecTemp.get(1) +
                                "&transactionID=" + request.getParameter("transactionID") + "&PINNumber=" + request.getParameter("PINNumber") +
                                "&search=y" + sMerchantIds + "\" target=\"_blank\">" +
                                NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");

if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommission))  + "</td>");
  }

				    if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
{
	  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommission))  + "</td>");
}

					if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommission))  + "</td>");
  }

if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommission))  + "</td>");
  }

out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommission))  + "</td>");
					

// 2008-11-11 FB. New column added for Jim
if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
{
	out.println("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblChannelPercentage))  + "</td>");
}

if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmount))  + "</td>");
  }

out.println("</tr>");

                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
                  
                  
                  
            //********************************************************************************//      
            //***************************SUMMARY REGION**************************************//      
            %>
            <tr class=row<%=intEvenOdd%>>
            <td colspan=3 align=right><%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
            <td align=left><%=intTotalQtySum%></td>
            <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum))%></td>
<%
if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommissionSum))  + "</td>");
  }

			if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommissionSum))  + "</td>");
  }
  			
			if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommissionSum))  + "</td>");
  }

if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommissionSum))  + "</td>");
  }
			
out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommissionSum))+ "</td>");
			

if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
{	
	if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	{
		dblChannelPercentageSum = (dblISOCommissionSum + dblRepCommissionSum + dblMerchantCommissionSum) /  dblTotalSalesSum * 100;
	}
	else if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	{
	 	dblChannelPercentageSum = (dblISOCommissionSum + dblAgentCommissionSum + dblSubAgentCommissionSum + dblRepCommissionSum + dblMerchantCommissionSum) / dblTotalSalesSum * 100;
	}

   out.println("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblChannelPercentageSum))  + "</td>");
}

if (strAccessLevel.equals(DebisysConstants.ISO))
  {
   out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmountSum))  + "</td>");
  }
  
out.println("</tr></table>");
			
			//********************************************************************************//      
            //***************************END SUMMARY REGION**************************************//   
}
else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>
