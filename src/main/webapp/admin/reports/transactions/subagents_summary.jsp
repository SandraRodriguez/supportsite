<%@page import="com.debisys.utils.DbUtil"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
 Vector vecSearchResults = new Vector();
 Vector warningSearchResults = new Vector();	
 Hashtable searchErrors = null;
 ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
 String strRepIds[] = null;
 ArrayList<String> titles = new ArrayList<String>();

 String noteTimeZone = "";
 String testTrx = "";  
 String titleReport = "";
 	
if (request.getParameter("search") != null)
{
  if (TransactionReport.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    strRepIds = request.getParameterValues("rids");
    if (strRepIds != null) {
            TransactionReport.setRepIds(strRepIds);
    } else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
            // Try to fill subagent IDs by agent id
            String agentId = request.getParameter("agentId");
            if (agentId != null) {
                List<String> lngSubAgentIds = DbUtil.getRepEntityChildIds(application, Long.valueOf(agentId));
                if (!lngSubAgentIds.isEmpty()) {
                    strRepIds = new String[lngSubAgentIds.size()];
                    strRepIds = lngSubAgentIds.toArray(strRepIds);
                    TransactionReport.setRepIds(strRepIds);
                }
            }
    }
  
    
    boolean hasPermissionIntTax = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                                                                                        DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
     										DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
     										
    //////////////////////////////////////////////////////////////////
	//HERE WE DEFINE THE REPORT'S HEADERS 
	headers = com.debisys.reports.ReportsUtil.getHeadersSummaryTransactions(SessionData,application, DebisysConstants.SUBAGENT );
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	
	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  	noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
	String keyLanguage = "jsp.admin.reports.transaction.subagent_summary";
	titleReport = Languages.getString( keyLanguage , SessionData.getLanguage() );
	testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());
	        
	if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	{
		//TO SCHEDULE REPORT
		titles.add(noteTimeZone);
		titles.add(titleReport);		
        titles.add(testTrx);
		TransactionReport.getSubAgentSummary( SessionData, application, DebisysConstants.SCHEDULE_REPORT, headers, titles );
		ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
		if (  scheduleReport != null  )
		{
			scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
			scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
			//scheduleReport.setAdditionalData("");
			scheduleReport.setTitleName( keyLanguage );   
		}	
		response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
	}
	else if ( request.getParameter("downloadReport") != null )
	{
		//TO DOWNLOAD ZIP REPORT
	    if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
		   titleReport = titleReport + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
		   titleReport = titleReport + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
		}
		titles.add(noteTimeZone);
		titles.add(titleReport);
        titles.add(testTrx);
		vecSearchResults = TransactionReport.getSubAgentSummary( SessionData, application, DebisysConstants.DOWNLOAD_REPORT, headers, titles );
		response.sendRedirect( TransactionReport.getStrUrlLocation() );			
	} 
	else
	{
		//TO SHOW REPORT
	  	if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
		   titleReport = titleReport + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
		   titleReport = titleReport + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
		}
		titles.add(noteTimeZone);
		titles.add(titleReport);
        titles.add(testTrx);
		vecSearchResults = TransactionReport.getSubAgentSummary( SessionData, application , DebisysConstants.EXECUTE_REPORT, headers, titles );		
	}	  
	
    if( hasPermissionIntTax )
	{   
		warningSearchResults = TransactionReport.getWarningMessage();
    }    
	
  }
  else
  {
   searchErrors = TransactionReport.getErrors();
  }


}
%>
<%@ include file="/includes/header.jsp" %>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.title2",SessionData.getLanguage()).toUpperCase()%></td>
    <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<!--<td colspan="3"  bgcolor="#FFFFFF">-->
	<td colspan="3" bgcolor="#FFFFFF" class="formArea2">

<%
if (searchErrors != null)
{
  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr></table>");
}

if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">

            <tr class="main"><td nowrap colspan="2"><%=noteTimeZone%><br/><br/></td></tr>
            <% 
            boolean showWarnings = false; 
            if ( showWarnings && warningSearchResults.size()>0){ %>
				<tr>
				<td class="main" style="color:red">
					<% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1)
								||  (!TransactionReport.checkfortaxtype(SessionData))){
				%>
					<%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
					<%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
				<% } %>
					<% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
						<%= warningSearchResults.get(i).toString()%> <br/>
					<% } %>
				</td>
				</tr>
				<%} %>
            <tr>
            	<td class="main">
					<%= titleReport %><br/>
					<font color="#ff0000"><%= testTrx %></font>
				</td>
			</tr>
            </table>
            
            <form name="downloadData" method=post action="admin/reports/transactions/subagents_summary.jsp">
	              <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
	              <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
	              <input type="hidden" name="search" value="y">
	              <input type="hidden" name="downloadReport" value="y">
	            	<%for(int i=0;i<strRepIds.length;i++){%>
					<input type="hidden" name="rids" value="<%=strRepIds[i]%>">
					<%}%>
	              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		    </form>
		              
            <table>
                  <tr>
                      <td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
                  </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2" class="sort-table">
            <tr>
            <%
            String strSortURL = "admin/reports/transactions/subagents_summary.jsp?search=y&startDate=" + TransactionReport.getStartDate() + "&endDate=" + TransactionReport.getEndDate() + "&rids=" + TransactionReport.getRepIds();
            %>
              <td class=rowhead2 valign=bottom>#</td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.business_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=1&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=1&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
<%
                if ( strAccessLevel.equals(DebisysConstants.ISO) && 
                		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
                {
%>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.customers.merchants_info.address",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=11&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=11&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=12&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=12&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=13&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=13&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=14&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=14&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
<%
                }
%>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=2&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=2&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=3&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=3&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
              <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=4&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=4&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
			  <%//DBSY-905
				if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
				%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=15&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=15&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=16&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=16&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
					  
			        <% if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
			    	  {%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=17&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=17&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=18&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=18&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
					<%}
				}%>
			  

<%
if (strAccessLevel.equals(DebisysConstants.ISO))
  {%>
  <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=9&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=9&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
 <%if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
    { 
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_iso_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=23&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=23&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
		} 
	}

  if ((strAccessLevel.equals(DebisysConstants.ISO)
    || strAccessLevel.equals(DebisysConstants.AGENT))
    && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {%>
  <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=8&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=8&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
 <%if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
    { 
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_agent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=22&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=22&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
		} 
}

  if (!strAccessLevel.equals(DebisysConstants.REP)
    && !strAccessLevel.equals(DebisysConstants.MERCHANT)
    && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {%>
  <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=7&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=7&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
 <%if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
    { 
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_subagent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=21&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=21&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
		} 
		}


if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
  {
%>
  <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=6&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=6&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
<%if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
    { 
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_rep_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=20&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=20&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
		} 
		}


  out.println("<td class=rowhead2 valign=bottom>"+Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() +"&nbsp;<a href=\"" + strSortURL + "&col=5&sort=1\"><img src=\"images/down.png\" height=11 width=11 border=0></a><a href=\"" + strSortURL + "&col=5&sort=2\"><img src=\"images/up.png\" height=11 width=11 border=0></a></td>");
  if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
    { 
%>
  <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.net_merchant_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&amp;col=19&amp;sort=1"><img width="11" height="11" border="0" src="images/down.png"></a><a href="<%=strSortURL%>&amp;col=19&amp;sort=2"><img width="11" height="11" border="0" src="images/up.png"></a></TD>
<% 
		} 
		
  
  if (strAccessLevel.equals(DebisysConstants.ISO))
{%>
  <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.adjustment",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=10&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=10&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
<%}

  out.println("</tr>");

                  double dblTotalSalesSum = 0;
                  double dblMerchantCommissionSum = 0;
                  double dblRepCommissionSum = 0;
                  double dblSubAgentCommissionSum = 0;
                  double dblAgentCommissionSum = 0;
                  double dblISOCommissionSum = 0;
                  double dblAdjAmountSum = 0;
                  
                  double dblTaxMerchantCommissionSum = 0;
                  double dblTaxRepCommissionSum = 0;
                  double dblTaxSubAgentCommissionSum = 0;
                  double dblTaxAgentCommissionSum = 0;
                  double dblTaxISOCommissionSum = 0;
                  
                  //DBSY-905
                  double dblbonus = 0;
                  double dbltotalrecharge = 0;
                  double dblnetamount = 0;
                  double dbltaxamount = 0;
                  double dblTaxMerchantCommission = 0;
                  double dblTaxRepCommission = 0;
                  double dblTaxSubAgentCommission = 0;
                  double dblTaxAgentCommission =0;
                  double dblTaxISOCommission =0;

                  int intTotalQtySum = 0;
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  int intCounter = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    int intTotalQty = Integer.parseInt(vecTemp.get(2).toString());
                    double dblTotalSales = Double.parseDouble(vecTemp.get(3).toString());
                    double dblMerchantCommission = Double.parseDouble(vecTemp.get(4).toString());
                    double dblRepCommission = Double.parseDouble(vecTemp.get(5).toString());
                    double dblSubAgentCommission = Double.parseDouble(vecTemp.get(6).toString());
                    double dblAgentCommission = Double.parseDouble(vecTemp.get(7).toString());
                    double dblISOCommission = Double.parseDouble(vecTemp.get(8).toString());
                    double dblAdjAmount = Double.parseDouble(vecTemp.get(9).toString());
                    		 if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
								   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
									&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
													) {
                     dblTaxMerchantCommission = Double.parseDouble(vecTemp.get(18).toString());
                     dblTaxRepCommission = Double.parseDouble(vecTemp.get(19).toString());
                     dblTaxSubAgentCommission = Double.parseDouble(vecTemp.get(20).toString());
                     dblTaxAgentCommission = Double.parseDouble(vecTemp.get(21).toString());
                     dblTaxISOCommission = Double.parseDouble(vecTemp.get(22).toString());
                     dblTaxMerchantCommissionSum=dblTaxMerchantCommissionSum + dblTaxMerchantCommission;
                     dblTaxRepCommissionSum = dblTaxRepCommissionSum + dblTaxRepCommission;
                     dblTaxSubAgentCommissionSum = dblTaxSubAgentCommissionSum + dblTaxSubAgentCommission;
                     dblTaxAgentCommissionSum = dblTaxAgentCommissionSum + dblTaxAgentCommission;
                     dblTaxISOCommissionSum = dblTaxISOCommissionSum + dblTaxISOCommission;
                    }
                  	//DBSY-905
                    if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
						dblbonus +=  Double.parseDouble(vecTemp.get(14).toString());
						dbltotalrecharge +=  Double.parseDouble(vecTemp.get(15).toString()); 
						if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
			    	  	{
			    	  		dblnetamount +=  Double.parseDouble(vecTemp.get(16).toString());
			    	  		dbltaxamount +=  Double.parseDouble(vecTemp.get(17).toString());
			    	  	}
					}
                    
                    
                    dblTotalSalesSum = dblTotalSalesSum + dblTotalSales;
                    dblMerchantCommissionSum=dblMerchantCommissionSum + dblMerchantCommission;
                    dblRepCommissionSum = dblRepCommissionSum + dblRepCommission;
                    dblSubAgentCommissionSum = dblSubAgentCommissionSum + dblSubAgentCommission;
                    dblAgentCommissionSum = dblAgentCommissionSum + dblAgentCommission;
                    dblISOCommissionSum = dblISOCommissionSum + dblISOCommission;
                    dblAdjAmountSum = dblAdjAmountSum + dblAdjAmount;
                     
                    
                    intTotalQtySum = intTotalQtySum + intTotalQty;

                            String sLocation = "";
                            if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
                            {
                              sLocation = "<td>" + vecTemp.get(10).toString() + "</td><td>" + vecTemp.get(11).toString() + "</td><td>" + vecTemp.get(12).toString() + "</td><td>" + vecTemp.get(13).toString() + "</td>";
                            }

                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                sLocation +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td align=right>" + intTotalQty + "</td>" +
                                "<td align=right><a href=\"admin/transactions/subagents_transactions.jsp?startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +  "&endDate="+URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")+"&search=y&repId="+ vecTemp.get(1) +"&report=y\">" + NumberUtil.formatCurrency(Double.toString(dblTotalSales))  + "</a></td>");
                  //DBSY-905
                  if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
						out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(14).toString())  + "</td>");
						out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(15).toString())  + "</td>");
						if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
			    	  	{
			    	  		out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(16).toString())  + "</td>");
							out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(17).toString())  + "</td>");
						}
				  }
if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommission))  + "</td>");
  	if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxISOCommission))  + "</td>");
	}
  }

if ((strAccessLevel.equals(DebisysConstants.ISO)
    || strAccessLevel.equals(DebisysConstants.AGENT))
    && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommission))  + "</td>");
  if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAgentCommission))  + "</td>");
	}
  }
if (!strAccessLevel.equals(DebisysConstants.REP)
    && !strAccessLevel.equals(DebisysConstants.MERCHANT)
    && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommission))  + "</td>");
    if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxSubAgentCommission))  + "</td>");
	}
  }
if (!strAccessLevel.equals(DebisysConstants.MERCHANT)) {

        if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
            out.println("<td align=right><a href=\"admin/reports/transactions/reps_summary.jsp?startDate="
                                                + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(
                                                TransactionReport.getEndDate(), "UTF-8") + "&search=y&subAgentId=" + vecTemp.get(1) + "&report=y\">"
                                                + NumberUtil.formatCurrency(Double.toString(dblRepCommission)) + "</a></td>");
        } else {
            out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommission)) + "</td>");
        }

        if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)
                && (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) {
            out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxRepCommission)) + "</td>");
        }
    }

out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommission))  + "</td>");
 if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxMerchantCommission))  + "</td>");
	}
if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmount))  + "</td>");
  }
out.println("</tr>");

                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            <tr class=row<%=intEvenOdd%>>
<%
                if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
                {
%>
                <td colspan=7 align=right>
<%
                }
                else
                {
%>
                <td colspan=3 align=right>
<%
                }
%>
            <%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
            <td align=right><%=intTotalQtySum%></td>
            <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum))%></td>
            
 			<%//DBSY-905
 			 if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
 			 %>
                 <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblbonus))%></td>
				 <td align=right><%=NumberUtil.formatCurrency(Double.toString(dbltotalrecharge))%></td>
				 
						<% if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
			    	  	{%>
			    	  		<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblnetamount))%></td>
							<td align=right><%=NumberUtil.formatCurrency(Double.toString(dbltaxamount))%></td>
<%						}
			}
			
			

if (strAccessLevel.equals(DebisysConstants.ISO))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommissionSum))  + "</td>");
   if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxISOCommissionSum))  + "</td>");
	}
  }
if ((strAccessLevel.equals(DebisysConstants.ISO)
    || strAccessLevel.equals(DebisysConstants.AGENT))
    && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommissionSum))  + "</td>");
   if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAgentCommissionSum))  + "</td>");
	}
  }
if (!strAccessLevel.equals(DebisysConstants.REP)
    && !strAccessLevel.equals(DebisysConstants.MERCHANT)
    && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommissionSum))  + "</td>");
    if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxSubAgentCommissionSum))  + "</td>");
	}
  }

if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
  {
    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommissionSum))  + "</td>");
     if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxRepCommissionSum))  + "</td>");
	}
  }
out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommissionSum))+ "</td>");
  if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
   (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					) {
		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxMerchantCommissionSum))  + "</td>");
	}
if (strAccessLevel.equals(DebisysConstants.ISO))
  {
   out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAdjAmountSum))  + "</td>");
  }  
out.println("</tr></table>");
}
else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>