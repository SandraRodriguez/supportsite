<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" %>
<%
  int section      = 4;
  int section_page = 22;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";
  Boolean AreAllMerchantsSelected = false;
  String strSalesIds[]= null;
  
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  String strRepIds[] = null;
  ArrayList<String> titles = new ArrayList<String>();
  String keyLanguage = "jsp.admin.reports.title38";
  String titleReport = Languages.getString( keyLanguage,SessionData.getLanguage() );
  String instructions = Languages.getString( "jsp.admin.reports.transactions.merchants.instructions2",SessionData.getLanguage() );
  
  if ( request.getParameter("download") != null )
  {
    String sURL = "";
    TransactionReport.setMerchantIds(request.getParameter("merchantIds"));
    TransactionReport.setSalesIds(request.getParameterValues("salesIds"));
    sURL = TransactionReport.downloadDailyLiability(application, SessionData, SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP), SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE),SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS));
    response.sendRedirect(sURL);
    return;
  }
  if (request.getParameter("search") != null)
  {
      String strMerchantIds[] = request.getParameterValues("mids");
      strSalesIds= request.getParameterValues("salesIds");
	  //Alfred A./DBSY-1082 - Modified this portion to always take merchant params for later limiting.
    if (strMerchantIds != null)
    {
        if(strMerchantIds[0] != "")
        {
            TransactionReport.setMerchantIds(strMerchantIds);
           if(strSalesIds!=null){
               if(strMerchantIds[0] != "")
            {
            TransactionReport.setSalesIds(strSalesIds);
            
             }
           }
        }
        else
        {
            String vals = request.getParameter("AllValues");
            String merchantIds[] = vals.split(":");
            String AgentTypes = request.getParameter("AgentTypes");
            String SubAgentTypes = request.getParameter("SubAgentTypes");
            String RepTypes = request.getParameter("RepTypes");

            if ((request.getParameter("AgentTypes")==null || request.getParameter("AgentTypes").equals("0"))
                    && (request.getParameter("SubAgentTypes")==null || request.getParameter("SubAgentTypes").equals("0"))
                    && (request.getParameter("RepTypes")==null || request.getParameter("RepTypes").equals("0")) )
            {
                            TransactionReport.setAreAllMerchantsSelected(true);
                            AreAllMerchantsSelected = true;
            }
            TransactionReport.setMerchantIds(merchantIds);
            TransactionReport.setSalesIds(strSalesIds);
        }
    }
      
    
	
    if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
    {
        //////////////////////////////////////////////////////////////////
        //HERE WE DEFINE THE REPORT'S HEADERS 
        headers = getHeadersDailyLiabilityReport( SessionData, strAccessLevel, true );
        //////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //TO SCHEDULE REPORT
        titles.add(titleReport);		
        titles.add(instructions);
        TransactionReport.getDailyLiability(SessionData, false, DebisysConstants.SCHEDULE_REPORT, headers, titles );
        ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
        if (  scheduleReport != null  )
        {
            scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
            scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
            //scheduleReport.setAdditionalData("");
            scheduleReport.setTitleName( keyLanguage );   
        }	
        response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
    }
    else
    {
        //////////////////////////////////////////////////////////////////
        //HERE WE DEFINE THE REPORT'S HEADERS 
        headers = getHeadersDailyLiabilityReport( SessionData, strAccessLevel, false );
        //////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        vecSearchResults = TransactionReport.getDailyLiability(SessionData, false, DebisysConstants.EXECUTE_REPORT, null, null );
    }	
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>

<%!public static ArrayList<ColumnReport> getHeadersDailyLiabilityReport(SessionData sessionData, String strAccessLevel, boolean isJasperResult )
   {
        ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
			
        if( strAccessLevel.equals(DebisysConstants.ISO) && !sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) 
        {
            headers.add(new ColumnReport("agent", Languages.getString("jsp.admin.agent", sessionData.getLanguage()).toUpperCase(), String.class, false));
        } 
        if( (strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO)) && !sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) 
        {
            headers.add(new ColumnReport("subagent", Languages.getString("jsp.admin.subagent", sessionData.getLanguage()).toUpperCase(), String.class, false));     
        } 
        if(strAccessLevel.equals(DebisysConstants.AGENT) ||strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) 
        {
            headers.add(new ColumnReport("rep", Languages.getString("jsp.admin.rep", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
         
        headers.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.merchant_id", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.merchant_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
        
        headers.add(new ColumnReport("contact_phone", Languages.getString("jsp.admin.reports.transactions.daily_liability_report.contact_phone_number", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("remaining_days", Languages.getString("jsp.admin.reports.transactions.daily_liability_report.remaining_days", sessionData.getLanguage()).toUpperCase(), Double.class, true));
        headers.add(new ColumnReport("available_credit", Languages.getString("jsp.admin.reports.transactions.daily_liability_report.available_credit", sessionData.getLanguage()).toUpperCase(), Double.class, true));
        headers.add(new ColumnReport("credit_limit", Languages.getString("jsp.admin.reports.transactions.daily_liability_report.credit_limit", sessionData.getLanguage()).toUpperCase(), Double.class, true));
        String terminalSales = Languages.getString("jsp.admin.reports.transactions.daily_liability_report.sales_since_last_adj", sessionData.getLanguage()).toUpperCase();
        if ( terminalSales !=null && isJasperResult )
        {
            terminalSales = terminalSales.replaceAll("<BR>", " ");
        }            
        headers.add(new ColumnReport("terminal_sales", terminalSales, Double.class, true));
        
        String avgAmount = Languages.getString("jsp.admin.reports.transactions.daily_liability_report.avg_sales_per_day", sessionData.getLanguage()).toUpperCase();
        if ( avgAmount!= null && isJasperResult  ) 
        {
            avgAmount = avgAmount.replaceAll("<BR>", " ");
        }
        headers.add(new ColumnReport("avg_amount", avgAmount, Double.class, true));
        
        if( sessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)) 
        {
            headers.add(new ColumnReport("island", Languages.getString("jsp.admin.reports.transactions.daily_liability_report.island", sessionData.getLanguage()).toUpperCase(), String.class, false));
            headers.add(new ColumnReport("rep", Languages.getString("jsp.admin.reports.transactions.daily_liability_report.rep", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
        if(sessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)) 
        {
            headers.add(new ColumnReport("lastpurchase", Languages.getString("jsp.admin.reports.transactions.daily_liability_report.lastpurchase", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
        if(strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)){
                    headers.add(new ColumnReport("salesman_id",Languages.getString("jsp.admin.reports.SalesRepId", sessionData.getLanguage()).toUpperCase(), String.class, false));
                    headers.add(new ColumnReport("salesmanname",Languages.getString("jsp.admin.reports.SalesRepName", sessionData.getLanguage()).toUpperCase(), String.class, false));
                     headers.add(new ColumnReport("account_no",Languages.getString("jsp.admin.reports.AccountNo", sessionData.getLanguage()).toUpperCase(), String.class, false));
         }
        return headers;
    }  
%>

  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= titleReport.toUpperCase()%></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td class="main"><font color="ff0000"><%=instructions%></font></td>
             </tr>
             <tr>
              <td class=main align=left valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>                
              </td>
            </tr>
            <tr>
            	<td colspan=2>
            <table>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>
                <FORM ACTION="admin/reports/transactions/daily_liability_report.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	<INPUT TYPE=hidden NAME=search VALUE="<%=request.getParameter("search")%>">
                	<INPUT TYPE=hidden NAME=merchantIds VALUE="<%=TransactionReport.getMerchantIds()%>">
                         <%if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
			 {
                        if(strSalesIds!=null){
                    for(int i=0;i<strSalesIds.length;i++){%>
				<input type="hidden" name="salesIds" value="<%=strSalesIds[i]%>">
				<%}}%>
                         <%}%>
                       <INPUT TYPE=hidden NAME=download VALUE="Y">
                	<%if (AreAllMerchantsSelected){TransactionReport.setAreAllMerchantsSelected(true);} %>
                	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                </FORM>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                </td>
              </tr>
            </table>
            	</td>
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>
				<%
				for(ColumnReport columnReport : headers)
				{
				  String columnName = columnReport.getLanguageDescription();
				%>				
					<td class=rowhead2 nowrap><%= columnName.toUpperCase()%></td>
				<%
				}
				%>
              </tr>
            </thead>
			<%
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;
              
            //Totals
            Double dblRemainingDaysTotal = 0d;
            double dblAvailableCredits = 0;
            double dblCreditLimit = 0;
            double dblSalesSinceLastAdj = 0;
            double dblAverageSalesPerDay = 0;
            while (it.hasNext())
            {
                Vector vecTemp = null;
                vecTemp = (Vector)it.next();
                if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
                {
                    out.print("<tr class=row" + intEvenOdd + ">" + 
                     "<td>" + intCounter++ + "</td>" + 
                     "<td nowrap>" + vecTemp.get(0) + "</td>" + 
                     "<td nowrap>" + vecTemp.get(1) + "</td>" + 
                     "<td nowrap>" + vecTemp.get(2) + "</td>" +
                       "<td nowrap>" + vecTemp.get(3) + "</td>" +
                     "<td nowrap align=right>" + vecTemp.get(4) + "</td>" );
                     
                     out.print("<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
                     "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
                     "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +
                     "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(8).toString()) + "</td>");
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)) 
                    {
                            out.print("<td nowrap>" + vecTemp.get(9) + "</td>");
                            out.print("<td nowrap>" + vecTemp.get(10) + "</td>");
                    }
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)) 
                    {
                            out.print("<td nowrap>" + vecTemp.get(11) + "</td>");
                    }  
                    if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)){
                           out.print(  "<td nowrap>" + vecTemp.get(13) + "</td>" +
                            "<td nowrap>" + vecTemp.get(14) + "</td>" +
                             "<td nowrap>" + vecTemp.get(15) + "</td>");
                       }                    
                }
                else if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                    out.print("<tr class=row" + intEvenOdd + ">" + 
                      "<td>" + intCounter++ + "</td>" + 
                      "<td nowrap>" + vecTemp.get(0) + "</td>" + 
                      "<td nowrap>" + vecTemp.get(1) + "</td>" + 
                      "<td nowrap>" + vecTemp.get(2) + "</td>" +
                      "<td nowrap>" + vecTemp.get(3) + "</td>" +
                      "<td nowrap>" + vecTemp.get(4) + "</td>" );
                       
                       out.print("<td nowrap>" + vecTemp.get(5) + "</td>" +
                      "<td nowrap align=right>" + vecTemp.get(6).toString() + "</td>" + 
                      "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +
                      "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(8).toString()) + "</td>" +
                      "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>" +
                      "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(10).toString()) + "</td>");
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)) 
                    {
                        out.print("<td nowrap>" + vecTemp.get(11) + "</td>");
                        out.print("<td nowrap>" + vecTemp.get(12) + "</td>");
                    }
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)) 
                    {
                        out.print("<td nowrap>" + vecTemp.get(13) + "</td>");
                    } 
                    if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
                       {
                           out.print(  "<td nowrap>" + vecTemp.get(14) + "</td>" +
                            "<td nowrap>" + vecTemp.get(15) + "</td>" +
                             "<td nowrap>" + vecTemp.get(16) + "</td>");
                       }                    
                }
                else if(strAccessLevel.equals(DebisysConstants.AGENT))
                {
                    out.print("<tr class=row" + intEvenOdd + ">" + 
                              "<td>" + intCounter++ + "</td>" + 
                              "<td nowrap>" + vecTemp.get(0) + "</td>" + 
                              "<td nowrap>" + vecTemp.get(1) + "</td>" + 
                              "<td nowrap>" + vecTemp.get(2) + "</td>" +
                              "<td nowrap>" + vecTemp.get(3) + "</td>" +
                              "<td nowrap>" + vecTemp.get(4) + "</td>" +
                              "<td nowrap align=right>" + vecTemp.get(5) + "</td>" + 
                              "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
                              "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +
                              "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(8).toString()) + "</td>" +
                              "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>");
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)) 
                    {
                        out.print("<td nowrap>" + vecTemp.get(10) + "</td>");
                        out.print("<td nowrap>" + vecTemp.get(11) + "</td>");
                    }
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)) {
                        out.print("<td nowrap>" + vecTemp.get(12) + "</td>");
                    }		
                }
                else if(strAccessLevel.equals(DebisysConstants.SUBAGENT) )
                {
                    out.print("<tr class=row" + intEvenOdd + ">" + 
                    "<td>" + intCounter++ + "</td>" + 
                    "<td nowrap>" + vecTemp.get(0) + "</td>" + 
                    "<td nowrap>" + vecTemp.get(1) + "</td>" + 
                    "<td nowrap>" + vecTemp.get(2) + "</td>" +
                    "<td nowrap>" + vecTemp.get(3) + "</td>" +
                    "<td nowrap align=right>" + vecTemp.get(4) + "</td>" + 
                    "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
                    "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
                    "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +
                    "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(8).toString()) + "</td>");
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)) 
                    {
                        out.print("<td nowrap>" + vecTemp.get(9) + "</td>");
                        out.print("<td nowrap>" + vecTemp.get(10) + "</td>");
                    }
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)) 
                    {
                        out.print("<td nowrap>" + vecTemp.get(11) + "</td>");
                    }		
                }
                else
                {
                    out.print("<tr class=row" + intEvenOdd + ">" + 
                    "<td>" + intCounter++ + "</td>" + 
                    "<td nowrap>" + vecTemp.get(0) + "</td>" + 
                    "<td nowrap>" + vecTemp.get(1) + "</td>" + 
                    "<td nowrap>" + vecTemp.get(2) + "</td>" +
                    "<td nowrap align=right>" + vecTemp.get(3) + "</td>" + 
                    "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(4).toString()) + "</td>" +
                    "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
                    "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
                    "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>");
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)) 
                    {
                            out.print("<td nowrap>" + vecTemp.get(8) + "</td>");
                            out.print("<td nowrap>" + vecTemp.get(9) + "</td>");
                    }
                    if(SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)) 
                    {
                            out.print("<td nowrap>" + vecTemp.get(10) + "</td>");
                    }	
                }
                out.println("</tr>");
                if (intEvenOdd == 1)
                {
                  intEvenOdd = 2;
                }
                else
                {
                  intEvenOdd = 1;
                }
                int startCounter = -1;
                //to get the totals
                if(strAccessLevel.equals(DebisysConstants.ISO) && 
                            SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
                {
                        startCounter = 4;
                }else if (strAccessLevel.equals(DebisysConstants.ISO)){
                        startCounter = 6;
                }else if (strAccessLevel.equals(DebisysConstants.AGENT)){
                        startCounter = 5;
                }else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
                        startCounter = 4;
                }else{
                        startCounter = 3;
                }            
                dblRemainingDaysTotal += Double.parseDouble(vecTemp.get(startCounter).toString());
                dblAvailableCredits += Double.parseDouble(vecTemp.get(startCounter+1).toString());
                dblCreditLimit += Double.parseDouble(vecTemp.get(startCounter+2).toString());
                dblSalesSinceLastAdj += Double.parseDouble(vecTemp.get(startCounter+3).toString());
                dblAverageSalesPerDay += Double.parseDouble(vecTemp.get(startCounter+4).toString());            	            	
            }
%> 
			<tfoot>
			<tr class=row<%=intEvenOdd%>>
<%
if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
%>
				<td colspan=5 align=right>
<%
}else if(strAccessLevel.equals(DebisysConstants.ISO)) {
 %>
				 <td colspan=7 align=right>
<%
 }else if(strAccessLevel.equals(DebisysConstants.AGENT)) {
%>
				<td colspan=6 align=right>
<%
}else if(strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
%>
				<td colspan=5 align=right>
<%
}else {
%>
				<td colspan=4 align=right>
<%
}
%>
				<%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
                                <td align=right><%= NumberUtil.formatAmount(dblRemainingDaysTotal.toString())%></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblAvailableCredits)) %></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblCreditLimit)) %></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblSalesSinceLastAdj)) %></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblAverageSalesPerDay))%></td>
			</tr>
			</tfoot>          
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
            
                    //<!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number"],0,false,false);
  if(<%=strAccessLevel.equals(DebisysConstants.ISO)&& SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){
            if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE) && SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)%> ) {
                   if(<%= SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number","CaseInsensitiveString","CaseInsensitiveString","DateTime","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
		    }
                else
                {
                   stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number","CaseInsensitiveString","CaseInsensitiveString","DateTime"],0,false,false);
                }
             }
	    else if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)%>) {
                       if(<%= SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "CaseInsensitiveString" , "CaseInsensitiveString","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
                    }else{
                        stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "CaseInsensitiveString" , "CaseInsensitiveString"],0,false,false);
                    }
		}else if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)%>) {
                     if(<%= SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime", "CaseInsensitiveString" , "CaseInsensitiveString","CaseInsensitiveString"],0,false,false);
                    }else{
                        stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime"],0,false,false);
                    }
		} else  {
                      if(<%= SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)%>){
		 	stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString"],0,false,false);
                    }else{
                        stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number"],0,false,false);
                    }
	  	}  
  }else if(<%=strAccessLevel.equals(DebisysConstants.ISO)%>){
            if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE) && SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)%> ) {
                   if(<%= SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString",  "Number", "Number", "Number", "Number", "Number", "Number", "CaseInsensitiveString","CaseInsensitiveString","DateTime","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
		    }
                else
                {
                     stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "CaseInsensitiveString","CaseInsensitiveString","DateTime"],0,false,false);
                }
             }
		else if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)%>) {
                    if(<%= SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "CaseInsensitiveString" , "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
		}else
                {
                    stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "CaseInsensitiveString" , "CaseInsensitiveString"],0,false,false);
                } 
             }  else if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)%>) {
                   if(<%= SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
		    }
                else
                {
                    stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime"],0,false,false);
                }
             }
              else  { 
                  if(<%= SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)%>){
  			stT1 = new SortROC(document.getElementById("t1"),
  			["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString"],0,false,false);
  		}else{
                       stT1 = new SortROC(document.getElementById("t1"),
  			["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number"],0,false,false); 
                }
            }
  }else if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){
		if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)%>) {
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "CaseInsensitiveString" , "CaseInsensitiveString"],0,false,false);
		}else if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)%>) {
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime"],0,false,false);
		} else  {   
  			stT1 = new SortROC(document.getElementById("t1"),
  			["None","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number"],0,false,false);
  		}
  }else if(<%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>){
		if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP)%>) {
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "CaseInsensitiveString" , "CaseInsensitiveString"],0,false,false);
		}else if(<%=SessionData.checkPermission(DebisysConstants.ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE)%>) {
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime"],0,false,false);
		} else  {    
  			stT1 = new SortROC(document.getElementById("t1"),
  			["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number"],0,false,false);
  		}
  }
  //-->
            
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
