<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":"
			+ request.getServerPort() + path + "/";
%>
<%@ page
	import="java.net.URLEncoder,com.debisys.utils.HTMLEncoder,java.util.*,com.debisys.customers.Merchant,com.debisys.utils.NumberUtil"%>
<%
	int section = 4;
	int section_page = 25;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
    
<%
    	Vector vecSearchResults = new Vector();
    	Hashtable searchErrors = null;
    	String start_date = "";
    	String end_date = "";
    	String merchant = "";
    	boolean blnIso = false;
    	String pincase = "";
    	
    	boolean fetchReport = true;
    	
    	if (request.getParameter("search") != null)
    	{
    	   pincase = request.getParameter("CaseNumber");
    	   if (pincase != null && !pincase.equals(""))
    	   {
    	       start_date = null; end_date = null; merchant = null;    	       
    	   }
    	   else
    	   {
	    		if (TransactionReport.validateDateRange(SessionData))
	    		{
	    			start_date = request.getParameter("startDate");
	    			end_date = request.getParameter("endDate");
	    		}
	    		else
	    		{
	    			searchErrors = TransactionReport.getErrors();
	    			fetchReport = false;
	    		}
	    	}
	    	
	    	if (fetchReport) 
	    	{
	    	    String merchantTemp = "";
                    
	            blnIso = true;
	            merchantTemp = request.getParameter("merchantList");
	            if (merchantTemp != null)
	            {
	                if (merchantTemp.equals(""))
	                {                           
	                    merchant = TransactionReport.getMerchantListReports(SessionData);
	                }
	                else
	                {
	                    blnIso = false;
	                    merchant = merchantTemp;
	                }
	            }
	            else // case for merchant level!
	            {                       
	                merchant = TransactionReport.getMerchantListReports(SessionData);
	            }
	    		vecSearchResults = TransactionReport.getWriteOffTransactions(start_date, end_date,
	                            merchant, blnIso);
            }
    	}
    %>
<%@ include file="/includes/header.jsp" %>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%=Languages.getString("jsp.admin.reports.write_off_request_report",SessionData.getLanguage()).toUpperCase()%></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" class="formArea2">
            <%
            	if (searchErrors != null)
            	{
            		out
            				.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"
            						+ Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
            		Enumeration enum1 = searchErrors.keys();
            		while (enum1.hasMoreElements())
            		{
            			String strKey = enum1.nextElement().toString();
            			String strError = (String) searchErrors.get(strKey);
            			out.println("<li>" + strError);
            		}
            		out.println("</font></td></tr></table>");
            	}
            	if (vecSearchResults != null && vecSearchResults.size() > 0)
            	{
            %>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
            <%if (start_date!=null && end_date!=null) {%>
              <td class="main">
                <%=Languages.getString("jsp.admin.reports.write_off_request_report_type_title_date",SessionData.getLanguage())%> 
                <BR>
                <%=DateUtil.formatDateTime(start_date)%> - <%=DateUtil.formatDateTime(end_date)%>
              </td>
            <%} %>
              <td class=main align=right valign=bottom>
                <%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%>
              </td>
            </tr>
             <tr>
              <td class="main">
                  <form method="post" action="admin/reports/transactions/write_off_request_export.jsp">
                            <label><%=Languages
                                        .getString("jsp.admin.reports.transactions.write_off_request_report.downloadLabel",SessionData.getLanguage())%></label>
							<input type="submit"								
								value="<%=Languages
										.getString("jsp.admin.reports.transactions.write_off_request_report.download",SessionData.getLanguage())%>">
							<input type="hidden" value="<%=start_date%>" name="startdate">
                      <input type="hidden" value="<%=end_date%>" name="enddate">
                      <input type="hidden" value="<%=merchant%>" name="merchant"> 
                      <input type="hidden" value="<%=blnIso%>" name="blnIso">
                  </form>                   
              </td>             
            </tr>          
            <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
                <thead>
                <tr class="SectionTopBorder">
                    <td class=rowhead2 align=center>#
                    </td>
					<td class=rowhead2 align=center><%=Languages
								.getString("jsp.admin.reports.write_off_request_report.grid.RecId",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center><%=Languages
										.getString("jsp.admin.reports.write_off_request_report.grid.Datetime",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center><%=Languages
										.getString("jsp.admin.reports.write_off_request_report.grid.BusinessName",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center><%=Languages
								.getString("jsp.admin.reports.write_off_request_report.grid.Site",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center><%=Languages
										.getString("jsp.admin.reports.write_off_request_report.grid.SKU",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center><%=Languages
										.getString("jsp.admin.reports.write_off_request_report.grid.ProdDescription",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
					<td class=rowhead2 align=center><%=Languages
								.getString("jsp.admin.reports.write_off_request_report.grid.amount",SessionData.getLanguage()).toUpperCase()%>&nbsp;
					</td>
				</tr>
			</thead>	
					<%
							Iterator it = vecSearchResults.iterator();
								int intEvenOdd = 1;
								int count = 1;
								while (it.hasNext())
								{
									Vector vecTemp = null;
									vecTemp = (Vector) it.next();
									out.println("<tr class=row" + intEvenOdd + ">");
									out.println("<td>" + (count++) + "</td>");
									out
											.println("<td nowrap>"
													+ "<a href=\"admin/reports/transactions/write_off_request_trx_summary.jsp?"
													+ "trxId=" + vecTemp.get(0)+ "\">" + vecTemp.get(0) + "</a>"
													+ "</td>");
									out.println("<td>" + vecTemp.get(1) + "</td>");
									out.println("<td>" + vecTemp.get(2) + "</td>");
									out.println("<td>" + vecTemp.get(3) + "</td>");
									out.println("<td>" + vecTemp.get(4) + "</td>");
									out.println("<td>" + vecTemp.get(5) + "</td>");
									out.println("<td>" + vecTemp.get(6) + "</td>");
									out.println("</tr>");
									
									if (intEvenOdd == 1)
									{
										intEvenOdd = 2;
									}
									else
									{
										intEvenOdd = 1;
									}
								}
								vecSearchResults.clear();
						%>
                </tr>
            </table>
            <%
            	}
            	else
            		if (vecSearchResults.size() == 0 && request.getParameter("search") != null
            				&& searchErrors == null)
            		{
            			out.println("<br><br><font color=ff0000>"
            					+ Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
            		}
            %>
            <SCRIPT type="text/javascript">
	            <!--
					  var stT1 = new SortROC(document.getElementById("t1"),
					  		["None","Integer", "String", "String", "Integer", "Integer", "String", "Number"],
					  		0,false,false);
	             -->            
            </SCRIPT>
        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp"%>
