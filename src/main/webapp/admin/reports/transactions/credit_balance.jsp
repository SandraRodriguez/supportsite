<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.users.SessionData" %>
<%
int section=4;
int section_page=22;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}

/*
 * Alfred A./DBSY-1082 - Generic xmlDoc loading depending on the active browser
 */
function createXMLDoc(){
	if(window.ActiveXObject){
		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
	}else if(document.implementation && document.implementation.createDocument){
		xmlDoc = document.implementation.createDocument("", "", null);
	} 
	else{
		alert('Form is not supported in your browser');
		return;
	}
}

//Alfred A./DBSY-1082 - Generic function to generate an array of values from a form.
function returnOptionsValues(formOptions){
	var paramArray = new Array();
	arrayCounter = 0;
	for(var i = 0;i<formOptions.length;i++){
		if(formOptions.options[i].value != 0 && formOptions.options[i].value != -1){
			paramArray[arrayCounter] = formOptions.options[i].value;
			arrayCounter++;
		}
	}
	return paramArray;
}

function generateURL(mainURL, additionalStringParam, paramArray, singleParam){
	var url = null;
	if(paramArray.length != 0){
		url = mainURL+paramArray[0];
		for(var i = 1; i < paramArray.length; i++){
			if(i < 100)
				url = url + additionalStringParam + paramArray[i];
		}
	}else{
		url = mainURL+singleParam;
	}
	return url;
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the ISO version.
 */
function importISOXML(){
	createXMLDoc();
	xmlDoc.async = false;
	var isoParam = <%=SessionData.getProperty("ref_id")%>;
	var url = "admin/reports/transactions/data_daily_liability.jsp?isoParam="+isoParam;
	var loaded = xmlDoc.load(url);
	if(loaded){
		createAgentDropDown(xmlDoc);
	}
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the Agent version.
 */
function importAgentXML(){
	createXMLDoc();
	xmlDoc.async = false;
	var agentParam = null;
	var agentParamArray = new Array();
	//Makes sure we take our own id if we're an agent.
	if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){
		agentParam = <%=SessionData.getProperty("ref_id")%>;
	}else{
		agentParam = document.mainform.AgentTypes.value;
		if(agentParam == 0){
			agentParamArray = returnOptionsValues(document.mainform.AgentTypes);
		}
	}
	if(agentParamArray.length > 100){
		var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
	}else{
		var url = generateURL("admin/reports/transactions/data_daily_liability.jsp?a=", "&a=", agentParamArray, agentParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createSubAgentDropDown(xmlDoc);
	}
}


/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the SubAgent version.
 */
function importSubAgentXML(){
	createXMLDoc();
	xmlDoc.async = false;
	var subAgentParam = null;
	var subAgentParamArray = new Array();
	if(<%=strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){
		subAgentParam = <%=SessionData.getProperty("ref_id")%>;
	}else if(<%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>){
		subAgentParam = <%=SessionData.getProperty("ref_id")%>;
	}
	else{
		subAgentParam = document.mainform.SubAgentTypes.value;
		if(subAgentParam == 0){
			subAgentParamArray = returnOptionsValues(document.mainform.SubAgentTypes);
		}
	}
	if(subAgentParamArray.length > 100){
		if(<%=strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){//You're an iso, and you've got a lot of subagents.  Agent value needs to be passed as well
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&subAgentTooLong="+document.mainform.AgentTypes.value+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){//Basically means you're an agent at this point, and you had tons of subagents underneath that are going to error when creating the reps dropdown.
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&subAgentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}
	}else{
		var url = generateURL("admin/reports/transactions/data_daily_liability.jsp?a=", "&a=", subAgentParamArray, subAgentParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createRepDropDown(xmlDoc);
	} else {
                    // display error message
                var errorMsg = null;
                if (xmlDoc.parseError && xmlDoc.parseError.errorCode != 0) {
                    errorMsg = "XML Parsing Error: " + xmlDoc.parseError.reason
                              + " at line " + xmlDoc.parseError.line
                              + " at position " + xmlDoc.parseError.linepos;
                }
                else {
                    if (xmlDoc.documentElement) {
                        if (xmlDoc.documentElement.nodeName == "parsererror") {
                            errorMsg = xmlDoc.documentElement.childNodes[0].nodeValue;
                        }
                    }
                }
                if (errorMsg) {
                    alert (errorMsg);
                }
                else {
                    alert ("There was an error while loading XML file!");
                }
            }
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the Rep version.
 */
function importRepXML(){
	createXMLDoc();
	xmlDoc.async = false;
	var repParam = null;
	var repParamArray = new Array();
	//Makes sure we take our own id if we're a rep.
	if(<%=strAccessLevel.equals(DebisysConstants.REP)%>){
		repParam = <%=SessionData.getProperty("ref_id")%>;
	}else{
		repParam = document.mainform.RepTypes.value;
		if(repParam == 0){
			repParamArray = returnOptionsValues(document.mainform.RepTypes);
		}
	}
	//Needs to handle iso, agent, and subagent here.
	if(repParamArray.length > 100){
		if(<%=strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){//You're an iso, and you've got a lot of subagents.  Agent value needs to be passed as well
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+document.mainform.SubAgentTypes.value+"&subAgentTooLong="+document.mainform.AgentTypes.value+"&agentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){//Basically means you're an agent at this point, and you had tons of subagents underneath that are going to error when creating the reps dropdown.
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+document.mainform.SubAgentTypes.value+"&subAgentTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}else if(<%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>){
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+<%=SessionData.getProperty("ref_id")%>;
		}
	}else{
		var url = generateURL("admin/reports/transactions/data_daily_liability.jsp?r=", "&r=", repParamArray, repParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createMerchantList(xmlDoc);
	}
}

/*
 * Alfred A./DBSY-1082 - Function that dynamically clear out form locations that
 * no longer need to be filled in anymore.
 */
function clearOptions(levelToClear){
	if(levelToClear == 0){
		var agentDropDown = document.mainform.SubAgentTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
	}else if(levelToClear == 1){
		var agentDropDown2 = document.mainform.RepTypes;
		for(var a = agentDropDown2.options.length;a>=0;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 2){
		var agentDropDown2 = document.mainform.mids;
		for(var a = agentDropDown2.options.length;a>=1;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 3){
		var agentDropDown = document.mainform.RepTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
		var agentDropDown2 = document.mainform.mids;
		for(var a = agentDropDown2.options.length;a>=1;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 4){
		var agentDropDown = document.mainform.SubAgentTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
		var agentDropDown2 = document.mainform.RepTypes;
		for(var a = agentDropDown2.options.length;a>=0;a--){
			agentDropDown2.options[a] = null;
		}
		var agentDropDown3 = document.mainform.mids;
		for(var a = agentDropDown3.options.length;a>=1;a--){
			agentDropDown3.options[a] = null;
		}
	}
}

/*
 * Alfred A./DBSY-1082 - Generic function that takes in a form and XML data,
 * which will then populate that form with the data.
 */
function genericFillIn(formType, x){
	//formType.options[0] = new Option("Select Option", -1);
	//formType.options[0] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_one",SessionData.getLanguage())%>", -1);
	formType.options[0] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage())%>", 0);
	//formType.options[1] = new Option("<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage())%>", 0);
	var dropDownCounter = 0;
	for (i=0;i<x.length;i++)
	{
		if(<%=SessionData.getProperty("ref_id")%> == x[i].getAttribute("val")){
  			continue;
  		}
		var name = x[i].firstChild.nodeValue;
		var value = x[i].getAttribute("val");
  		
  		formType.options[dropDownCounter+1] = new Option(name, value);
  		dropDownCounter++;
	}
}

// Alfred A./DBSY-1082 - These 3 functions merely set up the generic fill-in above.
function createAgentDropDown(xmlDoc){
	genericFillIn(document.mainform.AgentTypes, xmlDoc.getElementsByTagName("display"));
	importAgentXML();
}

function createSubAgentDropDown(xmlDoc){
	clearOptions(4);
	genericFillIn(document.mainform.SubAgentTypes, xmlDoc.getElementsByTagName("display"));
	importSubAgentXML();
}

function createRepDropDown(xmlDoc){
	clearOptions(3);
	genericFillIn(document.mainform.RepTypes, xmlDoc.getElementsByTagName("display"));
	importRepXML();
}

/*
 * Alfred A./DBSY-1082 - Slightly different function that functions like the generic fill in above, 
 * but also gives a hidden attribute a value in case the user has selected the ALL option in Merchants.
 */
function createMerchantList(xmlDoc){
	clearOptions(2);
	var agentDropDown = document.mainform.mids;
	var x = xmlDoc.getElementsByTagName("display");
	var allValues = new Array();
	var dropDownCounter = 0;
	for (i=0;i<x.length;i++)
	{
		if(<%=SessionData.getProperty("ref_id")%> == x[i].getAttribute("val")){
  			continue;
  		}
		var name = x[i].firstChild.nodeValue;
		var value = x[i].getAttribute("val");
		allValues[i] = value;
  		
  		agentDropDown.options[dropDownCounter+1] = new Option(name, value);
  		dropDownCounter++;
	}
	
	document.mainform.AllValues.value = allValues.join(":");
}

</SCRIPT>
<%
if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
%>
<body onload="importSubAgentXML()">
<%
}else if(strAccessLevel.equals(DebisysConstants.ISO)) {
%>
<body onload="importISOXML()">
<%
}
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title38",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="post" action="admin/reports/transactions/credit_balance_repoirt.jsp" onSubmit="scroll();" id="daily_form">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
					<table width=400>
					<%
					if(strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
					%>
					    	 <tr class="main">
								 <td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.agent.select",SessionData.getLanguage())%></td>
							     <td><SELECT NAME="AgentTypes" onchange="importAgentXML()"></SELECT></td>
							 </tr>	
							 
						   <tr></tr><tr></tr>
					<%
					}if((strAccessLevel.equals(DebisysConstants.ISO)|| strAccessLevel.equals(DebisysConstants.AGENT)) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
					%>
							<tr class="main">
								<td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.sub_agent.select",SessionData.getLanguage())%></td>
								<td><SELECT NAME="SubAgentTypes" onchange ="importSubAgentXML()"> </SELECT></td>
							</tr>
							
							<tr></tr><tr></tr>
					<%
					}if(strAccessLevel.equals(DebisysConstants.ISO)) {
					%>		 
							<tr class="main">
								<td nowrap><%=Languages.getString("jsp.admin.reports.transactions.daily_liability.rep.select",SessionData.getLanguage())%></td>
								<td><SELECT NAME="RepTypes" onchange="importRepXML()"> </SELECT></td>
							</tr>
							
							<tr></tr><tr></tr>
					<%
					}
					%>
							 
					<tr>
							<td class=main valign=top nowrap> <%=Languages.getString("jsp.admin.reports.transactions.daily_liability.merchant.select",SessionData.getLanguage())%></td>
					        <td class=main valign=top>
								<select name="mids" size="10" style='width:250px;' multiple>
								  <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
								  <%
								  if(strAccessLevel.equals(DebisysConstants.REP)){
									Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
									Iterator it = vecMerchantList.iterator();
									while (it.hasNext())
									{
										Vector vecTemp = null;
										vecTemp = (Vector) it.next();
										out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
									}
								  }
								   %>
								</select>	
					         <br /><br />
					        *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%><br/>
					        <%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions2",SessionData.getLanguage())%><br/>
					        <%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions3",SessionData.getLanguage())%>
						</td>
					</tr>
					<tr>
					    <td class=main colspan=2 align=center>
					      <input type="hidden" name="search" value="y">
					      <input type="hidden" name="AllValues">
					      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
					    </td>
					</tr>
					
					</table>
               </td>
              </tr>
              </form>
            </table>

          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>