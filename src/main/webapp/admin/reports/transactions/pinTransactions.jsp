<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport,
                 java.util.*" %>
<%@page import="com.debisys.tools.MarkedCollection"%>
<%@page import="com.debisys.utils.TimeZone"%>
<%@page import="com.debisys.reports.TransactionReport"%>
<%@page import="com.debisys.customers.CustomerSearch"%>
<jsp:useBean id="task" scope="session" class="com.debisys.utils.TaskBean"></jsp:useBean>
<%
int section=4;
int section_page=1;
boolean downloadbar  = false;
boolean maxdownloadbar  = false;
int maxdownloadset=-1 ;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
MarkedCollection<String> markedRow = null;
String markers = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String strMerchIds = "";

//DBSY-905
int imaxdownloadbar = -1;
String positionMarkers = null;
int intSectionPage = 0;
int recordcount = 0;
boolean isDownload = false;
 String repId  = "";


//this flag means if the user is in International Default
boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
   									DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
   
boolean viewReferenceCard = false;
   if ( isInternationalDefault ){
   	viewReferenceCard = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);							
}

if (request.getParameter("download") != null)
{
  if (request.getParameter("section_page") != null)
  {
    try
    {
      intSectionPage=Integer.parseInt(request.getParameter("section_page"));
    }
    catch(NumberFormatException ex)
    {
     response.sendRedirect(request.getHeader("referer"));
     return;
    }
  }  
      
    if( request.getParameter("repId") != null )
	{
	  repId = request.getParameter("repId");
	  TransactionSearch.setRepId( repId );
	}
	
    if (request.getParameter("markers") != null) {
        positionMarkers = request.getParameter("markers"); 
    } 
    
    if (request.getParameter("invoiceID") != null)
    {
    	TransactionSearch.setInvoiceNo(request.getParameter("invoiceID").toString());
    }
      //DBSY-908 
    if (request.getParameter("recordcount") != null) {
        recordcount = Integer.valueOf(request.getParameter("recordcount")); 
    } 
      //DBSY-908 
    if (request.getParameter("downloadbar") != null) {
        downloadbar = Boolean.valueOf(request.getParameter("downloadbar")); 
    } 
      //DBSY-908 
    if (request.getParameter("maxdownloadbar") != null) {
        imaxdownloadbar = Integer.valueOf(request.getParameter("maxdownloadbar")); 
    } 
    
    if (request.getParameter("merchantIds") != null) {
        TransactionSearch.setMerchantIds(request.getParameter("merchantIds"));
    }

  if (TransactionSearch.validateDateRange(SessionData))
  {
    if ( request.getParameter("millennium_no") != null )
    {
    	TransactionSearch.setMillennium_No(request.getParameter("millennium_no"));
    }
    String strUrlLocation = "";
	String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
			    
	if ( strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)  && (request.getParameter("chkUseTaxValue") != null))
    {
		if(  downloadbar ){
	     	task.setRunning(true);
	     	task.setrecordcount(recordcount);
	     	if(imaxdownloadbar!=-1 && recordcount>imaxdownloadbar){
	     		task.setmax(imaxdownloadbar);
     			task.setrecordcount(imaxdownloadbar);
     		}
	     	task.setvar(true,SessionData, intSectionPage, application, TransactionSearch);
		    isDownload = true;
			}
			else{
	   		 if(imaxdownloadbar!=-1)
	     		TransactionSearch.setmax(imaxdownloadbar);
		    if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
		    {
				String sTmp = SessionData.getProperty("ref_id");
				SessionData.setProperty("ref_id", request.getParameter("repId"));
				SessionData.setProperty("access_level", DebisysConstants.ISO);
				SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
	     	 	strUrlLocation = TransactionSearch.downloadPinHistoryMx(SessionData, intSectionPage, application, true);
				SessionData.setProperty("ref_id", sTmp);
				SessionData.setProperty("access_level", DebisysConstants.CARRIER);
				SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
		    }
		    else if ( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER))
		    {
		        TransactionSearch.setMerchantIds(request.getParameterValues("ISOIds"));
		        TransactionSearch.setRepId(TransactionSearch.getMerchantIds());
		        TransactionSearch.setMerchantIds("");
	     	 	strUrlLocation = TransactionSearch.downloadPinHistoryMx(SessionData, intSectionPage, application, true);
		    }
		    else
		    {
	     	 	strUrlLocation = TransactionSearch.downloadPinHistoryMx(SessionData, intSectionPage, application, true);
		    }
     	 	 response.sendRedirect(strUrlLocation);
     	 	}
    }
    else 
    {
      	if( downloadbar ){
     	task.setRunning(true);
     	task.setrecordcount(recordcount);
	    if(imaxdownloadbar!=-1  && recordcount>imaxdownloadbar){
	     	task.setmax(imaxdownloadbar);
     		task.setrecordcount(imaxdownloadbar);
     	}
     	task.setvar(false,SessionData, intSectionPage, application, TransactionSearch);
		isDownload = true;
		}
		else{
	   		 if(imaxdownloadbar!=-1)
	     			TransactionSearch.setmax(imaxdownloadbar);
	     			  
		    if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
		    {
				String sTmp = SessionData.getProperty("ref_id");
				SessionData.setProperty("ref_id", request.getParameter("repId"));
				SessionData.setProperty("repName", request.getParameter("repName"));
				SessionData.setProperty("access_level", DebisysConstants.ISO);
				SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
	     	 	strUrlLocation = TransactionSearch.downloadPinHistory(SessionData, intSectionPage, application, true);
				SessionData.setProperty("ref_id", sTmp);
				SessionData.setProperty("access_level", DebisysConstants.CARRIER);
				SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
		    }
		    else if ( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER))
		    {
		        TransactionSearch.setMerchantIds(request.getParameterValues("ISOIds"));
		        TransactionSearch.setRepIdMultiple(TransactionSearch.getMerchantIds());
		        TransactionSearch.setMerchantIds("");
	     	 	strUrlLocation = TransactionSearch.downloadPinHistory(SessionData, intSectionPage, application, true);
		    }
		    else
		    {
	     	 	strUrlLocation = TransactionSearch.downloadPinHistory(SessionData, intSectionPage, application, true);
		    }
     	 	 response.sendRedirect(strUrlLocation);
     	 	}
    }   
  }
  else
  {
     response.sendRedirect(request.getHeader("referer"));
     return;
  }
}
//////////////////////////////
if (request.getParameter("search") != null)
{
  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }


  if (TransactionSearch.validateDateRange(SessionData))
  {
  
    if (request.getParameter("invoiceID") != null)
    {
    	TransactionSearch.setInvoiceNo(request.getParameter("invoiceID").toString());
    }
    if (request.getParameter("merchId") != null)
    {
        strMerchIds = request.getParameter("merchId");
    }
    if(strMerchIds != "")
    {
        TransactionSearch.setMerchantIds(strMerchIds);  
    }
    else
    {
        String strMerchantIds[] = request.getParameterValues("mids");
	    if ( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER))
	    {
	        strMerchantIds = request.getParameterValues("ISOIds");
            TransactionSearch.setMerchantIds(strMerchantIds);
            TransactionSearch.setRepIdMultiple(TransactionSearch.getMerchantIds());
            strMerchantIds = null;
            TransactionSearch.setMerchantIds("");
	    }
    	if (strMerchantIds != null)
    	{
            TransactionSearch.setMerchantIds(strMerchantIds);
    	}  	
  	}
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
    {
		String sTmp = SessionData.getProperty("ref_id");
		SessionData.setProperty("ref_id", request.getParameter("repId"));
		SessionData.setProperty("access_level", DebisysConstants.ISO);
		SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
		vecSearchResults = TransactionSearch.searchPinTransactions(intPage, intPageSize, SessionData, section_page, application, false, true);
		SessionData.setProperty("ref_id", sTmp);
		SessionData.setProperty("access_level", DebisysConstants.CARRIER);
		SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
    }
    else
    {
		vecSearchResults = TransactionSearch.searchPinTransactions(intPage, intPageSize, SessionData, section_page, application, false, true);
    }
    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    	
    	// DBSY 908
   	// checking download bar settings if Enabled and if feature is needed.
		String download = DebisysConfigListener.getdownloadbar(application);
		if(download!=null && !download.equals("")){
		try{
			int downloadset = Integer.parseInt(download);
			if( downloadset <= intRecordCount ){
				 downloadbar = true;
			}
		}
		catch(NumberFormatException e){
		 	downloadbar = false;
		}
		}
		
		
		String maxdownload = DebisysConfigListener.getmaxdownload(application);
		if(maxdownload!=null && !maxdownload.equals("")){
		try{
			 maxdownloadset = Integer.parseInt(maxdownload);
			if( maxdownloadset < intRecordCount && maxdownloadset!=0){
				maxdownloadbar = true; 
			}
		}
		catch(NumberFormatException e){
		 	maxdownloadbar = false;
		}
		}
		
    vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize);
      if ((intPageCount * intPageSize) < intRecordCount)
      {
        intPageCount++;
      }
    }
  }
  else
  {
   searchErrors = TransactionSearch.getErrors();
  }


}
else
{
	if( request.getParameter("repId") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
	{
		repId = request.getParameter("repId");
		TransactionSearch.setRepId( repId );
	}
}
%>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
function check(){
	
 if(!IsNumeric(document.mainform.invoiceID.value)){
 	document.mainform.invoiceID.value  = "";
 	alert ("Invoice must be numberic");
	return false; 
 }
 if(!IsNumeric(document.mainform.transactionID.value)){
 	document.mainform.transactionID.value  = "";
 	alert ("Transaction ID must be numberic");
	return false; 
 }
   scroll();
   return true;
}         
function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
   }

var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2("false")', 150);
}
function openViewPIN(sTransId)
{
  var sURL = "/support/admin/transactions/view_pin.jsp?trans_id=" + sTransId;
  var sOptions = "left=" + (screen.width - (screen.width * 0.4))/2 + ",top=" + (screen.height - (screen.height * 0.3))/2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
  var w = window.open(sURL, "ViewPIN", sOptions, true);
  w.focus();
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="800" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" align="left" width="2000"><%=Languages.getString("jsp.admin.reports.transactions.transactions.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20" align="right"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="get" action="admin/reports/transactions/pinTransactions.jsp"  onsubmit="return check();" >
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
<%
	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	if((vTimeZoneData == null) || ((vTimeZoneData != null) && (vTimeZoneData.size() == 0)))
	{
		Long merchantRepId = DbUtil.getMerchantRep(SessionData.getProperty("ref_id"), application);
		if(merchantRepId != Long.MIN_VALUE)
		{
			vTimeZoneData = TimeZone.getTimeZoneByRep(merchantRepId);		
		}
	}
	
	if((vTimeZoneData != null) && (vTimeZoneData.size() > 0))
	{
%>
        <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
<%
	}
	else
	{
%>   
	   <tr class="main"><td nowrap colspan="2"><br/><br/></td></tr>  
<%
	}
%>     
        <tr class="main">
         <td nowrap valign="top">
<%
	if (!(StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER)))
	{
		out.print(Languages.getString("jsp.admin.iso_name",SessionData.getLanguage()) + ":&nbsp;");
		if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
		{
			out.print(request.getParameter("repName"));
		}
		else
		{
			out.print(SessionData.getUser().getCompanyName());
		}
	}
%>
		</td>
         <td>&nbsp;</td>
        </tr>  	          
        <tr class="main">
         <td nowrap valign="top">
         <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
         <%}else{ %>
         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
         <%}%>
         <td>&nbsp;</td>
        </tr>           
               
               <tr>
               <td valign="top" nowrap>
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<br/>" + strError);
}

  out.println("</font></td></tr>");
}
%>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td><td><input class="plain" id="startDate" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td> <input class="plain" id="endDate" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
		<tr class="main">
		    <td nowrap><%=Languages.getString("jsp.admin.reports.transaction_id",SessionData.getLanguage())%>: </td><td><input name="transactionID" id="transactionID" value="<%=TransactionSearch.getTransactionID()%>"></td> 
		</tr>
		
		<% if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{%>
		<tr class="main">
		    <td nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>: </td><td><input name="invoiceID" id="invoiceID" value="<%=TransactionSearch.getInvoiceNo()%>"></td> 
		</tr>
		<%} 
		else
		{ %>
		<tr style="display:none;" class="main">
		    <td style="display:none;" nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>: </td><td><input name="invoiceID" id="invoiceID" style="display:none;"  value="<%=TransactionSearch.getInvoiceNo()%>"></td> 
		</tr>
<%
		}
		if ( !strAccessLevel.equals(DebisysConstants.CARRIER) )
		{
%>
		<tr class="main">
		    <td nowrap><%=Languages.getString("jsp.admin.reports.pinreturn_search.pin",SessionData.getLanguage())%>: </td><td><input name="PINNumber" id="PINNumber" value="<%=TransactionSearch.getPINNumber()%>"></td> 
		</tr>
		<tr class="main">
		    <td nowrap><%=Languages.getString("jsp.includes.menu.pinTransactions_search.controlNo",SessionData.getLanguage())%>: </td><td><input name="controlNumber" id="controlNumber" value="<%=TransactionSearch.getControlNumber()%>"></td> 
		</tr>
		<tr class="main">
		    <td nowrap><%=Languages.getString("jsp.includes.menu.pinTransactions_search.serialNo",SessionData.getLanguage())%>: </td><td><input name="serialNumber" id="serialNumber" value="<%=TransactionSearch.getSerialNumber()%>"></td> 
		</tr>
<%
		}
			if ( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER))
			{
		%>
				<tr class="main">
					<td class=main valign=top nowrap><%=Languages.getString("jsp.productapproval.selectISO",SessionData.getLanguage())%></td>
					<td class=main valign=top>
						<select name="ISOIds" size="10" multiple>
							<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
	{
	Vector vISOs = (new CustomerSearch()).searchRep(1, Integer.MAX_VALUE, SessionData, DebisysConstants.REP_TYPE_ISO_5_LEVEL);
	if ( vISOs.size() > 1 )
	{
		vISOs.remove(0);
		Iterator it = vISOs.iterator();
		while (it.hasNext())
		{
			Vector vTemp = null;
			vTemp = (Vector) it.next();
			out.println("<option value=" + vTemp.get(1) +">" + vTemp.get(0) + "</option>");
		}
	}
	else
	{
		out.println("<option value=''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>");
	}
	}
%>
						</select>
				        <br>
					</td>
				</tr>
		<%
			}
			else if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
			{	
		%>
				<tr class="main">
				    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%></td>
				    <td class=main valign=top>
<%
				if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
				{
%>
						<input type="hidden" name="repName" value="<%=request.getParameter("repName")%>">
						<input type="hidden" name="repId" value="<%=request.getParameter("repId")%>">
<%
				}
%>
				        <select name="mids" id="mids" size="10" multiple>
							<%
                                if(strMerchIds == "")
                                {
                                    out.println("<option value=\"\" selected>" + Languages.getString("jsp.admin.reports.all",SessionData.getLanguage()) + "</option>");
                                }
                                else
                                {
                                    out.println("<option value=\"\">" + Languages.getString("jsp.admin.reports.all",SessionData.getLanguage()) + "</option>");
                                }
								Vector vecMerchantList;
								String sTmp = SessionData.getProperty("ref_id");
								
								System.out.println("strAccessLevel strAccessLevel"+strAccessLevel);
								if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
								{
									TransactionReport.setCarrierId(sTmp);
									//String sTmp = SessionData.getProperty("ref_id");
									SessionData.setProperty("ref_id", request.getParameter("repId"));
									vecMerchantList = Merchant.getMerchantListReports(SessionData);
									SessionData.setProperty("ref_id", sTmp);
									TransactionReport.setCarrierId(null);
								}
								else
								{
									vecMerchantList = Merchant.getMerchantListReports(SessionData);
								}
							  	Iterator it = vecMerchantList.iterator();
							  	while (it.hasNext())
							  	{
							    	Vector vecTemp = null;
							    	vecTemp = (Vector) it.next();
                                    if(strMerchIds == vecTemp.get(0))
                                    {
							    	    out.println("<option selected value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                                    }
                                    else
                                    {
                                        out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                                    }
							  	}
							%>
				        </select>
				        <br>
				        *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%>
					</td>
				</tr>
		<%
			}
		%>
<tr>    
    <td class=main>
    	<br>
      	<input type="hidden" name="search" value="y">
<%
	if (!(StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER)))
	{
%>
      	<input type="hidden" name="repId" value="<%=TransactionSearch.getRepId()%>"> 
<%
	}
%>
      	<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.transactions.show_transactions",SessionData.getLanguage())%>"  >
    </td>
</tr>
</table>
<table width=400>
<tr>
	<td class="main" nowrap>
		* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
	</td>
</tr>
</table>
               </td>
              </tr>
              </form>
            </table>
            <% if(isDownload){%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>
<div><iframe width=350 height=200 name="iframe" id="iframe" src="admin/transactions/status.jsp" frameborder="0" style="visibility:visible;">
		</iframe></div></td></tr></table><%
new Thread(task).start();
}
          
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>

            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
              if (!TransactionSearch.getStartDate().equals("") && !TransactionSearch.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(TransactionSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getEndDateFormatted()));
                }
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%></td></tr>
            <tr>
              <td align=left class="main" nowrap>
<% if(!isDownload){%>                   
  			  <form name="downloadform" method=post action="admin/reports/transactions/pinTransactions.jsp" onSubmit="scroll2();" >
<%
				if( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER) )
				{
%>
				<input type="hidden" name="ISOIds" value="<%=TransactionSearch.getRepId()%>">
<%
				}
				if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
				{
%>
				<input type="hidden" name="repName" value="<%=request.getParameter("repName")%>">
				<input type="hidden" name="repId" value="<%=request.getParameter("repId")%>">
<%
				}
%>
              <input type="hidden" name="startDate" value="<%=TransactionSearch.getStartDate()%>">
              <input type="hidden" name="endDate" value="<%=TransactionSearch.getEndDate()%>">
              <input type="hidden" name="page" value="<%=intPage%>">
              <input type="hidden" name="section_page" value="<%=section_page%>">
              <input type="hidden" name="download" value="y">
              <input type="hidden" id="merchantIds" name="merchantIds" value="<%=TransactionSearch.getMerchantIds()%>">              
              <input type="hidden" name="transactionID" value="<%=TransactionSearch.getTransactionID()%>">       
              <input type="hidden" name="invoiceID" value="<%=TransactionSearch.getInvoiceNo()%>">
              <input type="hidden" name="PINNumber" value="<%=TransactionSearch.getPINNumber()%>">
              <input type="hidden" name="controlNumber" value="<%=TransactionSearch.getControlNumber()%>">
              <input type="hidden" name="serialNumber" value="<%=TransactionSearch.getSerialNumber()%>">
              <input type="hidden" id="markers" name="markers" value="<%=markers%>">
              <input type="hidden" id="recordcount" name="recordcount" value="<%=intRecordCount%>">
              <input type="hidden" id="downloadbar" name="downloadbar" value="<%=downloadbar%>">
              <% if(maxdownloadbar){ %><input type="hidden" id="maxdownloadbar" name="maxdownloadbar" value="<%=maxdownloadset%>"><% } %>
              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
              </form>
              <br/>
             
             <% if(maxdownloadbar){ %>
              <FONT COLOR="red"> <%=Languages.getString("jsp.admin.warning_maxdownload",SessionData.getLanguage())%><%=maxdownloadset%><%=Languages.getString("jsp.admin.warning_secmaxdownload",SessionData.getLanguage())%>
              </font><% }
              } %>
              </td>
            </tr>
            <tr>
              <td align=right class="main" nowrap>
<%
				String sCarrierParams = "";
				if( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER) )
				{
					sCarrierParams = "&ISOIds=" + TransactionSearch.getRepId();
				}
				if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
				{
					sCarrierParams = "&repId=" + request.getParameter("repId") + "&repName=" + request.getParameter("repName");
				}

              if (intPage > 1)
              {
                out.println("<a href=\"admin/reports/transactions/pinTransactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&merchId=" + URLEncoder.encode(TransactionSearch.getMerchantIds(), "UTF-8") +"&page=1" + sCarrierParams + "\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"admin/reports/transactions/pinTransactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&merchId=" + URLEncoder.encode(TransactionSearch.getMerchantIds(), "UTF-8") +"&page=" + (intPage-1) + sCarrierParams + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }
              
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"admin/reports/transactions/pinTransactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&merchId=" + URLEncoder.encode(TransactionSearch.getMerchantIds(), "UTF-8") + "&page=" + i + sCarrierParams + "\">" + i + "</a>&nbsp;");
                }               

              }

              if (intPage <= (intPageCount-1))
              {
                out.println("<a href=\"admin/reports/transactions/pinTransactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&merchId=" + URLEncoder.encode(TransactionSearch.getMerchantIds(), "UTF-8") + "&page=" + (intPage+1) + sCarrierParams + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"admin/reports/transactions/pinTransactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&merchId=" + URLEncoder.encode(TransactionSearch.getMerchantIds(), "UTF-8") + "&page=" + (intPageCount) + sCarrierParams + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
              }
              %>
              </td>
            </tr>
            </table>
            <table>
                <tr>
                    <td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
                </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
              <td class=rowhead2>#</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
               <td class=rowhead2><%=Languages.getString("jsp.admin.reports.city",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.county",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.reference",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%></td>
			  
<%

/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                	if(strAccessLevel.equals(DebisysConstants.ISO)){
               	 		if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
%>               	  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;</td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()) %></td>
<%               	 	}
                	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>					  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;</td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                	 }
                   	}
                   	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
%>                   	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
                   	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></td>
		              <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                 	}
                   	else if (strAccessLevel.equals(DebisysConstants.REP)){
%>					  <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></td>	
		              <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
                   	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
%>                    <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></td>
<%                  }
%>				


<%
				}
/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/				
%>


<%
if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
%>
			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%></td>
			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%></td>
			  
	        <% if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
	    	  {%>
			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></td>
			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></td>
	  		  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxpercentage",SessionData.getLanguage()).toUpperCase()%></td>
	  		  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxtype",SessionData.getLanguage()).toUpperCase()%></td>
			<%}
}
%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.description",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()).toUpperCase()%></td>
<%
				if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT))
 				{ 		//Authorization Carrier Number
%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_carrier_number",SessionData.getLanguage()).toUpperCase()%></td>					  
<%
				}
				if (SessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA)) { 
%>
					<td class=rowhead2><%=Languages.getString("jsp.admin.reports.additionalData",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}

     //Added by jacuna.R25. DBSY-525
if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) 
	&& strAccessLevel.equals(DebisysConstants.ISO)
	&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) 
{%>
			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.pin_number",SessionData.getLanguage()).toUpperCase()%></td>		
<%}
if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) 
	&& strAccessLevel.equals(DebisysConstants.ISO)
	&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) 
{%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ach_date",SessionData.getLanguage()).toUpperCase()%></td>			
<%}//End aded by jacuna.R25. DBSY-525%> 		            
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_type",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.TerminalDescription",SessionData.getLanguage()).toUpperCase()%></td>
            	 <% if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{%>
					<td class=rowhead2><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage()).toUpperCase()%></td>
                <%} %>
                
                <%if ( viewReferenceCard ){%>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.DTU1204",SessionData.getLanguage()).toUpperCase()%></td>
                <%}%>
                
                
                
            </tr>
            <%
                  int intCounter = 1;
                  //intCounter = intCounter - ((intPage-1)*intPageSize);
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;                  
                 
                  String type = "";
                  String row = "";
                  // set all the field positions as displayed by transactions.jsp
  
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    markedRow = null;
                    markedRow = new MarkedCollection<String>(vecTemp);
                    // check if type is "Return" of "Adjust", if so, display the whole row in red
                    type = (String)vecTemp.get(14);
                    if (type.equals("Return") || type.equals("Adjustment") || type.equals("Credit")||type.equals("Write Off"))
                    {
                    	row = "rowred";
                    }
                    else
                    {
                    	row = "row";
                    }
                    String sViewPIN = "";
                    String sPinNumber = "N/A";
                    if ( SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) )
                    {
                        // LOCALIZATION - Section modified to accept just a result string with the currency amount
                        // From the DB.
                      //if the trans_type=2 which is a pin sale
                      if ((vecTemp.get(13).toString().equals("2")) && Double.parseDouble(vecTemp.get(9).toString().replaceAll(",", "")) > 0)
                      {
                        sViewPIN = "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + vecTemp.get(0).toString() + "');\"><span class=\"showLink\"><span class=\"showLink\">" + vecTemp.get(12).toString() + "</span></span></a>";
                      	sPinNumber = vecTemp.get(19).toString();
                      }
                      else
                      {
                        sViewPIN = vecTemp.get(12).toString();
                      }

                    }
                    else
                    {
                      sViewPIN = vecTemp.get(12).toString();
                    }
					String[] productDescription = vecTemp.get(11).toString().split("\\/");
					
                    out.println("<tr class=" + row + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td>" + markedRow.get(0)+ "</td>" +
                                "<td>" + markedRow.get(1) + "</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(markedRow.get(2))  + "</td>" +
                                "<td>" + markedRow.get(3) + "</td>" +
                                "<td nowrap>" + markedRow.get(4) + "</td>" +
                                 //city
                                "<td nowrap>" +  vecTemp.get(5).toString() + "</td>" +
                                 //county
                                "<td nowrap>" +  vecTemp.get(28).toString() + "</td>" +
                                //clerk
                                "<td nowrap>" + markedRow.get(6) + "</td>" +
                                "<td>" + markedRow.get(7) + "</td>" +
                                "<td>" + markedRow.get(8) + "</td>" +
			                    //DBSY-905 Tax Calculation
			                    //amount
			                                "<td nowrap>" + NumberUtil.formatCurrency(markedRow.get(9)) + "</td>");
			                           
			                                              //billing_typeId
				                 /* if( strAccessLevel.equals(DebisysConstants.CARRIER) )
				                   	out.println("<td nowrap>" +  vecTemp.get(34).toString() + "</td>");*/
                   	     
			                    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
			                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
			                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //iso
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
			                     		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
			                     	 }
			                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			                     	 	out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
			                     	 }
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(29).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
		                         	}
		                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
		                         		out.println("<td nowrap>" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //merchant
		                         	}
		                        }  
		                    	/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			                                    
			                    if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
			                    //bonus
			                                out.println("<td nowrap>" + markedRow.get(17) + "</td>" +
			                    //total Recharge
			                                "<td nowrap>" + markedRow.get(18) + "</td>");
			                         
			                        if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
				    	 					{
				    	 		//net amount 
			                                out.println("<td nowrap>" + markedRow.get(24) + "</td>" +
			                    //tax amount
			                                "<td nowrap>" + markedRow.get(25) + "</td>" + 
			                                "<td nowrap>" + markedRow.get(26) + "%</td>" + 
			                                "<td nowrap>" + markedRow.get(27) + "</td>");
			                                }
			                    }
                                //bal
                                out.println("<td nowrap>" + NumberUtil.formatCurrency(markedRow.get(10)) + "</td>" +
                                //product
                                "<td nowrap width=200>" + productDescription[0] + "</td>" +
                                //product description                                
                                "<td nowrap width=200>" + productDescription[1] + "</td>" +
                                
                                "<td>" +  markedRow.mark(sViewPIN, 12) + "</td>");
                                
                                if ( SessionData.checkPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT) )
				 				{
				 					//Authorization Number
				 				 	out.println("<td nowrap>" + markedRow.get(15) + "</td>");
				 				}
				 				
                                String value="";
                                if (SessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA)) 
                                {
                                  	value = markedRow.get(22);
                                		 value = value == null || value.equals("") ? Languages.getString("jsp.admin.reports.additionalData.default",SessionData.getLanguage()) : value;
                                }
					if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) 
						&& strAccessLevel.equals(DebisysConstants.ISO)
						&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
								out.println("<td>" + markedRow.mark(sPinNumber, 19) + "</td>"); 
                    }                    
					if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) 
						&& strAccessLevel.equals(DebisysConstants.ISO)
						&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
								out.println("<td>" + markedRow.get(20) + "</td>");
                    }                    
                                out.println("<td>" + markedRow.mark(type, 14) + "</td>" );
                                out.println("<td>" + vecTemp.get(35) + "</td>");
                     
                    if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{
                    //DBSY-919 Invoice Number Field
                        out.println("<td>" + markedRow.get(23) + "</td>" );
                    //////  
                    }
                    
                    if ( viewReferenceCard ){
				   		out.println("<td>" + vecTemp.get(34) + "</td>");
				   	}
				   		
                    out.println("</tr>");
                    
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                   	
                  vecSearchResults.clear();
                  markers = markedRow.getMarkers();
            %>
            </table>

<%
}
else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>