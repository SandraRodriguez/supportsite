<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.languages.*,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 4;
  int section_page = 18;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  
  int intErrorsSum = 0;
  String products = "";
  
    boolean isInternal = Boolean.FALSE;
  
    if(request.getParameter("isInternal") != null) {            
         if(request.getParameter("isInternal") == "true" || request.getParameter("isInternal").equalsIgnoreCase("true")) {
             isInternal = Boolean.TRUE;
         }
    }        
  
    if (TransactionReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));
      products = request.getParameter("products");
      if (products != null && !products.trim().equals(""))
      {
        	TransactionReport.setProductIds(products);
      }
      vecSearchResults = TransactionReport.getTransactionErrorSummaryByTypes(SessionData, isInternal);
      String sURL = com.debisys.transactions.TransactionSearch.downloadErrorSummaryReport(application, vecSearchResults,SessionData);
      response.sendRedirect(sURL);
      return;
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
%>
