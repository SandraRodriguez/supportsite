<%@page import="com.debisys.customers.*"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="com.debisys.utils.*"%>
<%@page import="java.util.Hashtable"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
    int section = 4;
    int section_page = 21;
%>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%  
	String disabledText = "";
	Hashtable<String, String> sharedBalOptions = new Hashtable<String, String>();
	sharedBalOptions.put("false", Languages.getString("jsp.admin.customers.reps.sharedbalance_status_no",SessionData.getLanguage()));
	sharedBalOptions.put("true", Languages.getString("jsp.admin.customers.reps.sharedbalance_status_yes",SessionData.getLanguage()));
%>
<style>
input[type="radio"]{
	float:left;
}
</style>
<script src="/includes/jquery.js"></script>
<script type="text/javascript">
	var count = 0
	var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ", "< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");
	var url = "admin/reports/transactions/merchant_balance_shared_credit.jsp"
	
	function scroll() {
	    document.mainform.submit.disabled = true;
	    document.mainform.submit.value = iProcessMsg[count];
	    count++
	    if (count = iProcessMsg.length)
	        count = 0
	    setTimeout('scroll()', 150);
	}
	
	function scroll2() {
        document.downloadform.submit.disabled = true;
        document.downloadform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll2()', 150);
    }
	
	function changeMerchantType(){
		value = $('input[name="merchantType"]:checked').val();
		if(value == 1){
			$("#merchantIdTxt").attr('disabled', false);
			$("#merchantIdLst").attr('disabled', true);
			$("#merchantSearch").attr('disabled', 'disabled');
			value = "<%=Languages.getString("jsp.admin.reports.all", SessionData.getLanguage()) %>"
			data = "<select size=\"10\" class=\"plain\" multiple=\"multiple\" name=\"merchantIdTxt\" id=\"merchantIdLst\" disabled=\"disabled\"><option value=\"ALL\" selected>" + value + "</option></select>"
			$("#merchantIdLst").html(data)
		} else if(value == 2) {
			$("#merchantIdTxt").attr('disabled', true);
			$("#merchantIdLst").attr('disabled', false);
			$("#merchantSearch").attr('disabled', false);
			populateMerchants()
		}
	}
	
	function changeBalanceType(){
		value = $("#sharedBalanceList").val();
		if(value == "true"){
			merchantLabel = "<%=Languages.getString("jsp.admin.customers.reps.rep_id", SessionData.getLanguage()) %>" + ":"
			selMerchantLabel = "<%=Languages.getString("jsp.admin.reports.Select_Reps", SessionData.getLanguage()) %>" + ":"
		} else if(value == "false") {
			merchantLabel = "<%=Languages.getString("jsp.admin.reports.merchant_id", SessionData.getLanguage()) %>" + ":"
			selMerchantLabel = "<%=Languages.getString("jsp.admin.reports.payments.index.select_merchants", SessionData.getLanguage()) %>" + ":"
		}
		$("#merchantLabel").text(merchantLabel)
		$("#selectMerchantLabel").text(selMerchantLabel)
		populateMerchants()
	}
	
	function populateMerchants(){
		value = $('input[name="merchantType"]:checked').val();
		if(value == 2) {
			sharedBal = $("#sharedBalanceList").val();
			$.post(url,
				{sharedBalance : sharedBal},
				function (data){
					$("#merchantIdLst").html(data)
				}
			);
		}
	}
	
	function filterMerchantList(){
		$(document).ready(function(){
			filter = $("#merchantSearch").val()
			$("#merchantIdLst option").each(function(){
				if ($(this).text().includes(filter)) {
				    $(this).show();
				   } else {
				    $(this).hide();
				   }
			})			
		})
	}
</script>

<table border="0" cellpadding="0" cellspacing="0" width="700">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.titleABR", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
    	<td colspan="3"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
            	<tr>
            		<td>
            			<form name="mainform" method="post" action="admin/reports/transactions/merchant_balance_availability_detail.jsp" onSubmit="scroll();">
            				<table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="formArea2">
                                    	<table width="95%">
                                            <tr class="main">
                                                <td nowrap valign="top">
                                                    <%if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {%>
                                                    <%=Languages.getString("jsp.admin.select_date_range", SessionData.getLanguage())%>:</td>
                                                    <%} else {%>
                                                    <%=Languages.getString("jsp.admin.select_date_range_international", SessionData.getLanguage())%>:
                                                	<%}%>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" nowrap>
                                                    <table>
                                                        <tr class="main">
                                                            <td width="50%"><%=Languages.getString("jsp.admin.start_date", SessionData.getLanguage())%>:</td>
                                                            <td width="50%" align="left"><input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if (self.gfPop)
                                                                        gfPop.fStartPop(document.mainform.startDate, document.mainform.endDate);
                                                                    return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                                        </tr>
                                                        <tr class="main">
                                                            <td width="50%"><%=Languages.getString("jsp.admin.end_date", SessionData.getLanguage())%>: </td>
                                                            <td width="50%" align="left"> <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if (self.gfPop)
                                                                        gfPop.fEndPop(document.mainform.startDate, document.mainform.endDate);
                                                                    return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                                        </tr>
                                                        <tr>
    														<td width="50%" class="main"><%=Languages.getString("jsp.admin.customers.reps.sharedbalance",SessionData.getLanguage())%></td>
															<td>
																<select style="display:block;" <%=disabledText%> id="sharedBalanceList" name="sharedBalance" onchange="changeBalanceType()">
																<%
																	for (String option: sharedBalOptions.keySet()) {
																%>
																		<option value=<%=option%> <%=sharedBalOptions.get(option)%>><%=sharedBalOptions.get(option)%></option>
																<%
																	}
																%>
																</select>
															</td>
                                                        </tr>
                                                        <tr>
                                                        	<td class="main" colspan=2 align=left>
                                                        		<table width="100%">
                                                        			<tr>
                                                        				<td valign="top" width="50%">
                                                        					<input type="radio" name="merchantType" id="merchantType" value="1" checked onclick="changeMerchantType()" class="leftAlign"/>
                                                        					<label id="merchantLabel"><%=Languages.getString("jsp.admin.reports.merchant_id", SessionData.getLanguage()) %>:</label>
                                                        					<input type="text" name="merchantIdTxt" id="merchantIdTxt"/>
                                                        				</td>
                                                        				<td valign="top" width="50%">
                                                        					<table>
                                                        						<tr>
                                                        							<td valign="top">
                                                        								<input type="radio" name="merchantType" id="merchantType" value="2" onclick="changeMerchantType()" class="leftAlign"/>
			                                                        					<label id="selectMerchantLabel" for="one">
			                                                        						<%=Languages.getString("jsp.admin.reports.payments.index.select_merchants", SessionData.getLanguage()) %>:
			                                                        					</label>
		                                                        					</td>
                                                        						</tr>
                                                        						<tr>
                                                        							<td>
                                                        								<table class="main" cellspacing="5">
                                                        									<tr><td>
	                                                        								<%=Languages.getString("jsp.tools.datatable.search",SessionData.getLanguage()) %>
	                                                        								<input type="text" name="merchantSearch" id="merchantSearch" onkeyup="filterMerchantList()" disabled/>
	                                                        								</td></tr>
	                                                        								<tr><td>
					                                                        					<select size="10" class="plain" multiple="multiple" name="merchantIdTxt" id="merchantIdLst" disabled="disabled" style="width:220px">
																									<option value="ALL" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
																								</select>
																							</td></tr>
																							<%-- <tr>
																								<td style="word-break:break-all;">
																								*<%=Languages.getString("jsp.admin.reports.payments.index.instructions", SessionData.getLanguage())%>
																								</td>
																							</tr> --%>
																						</table>
																					</td>
																				</tr>
																			</table>
<%-- 																		<br> --%>
                                                        				</td>
                                                        			</tr>
                                                        		</table>
                                                        	</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=main colspan=2 align=left>
                                                                <input type="hidden" name="search" value="y">
                                                                <input type="submit" name="submit" onclick="validateSchedule(0);" value="<%=Languages.getString("jsp.admin.reports.show_report", SessionData.getLanguage())%>">
                                                            </td>
                                                            <td>
                                                                <jsp:include page="/admin/reports/schreportoption.jsp">
                                                                    <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
                                                                    <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
                                                                </jsp:include>
                                                                <input type="hidden" name="search" value="n">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="main" colspan=2>
                                                                * <%=Languages.getString("jsp.admin.reports.general", SessionData.getLanguage())%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
            			</form>
            		</td>
            	</tr>
            </table>
        </td>
    </tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>
