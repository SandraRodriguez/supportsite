<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.utils.NumberUtil" %>
<%
    int section=4;
    int section_page=25;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
  <%@ include file="/includes/header.jsp" %>

  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%= Languages.getString("jsp.admin.reports.write_off_request_report",SessionData.getLanguage()).toUpperCase() %></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <table width="100%" border="0" cellspacing="0" cellpadding="8">
            <%
		    Vector vTemp = TransactionReport.getWriteOffDetail(Integer.parseInt(request.getParameter("trxId")));
		    if ( vTemp.size() > 0 )
		    {
			%>
                <tr class="main">
                    <td>
                        <br><b><%=Languages.getString("jsp.admin.reports.write_off_request_report.trxNumber",SessionData.getLanguage())%></b>
                        <%
                           out.println(": " + vTemp.get(0));
                        %> 
                    </td>
                </tr>
                <tr class="main">
                        <td nowrap><b><%=Languages.getString("jsp.admin.reports.write_off_request_report.originalTrxNumber",SessionData.getLanguage())%></b>
                            <%
                                out.println(": " + vTemp.get(1));
                            %>
                        </td>                    
                </tr>
                <tr class="main">
                        <td nowrap><b><%=Languages.getString("jsp.admin.reports.write_off_request_report.originalTrxType",SessionData.getLanguage())%></b>
                            <%
                                out.println(": " + vTemp.get(2) + " - " + (vTemp.get(3).equals(DebisysConstants.TRX_TYPE_SALE)?Languages.getString("jsp.admin.transactions.write_off_request.sale",SessionData.getLanguage()):Languages.getString("jsp.admin.transactions.write_off_request.recharge",SessionData.getLanguage())));
                            %>
                        </td>                    
                </tr>   
                <tr class="main">
                        <td nowrap><b><%=Languages.getString("jsp.admin.reports.write_off_request_report.username",SessionData.getLanguage())%></b>
                            <%
                                out.println(": " + vTemp.get(4));
                            %>
                        </td>                    
                </tr>  
                <tr class="main">
                        <td nowrap><b><%=Languages.getString("jsp.admin.reports.write_off_request_report.reason",SessionData.getLanguage())%>:</b>
                                <tr class="main"><td><%=vTemp.get(5)%></td></tr>
                                <tr><td><input type=reset id="btnReturn" value="<%=Languages.getString("jsp.admin.reports.write_off_request_report.return",SessionData.getLanguage())%>" onclick="history.go(-1);"></td></tr>
<%
    }else{
				out.println("<tr><td><font color=ff0000>"
            					+ Languages.getString("jsp.admin.reports.write_off_request_report.writeOffNotFound",SessionData.getLanguage()) + "</font></td></tr>");
%>
            					<tr><td><input type=reset id="btnReturn" value="<%=Languages.getString("jsp.admin.reports.write_off_request_report.return",SessionData.getLanguage())%>" onclick="history.go(-1);"></td></tr>  
<%
    }
%>
                            </table>

                        </td>                    
                </tr>              
            </table>        
        </td>
    </tr>
</table>
  <%@ include file="/includes/footer.jsp" %>
