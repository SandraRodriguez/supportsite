<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="com.debisys.utils.ColumnReport"%>
<%@page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil"%> 
<%@page import="com.debisys.utils.TimeZone"%> 
<% 
int section=4; 
int section_page=46; 
%> 
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"></jsp:useBean> 
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"></jsp:useBean> 
<jsp:setProperty name="TransactionReport" property="*" /> 
<%@include file="/includes/security.jsp"%> 
<% 
    Vector vecSearchResults = new Vector(); 
    Hashtable searchErrors = null; 
    Vector warningSearchResults = new Vector();
    String strselectagentslist[]=null,strsubagentslist[]=null,strrepslist[]=null,strmerchantslist[]=null; 
    String header="";  
    boolean bUseTaxValue = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns 
    int intRecordCount = 0; 
    int intPage = 1; 
    int intPageSize = 50; 
    int intPageCount = 1; 
 
    int agentname = 2; 
    int subagentname = 3; 
    int repname = 4; 
    int merchantname = 5; 
    int merchantid = 6; 
    int city = 7; 
    int country = 8; 
    int rechargevalue = 9; 
    int bonus = 10; 
    int totalrecharge = 11; 
    int productid = 13; 
    int state = 14; 
    int lasttrx = 15; 
    int isorate  = 16; 
    int agentrate = 17; 
    int subagentrate = 18; 
    int reprate = 19; 
    int merchantrate = 20; 
    int productname = 21; 
    int trxcount = 22; 
    int netamount = 23; 
    int taxamount = 24; 

    int ASC = 1; 
    int DESC = 2; 
 
    ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
    ArrayList<String> titles = new ArrayList<String>();

    String keyLanguage = "jsp.admin.reports.title46";
    String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
    
    Vector vTimeZoneData = null; 
    if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) ) 
    { 
        vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id"))); 
    } 
    else 
    { 
        vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id"))); 
    } 
    String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+" "+vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
    
    String strRefId = SessionData.getProperty("ref_id"); 
    
    //////////////////////////////////////////////////////////////////
    //HERE WE DEFINE THE REPORT'S HEADERS 
    headers = com.debisys.reports.ReportsUtil.getHeadersSummaryTrxByProductMerchant( SessionData, application, bUseTaxValue);
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    String headerUserInfo = "";
    String filteredBy = "";
    
    if ( request.getParameter("Download") != null ) 
    { 
        //download  
        String sURL = ""; 

        String strProductIds[] = request.getParameterValues("pids"); 
        if (strProductIds != null) 
        { 
          TransactionReport.setProductIds(strProductIds); 
        }
        if(request.getParameter("attachedheader")!=null) 
                TransactionReport.setattachedheader(request.getParameter("attachedheader")); 

        if(request.getParameter("selectagentslist")!=null) 
                strselectagentslist = TransactionReport.convertListToArray(request.getParameter("selectagentslist")); 

        if(request.getParameter("subagentslist")!=null) 
                strsubagentslist = TransactionReport.convertListToArray(request.getParameter("subagentslist")); 

        if(request.getParameter("repslist")!=null) 
                strrepslist = TransactionReport.convertListToArray(request.getParameter("repslist")); 

        if(request.getParameter("merchantslist")!=null) 
                strmerchantslist = TransactionReport.convertListToArray(request.getParameter("merchantslist"));     

        TransactionReport.setSelectedList(strAccessLevel,strDistChainType,strRefId,strselectagentslist,strsubagentslist,strrepslist,strmerchantslist); 	 
        if(TransactionReport.getSelectionType()!=0)
        { 
            vecSearchResults = TransactionReport.getMerchantProductSummary(intPage, intPageSize, SessionData, application,false,true, 
                                DebisysConstants.DOWNLOAD_REPORT,null,null);  
            sURL = vecSearchResults.get(1).toString(); 
            response.sendRedirect(sURL); 
            return; 
        }  
    } 
 
    if (request.getParameter("page") != null) 
    { 
      try 
      { 
        intPage=Integer.parseInt(request.getParameter("page")); 
      } 
      catch(NumberFormatException ex) 
      { 
        intPage = 1; 
      } 
    } 
 
    if (intPage < 1) 
    { 
      intPage=1; 
    } 
 
    if (request.getParameter("search") != null) 
    { 
        if(request.getParameter("col")!=null) 
                TransactionReport.setCol(request.getParameter("col").toString()); 
        if(request.getParameter("sort")!=null) 
                TransactionReport.setSort(request.getParameter("sort").toString()); 
       //search 
       if (TransactionReport.validateDateRange(SessionData)) 
       { 
            SessionData.setProperty("start_date", request.getParameter("startDate")); 
            SessionData.setProperty("end_date", request.getParameter("endDate")); 
            String strProductIds[] = request.getParameterValues("pids"); 
            if (strProductIds != null) 
            { 
                TransactionReport.setProductIds(strProductIds); 
            } 
            TransactionReport.setStartDate(request.getParameter("startDate")); 
            TransactionReport.setEndDate(request.getParameter("endDate")); 

            if(request.getParameter("selectagentslist")!=null) 
                strselectagentslist = request.getParameterValues("selectagentslist"); 

            if(request.getParameter("subagentslist")!=null) 
                strsubagentslist = request.getParameterValues("subagentslist"); 

            if(request.getParameter("repslist")!=null) 
                strrepslist = request.getParameterValues("repslist"); 

            if(request.getParameter("merchantslist")!=null) 
                strmerchantslist = request.getParameterValues("merchantslist");     
            
            TransactionReport.setSelectedList(strAccessLevel,strDistChainType,strRefId,strselectagentslist,strsubagentslist,strrepslist,strmerchantslist); 	 
            if(TransactionReport.getSelectionType()!=0)
            { 
                boolean isScheduleReportRequest = request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y");
                if ( !isScheduleReportRequest )
                {
                    vecSearchResults = TransactionReport.getMerchantProductSummary(intPage, intPageSize, SessionData, application,false,false, DebisysConstants.EXECUTE_REPORT,null,null);  
                    warningSearchResults = TransactionReport.getWarningMessage();
                    if(vecSearchResults!=null)
                    { 
                        intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString()); 
                        vecSearchResults.removeElementAt(0); 
                    } 
                    headerUserInfo = TransactionReport.getHeaderUserInfo( SessionData, application );
                    filteredBy = TransactionReport.getfilteredby(SessionData);
                }
                else
                { 
                    titles.add(noteTimeZone);
                    titles.add( Languages.getString(keyLanguage,SessionData.getLanguage()) );
                    TransactionReport.getMerchantProductSummary(intPage, intPageSize, SessionData, application,false,false, DebisysConstants.SCHEDULE_REPORT, headers, titles);
                    
                    ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
                    if (  scheduleReport != null  )
                    {
                        scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
                        scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
                        scheduleReport.setTitleName( keyLanguage );   
                    }	
                    response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );  
                }    
            } 
            if (intRecordCount>0) 
            { 
                intPageCount = (intRecordCount / intPageSize); 
                if ((intPageCount * intPageSize) < intRecordCount) 
                { 
                    intPageCount++; 
                } 
            } 
      } 
      else 
      { 
        searchErrors = TransactionReport.getErrors(); 
      } 

    } 
%> 
<font face="Arial Rounded MT Bold"><%@include file="/includes/header.jsp"%></font> 
<font face="Arial Rounded MT Bold"><%    
  if (vecSearchResults != null && vecSearchResults.size() > 0)  
  {  
%></font> 
    <link href="includes/sortROC.css" type="text/css" rel="StyleSheet"> 
    <font face="Arial Rounded MT Bold"><script src="includes/sortROC.js" type="text/javascript"></script></font> 
<font face="Arial Rounded MT Bold"><%  
  }  
%></font> 

           <TABLE cellSpacing=0 cellPadding=0 width=750 
            background=images/top_blue.gif border=0>
              <TBODY>
              <TR>
                <TD width=23 height=20><IMG height=20 src="images/top_left_blue.gif" width=18></TD>
                <TD width="3000" class=formAreaTitle><B><%=titleReport.toUpperCase()%></B></TD>
                <TD width=28 height=20><IMG src="images/top_right_blue.gif"></TD></TR>
              <TR>
                <TD colSpan=3>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" 
                  bgColor=#7B9EBD border=0>
                    <TBODY>
                    <TR>
                      <TD width=1 bgColor=#7B9EBD><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top align=middle bgColor=#ffffff>
                        <TABLE width="100%" border=0 
                        align=center cellPadding=2 cellSpacing=0 class="fondoceldas">
                          <TBODY>
                          <TR>
                            <TD class=main>
                            
	<font face="Arial Rounded MT Bold"><%  
        if (searchErrors != null)  
        {  
            out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");  
            Enumeration enum1=searchErrors.keys();  
            while(enum1.hasMoreElements())  
            {  
              String strKey = enum1.nextElement().toString();  
              String strError = (String) searchErrors.get(strKey);  
              out.println("<li>" + strError);  
            }
            out.println("</font></td></tr></table>");  
        }
	boolean addColumnsDTU1281jsp = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_ADDITIONAL_COLUMNS_TRX_SUMM_PRODUCT_MERCHANT_REPORT);
	if (vecSearchResults != null && vecSearchResults.size() > 0)  
	{  
%></font> 
 
    <tr class="main"><td nowrap ><%=noteTimeZone%><br/><br/></td></tr>                  

<%  boolean showwarningSearchResults = false; 
    if ( warningSearchResults.size()>0 && showwarningSearchResults )
    { 
%>
        <tr>
        <td class="main" style="color:red">
                                <% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1)
                                        ||  (!TransactionReport.checkfortaxtype(SessionData))){
        %>
                                        <%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
                                        <%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
        <% } %>
                <% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
                        <%= warningSearchResults.get(i).toString()%> <br/>
                <% } %>
        </td>
        </tr>
<%
    }
%>
		    
           <td class="main"><%=intRecordCount%> result(s) found<% 
            if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) 
            { 
               out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted())); 
            } 
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%> 
            <br><%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%> 
            <br><% 
              header= Languages.getString("jsp.admin.report.transactions.products.merchants.report_generated",SessionData.getLanguage()) + "  "+ TransactionReport.currentdate(SessionData); 
              header+= "\n" + headerUserInfo + "\n" +filteredBy;  
              %>
             
            <table>
              <tr><td><font face="Arial Rounded MT Bold">&nbsp;</font></td></tr>
              <tr><td class="main"><font face="Arial Rounded MT Bold"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.report_generated",SessionData.getLanguage()) + " " + TransactionReport.currentdate(SessionData)%> 
              <br><%=headerUserInfo%></font>
              <font face="Arial Rounded MT Bold"> 
              <br><%=filteredBy%></font>
              </td></tr>
              <tr><td><font face="Arial Rounded MT Bold">&nbsp;</font></td></tr>
              <tr>
                <td>
                  <FORM ACTION="admin/reports/transactions/products_merchants_summary.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	
                	<font face="Arial Rounded MT Bold"><input type="hidden" name="startDate" value='<%=request.getParameter("startDate")%>'> 
                	<input type="hidden" name="endDate" value='<%=request.getParameter("endDate")%>'> 
                	<input type="hidden" name="pids" value="<%=TransactionReport.getProductIds()%>"> 
					<input type="hidden" name="col" value="<%=TransactionReport.getCol()%>"> 
					<input type="hidden" name="sort" value="<%=TransactionReport.getSort()%>"> 
					<input type="hidden" name="selectagentslist" value='<%=TransactionReport.strictconvertArrayToList(request.getParameterValues("selectagentslist"))%>'> 
					<input type="hidden" name="subagentslist" value='<%=TransactionReport.strictconvertArrayToList(request.getParameterValues("subagentslist"))%>'> 
					<input type="hidden" name="repslist" value='<%=TransactionReport.strictconvertArrayToList(request.getParameterValues("repslist"))%>'> 
					<input type="hidden" name="merchantslist" value='<%=TransactionReport.strictconvertArrayToList(request.getParameterValues("merchantslist"))%>'> 
					<input type="hidden" name="attachedheader" value="<%=header%>"> 
                	<input type="hidden" name="Download" value="Y"> 
                	<input type="submit" id="btnSubmit" value='<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>'></font>
                </FORM>
                </td>
              </tr>
            </table>
            </td>	    
		                                
    </td>
    <td width="18">&nbsp;</td>
    </tr>
    </table>
    <div align=right class="backceldas"><font size="1"></div>
                    </td>
                    <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                </tr>
                <tr>
                    <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>  


<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
<% if ( warningSearchResults.size()>0 && showwarningSearchResults)
    { %>
        <tr>
        <td class="main" style="color:red">
                                <% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1)
                                        ||  (!TransactionReport.checkfortaxtype(SessionData))){
        %>
                                        <%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
                                        <%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
        <% } %>
                <% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
                        <%= warningSearchResults.get(i).toString()%> <br/>
                <% } %>
        </td>
        </tr>
<%
    } 
%>
 
            <tr>
              <td align=right class="main" nowrap><% 
              if (intPage > 1) 
              { 
                out.println("<a href=\"admin/reports/transactions/products_merchants_summary.jsp?search=y&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8")+ "&selectagentslist=" + request.getParameter("selectagentslist")+ "&subagentslist="+request.getParameter("subagentslist")+ "&repslist="+request.getParameter("repslist")+ "&merchantslist="+request.getParameter("merchantslist") + "&pids=" + TransactionReport.getProductIds() + "&page=1"  + "\">First</a>&nbsp;"); 
                out.println("<a href=\"admin/reports/transactions/products_merchants_summary.jsp?search=y&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8")+ "&selectagentslist=" + request.getParameter("selectagentslist")+ "&subagentslist="+request.getParameter("subagentslist")+ "&repslist="+request.getParameter("repslist")+ "&merchantslist="+request.getParameter("merchantslist") + "&pids=" + TransactionReport.getProductIds() + "&page=" + (intPage-1) + "\">&lt;&lt;Prev</a>&nbsp;"); 
              } 
 
              int intLowerLimit = intPage - 12; 
              int intUpperLimit = intPage + 12; 
 
              if (intLowerLimit<1) 
              { 
                intLowerLimit=1; 
                intUpperLimit = 25; 
              } 
 
              for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) 
              { 
                if (i==intPage) 
                { 
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;"); 
                } 
                else 
                { 
                  out.println("<a href=\"admin/reports/transactions/products_merchants_summary.jsp?search=y&resultCode=" + TransactionReport.getResultCode() +  "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&selectagentslist=" + request.getParameter("selectagentslist")+ "&subagentslist="+request.getParameter("subagentslist")+ "&repslist="+request.getParameter("repslist")+ "&merchantslist="+request.getParameter("merchantslist") + "&pids=" + TransactionReport.getProductIds() + "&page=" + i + "\">" +i+ "</a>&nbsp;"); 
                } 
              } 
 
              if (intPage <= (intPageCount-1)) 
              { 
                out.println("<a href=\"admin/reports/transactions/products_merchants_summary.jsp?search=y&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&selectagentslist=" + request.getParameter("selectagentslist")+ "&subagentslist="+request.getParameter("subagentslist")+ "&repslist="+request.getParameter("repslist")+ "&merchantslist="+request.getParameter("merchantslist") + "&pids=" + TransactionReport.getProductIds() + "&page=" + (intPage+1) + "\">Next&gt;&gt;</a>&nbsp;"); 
                out.println("<a href=\"admin/reports/transactions/products_merchants_summary.jsp?search=y&resultCode=" + TransactionReport.getResultCode() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&selectagentslist=" + request.getParameter("selectagentslist")+ "&subagentslist="+request.getParameter("subagentslist")+ "&repslist="+request.getParameter("repslist")+ "&merchantslist="+request.getParameter("merchantslist") + "&pids=" + TransactionReport.getProductIds() + "&page=" + (intPageCount) + "\">Last</a>"); 
              } 
 
              %>
              <br></td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2"> 
            <thead align="center"> 
      	      <tr> 
                 <% boolean showCol = true;
                if ( showCol )
                {%>
            
                <td class="rowhead2">#</td> 
                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.agent",SessionData.getLanguage()).toUpperCase()%>&nbsp;
                    <a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=agentname%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'>
                            <img height="11" width="11" border="0" src="images/down.png">
                    </a>
                    <a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=agentname%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'>
                            <img height="11" width="11" border="0" src="images/up.png">
                    </a>
                </td> 
                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.subagent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=subagentname%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=subagentname%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.rep",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=repname%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=repname%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.merchant",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=merchantname%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=merchantname%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td>  
                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.merchantID",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=merchantid%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=merchantid%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.city",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=city%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=city%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <%//country switched to state for USA  
                if( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)  
                 || ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  
                            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )  
                ){%> 
                  <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.state",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=state%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=state%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                  <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.recharegevalue",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=rechargevalue%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=rechargevalue%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <%}else{%> 
                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.recharegevalue",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=rechargevalue%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=rechargevalue%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.bonusAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=bonus%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=bonus%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.totalRecharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=totalrecharge%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=totalrecharge%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 

               <%
                if( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)){ %> 
               <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=netamount%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=netamount%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=taxamount%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=taxamount%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 

               <%}}%> 

                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.SKU",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=productid%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=productid%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td>               
                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.Product",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=productname%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=productname%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td>        
                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.lasttransaction",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=lasttrx%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=lasttrx%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td>               


      <%       if ( strAccessLevel.equals(DebisysConstants.ISO) ){%> 
                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.iso_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=isorate%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=isorate%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                <%}%><%if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){   
                    if ( strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) ){%> 

                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.agent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=agentrate%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=agentrate%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
                 <%}  
            if ( strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) ){%> 
                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.subagent_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=subagentrate%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=subagentrate%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td>               
                 <%}%><%}  
            if ( strAccessLevel.equals(DebisysConstants.ISO)  || strAccessLevel.equals(DebisysConstants.AGENT)   || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP) ){%> 

                <td nowrap class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.rep_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=reprate%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=reprate%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 
             <%}%> 


                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.merchant_percent",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=merchantrate%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=merchantrate%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 

                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.trxnumber",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=trxcount%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/down.png"></a><a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=trxcount%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'><img height="11" width="11" border="0" src="images/up.png"></a></td> 

                <%
                if( addColumnsDTU1281jsp ){ %> 
                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.salesTitle",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td> 
                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.returnsTitle",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                <td class="rowhead2"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.netTitle",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                            <%}%> 
                            
                <%
                }
                else
                {
                %>
                
                    <td class="rowhead2" valign="bottom">#</td>
                    
                    <%  
                    for ( ColumnReport columnReport : headers )
                    {
                    %>

                    <td nowrap class=rowhead2>
                      <%=columnReport.getLanguageDescription()%>
                      
                        <a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=agentname%>&amp;sort=<%=ASC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'>
                            <img height="11" width="11" border="0" src="images/down.png">
                        </a>
                        <a href='admin/reports/transactions/products_merchants_summary.jsp?search=y&amp;page=<%=intPage%>&amp;col=<%=agentname%>&amp;sort=<%=DESC%>&amp;selectagentslist=<%=request.getParameter("selectagentslist")%>&amp;subagentslist=<%=request.getParameter("subagentslist")%>&amp;repslist=<%=request.getParameter("repslist")%>&amp;merchantslist=<%=request.getParameter("merchantslist")%>&amp;resultCode=<%=TransactionReport.getResultCode()%>&amp;startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&amp;endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>&amp;pids=<%=TransactionReport.getProductIds()%>'>
                            <img height="11" width="11" border="0" src="images/up.png">
                        </a>
                            
                    </td>

                    <% 
                    }
                    %>                
                
                
                
                <%
                }
                %>
                
            </tr> 
            </thead>
            
            <% 
                  int intCounter = 1; 
                  Iterator it = vecSearchResults.iterator(); 
                  int intEvenOdd = 1; 
                  while (it.hasNext()) 
                  { 
                    Vector vecTemp = null; 
                    vecTemp = (Vector) it.next(); 
                    out.println("<tr class=row" + intEvenOdd +">" + 
                                "<td nowrap>" + intCounter++ + "</td>" + 
                                "<td nowrap>" + vecTemp.get(2)+ "</td>" + 
                                "<td nowrap>" + vecTemp.get(1) + "</td>" + 
                                "<td nowrap align=\"right\">" + vecTemp.get(0) + "</td>" + 
                                "<td nowrap align=\"right\">" + vecTemp.get(3) + "</td>" + 
                                "<td nowrap align=\"right\">" + vecTemp.get(4) + "</td>" + 
                                "<td nowrap align=\"right\">" + vecTemp.get(5) + "</td>" ); 
                                //country switched to state for USA 
              if( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	      		  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) 
              ){ 
                                out.println("<td nowrap align=\"right\">" + vecTemp.get(7) + "</td>" + 
                                 "<td nowrap align=\"right\">" + vecTemp.get(8) + "</td>" + 
                                 "<td nowrap align=\"right\">" + vecTemp.get(9) + "</td>" ); 
                                 //DBSY-905 Tax Calculation 
			                        if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) 
				    	 					{ 
			                     //net amount  
			                         	       out.println("<td nowrap>" + vecTemp.get(20) + "</td>" + 
			                     //tax amount 
			                                 "<td nowrap>" + vecTemp.get(21) + "</td>"); 
			                                } 
               } 
               else{ 
              					 out.println("<td nowrap align=\"right\">" + vecTemp.get(19) + "</td>"+ 
                                 "<td nowrap align=\"right\">" + vecTemp.get(7) + "</td>" ); 
			        } 
              out.println("<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>"+ 
                                 "<td nowrap align=\"right\">" + vecTemp.get(11) + "</td>"+ 
                                 "<td nowrap align=\"right\">" + vecTemp.get(12) + "</td>" ); 
                   
                   if ( strAccessLevel.equals(DebisysConstants.ISO) ){ 
                              out.println("<td nowrap align=\"right\">" + vecTemp.get(13) + "</td>" ); 
             } 
              
         if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){  
		  if ( strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) ){ 
              out.println("<td nowrap align=\"right\">" + vecTemp.get(14) + "</td>" ); 
                   } 
          if ( strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) ){ 
              out.println("<td nowrap align=\"right\">" + vecTemp.get(15) + "</td>" ); 
                } 
          } 
          if ( strAccessLevel.equals(DebisysConstants.ISO)  || strAccessLevel.equals(DebisysConstants.AGENT)   || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP) )
          { 
              out.println("<td nowrap align=\"right\">" + vecTemp.get(16) + "</td>"  ); 
          }  
           
 
 
			out.println("<td nowrap align=\"right\">" + vecTemp.get(17) + "</td>"); 
            out.println("<td nowrap align=\"right\">" + vecTemp.get(18) + "</td>"); 
            
            if( addColumnsDTU1281jsp ){
               out.println("<td nowrap align=\"right\">" + vecTemp.get(22) + "</td>"  );
               out.println("<td nowrap align=\"right\">" + vecTemp.get(23) + "</td>"  );               
               out.println("<td nowrap align=\"right\">" + vecTemp.get(24) + "</td>"  );
            }
            out.println("</tr>");
                    
            if (intEvenOdd == 1) 
            { 
              intEvenOdd = 2; 
            } 
            else 
            { 
              intEvenOdd = 1; 
            } 
 
        } 
        vecSearchResults.clear(); 
            %>
            </table>
            </table>
             <%
}
else
{
if(TransactionReport.getSelectionType()==0)	
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_merchant",SessionData.getLanguage())+"</font>");
else
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
	
}
%>             

	 

<%@ include file="/includes/footer.jsp" %>