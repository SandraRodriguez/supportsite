<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.languages.*,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 4;
  int section_page = 18;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;

  int intErrorsSum = 0;

    if (TransactionReport.validateDateRange(SessionData))
    {
      	SessionData.setProperty("start_date", request.getParameter("startDate"));
      	SessionData.setProperty("end_date", request.getParameter("endDate"));
		String strProviders = request.getParameter("provider_ids");
		if (strProviders != null)
		{
			TransactionReport.setProviderIDs(strProviders);
		}
		if (TransactionReport.validateProviderList(SessionData))
		{
			String strProviderErrors = request.getParameter("provider_errors");
			if (strProviderErrors != null)
			{
				TransactionReport.setProviderErrorCodes(strProviderErrors);
			}
			if (TransactionReport.validateErrorList(SessionData))
			{
				String strProductIds = request.getParameter("products");
				if (strProductIds != null)
				{
					TransactionReport.setProductIds(strProductIds);
				}
				vecSearchResults = TransactionReport.getTransactionErrorSummaryByProviderErrors(SessionData);
				String sURL = com.debisys.transactions.TransactionSearch.downloadProviderErrorsSummaryReport(application, vecSearchResults,SessionData);
				response.sendRedirect(sURL);
				return;
			}
			else
			{
				searchErrors = TransactionReport.getErrors();
			}
		}
		else
		{
			searchErrors = TransactionReport.getErrors();
		}
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
%>
