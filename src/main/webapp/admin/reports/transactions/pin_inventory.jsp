<%@ page
	import="java.net.URLEncoder,com.debisys.utils.HTMLEncoder,java.util.*,com.debisys.customers.Merchant,com.debisys.reports.TransactionReport"%>
<%
	int section = 4;
	int section_page = 31;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}

</SCRIPT>


            <TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
              <TBODY>
              <TR>
                <TD align=left width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=images/top_blue.gif width="2000">&nbsp;<B><%=Languages.getString("jsp.admin.reports.pininventory.title",SessionData.getLanguage()).toUpperCase()%></B></TD>
                <TD align=right width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_right_blue.gif" width=18></TD></TR>
                  <TR>
                <TD colSpan=3>
                  <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
                  bgColor=#fffcdf class="backceldas">
                    <TBODY>
                    <TR class="backceldas">
                      <TD width=1 bgColor=#003082><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top align=middle bgColor=#ffffff>
						<form name="mainform" method="post"	action="admin/reports/transactions/pin_inventory_summary.jsp" onSubmit="scroll();">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="formArea2">
										<table width="300">											
											<tr>
												<td valign="top" nowrap>
													<table width=400>														
														<tr>
															<td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.products.option",SessionData.getLanguage())%></td>
															<td class=main valign=top>
																<select name="pids" size="10" multiple>
																	<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
																	<%
																	     //SessionData.setProperty("filterChain", "f");
																	     //SessionData.setProperty("filterTerminalRatePlans", "f");
																		for (Vector vecTemp : TransactionReport.getProductListForPinInventory(SessionData)) {
																			out.println("<option value=" + vecTemp.get(0) + ">" + vecTemp.get(1) + "(" + vecTemp.get(0) + ")</option>");
																		}
																	%>
																</select>
																<br>
																*<%=Languages.getString("jsp.admin.reports.transactions.products.instructions",SessionData.getLanguage())%>
															</td>
														</tr>
														<tr>
															<td class=main align=center>
																<input type="hidden" name="search" value="y">
																<input type="submit" name="submit"
																	value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="validateSchedule(0);">
															</td>
															    <jsp:include page="/admin/reports/schreportoption.jsp">
															  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
															 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
																</jsp:include>
														</tr>
													</table>
												</td>
											</tr>
											</table>
									</td>
									
								</tr>
							</table>
							</form>
                        </td>
                </td>
                <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	         </tr>
	         <tr>
		            <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
            </tr>
            </table>
        </td>
    </tr>
</table>

<%@ include file="/includes/footer.jsp"%>