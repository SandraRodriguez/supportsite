<%@ page import="java.net.URLEncoder,  
         com.debisys.utils.HTMLEncoder,
         java.util.*, 
         com.debisys.customers.Merchant,
         com.debisys.reports.TransactionReport" %>
<%
    int section = 4;
    int section_page = 33;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>


<SCRIPT LANGUAGE="JavaScript">
    var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ", "< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");

    function scroll()
    {
        document.mainform.submit.disabled = true;
        document.mainform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll()', 150);
    }

</SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.pin_remaining_inventory_report", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">

            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form name="mainform" method="get" action="admin/reports/transactions/pin_remaining_inventory_summary.jsp" onSubmit="scroll();">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="formArea2">              
                                        <table width="300">
                                            <tr class="main">

                                            <tr>
                                                <td valign="top" nowrap>
                                                    <table width=400>
                                                        <tr class="main">
                                                            <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.pin_remaining_inventory.productLabel", SessionData.getLanguage())%>:</td>
                                                            <td class=main valign=top>
                                                                <select name="pid">
                                                                    <%
                                                                        SessionData.setProperty("pins_only", Boolean.TRUE.toString());
                                                                        Vector vecProductList = TransactionReport.getProductListForPinInventory(SessionData);//TransactionReport.getProductList(SessionData, null);
                                                                        SessionData.setProperty("pins_only", null);
                                                                        Iterator it = vecProductList.iterator();
                                                                        while (it.hasNext()) {
                                                                            Vector vecTemp = null;
                                                                            vecTemp = (Vector) it.next();
                                                                            out.println("<option value=" + vecTemp.get(0)
                                                                                    + ">" + vecTemp.get(1) + "("
                                                                                    + vecTemp.get(0) + ")</option>");
                                                                        }
                                                                        if (vecProductList.size() == 0) {
                                                                            out.println("<option value=>" + Languages.getString("jsp.admin.reports.pin_remaining_inventory.no_product_error", SessionData.getLanguage()) + "</option>");
                                                                        }
                                                                    %>
                                                                </select>
                                                            </td>
                                                        </tr> 
                                                        <tr class="main">                                                     
                                                            <td>
                                                                <input type="hidden" name="search" value="y">
                                                                <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.pin_remaining_inventory.show_rem_pin_inventory", SessionData.getLanguage())%>"
                                                                       <%=(vecProductList.size() == 0) ? "disabled" : ""%> onclick="validateSchedule(0);">
                                                            </td>
                                                            <%
                                                                if (vecProductList.size() != 0) {
                                                            %>
                                                            <TD>
                                                                <jsp:include page="/admin/reports/schreportoption.jsp">
                                                                    <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
                                                                    <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
                                                                </jsp:include>
                                                            </TD>
                                                            <%
                                                                }
                                                            %>

                                                        </tr>                                                       
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>          
                        </form>
                    </td>
                </tr>                                
            </table>
        </td>
    </tr>  
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe> 
<%@ include file="/includes/footer.jsp" %>                        
