<%@ page
	import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.NFinanseReport"%>
<%
int intACHRange = DebisysConfigListener.getACHRange(application) - 1;
if(intACHRange <= 0){
	intACHRange = 180;
}
int section=4;
int section_page=43;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<script language="JavaScript">
	function PadLeft(sValue, sPad, nLength){
		sValue = sValue.toString();
	  	while ( sValue.length < nLength ){sValue = sPad + sValue;}
		return sValue;
	}			    											
	function setDate(){
		if(!document.getElementById("startDate").value.length == 0){
			var dtStart = Date.parse(document.getElementById("startDate").value);
			dtStart = new Date(dtStart);
			var sStartYear = dtStart.getFullYear();
			var sStartMonth = PadLeft(dtStart.getMonth() + 1, "0", 2);
			var sStartDay = PadLeft(dtStart.getDate(), "0", 2);
			dtStart.setDate(dtStart.getDate() + <%=intACHRange%>);
			var sEndYear = dtStart.getFullYear();
			var sEndMonth = PadLeft(dtStart.getMonth() + 1, "0", 2);
			var sEndDay = PadLeft(dtStart.getDate(), "0", 2);
			if(self.gfPop) gfPop.fPopCalendar(document.getElementById("endDate"),[[sStartYear,sStartMonth,sStartDay],[sEndYear,sEndMonth,sEndDay],[sStartYear,sStartMonth]]);
		}else{
			if(self.gfPop) gfPop.fEndPop(document.getElementById("startDate"),document.getElementById("endDate"));
		}	                                        	            		
		return false;
	}
	function checkDates(){
		var ok = true;
		var errorMessage = '<%=Languages.getString("jsp.admin.reports.nfinanse.dateerror",SessionData.getLanguage())%>';
		if(!validateDate(document.getElementById("startDate"))){
	    	ok=false;
	    	document.getElementById("startDate").focus();
		}
		if(!validateDate(document.getElementById("startDate"))){
	    	ok=false;
	    	document.getElementById("startDate").focus();
		}
		if(!ok){
			alert(errorMessage);
		}
		return ok;
	}
	function validateDate(fld) {
	    var RegExPattern = /^(?=\d)(?:(?:(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})|(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))|(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2}))($|\ (?=\d)))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
	    if ((fld.value.match(RegExPattern)) && (fld.value!='')) {
	        return true; 
	    } 
	    return false;
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" width="2000"
			class="formAreaTitle">
			&nbsp;<%=Languages.getString("jsp.admin.reports.nfinanse.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20">
			<img src="images/top_right_blue.gif">
		</td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
				<tr>
					<td>
						<form name="mainform" method="get"
							action="admin/reports/transactions/nfinanse_summary.jsp"
							onSubmit="scroll();">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="formArea2">
										<table width="1000">
											<tr class="main">
												<td nowrap="nowrap" valign="top"><b>
							               	 <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
									         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</b></td>
									         <%}else{ %>
									         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</b></td>
									         <%}%>	<td>&nbsp;</td>
											</tr>
											<tr class="main">
												<td nowrap="nowrap"><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
												<td>
													<input class="plain" id="startDate" name="startDate" value='<%=SessionData.getProperty("start_date")%>' size="12">
													<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.getElementById('startDate'),document.getElementById('endDate'));return false;" hidefocus=""> <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""> </a>
												</td>
											</tr>
											<tr class="main">
												<td nowrap="nowrap"><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:
												</td>
												<td>
													<input class="plain" id="endDate" name="endDate" value='<%=SessionData.getProperty("end_date")%>' size="12"><!-- onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;"  -->
													<a href="javascript:void(0)"  onclick="setDate();" HIDEFOCUS=""><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""> </a>
												</td>
											</tr>
											<tr>
												<td class="main" colspan="2">
													*
													<%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
												</td>
											</tr>
											<tr>
												<td class="main" valign="top" nowrap="nowrap"><b><%=Languages.getString("jsp.admin.reports.nfinanse.categories.option",SessionData.getLanguage())%></b></td>
												<td class="main" valign="top">
													<select name="categoryIds" size="10" multiple="multiple">
														<option value="" selected="selected"><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
														<option value="6"><%=Languages.getString("jsp.admin.reports.nfinanse.GiftCard",SessionData.getLanguage())%></option>
														<option value="5"><%=Languages.getString("jsp.admin.reports.nfinanse.StoredValue",SessionData.getLanguage())%></option>
													</select>
													<br>*<%=Languages.getString("jsp.admin.reports.transactions.products.instructions",SessionData.getLanguage())%></br>
												</td>
											</tr>
											<%
									if(!strAccessLevel.equals(DebisysConstants.MERCHANT)){
									%>
											<tr>
												<td class="main" valign="top" nowrap="nowrap"><b><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%></b></td>
												<td class="main" valign="top">
													<select name="merchantIds" size="10" multiple="multiple">
														<option value="" selected="selected"><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
										<%
										Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
										Iterator it = vecMerchantList.iterator();
										while (it.hasNext())
										{
											Vector vecTemp = null;
											vecTemp = (Vector) it.next();
											out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
										}
										%>
													</select>
													<br>
													*<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%>
												</td>
											</tr>
									<%}%>
											<tr class="main">
												<td nowrap="nowrap" valign="top"><b><%=Languages.getString("jsp.admin.reports.rowsperpage",SessionData.getLanguage())%></b></td>
												<td>
													<select name="rowsperpage">
												<%
													for(int i=1;i<=10;i++){%>
														<option value="<%=(i*100)%>"><%=(i*100)%> rows</option>
													<%}
												%>
													</select>
												</td>
											</tr>
											<tr>
												<td class="main" colspan="2" align="center">
													<input type="hidden" name="search" value="y">
													<input type="submit" name="submit" onclick="return checkDates();" value='<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>'>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe width="132" height="142" name="gToday:contrast:agenda.js"
	id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;">
</iframe>
<%@ include file="/includes/footer.jsp"%>