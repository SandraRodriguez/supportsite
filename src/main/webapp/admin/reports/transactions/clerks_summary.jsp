<%@page import="com.debisys.schedulereports.ScheduleReport"%>
<%@page import="com.debisys.utils.ColumnReport"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 4;
  int section_page = 17;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
    Vector<Vector<String>>    vecSearchResults = new Vector<Vector<String>>();
    Vector warningSearchResults = new Vector();
    Hashtable searchErrors     = null;
    String    sortString       = "";
    boolean   bUseTaxValue     = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns
    String customConfig = DebisysConfigListener.getCustomConfigType(application);
    ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
    ArrayList<String> titles = new ArrayList<String>();
    String sortJavascript = new String();
    
    String testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());  
    String keyLanguage = "jsp.admin.reports.title30";
    String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
    
    //////////////////////////////////////////////////////////////////
    //HERE WE DEFINE THE REPORT'S HEADERS 
    headers = com.debisys.reports.ReportsUtil.getHeadersSummaryTrxByClerk( SessionData, application);
    sortJavascript = (String)SessionData.getProperty("sortJavascript");
    SessionData.removeProperty("sortJavascript");
    //System.out.println("sortJavascript: "+sortJavascript);
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
           
    Vector vTimeZoneData = null;
    if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
    {
        vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
    }
    else
    {
        vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
    }
    String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+" "+vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
    
    if ( request.getParameter("Download") != null )
    {
      String sURL = "";
      TransactionReport.setMerchantIds(request.getParameter("merchantIds"));
      if (customConfig.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (request.getParameter("chkUseTaxValue") != null))
      {
          bUseTaxValue = true;
          sURL = TransactionReport.downloadClerkSummary(application, SessionData, true);
      }
      else
      {
          sURL = TransactionReport.downloadClerkSummary(application, SessionData, false);
      }
      response.sendRedirect(sURL);
      return;
    }
  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
        SessionData.setProperty("start_date", request.getParameter("startDate"));
        SessionData.setProperty("end_date", request.getParameter("endDate"));
        String strMerchantIds[] = request.getParameterValues("mids");
        if (strMerchantIds != null)
        {
          TransactionReport.setMerchantIds(strMerchantIds);
        }
        
        if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
	{
            titles.add(noteTimeZone);
            titles.add(testTrx);
            titles.add( Languages.getString(keyLanguage,SessionData.getLanguage()) );
            TransactionReport.getClerkSummary(SessionData,application, DebisysConstants.SCHEDULE_REPORT, headers, titles);
            ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
            if (  scheduleReport != null  )
            {
                scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
                scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
                scheduleReport.setTitleName( keyLanguage );   
            }	
            response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );            
        }
        else
        {
            if ( customConfig.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (request.getParameter("chkUseTaxValue") != null) )
            {
                bUseTaxValue = true;
                vecSearchResults = TransactionReport.getClerkSummaryMx(SessionData, application);
            }
            else
            {
                vecSearchResults = TransactionReport.getClerkSummary(SessionData,application, DebisysConstants.EXECUTE_REPORT, null, null);
            }	  
            if ( TransactionReport.getWarningMessage() != null)
            {    
                warningSearchResults = TransactionReport.getWarningMessage();
            }
        }        
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
        &nbsp;
        <%= titleReport.toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
          Enumeration enum1 = searchErrors.keys();
          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }
          out.println("</font></td></tr></table>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
<%
			
%>
            <tr class="main"><td nowrap colspan="2"><%=noteTimeZone%><br/><br/></td></tr>
          <% boolean showWarnings = false;
            if ( showWarnings && warningSearchResults.size()>0)
            { 
          %>
            <tr>
                <td class="main" style="color:red">
                <% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1) ||  (!TransactionReport.checkfortaxtype(SessionData)))
                {
                %>
                <%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
                <%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
              <%}%>
                <% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
                        <%= warningSearchResults.get(i).toString()%> <br/>
                <% } %>
                </td>
            </tr>
            <% } %>
            <tr>
              <td class="main">
                <%= titleReport %>
<%
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                  out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
%>
                <br>
                <font color="#ff0000"><%= testTrx %></font>
              </td>
              <td class=main align=right valign=bottom><%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %></td>
            </tr>
            <tr>
            	<td colspan=2>
                    <table>
                      <tr><td>&nbsp;</td></tr>
                      <tr>
                        <td>
                        <FORM ACTION="admin/reports/transactions/clerks_summary.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                                <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                                <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                                <INPUT TYPE=hidden NAME=chkUseTaxValue VALUE="<%=request.getParameter("chkUseTaxValue")%>">
                                <INPUT TYPE=hidden NAME=merchantIds VALUE="<%=TransactionReport.getMerchantIds()%>">
                                <INPUT TYPE=hidden NAME=Download VALUE="Y">
                                <INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                        </FORM>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                        </td>
                      </tr>
                    </table>
            	</td>
            </tr>
          </table>
          <table>
            <tr>
                <td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>              
                <tr class="SectionTopBorder">
                  <td class=rowhead2>
                    #
                  </td>
                    <% for ( ColumnReport columnReport : headers ){%>

                       <td class=rowhead2>
                         <%=columnReport.getLanguageDescription()%>
                       </td>

                    <%}%>
                 </tr>               
            </thead>
<%
            double   dblTotalSalesSum         = 0;
            double   dblTotalBonus = 0;
            double   dblMerchantCommissionSum = 0;
            double   dblRepCommissionSum      = 0;
            double   dblSubAgentCommissionSum = 0;
            double   dblAgentCommissionSum    = 0;
            double   dblISOCommissionSum      = 0;
            double   dblVATTotalSalesSum      = 0;
            double   dblVATMerchantCommissionSum = 0;
            double   dblVATRepCommissionSum      = 0;
            double   dblVATSubAgentCommissionSum = 0;
            double   dblVATAgentCommissionSum    = 0;
            double   dblVATISOCommissionSum      = 0;
            int      intTotalQtySum              = 0;
            double   dblTaxAmountSum		     = 0;
            Iterator it                          = vecSearchResults.iterator();
            int      intEvenOdd                  = 1;
            int      intCounter               = 1;              
            
            Vector vecTemp = null;
            
            if (it.hasNext())
            {
              vecTemp = (Vector)it.next();
              dblTotalSalesSum = Double.parseDouble(vecTemp.get(0).toString());
              dblMerchantCommissionSum = Double.parseDouble(vecTemp.get(2).toString());
              dblRepCommissionSum = Double.parseDouble(vecTemp.get(3).toString());
              dblSubAgentCommissionSum = Double.parseDouble(vecTemp.get(4).toString());
              dblAgentCommissionSum = Double.parseDouble(vecTemp.get(5).toString());
              dblISOCommissionSum = Double.parseDouble(vecTemp.get(6).toString());
              if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
              {
                dblVATTotalSalesSum = Double.parseDouble(vecTemp.get(1).toString());
              }
              
              if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
              {
                dblTaxAmountSum= Double.parseDouble(vecTemp.get(7).toString());
              }
            }               
              
            while (it.hasNext())
            {
              vecTemp = null;
              vecTemp = (Vector)it.next();

              int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
              double dblTotalSales         = Double.parseDouble(vecTemp.get(3).toString());
              double dblMerchantCommission = Double.parseDouble(vecTemp.get(7).toString());
              double dblRepCommission      = Double.parseDouble(vecTemp.get(8).toString());
              double dblSubAgentCommission = Double.parseDouble(vecTemp.get(9).toString());
              double dblAgentCommission    = Double.parseDouble(vecTemp.get(10).toString());
              double dblISOCommission      = Double.parseDouble(vecTemp.get(11).toString());
              double dbBonusAmount         = Double.parseDouble(vecTemp.get(12).toString());
              dblTotalBonus               += dbBonusAmount;
              double dblVATTotalSales      = 0;
              double dblTaxAmount	   = 0;              
              double averageAmount         = dblTotalSales/intTotalQty;
              String mxUseTaxValue         = "";
              intTotalQtySum               = intTotalQtySum + intTotalQty;

              if ( bUseTaxValue || SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                      DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                        DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
              {
                dblVATTotalSales = Double.parseDouble(vecTemp.get(6).toString());
                mxUseTaxValue = "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue");
              }
              if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                      DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                        DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
              {
                dblTaxAmount= Double.parseDouble(vecTemp.get(14).toString());
              }
              out.print("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + "<td nowrap>" + 
              vecTemp.get(0) + "</td>" + "<td>" + vecTemp.get(1) + "</td>" + "<td align=left>" + intTotalQty + "</td>" + 
                      "<td align=right><a href=\"admin/reports/transactions/clerks_transactions.jsp?startDate=" + 
                      URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + 
                      URLEncoder.encode(TransactionReport.getEndDate(),   "UTF-8") + "&search=y&clerkId=" + vecTemp.get(13) + "&siteId=" + vecTemp.get(5) + mxUseTaxValue +
                      "\" target=\"_blank\">" + NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");

                if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
                {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dbBonusAmount)) + "</td>");
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dbBonusAmount + dblTotalSales)) + "</td>");
                }

                out.println(((bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                        DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                          DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))?"<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATTotalSales)) + "</td>":""));

                if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)
                        && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
                        && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                {
                   out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAmount)) + "</td>");
                }
                     
                out.println( "<td align=right>" + NumberUtil.formatCurrency(Double.toString(averageAmount)) + "</td>");

                if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommission)) + "</td>");
                }

                if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommission)) + "</td>");
                }

                if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommission)) + "</td>");
                }

                if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommission)) + "</td>");
                }
                out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblMerchantCommission)) + "</td>");              
                out.println("</tr>");
              if (intEvenOdd == 1)
              {
                intEvenOdd = 2;
              }
              else
              {
                intEvenOdd = 1;
              }
            }
%> 
			<tfoot>
              <tr class=row<%= intEvenOdd %>>
                <td colspan=3 align=right>
                  <%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                </td>
                <td align=left>
                  <%= intTotalQtySum %>
                </td>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %>
                </td>
<%
if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
%>
				<td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalBonus)) %>
                </td>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalBonus+dblTotalSalesSum)) %>
                </td>
<%
}
                if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                {
%>
                <TD ALIGN="right"><%=NumberUtil.formatCurrency(Double.toString(dblVATTotalSalesSum))%></TD>
<%
                }
%>
<%                if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                {
%>
                <TD ALIGN="right"><%=NumberUtil.formatCurrency(Double.toString(dblTaxAmountSum))%></TD>
<%
                }
%>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum/intTotalQtySum)) %>
                </td>
<%
                if (strAccessLevel.equals(DebisysConstants.ISO))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblISOCommissionSum)) + "</td>");
                  if ( bUseTaxValue && false)
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATISOCommissionSum)) + "</td>");
                  }
                }

                if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentCommissionSum)) + 
                          "</td>");
                  if ( bUseTaxValue && false)
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATAgentCommissionSum)) + "</td>");
                  }
                }

                if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblSubAgentCommissionSum)) + 
                          "</td>");
                  if ( bUseTaxValue && false)
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATSubAgentCommissionSum)) + "</td>");
                  }
                }

                if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblRepCommissionSum)) + "</td>")
                          ;
                  if ( bUseTaxValue && false)
                  {
                    out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATRepCommissionSum)) + "</td>");
                  }
                }
%>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblMerchantCommissionSum)) %>
                </td>
<%
                if ( bUseTaxValue && false)
                {
                  out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATMerchantCommissionSum)) + "</td>");
                }
%>
              </tr>
            </tfoot>
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
        	String bonusOrder = "";
        	if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
        		bonusOrder = " \"Number\", ";
        	}
%>
          <SCRIPT type="text/javascript">
            
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  [<%= sortJavascript %>,"Number"],0,false,false);
  -->
            
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
