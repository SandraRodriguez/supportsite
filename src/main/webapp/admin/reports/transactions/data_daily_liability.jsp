<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.debisys.tools.getChildData"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
	//data_daily_liability.jsp - Written by Alfred A. for DBSY-1082
	String agentTooLong = request.getParameter("agentTooLong");
	String subAgentTooLong = request.getParameter("subAgentTooLong");
	String repTooLong = request.getParameter("repTooLong");
	String isoIDArray[] = request.getParameterValues("isoParam");
	String agentIDArray[] = request.getParameterValues("a");
	String repIDArray[] = request.getParameterValues("r");
	getChildData db = null;
	
	if(agentTooLong != null || subAgentTooLong != null || repTooLong != null){
		db = new getChildData(null, -2, agentTooLong, subAgentTooLong, repTooLong);
	}else if(isoIDArray != null){
		db = new getChildData(isoIDArray, 0, null, null, null);
	}else if(agentIDArray != null){
		db = new getChildData(agentIDArray, 0, null, null, null);
	}else if(repIDArray != null){
		db = new getChildData(repIDArray, -1, null, null, null);
	}
	
	List<String> output = db.getIDs();
	List<String> nameOutput = db.getNames();

	Iterator<String> iterator = output.iterator();
	Iterator<String> nameIterator = nameOutput.iterator();
	
	//Beginning of our XML response section.
	response.setContentType("text/xml");
	response.setCharacterEncoding("UTF-8");//UTF-8
	out.clear();
	//out.println("<?xml version='1.0'?>");
	out.println("<results>");
	while(iterator.hasNext() && nameIterator.hasNext()){
		String name = nameIterator.next();
		String val = iterator.next();
		out.println("<display val=\""+val+"\">"+name+"</display>");
	}
	out.println("</results>");
%>