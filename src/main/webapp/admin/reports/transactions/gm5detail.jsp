<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,com.debisys.utils.NumberUtil,
                 com.debisys.customers.Merchant,
                 org.apache.torque.Torque,java.sql.*,java.util.Date" %>
<%
int section=4;
int section_page=3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<jsp:setProperty name="TransactionReport" property="*"/>


<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
    

<%
int reg=15;
Hashtable searchErrors = null;
Vector    vecSearchResults = new Vector();
String chkOpcAll="checked";
String chkOpcNon="";
boolean bReport=false;

if (request.getParameter("datei") != null && request.getParameter("datei") != null && request.getParameter("type") != null)
{
   java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
   formatter.setLenient(false);        
    
   if ( request.getParameter("type").equals("all") )
   {
      bReport=true;
      chkOpcNon="";
      chkOpcAll="checked";
   }
   else
   {
      bReport=false;
      chkOpcNon="checked";
      chkOpcAll="";
   }
   
   TransactionReport.setStartDate(SessionData.getProperty("start_date"));
   TransactionReport.setEndDate(SessionData.getProperty("end_date"));
   
   vecSearchResults = TransactionReport.getGM5Details(SessionData,bReport);
  
}
if (searchErrors != null)
{
    out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
    Enumeration enum1=searchErrors.keys();
    while(enum1.hasMoreElements())
    {
      String strKey = enum1.nextElement().toString();
      String strError = (String) searchErrors.get(strKey);
      out.println("<li>" + strError);
    }
    out.println("</font></td></tr></table>");
}
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.GM5_Det",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="post" action="admin/reports/transactions/gm5.jsp" onSubmit="scroll();">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
              <tr class="main">
               <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
         <%}else{ %>
         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
         <%}%><td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
                    <table width=400>
                    <tr class="main">
                        <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
                        <td>
                            <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12" disabled>
                            <a href="javascript:void(0)" HIDEFOCUS>
                              <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
                            </a>
                        </td>
                    </tr>
                    <tr class="main">
                        <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td><td> 
                            <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12" disabled>
                            <a href="javascript:void(0)"  HIDEFOCUS>
                              <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.gm5.option",SessionData.getLanguage())%></td>
                        <td class=main valign=top>
                            <%=Languages.getString("jsp.admin.reports.GM5ALL",SessionData.getLanguage())%> 
                            <input id="opcFilter" name="opcFilter" type="radio" value="all" <%=chkOpcAll%>  disabled>
                            
                            <input id="opcFilter" name="opcFilter" type="radio" value="non" <%=chkOpcNon%> disabled>
                            <%=Languages.getString("jsp.admin.reports.GM5Non",SessionData.getLanguage())%>
                         </td>
                    </tr>
                    
                    </table>
               </td>
              </tr>
              <tr>
                <td>    
                                  
                </td>    
               </tr>
              </form>
            </table>
                                
                          
          </td>
      </tr>
    </table>
    
</td>
</tr>
</table>

<% if (vecSearchResults.size() <= 0) {%>

    <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
    <tr>
        <td>
             <%=Languages.getString("jsp.admin.reports.GM5.not_found",SessionData.getLanguage())%>
        </td>
    </tr> 
    <table>
<%}
else{%>

 <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
                            <thead>
                                <tr class="SectionTopBorder"> 
                                    <td class=BoldClickableBody nowrap>
                                      #
                                    </td>
                                    <td class=BoldClickableBody nowrap>
                                      <%=Languages.getString("jsp.admin.reports.GM5_det1",SessionData.getLanguage())%>
                                    </td>
                                     <td class=BoldClickableBody nowrap>
                                      <%=Languages.getString("jsp.admin.reports.GM5_det2",SessionData.getLanguage())%>
                                    </td>
                                    <td class=BoldClickableBody nowrap>
                                      <%=Languages.getString("jsp.admin.reports.GM5_det3",SessionData.getLanguage())%>
                                    </td>
                                    <td class=BoldClickableBody nowrap>
                                       <%=Languages.getString("jsp.admin.reports.GM5_det4",SessionData.getLanguage())%>
                                    </td>
                                     <td class=BoldClickableBody nowrap>
                                       <%=Languages.getString("jsp.admin.reports.GM5_det5",SessionData.getLanguage())%>
                                    </td>
                                    <td class=BoldClickableBody nowrap>
                                       <%=Languages.getString("jsp.admin.reports.GM5_det6",SessionData.getLanguage())%>
                                    </td>
                                    
                                </tr>
                            </thead>

                         <%
                      		// FB - 2008-09-10 - There must be an empty array to show the right messages
                         	if(!vecSearchResults.isEmpty())
                         	{
	                           Iterator it                       = vecSearchResults.iterator();
	                            int      intEvenOdd               = 1;
	                            int      intCounter               = 1;                        
	                            while (it.hasNext())                    
	                            {                                       
	                                Vector vecTemp = null;              
	                                vecTemp = (Vector)it.next();        
	                                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
	                                formatter.setLenient(false);        
	                                //Date dtDaterequest = formatter.parse(vecTemp.get(2).toString());
	                                //Date dtDaterepaid = formatter.parse(vecTemp.get(5).toString());   %>                                    
	                            <tr class=row<%=intEvenOdd%>>                                 
	                                <td><%=intCounter++%></td>
	                                <td><%=vecTemp.get(2).toString()%></td>
	                                <td><%=vecTemp.get(3).toString()%></td> 
	                                <td><%=vecTemp.get(0).toString()%></td> 
	                                <td><%=vecTemp.get(5).toString()%></td> 
	                                <td><%=(vecTemp.get(4).toString().equals("0")?"":vecTemp.get(4).toString())%></td>          
									<td><%=vecTemp.get(6).toString()%></td>                      
	                            </tr>                              
	                           <%
	                            if (intEvenOdd == 1)
	                            {
	                              intEvenOdd = 2;
	                            }
	                            else
	                            {
	                              intEvenOdd = 1;
	                            }
	                            
	                            }
                            }
                            else
							{
							 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
							}
                         %>                         
                            
                           
                        </table>  
                       
                       
<SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["Number","CaseInsensitiveString","CaseInsensitiveString", "Number", "Number", "Number", "Number"],0,false,false);
  -->
          </SCRIPT> 
                        
<%}%>
 
</td>
</tr>  
</table>   

          
                         
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>