<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Rep,
                 com.debisys.customers.Merchant" %>
                 
<%@page import="com.debisys.utils.TimeZone"%>
<%@page import="com.debisys.reports.TransactionReport"%>
<%
int section = 4;
int section_page = 58;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script type="text/javascript" src="/support/includes/jquery.js"></script>


<table border="0" cellpadding="0" cellspacing="0" width="750">
  <tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
		<td colspan="3" bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				align=center>
				<tr>
					<td>
						<form name="mainform" method="post" action="admin/reports/transactions/real_time_transaction_report_detail.jsp" onSubmit="document.getElementById('btnSubmit').disabled = true;">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="formArea2">
										<table width="300">

											<tr>
												<td valign="top" nowrap>
													<table width=400>

														<tr>
															<td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.realTimeTransactionReport.selectTerminalType",SessionData.getLanguage())%></td>
															<td class=main valign=top>
															<select id="terminal_types" name="terminal_types" size="30" multiple style="width: 300px">
																	<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
																	<%
																	    Vector v = TransactionReport.getTerminalTypesByIsologged(SessionData);
																	    Iterator it = v.iterator();
																	    while (it.hasNext()){
																	      Vector vTemp = null;
																	      vTemp = (Vector) it.next();
																	      out.println("<option value=" + vTemp.get(0) +">" + vTemp.get(1) + "</option>");
																	    }
																	%>
															</select></td>
														</tr>

														<tr>
															<td class=main colspan=2 align=center>
															<input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>