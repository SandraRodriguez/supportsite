<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*,
                 com.debisys.reports.pojo.ReportTrxSummaryByAmount,	
                 com.debisys.reports.pojo.TrxSummaryByAmountPojo,
                 com.debisys.currency.Currency,
                 com.debisys.utils.TimeZone,
                 com.debisys.utils.ColumnReport,
                 com.debisys.schedulereports.ScheduleReport" %>
                 

<%
	int section = 4;
	int section_page = 16;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
 	Vector warningSearchResults = new  Vector();
	Hashtable searchErrors = null;
	 
	ArrayList<TrxSummaryByAmountPojo> trxSummAmount = new ArrayList<TrxSummaryByAmountPojo>();
	ReportTrxSummaryByAmount report = new ReportTrxSummaryByAmount();
	ArrayList<String> warnings = new ArrayList<String>();
	TrxSummaryByAmountPojo columnDescriptions = new TrxSummaryByAmountPojo("", "", "", "", "");
	
	String prevStartDate = "";
	String prevEndDate = "";
	
	String denomination = Languages.getString("jsp.admin.reports.transactions.promo_summary.denomination_range",SessionData.getLanguage()).toUpperCase();
	String titleHTML = Languages.getString("jsp.admin.reports.title7",SessionData.getLanguage()).toUpperCase();
	String strUrlParams = "?startDate="+request.getParameter("startDate")+"&endDate="+request.getParameter("endDate");
		
	boolean valAmounts = TransactionReport.validateMinMaxAmount(SessionData);	
	boolean valDates   = TransactionReport.validateDateRange(SessionData);
	TransactionReport.setWarningmessage(new Vector<String>());
	String sc = request.getParameter("sheduleReport");
	System.out.println(valAmounts + " " + valDates );
	if (request.getParameter("search") != null)
	{		  
		  if ( valDates && valAmounts )
		  {
		  	  TransactionReport.setStartDate(request.getParameter("startDate"));
			  TransactionReport.setEndDate(request.getParameter("endDate"));
		  	  if ( sc != null && sc.equals("y") )
			  {
			    //TO SCHEDULE REPORT
				TransactionReport.getTrxSummaryByAmount(SessionData, application, DebisysConstants.SCHEDULE_REPORT );
				
				ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
				if (  scheduleReport != null  )
				{
					scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
					scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
					scheduleReport.setTitleName( "jsp.admin.reports.title7" );   
				}	
				SessionData.setProperty("start_date", TransactionReport.getStartDate());
			    SessionData.setProperty("end_date", TransactionReport.getEndDate());	
				response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );			
			  }
			  else	 
			  {    
		    	report = TransactionReport.getTrxSummaryByAmount(SessionData, application, DebisysConstants.EXECUTE_REPORT);
		    	trxSummAmount = report.getRecords();
		    	warnings = report.getWarnnings();
		    	columnDescriptions = report.getColumnsDescriptions();
		    	titleHTML = report.getTitle().toUpperCase();		    		
		    	prevStartDate = (String)SessionData.getProperty("prevStartDate");
		    	prevEndDate = (String)SessionData.getProperty("prevEndDate");
		     }		    
		  }
		  else
		  {
		   		searchErrors = TransactionReport.getErrors();
		  }
	}
	else if (request.getParameter("download") != null)
	{		
		//System.out.println(" download -- valAmounts: "+valAmounts+" valDates: "+valDates);	
		TransactionReport.setStartDate(request.getParameter("startDate"));
		TransactionReport.setEndDate(request.getParameter("endDate"));
		TransactionReport.getTrxSummaryByAmount(SessionData, application, DebisysConstants.DOWNLOAD_REPORT);
		response.sendRedirect( TransactionReport.getStrUrlLocation() );
		    	
	}
	else 
	  		
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="500">
  <tr>
    <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= titleHTML %></td>
    <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF" class="formArea2">
<%
	boolean bool_custom_config_mexico = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
	boolean bool_custom_config_default = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	
	boolean bool_deploy_type_intl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
	boolean bool_perm_enable_tax_calc = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);

	if (searchErrors != null)
	{
  		out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1 = searchErrors.keys();		
		while(enum1.hasMoreElements())
		{
		  String strKey = enum1.nextElement().toString();
		  String strError = (String) searchErrors.get(strKey);
		  out.println("<li>" + strError);
		}
  		out.println("</font></td></tr></table>");
  	}

	if ( trxSummAmount.size() == 0 )
    {
 		out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
	}	
	else if ( trxSummAmount.size() > 0 )
    {	
   		boolean flagPresentation = ( (bool_custom_config_mexico && (request.getParameter("chkUseTaxValue") != null) )
							 		 || 
							 		 (bool_perm_enable_tax_calc && bool_deploy_type_intl	&& bool_custom_config_default)
							 	   );
					 	   
		Vector<String> vTimeZoneData = new Vector<String>();
		if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
		{
			vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
		}
		else
		{
			vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
		}		
		
		StringBuilder messages = new StringBuilder();
                boolean showWarnings = false;
		if ( showWarnings && warnings != null && warnings.size() > 0 )
		{
			for( String warn : warnings )
			{
	      		messages.append(warn + "<br/>");
	      	}
		}
%>

	<form name="mainform" id="mainform" method="post" action="admin/reports/transactions/trx_summary_by_amount.jsp<%=strUrlParams%>" onSubmit="scroll();">
	
        <table width="100%" cellspacing="1" cellpadding="2">
        <tr class="main">
        		<input type="hidden" name="minAmount" value="<%=TransactionReport.getMinAmount() %>" >
        		<input type="hidden" name="maxAmount" value="<%=TransactionReport.getMaxAmount() %>" >
        	<td nowrap colspan="4"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/>
        	</td>
        </tr>
        <tr>
        	<td>
        		<input type="submit" name="download" value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
        	</td>            	  
        </tr>
        <tr>
       	  
        </tr>
        <% if ( flagPresentation )
   		{%>
        <tr class="errorText">
        	<td colspan="4"><%= messages %></td>
        </tr>
        <%}%>		      
        <tr>
           <td class=rowhead2 valign=bottom><%=columnDescriptions.getDescription().toUpperCase()%></td>
           <td class=rowhead2 valign=bottom><%=columnDescriptions.getCurrentPeriod().toUpperCase()%></td>
           <td class=rowhead2 valign=bottom><%=columnDescriptions.getPreviuosPeriod().toUpperCase()%></td>
           <td class=rowhead2 valign=bottom><%=columnDescriptions.getChange().toUpperCase()%></td>
        </tr>
             
	   <%
 		if ( flagPresentation )
    	{
      	%>			
        
		<tr class="row1" >
           <td><%= trxSummAmount.get(0).getDescription() %></td>      
           <td align=center><%= trxSummAmount.get(0).getCurrentPeriod() %></td>
           <td align=center><%= trxSummAmount.get(0).getPreviuosPeriod() %></td>   
           <td><%= trxSummAmount.get(0).getChange() %></td>
        </tr>
            
        <!-- NUMBER OF TRANSACTIONS -->
         <% 
         String sCurrentSelectedQty = "<a href='admin/reports/transactions/promo_transactions.jsp?search=y&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() +
			 "&startDate=" + request.getParameter("startDate") + "&endDate=" + request.getParameter("endDate") + "' target=_blank>" + trxSummAmount.get(1).getCurrentPeriod() + "</a>";
	
		  String sPrevSelectedQty = "<a href='admin/reports/transactions/promo_transactions.jsp?search=y&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() +
			 "&startDate=" + prevStartDate + "&endDate=" + prevEndDate + "' target=_blank>" + trxSummAmount.get(1).getPreviuosPeriod() + "</a>";
	  		
         %>
         <!-- RECHARGE -->
         <tr class="row1" >
           <td><%= trxSummAmount.get(1).getDescription() %></td>      			 
           <td align=right><%= sCurrentSelectedQty %></td>
           <td align=right><%= sPrevSelectedQty %></td>   
           <td align=right><%= trxSummAmount.get(1).getChange() %></td>
         </tr>         
         <tr class="row2">
           <td><%= trxSummAmount.get(2).getDescription() %></td>      			 
           <td align=right><%= trxSummAmount.get(2).getCurrentPeriod() %></td>
           <td align=right><%= trxSummAmount.get(2).getPreviuosPeriod() %></td>   
           <td align=right><%= trxSummAmount.get(2).getChange() %></td>
         </tr>            
		 <%	
		 if( bool_deploy_type_intl && bool_custom_config_default ) 
      	 {
      	 %>
   		 <tr class="row2">
           <td><%= trxSummAmount.get(3).getDescription() %></td>      		   
           <td align=right><%= trxSummAmount.get(3).getCurrentPeriod() %></td>
           <td align=right><%= trxSummAmount.get(3).getPreviuosPeriod() %></td> 
           <td align=right><%= trxSummAmount.get(3).getChange() %></td>
         </tr>
         <tr class="row2" >
           <td><%= trxSummAmount.get(4).getDescription() %></td>      		   
           <td align=right><%= trxSummAmount.get(4).getCurrentPeriod() %></td>
           <td align=right><%= trxSummAmount.get(4).getPreviuosPeriod() %></td> 
           <td align=right><%= trxSummAmount.get(4).getChange() %></td>
         </tr>	      		
        <%}%>
   		<tr class="row1" >
           <td><%= trxSummAmount.get(5).getDescription() %></td>      			
           <td align=right><%= trxSummAmount.get(5).getCurrentPeriod() %></td>
           <td align=right><%= trxSummAmount.get(5).getPreviuosPeriod() %></td>  
           <td align=right><%= trxSummAmount.get(5).getChange() %></td>
         </tr>
         <tr class="row2" >
           <td><%= trxSummAmount.get(6).getDescription() %></td>      			
           <td align=right><%= trxSummAmount.get(6).getCurrentPeriod() %></td>
           <td align=right><%= trxSummAmount.get(6).getPreviuosPeriod() %></td>  
           <td align=right><%= trxSummAmount.get(6).getChange() %></td>
         </tr>
         <tr class="row1" >
           <td><%= trxSummAmount.get(7).getDescription() %></td>      			
           <td align=right><%= trxSummAmount.get(7).getCurrentPeriod() %></td>
           <td align=right><%= trxSummAmount.get(7).getPreviuosPeriod() %></td>  
           <td align=right><%= trxSummAmount.get(7).getChange() %></td>
         </tr>	      		
   <%} 
     else 
     {%>
		 
            
          <tr class=row1 >
            <td><%= trxSummAmount.get(0).getDescription() %></td>      
            <td align=center><%= trxSummAmount.get(0).getCurrentPeriod() %></td>
            <td align=center><%= trxSummAmount.get(0).getPreviuosPeriod() %></td>   
            <td><%= trxSummAmount.get(0).getChange() %></td>
          </tr>
          
          <!-- NUMBER OF TRANSACTIONS -->
          <% 
          String sCurrentSelectedQty = "<a href='admin/reports/transactions/promo_transactions.jsp?search=y&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() +
				 "&startDate=" + request.getParameter("startDate") + "&endDate=" + request.getParameter("endDate") + "' target=_blank>" + trxSummAmount.get(1).getCurrentPeriod() + "</a>";
		
			String sPrevSelectedQty = "<a href='admin/reports/transactions/promo_transactions.jsp?search=y&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() +
				 "&startDate=" + prevStartDate + "&endDate=" + prevEndDate + "' target=_blank>" + trxSummAmount.get(1).getPreviuosPeriod() + "</a>";
		  		
          %>
           <tr class=row1 >
             <td><%= trxSummAmount.get(1).getDescription() %></td>      			 
             <td align=right><%= sCurrentSelectedQty %></td>
             <td align=right><%= sPrevSelectedQty %></td>   
             <td align=right><%= trxSummAmount.get(1).getChange() %></td>
           </tr>
           
           <tr class=row2>
             <td><%= trxSummAmount.get(2).getDescription() %></td>      			 
             <td align=right><%= trxSummAmount.get(2).getCurrentPeriod() %></td>
             <td align=right><%= trxSummAmount.get(2).getPreviuosPeriod() %></td>   
             <td align=right><%= trxSummAmount.get(2).getChange() %></td>
           </tr>
            
            <%	
			if( bool_deploy_type_intl && bool_custom_config_default ) 
	      	{
	      	%>
      		<tr class=row2>
              <td><%= trxSummAmount.get(3).getDescription() %></td>      		   
              <td align=right><%= trxSummAmount.get(3).getCurrentPeriod() %></td>
              <td align=right><%= trxSummAmount.get(3).getPreviuosPeriod() %></td> 
              <td align=right><%= trxSummAmount.get(3).getChange() %></td>
            </tr>
            <tr class=row2 >
              <td><%= trxSummAmount.get(4).getDescription() %></td>      		   
              <td align=right><%= trxSummAmount.get(4).getCurrentPeriod() %></td>
              <td align=right><%= trxSummAmount.get(4).getPreviuosPeriod() %></td> 
              <td align=right><%= trxSummAmount.get(4).getChange() %></td>
            </tr>	      		
	        <%}%>
	        
	        <tr class=row1 >
              <td><%= trxSummAmount.get(7).getDescription() %></td>      			
              <td align=right><%= trxSummAmount.get(7).getCurrentPeriod() %></td>
              <td align=right><%= trxSummAmount.get(7).getPreviuosPeriod() %></td>  
              <td align=right><%= trxSummAmount.get(7).getChange() %></td>
            </tr>
	<%
	  }      
    %>
    </table>
    </form>
    <%
	}	
	%> 
          </td>
      </tr>  
</table>
<%@ include file="/includes/footer.jsp" %>
