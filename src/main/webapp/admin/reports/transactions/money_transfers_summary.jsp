<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.users.User"%>
<%
int section=4;
int section_page=36;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
    
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<% 
String[] strProductIds = request.getParameterValues("pids");
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
int intRecordCount = 0;
Hashtable searchErrors = null;
Vector vecSearchResults = new Vector();

if (request.getParameter("search") != null)
{
	if (request.getParameter("page") != null)
	{
	  try
	  {
	    intPage=Integer.parseInt(request.getParameter("page"));
	  }
	  catch(NumberFormatException ex)
	  {
	    intPage = 1;
	  }
	}
	if (intPage < 1)
	{
	  intPage=1;
	}
	
  	if (TransactionReport.validateDateRange(SessionData))
  	{
	  	SessionData.setProperty("start_date", request.getParameter("startDate"));
		SessionData.setProperty("end_date", request.getParameter("endDate"));
	    
	    if (strProductIds != null)
	    {
	    	if(strProductIds[0] != null && "".equals(strProductIds[0])){
				  Vector vecProductList = TransactionReport.getProductOnlyByCategoryList(SessionData, "11" );//money transfer
				  Iterator it = vecProductList.iterator();
				  strProductIds = new String[vecProductList.size()];
				  int i=0;
				  while (it.hasNext())
				  {
					    Vector vecTemp = null;
					    vecTemp = (Vector) it.next();
					    strProductIds[i++] = vecTemp.get(0).toString();
	    		   }
	    	}
	    
	      TransactionReport.setProductIds(strProductIds);
	    }
	    String strMerchantIds[] = request.getParameterValues("merchantIds");	    
	    if (strMerchantIds != null)
	    {
	      TransactionReport.setMerchantIds(strMerchantIds);
	    }		


		vecSearchResults = TransactionReport.getMoneyTransfers(intPage,intPageSize,SessionData);
		intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
		vecSearchResults.removeElementAt(0);
	    if (intRecordCount>0)
	    {
	      intPageCount = (intRecordCount / intPageSize);
	      if ((intPageCount * intPageSize) < intRecordCount)
	      {
	        intPageCount++;
	      }
	    }		
	}else{
		searchErrors = TransactionReport.getErrors();
	}
}
%>

    <SCRIPT LANGUAGE="JavaScript">
	var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

    function scroll2()
    {
    document.downloadform.submit.disabled = true;  
    document.downloadform.submit.value = iProcessMsg[count];
    count++
    if (count = iProcessMsg.length) count = 0
    setTimeout('scroll2()', 150);
    }
    
    function submitPage(thePage)
    {
        document.submitPageForm.page.value = thePage;  
    	document.submitPageForm.submit();
    }
	</SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="980">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.money_transfers.header",SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
       
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>                        
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="formArea2">
               <%
            	if (searchErrors != null)
            	{
            		out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"
            					+ Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
            		Enumeration enum1 = searchErrors.keys();
            		while (enum1.hasMoreElements())
            		{
            			String strKey = enum1.nextElement().toString();
            			String strError = (String) searchErrors.get(strKey);
            			out.println("<li>" + strError);
            		}
            		out.println("</font></td></tr></table>");
            	}
            	%>                 

<%
if(strProductIds != null && strProductIds.length > 0 &&
!"".equals(SessionData.getProperty("start_date")) &&
!"".equals(SessionData.getProperty("end_date"))){

	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
<br>
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	  <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
	    {
	         out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
	    }
	  %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%></td></tr>
	  <tr>
	    <td align=left class="main" nowrap>
	    <form name="downloadform" method=post action="admin/reports/transactions/money_transfers_export.jsp" onSubmit="scroll2();">
		    <input type="hidden" name="startDate" value="<%=TransactionReport.getStartDate()%>">
		    <input type="hidden" name="endDate" value="<%=TransactionReport.getEndDate()%>">
		    <input type="hidden" name="page" value="<%=intPage%>">
		    <input type="hidden" name="section_page" value="<%=section_page%>">
		    <input type="hidden" name="download" value="y">
		    <input type="hidden" id="pids" name="pids" value="<%=TransactionReport.getProductIds()%>">
		    <input type="hidden" id="merchantIds" name="merchantIds" value="<%=TransactionReport.getMerchantIds()%>"> 
		    <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.money_transfers.download",SessionData.getLanguage())%>">
	    </form>
	    </td>
	  </tr>
	  <tr>
	    <td align=right class="main" nowrap>
	    <form name="submitPageForm" id="submitPageForm" method="post" action="admin/reports/transactions/money_transfers_summary.jsp">
	    	<input type="hidden" name="search" value="<%=URLEncoder.encode(request.getParameter("search"), "UTF-8")%>" />
	    	<input type="hidden" name="startDate" value="<%=TransactionReport.getStartDate()%>" />
	    	<input type="hidden" name="endDate" value="<%=TransactionReport.getEndDate()%>" />
	    	<input type="hidden" name="pids" value="<%=TransactionReport.getProductIds()%>" />
	    	<input type="hidden" name="merchantIds" value="<%=TransactionReport.getMerchantIds()%>" />
	    	<input type="hidden" name="page" value="1" />	    	
		    <%
		    if (intPage > 1)
		    {
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage(1);return false;\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+ (intPage-1) +");return false;\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
		    }
		    int intLowerLimit = intPage - 12;
		    int intUpperLimit = intPage + 12;
		
		    if (intLowerLimit<1)
		    {
		      intLowerLimit=1;
		      intUpperLimit = 25;
		    }
		
		    for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
		    {
		      if (i==intPage)
		      {
		        out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
		      }
		      else
		      {
		        out.println("<a href=\"javascript:;\" onClick=\"submitPage("+ i +");return false;\">" + i + "</a>&nbsp;");
		      }
		    }
		
		    if (intPage <= (intPageCount-1))
		    {
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+(intPage+1)+");return false;\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+(intPageCount)+");return false;\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
		    }		
		    %>
	    </form>
	    </td>
	  </tr>
	  </table>
 <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
     <thead>
     <tr class="SectionTopBorder">
        <td class=rowhead2 align=center>#</td>
        <td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.DBA",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.PrincipalAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.FixedFee",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.VariableFee",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.TotalAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.PayoutCountry",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.PayoutCurrency",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.PayoutAmount",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.TransactionDate",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.TransactionID",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.ProviderTransactionID",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.MerchantLocation",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.ProviderOtherInfo",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.SKU",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
		<td class=rowhead2 align=center><%=Languages.getString("jsp.admin.reports.money_transfers.TransactionType",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
	</tr>
</thead>	
<%
			Iterator itResults = vecSearchResults.iterator();
			int intEvenOdd = 1;
			int count = 1;
			while (itResults.hasNext())
			{
				Vector vecTemp = null;
				vecTemp = (Vector) itResults.next();
				String amountValue = "";
                if ( vecTemp.get(4).toString().contains("-"))
                {
                	amountValue = "<font color=#ff0000>"+vecTemp.get(4)+"</font>";
                }
                else
                {
                	amountValue = vecTemp.get(4).toString();
                }				
				out.println("<tr class=row" + intEvenOdd + ">");
				out.println("<td>" + (count++) + "</td>");
				out.println("<td>" + vecTemp.get(0) + "</td>");
				out.println("<td nowrap align=right>" + vecTemp.get(1) + "</td>");
				out.println("<td align=right>" + vecTemp.get(2) + "</td>");
				out.println("<td align=right>" + vecTemp.get(3) + "</td>");
				out.println("<td align=right>" + amountValue + "</td>");
				out.println("<td>" + vecTemp.get(5) + "</td>");
				out.println("<td align=center>" + vecTemp.get(6) + "</td>");
				out.println("<td align=right>" + vecTemp.get(7) + "</td>");
				out.println("<td>" + vecTemp.get(8) + "</td>");
				out.println("<td align=center>" + vecTemp.get(9) + "</td>");
				out.println("<td align=center>" + vecTemp.get(10) + "</td>");
				out.println("<td>" + vecTemp.get(11) + "</td>");
				out.println("<td>" + vecTemp.get(12) + "</td>");
				out.println("<td>" + vecTemp.get(13) + "</td>");
				out.println("<td>" + vecTemp.get(14) + "</td>");					
				out.println("</tr>");
				
				if (intEvenOdd == 1)
				{
					intEvenOdd = 2;
				}
				else
				{
					intEvenOdd = 1;
				}
			}
			vecSearchResults.clear();
	%>
<SCRIPT type="text/javascript">
     <!--
	var stT1 = new SortROC(document.getElementById("t1"),
							["None","String","Number","Number","Number","Number","String","String","Number","DateTime","Number","Number","String","String","Number","String"],
							0,false,false);
      -->            
</SCRIPT>
</table>
<%
	}else if(vecSearchResults.size() == 0 && request.getParameter("search") != null)
	{
		out.println("<br><br><font color=ff0000>"
				+ Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
	}
 %>
<%
}
%>
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>                                
            </table>
        </td>
    </tr>  
</table>

<%@ include file="/includes/footer.jsp" %>
