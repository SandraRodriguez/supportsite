<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*,
                 com.debisys.reports.pojo.*,
                 com.debisys.utils.ColumnReport,
                 com.debisys.utils.NumberUtil, com.debisys.schedulereports.ScheduleReport" %>
                 
<%@page import="com.debisys.utils.TimeZone"%>
<%
	int section = 4;
	int section_page = 19;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
SummaryReport summaryReport = new SummaryReport();
ArrayList<String> titles = new ArrayList<String>();
Vector vTimeZoneData = null;

if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
{
        vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
}
else
{
        vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
}

Vector warningSearchResults = new  Vector();
Vector vecCurrent = new Vector();
//Vector vecPast = new Vector();
Hashtable searchErrors = null;
String message = ""; 
TransactionReport.setWarningmessage(new Vector<String>());
	
	
String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
String keyLanguage = "jsp.admin.reports.title34";

//String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
//String testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());
  	
  	
  	//TrxSummPhonePojo trxSummPhonePojoResult = new TrxSummPhonePojo();
	if (request.getParameter("search") != null)
	{
            if (TransactionReport.validateDateRange(SessionData) && request.getParameter("phoneNumber") != null && !(request.getParameter("phoneNumber")).equals(""))
            {	  		
                if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
                {			
                        //TO SCHEDULE REPORT
                        titles.add(noteTimeZone);
                        titles.add( Languages.getString(keyLanguage,SessionData.getLanguage()) );
                        TransactionReport.getPhoneSummary(SessionData,application, DebisysConstants.SCHEDULE_REPORT, titles);
                        ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
                        if (  scheduleReport != null  )
                        {
                                scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
                                scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
                                scheduleReport.setTitleName( keyLanguage );
                                scheduleReport.setAdditionalData( TransactionReport.getPhoneNumber() );    
                        }	
                        response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
                }
                else if ( request.getParameter("downloadReport") != null )
                {
                        //TO DOWNLOAD ZIP REPORT
                        TransactionReport.getPhoneSummary(SessionData,application, DebisysConstants.DOWNLOAD_REPORT, titles);
                        if ( TransactionReport.getStrUrlLocation() != null )
                                response.sendRedirect( TransactionReport.getStrUrlLocation() );	
                }	
                else
                {	
                   //pull last months
                   TransactionReport.setStartDate(request.getParameter("startDate"));
                   TransactionReport.setEndDate(request.getParameter("endDate"));
                   SessionData.setProperty("start_date", request.getParameter("startDate"));
                   SessionData.setProperty("end_date", request.getParameter("endDate"));
                   summaryReport = TransactionReport.getPhoneSummary(SessionData,application, DebisysConstants.EXECUTE_REPORT, titles);			   
                } 			     			    
            }
	    else
	    {
                if(!TransactionReport.validateDateRange(SessionData))
                {
                    searchErrors = TransactionReport.getErrors();
                }
                if(request.getParameter("phoneNumber") == null || (request.getParameter("phoneNumber")).equals(""))
                {
                    if (searchErrors == null)
                    {
                            searchErrors = new Hashtable();
                    }
                    searchErrors.put("zphoneNumber",Languages.getString("com.debisys.reports.error6",SessionData.getLanguage()));	
                }
	    }
	}
%>
<%@ include file="/includes/header.jsp" %>
<%
	boolean bool_custom_config_mexico = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
	boolean bool_custom_config_default = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	
	boolean bool_deploy_type_intl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
	boolean bool_perm_enable_tax_calc = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
%>

  
<table border="0" cellpadding="0" cellspacing="0" width="500" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title34",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
												<table width=400>
<%
    if (searchErrors != null)
    {
        out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
        Enumeration enum1 = searchErrors.keys();

        while(enum1.hasMoreElements())
        {
            String strKey = enum1.nextElement().toString();
            String strError = (String) searchErrors.get(strKey);
            out.println("<li>" + strError);
        }

        out.println("</font></td></tr>");
    }
%>
												</table>
               </td>
              </tr>
            </table>
<%
    if ( summaryReport.getRecords().size() > 0 )
    {
%>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr class="main"><td nowrap colspan="4"><%=noteTimeZone %><br/><br/></td></tr>
<%     
            String taxtype ="";
            if(bool_perm_enable_tax_calc &&  bool_deploy_type_intl && bool_custom_config_default) 
            {   
                // The default tax was used in the calculation
                if( TransactionReport.getWarningMessage().size()>0)
                        taxtype = TransactionReport.getWarningMessage().get(0);

                TransactionReport.setWarningmessage(new Vector<String>());
                warningSearchResults = TransactionReport.getWarningMessage(SessionData, SessionData.getProperty("end_date"), SessionData.getProperty("start_date")); 
            }
            boolean showWarnings = false;
            if (showWarnings && warningSearchResults.size() > 0) 
            {
                message = message + Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage()) + "<br />";
                message = message + Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage()) + "<br />";

                for (int i = 0; i < warningSearchResults.size(); i++){  
                    message = message + warningSearchResults.get(i).toString() + "<br />";
                }
%>
                <tr class="errorText">
                        <td colspan="4"><%= message %></td>
                </tr>					
<%
            } 
%>            							
                <tr class="errorText">
                        <td colspan="4"><%= taxtype %></td>
                </tr>
                <tr class="errorText">
                    <td colspan="4">
                        <form target="blank" method=post action="admin/reports/transactions/transaction_summary_by_phone.jsp">
                            <input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
                            <input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
                            <input type=hidden name="phoneNumber" value="<%=TransactionReport.getPhoneNumber()%>">
                            <input type="hidden" name="downloadReport" value="y">
                            <input type="hidden" name="search" value="y">		                    
                            <input id=btnSubmit type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                        </form>
                    </td>
                </tr>										
                <tr>
                    <td colspan="4">
                        <table width="100%" cellspacing="1" cellpadding="2">
            <%
                int rowClass = 1;
                for( SummaryReportPojo summaryReportPojo : summaryReport.getRecords())
                {
                       String description = summaryReportPojo.getDescription();
                       String current     = summaryReportPojo.getCurrentPeriod();
                       String previous    = summaryReportPojo.getPreviuosPeriod();
                       String change      = summaryReportPojo.getChange();
                       if ( summaryReportPojo.getRecordType().equals("T"))
                       {
            %>
                            <tr>
                                   <td class=rowhead2 colspan="4" valign=bottom><%=description%></td>
                            </tr>            
            <%
                        }
                        else if ( summaryReportPojo.getRecordType().equals("R") || summaryReportPojo.getRecordType().equals("P") )
                        {
            %>
                            <tr class=row<%=rowClass%> >
                                   <td valign=bottom><%=description%></td>
                            <%			                	
                            if ( summaryReportPojo.getRecordType().equals("R") )
                            {
                            %>     
                                    <td valign=bottom><%=current%></td>
                                    <td valign=bottom><%=previous%></td>
                            <%}
                            else
                            {%>
            <td align=bottom nowrap>
                <a href="admin/reports/transactions/transaction_detail_by_phone.jsp?startDate=<%= URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") %>&endDate=<%= URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") %>&search=y&phoneNumber=<%= URLEncoder.encode(TransactionReport.getPhoneNumber(), "UTF-8") %>"  target="_blank" > 
                   <%= current %> </a>
            </td>
            <td align=bottom nowrap>
                    <a href="admin/reports/transactions/transaction_detail_by_phone.jsp?startDate=<%= URLEncoder.encode(DateUtil.addSubtractMonths(TransactionReport.getStartDate(),-1), "UTF-8") %>&endDate=<%= URLEncoder.encode(DateUtil.addSubtractMonths(TransactionReport.getEndDate(),-1), "UTF-8") %>&search=y&phoneNumber=<%= URLEncoder.encode(TransactionReport.getPhoneNumber(), "UTF-8") %>"  target="_blank" >
                    <%= previous %></a>
            </td>
                            <%}%>                 
            <td valign=bottom><%=change%></td>
            </tr>            
                    <%
                        if ( rowClass == 1 )
                          rowClass = 2;
                        else  
                          rowClass = 1;
                        }  
                        else if ( summaryReportPojo.getRecordType().equals("C"))
                        {
                        %>
            <tr>
                   <td class=rowhead2 valign=bottom><%=description%></td>
                   <td class=rowhead2 valign=bottom><%=current%></td>
                   <td class=rowhead2 valign=bottom><%=previous%></td>
                   <td class=rowhead2 valign=bottom><%=change%></td>
            </tr>            
            <%            		
                         }
            }
            %>        
                            </table>  	
      	    		</td>
       	   	   </tr>      	 
      		
 <% }
	else if (vecCurrent.size()==0 && request.getParameter("search") != null && searchErrors == null)
	{
 		out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
	}
%>
          </td>
      </tr>
    </table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>