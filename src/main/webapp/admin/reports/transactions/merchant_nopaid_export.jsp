<%@ page import="java.util.*" %>
<%
int section=3;
int section_page=4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>

<%
   Vector vecSearchResults = new Vector();
   String strRuta="";
   String ids = request.getParameter("ids");
   vecSearchResults = TransactionReport.getMerchantNoPaid(SessionData, application, ids, DebisysConstants.DOWNLOAD_REPORT, null, null);  
   strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,2,SessionData);
   if ( strRuta.length() > 0)
   {
     response.sendRedirect(strRuta);    
   }
%>
