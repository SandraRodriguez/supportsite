<%@ page import="java.util.*,
         java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    String strReport = request.getParameter("report");
    int section = 4;
    int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%    Vector vecSearchResults = new Vector();
    Hashtable searchErrors = null;
    int intRecordCount = 0;
    int intPage = 1;
    int intPageSize = 50;
    int intPageCount = 1;

    int DATE_TIME = 1;
    int MILLENNIUM_NO = 2;
    int TYPE = 3;
    int HOST_ID = 4;
    int PORT = 5;
    int DURATION = 6;
    int INFO1 = 7;
    int RESULT_CODE = 8;
    int TRANSACTION_ID = 9;
    int DBA = 10;
    int MOBILE = 11;
    int INVOICE_NUMBER = 12;
    int PROVIDER_RESPONSE = 13;

    int ASC = 1;
    int DESC = 2;

    String products = "";

    boolean showAccountIdQRCode = false;
    showAccountIdQRCode = SessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);

    if (request.getParameter("Download") != null) {
        String sURL = TransactionReport.downloadMerchantErrorDetail(application, SessionData);
        response.sendRedirect(sURL);
        return;
    }

    if (request.getParameter("page") != null) {
        try {
            intPage = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException ex) {
            intPage = 1;
        }
    }

    if (intPage < 1) {
        intPage = 1;
    }

    if (TransactionReport.validateDateRange(SessionData)) {
        SessionData.setProperty("start_date", request.getParameter("startDate"));
        SessionData.setProperty("end_date", request.getParameter("endDate"));

        products = request.getParameter("products");
        if (products != null && !products.trim().equals("")) {
            TransactionReport.setProductIds(products);
        }

        vecSearchResults = TransactionReport.getMerchantErrorDetail(intPage, intPageSize, SessionData, true);
        intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
        vecSearchResults.removeElementAt(0);
        if (intRecordCount > 0) {
            intPageCount = (intRecordCount / intPageSize);
            if ((intPageCount * intPageSize) < intRecordCount) {
                intPageCount++;
            }
        }
    }
    else {
        searchErrors = TransactionReport.getErrors();
    }
%>
<%@ include file="/includes/header.jsp" %>


<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
    <tr>
        <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td class="formAreaTitle" align=left width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.transaction_error_details.title", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF">
            <%
                if (vecSearchResults != null && vecSearchResults.size() > 0) {
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <%
                    Vector vTimeZoneData = null;
                    if (SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT)) {
                        vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
                    }
                    else {
                        vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
                    }
                %>
                <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
                <tr><td class="main"><%=intRecordCount%> result(s) found<%
                    if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                        out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                    }
                        %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{Integer.toString(intPage), Integer.toString(intPageCount)}, SessionData.getLanguage())%>
                        <br><%=Languages.getString("jsp.admin.reports.click_to_sort", SessionData.getLanguage())%>
                        <table>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <FORM ACTION="admin/reports/transactions/transaction_error_details.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                                        <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                                        <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                                        <INPUT TYPE=hidden NAME=merchantId VALUE="<%=request.getParameter("merchantId")%>">
                                        <INPUT TYPE=hidden NAME=resultCodes VALUE="<%=com.debisys.utils.StringUtil.toString(request.getParameter("resultCodes"))%>">
                                        <INPUT TYPE=hidden NAME=Download VALUE="Y">
                                        <%
                            if (products != null && !products.trim().equals("")) {%>
                                        <input type=hidden name="products" value="<%=TransactionReport.getProductIds()%>">
                                        <%}
                                        %>
                                        <INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download", SessionData.getLanguage())%>">
                                    </FORM>
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <FORM ACTION="admin/reports/transactions/print_transaction_error_details.jsp" METHOD=post TARGET=blank>
                                        <INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
                                        <INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
                                        <INPUT TYPE=hidden NAME=merchantId VALUE="<%=request.getParameter("merchantId")%>">
                                        <INPUT TYPE=hidden NAME=page VALUE="<%=com.debisys.utils.StringUtil.toString(request.getParameter("page"))%>">
                                        <INPUT TYPE=hidden NAME=resultCodes VALUE="<%=com.debisys.utils.StringUtil.toString(request.getParameter("resultCodes"))%>">
                                        <%
                            if (products != null && !products.trim().equals("")) {%>
                                        <input type=hidden name="products" value="<%=TransactionReport.getProductIds()%>">
                                        <%}
                                        %>
                                        <INPUT ID=btnPrint TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.print", SessionData.getLanguage())%>">
                                    </FORM>
                                </td>
                            </tr>
                        </table>
                    </td></tr>
                <tr>
                    <td align=right class="main" nowrap>
                        <%
                            if (intPage > 1) {
                                out.println("<a href=\"admin/reports/transactions/transaction_error_details.jsp?merchantId=" + TransactionReport.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&products=" + TransactionReport.getProductIds() + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=1\">First</a>&nbsp;");
                                out.println("<a href=\"admin/reports/transactions/transaction_error_details.jsp?merchantId=" + TransactionReport.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&products=" + TransactionReport.getProductIds() + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPage - 1) + "\">&lt;&lt;Prev</a>&nbsp;");
                            }

                            int intLowerLimit = intPage - 12;
                            int intUpperLimit = intPage + 12;

                            if (intLowerLimit < 1) {
                                intLowerLimit = 1;
                                intUpperLimit = 25;
                            }

                            for (int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) {
                                if (i == intPage) {
                                    out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                                }
                                else {
                                    out.println("<a href=\"admin/reports/transactions/transaction_error_details.jsp?merchantId=" + TransactionReport.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&products=" + TransactionReport.getProductIds() + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + i + "\">" + i + "</a>&nbsp;");
                                }
                            }

                            if (intPage <= (intPageCount - 1)) {
                                out.println("<a href=\"admin/reports/transactions/transaction_error_details.jsp?merchantId=" + TransactionReport.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&products=" + TransactionReport.getProductIds() + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPage + 1) + "\">Next&gt;&gt;</a>&nbsp;");
                                out.println("<a href=\"admin/reports/transactions/transaction_error_details.jsp?merchantId=" + TransactionReport.getMerchantId() + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&products=" + TransactionReport.getProductIds() + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&col=" + URLEncoder.encode(TransactionReport.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(TransactionReport.getSort(), "UTF-8") + "&page=" + (intPageCount) + "\">Last</a>");
                            }

                        %>
                    </td>
                </tr>

            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
                <thead>
                    <tr>
                        <td class=rowhead2>#</td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=DATE_TIME%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=DATE_TIME%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_id", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=TRANSACTION_ID%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=TRANSACTION_ID%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.amount", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.dba", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=DBA%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=DBA%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk_id", SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.product_id", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=INFO1%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=INFO1%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.phone", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=MOBILE%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=MOBILE%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.term_no", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=MILLENNIUM_NO%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=MILLENNIUM_NO%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.type", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=TYPE%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=TYPE%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.host", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=HOST_ID%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=HOST_ID%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.port", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=PORT%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=PORT%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.reports.duration", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=DURATION%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=DURATION%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.result", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=RESULT_CODE%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=RESULT_CODE%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>              
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.provider_response", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=PROVIDER_RESPONSE%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=PROVIDER_RESPONSE%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                                <%   if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {%>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.invoiceno", SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=INVOICE_NUMBER%>&sort=<%=ASC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/reports/transactions/transaction_error_details.jsp?page=<%=intPage%>&col=<%=INVOICE_NUMBER%>&sort=<%=DESC%>&merchantId=<%=TransactionReport.getMerchantId()%>&startDate=<%=URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8")%>&endDate=<%=URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")%>"><img src="images/up.png" height=11 width=11 border=0></a></td>
                                <%} %>

                        <%if (showAccountIdQRCode) {%>
                         <!-- <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.reference", SessionData.getLanguage()).toUpperCase()%></td>-->
                        <%}%>


                </thead>
                <%
                    int intCounter = 1;
                    Iterator it = vecSearchResults.iterator();
                    int intEvenOdd = 1;
                    while (it.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        out.println("<tr class=row" + intEvenOdd + ">"
                                + "<td nowrap>" + intCounter++ + "</td>"
                                + // Date modified for localization - SW 
                                "<td nowrap>" + vecTemp.get(0) + "</td>"
                                + "<td nowrap>" + vecTemp.get(1) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(2) + "</td>"
                                + "<td nowrap>" + vecTemp.get(3) + "</td>" +//DBA
                                "<td nowrap align=\"right\">" + vecTemp.get(4) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(5) + "</td>"
                                + "<td nowrap>" + vecTemp.get(6) + "</td>" +//MOBILE
                                "<td nowrap align=\"right\">" + vecTemp.get(7) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(8) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(9) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(11) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(12) + "</td>"
                                + "<td nowrap align=\"right\">" + vecTemp.get(13) + "</td>");
                        //INVOICE NUMBER DBSY919
                        if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
                                && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                            out.println("<td nowrap>" + vecTemp.get(14) + "</td>");
                        }
                        //if (vecTemp.size() >= 16 && showAccountIdQRCode) {
                          //  out.println("<td>" + vecTemp.get(15) + "</td>");
                        //}
                        //else if (showAccountIdQRCode) {
                          //  out.println("<td></td>");
                        //}

                        out.println("</tr>");
                        if (intEvenOdd == 1) {
                            intEvenOdd = 2;
                        }
                        else {
                            intEvenOdd = 1;
                        }

                    }
                    vecSearchResults.clear();
                %>
            </table>

            <%
                }
                else {
                    out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                }
            %>
        </td>
    </tr>
</table>


<%@ include file="/includes/footer.jsp" %>