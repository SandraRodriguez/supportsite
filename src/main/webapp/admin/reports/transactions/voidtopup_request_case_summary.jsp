<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.utils.NumberUtil" %>
<%
    int section=4;
    int section_page=25;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
  <%@ include file="/includes/header.jsp" %>

  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%= Languages.getString("jsp.admin.reports.voidtopup_request_report",SessionData.getLanguage()).toUpperCase() %></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr class="main">
                    <td>
                        <br><font color="#ff0000"><%=Languages.getString("jsp.admin.reports.voidtopup_request_report.case",SessionData.getLanguage())%>
                        <%
                           out.println(": " + request.getParameter("pincase"));
                        %> 
                        </font>
                    </td>
                </tr>
                <tr class="main">
                        <td nowrap><%=Languages.getString("jsp.admin.reports.voidtopup_request_report.reason",SessionData.getLanguage())%>
                            <%
                                out.println(": " + request.getParameter("reason"));
                            %>
                        </td>                    
                </tr>
                <tr class="main">
                        <td nowrap><%=Languages.getString("jsp.admin.reports.voidtopup_request_report.status",SessionData.getLanguage())%>
                            <%
                                out.println(": " + request.getParameter("status"));
                            %>
                        </td>                    
                </tr>   
                <tr class="main">
                        <td nowrap><%=Languages.getString("jsp.admin.reports.voidtopup_request_report.comments",SessionData.getLanguage())%>
<%
    Vector vTemp = TransactionReport.getPINReturnRequestCommentsById(Long.parseLong(request.getParameter("pincase")));
    if ( vTemp.size() > 0 )
    {
%>
                            <table>
<%
    }
    for ( int i = 0; i < vTemp.size(); i++ )
    {
%>
                                <tr class="main"><td><%=((Vector)vTemp.get(i)).get(1)%></td></tr>
<%
    }
    if ( vTemp.size() > 0 )
    {
%>
                            </table>
<%
    }
%>
                        </td>                    
                </tr>              
            </table>        
        </td>
    </tr>
</table>
  <%@ include file="/includes/footer.jsp" %>
