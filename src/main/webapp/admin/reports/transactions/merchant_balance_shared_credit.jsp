<%@page import="com.debisys.languages.Languages"%>
<%@page import="com.debisys.reports.schedule.MerchantAvailableBalanceReport"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="MerchantAvailableBalanceReport" class="com.debisys.reports.schedule.MerchantAvailableBalanceReport" scope="request"/>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<% 
	MerchantAvailableBalanceReport report = new MerchantAvailableBalanceReport();
	String sharedBalance = request.getParameter("sharedBalance");
	boolean isSharedBalance = Boolean.parseBoolean(sharedBalance);
	report.setSharedBalance(isSharedBalance);
	report.setStrRefId(SessionData.getProperty("ref_id"));
	ArrayList<HashMap<String, String>> merchantIdList = new ArrayList<HashMap<String, String>>();
	if(isSharedBalance)
		merchantIdList = report.getRepsByIso();
	else
		merchantIdList = report.getMerchantsByIso();
%>

<select size="10" class="plain" multiple="multiple" name="merchantIdTxt" id="merchantIdLst" disabled="disabled">
	<option value="ALL" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
	<%
	    for(HashMap<String, String> merchant : merchantIdList){
	    	if(isSharedBalance){
	    		out.println("<option value=" + merchant.get("rep_id") +">" + merchant.get("rep_id") + " - " + merchant.get("businessname") + "</option>");
	    	} else {
	      		out.println("<option value=" + merchant.get("merchant_id") +">" + merchant.get("merchant_id") + " - " + merchant.get("dba") + "</option>");
	    	}
	    }
	%>
</select>