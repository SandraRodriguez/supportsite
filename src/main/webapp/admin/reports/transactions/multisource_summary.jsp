<%@page import="com.debisys.reports.GeneralReports"%>
<%@page import="com.debisys.reports.pojo.MultiSurcePojo"%>
<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.reports.pojo.ProviderPojo,
                 com.debisys.reports.pojo.ProductPojo" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
int section=4;
int section_page=60;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="MultiSource" class="com.debisys.reports.multisource.MultiSource" scope="request"/>
<jsp:setProperty name="MultiSource" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<%
String path = request.getContextPath(); 
%>
<style type="text/css" title="currentStyle">
    @import "<%=path%>/includes/media/demo_table.css";
</style>
  
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>

<%
    String labelProcessing      = Languages.getString("jsp.tools.datatable.processing",SessionData.getLanguage());
    String labelShowRecords     = Languages.getString("jsp.tools.datatable.showRecords",SessionData.getLanguage());
    String labelNoRecords       = Languages.getString("jsp.tools.datatable.norecords",SessionData.getLanguage());
    String labelNoDataAvailable = Languages.getString("jsp.tools.datatable.noDataAvailable",SessionData.getLanguage());
    String labelInfo            = Languages.getString("jsp.tools.datatable.info",SessionData.getLanguage());
    String labelInfoEmpty       = Languages.getString("jsp.tools.datatable.infoEmpty",SessionData.getLanguage());
    String labelFilter          = Languages.getString("jsp.tools.datatable.filter",SessionData.getLanguage());
    String labelSearch          = Languages.getString("jsp.tools.datatable.search",SessionData.getLanguage());
    String labelLoading         = Languages.getString("jsp.tools.datatable.loading",SessionData.getLanguage());
    String labelFirst           = Languages.getString("jsp.tools.datatable.first",SessionData.getLanguage());
    String labelLast            = Languages.getString("jsp.tools.datatable.last",SessionData.getLanguage());
    String labelNext            = Languages.getString("jsp.tools.datatable.next",SessionData.getLanguage());
    String labelPrevious        = Languages.getString("jsp.tools.datatable.previous",SessionData.getLanguage());
    String labelShow            = Languages.getString("jsp.tools.datatable.show",SessionData.getLanguage());
    String labelRecords         = Languages.getString("jsp.tools.datatable.records",SessionData.getLanguage());
    
%>

<script type="text/javascript" charset="utf-8">    
    $(document).ready(function() 
        {	              
            reloadTableTerminals();                                
        }
    );
</script>

<script type="text/javascript" charset="utf-8">	
    function reloadTableTerminals()
    {
        $('#multiSourceTable').dataTable( {
                "iDisplayLength": 30,
                "bLengthChange": true,
                "bFilter": true,
                "sPaginationType": "full_numbers",
                                 
                "oLanguage": {
                    "sProcessing":      "<%=labelProcessing%>",
                    "sLengthMenu":     "<%=labelShowRecords%>",
                    "sZeroRecords":    "<%=labelNoRecords%>",
                    "sEmptyTable":     "<%=labelNoDataAvailable%>",
                    "sInfo":           "<%=labelInfo%>",
                    "sInfoEmpty":      "<%=labelInfoEmpty%>",
                    "sInfoFiltered":   "<%=labelFilter%>",
                    "sInfoPostFix":    "",
                    "sSearch":         "<%=labelSearch%>",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "<%=labelLoading%>",
                    "oPaginate": {
                        "sFirst":    "<%=labelFirst%>",
                        "sLast":     "<%=labelLast%>",
                        "sNext":     "<%=labelNext%>",
                        "sPrevious": "<%=labelPrevious%>"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "sLengthMenu": ' <%=labelShow%> <select id="pagesNumbers" >'+
                                                '<option value="10">10</option>'+
                                                '<option value="20">20</option>'+
                                                '<option value="30">30</option>'+
                                                '<option value="40">40</option>'+
                                                '<option value="50">50</option>'+
                                                '<option value="-1">All</option>'+
                                                '</select> <%=labelRecords%>'
                 },
             "aaSorting": []     
        }				
        );
    }
</script>
                                                
<%
    
    String[] providerValues = null;
    String[] productValues = null;
    String strRefId = SessionData.getProperty("ref_id");
    ArrayList<MultiSurcePojo> multiSource = null;
    String startDate       = request.getParameter("startDate");
    String endDate         = request.getParameter("endDate");
    String trxId           = request.getParameter("trxId");
    String typeMultiSource = request.getParameter("typeMultiSource");
    String keyMainTitle     = "jsp.tools.dtu2536.report.main";
     
    Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
    String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
    
    String titleReport = Languages.getString(keyMainTitle,SessionData.getLanguage()) + " "+
                         Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +
                         HTMLEncoder.encode( startDate );
    
    titleReport = titleReport + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( endDate );
                
    if ( request.getMethod().equals("POST"))
    {
        SessionData.setProperty("start_date",startDate);
        SessionData.setProperty("end_date",endDate);
        providerValues = request.getParameterValues("providersCombo");
        productValues  = request.getParameterValues("productsCombo");
               
        multiSource = com.debisys.reports.multisource.MultiSource.findTrxMultiSource(strDistChainType,
                                                                                    strAccessLevel, 
                                                                                    strRefId,
                                                                                    providerValues,
                                                                                    productValues,
                                                                                    startDate,
                                                                                    endDate, 
                                                                                    trxId,
                                                                                    typeMultiSource, 
                                                                                    SessionData.getLanguage());
        
        if ( request.getParameter("download") != null)
        {
            GeneralReports generalReports = new GeneralReports();
            request.setAttribute("multiSourceTransactionsReport",multiSource);
            String sURL = generalReports.downloadMultiSourceTrxs(application, SessionData, request);
            response.sendRedirect(sURL);
            return;
        }
            
    }
    
    String mainTitle        = Languages.getString(keyMainTitle,SessionData.getLanguage()).toUpperCase(); 
    String labelDate        = Languages.getString("jsp.tools.dtu2536.report.titles.Date",SessionData.getLanguage());
    String labeltrxNo       = Languages.getString("jsp.tools.dtu2536.report.titles.trxNo",SessionData.getLanguage());
    String labelrelatedTrx  = Languages.getString("jsp.tools.dtu2536.report.titles.relatedTrx",SessionData.getLanguage());
    String labelprovider    = Languages.getString("jsp.tools.dtu2536.report.titles.provider",SessionData.getLanguage());
    String labelsku         = Languages.getString("jsp.tools.dtu2536.report.titles.sku",SessionData.getLanguage());
    String labelamt         = Languages.getString("jsp.tools.dtu2536.report.titles.amt",SessionData.getLanguage());
    String labeltype        = Languages.getString("jsp.tools.dtu2536.report.titles.type",SessionData.getLanguage());
    String labelmultiSource = Languages.getString("jsp.tools.dtu2536.report.titles.multiSource",SessionData.getLanguage());
    String labelDownload    = Languages.getString("jsp.tools.dtu2536.download",SessionData.getLanguage());
     
    String labelTypeAll         = Languages.getString("jsp.tools.dtu2536.report.type.all",SessionData.getLanguage());
    String labelTypeMultiSource = Languages.getString("jsp.tools.dtu2536.report.type.multisource",SessionData.getLanguage());
    String labelTypeDirect      = Languages.getString("jsp.tools.dtu2536.report.type.direct",SessionData.getLanguage());
    
    String labelBack            = Languages.getString("jsp.tools.dtu2536.back",SessionData.getLanguage());
    
    String typeReport = "";
    if ( typeMultiSource.equals("0") )
    {
        typeReport = labelTypeAll;
    }
    else if ( typeMultiSource.equals("1") )
    {
        typeReport = labelTypeMultiSource;
    }
    else 
    {
        typeReport = labelTypeDirect;
    }
%>

    <table border="0" cellpadding="0" cellspacing="0" width="110%" background="images/top_blue.gif">
    
    <tr>
        <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td class="formAreaTitle" align=left width="1000">&nbsp;<%=mainTitle%></td>
        <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr bgcolor="#FFFFFF">
        <td width="18" height="20" align=left></td>
        <td class="formAreaTitle" align=left width="1000">&nbsp;<a href="<%=path%>/admin/reports/transactions/multisource.jsp?reportType=1" ><%=labelBack%></a> </td>
        <td width="12" height="20" align=right></td>
    </tr>
    <tr>
    <td colspan="3">
            <table width="100%" border="0" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0">
                <tr class="main"><td nowrap colspan="2"><%= noteTimeZone %><br/><br/></td></tr>
                <tr class="main"><td nowrap colspan="2"><%= typeReport %><br/><br/></td></tr>
                <tr class="main"><td nowrap colspan="2"><%= titleReport %><br/><br/></td></tr>
                <tr>
                    <td colspan="3" align="left" width="1000">
                        <form name="mainform" id="mainform" action="admin/reports/transactions/multisource_summary.jsp" method="post" >
                            <input type="hidden" id="startDate" name="startDate" value="<%=startDate%>" />
                            <input type="hidden" id="endDate" name="endDate" value="<%=endDate%>" />
                            <input type="hidden" id="trxId" name="trxId" value="<%=trxId%>" />
                            <input type="hidden" id="typeMultiSource" name="typeMultiSource" value="<%=typeMultiSource%>" />
                            <input type="hidden" id="providerValues" name="providerValues" value="<%=providerValues%>" />
                            <input type="hidden" id="productValues" name="productValues" value="<%=productValues%>" />
                            
                            <input type="submit" id="download" name="download" value="<%=labelDownload%>" />        
                        </form>
                        
                    </td>                      
                </tr>
                <tr class="formArea2">
                    <td align=center colspan="3" class="main">
                        <table width="100%" cellpadding="0"  cellspacing="0" border="0" class="display" id="multiSourceTable"> 
                            <thead>
                                <tr class="rowhead2">
                                    <th>#</th>  
                                    <th><%=labelDate%></th>               
                                    <th><%=labeltrxNo%></th>               
                                    <th><%=labelrelatedTrx%></th>               
                                    <th><%=labelprovider%></th>               
                                    <th><%=labelsku%></th>               
                                    <th><%=labelamt%></th>                                       
                                    <th><%=labeltype%></th>                                       
                                    <th><%=labelmultiSource%></th>                                       
                                </tr>
                            </thead>
                            <tbody>
                             <%
                             for( MultiSurcePojo multisourceRow : multiSource )
                             { 
                             %>
                                <tr>
                                    <td><%=multisourceRow.getRow()%></td>               
                                    <td><%=multisourceRow.getDateTime()%></td>               
                                    <td><%=multisourceRow.getTrxId()%></td>               
                                    <td><%=multisourceRow.getTrxIdRelated()%></td>               
                                    <td><%=multisourceRow.getProduct().getProvider().getDescripton()%>(<%=multisourceRow.getProduct().getProvider().getId()%>)</td>
                                    <td><%=multisourceRow.getProduct().getDescripton()%></td>               
                                    <td><%=multisourceRow.getAmount()%></td>                                                    
                                    <td><%=multisourceRow.getType()%></td>
                                    <td><%=multisourceRow.getMultiSource()%> </td>                                                    
                                </tr>
                            <%
                             }     
                            %>  
                            </tbody>
                        </table>
                    </td>	 
                </tr>
            </table> 
     </td>
    </tr>
     
    
  
  </table>

<%@ include file="/includes/footer.jsp" %>