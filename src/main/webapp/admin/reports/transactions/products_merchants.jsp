<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport" %>
<%
int section=4;
int section_page=49;
%>
 
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT SRC="includes/products_merchants.js" type="text/javascript"></SCRIPT>
<script language="javascript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}
</script>
<%
String strRefId = SessionData.getProperty("ref_id");
 %>
<table border="0" cellpadding="0" cellspacing="0" width="900">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title46",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td><form name="mainform" id="mainform" method="post" action="admin/reports/transactions/products_merchants_summary.jsp" onSubmit="return check();">
	    <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
              <tr class="main">
               <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
		         <%}%><td>&nbsp;</td>
               </tr>
<tr class="main" >
    <td nowrap><div width="15" ><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</div><input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<tr class="main">
    <td nowrap ><div width="15" ><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:</div><input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
               <tr>
               <td valign="top" nowrap>
               
<table width="900"  >

<%
 if ( strAccessLevel.equals(DebisysConstants.ISO) )
 {
%>
    <div class="float" valign="top" align="left"  nowrap width="30" >
    <%if(!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){ %>
    <%=Languages.getString("jsp.admin.reports.transactions.agents.option",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br/>
    
    	
        <select name="selectagentslist" id='selectagentslist' size="10" multiple='multiple' onchange="showSub(<%=strRefId%>,'agent');">		
    	          
		<option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
							<%
							  Vector vecRepList = TransactionReport.getAgentList(SessionData);
							  Iterator ite = vecRepList.iterator();
							  while (ite.hasNext())
							  {
							    Vector vecTemp = null;
							    vecTemp = (Vector) ite.next();
							    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							  }
							  %>
        </select>
        
        </div>
       
        
    <div class="float" valign=top   align="left"  name="subagentslistbox" id='subagentslistbox' style="display:none;" nowrap>
        <%=Languages.getString("jsp.admin.reports.transactions.subagents.option",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
        <select name="subagentslist" id='subagentslist' size="10" multiple='multiple' style="display:none;" onchange="showSub(<%=strRefId%>,'subagent');">
        </select></div>
    <% }%>

    <%if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){ %>
    <div class="float" valign=top  align="left" name="repslistbox" id='repslistbox' nowrap> 
    <%=Languages.getString("jsp.admin.reports.Select_Reps",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
   
       <select name="repslist" id="repslist" size="10" multiple  onchange="showSub(<%=strRefId%>,'reps');">
       <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
							<%
							  Vector vecRepList = TransactionReport.getRepList(SessionData);
							  Iterator ite = vecRepList.iterator();
							  while (ite.hasNext())
							  {
							    Vector vecTemp = null;
							    vecTemp = (Vector) ite.next();
							    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							  }
							  %>
     <% }else{ %>
      <div class="float" valign=top  align="left" name="repslistbox" id='repslistbox' style="display:none;" nowrap> 
    <%=Languages.getString("jsp.admin.reports.Select_Reps",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
         <select name="repslist" id="repslist" size="10" multiple style="display:none;" onchange="showSub(<%=strRefId%>,'reps');">
    <% }%>	
       </select></div>

  <div class="float" VALIGN="top" align="left"  name="merchantslistbox" id='merchantslistbox' style="display:none;" nowrap>
  <%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <SELECT NAME="merchantslist" id="merchantslist" SIZE=10 multiple="multiple"  style="display:none;" >
    </SELECT></div>
<%

 }else if ( strAccessLevel.equals(DebisysConstants.AGENT) ){
%> 
  <div class="float" valign=top  align="left"  name="subagentslistbox" id='subagentslistbox'  nowrap>
        <%=Languages.getString("jsp.admin.reports.transactions.subagents.option",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
        <select name="subagentslist" id='subagentslist' size="10" multiple='multiple' onchange="showSub(<%=strRefId%>,'subagent');">
        <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
							<%
							  Vector vecRepList = TransactionReport.getSubAgentList(SessionData);
							  Iterator ite = vecRepList.iterator();
							  while (ite.hasNext())
							  {
							    Vector vecTemp = null;
							    vecTemp = (Vector) ite.next();
							    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							  }
							  %>
		</select></div>
    <div class="float"  align="left" valign=top name="repslistbox" id='repslistbox' style="display:none;" nowrap> 
    <%=Languages.getString("jsp.admin.reports.Select_Reps",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
         <select name="repslist" id="repslist" size="10" multiple style="display:none;" onchange="showSub(<%=strRefId%>,'reps');">
       </select></div>
  <div class="float" VALIGN="top" align="left" name="merchantslistbox" id='merchantslistbox' style="display:none;" nowrap>
  <%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <SELECT NAME="merchantslist" id="merchantslist" SIZE=10 multiple="multiple"  style="display:none;" >
    </SELECT></div>
<%
 }else if ( strAccessLevel.equals(DebisysConstants.SUBAGENT) ){
%>  
    <div class="float" align="left"  valign=top name="repslistbox" id='repslistbox' nowrap> 
    <%=Languages.getString("jsp.admin.reports.Select_Reps",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
         <select name="repslist" id="repslist" size="10" multiple onchange="showSub(<%=strRefId%>,'reps');">
       <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
							<%
							  Vector vecRepList = TransactionReport.getRepList(SessionData);
							  Iterator ite = vecRepList.iterator();
							  while (ite.hasNext())
							  {
							    Vector vecTemp = null;
							    vecTemp = (Vector) ite.next();
							    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							  }
							  %>
       </select></div>
  <div class="float" VALIGN="top" align="left"  name="merchantslistbox" id='merchantslistbox' style="display:none;" nowrap>
  <%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <SELECT NAME="merchantslist" id="merchantslist" SIZE=10 multiple="multiple"  style="display:none;" >
    </SELECT></div>
  <%
 }else if ( strAccessLevel.equals(DebisysConstants.REP) ){
%> 
    <div class="float" VALIGN="top" align="left"  name="merchantslistbox" id='merchantslistbox'  nowrap>
  <%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;<br>
    <SELECT NAME="merchantslist" id="merchantslist" SIZE=10 multiple="multiple">
                <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
  Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
  Iterator it = vecMerchantList.iterator();
  while (it.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
  }

%>
	</SELECT></div>

<%
 }
%>



<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.products.option",SessionData.getLanguage())%></td>
</tr>
<tr>   <td class=main valign=top colspan="4">
        <select name="pids" size="10" multiple>
          <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
  Vector vecProductList = TransactionReport.getProductList(SessionData, null);
  Iterator it = vecProductList.iterator();
  while (it.hasNext())
  {
    Vector vecTemp = null;
    vecTemp = (Vector) it.next();
    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "("+vecTemp.get(0)+")</option>");
  }

%>
        </select>
</td>
</tr>
</table>
               </td>
              </tr>
    <tr>
        <td class="main" align="center">
          <input type="hidden" name="search" value="y">
          <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
        </td>
         <jsp:include page="/admin/reports/schreportoption.jsp">
            <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
            <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
        </jsp:include>
    </tr>
<tr>
<td class="main" colspan="4">
* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
</td>
</tr>
<tr>
<td class="main" colspan="4">
*<%=Languages.getString("jsp.admin.reports.transactions.all.instructions",SessionData.getLanguage())%>
</td>
</tr>           
            </table>

          </td>
      </tr>
    </table>

</form>
</td>
</tr>     

</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>