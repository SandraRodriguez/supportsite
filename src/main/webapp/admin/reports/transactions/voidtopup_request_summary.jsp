<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":"
			+ request.getServerPort() + path + "/";
%>
<%@ page
	import="java.net.URLEncoder,com.debisys.utils.HTMLEncoder,java.util.*,com.debisys.customers.Merchant,com.debisys.utils.NumberUtil,
                 com.debisys.reports.schedule.VoidTopupRequestReport,
                 com.debisys.schedulereports.ScheduleReport,
                 com.debisys.utils.ColumnReport"%>
<%
	int section = 4;
	int section_page = 25;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>

<jsp:useBean id="VoidTopupRequestReport"  
	class="com.debisys.reports.schedule.VoidTopupRequestReport" 
	scope="request"/>
<%@ include file="/includes/security.jsp" %>
    
<%
		SessionData.setProperty("reportId", DebisysConstants.SC_VOID_TOPUP_REQUESTS);
		String refId =  request.getParameter("ref_id");
		String refId2 = SessionData.getProperty("ref_id");
		SessionData.setProperty("ref_id", SessionData.getUser().getRefId());
    	Vector<Vector<Object>> vecSearchResults = null;
    	Hashtable searchErrors = null;
    	String start_date = "";
    	String end_date = "";
    	String status = "";
    	String merchant = "";
    	String pincase = "";
       	boolean fetchReport = true;
       	VoidTopupRequestReport report = null;
    	boolean allStatusSelected = false;
    	boolean allMerchantsSelected = false;

    	allStatusSelected =  Boolean.valueOf(request.getParameter("allStatusSelected"));
    	allMerchantsSelected = Boolean.valueOf(request.getParameter("allMerchantsSelected"));
       	String reportTitleKey = "jsp.admin.reports.voidtopup_request_report";
       	String reportTitle = Languages.getString(reportTitleKey,SessionData.getLanguage()).toUpperCase();
       	
    	if (request.getParameter("search") != null)
        {
        	pincase = request.getParameter("CaseNumber");
        	if ((pincase != null) && (!pincase.equals("")))
        	{
        		start_date = null; 
        		end_date = null; 
        		status = null; 
        		merchant = null;  
        		allStatusSelected = false;
            	allMerchantsSelected = false;
        	}
        	else
        	{
        		String trReportStartDate = TransactionReport.getStartDate();
        		if((trReportStartDate == null) || ((trReportStartDate != null) && (trReportStartDate.trim().equals(""))))
        		{
        			trReportStartDate = request.getParameter("start_date");
        			TransactionReport.setStartDate(trReportStartDate);	
        		}
        		String trReportEndDate = TransactionReport.getEndDate();
        		if((trReportEndDate == null) || ((trReportEndDate != null) && (trReportEndDate.trim().equals(""))))
        		{
        			trReportEndDate = request.getParameter("end_date");
        			TransactionReport.setEndDate(trReportEndDate);	
        		}
        		SessionData.setProperty("start_date", trReportStartDate);
        		SessionData.setProperty("end_date", trReportEndDate);

        		
        		if (TransactionReport.validateDateRange(SessionData))
        		{
        			start_date = trReportStartDate;
        			end_date = trReportEndDate;
        			status = request.getParameter("statusList");
        	        if(status != null)
        	        {
        	        	if(status.equals(""))
        	        	{
        	        		// If all status are needed then no status string should be filled 
        	        		allStatusSelected = true;
        	        	}
        	        }
        		}
        		else
        		{
        			searchErrors = TransactionReport.getErrors();
        			fetchReport = false;
        		}
        	}
    	    	
    	    if (fetchReport) 
    	    {
    	    	String merchantTemp = "";
                        
                merchantTemp = request.getParameter("merchantList");
                if (merchantTemp != null)
                {
                    if ((merchantTemp.equals("")) && ((pincase == null) || ((pincase != null) && (pincase.equals("")))))
                    {                           
                        allMerchantsSelected = true;
                    }
                    else
                    {
                        merchant = merchantTemp;
                    }
                }
                else // case for merchant level!
                {
                	if((pincase == null) || ((pincase != null) && (pincase.equals(""))))
                	{
                		allMerchantsSelected = true;
                	}
                }
                
                report = new VoidTopupRequestReport(SessionData, application, pincase, merchant, status, start_date, end_date, false, allStatusSelected, allMerchantsSelected); 
                
    	if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
    	{
    		//TO SCHEDULE REPORT
    		report.setScheduling(true);
    		if(report.scheduleReport())
    		{
    			ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
    			if (  scheduleReport != null  )
    			{
    				scheduleReport.setStartDateFixedQuery( report.getStart_date() );
    				scheduleReport.setEndDateFixedQuery( report.getEnd_date() );
    				scheduleReport.setTitleName( reportTitleKey );   
    			}	
    			response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
    		}
    	}
    	else if ( request.getParameter("downloadReport") != null )
    	{
    		//TO DOWNLOAD ZIP REPORT
    		report.setScheduling(false);
    		String zippedFilePath = report.downloadReport();
    		if((zippedFilePath != null) &&(!zippedFilePath.trim().equals("")))
    		{
    			response.sendRedirect( zippedFilePath );
    		}
    	}
    	else
    	{
    		//TO SHOW REPORT
    		report.setScheduling(false);
    		vecSearchResults = report.getResults();
    	}
            }
        }// close if search
    %>

<%@ include file="/includes/header.jsp" %>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
        <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=reportTitle%></td>
        <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" class="formArea2">
<%
  	if (searchErrors != null)
  	{
  		out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
  		Enumeration enum1 = searchErrors.keys();
  		while (enum1.hasMoreElements())
  		{
  			String strKey = enum1.nextElement().toString();
  			String strError = (String) searchErrors.get(strKey);
  			out.println("<li>" + strError);
  		}
  		out.println("</font></td></tr></table>");
  	}
  	if (vecSearchResults != null && vecSearchResults.size() > 0)
  	{
%>
        	    
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
<%
		if ((start_date != null) && (end_date != null)) 
		{
%>
              <td class="main">
                <%=reportTitle%> 
                <BR>
                <%=DateUtil.formatDateTime(start_date)%> - <%=DateUtil.formatDateTime(end_date)%>
              </td>            
<%
		} 
%>
              <td class=main align=right valign=bottom>
                <%=Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage())%>
              </td>
            </tr>
            <tr>
              <td class="main">
              	<form name="downloadData" method=post action="admin/reports/transactions/voidtopup_request_summary.jsp">
					<input type="hidden" name="start_date" value="<%=start_date%>">
					<input type="hidden" name="end_date" value="<%=end_date%>">
					<input type="hidden" name="search" value="y">
					<input type="hidden" name="downloadReport" value="y">
					<input type="hidden" value="<%=status%>" name="statusList">
                    <input type="hidden" value="<%=merchant%>" name="merchantList"> 
	        	    <INPUT TYPE=hidden NAME=allStatusSelected VALUE="<%=allStatusSelected%>">
                	<INPUT TYPE=hidden NAME=allMerchantsSelected VALUE="<%=allMerchantsSelected%>">

<%
         if ((pincase != null) && (!pincase.equalsIgnoreCase("")))
         {
%>
                    <input type="hidden" value="<%=pincase%>" name="CaseNumber">
<%
       	 }
%>
              		<label>
              			<%=Languages.getString("jsp.admin.reports.transactions.voidtopup_request_report.downloadLabel",SessionData.getLanguage())%>
              		</label>
					<input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.voidtopup_request_report.download",SessionData.getLanguage())%>">
            	</form>
              </td>             
            </tr>          
            <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
                <thead>
                	<tr class="SectionTopBorder">
                    	<td class=rowhead2 align=center>#
                    	</td>
<% 
		// Set the column headers
		ArrayList<ColumnReport> reportColumns = report.getReportRecordHeaders();
		if((reportColumns != null) && (reportColumns.size() > 0))
		{
			for(int columnIndex = 0;columnIndex < reportColumns.size();columnIndex++)
			{
%>                    
						<td class=rowhead2 align=center><%=reportColumns.get(columnIndex).getLanguageDescription()%>&nbsp;
						</td>
<%
			}
		}
%>
					</tr>
				</thead>	
<%
		Iterator it = vecSearchResults.iterator();
		int intEvenOdd = 1;
		int count = 1;
		while (it.hasNext())
		{
			Vector vecTemp = null;
			vecTemp = (Vector) it.next();
			out.println("<tr class=row" + intEvenOdd + ">");
			out.println("<td>" + (count++) + "</td>");
			out.println("<td nowrap>"
							+ "<a href=\"admin/reports/transactions/voidtopup_request_case_summary.jsp?"
							+ "pincase=" + vecTemp.get(0) + "&reason=" + vecTemp.get(10)
							+ "&status=" + vecTemp.get(9) + "\">" + vecTemp.get(0) + "</a>"
							+ "</td>");
			out.println("<td>" + vecTemp.get(1) + "</td>");
			out.println("<td>" + vecTemp.get(2) + "</td>");
			out.println("<td>" + vecTemp.get(3) + "</td>");
			out.println("<td>" + vecTemp.get(4) + "</td>");
			out.println("<td>" + vecTemp.get(5) + "</td>");
			out.println("<td>" + vecTemp.get(6) + "</td>");
			out.println("<td>" + vecTemp.get(7) + "</td>");
			out.println("<td>" + vecTemp.get(8) + "</td>");
			out.println("<td>" + vecTemp.get(9) + "</td>");
			out.println("<td>" + vecTemp.get(10) + "</td>");
			out.println("<td>" + vecTemp.get(11) + "</td>");
			out.println("<td>" + vecTemp.get(12) + "</td>");
			out.println("</tr>");
									
			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}
		vecSearchResults.clear();
%>
                </tr>
            </table>
<%
   	}
    else
    {
     	if ((vecSearchResults == null) || (((vecSearchResults != null) && (vecSearchResults.size() == 0)) && (request.getParameter("search") != null) && (searchErrors == null)))
   		{
   			out.println("<br><br><font color=ff0000>"
   					+ Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
   		}
    }
%>
            <SCRIPT type="text/javascript">
	            <!--
					  var stT1 = new SortROC(document.getElementById("t1"),
					  		["None","Integer", "String", "String", "String", "String", "String", "String", "String", "String", "String", "String", "String", "String"],
					  		0,false,false);
	             -->            
            </SCRIPT>
        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>