<%@ page import="com.debisys.languages.Languages,
				 com.debisys.reports.TransactionReport,
				 com.debisys.utils.DateUtil,
				 com.debisys.utils.HTMLEncoder,
				 java.util.Vector" %>
<%
int section = 9;
int section_page = 1; 
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.reports.pinreturn_search.requestdetailtitle",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<script>
function openDetails(nCaseNumber)
{
  var sURL = "pinreturn_searchdetails.jsp?caseNumber=" + nCaseNumber;
  var sOptions = "left=" + (screen.width - (screen.width * 0.6))/2 + ",top=" + (screen.height - (screen.height * 0.7))/2 + ",width=" + (screen.width * 0.6) + ",height=" + (screen.height * 0.7) + ",location=no,menubar=no,resizable=yes";
  var w = window.open(sURL, "_blank", sOptions, true);
  w.focus();
}

function validateComments(txtComments)
{
  if ( txtComments.value.length > 512 )
  {
    txtComments.value = txtComments.value.substring(0, 512);
  }
  document.getElementById('btnSave').disabled = (txtComments.value.length == 0);
  if ( document.getElementById('btnRecycle') != null )
  {
    document.getElementById('btnRecycle').disabled = document.getElementById('btnSave').disabled;
  }
}

function DisableButtons(sStatus)
{
  document.getElementById('btnSave').disabled = true;
  if ( sStatus == "ISO_CREDITED_NOT_RECYCLED" )
  {
    document.getElementById('btnRecycle').disabled = true;
  }
}
</script>
<%
if ( request.getParameter("caseNumber") != null )
{//If a case number was specified
	Vector vResults = new Vector();	
	try
	{
		long lCaseNumber = Long.parseLong(request.getParameter("caseNumber"));

		if ( request.getParameter("txtComments") != null )
		{//If a comment should be added to this case
%>
<table cellspacing="1" cellpadding="1" border="0">
	<tr>
    	<td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.commentaddition",SessionData.getLanguage()).toUpperCase()%></b></td>
<%
			if ( TransactionReport.insertPINReturnRequestComment(lCaseNumber, request.getParameter("txtComments")) )
			{
%>
		<td class="row1"><%=Languages.getString("jsp.admin.reports.pinreturn_search.successful",SessionData.getLanguage())%></td>
<%
			}
			else
			{
%>
		<td class="row1"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failed",SessionData.getLanguage())%></td>
<%
			}
%>
	</tr>
<%
			if ( (request.getParameter("action") != null) && (request.getParameter("action").equals("void")) )
			{//If a void of previous credit was requested
%>
	<tr>
		<td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.pinactivation",SessionData.getLanguage()).toUpperCase()%></b></td>
<%
				if ( TransactionReport.activatePIN(Integer.parseInt(request.getParameter("millennium_no")), Integer.parseInt(request.getParameter("control_no")), Math.abs(Double.parseDouble(request.getParameter("amount"))), SessionData.getProperty("username")) )
				{
					TransactionReport.activatePIN(Integer.parseInt(request.getParameter("millennium_no")), Integer.parseInt(request.getParameter("control_no")), 13, 0, SessionData.getProperty("username"));
%>
		<td class="row2"><%=Languages.getString("jsp.admin.reports.pinreturn_search.successful",SessionData.getLanguage())%></td>
<%
				}
				else
				{
%>
		<td class="row2"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failed",SessionData.getLanguage())%></td>
<%
				}
%>
	</tr>
<%
				int nNotCreditedId = 0;
				//Retrieve ID for NotCredited status
				Vector vecStatusList = TransactionReport.getPINRequestStatus(true);
				for ( int i = 0; i < vecStatusList.size(); i++ )
				{
					Vector vTemp = (Vector)vecStatusList.get(i);
					if ( vTemp.get(2).toString().equals("ISO_NOTCREDITED") )
					{
						nNotCreditedId = Integer.parseInt(vTemp.get(0).toString());
						break;
					}
				}
%>
	<tr>
		<td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.statuschange",SessionData.getLanguage()).toUpperCase()%></b></td>
<%
				if ( TransactionReport.updatePINReturnRequest((int)lCaseNumber, nNotCreditedId) )
				{
%>
		<td class="row1"><%=Languages.getString("jsp.admin.reports.pinreturn_search.successful",SessionData.getLanguage())%></td>
<%
				}
				else
				{
%>
		<td class="row1"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failed",SessionData.getLanguage())%></td>
<%
				}
%>
	</tr>
<%
			}//End of if a void of previous credit was requested
			else if ( (request.getParameter("action") != null) && (request.getParameter("action").equals("recycle")) )
			{//If a recycle was requested
%>
	<tr>
		<td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.pinrecycle",SessionData.getLanguage()).toUpperCase()%></b></td>
<%
				if ( TransactionReport.doCreditRecycleForPINRequest(Integer.parseInt(request.getParameter("sku")), request.getParameter("pin"), false, true, request.getParameter("millennium_no")) )
				{
%>
		<td class="row2"><%=Languages.getString("jsp.admin.reports.pinreturn_search.successful",SessionData.getLanguage())%></td>
<%
				}
				else
				{
%>
		<td class="row2"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failed",SessionData.getLanguage())%></td>
<%
				}
%>
	</tr>
<%
				int nCreditedId = 0;
				//Retrieve ID for Credited status
				Vector vecStatusList = TransactionReport.getPINRequestStatus(true);
				for ( int i = 0; i < vecStatusList.size(); i++ )
				{
					Vector vTemp = (Vector)vecStatusList.get(i);
					if ( vTemp.get(2).toString().equals("ISO_CREDITED") )
					{
						nCreditedId = Integer.parseInt(vTemp.get(0).toString());
						break;
					}
				}
%>
	<tr>
		<td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.statuschange",SessionData.getLanguage()).toUpperCase()%></b></td>
<%
				if ( TransactionReport.updatePINReturnRequest((int)lCaseNumber, nCreditedId) )
				{
%>
		<td class="row1"><%=Languages.getString("jsp.admin.reports.pinreturn_search.successful",SessionData.getLanguage())%></td>
<%
				}
				else
				{
%>
		<td class="row1"><%=Languages.getString("jsp.admin.reports.pinreturn_search.failed",SessionData.getLanguage())%></td>
<%
				}
%>
	</tr>
<%
			}//End of if a void of previous credit was requested
		}//End of if a comment should be added to this case
%>
</table>
<%
		vResults = TransactionReport.getPINReturnRequests(application, "","", 0, new String[]{""}, new String[]{""}, lCaseNumber, SessionData, "0"/*Search By RequestDate*/);
%>
<table width="100%" height="100%" style="height:100%;">
	<tr>
		<td align="left" valign="top">
			<table cellspacing="1" cellpadding="1" border="0">
				<tr>
					<td class="rowhead2" colspan="2" align="center" style="text-align:center;"><b><%=SessionData.getString("jsp.admin.reports.pinreturn_search.requestdetaillabel").toUpperCase().replaceAll("&NBSP;", "&nbsp;")%>&nbsp;<%=lCaseNumber%></b></td>
				</tr>
<%
		if ( vResults.size() > 0 )
		{//If there are results
			vResults = (Vector)vResults.get(0);
%>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.casenumber",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row1"><%=vResults.get(0).toString()%></td>
                </tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.businessname",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row2"><%=vResults.get(3).toString()%></td>
                </tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.controlnumber",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row1"><%=vResults.get(6).toString()%></td>
                </tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.sku",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row2"><%=vResults.get(5).toString()%></td>
                </tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.productdescription",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row1"><%=vResults.get(7).toString()%></td>
                </tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.reason",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row2"><%=Languages.getString("jsp.admin.reports.pinreturn_search.reason_" + vResults.get(10).toString(),SessionData.getLanguage())%></td>
                </tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.status",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row1"><%=Languages.getString("jsp.admin.reports.pinreturn_search.status_" + vResults.get(9).toString(),SessionData.getLanguage())%></td>
                </tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.reports.pinreturn_search.comments",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row2"><div height="70px" style="height:70px;overflow:auto;">
                    	<ul>
<%
			Vector vComments = (Vector)vResults.get(11);
			for (int i = 0; i < vComments.size(); i++)
			{
				Vector vTemp = (Vector)vComments.get(i);
%>
							<li>(<%=DateUtil.formatDateTime((java.sql.Timestamp)vTemp.get(0))%>)<br><%=HTMLEncoder.encode(vTemp.get(1).toString()).replaceAll("\r\n", "<br>")%></li>
<%
			}
%>
						</ul>
					</div></td>
                </tr>
                <tr>
                    <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.pinreturn_search.lastrequest",SessionData.getLanguage())%></td>
<%
			Vector vLastRequest = TransactionReport.getLastPINReturnRequestByControlNumber(application, Long.parseLong(vResults.get(0).toString()), SessionData);
			if ( vLastRequest.size() > 0 )
			{
				vLastRequest = (Vector)vLastRequest.get(0);
%>
                    <td class="row1"><a href="javascript:void(0);" onclick="openDetails(<%=vLastRequest.get(0)%>);"><%=vLastRequest.get(0)%></a></td>
<%
			}
			else
			{
%>
		            <td class="row1"><%=Languages.getString("jsp.admin.reports.pinreturn_search.none",SessionData.getLanguage())%></td>
<%
			}
%>
                </tr>
<%
		}//End of if there are results
		else
		{//Else show the message of no results
%>
				<tr>
					<td class="main" colspan="2"><%=Languages.getString("jsp.admin.reports.pinreturn_search.noresults",SessionData.getLanguage())%></td>
				</tr>
<%
		}//End of else show the message of no results
%>
			</table>
			<br><br>
<%
		if ( vResults.size() > 0 )
		{//If there are results
%>
			<form method="post" action="pinreturn_searchdetails.jsp" onsubmit="DisableButtons('<%=vResults.get(9).toString()%>');">
				<input type="hidden" name="caseNumber" value="<%=request.getParameter("caseNumber")%>">
				<table>
					<tr><td class="main"><%=Languages.getString("jsp.admin.reports.pinreturn_search.additionalcomments",SessionData.getLanguage())%></td></tr>
					<tr><td><textarea name="txtComments" cols="40" rows="5" onpropertychange="validateComments(this);"></textarea></td></tr>
<%
			if ( vResults.get(9).toString().equals("ISO_CREDITED") )
			{
%>
					<tr><td>
						<input type="submit" id="btnSave" disabled="disabled" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.voidprevcredit",SessionData.getLanguage())%>" onclick="return confirm('<%=Languages.getString("jsp.admin.reports.pinreturn_search.confirmvoidprevcredit",SessionData.getLanguage())%>');">
						<input type="hidden" name="action" value="void">
						<input type="hidden" name="millennium_no" value="<%=vResults.get(4)%>">
						<input type="hidden" name="control_no" value="<%=vResults.get(6)%>">
						<input type="hidden" name="amount" value="<%=vResults.get(12)%>">
					</td></tr>
<%
			}
			else if ( vResults.get(9).toString().equals("ISO_CREDITED_NOT_RECYCLED") )
			{
%>
					<tr><td>
						<input type="submit" id="btnSave" disabled="disabled" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.voidprevcredit",SessionData.getLanguage())%>" onclick="document.getElementById('action').value='void';return confirm('<%=Languages.getString("jsp.admin.reports.pinreturn_search.confirmvoidprevcredit",SessionData.getLanguage())%>');">
						<input type="hidden" name="action" value="">
						<input type="hidden" name="millennium_no" value="<%=vResults.get(4)%>">
						<input type="hidden" name="control_no" value="<%=vResults.get(6)%>">
						<input type="hidden" name="amount" value="<%=vResults.get(12)%>">
						<input type="hidden" name="sku" value="<%=vResults.get(5)%>">
						<input type="hidden" name="pin" value="<%=vResults.get(8)%>">
						<input type="submit" id="btnRecycle" disabled="disabled" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.recyclepin",SessionData.getLanguage())%>" onclick="document.getElementById('action').value='recycle';return confirm('<%=Languages.getString("jsp.admin.reports.pinreturn_search.confirmrecyclepin",SessionData.getLanguage()).replaceAll("_ctrlno_", vResults.get(6).toString())%>');">
					</td></tr>
<%
			}
			else
			{
%>
					<tr><td align="center"><input type="submit" id="btnSave" disabled="disabled" value="<%=Languages.getString("jsp.admin.reports.pinreturn_search.savecomments",SessionData.getLanguage())%>"></td></tr>
<%
			}
%>
				</table>
			</form>
<%
		}//End of if there are results
%>
		</td>
	</tr>
</table>
<%
	}
	catch (Exception e)
	{
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" nowrap="nowrap"><b>Error in page</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class="main"><%=Languages.getString("jsp.admin.reports.pinreturn_search.checklog",SessionData.getLanguage())%><br><%=e.getLocalizedMessage()%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}//End of catch
}//End of if a case number was specified
else
{//Else there is no case number to retrieve
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" nowrap="nowrap"><b>Error</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class="main"><%=Languages.getString("jsp.admin.reports.pinreturn_search.nocasenumber",SessionData.getLanguage())%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
}//End of else there is no case number to retrieve
%>
</body>
</html>
