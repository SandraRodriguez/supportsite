<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport" %>
                 
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 4;
  int section_page = 38;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";
  String strCities[] = request.getParameterValues("cities");
  
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();
  String noteTimeZone = "";
  
  String keyLanguage = "jsp.admin.reports.title44";
  
  String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
  String testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());
  
  Vector vTimeZoneData = null;
  String subTitleReportFixed = "";
  if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
  {
	subTitleReportFixed = Languages.getString(keyLanguage,SessionData.getLanguage()) + " "+Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
	subTitleReportFixed = subTitleReportFixed + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );
  }
  		
  if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
  {
	vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
  }
  else
  {
	vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  }
  
  noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
  
  boolean isInternational = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
								&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
  
  if (request.getParameter("search") != null)
  {
	    if (TransactionReport.validateDateRange(SessionData))
	    {
	      SessionData.setProperty("start_date", request.getParameter("startDate"));
	      SessionData.setProperty("end_date", request.getParameter("endDate"));
	
	      strCities = request.getParameterValues("cities");
	      if (strCities != null)
	      {
	        TransactionReport.setCities(strCities);
	      }
	      
	      //////////////////////////////////////////////////////////////////
	  	  //HERE WE DEFINE THE REPORT'S HEADERS 
	      headers = getHeadersSummaryTransactionsByCity( SessionData, isInternational );
	      //////////////////////////////////////////////////////////////////
	      //////////////////////////////////////////////////////////////////
	  
	  
	  	  if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
		  {
				//TO SCHEDULE REPORT
				titles.add(noteTimeZone);
				titles.add(titleReport);		
		        titles.add(testTrx);
				TransactionReport.getCitySummary( SessionData, application, DebisysConstants.SCHEDULE_REPORT, headers, titles );
				ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
				if (  scheduleReport != null  )
				{
					scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
					scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
					scheduleReport.setTitleName( keyLanguage );
					response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );   
				}	
				else
	  	      		vecSearchResults = TransactionReport.getCitySummary(SessionData, application, DebisysConstants.EXECUTE_REPORT, headers, titles);
				
		  }
		  else if ( request.getParameter("downloadReport") != null )
	  	  {
	  	  	titles.add( noteTimeZone );
	  	    titles.add( subTitleReportFixed );					
		    titles.add( testTrx );
			TransactionReport.getCitySummary( SessionData, application, DebisysConstants.DOWNLOAD_REPORT, headers, titles );
			response.sendRedirect( TransactionReport.getStrUrlLocation() );			  
		  }
		  else
		  {
	  	      vecSearchResults = TransactionReport.getCitySummary(SessionData, application, DebisysConstants.EXECUTE_REPORT, headers, titles);
	  	  }     
	    }
	    else
	    {
	      searchErrors = TransactionReport.getErrors();
	    }
  }
  
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>

<%!public static ArrayList<ColumnReport> getHeadersSummaryTransactionsByCity( SessionData sessionData, boolean isInternational )
   {
	 ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
	 headers.add(new ColumnReport("phys_city", Languages.getString("jsp.admin.customers.merchants_edit.city", sessionData.getLanguage()).toUpperCase(), String.class, false));
	 headers.add(new ColumnReport("qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), String.class, false));
	 headers.add(new ColumnReport("total_sales", Languages.getString("jsp.admin.reports.recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
	 if( isInternational )
	 {
	 	headers.add(new ColumnReport("total_bonus", Languages.getString("jsp.admin.reports.bonus", sessionData.getLanguage()).toUpperCase(), Double.class, true));
	 } 
	 headers.add(new ColumnReport("total_recharge", Languages.getString("jsp.admin.reports.total_recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
	 return headers;
	}  
  %>

<table border="0" cellpadding="0" cellspacing="0" width="550">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= titleReport %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
	titleReport = titleReport.toUpperCase(); 
    if (searchErrors != null)
    {
		out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString( "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

        Enumeration enum1 = searchErrors.keys();

        while (enum1.hasMoreElements())
        {
          String strKey   = enum1.nextElement().toString();
          String strError = (String)searchErrors.get(strKey);

          out.println("<li>" + strError);
        }
        out.println("</font></td></tr></table>");
     }

     if (vecSearchResults != null && vecSearchResults.size() > 0)
     {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">

            <tr class="main"><td nowrap colspan="2"><%=noteTimeZone%><br/><br/></td></tr>
            <tr>
              <td class="main"><%= subTitleReportFixed %>
                <br>
                <br>
                	<font color="#ff0000"><%= testTrx %></font>
                <br>                
              </td>
            </tr>
            
            <tr>
              <td class="main">
                 <form name="downloadData" method=post action="admin/reports/transactions/transaction_summary_by_city.jsp">
	              <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
	              <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
	              <input type="hidden" name="search" value="y">
	              <input type="hidden" name="downloadReport" value="y">
	            	<%for(int i=0;i<strCities.length;i++){%>
					<input type="hidden" name="cities" value="<%=strCities[i]%>">
					<%}%>
	              <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		    	 </form>
		                  
              </td>
            </tr>
            
           
		    
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2 width="4%">
                  #
                </td>
                <% 
                for(ColumnReport colum: headers)
                { 
                  String columnDescription = colum.getLanguageDescription();
                  %>
                   <td class=rowhead2 nowrap ><%= columnDescription.toUpperCase()%>
                   </td>
                  <%
                }
                %>                           
              </tr>
            </thead>
<%
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;
            double dblTotalSalesSum = 0;
            double dblTotalBonus = 0;
            double dblTotalRechargeSum = 0;
            int intTotalQtySum = 0;
            
             while (it.hasNext())
              {
               Vector vecTemp = null;
                vecTemp = (Vector)it.next();
                    int intTotalQty = Integer.parseInt(vecTemp.get(1).toString());
                    double dblTotalSales = Double.parseDouble(vecTemp.get(2).toString());
                    double dblBonus = Double.parseDouble(vecTemp.get(3).toString());
                    double dblTotalRecharge = Double.parseDouble(vecTemp.get(4).toString()); 
                    intTotalQtySum = intTotalQtySum + intTotalQty;
                    dblTotalSalesSum = dblTotalSalesSum + dblTotalSales;
                    dblTotalBonus += dblBonus;
                    dblTotalRechargeSum += dblTotalRecharge;
	   
	       	out.print("<tr class=row" + intEvenOdd + ">" +
	       			"<td>" + intCounter++ + "</td>" +
	       			"<td nowrap>" + vecTemp.get(0) + "</td>" + 
	       			"<td>" + intTotalQty + "</td>" + 
	           		"<td align=right><a href=\"admin/reports/transactions/city_transactions.jsp?" + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), 
	           	"UTF-8") + "&search=y&cityId=" + vecTemp.get(0) + "&report=y&printbtn=\" target=\"_blank\">" + NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
	          
	       	if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) 
	       			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
	       	{ 
                      out.println("<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>"); 
                      }
		
                      out.println("<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblTotalRecharge)) + "</td>");
                
                if (intEvenOdd == 1)
                {
                  intEvenOdd = 2;
                }
                else
                {
                  intEvenOdd = 1;
                }
              }
%>
            <tfoot>
              <tr class=row<%= intEvenOdd %>>
                <td colspan=2 align=right>
                  <%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                </td>
                <td align=left>
                  <%= intTotalQtySum %>
                </td>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %>
                </td>
<%
		if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) 
				&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
		{
%>
                		<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalBonus))%></td>
<%
		} 
%>
                <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalRechargeSum))%></td>
              </tr>
            </tfoot>
          </table>
<%
	} else  if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
      
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "CaseInsensitiveString", "Number", "Number", "Number", "Number"],0,false,false);
  -->
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>