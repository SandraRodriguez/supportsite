<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.languages.*,
                 com.debisys.utils.HTMLEncoder" %>
<%
int section=4;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String queryType = null;
boolean showInfoMultiSource = (SessionData.checkPermission(DebisysConstants.PERM_SHOW_MULTISOURCE_INFO)?true:false); 

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }
  if (request.getParameter("queryType") != null)
  {
    queryType = request.getParameter("queryType");
  }

  if (intPage < 1)
  {
    intPage=1;
  }
  
  boolean isInternal = Boolean.FALSE;
  if(request.getParameter("isInternal") != null) {            
        if(request.getParameter("isInternal") == "true" || request.getParameter("isInternal").equalsIgnoreCase("true")) {
            isInternal = Boolean.TRUE;
        }
  }

  if (TransactionReport.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));

    String products = request.getParameter("products");
    if (products != null && !products.trim().equals(""))
    {
        TransactionReport.setProductIds(products);
    }
    String providersIds = request.getParameter("providersIds");
    if (providersIds != null && !providersIds.trim().equals(""))
    {
        TransactionReport.setProviderIDs(providersIds);
    }
    vecSearchResults = TransactionReport.getTransactionErrorDetailByTypes(intPage, intPageSize, SessionData, true, queryType, isInternal);
    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize);
      if ((intPageCount * intPageSize) < intRecordCount)
      {
        intPageCount++;
      }
    }
  }
  else
  {
    searchErrors = TransactionReport.getErrors();
  }
%>
<html>
<head>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="../../../images/top_blue.gif">
  <tr>
	  <td width="18" height="20" align=left><img src="../../../images/top_left_blue.gif" width="18" height="20"></td>
	  <td class="formAreaTitle" align=left width="3000">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.transaction_error_details.title",SessionData.getLanguage()).toUpperCase()%></td>
	  <td width="12" height="20" align=right><img src="../../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=intRecordCount%> result(s) found<%
              if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
            </td></tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }
              %>
              </td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <thead>
      	      <tr>
              <td class=rowhead2>#</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.amount",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.product_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.phone",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.type",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.host",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.port",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.duration",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.result",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.provider_response",SessionData.getLanguage()).toUpperCase()%></td>
              
              <% 
              if ( showInfoMultiSource )
              {        
              %>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.tools.dtu2536.ProvBackup",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.tools.dtu2536.MultiSource",SessionData.getLanguage()).toUpperCase()%></td>
                <%  
              }
              if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{%>
					<td class=rowhead2><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage()).toUpperCase()%></td>
					 <%} %>
            </tr>
            </thead>
            <%
                  int intCounter = 1;
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td nowrap>" + intCounter++ + "</td>" +
                                "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                "<td nowrap>" + vecTemp.get(1) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(2) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(3) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(4) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(5) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(6) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(7) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(8) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(9) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(11) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(12) + "</td>" +
                                "<td nowrap align=\"right\">" + vecTemp.get(13) + "</td>" );
                  if ( showInfoMultiSource )
                  {
                      out.println("<td nowrap align=\"right\">" + vecTemp.get(14) + "</td>");
                      String markMultiSource = "";
                      if ( vecTemp.get(15)=="1" )
                      {
                         markMultiSource = Languages.getString("jsp.tools.dtu2536.MultiSourceFlag",SessionData.getLanguage()).toUpperCase();     
                      }
                      out.println("<td nowrap align=\"center\">" + markMultiSource + "</td>");
                  } 
                  if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      				&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      				&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{
                     //DBSY-919 Invoice Number Field
                     out.println("<td>" + vecTemp.get(14) + "</td>" + "</tr>");
                     ///
                    }
                    else
                    	 out.println("</tr>");

                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>
            <table id="tblClose" style="display:block;width:100%;">
              <tr><td>&nbsp;</td></tr>
              <tr><td align=center><input type=button value="<%=Languages.getString("jsp.admin.reports.closewindow",SessionData.getLanguage())%>" onclick="window.close();/*window.setTimeout('', 1000);*/"></td></tr>
            </table>
<%
}
else
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
<script>
document.getElementById("tblClose").style.display = "none";
window.print();
document.getElementById("tblClose").style.display = "block";
</script>
</body>
</html>