<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*,
                 com.debisys.reports.pojo.*,
                 com.debisys.utils.ColumnReport,
                 com.debisys.utils.NumberUtil, com.debisys.schedulereports.ScheduleReport" %>
                 
<%@page import="com.debisys.utils.TimeZone"%>
<%
	int section = 4;
	int section_page = 19;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<script>
li {
        width: 25px;
        height: 25px;
        border: solid 1px black;
        display: inline;
        margin: 0 2px 0 0;
        float: left;
}
</script>
<%
	ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
    ArrayList<String> titles = new ArrayList<String>();
    
	String titleDateRange = Languages.getString("jsp.admin.reports.transactions.date_range",SessionData.getLanguage());
    String titleTopUps    = Languages.getString("jsp.admin.reports.transactions.number_of_top_ups",SessionData.getLanguage());
   	String titleRecharge  = Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage());
   	String titleBonus     = Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage());
   	String titleTotalRecharge = Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage());
   	String titleNetAmount =  Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage());
   	String titleTaxAmount = Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage());
   	String titleNumberLocations = Languages.getString("jsp.admin.reports.transactions.number_of_locations",SessionData.getLanguage());
      	
    String titleDescription = Languages.getString("jsp.admin.reports.description",SessionData.getLanguage()).toUpperCase();
    String titleCurrenPeriod = Languages.getString("jsp.admin.reports.current_period",SessionData.getLanguage()).toUpperCase();
    String titlePreviousPeriod = Languages.getString("jsp.admin.reports.previous_period",SessionData.getLanguage()).toUpperCase();
    String titlePercentageChange = Languages.getString("jsp.admin.reports.percentage_change",SessionData.getLanguage()).toUpperCase();
              
    //////////////////////////////////////////////////////////////////
	//HERE WE DEFINE THE REPORT'S HEADERS 
	headers = getHeadersTransactionsSummaryByPhoneNumber( SessionData, titleDateRange, titleTopUps, titleRecharge,  titleBonus, titleTotalRecharge, titleNetAmount, 
														  titleTaxAmount, titleNumberLocations, titleDescription, titleCurrenPeriod, titlePreviousPeriod, titlePercentageChange);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	  		
	Vector vTimeZoneData = null;
		
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	
	Vector warningSearchResults = new  Vector();
	Vector vecCurrent = new Vector();
	Vector vecPast = new Vector();
	Hashtable searchErrors = null;
	String message = ""; 
	TransactionReport.setWarningmessage(new Vector<String>());
	
	
    String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
    String keyLanguage = "jsp.admin.reports.title34";
  
    String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
    String testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());
  	
  	
  	TrxSummPhonePojo trxSummPhonePojoResult = new TrxSummPhonePojo();
	if (request.getParameter("search") != null)
	{
	  	if (TransactionReport.validateDateRange(SessionData) && request.getParameter("phoneNumber") != null && !(request.getParameter("phoneNumber")).equals(""))
	  	{	    
	  	
	  		
	  		  		
	  		if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
			{			
				//TO SCHEDULE REPORT
				titles.add(noteTimeZone);
		  		titles.add( Languages.getString(keyLanguage,SessionData.getLanguage()) );
				TransactionReport.getPhoneSummary(SessionData,application, DebisysConstants.SCHEDULE_REPORT, headers, titles);
				
				ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
				if (  scheduleReport != null  )
				{
					scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
					scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
					scheduleReport.setTitleName( keyLanguage );   
				}	
				response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
			}
			else if ( request.getParameter("downloadReport") != null )
	  		{
	  			//TO DOWNLOAD ZIP REPORT
	  			TransactionReport.getPhoneSummary(SessionData,application, DebisysConstants.DOWNLOAD_REPORT, headers, titles);
	  			if ( TransactionReport.getStrUrlLocation() != null )
	  				response.sendRedirect( TransactionReport.getStrUrlLocation() );	
	  		}	
			else
			{	
			   //pull last months
			    TransactionReport.setStartDate(DateUtil.addSubtractMonths(request.getParameter("startDate"),-1));
			    TransactionReport.setEndDate(DateUtil.addSubtractMonths(request.getParameter("endDate"),-1));
			    //vecPast = TransactionReport.getPhoneSummary(SessionData,application);
			
			    TransactionReport.setStartDate(request.getParameter("startDate"));
			    TransactionReport.setEndDate(request.getParameter("endDate"));
			    SessionData.setProperty("start_date", request.getParameter("startDate"));
			    SessionData.setProperty("end_date", request.getParameter("endDate"));
			    //vecCurrent = TransactionReport.getPhoneSummary(SessionData,application);
			    
			    trxSummPhonePojoResult = TransactionReport.getPhoneSummary(SessionData,application, DebisysConstants.EXECUTE_REPORT, headers, titles);
			   
			} 
			     
			    
	  	}
	    else
	    {
		    if(!TransactionReport.validateDateRange(SessionData))
		    {
		    	searchErrors = TransactionReport.getErrors();
		    }
		    if(request.getParameter("phoneNumber") == null || (request.getParameter("phoneNumber")).equals(""))
		    {
		        if (searchErrors == null)
		        {
		        	searchErrors = new Hashtable();
		        }
		        searchErrors.put("zphoneNumber",Languages.getString("com.debisys.reports.error6",SessionData.getLanguage()));	
		    }
	    }
	}
%>
<%@ include file="/includes/header.jsp" %>
<%
	boolean bool_custom_config_mexico = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
	boolean bool_custom_config_default = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	
	boolean bool_deploy_type_intl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
	boolean bool_perm_enable_tax_calc = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
%>

<%!
  public static ArrayList<ColumnReport> getHeadersTransactionsSummaryByPhoneNumber( SessionData sessionData, String titleDateRange, 
  																				    String titleTopUps, String titleRecharge,  
  																					String titleBonus, String titleTotalRecharge, 
  																					String titleNetAmount, String titleTaxAmount, 
  																					String titleNumberLocations, String titleDescription, 
  																					String titleCurrenPeriod, String titlePreviousPeriod, 
  																					String titlePercentageChange	)
  {
	ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
	
	/*INIT CURRENT PERIOD*/
	
	ColumnReport numTopUps = new ColumnReport("sale_amount", titleRecharge + " " + titleCurrenPeriod,Double.class,false);
	headers.add(numTopUps);
	
	ColumnReport totalTrx = new ColumnReport("totalTrx",  titleTopUps + " " + titleCurrenPeriod, Double.class,false);
	headers.add(totalTrx);
	
	ColumnReport bonus = new ColumnReport("bonus", titleBonus  + " " + titleCurrenPeriod ,Double.class,false);
	headers.add(bonus);
	
	ColumnReport total_Recharge = new ColumnReport("total_Recharge",  titleTotalRecharge + " " + titleCurrenPeriod, Double.class,false);
	headers.add(total_Recharge);
	
	ColumnReport net_amount = new ColumnReport("net_amount", titleNetAmount + " " + titleCurrenPeriod, Double.class,false);
	headers.add(net_amount);
		
	ColumnReport tax = new ColumnReport("tax", titleTaxAmount + " " + titleCurrenPeriod, Double.class,false);
	headers.add(tax);
	
	ColumnReport location_count = new ColumnReport("location_count", titleNumberLocations + " " + titleCurrenPeriod, Double.class,false);
	headers.add(location_count);
	/*END CURRENT PERIOD*/
	
	
	/*INIT LAST PERIOD*/
	ColumnReport sale_amountP = new ColumnReport("sale_amountP", titleRecharge + " " +titlePreviousPeriod ,Double.class,false);
	headers.add(sale_amountP);
	
	ColumnReport totalTrxP = new ColumnReport("totalTrxP", titleTopUps + " " +titlePreviousPeriod,Double.class,false);
	headers.add(totalTrxP);
	
	ColumnReport  bonusP = new ColumnReport("bonusP", titleBonus + " " +titlePreviousPeriod,Double.class,false);
	headers.add(bonusP);
	
	ColumnReport total_RechargeP = new ColumnReport("total_RechargeP", titleTotalRecharge + " " +titlePreviousPeriod,Double.class,false);
	headers.add(total_RechargeP);
	
	ColumnReport net_amountP = new ColumnReport("net_amountP", titleNetAmount + " " +titlePreviousPeriod,Double.class,false);
	headers.add(net_amountP);
		
	ColumnReport taxP = new ColumnReport("taxP", titleTaxAmount + " " +titlePreviousPeriod,Double.class,false);
	headers.add(taxP);
	
	ColumnReport location_countP = new ColumnReport("location_countP", titleNumberLocations + " " +titlePreviousPeriod,Double.class,false);
	headers.add(location_countP);
	
	
	/*END LAST PERIOD*/
	
	
	/*INI PERCENT CHANGE*/
	ColumnReport percentTopUps = new ColumnReport("percentTopUps", titlePercentageChange +" "+ titleTopUps ,Double.class,false);
	headers.add(percentTopUps);
	
	ColumnReport percentRecharge = new ColumnReport("percentRecharge", titlePercentageChange +" "+ titleRecharge ,Double.class,false);
	headers.add(percentRecharge);
	
	ColumnReport percentBonus = new ColumnReport("percentBonus", titlePercentageChange +" "+ titleBonus ,Double.class,false);
	headers.add(percentBonus);
	
	ColumnReport percentTotalRecharge = new ColumnReport("percentTotalRecharge", titlePercentageChange + " "+ titleTotalRecharge ,Double.class,false);
	headers.add(percentTotalRecharge);
	
	ColumnReport percentNetAmt = new ColumnReport("percentNetAmt", titlePercentageChange +" "+ titleNetAmount ,Double.class,false);
	headers.add(percentNetAmt);
	
	ColumnReport percentTaxAmt = new ColumnReport("percentTaxAmt", titlePercentageChange +" "+ titleTaxAmount ,Double.class,false);
	headers.add(percentTaxAmt);
					
	ColumnReport percentLocationCount = new ColumnReport("percentLocationCount", titlePercentageChange +" "+ titleNumberLocations,Double.class,false);
	headers.add(percentLocationCount);
	
	/*END PERCENT CHANGE*/
					
	ColumnReport startDateCol = new ColumnReport("startDate", titleCurrenPeriod,Double.class,false);
	headers.add(startDateCol);
	
	ColumnReport endDateCol = new ColumnReport("endDate", titleCurrenPeriod,Double.class,false);
	headers.add(endDateCol);
	
	ColumnReport startDateP = new ColumnReport("startDateP", titlePreviousPeriod,Double.class,false);
	headers.add(startDateP);
	
	ColumnReport endDateP = new ColumnReport("endDateP", titlePreviousPeriod,Double.class,false);
	headers.add(endDateP);
	
	
	return headers;
 }  
%>
  
<table border="0" cellpadding="0" cellspacing="0" width="500" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title34",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
												<table width=400>
<%
	if (searchErrors != null)
	{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1 = searchErrors.keys();

		while(enum1.hasMoreElements())
		{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
		}

  out.println("</font></td></tr>");
	}
%>
												</table>
               </td>
              </tr>
            </table>
<%
	//vecCurrent != null && vecCurrent.size() > 0
	if ( trxSummPhonePojoResult.getListPeriods()!=null && trxSummPhonePojoResult.getListPeriods().size()>0 )
	{
%>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr class="main"><td nowrap colspan="4"><%=noteTimeZone %><br/><br/></td></tr>
<%     
		String taxtype ="";
		if(bool_perm_enable_tax_calc &&  bool_deploy_type_intl && bool_custom_config_default) 
		{   
			// The default tax was used in the calculation
				if( TransactionReport.getWarningMessage().size()>0)
					taxtype = TransactionReport.getWarningMessage().get(0);
					
				TransactionReport.setWarningmessage(new Vector<String>());
				warningSearchResults = TransactionReport.getWarningMessage(SessionData, SessionData.getProperty("end_date"), SessionData.getProperty("start_date")); 
		}	
		if (warningSearchResults.size() > 0) 
		{
			message = message + Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage()) + "<br />";
			message = message + Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage()) + "<br />";

			for (int i = 0; i < warningSearchResults.size(); i++){  
				message = message + warningSearchResults.get(i).toString() + "<br />";
			}
%>
				<tr class="errorText">
					<td colspan="4"><%= message %></td>
				</tr>
										
<%
		} 
%>            							
				<tr class="errorText">
					<td colspan="4"><%= taxtype %></td>
				</tr>
				<tr class="errorText">
					<td colspan="4">
						<form target="blank" method=post action="admin/reports/transactions/transaction_summary_by_phone.jsp">
		                    <input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
		                    <input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
		                    <input type=hidden name="phoneNumber" value="<%=TransactionReport.getPhoneNumber()%>">
		                    <input type="hidden" name="downloadReport" value="y">
		                    <input type="hidden" name="search" value="y">		                    
		                    <input id=btnSubmit type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		                </form>
					</td>
				</tr>
										
            
<%
	if( false )
	{
			String topUpCountCurrent = vecCurrent.get(2).toString();
            double dbTopUpCountCurrent = Double.parseDouble(topUpCountCurrent);

			String topUpCountPast = vecPast.get(2).toString();
            double dbTopUpCountPast = Double.parseDouble(topUpCountPast);
            
			double dbTotalAmountCurrent = Double.parseDouble(vecCurrent.get(3).toString());
			double dbTotalAmountPast = Double.parseDouble(vecPast.get(3).toString());
            
		// DBSY-905 
            double dbBonusAmountPast = 0; 
            double dbTotalRechargesAmountPast = 0; 
            double dbBonusAmountCurrent = 0; 
            double dbTotalRechargesAmountCurrent  = 0; 
            double dbNetAmountPast = 0;
            double dbTaxAmountPast  = 0;
            double dbNetAmountCurrent = 0;
            double dbTaxAmountCurrent = 0;
            
            if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
			dbBonusAmountPast = Double.parseDouble(vecPast.get(5).toString()); 
			dbTotalRechargesAmountPast = Double.parseDouble(vecPast.get(6).toString()); 
			dbBonusAmountCurrent = Double.parseDouble(vecCurrent.get(5).toString()); 
			dbTotalRechargesAmountCurrent = Double.parseDouble(vecCurrent.get(6).toString()); 
				
              if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))) 
	  		{
				dbNetAmountPast = Double.parseDouble(vecPast.get(8).toString());
				dbTaxAmountPast = Double.parseDouble(vecPast.get(7).toString());
				dbNetAmountCurrent = Double.parseDouble(vecCurrent.get(8).toString());
				dbTaxAmountCurrent = Double.parseDouble(vecCurrent.get(7).toString());
       		}
            }
        ///////    
            
		String locationCountCurrent = vecCurrent.get(4).toString();
            double dbLocationCountCurrent = Double.parseDouble(locationCountCurrent);
            
		String locationCountPast = vecPast.get(4).toString();
            double dbLocationCountPast = Double.parseDouble(locationCountPast);
            
		double dbl_percentTopUpCount = 0.0;
		double dbl_percentTotalAmount= 0.0;
		double dbl_percentBonusAmount= 0.0;
		double dbl_percentTotalRechargesAmount= 0.0;
		double dbl_percentNetAmount= 0.0;
		double dbl_percentTaxAmount= 0.0;
		double dbl_percentLocationCount = 0.0;
            
		// make sure you don't divide by zero
        if (dbTopUpCountPast != 0) {
        	dbl_percentTopUpCount = (dbTopUpCountCurrent - dbTopUpCountPast) / dbTopUpCountPast * 100;
        } else if(dbTopUpCountCurrent != 0){
        	dbl_percentTopUpCount = 100.0;
            }
        if (dbTotalAmountPast != 0) {
        	dbl_percentTotalAmount = (dbTotalAmountCurrent - dbTotalAmountPast) / dbTotalAmountPast * 100;
        } else if(dbTopUpCountCurrent != 0){
        	dbl_percentTotalAmount = 100.0;
            }
        if (dbBonusAmountPast != 0) {
        	dbl_percentBonusAmount = (dbBonusAmountCurrent - dbBonusAmountPast) / dbBonusAmountPast * 100;
        } else if(dbBonusAmountCurrent != 0){
        	dbl_percentBonusAmount = 100.0;
            }
        if (dbTotalRechargesAmountPast != 0) {
        	dbl_percentTotalRechargesAmount = (dbTotalRechargesAmountCurrent - dbTotalRechargesAmountPast) / dbTotalRechargesAmountPast * 100;
        } else if(dbTotalRechargesAmountCurrent != 0){
        	dbl_percentTotalRechargesAmount = 100.0;
            }
        if (dbNetAmountPast != 0) {
        	dbl_percentNetAmount = (dbNetAmountCurrent - dbNetAmountPast) / dbNetAmountPast * 100;
        } else if(dbNetAmountCurrent != 0){
        	dbl_percentNetAmount = 100.0;
            }
        if (dbTaxAmountPast != 0) {
        	dbl_percentTaxAmount = (dbTaxAmountCurrent - dbTaxAmountPast) / dbTaxAmountPast * 100;
        } else if(dbTaxAmountCurrent != 0){
        	dbl_percentTaxAmount = 100.0;
            }
        if (dbLocationCountPast != 0) {
        	dbl_percentLocationCount = (dbLocationCountCurrent - dbLocationCountPast) / dbLocationCountPast * 100;
        } else if(dbLocationCountCurrent != 0){
        	dbl_percentLocationCount = 100.0;
            }
            
      	out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.date_range",SessionData.getLanguage())
      		+"</td><td  align=right nowrap>" + HTMLEncoder.encode(vecCurrent.get(1).toString()) + "</td><td  align=right nowrap>" 
      		+ HTMLEncoder.encode(vecPast.get(1).toString()) + "</td><td></td></tr>");
            
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.number_of_top_ups",SessionData.getLanguage())
      +"</td><td  align=right nowrap><a href=\"admin/reports/transactions/transaction_detail_by_phone.jsp?startDate="
      + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" 
      + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&search=y&phoneNumber=" 
      + URLEncoder.encode(TransactionReport.getPhoneNumber(), "UTF-8") + "\" target=\"_blank\">" 
      + topUpCountCurrent+ "</a></td><td  align=right nowrap><a href=\"admin/reports/transactions/transaction_detail_by_phone.jsp?startDate="
      + URLEncoder.encode(DateUtil.addSubtractMonths(TransactionReport.getStartDate(),-1), "UTF-8") + "&endDate=" 
      + URLEncoder.encode(DateUtil.addSubtractMonths(TransactionReport.getEndDate(),-1), "UTF-8") + "&search=y&phoneNumber=" 
      + URLEncoder.encode(TransactionReport.getPhoneNumber(), "UTF-8") + "\" target=\"_blank\">" + topUpCountPast 
      		+ "</td><td nowrap align=\"right\">" + NumberUtil.formatAmount(String.valueOf(dbl_percentTopUpCount)) + "%</td></tr>");
      
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage())+
      "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTotalAmountCurrent))
      + "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTotalAmountPast))
      		+ "</td><td nowrap  align=right>" + NumberUtil.formatAmount(String.valueOf(dbl_percentTotalAmount)) + "%</td></tr>");
      
      if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
      	{
      
      //DBSY-905 bonus
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage())+
      "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbBonusAmountCurrent))
      + "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbBonusAmountPast) )
	      		+ "</td><td nowrap  align=right>" + dbl_percentBonusAmount + "%</td></tr>");
      
      //DBSY-905 total recharge
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage())+
      "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTotalRechargesAmountCurrent) )
      + "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTotalRechargesAmountPast) )
	      		+ "</td><td nowrap  align=right>" + dbl_percentTotalRechargesAmount + "%</td></tr>");
	      
      if(SessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))) 
	  {
      //DBSY-905 netAmount
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage())+
      "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbNetAmountCurrent))
      + "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbNetAmountPast))
	      			+ "</td><td nowrap  align=right>" + dbl_percentNetAmount + "%</td></tr>");
      
      //DBSY-905 taxAmount
      out.println("<tr class=row1><td nowrap>"+Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage())+
      "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTaxAmountCurrent))
      + "</td><td  align=right nowrap>" + NumberUtil.formatCurrency(String.valueOf(dbTaxAmountPast)) 
	      			+ "</td><td nowrap  align=right>" + dbl_percentTaxAmount + "%</td></tr>");
      }
      }
      
      out.println("<tr class=row2><td nowrap>"+Languages.getString("jsp.admin.reports.transactions.number_of_locations",SessionData.getLanguage())
      +"</td><td  align=right nowrap>" + locationCountCurrent+ "</td><td  align=right nowrap>" + locationCountPast 
      		+ "</td><td nowrap  align=right>" + dbl_percentLocationCount + "%</td></tr>");
      
      out.println("</table>");
      
      vecCurrent.clear();
      vecPast.clear();
      }
      else
      {   
      	TrxSummPhonePeriodPojo current = trxSummPhonePojoResult.getListPeriods().get(0);
      	TrxSummPhonePeriodPojo previous = trxSummPhonePojoResult.getListPeriods().get(1);
      	
      	%>
      	
      		<tr>
              <td colspan=4 class=rowhead2><%=Languages.getString("jsp.admin.reports.title34",SessionData.getLanguage()).toUpperCase()%> <%=HTMLEncoder.encode(TransactionReport.getPhoneNumber())%></td>
            </tr>
            <tr>
              <td class=rowhead2 valign=bottom><%=titleDescription%></td>
              <td class=rowhead2 valign=bottom><%=titleCurrenPeriod%></td>
              <td class=rowhead2 valign=bottom><%=titlePreviousPeriod%></td>
              <td class=rowhead2 valign=bottom><%=titlePercentageChange%></td>
            </tr>
      		<tr class=row1>
      			<td nowrap><%= titleDateRange %></td>
      			<td align=right nowrap><%= current.getStartDate() %> - <%= current.getEndDate() %></td>
      			<td align=right nowrap><%= previous.getStartDate() %> - <%= previous.getEndDate() %></td>
      			<td align=right nowrap></td>      		
      		</tr>      		
      		<tr class=row2>
      			<td nowrap><%= titleTopUps %></td>
      			<td align=right nowrap>
      					<a href="admin/reports/transactions/transaction_detail_by_phone.jsp?startDate=<%= URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") %>&endDate=<%= URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") %>&search=y&phoneNumber=<%= URLEncoder.encode(TransactionReport.getPhoneNumber(), "UTF-8") %>"  target="_blank" > 
      						<%= current.getTotalTrx() %> </a>
      			</td>
      			<td align=right nowrap>
      				<a href="admin/reports/transactions/transaction_detail_by_phone.jsp?startDate=<%= URLEncoder.encode(DateUtil.addSubtractMonths(TransactionReport.getStartDate(),-1), "UTF-8") %>&endDate=<%= URLEncoder.encode(DateUtil.addSubtractMonths(TransactionReport.getEndDate(),-1), "UTF-8") %>&search=y&phoneNumber=<%= URLEncoder.encode(TransactionReport.getPhoneNumber(), "UTF-8") %>"  target="_blank" >
      				<%= previous.getTotalTrx() %></a>
      			</td>
      			<td align=right nowrap><%= NumberUtil.formatPercentage( trxSummPhonePojoResult.getTotalTrx().toString() ) %></td>      		
      		</tr>      		
      		<tr class=row1>
      			<td nowrap><%= titleRecharge %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( current.getSaleAmount().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( previous.getSaleAmount().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatPercentage( trxSummPhonePojoResult.getSaleAmount().toString() ) %></td>      		
      		</tr>      		
      		<tr class=row1>
      			<td nowrap><%= titleBonus %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( current.getBonus().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( previous.getBonus().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatPercentage( trxSummPhonePojoResult.getBonus().toString() ) %></td>      		
      		</tr>      		
      		<tr class=row1>
      			<td nowrap><%= titleTotalRecharge %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( current.getTotalRecharge().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( previous.getTotalRecharge().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatPercentage( trxSummPhonePojoResult.getTotalRecharge().toString() ) %></td>      		
      		</tr>
      		<tr class=row1>
      			<td nowrap><%= titleNetAmount %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( current.getNetAmount().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( previous.getNetAmount().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatPercentage( trxSummPhonePojoResult.getNetAmount().toString() ) %></td>       		
      		</tr>
      		<tr class=row1>
      			<td nowrap><%= titleTaxAmount %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( current.getTax().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatCurrency( previous.getTax().toString() ) %></td>
      			<td align=right nowrap><%= NumberUtil.formatPercentage( trxSummPhonePojoResult.getTax().toString() ) %></td>       		
      		</tr>      	
      		<tr class=row2>
      			<td nowrap><%= titleNumberLocations %></td>
      			<td align=right nowrap><%= current.getLocationCount().toString()  %></td>
      			<td align=right nowrap><%= previous.getLocationCount().toString()  %></td>
      			<td align=right nowrap><%= NumberUtil.formatPercentage( trxSummPhonePojoResult.getLocationCount().toString() ) %></td>      		
      		</tr>   
      	
      	
      		
      	<% 
            
      }
      
	}
	else if (vecCurrent.size()==0 && request.getParameter("search") != null && searchErrors == null)
	{
 		out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
	}
%>
          </td>
      </tr>
    </table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>