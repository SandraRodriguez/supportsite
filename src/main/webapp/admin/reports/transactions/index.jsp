<%@page import="com.debisys.utils.HTMLEncoder, com.debisys.presentation.ReportGroup"%>
<%
	int section=4;
	int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
ReportGroup groups = ReportGroup.createTransactionsGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
%>
           <TABLE cellSpacing=0 cellPadding=0 width=750 background=images/top_blue.gif border=0>
              <TBODY>
              <TR>
                <TD width=23 height=20><IMG height=20 src="images/top_left_blue.gif" width=18></TD>
                <TD width="3000" class=formAreaTitle><B><%=Languages.getString("jsp.admin.reports.transactions",SessionData.getLanguage()).toUpperCase()%></B></TD>
                <TD width=28 height=20><IMG 
                  src="images/top_right_blue.gif"></TD></TR>
              <TR>
                <TD colSpan=3>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" 
                  bgColor=#7B9EBD border=0>
                    <TBODY>
                    <TR>
                      <TD width=1 bgColor=#7B9EBD><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top align=left bgColor=#ffffff>
                        <TABLE width="100%" border=0 
                        align=left cellPadding=2 cellSpacing=0 class="fondoceldas">
                          <TBODY>
                          <TR>
                            <TD class=main>
                            

<%@ include file="../qcommReportsFragment.jsp" %>
								<TABLE class=reportGroupTable>
                                <TBODY>
                                <TR>
                                <TH align=left><IMG height=22 
                                src="images/transaction_cube.png" 
                                width=22 border=0><span class="main">
                                <%=Languages.getString("jsp.admin.reports.transactions",SessionData.getLanguage()).toUpperCase()%>
                                </SPAN>
                                </TH></TR>
                                <TR>
                                <TD>
                                <UL class=sublevel name="main">
                                <%groups.showItem(out);%>
                                </UL></TD></TR>
                                </TH></TR>
                                </TBODY></TABLE>
									
							    </td>
							</tr>
			  			</table>
		    		</td>
		    		<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
		  		</tr>
				<tr>
			  		<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
		  		</tr>
		 	</table>
	 	</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>