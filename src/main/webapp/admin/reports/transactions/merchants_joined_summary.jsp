<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 4;
  int section_page = 39;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
  <jsp:setProperty name="TransactionReport" property="*" />
  <jsp:setProperty name="CustomerSearch" property="*"/>  
  <%@ include file="/includes/security.jsp" %>
  <%@ include file="/includes/header.jsp" %>
<%

Vector<Vector<Object>>    vecSearchResults = new Vector<Vector<Object>>();
Hashtable searchErrors     = null;
String    sortString       = "";

int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
  
String strAgentIds[] = request.getParameterValues("AgentIds");
String strSubAgentIds[] = request.getParameterValues("SubAgentIds");

  if (request.getParameter("search") != null)
  {
  	if (request.getParameter("page") != null)
	{
	  try
	  {
	    intPage=Integer.parseInt(request.getParameter("page"));
	  }
	  catch(NumberFormatException ex)
	  {
	    intPage = 1;
	  }
	}
	if (intPage < 1)
	{
	  intPage=1;
	}
    if (TransactionReport.validateDateRange(SessionData))
    {
		SessionData.setProperty("start_date", request.getParameter("startDate"));
		SessionData.setProperty("end_date", request.getParameter("endDate"));
		
		if(strAgentIds != null && strSubAgentIds != null){
			if(strAgentIds[0]!=null && "".equals(strAgentIds[0])){
				Vector vecAgents = TransactionReport.getAgentList(SessionData); 
				Iterator it = vecAgents.iterator();				
  				strAgentIds = new String[vecAgents.size()];
  				int i=0;
  				while(it.hasNext()){
  					Vector vecTemp = null;
  					vecTemp = (Vector)it.next();
  					strAgentIds[i++] = vecTemp.get(0).toString();
  				}			
			}
		
			if(strSubAgentIds[0]!=null && "".equals(strSubAgentIds[0])){
				Vector vecSubAgents = TransactionReport.getSubAgentList(SessionData);
				Iterator it = vecSubAgents.iterator();
				strSubAgentIds = new String[vecSubAgents.size()];
				int i=0;
				while(it.hasNext()){
					Vector vecTemp = null;
					vecTemp = (Vector)it.next();
					strSubAgentIds[i++] = vecTemp.get(0).toString();					
				}
			}		
		}				
		CustomerSearch.setAgentIds(strAgentIds);
		CustomerSearch.setSubAgentIds(strSubAgentIds);
		vecSearchResults = CustomerSearch.listJoinedMerchants(	intPage, 
																intPageSize, 
																SessionData, 
																application);

    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }

  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
	Iterator it  = vecSearchResults.iterator();
	Vector vecCounts = (Vector)it.next();
	Integer intNumRecords = (Integer)vecCounts.get(0); 
	intRecordCount = intNumRecords.intValue();
	if (intRecordCount>0)
	{
	  intPageCount = (intRecordCount / intPageSize);
	  if ((intPageCount * intPageSize) < intRecordCount)
	  {
	    intPageCount++;
	  }
	}	
  }
%>
	<SCRIPT LANGUAGE="JavaScript">
		var count = 0
	    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");
	
	    function scroll2()
	    {
	    document.downloadform.submit.disabled = true;  
	    document.downloadform.submit.value = iProcessMsg[count];
	    count++
	    if (count = iProcessMsg.length) count = 0
	    setTimeout('scroll2()', 150);
	    }
	    
	    function submitPage(thePage)
	    {
	        document.submitPageForm.page.value = thePage;  
	    	document.submitPageForm.submit();
	    }
	</SCRIPT>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%=Languages.getString("jsp.admin.reports.merchants_joined.title",SessionData.getLanguage()).toUpperCase()%></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }
		
        if (intRecordCount > 0)
        {
        	int      intEvenOdd               = 1;
            int      intCounter               = 1;
       		Iterator it  = vecSearchResults.iterator();
			Vector vecCounts = (Vector)it.next();

%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td class="main">
<%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
<%
                out.println(Languages.getString("jsp.admin.reports.merchants_joined.summary",SessionData.getLanguage()));
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                  out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
%>.  <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
              </td>
              
	  <tr>
	    <td align=left class="main" nowrap>
	    <form name="downloadform" method=post action="admin/reports/transactions/merchants_joined_export.jsp" onSubmit="scroll2();">
		    <input type="hidden" name="startDate" value="<%=TransactionReport.getStartDate()%>">
		    <input type="hidden" name="endDate" value="<%=TransactionReport.getEndDate()%>">
		    <input type="hidden" name="page" value="<%=intPage%>">
		    <input type="hidden" name="section_page" value="<%=section_page%>">
		    <input type="hidden" name="download" value="y">
		    <input type="hidden" id="AgentIds" name="AgentIds" value="<%=CustomerSearch.getAgentIds()%>">
		    <input type="hidden" id="SubAgentIds" name="SubAgentIds" value="<%=CustomerSearch.getSubAgentIds()%>"> 
		    <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.money_transfers.download",SessionData.getLanguage())%>">
	    </form>
	    </td>
	  </tr>
	  <tr>
	    <td align=right class="main" nowrap>
	    <form name="submitPageForm" id="submitPageForm" method="post" action="admin/reports/transactions/merchants_joined_summary.jsp">
	    	<input type="hidden" name="search" value="<%=URLEncoder.encode(request.getParameter("search"), "UTF-8")%>" />
	    	<input type="hidden" name="startDate" value="<%=TransactionReport.getStartDate()%>" />
	    	<input type="hidden" name="endDate" value="<%=TransactionReport.getEndDate()%>" />
		    <input type="hidden" id="AgentIds" name="AgentIds" value="<%=CustomerSearch.getAgentIds()%>">
		    <input type="hidden" id="SubAgentIds" name="SubAgentIds" value="<%=CustomerSearch.getSubAgentIds()%>"> 
	    	<input type="hidden" name="page" value="1" />	    	
		    <%
		    if (intPage > 1)
		    {
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage(1);return false;\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+ (intPage-1) +");return false;\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
		    }
		    int intLowerLimit = intPage - 12;
		    int intUpperLimit = intPage + 12;
		
		    if (intLowerLimit<1)
		    {
		      intLowerLimit=1;
		      intUpperLimit = 25;
		    }
		
		    for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
		    {
		      if (i==intPage)
		      {
		        out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
		      }
		      else
		      {
		        out.println("<a href=\"javascript:;\" onClick=\"submitPage("+ i +");return false;\">" + i + "</a>&nbsp;");
		      }
		    }
		
		    if (intPage <= (intPageCount-1))
		    {
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+(intPage+1)+");return false;\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
		      out.println("<a href=\"javascript:;\" onClick=\"submitPage("+(intPageCount)+");return false;\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
		    }		
		    %>
	    </form>
	    </td>
	  </tr>              
              
              
              <tr>
              <td class="main">
              <%=Languages.getString("jsp.admin.iso_name",SessionData.getLanguage())%>:&nbsp;<%=SessionData.getUser().getCompanyName()%>
              </td>
              </tr>
              <tr>
              <td class=main align=right valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
          </table>
          <table>
              <tr>
                  <td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
              </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>#</td>
                <td class=rowhead2 nowrap width=100><%=Languages.getString("jsp.admin.reports.merchants_joined.SubAgent",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap width=100><%=Languages.getString("jsp.admin.reports.merchants_joined.Rep",SessionData.getLanguage()).toUpperCase()%></td>                                 
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.merchants_joined.control_number",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.merchants_joined.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.merchants_joined.DBA",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 valign=bottom nowrap><%=Languages.getString("jsp.admin.reports.merchants_joined.Route",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 valign=bottom nowrap><%=Languages.getString("jsp.admin.reports.merchants_joined.Joined_Date",SessionData.getLanguage()).toUpperCase()%></td>        
				<td class=rowhead2 valign=bottom nowrap><%=Languages.getString("jsp.admin.reports.merchants_joined.status",SessionData.getLanguage()).toUpperCase()%></td>                  
                <TD CLASS="rowhead2" VALIGN="bottom" ><%=Languages.getString("jsp.admin.reports.merchants_joined.Payments_summary",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD CLASS="rowhead2" VALIGN="bottom"><%=Languages.getString("jsp.admin.reports.merchants_joined.last_transactions_date",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD CLASS="rowhead2" VALIGN="bottom" ><%=Languages.getString("jsp.admin.reports.merchants_joined.transactions_amount",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD CLASS="rowhead2" VALIGN="bottom" ><%=Languages.getString("jsp.admin.reports.merchants_joined.Recharges",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD CLASS="rowhead2" VALIGN="bottom" ><%=Languages.getString("jsp.admin.reports.merchants_joined.Balance",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD CLASS="rowhead2" VALIGN="bottom" ><%=Languages.getString("jsp.admin.reports.merchants_joined.Contact",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD CLASS="rowhead2" VALIGN="bottom" ><%=Languages.getString("jsp.admin.reports.merchants_joined.PhoneNumbers",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD CLASS="rowhead2" VALIGN="bottom" ><%=Languages.getString("jsp.admin.reports.merchants_joined.Email",SessionData.getLanguage()).toUpperCase()%></TD>
              </tr>
            </thead>
<%

			
            while(it.hasNext())
            {
				Vector vecTemp = null;
				vecTemp = (Vector)it.next();
				out.println("<tr class=row" + intEvenOdd +">");
				out.println("	<td>" + intCounter++ + "</td>");
              	out.println("	<td>" + vecTemp.get(0) + "</td>");
              	out.println("	<td>" + vecTemp.get(1) + "</td>");
              	out.println("	<td>" + vecTemp.get(2) + "</td>");
              	out.println("	<td>" + vecTemp.get(3) + "</td>");
              	out.println("	<td>" + vecTemp.get(4) + "</td>");
              	out.println("	<td>" + vecTemp.get(5) + "</td>");
              	out.println("	<td>" + vecTemp.get(6) + "</td>");
              	out.println("	<td>" + vecTemp.get(7) + "</td>");              	
              	out.println("	<td align=right>" +NumberUtil.formatCurrency( vecTemp.get(8).toString()) + "</td>");//TotalPayment
              	out.println("	<td>" + vecTemp.get(9).toString() + "</td>");//last_transaction_date              	
              	out.println("	<td align=center>" + ((Integer)vecTemp.get(10)).toString() + "</td>");
              	out.println("	<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(11).toString())  + "</td>");//total_recharge
              	out.println("	<td align=right>" + NumberUtil.formatCurrency( vecTemp.get(12).toString() ) + "</td>");//available_credit
              	out.println("	<td>" + vecTemp.get(13).toString() + "</td>");
              	out.println("	<td>" + vecTemp.get(14).toString() + "</td>");
              	out.println("	<td>" + vecTemp.get(15).toString() + "</td>");
              	out.println("</tr>");
                if (intEvenOdd == 1)
                	intEvenOdd = 2;
                else
                	intEvenOdd = 1;
                           	
            }
%>
            <tfoot>
              <tr class=row<%= intEvenOdd %>>

              </tr>
            </tfoot>
          </table>
<%
        }
        else
        if (intRecordCount == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
        	System.out.println("Order: " + sortString);
%>
          <SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "String","String","String","String","String","String","String","String","Number", "DateTime", "Number","Number","Number","String","String","String"],0,false,false);
                    
  -->
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
