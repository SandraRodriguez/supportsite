<%@ page import="java.util.*,
         com.debisys.utils.*,
         java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 4;
    int section_page = 20;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>

<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%    Vector warningSearchResults = new Vector();
    Vector vecSearchResults = new Vector();
    Hashtable searchErrors = null;
    int intRecordCount = 0;
    int intPage = 1;
    int intPageSize = 50;
    int intPageCount = 1;
    String message = "";

    //this flag means if the user is in International Default
    boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

    boolean viewReferenceCard = false;
    if (isInternationalDefault) {
        viewReferenceCard = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
    }

    boolean showAccountIdQRCode = false;
    showAccountIdQRCode = SessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);

    if (request.getParameter("download") != null) {
        String sURL = "";
        TransactionReport.setStartDate(request.getParameter("startDate"));
        TransactionReport.setEndDate(request.getParameter("endDate"));
        TransactionReport.setMinAmount(request.getParameter("minAmount"));
        TransactionReport.setMaxAmount(request.getParameter("maxAmount"));
        sURL = TransactionReport.downloadPromoDetails(application, SessionData);
        response.sendRedirect(sURL);
        return;
    }
    if (request.getParameter("search") != null) {
        if (request.getParameter("page") != null) {
            try {
                intPage = Integer.parseInt(request.getParameter("page"));
            } catch (NumberFormatException ex) {
                intPage = 1;
            }
        }

        if (intPage < 1) {
            intPage = 1;
        }

        if (TransactionReport.validateDateRange(SessionData)) {
            TransactionReport.setStartDate(request.getParameter("startDate"));
            TransactionReport.setEndDate(request.getParameter("endDate"));
            TransactionReport.setMinAmount(request.getParameter("minAmount"));
            TransactionReport.setMaxAmount(request.getParameter("maxAmount"));
            vecSearchResults = TransactionReport.getPromoDetails(intPage, intPageSize, SessionData, application, true);
            intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
            vecSearchResults.removeElementAt(0);
            if (intRecordCount > 0) {
                intPageCount = (intRecordCount / intPageSize);
                if ((intPageCount * intPageSize) < intRecordCount) {
                    intPageCount++;
                }
            }
        }
        else {
            searchErrors = TransactionReport.getErrors();
        }
    }
%>
<%@ include file="/includes/header.jsp" %>
<%
    boolean bool_custom_config_mexico = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
    boolean bool_custom_config_default = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

    boolean bool_deploy_type_intl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
    boolean bool_perm_enable_tax_calc = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);

    if (vecSearchResults != null && vecSearchResults.size() > 0) {
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
    <%
        }
    %>
<SCRIPT LANGUAGE="JavaScript">
    var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ", "< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
    <tr>
        <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td class="formAreaTitle" align=left width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.promo_transactions", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <%
                            if (vecSearchResults != null && vecSearchResults.size() > 0) {
                        %>
                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                            <%
                                Vector vTimeZoneData = null;
                                if (SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT)) {
                                    vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
                                }
                                else {
                                    vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
                                }
                            %>
                            <tr class="main">
                                <td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/>
                                </td>
                            </tr>
                            <tr>
                                <td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found", SessionData.getLanguage()) + " "%><%
                                    if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals("")) {
                                        out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                                    }
                                    %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{Integer.toString(intPage), Integer.toString(intPageCount)}, SessionData.getLanguage())%>
                                </td>
                            </tr> 			
                            <tr>
                                <td align=left class="main" nowrap>
                                    <form method=post action="admin/reports/transactions/promo_transactions.jsp">
                                        <input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
                                        <input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
                                        <input type="hidden" name="minAmount" value="<%=TransactionReport.getMinAmount()%>">
                                        <input type="hidden" name="maxAmount" value="<%=TransactionReport.getMaxAmount()%>">
                                        <input type="hidden" name="search" value="y">
                                        <input type="hidden" name="download" value="y">
                                        <input type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download", SessionData.getLanguage())%>">
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td align=right class="main" nowrap>
                                    <%
                                        if (intPage > 1) {
                                            out.println("<a href=\"admin/reports/transactions/promo_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() + "&page=1\">First</a>&nbsp;");
                                            out.println("<a href=\"admin/reports/transactions/promo_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() + "&page=" + (intPage - 1) + "\">&lt;&lt;Prev</a>&nbsp;");
                                        }

                                        int intLowerLimit = intPage - 12;
                                        int intUpperLimit = intPage + 12;

                                        if (intLowerLimit < 1) {
                                            intLowerLimit = 1;
                                            intUpperLimit = 25;
                                        }

                                        for (int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) {
                                            if (i == intPage) {
                                                out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                                            }
                                            else {
                                                out.println("<a href=\"admin/reports/transactions/promo_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() + "&page=" + i + "\">" + i + "</a>&nbsp;");
                                            }
                                        }

                                        if (intPage <= (intPageCount - 1)) {
                                            out.println("<a href=\"admin/reports/transactions/promo_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() + "&page=" + (intPage + 1) + "\">Next&gt;&gt;</a>&nbsp;");
                                            out.println("<a href=\"admin/reports/transactions/promo_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + "&minAmount=" + TransactionReport.getMinAmount() + "&maxAmount=" + TransactionReport.getMaxAmount() + "&page=" + (intPageCount) + "\">Last</a>");
                                        }
                                    %>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="1" cellpadding="2" class="sort-table" id="t1">
                            <thead>
                                <tr class="SectionTopBorder">
                                    <td class=rowhead2 nowrap>#</td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.tran_no", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.term_no", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.dba", SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.merchant_id", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.date", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%
                                        if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                    %>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.phys_state", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%
                                        }
                                    %>               
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.city", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.county", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.reference", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.ref_no", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.recharge", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%
                                        /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                        if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                    %>               	  <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%               	 	}
                                    else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                    %>					  <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%                	 }
                                    }
                                    else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                    %>                   	
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%                  }
                                    else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                    %>		              <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%                 	}
                                    else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                    %>					  <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%                  }
                                    else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                    %>                    <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%                  }
                                    %>				


                                    <%
                                        }
                                        /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                    %>                    				              	

                                    <%
                                        //DBSY-905
                                        if (isInternationalDefault) {
                                    %>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.bonus", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.total_recharge", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%
                                        if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                                    %>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.netAmount", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxAmount", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxpercentage", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.taxtype", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%
                                            }
                                        }
                                    %>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.balance", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.product", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.description", SessionData.getLanguage()).toUpperCase()%></td>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.control_no", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%
                                        if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                    %>
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.authorization_number", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%
                                        }

                                        if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application) && isInternationalDefault) {
                                    %>                 
                                    <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.invoiceno", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%
                                        }
                                    %>
                                    <%if (viewReferenceCard) {%>
                                    <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.DTU1204", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%}%>

                                    <%if (showAccountIdQRCode) {%>
                                    <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.reference", SessionData.getLanguage()).toUpperCase()%></td>
                                    <%}%>
                                </tr>

                                </tr>
                            </thead>
                            <%
                                int intCounter = 1;

                                String sPhysState = "", sAuthorizationNo = "";

                                Iterator it = vecSearchResults.iterator();
                                int intEvenOdd = 1;

                                while (it.hasNext()) {
                                    Vector vecTemp = null;
                                    vecTemp = (Vector) it.next();

                                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                        sPhysState = "<td nowrap>" + vecTemp.get(14).toString() + "</td>";
                                    }
                                    else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                        sAuthorizationNo = "<td nowrap>" + vecTemp.get(13).toString() + "</td>";
                                    }

                                    String[] productDescription = vecTemp.get(11).toString().split("\\/");

                                    out.println("<tr class=row" + intEvenOdd + ">"
                                            + "<td nowrap>" + intCounter++ + "</td>"
                                            + "<td nowrap>" + vecTemp.get(0) + "</td>"
                                            + "<td nowrap>" + vecTemp.get(1) + "</td>"
                                            + "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString()) + "</td>"
                                            + "<td nowrap>" + vecTemp.get(3) + "</td>"
                                            + "<td nowrap>" + vecTemp.get(4) + "</td>"
                                            + sPhysState
                                            + "<td nowrap>" + vecTemp.get(5) + "</td>"
                                            + "<td nowrap>" + vecTemp.get(20) + "</td>"
                                            + "<td nowrap>" + vecTemp.get(6) + "</td>"
                                            + "<td nowrap>" + vecTemp.get(7) + "</td>"
                                            + "<td nowrap>" + vecTemp.get(8) + "</td>"
                                            + //DBSY-905 Tax Calculation
                                            //amount
                                            "<td nowrap>" + vecTemp.get(9) + "</td>");

                                    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                    if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                                        if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(27).toString()) + "</td>"); //iso
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                                }
                                                else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(27).toString()) + "</td>"); //iso
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //agente
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //subagente
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                                }
                                            }
                                            else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //agente
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //subagente
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                            }
                                            else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //subagente
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                            }
                                            else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                            }
                                            else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                            }
                                        }
                                    }
                                    else {
                                        if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(27).toString()) + "</td>"); //iso
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                                }
                                                else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(27).toString()) + "</td>"); //iso
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //agente
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //subagente
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                    out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                                }
                                            }
                                            else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(25).toString()) + "</td>"); //agente
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //subagente
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                            }
                                            else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(26).toString()) + "</td>"); //subagente
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                            }
                                            else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(24).toString()) + "</td>"); //rep
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                            }
                                            else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                                out.println("<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(23).toString()) + "</td>"); //merchant
                                            }
                                        }
                                    }
                                    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/

                                    if (isInternationalDefault) {
                                        //bonus
                                        out.println("<td nowrap>" + vecTemp.get(16) + "</td>"
                                                + //total Recharge
                                                "<td nowrap>" + vecTemp.get(17) + "</td>");

                                        if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                                            //net amount 
                                            out.println("<td nowrap>" + vecTemp.get(18) + "</td>"
                                                    + //tax amount
                                                    "<td nowrap>" + vecTemp.get(19) + "</td>"
                                                    + // taxpercentage
                                                    "<td nowrap>" + NumberUtil.formatAmount(vecTemp.get(21).toString()) + "%</td>"
                                                    + // taxtype
                                                    "<td nowrap>" + vecTemp.get(22) + "</td>");
                                        }
                                    }

                                    //bal
                                    out.println(
                                            "<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>"
                                            + "<td nowrap align=\"right\">" + productDescription[0] + "</td>"
                                            + "<td nowrap align=\"right\">" + productDescription[1] + "</td>"
                                            + "<td nowrap align=\"right\">" + vecTemp.get(12) + "</td>"
                                            + sAuthorizationNo);

                                    if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application) && isInternationalDefault) {
                                        //DBSY-919 Invoice Number Field
                                        out.println("<td>" + vecTemp.get(15) + "</td>");
                                    }

                                    if (viewReferenceCard) {
                                        out.println("<td>" + vecTemp.get(28) + "</td>");
                                    }
                                    
                                    if (vecTemp.size() >= 30 && showAccountIdQRCode) {
                                        out.println("<td>" + vecTemp.get(29) + "</td>");
                                    }
                                    else if (showAccountIdQRCode) {
                                        out.println("<td></td>");
                                    }
                                    out.println("</tr>");

                                    if (intEvenOdd == 1) {
                                        intEvenOdd = 2;
                                    }
                                    else {
                                        intEvenOdd = 1;
                                    }
                                }
                                vecSearchResults.clear();
                            %>
                        </table>
                        <%
                            }
                            else if (intRecordCount == 0 && request.getParameter("search") != null && searchErrors == null) {
                                out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                            }

                            if (vecSearchResults != null) {
                        %>
                        <SCRIPT type="text/javascript">
                            var stT1 = new SortROC(document.getElementById("t1"),
                                    ["None", "Number", "Number", "CaseInsensitiveString", "Number", "Date", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "CaseInsensitiveString", "Number"], 0, false, false);
                        </SCRIPT>
                        <%
                            }
                        %>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>