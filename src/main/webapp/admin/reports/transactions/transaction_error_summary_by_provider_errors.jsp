<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.reports.ReportsUtil,
                 com.debisys.utils.ColumnReport,
                 com.debisys.utils.NumberUtil, com.debisys.schedulereports.ScheduleReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 4;
  int section_page = 18;
  String products = "";
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();
  String noteTimeZone = "";
  String testTrx = "";  
  String keyLanguage = "jsp.admin.reports.titleProvErrors";
  String titleReport = Languages.getString(keyLanguage,SessionData.getLanguage());
  
  
  Vector vTimeZoneData = null;
  if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
  {
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
  }
  else
  {
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  }
  noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
  String titleReportResults = "";
  if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
  { 
     titleReportResults = Languages.getString(keyLanguage,SessionData.getLanguage()) + " "+Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +HTMLEncoder.encode( TransactionReport.getStartDateFormatted() );
	 titleReportResults = titleReportResults + " "+Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " "+ HTMLEncoder.encode( TransactionReport.getEndDateFormatted() );                     
  }			
			
  int intErrorsSum = 0;

  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
    	SessionData.setProperty("start_date", request.getParameter("startDate"));
      	SessionData.setProperty("end_date", request.getParameter("endDate"));

		String strProviders[] = request.getParameterValues("ProvidersList");
		if (strProviders != null)
		{
			TransactionReport.setProviderIDs(strProviders);
		}
		if (TransactionReport.validateProviderList(SessionData))
		{
			String strProviderErrors[] = request.getParameterValues("ErrorsList");
			if (strProviderErrors != null)
			{
				TransactionReport.setProviderErrorCodes(strProviderErrors);
			}
			if (TransactionReport.validateErrorList(SessionData))
			{
				String strProductIds[] = request.getParameterValues("pids");
				if (strProductIds != null)
				{
					TransactionReport.setProductIds(strProductIds);
					products = String.valueOf(strProductIds);
				}
								
				//////////////////////////////////////////////////////////////////
				//HERE WE DEFINE THE REPORT'S HEADERS 
				headers = getHeadersTrxSummErrorByErrorProviders( SessionData );
				//////////////////////////////////////////////////////////////////
				//////////////////////////////////////////////////////////////////
				if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
				{
				  		//TO SCHEDULE REPORT
				  		titles.add(noteTimeZone);
			      		titles.add(testTrx);
				  		titles.add(titleReport);
						TransactionReport.getTransactionErrorSummaryByProviderErrors(SessionData, DebisysConstants.SCHEDULE_REPORT, headers, titles, false);
						ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
						if (  scheduleReport != null  )
						{
							scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
							scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
							scheduleReport.setTitleName( keyLanguage );   
						}	
						response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
				}
                                  else if(request.getParameter("isInternal") != null) {      	          
                                    if(request.getParameter("isInternal") == "true" || request.getParameter("isInternal").equals("true")) {
                                        vecSearchResults = TransactionReport.getTransactionErrorSummaryByProviderErrors(SessionData, DebisysConstants.EXECUTE_REPORT, null, null, true);
                                    }
                                  }
				else
				{      	
			    	vecSearchResults = TransactionReport.getTransactionErrorSummaryByProviderErrors(SessionData, DebisysConstants.EXECUTE_REPORT, null, null, false);
			    }
								
				
			}
			else
			{
				searchErrors = TransactionReport.getErrors();
			}
		}
		else
		{
			searchErrors = TransactionReport.getErrors();
		}
    }
    else
    {
    	searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>

<%!public static ArrayList<ColumnReport> getHeadersTrxSummErrorByErrorProviders(SessionData sessionData )
   {
   
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add(new ColumnReport("H2HProviderResultCode", Languages.getString("jsp.admin.reports.error_code", sessionData.getLanguage()).toUpperCase(), Integer.class, false));
		headers.add(new ColumnReport("provider_error_message", Languages.getString("jsp.admin.reports.error_type", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("error_qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		
		//ColumnReport flagToNotSummVoids = new ColumnReport("typegroup", "typegroup", Integer.class, false);
		//flagToNotSummVoids.setPutInTemplate(false);
		//headers.add(flagToNotSummVoids);
		
		headers.add(new ColumnReport("totalCount", Languages.getString("jsp.admin.reports.total_errors", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		
		//$V{errorPerc}
		ColumnReport errorPerc = new ColumnReport("errorPerc", "Error %", Double.class, false);
		errorPerc.setValueExpressionWhenIsCalculate("($F{error_qty}/$F{totalCount})*100");
		headers.add(errorPerc);
		
		return headers;		        
	}  
  %>
  
  <table border="0" cellpadding="0" cellspacing="0" width="600">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" >
        &nbsp;
        <%= titleReport.toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea">
<%

        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }


        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr class="main"><td nowrap colspan="2"><%=noteTimeZone%><br/><br/></td></tr>
            <tr>
              <td class="main"><%= titleReportResults %><br>
            <table>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>
                  <form target="blank" method=post action="admin/reports/transactions/export_transaction_error_summary_by_provider_errors.jsp" onsubmit="document.getElementById('btnSubmit').disabled=true;">
                    <input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
                    <input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
                    <input type=hidden name="merchant_ids" value="<%=TransactionReport.getMerchantIds()%>">                    
                    <input type=hidden name="provider_ids" value="<%=TransactionReport.getProviderIDs()%>">
                    <input type=hidden name="provider_errors" value="<%=TransactionReport.getProviderErrorCodes()%>">
                     <%
                    if (products != null && !products.trim().equals(""))
    				{%>
    					<input type=hidden name="products" value="<%=TransactionReport.getProductIds()%>">
    				<%}
    				%>
                    <input id=btnSubmit type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                  </form>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <form target="blank" method=post action="admin/reports/transactions/print_transaction_error_summary_by_provider_errors.jsp">
                    <input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
                    <input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
                    <input type=hidden name="merchant_ids" value="<%=TransactionReport.getMerchantIds()%>">
                    <input type=hidden name="provider_ids" value="<%=TransactionReport.getProviderIDs()%>">
                    <input type=hidden name="provider_errors" value="<%=TransactionReport.getProviderErrorCodes()%>">
                     <%
                    if (products != null && !products.trim().equals(""))
    				{%>
    					<input type=hidden name="products" value="<%=TransactionReport.getProductIds()%>">
    				<%}
    				%>
                    <input type=submit value="<%=Languages.getString("jsp.admin.reports.print",SessionData.getLanguage())%>">
                  </form>
                </td>
              </tr>
            </table><BR><BR>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2 width="5%">
                  #
                </td>
                <td class=rowhead2 nowrap width="15%">
                  <%= Languages.getString("jsp.admin.reports.error_code",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap width="40%">
                  <%= Languages.getString("jsp.admin.reports.error_type",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap width="15%">
                  <%= Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase() %>
                </td>                 
                <td class=rowhead2 nowrap width="15%">
                  <%= Languages.getString("jsp.admin.reports.total_errors",SessionData.getLanguage()).toUpperCase() %>
                </td>                
                <td class=rowhead2 nowrap width="15%">
                  <%= Languages.getString("jsp.admin.reports.error_percent",SessionData.getLanguage()).toUpperCase() %>
                </td>
              </tr>
            </thead>
<%
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;

            if (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector)it.next();
              intErrorsSum = Integer.parseInt(vecTemp.get(0).toString());
            }

            while (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector)it.next();
              int intTotalQty = Integer.parseInt(vecTemp.get(2).toString());
              int errors = Integer.parseInt(vecTemp.get(2).toString());
              double errors_percent = 0;
              if (intErrorsSum == 0)
              {
              	errors_percent = 100;
              }
              else
              {
                errors_percent = ((double)errors/intErrorsSum) * 100;
              }

              // conditional filter that modifies detail query for void errors cases
              String appendToURL = "";
              appendToURL += "&queryType=providerErrors";

              out.print("<tr class=row" + intEvenOdd + ">" +
              "<td nowrap>" + intCounter++ + "</td>" +
              "<td nowrap>" + vecTemp.get(0) + "</td>" +
              "<td nowrap>" + vecTemp.get(1) + "</td>" +
              "<td nowrap><a href=\"admin/reports/transactions/transaction_error_detail_by_types.jsp?startDate=" + URLEncoder.encode(
              TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") +
              "&resultCode=" + vecTemp.get(0).toString() +"&products="+TransactionReport.getProductIds()+"&merchantIds=" + TransactionReport.getMerchantIds() + "&providersIds="+TransactionReport.getProviderIDs()+ appendToURL
              + "\" target=\"_blank\">" + vecTemp.get(2) + "</td>" +
              "<td nowrap>" + intErrorsSum + "</td>" +
              "<td nowrap align=\"right\">" + NumberUtil.formatAmount(Double.toString(errors_percent)) + "%</td></tr>");

              if (intEvenOdd == 1)
              {
                intEvenOdd = 2;
              }
              else
              {
                intEvenOdd = 1;
              }
            }
%>
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">

                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "Number", "Number"],0,false,false);
  -->

          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
