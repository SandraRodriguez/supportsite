<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.ColumnReport, com.debisys.schedulereports.ScheduleReport,
                 com.debisys.reports.ReportsUtil" %>
                 
<%@page import="com.debisys.utils.TimeZone"%>
<%@page import="net.sf.jasperreports.engine.type.CalculationEnum"%>
<%
  int section      = 4;
  int section_page = 20;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  Vector warningSearchResults = new Vector();
  String    sortString       = "";
  boolean   bUseTaxValue     = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns

  ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
  ArrayList<String> titles = new ArrayList<String>();
  String noteTimeZone = "";
  String testTrx = Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage());  
  String keyLanguage = "jsp.admin.reports.title36";
  String titleReport = titleReport = Languages.getString( keyLanguage , SessionData.getLanguage() );;
  String subTitleReport = Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_type_summary_report",SessionData.getLanguage());
  
  Vector vTimeZoneData = null;
  if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
  {
	vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
  }
  else
  {
	vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
  }
	
	
  if ( request.getParameter("download") != null )
  {
  	  String sURL = "";
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));
      TransactionReport.setTerminalTypes(request.getParameter("terminalTypes"));
      TransactionReport.setProductIds(request.getParameter("products"));
      TransactionReport.setMerchantIds(request.getParameter("merchants"));
	  TransactionReport.setMinAmount(request.getParameter("minAmount"));
	  TransactionReport.setMaxAmount(request.getParameter("maxAmount"));
      if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
              (request.getParameter("chkUseTaxValue") != null)
            )
         {
           bUseTaxValue = true;
      	  sURL = TransactionReport.downloadTerminalSummary(application, SessionData, true);
         }
         else
         {
         	  sURL = TransactionReport.downloadTerminalSummary(application, SessionData, false);
         }
      response.sendRedirect(sURL);
	  return;
  }
  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));

      String strTerminalTypes[] = request.getParameterValues("terminals");
      String strProducts[] = request.getParameterValues("pids");
      String strMerchants[] = request.getParameterValues("merchantIds");

      if (strTerminalTypes != null)
      {
        TransactionReport.setTerminalTypes(strTerminalTypes);
      }
      if (strProducts != null)
      {
        TransactionReport.setProductIds(strProducts);
      }
      if (strMerchants != null)
      {
        TransactionReport.setMerchantIds(strMerchants);
      }
      if ( request.getParameter("txtMinAmount") != null )
      {
    	  TransactionReport.setMinAmount(request.getParameter("txtMinAmount"));
      }
      if ( request.getParameter("txtMaxAmount") != null )
      {
    	  TransactionReport.setMaxAmount(request.getParameter("txtMaxAmount"));
      }
      
      if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
           (request.getParameter("chkUseTaxValue") != null)
         )
      {
        bUseTaxValue = true;
        vecSearchResults = TransactionReport.getTerminalSummaryMx(SessionData, application);
        headers = getHeadersTrxSummByTerminalType( SessionData, application, strAccessLevel, strDistChainType, bUseTaxValue );
      }
      else
      {
      	  //////////////////////////////////////////////////////////////////
		  //HERE WE DEFINE THE REPORT'S HEADERS 
		  headers = getHeadersTrxSummByTerminalType( SessionData, application, strAccessLevel, strDistChainType, bUseTaxValue );
		  //////////////////////////////////////////////////////////////////
		  //////////////////////////////////////////////////////////////////
	      
	      if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
		  {
				//TO SCHEDULE REPORT
				titles.add(noteTimeZone);								
				titles.add(titleReport);		
		        titles.add(testTrx);
				TransactionReport.getTerminalSummary(SessionData,application, DebisysConstants.SCHEDULE_REPORT, headers, titles );
				ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
				if (  scheduleReport != null  )
				{
					scheduleReport.setStartDateFixedQuery( TransactionReport.getStartDate() );
					scheduleReport.setEndDateFixedQuery( TransactionReport.getEndDate() );
					scheduleReport.setTitleName( keyLanguage );
				}
				response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
		  }
		  else
		  {
			 vecSearchResults = TransactionReport.getTerminalSummary(SessionData,application, DebisysConstants.EXECUTE_REPORT, null, null );
	  		 warningSearchResults = TransactionReport.getWarningMessage();
		  }      	
      }
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>

<%!public static ArrayList<ColumnReport> getHeadersTrxSummByTerminalType(SessionData sessionData, ServletContext application, String strAccessLevel,
																		 String strDistChainType, boolean bUseTaxValue )
	{
            boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(sessionData, application);
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
			
		if (!(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
					DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
		{//If we are not in EC
			headers.add(new ColumnReport("terminal_type", Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_type", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
        headers.add(new ColumnReport("terminal_description", Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_description", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("Registered", Languages.getString("jsp.admin.reports.RegisteredTerminals", sessionData.getLanguage()).toUpperCase(), Double.class, true));
        headers.add(new ColumnReport("Active", Languages.getString("jsp.admin.reports.ActiveTerminals", sessionData.getLanguage()).toUpperCase(), Double.class, true));
        headers.add(new ColumnReport("qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Double.class, true));
        headers.add(new ColumnReport("total_sales", Languages.getString("jsp.admin.reports.recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
         //total_recharge
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) 
		{
           	if(strAccessLevel.equals(DebisysConstants.ISO))
           	{
          	 	if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
          	 	{
           		   headers.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           		   headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           		   headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           		}
           	 	else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
           	 	{
           	 	   headers.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 	   headers.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 	   headers.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 	   headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 	   headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 	}
             }
             else if (strAccessLevel.equals(DebisysConstants.AGENT))
             {
              	headers.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 	headers.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 	headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 	headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	 }
             else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
             {
               headers.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	   headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	   headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
             }
             else if (strAccessLevel.equals(DebisysConstants.REP))
             {
               headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
           	   headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
             }
             else if(strAccessLevel.equals(DebisysConstants.MERCHANT))
             {
               headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
             }               
		}
		
		if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
						DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
			headers.add(new ColumnReport("total_bonus", Languages.getString("jsp.admin.reports.bonus", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			headers.add(new ColumnReport("total_recharge", Languages.getString("jsp.admin.reports.total_recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
              
		}
        if ( bUseTaxValue )
        {
           	headers.add(new ColumnReport(" ", Languages.getString("jsp.admin.reports.total", sessionData.getLanguage()).toUpperCase()+
              		" "+Languages.getString("jsp.admin.reports.minusvat",sessionData.getLanguage()).toUpperCase(), String.class, false));                
        }
	   	if(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
  				DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && 
  					DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
			ColumnReport tax_total_sales = new ColumnReport("tax_total_sales", Languages.getString("jsp.admin.reports.netAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true);
			headers.add(tax_total_sales);			
			headers.add(new ColumnReport("taxamount", Languages.getString("jsp.admin.reports.taxAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
        }        		
		ColumnReport averageAmount = new ColumnReport("averageAmount", Languages.getString("jsp.admin.reports.avg_per_trans", sessionData.getLanguage()).toUpperCase(), Double.class, false);
        averageAmount.setValueExpressionWhenIsCalculate("$F{total_sales}/$F{qty}");
		headers.add( averageAmount );
        
        if (isIntlAndHasDataPromoPermission) {
            headers.add(new ColumnReport("PromoData", Languages.getString("jsp.reports.reports.transactions.form.table.column.name.promoData", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
        
        return headers;
       
	}
  %>
  
  <table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= titleReport.toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
          Enumeration enum1 = searchErrors.keys();
          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }
          out.println("</font></td></tr></table>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr class="main"><td nowrap colspan="2"><%= noteTimeZone %><br/><br/></td></tr>
            <tr>
          
            	<% 
                boolean showWarnings = false;
                if ( showWarnings && warningSearchResults.size()>0){ %>
				<tr>
				<td class="main" style="color:red">
						<% if(   (TransactionReport.checkfortaxtype(SessionData) && warningSearchResults.size()>1)
								||  (!TransactionReport.checkfortaxtype(SessionData))){
				%>
					<%=Languages.getString("jsp.admin.reports.warning_range_date",SessionData.getLanguage())%><br/>
					<%=Languages.getString("jsp.admin.reports.warning_range_date_c",SessionData.getLanguage())%><br/> 
				<% } %>
					<% for ( int i=0; i<warningSearchResults.size(); i++){  %> 
						<%= warningSearchResults.get(i).toString()%> <br/>
					<% } %>
				</td>
				</tr>
                <%} %>
			 
			 <tr>
              <td class="main"><%= subTitleReport %>
<%
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                  out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
%>
                <br>
<%
                if (!TransactionReport.getMinAmount().equals("") && !TransactionReport.getMaxAmount().equals(""))
                {
                  out.println(Languages.getString("jsp.admin.ach.summary.amount",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getMinAmount()) + 
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getMaxAmount()));
                }
%>               <br> 
                <font color="#ff0000"><%= testTrx %></font>
              </td>
              <td class=main align=right valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
            <tr>
              <td align=left class="main" nowrap>
              <form method=post action="admin/reports/transactions/terminals_summary.jsp">
	              <input type="hidden" name="startDate" value="<%=TransactionReport.getStartDate()%>">
	              <input type="hidden" name="endDate" value="<%=TransactionReport.getEndDate()%>">
	              <input type="hidden" name="terminalTypes" value="<%=TransactionReport.getTerminalTypes()%>">
	              <input type="hidden" name="products" value="<%=TransactionReport.getProductIds()%>">
	              <input type="hidden" name="merchants" value="<%=TransactionReport.getMerchantIds()%>">
	              <input type="hidden" name="minAmount" value="<%=TransactionReport.getMinAmount()%>">
	              <input type="hidden" name="maxAmount" value="<%=TransactionReport.getMaxAmount()%>">
	              <% if(request.getParameter("chkUseTaxValue")!=null){%>
                	<INPUT TYPE=hidden NAME=chkUseTaxValue VALUE="<%=request.getParameter("chkUseTaxValue")%>">
     			<% }%>	              
     			<input type="hidden" name="download" value="y">
	              <input type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
              </form>
              </td>
            </tr>
          </table>
          <table>
                <tr>
                    <td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
                </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>
				<%
				for(ColumnReport columnReport : headers)
				{				  	
				  	if ( columnReport.isPutInTemplate() )
				  	{
				  		String columnName = columnReport.getLanguageDescription();
					%>				
						<td class=rowhead2 nowrap><%= columnName.toUpperCase()%></td>
					<%
					}
				}
				%>
              </tr>
            </thead>
<%
            double   dblTotalSalesSum         = 0;
            double   dblVATTotalSalesSum      = 0;
            double  dblTaxSum 				  = 0;
            double  dblTotalBonusSum 		  = 0;
            double  dblTotalRechargesSum      = 0;
            int      intTotalQtySum           = 0;
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;
            int      nRegTerminalCount        = 0;
            int      nActiveTerminalCount     = 0;
            
            double dblIsoSum=0;
            double dblMerchantSum=0;
            double dblRepSum=0;
            double dblAgentSum=0;
            double dblSubAgentSum=0;
            boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(SessionData, application);
            boolean isEnabledTaxCalculationInSsReports = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
              
            
            Vector vecTemp = null;
            
            if (it.hasNext())
            {
              vecTemp = (Vector)it.next();
              dblTotalSalesSum = Double.parseDouble(vecTemp.get(0).toString());
              if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				{
              dblTotalBonusSum = Double.parseDouble(vecTemp.get(1).toString());
              dblTotalRechargesSum = Double.parseDouble(vecTemp.get(2).toString());
              }
              if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
              {
                dblVATTotalSalesSum = Double.parseDouble(vecTemp.get(3).toString());
              }
              if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
              {
              dblTaxSum=dblTotalSalesSum-dblVATTotalSalesSum;
              }
            } 
              
              
            while (it.hasNext())
            {
              vecTemp = null;
              vecTemp = (Vector)it.next();
              /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
               if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
               dblMerchantSum+= Double.parseDouble(vecTemp.get(9).toString());
               dblRepSum+=Double.parseDouble(vecTemp.get(10).toString());
               dblAgentSum+=Double.parseDouble(vecTemp.get(11).toString());
               dblSubAgentSum+=Double.parseDouble(vecTemp.get(12).toString());
               dblIsoSum+=Double.parseDouble(vecTemp.get(13).toString());
               }
              /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
               
              int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
              double dblTotalSales  = Double.parseDouble(vecTemp.get(3).toString());
              double dblTotalRecharges = 0 ,dblBonusTotalSales = 0;
              if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				{
               dblBonusTotalSales         = Double.parseDouble(vecTemp.get(4).toString());
               dblTotalRecharges         = Double.parseDouble(vecTemp.get(5).toString());
              }
               
              double dblVATTotalSales      = 0;
              double dbTemp =  dblTotalSales + intTotalQty;
              double dblTaxAmount = 0 ;
              double averageAmount = 0 ;
              if(dbTemp > 0)
              {
              	averageAmount         = dblTotalSales/intTotalQty;
              }
              String mxUseTaxValue         = "";
              String sTerminalTypeID       = "";
              String sTerminalTypeCounts   = "";
              intTotalQtySum = intTotalQtySum + intTotalQty;

              if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
              			DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
              					DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
              {
              //TransactionReport.cat.debug("dblVATTotalSales" + Double.parseDouble(vecTemp.get(8).toString()));
                dblVATTotalSales = Double.parseDouble(vecTemp.get(8).toString());
                mxUseTaxValue = "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue");
              }
              if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
              		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
              			DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
              {
                dblTaxAmount=dblTotalSales-dblVATTotalSales;
              }
			if (!(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
					DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
			{//If we are not in EC
				sTerminalTypeID = "<td nowrap>" + vecTemp.get(0) + "</td>";
			}//End of if we are not in EC
			
				sTerminalTypeCounts = "<td align=right nowrap>" + vecTemp.get(6) + "</td><td align=right nowrap>" + vecTemp.get(7) + "</td>";
				nRegTerminalCount += Integer.parseInt(vecTemp.get(6).toString());
				nActiveTerminalCount += Integer.parseInt(vecTemp.get(7).toString());
              	String ultTrans = "";
              	Double dblAvgAmn = Double.parseDouble("0");
              	if(intTotalQty > 0)
              	{
              		ultTrans = "<a href=\"admin/reports/transactions/terminals_transactions.jsp?startDate=" + URLEncoder.encode(
                      TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), 
                      "UTF-8") + "&search=y&terminalType=" + vecTemp.get(0) + "&terminalDesc=" + URLEncoder.encode((String)vecTemp.get(1),"UTF-8" ) +
                      mxUseTaxValue +
                      "&products=" + TransactionReport.getProductIds() + 
                      "&merchants=" + TransactionReport.getMerchantIds() + 
                      "&minAmount=" + TransactionReport.getMinAmount() + 
                      "&maxAmount=" + TransactionReport.getMaxAmount() + 
                      "\" target=\"_blank\">" + NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a>";
                      
                      System.out.println("ultTrans >>"+ultTrans);
                      dblAvgAmn = averageAmount;
              	}
              	else
              	{
              		ultTrans = NumberUtil.formatCurrency(Double.toString(dblTotalSales));
              		dblAvgAmn = dblTotalSales;
              	}
              out.print("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + sTerminalTypeID +
            		  "<td>" + vecTemp.get(1) + "</td>" + sTerminalTypeCounts + "<td align=right>" + intTotalQty + "</td>" + 
                      "<td align=right>" + ultTrans + "</td>");
                      
              /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
	                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
	                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
	                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
	                     		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(13).toString())+ "</td>"); //iso
	                     		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(10).toString())+ "</td>"); //rep
	                     		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(9).toString())+ "</td>"); //merchant
	                     	 }
	                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
	                     	 	out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(13).toString())+ "</td>"); //iso
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(11).toString())+ "</td>"); //agente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(12).toString())+ "</td>"); //subagente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(10).toString())+ "</td>"); //rep
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(9).toString())+ "</td>"); //merchant
	                     	 }
                         	}
                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(11).toString())+ "</td>"); //agente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(12).toString())+ "</td>"); //subagente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(10).toString())+ "</td>"); //rep
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(9).toString())+ "</td>"); //merchant
                         	}
                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(12).toString())+ "</td>"); //subagente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(10).toString())+ "</td>"); //rep
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(9).toString())+ "</td>"); //merchant
                         	}
                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(10).toString())+ "</td>"); //rep
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(9).toString())+ "</td>"); //merchant
                         	}
                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(vecTemp.get(9).toString())+ "</td>"); //merchant
                         	}
                        }  
                    	/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                      if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                      		DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                      {
                      	 out.print( "<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblBonusTotalSales)) 
                      		+ "</td>" + "<td align=right>" 
                      		+ NumberUtil.formatCurrency(Double.toString(dblTotalRecharges)) + "</td>" );
                      }
                      
                      out.print("" + 
                      ((bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& 
                      DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
                       && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
                       ?"<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATTotalSales)) + "</td>":""));
                      
                      
                     if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                      		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                      			DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                     {
                        out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAmount)) + "</td>");
                     }
                      out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAvgAmn))  + "</td>");
                    if (isIntlAndHasDataPromoPermission) {
                        try {
                            out.println("<td align=right>" + vecTemp.get(14) + "</td>");
                        } catch (java.lang.ArrayIndexOutOfBoundsException arrayError) {}

                    } 

              out.println("</tr>");

              if (intEvenOdd == 1)
              {
                intEvenOdd = 2;
              }
              else
              {
                intEvenOdd = 1;
              }
            }
            double dbTempSum =  intTotalQtySum + intTotalQtySum;
            double averageAmountTot =0;
            if(dbTempSum > 0)
            {
            	averageAmountTot = dblTotalSalesSum/intTotalQtySum;
            }
%> 
			<tfoot>
              <tr class=row<%= intEvenOdd %>>
<%
        	String sTerminalTypeID = "2";
			if (!(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
			{//If we are not in EC
				sTerminalTypeID = "3";
			}//End of if we are not in EC
%>
                <td colspan=<%=sTerminalTypeID%> align=right>
                  <%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                </td>
                <td align=right>
                  <%=nRegTerminalCount%>
                </td>
                <td align=right>
                  <%=nActiveTerminalCount%>
                </td>
                <td align=right>
                  <%= intTotalQtySum %>
                </td>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %>
                </td>
<%                
                /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
	                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
	                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
	                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
	                     		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblIsoSum))+ "</td>"); //iso
	                     		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblRepSum))+ "</td>"); //rep
	                     		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblMerchantSum))+ "</td>"); //merchant
	                     	 }
	                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
	                     	 	out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblIsoSum))+ "</td>"); //iso
                          		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentSum))+ "</td>"); //agente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblSubAgentSum))+ "</td>"); //subagente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblRepSum))+ "</td>"); //rep
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblMerchantSum))+ "</td>"); //merchant
	                     	 }
                         	}
                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
                          		out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAgentSum))+ "</td>"); //agente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblSubAgentSum))+ "</td>"); //subagente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblRepSum))+ "</td>"); //rep
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblMerchantSum))+ "</td>"); //merchant
                         	}
                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
                            		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblSubAgentSum))+ "</td>"); //subagente
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblRepSum))+ "</td>"); //rep
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblMerchantSum))+ "</td>"); //merchant
                         	}
                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblRepSum))+ "</td>"); //rep
                          		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblMerchantSum))+ "</td>"); //merchant
                         	}
                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
                         		out.println("<td align=right>" +NumberUtil.formatCurrency(Double.toString(dblMerchantSum))+ "</td>"); //merchant
                         	}
                        }  
                    	/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
 %>                   	                
                
                
 <%            if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
                     {%>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalBonusSum)) %>
                </td>
                
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(dblTotalRechargesSum	)) %>
                </td>        
<%				}
                if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) )
                {
%>
                <TD ALIGN="right"><%=NumberUtil.formatCurrency(Double.toString(dblVATTotalSalesSum))%></TD>
<%
                }
%>
<%
                if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
                {
%>
                <TD ALIGN="right"><%=NumberUtil.formatCurrency(Double.toString(dblTaxSum))%></TD>
<%
                }
%>
                <td align=right>
                  <%= NumberUtil.formatCurrency(Double.toString(averageAmountTot)) %>
                </td>
              </tr>
            </tfoot>
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
        	String sTerminalTypeID = "";
        	String sTerminalTypeCounts = "";
        	if(bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
        	{
        		sTerminalTypeCounts = "\"Number\",";
        	}
%>
          <SCRIPT type="text/javascript">
            
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),  
   ["None","CaseInsensitiveString","Number","Number","Number","Number","Number",<%=sTerminalTypeCounts%>"Number"],0,false,false);
  -->
            
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
