<%@ page import="java.util.*,
                 com.debisys.utils.DebisysConfigListener,
                 com.debisys.languages.Languages" %>
<%
int section=2;
int section_page=4;
String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 25;
int intPageCount = 1;
String merchantId = "";

int intTotalQty= 0;
if(request.getParameter("intTotalQty")!=null)
 intTotalQty = Integer.parseInt(request.getParameter("intTotalQty"));

if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
{
  merchantId = request.getParameter("merchantId");
}
 else
{
  merchantId = SessionData.getProperty("ref_id");
  Merchant.setMerchantId(merchantId);
}

String strUrlParams = "?merchantId=" + merchantId + "&startDate=" + Merchant.getStartDate() + "&endDate=" + Merchant.getEndDate();

if (merchantId != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }
  
  SessionData.setProperty("reportId", DebisysConstants.SC_MER_EXT_CRED_HISTORY);
  
  if(request.getParameter("user") == null)
  {
	if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
	{  
			// If columns change remember to change getUserMerchantCreditsMx
	      vecSearchResults = Merchant.getMerchantCreditsMx(intPage, intPageSize, SessionData, application, true);
	}
	else
	{
	      vecSearchResults = Merchant.getMerchantCredits(intPage, intPageSize, SessionData, application, true, DebisysConstants.EXECUTE_REPORT, null, null);
	}	
  } 
  else 
  {
	if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) 
	{  
		// If columns change remember to change getMerchantCreditsMx
	      vecSearchResults = Merchant.getUserMerchantCreditsMx(intPage, intPageSize, SessionData,application, true);
	}
	else
	{
	      vecSearchResults = Merchant.getUserMerchantCredits(intPage, intPageSize, SessionData,application, true);
	}
	strUrlParams += "&user=YES";
  }
  
    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    vecSearchResults.removeElementAt(0);
    if (intTotalQty>0)
    {
      intPageCount = (intTotalQty / intPageSize) + 1;
      if ((intPageCount * intPageSize) + 1 >= intTotalQty)
      {
        intPageCount++;
      }
    }
  }
  else
  {
   searchErrors = Merchant.getErrors();
  }



%>
<html>
  <head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.customers.merchants_credit_history.title",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="85%" background="../../images/top_blue.gif">
	<tr>
    <td width="18" height="20" align="left"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" align="left" width="2000"><%=Languages.getString("jsp.admin.customers.merchants_credit_history.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20" align="right"><img src="../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td class="main">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" class="main">
<table width="100%">
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
</table>
               </td>
              </tr>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=intTotalQty + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage())+"." + Languages.getString("jsp.admin.displaying", new Object []{Integer.toString(intPage), Integer.toString(intPageCount-1)},SessionData.getLanguage())%></td></tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"merchants_external_credit_history.jsp" + strUrlParams +"&intTotalQty="+intTotalQty+ "&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"merchants_external_credit_history.jsp" + strUrlParams +"&intTotalQty="+intTotalQty+ "&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"merchants_external_credit_history.jsp" + strUrlParams + "&intTotalQty="+intTotalQty+ "&page=" + i + "\">" + i + "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"merchants_external_credit_history.jsp" + strUrlParams + "&intTotalQty="+intTotalQty+ "&page=" + (intPage+1) + "\">"+Languages.getString("jsp.admin.next",SessionData.getLanguage())+"&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"merchants_external_credit_history.jsp" + strUrlParams + "&intTotalQty="+intTotalQty+ "&page=" + (intPageCount-1) + "\">"+Languages.getString("jsp.admin.last",SessionData.getLanguage())+"</a>");
              }

              %>
              </td>
            </tr>
            </table>
            <table cellspacing="1" cellpadding="2">
            <tr>
            <%
               if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {  
            	   out.println("<td class=rowhead2>#</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.user",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.user.dba",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.desc",SessionData.getLanguage()).toUpperCase() + "</td>");
                    
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.bank",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.reference_number",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.deposit_date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.concept",SessionData.getLanguage()).toUpperCase() + "</td>");
                    
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.paymentType",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.new_avail_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.commission",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.net_payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.extern.DBA",SessionData.getLanguage()).toUpperCase() + "</td>");
               }else{
            	   out.println("<td class=rowhead2>#</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.user",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.user.dba",SessionData.getLanguage()) + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.desc",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.new_avail_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.commission",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.net_payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                     out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.extern.DBA",SessionData.getLanguage()).toUpperCase() + "</td>");           
               }
            %> 
            </tr>
            <%
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  int count = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {  
                        out.println("<tr class=row" + intEvenOdd +">" +
                        		"<td>" + count+ "</td>" +   
                                    "<td>" + vecTemp.get(0)+ "</td>" +
                                    "<td>" + vecTemp.get(1) + "</td>" +
                                    "<td>" + vecTemp.get(11) + "</td>" +
                                    "<td>" + vecTemp.get(2) + "</td>" +
                                    "<td>" + vecTemp.get(3) + "</td>" +
                                    "<td>" + vecTemp.get(4) + "</td>" +
                                    // Date modified for localization - SW  
                                    "<td>" + vecTemp.get(5).toString() + "</td>" +
                                    "<td>" + vecTemp.get(6) + "</td>" +
                                    "<td>" + vecTemp.get(7) + "</td>" +
                                    "<td>" + vecTemp.get(12) + "</td>" + //paymentType 
                                    "<td>" + vecTemp.get(8) + "</td>" +
                                    "<td>" + vecTemp.get(9) + "</td>" + // Commission comes formated from the data retriver method
                                    "<td>" + vecTemp.get(10) + "</td>" +
                                    "<td>" + vecTemp.get(13) + "</td>" +
                            "</tr>");
                            //DateUtil.formatDateTime(vecTemp.get(5).toString())
                    }else{
                        out.println("<tr class=row" + intEvenOdd +">" +
                        		    "<td>" + count+ "</td>" +    
                                    "<td>" + vecTemp.get(0)+ "</td>" +
                                    "<td>" + vecTemp.get(1) + "</td>" +
                                    "<td>" + vecTemp.get(7) + "</td>" +
                                    "<td>" + vecTemp.get(2) + "</td>" +
                                    "<td>" + vecTemp.get(3) + "</td>" +
                                    "<td>" + vecTemp.get(4) + "</td>" +
                                    // formatting of COMMISSION delayed until presentation layer
                                    "<td>" + com.debisys.utils.NumberUtil.formatAmount(vecTemp.get(5).toString()) + "%</td>" +
                                    "<td>" + vecTemp.get(6) + "</td>" +
                                    "<td>" + vecTemp.get(8) + "</td>" +
                            "</tr>");                        
                    }
                    count++;
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>
            <input type="button" value="<%=Languages.getString("jsp.admin.reports.Print_This_Page",SessionData.getLanguage())%>" onclick="window.print()">

<%
}
else if (intRecordCount==0 && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.customers.merchants_credit_history.not_found",SessionData.getLanguage())+"</font>");
}
%>

</td>
      </tr>
    </table>
</td>
</tr>
</table>
</body>
</html>
