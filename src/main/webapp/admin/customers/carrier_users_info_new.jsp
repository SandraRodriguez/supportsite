<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.util.Hashtable,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%
	int section=14;
	int section_page=4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:setProperty name="User" property="*"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<%
	Hashtable userErrors = new Hashtable();
	Vector vecUsers = new Vector();
	
	String errorUsername = "";
	String errorPassword = "";
	String errorEmail="";
	String[] errorProducts;
	
	// Form has been submitted
	if(request.getParameter("submitted") != null) {
			
		// Adding in the user
		if (User.validateAddUpdateUser(SessionData, getServletContext()))
       	{
			User.addUser(SessionData,application,false);
         	// Delete all permissions and make sure only view carrier reports permission is set
         	User.deleteUserPermissions();
           	User.setPermissionTypeId(DebisysConstants.PERM_VIEW_CARRIER_REPORTS);
           	User.addUserPermission();
                
                User.setPermissionTypeId(DebisysConstants.PERM_VIEW_CARRIER_TOOLS);
           	User.addUserPermission();
                

    		String[] products = request.getParameterValues("products");
    		User.addCarrierUserProduct(products, null);
       	}

		userErrors = User.getErrors();
		
		if(userErrors.size() == 0) {
     		User.setUsername("");
     		User.setPassword("");
     		User.setEmail("");
		}
	}
	
	if(userErrors.size() > 0 || request.getParameter("submitted") == null) 
	{
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
		          		<table width=100% border=0>
	              			<tr class="main">
	               				<td nowrap valign="top"><%=Languages.getString("jsp.admin.customers.carrier_users_info.new.instructions",SessionData.getLanguage())%></td>
	               			</tr>
	              		</table>
						<form name="carrierUsersInfoForm" method="post" action="admin/customers/carrier_users_info_new.jsp" >
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
<%
		if (userErrors.size() > 0)
		{
	  		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
			Enumeration enum1=userErrors.keys();
	
			while(enum1.hasMoreElements())
			{
	  			String strKey = enum1.nextElement().toString();
	  			String strError = (String) userErrors.get(strKey);
	  			out.println("<li>" + strError);
			}
	
	  		out.println("</font></td></tr>");
		}
%>	
				     		<tr>
					        	<td>
				          			<table border=0>
										<tr class="main">
											<td><%=Languages.getString("jsp.admin.customers.carrier_users.username",SessionData.getLanguage())%></td>
											<td>
												<input type="text" id="username" name="username" value="<%=User.getUsername()%>"/>
											</td>
										</tr>
										<tr class="main">
											<td><%=Languages.getString("jsp.admin.customers.carrier_users.password",SessionData.getLanguage())%> </td>
											<td>
												<input type="text" id="password" name="password" value="<%=User.getPassword()%>"/>
											</td>
										</tr>
										<tr class="main">
											<td><%=Languages.getString("jsp.admin.customers.user_logins.email",SessionData.getLanguage())%> </td>
											<td>
												<input type="text" id="email" name="email" value="<%=User.getEmail()%>"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
				     		<tr>
					        	<td>
					        		<table>
										<tr>
											<td class=rowhead3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.products",SessionData.getLanguage()).toUpperCase()%></td>
										</tr>
										<tr>
											<td>
												<div class="main" style="overflow:auto;width:980px;height:400px;border:1px solid #336699;padding-left:5px;font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 8pt;">
<%
		Vector<Vector<String>> vecProductResults = User.getProducts();
	
		String providerID = "";
		Iterator<Vector<String>> itProducts = vecProductResults.iterator();
			
		// Basically get a list of all of the products and organize them by provider.
		// Show them in a list sorted by provider, with each product having a checkbox
		while (itProducts.hasNext())
		{
			Vector<String> vecProducts = null;
			vecProducts = (Vector<String>) itProducts.next();
			String selected = "";
			
			// Getting the first product/provider
			if(providerID.equals("")) {
				providerID = vecProducts.get(0);
				out.println("<font size=\"4\" ><b>" + vecProducts.get(1) + " (" + vecProducts.get(0) + ")</b></font><br>");
			}
			
			// Finished one set of products for a specific provider, on to the next one
			if(!vecProducts.get(0).equals(providerID)) {
				providerID = vecProducts.get(0);
				out.println("<font size=\"4\" ><b>" + vecProducts.get(1) + " (" + vecProducts.get(0) + ")</b></font><br>");
			}
			
			if(vecProducts.get(5).equals("1"))
				selected = "CHECKED";
			
			out.println("<input type=\"checkbox\" id=\"products\" name=\"products\" value=\"" + vecProducts.get(0) + "_" + vecProducts.get(2) + "\" " + selected + ">&nbsp;&nbsp;" + vecProducts.get(3) + " (" + vecProducts.get(4) + ") " +vecProducts.get(2)+ "<br>");
		}
%>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
				     		<tr>
					        	<td>
					        		<table>
					        			<tr>
					        				<td>
							          			<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.savebutton",SessionData.getLanguage())%>" />
							          			<input type="hidden" id="submitted" name="submitted" value="1" />
											    <input type="hidden" id="refId" name="refId" value="<%=SessionData.getProperty("ref_id")%>">
											    <input type="hidden" id="refType" name="refType" value="1">
											    <input type="hidden" id="status" name="status" value="1">
												</form>
					        				</td>
					        				<td>
												<form name="cancelForm" method="get" action="admin/customers/carrier_users.jsp" >
													<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.cancelbutton",SessionData.getLanguage())%>">
													<input type="hidden" id="search" name="search" value="y">
												</form>
					        				</td>
					        			</tr>
					        		</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
	}else {
		// Carrier user created successfully
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="970" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.title",SessionData.getLanguage())%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<form name="backToCarrierUsers" action="admin/customers/carrier_users.jsp"> 
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center" class="main"><%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.CarrierAccountCreationSuccessful",SessionData.getLanguage())%>!</td>
							</tr>
							<tr>
				        		<td align="center">
				       				<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.BackToCarrierUsers",SessionData.getLanguage())%>" />
									<input type="hidden" id="search" name="search" value="y">
				       			</td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
	}
%>
<%@ include file="/includes/footer.jsp" %>