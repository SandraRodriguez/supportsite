<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.terminals.TerminalRegistration"%>
<%@page import="com.debisys.terminals.JavaHandSet"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
	
	String siteId = request.getParameter("siteId");
	String branding = request.getParameter("branding");
	String userName = request.getParameter("userName");
	String password = request.getParameter("password");
	String regCode = request.getParameter("regCode");
	String IdTerminalRegistration = request.getParameter("IdTerminalRegistration");
	boolean result = false;
	boolean hasPermissionToGenerateNewRegCode = SessionData.checkPermission(com.debisys.utils.DebisysConstants.PERM_TO_GENERATE_NEW_REGCODE);
	
	try
	{
		JavaHandSet javaHandset; // = new JavaHandSet(userName,password,branding,Boolean.parseBoolean(regCode),Integer.parseInt(IdTerminalRegistration),siteId);
		if (hasPermissionToGenerateNewRegCode)
		{
			javaHandset = new JavaHandSet(userName,password,branding,Boolean.parseBoolean(regCode),Integer.parseInt(IdTerminalRegistration),siteId);
		}
		else
		{
			javaHandset = new JavaHandSet(userName,password,branding,false,Integer.parseInt(IdTerminalRegistration),siteId);
		}
		result = TerminalRegistration.updatePCTerminalRegistrationNokiaS40(javaHandset,request,SessionData);
		
		if ( Boolean.parseBoolean(regCode) )
		{
			out.print(javaHandset.getJavaHandSetRegistrationCode());
		}
	
	}
	catch(Exception ex)
	{
		this.log("updateNokiaInfo.jsp: "+ex);
	}
	
%>


