<%@ page  language="java" import="java.util.*,
								  com.debisys.languages.Languages,
								  com.debisys.properties.Properties,
								  com.debisys.utils.*,com.debisys.terminals.JavaHandSet" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="JavaHandSet" class="com.debisys.terminals.JavaHandSet" scope="request"/>
<jsp:setProperty name="JavaHandSet" property="*"/>


<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
	StringBuilder comboBrandings = new StringBuilder();
	String instance = DebisysConfigListener.getInstance(application);
	
	String JavaHandSetUserNameLang = Languages.getString("jsp.admin.customers.terminal.javaUserName",SessionData.getLanguage());
	String JavaHandSetPasswordLang = Languages.getString("jsp.admin.customers.terminal.javaPasswords",SessionData.getLanguage());
	String javaBrandingLang = Languages.getString("jsp.admin.customers.terminal.javaHandset_term_brand",SessionData.getLanguage());
	String javaTitleLang = Languages.getString("jsp.admin.customers.terminal.titleJavaHandSetInformation",SessionData.getLanguage());
	
	String txtRegistration="";
	String userName = "";
	String password = "";
	String branding="";
	String siteId="";
	String IdTerminalRegistration="";
	
	boolean hasPermissionToGenerateNewRegCode = SessionData.checkPermission(DebisysConstants.PERM_TO_GENERATE_NEW_REGCODE);
	
	
	boolean showChangeRegCode=false;
	String styleTable = "display: none";
	
	if ( request.getParameter("show").equals("1") )
	{		
		txtRegistration = request.getParameter("txtRegistration");
		userName = request.getParameter("userTerminal");
		password = request.getParameter("passwordTerminal");
		branding = request.getParameter("brandTerminal");
		siteId = request.getParameter("siteId");
		IdTerminalRegistration = request.getParameter("IdTerminalRegistration");
		
		JavaHandSet.setJavaHandSetPassword(password);
		JavaHandSet.setJavaHandSetUserName(userName);
		styleTable="";
		showChangeRegCode=true;
		comboBrandings.append(request.getParameter("comboBrandings"));	
	}
	else
	{
		if (request.getParameter("comboBrandings") != null)
		{
			comboBrandings.append(request.getParameter("comboBrandings"));
		}	
	}
	
%>

<script type="text/javascript">
	function findNewUUIRegCode()
	{
		//$.post("admin/customers/terminals/uniqueRegCode.jsp?Random=" + Math.random(), function(data){
		//	$("#uniqueRegCode").val(data);
		//});																
	}
</script>
<script type="text/javascript">
	function ValidateNokiaTermPassword(c)
	{		
	  var exp = new RegExp("^([0-9\])+$");
	  if ( !exp.test(c.value) && (c.value.length > 0) )
	  {
		alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidpassword",SessionData.getLanguage())%>');
		window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
		return false;
	  }
	  return true;
	}
</script>

<script type="text/javascript">
	function saveInfoNokiaS40()
	{  
		var userName = $("#javaHandSetUserName").val();
		var password = $("#javaHandSetPassword").val();
		var branding = $("#javaHandSetBranding").val();
		var regCode = $("#chkGenerateNewRegCode").is(':checked');
		var siteId = $("#siteId").val();
		var IdTerminalRegistration = $("#IdTerminalRegistration").val();
		//alert("TR: "+IdTerminalRegistration);
				
		$("#statusChange").show();					
		$.post("admin/customers/terminals/updateNokiaInfo.jsp?Random=" + Math.random(), 
			{ siteId: siteId, branding: branding, userName: userName, password:password, regCode:regCode, IdTerminalRegistration:IdTerminalRegistration },
			function(data)
			{
				if ( regCode )
				{
					//alert(data);
					$("#uniqueRegCode").val(data);
				}	
				$("#statusChange").hide();			
			}
		);
		
	}
</script>

<table id="tableJavaHandSet" name="tableJavaHandSet" style="<%=styleTable%>" width="100%" >
	<tr>
		<td class="formAreaTitle"><%=javaTitleLang%></td>
	</tr>	 	
	<tr>
		<td class="formArea2">
			<table width="100%">
				<tr><td></td><td>&nbsp;</td><td></td><td>&nbsp;&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>
				<!--
				<tr class="main">
					<td nowrap="nowrap"><%=javaBrandingLang%></td>
					<td colspan="3">
						<select id="javaHandSetBranding" name="javaHandSetBranding"  >
							<%=comboBrandings%>
						</select>
					</td>						
				</tr>				  
				<tr class="main">
					<td><%=JavaHandSetUserNameLang%></td>
					<td><input id="javaHandSetUserName" name="javaHandSetUserName" type="text" value="<%=JavaHandSet.getJavaHandSetUserName()%>"></td>
					<td><%=JavaHandSetPasswordLang%></td>
					<td><input id="javaHandSetPassword" name="javaHandSetPassword" type="text" value="<%=JavaHandSet.getJavaHandSetPassword()%>" onblur="return ValidateNokiaTermPassword(this)"></td>
				</tr>
				-->
				
			<%if (showChangeRegCode){ %>
				<tr class="main">
					<td>
						 <input id="javaHandSetUserNameOld" name="javaHandSetUserNameOld" type="hidden" value="<%=JavaHandSet.getJavaHandSetUserName()%>" >
						 <input id="javaHandSetPasswordOld" name="javaHandSetPasswordOld" type="hidden" value="<%=JavaHandSet.getJavaHandSetPassword()%>" >
						 <input id="brandingOld" name="brandingOld" type="hidden" value="<%=branding%>" >
						 <input id="txtRegistrationOld" name="txtRegistrationOld" type="hidden" value="<%=txtRegistration%>" >
						 
						 <input id="siteId" name="siteId" type="hidden" value="<%=siteId%>" >
						 <input id="IdTerminalRegistration" name="IdTerminalRegistration" type="hidden" value="<%=IdTerminalRegistration%>" >					
						<%=Languages.getString("jsp.admin.customers.terminal.registrationcode",SessionData.getLanguage())%>
					</td>
					<td>
						<input id="uniqueRegCode" name="uniqueRegCode" type="text" value="<%=txtRegistration%>" size="20" maxlength="15"  disabled="disabled"> 
					</td>
					
					<% if (hasPermissionToGenerateNewRegCode){ %>
						<td>
							<input type="checkbox" id="chkGenerateNewRegCode"><%=Languages.getString("jsp.admin.customers.terminal.generate",SessionData.getLanguage())%>
						</td>
						<td>
							<input type="button" onclick="saveInfoNokiaS40();" value="<%=Languages.getString("jsp.admin.customers.terminal.javaSaveChanges",SessionData.getLanguage())%>"  />
						</td>
						<td>
							<div id="statusChange" style="display:none ">
							 Updating....<img alt="" src="images/ajax-loading.gif">
							</div>
						</td>
					<%}%>
				 </tr>				 		
			<%}%>							
			</table>
		</td>
	</tr>	
</table>
