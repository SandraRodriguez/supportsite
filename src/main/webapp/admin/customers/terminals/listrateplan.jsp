<%@ page language="java" import="java.util.*,
								 com.debisys.utils.*,
								 com.debisys.languages.Languages,
								 com.debisys.terminals.Terminal" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.customers.Merchant"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%

	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
	String divName = request.getParameter("divName");
	String merchantId = request.getParameter("merchantId");
	String repId = request.getParameter("repId");
	String RateplanModel = request.getParameter("RateplanModel");
		
	ArrayList<String> plans = Merchant.getRatePlansByRepId(Long.parseLong(repId));
	StringBuffer plansCombo = new StringBuffer();
	if (plans!=null)
	{
	 	plansCombo.append("<select id='EmidaRatePlan' name='EmidaRatePlan' onchange=\"updateRatePlanEmida(this.value,'"+divName+"','"+merchantId+"')\">");
	 	plansCombo.append("<option value='-1' selected>--------</option>");
	 	for(String plan : plans)
	 	{
	 		String[] planInfo = plan.split("\\#");
	 		plansCombo.append("<option value='"+planInfo[0]+"'>"+planInfo[1]+"("+planInfo[2]+"-"+planInfo[0]+")</option>");
	 	}
	 	plansCombo.append("</select>");
	}		 	
%>

	<tr>
 		<td class="main">
 			<%=Languages.getString("jsp.admin.customers.merchants_info.rateplan",SessionData.getLanguage())%>
 		</td>
		<td>
			<input type="hidden" id="EmidaRatePlanValue" name="EmidaRatePlanValue" value="<%=RateplanModel%>" />
			<%=plansCombo.toString()%>
		</td>
	</tr>
