<%-- 
    Document   : terminalAssociationDB
    Created on : Dec 19, 2018, 9:52:08 AM
    Author     : nmartinez
--%>
<%@page import="com.debisys.utils.QueryReportDataAssoc.ActionType"%>
<%@page import="com.debisys.utils.QueryReportDataAssoc.ReportType"%>
<%@page import="com.debisys.terminals.TerminalAssociationsPojo"%>
<%@page import="com.debisys.terminals.TerminalAssociations"%>
<%@page import="com.debisys.utils.QueryReportDataAssoc"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.debisys.utils.JSONObject"%>
<%@page import="com.debisys.utils.JSONArray"%>

<%@page import="java.util.List"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />


<%
    QueryReportDataAssoc reporInfo = new QueryReportDataAssoc();
    String action = request.getParameter("action");
    String searchType = request.getParameter("searchType");
    
    String start = request.getParameter("start");
    String end = request.getParameter("rows");
    String sortField = request.getParameter("sortField");
    String sortOrder = request.getParameter("sortOrder");
    
    String isoId = request.getParameter("isoId");
            
    reporInfo.getDataFilter().put("action", action);
        
    if (searchType.equals("current")){
        reporInfo.setReportType(ReportType.CURRENT_TERMINALS);
        String siteId = request.getParameter("siteId");
        reporInfo.getDataFilter().put("siteId", siteId);
        if (action.equals("count")){
            reporInfo.setReportFeature(ActionType.COUNT);
            Integer rows = TerminalAssociations.getTerminalAssociationsByTerminalIdCount(reporInfo);
            out.print(rows);
        } else{
            reporInfo.setReportFeature(ActionType.LIST);
            reporInfo.setInfoReportData(sortField, sortOrder, start, end);   
            List<TerminalAssociationsPojo> data = TerminalAssociations.getTerminalAssociationsByTerminalId(reporInfo);
            String json = new Gson().toJson(data);
            response.setContentType("application/json");
            out.print(json);        
        }       
        
    } else if (searchType.equals("grouped") && (isoId != null) ){
        String filter = request.getParameter("filter");
        String terminalTypeId = request.getParameter("terminalTypeId");
        reporInfo.getDataFilter().put("filter", filter);
        reporInfo.getDataFilter().put("isoId", isoId);
        reporInfo.getDataFilter().put("terminalTypeId", terminalTypeId);
        
        reporInfo.setReportType(ReportType.GROUPED);
        if (action.equals("count")){
            reporInfo.setReportFeature(ActionType.COUNT);
            Integer rows = TerminalAssociations.getTerminalAssociationsByTerminalIdCount(reporInfo);
            out.print(rows);
        } else{
            reporInfo.setReportFeature(ActionType.LIST);
            reporInfo.setInfoReportData(sortField, sortOrder, start, end);   
            List<TerminalAssociationsPojo> data = TerminalAssociations.getTerminalAssociationsByTerminalId(reporInfo);
            String json = new Gson().toJson(data);
            response.setContentType("application/json");
            out.print(json);        
        }          
    } else if (searchType.equals("search") && (isoId != null) ){
        String filter = request.getParameter("filter");
        String terminalTypeId = request.getParameter("terminalTypeId");
        reporInfo.getDataFilter().put("filter", filter);
        reporInfo.getDataFilter().put("isoId", isoId);
        reporInfo.getDataFilter().put("terminalTypeId", terminalTypeId);
        
        reporInfo.setReportType(ReportType.SEARCH_INFO);
        if (action.equals("count")){
            reporInfo.setReportFeature(ActionType.COUNT);
            Integer rows = TerminalAssociations.getTerminalAssociationsByTerminalIdCount(reporInfo);
            out.print(rows);
        } else{
            reporInfo.setReportFeature(ActionType.LIST);
            reporInfo.setInfoReportData(sortField, sortOrder, start, end);   
            List<TerminalAssociationsPojo> data = TerminalAssociations.getTerminalAssociationsByTerminalId(reporInfo);
            String json = new Gson().toJson(data);
            response.setContentType("application/json");
            out.print(json);        
        }          
        
    } else if (searchType.equals("associate")){
        //"currentTerminalId": currentTerminalId, "relatedTerminaId": relatedTerminaId
        String currentTerminalId = request.getParameter("currentTerminalId");
        String relatedTerminaId = request.getParameter("relatedTerminaId");
        Integer row = TerminalAssociations.addTerminalAssociation(SessionData, request,currentTerminalId, relatedTerminaId);                
        out.print(row);
        
    } else if (searchType.equals("deleteAssociation")){
        String id = request.getParameter("id");
        String masterId = request.getParameter("masterId");
        Integer row = TerminalAssociations.deleteTerminalAssociationById(id, masterId);                
        out.print(row);        
    } 
%>