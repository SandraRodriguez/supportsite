<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.terminals.TerminalMapping"%>
<%@page import="com.debisys.languages.Languages"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
	ArrayList<TerminalMapping> listMappings = new ArrayList<TerminalMapping>();
	
	if ( session.getAttribute("terminalMappings") != null)
	{
		listMappings = (ArrayList<TerminalMapping>)session.getAttribute("terminalMappings");
	}
	
	String providersOptions = request.getParameter("provOptions");

%>

<script type="text/javascript">

	function validateFieldsTerminalMappingNew(providerSelected,terminalMapping,userNameMapping,passwordMapping,action)
	{
	   var result=true;
	   var msg="";
	   if ( action != "D")
	   {
		   if (providerSelected=="-1")
		   {
		   		result=false;
		   		msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.msg1",SessionData.getLanguage())%>"+"\n";			 	
		   }
		   if (terminalMapping.length == 0 && userNameMapping.length == 0 && passwordMapping.length == 0 )
		   {		   		
		   	   result=false;
		   	   msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.errorMsg3",SessionData.getLanguage())%>"+"\n";
		   	   msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.terminalId",SessionData.getLanguage())%>"+"\n";
		   	   msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.userName",SessionData.getLanguage())%>"+"\n";
		   	   msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.password",SessionData.getLanguage())%>"+"\n";
		   }		   
		   if (!result)
		   	alert(msg);
	   }
	   return result;
	}
	
	function setProvider(control,providerId)
	{
		var elementsCombo = 0;
		var elementsComboRemoved = 0;
		//alert("setup: "+providerId+" control: "+control);
		$("#"+control+" option").each(function()
			{
				valueSelect = $(this).attr("value");
				if ( valueSelect==providerId)
				{					
					$("#"+control).val(valueSelect);									
					$("#providerMapping option").each(function()
					{
						valueMaster = $(this).attr("value");
						elementsCombo = elementsCombo + 1;
						if ( valueMaster==providerId )
						{							
							$(this).remove();
							elementsComboRemoved = elementsComboRemoved + 1;
						}					
					});
				}
		 	}
		);
		$("#"+control).attr("disabled","disabled");
		if ( elementsCombo == elementsComboRemoved+1 )
		{
			$("#trNewTerminalMapping").remove();
		}
		$("#providerTerminalMapping_"+providerId+"_NEW").attr("disabled","disabled");
		$("#userNameMapping_"+providerId+"_NEW").attr("disabled","disabled");
		$("#passwordMapping_"+providerId+"_NEW").attr("disabled","disabled");
	}
	
	function enableControlTerminalMapping(providerId)
	{
		$("#providerTerminalMapping_"+providerId+"_NEW").attr("disabled",false);
		$("#userNameMapping_"+providerId+"_NEW").attr("disabled",false);
		$("#passwordMapping_"+providerId+"_NEW").attr("disabled",false);
		$("#editNewMapping_"+providerId+"_NEW").attr("disabled","disabled");
		$("#updateNewMapping_"+providerId+"_NEW").attr("disabled",false);
		$("#deleteNewMapping_"+providerId+"_NEW").attr("disabled",false);		
	}
	
	function addCacheNewProviderTerminalMapping(action)
	{
		var providerSelected = $("#providerMapping").val();
		var terminalMapping = $("#terminalMapping").val();
		var userNameMapping = $("#userNameMapping").val();
		var passwordMapping = $("#passwordMapping").val();
	    		
		if ( validateFieldsTerminalMappingNew(providerSelected,terminalMapping,userNameMapping,passwordMapping,action) )
		{			
			//alert(providerSelected+" "+terminalMapping+" "+userNameMapping+" "+passwordMapping+" "+siteId);
			
			var pageJSP = "admin/customers/terminals/mapping/terminalMappingDBNew.jsp";
			  
			$.post(pageJSP, 
				{ 
			  	   providerSelected: providerSelected,	
			  	   terminalMapping: terminalMapping,
			  	   userNameMapping: userNameMapping,
			  	   passwordMapping: passwordMapping,
			  	   action: action	   
			  	},
				function saveNewProviderTerminalMappingHtml(data)
				{	
					paintNewTerminalMappings("<%=providersOptions%>");
					
					$("#terminalMapping").val("");
					$("#userNameMapping").val("");
					$("#passwordMapping").val("");
					
					$("#messagesZone").html(data);	 
					$("#messagesZone").fadeIn(100);	
				    $("#messagesZone").css("color","blue");					    
				    $("#messagesZone").fadeOut(4000);				    														
				}
			  );
		}	
	
	}
	
	function updateCacheTerminalMapping(action,providerId)
	{		
	    var prefixControl = "_"+providerId+"_NEW";
		var providerSelected = $("#providerMapping"+prefixControl).val();	
		var terminalMapping  = $("#providerTerminalMapping"+prefixControl).val();
		var userNameMapping  = $("#userNameMapping"+prefixControl).val();
		var passwordMapping  = $("#passwordMapping"+prefixControl).val();
				
		if ( validateFieldsTerminalMappingNew(providerSelected,terminalMapping,userNameMapping,passwordMapping,action) )
		{		
			var pageJSP = "admin/customers/terminals/mapping/terminalMappingDBNew.jsp";
					  
			//alert(action+","+providerId+","+siteId+" "+providerSelected+" "+terminalMapping+" "+userNameMapping+" "+passwordMapping);
			 
			$.post(pageJSP, 
				{ 
				   providerSelected: providerSelected,		
			  	   terminalMapping: terminalMapping,
			  	   userNameMapping: userNameMapping,
			  	   passwordMapping: passwordMapping,
			  	   providerId: providerId,
			  	   action: action	 		  	   	   
			  	},
				function updateNewProviderTerminalMappingHtml(data)
				{
					paintNewTerminalMappings("<%=providersOptions%>");
					$("#messagesZone").html(data);	 
					$("#messagesZone").fadeIn(100);	
				    $("#messagesZone").css("color","blue");					    
				    $("#messagesZone").fadeOut(4000);
				}
			  );
		 } 				
	}
</script>

	<table>
   	  <tr class="rowhead2">
			<td align="center"><%=Languages.getString("com.debisys.terminals.jsp.mapping.provider",SessionData.getLanguage())%></td>
			<td align="center"><%=Languages.getString("com.debisys.terminals.jsp.mapping.terminalId",SessionData.getLanguage())%></td>
			<td align="center"><%=Languages.getString("com.debisys.terminals.jsp.mapping.userName",SessionData.getLanguage())%></td>
			<td align="center"><%=Languages.getString("com.debisys.terminals.jsp.mapping.password",SessionData.getLanguage())%></td>
			<td align="center"></td>
	  </tr>
	  
	<% 
	int rowClass=1;
	for(TerminalMapping tmapping : listMappings)
	{
		String idRow = tmapping.getProviderId()+"_NEW";		
 		%>		
 		
		 	<tr class="row<%=rowClass%>" >
		 		<td align="center">
		 			<select id="providerMapping_<%=idRow%>" name="providerMapping_<%=idRow%>">
		 				<%=providersOptions%>
		 			</select>
		 		</td>
				<td align="center">
					 <input type="text" id="providerTerminalMapping_<%=idRow%>" name="providerTerminalMapping_<%=idRow%>" value="<%=tmapping.getProviderTerminalId()%>" />
				</td> 		
				<td align="center">
					<input type="text" id="userNameMapping_<%=idRow%>" name="userNameMapping_<%=idRow%>" value="<%=tmapping.getProviderMerchantId()%>" />
				</td>
				<td align="center">
					<input type="text" id="passwordMapping_<%=idRow%>" name="passwordMapping_<%=idRow%>" value="<%=tmapping.getProviderTerminalData()%>" />
				</td>
				<td  align="center">
					<table>
						<tr>
							<td align="center"> 
								<input type="button" 
								 id="editNewMapping_<%=idRow%>"   
								 value="<%=Languages.getString("com.debisys.terminals.jsp.mapping.edit",SessionData.getLanguage())%>" 
								 onclick="enableControlTerminalMapping(<%=tmapping.getProviderId()%>)"  />						
							</td>
							<td align="center"> 
								<input type="button" disabled="disabled" 
								 id="updateNewMapping_<%=idRow%>" name="updateNewMapping_<%=idRow%>"  
								 value="<%=Languages.getString("com.debisys.terminals.jsp.mapping.update",SessionData.getLanguage())%>" 
								 onclick="updateCacheTerminalMapping('<%=TerminalMapping.UpdateTerminalMapping%>',<%=tmapping.getProviderId()%>)"  />						
							</td>
							<td align="center">						
								<input type="button" disabled="disabled"
								 id="deleteNewMapping_<%=idRow%>" name="deleteNewMapping_<%=idRow%>" 
								 value="<%=Languages.getString("com.debisys.terminals.jsp.mapping.delete",SessionData.getLanguage())%>" 
								 onclick="updateCacheTerminalMapping('<%=TerminalMapping.DeleteTerminalMapping%>',<%=tmapping.getProviderId()%>)" />
							</td>	
						</tr>							
					</table>			
		 	</tr>
	 		<script type="text/javascript">
		 		setProvider("providerMapping_<%=idRow%>",<%=tmapping.getProviderId()%>);
		 	</script>	 	
	<% 
		if (rowClass==1)
		{
			rowClass=0;
		}
		else
		{
			rowClass=1;
		}
 	}
 	%>
	  <tr id="trNewTerminalMapping" class="main" >
	 		<td align="center">
	 			<select id="providerMapping" name="providerMapping">
	 				<%=providersOptions%>
	 			</select>
	 		</td>
			<td align="center">
				 <input type="text" id="terminalMapping" name="terminalMapping" value="" />
			</td> 		
			<td align="center">
				<input type="text" id="userNameMapping" name="userNameMapping" value="" />
			</td>
			<td align="center">
				<input type="text" id="passwordMapping" name="passwordMapping" value="" />
			</td>
			<td align="center">
				<input 
				 type="button" 
				 id="saveNewMapping" 
				 name="saveNewMapping" 
				 value="<%=Languages.getString("com.debisys.terminals.jsp.mapping.add",SessionData.getLanguage())%>" 
				 onclick="addCacheNewProviderTerminalMapping('<%=TerminalMapping.InsertTerminalMapping%>');" />
			</td>				
	  </tr>			 	   			
    </table>
