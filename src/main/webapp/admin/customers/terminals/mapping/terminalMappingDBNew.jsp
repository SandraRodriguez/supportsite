<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.terminals.TerminalMapping"%>
<%@page import="com.debisys.languages.Languages"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
		
	String providerSelected = request.getParameter("providerSelected");	
	String terminalMapping = request.getParameter("terminalMapping");	
	String userNameMapping = request.getParameter("userNameMapping");
	String passwordMapping = request.getParameter("passwordMapping");
    
    String action = request.getParameter("action");
    String providerId = request.getParameter("providerId");
	
	int result=0;
	String messageResult="";
	
	try
	{	
		
		TerminalMapping currentTerminalMapping = new TerminalMapping();				
		currentTerminalMapping.setProviderId(Integer.parseInt(providerSelected));
		currentTerminalMapping.setAction(TerminalMapping.InsertTerminalMapping);
		currentTerminalMapping.setProviderMerchantId(userNameMapping);
		currentTerminalMapping.setProviderTerminalData(passwordMapping);
		currentTerminalMapping.setProviderTerminalId(terminalMapping);
				
		ArrayList<TerminalMapping> listMappings = new ArrayList<TerminalMapping>();
		
		if ( action.equals(TerminalMapping.InsertTerminalMapping) )
		{			
			if ( session.getAttribute("terminalMappings") != null)
			{				
				listMappings = (ArrayList<TerminalMapping>)session.getAttribute("terminalMappings");									
			}			
			listMappings.add(currentTerminalMapping);				
			session.setAttribute("terminalMappings",listMappings);			
			messageResult = Languages.getString("com.debisys.terminals.jsp.mapping.AddOk",SessionData.getLanguage());			
		}
		else if ( action.equals(TerminalMapping.UpdateTerminalMapping) || action.equals(TerminalMapping.DeleteTerminalMapping)  )
		{				
			if ( session.getAttribute("terminalMappings") != null)
			{
				listMappings = (ArrayList<TerminalMapping>)session.getAttribute("terminalMappings");					
			}
			if ( action.equals(TerminalMapping.UpdateTerminalMapping) )
			{
				for( TerminalMapping terminalMap : listMappings )
				{
					if ( action.equals(TerminalMapping.UpdateTerminalMapping) )
					{
						if (terminalMap.getProviderId() == currentTerminalMapping.getProviderId() )
						{
							terminalMap.setProviderTerminalData(passwordMapping);
							terminalMap.setProviderTerminalId(terminalMapping);
							terminalMap.setProviderMerchantId(userNameMapping);
							messageResult = Languages.getString("com.debisys.terminals.jsp.mapping.UpdateOK",SessionData.getLanguage());
						}
					}						
				}
			}
			else if ( action.equals(TerminalMapping.DeleteTerminalMapping) )
			{
				ArrayList<TerminalMapping> listMappingsNew = new ArrayList<TerminalMapping>();
				for( TerminalMapping terminalMap : listMappings )
				{
					if (terminalMap.getProviderId() != currentTerminalMapping.getProviderId() )
					{
						listMappingsNew.add(terminalMap);
					}
				}
				session.setAttribute("terminalMappings",listMappingsNew);
				messageResult = Languages.getString("com.debisys.terminals.jsp.mapping.DeleteOk",SessionData.getLanguage());										
			}							
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();		
	}		  	   
	//messageResult	
%>





