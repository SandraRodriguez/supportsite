<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.terminals.TerminalMapping"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@page import="com.debisys.provider.Provider"%>
<%@page import="com.debisys.languages.Languages"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
	boolean terminalMappingProvider=false;
	
	Integer siteId = new Integer(request.getParameter("siteId"));
	
	StringBuilder providers = new StringBuilder();
	
	ArrayList<Provider> listProvider  = Provider.getListProvidersByRefId(SessionData.getProperty("ref_id"));
	for(Provider prov : listProvider)
	{
		terminalMappingProvider=true;
		providers.append("<option value='"+prov.getProvider_id()+"'>"+prov.getName()+"("+prov.getProvider_id()+")</option>");		
	}		
	providers.append("<option selected value='-1'>------</option>");		
	
	if (terminalMappingProvider)
	{
	%>

		<script type="text/javascript">
			$(document).ready( function() 
				{	
					paintCurrentTerminalMappings(<%=siteId%>,"<%=providers.toString()%>");
				}
			)	
			function paintCurrentTerminalMappings(siteId,provOptions)
			{	
				if ( siteId!=null )
				{ 
				  var pageJSP = "admin/customers/terminals/mapping/terminalMappingList.jsp";
				  
				  $.post(pageJSP, 
				  		 { 
				  		   siteId: siteId, 
				  		   provOptions: provOptions 		  		   
				  		 },
						 function paintRulesHtml(data)
						 {		
							$("#rowsAlreadySetUp").html(data);																
						 }
					    );
				}		
			}	 
		</script>


<table id="tableTerminalMapping" class="formArea2" width="100%">
	<tr>
		<td>			
			<table width="50%">
				<tr>
					<td class="formAreaTitle2"><%=Languages.getString("com.debisys.terminals.jsp.mapping.terminalMapping",SessionData.getLanguage())%>:</td>
				</tr>
				<tr>
			 	   <td colspan="2">
			 	   	<div id="rowsAlreadySetUp" ></div> 		
			 	   </td>
			 	</tr>
			 	<tr>
			 	   <td colspan="2">
			 	   	<div id="messagesZone"></div> 		
			 	   </td>
			 	</tr>			 	
			</table>				
		</td>
	</tr>
</table>
<%}
  else
  {
  	boolean showWarnnig=false;
	if (showWarnnig)
	{
  	%>
	   <table id="tableTerminalMapping" class="formArea2" width="100%">
		<tr>
			<td>			
				<table width="50%">
					<tr>
						<td class="formAreaTitle2"><%=Languages.getString("com.debisys.terminals.jsp.mapping.terminalMapping",SessionData.getLanguage())%>:</td>
					</tr>				
					<tr>
				 	   <td colspan="2" style="color:red">
				 	   	<%=Languages.getString("com.debisys.terminals.jsp.mapping.terminalMappingIsoWarnnig",SessionData.getLanguage())%>			 	   	 		
				 	   </td>
				 	</tr>			 	
				</table>				
			</td>
		  </tr>
		</table>
   <%
    }  
  }%>

