<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.terminals.TerminalMapping"%>
<%@page import="com.debisys.languages.Languages"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
		
	String providerSelected = request.getParameter("providerSelected");	
	String terminalMapping = request.getParameter("terminalMapping");	
	String userNameMapping = request.getParameter("userNameMapping");
	String passwordMapping = request.getParameter("passwordMapping");
    String siteId = request.getParameter("siteId");
    String action = request.getParameter("action");
    String providerId = request.getParameter("providerId");
	
	int result=0;
	String messageResult="";
	
	try
	{
		TerminalMapping newTerminalMapping = new TerminalMapping();
		
		newTerminalMapping.setDebisysTerminalId(Integer.parseInt(siteId));
		newTerminalMapping.setProviderId(Integer.parseInt(providerSelected));
		newTerminalMapping.setAction(action);
		newTerminalMapping.setProviderMerchantId(userNameMapping);
		newTerminalMapping.setProviderTerminalData(passwordMapping);
		newTerminalMapping.setProviderTerminalId(terminalMapping);
		
		if ( !action.equals(TerminalMapping.InsertTerminalMapping))
		{
			newTerminalMapping.setCurrentProviderId(Integer.parseInt(providerId));
		}
		
		result = newTerminalMapping.save();	
		
		if (result == 1)
		{
			if ( action.equals(TerminalMapping.InsertTerminalMapping) )
			{
				messageResult = Languages.getString("com.debisys.terminals.jsp.mapping.AddOk",SessionData.getLanguage());
			}
			else if ( action.equals(TerminalMapping.UpdateTerminalMapping) )
			{
				messageResult = Languages.getString("com.debisys.terminals.jsp.mapping.UpdateOK",SessionData.getLanguage());
			}
			else if ( action.equals(TerminalMapping.DeleteTerminalMapping) )
			{
				messageResult = Languages.getString("com.debisys.terminals.jsp.mapping.DeleteOk",SessionData.getLanguage());
			}
		}
		else if ( result == -1 )
		{
			messageResult = Languages.getString("com.debisys.terminals.jsp.mapping.errorMsg1",SessionData.getLanguage());
		}	
		else if ( result == -2 )
		{
			messageResult = Languages.getString("com.debisys.terminals.jsp.mapping.errorMsg2",SessionData.getLanguage());
		}
		
	}
	catch(Exception e)
	{
		e.printStackTrace();		
	}		  	   
	
%>

<%=messageResult%>

