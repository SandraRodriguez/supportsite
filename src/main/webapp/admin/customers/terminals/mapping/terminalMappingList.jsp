<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.terminals.TerminalMapping"%>
<%@page import="com.debisys.languages.Languages"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<% 
	
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
 
 	String providersOptions = request.getParameter("provOptions");
 	Integer siteId = new Integer(request.getParameter("siteId")); 		
						
%>
<script type="text/javascript">
	function setProvider(control,providerId)
	{
		var elementsCombo = 0;
		var elementsComboRemoved = 0;
		//alert("setup: "+providerId+" control: "+control);
		$("#providerMapping_"+control+" option").each(function()
			{
				valueSelect = $(this).attr("value");
				if ( valueSelect==providerId)
				{					
					$("#providerMapping_"+control).val(valueSelect);									
					$("#providerMapping option").each(function()
					{
						elementsCombo = elementsCombo + 1;
						valueMaster = $(this).attr("value");
						if ( valueMaster==providerId )
						{							
							$(this).remove();
							elementsComboRemoved = elementsComboRemoved + 1;
						}					
					});
				}
		 	}
		);
		$("#providerMapping_"+control).attr("disabled","disabled");				
		if ( elementsCombo == elementsComboRemoved+1 )
		{
			$("#trNewTerminalMapping").remove();
		}
		$("#providerTerminalMapping_"+control).attr("disabled","disabled");
		$("#userNameMapping_"+control).attr("disabled","disabled");
		$("#passwordMapping_"+control).attr("disabled","disabled");
	}
	function enableControlTerminalMapping(controlId)
	{
		$("#providerTerminalMapping_"+controlId).attr("disabled",false);
		$("#userNameMapping_"+controlId).attr("disabled",false);
		$("#passwordMapping_"+controlId).attr("disabled",false);
		$("#editNewMapping_"+controlId).attr("disabled","disabled");
		$("#updateNewMapping_"+controlId).attr("disabled",false);
		$("#deleteNewMapping_"+controlId).attr("disabled",false);
	}
</script>

<script type="text/javascript">	
	function updateTerminalMapping(action,providerId,siteId)
	{		
	    var prefixControl = "_"+providerId+"_"+siteId;
		var providerSelected = $("#providerMapping"+prefixControl).val();	
		var terminalMapping  = $("#providerTerminalMapping"+prefixControl).val();
		var userNameMapping  = $("#userNameMapping"+prefixControl).val();
		var passwordMapping  = $("#passwordMapping"+prefixControl).val();
		
		if ( validateFieldsTerminalMapping(providerSelected,terminalMapping,userNameMapping,passwordMapping,action) )
		{		
			var pageJSP = "admin/customers/terminals/mapping/terminalMappingDB.jsp";
					  
			//alert(action+","+providerId+","+siteId+" "+providerSelected+" "+terminalMapping+" "+userNameMapping+" "+passwordMapping);
			 
			$.post(pageJSP, 
				{ 
				   providerSelected: providerSelected,		
			  	   terminalMapping: terminalMapping,
			  	   userNameMapping: userNameMapping,
			  	   passwordMapping: passwordMapping,
			  	   siteId: siteId,
			  	   providerId: providerId,
			  	   action: action	 		  	   	   
			  	},
				function updateProviderTerminalMappingHtml(data)
				{						
					paintCurrentTerminalMappings(<%=siteId%>,"<%=providersOptions%>");
					$("#messagesZone").html(data);	 
					$("#messagesZone").fadeIn(100);	
				    $("#messagesZone").css("color","blue");					    
				    $("#messagesZone").fadeOut(4000);
				}
			  );
		 } 				
	}
</script>

<script type="text/javascript">	
	function addNewProviderTerminalMapping(action)
	{	   
		var providerSelected = $("#providerMapping").val();
		var terminalMapping = $("#terminalMapping").val();
		var userNameMapping = $("#userNameMapping").val();
		var passwordMapping = $("#passwordMapping").val();
			
		if ( validateFieldsTerminalMapping(providerSelected,terminalMapping,userNameMapping,passwordMapping,action) )
		{			
			var siteId = "<%=siteId%>";
			
			//alert(providerSelected+" "+terminalMapping+" "+userNameMapping+" "+passwordMapping+" "+siteId);
			
			var pageJSP = "admin/customers/terminals/mapping/terminalMappingDB.jsp";
			  
			$.post(pageJSP, 
				{ 
			  	   providerSelected: providerSelected,	
			  	   terminalMapping: terminalMapping,
			  	   userNameMapping: userNameMapping,
			  	   passwordMapping: passwordMapping,
			  	   siteId: siteId,
			  	   action: action	   
			  	},
				function saveNewProviderTerminalMappingHtml(data)
				{	
					paintCurrentTerminalMappings(<%=siteId%>,"<%=providersOptions%>");
					$("#terminalMapping").val("");
					$("#userNameMapping").val("");
					$("#passwordMapping").val("");
					
					$("#messagesZone").html(data);	 
					$("#messagesZone").fadeIn(100);	
				    $("#messagesZone").css("color","blue");					    
				    $("#messagesZone").fadeOut(4000);				    														
				}
			  );
		}		
	}	
</script>

<script type="text/javascript">	
	function validateFieldsTerminalMapping(providerSelected,terminalMapping,userNameMapping,passwordMapping,action)
	{
	   var msg="";
	   var result=true;
	   
	   if ( action != "D")
	   {
		   if (providerSelected=="-1")
		   {
		   		result=false;
		   		msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.msg1",SessionData.getLanguage())%>"+"\n";			 	
		   }
		   if (terminalMapping.length == 0 && userNameMapping.length == 0 && passwordMapping.length == 0 )
		   {		   		
		   	   result=false;
		   	   msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.errorMsg3",SessionData.getLanguage())%>"+"\n";
		   	   msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.terminalId",SessionData.getLanguage())%>"+"\n";
		   	   msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.userName",SessionData.getLanguage())%>"+"\n";
		   	   msg = msg + "<%=Languages.getString("com.debisys.terminals.jsp.mapping.password",SessionData.getLanguage())%>"+"\n";
		   }		   
		   if (!result)
		   	alert(msg);
	   	
	   }
	   return result;
	}
	
</script>	

<table width="50%">
	<tr class="rowhead2">
		<td align="center"><%=Languages.getString("com.debisys.terminals.jsp.mapping.provider",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("com.debisys.terminals.jsp.mapping.terminalId",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("com.debisys.terminals.jsp.mapping.userName",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("com.debisys.terminals.jsp.mapping.password",SessionData.getLanguage())%></td>
		<td align="center"></td>
	</tr>
<%	
 	
	ArrayList<TerminalMapping> listMappings = TerminalMapping.getListTerminalMappingBySiteId(siteId,SessionData.getProperty("ref_id"));
	
	int rowClass=1;
	for(TerminalMapping tmapping : listMappings)
	{
		String idRow = tmapping.getProviderId()+"_"+ tmapping.getDebisysTerminalId();		
 		%>		
 		
		 	<tr class="row<%=rowClass%>" >
		 		<td align="center">
		 			<select id="providerMapping_<%=idRow%>" name="providerMapping_<%=idRow%>">
		 				<%=providersOptions%>
		 			</select>
		 		</td>
				<td align="center">
					 <input type="text" id="providerTerminalMapping_<%=idRow%>" name="providerTerminalMapping_<%=idRow%>" value="<%=tmapping.getProviderTerminalId()%>" />
				</td> 		
				<td align="center">
					<input type="text" id="userNameMapping_<%=idRow%>" name="userNameMapping_<%=idRow%>" value="<%=tmapping.getProviderMerchantId()%>" />
				</td>
				<td align="center">
					<input type="text" id="passwordMapping_<%=idRow%>" name="passwordMapping_<%=idRow%>" value="<%=tmapping.getProviderTerminalData()%>" />
				</td>
				<td  align="center">
					<table>
						<tr>
							<td align="center"> 
								<input type="button" 
								 id="editNewMapping_<%=idRow%>"   
								 value="<%=Languages.getString("com.debisys.terminals.jsp.mapping.edit",SessionData.getLanguage())%>" 
								 onclick="enableControlTerminalMapping('<%=idRow%>')"  />						
							</td>
							<td align="center"> 
								<input type="button" disabled="disabled"
								 id="updateNewMapping_<%=idRow%>"	 
								 name="updateNewMapping_<%=idRow%>"  
								 value="<%=Languages.getString("com.debisys.terminals.jsp.mapping.update",SessionData.getLanguage())%>" 
								 onclick="updateTerminalMapping('<%=TerminalMapping.UpdateTerminalMapping%>',<%=tmapping.getProviderId()%>,<%=tmapping.getDebisysTerminalId()%>)"  />						
							</td>
							<td align="center">						
								<input type="button" disabled="disabled"
								 id="deleteNewMapping_<%=idRow%>"	
								 name="deleteNewMapping_<%=idRow%>" 
								 value="<%=Languages.getString("com.debisys.terminals.jsp.mapping.delete",SessionData.getLanguage())%>" 
								 onclick="updateTerminalMapping('<%=TerminalMapping.DeleteTerminalMapping%>',<%=tmapping.getProviderId()%>,<%=tmapping.getDebisysTerminalId()%>)" />
							</td>	
						</tr>							
					</table>			
		 	</tr>
	 		<script type="text/javascript">
		 		setProvider("<%=idRow%>",<%=tmapping.getProviderId()%>);
		 	</script>	 	
	<% 
		if (rowClass==1)
		{
			rowClass=0;
		}
		else
		{
			rowClass=1;
		}
 	}
 	%>
 	<tr id="trNewTerminalMapping" class="main" >
 		<td align="center">
 			<select id="providerMapping" name="providerMapping">
 				<%=providersOptions%>
 			</select>
 		</td>
		<td align="center">
			 <input type="text" id="terminalMapping" name="terminalMapping" value="" />
		</td> 		
		<td align="center">
			<input type="text" id="userNameMapping" name="userNameMapping" value="" />
		</td>
		<td align="center">
			<input type="text" id="passwordMapping" name="passwordMapping" value="" />
		</td>
		<td align="center">
			<input 
			 type="button" 
			 id="saveNewMapping" 
			 name="saveNewMapping" 
			 value="<%=Languages.getString("com.debisys.terminals.jsp.mapping.add",SessionData.getLanguage())%>" 
			 onclick="addNewProviderTerminalMapping('<%=TerminalMapping.InsertTerminalMapping%>');" />
		</td>				
 	</tr>		
</table>
	 	
	 		