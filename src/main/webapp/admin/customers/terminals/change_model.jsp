<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.users.SessionData, 
				com.debisys.pincache.Pincache"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String repId = request.getParameter("repId");
String merchantId = request.getParameter("merchantId");
String labelModel="";
String typeModel ="0";
String userName = SessionData.getUser().getUsername();
String useRatePlan = request.getParameter("useRatePlan");
String labelUseRatePlanModelISO = com.debisys.languages.Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelISO",SessionData.getLanguage());
String labelUseRatePlanModelEmida = com.debisys.languages.Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelEmida",SessionData.getLanguage());
String title = com.debisys.languages.Languages.getString("jsp.admin.customers.merchants_terminals_changeModel_title",SessionData.getLanguage());

boolean goBackNow = false;
if(!Pincache.hasPinesAssigned(merchantId)){
	if (request.getMethod().equals("POST")){
		ArrayList<String> plans = com.debisys.customers.Merchant.getRatePlansByRepId(Long.parseLong(repId));
		ArrayList<String> terminals = com.debisys.customers.Merchant.getTerminalsByMerchantID(Long.parseLong(merchantId));
		ArrayList<String> terminalsUpdate = new ArrayList<String>();
		labelModel= labelUseRatePlanModelEmida;
		if ( (request.getParameter("typeModel")!= null) &&  request.getParameter("typeModel").equals("ISoRatePlan") ){
			typeModel ="1";
			labelModel = labelUseRatePlanModelISO;
		}
		if (terminals.size()>0){
			for(String siteInfo : terminals){
				String[] sitePlan = siteInfo.split("\\#");
				String siteRatePlan = request.getParameter("site_"+sitePlan[0]);
				if (siteRatePlan!=null){
					terminalsUpdate.add(siteRatePlan+"#"+sitePlan[0]);
				}	
			}
		}
		com.debisys.customers.Merchant.updateRatePlanModelAndTerminals(terminalsUpdate,merchantId,typeModel,userName,SessionData);
		goBackNow=true;
	}
}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>----------</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	
	<link href="<%=path%>/default.css" type="text/css" rel="stylesheet">
	
	<script language="JavaScript" src="<%=path%>/includes/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
		function findTerminalWithOutPlan(typeModel,repId,merchantId)
		{
			if ( typeModel== "-1" )
			{
				$("#divTerminalOut").html("");
				//$("#btnSubmit")
				$('#btnSubmit').attr("disabled", true);
			}
			else
			{
				$('#btnSubmit').attr("disabled", false);
				var model="0";
				if (typeModel=="ISoRatePlan")
				{
					model="1";
				}
			 	$("#divTerminalOut").load("admin/customers/terminals/terminals_out_rateplan.jsp?repId="+repId+"&merchantId="+merchantId+"&model="+model+"&Random=" + Math.random());
			}			
		}
	
		function updateLabelAndBack() 
		{	
			//alert('<%=labelModel%>');
			//alert('<%=typeModel%>');
					
			self.opener.changeLabelModelRatePlan('<%=labelModel%>','<%=typeModel%>');
			window.close();
		}
	</script>
  </head>
  
  <body>
  <%if (!goBackNow){ %>
     <form action="<%=path%>/admin/customers/terminals/change_model.jsp" method="post">
	    <table class="formArea" align="center">
	    	<tr>
	    		<td colspan="2" align="center" >
		    		<%=title%>	
		    		<input type="hidden" name="repId" value="<%=repId%>" />
		    		<input type="hidden" name="merchantId" value="<%=merchantId%>" />
	    		</td>    	
	    	</tr>
	    	<tr>
	    		<td>
	    			<select id="typeModel" name="typeModel" onchange="findTerminalWithOutPlan(this.options[this.selectedIndex].value,<%=repId%>,<%=merchantId%>)">
	    				<%if ( useRatePlan.equals("1") ) { %>
	    					<option value="EmidaRatePlan"><%=labelUseRatePlanModelEmida%></option>
	    				<%}
	    				  else{	 %>
	    					<option value="ISoRatePlan"><%=labelUseRatePlanModelISO%></option>
	    				<%} %>
	    				<option value="-1" selected="selected">----</option>
	    			</select>	    		     		 
	    		</td> 
	    	</tr>
	   		<tr> 
	    		<td colspan="2"><br>
	    			<div id="divTerminalOut" style="height:180px;overflow: auto">
	    				
	    			</div>	    			
	    		<br></td>
	    	</tr>
	    	<tr>
	    		<td>
	    			<input id="btnSubmit" name="btnSubmit" type="submit" value="Change" disabled="disabled" >
	    			<input type="button" value="Cancel" onclick="window.close();">	    			
	    		</td>
	    	</tr>	    
	    </table>
    </form>
  </body>
  <%
  }
  else{
  %>
  	<script type="text/javascript">
  	 updateLabelAndBack(); 
  	</script>
  <%
  } %>
</html>
