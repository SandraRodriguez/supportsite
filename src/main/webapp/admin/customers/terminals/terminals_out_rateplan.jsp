<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.util.*,com.debisys.customers.Merchant,com.debisys.languages.Languages" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
	 //String path = request.getContextPath();
	 //String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	 
	 String repId = request.getParameter("repId");
	 String merchantId = request.getParameter("merchantId");
	 String modelToFind = request.getParameter("model");
	 
	 Boolean showDiv=false;
	 StringBuffer plansCombo = new StringBuffer();
	 
	 ArrayList<String> warningLabel= new ArrayList<String>();
	 ArrayList<String> plans = new ArrayList<String>();
	 ArrayList<String> terminals = new ArrayList<String>();
	 
	 
	 if (modelToFind.equals("1"))
	 {
		 terminals = Merchant.getTerminalsByMerchantID(Long.parseLong(merchantId));
		 	 
		 if (terminals.size()==0)
		 {
		 	return;
		 }
		 plans = com.debisys.customers.Merchant.getRatePlansByRepId(Long.parseLong(repId));
		 
		 if (plans!=null && plans.size()>0)
		 {
		 	plansCombo.append("<select id='XXXXXX' name='XXXXXX' >");
		 	for(String plan : plans)
		 	{
		 		String[] planInfo = plan.split("\\#");	 			 		
		 		plansCombo.append("<option value='"+planInfo[0]+"'>"+planInfo[1]+"("+planInfo[2]+")</option>");		 		
		 	}
		 	plansCombo.append("</select>");
		 	
		 	warningLabel.add(Languages.getString("jsp.admin.customers.merchants_terminals_changeModel_before",SessionData.getLanguage())); 
		 	showDiv = true;
		 }
		 else
		 {
		 	warningLabel.add(Languages.getString("jsp.admin.customers.merchants_terminals_changeModel_before",SessionData.getLanguage())+" ");
		 	warningLabel.add(Languages.getString("jsp.admin.customers.merchants_terminals_changeModel_no_plans",SessionData.getLanguage()));
		 }
	}
	else
	{
		warningLabel.add(Languages.getString("jsp.admin.customers.merchants_terminals_changeModel_message1",SessionData.getLanguage())+" "+Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelEmida",SessionData.getLanguage())); 
		warningLabel.add(Languages.getString("jsp.admin.customers.merchants_terminals_changeModel_message2",SessionData.getLanguage()));
		warningLabel.add(Languages.getString("jsp.admin.customers.merchants_terminals_changeModel_message3",SessionData.getLanguage()));
		warningLabel.add(Languages.getString("jsp.admin.customers.merchants_terminals_changeModel_message4",SessionData.getLanguage()));
	}
	
	for(String warnings : warningLabel){
	%>
	<label style="color:red">
		<%=warnings%><%="<br/>"%>			 	  
	</label>
	<%
	}

	if (showDiv)
	{
%>	
	<table border="0">
		<tr class="main">
			<td align="center" colspan="2" style="color: red">
					
			</td>
		</tr>
		<tr class="rowhead">
		 	<td>siteID</td>
		 	<td>Plan</td>
		</tr>
		<%
		  for(String site : terminals)
		  {
		  	boolean showTerminal=true;
		  	String[] sitePlan = site.split("\\#");
		  	String siteIdControl = sitePlan[0];
		  	for(String plan : plans)
		 	{
		 		String[] planInfo = plan.split("\\#");	 			 		
		 		if ( planInfo[0].equals(sitePlan[1]))
		 		{
		 			System.out.println("This site has a rateplan that already exist in the Rep Id. "+sitePlan[0]+":["+planInfo[0]+"/"+planInfo[1]+"]");
		 			showTerminal=false;
		 		}	 		
		 	}
		 	if (showTerminal)
		 	{
		    %>
		  	<tr class="main">
			 	<td><%=siteIdControl%></td>
			 	<td>
			 	 <% 
			 	  if (plansCombo.length()>0)
			 	  {
			 			String combo = plansCombo.toString();
			 			combo = combo.replaceAll("XXXXXX","site_"+siteIdControl);
			 	        %>
			 			<%=combo%>
			 	 <%
			 	 }
			 	 %> 			 		
			 	</td>
		 	</tr>  	
		    <%  
		 	} 
		  } 
		 %>  		 
	</table>


	<%}%>
