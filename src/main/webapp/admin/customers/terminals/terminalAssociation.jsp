<%-- 
    Document   : terminalAssociation
    Created on : Dec 17, 2018, 2:09:08 PM
    Author     : nmartinez
--%>

<%@page import="com.debisys.languages.Languages" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.users.SessionData"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    
    String buttonAsso  = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.buttonAssociate",SessionData.getLanguage());
    String buttonCancel= Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.buttonFinish",SessionData.getLanguage());  
    String mainTitle = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.title",SessionData.getLanguage());  
        
    String lblSite = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.label.terminalId",SessionData.getLanguage());  
    String lblMerchant = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.label.merchantName",SessionData.getLanguage());  
    String lblMerchantId = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.label.merchantId",SessionData.getLanguage());  
    String lblTerminalLabel = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.label.terminalLabel",SessionData.getLanguage());  
    String lblRep = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.label.Rep",SessionData.getLanguage());  
    String lblSubAgent = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.label.Sub",SessionData.getLanguage());  
    String lblAgent = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.label.Agent",SessionData.getLanguage());  
    String lblRepId = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.label.RepId",SessionData.getLanguage());  
    String lblSearch = Languages.getString("jsp.admin.transactions.inventorySims.search",SessionData.getLanguage());  
    String lblAlReady = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.AlReady",SessionData.getLanguage());  
    String lblCurrent = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.Current",SessionData.getLanguage());  
    String buttonDeleteAssociation = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.DeleteAssociation",SessionData.getLanguage());  
    
    String deleteWarnning = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.DeleteAssociationMessage",SessionData.getLanguage());  
    String forLabel = Languages.getString("jsp.admin.for",SessionData.getLanguage());  
            
    String deleteTitle = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.DeleteAssociationTitle",SessionData.getLanguage());  
    String noRecordFound = Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage());  
    String groupColumn = Languages.getString("jsp.admin.customers.merchants_info.terminalAssociation.GroupColumn",SessionData.getLanguage());              
            
    String iso_id = SessionData.getProperty("iso_id");
    String path = request.getContextPath();    
%>

<link href="css/font-awesome/font-awesome.min.css" type="text/css" rel="stylesheet">

<script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script> 
<script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
<link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/> 
<link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>

<link href="css/primeui-2.0-min.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="/support/includes/primeui/primeui-3.0.2-min.js" type="text/javascript" charset="utf-8"></script>

<style type="text/css">
    
    .hidden-column{
        display: none;
    }
    .bodyClassData{
        font-size: 10px;
    }
    .headerClassData{
        font-size: 10px;
    }
</style>

<script type="text/javascript">

    var totalRecords = "";
    var mainTitle = "<%=mainTitle%>";    
    var uri = '/support/admin/customers/terminals/terminalAssociationDB.jsp';
    var buttonAsso   = "<%=buttonAsso%>";
    var buttonCancel = "<%=buttonCancel%>";
    var buttonDeleteAssoMessage = "<%=buttonDeleteAssociation%>";
    var buttonDeleteAsso = "<%=buttonDeleteAssociation%>";
    var labelSiteId = "<%=lblSite%>";
    var labelMerchantName = "<%=lblMerchant%>";
    var labelMerchantId = "<%=lblMerchantId%>";
    var labelTerminalLabel = "<%=lblTerminalLabel%>";
    var labelRep = "<%=lblRep%>";
    var labelSubAgent = "<%=lblSubAgent%>";
    var labelAgent = "<%=lblAgent%>";
    var lblRepId = "<%=lblRepId%>";
    var labelGroup = "<%=groupColumn%>";
    var currentMasterIdTerminalId;
    var currentTerminalId;
    var currentMerchantId;
    var currentTerminalTypeId;
    var dialogWarnnings;
    var workingTerminalId;
    
    var yourPosition = {
        my: "center top",
        at: "center top+50"
    };
 
$(function() {
    
    $('#spinw').hide();
    
    currentMasterIdTerminalId = undefined;
    
    dialogWarnnings = $( "#dialog-Warnnings" ).dialog({
            title: "<%=deleteTitle%>",        
            autoOpen: false,
            height: "auto",
            width: 540,
            modal: true,
            buttons: [
            {text: buttonCancel,
                click: function() {
                    dialogWarnnings.dialog( "close" );
                }
            },
            {text: buttonDeleteAsso,
                click: function() {
                    var dataInfo = {'searchType': 'deleteAssociation', 'id': workingTerminalId, 'masterId': currentMasterIdTerminalId};
                    debugger;                    
                    $('#spinw').show();
                    $.ajax({
                        type:"GET",
                        data: dataInfo,
                        url: uri,
                        cache: false,
                        context:this,
                        success:function(response){                                                       
                            if (parseInt(response.trim())>1 || (workingTerminalId===parseInt(currentTerminalId)) ){                                
                                currentMasterIdTerminalId = undefined;                                
                            }
                            var data = {"searchType": "current", "siteId": currentTerminalId};
                            fillTableByName(data, "currentTerminalsResult", $('#spinw'));
                            $( ".btnSearchTerminals" ).click();
                            dialogWarnnings.dialog( "close" );
                        },
                        error:function(err){
                            $('#spinw').hide();
                            console.log(err);
                        }
                    });
                }
            }
            ],
            close: function() {
                dialogWarnnings.dialog( "close" );              
            }
        });
    dialogWarnnings.dialog( "close" );
    
    var dialog = $( "#dialog-TerminalAssociation" ).dialog({
        title: mainTitle,
        autoOpen: false,
        height: "auto",
        width: 1300,
        modal: true,
        buttons: [
        {text: buttonCancel,
            click: function() {
                dialog.dialog( "close" );
            }
        }
        ],
        close: function() {
            dialog.dialog( "close" );              
        }
    });
    dialog.dialog({
        position: yourPosition
    });

    $( "#dialog-TerminalAssociation" ).dialog( "close" );  
        
    $( ".linkAssociate" ).on( "click", function(){
        currentMasterIdTerminalId = undefined;
        var tmpIds = this.id.split("*");
        currentTerminalId = tmpIds[0];
        currentMerchantId = tmpIds[1];
        currentTerminalTypeId = tmpIds[2];
        var terminalTypeDescription = tmpIds[3];
        dialog.dialog( {title: mainTitle + " ["+currentTerminalId+ "] "+terminalTypeDescription} );
        dialog.dialog( "open" );
                
        var data = {"searchType": "current", "siteId": currentTerminalId};
        fillTableByName(data, "currentTerminalsResult", $('#spinc'));
        
        data = {"searchType": "search", "filter": "", "terminalTypeId": currentTerminalTypeId, "isoId" : "<%=iso_id%>"};
        fillTableByName(data, "terminalsResult", $('#spin'));
        
        debugger;
        data = {"searchType": "grouped", "filter": "", "terminalTypeId": currentTerminalTypeId, "isoId" : "<%=iso_id%>"};
        fillTableByName(data, "groupedTerminals", $('#spin'));
        
        
        $("#filter").val("");
    });
    
    $( ".btnSearchTerminals" ).on( "click", function(){
        var filter = $("#filter").val();
        var data = {"searchType": "search", "filter": filter, "terminalTypeId": currentTerminalTypeId, "isoId": "<%=iso_id%>"};
        fillTableByName(data, "terminalsResult", $('#spin'));
        
        data = {"searchType": "grouped", "filter": "", "terminalTypeId": currentTerminalTypeId, "isoId" : "<%=iso_id%>"};
        fillTableByName(data, "groupedTerminals", $('#spin'));
        
    });     
        
});

    function fillTableByName(dataQuery, tableName, spinnerControl){
        spinnerControl.show(1);                
        var data = $.extend(dataQuery, {"action": "count"} );        
        $.ajax({
            type:"GET",
            data: data,
            url: uri,
            cache: false,            
            context:this,
            success:function(data){                
                totalRecords = data.trim();
                fillTerminalTables(dataQuery, tableName, spinnerControl);                
            },
            error:function(err){                
                console.log(err);
                spinnerControl.hide(1);
            }
        });
    }
    
    function fillTerminalTables(dataQuery, tableName, spinnerControl){
        var searchTypeVal = dataQuery.searchType;
        var sc = false;
        var pag= {
            rows: 15,
            pageLinks:10,
            totalRecords: totalRecords
        };
        var scroH = '';
        var scroW = '';        
        $('#'+tableName).puidatatable({
            emptyMessage: '<%=noRecordFound%>',
            scrollHeight: scroH,
            scrollWidth: scroW,
            scrollable: sc,
            lazy:true,
            paginator: pag,                        
            columns: [        
                {field:'masterIdToAssociate', headerText: labelGroup, sortable:false,headerStyle:'width:70px',
                    headerClass:function(){ return showColumnBySearchType(searchTypeVal); },
                    bodyClass:function(){ return showColumnBySearchType(searchTypeVal); }
                },
    {field:'terminalId', bodyClass:'bodyClassData', headerClass:'headerClassData', headerStyle:'width:80px',  sortable:false, headerText: labelSiteId,
        content: function(row){return formatField(row,'terminalId', searchTypeVal);}},
    {field:'merchantName', bodyClass:'bodyClassData', headerClass:'headerClassData', headerStyle:'width:120px', sortable:false, headerText: labelMerchantName},                           
    {field:'merchantId', bodyClass:'bodyClassData', headerClass:'headerClassData', sortable:false, headerStyle:'width:100px', headerText: labelMerchantId },
    {field:'terminalName', bodyClass:'bodyClassData', headerClass:'headerClassData', sortable:false, headerStyle:'width:80px', headerText: labelTerminalLabel,
        content: function(row){return formatFieldTerminalLabel(row,'terminalName', searchTypeVal);}},
    {field:'repName', bodyClass:'bodyClassData', headerClass:'headerClassData', sortable:false, headerStyle:'width:120px', headerText: labelRep},
    {field:'repId', bodyClass:'bodyClassData', headerClass:'headerClassData', sortable:false, headerStyle:'width:100px', headerText: lblRepId}    
            ],                        
            sortField:'terminalId',
            sortOrder:'1',
            datasource: function(callback, ui){
                if (ui.first !== 0){
                    spinnerControl.show(1);    
                }                
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder === 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var dataQ = {"start":start, "sortField": sortField, "sortOrder":sortOrder, "rows": (start+rows)};
                var data = $.extend(dataQuery, dataQ, {"action": "list"} );            
                $.ajax({
                    type:"GET",
                    data: data,
                    url: uri,
                    cache: false,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        callback.call(this, response);
                        spinnerControl.hide(1);                        
                    },
                    error:function(err){
                        spinnerControl.hide(1);
                        console.log(err);
                    }
                });
            },
            resizableColumns: true,
            columnResizeMode: 'expand'
        });
        //scrollable: true,
        
        $('#'+tableName).puidatatable('reset');        
    }
    
    
    function showColumnBySearchType(type){
        var result = 'hidden-column';
        if (type==="grouped"){
            result = '';
        }
        return result;    
    }
    
    function deleteWarnning(terminalId){
        workingTerminalId = terminalId;
        $("#deleteWarnning").text("<%=deleteWarnning%> <%=forLabel%> ["+terminalId+"]");
        dialogWarnnings.dialog( "open" );
    }
    
    function addTerminalToAssociation(terminalId, relatedTerminaId){        
        var data = {"searchType": "associate", "currentTerminalId": terminalId, "relatedTerminaId": relatedTerminaId};
        $('#spin').show();
        $.ajax({ 
            type:"GET",
            data: data,
            url: uri,
            cache: false,
            context:this,
            success:function(response){
                $('#spin').hide();
                debugger;
                if (response.trim()==='1'){
                    setTimeout(function(){                    
                        var data = {"searchType": "current", "siteId": currentTerminalId};
                        fillTableByName(data, "currentTerminalsResult", $('#spin'));                    
                        $( ".btnSearchTerminals" ).click();
                      }, 500);                    
                }                
            },
            error:function(err){
                $('#spin').hide();
                console.log(err);
            }
        });
        
    }
    
    function formatFieldTerminalLabel(data, fieldName, type){
        var dataInfo="";        
        dataInfo = data[fieldName];        
        if ( dataInfo === undefined || dataInfo.length === 0){
            dataInfo = "N/A";
        }
        return dataInfo;
    }
    
    var colorsTable = ["#63b598", "#ce7d78", "#ea9e70", "#a48a9e", "#0d5ac1" ,
        "#f205e6" ,"#1c0365" ,"#14a9ad" ,"#4ca2f9" ,"#a4e43f" ,"#d298e2" ,"#6119d0",
        "#d2737d" ,"#c0a43c" ,"#f2510e" ,"#651be6" ,"#79806e" ,"#61da5e" ,"#cd2f00" ,
        "#9348af" ,"#01ac53" ,"#c5a4fb" ,"#996635","#b11573" ,"#4bb473" ,"#75d89e" ,
        "#2f3f94" ,"#2f7b99" ,"#da967d" ,"#34891f" ,"#b0d87b" ,"#ca4751" ,"#7e50a8" ,
        "#c4d647" ,"#e0eeb8" ,"#11dec1" ,"#289812" ,"#566ca0" ,"#ffdbe1" ,"#2f1179" ,
        "#935b6d" ,"#916988" ,"#513d98" ,"#aead3a", "#9e6d71", "#4b5bdc", "#0cd36d",
        "#250662", "#cb5bea", "#228916", "#ac3e1b", "#df514a", "#539397", "#880977",
        "#f697c1", "#ba96ce", "#679c9d", "#c6c42c", "#5d2c52", "#48b41b", "#e1cf3b",
        "#5be4f0", "#57c4d8", "#a4d17a", "#225b8", "#be608b", "#96b00c", "#088baf",
        "#f158bf", "#e145ba", "#ee91e3", "#05d371", "#5426e0", "#4834d0", "#802234",
        "#6749e8", "#0971f0", "#8fb413", "#b2b4f0", "#c3c89d", "#c9a941", "#41d158",
        "#fb21a3", "#51aed9", "#5bb32d", "#807fb", "#21538e", "#89d534", "#d36647",
        "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
        "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
        "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#21538e", "#89d534", "#d36647",
        "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
        "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
        "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#9cb64a", "#996c48", "#9ab9b7",
        "#06e052", "#e3a481", "#0eb621", "#fc458e", "#b2db15", "#aa226d", "#792ed8",
        "#73872a", "#520d3a", "#cefcb8", "#a5b3d9", "#7d1d85", "#c4fd57", "#f1ae16",
        "#8fe22a", "#ef6e3c", "#243eeb", "#1dc18", "#dd93fd", "#3f8473", "#e7dbce",
        "#421f79", "#7a3d93", "#635f6d", "#93f2d7", "#9b5c2a", "#15b9ee", "#0f5997",
        "#409188", "#911e20", "#1350ce", "#10e5b1", "#fff4d7", "#cb2582", "#ce00be",
        "#32d5d6", "#17232", "#608572", "#c79bc2", "#00f87c", "#77772a", "#6995ba",
        "#fc6b57", "#f07815", "#8fd883", "#060e27", "#96e591", "#21d52e", "#d00043",
        "#b47162", "#1ec227", "#4f0f6f", "#1d1d58", "#947002", "#bde052", "#e08c56",
        "#28fcfd", "#bb09b", "#36486a", "#d02e29", "#1ae6db", "#3e464c", "#a84a8f",
        "#911e7e", "#3f16d9", "#0f525f", "#ac7c0a", "#b4c086", "#c9d730", "#30cc49",
        "#3d6751", "#fb4c03", "#640fc1", "#62c03e", "#d3493a", "#88aa0b", "#406df9",
        "#615af0", "#4be47", "#2a3434", "#4a543f", "#79bca0", "#a8b8d4", "#00efd4",
        "#7ad236", "#7260d8", "#1deaa7", "#06f43a", "#823c59", "#e3d94c", "#dc1c06",
        "#f53b2a", "#b46238", "#2dfff6", "#a82b89", "#1a8011", "#436a9f", "#1a806a",
        "#4cf09d", "#c188a2", "#67eb4b", "#b308d3", "#fc7e41", "#af3101", "#ff065",
        "#71b1f4", "#a2f8a5", "#e23dd0", "#d3486d", "#00f7f9", "#474893", "#3cec35",
        "#1c65cb", "#5d1d0c", "#2d7d2a", "#ff3420", "#5cdd87", "#a259a4", "#e4ac44",
        "#1bede6", "#8798a4", "#d7790f", "#b2c24f", "#de73c2", "#d70a9c", "#25b67",
        "#88e9b8", "#c2b0e2", "#86e98f", "#ae90e2", "#1a806b", "#436a9e", "#0ec0ff",
        "#f812b3", "#b17fc9", "#8d6c2f", "#d3277a", "#2ca1ae", "#9685eb", "#8a96c6",
        "#dba2e6", "#76fc1b", "#608fa4", "#20f6ba", "#07d7f6", "#dce77a", "#77ecca"];
    var countCurrentColor=-1;
    var currentId="";
    var oldRecord="";
    
    function formatField(data, fieldName, type){ 
        var dataInfo="";        
        dataInfo = data[fieldName];                
        if (type === 'search'){                      
            var tId= data['terminalId'];            
            if (currentTerminalId!==tId && (currentMasterIdTerminalId === undefined || data['masterIdToAssociate']===undefined) ){
                var siteId = "'"+data['id']+"'";//this is a new one terminal to associate
                dataInfo = '<a href="javascript:void(0);" onclick="addTerminalToAssociation('+currentTerminalId+','+siteId+');" style="text-decoration: underline;">'+dataInfo+'</a>';
            }          
        } else if (type==='current'){              
            if (data['masterIdToAssociate']!==undefined){
                currentMasterIdTerminalId = data['masterIdToAssociate'];
            }            
            dataInfo = '<a href="javascript:void(0);" onclick="deleteWarnning('+dataInfo+');" style="color:red;text-decoration: underline;">'+dataInfo+'</a>';           
        }  else if (data['masterIdToAssociate'] ){                        
            debugger;
            if (oldRecord!==data['masterIdToAssociate']){
                countCurrentColor++;     
            } else{
                if (colorsTable.length===countCurrentColor){
                    countCurrentColor=0;     
                }
            }
            var colorToPaint  = colorsTable[countCurrentColor]; 
            //dataInfo = '<span style="color:'+colorToPaint+'">'+dataInfo+'</span>';
            var siteId = "'"+data['id']+"'";
            if (currentMasterIdTerminalId === undefined){
                dataInfo = '<a href="javascript:void(0);" onclick="addTerminalToAssociation('+currentTerminalId+','+siteId+');" style="color:'+colorToPaint+';text-decoration: underline;">'+dataInfo+'</a>';
            } else{
                dataInfo = '<span style="color:'+colorToPaint+'">'+dataInfo+'</span>';
            }
            oldRecord = data['masterIdToAssociate'];            
        }    
        return dataInfo;
    }
</script>    

<div id="dialog-TerminalAssociation" >
    <table cellspacing="50px" border="0" style="width: 60%">
        <tr>            
            <td colspan="2">
                <input type="text" id="filter" name="filter"  />
                <input type="button" value="<%=lblSearch%>" class="btnSearchTerminals" />           
            </td>            
            <td colspan="1" >
                <p style="margin: 3px;" >
                    <i style="font-size: 20px; margin-left: 10px;" id="spin" name="spin" class="fa fa-refresh fa-spin"></i>
                </p>
            </td>
            <td colspan="2">
                <p style="margin: 3px;" >
                    <%=lblAlReady%>
                </p>
            </td>
        </tr>    
        <tr style="height: 10px;margin-top:5px; width: 100%">
            <td colspan="3" style="vertical-align:top;margin: 10px; padding: 5px;">
                <div id="terminalsResult" name="terminalsResult" ></div>
            </td>
            <td colspan="2" style="vertical-align:top;margin: 10px; padding: 5px;">
                <div id="groupedTerminals" name="groupedTerminals" ></div>
            </td>
        </tr>
        <tr>   
            <td colspan="4"><%=lblCurrent%></td>
            <td colspan="1" id="spinc" name="spinc">
                <p style="margin: 3px;" class="fa fa-refresh fa-spin">
                    <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                </p>
            </td>
        </tr>
        <tr style="height: 10px;margin-top:5px; width: 100%" > 
            <td colspan="5" style="vertical-align:top;">
                <div id="currentTerminalsResult" name="currentTerminalsResult"  ></div>
            </td>            
        </tr>       
    </table>                               
</div>
        
<div id="dialog-Warnnings" >
    <table cellspacing="10" cellpadding="5" border="0" style="width: 90%">
        <tr>
            <td colspan="1" id="spinw" name="spinw">
                <p style="margin: 3px;" id="spinnerInfo" name="spinnerInfo" class="fa fa-refresh fa-spin">
                    <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                </p>
            </td>
        </tr>        
        <tr><td style="color:red;" ><p style="margin: 3px;" id="deleteWarnning"></p></td></tr>
    </table>
</div>
