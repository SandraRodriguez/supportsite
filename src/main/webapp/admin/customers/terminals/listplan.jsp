<%@ page language="java" import="java.util.*,
								 com.debisys.utils.*,
								 com.debisys.languages.Languages" pageEncoding="ISO-8859-1"%>

<%@page import="com.debisys.customers.Merchant"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
	String strAccessLevel = SessionData.getProperty("access_level");
	String strDistChainType = SessionData.getProperty("dist_chain_type");
	
	String plan = request.getParameter("plan");
	String merchantId = request.getParameter("merchantId");
		
	ArrayList<String> planRates = Merchant.getRatePlansDetailByMerchantId(new Integer(plan),merchantId);	
%>

	<table id="PlanModelMerchant" width="100">
	<tr>
		<td class="rowhead2" width="100" nowrap="nowrap"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.total",SessionData.getLanguage()).toUpperCase()%></td>
	<%
			if (strAccessLevel.equals(DebisysConstants.ISO)){
	%>
		<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%></td>
	<%
			}
			if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
	%>
	    <td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%></td>
	<%
			}
			if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
	%>
		<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%></td>
	<%
			}
	%>
		<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%></td>
		<td id="hotOrderProduct" class="rowhead2" width="75" align="center" ><%=Languages.getString("jsp.admin.rateplans.tsorder",SessionData.getLanguage()).toUpperCase()%></td>
	 </tr>

	<%
	boolean showColumnHotOrder=false;
	int intEvenOdd = 1;
	ArrayList<String> cacheProducts= new ArrayList<String>();
	for(String planRatesInfo : planRates )
	{		
		
		String[] infoPlan = planRatesInfo.split("\\#");
	
		cacheProducts.add(infoPlan[0]);
			
		double iso = Double.parseDouble(infoPlan[2]);
		double agent = Double.parseDouble(infoPlan[3]);
		double subagent = Double.parseDouble(infoPlan[4]);
		double rep = Double.parseDouble(infoPlan[5]);
		double merchant = Double.parseDouble(infoPlan[6]);
		String hotOrder = infoPlan[7];
		if ( !hotOrder.equals("0"))
		{
			showColumnHotOrder=true;
		}
		double total=0;
		
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			total = iso+agent+subagent+rep+merchant;
		}
		
		%>
		<tr class="row<%=intEvenOdd%>">
			<td ><%=infoPlan[0]%></td>
			<td nowrap="nowrap" ><%=infoPlan[1]%></td>
			<td><%=NumberUtil.formatAmount(Double.toString(total))%></td>
			<%
			if (strAccessLevel.equals(DebisysConstants.ISO)){
			%>
				<td><%=NumberUtil.formatAmount(infoPlan[2])%></td>
			<%
			}
			if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
			%>
			    <td><%=NumberUtil.formatAmount(infoPlan[3])%></td>
			<%
			}
			if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
			%>
			<td><%=NumberUtil.formatAmount(infoPlan[4])%></td>
			<%
			}%>
			<td><%=NumberUtil.formatAmount(infoPlan[5])%></td>
			<td><%=NumberUtil.formatAmount(infoPlan[6])%> </td>
			<td id="<%=infoPlan[0]%>hotOrder"><%=hotOrder%></td>
		</tr>
	<% 
		if (intEvenOdd == 1){
			intEvenOdd = 2;
		}else{
			intEvenOdd = 1;
		}
	}
	boolean actionShowHotOrder=false;
	if (showColumnHotOrder && SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION))
	{
		actionShowHotOrder=true;
	}
		%>
		<script type="text/javascript">
			
		<%
		if (actionShowHotOrder)
		{
			%>
			$("#hotOrderProduct").show();
			<%
		}
		else
		{
			%>
			$("#hotOrderProduct").hide();
			<%
		}
		for (String site : cacheProducts)
		{
			if (actionShowHotOrder)
			{
				%>
				$("#<%=site%>hotOrder").show();
				<%
			}
			else
			{
				%>
				$("#<%=site%>hotOrder").hide();
				<%
			}
		
		}
		%></script><%
	%>

