<%@ page import="com.debisys.utils.*,
				 com.debisys.languages.*,
                 com.debisys.terminals.Terminal,
                 java.util.*,
                 com.debisys.terminalTracking.*,
                 com.debisys.customers.Merchant,
                 com.debisys.users.User"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="ValidateEmail" class="com.debisys.utils.ValidateEmail" scope="page"/>
<jsp:setProperty name="Terminal" property="*"/>
<jsp:setProperty name="RatePlan" property="*"/>
<jsp:setProperty name="Merchant" property="*"/>
<%
int section = 2;
int section_page = 11;
 %>                 
                 
<%@ include file="/includes/security.jsp" %>
<%
  response.setCharacterEncoding("utf-8");
  
  String ref_id = request.getParameter("refID");

	boolean isUpdatedsuccessful = false;
if(request.getParameter("Remove") != null
	&& Terminal.getSerialNumber() !=null
	&& Terminal.getSiteId() != null)
{
	isUpdatedsuccessful = Terminal.removeAllSerialNumbers(SessionData);
}

  Vector terminals = Terminal.getTerminalsBySerialNumbers(SessionData, Terminal.getSerialNumber(),Terminal.getSiteId(),false, 0, 1);
  
%>
<html>
<head>
  <link href="../../default.css" type="text/css" rel="stylesheet">

  <script type="text/javascript">
    function SetSize()
    {
      var nWidth = document.getElementById("oBody").scrollWidth;
      if ( nWidth > screen.width )
      {
        nWidth = screen.width * 0.5;
      }
      var nHeight = document.getElementById("oBody").scrollHeight;
      if ( nHeight > screen.height )
      {
        nHeight = screen.height * 0.5;
      }
      window.resizeTo(nWidth, nHeight);
      window.moveTo((screen.width - nWidth)/2, (screen.height - nHeight)/2);
    }
  </script>
</head>
<body id="oBody" bgcolor="#ffffff" _onload="SetSize()">
<table width="100%"><tr><td align=center>
<form name="mainform" method="post" action="/support/admin/customers/terminal_sn_deactivation.jsp">
<input type="hidden" id="submittedITS" name="submittedITS" value="y"> 

<table border="0" cellpadding="0" cellspacing="0" width="700">
  <tr>
    <td background="/support/images/top_blue.gif" width="1%" align="left"><img src="/support/images/top_left_blue.gif" width="18" height="20"></td>
    <td background="/support/images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("com.debisys.terminals.serialnumber_deactivation.title",SessionData.getLanguage()).toUpperCase()%></b></td>
    <td background="/support/images/top_blue.gif" width="1%" align="right"><img src="/support/images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
    <td colspan=4 bgcolor="#FFFFFF">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
        <tr>
        	<td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
	     	<tr>
		        <td class="formArea">
		          <table width="300">
	              <tr class="main">
	               <td nowrap valign="top">
<%
	if(request.getParameter("Remove") == null && terminals.size()>0 
				&& ((Integer)((Vector)terminals.get(0)).get(0)).intValue()>0 )
	{
 %>
	               <%=Languages.getString("com.debisys.terminals.serialnumber_deactivation.summary", new Object[]{ Terminal.getSerialNumber() },SessionData.getLanguage())%>?
	               	<BR>
	               	<form name="" method="post" action="admin/customers/terminal_sn_deactivation.jsp">	  
	               		<input type="hidden" name="serialNumber" value="<%=Terminal.getSerialNumber()%>" />
	               		<input type="hidden" name="siteId" value="<%=Terminal.getSiteId()%>" />               		
	               		<input type="submit" name="Remove" value="<%=Languages.getString("com.debisys.terminals.serialnumber_deactivation.removeAll",SessionData.getLanguage())%>" />
					</form>
<%
	}
	else if(isUpdatedsuccessful)
	{
%>
	               <%=Languages.getString("com.debisys.terminals.serialnumber_deactivation.deactivationSuccessful",SessionData.getLanguage())%>
	               	<BR>
	               		<input type="button" name="CloseRemove" value="<%=Languages.getString("com.debisys.terminals.serialnumber_deactivation.closeWindow",SessionData.getLanguage())%>" onclick="self.close();">	
<%
	}
	else
	{
%>	
	               <%=Languages.getString("com.debisys.terminals.serialnumber_deactivation.no_serialNumbers",SessionData.getLanguage())%>
	               	<BR>	               		
	               		<input type="button" name="CloseRemove" value="<%=Languages.getString("com.debisys.terminals.serialnumber_deactivation.closeWindow",SessionData.getLanguage())%>" onclick="self.close();">
<%
	}
 %>
	            </td>
	            </tr>
	        	</table>
	        	</td>
	        </tr>
       </table>
       
    </td>
    
  </tr>
</table>
</form>
</td>
</tr>
</table>
  </body>
</html>
