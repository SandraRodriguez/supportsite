<%@ page import="java.util.Vector,
                 com.debisys.utils.*,
                 com.debisys.languages.Languages" %>
<%
int section=2;
int section_page=15;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<html>
<head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.title",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff" onload="window.focus()">
<%
Vector vResults = new Vector();

if ( request.getParameter("repId") != null )
{
	Rep.getRep(SessionData, application);
	vResults = Rep.getRepMerchants(SessionData, application);
}
%>
  <script>
  function ValidatePaymentValue(cText)
  {
    var sText = cText.value;
    var sResult = "";
    var bHasPeriod = false;
    for ( i = 0; i < sText.length; i++ )
    {
      if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) )
      {
        sResult += sText.charAt(i);
      }
      if ( !bHasPeriod && sText.charCodeAt(i) == 46 )/*period*/
      {
        sResult += sText.charAt(i);
        bHasPeriod = true;
      }
    }
    if ( sResult.length == 0 )
    {
      sResult = "0";
    }
    if ( sText != sResult )
    {
      cText.value = sResult;
    }
  }//End of function ValidatePaymentValue

  function SumTotal(bSilent)
  {
    var nMerchants = <%=vResults.size()%>;
    var dTotal = 0.0;
    var i = 0;
    
    document.getElementById("txt_Payment").value = FormatAmount(parseFloat(self.opener.GetPaymentAmountValue()));
    if ( isNaN(document.getElementById("txt_Payment").value) )
    {
      document.getElementById("txt_Payment").value = 0.0;
    }
    for (i = 0; i < nMerchants; i++)
    {
      if ( !bSilent && parseFloat(document.getElementById("txt_" + i).value) > parseFloat(document.getElementById("run_" + i).value) )
      {
        alert("<%=Languages.getString("jsp.admin.customers.reps.merchantpayment.paymentgreaterthanrunning",SessionData.getLanguage())%>");
        document.getElementById("txt_" + i).focus();
        document.getElementById("txt_" + i).select();
        return false;
      }
      dTotal += parseFloat(document.getElementById("txt_" + i).value);
    }
    document.getElementById("txt_Total").value = FormatAmount(dTotal);

    if ( !bSilent && Math.abs(dTotal) > Math.abs(document.getElementById("txt_Payment").value) )
    {
      alert("<%=Languages.getString("jsp.admin.customers.reps.merchantpayment.assignedgreaterthantotal",SessionData.getLanguage())%>");
      return false;
    }

    return true;
  }//End of function SumTotal
  
  function ValidatePayment()
  {
    if (SumTotal(false))
    {
      var nMerchants = <%=vResults.size()%>;
      var sText = "&";
      var i = 0;
      for (i = 0; i < nMerchants; i++)
      {
        sText += document.getElementById("mer_" + i).value + ":" + document.getElementById("txt_" + i).value + "&";
      }
      self.opener.SetRepMerchantPayment(sText);
      window.close();
    }
  }//End of function ValidatePayment
  
  function CancelPayment()
  {
    self.opener.SetRepMerchantPayment("");
    window.close();
  }
  
  function AssignEqual()
  {
    var nMerchants = <%=vResults.size()%>;
    var i = 0;

    document.getElementById("txt_Payment").value = FormatAmount(parseFloat(self.opener.GetPaymentAmountValue()));
    if ( isNaN(document.getElementById("txt_Payment").value) )
    {
      document.getElementById("txt_Payment").value = 0.0;
    }

    for (i = 0; i < nMerchants; i++)
    {
      document.getElementById("txt_" + i).value = FormatAmount(document.getElementById("txt_Payment").value / nMerchants);
    }
  }//End of function AssignEqual

	function FormatAmount(n)
	{
  		var s = "" + Math.round(n * 100) / 100;
  		var i = s.indexOf('.');
  		if (i < 0)
  		{
  		  return s + ".00";
  		}
  		var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3);
  		if (i + 2 == s.length)
  		{
  		  t += "0";
  		}
  		return t;
	}//End of function FormatAmount
  </script>
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="18" height="20"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
      <td background="../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.reps.merchantpayment.title",SessionData.getLanguage()).toUpperCase()%></td>
      <td width="12" height="20"><img src="../../images/top_right_blue.gif"></td>
    </tr>
    <tr>
      <td colspan="3"  bgcolor="#FFFFFF">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
          <tr>
            <td>
              <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="formArea">
<%
if (vResults.size() > 0)
{//If there are results
%>
                    <table width="100%" cellspacing="1" cellpadding="1" border="0">
                      <tr>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantid",SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantdba",SessionData.getLanguage()).toUpperCase()%></td>
                        <!--<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantlimit",SessionData.getLanguage()).toUpperCase()%></td>-->
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantrunning",SessionData.getLanguage()).toUpperCase()%></td>
                        <!--<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantavailable",SessionData.getLanguage()).toUpperCase()%></td>-->
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantvalue",SessionData.getLanguage()).toUpperCase()%></td>
                      </tr>
<%
	for ( int i = 0; i < vResults.size(); i++ )
	{
		Vector vTemp = (Vector)vResults.get(i);
%>
                      <tr>
                        <td class='row<%=(i % 2) + 1%>' nowrap><%=vTemp.get(0)%></td>
                        <td class='row<%=(i % 2) + 1%>' nowrap><%=vTemp.get(1)%></td>
                        <!--<td class='row<%=(i % 2) + 1%>' align='right' nowrap><%=NumberUtil.formatAmount(vTemp.get(2).toString())%></td>-->
                        <td class='row<%=(i % 2) + 1%>' align='right' nowrap><%=NumberUtil.formatAmount(vTemp.get(3).toString())%></td>
                        <!--<td class='row<%=(i % 2) + 1%>' align='right' nowrap><%=NumberUtil.formatAmount(vTemp.get(4).toString())%></td>-->
                        <td class='row<%=(i % 2) + 1%>' align='right' nowrap>
                          <input id='txt_<%=i%>' type=text maxlength=8 size=12 value='0.00' onpropertychange='ValidatePaymentValue(this);SumTotal(true);' onblur='this.value=FormatAmount(this.value);'>
                          <input id='mer_<%=i%>' type=hidden value='<%=vTemp.get(0)%>'>
                          <input id='run_<%=i%>' type=hidden value='<%=NumberUtil.formatAmount(vTemp.get(3).toString())%>'>
                        </td>
                      </tr>
<%
	}
%>
                      <tr class=main>
                        <td colspan=4 align='right' nowrap>
                          <b><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.totalassigned",SessionData.getLanguage())%>:</b>&nbsp;&nbsp;&nbsp;
                          <input id='txt_Total' type=text size=12 value='0' readonly>
                        </td>
                      </tr>
                      <tr class=main>
                        <td colspan=4 align='right' nowrap>
                          <b><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.paymentamount",SessionData.getLanguage())%>:</b>&nbsp;&nbsp;&nbsp;
                          <input id='txt_Payment' type=text size=12 value='<%=NumberUtil.formatAmount(request.getParameter("amount"))%>' readonly>
                        </td>
                      </tr>
                      <tr><td>&nbsp;</td></tr>
                      <tr>
                        <td colspan=4 align='center'>
                          <input id='btnApply' type=button value='<%=Languages.getString("jsp.admin.customers.reps.merchantpayment.applypayment",SessionData.getLanguage())%>' onclick='ValidatePayment();'>&nbsp;&nbsp;&nbsp;
                          <input id='btnCancel' type=button value='<%=Languages.getString("jsp.admin.customers.reps.merchantpayment.cancelpayment",SessionData.getLanguage())%>' onclick='CancelPayment();'>&nbsp;&nbsp;&nbsp;
                          <input id='btnAssign' type=button value='<%=Languages.getString("jsp.admin.customers.reps.merchantpayment.assigntomerchants",SessionData.getLanguage())%>' onclick='AssignEqual();'>
                        </td>
                      </tr>
                    </table>
<%
}//End of if there are results
else
{
%>
                    <script>
                      self.opener.SetRepMerchantPayment("&");
                      window.close();                    
                    </script>
<%
}
%>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>