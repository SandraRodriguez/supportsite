<%@ page import="com.debisys.users.User,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*" %>
<%
int section=2;
int section_page=12;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:setProperty name="User" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
if (!User.checkPermissions(SessionData)){ //check permissions
  response.sendRedirect("/support/message.jsp?message=1");
  return;
}


Hashtable userErrors = new Hashtable();
String strMessage = request.getParameter("message");
Vector vecUsers = new Vector();
int numWebPermissionTypes = User.getSequenceLastWebPermissionTypes();
if (strMessage == null){
	strMessage="";
}
if (request.getParameter("action") != null){
	try{
		if (request.getParameter("action").equals("add")){
			if (request.getParameter("perm_25") !=null){
				if(User.validateAddUpdateMobileUser(SessionData)){
					String user_enabled = "" + request.getParameter("enabled");
					User.setStatus(user_enabled.equals("y")?1:0);
					User.addUser(SessionData,application,true);
					//now add permissions
					//delete them all first
					User.deleteUserPermissions();
					for (int j=1;j<=numWebPermissionTypes;j++){
						if (request.getParameter("perm" + "_" + j) != null){
							User.setPermissionTypeId(Integer.toString(j));
							User.addUserPermission();
						}
					}
					User.setUsername("");
					User.setPassword("");
					User.setName("");
					User.setEmail("");
				}
			}else{
				if (User.validateAddUpdateUser(SessionData, getServletContext())){
					String user_enabled = "" + request.getParameter("enabled");
					User.setStatus(user_enabled.equals("y")?1:0);
					User.addUser(SessionData,application,false);
					//now add permissions
					//delete them all first
					User.deleteUserPermissions();
					for (int j=1;j<=numWebPermissionTypes;j++){
						if (request.getParameter("perm" + "_" + j) != null){
							User.setPermissionTypeId(Integer.toString(j));
							User.addUserPermission();
						}
					}
						User.setUsername("");
					User.setPassword("");
					User.setName("");
					User.setEmail("");
				}
			}
			vecUsers = User.getUserList(SessionData);
			userErrors = User.getErrors();
		}else if(request.getParameter("action").equals("update")){
			String[] passwordIds = request.getParameterValues("passwordIds");
			if (passwordIds != null){
				String strPasswordId = "";
				for(int i=0; i<passwordIds.length;i++){
					strPasswordId = passwordIds[i];
					if (strPasswordId !=null && !strPasswordId.equals("")){
						if (request.getParameter("username_" + strPasswordId) != null && request.getParameter("password_" + strPasswordId) != null){
							User.setpasswordId(strPasswordId);
							User.setUsername(request.getParameter("username_" + strPasswordId));
							User.setPassword(request.getParameter("password_" + strPasswordId));
							String name =request.getParameter("name_" + strPasswordId);
							if(name==null){
							name ="";
							} 
							User.setName(name);
							String email =request.getParameter("email_" + strPasswordId);
							if(email==null){
							email ="";
							} 
							User.setEmail(email);
							String user_enabled = "" + request.getParameter("enabled" + strPasswordId);
							User.setStatus(user_enabled.equals("y")?1:0);
							if (User.validateEmail(SessionData)){
								String reason = "" + request.getParameter("reason" + strPasswordId);
								User.updateUser(SessionData, application, reason);
								//now update permissions
								//delete them all first
								User.deleteUserPermissions();
								for (int j=1;j<=numWebPermissionTypes;j++){
									if (request.getParameter("perm_" + strPasswordId + "_" + j) != null){
										User.setPermissionTypeId(Integer.toString(j));
										User.addUserPermission();
									}
								}
							}else if (User.isError()){
								Hashtable tempErrors = User.getErrors();
							if (tempErrors.containsKey("email")){
									userErrors.put("email_" + strPasswordId, tempErrors.get("email").toString());
								}
								
							}
						}
					}
				}
			}
			User.setpasswordId("");
			User.setUsername("");
			User.setPassword("");
			User.setName("");
			User.setEmail("");
			vecUsers = User.getUserList(SessionData);
		}else if(request.getParameter("action").equals("delete")){
			User.deleteUser(SessionData,application);
			vecUsers = User.getUserList(SessionData);
			userErrors = User.getErrors();
		}
	}catch (Exception e){
	}
}else{
	vecUsers = User.getUserList(SessionData);
}
%>
<%@ include file="/includes/header.jsp" %>
		<style>
			#popBG{z-index:2; position:absolute; left:0px; top: 0px; width: 100%; height: 1000px;background-image: url(images/popupbgpixolive.png); filter: alpha (opacity=100);}
			#pop{z-index:3; position:relative; border: 1px solid #333333; text-align:center;}
			#popContent{overflow: scroll;}
			#popClose{float:right; margin:5px; cursor:pointer; position:relative;}
			.grayLabel{color:gray; font-weight:bold;}
		</style>
		<script type="text/javascript">
			var current_enabled;
			//$(function () {
			$(document).ready(function(){
				$("#popClose, #cancelDisable, #okDisable").click(
					function(){
						var cindex = current_enabled.replace("enabled", "");
						$("#popBG").fadeOut('slow');
						if(this.id != "okDisable"){
							if(current_enabled) {
								$("#" + current_enabled).attr("checked", true);
							}
							$("#reason" + cindex).val("");
						}else{
							var reason = $.trim($("#reason").val())
							if(reason){
								$("#reason" + cindex).val(reason);
							}else{
								alert("Please specify a reason");
								$("#popBG").fadeIn('fast');
								return;
							}
						}
						$("#reason").val("");
						$("#" + current_enabled).focus();
						current_enabled = null;
					}
				);
				/* Update links */
				$('.updatePass').click(function(){
					var pid = $(this).data('pid');
					var lnk = 'admin/customers/user_logins_update_pass.jsp?refId=<%=User.getRefId()%>&refType=<%=User.getRefType()%>&action=updatePassword';
					lnk = lnk + '&oldPassword=' + $('#password_'+pid).val();
					lnk = lnk + '&passId=' + $('#pid_'+pid).val();
					lnk = lnk + '&mobileOn=' + $('#perm_'+pid+'_25').is(':checked').toString();
					lnk = lnk + '&personId=' + $('#personId_'+pid).val();
					lnk = lnk + '&personName=' + $('#personName_'+pid).val().replace(/\s/g, '');
					$('#messages').load(lnk, function(data){
						$('#messages').html(data);// put the response message in the message box
						if($('#messages').children().first().hasClass('error')){// if is an error message
							// restore the original password
							$('#password_'+pid).val($('#password_'+pid).data('original'));
						}
					});
					return false;
				});				
			});
			//});	
			function showDisableDialog(obj){
				if(obj.checked == false && $(obj).attr("startvalue") == "1"){
					var w = $(document).width();
					var h = $(document).height(); 
					var p_w = 400;//$("#pop").width();
					var p_h = 250;//$("#pop").height();
					p_w = (p_w < w*0.8)?p_w:w*0.8;
					p_h = (p_h < h*0.6)?p_h:h*0.6;
					var cw = $(obj).position().left; //(w/2) - (p_w/2);
					var ch = $(obj).position().top; //(h/2) - (p_h/2);
					//$("#popContent").html("");
					current_enabled = obj.id;
					$("#popContent").css("width", (p_w-10) + "px").css("height", (p_h-10) + "px");
					$("#pop").css("position", "absolute").css("width", p_w + "px").css("height", p_h + "px").css("left", cw + "px").css("top", ch + "px");
					$("#popBG").css("width", w + "px").css("height", h + "px").fadeIn('fast');
				}
			}
		</script>
		<style>
			.error{border: 1px solid #880000; background-color: #FF8888; padding: 30px;}
			.success{border: 1px solid #008800; background-color: #88FF88; padding: 30px;}
		</style>
	<body>
		<table border="0" cellpadding="0" cellspacing="0" width="750">
			<tr>
				<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
				<td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.customers.user_logins.title",SessionData.getLanguage()).toUpperCase()%></td>
				<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
			</tr>
			<tr>
				<td colspan="3">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td class="formArea">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td class=main align=center>
											<br><br>
											<%=Languages.getString("jsp.admin.customers.user_logins.update_instructions",SessionData.getLanguage())%>
											<br><br>
											<div id="messages"></div>
											<form method=post action="admin/customers/user_logins.jsp">
												<!--<input type="hidden" id="oldPassword" name="oldPassword" value="">  --> 
												<table border=0 width=200>
													<tr>
														<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.username",SessionData.getLanguage()).toUpperCase()%></td>
														<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.password",SessionData.getLanguage()).toUpperCase()%></td>
														<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.name",SessionData.getLanguage()).toUpperCase()%></td>
														<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.email",SessionData.getLanguage()).toUpperCase()%></td>
														<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.enabled",SessionData.getLanguage()).toUpperCase()%></td>
														<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.statuslastchange",SessionData.getLanguage()).toUpperCase()%></td>
<%
//get the user level type
String userAccessLevel = "";
if (User.getRefType().equals(DebisysConstants.PW_REF_TYPE_AGENT)){
	userAccessLevel = DebisysConstants.AGENT;
}else if (User.getRefType().equals(DebisysConstants.PW_REF_TYPE_SUBAGENT)){
	userAccessLevel = DebisysConstants.SUBAGENT;
}else if (User.getRefType().equals(DebisysConstants.PW_REF_TYPE_REP_ISO)){
	userAccessLevel = DebisysConstants.REP;
}else if (User.getRefType().equals(DebisysConstants.PW_REF_TYPE_MERCHANT)){
	userAccessLevel = DebisysConstants.MERCHANT;
}
Vector vecAssignablePermissions = User.getAssignablePermissions(SessionData, userAccessLevel);
Iterator it = null;
if (vecAssignablePermissions !=null && vecAssignablePermissions.size() > 0){
    it = vecAssignablePermissions.iterator();
    while (it.hasNext()){
        Vector vecTemp = null;
        vecTemp = (Vector) it.next();
        String codeLanguage = vecTemp.get(1).toString().replaceAll("/", "_").replaceAll(" ","_");
        if(vecTemp.get(0).toString().equals("13")){
            if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
                out.println("<td class=rowhead2>" + Languages.getString("jsp.admin_" + codeLanguage + "_Code",SessionData.getLanguage()).toUpperCase().replaceFirst(" ", "<br>") + "</td>");
            }
        }else if(vecTemp.get(0).toString().equals("19")){
            if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
                out.println("<td class=rowhead2>" + Languages.getString("jsp.admin_" + codeLanguage + "_Code",SessionData.getLanguage()).toUpperCase().replaceFirst(" ", "<br>") + "</td>");
            }
        }else{                    
            String permLangDesc = Languages.getString("jsp.admin_" + codeLanguage + "_Code",SessionData.getLanguage());
            System.out.println(permLangDesc + " "+codeLanguage);
            out.println("<td class=rowhead2>" + permLangDesc.toUpperCase().replaceFirst(" ", "<br>") + "</td>");
        }
    }
}
%>
													</tr>
<%
// Get ids of all checkboxes for Mobile Authentication
Vector vecTemp3 = null;
String Ids = "";
Iterator it3 = vecAssignablePermissions.iterator();

while(it3.hasNext()){
	vecTemp3 = (Vector) it3.next();
	// If permissions 13 or 19 exist, add them to the list to be greyed out and unchecked.
	// Otherwise do nothing.
	if( vecTemp3.get(0).toString().equals("13") || vecTemp3.get(0).toString().equals("19")){
		if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
			Ids += "_" + vecTemp3.get(0).toString();
		}	
	}
	// Leave permission 5 (Update Credit Limits) alone for this web permission
	else if(!vecTemp3.get(0).toString().equals("5")){
		Ids += "_" + vecTemp3.get(0).toString();
	}
}
if (vecUsers !=null && vecUsers.size() > 0){
	int intEvenOdd = 1;
	it = vecUsers.iterator();
	while (it.hasNext()){
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		String tmpPasswordId = (String) vecTemp.get(0);
%>
					<input type=hidden id=pId name=pId value=<%=tmpPasswordId%>>
<%
		out.print("<input class=row"+intEvenOdd+" type=\"hidden\" name=\"passwordIds\" value=\""+tmpPasswordId+"\"><tr class=row"+intEvenOdd+">" +
			"<td><input type=\"text\" readonly=\"readonly\" id=\"username_" +tmpPasswordId+ "\" name=\"username_" +tmpPasswordId+ "\" onChange=\"checkMobileAuth(this.value, 'perm_" +tmpPasswordId+ "_25', '" + Ids + "', document)\" value=\"");
		if (userErrors != null && userErrors.containsKey("username_" + tmpPasswordId) && request.getParameter("action").equals("update") ){
			out.print(request.getParameter("username_" + tmpPasswordId) +"\" size=\"20\" maxlength=\"50\"><font color=ff0000>*<br>" + userErrors.get("username_" + tmpPasswordId) + "</font>");
		}else{
			out.print(vecTemp.get(1) +"\" size=\"20\" maxlength=\"50\">");
		}
		%></td><td><input type="password"  data-original="<%=vecTemp.get(2)%>" id="password_<%=tmpPasswordId%>" name="password_<%=tmpPasswordId%>" onChange="checkMobileAuth(this.value, 'perm_<%=tmpPasswordId%>_25', '<%=Ids%>', document)"<%
		if (userErrors != null && userErrors.containsKey("password_" + tmpPasswordId) && request.getParameter("action").equals("updatePassword")){
			%> value="<%=request.getParameter("password_" + tmpPasswordId)%>" size="20" maxlength="50"><font color=ff0000>*<br><%=userErrors.get("password_" + tmpPasswordId)%></font><%
		}else{
			%> value="<%=vecTemp.get(2)%>" size="20" maxlength="50"><%
		}
		out.print("</td><td><input type=\"text\" id=\"name_" +tmpPasswordId+ "\" name=\"name_" +tmpPasswordId+ "\" onChange=\"checkMobileAuth(this.value, 'perm_" +tmpPasswordId+ "_25', '" + Ids + "', document)\" value=\"");
		if (userErrors != null && userErrors.containsKey("name_" + tmpPasswordId) && request.getParameter("action").equals("update") ){
			out.print(request.getParameter("name_" + tmpPasswordId) + "\" size=\"20\" maxlength=\"50\"><font color=ff0000>*<br>" + userErrors.get("name" + tmpPasswordId) + "</font>");
		}else{
			out.print(vecTemp.get(6) +"\" size=\"20\" maxlength=\"50\">");
		}
		out.print("</td><td><input type=\"text\" id=\"email_" +tmpPasswordId+ "\" name=\"email_" +tmpPasswordId+ "\" onChange=\"checkMobileAuth(this.value, 'perm_" +tmpPasswordId+ "_25', '" + Ids + "', document)\" value=\"");
		if (userErrors != null && userErrors.containsKey("email_" + tmpPasswordId) && request.getParameter("action").equals("update") ){
			out.print(request.getParameter("email_" + tmpPasswordId) + "\" size=\"20\" maxlength=\"50\"><font color=ff0000>*<br>" + userErrors.get("email_" + tmpPasswordId) + "</font>");
		}else{
			out.print(vecTemp.get(7) +"\" size=\"20\" maxlength=\"50\">");
		}
		out.println("</td>");
%>
														<td align="left" nowrap>
															<input type="checkbox" id="enabled<%=tmpPasswordId%>" name="enabled<%=tmpPasswordId%>" value="y" onclick="showDisableDialog(this);" startvalue="<%=vecTemp.get(4)%>"
<%
		if(vecTemp.get(4).equals("1")){
			%>checked="checked" <%
		}
%>
>
															<input type="hidden" id="reason<%=tmpPasswordId%>" name="reason<%=tmpPasswordId%>" value="">
														</td>
														<td align="left" nowrap><%=vecTemp.get(5) %></td>
<%
		//begin permissions check boxes
		Hashtable hashPerms = (Hashtable) vecTemp.get(3);
		if (vecAssignablePermissions !=null && vecAssignablePermissions.size() > 0){
			Iterator it2 = vecAssignablePermissions.iterator();
			while (it2.hasNext()){
				Vector vecTemp2 = null;
				vecTemp2 = (Vector) it2.next();
				String tmpPermTypeId = vecTemp2.get(0).toString();
				if(tmpPermTypeId.equals("19") ){
					if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
						out.print("<td align=center><input type=checkbox id=\"perm_" + tmpPasswordId +  "_" + tmpPermTypeId + "\" name=\"perm_" + tmpPasswordId +  "_" + tmpPermTypeId + "\" value=y ");
						if (hashPerms.containsKey(tmpPermTypeId)){
							out.print("checked ");
						}
						// Disable this permission if the Mobile Authentication permission is enabled
						if (hashPerms.containsKey("25")){
							out.print("DISABLED ");
						}
						out.print("></td>");
					}
				}else if(tmpPermTypeId.equals("13") ){
					if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
						out.print("<td align=center><input type=checkbox id=\"perm_" + tmpPasswordId +  "_" + tmpPermTypeId + "\" name=\"perm_" + tmpPasswordId +  "_" + tmpPermTypeId + "\" value=y ");
						if (hashPerms.containsKey(tmpPermTypeId)){
							out.print("checked ");
						}
						// Disable this permission if the Mobile Authentication permission is enabled
						if (hashPerms.containsKey("25")){
							out.print("DISABLED ");
						}
						out.print("></td>");
					}
				}
				// BEGIN DBSY-431 - SW
				// Segregating permission 5 so that it will not be disabled when permission 25 is set.
				else if(tmpPermTypeId.equals("5") ){
					out.print("<td align=center><input type=checkbox id=\"perm_" + tmpPasswordId +  "_" + tmpPermTypeId + "\" name=\"perm_" + tmpPasswordId +  "_" + tmpPermTypeId + "\" value=y ");
					if (hashPerms.containsKey(tmpPermTypeId)){
						out.print("checked ");
					}
					out.print("></td>");
				}
				// Special permission for Mobile Authentication
				else if( tmpPermTypeId.equals("25")){
					%><td align=center><input type=checkbox id="perm_<%=tmpPasswordId%>_<%=tmpPermTypeId%>" name="perm_<%=tmpPasswordId%>_<%=tmpPermTypeId%>" value="y" onClick="MobileAuthentication(document, 'perm_<%=tmpPasswordId%>_<%=tmpPermTypeId%>', '<%=Ids%>');"<%
					if (hashPerms.containsKey(tmpPermTypeId)){
						%>checked="checked"<%
					}
					%>></td><%	
				}else{// END DBSY-431 - SW
					%><td align=center><input type=checkbox id="perm_<%=tmpPasswordId%>_<%=tmpPermTypeId%>" name="perm_<%=tmpPasswordId%>_<%=tmpPermTypeId%>" value="y" <%
					if (hashPerms.containsKey(tmpPermTypeId)){
						%>checked="checked" <%
					}
					// Disable this permission if the Mobile Authentication permission is enabled
					if (hashPerms.containsKey("25")){
						%>disabled="disabled" <%
					}
					%>></td><%
				} 
			}
		}
%>
														<td align="center">
															<input type="hidden" id="pid_<%=vecTemp.get(0)%>" name="pid_<%=vecTemp.get(0)%>" value="<%=vecTemp.get(0)%>" />
															<input type="hidden" id="personId_<%=vecTemp.get(0)%>" name="personId_<%=vecTemp.get(0)%>" value="<%=vecTemp.get(1)%>" />
															<input type="hidden" id="personName_<%=vecTemp.get(0)%>" name="personName_<%=vecTemp.get(0)%>" value="<%=vecTemp.get(6)%>" />
															<a href="admin/customers/user_logins.jsp?action=delete&passwordId=<%=vecTemp.get(0)%>&refId=<%=User.getRefId()%>&refType=<%=User.getRefType()%>">
																<%=Languages.getString("jsp.admin.customers.user_logins.delete",SessionData.getLanguage())%>
															</a>
														</td>
														<td align="center">
															<a class="updatePass" href="admin/customers/user_logins.jsp?refId=<%=User.getRefId()%>&refType=<%=User.getRefType()%>" data-pid="<%=vecTemp.get(0)%>">
																<%=Languages.getString("jsp.admin.customers.user_logins.updatePassword",SessionData.getLanguage())%>
															</a>
														</td>
													</tr>
<%
	}
}else{
%>
													<tr>
														<td colspan="3" class=row2>
															<%=Languages.getString("jsp.admin.customers.user_logins.not_found",SessionData.getLanguage())%>
														</td>
													</tr>
<%
}
%>
												</table>
												<input type=hidden name=action value=update>
												<input type=hidden name=refId value="<%=User.getRefId()%>">
												<input type=hidden name=refType value="<%=User.getRefType()%>">
												<input type=submit name=submit value="<%=Languages.getString("jsp.admin.customers.user_logins.update_users",SessionData.getLanguage())%>">
											</form>
											<br><br>
											<%=Languages.getString("jsp.admin.customers.user_logins.add_instructions",SessionData.getLanguage())%><br>
											<br><br>
											<form method=post action="admin/customers/user_logins.jsp">
												<table cellpadding=3 cellspacing=0 class=main width="400">
													<tr>
														<td align=center><b><%=Languages.getString("jsp.admin.customers.user_logins.enter_new_user",SessionData.getLanguage())%></b></td>
													</tr>
<%
if (userErrors != null && userErrors.containsKey("username") && request.getParameter("action").equals("add") ){
%>
													<tr>
														<td colspan=2><font color=ff0000>*<%=User.getFieldError("username")%></font></td>
													</tr>
<%
}
if (userErrors != null && userErrors.containsKey("password") && request.getParameter("action").equals("add") ){
%>
													<tr>
														<td><font color=ff0000>*<%=User.getFieldError("password")%></font></td>
													</tr>
<%
}
if (userErrors != null && userErrors.containsKey("email") && request.getParameter("action").equals("add") ){
%>
													<tr>
														<td colspan=2><font color=ff0000>*<%=User.getFieldError("email")%></font></td>
													</tr>
<%
}
if (userErrors != null && userErrors.containsKey("name") && request.getParameter("action").equals("add") ){
%>
													<tr>
														<td><font color=ff0000>*<%=User.getFieldError("name")%></font></td>
													</tr>
<%
}
if (userErrors != null && userErrors.containsKey("lastname") && request.getParameter("action").equals("add") ){
%>
													<tr>
														<td><font color=ff0000>*<%=User.getFieldError("lastname")%></font></td>
													</tr>
<%
}
%>
													<tr>
														<td>
															<table border=0 width=200 class=main>
																<tr>
																	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.username",SessionData.getLanguage()).toUpperCase()%></td>
																	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.password",SessionData.getLanguage()).toUpperCase()%></td>
																	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.name",SessionData.getLanguage()).toUpperCase()%></td>
																	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.email",SessionData.getLanguage()).toUpperCase()%></td>
																	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.user_logins.enabled",SessionData.getLanguage()).toUpperCase()%></td>
<%
if (vecAssignablePermissions !=null && vecAssignablePermissions.size() > 0){
	it = vecAssignablePermissions.iterator();
	while (it.hasNext()){
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		if ( vecTemp.get(0).toString().equals("19") ){
			if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
				out.println("<td class=rowhead2>" + Languages.getString("jsp.admin_" + vecTemp.get(1).toString().replaceAll("/", "_").replaceAll(" ","_") + "_Code",SessionData.getLanguage()).replaceFirst(" ", "<br>").toUpperCase() + "</td>");
			}
		}else if ( vecTemp.get(0).toString().equals("13") ){
			if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
				out.println("<td class=rowhead2>" + Languages.getString("jsp.admin_" + vecTemp.get(1).toString().replaceAll("/", "_").replaceAll(" ","_") + "_Code",SessionData.getLanguage()).replaceFirst(" ", "<br>").toUpperCase() + "</td>");
			}
		}else{
			out.println("<td class=rowhead2>" + Languages.getString("jsp.admin_" + vecTemp.get(1).toString().replaceAll("/", "_").replaceAll(" ","_") + "_Code",SessionData.getLanguage()).replaceFirst(" ", "<br>").toUpperCase() + "</td>");
		}
	}
}
%>
																</tr>
																<tr>
																	<td align="left" nowrap><input type="text" id="username" name="username" value="<%=User.getUsername()%>" <%out.print("onChange=\"checkMobileAuth(this.value, 'perm_25', '" + Ids + "', document)\"");%> size="20" maxlength="50"><%if (userErrors != null && userErrors.containsKey("username") && request.getParameter("action").equals("add") ) out.print("<font color=ff0000>*</font>");%></td>
																	<td align="left" nowrap><input type="text" id="password" name="password" value="<%=User.getPassword()%>" <%out.print("onChange=\"checkMobileAuth(this.value, 'perm_25', '" + Ids + "', document)\"");%> size="20" maxlength="50"><%if (userErrors != null && userErrors.containsKey("password") && request.getParameter("action").equals("add")) out.print("<font color=ff0000>*</font>");%></td>
																	<td align="left" nowrap><input type="text" id="name" name="name" value="<%=User.getName()%>" <%out.print("onChange=\"checkMobileAuth(this.value, 'perm_25', '" + Ids + "', document)\"");%> size="20" maxlength="50"><%if (userErrors != null && userErrors.containsKey("name") && request.getParameter("action").equals("add")) out.print("<font color=ff0000>*</font>");%></td>
																	<td align="left" nowrap><input type="text" id="email" name="email" value="<%=User.getEmail()%>" <%out.print("onChange=\"checkMobileAuth(this.value, 'perm_25', '" + Ids + "', document)\"");%> size="20" maxlength="50"><%if (userErrors != null && userErrors.containsKey("email") && request.getParameter("action").equals("add") ) out.print("<font color=ff0000>*</font>");%></td>
																	<td align="left" nowrap><input type="checkbox" id="enabled" name="enabled" value="y" <%=(User.getStatus() == 1?"checked":"") %> <%out.print("onChange=\"checkMobileAuth(this.value, 'perm_25', '" + Ids + "', document)\"");%> size="20" maxlength="50"></td>
<%
if (vecAssignablePermissions !=null && vecAssignablePermissions.size() > 0){
	it = vecAssignablePermissions.iterator();// New iterator for DBSY-431 
	while (it.hasNext()){
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		String tmpPermTypeId = vecTemp.get(0).toString();
		if( tmpPermTypeId.equals("19")){
			if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
				out.print("<td align=center ><input type=checkbox name=\"perm_" + tmpPermTypeId + "\" value=y ");
				if (request.getParameter("perm_" + tmpPermTypeId) !=null){
					out.print("checked ");
				}
				// Disable this permission if the Mobile Authentication permission is enabled
				if (request.getParameter("perm_25") !=null){
					out.print("DISABLED ");
				}
				out.print("></td>");
			}	
		}else if( tmpPermTypeId.equals("13")){
			if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
				out.print("<td align=center><input type=checkbox id=\"perm_" + tmpPermTypeId + "\" name=\"perm_" + tmpPermTypeId + "\" value=y ");
				if (request.getParameter("perm_" + tmpPermTypeId) !=null){
					out.print("checked ");
				}
				// Disable this permission if the Mobile Authentication permission is enabled
				if (request.getParameter("perm_25") !=null){
					out.print("DISABLED ");
				}
				out.print("></td>");
			}	
		}
		// BEGIN DBSY-431 - SW
		// Segregating permission 5 so that it will not be disabled when permission 25 is set.
		else if( tmpPermTypeId.equals("5")){
			out.print("<td align=center><input type=checkbox id=\"perm_" + tmpPermTypeId + "\" name=\"perm_" + tmpPermTypeId + "\" value=y ");
			if (request.getParameter("perm_" + tmpPermTypeId) !=null){
				out.print("checked ");
			}
			out.print("></td>");	
		}
		// Special permission for Mobile Authentication
		else if( tmpPermTypeId.equals("25")){
			out.print("<td align=center><input type=checkbox id=\"perm_" + tmpPermTypeId + "\" name=\"perm_" + tmpPermTypeId + "\" value=y onClick=\"MobileAuthentication(document, \'perm_" + tmpPermTypeId + "\', \'" + Ids + "\');\"");
			if (request.getParameter("perm_" + tmpPermTypeId) !=null){
				out.print("checked");
			}
			out.print("></td>");	
		} // END DBSY-431 - SW
		else{
			out.print("<td align=center><input type=checkbox id=\"perm_" + tmpPermTypeId + "\" name=\"perm_" + tmpPermTypeId + "\" value=y ");
			if (request.getParameter("perm_" + tmpPermTypeId) !=null){
				out.print("checked ");
			}
			// Disable this permission if the Mobile Authentication permission is enabled
			if (request.getParameter("perm_25") !=null){
				out.print("DISABLED ");
			}
			out.print("></td>");
		}
	}
}
%>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td align=center>
															<input type=hidden name=action value=add>
															<input type=hidden name=refId value="<%=User.getRefId()%>">
															<input type=hidden name=refType value="<%=User.getRefType()%>">
															<input type=submit name=submit value="<%=Languages.getString("jsp.admin.customers.user_logins.add_user",SessionData.getLanguage())%>">
														</td>
													</tr>
												</table>
												<br><br>
											</form>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div id="popBG" style="display:none;">
			<div id="pop">
				<table border="5" cellpadding="0" cellspacing="0" bgcolor="white" width="100%" height="100%">
					<tr>
						<th>
							<div id="popClose"><img src="images/closebutton.png"></div>
							<h3>Disable User</h3>
						</th>
					</tr>
					<tr>
						<th align="left">
							<div style="padding:5px"><%= Languages.getString("com.debisys.terminals.jsp.terminaldisabled.reason",SessionData.getLanguage()) %>:</div>
						</th>
					</tr>
					<tr>
						<td align="center">
							<textarea id="reason" name="reason" rows="4" cols="50"></textarea><br>
						</td>
					</tr>
					<tr>
						<td align="right" nowrap>
							<input type="button" id="okDisable" value="Ok">
							<input type="button" id="cancelDisable" value="Cancel">
						</td>
					</tr>
				</table>
			</div>
		</div>
<%@ include file="/includes/footer.jsp" %>