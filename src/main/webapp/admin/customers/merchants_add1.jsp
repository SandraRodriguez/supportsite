<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.reports.TransactionReport,
                 com.debisys.utils.*"
%>
<%
int section=2;
int section_page=9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
if ((strAccessLevel.equals(DebisysConstants.ISO)
    || strAccessLevel.equals(DebisysConstants.REP))
    ||
    (!RatePlan.getRepId().equals("")
    && (strAccessLevel.equals(DebisysConstants.AGENT)
        || strAccessLevel.equals(DebisysConstants.SUBAGENT))))
{
  response.sendRedirect("/support/admin/customers/merchants_add2.jsp");
}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
<%
if (!((strAccessLevel.equals(DebisysConstants.ISO)
    || strAccessLevel.equals(DebisysConstants.REP))
    ||
    (!RatePlan.getRepId().equals("")
    && (strAccessLevel.equals(DebisysConstants.AGENT)
        || strAccessLevel.equals(DebisysConstants.SUBAGENT)))))
{
    %>
    <form name="mainform" method="post" action="admin/customers/merchants_add1.jsp">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea">
            <%=Languages.getString("jsp.admin.customers.merchants_add.please_select_rep",SessionData.getLanguage())%>
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
<table width=400>
<tr>
    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.customers.merchants_add.select_rep",SessionData.getLanguage())%></td>
    <td class=main valign=top>

<%
  Vector vecRepList = RatePlan.getRepList(SessionData);
  if (vecRepList != null && vecRepList.size() > 0)
  {
    out.println("<select name=\"repId\">");
     Iterator it = vecRepList.iterator();
       while (it.hasNext())
       {
         Vector vecTemp = null;
         vecTemp = (Vector) it.next();
         String tmpRepId = vecTemp.get(0).toString();
         out.println("<option value=\"" + tmpRepId + "\" ");
         if (RatePlan.getRepId().equals(tmpRepId))
         {
           out.println("selected");
         }
         if (strAccessLevel.equals(DebisysConstants.AGENT))
         {
         out.println(">" + vecTemp.get(2) + "->" + vecTemp.get(1) + "</option>");
         }
         else
         {
         out.println(">" + vecTemp.get(1) + "</option>");
         }
       }
    out.println("</select>");

  }
  else
  {
      out.println("<font color=ff0000>"+Languages.getString("jsp.admin.customers.merchants_add.no_reps_found",SessionData.getLanguage())+"</font>");
  }


%>
</td>
</tr>
<tr>
    <td class=main colspan=2 align=center>
        <%
        if (vecRepList != null && vecRepList.size() > 0)
        {
        %>
      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.submit_next",SessionData.getLanguage())%>">
      <%}%>
    </td>
</tr>
</table>
               </td>
              </tr>
              </form>
            </table>

          </td>
      </tr>
    </table>

    <%
  }

%>


</td>
</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>