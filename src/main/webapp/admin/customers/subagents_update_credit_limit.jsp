<%@ page import="com.debisys.utils.NumberUtil,
                 java.util.Hashtable"%>
<%
int section=2;
int section_page=17;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="page"/>
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%

  if (NumberUtil.isNumeric(Rep.getCreditLimit()))
  {
    if (Rep.getAssignedCredit(SessionData) > (Double.parseDouble(Rep.getCreditLimit())))
    {
      response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=8");
      return;
    }
    else if (Double.parseDouble(Rep.getCreditLimit()) < 0)
    {
      response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=6");
      return;
    }
    else
    {
        if (Rep.checkAgentCreditLimit(SessionData, application))
        {
           Rep.updateSubagentLiabilityLimit2(SessionData,application);
            response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId());
            return;
        }else
        {
          response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=31");
          return;
        }
            
    }
  }
  else
  {
    response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=6");
    return;
  }

%>