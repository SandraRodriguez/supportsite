<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.languages.Languages" %>
<%
int section=2;
int section_page=4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
String merchantId = "";
String startDate = "";
String endDate = "";
merchantId = request.getParameter("merchantId");
startDate = request.getParameter("startDate");
endDate = request.getParameter("endDate");


//String strUrlParams = "?merchantId=" + merchantId + "&startDate=" + Merchant.getStartDate() + "&endDate=" + Merchant.getEndDate();

if (request.getParameter("search") != null)
{
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    
    vecSearchResults = Merchant.getMerchantNotes(SessionData);
}

%>
<html>
	<head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.customers.merchants_notes.title",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="600" background="../../images/top_blue.gif">
	<tr>
	    <td background="../../images/top_blue.gif" align=left><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
	    <td background="../../images/top_blue.gif" width="2000" class="formAreaTitle" align=left>&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_notes.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td background="../../images/top_blue.gif" align=right><img src="../../images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
	  			<tr>
	    			<td>
		    			<form name="mainform" method="post" onSubmit="scroll();">
	      				<table border="0" width="100%" cellpadding="0" cellspacing="0">
	     					<tr>
		        				<td class="formArea2">
		          					<table width="300">
	              						<tr class="main">
	               							<td nowrap valign="top">
	               							<%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
		         <%}%>
	               							<td>&nbsp;</td>
	               						</tr>
	               						<tr>
	               							<td valign="top" nowrap>
												<table width=400>
													<tr class="main">
	  													<td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:
	  													</td>
	  													<td>
	  														<input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="../calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
														</td>
													</tr>
													<tr class="main">
													    <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: 
													    </td>
													    <td> 
													    	<input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="../calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
													    </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
										    <td class=main>
										      	<input type="hidden" name="search" value="y">
										      	<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
										    </td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
    				<td class="main">
      					<table border="0" width="100%" cellpadding="0" cellspacing="0">
     						<tr>
	        					<td class="formArea2" class="main">
									<table width=400>
									</table>
               					</td>
              				</tr>
            			</table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            			<table width="100%" border="0" cellspacing="0" cellpadding="2">
            				<tr>
            					<td class="main">
          						<%=Languages.getString("jsp.admin.customers.merchants_notes.instructions",SessionData.getLanguage())%>
<%
		                if (!Merchant.getStartDate().equals("") && !Merchant.getEndDate().equals(""))
		                {
		                  	out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(Merchant.getStartDateFormatted()) + 
		                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(Merchant.getEndDateFormatted()));
		                }
%>
            					</td>
            				</tr>
            			</table>
            			<table width="100%" cellspacing="1" cellpadding="2">
            				<tr>
              					<td class=rowhead2><%=Languages.getString("jsp.admin.customers.merchants_notes.date",SessionData.getLanguage()).toUpperCase()%></td>
              					<td class=rowhead2><%=Languages.getString("jsp.admin.customers.merchants_notes.user",SessionData.getLanguage()).toUpperCase()%></td>
              					<td class=rowhead2><%=Languages.getString("jsp.admin.customers.merchants_notes.description",SessionData.getLanguage()).toUpperCase()%></td>
            				</tr>
<%
	Iterator it = vecSearchResults.iterator();
	int intEvenOdd = 1;
	while (it.hasNext())
	{
	  	Vector vecTemp = null;
	  	vecTemp = (Vector) it.next();
	  	out.println("<tr class=rowwrap" + intEvenOdd +">" +
	              "<td nowrap>" + vecTemp.get(0)+ "</td>" +
	              "<td nowrap>" + vecTemp.get(1) + "</td>");
	  	String strDescription = vecTemp.get(2).toString();
	  	if (strDescription.indexOf("aba|") > 0 || strDescription.indexOf("account_no|") > 0)
	  	{
	    	out.print("<td>Bank Account information updated</td>");                      
	  	}
	  	else
	  	{
	  		if(strDescription.startsWith("Merchant type changed from") || strDescription.startsWith("Tipo de comercio modificado de")){
	  			strDescription = strDescription.replaceAll(DebisysConstants.REP_CREDIT_TYPE_PREPAID, Languages.getString("jsp.admin.customers.merchants_info.credit_type1",SessionData.getLanguage()));
	  			strDescription = strDescription.replaceAll(DebisysConstants.REP_CREDIT_TYPE_CREDIT, Languages.getString("jsp.admin.customers.merchants_info.credit_type2",SessionData.getLanguage()));
	  			strDescription = strDescription.replaceAll(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED, Languages.getString("jsp.admin.customers.merchants_info.credit_type3",SessionData.getLanguage())) ;
	  			
	  		}
	  	
	    	out.print("<td>" + strDescription.replaceAll("\r\n", "<BR>") + "</td>");
	  	}
	
	  	out.println("</tr>");
	  	if (intEvenOdd == 1)
	  	{
	    	intEvenOdd = 2;
	  	}
	  	else
	  	{
	    	intEvenOdd = 1;
	  	}
	}
	vecSearchResults.clear();
%>
            			</table>

<%
}
else if(request.getParameter("search") != null)
{
 	out.println("<br><font color=ff0000>"+Languages.getString("jsp.admin.customers.merchants_notes.not_found",SessionData.getLanguage())+"</font>");
}
%>
					</td>
      			</tr>
    		</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="../calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
