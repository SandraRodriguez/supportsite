<%@ page import="java.util.HashMap,
                 java.util.Enumeration,
                 java.util.Vector,
                 java.util.Iterator,
                 java.util.StringTokenizer"%>
<%
int section=2;
int section_page=3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="skuGlue" class="com.debisys.customers.SkuGlue" scope="page"/>
<jsp:useBean id="SKUGlueSearch" class="com.debisys.customers.SkuGlueSearch" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%
String product = request.getParameter("product");
String merchant_id = request.getParameter("merchant_id");
String description ="";
String notes = "";
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
Vector vecSearchResults = new Vector();
SKUGlueSearch.setProductId(product);
vecSearchResults = SKUGlueSearch.searchSkuNoWLProductsDesc(intPage, intPageSize, SessionData, application);
Iterator it = vecSearchResults.iterator();
while (it.hasNext()){
	Vector vecTemp = null;
	vecTemp = (Vector) it.next();
	description = vecTemp.get(1).toString();
	notes = "Customer Note: Merchant Updated: Product: " + product + " " + description +  "  pending approval.";	
}	

String strRedirectUrl = request.getParameter("redirect_url");
if(!product.equals("") && !product.equals(null) && !product.equals("0")){
skuGlue.insertProduct(SessionData, merchant_id,product,notes);
}


if (strRedirectUrl == null || strRedirectUrl.equals(""))
{
  if(!product.equals("") && !product.equals(null) && !product.equals("0")){
  response.sendRedirect("approvedRestrictedProducts.jsp?search=y&merchant_Id="+merchant_id+"&strmsj=updated");
  }
  else{
   response.sendRedirect("approvedRestrictedProducts.jsp?search=y&merchant_Id="+merchant_id);
  }
}
else
{
 response.sendRedirect(strRedirectUrl);
}
%>