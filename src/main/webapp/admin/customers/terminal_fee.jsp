<%@ page import="com.debisys.customers.Merchant,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 java.util.*" %>
<%
    int section=2;
    int section_page=7;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Fee" class="com.debisys.terminals.TerminalFee" scope="request"/>
<jsp:setProperty name="Fee" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
   
    String strMessage = request.getParameter("message");
    Vector vecFees = new Vector();
    Hashtable FeeErrors = null;
    if (strMessage == null)
    {
        strMessage="";
    }
    if (request.getParameter("submitted") != null)
    {
        try
        {
            if (request.getParameter("submitted").equals("y"))
            {
                String deleteRange[]=request.getParameterValues("deleteRange");
                String newClerkCode = request.getParameter("newClerkCode");
                String newClerkName = request.getParameter("newClerkName");
                
      
                String newClerkLastName = "";
                if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                {
                    newClerkLastName = request.getParameter("newClerkLastName");
                }
                if (deleteRange != null && deleteRange.length > 0)
                {
                    for(int i=0; i<deleteRange.length;i++) {
                             Fee.deleteRange(SessionData, deleteRange[i]);
                    }
                }
        
                else if ( request.getParameter("mxFeeTable") == null )
                {

                    if (Fee.Validate(SessionData)){

                            Fee.addRange(SessionData);

                    }
                    else{
                     FeeErrors = Fee.getErrors();   
                    }
                }

                
        //        response.sendRedirect("terminal_fee.jsp?merchantId=" + Fee.getMerchantId() + "&siteId=" + Fee.getSiteId());
               // return;
            }
        }

         catch (Exception e)
         {
        }

    }
 
      vecFees = Fee.getRanges(SessionData);
    
    


%>
<script language="JavaScript">
<!--
var form = "";
var error = false;
var error_message = "";

function check_input(field_name, field_size, message) {
  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var field_value = form.elements[field_name].value;

    if (field_value == '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}//End of function check_input



function check_form(form_name) {
  error = false;
  form = form_name;
  error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";

<%
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
  check_input("from", 1, "<%=Languages.getString("jsp.admin.customers.terminal_fee.no_from",SessionData.getLanguage())%>");
  check_input("to", 1, "<%=Languages.getString("jsp.admin.customers.terminal_fee.no_to",SessionData.getLanguage())%>");
  check_input("ammount", 1, "<%=Languages.getString("jsp.admin.customers.terminal_fee.no_fee",SessionData.getLanguage())%>");
<%
   } //End of if when deploying in Mexico
%>
  
  if (error == true)
  {
    alert(error_message);
    return false;
  }
  else
  {
    return true;
  }
}//End of function check_form

  function validateInteger(c) {
    if ( c.value.length > 0 ) {
    	if (isNaN(c.value)) {
    		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
    		c.focus();
                c.select();
    		return (false);
        } else if (c.value < 0) {
	    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
	    	c.focus();
                c.select();
	    	return (false);
	}
    }
  }//End of function validateInteger

//-->
</script>
    <table border="0" cellpadding="0" cellspacing="0" width="750">
        <tr>
            <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
            <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.terminal_fee.title",SessionData.getLanguage()).toUpperCase()%></td>
            <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#ffffff">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
                    <tr>
                        <td class="formArea2">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                                           <%

                                                if (FeeErrors != null) {
        out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
        Enumeration enum1=FeeErrors.keys();
        while(enum1.hasMoreElements()) {
            String strKey = enum1.nextElement().toString();
            String strError = (String) FeeErrors.get(strKey);
            if (strError != null && !strError.equals("")) {
                out.println("<li>" + strError);
            }
        }

        out.println("</font></td></tr>");
                                                }
                                                                           %>
                                <tr>
                                    <td class=main align=center>
                                        <br><br>
<%
 if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
 {
%>
                                        <form name="terminalFeeTable" method=post action="admin/customers/terminal_fee.jsp">
                                          <input type=hidden name=submitted value="y">
                                          <input type=hidden name=mxFeeTable value="y">
                                          <input type=hidden name=merchantId value="<%=Fee.getMerchantId()%>">
                                          <input type=hidden name=siteId value="<%=Fee.getSiteId()%>">
<%
 }
 else
 {
%>
                                        <form name="terminalFee" method=post action="admin/customers/terminal_fee.jsp">
<%
 }
%>
                                        <table border=0 width=<%=((customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))?"80%":"200")%>>
                                            
                                            <tr>
                                               <td class=rowhead2><%=Languages.getString("jsp.admin.customers.terminal_fee.type",SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.customers.terminal_fee.from",SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.customers.terminal_fee.to",SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.customers.terminal_fee.ammount",SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.customers.terminal_fee.delete",SessionData.getLanguage()).toUpperCase()%></td>
                                            </tr>
                                            <%
                                              if ( (vecFees.size() == 0) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
                                              {
                                                out.print("<TR><TD COLSPAN=5 ALIGN=center CLASS=main>" + Languages.getString("jsp.admin.customers.terminal_fee.nofees",SessionData.getLanguage()) + "</TD></TR>");
                                              }
                                              else
                                              {
                                                Iterator it = vecFees.iterator();
                                                while (it.hasNext()) {
                                                    Vector vecTemp = null;
                                                    vecTemp = (Vector) it.next();
                                                     if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                                                     {
                                                      out.println("<tr class=row2>" +
                                                            "<td class=main nowrap>" + vecTemp.get(2).toString() + "</td>" +
                                                            "<td class=main nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(3).toString()) + "</td>" +
                                                            "<td class=main nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(4).toString()) + "</td>" +
                                                            "<td class=main nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
                                                            "<td class=main nowrap align=center><input type=checkbox name=deleteRange value=\"" + vecTemp.get(0) + "\"></td>" +
                                                            "</tr>");
                                                     }
                                                     else
                                                     {
                                                    out.println("<tr class=row2>" +
                                                            "<td class=main nowrap>" + vecTemp.get(2).toString() + "</td>" +
                                                            "<td class=main nowrap>" + vecTemp.get(3) + "</td>" +
                                                            "<td class=main nowrap>" + vecTemp.get(4) + "</td>" +
                                                            "<td class=main nowrap>" + vecTemp.get(5) + "</td>" +
                                                            "<td class=main nowrap><input type=checkbox name=deleteRange value=\"" + vecTemp.get(0) + "\"></td>" +
                                                            "</tr>");
                                                }
                                                }
                                              }
                                            %>
<%
 if ( (vecFees.size() > 0) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
 {
%>
                                            <tr><td colspan="5" align="center"><input type=submit name=submit value="<%=Languages.getString("jsp.admin.customers.terminal_fee.deletefees",SessionData.getLanguage())%>"></td></tr>
<%
 }
                                            %>
                                        </table>
<%
 if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
 {
%>
                                        </form>
<%
        out.print(Languages.getString("jsp.admin.customers.terminal_fee.instructions2",SessionData.getLanguage()));
 }
 else
 {
        out.print(Languages.getString("jsp.admin.customers.terminal_fee.instructions",SessionData.getLanguage()));
 }
%>
<%
 if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
 {
%>
                                        <form name="terminalFee" method=post action="admin/customers/terminal_fee.jsp" onSubmit="return check_form(terminalFee);">
<%
 }
%>
                                        <table border=0 cellpadding=3 cellspacing=0 class=main>
                                            <tr>
                                                <td colspan=2 align=center><b><%=Languages.getString("jsp.admin.customers.terminal_fee.enter",SessionData.getLanguage())%></b></td>
                                            </tr>
                                                            <tr>
                                                            <td class=main><%=Languages.getString("jsp.admin.customers.terminal_fee.from",SessionData.getLanguage())%><%if (FeeErrors != null && FeeErrors.containsKey("from")) out.print("<font color=ff0000>*</font>");%></td>
                                                            <td><input type=text name=from value="" size=20 maxlength=24 <%=((customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))?"onblur='validateInteger(this);'":"")%>></td>
                                                            </tr>
                                                            <tr>
                                                            <td class=main><%=Languages.getString("jsp.admin.customers.terminal_fee.to",SessionData.getLanguage()) %><%if (FeeErrors != null && FeeErrors.containsKey("to")) out.print("<font color=ff0000>*</font>");%></td>
                                                            <td><input type=text name=to value="" size=20 maxlength=10 <%=((customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))?"onblur='validateInteger(this);'":"")%>></td>
                                                            </tr>
                                                            <tr>
                                                            <td class=main><%=Languages.getString("jsp.admin.customers.terminal_fee.ammount",SessionData.getLanguage()) %><%if (FeeErrors != null && FeeErrors.containsKey("ammount")) out.print("<font color=ff0000>*</font>");%></td>
                                                            <td class=main><input type=text name=ammount value="" size=20 maxlength=10 <%=((customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))?"onblur='validateInteger(this);'":"")%>></td>
                                                            </tr>
                                             
                                            <tr>
                                            <td>
                                            <div id="errorVal" style="display:none;">
                                            </div>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td colspan=2 align=center>
                                                    <input type=hidden name=merchantId value="<%=Fee.getMerchantId()%>">
                                                    <input type=hidden name=siteId value="<%=Fee.getSiteId()%>">
                                                    <input type=hidden name=submitted value="y">
                                                    <input type=submit name=submit value="<%=Languages.getString("jsp.admin.customers.terminal_fee.submit",SessionData.getLanguage())%>">
                                                    <%
                                                      
                                                        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                                                        {
                                                            out.println("</form><form action='admin/customers/merchants_info.jsp'><input type=submit value=\"" + Languages.getString("jsp.admin.customers.terminal_fee.back",SessionData.getLanguage()) + "\">");
                                                            out.println("<input type=hidden name=merchantId value='" + Fee.getMerchantId() + "'>");
                                                            out.println("</form>");
                                                        }
                                                        
                                                    %>
                                                </td>
                                            </tr>
                                        </table>
<%
  if (!customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  {
%>
                                        </form>
<%
  }
%>
                                        <br><br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp" %>