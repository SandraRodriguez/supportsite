<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@ page import="com.debisys.users.User,
                 com.debisys.languages.Languages,
                 com.debisys.utils.*,
                 com.debisys.customers.Rep"%> 

<%
int section=2;
int section_page=21;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request"/>
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>

<script language="JavaScript">
  
  function DisableButtons(){
		var ctl  = document.getElementById("btnApplyPayment");
		if ( ctl != null ){
			ctl.disabled = true;
		}
	}
        
 function checkPayment(){
		var formApplyPmt = document.mainform.amount.value;
		if (isNaN(formApplyPmt)){
			alert("<%=Languages.getString("jsp.admin.customers.reps_info.jsmsg1",SessionData.getLanguage())%>");
			document.mainform.amount.focus();
			return false;
		}
                else if (formApplyPmt==0.00 ){
			alert("<%=Languages.getString("jsp.admin.customers.merchants_info.error5",SessionData.getLanguage())%>");
			document.mainform.amount.focus();
			return false;
		}
                else if(formApplyPmt<= 0.00)
                {
                         alert("<%=Languages.getString("jsp.admin.customers.merchants_info.error5",SessionData.getLanguage())%>");
			document.mainform.amount.focus();
                        document.getElementById('amount').focus();
			return false;
                }
	}
        
        
         function checkRep(){
          	if (document.getElementById('reps').options[document.getElementById('reps').selectedIndex].value=='-1'){
			alert("<%=Languages.getString("jsp.admin.customers.reps_transfer.error",SessionData.getLanguage())%>");
			document.mainform.mainform.focus();
			return false;
		}
	}
        
  function submitForm()
  {
      checkPayment();
      if (document.getElementById('reps').options[document.getElementById('reps').selectedIndex].value=='-1'){
			alert("<%=Languages.getString("jsp.admin.customers.reps_transfer.error",SessionData.getLanguage())%>");
			document.mainform.mainform.focus();
			return false;
    }
      document.mainform.selectedRepId.value=document.getElementById('reps').options[document.getElementById('reps').selectedIndex].value;
      document.mainform.paymentAmount.value=document.getElementById('amount').value;
      document.mainform.paymentDescription.value=document.getElementById('description').value;
      document.mainform.repId.value="<%=Rep.getRepId()%>";
      document.getElementById('mainform').submit();
  }
  
        </script>
<%
String strMessage = request.getParameter("message");
if (strMessage == null){
	strMessage = "";
}
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
String keyLanguage = "jsp.admin.customers.reps_transfer.title";
String strUrlParams = "?repId=" + Rep.getRepId();

long isoId=0;
if (request.getParameter("repId") != null)
{
	  Rep.getRep(SessionData, application);
          isoId=Long.valueOf(new User().getISOId(DebisysConstants.REP, Rep.getRepId()));
          //Rep.getIsoIDForRep(Rep.getRepId());
}
  else
  {
   searchErrors = Rep.getErrors();
  }

%>
<html>
  <head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString(keyLanguage, SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="700" background="../../images/top_blue.gif">
	<tr>
    <td width="18" height="20" align="left"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" align="left" width="3000"><%=Languages.getString("jsp.admin.customers.reps_transfer.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20" align="right"><img src="../../images/top_right_blue.gif"></td>
  </tr>
  	<tr>
		<td colspan="3" bgcolor="#ffffff">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
				<tr bgcolor="ffffff">
                                    <!--<td align="center" class="main"><img src="images/information.png" border="0" valign="middle">-->
					<font color=000000>
<%
if ( strMessage.equals("13") ){
	out.println("<br><font color=ff0000>" + request.getParameter("dbMessage")+"</font>");
}else if ( strMessage.equals("14") ){
	out.println("<br><font color=ff0000>" +Languages.getString("jsp.admin.customers.merchants_info.paymentsuccessful",SessionData.getLanguage()) +"</font>");
}
%>
					</font>
				</td>
			</tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td class="main">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" class="main">
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
           <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.reps_info.apply_payment",SessionData.getLanguage())%></td></tr>
										<tr>
											<td class="formArea2" valign="top">
												<!--<form name="mainform" id="mainform" method="post" action="reptransfer.jsp<%=strUrlParams%>" onSubmit="scroll();">-->
												<form name="mainform" id="mainform" method="post" action="repTransfer_make_payment.jsp<%=strUrlParams%>" onsubmit="DisableButtons();">
												<input type="hidden" name="repId" value="<%=Rep.getRepId()%>">
												<input type="hidden" name="selectedRepId" value="">
												<input type="hidden" name="paymentAmount" value="0.0">
												<input type="hidden" name="paymentDescription" value="">		
                                                                                                    <table width="100%">
														<tr class="main">
															<td nowrap="nowrap" colspan="2">
																<table cellpadding="0" cellspacing="0">
																	<tr class="main">
																		<td align="center" nowrap="nowrap">
                                                                                                                                                    <td nowrap="nowrap">
														<%=Languages.getString("jsp.admin.customers.reps_transfer.reps",SessionData.getLanguage())%>
													</td>
													<td nowrap="nowrap">
                                                                                                            <select name="reps" id="reps" onchange="checkRep()">
                                                                                                                <option value="-1" selected="selected">Select</option>
<%
	Vector repVec = com.debisys.customers.Rep.getRepsForISO(isoId);
	Iterator itReps = repVec.iterator();
	while (itReps.hasNext()) {
		Vector vecTemp = null;
		vecTemp = (Vector)itReps.next();
		String repsId = vecTemp.get(0).toString();
		String repsName = vecTemp.get(1).toString();
                if(!repsId.equals(Rep.getRepId()))
			out.println("<option value=\"" + repsId + "\">" + repsName + "</option>");
		
	        }

%>

														</select>
													</td>
									
																		<b><%=Languages.getString("jsp.admin.customers.reps_info.gross_payment",SessionData.getLanguage())%></b>

																			<input type="text" name="amount" id="amount" value="0.00" size="7"  onBlur="checkPayment();">

																		</td>
																		<td align="center" nowrap="nowrap">&nbsp;<b>-</b>&nbsp;</td>
																		<td><%=Languages.getString("jsp.admin.customers.reps_info.description",SessionData.getLanguage())%>:</b>
                                                                                                                                                    <input type="text" name="description" id="description" value="" size="20">(<%=Languages.getString("jsp.admin.optional",SessionData.getLanguage())%>)
                                                                                                                                                </td>
                                                                                                                                        </tr>
                                                                                                                                        <tr>
                                                                                                                                                <td colspan="2" nowrap="nowrap">
                                                                                                                                               <!-- <input id="btnApplyPayment" type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.reps_info.update",SessionData.getLanguage())%>">&nbsp;!-->
                                                                                                                                                 <input type="button" id="btnApplyPayment" onClick="submitForm()" value="<%=Languages.getString("jsp.admin.customers.reps_info.update",SessionData.getLanguage())%>">&nbsp;
                                                                                                                                         </tr>
																</table>
															</td>
														</tr>
											
        </tr>
</table>
</form>
                                                                                        </td>
                                                                                </tr>
</table>
</body>
</html>