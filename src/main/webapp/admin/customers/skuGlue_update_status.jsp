<%@ page import="java.util.HashMap,
                 java.util.Enumeration,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil,  
                 java.util.StringTokenizer"%>
                 
<%
int section=2;
int section_page=3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="skuGlue" class="com.debisys.customers.SkuGlue" scope="page"/>
<jsp:useBean id="SKUGlueSearch" class="com.debisys.customers.SkuGlueSearch" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%
int countReg = Integer.parseInt(request.getParameter("countReg"));
int i=0;
String checkValues[];
String newStatus="";
String notes = "";
String merchant_id = request.getParameter("merchant_id");
String strRedirectUrl = request.getParameter("redirect_url");
String productId ="";
String status = "";
String description = "";
String productsforUpdate = "";
String productsforNotUpdate ="";
Vector vecSearchResults = new Vector();
Vector vecCounts = new Vector();
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
int disabled=0;
String strParentRepName = "";
SKUGlueSearch.setMerchantId(merchant_id);
vecSearchResults = SKUGlueSearch.searchSkuGlue(intPage, intPageSize, SessionData, application);
Iterator it = vecSearchResults.iterator();
it.next();
while (it.hasNext()){
	Vector vecTemp = null;
	vecTemp = (Vector) it.next();
	productId = vecTemp.get(0).toString();
	description = vecTemp.get(1).toString();
	status = vecTemp.get(2).toString();
	newStatus = request.getParameter("p_"+productId);
	if(!status.equals(newStatus)){
	  if(Integer.parseInt(newStatus) == 0){
	  	  notes = "Customer Note: Merchant Updated: Product: " + productId + " " + description + "  approval denied.";	  
	  	  disabled = 1;
	  }
	  else if(Integer.parseInt(newStatus) == 1){
	  	  notes = "Customer Note: Merchant Updated: Product: " + productId + " " + description + "  approval to sell." ;	
	  	  disabled = 0;
	  }
	  else if(Integer.parseInt(newStatus) ==2){
	   	  notes = "Customer Note: Merchant Updated: Product: " + productId + " " + description + "  pending approval.";	
	   	  disabled = 1;
	  } 
		skuGlue.updateMerchantProductStatus(SessionData, merchant_id,productId, newStatus, notes);
		
		skuGlue.updateTerminalRatesSkuGlue(SessionData, merchant_id,productId, disabled, notes);
		
	}
}	       
if (strRedirectUrl == null || strRedirectUrl.equals(""))
{
  response.sendRedirect("approvedRestrictedProducts.jsp?search=y&merchant_Id="+merchant_id+"&strmsj=updated");
}
else
{
 response.sendRedirect(strRedirectUrl);
}
%>