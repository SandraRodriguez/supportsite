<%@ page import="java.util.*,
                 com.debisys.languages.*,
                 com.debisys.utils.LogChanges" %>
<%@page import="com.debisys.utils.StringUtil"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
<jsp:setProperty name="Terminal" property="*"/>
<%
int section = 2;
int section_page = 11;
LogChanges.setSession(SessionData);
LogChanges.setContext(application);
String terminalType = request.getParameter("terminalType");
%>
<%@ include file="/includes/security.jsp" %>
<html>
<head>
  <link href="../../default.css" type="text/css" rel="stylesheet">
   <script>
   function SetFrameSize()
   {
     self.parent.SetFrameSize(document.getElementById("oBody").scrollWidth, document.getElementById("oBody").scrollHeight);
   }
   function SetMinSize()
   {
     self.parent.SetFrameSize(1250, 200);
   }
   </script>
</head>
<body id="oBody" bgcolor="#ffffff" onload="window.setTimeout('SetFrameSize()', 3000)" onunload="SetMinSize()">
<%
  Hashtable terminalErrors = new Hashtable();
  if ( request.getParameter("save") != null )
  {
    if ( !Terminal.isRegistrationCodeUnique(StringUtil.toString(request.getParameter("edit"))))
    {
      terminalErrors.put("registrationCode", Languages.getString("jsp.admin.customers.terminal.invalidregistrationcode",SessionData.getLanguage()));
    }
    if (!Terminal.isUserIDUnique(StringUtil.toString(request.getParameter("edit"))))
    {
      terminalErrors.put("userIdUnique", Languages.getString("jsp.admin.customers.terminal.uniqueuserid",SessionData.getLanguage()));
    }
    if ( terminalErrors.size() == 0 )
    {
      if ( request.getParameter("save").length() > 0 )
      {        
        boolean resultUpdateService = Terminal.updateWebserviceRegistration(application, SessionData, request);
        if (terminalType.equalsIgnoreCase(DebisysConstants.WEB_SERVICE_TERMINAL_TYPE) && !resultUpdateService) 
            terminalErrors.put("invalidPass", Languages.getString("jsp.admin.customers.terminal.invalid.password",SessionData.getLanguage()));
        response.sendRedirect("/support/admin/customers/webserviceterm_users.jsp?siteId=" + Terminal.getSiteId()+"&terminalType="+terminalType);
      }
      else
      {
        Terminal.addPCTerminalRegistration(application, SessionData, request);
      }
    }
    else
    {
    out.println("<table><tr class=main><td><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
    Enumeration enum1 = terminalErrors.keys();
    while (enum1.hasMoreElements())
    {
      String strKey = enum1.nextElement().toString();
      String strError = (String)terminalErrors.get(strKey);
      if (strError != null && !strError.equals(""))
      {
        out.println("<li>" + strError + "</li>");
      }
    }
    out.println("</font></td></tr></table>");
  }
  }//End of if ( request.getParameter("save") != null )

  if ( StringUtil.toString(request.getParameter("add")).length() > 0 || StringUtil.toString(request.getParameter("edit")).length() > 0 || terminalErrors.size() > 0)
  {
    Vector vResults = null;
    if ( request.getParameter("edit") != null )
    {
      vResults = (Vector)Terminal.getPCTerminalRegistrations(StringUtil.toString(request.getParameter("edit")), application).get(0);
      if ( request.getParameter("save") == null )
      {
        Terminal.setTxtRegistration(vResults.get(2).toString());
        Terminal.setPcTermUsername(vResults.get(5).toString());
        Terminal.setPcTermPassword(vResults.get(6).toString());
        Terminal.setChkRegCode(vResults.get(9).toString().equals("-1")?true:false);
        Terminal.setPcTermBrand(vResults.get(10).toString());
        Terminal.setChkIPRestriction(vResults.get(11).toString().equals("-1")?true:false);
        Terminal.setTxtIPAddress(StringUtil.toString(vResults.get(12).toString()));
        Terminal.setTxtSubnetMask(StringUtil.toString(vResults.get(25).toString()));
        Terminal.setTxtLoginErrorCount(StringUtil.toString(vResults.get(14).toString()));
        Terminal.setDdlRecordStatus(StringUtil.toString(vResults.get(13).toString()));
        Terminal.setChkCertificate(vResults.get(26).toString().equals("-1")?true:false);
        Terminal.setChkAllowCertificate(vResults.get(30).toString().equals("-1")?true:false);
        Terminal.setTxtAllowCertificate(StringUtil.toString(vResults.get(31).toString()));
      }
      vResults.set(4, (StringUtil.toString(vResults.get(4).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(4).toString()));
      vResults.set(7, (StringUtil.toString(vResults.get(7).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(7).toString()));
      vResults.set(15, (StringUtil.toString(vResults.get(15).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(15).toString()));
      vResults.set(16, (StringUtil.toString(vResults.get(16).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(16).toString()));
      vResults.set(17, (StringUtil.toString(vResults.get(17).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(17).toString()));
      vResults.set(18, (StringUtil.toString(vResults.get(18).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(18).toString()));
      vResults.set(27, (StringUtil.toString(vResults.get(27).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(27).toString()));
      vResults.set(28, (StringUtil.toString(vResults.get(28).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(28).toString()));
      vResults.set(29, (StringUtil.toString(vResults.get(29).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(29).toString()));
      vResults.set(32, (StringUtil.toString(vResults.get(32).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(32).toString()));
    }
    else
    {
      vResults = new Vector();
      for ( int i = 0; i < 33; i++ )
      {
        vResults.add("");
      }
    }
%>
<script>
var error = false;
var error_message = "";

function check_input(field_name, field_size, message)
{
  if (document.getElementById(field_name) && (document.getElementById(field_name).type != "hidden"))
  {
    var field_value = document.getElementById(field_name).value;
    if (field_value == '' || field_value.length < field_size)
    {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}//End of function check_input

function check_form()
{
  error = false;
  error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";

    check_input("pcTermBrand", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_brand",SessionData.getLanguage())%>");
    check_input("pcTermUsername", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_webserviceusername",SessionData.getLanguage())%>");
    check_input("pcTermPassword", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_webservicepassword",SessionData.getLanguage())%>");
<%
  if ( request.getParameter("edit") == null )
  {
%>
    if ( document.getElementById("chkManualRegCode").checked )
    {
      check_input("txtRegistration", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_regcode",SessionData.getLanguage())%>");
    }
<%
  }
%>
    if ( document.getElementById("chkIPRestriction").checked )
    {
      check_input("txtIPAddress", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_ipaddress",SessionData.getLanguage())%>");
      check_input("txtSubnetMask", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_subnetmask",SessionData.getLanguage())%>");
    }
    if ( document.getElementById("chkAllowCertificate").checked )
    {
      check_input("txtAllowCertificate", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_certamount",SessionData.getLanguage())%>");
    }

  if (error == true)
  {
    alert(error_message);
    return false;
  }
  else
  {
    var ctl = document.getElementById("btnSubmit");
    if ( ctl != null )
    {
      ctl.disabled = true;
    }    
    ctl = document.getElementById("btnCancel");
    if ( ctl != null )
    {
      ctl.disabled = true;
    }    
    return true;
  }
}//End of function check_form
</script>



    <form action="/support/admin/customers/webserviceterm_users.jsp?terminalType=<%=terminalType%>" method="post" onsubmit="return check_form(this);">
  <table width="100%">
    <tr><td></td><td>&nbsp;</td><td></td><td>&nbsp;&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>
    
    <tr class=main>
<%
LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_USERNAME), Terminal.getPcTermUsername(), Terminal.getSiteId());
LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_PASSWORD), Terminal.getPcTermPassword(), Terminal.getSiteId());
LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_ENFORCECERT), String.valueOf(Terminal.isChkCertificate()), Terminal.getSiteId());
LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CERTINSTALL), String.valueOf(Terminal.isChkAllowCertificate()), Terminal.getSiteId());
LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CERTREMAINING), Terminal.getTxtAllowCertificate(), Terminal.getSiteId());
LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_ENFORCEREGCODE), String.valueOf(Terminal.isChkRegCode()), Terminal.getSiteId());
LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_REGCODE), Terminal.getTxtRegistration(), Terminal.getSiteId());
%>
      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.webservice_userName",SessionData.getLanguage())%></td>
      <td></td>
      <td nowrap><input type="text" ID="pcTermUsername" name="pcTermUsername" value="<%=Terminal.getPcTermUsername()%>" size="20" maxlength="15"></td>
      <td></td>
      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.webservice_password",SessionData.getLanguage())%></td>
      <td></td>
      <td>
          <input type="password" ID="pcTermPassword" name="pcTermPassword" value="<%=Terminal.getPcTermPasswordDecrypt(application)%>" size="20" maxlength="15" onblur="return ValidatePCTermPassword(this);">
        <%
        
        if (!terminalType.equals(DebisysConstants.VERIFONE_TERMINAL_TYPE)){            
        %>
        <p class="errorLabelPass"><%=Languages.getString("jsp.admin.customers.terminal.invalid.password1",SessionData.getLanguage())%></p>
        <script>
          function ValidatePCTermPassword(c)
          {
            var exp = new RegExp("^([a-zA-Z0-9\$])+$");
            
            return true;
          }
        </script>
        <%}%>
      </td>
    </tr>

    
<%
  if ( request.getParameter("edit") != null )
  {
%>
   
    
<%
  }
%>
   
    <tr class=main><td colspan=3><label><%=Languages.getString("jsp.admin.customers.terminal.enforce_iprestriction",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkIPRestriction" name="chkIPRestriction" onclick="document.getElementById('txtIPAddress').disabled = document.getElementById('txtSubnetMask').disabled = !this.checked;"></label></td></tr>
    <tr class=main>
      <td nowrap><%=Languages.getString("jsp.admin.customers.terminal.ipaddress",SessionData.getLanguage())%></td>
      <td></td>
      <td>
        <input type="text" ID="txtIPAddress" name="txtIPAddress" value="<%=Terminal.getTxtIPAddress()%>" size="20" maxlength="15" disabled onblur="return ValidateIPAddress(this);">
        <script>
          function ValidateIPAddress(c)
          {
            var exp = new RegExp("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
            if ( !exp.test(c.value) && (c.value.length > 0) )
            {
              alert('<%=Languages.getString("jsp.admin.invalidipaddress",SessionData.getLanguage())%>');
              window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
              return false;
            }
            if ( c.value.length > 0 )
            {
              var v = c.value.split(".");
              for (var s in v)
              {
                if ( parseInt(v[s]) > 255 )
                {
                  alert('<%=Languages.getString("jsp.admin.invalidipaddress",SessionData.getLanguage())%>');
                  window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
                  return false;
                }
              }
            }
            return true;
          }//End of function ValidateIPAddress
        </script>
      </td>
      <td></td>
      <td nowrap><%=Languages.getString("jsp.admin.customers.terminal.subnetmask",SessionData.getLanguage())%></td>
      <td></td>
      <td>
        <input type="text" ID="txtSubnetMask" name="txtSubnetMask" value="<%=Terminal.getTxtSubnetMask()%>" size="20" maxlength="19" disabled onblur="return ValidateSubnetMask(this);">
        <script>
          function ValidateSubnetMask(c)
          {
            var exp = new RegExp("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})(\\/(\\d{1,3}))*$");
            if ( !exp.test(c.value) && (c.value.length > 0) )
            {
              alert('<%=Languages.getString("jsp.admin.invalidsubnetmask",SessionData.getLanguage())%>');
              window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
              return false;
            }
            if ( c.value.length > 0 )
            {
              var v = c.value.split("/");
              v = v[0].split(".");
              for (var s in v)
              {
                if ( parseInt(v[s]) > 255 )
                {
                  alert('<%=Languages.getString("jsp.admin.invalidsubnetmask",SessionData.getLanguage())%>');
                  window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
                  return false;
                }
              }
            }
            return true;
          }//End of function ValidateSubnetMask
        </script>
      </td>
    </tr>
    
    

  </table>
<%
    if ( Terminal.isChkRegCode() )
    {
      out.print("<script>document.getElementById('chkRegCode').click();</script>");
    }
    if ( Terminal.isChkManualRegCode() )
    {
      out.print("<script>document.getElementById('chkManualRegCode').click();</script>");
    }
    if ( Terminal.isChkIPRestriction() )
    {
      out.print("<script>document.getElementById('chkIPRestriction').click();</script>");
    }
    if ( Terminal.isChkCertificate() )
    {
      out.print("<script>document.getElementById('chkCertificate').click();</script>");
    }
    if ( Terminal.isChkAllowCertificate() )
    {
      out.print("<script>document.getElementById('chkAllowCertificate').click();</script>");
    }
%>
    <br><br>
    <input type=hidden name="save" value="<%=StringUtil.toString(request.getParameter("edit"))%>">
    <input type=hidden name="siteId" value="<%=Terminal.getSiteId()%>">
<%
  if ( StringUtil.toString(request.getParameter("edit")).length() == 0 )
  {
%>
    <input id="btnSubmit" type=submit value="<%=Languages.getString("jsp.admin.customers.terminal.add_pc_term",SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
<%
  }
  else
  {
%>
    <input id="btnSubmit" type=submit value="<%=Languages.getString("jsp.admin.customers.terminal.save_pc_term",SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
    <input type=hidden name="edit" value="<%=StringUtil.toString(request.getParameter("edit"))%>">
<%
  }
%>
  </form>
  <form action="/support/admin/customers/webserviceterm_users.jsp?terminalType=<%=terminalType%>" method="post">
    <input type=hidden name="siteId" value="<%=Terminal.getSiteId()%>">
    <input id="btnCancel" type=submit value="<%=Languages.getString("jsp.admin.cancel",SessionData.getLanguage())%>">
  </form>
<%
  }
  else
  {//Else show results
    Vector vResults = Terminal.getPCTerminalRegistrations("", application);
    if ( vResults.size() > 0 )
    {
%>
  <table>
    <tr class=rowhead>
      <td nowrap>ID</td>
      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.webservice_userName",SessionData.getLanguage())%></td>
      <td nowrap><%=Languages.getString("jsp.admin.customers.terminal.iprestricted",SessionData.getLanguage())%></td>
    </tr>
      
<%
      Iterator it = vResults.iterator();
      int nEvenOdd = 1;
      while ( it.hasNext() )
      {
        Vector vTmp = (Vector)it.next();
%>
    <tr class=row<%=nEvenOdd%>>
      <td nowrap><a href="/support/admin/customers/webserviceterm_users.jsp?edit=<%=vTmp.get(0)%>&siteId=<%=Terminal.getSiteId()%>&terminalType=<%=terminalType%>"><%=vTmp.get(0)%></a></td>
      <td nowrap><%=vTmp.get(5)%></td>
      <td align=center nowrap><%=vTmp.get(11).equals("-1")?Languages.getString("jsp.admin.customers.terminal.pc_term_yes",SessionData.getLanguage()):Languages.getString("jsp.admin.customers.terminal.pc_term_no",SessionData.getLanguage())%></td>
    </tr>
<%
        vTmp = null;
        nEvenOdd = (nEvenOdd == 1)?2:1;
      }
      it = null;
%>
  </table>
<%
    }
    else
    {
%>
  <table><tr><td class=main><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></td></tr></table>
<%
    }
    vResults = null;
%>
  <br><br>
  <%=Languages.getString("jsp.admin.customers.terminal.sms.howtoeditterminal",SessionData.getLanguage())%>
  
<%
  }//End of else show results
%>
</body>
</html>
