<%@ page import="com.debisys.languages.Languages,
				 com.debisys.customers.Merchant,
				 com.debisys.reports.TransactionReport,
				 com.debisys.utils.DateUtil,
				 com.debisys.utils.StringUtil,
				 com.debisys.utils.HTMLEncoder,
				 java.util.Vector,
				 java.util.Iterator,
				 java.util.Enumeration" %>
<%
int section = 2;
int section_page = 4; 
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<html>
<head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString("jsp.admin.customers.add_merchant_notes.title",SessionData.getLanguage()).toUpperCase()%></title>
</head>
<body bgcolor="#ffffff">
<script>

function validateComments(txtNote)
{
  if ( txtNote.value.length > 255 )
  {
    txtNote.value = txtNote.value.substring(0, 254);
  }
  document.getElementById('btnSave').disabled = (txtNote.value.length == 0);
}
</script>
<%
if ( request.getParameter("merchantId") != null )
{//If a case number was specified
	try
	{
		String submitted = request.getParameter("submitted");
		long merchantId = Long.parseLong(request.getParameter("merchantId"));
		String businessName = request.getParameter("businessName");
		String message = "";
		if (request.getParameter("message")!= null)
			message = request.getParameter("message");
		
		
		if (( submitted!= null)&&( submitted.equals("y"))){
			String note = request.getParameter("txtNote");
			if (note!=null && note.trim().length()>0){
				Merchant.addMerchantNote(merchantId, note, SessionData.getUser().getUsername());
				message = Languages.getString("jsp.admin.customers.add_merchant_notes.successMessage",SessionData.getLanguage());
				response.sendRedirect("merchants_notes_add.jsp?merchantId=" + merchantId + "&businessName=" + businessName + "&message=" + message);
			}else{
				message = Languages.getString("jsp.admin.customers.add_merchant_notes.errorMessage",SessionData.getLanguage());
				response.sendRedirect("merchants_notes_add.jsp?merchantId=" + merchantId + "&businessName=" + businessName + "&message=" + message);
			}
		}else{
%>
<table width="100%" height="100%" style="height:100%;">
	<tr>
		<td align="left" valign="top">
			<table cellspacing="1" cellpadding="1" border="0">
				<tr>
					<td class="rowhead2" colspan=2 align=center style="text-align:center;"><b><%=Languages.getString("jsp.admin.customers.add_merchant_notes.title",SessionData.getLanguage()).toUpperCase()%></b></td>
				</tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.customers.add_merchant_notes.merchantId",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row1"><%=merchantId%></td>
                </tr>
                <tr>
                    <td class="rowhead2"><b><%=Languages.getString("jsp.admin.customers.add_merchant_notes.businessName",SessionData.getLanguage()).toUpperCase()%></b></td>
                    <td class="row2"><%=businessName%></td>
                </tr>
		</table>
			<br>
			<font class="main" color=ff0000><%=message %></font>
			<br>
			<form method=post action="merchants_notes_add.jsp">
				<input type=hidden name=merchantId value="<%=request.getParameter("merchantId")%>">
				<input type=hidden name=businessName value="<%=request.getParameter("businessName")%>">
				<input type="hidden" name="submitted" value="y">
				<table>
					<tr><td class="main"><%=Languages.getString("jsp.admin.customers.add_merchant_notes.newMerchantNote",SessionData.getLanguage())%></td></tr>
					<tr><td><textarea name=txtNote cols=40 rows=5 onpropertychange="validateComments(this);"></textarea></td></tr>

					<tr><td align=center><input type=submit id=btnSave value="<%=Languages.getString("jsp.admin.customers.add_merchant_notes.saveMerchantNote",SessionData.getLanguage())%>"></td></tr>
				</table>
			</form>

		</td>
	</tr>
</table>
<%
	}//End else submitted different than "y"
	}
	catch (Exception e)
	{
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" nowrap><b>Error in page</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class=main><%=Languages.getString("jsp.admin.customers.add_merchant_notes.checklog",SessionData.getLanguage())%><br><%=e.getLocalizedMessage()%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}//End of catch
}//End of if a merchant id was specified
else
{//Else there is no merchant id to retrieve
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" nowrap><b>Error</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class=main><%=Languages.getString("jsp.admin.customers.add_merchant_notes.noMerchantId",SessionData.getLanguage())%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
}//End of else there is no case number to retrieve
%>
</body>
</html>
