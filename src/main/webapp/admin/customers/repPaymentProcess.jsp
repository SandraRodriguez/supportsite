<%-- 
    Document   : repPaymentProcess
    Created on : Mar 1, 2018, 1:32:55 PM
    Author     : fernandob
--%>

<%@page import="com.debisys.customers.Rep"%>
<%@page import="com.debisys.utils.OperationResult"%>
<%@page import="com.debisys.utils.EntityAccountTypes"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.lang.reflect.Type"%>
<%@page import="com.debisys.utils.DateUtil"%>
<%@page import="net.emida.supportsite.dto.Bank"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="com.debisys.utils.DebisysConstants"%>
<%@page import="com.emida.utils.dateUtils.DateUtils"%>
<%@page import="net.emida.supportsite.dto.RepCredit"%>
<%@page import="net.emida.supportsite.dto.DepositType"%>
<%@page import="net.emida.supportsite.dao.RepCreditDao"%>
<%@page import="net.emida.supportsite.dao.DepositTypeDao"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="net.emida.supportsite.dto.PaymentNotification"%>
<%@page import="net.emida.supportsite.dto.RepCreditPaymentData"%>
<%@page import="com.debisys.customers.payments.RepPaymentMxAgent"%>
<%@page import="com.debisys.customers.payments.RepLiabilityData"%>


<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>


<%!
    private String getDepositTypesByBankId(long bankId) {
        String retValue = "";

        List<DepositType> depositTypesList = DepositTypeDao.getDepositTypesByBankId(bankId);
        if (!depositTypesList.isEmpty()) {
            try {
                Gson gson = new Gson();
                Type type = new TypeToken<List<DepositType>>() {
                }.getType();
                retValue = gson.toJson(depositTypesList, type);
            } catch (Exception localException) {
                System.out.println(String.format("Error while getting deposit types by bank : %s", localException.toString()));
            }
        }
        return retValue;
    }
%>


<%
    String requestAction = request.getParameter("action");
    if (requestAction.equals("getDepositTypesByBankId")) {
        String bankIds = request.getParameter("bankIds"); // Bank id and bank id new comes separated by pipe
        String[] bankIdList = bankIds.split("\\|");
        long bankId = (bankIdList.length > 0)?Long.valueOf(bankIdList[0]):0L;
        String jsonResponse = getDepositTypesByBankId(bankId);
        response.setContentType("application/json");
        out.println(jsonResponse);
        out.flush();
    } else { // Apply payment
        OperationResult opResult = null;
        try {
            Rep paymentRep = new Rep();
            paymentRep.setRepId(request.getParameter("repPaymentRepId"));
            paymentRep.getRep(SessionData, application);
            // Get do payment parameters
            int deploymentType = Integer.valueOf(request.getParameter("repPaymentDeploymentType"));
            int customConfigType = Integer.valueOf(request.getParameter("repPaymentCustomConfigType"));
            Calendar depositDate = DateUtil.fromStringToCalendar(request.getParameter("depositDate"), "dd/MM/yyyy");
            BigDecimal paymentAmount = new BigDecimal(request.getParameter("finalAmount"));
            RepCredit repCredit = new RepCredit();
            repCredit.setRepId(Long.valueOf(paymentRep.getRepId()));
            repCredit.setLogonId(SessionData.getUser().getUsername());
            repCredit.setDescription(request.getParameter("paymentComments"));
            repCredit.setAmount(paymentAmount);
            repCredit.setCommission(new BigDecimal(request.getParameter("finalCommission")));
            repCredit.setDepositDate(depositDate);

            RepCreditPaymentData paymentData = new RepCreditPaymentData();
            String banksValue = request.getParameter("banks");
            String[] bankIdList = banksValue.split("\\|");
            paymentData.setBankId(bankIdList[1]);
            paymentData.setDepositTypeId(request.getParameter("depositTypes"));
            paymentData.setInvoiceTypeId(request.getParameter("receiptType"));
            paymentData.setAccountNumber(request.getParameter("accountNumber"));
            paymentData.setDocumentNumber(request.getParameter("documentNumber"));

            opResult = RepPaymentMxAgent.doPayment(paymentRep, repCredit, paymentData,
                    deploymentType, customConfigType, SessionData.getLanguage());
            if (opResult != null) {

            } else {
                opResult = new OperationResult();
                opResult.setError(true);
                opResult.setResultCode("004");
                opResult.setResultMessage(Languages.getString("jsp.admin.customers.reps.payment.msgs.error.generalError", SessionData.getLanguage()));
            }
        } catch (Exception localException) {
            System.out.println(String.format("repPaymentProcess.Error while generating rep payment response : %s", localException.toString()));
            opResult = new OperationResult();
            opResult.setError(true);
            opResult.setResultCode("004");
            opResult.setResultMessage(Languages.getString("jsp.admin.customers.reps.payment.msgs.error.generalError", SessionData.getLanguage()));
        }
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(opResult.toJsonString());
            response.flushBuffer();

        } catch (Exception responseException) {
            System.out.println(String.format("repPaymentProcess.Error while exporting rep payment response : %s", responseException.toString()));
        }
    }


%>
