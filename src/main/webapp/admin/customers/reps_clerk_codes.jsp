<%@ page
	import="com.debisys.customers.Rep,java.net.URLEncoder,com.debisys.utils.HTMLEncoder,java.util.*"%>
<%
	int section = 2;
	int section_page = 15;
	String strCustomConfigType = DebisysConfigListener
			.getCustomConfigType(application);
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp"%>

<%
	String strMessage = request.getParameter("message");
	Vector vecClerkCodes = new Vector();
	Hashtable repErrors = null;

	if (strMessage == null) {
		strMessage = "";
	}
	if (request.getParameter("submitted") != null) {
		try {
			if (request.getParameter("submitted").equals("y")) {
				String deleteClerkCode = request
						.getParameter("deleteClerkCode");
				String newClerkCode = request
						.getParameter("newClerkCode");
				String newClerkName = request
						.getParameter("newClerkName");
				String newClerkLastName = "";
				if (strCustomConfigType
							.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
						newClerkLastName = request
								.getParameter("newClerkLastName");
					}
				if (deleteClerkCode != null
						&& !deleteClerkCode.equals("")) {
					if (!newClerkCode.equals("1111")){
						int result = Rep.delGlobalClerkCode(SessionData, this.getServletContext(), newClerkCode);
						out
											.println("<script>alert(\""
													+ Languages
															.getString("jsp.admin.customers.reps_clerk_codes.UpdatedTerminals",SessionData.getLanguage() ) + result
													+  "\")</script>");
					}else{
						out
											.println("<script>alert(\""
													+ Languages
															.getString("com.debysis.customers.reps_clerk_codes.QComError",SessionData.getLanguage()) + ""
													+  "\")</script>");
					}
				} else if (strCustomConfigType
							.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
						if (newClerkCode != null
								&& !newClerkCode.equals("")) {
							Vector results = Rep.addGlobalClerkCode(SessionData, this.getServletContext(), newClerkCode, newClerkName, newClerkLastName);
							repErrors = Rep.getErrors();
							if (repErrors != null && repErrors.size()>0 ) {
								String strError = (String) repErrors
										.get("ErrorValidate");
								if (strError.equals("Error")) {
									out
											.println("<script>alert(\""
													+ Languages
															.getString("jsp.admin.customers.reps_clerk_codes.validateInfo",SessionData.getLanguage())
													+ "\")</script>");
								}
							}else{
								out
										.println("<script>alert(\""
												+ Languages
														.getString("jsp.admin.customers.reps_clerk_codes.UpdatedTerminals",SessionData.getLanguage() ) + results.get(0)
												+ " " + Languages
														.getString("jsp.admin.customers.reps_clerk_codes.IgnoredTerminals",SessionData.getLanguage()) + results.get(1)
												+ "\")</script>");
							}
						}
					} else {
						if (newClerkCode != null
								&& !newClerkCode.equals("")) {
							Vector results = Rep.addGlobalClerkCode(SessionData, this.getServletContext(), newClerkCode, newClerkName, newClerkLastName);
							out
											.println("<script>alert(\""
													+ Languages
															.getString("jsp.admin.customers.reps_clerk_codes.UpdatedTerminals",SessionData.getLanguage()) + results.get(0)
													+ " " + Languages
															.getString("jsp.admin.customers.reps_clerk_codes.IgnoredTerminals",SessionData.getLanguage())+ results.get(1)
													+ "\")</script>");
						}
					}
				}

		} catch (Exception e) {
		}

	}
	/*  if (Merchant.isError())
	 {
	 response.sendRedirect("/support/message.jsp?message=4");
	 return;
	 }*/
%>
<%@ include file="/includes/header.jsp"%>
<script language="JavaScript">
<!--
var form = "";
var error = false;
var error_message = "";

function check_input(field_name, field_size, message) {
  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var field_value = form.elements[field_name].value;
	alert(field_name + field_value.length);
    if (field_value == '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}//End of function check_input

function check_form(form_name) {
  alert(error_message);
  error = false;
  form = form_name;
  error_message = "<%=Languages.getString("jsp.admin.customers.reps_clerk_codes.jsmsg2",SessionData.getLanguage())%>";

<%
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
  check_input("newClerkName", 1, "<%=Languages
										.getString("jsp.admin.customers.reps_clerk_codes.no_clerkname",SessionData.getLanguage())%>");
  check_input("newClerkLastName", 1, "<%=Languages
										.getString("jsp.admin.customers.reps_clerk_codes.no_clerklastname",SessionData.getLanguage())%>");
  check_input("newClerkCode", 1, "<%=Languages
										.getString("jsp.admin.customers.reps_clerk_codes.no_clerkcode",SessionData.getLanguage())%>");
<%
   } //End of if when deploying in Mexico
%>
  
  if (error == true)
  {
    alert(error_message);
    return false;
  }
  else
  {
    return true;
  }
}//End of function check_form
//-->
</script>

<!--  Tabla Eliminada -->
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.reps_clerk_codes.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
  <tr>
    <td class="formArea2">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class=main align=center>
          <br><br>
<%
 if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
 {
%>
          <form method=post action="admin/customers/reps_clerk_codes.jsp">
            <input type=hidden name=submitted value="y">
            <input type=hidden name=mxFeeTable value="y">
            <input type=hidden name=merchantId value="<%=Rep.getRepId()%>">
<%
 }
 else
 {
%>
          <form name="merchantClerkCode" method=post action="admin/customers/reps_clerk_codes.jsp">
<%
 }
%>
<%
		out
				.print(Languages
						.getString("jsp.admin.customers.reps_clerk_codes.instructions",SessionData.getLanguage()));
%>
<%
	if (customConfigType
			.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
%>
<form name="repClerkCode" method=post
	action="admin/customers/reps_clerk_codes.jsp"
	onSubmit="return check_form(repClerkCode);">
	<%
		}
	%>
	<table border=0 cellpadding=3 cellspacing=0 class=main>

		<%
			if (customConfigType
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
				out
						.println("<tr>"
								+ "<td>"
								+ Languages
										.getString("jsp.admin.customers.reps_clerk_codes.clerk_name",SessionData.getLanguage())
								+ "</td>"
								+ "<td><input type=text name=newClerkName value=\"\" size=20 maxlength=16 ></td>"
								+ "</tr>"
								+ "<tr>"
								+ "<td>"
								+ Languages
										.getString("jsp.admin.customers.reps_clerk_codes.clerk_lastName",SessionData.getLanguage())
								+ "</td>"
								+ "<td><input type=text name=newClerkLastName value=\"\" size=20 maxlength=24 ></td>"
								+ "</tr>"
								+ "<tr>"
								+ "<td>"
								+ Languages
										.getString("jsp.admin.customers.reps_clerk_codes.clerk_code",SessionData.getLanguage())
								+ "</td>"
								+ "<td><input type=text name=newClerkCode value=\"\" size=20 maxlength=10></td>"
								+ "</tr>");
			} else {
				out
						.println("<tr>"
								+ "<td>"
								+ Languages
										.getString("jsp.admin.customers.reps_clerk_codes.clerk_code",SessionData.getLanguage())
								+ "</td>"
								+ "<td><input type=text name=newClerkCode value=\"\" size=20 maxlength=10></td>"
								+ "</tr>"
								+ "<tr>"
								+ "<td>"
								+ Languages
										.getString("jsp.admin.customers.reps_clerk_codes.clerk_name",SessionData.getLanguage())
								+ "</td>"
								+ "<td><input type=text name=newClerkName value=\"\" size=20 maxlength=50>"
								+ Languages.getString("jsp.admin.optional",SessionData.getLanguage())
								+ "</td>" + "</tr>");
			}
		%>
		<tr>
			<td>
				<div id="errorVal" style="display: none;">
				</div>
			</td>
		</tr>
		<tr>
			<td colspan=2 align=center>
				<input type=hidden name=repId value="<%=Rep.getRepId()%>">
				<input type=hidden name=submitted value="y">
				<input type=submit name=submit
					value="<%=Languages
									.getString("jsp.admin.customers.reps_clerk_codes.submit",SessionData.getLanguage())%>">
				<input type=submit name=deleteClerkCode
					value="<%=Languages
									.getString("jsp.admin.customers.reps_clerk_codes.delete",SessionData.getLanguage())%>">
				<%
					if (customConfigType
							.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
						out
								.println("</form><form action='admin/customers/reps_info.jsp'><input type=submit value=\""
										+ Languages
												.getString("jsp.admin.customers.reps_clerk_codes.back",SessionData.getLanguage())
										+ "\">");
						out.println("<input type=hidden name=repId value='"
								+ Rep.getRepId() + "'>");
						out.println("</form>");
					}
				%>
			</td>
		</tr>
	</table>
	<br>
	<br>
	<%
		if (!customConfigType
				.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
	%>
	</td>
	</tr>
<%
	}
%>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp"%>