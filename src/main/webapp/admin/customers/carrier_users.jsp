<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%
	int section=14;
	int section_page=1;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
	String strParentRepName = "";
	Vector<Vector<String>> vecResults = new Vector<Vector<String>>();
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;

	if (request.getParameter("search") != null)
	{
		if (request.getParameter("page") != null)
		{
		  	try
		  	{	
		    	intPage=Integer.parseInt(request.getParameter("page"));
		  	}
		  	catch(NumberFormatException ex)
		  	{
		    	intPage = 1;
		  	}
		}
	
		if (intPage < 1)
		{
		  intPage=1;
		}
		
  		if (request.getParameter("repId") != null && !request.getParameter("repId").equals(""))
  		{
    		strParentRepName = CustomerSearch.getParentRepName(request.getParameter("repId"));
  		}
  		else
  		{
    		strParentRepName = SessionData.getProperty("company_name");
  		} 
  		
  		vecResults = CustomerSearch.getCarrierUsers(intPage, intPageSize, SessionData.getProperty("ref_id"));
  		Vector<String> vecRecCount = vecResults.get(0);
  		intRecordCount = Integer.parseInt(vecRecCount.get(0).toString());
  		vecResults.removeElementAt(0);
  	  	
  		if (intRecordCount>0)
		{
			intPageCount = (intRecordCount / intPageSize) + 1;
  		    if ((intPageCount * intPageSize) + 1 >= intRecordCount)
  		    {
  		      	intPageCount++;
  		    }
		}
	}
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users.title",SessionData.getLanguage()).toUpperCase() + " " + strParentRepName.toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
		    			<form name="searchCarrierUsersForm" method="get" action="admin/customers/carrier_users.jsp" >
		          		<table border=0>
	              			<tr class="main">
	               				<td nowrap valign="top"><%=Languages.getString("jsp.admin.narrow_results",SessionData.getLanguage())%>:</td>
	               			</tr>
	               			<tr class=main>
	               				<td valign="top" nowrap colspan=2>
	               					<input name="criteria" type="text" size="40" maxlength="64"><br>
	               					<%=Languages.getString("jsp.admin.customers.carrier_users.search_instructions",SessionData.getLanguage())%>
	               				</td>
	               				<td valign="top" align="left">
				                  	<input type="hidden" name="search" value="y">
				                  	<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
	               				</td>
	              			</tr>
	              		</table>
	              		</form>
						<form name="newCarrierUserForm" method="get" action="admin/customers/carrier_users_info_new.jsp" >
						<table>
							<tr>
								<td align="center">
									<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users.add_carrier_user",SessionData.getLanguage())%>" />
								</td>
							</tr>
						</table>
						</form>
<%
	if (vecResults != null && vecResults.size() > 0)
	{
%>
            			<table width="100%" cellspacing="1" cellpadding="1" border="0">
	            			<tr>
	            				<td class="main" colspan=7><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
            <%
              if (!CustomerSearch.getCriteria().equals("")) {out.println(Languages.getString("jsp.admin.for",SessionData.getLanguage()) + " \"" + HTMLEncoder.encode(CustomerSearch.getCriteria()) + "\"");}
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount-1)},SessionData.getLanguage())%><br><%=Languages.getString("jsp.admin.customers.carrier_users.nav_instructions",SessionData.getLanguage())%>
	            				</td>
							</tr> 
	            			<tr>
	              				<td align=right class="main" nowrap colspan=7>
<%
		if (intPage > 1)
       	{
			out.println("<a href=\"admin/customers/carrier_users.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=1&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" + Languages.getString("jsp.admin.first",SessionData.getLanguage()) + "</a>&nbsp;");
         	out.println("<a href=\"admin/customers/carrier_users.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + (intPage-1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
       	}
       	int intLowerLimit = intPage - 12;
       	int intUpperLimit = intPage + 12;

       	if (intLowerLimit<1)
       	{
         	intLowerLimit=1;
         	intUpperLimit = 25;
       	}	

       	for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
       	{
         	if (i==intPage)
         	{
          		out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
         	}
         	else
         	{
           		out.println("<a href=\"admin/customers/carrier_users.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + i + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" + i + "</a>&nbsp;");
         	}
       	}

       	if (intPage < (intPageCount-1))
       	{
         	out.println("<a href=\"admin/customers/carrier_users.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + (intPage+1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
         	out.println("<a href=\"admin/customers/carrier_users.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + (intPageCount-1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
       	}
%>
              					</td>
            				</tr>           				
				     		<tr>
			            		<td class=rowhead2><%=Languages.getString("jsp.admin.customers.carrier_users.username",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/carrier_users.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=1&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/customers/carrier_users.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=1&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
				            	 <!-- <td class=rowhead2><%=Languages.getString("jsp.admin.customers.carrier_users.password",SessionData.getLanguage()).toUpperCase()%></td> -->
				            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.carrier_users.edit_password",SessionData.getLanguage()).toUpperCase()%></td>
				            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.carrier_users.edit_email",SessionData.getLanguage()).toUpperCase()%></td>
				            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.carrier_users.allowed_products",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2>&nbsp;</td>
			           	 	</tr>
<%
		Iterator<Vector<String>> it = vecResults.iterator();
		int intEvenOdd = 1;
		while (it.hasNext())
		{
	  		Vector<String> vecTemp = null;
	  		vecTemp = (Vector<String>) it.next();
	  		out.println("<tr class=row" + intEvenOdd +">");
	  		out.println("<td>" + vecTemp.get(1) + "</td>");
	  		//out.println("<td>" + vecTemp.get(2) + "</td>");
	 		out.println("<td><a href=\"admin/customers/carrier_users_info_edit.jsp?passwordId=" + vecTemp.get(0) + "\">"+Languages.getString("jsp.admin.customers.carrier_users.editopt",SessionData.getLanguage())+"</a></td>");
	 		out.println("<td><a href=\"admin/customers/carrier_users_info_emailEdit.jsp?passwordId=" + vecTemp.get(0) + "\">"+Languages.getString("jsp.admin.customers.carrier_users.editopt",SessionData.getLanguage())+"</a></td>");
	 		out.println("<td><a href=\"admin/customers/carrier_users_products_summary.jsp?&passwordId=" + vecTemp.get(0) + "\">"+Languages.getString("jsp.admin.customers.carrier_users.productsopt",SessionData.getLanguage())+"</a></td>");
	 		out.println("<td><a href=\"admin/customers/carrier_users_info_delete.jsp?passwordId=" + vecTemp.get(0) + "\">"+Languages.getString("jsp.admin.customers.carrier_users.deleteopt",SessionData.getLanguage())+"</a></td>");
	 		out.println("</tr>");
	
			if (intEvenOdd == 1)
	    		intEvenOdd = 2;
	  		else
	    		intEvenOdd = 1;
		}
		vecResults.clear();
%>		
						</table>
<%	
	}
	else if (vecResults.size()==0 && request.getParameter("search") != null)
	{
%>
						<table width="100%">
				           	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		
	}
%>						
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>