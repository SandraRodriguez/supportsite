<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>

<%
int section=2;
int section_page=2;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="SKUGlueSearch" class="com.debisys.customers.SkuGlueSearch" scope="request"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Vector vecCounts = new Vector();
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String strParentRepName = "";

String merchant_id =request.getParameter("merchant_Id");

String strmsj = request.getParameter("strmsj");

if (request.getParameter("search") != null)
{
  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  SKUGlueSearch.setMerchantId(merchant_id);
  vecSearchResults = SKUGlueSearch.searchSkuGlue(intPage, intPageSize, SessionData, application);
  vecCounts = (Vector) vecSearchResults.get(0);
  intRecordCount = Integer.parseInt(vecCounts.get(0).toString());
  vecSearchResults.removeElementAt(0);
  if (intRecordCount>0)
  {
    intPageCount = (intRecordCount / intPageSize);
    if ((intPageCount * intPageSize) < intRecordCount)
    {
      intPageCount++;
    }
  }

  if (request.getParameter("repId") != null && !request.getParameter("repId").equals(""))
  {
    strParentRepName = Merchant.getMerchantRepName(request.getParameter("repId"));
  }
  else
  {
    strParentRepName = SessionData.getProperty("company_name");
  }
}
%>

<%@ include file="/includes/header.jsp" %>

<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants.title",SessionData.getLanguage()).toUpperCase() + " " + strParentRepName.toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
	          <% if(strmsj != null && strmsj!="") { %>
	          
	          <td nowrap valign="top" align="center" class="main"><font color=ff0000><%=Languages.getString("jsp.admin.customers.skuglue.msgupdate",SessionData.getLanguage())%></font></td>
	          
	          <%} %>
           

              <%if (SessionData.checkPermission(DebisysConstants.PERM_MERCHANTS))
              {%>
              <tr>
              <td colspan=3 align=left>
                 
              </td>
              </tr>
              <%}%>
            </table>


            <table width="100%" cellspacing="1" cellpadding="1">
            <tr><td class="main" colspan=8><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
                          
            </td></tr>
            <%
			if (vecSearchResults != null && vecSearchResults.size() > 0)
			{
			%>
			            <tr>
              <td colspan=9 class="main" align="right">
              <br>
              <%
              //add page numbers here
              String baseURL = "admin/customers/approvedRestrictedProducts.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(SKUGlueSearch.getCriteria(), "UTF-8") + "&repId=" + URLEncoder.encode(SKUGlueSearch.getRepId(), "UTF-8") + "&col=" + URLEncoder.encode(SKUGlueSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(SKUGlueSearch.getSort(), "UTF-8");
              out.println(DbUtil.displayNavigation(baseURL, intPageSize, intRecordCount, intPage,SessionData));
              %>
              </td>
            </tr>
            <tr>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.products.id",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.products.description",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.whitelist.status",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                <td class=rowhead2><%=Languages.getString("jsp.admin.customers.whitelist.action",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                <td class=rowhead2>&nbsp;</td>
            </tr>
            <form method="post" action="admin/customers/skuGlue_update_status.jsp"><div align="center"> 
            <%
                  Iterator it = vecSearchResults.iterator();
                  String checkboxReadOnly = "";            
                  int intEvenOdd = 1;    
                  
                  int p = 0;
                  
                  checkboxReadOnly = "";                
                  

                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    p=p+1;
                    out.println("<tr class=row"+intEvenOdd+">" +
                                "<td width=175>" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
                                "<td nowrap>" + vecTemp.get(1) + "</td>" +
                                
                                "<td nowrap>" + vecTemp.get(2) + "</td>" +
                                "<td nowrap>");
                                
                                if (vecTemp.get(2).equals("0")) //Pending
				                    {
				                     out.println(Languages.getString("jsp.admin.customers.products.denied",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+"  value=\""+"0"+ "\" checked" + checkboxReadOnly + ">");
				                     out.println(Languages.getString("jsp.admin.customers.products.approve",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+" value=\""+"1"+ "\" " + checkboxReadOnly + ">");
				                     out.println(Languages.getString("jsp.admin.customers.products.pending",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+" value=\""+"2"+ "\" " + checkboxReadOnly + ">");
				                    }
                                 else if (vecTemp.get(2).equals("1")) //Pending
				                    {
				                     out.println(Languages.getString("jsp.admin.customers.products.denied",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+"  value=\""+"0"+ "\" " + checkboxReadOnly + ">");
				                     out.println(Languages.getString("jsp.admin.customers.products.approve",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+" value=\""+"1"+ "\" checked" + checkboxReadOnly + ">");
				                     out.println(Languages.getString("jsp.admin.customers.products.pending",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+" value=\""+"2"+ "\" " + checkboxReadOnly + ">");
				                    }
				                  else if (vecTemp.get(2).equals("2")) //Pending
				                    {
				                     out.println(Languages.getString("jsp.admin.customers.products.denied",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+"  value=\""+"0"+ "\" " + checkboxReadOnly + ">");
				                     out.println(Languages.getString("jsp.admin.customers.products.approve",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+" value=\""+"1"+ "\" " + checkboxReadOnly + ">");
				                     out.println(Languages.getString("jsp.admin.customers.products.pending",SessionData.getLanguage()) +"<input type=radio name=p_"+vecTemp.get(0)+" value=\""+"2"+ "\" checked  " + checkboxReadOnly + ">");
				                    }  
				               out.println("</td>"); 
                    out.println("</tr>");
                     if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }
                  }
                  //take the trailing comma out
                  vecSearchResults.clear();
            %></div>
 			<input type="hidden" name="merchant_id" value="<%=merchant_id%>">
 			<input type="hidden" name="countReg" value="<%=p%>">
            <tr><td colspan=9 class="rowhead2">&nbsp;<div align=right>
            <%if(SessionData.checkPermission(DebisysConstants.PERM_MANAGE_APPROVED_RESTRICTED_PRODUCTS))
            {%>
            <input type="reset" value="<%=Languages.getString("jsp.admin.customers.skuglue.reset",SessionData.getLanguage())%>"><input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.merchants.save_changes",SessionData.getLanguage())%>">
            <%}%>
            </div></td>
            </tr>
            </form>
			
			<%
			}
			else if (intRecordCount==0 && request.getParameter("search") != null)
			{
			 out.println("<table width=\"100%\" cellspacing=\"1\" cellpadding=\"1\"><tr><td class=\"main\"><br><br><font color=ff0000>"+Languages.getString("jsp.admin.customers.skuglue.merchants.not_found",SessionData.getLanguage())+"</font></td></tr></table>");
			}
			%>

           
           <form method="post" action="admin/customers/skuGlue_add_product.jsp"><div align="center"> 
           <%Vector vecSearchResults2 = new Vector();
            	  SKUGlueSearch.setMerchantId(merchant_id);
  				vecSearchResults2 = SKUGlueSearch.searchSkuNoWLProducts(intPage, intPageSize, SessionData, application);
  				Iterator it2 = vecSearchResults2.iterator();
  				
  				if (!vecSearchResults2.isEmpty()) {
  				 %>
	            <tr>
	            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.products.id",SessionData.getLanguage())%>&nbsp;</td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            </tr>
	            
            
	            <tr>	
	            	<td>
	            	<% 
	            		out.println("<SELECT NAME=product"+ ">");
		  				out.println("<OPTION value=\""+"0"+ "\" " +">"+ Languages.getString("jsp.admin.customers.skuglue.select",SessionData.getLanguage()) + "");
		  				while (it2.hasNext())
		                  {
		                    Vector vecTemp2 = null;
		                    vecTemp2 = (Vector) it2.next();
							out.println("<OPTION value=\""+vecTemp2.get(0)+ "\" " +">"+vecTemp2.get(1) + " ("+vecTemp2.get(0) + ")");
						
		  				 }	
		            	out.println("</SELECT>");
		            	vecSearchResults2.clear();
		            	%>
					</td>
	            </tr>
           
	 			<input type="hidden" name="merchant_id" value="<%=merchant_id%>">
	             <tr><td colspan=9 class="rowhead2">&nbsp;<div align=right>
	            <%if(SessionData.checkPermission(DebisysConstants.PERM_MANAGE_APPROVED_RESTRICTED_PRODUCTS))
	            {%>
	            <input type="reset" value="<%=Languages.getString("jsp.admin.customers.skuglue.reset",SessionData.getLanguage())%>"><input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.skuglue.add",SessionData.getLanguage())%>">
	            <%}%>
	            </div></td>
	            </tr>
	            <%} %>
            </form>
	
            </table>


          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp" %>
