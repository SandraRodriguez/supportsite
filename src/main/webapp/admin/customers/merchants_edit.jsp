<%@page import="com.debisys.bizUtil.City"%>
<%@page import="com.debisys.bizUtil.District"%>
<%@page import="com.debisys.customers.MerchantInvoicePayments"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page
	import="java.net.URLEncoder,
                com.debisys.utils.*,
                java.util.*,
                com.debisys.utils.TimeZone,
                com.debisys.tools.s2k,
                com.debisys.pincache.Pincache,
                java.text.SimpleDateFormat,
                com.debisys.properties.Properties,
                com.debisys.reports.PropertiesByFeature" pageEncoding="ISO-8859-1"%>

<%@page import="com.debisys.users.User"%>
<%@page import="java.net.URLDecoder"%>
<%
int section=2;
int section_page=8;

%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
<jsp:setProperty name="Merchant" property="*" />
<%@ include file="/includes/security.jsp"%>
<%
/******************************************************************************************/
/******************************************************************************************/
/*GEOLOCATION FEATURE*/
boolean hasPermissionGeolocation = SessionData.checkPermission(DebisysConstants.PERM_GEOLOCATION_FEAUTURE);
String instance = DebisysConfigListener.getInstance(application);
/*GEOLOCATION FEATURE*/
/******************************************************************************************/
/******************************************************************************************/

       
boolean hasPermissionManage_New_Rateplans = SessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS);
boolean bExternalRep=com.debisys.users.User.isExternalRepAllowedEnabled(SessionData);
boolean bMerchantAssignment=SessionData.checkPermission(DebisysConstants.PERM_CAN_SET_MERCHANT_ASSIGNMENT);
Hashtable merchantErrors = null;


if ( session.getAttribute("vecContacts") != null){
	Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
}

if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").length() > 0) )
{
	if ( (request.getParameter("mxContactAction").equals("ADD")) && (session.getAttribute("vecContacts") != null) ){
		Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
	}else if ( request.getParameter("mxContactAction").equals("SAVE") ){
		Vector vTemp = new Vector();
		vTemp.add(request.getParameter("contactTypeId"));
		vTemp.add(request.getParameter("contactName"));
		vTemp.add(request.getParameter("contactPhone"));
		vTemp.add(request.getParameter("contactFax"));
		vTemp.add(request.getParameter("contactEmail"));
		vTemp.add(request.getParameter("contactCellPhone"));
		vTemp.add(request.getParameter("contactDepartment"));
		Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
		if ( (request.getParameter("mxContactIndex") == null) || request.getParameter("mxContactIndex").equals("") ){
			Merchant.getVecContacts().add(vTemp);
		}else{
			Merchant.getVecContacts().setElementAt(vTemp, Integer.parseInt(request.getParameter("mxContactIndex")));
		}
		session.setAttribute("vecContacts", Merchant.getVecContacts());
	}else if ( request.getParameter("mxContactAction").equals("EDIT") ){
		Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
		Vector vec = (Vector)Merchant.getVecContacts().elementAt(Integer.parseInt(request.getParameter("mxContactIndex")));
		Merchant.setContactTypeId(Integer.parseInt(vec.get(0).toString()));
		Merchant.setContactName(vec.get(1).toString());
		Merchant.setContactPhone(vec.get(2).toString());
		Merchant.setContactFax(vec.get(3).toString());
		Merchant.setContactEmail(vec.get(4).toString());
		Merchant.setContactCellPhone(vec.get(5).toString());
		Merchant.setContactDepartment(vec.get(6).toString());
	}else if ( request.getParameter("mxContactAction").equals("DELETE") ){
		Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
		Merchant.getVecContacts().removeElementAt(Integer.parseInt(request.getParameter("mxContactIndex")));
		session.setAttribute("vecContacts", Merchant.getVecContacts());
	}else if ( (request.getParameter("mxContactAction").equals("CANCEL")) && (session.getAttribute("vecContacts") != null) ){
		Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
	}else if ( request.getParameter("mxContactAction").equals("--") ){
    	Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
     }	
}
else if (request.getParameter("submitted") != null){
	try{
		if (request.getParameter("submitted").equals("y")){
			if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ){//If the code reachs here is because there is at least one contact and the session var exists
				Vector vTmp = (Vector)session.getAttribute("vecContacts");
				Merchant.setVecContacts(vTmp);
				vTmp = (Vector)vTmp.get(0);
				Merchant.setContactName(vTmp.get(1).toString());
				Merchant.setContactPhone(vTmp.get(2).toString());
				Merchant.setContactFax(vTmp.get(3).toString());
				Merchant.setContactEmail(vTmp.get(4).toString());
				Merchant.setContactCellPhone(vTmp.get(5).toString());
				Merchant.setContactDepartment(vTmp.get(6).toString());
				Merchant.setCreditLimit(request.getParameter("newCreditLimit"));
				if ( request.getParameter("lblNewSharedBalance")!= null ){
					if ( request.getParameter("lblNewSharedBalance").equals("0") ){
						Merchant.setNewMerchantType(request.getParameter("newCreditType"));
					}else{
						Merchant.setNewMerchantType(DebisysConstants.MERCHANT_TYPE_SHARED);
					}
				}
			}
			String UseRatePlanModel="";
			String allowExternalRepCheck = "";
			String allowExternalOutsideHierarchyCheck="";
			if(Merchant.validateMerchant(SessionData, application)){
				UseRatePlanModel = request.getParameter("UseRatePlanModel");
				if ( UseRatePlanModel!=null && UseRatePlanModel.equals("on")){
					Merchant.setUseRatePlanModel(true);
				}else{
					Merchant.setUseRatePlanModel(false);
				}
				Merchant.setDEntityAccountType(request.getParameter("disabledentityAccountType"));
				allowExternalRepCheck = request.getParameter("allowExternalReps");
				if(allowExternalRepCheck != null && allowExternalRepCheck.equals("Yes")){
					Merchant.updateMerchantPermissions(true, Merchant.flagExternalRepsAllowed);
				}else{
					Merchant.updateMerchantPermissions(false, Merchant.flagExternalRepsAllowed);
				}
				 allowExternalOutsideHierarchyCheck = request.getParameter("allowMerchantAssignment");
				if(allowExternalOutsideHierarchyCheck != null && allowExternalOutsideHierarchyCheck.equals("Yes")){
					Merchant.updateMerchantExternalOutsideHierarchy(true, Merchant.flagMerchantAssignmentAllowed);
				}else{
					Merchant.updateMerchantExternalOutsideHierarchy(false, Merchant.flagMerchantAssignmentAllowed);
				}
				Merchant.updateMerchant(SessionData, application,request);				
				if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ){
					session.removeAttribute("vecContacts");
				}
				response.sendRedirect("/support/admin/customers/merchants_info.jsp?message=2&merchantId=" + Merchant.getMerchantId());
				return;
			}else{
				if(Merchant.getsalesmanid().length()>0){
					String strRefId = SessionData.getProperty("ref_id");
					Merchant.setsalesmanname( Merchant.getsalesmannamebyid(Merchant.getsalesmanid(),strRefId));
				}
				merchantErrors = Merchant.getErrors();
			}
		}
	}catch (Exception e){
		System.out.println("ERROR "+e);
	}
}else{
	Merchant.getMerchant(SessionData,application);
	if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){//If when deploying in Mexico
		Merchant.getMerchantMx(SessionData, application);
		session.setAttribute("vecContacts", Merchant.getVecContacts());
	}
	LogChanges.setSession(SessionData);
	LogChanges.setContext(application);
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE), String.valueOf(Merchant.getPaymentType()));
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR), String.valueOf(Merchant.getProcessType()));
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA), String.valueOf(Merchant.getRoutingNumber()));
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER), String.valueOf(Merchant.getAccountNumber()));
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID), String.valueOf(Merchant.getTaxId()));
	/*R31.1 Billing ABernal - Emunoz */
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_REGULATORYFEEID), String.valueOf(Merchant.getRecId()));
	/*END R31.1 Billing ABernal - Emunoz */

    // DBSY-1072 eAccounts Interface
    LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_MERCHANT), String.valueOf(Merchant.getEntityAccountType()));

	if (Merchant.isError()){
		response.sendRedirect("/support/message.jsp?message=4");
		return;
	}
}

/******************************************************************************************/
/******************************************************************************************/
/*GEOLOCATION FEATURE*/
int graceDaysLeft = 0, graceDays;
String installationDate;

installationDate = Properties.getPropertyValue(instance, "global","address.installationDate") + "";
try{
	graceDays = Integer.parseInt(Properties.getPropertyValue(instance, "global","address.daysOfGrace"));
}catch(Exception ex){
	graceDays = 0;
}
if(installationDate.equals("")){
	graceDaysLeft = 0;
}else{
	SimpleDateFormat sdf= new SimpleDateFormat("yyyy/MM/dd");
	java.util.Date d = null;
	try{
		d = sdf.parse(installationDate);
		Date today = new Date();
		int diff = (int)((long)(today.getTime() - d.getTime())/(1000*60*60*24));
		graceDaysLeft = graceDays - diff;
	}catch(Exception ex){
		//TODO: installationDate no se ha establecido en la tabla de properties
	}
}
/*INIT Internationalitation*/
String labelValidatingAddressInformation = Languages.getString("jsp.admin.geolocation.validatingAddress",SessionData.getLanguage());
String labelInvalidInput = Languages.getString("jsp.admin.geolocation.invalidInput",SessionData.getLanguage());
String labelKeyBad = Languages.getString("jsp.admin.geolocation.invalidKey",SessionData.getLanguage());
String labelAddressNotFound = Languages.getString("jsp.admin.geolocation.invalidInputBad",SessionData.getLanguage());
String labelInputWarnning1 = Languages.getString("jsp.admin.geolocation.warnning1",SessionData.getLanguage());
String labelInputWarnning2 = Languages.getString("jsp.admin.geolocation.warnning2",SessionData.getLanguage());
String labelInputWarnning3 = Languages.getString("jsp.admin.geolocation.warnning3",SessionData.getLanguage());
String labelInputWarnning4 = Languages.getString("jsp.admin.geolocation.warnning4",SessionData.getLanguage());
String labelInputWarnning5 = Languages.getString("jsp.admin.geolocation.warnning5",SessionData.getLanguage());
String labelCurrentAddress = Languages.getString("jsp.admin.geolocation.addressIs",SessionData.getLanguage());
String labelSuggestion1 = Languages.getString("jsp.admin.geolocation.suggestions1",SessionData.getLanguage());
String labelSuggestion2 = Languages.getString("jsp.admin.geolocation.suggestions2",SessionData.getLanguage());
String labelApply = Languages.getString("jsp.admin.geolocation.apply",SessionData.getLanguage());
String labelCancel = Languages.getString("jsp.admin.geolocation.cancel",SessionData.getLanguage());
String labelNotConnect = Languages.getString("jsp.admin.geolocation.notConnect",SessionData.getLanguage());
/*END Internationalitation*/

/*GEOLOCATION FEATURE*/
/******************************************************************************************/
/******************************************************************************************/
%>


<%@ include file="/includes/header.jsp"%>

<%

boolean isInternational = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                        &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
 
StringBuilder putMarker = new StringBuilder();
boolean permLocalizeMerchant = SessionData.checkPermission(DebisysConstants.PERM_ALLOW_LOCALIZE_MERCHANT_ON_MAP);
if ( permLocalizeMerchant )
{     
    //String sessionLanguage = SessionData.getUser().getLanguageCode();  
    ArrayList<PropertiesByFeature> propertiesByFeature = PropertiesByFeature.findPropertiesByFeature(PropertiesByFeature.FEATURE_MERCHANT_MAP_LOCATOR);
 
    String latitudeValue       = "latitude";    
    String longitudeValue      = "longitude";
    String zoomValue           = "zoom";
    HashMap propertiesValues = new HashMap();
    propertiesValues.put(latitudeValue, "25.05"); 
    propertiesValues.put(longitudeValue, "-77.33");
    propertiesValues.put(zoomValue, "11");
    for( PropertiesByFeature property : propertiesByFeature)
    {
        if ( propertiesValues.containsKey( property.getProperty() ))
        {
            propertiesValues.put( property.getProperty(), property.getValue());
        }
    }               
    if ( Merchant.getMerchantId() != null  )
    { 
        putMarker.append(" merchantId= "+Merchant.getMerchantId()+";"+'\n');     
    }
    if ( Merchant.getLatitude() != 0 && Merchant.getLongitude() != 0 )
    {                  
        putMarker.append(" hideWarnning1 = 1;"+'\n');     
        putMarker.append(" document.getElementById('longitudeCurrent').value = "+Merchant.getLongitude()+"; "+'\n');                      
        putMarker.append(" document.getElementById('latitudeCurrent').value = "+Merchant.getLatitude()+"; "+'\n');                              
        putMarker.append(" $('#mainTableSupport').show();"+'\n');                              
        putMarker.append(" $('#divPrintBanner').show();"+'\n');
        putMarker.append(" $('#map').hide();"+'\n');        
    } else{
        putMarker.append(" $('#mainTableSupport').hide();"+'\n');                              
        putMarker.append(" $('#divPrintBanner').hide();"+'\n');
        putMarker.append(" $('#map').show();"+'\n');
        putMarker.append(" initMap(); "+'\n');
    }   
%>
    <link rel="stylesheet" type="text/css" href="css/maps.css" title="style">               
    
    <script type="text/javascript">

        $(document).ready(function() {
            latitudeMap   = <%=propertiesValues.get(latitudeValue)%>;
            longitudeMap = <%=propertiesValues.get(longitudeValue)%>;
            zoomPoint    = <%=propertiesValues.get(zoomValue)%>; 
            
            typeAction=1;            
            <%=putMarker%>                        
        });
    </script>	
             
<%}%>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<%
	//s2k loaded style and javascript for DBSY-931 System 2000 Tools
if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
		&&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
		&& strAccessLevel.equals(DebisysConstants.ISO)
		&& SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
	) { //If when deploying in International and user is an ISO level
%>
<script type="text/javascript" src="includes/s2k/salesmannameid.js"></script>
<%}%>
<script type="text/javascript" src="includes/merchants_edit.js"></script>
<script language="JavaScript" src="includes/merchants_validations.js"></script>
<!--****************************************************************************************-->
<!--****************************************************************************************-->
<!--GEOLOCATION FEATURE-->
<link rel="stylesheet" type="text/css" href="css/popUp.css" title="style">
<style>
	.sep{clear:both; float:none;}
	.address{padding: 10px;}
	input{font-size: 10pt;}
	button{font-size: 10pt;}
	.address-option{border: 1px dotted gray;}
	.address-map{float: right; border: 1px dotted gray;}
	.address-info{max-width: 330px; witdh: 330px; float: left; text-align: left; padding: 5px; border: 1px dotted gray; overflow: auto;}
	.address-message{align: center; font-size: 12px; font-weight:bold;}
	.address-suggestions{clear:both; overflow: auto; padding-top:20px;}
	.address-buttons{align:center;}
        
        
</style>
<script language="JavaScript" src="includes/popUp.js"></script>
<script language="JavaScript" src="includes/AddressValidation.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8">
<%
    Enumeration enumErr = null;
    StringBuilder valError = new StringBuilder();
    if (merchantErrors!=null){
        enumErr = merchantErrors.keys();
        while ( enumErr.hasMoreElements() )
        {
          String strKey = enumErr.nextElement().toString();
          String strError = (String) merchantErrors.get(strKey);
          if (strError != null && !strError.equals(""))
          {
            valError.append("valControls.push( {'control':'"+strKey+"', 'messages': ['"+strError+"']});");        
          }
        }
    }
    %>
    $(function () {
      
    <%
    if (valError.toString().length()>0){
        out.println(valError);
        out.println("showValidationMessages(true); showMessagesValidations(); valControls=[];");        
    }
    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && hasPermissionGeolocation )
    {
%>
        //******Internationalitation******************//
        labelValidatingAddressInformation = '<%=labelValidatingAddressInformation%>';
        labelInvalidInput = '<%=labelInvalidInput%>';
        labelKeyBad = '<%=labelKeyBad%>';
        labelAddressNotFound = '<%=labelAddressNotFound%>';
        labelInputWarnning1 = '<%=labelInputWarnning1%>';
        labelInputWarnning2 = '<%=labelInputWarnning2%>';
        labelInputWarnning3 = '<%=labelInputWarnning3%>';
        labelInputWarnning4 = '<%=labelInputWarnning4%>';
        labelInputWarnning5 = '<%=labelInputWarnning5%>';
        labelCurrentAddress = '<%=labelCurrentAddress%>';
        labelSuggestion1 = '<%=labelSuggestion1%>';
        labelSuggestion2 = '<%=labelSuggestion2%>';
        labelApply = '<%=labelApply%>';
        labelCancel = '<%=labelCancel%>';
        labelNotConnect = '<%=labelNotConnect%>';
        //******Internationalitation******************//

        graceDays = '<%=graceDaysLeft%>';
        popAddress = new PopUp('addressValidation', 'Address validation', 600, 400);
        $('input[name=physAddress], input[name=physCity], input[name=physCounty], input[name=physState], input[name=physZip], input[name=latitude], input[name=longitude]').change(function(){
                $('#addressValidationStatus').val('0');
        });

        $('#Buttonsubmit').click(function(){            
            if($('#addressValidationStatus').val() === 0){
                    validateAddress('admin/tools/address.jsp'
                            ,$('input[name=physAddress]').val()
                            ,$('input[name=physCity]').val()
                            ,$('input[name=physState]').val()
                            ,$('input[name=physZip]').val()
                            ,function(data){
                                    if ( data !== null ){
                                            $('input[name=physAddress]').val(data[0]);
                                            $('input[name=physCity]').val(data[1]);
                                            $('input[name=physCounty]').val(data[2]);
                                            $('input[name=physState]').val(data[3]);
                                            $('input[name=physZip]').val(data[4]);
                                            $('input[name=latitude]').val(data[5]);
                                            $('input[name=longitude]').val(data[6]);
                                            $('span[name=latvalue]').text(data[5]);
                                            $('span[name=longvalue]').text(data[6]);
                                            $('#addressValidationStatus').val('1');
                                    }
                                    $('#formMerchant').submit();
                            }
                            ,null);
            }else{                    
                $('#formMerchant').submit();                    
            }
            return false;
        });
        $('#cancel').click(function(){history.go(-1);});
	
<%
}else{
%>
	
    $('#Buttonsubmit').click(function(){
        $('#formMerchant').submit();
    });
    $('#cancel').click(function(){history.go(-1);});
	

<%
}
/*GEOLOCATION FEATURE*/
/******************************************************************************************/
/******************************************************************************************/
%>
    });

var emailPattern = '^[_a-zA-Z0-9-\\/]+(\\.[_a-zA-Z0-9-\\/]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$';

function check_form(form_name) {
    //alert("calling check_form");
    if (submitted === true) {
        alert("<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg1",SessionData.getLanguage())%>");
        return false;
    }
    error = false;
    form = form_name;
    error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";
    check_input("businessName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_business_name",SessionData.getLanguage())%>");
    check_input("dba", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_dba",SessionData.getLanguage())%>");
    //alert("calling check_form 1");
<%
if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
    //check_input("businessHours", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_businesshours",SessionData.getLanguage())%>");
<%
} //End of if when deploying in Mexico
%>
    //alert("calling check_form 2");
    check_input("physAddress", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_street_address",SessionData.getLanguage())%>");
		
    <%
    String mandatoryMerchantClassification = DebisysConfigListener.getMandatoryMerchantClassification(application); 
    if (mandatoryMerchantClassification.trim().equals("1")){%>
        check_input("merchantClassification", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_merchantClassification",SessionData.getLanguage())%>");
        check_input("merchantClassificationOtherDesc", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_merchantClassificationOtherDesc",SessionData.getLanguage())%>");
    <%}
    	
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
%>
    check_input("physCity", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_city",SessionData.getLanguage())%>");
    check_input("physState", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_state",SessionData.getLanguage())%>");
    check_input("physZip", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_zip",SessionData.getLanguage())%>");
    check_input("accountNumber", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_account_number",SessionData.getLanguage())%>");
    check_input("routingNumber", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_routing_number",SessionData.getLanguage())%>");
<%
}else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
%>    
    check_input("physCountry", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_country",SessionData.getLanguage())%>");
<%
}

if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
    //alert("calling check_form 3");
    if ( !document.getElementById('chkBillingAddress'.checked) ) {
        check_input("mailAddress", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_billing_address",SessionData.getLanguage())%>");
        check_input("mailCountry", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_billing_country",SessionData.getLanguage())%>");
    }
    check_input("contactData", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact_data",SessionData.getLanguage())%>");
    check_input("taxId", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_taxid",SessionData.getLanguage())%>");
    //alert(document.getElementById('trCreditSelection').style.display);
    if ( document.getElementById('trCreditSelection') !== null ){
       if ( document.getElementById('trCreditSelection').style.display !== 'none' )
       {
            check_input("txtNewCreditLimit", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_creditlimit",SessionData.getLanguage())%>");
       }
    }		

<%
} //End of if when deploying in Mexico
else
{
%>

    check_input("contactName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact",SessionData.getLanguage())%>");
    check_input("contactPhone", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_phone",SessionData.getLanguage())%>");

    <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){%>
    var contactEmail = document.getElementById('contactEmail');
    if (contactEmail!==null){
        debugger;
        ValidateRegExp(contactEmail, emailPattern);
        check_input("contactEmail", 3, "<%=Languages.getString("jsp.admin.emailError",SessionData.getLanguage())%>");                 
    }    
    //var mailNotificationAddress = document.getElementById('mailNotificationAddress');
    //if (mailNotificationAddress!==null){
    //    ValidateRegExp(mailNotificationAddress, emailPattern, false);
    //}
    <%}%>
<%
}
if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES)){
%>
		validate_select_processType("processType", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_processType",SessionData.getLanguage())%>");

		//alert("calling check_form PERM_UPDATE_PAYMENT_FEATURES");
<%
}

// DBSY-1072 eAccounts Interface
if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)){
%>
	//alert("calling check_form PERM_EDIT_ACCOUNT_TYPE");
	check_input("entityAccountType", 1, "<%=Languages.getString("jsp.admin.customers.error_entityAccountType",SessionData.getLanguage())%>");
<%
}

	//s2k javascript check values for DBSY-931 System 2000 Tools
if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
		&&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
		&& strAccessLevel.equals(DebisysConstants.ISO)
		&& SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
) { //If deploying in International and user is an ISO
%>
    //alert("calling check_form 395");
    var name = form.salesmanid.options[form.salesmanname.selectedIndex].value;
    var id = form.salesmanid.options[form.salesmanid.selectedIndex].value;
    // skip check if both empty
    if( id.length >0  || name.length >0 ){
            if( id === "" || id === null ){
                    check_input("salesmanid", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_salesmanid",SessionData.getLanguage())%>");
            }else if( name === "" || name === null ){
                    check_input("salesmanname", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_salesmanname",SessionData.getLanguage())%>");
            }
    }
		//alert("calling check_form 405");
<%
}
//Changed by LF on 2006/07/04 because the validation for TaxID was already added and also a language resource already exists
//changed by jay on 6/22/06
//since it should be required for mexico and domestic but not international
if ( !(Merchant.isAchAccount() || (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && !SessionData.checkPermission(DebisysConstants.PERM_EDIT_BANKING_INFO_MERCHANT))))
{
%>
    <%if (!deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){%>
        check_input("taxId", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_tax_id",SessionData.getLanguage())%>");
    <%}%>
    var isCorrectAba = verifyAba(9,'/^[0-9]{9}$/',"<%=Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage())%>");
    if(isCorrectAba ===false){
        error = true;
        error_message = error_message + "* "+"<%=Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage())%>"+" \n";
    }
    var isCorrectAccount = verifyAccount(20,"<%=Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage())%>");
    if(isCorrectAccount === false){
        error = true;
        error_message = error_message + "* "+"<%=Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage())%>"+" \n";
    }

<%
}
%>
    if (document.getElementById('monthlyFeeThreshold')!==null){
        validateMonthlyFee(document.getElementById('monthlyFeeThreshold'));
    }    
    //alert("End check_form javascript!! "+error);
    if (error === true){
            //alert(error_message);
            showMessagesValidations();
            showValidationMessages(true);                
            return false;
    }else{
            submitted = true;
            return true;
    }
}
	
function validateMonthlyFee(c) {
    var montlhyFeeValidationMessage = "<%=Languages.getString("com.debisys.terminals.error_monthly_fee",SessionData.getLanguage())%> ";
    if ( c.value.length > 0 && parseFloat(c.value) !== '0' ) {                      
        if (isNaN(c.value)) {              
            c.value = '';
            check_input(c.id, 1, montlhyFeeValidationMessage);
            c.value = '0';
            error = true;
        } else if (parseFloat(c.value) < 0) {
            c.value = '';            
            check_input(c.id, 1, montlhyFeeValidationMessage);
            c.value = '0';            
            error = true;
        } else {
            check_input(c.id, 0, montlhyFeeValidationMessage);            
        }
    } else {
        check_input(c.id, 1, montlhyFeeValidationMessage);            
    }
}
	function validate_select(field_name, message) {
	  if (form.elements[field_name] && (form.elements[field_name].type !== "hidden")) {
	    var field_value = form.elements[field_name].value;
	
	    if (field_value === '' || field_value === "-1"  || field_value === "0") {
	      error_message = error_message + "* " + message + "\n";
	      error = true;
	    }
	  }
	}
	
	function check_inputClassificationDesc(message) {
	// document.getElementById('merchantClassificationOtherDescDiv').style.display="none"
	  if (form.elements["merchantClassificationOtherDesc"] 
                  && form.elements["merchantClassificationOtherDesc"].type !== "hidden" 
                    && document.getElementById("merchantClassificationOtherDescDiv").style.display !== "none") {
	    var field_value = form.elements["merchantClassificationOtherDesc"].value;
	
	    if (field_value === '' || field_value.length > 150) {
	      error_message = error_message + "* " + message + "\n";
	      error = true;
	    }
	  }
	}
	
	function merchantClassificationCheck(){		    
            var merchantClassificationId = document.getElementById('merchantClassification');
            $.getJSON("/support/includes/ajax.jsp?class=com.debisys.customers.Merchant&method=getMerchantClassificationByIdAjax&value=" + merchantClassificationId.value + "&rnd=" + Math.random(),
            function(data){
                $.each(data.items, function(i, item){
                    if(item.hasClassificationDescription === true){
                        document.getElementById('merchantClassificationOtherDescDiv').style.display="inline";
                        document.getElementById('merchantClassificationOtherDesc').style.display="inline";
                    }
                    else{
                        document.getElementById('merchantClassificationOtherDesc').value = "";
                        document.getElementById('merchantClassificationOtherDescDiv').style.display="none";
                        document.getElementById('merchantClassificationOtherDesc').style.display="none";		        
                    }
                });
             });
	}

	function validateIntegerGreaterThanX(c, x) {
		if ( c.value.length > 0 ) {
			if (isNaN(c.value)) {
				alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
				c.focus();
				c.select();
				return (false);
			} else if (c.value <= x) {
				var s = '<%=Languages.getString("jsp.admin.errorIntegerGreatherThanX",SessionData.getLanguage())%>' + x;
				alert(s);
				c.focus();
				c.select();
				return (false);
			}
		}
	}//End of function validateIntegerGreaterThanX

	function ValidateTextAreaLength(c, x) {
		if ( c.value.length > x ) {
			alert('<%=Languages.getString("jsp.admin.errorTextLongerThanX",SessionData.getLanguage())%>' + x);
			c.focus();
			c.select();
			return false;
		}
	}//End of function ValidatePhysAddress

	function ValidateRegExp(c, e) {
            var exp = new RegExp(e);
            //exp.compile(e);
            if ( !exp.test(c.value) && (c.value.length > 0) ) {
                //alert('<%=Languages.getString("jsp.admin.errorExpressionInvalid",SessionData.getLanguage())%>');
                //c.focus();
                //c.select();
                c.value='';
                return false;
            }
            return true;
	}//End of function ValidateRegExp


	function SetRepSharedBalance(nValue, nCreditType){
		var nCurrentValue = <%=((Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_SHARED))?1:0)%>;
		if ( nValue !== nCurrentValue ){
			document.getElementById("tblRepChange").style.display = "block";
			if ( nValue === 1 ){
				document.getElementById("trCreditSelection").style.display = "none";
				document.getElementById("trSharedBalance").style.display = "block";
			}else{			    
				document.getElementById("trCreditSelection").style.display = "block";
				document.getElementById("trSharedBalance").style.display = "none";
<%
if ( request.getParameter("newCreditLimit") == null ){
%>
				document.getElementById("txtNewCreditLimit").value = "";
<%
}
%>
			}
		}else{
			document.getElementById("tblRepChange").style.display = "none";
		}
		document.getElementById("lblNewSharedBalance").value = nValue;
<%
if ( request.getParameter("newCreditType") == null ){
%>
		if ( nCreditType < 3 ){
			document.getElementById("ddlNewCreditType").value = nCreditType;
		}
<%
}
%>
	}//End of function SetRepSharedBalance
//-->


  //DTU-369 Payment Notifications
  function chkPaymentNoti()
  {
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
		<%
		if (Merchant.isPaymentNotificationSMS())
		{
		%>
		 	valuePayNotiSMSChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiSMSChk.checked=false;
		<%
		}
		%>
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
		<%
		if (Merchant.isPaymentNotificationMail())
		{
		%>
		 	valuePayNotiMailChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiMailChk.checked=false;
		<%
		}
		%>
  		var valuePayNotiChk = document.getElementById('paymentNotifications');
		<%
		if (Merchant.isPaymentNotifications())
		{
		%>
		 	valuePayNotiChk.checked=true;
  			valuePayNotiSMSChk.disabled=false;
  			valuePayNotiMailChk.disabled=false;
		<%
		}
		else
		{
		%>
			valuePayNotiChk.checked=false;
			valuePayNotiSMSChk.checked=false;
			valuePayNotiMailChk.checked=false;
			valuePayNotiSMSChk.disabled=true;
			valuePayNotiMailChk.disabled=true;
		<%
		}
		%>
  }
  function changePayNoti()
  {
  	var payNoti = document.getElementById('paymentNotifications').checked;
  	if(payNoti === true)
	{
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  		valuePayNotiSMSChk.checked=false;
  		valuePayNotiSMSChk.disabled=false;
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  		valuePayNotiMailChk.checked=false;
  		valuePayNotiMailChk.disabled=false;
  	}
  	else
	{
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  		valuePayNotiSMSChk.checked=false;
  		valuePayNotiSMSChk.disabled=true;
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  		valuePayNotiMailChk.checked=false;
  		valuePayNotiMailChk.disabled=true;
  	}
  }

  function changePayNotiSMS()
  {
  	var payNotiSMS = document.getElementById('paymentNotificationSMS').checked;
  	var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  	if(payNotiSMS === true)
	{
  		valuePayNotiMailChk.checked=false;
  	}
  	else
	{
  		valuePayNotiMailChk.checked=true;
  	}
  }

  function changePayNotiMail()
  {
  	var payNotiMail = document.getElementById('paymentNotificationMail').checked;
  	var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  	if(payNotiMail === true)
	{
  		valuePayNotiSMSChk.checked=false;
  	}
  	else
	{
  		valuePayNotiSMSChk.checked=true;
  	}
  }

//DTU-480 Low Balance Alert Message SMS Mail
    function chkBalanceNoti()
  	{
		var valueBalNotiValue = document.getElementById('balanceNotificationValue');
		var valueBalNotiDays = document.getElementById('balanceNotificationDays');
		  	
    	var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		<%
		if (Merchant.isBalanceNotificationTypeValue())
		{
		%>
		 	valueBalNotiValueChk.checked=true;
		 	valueBalNotiValue.disabled=false;
		<%
		}
		else
		{
		%>
			valueBalNotiValueChk.checked=false;
			valueBalNotiValue.disabled=true;
		<%
		}
		%>
    	var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		<%
		if (Merchant.isBalanceNotificationTypeDays())
		{
		%>
		 	valueBalNotiDaysChk.checked=true;
		 	valueBalNotiDays.disabled=false;
		<%
		}
		else
		{
		%>
			valueBalNotiDaysChk.checked=false;
			valueBalNotiDays.disabled=true;
		<%
		}
		%>
  		var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		<%
		if (Merchant.isBalanceNotificationSMS())
		{
		%>
		 	valueBalNotiSMSChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiSMSChk.checked=false;
		<%
		}
		%>
  		var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		<%
		if (Merchant.isBalanceNotificationMail())
		{
		%>
		 	valueBalNotiMailChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiMailChk.checked=false;
		<%
		}
		%>
  		var valueBalNotiChk = document.getElementById('balanceNotifications');
		<%
		if (Merchant.isBalanceNotifications())
		{
		%>
		 	            valueBalNotiChk.checked=true;	
 			            valueBalNotiSMSChk.disabled=false;
  			            valueBalNotiMailChk.disabled=false;
						valueBalNotiValueChk.disabled=false;
						valueBalNotiDaysChk.disabled=false;
		<%
		}
		else
		{
		%>
			            valueBalNotiChk.checked=false;
			            valueBalNotiSMSChk.disabled=true;
			            valueBalNotiSMSChk.checked = false;
			            valueBalNotiMailChk.disabled=true;
			            valueBalNotiMailChk.checked = false;
						valueBalNotiValueChk.disabled=true;
						valueBalNotiValueChk.checked = false;
						valueBalNotiDaysChk.disabled=true;
						valueBalNotiDaysChk.checked = false;
						valueBalNotiValue.disabled=true;
						valueBalNotiDays.disabled=true;	
		<%
		}
		%>
  }  
  
				function changeBalNoti() {
		        var balNoti = document.getElementById('balanceNotifications').checked;
		        if (balNoti == true) {
		            var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		            valueBalNotiDaysChk.checked = false;
		            valueBalNotiDaysChk.disabled = false;
		            var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		            valueBalNotiValueChk.checked = false;
		            valueBalNotiValueChk.disabled = false;				
		            var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		            valueBalNotiSMSChk.checked = false;
		            valueBalNotiSMSChk.disabled = false;
		            var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		            valueBalNotiMailChk.checked = false;
		            valueBalNotiMailChk.disabled = false;
					var valueBalNotiValue = document.getElementById('balanceNotificationValue');
					var valueBalNotiDays = document.getElementById('balanceNotificationDays');
					valueBalNotiValue.disabled=true;
					valueBalNotiDays.disabled=true;		            
		        }
		        else {
		            var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		            valueBalNotiDaysChk.checked = false;
		            valueBalNotiDaysChk.disabled = true;
		            var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		            valueBalNotiValueChk.checked = false;
		            valueBalNotiValueChk.disabled = true;
		            var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		            valueBalNotiSMSChk.checked = false;
		            valueBalNotiSMSChk.disabled = true;
		            var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		            valueBalNotiMailChk.checked = false;
		            valueBalNotiMailChk.disabled = true;
					var valueBalNotiValue = document.getElementById('balanceNotificationValue');
					var valueBalNotiDays = document.getElementById('balanceNotificationDays');
					valueBalNotiValue.disabled=true;
					valueBalNotiDays.disabled=true;			            
		        }
		    }

              function changeBalNotiValue()
              {
  	            var balNotiValue = document.getElementById('balanceNotificationTypeValue').checked;
				var valueBalNotiValue = document.getElementById('balanceNotificationValue');
  	            if(balNotiValue === true)
	            {
					valueBalNotiValue.disabled=false;
  	            }
  	            else
	            {
					valueBalNotiValue.disabled=true;
  	            }
              }
  
              function changeBalNotiDays()
              {
  	            var balNotiDays = document.getElementById('balanceNotificationTypeDays').checked;
				var valueBalNotiDays = document.getElementById('balanceNotificationDays');					
  	            if(balNotiDays === true)
	            {
					valueBalNotiDays.disabled=false;						
				}
				else
				{
					valueBalNotiDays.disabled=true;						
				}
              }	
              
		    function changeBalNotiSMS() 
		    {
		        var balNotiSMS = document.getElementById('balanceNotificationSMS').checked;
		        var valueBalNotiMail = document.getElementById('balanceNotificationMail');
		        if (balNotiSMS === true) 
		        {
		            valueBalNotiMail.checked = false;
		        }
		        else 
		        {
		            valueBalNotiMail.checked = true;
		        }
		    }

		    function changeBalNotiMail() 
		    {
		        var balNotiMail = document.getElementById('balanceNotificationMail').checked;
        		var valueBalNotiSMS = document.getElementById('balanceNotificationSMS');		        
		        if (balNotiMail == true) 
		        {
		    
		            valueBalNotiSMS.checked = false;
		        }
		        else 
		        {
		            valueBalNotiSMS.checked = true;
		        }
		    }              
              			
			
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" width="2000"
			class="formAreaTitle">
			&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_edit.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20">
			<img src="images/top_right_blue.gif">
		</td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#ffffff" class="main">
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				bgcolor="#ffffff">
				<tr>
					<td class="formArea2">
						<br>
						<span class="main"><%=Languages.getString("jsp.admin.customers.merchants_edit.instructions",SessionData.getLanguage())%></span>
						<br>
						<br>
						<table border="0" width="100%" cellpadding="0" cellspacing="0"
							align="left">
							<tr>
								<td>
									<form id="formMerchant" name="formMerchant" method="post" action="admin/customers/merchants_edit.jsp" onsubmit="return check_form(formMerchant);">
										<table width="100%">
<%
if (merchantErrors != null){
	out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
	Enumeration enum1=merchantErrors.keys();
	while(enum1.hasMoreElements()){
		String strKey = enum1.nextElement().toString();
		String strError = (String) merchantErrors.get(strKey);
		out.println("<li>" + strError + "</li>");
	}
	out.println("</font></td></tr>");
}
%>
											<tr>
												<td class="formAreaTitle2"><%=Languages.getString("jsp.admin.customers.merchants_edit.company_information",SessionData.getLanguage())%></td>
											</tr>
											<tr>
												<td class="formArea2">
													<table>
<%
if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
														<tr>
															<td class="main">
																<b><%=Languages.getString("jsp.admin.customers.merchants_add.Region",SessionData.getLanguage())%>:</b>
															</td>
															<td nowrap="nowrap" class="main">
																<select id="region" name="region">
<%
	Vector vecRegions = com.debisys.customers.Merchant.getRegions(customConfigType);
	Iterator itRegion = vecRegions.iterator();
	while (itRegion.hasNext()) {
		Vector vecTemp = null;
		vecTemp = (Vector)itRegion.next();
		String strRegId = vecTemp.get(0).toString();
		String strRegName = vecTemp.get(1).toString();
		if ( (request.getParameter("region") != null) && (strRegId.equals(request.getParameter("region"))) ){
			out.println("<OPTION VALUE=\"" + strRegId + "\" SELECTED>" + strRegName + "</OPTION>");
		}else if ( strRegId.equals( (new Integer(Merchant.getRegion())).toString() ) ){
			out.println("<OPTION VALUE=\"" + strRegId + "\" SELECTED>" + strRegName + "</OPTION>");
		}else{
			out.println("<OPTION VALUE=\"" + strRegId + "\">" + strRegName + "</OPTION>");
		}
	}
%>
																</select>
															</td>
														</tr>												
														
														<tr>
															<td class="main">
																<b><%=Languages.getString("jsp.admin.customers.merchants_add.control_number",SessionData.getLanguage())%>:</b>
															</td>
															<td class="main" nowrap="nowrap">
																<input type="text" id="controlNumber" name="controlNumber" size="20" value="<%=(Merchant.getControlNumber()==null)?"":Merchant.getControlNumber()%>" />
															</td>
														</tr>
<%
}// MX

														
															Vector vecMerchantClassifications = Merchant.getMerchantClassifications(SessionData);
															Iterator itMerchantClassifications = vecMerchantClassifications.iterator();
														%>
														<tr>
															<td class="main">
																<b><%=Languages.getString("jsp.admin.customers.merchants_add.merchant_classification",SessionData.getLanguage())%>:</b>
															</td>
															<td class="main" nowrap="nowrap">
																<select id="merchantClassification" name="merchantClassification" onchange="merchantClassificationCheck()">
																	<option value="0"></option>
																	<%
																while(itMerchantClassifications.hasNext()){
																	Vector vecTemp = null;
																	vecTemp = (Vector)itMerchantClassifications.next();
																	String strClassifId = vecTemp.get(0).toString();
																	String strClassifName = vecTemp.get(1).toString();
																	String strSelected = (strClassifId.equals(Integer.toString(Merchant.getMerchantClassification())))?"SELECTED":"";
																	out.println("<option value=\""+strClassifId+"\" "+strSelected+">"+strClassifName+"</option>");
																}
															%>
																</select>
        <div style="<%=(Merchant.getMerchantClassificationOtherDesc()==null || Merchant.getMerchantClassificationOtherDesc().trim().equals(""))?"display:none":"display:inline"%>" id="merchantClassificationOtherDescDiv">
            <b id="merchantClassificationOtherDescB"><%=Languages.getString("jsp.admin.customers.merchants_edit.merchantClassificationOtherDesc",SessionData.getLanguage())%></b>
            <input type="text" id="merchantClassificationOtherDesc" name="merchantClassificationOtherDesc" size="30"
                style="<%=(Merchant.getMerchantClassificationOtherDesc()==null || Merchant.getMerchantClassificationOtherDesc().trim().equals(""))?"display:none":"display:inline"%>"
                value="<%=(Merchant.getMerchantClassificationOtherDesc()==null)?"":Merchant.getMerchantClassificationOtherDesc()%>"/>
        </div>
															</td>
														</tr>

<%if (strAccessLevel.equals(DebisysConstants.ISO) ||strAccessLevel.equals(DebisysConstants.AGENT) ||strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>
														<tr>
															<td class="main" nowrap="nowrap">
																<b> <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
																	<%=Languages.getString("jsp.admin.customers.merchants_edit.sales_rep",SessionData.getLanguage())%>
																	<% } else{%> <%=Languages.getString("jsp.admin.customers.merchants_edit.sales_rep_international",SessionData.getLanguage())%>
																	<%}%>:</b>
															</td>
															<td nowrap="nowrap" class="main">
																<input type="text" id="repName" name="repName" value="<%=Merchant.getRepName()%>" size="30" readonly="readonly">
<%
	if (merchantErrors != null && merchantErrors.containsKey("repId")) out.print("<font color=ff0000>*</font>");
%>
																<input type="hidden" name="runningLiability" value="<%=Merchant.getRunningLiability()%>">
																
<%
	if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
		//if ( Double.parseDouble(Merchant.getRunningLiability()) == 0 ){
		if(!Pincache.hasPinesAssigned(Merchant.getMerchantId())){
%>
																<input type="button" name="repSelect" id="repSelect" value="<%=Languages.getString("jsp.admin.customers.merchants_edit.select_rep",SessionData.getLanguage())%>" onClick="showRepSelector();">
																<br>
																<table id='tblRepChange' border='0' style='display: none;'>
																	<tr class='main'>
																		<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_info.repchangenotice",SessionData.getLanguage())%></td>
																	</tr>
																	<tr id="trCreditSelection" class="main" style="display: none;">
																		<td>
																			<table>
																				<tr class='main'>
																					<td colspan="3" nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_info.repchangeselectnotice",SessionData.getLanguage())%></td>
																				</tr>
																				<tr class='main'>
																					<td>
																						<b><%=Languages.getString("jsp.admin.customers.merchants_info.credit_limit_type",SessionData.getLanguage())%>:</b>
																					</td>
																					<td>&nbsp;</td>
																					<td>
																						<select id='ddlNewCreditType' name='newCreditType'>
<%
		String sPrepaidType = "";
		String sCreditType = "";
		if ( request.getParameter("newCreditType") != null ){
			if ( request.getParameter("newCreditType").equals("1") ){
				sPrepaidType = "selected";
			}else{
				sCreditType = "selected";
			}
		}
		String sSharedBalance = "0";
		if ( request.getParameter("lblNewSharedBalance") != null ){
			sSharedBalance = request.getParameter("lblNewSharedBalance");
		}
%>
																							<option value='1' <%=sPrepaidType%>><%=Languages.getString("jsp.admin.customers.merchants_info.credit_type1",SessionData.getLanguage())%></option>
																							<option value='2' <%=sCreditType%>><%=Languages.getString("jsp.admin.customers.merchants_info.credit_type2",SessionData.getLanguage())%></option>
																						</select>
																						<input id="lblNewSharedBalance" type="hidden" name="lblNewSharedBalance" value="<%=sSharedBalance%>">
																					</td>
																				</tr>
																				<tr class="main">
																					<td>
																						<b><%=Languages.getString("jsp.admin.customers.merchants_info.credit_limit",SessionData.getLanguage())%>:</b>
																					</td>
																					<td>&nbsp;</td>
<%
		String sCreditLimit = "";
		if ( request.getParameter("newCreditLimit") != null ){
			sCreditLimit = request.getParameter("newCreditLimit");
		}
%>
																					<td>
																						<input id='txtNewCreditLimit' type="text" name="newCreditLimit" value="<%=sCreditLimit%>"
																							size="8" maxlength="15" onblur="validateIntegerGreaterThanX(this, 0);">
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr id="trSharedBalance" class="main" style="display: none;">
																		<td>
																			<table>
																				<tr class='main'>
																					<td>
																						<b><%=Languages.getString("jsp.admin.customers.merchants_info.credit_limit_type",SessionData.getLanguage())%>:</b>
																					</td>
																					<td>&nbsp;</td>
																					<td><%=Languages.getString("jsp.admin.customers.merchants_info.credit_type4",SessionData.getLanguage())%></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
<%
		}
		if ( request.getParameter("submitted") != null && request.getParameter("mxContactAction") == null && request.getParameter("invoicingAction") == null ){
%>
																<script>
																	SetRepSharedBalance(document.getElementById("lblNewSharedBalance").value, 4);
																</script>
<%
		}
                if(request.getParameter("invoice") != null){
                    String invoiceValue = request.getParameter("invoice");
                    if(invoiceValue.equals("NINV")){
                        MerchantInvoicePayments.deletedAllMerchantInvoicePayments(Merchant.getMerchantId());
                    }
                    
                }
    
	}else{
		if(!Pincache.hasPinesAssigned(Merchant.getMerchantId())){
%>
																<input type="button" name="repSelect" id="repSelect"
																	value="<%=Languages.getString("jsp.admin.customers.merchants_edit.select_rep",SessionData.getLanguage())%>"
																	onClick="showRepSelector();">
																<br>
<%
		}
	}
%>
															</td>
														</tr>
														<input id='lblOldRepID' type="hidden" name="oldRepId" value="<%=Merchant.getOldRepId()%>">
														<input type="hidden" name="creditTypeId" value="<%=Merchant.getCreditTypeId()%>">
<%
}
%>
														<input type="hidden" name="merchantType" value="<%=Merchant.getMerchantType()%>">
														<input type="hidden" name="repId" id="repId" value="<%=Merchant.getRepId()%>">
														<input type="hidden" name="merchantId" id="merchantId" value="<%=Merchant.getMerchantId()%>">
														<tr>
															<td class="main">
																<b><%=Languages.getString("jsp.admin.customers.merchants_edit.business_name",SessionData.getLanguage())%>:</b>
															</td>
															<td nowrap="nowrap" class="main">
																<input type="text" id="businessName" name="businessName"
																	value="<%=Merchant.getBusinessName()%>" size="20"
																	maxlength="50">
																<%if (merchantErrors != null && merchantErrors.containsKey("businessName")) out.print("<font color=ff0000>*</font>");%>
															</td>
														</tr>
<%
    boolean isDomestic = com.debisys.utils.DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
    boolean permissionEnableEditMerchantAccountExecutives= SessionData.checkPermission(DebisysConstants.PERM_Enable_Edit_Merchant_Account_Executives);
    Long isoID = Long.parseLong(SessionData.getUser().getISOId(strAccessLevel,SessionData.getUser().getRefId()));
    List<District> districts=null;
    if (isInternational){
        districts = District.findByCountryByIso(isoID);
        if (districts==null){
            isInternational=false;
        }
    }
                        
if (permissionEnableEditMerchantAccountExecutives){	
	//System.out.println("*********************** "+isoID+" ********************");
	ArrayList<com.debisys.tools.AccountExecutive> executives = com.debisys.tools.AccountExecutive.findExecutivesByActor(isoID,request,true,SessionData);
	StringBuffer optionsExecutives = new StringBuffer();
	boolean hasExecutive=false;
	for(com.debisys.tools.AccountExecutive account : executives){
		if (Merchant.getExecutiveAccountId()==account.getId()){
			optionsExecutives.append("<option selected value='"+account.getId()+"'>"+account.getNameExecutive()+"</option>");
			hasExecutive=true;
		}else{
			optionsExecutives.append("<option value='"+account.getId()+"'>"+account.getNameExecutive()+"</option>");
		}
	}
	if (hasExecutive){
		optionsExecutives.append("<option value='0'>-----</option>");
	}else{
		optionsExecutives.append("<option value='0' selected >-----</option>");
	}
%>
														<tr>
															<td class="main" nowrap="nowrap">
																<b><%=Languages.getString("jsp.admin.reports.tools.accountexecutives.label",SessionData.getLanguage())%>: </b>
															</td>
															<td class="main" nowrap="nowrap" colspan="2">
																<select id="executiveAccountId"
																	name="executiveAccountId">
																	<%=optionsExecutives.toString()%>
																</select>
															</td>
														</tr>
<%
}
%>
														<tr>
															<td class="main">
																<b><%=Languages.getString("jsp.admin.customers.merchants_edit.dba",SessionData.getLanguage())%>:</b>
															</td>
															<td nowrap="nowrap" class="main">
																<input type="text" id="dba" name="dba" value="<%=Merchant.getDba()%>" size="20" maxlength="50">
<%
	if (merchantErrors != null && merchantErrors.containsKey("dba")) out.print("<font color=ff0000>*</font>");
%>
															</td>
														</tr>
														<%if (!isDomestic){%>
														<tr>
															<td class="main">
																<b><%=Languages.getString("jsp.admin.customers.merchants_edit.merchantOwnerId",SessionData.getLanguage())%>:</b>
															</td>
															<td nowrap="nowrap" class="main">
																<input type="text" id="merchantOwnerId" name="merchantOwnerId" value="<%=Merchant.getMerchantOwnerId()%>" size="12" maxlength="12">
															</td>
														</tr>
                                                                                                                <%}%>
<%
	if (SessionData.checkPermission(DebisysConstants.PERM_REGULATORY_FEE)) {
		Vector vecSearchResults2 = new Vector();
		vecSearchResults2 = Merchant.merchantRegulatoryFees(SessionData, application);
		Iterator it2 = vecSearchResults2.iterator();
		String selected = "";
%>
														<tr>
															<td class="main">
																<b><%=Languages.getString("jsp.admin.customers.merchants_edit.regulatoryFee",SessionData.getLanguage())%>:</b>
															</td>
															<td nowrap="nowrap" class="main">
<%
		if (!vecSearchResults2.isEmpty()) {
%>
																<select name="regulatoryFeeId" id="regulatoryFeeId">
<%
			while (it2.hasNext()){
				Vector vecTemp2 = null;
				vecTemp2 = (Vector) it2.next();
				if(Integer.toString(Merchant.getRegulatoryFeeId()).equals(vecTemp2.get(0))){
					selected="selected";
				}else{
					selected = "";
				}
				out.println("<OPTION value=\""+vecTemp2.get(0)+ "\" " + " " +selected +">"+vecTemp2.get(1) + " ("+vecTemp2.get(0) + ")");
			}
			out.println("</SELECT>");
			vecSearchResults2.clear();
%>
																</select>
<%
		}
%>
															</td>
														</tr>
<%
	}
	if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){//If when deploying in Mexico
%>
														<tr>
															<TD class="main">
																<B><%=Languages.getString("jsp.admin.customers.merchants_edit.businesstype",SessionData.getLanguage())%>:</B>
															</TD>
															<TD nowrap="nowrap" CLASS="main">
																<SELECT id="businessTypeId" name="businessTypeId">
<%
		Vector vecBusinessTypes = com.debisys.customers.Merchant.getBusinessTypes(customConfigType);
		Iterator itBusinessTypes = vecBusinessTypes.iterator();
		while (itBusinessTypes.hasNext()) {
			Vector vecTemp = null;
			vecTemp = (Vector)itBusinessTypes.next();
			String strBusinessTypeId = vecTemp.get(0).toString();
			String strBusinessTypeCode = vecTemp.get(1).toString();
			if ( (request.getParameter("businessTypeId") != null) && (strBusinessTypeId.equals(request.getParameter("businessTypeId"))) )
				out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\" SELECTED>" + strBusinessTypeCode + "</OPTION>");
			else if ( strBusinessTypeId.equals(Merchant.getBusinessTypeId()) )
				out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\" SELECTED>" + strBusinessTypeCode + "</OPTION>");
			else
				out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\">" + strBusinessTypeCode + "</OPTION>");
		}
%>
																</SELECT>
															</TD>
														</tr>
														<tr>
															<TD CLASS="main">
																<B><%=Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment",SessionData.getLanguage())%>:</B>
															</TD>
															<TD NOWRAP="nowrap" CLASS="main">
																<SELECT id="merchantSegmentId" NAME="merchantSegmentId">
<%
		Vector vecMerchantSegments = com.debisys.customers.Merchant.getMerchantSegments();
		Iterator itMerchantSegments = vecMerchantSegments.iterator();
		while (itMerchantSegments.hasNext()) {
			Vector vecTemp = null;
			vecTemp = (Vector)itMerchantSegments.next();
			String strMerchantSegmentId = vecTemp.get(0).toString();
			String strMerchantSegmentCode = vecTemp.get(1).toString();
			if ( (request.getParameter("merchantSegmentId") != null) && strMerchantSegmentId.equals(request.getParameter("merchantSegmentId")))
				out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
			else if ( strMerchantSegmentId.equals(Integer.toString(Merchant.getMerchantSegmentId())) )
				out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
			else
				out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
		}
%>
																</SELECT>
															</TD>
														</tr>
														<tr>
															<TD CLASS="main">
																<B><%=Languages.getString("jsp.admin.customers.merchants_edit.businesshours",SessionData.getLanguage())%>:</B>
															</TD>
															<TD NOWRAP="nowrap" CLASS="main">
																<INPUT TYPE="text" id="businessHours" name="businessHours"
																	VALUE="<%=Merchant.getBusinessHours()%>" SIZE="20"
																	MAXLENGTH="50">
															</TD>
														</tr>
														<%
	} //End of if when deploying in Mexico
%>
														<tr>
															<TD CLASS="main">
																<B><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZone",SessionData.getLanguage())%>:</B>
															</TD>
															<TD NOWRAP="nowrap" CLASS="main">
																<SELECT id="timeZoneId" NAME="timeZoneId">
<%
		Iterator<Vector<String>> itList = TimeZone.getTimeZoneList().iterator();
		while ( itList.hasNext() ){
			Vector vItem = itList.next();
			String sSelected = "";
			if (vItem.get(0).toString().equals(Integer.toString(Merchant.getTimeZoneId()))){
				sSelected = "SELECTED";
			}
%>
																	<OPTION VALUE="<%=vItem.get(0).toString()%>"
																		<%=sSelected%>><%=vItem.get(2).toString()%>
																		[<%=vItem.get(1).toString()%>]
																	</OPTION>
																	<%
			vItem.clear();
			vItem = null;
		}
		itList = null;
%>
																</SELECT>
															</TD>
														</tr>
														<tr>
															<TD CLASS="main">&nbsp;</TD>
															<TD CLASS="main">
																<FONT COLOR="red"><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZoneWarning",SessionData.getLanguage())%></FONT>
															</TD>
														</tr>
<%
		// DBSY-931 System 2000 Tools s2k
		if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
				&&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
				&& strAccessLevel.equals(DebisysConstants.ISO)
				&& SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
		) { //If when deploying in International and user is an ISO level
			String strRefId = SessionData.getProperty("ref_id");
%>
														<tr>
															<TD CLASS="main">
																<B><%= Languages.getString("jsp.admin.tools.s2k.addedit.terms",SessionData.getLanguage())%>:</B>
															</TD>
 															<TD NOWRAP="nowrap" CLASS="main">
 																<INPUT  TYPE="text" id="terms" name="terms" VALUE="<%=Merchant.getterms()%>" SIZE="6" MAXLENGTH="4">
															</TD>
														</tr>
														<tr>
															<TD CLASS="main">
																<B><%= Languages.getString("jsp.admin.customers.salesmanid",SessionData.getLanguage())%>:</B>
															</TD>
															<TD NOWRAP="nowrap" CLASS="main">
<%
			Vector salesman = new s2k().gets2ksalesman(SessionData.getProperty("iso_id"));
%>
																<select name="salesmanid" id ="salesmanid" onchange="loadid('<%=strRefId%>');" >
<%
			out.println("<OPTION VALUE=\"\" >" + "</OPTION>");
			for ( int v=0 ; v<salesman.size();v++){
				String temp = ((Vector)salesman.get(v)).get(0).toString();
				out.println("<OPTION VALUE=\"" + temp );
				if(Merchant.getsalesmanid().equals(temp))
					out.println("\" selected >" +  temp  + "</OPTION>");
				else if(temp.equals(request.getParameter("salesmanid")))
					out.println("\" selected >" +  temp  + "</OPTION>");
				else
					out.println("\"  >" +  temp  + "</OPTION>");
			}
%>
																</select>
																<b><%= Languages.getString("jsp.admin.customers.salesmanname",SessionData.getLanguage())%>:</b>
																<select name="salesmanname"  id="salesmanname"  onchange="loadname('<%=strRefId%>');" >
<%
			out.println("<OPTION VALUE=\"\" >" + "</OPTION>");
			for ( int v=0 ; v<salesman.size();v++){
				String temp = ((Vector)salesman.get(v)).get(1).toString();
				out.println("<OPTION VALUE=\"" + temp );
				if(Merchant.getsalesmanname().equals(temp))
					out.println("\" selected >" +  temp  + "</OPTION>");
				else if(temp.equals(request.getParameter("salesmanname")))
					out.println("\" selected >" +  temp  + "</OPTION>");
				else
					out.println("\"  >" +  temp  + "</OPTION>");
			}
%>
																</select>
															</td>
														</tr>
<%
		}
		if (hasPermissionManage_New_Rateplans){
			String labelUseRatePlanModel="";
			String typeRatePlanModel="";
			if (Merchant.isUseRatePlanModel()){
				labelUseRatePlanModel = Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelISO",SessionData.getLanguage());
				typeRatePlanModel="1";
			}else{
				labelUseRatePlanModel = Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelEmida",SessionData.getLanguage());
				typeRatePlanModel="0";
			}
%>
														<tr>
<%
			String additionalMessage="";
			if (Merchant.getWarningChangeModel()!=null && Merchant.getWarningChangeModel().length()>0 ){
				additionalMessage = Merchant.getWarningChangeModel()+"<br/>";
			}
%>
															<TD CLASS="main">
																<B id="labelUseRatePlanModel" ><%=labelUseRatePlanModel %>:</B>
																<input type="hidden" id="typeRatePlanModel" value="<%=typeRatePlanModel%>" >
															</TD>
															<TD CLASS="main" align="left">
																<table CLASS="main">
																	<tr>
																		<td>
<%
			if(!Pincache.hasPinesAssigned(Merchant.getMerchantId())){
%>
																			<a href="" onClick="showChangeRatePlanWindow('<%=Merchant.getMerchantId()%>','<%=Merchant.getRepId()%>');return false">
													 							<%=Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModel",SessionData.getLanguage())%>
																			</a>
<% 
			}
%>
																		</td>
																		<td><%=additionalMessage%></td>
																	</tr>
																</table>
															</TD>
														</tr>
<%
		}

		// DBSY-1072 eAccounts Interface
		if(SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
%>
														<tr>
															<td class="main">
																<b><%= Languages.getString("jsp.admin.customers.entity_account_type",SessionData.getLanguage())%>:</b>
															</td>
<%
			// Determine if the parent account is External/Internal
			// If External, child should be External and no changes are allowed without first changing the parent
			boolean changeAllowed = EntityAccountTypes.SubsidiaryCanChange(Merchant.getRepId());
			int acctType = Merchant.getEntityAccountType();

			if(changeAllowed)
			{
				Hashtable<Integer, String> ht_accountTypes = EntityAccountTypes.getAccountTypes();
%>
															<td>
																<select id="entityAccountType" name="entityAccountType">
<%
				int old_type = -1;
				for (Enumeration<Integer> e = ht_accountTypes.keys(); e.hasMoreElements();) {
					int type_id = e.nextElement();
					if(type_id == acctType) {
						old_type = type_id;
						out.println("<option value=\"" + type_id + "\" selected>" + ht_accountTypes.get(type_id) + "</option>");
					} else {
			    		out.println("<option value=\"" + type_id + "\">" + ht_accountTypes.get(type_id) + "</option>");
					}
				}
%>
																</select>
																<input type="hidden" id="disabledentityAccountType" name="disabledentityAccountType" value="" />
															</td>
<%
			} else {
%>

															<td>
														<select id="entityAccountType" name="entityAccountType" disabled=true >
<%
				Hashtable<Integer, String> ht_accountTypes = EntityAccountTypes.getAccountTypes();
				int old_type = -1;
				for (Enumeration<Integer> e = ht_accountTypes.keys(); e.hasMoreElements();) {
					int type_id = e.nextElement();
					if(type_id == acctType) {
						old_type = type_id;
						out.println("<option value=\"" + type_id + "\" selected>" + ht_accountTypes.get(type_id) + "</option>");
					} else {
			    		out.println("<option value=\"" + type_id + "\">" + ht_accountTypes.get(type_id) + "</option>");
					}
				}
%>
																</select>
																<input type="hidden" id="disabledentityAccountType" name="disabledentityAccountType" value="1" />
															</td>
<%
			}
		}
			if(bExternalRep && !strAccessLevel.equals(DebisysConstants.REP)) {
%>
														</tr>
														<tr>
															<td class="main">
																<b><%= Languages.getString("jsp.admin.customers.allowExternalReps",SessionData.getLanguage())%>:</b>
															</td>
															<td>
																<input type="checkbox" name="allowExternalReps" value="Yes" <%=(Merchant.getMerchantPermissions() == 1)?"checked":""%> /> <br />
															</td>
														</tr>
<%
			}

%>
<%  if(bMerchantAssignment && ( strAccessLevel.equals(DebisysConstants.ISO) ||  strAccessLevel.equals(DebisysConstants.AGENT))){ %>
												<tr>
													<td class="main">
														<b><%= Languages.getString("jsp.admin.customers.allowMerchantAssignment",SessionData.getLanguage())%>:</b>
													</td>
													<td>
													<input type="checkbox" name="allowMerchantAssignment" value="Yes" <%=(Merchant.getMerchantExternalOutsideHierarchy() == 1)?"checked":""%> /> <br />

													</td>
								</tr>

<%}%>
<script type="text/javascript">
function addToParent(repId, repName, bIsShared, bIsUnlimited,CanChangeAccountType)
{
  	document.formMerchant.repId.value = repId;
	document.formMerchant.repName.value = repName;
  <%
   // DBSY-1072 eAccounts Interface
   if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)){
%>

										var combo = document.getElementById('entityAccountType');
										if(CanChangeAccountType!=null){
											if(!CanChangeAccountType){
											      combo.disabled = true;
											      combo.options.selectedIndex ="0";
											      var temp = document.getElementById('disabledentityAccountType');
											      temp.value = "1";
											}else
												{
												combo.disabled = false;
												combo.options.selectedIndex ="0";
												var temp = document.getElementById('disabledentityAccountType');
											    temp.value = "";
											}

										}else
										{
											combo.disabled = false;
											combo.options.selectedIndex ="0";
											var temp = document.getElementById('disabledentityAccountType');
											temp.value = "";
										}

							<%}%>
}
</script>

													</table>
												</td>
											</tr>

<%
 if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ) { //If when deploying in Mexico
 
   %>
   		<tr>
			<td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add.Invoice_title",SessionData.getLanguage())%></td>
		</tr>
   <% 
 	String merchantId = Merchant.getMerchantId();
 	if ( merchantId != null && merchantId != "" ){
		%>
   		<tr>
			<td>
				<jsp:include page="merchants/invoicingupdate.jsp">
					<jsp:param value="<%=customConfigType%>" name="customConfigType"/>					
				</jsp:include>
			</td>
		</tr>
       <% 
	}
	else
	{
		%>
   		<tr>
			<td>
				<jsp:include page="merchants/invoicing.jsp">
					<jsp:param value="<%=customConfigType%>" name="customConfigType"/>					
				</jsp:include>
			</td>
		</tr>
   <% 
	}
%>
		
		
											
<%}%>											
											
											
											
											
											
											
											
											
											
											<tr>
												<td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_edit.address",SessionData.getLanguage())%></td>
											</tr>
											<tr>
												<td class="formArea2">
													<table width="100">
														<tr class="main">
															<td><b><%=Languages.getString("jsp.admin.customers.merchants_edit.street_address",SessionData.getLanguage())%>:</b></td>
<%
		if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
															<TD COLSPAN="4" nowrap="nowrap">
																<TEXTAREA NAME="physAddress" id="physAddress" ROWS="2" COLS="50"
																	ONKEYUP="if (this.value.length > 50) { this.value = this.value.substring(0, 50); }"
																	ONBLUR="ValidateTextAreaLength(this, 50);"><%=Merchant.getPhysAddress()%></TEXTAREA>
<%
			if (merchantErrors != null && merchantErrors.containsKey("physAddress")) out.print("<font color=ff0000>*</font>");
%>
															</TD>
															<%
		}else{//Else if when deploying in others
%>
															<td nowrap="nowrap">
																<input type="text" id="physAddress" name="physAddress"
																	value="<%=Merchant.getPhysAddress()%>" size="20"
																	maxlength="50">
<%
			if (merchantErrors != null && merchantErrors.containsKey("physAddress")) out.print("<font color=ff0000>*</font>");
%>
															</td>
<%
		}
		if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
														</tr>
														<TR CLASS="main">
															<TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage())%>:
															</TD>
															<TD nowrap="nowrap">
																<INPUT TYPE="text" id="physColony" NAME="physColony" VALUE="<%=Merchant.getPhysColony()%>" SIZE="20" MAXLENGTH="50">
															</TD>
															<TD>&nbsp;</TD>
															<TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage())%>:
															</TD>
															<TD nowrap="nowrap">
																<INPUT TYPE="text" id="physDelegation" NAME="physDelegation" VALUE="<%=Merchant.getPhysDelegation()%>" SIZE="20" MAXLENGTH="50">
															</TD>
<%
		} //End of if when deploying in Mexico
%>
														</tr>
<%
		if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
														<TR CLASS="main">
															<TD nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())%>:</TD>
															<TD nowrap="nowrap">
																<INPUT TYPE="text" id="physCity" NAME="physCity" VALUE="<%=Merchant.getPhysCity()%>" SIZE="20" MAXLENGTH="50">
<%
			if (merchantErrors != null && merchantErrors.containsKey("physCity")) out.print("<font color=ff0000>*</font>");
%>
															</TD>
															<TD>&nbsp;</TD>
															<TD nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage())%></TD>
															<TD nowrap="nowrap">
																<INPUT TYPE="text" id="physZip" NAME="physZip" VALUE="<%=Merchant.getPhysZip()%>" SIZE="20" MAXLENGTH="5" ONBLUR="ValidateRegExp(this, '[0-9]{5}');">
<%
			if (merchantErrors != null && merchantErrors.containsKey("physZip")) out.print("<font color=ff0000>*</font>");
%>
															</TD>
															<TD>&nbsp;</TD>
															<TD nowrap="nowrap">
																<B><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())%></B>
															</TD>
															<TD nowrap="nowrap">
																<SELECT id="physState" NAME="physState">
<%
			Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
			Iterator itStates = vecStates.iterator();
			while (itStates.hasNext()) {
				Vector vecTemp = null;
				vecTemp = (Vector)itStates.next();
				String strStateId = vecTemp.get(0).toString();
				String strStateName = vecTemp.get(1).toString();
				if ( (request.getParameter("physState") != null) && (strStateId.equals(request.getParameter("physState"))) )
					out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
				else if ( strStateId.equals(Merchant.getPhysState()) )
					out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
				else
					out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
			}
%>
																</SELECT>
															</TD>
														</tr>
<%//End of if when deploying in Mexico
		}else{ //Else other deployments
%>
														<tr class="main">
															<td nowrap="nowrap">
<%
			if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
				out.println("<b>"+Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())+":</b>");
			}else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
				out.println(Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())+":");
			}
%>
															</td>
															<td nowrap="nowrap">
                        <%   
                        
                        if (!isInternational){
                        %>
                            <input type="text" id="physCity" name="physCity" value="<%=Merchant.getPhysCity()%>" size="15" maxlength="50">
                        <%
                            } else{
                                StringBuilder optCities = new StringBuilder();
                                optCities.append("<option>"+Merchant.getPhysCity()+"</option>");
                        %>
                            <select id="physCityI" name="physCityI" onchange="updatePhysCity(false);" >
                                <%=optCities.toString()%>
                            </select>
                            <input type="hidden" id="physCity" name="physCity" value="<%=Merchant.getPhysCity()%>" >
                            
                            <script language="JavaScript" type="text/javascript" charset="utf-8">
                                function updatePhysCity(flagControl){
                                    if (flagControl){
                                        $("#physCityI option").each(function(i){
                                            //alert($(this).text() + " : " + $(this).val());                                        
                                            if ($(this).text() === $("#physCity").val()){
                                                $(this).prop("selected", true);
                                                $("#physCity").val($(this).text());
                                            }
                                        });
                                    } else{                                     
                                        var cityName = $( "#physCityI option:selected" ).val();                                    
                                        $("#physCity").val(cityName);
                                    }                                    
                                }
                            </script>
                        <%
                        }
                        %>
                        
<%
			if (merchantErrors != null && merchantErrors.containsKey("physCity")) out.print("<font color=ff0000>*</font>");
			if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
				out.println("<b>"+Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())+":</b>");
			}else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
				out.println(Languages.getString("jsp.admin.customers.merchants_edit.state_international",SessionData.getLanguage())+":");
			}
%>
																<input type="text" id="physState" name="physState" value="<%=Merchant.getPhysState()%>" size="3" maxlength="2">
<%
			if (merchantErrors != null && merchantErrors.containsKey("physState")) out.print("<font color=ff0000>*</font>");
			if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
				out.println("<b>"+Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic",SessionData.getLanguage())+":</b>");
			}else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
				out.println(Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage()) + ":");
			}
%>
																<input type="text" id="physZip" name="physZip" value="<%=Merchant.getPhysZip()%>" size="10" maxlength="10">
<%
			if (merchantErrors != null && merchantErrors.containsKey("physZip")) out.print("<font color=ff0000>*</font>");
%>
															</td>
														</tr>
<%
		} //End of else other deployments
%>
														<tr class="main">
															<td><%=Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage())%>:
															</td>
                <td nowrap="nowrap">
                    
                    <%   
                        
                        if (!isInternational){
                        %>
                            <input type="text" id="physCounty" name="physCounty" value="<%=Merchant.getPhysCounty()%>" size="20" maxlength="20">
                        <%
                            } else{
                                
                                StringBuilder optCounties = new StringBuilder();
                                String tmpPhysCounty = "";
                                if (Merchant.getPhysCounty()!=null && Merchant.getPhysCounty().length()>0){
                                    tmpPhysCounty = Merchant.getPhysCounty();                                    
                                }
                                boolean ghostCounty = false;
                                for(District district : districts){
                                    if (district.getName().equalsIgnoreCase(tmpPhysCounty)){
                                        ghostCounty = true;
                                        optCounties.append("<option selected value='"+district.getName()+"' >"+district.getName()+"</option>");
                                    } else{
                                        optCounties.append("<option value='"+district.getName()+"' >"+district.getName()+"</option>");
                                    }
                                }
                                if (!ghostCounty && tmpPhysCounty.length()>0){
                                    optCounties.append("<option selected value='"+tmpPhysCounty+"' >"+tmpPhysCounty+"</option>");
                                }
                                List<City> cities = City.findAllCities(isoID);
                                StringBuilder tmpCities = new StringBuilder();
                                tmpCities.append("let citiesArray = [");
                                for(City city : cities){
                                    String ci = "{ name: '"+city.getName()+"',  districtName: '"+city.getDistrictName()+"' }";
                                    tmpCities.append(ci+ ",");
                                }
                                String cc = tmpCities.toString().substring(0, tmpCities.toString().length()-1);
                                cc = cc + "];";
                                
                        %>
                            <select id="physCountyI" name="physCountyI" onchange="populateCities();"  >
                                <%=optCounties.toString()%>
                            </select>
                            <input type="hidden" id="physCounty" name="physCounty" value="<%=tmpPhysCounty%>" >
                            
                            <script language="JavaScript" type="text/javascript" charset="utf-8">
                                <%=cc%>
                                function populateCities(){                                    
                                    var districtName = $( "#physCountyI option:selected" ).val();    
                                    $("#physCounty").val(districtName);
                                    var selectedCities = citiesArray.filter(x => x.districtName === districtName);
                                    var options = $("#physCityI");
                                    options.empty();                                    
                                    $.each(selectedCities, function(i, item) {                                        
                                        options.append($("<option  />").val(item.name).text(item.name));
                                    });   
                                    var physCityCurrent = $("#physCity").val();
                                    if (physCityCurrent!==null && physCityCurrent.length>0){
                                        options.append($("<option  />").val(physCityCurrent).text(physCityCurrent));
                                    }
                                }
                                $(document).ready(function() {
                                    populateCities();                                    
                                    updatePhysCity(true);
                                });
                            </script>    
                            
                            
                        <%
                        }
                        %>
                        
                        
                        <%if (merchantErrors != null && merchantErrors.containsKey("physCounty")) out.print("<font color=ff0000>*</font>");%>
                </td>
														</tr>
														<tr class="main">
															<td>
<%
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
			out.println("<b>");
		}
%>
																<%=Languages.getString("jsp.admin.customers.merchants_edit.country",SessionData.getLanguage())%>:
															</td>
															<td nowrap="nowrap">
															<!--input type="hidden" id="physCountry" name="physCountry" value="<%=Merchant.getPhysCountry()%>"-->
<%
		String[][] countries = CountryInfo.getAllCountries(CountryInfo.getIso(SessionData.getProperty("ref_id"),SessionData.getProperty("ref_type")));
		if(countries[0][0].length() > 0 ){
%>
																<select id="physCountry" name="physCountry">
<%
			if(countries.length == 1){
				out.println("<OPTION VALUE=\"" + countries[0][0].toString() + "\" SELECTED>" + countries[0][1].toString()  + "</OPTION>");
			}else{
				out.println("<OPTION VALUE=\"\">" + "" + "</OPTION>");
				for ( int v=0 ; v<countries.length;v++){
					if ( countries[v][0].toString().equals(Merchant.getPhysCountry().toString()) )
						out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\" SELECTED>" + countries[v][1].toString()  + "</OPTION>");
					else
						out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\">" + countries[v][1].toString()  + "</OPTION>");
				}
			}
 %>
																</select>
<%
		}else{
			out.print("<font color=ff0000>Database Error Country must be set for ISO</font>");
		}
		if (merchantErrors != null && merchantErrors.containsKey("physCountry")){
			out.print("<font color=ff0000>*</font>");
		}
%>
															</td>
														</tr>
<%
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
			out.println("<tr class=\"main\">");
			out.println("<td>" + Languages.getString("jsp.admin.customers.merchants_edit.route",SessionData.getLanguage()) + ":</td>");
			out.println("<td nowrap=\"nowrap\">");
			out.println("<SELECT ID=\"merchantRouteId\" NAME=\"merchantRouteId\">");
			out.println("<OPTION VALUE=\"-1\"> </OPTION>");
			Vector vecMerchantRoutes = Merchant.getRoutes();
			Iterator itMerchantRoutes = vecMerchantRoutes.iterator();
			while (itMerchantRoutes.hasNext()) {
				Vector vecTemp = null;
				vecTemp = (Vector)itMerchantRoutes.next();
				String strMerchantRouteId = vecTemp.get(0).toString();
				String strMerchantRouteName = vecTemp.get(1).toString().trim();
				if (strMerchantRouteName.equals(Merchant.getMerchantRouteName()))
					out.println("<OPTION VALUE=\"" + strMerchantRouteId + "\" SELECTED>" + strMerchantRouteName + "</OPTION>");
				else
					out.println("<OPTION VALUE=\"" + strMerchantRouteId + "\">" + strMerchantRouteName + "</OPTION>");
			}
			out.println("</SELECT>");
			out.println("</td>");
			out.println("</tr>");
		}

		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && hasPermissionGeolocation){
		%>
			<tr class="main">
				<td><%=Languages.getString("jsp.admin.customers.merchants_edit.latitude",SessionData.getLanguage())%>:</td>
				<td><input name="latitude" type="hidden" value="<%=Merchant.getLatitude()%>"><span name="latvalue"><%=Merchant.getLatitude()%></span></td>
			</tr>
			<tr class="main">
				<td><%=Languages.getString("jsp.admin.customers.merchants_edit.longitude",SessionData.getLanguage())%>:</td>
				<td><input name="longitude" type="hidden" value="<%=Merchant.getLongitude()%>"><span name="longvalue"><%=Merchant.getLongitude()%></span></td>
			</tr>
		<%
		}
		else{
		%>
			<tr>
				<td>
					<input id="latitude" name="latitude"  type="hidden" value="<%=Merchant.getLatitude()%>">
					<input id="longitude" name="longitude" type="hidden" value="<%=Merchant.getLongitude()%>">
				</td>
			</tr>
		<%
		}
		%>
		
                 
                <%                
                if ( permLocalizeMerchant )
                {          
                    HashMap parametersName = new HashMap();
                    String sessionLanguage = sessionUser.getUser().getLanguageCode();
                    String viewMerchantMapping = "viewMerchantMapping";
                    ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport(ResourceReport.RESOURCE_MERCHANT_MAP_LOCATOR, sessionLanguage, viewMerchantMapping);
                    parametersName.put(viewMerchantMapping, "View Merchant Mapping!!");
                    for( ResourceReport resource : resources){
                        if ( parametersName.containsKey( resource.getKeyMessage() ) ){
                            parametersName.put( resource.getKeyMessage(), resource.getMessage());            
                        }
                    }
                %>
                
                <tr align="center" >
                    <td colspan="2" align="left" style="width: 250px">
                        <input id="btnViewMerchantMapping" type="button" onclick="goMapAdd();" value="<%=parametersName.get(viewMerchantMapping)%>">                                                
                    </td>
                </tr>                             
                
                <%
                }
                %>
                
                
                
                                                                           </table>
												</td>
											</tr>
<%
		if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>

											<tr>
												<TD CLASS="formAreaTitle2">
													<BR>
													<%=Languages.getString("jsp.admin.customers.merchants_edit.billingaddress",SessionData.getLanguage())%>&nbsp;
													<BR>
												</TD>
											</tr>
											<tr>
												<TD CLASS="main">

													<b><INPUT TYPE="checkbox" ID="chkBillingAddress"
															NAME="chkBillingAddress"
															<%=((request.getParameter("chkBillingAddress") != null)?"CHECKED":"")%>
															ONCLICK="EnableBillingAddress(this.checked);">
														<LABEL FOR="chkBillingAddress"><%=Languages.getString("jsp.admin.customers.merchants_edit.billingaddresstitle",SessionData.getLanguage())%></LABEL></b>
												</TD>
											</tr>
											<tr>
												<td nowrap="nowrap" class="main">
<%
			if ( Merchant.getPrintBillingAddress() ){
%>
													<b><input type="checkbox" name="printBillingAddress" checked="checked"></b>
<%
			}else{
%>
													<b><input type="checkbox" name="printBillingAddress">
<%
			}
%>
													<%=Languages.getString("jsp.admin.customers.merchants_add.printBillingAddress",SessionData.getLanguage())%></b>
												</td>
											</tr>
											<tr>
												<td class="formArea2">
													<table width="100">
														<tr class="main">
															<td>
																<b><%=Languages.getString("jsp.admin.customers.merchants_edit.street_address",SessionData.getLanguage())%>:</b>
															</td>
															<TD COLSPAN="4" nowrap="nowrap">
																<TEXTAREA ID="mailAddress" NAME="mailAddress" ROWS="2"
																	COLS="50"
																	ONKEYUP="if (this.value.length > 50) { this.value = this.value.substring(0, 50); }"
																	ONBLUR="ValidateTextAreaLength(this, 50);"><%=Merchant.getMailAddress()%></TEXTAREA>
																<%if (merchantErrors != null && merchantErrors.containsKey("billingAddress")) out.print("<font color=ff0000>*</font>");%>
															</TD>
														</tr>
														<TR CLASS="main">
															<TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage())%>:
															</TD>
															<TD nowrap="nowrap">
																<INPUT TYPE="text" ID="mailColony" NAME="mailColony"
																	VALUE="<%=Merchant.getMailColony()%>" SIZE="20"
																	MAXLENGTH="50">
															</TD>
															<TD>
																&nbsp;
															</TD>
															<TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage())%>:
															</TD>
															<TD nowrap="nowrap">
																<INPUT TYPE="text" ID="mailDelegation"
																	NAME="mailDelegation"
																	VALUE="<%=Merchant.getMailDelegation()%>" SIZE="20"
																	MAXLENGTH="50">
															</TD>
														</tr>
														<TR CLASS="main">
															<TD nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())%>:
															</TD>
															<TD nowrap="nowrap">
																<INPUT TYPE="text" ID="mailCity" NAME="mailCity"
																	VALUE="<%=Merchant.getMailCity()%>" SIZE="20"
																	MAXLENGTH="30">
																<%if (merchantErrors != null && merchantErrors.containsKey("billingCity")) out.print("<font color=ff0000>*</font>");%>
															</TD>
															<TD>
																&nbsp;
															</TD>
															<TD nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage())%>:
															</TD>
															<TD nowrap="nowrap">
																<INPUT TYPE="text" ID="mailZip" NAME="mailZip"
																	VALUE="<%=Merchant.getMailZip()%>" SIZE="20"
																	MAXLENGTH="5"
																	ONBLUR="ValidateRegExp(this, '[0-9]{5}');">
																<%if (merchantErrors != null && merchantErrors.containsKey("billingZip")) out.print("<font color=ff0000>*</font>");%>
															</TD>
															<TD>
																&nbsp;
															</TD>
															<TD nowrap="nowrap">
																<B><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())%></B>
															</TD>
															<TD nowrap="nowrap">
																<SELECT ID="mailState" NAME="mailState">
<%
			Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
			Iterator itStates = vecStates.iterator();
			while (itStates.hasNext()) {
				Vector vecTemp = null;
				vecTemp = (Vector)itStates.next();
				String strStateId = vecTemp.get(0).toString();
				String strStateName = vecTemp.get(1).toString();
				if ( (request.getParameter("mailState") != null) && (strStateId.equals(request.getParameter("mailState"))) )
					out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
				else if ( strStateId.equals(Merchant.getMailState()) )
					out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
				else
					out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
			}
%>
																</SELECT>
															</TD>
														</tr>
														<tr class="main">
															<td><%=Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage())%>:
															</td>
															<td nowrap="nowrap">
																<input type="text" id="mailCounty" name="mailCounty"
																	value="<%=Merchant.getMailCounty()%>" size="20"
																	maxlength="20">
																<%if (merchantErrors != null && merchantErrors.containsKey("billingCounty")) out.print("<font color=ff0000>*</font>");%>
															</td>
														</tr>
														<tr class="main">
															<td>
																<B><%=Languages.getString("jsp.admin.customers.merchants_edit.country",SessionData.getLanguage())%>:</B>
															</td>
															<td nowrap="nowrap">
<%
			if(countries[0][0].length() > 0 ){
%>

																<select id="mailCountry" name="mailCountry">
<%
				if(countries.length == 1){
					out.println("<OPTION VALUE=\"" + countries[0][0].toString() + "\" SELECTED>" + countries[0][1].toString()  + "</OPTION>");
				}else{
					out.println("<OPTION VALUE=\"\">" + "" + "</OPTION>");
					for ( int v=0 ; v<countries.length;v++){
						if ( countries[v][0].toString().equals(Merchant.getMailCountry().toString()) )
							out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\" SELECTED>" + countries[v][1].toString()  + "</OPTION>");
						else
							out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\">" + countries[v][1].toString()  + "</OPTION>");
					}
				}
 %>
																</select>
<%
			}else{
				out.print("<font color=ff0000>Database Error Country must be set for ISO</font>");
			}
			if (merchantErrors != null && merchantErrors.containsKey("billingCountry")){
				out.print("<font color=ff0000>*</font>");
			}
%>
															</td>
														</tr>
													</table>
												</td>
											</tr>
<%
			if ( request.getParameter("chkBillingAddress") != null ) out.print("<SCRIPT>EnableBillingAddress(true);</SCRIPT>");
			//else if ( request.getParameter("submitted") != null )
			//out.print("<SCRIPT>EnableBillingAddress(false);</SCRIPT>");
		} //End of if when deploying in Mexico
%>
											<tr>
												<td class="formAreaTitle2">
													<br><%=Languages.getString("jsp.admin.customers.merchants_edit.contact_information",SessionData.getLanguage())%>
												</td>
											</tr>
											<%
		if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){//If when deploying in Mexico
%>
											<tr>
												<TD CLASS="formArea2">
													<INPUT TYPE="hidden" ID="mxContactAction"
														NAME="mxContactAction" VALUE="">
													<TABLE WIDTH="100%" CELLSPACING="1" CELLPADDING="1"
														BORDER="0">
														<tr>
															<TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contacttype",SessionData.getLanguage()).toUpperCase()%></TD>
															<TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contact",SessionData.getLanguage()).toUpperCase()%></TD>
															<TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.phone",SessionData.getLanguage()).toUpperCase()%></TD>
															<TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.fax",SessionData.getLanguage()).toUpperCase()%></TD>
															<TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.email",SessionData.getLanguage()).toUpperCase()%></TD>
															<TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.cellphone",SessionData.getLanguage()).toUpperCase()%></TD>
															<TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.department",SessionData.getLanguage()).toUpperCase()%></TD>
															<TD CLASS="rowhead2"></TD>
														</tr>
														<%
			if ( Merchant.getVecContacts().size() > 0 ){//If there are contacts to show
%>
														<tr>
															<TD>
																<INPUT TYPE="hidden" ID="mxContactIndex"
																	NAME="mxContactIndex" VALUE="">
																<DIV STYLE="display: none;">
																	<INPUT TYPE="text" id="contactData" name="contactData"
																		VALUE="<%=Merchant.getVecContacts().size()%>">
																</DIV>
															</TD>
														</tr>
<%
				int intEvenOdd = 1;
				int contactIndex = 0;
				Vector vecContacts = Merchant.getVecContacts();
				Iterator itContacts = vecContacts.iterator();
				while (itContacts.hasNext()){
					Vector vecTemp = null;
					vecTemp = (Vector)itContacts.next();
%>
														<TR CLASS="row<%=intEvenOdd%>">
<%
					if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("EDIT")) && (request.getParameter("mxContactIndex") != null) && (contactIndex == Integer.parseInt(request.getParameter("mxContactIndex"))) ){//If we are editing an item
%>
															<TD>
																<SCRIPT>document.getElementById("mxContactIndex").value = "<%=contactIndex%>";</SCRIPT>
																<SELECT NAME="contactTypeId">
<%
						Vector vecContactTypes = com.debisys.customers.Merchant.getContactTypes();
						Iterator itContactTypes = vecContactTypes.iterator();
						while (itContactTypes.hasNext()){
							vecTemp = null;
							vecTemp = (Vector)itContactTypes.next();
							String strContactTypeId = vecTemp.get(0).toString();
							String strContactTypeCode = vecTemp.get(1).toString();
							if (strContactTypeId.equals(Integer.toString(Merchant.getContactTypeId()))){
								out.println("<OPTION VALUE=\"" + strContactTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
							}else{
								out.println("<OPTION VALUE=\"" + strContactTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
							}
						}
%>
																</SELECT>
															</TD>
															<TD>
																<INPUT TYPE="text" id="contactName" NAME="contactName"
																	VALUE="<%=Merchant.getContactName()%>" SIZE="15"
																	MAXLENGTH="50">
															</TD>
															<TD>
																<INPUT TYPE="text" id="contactPhone" NAME="contactPhone"
																	VALUE="<%=Merchant.getContactPhone()%>" SIZE="15"
																	MAXLENGTH="15">
															</TD>
															<TD>
																<INPUT TYPE="text" id="contactFax" NAME="contactFax"
																	VALUE="<%=Merchant.getContactFax()%>" SIZE="15"
																	MAXLENGTH="15">
															</TD>
															<TD>
																<INPUT TYPE="text" NAME="contactEmail"
																	VALUE="<%=Merchant.getContactEmail()%>" SIZE="15"
																	MAXLENGTH="200">
															</TD>
															<TD>
																<INPUT TYPE="text" NAME="contactCellPhone"
																	VALUE="<%=Merchant.getContactCellPhone()%>" SIZE="15"
																	MAXLENGTH="50">
															</TD>
															<TD>
																<INPUT TYPE="text" NAME="contactDepartment"
																	VALUE="<%=Merchant.getContactDepartment()%>" SIZE="15"
																	MAXLENGTH="50">
															</TD>
<%
					//End of if we are editing an item
					}else{//Else just show the item data
%>
															<TD><%=com.debisys.customers.Merchant.getContactTypeById(vecTemp.get(0).toString(),SessionData)%></TD>
															<TD><%=StringUtil.toString(vecTemp.get(1).toString())%></TD>
															<TD><%=StringUtil.toString(vecTemp.get(2).toString())%></TD>
															<TD><%=StringUtil.toString(vecTemp.get(3).toString())%></TD>
															<TD><%=StringUtil.toString(vecTemp.get(4).toString())%></TD>
															<TD><%=StringUtil.toString(vecTemp.get(5).toString())%></TD>
															<TD><%=StringUtil.toString(vecTemp.get(6).toString())%></TD>
															<TD>
																<TABLE>
																	<tr>
																		<TD>
																			<INPUT TYPE="image"
																				TITLE="<%=Languages.getString("jsp.admin.customers.merchants_edit.editcontact",SessionData.getLanguage())%>"
																				SRC="images/icon_edit.gif"
																				ONCLICK="document.getElementById('mxContactIndex').value = '<%=contactIndex%>';mxEditContact();">
																		</TD>
																		<TD>&nbsp;</TD>
																		<TD>
																			<INPUT TYPE="image"
																				TITLE="<%=Languages.getString("jsp.admin.customers.merchants_edit.deletecontact",SessionData.getLanguage())%>"
																				SRC="images/icon_delete.gif"
																				ONCLICK="document.getElementById('mxContactIndex').value = '<%=contactIndex%>';mxDeleteContact();">
																		</TD>
																	</tr>
																</TABLE>
															</TD>
<%
					}//End of else just show the item data
%>
														</tr>
<%
					intEvenOdd = ((intEvenOdd == 1)?2:1);
					contactIndex++;
				}
	}//End of if there are contacts to show
	else if ( (request.getParameter("mxContactAction") == null) || (request.getParameter("mxContactAction") != null) && !request.getParameter("mxContactAction").equals("ADD")){//Else show a message of no contacts
%>
														<tr>
															<TD CLASS="main" COLSPAN="8" ALIGN="center"><%=Languages.getString("jsp.admin.customers.merchants_edit.nocontacts",SessionData.getLanguage())%></TD>
														</tr>
														<tr>
															<TD CLASS="main" COLSPAN="8" ALIGN="center">
																<DIV STYLE="display: none;">
																	<INPUT TYPE="text" NAME="contactData" VALUE="">
																</DIV>
															</TD>
														</tr>
<%
	}//End of else show a message of no contacts
	if ( (request.getParameter("mxContactAction") == null) ||
			((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("SAVE"))) ||
			((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("CANCEL"))) ||
			((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("DELETE")))
			|| request.getParameter("mxContactAction").equals("--")
	){//If we need to show the ADD button
%>
														<tr>
															<TD CLASS="main" COLSPAN="8" ALIGN="right">
																<INPUT TYPE="button" ID="mxBtnAddContact"
																	VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.addcontact",SessionData.getLanguage())%>"
																	ONCLICK="mxAddContact();">
															</TD>
														</tr>
														<%
	}//End of if we need to show the ADD button
	else if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("ADD")) )
	{//Else if we need to show the row for a new Contact
%>
														<TR CLASS="main">
															<TD>
																<DIV STYLE="display: none;">
																	<INPUT TYPE="text" NAME="contactData"
																		VALUE="<%=((Merchant.getVecContacts().size() == 0)?"":"1")%>">
																</DIV>
																<SELECT NAME="contactTypeId">
<%
		Vector vecContactTypes = com.debisys.customers.Merchant.getContactTypes();
		Iterator itContactTypes = vecContactTypes.iterator();
		while (itContactTypes.hasNext()){
			Vector vecTemp = null;
			vecTemp = (Vector)itContactTypes.next();
			String strContactTypeId = vecTemp.get(0).toString();
			String strContactTypeCode = vecTemp.get(1).toString();
			if (strContactTypeId.equals(request.getParameter("contactTypeId"))){
				out.println("<OPTION VALUE=\"" + strContactTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
			}else{
				out.println("<OPTION VALUE=\"" + strContactTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
			}
		}
%>
																</SELECT>
															</TD>
															<TD>
																<INPUT TYPE="text" id="contactName" NAME="contactName"
																	VALUE="<%=Merchant.getContactName()%>" SIZE="15"
																	MAXLENGTH="50">
															</TD>
															<TD>
																<INPUT TYPE="text" id="contactPhone" NAME="contactPhone"
																	VALUE="<%=Merchant.getContactPhone()%>" SIZE="15"
																	MAXLENGTH="15">
															</TD>
															<TD>
																<INPUT TYPE="text" NAME="contactFax"
																	VALUE="<%=Merchant.getContactFax()%>" SIZE="15"
																	MAXLENGTH="15">
															</TD>
															<TD>
																<INPUT TYPE="text" NAME="contactEmail"
																	VALUE="<%=Merchant.getContactEmail()%>" SIZE="15"
																	MAXLENGTH="200">
															</TD>
															<TD>
																<INPUT TYPE="text" NAME="contactCellPhone"
																	VALUE="<%=Merchant.getContactCellPhone()%>" SIZE="15"
																	MAXLENGTH="50">
															</TD>
															<TD>
																<INPUT TYPE="text" NAME="contactDepartment"
																	VALUE="<%=Merchant.getContactDepartment()%>" SIZE="15"
																	MAXLENGTH="50">
															</TD>
														</tr>
<%
		}//End of else if we need to show the row for a new Contact
		if ( ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("ADD"))) ||
		((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("EDIT")))
		){//If we need to show the SAVE-CANCEL button
%>
														<tr>
															<TD CLASS="main" COLSPAN="8" ALIGN="right">
																<INPUT TYPE="button" ID="mxBtnSaveContact"
																	VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.savecontact",SessionData.getLanguage())%>"
																	ONCLICK="mxSaveContact('<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>','<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact",SessionData.getLanguage())%>','<%=Languages.getString("jsp.admin.customers.merchants_edit.error_phone",SessionData.getLanguage())%>');">
																<INPUT TYPE="button" ID="mxBtnCancelContact"
																	VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.cancelsavecontact",SessionData.getLanguage())%>"
																	ONCLICK="mxCancelSaveContact();">
															</TD>
														</tr>
<%
		}//End of if we need to show the SAVE-CANCEL button
%>
													</TABLE>
												</TD>
											</tr>
				     						<tr>
					        					<td class="formArea2">
					        						<table width="100">
                                                                                                    <tr class="main" >
                                                                                                        <td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td>
                                                                                                        <td><input type="text" name="smsNotificationPhone" id="smsNotificationPhone" value="<%=Merchant.getSmsNotificationPhone()%>" size="20" maxlength="15" ></td>
                                                                                                    </tr>
                                                                                                    <tr class="main" >
                                                                                                        <td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td>
                                                                                                        <td nowrap><input type="text" name="mailNotificationAddress" id="mailNotificationAddress" value="<%=Merchant.getMailNotificationAddress()%>" size="20" maxlength="50" ></td>
                                                                                                    </tr>												
                                                                                                    <tr class="main">
														<td class="main" nowrap>
                                                                                                                    <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" onClick="changePayNoti()" />
														</td>
													</tr>
													<tr class="main">
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr class="main">
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" onClick="changePayNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" onClick="changePayNotiMail()" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td class="main" nowrap>
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" onClick="changeBalNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" onClick="changeBalNotiValue()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" onClick="changeBalNotiDays()" />
														</td>
													</tr>													
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><input type="text" class="numeric" name="balanceNotificationValue" id="balanceNotificationValue" value="<%=Merchant.getBalanceNotificationValue()%>" size="20" maxlength="15" ></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><input type="text" class="numeric" name="balanceNotificationDays" id="balanceNotificationDays" value="<%=Merchant.getBalanceNotificationDays()%>" size="20" maxlength="50" ></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" onClick="changeBalNotiSMS()"/>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" onClick="changeBalNotiMail()"/>
														</td>
													</tr>
<%
   }
%>	
													</table>
													<script>chkPaymentNoti()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>chkBalanceNoti()</script>
<%
   }
%>														
												</td>
											</tr>
<%
	}else{//Else when deploying in others
%>
											<tr>
												<td class="formArea2">
													<table width="100">
														<tr class="main">
															<td nowrap="nowrap">
																<b><%=Languages.getString("jsp.admin.customers.merchants_edit.contact",SessionData.getLanguage())%>:</b>
															</td>
															<td nowrap="nowrap">
																<input type="text" id="contactName" name="contactName"
																	value="<%=Merchant.getContactName()%>" size="20"
																	maxlength="50">
																<%if (merchantErrors != null && merchantErrors.containsKey("contactName")) out.print("<font color=ff0000>*</font>");%>
															</td>
														</tr>
														<tr class="main">
															<td>
																<b><%=Languages.getString("jsp.admin.customers.merchants_edit.phone",SessionData.getLanguage())%>:</b>
															</td>
															<td nowrap="nowrap">
																<input type="text" id="contactPhone" name="contactPhone"
																	value="<%=Merchant.formatPhone(application, Merchant.getContactPhone())%>" size="15"
																	maxlength="15">
																<%if (merchantErrors != null && merchantErrors.containsKey("contactPhone")) out.print("<font color=ff0000>*</font>");%>
															</td>
														</tr>
														<tr class="main">
															<td><%=Languages.getString("jsp.admin.customers.merchants_edit.fax",SessionData.getLanguage())%>:
															</td>
															<td >
																<input type="text" id="contactFax" name="contactFax"
																	value="<%=Merchant.formatPhone(application, Merchant.getContactFax())%>" size="15"
																	maxlength="15">
																<%if (merchantErrors != null && merchantErrors.containsKey("contactFax")) out.print("<font color=ff0000>*</font>");%>
															</td>
														</tr>
														<tr class="main">
															<td><%=Languages.getString("jsp.admin.customers.merchants_edit.email",SessionData.getLanguage())%>:
															</td>
															<td >
																<input type="text" id="contactEmail" name="contactEmail"
																	value="<%=Merchant.getContactEmail()%>" size="20"
																	maxlength="200">
																<%if (merchantErrors != null && merchantErrors.containsKey("contactEmail")) out.print("<font color=ff0000>*</font>");%>
															</td>
														</tr>
													<tr class="main">
                                                                                                            <td  ><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td>
                                                                                                            <td nowrap><input type="text" name="smsNotificationPhone" id="smsNotificationPhone" value="<%=Merchant.getSmsNotificationPhone()%>" size="20" maxlength="15" ></td>
													</tr>
													<tr class="main">
                                                                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td>
                                                                                                            <td nowrap><input type="text" name="mailNotificationAddress" id="mailNotificationAddress" value="<%=Merchant.getMailNotificationAddress()%>" size="20" maxlength="50" ></td>
													</tr>												
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" onClick="changePayNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" onClick="changePayNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" onClick="changePayNotiMail()" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" onClick="changeBalNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" onClick="changeBalNotiValue()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" onClick="changeBalNotiDays()" />
														</td>
													</tr>													
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><input type="text" class="numeric" name="balanceNotificationValue" id="balanceNotificationValue" value="<%=Merchant.getBalanceNotificationValue()%>" size="20" maxlength="15" ></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><input type="text" class="numeric"  name="balanceNotificationDays" id="balanceNotificationDays" value="<%=Merchant.getBalanceNotificationDays()%>" size="20" maxlength="15" ></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" onClick="changeBalNotiSMS()"/>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" onClick="changeBalNotiMail()"/>
														</td>
													</tr>
<%
   }
%>
						            					</table>
						            					<script>chkPaymentNoti()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>chkBalanceNoti()</script>
<%
   }
%>															
												</td>
											</tr>
<%
	}//End of else when deploying in others

	if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES)){
%>
											<tr>
												<td class="formAreaTitle2">
													<br><%=Languages.getString("jsp.admin.customers.common_info_edit.payment_information",SessionData.getLanguage())%></td>
											</tr>
											<tr>
												<td class="formArea2">
													<table width="100">
														<tr class="main">
															<td nowrap="nowrap">
																<%=Languages.getString("jsp.admin.customers.common_info_edit.payment_type",SessionData.getLanguage())%>
															</td>
															<td nowrap="nowrap">
																<select name="paymentType">
<%
		Vector vecPaymentTypes = com.debisys.customers.Merchant.getPaymentTypes();
		Iterator itPaymentTypes = vecPaymentTypes.iterator();
		boolean flag_payment=false;
		while (itPaymentTypes.hasNext()) {
			Vector vecTemp = null;
			vecTemp = (Vector)itPaymentTypes.next();
			String strPaymentTypeId = vecTemp.get(0).toString();
			String strPaymentTypeCode = vecTemp.get(1).toString();
			if ( (request.getParameter("paymentType") != null) && (strPaymentTypeId.equals(request.getParameter("paymentType"))) ){
				flag_payment = true;
				out.println("<option value=\"" + strPaymentTypeId + "\" selected>" + strPaymentTypeCode + "</option>");
			}else if ( strPaymentTypeId.equals(Integer.toString(Merchant.getPaymentType())) ){
				flag_payment = true;
				out.println("<option value=\"" + strPaymentTypeId + "\" selected>" + strPaymentTypeCode + "</option>");
			}else
				out.println("<option value=\"" + strPaymentTypeId + "\">" + strPaymentTypeCode + "</option>");
		}
		if ( !flag_payment ){
%>
																	<option value="-1" selected="selected">--</option>
<%
		}
%>
																</select>
															</td>
															<td nowrap="nowrap">
<%
		if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
																<%=Languages.getString("jsp.admin.customers.common_info_edit.process_type",SessionData.getLanguage())%>
<%
		} else{
%>
																<%=Languages.getString("jsp.admin.customers.common_info_edit.process_type_international",SessionData.getLanguage())%>
<%
		}
%>
															</td>
															<td nowrap="nowrap">
																<select name="processType">
<%
		Vector vecProcessTypes = com.debisys.customers.Merchant.getProcessorTypes();
		Iterator itProcessTypes = vecProcessTypes.iterator();
		boolean flag_process=false;
		boolean isSelectProcess = false;
		while (itProcessTypes.hasNext()) {
			Vector vecTemp = null;
			vecTemp = (Vector)itProcessTypes.next();
			String strProcessTypeId = vecTemp.get(0).toString();
			String strProcessTypeCode = vecTemp.get(1).toString();
			if ( (request.getParameter("processType") != null) && (strProcessTypeId.equals(request.getParameter("processType"))) ){
				flag_process = true;
				out.println("<option value=\"" + strProcessTypeId + "\" selected>" + strProcessTypeCode + "</option>");
			}else if ( strProcessTypeId.equals(Integer.toString(Merchant.getProcessType())) ){
				flag_process = true;
				out.println("<option value=\"" + strProcessTypeId + "\" selected>" + strProcessTypeCode + "</option>");
			}else{
				String processorDefaultId = Merchant.getProcessDefaultValueId(application);
				if(processorDefaultId.trim().equals(strProcessTypeId)){
					isSelectProcess = true;
					out.println("<option value=\"" + strProcessTypeId + "\" selected>" + strProcessTypeCode + "</option>");
				}else{
					out.println("<option value=\"" + strProcessTypeId + "\">" + strProcessTypeCode + "</option>");
				}
			}
		}
	if ( !flag_process ){
%>
											<option value="-1" <%if(!isSelectProcess){%>selected="selected"<%}%>>--</option>
<%
	}
%>
																</select>
															</td>

														</tr>

													</table>
												</td>
											</tr>
<%
}
%>
											<tr>
												<td class="formAreaTitle2">
													<br><%=Languages.getString("jsp.admin.customers.merchants_edit.banking_information",SessionData.getLanguage())%></td>
											</tr>
											<tr>
												<td class="formArea2">
													<table width="100">
														<jsp:include page="merchants_test_account.jsp"/>
														<tr class="main">
															<TD nowrap="nowrap">
<%
if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
	out.print("<B>" + Languages.getString("jsp.admin.customers.merchants_edit.tax_id",SessionData.getLanguage()) + ":</B>");
%>
															</TD>
															<td nowrap="nowrap">
																<input type="text" id="taxId" name="taxId"
																	value="<%=Merchant.getTaxId()%>" size="15"
																	maxlength="15"
																	ONBLUR="ValidateRegExp(this, '[A-Z]{3,4}-[0-9]{6}-[A-Z0-9]{3}')">
<%
	if (merchantErrors != null && merchantErrors.containsKey("taxId")) out.print("<font color=ff0000>*</font>");
%>
																&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_edit.sample_taxid",SessionData.getLanguage())%>
															</td>
														</tr>
<%
}else{ //Else when deploying in others
%>
	<tr CLASS="main"><td>
<%
	boolean hasPermissionEditBankingInformation = SessionData.checkPermission(DebisysConstants.PERM_EDIT_BANKING_INFO_MERCHANT);
	//added by jay to make it compatible with domestic requirement where tax id is required.
	if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
		out.print("<b>");
	}
	out.print(Languages.getString("jsp.admin.customers.merchants_edit.tax_id_international",SessionData.getLanguage()) + ":");
%>
														</td>
														<td nowrap="nowrap">
															<input type="text" id="taxId" name="taxId"
<%
	if(Merchant.isAchAccount() || (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && !hasPermissionEditBankingInformation)){
		%> readonly="readonly"<%
	}
%>
																value="<%=Merchant.getTaxId()%>" size="15"
																maxlength="15">
<%
	if (merchantErrors != null && merchantErrors.containsKey("taxId")) out.print("<font color=ff0000>*</font>");
%>
														</td>
														</tr>
														<%
}
%>

														<%
if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
														<TR CLASS="main">
															<TD nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_edit.bankname",SessionData.getLanguage())%>:&nbsp;
															</TD>
															<TD>
																<INPUT TYPE="text" NAME="bankName"
																	VALUE="<%=Merchant.getBankName()%>" SIZE="15"
																	MAXLENGTH="50">
																<%if (merchantErrors != null && merchantErrors.containsKey("bankName")) out.print("<font color=ff0000>*</font>");%>
															</TD>
														</tr>
														<TR CLASS="main">
															<TD nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_edit.accounttype",SessionData.getLanguage())%>:&nbsp;
															</TD>
															<TD>
																<SELECT NAME="accountTypeId">
<%
	Vector vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
	Iterator itAccountTypes = vecAccountTypes.iterator();
	while (itAccountTypes.hasNext()) {
		Vector vecTemp = null;
		vecTemp = (Vector)itAccountTypes.next();
		String strAccountTypeId = vecTemp.get(0).toString();
		String strAccountTypeCode = vecTemp.get(1).toString();
		if ( (request.getParameter("accountTypeId") != null) && (strAccountTypeId.equals(request.getParameter("accountTypeId"))) )
			out.println("<OPTION VALUE=\"" + strAccountTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
		else if (strAccountTypeId.equals(Integer.toString(Merchant.getAccountTypeId())))
			out.println("<OPTION VALUE=\"" + strAccountTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
		else
			out.println("<OPTION VALUE=\"" + strAccountTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
	}
%>
																</SELECT>
															</TD>
														</tr>
														<%
} //End of if when deploying in Mexico
%>
														<tr class="main">
															<td nowrap="nowrap">
<%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
	out.println("<b>"+Languages.getString("jsp.admin.customers.merchants_edit.routing_number",SessionData.getLanguage())+":</b>");
}else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
	if ( (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) )
		out.println(Languages.getString("jsp.admin.customers.merchants_edit.routing_number",SessionData.getLanguage())+":");
	else
		out.println(Languages.getString("jsp.admin.customers.merchants_edit.routing_number_international",SessionData.getLanguage())+":");
}
%>:
															</td>
															<td nowrap="nowrap" >
<%
String readOnly="";
String regExpresion="";
String maxlength="";
String merchantCreditType="";
merchantCreditType=Merchant.getMerchantType();
boolean hasPermissionEditBankingInformation = SessionData.checkPermission(DebisysConstants.PERM_EDIT_BANKING_INFO_MERCHANT);
if(Merchant.isAchAccount() || (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && !hasPermissionEditBankingInformation)){
	readOnly="disabled='disabled'"; //"readonly='readonly'";
}
if ( (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) ){
	regExpresion="ValidateRegExp(this, '[0-9]{18}');";
	maxlength="18";
}else if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) ){
	//regExpresion="ValidateRegExp(this, '[0-9a-zA-Z]{9}');";
	regExpresion="/^[0-9]{9}$/";
	maxlength="9";
}
%>

<%
String textErrorAba = Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage());
if(readOnly.equals("")){
%>
																<input type="text" id="routingNumber" name="routingNumber" <%=readOnly%> value="<%=Merchant.getRoutingNumber()%>" size="15" maxlength="<%=maxlength%>"
<%
if ( (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) ){
%>
																	onblur="<%=regExpresion%>"
<%
}else{
	if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
%>
																	onKeyUp="verifyAba(9,'<%=regExpresion%>','<%=textErrorAba%>');"
<%
}
}
if(Merchant.getRoutingNumber().startsWith("NA")){
%>
																	disabled = "false";
<%
}
%>
																	>
																<%
																}
																else{
																	out.println(Merchant.getRoutingNumber());
																	%><input type="hidden" id="routingNumber" name="routingNumber" value="<%=Merchant.getRoutingNumber()%>"/> <%
																}
																if (merchantErrors != null && merchantErrors.containsKey("routingNumber")) out.print("<font color=ff0000>*</font>");%>
															</td>
<%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
%>
															<td nowrap>
																<%String textInfoAba = Languages.getString("jsp.admin.customers.field_no_applicable.info_field_no_applicable",SessionData.getLanguage()); %>
																<label>
																<%if(readOnly.equals("")){ %>
																<input type="checkbox" id="optionAba" name="optionAba" <%=readOnly%> value="optionAba"  onClick="changeCheckAba()"
<%
	if(Merchant.getRoutingNumber().startsWith("NA")){
%>
																	 checked="checked"
<%
	}
%>

<%
	if(merchantCreditType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT) || merchantCreditType.equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED)){
%>
																	 disabled="true"
<%
	}
%>
																><%=textInfoAba%><%} %>
																</label>
															</td>
															<td><div id="div_info_aba" name="div_info_aba" ></div></td>
<%
}
%>
														</tr>
														<tr class="main">
															<td>
<%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
	out.println("<b>"+Languages.getString("jsp.admin.customers.merchants_edit.account_number",SessionData.getLanguage())+":</b>");
}else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
	out.println(Languages.getString("jsp.admin.customers.merchants_edit.account_number",SessionData.getLanguage())+":");
}
%>:
															</td>
															<td nowrap="nowrap">
																<%String textErrorAccount = Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage()); %>
																<%if(readOnly.equals("")){ %>
																<input type="text" id="accountNumber" name="accountNumber" <%=readOnly%>
<%																%> value="<%=Merchant.getAccountNumber()%>" size="15" maxlength="20" <%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
																%> onKeyUp="verifyAccount(20,'<%=textErrorAccount%>');" <%
	if(Merchant.getAccountNumber().startsWith("NA")){
 																%> disabled = "false" <%
	}
        

	

}
%>
																	>
<%
}
else{
	out.println(Merchant.getAccountNumber());
	%><input type="hidden" id="accountNumber" name="accountNumber" value="<%=Merchant.getAccountNumber()%>"/> <%
}
if (merchantErrors != null && merchantErrors.containsKey("accountNumber")) out.print("<font color=ff0000>*</font>");
%>
															</td>
<%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
%>
															<td nowrap>
																<%String textInfoAccount = Languages.getString("jsp.admin.customers.field_no_applicable.info_field_no_applicable",SessionData.getLanguage()); %>
																<label>
																<%if(readOnly.equals("")){ %>
																<input type="checkbox" id="optionAccount" name="optionAccount" <%=readOnly%> value="optionAccount" onClick="changeCheckAccount()"
<%
	if(Merchant.getAccountNumber().startsWith("NA")){
%>
																checked="checked"
<%
	}
%>
<%
	if(merchantCreditType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT) || merchantCreditType.equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED)){
%>
																	 disabled="true"
<%
	}
%>
																><%=textInfoAccount%><%} %>
																</label>
															</td>
															
<%
}
%>
															<%
													if(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
													{
														if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_BANKING_ACCOUNT_TYPE_FOR_MERCHANT))
														{
															%>
															
																<tr CLASS="main">
																	<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_edit.accounttype",SessionData.getLanguage())%>:&nbsp;</td>
																	<td>
																		<select name="accountTypeId">
																		<%
																			Vector vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
																			Iterator itAccountTypes = vecAccountTypes.iterator();
																			boolean accountTypeSetup = false;
																			while (itAccountTypes.hasNext()) 
																			{
																				Vector vecTemp = null;
																				vecTemp = (Vector)itAccountTypes.next();
																				String strAccountTypeId = vecTemp.get(0).toString();
																				String strAccountTypeCode = vecTemp.get(1).toString();
																				if ( Merchant.getAccountTypeId() ==  Integer.parseInt(strAccountTypeId) )
																				{
																					out.println("<OPTION VALUE=\"" + strAccountTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
																					accountTypeSetup= true;
																				}
																				else
																				{
																					out.println("<OPTION VALUE=\"" + strAccountTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
																				}
																			}
																			if ( !accountTypeSetup )
																			{
																				out.println("<OPTION VALUE=\"-1\" SELECTED>---</OPTION>");
																			}
																		%>
																		</select>
																	</td>
																</tr>
														  <%}
														  	else{
														  	%>
														  	<tr>
															  	<td>
															  		<input type="hidden" name="accountTypeId" value="<%=Merchant.getAccountTypeId()%>" >
															  	</td>
														  	</tr>														  	
														  	<% 														  	
														  	}%>
														
																<tr class="main">
																	<td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankAddress",SessionData.getLanguage())%>:</td><td><input type="text" name="bankAddress" id="bankAddress" value="<%=StringUtil.toString(Merchant.getBankAddress())%>" size="20" maxlength="50" ></td>
																	<td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankCity",SessionData.getLanguage())%>:</td><td><input type="text" name="bankCity" id="bankCity" value="<%=StringUtil.toString(Merchant.getBankCity())%>" size="20" maxlength="50" ></td>
																</tr>
																<tr class="main">
																	<td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankState",SessionData.getLanguage())%>:</td><td><input type="text" name="bankState" id="bankState" value="<%=StringUtil.toString(Merchant.getBankState())%>" size="20" maxlength="50" ></td>
																	<td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankZip",SessionData.getLanguage())%>:</td><td><input type="text" name="bankZip" id="bankZip" value="<%=StringUtil.toString(Merchant.getBankZip())%>" size="20" maxlength="10" ></td>
																</tr>
																<tr class="main">
																	<td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankContactName",SessionData.getLanguage())%>:</td><td><input type="text" name="bankContactName" id="bankContactName" value="<%=StringUtil.toString(Merchant.getBankContactName())%>" size="20" maxlength="200" ></td>
																	<td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankContactPhone",SessionData.getLanguage())%>:</td><td><input type="text" name="bankContactPhone" id="bankContactPhone" value="<%=StringUtil.toString(Merchant.getBankContactPhone())%>" size="20" maxlength="15" ></td>
																</tr>
															<%
																}
															%>
															<td><div id="div_info_account" name="div_info_account" ></div></td>
														</tr>
													</table>
												</td>
											</tr>
											<%
if ( !customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){//If not deploying in Mexico
%>
											<tr>
												<td class="formAreaTitle2">
													<br><%=Languages.getString("jsp.admin.customers.merchants_edit.terminal_information",SessionData.getLanguage())%></td>
											</tr>
											<tr>
												<td class="formArea2"><%=Languages.getString("jsp.admin.customers.merchants_edit.monthly_fee_desc",SessionData.getLanguage())%><br>
													<table width="100">
														<tr class="main">
															<td nowrap="nowrap">
																<%=Languages.getString("jsp.admin.customers.merchants_edit.monthly_fee_threshold",SessionData.getLanguage())%>:
															</td>
															<td nowrap="nowrap">
																<input type="text" id="monthlyFeeThreshold" name="monthlyFeeThreshold"
																	value="<%=Merchant.getMonthlyFeeThreshold()%>"
																	size="15" maxlength="15">
																<%if (merchantErrors != null && merchantErrors.containsKey("monthlyFeeThreshold")) out.print("<font color=ff0000>*</font>");%>(<%=Languages.getString("jsp.admin.optional",SessionData.getLanguage())%>)
															</td>
														</tr>
													</table>
												</td>
											</tr>
<%
}
%>
											<tr>
												<td class="formAreaTitle2" align="center">
													<input type="hidden" name="submitted" name="submitted" value="y">
													<input type="hidden" name="noCreditCheck" value="<%=Merchant.getNoCreditCheck()%>">
													<input type="hidden" id="addressValidationStatus" name="addressValidationStatus"  value="<%=Merchant.getAddressValidationStatus()%>">

													<input type="button" id="Buttonsubmit" name="Buttonsubmit" value="<%=Languages.getString("jsp.admin.customers.merchants_edit.update_merchant",SessionData.getLanguage())%>">
													<input type="button" value="<%=Languages.getString("jsp.admin.cancel",SessionData.getLanguage())%>" onClick="history.go(-1)">

												</td>
											</tr>
										</table>
									</form>
								</td>
<td id="messagesZone" style="width: 150px;">
    <div id="divMessages" style="width: 550px;">
       
    </div>    
</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>