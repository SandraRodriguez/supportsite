<%-- 
    Document   : updateMerchant
    Created on : Mar 10, 2016, 4:00:43 PM
    Author     : nmartinez
--%>

<%@page import="com.debisys.customers.Merchant"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<%
    String merchantId =request.getParameter("merchantId");
    String latitude =request.getParameter("latitude");
    String longitude =request.getParameter("longitude");
    
    int result = Merchant.updateLatLong(merchantId, latitude, longitude);
    out.println(result);
%>
