
<%@page import="java.util.Map"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.debisys.users.User"%>
<%@page import="com.debisys.utils.DebisysConstants"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="com.google.gson.JsonObject"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
    response.setContentType("application/json");
    String userName = request.getParameter("username");
    String userPassword = request.getParameter("password");
    String name = request.getParameter("name");
    String mailNotification = request.getParameter("mailNotification");
    
    User userValidation = new User();
    userValidation.setUsername(userName);
    userValidation.setPassword(userPassword);
    userValidation.setpasswordId(null);
    if (name!=null){
        userValidation.setName(name);
    }
    userValidation.setEmail(mailNotification);
    JsonObject json = new JsonObject();
    
    userValidation.validateAddUpdateUser(SessionData, application);
    Hashtable<String, String> errors = userValidation.getErrors();
    
    if (errors!=null){
        for (Map.Entry<String, String> entry : errors.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            json.addProperty(key, value.toString());
        }
    }
    out.print(json);
%>