<%@ page language="java" 
         import="java.util.*,
         com.debisys.utils.*,
         com.debisys.languages.*,
         com.debisys.customers.MerchantInvoicePayments" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String customConfigType = request.getParameter("customConfigType");
    String merchantId = Merchant.getMerchantId();
    ArrayList<MerchantInvoicePayments> merchantPaymentsTypes = null;
    String disabledControls = "";
    boolean showAddButton = true;
    boolean showNewRecord = false;
    boolean editingMerchant = false;
    String messageOrWarning = "";
    boolean resultTransaction = false;

    if (merchantId != null && merchantId != "") {
        editingMerchant = true;
    } else {
        editingMerchant = false;
        merchantId = "0";
    }

    if (Merchant.getInvoice() == 0) {
        disabledControls = "disabled";
    }


    String invoicingAction = request.getParameter("invoicingAction");
    if (invoicingAction != null) {
        if (invoicingAction.equals("add")) {
            showAddButton = false;
            showNewRecord = true;
        } else if (invoicingAction.equals("delete")) {
            String idToDelete = request.getParameter("invoicingAction_Index");
            resultTransaction = MerchantInvoicePayments.deleteInvoicePayment(idToDelete, merchantId);
            //merchantPaymentsTypes = MerchantInvoicePayments.findMerchantInvoicePayments(merchantId);
            //if ( !resultTransaction )
            //{
            //	messageOrWarning = Languages.getString("jsp.admin.customers.merchants_add.Invoice_Error",SessionData.getLanguage());
            //}		    
        } else if (invoicingAction.equals("edit")) {
            String idToEdit = request.getParameter("invoicingAction_Index");
            merchantPaymentsTypes = MerchantInvoicePayments.findMerchantInvoicePayments(merchantId);
            if (merchantPaymentsTypes != null) {
                for (MerchantInvoicePayments merPayment : merchantPaymentsTypes) {
                    if (merPayment.getId().equals(new Integer(idToEdit))) {
                        merPayment.setShowControls(true);
                    }
                }
            }
            showAddButton = false;
        } else if (invoicingAction.equals("save")) {
            String typeInvoicing = request.getParameter("new_paymentTypeForInvoiceGeneration");
            String accountInvoicing = request.getParameter("new_accountNumberForInvoiceGeneration");
            MerchantInvoicePayments currentMerchantInvoicingPayment = new MerchantInvoicePayments();
            currentMerchantInvoicingPayment.setAccountNumber(accountInvoicing);
            currentMerchantInvoicingPayment.setMerchantId(new Long(merchantId));
            currentMerchantInvoicingPayment.setPaymentTypeId(new Integer(typeInvoicing));
            if (typeInvoicing != null) {
                MerchantInvoicePayments.addInvoicePayment(currentMerchantInvoicingPayment);
            }
        } else if (invoicingAction.equals("update")) {
            String invoicingActionIndex = request.getParameter("invoicingAction_Index");
            String typeInvoicing = request.getParameter("paymentTypeForInvoiceGeneration");
            String accountInvoicing = request.getParameter("accountNumberForInvoiceGeneration");
            MerchantInvoicePayments currentMerchantInvoicingPayment = new MerchantInvoicePayments();
            currentMerchantInvoicingPayment.setAccountNumber(accountInvoicing);
            currentMerchantInvoicingPayment.setMerchantId(new Long(merchantId));
            currentMerchantInvoicingPayment.setPaymentTypeId(new Integer(typeInvoicing));
            currentMerchantInvoicingPayment.setId(new Integer(invoicingActionIndex));
            if (typeInvoicing != null) {
                MerchantInvoicePayments.updateInvoicePayment(currentMerchantInvoicingPayment);
            }
        }
    }

    if (merchantPaymentsTypes == null) {
        merchantPaymentsTypes = MerchantInvoicePayments.findMerchantInvoicePayments(merchantId);
    }
%>

<script type="text/javascript">

    <%
        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
    $(document).ready(function () {
        $("#invoice").change(onSelectChangeInvoice);
        $("#new_paymentTypeForInvoiceGeneration").change(onSelectChangePaymentTypeForInvoiceGeneration);
        $("#paymentTypeForInvoiceGeneration").change(onSelectChangePaymentTypeForInvoiceGenerationUpdate);

        $("#new_accountNumberForInvoiceGeneration").keydown(function (event)
        {
            // Allow: backspace, delete, tab, escape, and enter
            if (event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 27 || event.keyCode === 13 ||
                    // Allow: Ctrl+A
                            //(event.keyCode == 65 && event.ctrlKey === true) ||
                                    // Allow: home, end, left, right
                                            (event.keyCode >= 35 && event.keyCode <= 39)) {
                                // let it happen, don't do anything
                                return;
                            } else {
                                // Ensure that it is a number and stop the keypress
                                var notNumber = (event.keyCode < 48 || event.keyCode > 57) || event.shiftKey;
                                notNumber = ((event.keyCode < 96 || event.keyCode > 105) && notNumber);
                                if (notNumber) {
                                    event.preventDefault();
                                }
                                ;
                            }
                            ;
                        });
                $("#new_accountNumberForInvoiceGeneration").keyup(function ()
                {
                    var accountLength = $("#new_accountNumberForInvoiceGeneration").val();
                    if (accountLength.length === 4) {
                        $("#btnSaveInvoicePayment").removeAttr('disabled');
                    } else
                    {
                        $("#btnSaveInvoicePayment").attr('disabled', 'disabled');
                    }
                });


                ////////////////////////////////////////////////////
                $("#accountNumberForInvoiceGeneration").keydown(function (event)
                {
                    // Allow: backspace, delete, tab, escape, and enter
                    if (event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 27 || event.keyCode === 13 ||
                            // Allow: Ctrl+A
                                    //(event.keyCode == 65 && event.ctrlKey === true) ||
                                            // Allow: home, end, left, right
                                                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                                        // let it happen, don't do anything
                                        return;
                                    } else {
                                        // Ensure that it is a number and stop the keypress
                                        var notNumber = (event.keyCode < 48 || event.keyCode > 57) || event.shiftKey;
                                        notNumber = ((event.keyCode < 96 || event.keyCode > 105) && notNumber);
                                        if (notNumber) {
                                            event.preventDefault();
                                        }
                                        ;
                                    }
                                    ;
                                });
                        $("#accountNumberForInvoiceGeneration").keyup(function ()
                        {
                            var accountLength = $("#accountNumberForInvoiceGeneration").val();
                            if (accountLength.length === 4) {
                                $("#btnSaveInvoicePayment").removeAttr('disabled');
                            } else
                            {
                                $("#btnSaveInvoicePayment").attr('disabled', 'disabled');
                            }
                        });
                        ////////////////////////////////////////////////////

                        onSelectChangeInvoice();
                    });

            function onSelectChangeInvoice()
            {
                var selected = $("#invoice option:selected");
                if ((selected !== null) && (selected.attr("id") !== "NINV"))
                {
                    $("#invoicing").val("true");
                    $("#btnAddInvoicingPaymentType").removeAttr('disabled');
                    $("#tableInvoicingPaymentType").css('display', 'inline');
                } else {
                    $("#tableInvoicingPaymentType").css('display', 'none');
                    $("#btnAddInvoicingPaymentType").attr('disabled', 'disabled');
                    $("#invoicing").val("false");
                }
            }

            function onSelectChangePaymentTypeForInvoiceGeneration() {
                var selected = $("#new_paymentTypeForInvoiceGeneration option:selected");
                var output = "";
                var idSelected = selected.attr("id");
                if (idSelected !== null && (idSelected === "C" || idSelected === "N/A")) {
                    $("#new_accountNumberForInvoiceGeneration").attr('disabled', 'disabled');
                    $("#new_accountNumberForInvoiceGeneration").val("");
                    $("#btnSaveInvoicePayment").removeAttr('disabled');
                    $("#spanAccountNumber").text("");
                } else
                {
                    $("#btnSaveInvoicePayment").attr('disabled', 'disabled');
                    $("#accountNumberForInvoiceGeneration").removeAttr('disabled');
                    $("#accountNumberForInvoiceGeneration").focus();
                    $("#spanAccountNumber").text("<%=Languages.getString("jsp.admin.customers.merchants_add.paymentWarning1", SessionData.getLanguage())%>");
                }
            }

            function onSelectChangePaymentTypeForInvoiceGenerationUpdate() {
                var selected = $("#paymentTypeForInvoiceGeneration option:selected");
                var output = "";
                var idSelected = selected.attr("id");
                if (idSelected !== null && (idSelected === "C" || idSelected === "N/A")) {
                    $("#accountNumberForInvoiceGeneration").attr('disabled', 'disabled');
                    $("#accountNumberForInvoiceGeneration").val("");
                    $("#btnSaveInvoicePayment").removeAttr('disabled');
                    $("#spanAccountNumber").text("");
                } else
                {
                    $("#btnSaveInvoicePayment").attr('disabled', 'disabled');
                    $("#accountNumberForInvoiceGeneration").removeAttr('disabled');
                    $("#accountNumberForInvoiceGeneration").focus();
                    $("#spanAccountNumber").text("<%=Languages.getString("jsp.admin.customers.merchants_add.paymentWarning1", SessionData.getLanguage())%>");
                }
            }


    <%
        }
    %>

</script>


<table width="100%" class="formArea2">	
    <%
        String Invoice_edit_title = Languages.getString("jsp.admin.customers.merchants_add.Invoice_edit", SessionData.getLanguage());
        String Invoice_delete_title = Languages.getString("jsp.admin.customers.merchants_add.Invoice_delete", SessionData.getLanguage());
        String Invoice_add_title = Languages.getString("jsp.admin.customers.merchants_add.Invoice_add", SessionData.getLanguage());
        String Invoice_save_title = Languages.getString("jsp.admin.customers.merchants_add.Invoice_save", SessionData.getLanguage());
        String Invoice_cancel_title = Languages.getString("jsp.admin.customers.merchants_cancel.message", SessionData.getLanguage());

        Vector paymentForInvoiceGeneration = Merchant.getPaymentTypesForInvoiceGeneration();
        Vector vecInvoiceTypes = Merchant.getInvoiceTypes();
        Iterator itInvoiceType = vecInvoiceTypes.iterator();
    %>
    <tr>
        <td class="main">
            <b><%=Languages.getString("jsp.admin.customers.merchants_add.Invoice_type", SessionData.getLanguage())%>:</b>
            <select name="invoice" id="invoice">
                <%
                    while (itInvoiceType.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) itInvoiceType.next();
                        String strInvoiceTypeId = vecTemp.get(0).toString();
                        String strInvoiceTypeName = vecTemp.get(1).toString();
                        String strInvoiceCode = vecTemp.get(2).toString();
                        String strSelected = ((strInvoiceTypeId.equals(Integer.toString(Merchant.getInvoice()))) ? "SELECTED" : "");
                        out.println("<option id='" + strInvoiceCode + "' value=\"" + strInvoiceTypeId + "\" " + strSelected + ">" + strInvoiceTypeName + "</option>");
                    }
                %>
            </select>
            <input type="hidden" id=invoicing name="invoicing" value="<%=Merchant.isInvoicing()%>" >
        </td>		  		
    </tr>
    <tr>			
        <td>			
            <table id="tableInvoicingPaymentType" name="tableInvoicingPaymentType" width="70%"  >
                <tr>
                    <td id="tdWarnings" style="color:red" colspan="3">
                        <%=messageOrWarning%>
                        <input type="hidden" id="invoicingAction" name="invoicingAction" value="" /> 
                        <input type="hidden" id="invoicingAction_Index" name="invoicingAction_Index" value="" /> 
                    </td>
                </tr>
                <tr>
                    <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_add.paymentTypeInvoice", SessionData.getLanguage())%></td>
                    <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_add.paymentAccountInvoiceNumber", SessionData.getLanguage())%></td>
                    <td class="rowhead2"></td>
                </tr>

                <%

                    if (merchantPaymentsTypes != null) {
                        int classTr = 1;
                        int index = 0;
                        for (MerchantInvoicePayments invoiceTypeMerchant : merchantPaymentsTypes) {
                            index = invoiceTypeMerchant.getId().intValue();
                            if (invoiceTypeMerchant.getShowControls()) {
                                Iterator pIteratorNew = paymentForInvoiceGeneration.iterator();
                                StringBuilder comboInvoicePayments = new StringBuilder();
                                comboInvoicePayments.append("<select id=\"paymentTypeForInvoiceGeneration\" name=\"paymentTypeForInvoiceGeneration\">");
                                while (pIteratorNew.hasNext()) {
                                    Vector vecTemp = null;
                                    vecTemp = (Vector) pIteratorNew.next();
                                    String id = vecTemp.get(0).toString();
                                    String code = vecTemp.get(1).toString();
                                    String description = vecTemp.get(2).toString();
                                    if (invoiceTypeMerchant.getPaymentTypeId() != null && invoiceTypeMerchant.getPaymentTypeId().toString().equals(id)) {
                                        comboInvoicePayments.append("<option id=\"" + code + "\" value=\"" + id + "\" selected >" + description + "</option>");
                                    } else {
                                        comboInvoicePayments.append("<option id=\"" + code + "\" value=\"" + id + "\" >" + description + "</option>");
                                    }
                                }
                                comboInvoicePayments.append("</select>");
                %>
                <tr class="row<%=classTr%>">
                    <td align="center">
                        <%=comboInvoicePayments.toString()%>	
                    </td>
                    <td align="center">
                        <input type="text" id="accountNumberForInvoiceGeneration" name="accountNumberForInvoiceGeneration"  
                               value="<%=invoiceTypeMerchant.getAccountNumber()%>"  maxlength="4" size="5"  />
                        <input type="hidden" id="typeInvoicingDescription" name="typeInvoicingDescription" value="">
                        <span id="spanAccountNumber" style="color:red"></span>
                    </td>
                    <td align="center" >
                        <table>
                            <tr>
                                <td><input id="btnSaveInvoicePayment" name="btnSaveInvoicePayment" type="button" 
                                           value="<%=Invoice_save_title%>" onclick="updateInvoicingPayment('<%=index%>')" /> </td>			
                                <td><input id="btnCancel" name="btnCancel" type="button" 
                                           value="<%=Invoice_cancel_title%>"  onclick="cancelInvoicingPayment()"/> </td> 			  	                   
                            </tr>
                        </table>
                    </td>                                                        
                </tr>					 					
                <%
                } else {
                %>
                <tr class="row<%=classTr%>">
                    <td align="center"><%=invoiceTypeMerchant.getDescription()%></td>
                    <td align="center"><%=invoiceTypeMerchant.getAccountNumber()%></td>
                    <td align="center" >
                        <table>
                            <tr>
                                <td><img title="<%=Invoice_edit_title%>" src="images/icon_edit.gif" onclick="editInvoicingPayment('<%=index%>')"></td>
                                <td>&nbsp;</td>
                                <td><img title="<%=Invoice_delete_title%>" src="images/icon_delete.gif" onclick="deletePaymentTypeRecord('<%=index%>')"></td>
                            </tr>
                        </table>
                    </td> 
                </tr>
                <%
                            }
                            invoiceTypeMerchant.setShowControls(false);
                            if (classTr == 1) {
                                classTr = 2;
                            } else {
                                classTr = 1;
                            }
                        }
                    }
                %>	
                <tr>
                    <% if (showAddButton) {%>
                    <td align="right" colspan="3">
                        <input id="btnAddInvoicingPaymentType" name="btnAddInvoicingPaymentType" type="button" value="<%=Invoice_add_title%>" 
                               onclick="addPaymentTypeRecord()" <%=disabledControls%>>

                    </td>	  		 
                    <%
                        }
                        if (showNewRecord) {
                            Iterator pIteratorNew = paymentForInvoiceGeneration.iterator();
                            StringBuilder comboInvoicePayments = new StringBuilder();
                            comboInvoicePayments.append("<select id=\"new_paymentTypeForInvoiceGeneration\" name=\"new_paymentTypeForInvoiceGeneration\">");
                            while (pIteratorNew.hasNext()) {
                                Vector vecTemp = null;
                                vecTemp = (Vector) pIteratorNew.next();
                                String id = vecTemp.get(0).toString();
                                String code = vecTemp.get(1).toString();
                                String description = vecTemp.get(2).toString();
                                comboInvoicePayments.append("<option id=\"" + code + "\" value=\"" + id + "\" >" + description + "</option>");
                            }
                            comboInvoicePayments.append("</select>");
                    %>
                    <td align="center">
                        <%=comboInvoicePayments.toString()%>	
                    </td>
                    <td align="center">
                        <input type="text" id="new_accountNumberForInvoiceGeneration" name="new_accountNumberForInvoiceGeneration" value=""  maxlength="4" size="5"  />
                        <span id="spanAccountNumber" style="color:red"></span>							
                    </td>
                    <td align="center" >
                        <table>
                            <tr>
                                <td>
                                    <input id="btnSaveInvoicePayment" type="button" value="<%=Invoice_save_title%>"  onclick="saveInvoicingPayment('-1')"/> 
                                </td>
                                <td>
                                    <input id="btnCancel" name="btnCancel" type="button" value="<%=Invoice_cancel_title%>"  onclick="cancelInvoicingPayment()"/>
                                </td>				                   
                            </tr>
                        </table>
                    </td>
                    <%}%>
                </tr>
            </table>
        </td>			
    </tr>

</table>
<SCRIPT>
            function updateInvoicingPayment(index)
            {
                var paymentTypeForInvoiceGeneration = $("#paymentTypeForInvoiceGeneration option:selected").val();
                $("#mxContactAction").val("--");
                document.getElementById('invoicingAction').value = "update";
                $("#invoicingAction_Index").val(index);
                document.formMerchant.onsubmit = new Function("return true;");
                document.formMerchant.Buttonsubmit.click();
                document.formMerchant.Buttonsubmit.disabled = true;
            }
            function saveInvoicingPayment(index)
            {
                var paymentTypeForInvoiceGeneration = $("#new_paymentTypeForInvoiceGeneration option:selected").val();
                $("#mxContactAction").val("--");
                document.getElementById('invoicingAction').value = "save";
                document.formMerchant.onsubmit = new Function("return true;");
                document.formMerchant.Buttonsubmit.click();
                document.formMerchant.Buttonsubmit.disabled = true;
            }
            function editInvoicingPayment(index)
            {
                $("#mxContactAction").val("--");
                document.getElementById('invoicingAction').value = "edit";
                document.getElementById('invoicingAction_Index').value = index;
                document.formMerchant.onsubmit = new Function("return true;");
                document.formMerchant.Buttonsubmit.click();
                document.formMerchant.Buttonsubmit.disabled = true;
                //alert("editInvoicingPayment");
            }
            function addPaymentTypeRecord()
            {
                $("#mxContactAction").val("--");
                document.getElementById('invoicingAction').value = "add";
                document.getElementById('Buttonsubmit').value = "-";
                document.formMerchant.onsubmit = new Function("return true;");
                document.formMerchant.Buttonsubmit.click();
                document.formMerchant.Buttonsubmit.disabled = true;
            }
            function deletePaymentTypeRecord(index)
            {
                $("#mxContactAction").val("--");
                $("#Buttonsubmit").val("-");
                $("#invoicingAction").val("delete");
                $("#invoicingAction_Index").val(index);
                document.formMerchant.onsubmit = new Function("return true;");
                document.formMerchant.Buttonsubmit.click();
                document.formMerchant.Buttonsubmit.disabled = true;
            }
            function cancelInvoicingPayment()
            {
                $("#mxContactAction").val("--");
                $("#Buttonsubmit").val("-");
                $("#invoicingAction").val("cancel");
                document.formMerchant.onsubmit = new Function("return true;");
                document.formMerchant.Buttonsubmit.click();
                document.formMerchant.Buttonsubmit.disabled = true;
            }
            function removeAllPaymentTypeRecords()
            {
                $("#mxContactAction").val("--");
                document.getElementById('invoicingAction').value = "deleteAll";
                document.getElementById('Buttonsubmit').value = "-";
                document.formMerchant.onsubmit = new Function("return true;");
                document.formMerchant.Buttonsubmit.click();
                document.formMerchant.Buttonsubmit.disabled = true;
            }
</SCRIPT>
