<%@page import="com.debisys.utils.JSONObject"%>
<%@page import="java.net.URLEncoder,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.languages.Languages,
                 java.util.*,
                 com.debisys.reports.TransactionReport,
                 com.debisys.customers.CustomerSearch" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<%
    String ids          = request.getParameter("ids");
    String typeEntity   = request.getParameter("type");
    String responseType = request.getParameter("responseType");
    String isforVerison = request.getParameter("isforVerison");
        
    String strDistChainType = SessionData.getProperty("dist_chain_type");
    
    ArrayList<String> fieldToRead = new ArrayList<String>();
    StringBuilder entities = new StringBuilder();
    Vector vecEntitiesList = new Vector();
    String gotoNextControl="";
    
    SessionData.setProperty( CustomerSearch.PROPERTY_SESSION_SQL_ENTITY, "1");
    if ( ids.length()>0 && !ids.equals("-1"))
    {
        SessionData.setProperty( CustomerSearch.PROPERTY_SESSION_SQL_ENTITY_IDS, ids);
    }
    else
    {
        ids = "";
    }
    
    if ( typeEntity.equals( DebisysConstants.AGENT ) )
    {
    	 gotoNextControl = DebisysConstants.SUBAGENT;
        CustomerSearch.searchRep(-1, -1, SessionData, DebisysConstants.REP_TYPE_AGENT);
        fieldToRead.add("rep_id");
        fieldToRead.add("businessname");
    }
    else if ( typeEntity.equals( DebisysConstants.SUBAGENT ) )
    {
    	gotoNextControl = DebisysConstants.REP;
        CustomerSearch.searchRep(-1, -1, SessionData, DebisysConstants.REP_TYPE_SUBAGENT);
        fieldToRead.add("rep_id");
        fieldToRead.add("businessname");
    }
    else if ( typeEntity.equals( DebisysConstants.REP ) )
    {
    	 gotoNextControl = DebisysConstants.MERCHANT;
        CustomerSearch.searchRep(-1, -1, SessionData, DebisysConstants.REP_TYPE_REP); 
        fieldToRead.add("rep_id");
        fieldToRead.add("businessname");
    }
    else if ( typeEntity.equals( DebisysConstants.MERCHANT ) )
    {
        if ( responseType != null && responseType.equals("html"))
        {
            fieldToRead.add("m.merchant_id");
            fieldToRead.add("m.dba");
            String sqlStatement = CustomerSearch.searchMerchantLocatorReport(DebisysConstants.REP, ids, strDistChainType, "", "","","", fieldToRead);
            sqlStatement = sqlStatement + " ORDER BY m.dba ASC ";
            SessionData.setProperty( CustomerSearch.PROPERTY_SESSION_SQL_ENTITY, sqlStatement);            
        }
        else
        {
            String statusMerchants = request.getParameter("statusmerchants");
            String period          = request.getParameter("period");
            String unitNumber      = request.getParameter("unitNumber");
            String trxNumber       = request.getParameter("trxNumber");
            
            fieldToRead.add("m.merchant_id");
            fieldToRead.add("m.dba");
            fieldToRead.add("m.latitude");
            fieldToRead.add("m.longitude");
            fieldToRead.add("m.datecancelled");
            fieldToRead.add("m.cancelled");
            fieldToRead.add("r.businessname");
            
            if ( statusMerchants!=null && statusMerchants.length() >0 )
            {
                StringBuilder sqlB = new StringBuilder();
                String[] statusMerchantSplit = statusMerchants.split(",");
                for (int i=0; i < statusMerchantSplit.length ; i++)
                {                    
                    String sqlStatement = CustomerSearch.searchMerchantLocatorReport(DebisysConstants.MERCHANT, ids, strDistChainType, 
                                            statusMerchantSplit[i], unitNumber, period, trxNumber, fieldToRead);
                    if ( i == statusMerchantSplit.length-1 )
                        sqlB.append( sqlStatement );
                    else
                        sqlB.append( sqlStatement + " UNION ");
                }
                sqlB.append( " ORDER BY m.dba ASC ");
                fieldToRead.add("trx");
                SessionData.setProperty( CustomerSearch.PROPERTY_SESSION_SQL_ENTITY, sqlB.toString());
                
            }
        }        
            
    }
    
    String sql = SessionData.getProperty( CustomerSearch.PROPERTY_SESSION_SQL_ENTITY);
    SessionData.removeProperty( CustomerSearch.PROPERTY_SESSION_SQL_ENTITY);
    SessionData.removeProperty( CustomerSearch.PROPERTY_SESSION_SQL_ENTITY_IDS);
        
    if ( sql != null && sql.length() > 0 )
    {
        if ( responseType != null && responseType.equals("html"))
        {
            vecEntitiesList = CustomerSearch.executeReport(sql, fieldToRead);
            Iterator it = vecEntitiesList.iterator();
            String all = Languages.getString("jsp.admin.reports.all",SessionData.getLanguage());
            if(isforVerison != null && isforVerison.equals("1")){
            	entities.append("<option value=-1 onclick=\"clicCombo("+typeEntity+","+gotoNextControl+")\">" + all + "</option>");
            }
            else{
            	entities.append("<option value=-1 >" + all + "</option>");
            }
            while (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector) it.next();
              entities.append("<option value=" + vecTemp.get(0) +" onclick=\"clicCombo("+typeEntity+","+gotoNextControl+")\">" + vecTemp.get(1) + "</option>");
            } 
        }
        else
        {
            JSONObject json = CustomerSearch.searchMerchantsWithLatLong(sql,fieldToRead);
            entities.append(json.toString());
            System.out.println(entities.toString());
        }       
    }    
    out.println( entities.toString() );
%>
