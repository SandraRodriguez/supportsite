<%-- 
    Document   : merchant_locator
    Created on : Apr 9, 2015, 9:35:58 AM
    Author     : nmartinez
--%>

<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.reports.TransactionReport,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.languages.Languages" %>
<%@page import="com.debisys.utils.TimeZone"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
    String strDistChainType = request.getParameter("strDistChainType");
    String strAccessLevel   = request.getParameter("strAccessLevel");    
    String strRefId = request.getParameter("strRefId");
    String showMerchants = request.getParameter("showMerchants");
    
    ArrayList<String> controlsByAccesLevel = new ArrayList<String>();
    
    String entityToLoad       = "";
    String prefixEntityControl = "options_";
    String accessLevel = strAccessLevel;
    String ACTIVATED = "1";
    
    String formatPatterControls = "%s";
       
    if ( accessLevel.equals(DebisysConstants.ISO) )
    {
        if ( strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) )
        {            
            entityToLoad       = DebisysConstants.REP;
            controlsByAccesLevel.add(String.format(formatPatterControls, entityToLoad));
            if(showMerchants == null || (showMerchants != null && showMerchants.equals(ACTIVATED))){
                controlsByAccesLevel.add(String.format(formatPatterControls, DebisysConstants.MERCHANT));
            }
        }
        else if ( strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) )
        {
            entityToLoad   = DebisysConstants.AGENT;
            controlsByAccesLevel.add(String.format(formatPatterControls, entityToLoad));
            controlsByAccesLevel.add(String.format(formatPatterControls, DebisysConstants.SUBAGENT));
            controlsByAccesLevel.add(String.format(formatPatterControls, DebisysConstants.REP));
            if(showMerchants == null || (showMerchants != null && showMerchants.equals(ACTIVATED))){
                controlsByAccesLevel.add(String.format(formatPatterControls, DebisysConstants.MERCHANT));
            }
        }
    }
    else if ( accessLevel.equals(DebisysConstants.AGENT) )
    {
        entityToLoad   = DebisysConstants.SUBAGENT;
        controlsByAccesLevel.add(String.format(formatPatterControls, entityToLoad));
        controlsByAccesLevel.add(String.format(formatPatterControls, DebisysConstants.REP));
        if(showMerchants == null || (showMerchants != null && showMerchants.equals(ACTIVATED))){
            controlsByAccesLevel.add(String.format(formatPatterControls, DebisysConstants.MERCHANT));
        }
                
    }
    else if ( accessLevel.equals(DebisysConstants.SUBAGENT) )
    {
        entityToLoad   = DebisysConstants.REP;
        controlsByAccesLevel.add(String.format(formatPatterControls, entityToLoad));
        if(showMerchants == null || (showMerchants != null && showMerchants.equals(ACTIVATED))){
            controlsByAccesLevel.add(String.format(formatPatterControls, DebisysConstants.MERCHANT));
        }
    }
    else if ( accessLevel.equals(DebisysConstants.REP) )
    {
        entityToLoad   = DebisysConstants.MERCHANT; 
        controlsByAccesLevel.add(String.format(formatPatterControls, entityToLoad));
    }
    else
    {
        response.sendRedirect("/support/admin/index.jsp");
    }     
%>

<script LANGUAGE="JavaScript"> 
        
    function fillControl(entityType, selectedIds)
    {                
        $.get( "admin/customers/merchants/data_entity.jsp?ids="+selectedIds+"&type="+entityType+"&responseType=html", function( data )
        {
            var entities = [];
            entities = data.trim(); 
            loadDataToControl(entities, entityType) 
        });
    }
    
    function loadDataToControl(data, controlId) 
    {
        $("#<%=prefixEntityControl%>"+controlId).empty(); //To reset previuos data
        $("#<%=prefixEntityControl%>"+controlId).append(data);        
    }     
    
    $(document).ready(function() {
        fillControl(<%=entityToLoad%>,<%=strRefId%>);
    });
    
</script>

    <input type="hidden" name="currentEntityType" id="currentEntityType" value=""/>                                    
    <input type="hidden" name="currentSelectedIds" id="currentSelectedIds" value=""/>                                    
    
    <table width="1500px">                                                        
        <tr>
            <td>
            <%
            String formatEmptyControls = "$(\"#options_%s\").empty(); "+'\n';
            for(String controlEntity : controlsByAccesLevel )
            {
                String gotoNextControl = "";
                StringBuilder emptyControls = new StringBuilder();
                String titleCombo = "";
                boolean doActions = true;
                if ( controlEntity.equals( DebisysConstants.AGENT ) )
                {
                    titleCombo = Languages.getString("jsp.admin.reports.transactions.daily_liability.agent.select",SessionData.getLanguage());
                    gotoNextControl = DebisysConstants.SUBAGENT;
                    emptyControls.append( String.format(formatEmptyControls, DebisysConstants.SUBAGENT));
                    emptyControls.append( String.format(formatEmptyControls, DebisysConstants.REP));
                    emptyControls.append( String.format(formatEmptyControls, DebisysConstants.MERCHANT));
                    //accessLevelReport = DebisysConstants.ISO;
                }
                else if ( controlEntity.equals( DebisysConstants.SUBAGENT ) )
                {
                    titleCombo = Languages.getString("jsp.admin.reports.transactions.daily_liability.sub_agent.select",SessionData.getLanguage());
                    gotoNextControl = DebisysConstants.REP;
                    emptyControls.append( String.format(formatEmptyControls, DebisysConstants.REP));
                    emptyControls.append( String.format(formatEmptyControls, DebisysConstants.MERCHANT));
                    //accessLevelReport = DebisysConstants.AGENT;
                }
                else if ( controlEntity.equals( DebisysConstants.REP ) )
                {
                    titleCombo = Languages.getString("jsp.admin.reports.transactions.daily_liability.rep.select",SessionData.getLanguage());
                    gotoNextControl = DebisysConstants.MERCHANT;
                    emptyControls.append( String.format(formatEmptyControls, DebisysConstants.MERCHANT));
                    //accessLevelReport = DebisysConstants.SUBAGENT;
                }
                else if ( controlEntity.equals( DebisysConstants.MERCHANT ) )
                {
                    titleCombo = Languages.getString("jsp.admin.reports.transactions.daily_liability.merchant.select",SessionData.getLanguage());
                    doActions = false;
                }
            %>
                                                    
            <script LANGUAGE="JavaScript">
                $(function() {
                    $('#<%=prefixEntityControl%><%=controlEntity%>').change(function() {
                        var unselectAll= false;
                        if ( $(this).val() == "-1" || $(this).val().indexOf("-1") >= 0 )
                        {
                            unselectAll = true;
                        }
                        
                        if ( unselectAll )
                        {
                            $('#<%=prefixEntityControl%><%=controlEntity%>').each(function(){ 

                                // Loop through the option elements inside the select element
                                $(this).find('option').each(function()
                                { 
                                    if( $(this).attr('value') != '-1' )
                                    {
                                        $(this).prop('selected', true);
                                        $(this).attr('selected','selected');  
                                    }  
                                    else
                                    {
                                        //$(this).attr('selected','false');  
                                        $(this).removeAttr("selected");
                                    }  
                                });
                            });                                
                        }
                        $("#currentEntityType").val(<%=controlEntity%>);
                        $("#currentSelectedIds").val($(this).val());
                        <%if (doActions){%>                        
                            fillControl("<%=gotoNextControl%>",$(this).val());                                  
                            <%=emptyControls%>                        
                        <%}%>
                    });
                    $('#<%=prefixEntityControl%><%=controlEntity%>').click(function() {
                        $("#currentEntityType").val(<%=controlEntity%>);
                        $("#currentSelectedIds").val($(this).val());
                    });
                    
                });
                
            </script>
                                                        
            <div id="div_<%=controlEntity%>" style="float: left;position: relative;width:350px;">
                <table width=70>
                    <tr class="main">
                        <td class=main valign=top nowrap><%=titleCombo%></td>
                    </tr>
                    <tr>                                                            
                        <td class=main valign=top>
                            <select id="<%=prefixEntityControl%><%=controlEntity%>" name="<%=prefixEntityControl%><%=controlEntity%>" size="15" style="width: 300px" multiple >                                                                       
                            </select>
                           <br>
                        </td>
                    </tr>                                                                    
                </table>                                                                    
            </div>
            <%
            }
            %>
            </td>                                                            
        </tr>
    </table>            
