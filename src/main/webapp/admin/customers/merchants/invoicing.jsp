<%@ page language="java" 
         import="java.util.*,
         com.debisys.utils.*,
         com.debisys.languages.*,
         com.debisys.customers.MerchantInvoicePayments" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String customConfigType = request.getParameter("customConfigType");
    String merchantId = Merchant.getMerchantId();
    ArrayList<MerchantInvoicePayments> merchantPaymentsTypes = null;
    String disabledControls = "";
    boolean showAddButton = true;
    boolean editingMerchant = false;
    String messageOrWarning = "";
    boolean resultTransaction = false;

    if (merchantId != null && merchantId != "") {
        editingMerchant = true;
    } else {
        editingMerchant = false;
        merchantId = "0";
    }

    if (Merchant.getInvoice() == 0) {
        disabledControls = "disabled";
    }

    String invoicingAction = request.getParameter("invoicingAction");

    if (invoicingAction != null) {

        if (request.getParameter("invoicingAction").equals("add")) {
            merchantPaymentsTypes = (ArrayList<MerchantInvoicePayments>) session.getAttribute("merchantInvoicePaymentsArray");
            if (merchantPaymentsTypes == null) {
                merchantPaymentsTypes = new ArrayList<MerchantInvoicePayments>();
            }
            merchantPaymentsTypes.add(new MerchantInvoicePayments(Integer.valueOf(-1), Long.parseLong(merchantId), Integer.valueOf("-1"), "", true));
            session.setAttribute("merchantInvoicePaymentsArray", merchantPaymentsTypes);
            showAddButton = false;
        } else if (request.getParameter("invoicingAction").equals("delete")) {
            String idToDelete = request.getParameter("invoicingAction_Index");
            merchantPaymentsTypes = (ArrayList<MerchantInvoicePayments>) session.getAttribute("merchantInvoicePaymentsArray");
            if (merchantPaymentsTypes != null) {
                if (editingMerchant) {
                    //resultTransaction = MerchantInvoicePayments.deleteInvoicePayment(idToDelete, merchantPaymentsTypes);
                    //merchantPaymentsTypes = MerchantInvoicePayments.findMerchantInvoicePayments(merchantId);
                    //if ( !resultTransaction )
                    //{
                    //	messageOrWarning = Languages.getString("jsp.admin.customers.merchants_add.Invoice_Error",SessionData.getLanguage());
                    //}
                } else {
                    merchantPaymentsTypes.remove(Integer.parseInt(idToDelete));
                }
                session.setAttribute("merchantInvoicePaymentsArray", merchantPaymentsTypes);
            }
        } else if (request.getParameter("invoicingAction").equals("edit")) {
            String idToDelete = request.getParameter("invoicingAction_Index");
            merchantPaymentsTypes = (ArrayList<MerchantInvoicePayments>) session.getAttribute("merchantInvoicePaymentsArray");
            if (merchantPaymentsTypes != null) {
                MerchantInvoicePayments invoicingPayment = merchantPaymentsTypes.get(Integer.parseInt(idToDelete));
                invoicingPayment.setShowControls(true);
            }
            showAddButton = false;
        } else if (request.getParameter("invoicingAction").equals("save")) {
            merchantPaymentsTypes = (ArrayList<MerchantInvoicePayments>) session.getAttribute("merchantInvoicePaymentsArray");
            if (merchantPaymentsTypes != null) {
                String idToSave = request.getParameter("invoicingAction_Index");
                String typeInvoicing = request.getParameter("paymentTypeForInvoiceGeneration");
                String accountInvoicing = request.getParameter("accountNumberForInvoiceGeneration");
                String typeInvoicingDescription = request.getParameter("typeInvoicingDescription");

                MerchantInvoicePayments currentMerchantInvoicingPayment = merchantPaymentsTypes.get(Integer.parseInt(idToSave));

                if (editingMerchant) {
                    /*
                        currentMerchantInvoicingPayment.setAccountNumber(accountInvoicing);
                        currentMerchantInvoicingPayment.setDescription(typeInvoicingDescription);
                        currentMerchantInvoicingPayment.setPaymentTypeId(new Integer(typeInvoicing));
                        if ( currentMerchantInvoicingPayment.getId() == -1 )
                        {
                                resultTransaction = MerchantInvoicePayments.addInvoicePayment(currentMerchantInvoicingPayment);		      		
                        }
                        else
                        {	
                            resultTransaction = MerchantInvoicePayments.updateInvoicePayment(currentMerchantInvoicingPayment);		      	    
                        }   
                        if ( !resultTransaction )
                        {
                                messageOrWarning = Languages.getString("jsp.admin.customers.merchants_add.Invoice_Error",SessionData.getLanguage());
                        } 
                        merchantPaymentsTypes = MerchantInvoicePayments.findMerchantInvoicePayments(merchantId);
                        session.setAttribute("merchantInvoicePaymentsArray",merchantPaymentsTypes);
                     */
                } else {
                    boolean invoicePaymentAlreadyAdded = false;
                    if (accountInvoicing == null) {
                        accountInvoicing = "";
                    }
                    for (MerchantInvoicePayments mMerchantInvoicePayments : merchantPaymentsTypes) {
                        if (accountInvoicing.equals(mMerchantInvoicePayments.getAccountNumber())
                                && typeInvoicing.equals(mMerchantInvoicePayments.getPaymentTypeId().toString())) {
                            invoicePaymentAlreadyAdded = true;
                        }
                    }
                    if (!invoicePaymentAlreadyAdded) {
                        currentMerchantInvoicingPayment.setAccountNumber(accountInvoicing);
                        currentMerchantInvoicingPayment.setDescription(typeInvoicingDescription);
                        currentMerchantInvoicingPayment.setPaymentTypeId(new Integer(typeInvoicing));
                    } else {
                        merchantPaymentsTypes.remove(Integer.parseInt(idToSave));
                    }
                }
            }
        } else if (request.getParameter("mxContactAction") != null) {
            merchantPaymentsTypes = (ArrayList<MerchantInvoicePayments>) session.getAttribute("merchantInvoicePaymentsArray");
        }
    } else if (request.getParameter("mxContactAction") != null) {
        merchantPaymentsTypes = (ArrayList<MerchantInvoicePayments>) session.getAttribute("merchantInvoicePaymentsArray");
    }
%>

<script type="text/javascript">

    <%
        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
    $(document).ready(function () {
        $("#invoice").change(onSelectChangeInvoice);
        $("#paymentTypeForInvoiceGeneration").change(onSelectChangePaymentTypeForInvoiceGeneration);
        $("#accountNumberForInvoiceGeneration").keydown(function (event) {
            // Allow: backspace, delete, tab, escape, and enter
            if (event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 27 || event.keyCode === 13 ||
                    // Allow: Ctrl+A
                            //(event.keyCode == 65 && event.ctrlKey === true) ||
                                    // Allow: home, end, left, right
                                            (event.keyCode >= 35 && event.keyCode <= 39)) {
                                // let it happen, don't do anything
                                return;
                            } else {
                                // Ensure that it is a number and stop the keypress
                                var notNumber = (event.keyCode < 48 || event.keyCode > 57) || event.shiftKey;
                                notNumber = ((event.keyCode < 96 || event.keyCode > 105) && notNumber);
                                if (notNumber) {
                                    event.preventDefault();
                                }
                                ;
                            }
                            ;
                        });
                $("#accountNumberForInvoiceGeneration").keyup(function () {
                    var accountLength = $("#accountNumberForInvoiceGeneration").val();
                    if (accountLength.length === 4) {
                        $("#btnSaveInvoicePayment").removeAttr('disabled');
                    } else
                    {
                        $("#btnSaveInvoicePayment").attr('disabled', 'disabled');
                    }
                });

                onSelectChangeInvoice();
            });

    function onSelectChangeInvoice()
    {
        var selected = $("#invoice option:selected");
        if ((selected !== null) && (selected.attr("id") !== "NINV"))
        {
            $("#invoicing").val("true");
            $("#btnAddInvoicingPaymentType").removeAttr('disabled');
            $("#tableInvoicingPaymentType").css('display', 'inline');
        } else {
            $("#tableInvoicingPaymentType").css('display', 'none');
            $("#btnAddInvoicingPaymentType").attr('disabled', 'disabled');
            $("#invoicing").val("false");
            //$("#invoicingAction").val("deleteAll");
        }
    }

    function onSelectChangePaymentTypeForInvoiceGeneration() {
        var selected = $("#paymentTypeForInvoiceGeneration option:selected");
        var idSelected = selected.attr("id");
        if (idSelected !== null && (idSelected === "C" || idSelected === "N/A")) {
            $("#accountNumberForInvoiceGeneration").attr('disabled', 'disabled');
            $("#accountNumberForInvoiceGeneration").val("");
            $("#btnSaveInvoicePayment").removeAttr('disabled');
            $("#spanAccountNumber").text("");
        } else
        {
            $("#btnSaveInvoicePayment").attr('disabled', 'disabled');
            $("#accountNumberForInvoiceGeneration").removeAttr('disabled');
            $("#accountNumberForInvoiceGeneration").focus();
            $("#spanAccountNumber").text("<%=Languages.getString("jsp.admin.customers.merchants_add.paymentWarning1", SessionData.getLanguage())%>");
        }
    }


    <%
        }
    %>

</script>


<table width="100%" class="formArea2">	
    <%
        String Invoice_edit_title = Languages.getString("jsp.admin.customers.merchants_add.Invoice_edit", SessionData.getLanguage());
        String Invoice_delete_title = Languages.getString("jsp.admin.customers.merchants_add.Invoice_delete", SessionData.getLanguage());
        String Invoice_add_title = Languages.getString("jsp.admin.customers.merchants_add.Invoice_add", SessionData.getLanguage());
        String Invoice_save_title = Languages.getString("jsp.admin.customers.merchants_add.Invoice_save", SessionData.getLanguage());

        Vector paymentForInvoiceGeneration = Merchant.getPaymentTypesForInvoiceGeneration();
        Vector vecInvoiceTypes = Merchant.getInvoiceTypes();
        Iterator itInvoiceType = vecInvoiceTypes.iterator();
    %>
    <tr>
        <td class="main">
            <b><%=Languages.getString("jsp.admin.customers.merchants_add.Invoice_type", SessionData.getLanguage())%>:</b>
            <select name="invoice" id="invoice" >
                <%
                    while (itInvoiceType.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) itInvoiceType.next();
                        String strInvoiceTypeId = vecTemp.get(0).toString();
                        String strInvoiceTypeName = vecTemp.get(1).toString();
                        String strInvoiceCode = vecTemp.get(2).toString();
                        String strSelected = ((strInvoiceTypeId.equals(Integer.toString(Merchant.getInvoice()))) ? "SELECTED" : "");
                        out.println("<option id='" + strInvoiceCode + "' value=\"" + strInvoiceTypeId + "\" " + strSelected + ">" + strInvoiceTypeName + "</option>");
                    }
                %>
            </select>
            <input type="hidden" id=invoicing name="invoicing" value="<%=Merchant.isInvoicing()%>" >
        </td>		  		
    </tr>
    <tr>			
        <td>			
            <table id="tableInvoicingPaymentType" name="tableInvoicingPaymentType" width="70%"  >
                <tr>
                    <td id="tdWarnings" style="color:red" colspan="3">
                        <%=messageOrWarning%>
                    </td>
                </tr>
                <tr>
                    <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_add.paymentTypeInvoice", SessionData.getLanguage())%></td>
                    <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_add.paymentAccountInvoiceNumber", SessionData.getLanguage())%></td>
                    <td class="rowhead2"></td>
                </tr>

                <%
                    if (merchantPaymentsTypes != null) {
                        int classTr = 1;
                        int index = 0;
                        for (MerchantInvoicePayments invoiceTypeMerchant : merchantPaymentsTypes) {
                            if (invoiceTypeMerchant.getShowControls()) {
                                Iterator pIteratorNew = paymentForInvoiceGeneration.iterator();
                                StringBuilder comboInvoicePayments = new StringBuilder();
                                comboInvoicePayments.append("<select id=\"paymentTypeForInvoiceGeneration\" name=\"paymentTypeForInvoiceGeneration\">");
                                //System.out.println(invoiceTypeMerchant.getPaymentTypeId());
                                while (pIteratorNew.hasNext()) {
                                    Vector vecTemp = null;
                                    vecTemp = (Vector) pIteratorNew.next();
                                    String id = vecTemp.get(0).toString();
                                    String code = vecTemp.get(1).toString();
                                    String description = vecTemp.get(2).toString();
                                    if (invoiceTypeMerchant.getPaymentTypeId() != null && invoiceTypeMerchant.getPaymentTypeId().toString().equals(id)) {
                                        comboInvoicePayments.append("<option id=\"" + code + "\" value=\"" + id + "\" selected >" + description + "</option>");
                                    } else {
                                        comboInvoicePayments.append("<option id=\"" + code + "\" value=\"" + id + "\" >" + description + "</option>");
                                    }
                                }
                                comboInvoicePayments.append("</select>");

                %>
                <tr class="row<%=classTr%>">
                    <td align="center">
                        <%=comboInvoicePayments.toString()%>	
                    </td>
                    <td align="center">
                        <input type="text" id="accountNumberForInvoiceGeneration" name="accountNumberForInvoiceGeneration"  value="<%=invoiceTypeMerchant.getAccountNumber()%>"  maxlength="4" size="5"  />
                        <input type="hidden" id="typeInvoicingDescription" name="typeInvoicingDescription" value="">
                        <span id="spanAccountNumber" style="color:red"></span>
                    </td>
                    <td align="center" >
                        <table>
                            <tr>
                                <td><input id="btnSaveInvoicePayment" name="btnSaveInvoicePayment" type="button" 
                                           value="<%=Invoice_save_title%>" onclick="saveInvoicingPayment('<%=index%>')" /> </td>				                   
                            </tr>
                        </table>
                    </td>                                                        
                </tr>					 					
                <%
                } else {
                %>
                <tr class="row<%=classTr%>">
                    <td align="center"><%=invoiceTypeMerchant.getDescription()%></td>
                    <td align="center"><%=invoiceTypeMerchant.getAccountNumber()%></td>
                    <td align="center" >
                        <table>
                            <tr>
                                <td><img title="<%=Invoice_edit_title%>" src="images/icon_edit.gif" onclick="editInvoicingPayment('<%=index%>')"></td>
                                <td>&nbsp;</td>
                                <td><img title="<%=Invoice_delete_title%>" src="images/icon_delete.gif" onclick="deletePaymentTypeRecord('<%=index%>')"></td>
                            </tr>
                        </table>
                    </td> 
                </tr>
                <%
                            }
                            invoiceTypeMerchant.setShowControls(false);
                            index++;
                            if (classTr == 1) {
                                classTr = 2;
                            } else {
                                classTr = 1;
                            }
                        }
                    }
                %>	
                <tr>
                    <td align="right" colspan="3">
                        <input type="hidden" id="invoicingAction" name="invoicingAction" value="" />
                        <input type="hidden" id="invoicingAction_Index" name="invoicingAction_Index" value="" />
                        <%
                            if (showAddButton) {
                        %>
                        <input id="btnAddInvoicingPaymentType" name="btnAddInvoicingPaymentType" type="button" value="<%=Invoice_add_title%>" 
                               onclick="addPaymentTypeRecord()" <%=disabledControls%>>
                        <%
                            }
                        %>
                    </td>
                </tr>
            </table>
        </td>			
    </tr>

</table>
<SCRIPT>
    function saveInvoicingPayment(index)
    {
        var paymentTypeForInvoiceGeneration = $("#paymentTypeForInvoiceGeneration option:selected");
        document.getElementById('typeInvoicingDescription').value = paymentTypeForInvoiceGeneration.text();
        document.getElementById('mxContactAction').value = "--";
        document.getElementById('invoicingAction').value = "save";
        document.getElementById('invoicingAction_Index').value = index;
        document.formMerchant.onsubmit = new Function("return true;");
        document.formMerchant.Buttonsubmit.click();
        document.formMerchant.Buttonsubmit.disabled = true;
    }
    function editInvoicingPayment(index)
    {
        document.getElementById('mxContactAction').value = "--";
        document.getElementById('invoicingAction').value = "edit";
        document.getElementById('invoicingAction_Index').value = index;
        document.formMerchant.onsubmit = new Function("return true;");
        document.formMerchant.Buttonsubmit.click();
        document.formMerchant.Buttonsubmit.disabled = true;
        //alert("editInvoicingPayment");
    }
    function addPaymentTypeRecord()
    {
        document.getElementById('invoicingAction').value = "add";
        document.getElementById('mxContactAction').value = "--";
        document.getElementById('Buttonsubmit').value = "-";
        document.formMerchant.onsubmit = new Function("return true;");
        document.formMerchant.Buttonsubmit.click();
        document.formMerchant.Buttonsubmit.disabled = true;
    }
    function deletePaymentTypeRecord(index)
    {
        document.getElementById('mxContactAction').value = "--";
        document.getElementById('Buttonsubmit').value = "-";
        document.getElementById('invoicingAction').value = "delete";
        document.getElementById('invoicingAction_Index').value = index;
        document.formMerchant.onsubmit = new Function("return true;");
        document.formMerchant.Buttonsubmit.click();
        document.formMerchant.Buttonsubmit.disabled = true;
    }
</SCRIPT>
