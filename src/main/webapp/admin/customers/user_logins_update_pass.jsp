<%@ page import="com.debisys.users.User,
				com.debisys.languages.Languages,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*" %>
<%
int section=2;
int section_page=12;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:setProperty name="User" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
if (!User.checkPermissions(SessionData)){ //check permissions
	//response.sendRedirect("/support/message.jsp?message=1");
	%>
	<div class="error">
		<%=Languages.getString("jsp.message.msg1",SessionData.getLanguage()).toUpperCase()%>
	</div>
	<%
	return;
}


Hashtable userErrors = new Hashtable();
String strMessage = request.getParameter("message");
Vector vecUsers = new Vector();
int numWebPermissionTypes = User.getSequenceLastWebPermissionTypes();
boolean isUpdatePasswordSuccess=false;
if (strMessage == null){
	strMessage="";
}
if (request.getParameter("action") != null){
	try{
		if(request.getParameter("action").equals("updatePassword")){
			String passwordId = request.getParameter("passId");
			String password=request.getParameter("oldPassword");
			String mobilePermOn=request.getParameter("mobileOn");
			User.setpasswordId(passwordId);
			User.setPassword(password);
			if(mobilePermOn.equals("false")){
				if (User.validatePassword(SessionData, getServletContext(), request)){
					isUpdatePasswordSuccess=User.updatePassword(SessionData,application);
                    if(isUpdatePasswordSuccess) {
                    %>                                       
                    <div class="success">
                        <%=Languages.getString("jsp.admin.passwordchangesuccess",SessionData.getLanguage()).toUpperCase()%>
                    </div>
                    <%
                     }else{%>
                      <div class="error">
                        <%=Languages.getString("com.debisys.users.error_password_match",SessionData.getLanguage()).toUpperCase()%>
                    </div> 
                     <%  }

				}else if (User.isError()){
					Hashtable tempErrors = User.getErrors();
					if (tempErrors.containsKey("password")){
						%>
						<div class="error">
							<%=tempErrors.get("password").toString()%>
						</div>
						<%
					}
				}
			}else{
				if (User.validateMobilePassword(SessionData)){
					isUpdatePasswordSuccess=User.updatePassword(SessionData,application);
					if(isUpdatePasswordSuccess) {
                    %>                                       
                    <div class="success">
                        <%=Languages.getString("jsp.admin.passwordchangesuccess",SessionData.getLanguage()).toUpperCase()%>
                    </div>
                    <%
                     }else{%>
                      <div class="error">
                        <%=Languages.getString("com.debisys.users.error_password_match",SessionData.getLanguage()).toUpperCase()%>
                    </div> 
                     <%  }
				}else if (User.isError()){
					Hashtable tempErrors = User.getErrors();
					if (tempErrors.containsKey("password")){
						%>
						<div class="error">
							<%=tempErrors.get("password").toString()%>
						</div>
						<%
					}
				}
			}
		}else{
			%>
			<div class="error">
				<%=Languages.getString("jsp.admin.passwordchangefailed",SessionData.getLanguage()).toUpperCase()%>
			</div>
			<%
		}
	}catch (Exception e){
		%>
		<div class="error">
			<%=Languages.getString("jsp.admin.passwordchangefailed",SessionData.getLanguage()).toUpperCase()%>
		</div>
		<%
	}
}else{
	%>
	<div class="error">
		<%=Languages.getString("jsp.admin.passwordchangefailed",SessionData.getLanguage()).toUpperCase()%>
	</div>
<%
}
 %>