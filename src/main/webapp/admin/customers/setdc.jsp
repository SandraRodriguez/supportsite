<%@page import="com.debisys.languages.Languages"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
	if ( request.getParameter("d") != null )
	{
		if ( request.getParameter("d").equals("0") || request.getParameter("d").equals("1") )
		{
			application.setAttribute("deploymentType", request.getParameter("d"));
		}
	}
	if ( request.getParameter("c") != null )
	{
		if ( request.getParameter("c").equals("0") || request.getParameter("c").equals("1") )
		{
			application.setAttribute("customConfigType", request.getParameter("c"));
		}
	}
	if ( request.getParameter("l") != null )
	{
		if ( request.getParameter("l").equals("e") || request.getParameter("l").equals("s") )
		{
			if ( request.getParameter("l").equals("e") )
			{
				application.setAttribute("language", "english");
				SessionData.setLanguage("english");
				Languages.reloadLanguage("english", SessionData);
			}
			else
			{
				application.setAttribute("language", "spanish");
				SessionData.setLanguage("spanish");
				Languages.reloadLanguage("spanish", SessionData);
			}
		}
	}
	out.println("D" + application.getAttribute("deploymentType") + "C" + application.getAttribute("customConfigType") + "L" + application.getAttribute("language").toString().charAt(0) + "S" + SessionData.getLanguage().charAt(0));
%>
<form method=post><input type=hidden name=d value=0><input type=hidden name=c value=0><input type=submit value=00></form>
<form method=post><input type=hidden name=d value=1><input type=hidden name=c value=0><input type=submit value=10></form>
<form method=post><input type=hidden name=d value=1><input type=hidden name=c value=1><input type=submit value=11></form>
<form method=post><input type=hidden name=l value=e><input type=submit value=E></form>
<form method=post><input type=hidden name=l value=s><input type=submit value=S></form>
