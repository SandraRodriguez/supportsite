<%@ page import="com.debisys.utils.NumberUtil,
                 java.util.Hashtable"%>
<%
int section=2;
int section_page=6;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="page"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
  if (Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_CREDIT)
     ||Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_PREPAID)
     ||Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED))
  {
    if (!Merchant.getCurrentMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED) &&
        Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED))
    {
      if (!Merchant.checkRepCreditTypeUnlimited(SessionData, application))
      {
        response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=9");
        return;
      }
    }
    Merchant.updateMerchantType(SessionData, application);
  }
  response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId());


%>