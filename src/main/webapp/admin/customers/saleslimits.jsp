<%@page import="com.debisys.languages.Languages"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.utils.NumberUtil"%>
<%
  int section      = 2;
  int section_page = 4;
  boolean disableValidateSaleLimits = false;
  boolean isRepSharedBalance = false;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request"/>
<jsp:setProperty name="Merchant" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
	double dEffectiveCreditLimit = 0;

	response.setCharacterEncoding("utf-8");
	Merchant.getMerchant(SessionData,application);
	Rep.setRepId(Merchant.getRepId());
	Rep.getRep(SessionData,application);
	isRepSharedBalance = (Rep.getSharedBalance().equals("0"))?false:true;
	
	if( (Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_PREPAID)) || isRepSharedBalance)
	{
		disableValidateSaleLimits = true;
	}
%>
<hr width="50%">
<%
	if ( request.getParameter("action").equals("show") || request.getParameter("action").equals("save") || request.getParameter("action").equals("extension") )
	{
		if ( request.getParameter("action").equals("save") )
		{
			Merchant.setSalesLimitType(request.getParameter("salesLimitType"));
			Merchant.setSalesLimitMonday(Double.parseDouble(request.getParameter("salesLimitMonday")));
			Merchant.setSalesLimitTuesday(Double.parseDouble(request.getParameter("salesLimitTuesday")));
			Merchant.setSalesLimitWednesday(Double.parseDouble(request.getParameter("salesLimitWednesday")));
			Merchant.setSalesLimitThursday(Double.parseDouble(request.getParameter("salesLimitThursday")));
			Merchant.setSalesLimitFriday(Double.parseDouble(request.getParameter("salesLimitFriday")));
			Merchant.setSalesLimitSaturday(Double.parseDouble(request.getParameter("salesLimitSaturday")));
			Merchant.setSalesLimitSunday(Double.parseDouble(request.getParameter("salesLimitSunday")));
			Merchant.setMaximumSalesLimit(request);
			Merchant.updateSalesLimits(SessionData, application);
			Merchant.getMerchant(SessionData,application);
		}
		else if ( request.getParameter("action").equals("extension") )
		{
			Merchant.grantTemporaryExtension(SessionData, application, Double.parseDouble(request.getParameter("amount")));
			Merchant.getMerchant(SessionData,application);
		}
%>
<table>
<%
		if ( Merchant.getSalesLimitType() != null ) //If a Maximum Sales Limit has been configured
		{
			if ( (Double.parseDouble(Merchant.getLiabilityLimit()) - Double.parseDouble(Merchant.getRunningLiability())) < (Merchant.getSalesLimitLiabilityLimit() - Merchant.getSalesLimitRunnningLiability()) )
			{
				dEffectiveCreditLimit = Double.parseDouble(Merchant.getLiabilityLimit()) - Double.parseDouble(Merchant.getRunningLiability());
			}
			else
			{
				dEffectiveCreditLimit = Merchant.getSalesLimitLiabilityLimit() - Merchant.getSalesLimitRunnningLiability();
			}
%>

<%
			if(disableValidateSaleLimits)
			{	 
%>				
				<tr>
					<td align="center">
						<br/><%=Languages.getString("jsp.admin.saleslimit.sharedBalanceNotice",SessionData.getLanguage())%>
					</td>
				</tr>
<%
			}
			else
			{
%>


	<tr class="main"><td><b><%=Languages.getString("jsp.admin.saleslimit.effectiveCreditLimit",SessionData.getLanguage())%></b>&nbsp;<%=NumberUtil.formatCurrency(Double.toString(dEffectiveCreditLimit))%></td></tr>
	<tr class="main">
		<td><%=Languages.getString("jsp.admin.saleslimit.effectiveCreditLimitNotice1",SessionData.getLanguage())%><br><%=Languages.getString("jsp.admin.saleslimit.effectiveCreditLimitNotice2",SessionData.getLanguage())%></td>
	</tr>
	
<%
			}
		}
%>
<%
		if ( Merchant.getSalesLimitType() == null )
		{
%>
	<tr class="main">
		<td><%=Languages.getString("jsp.admin.saleslimit.featuredisabled",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;<input type="button" value="<%=Languages.getString("jsp.admin.saleslimit.edit",SessionData.getLanguage())%>" onclick="doSalesLimitEdit()"></td>
	</tr>
<%
		}
		else if ( Merchant.getSalesLimitType().equals("DAILY") )
		{
%>
	<tr class="main">
		<td><b><%=Languages.getString("jsp.admin.saleslimit.maxdailysaleslimit",SessionData.getLanguage())%></b>&nbsp;&nbsp;&nbsp;<input type="button" value="<%=Languages.getString("jsp.admin.saleslimit.edit",SessionData.getLanguage())%>" onclick="doSalesLimitEdit()"></td>
	</tr>
	<tr><td class="formArea2">
		<table cellspacing="0">
			<tr>
				<td class=""></td>
				<td class="">&nbsp;&nbsp;&nbsp;</td>
				<td class="rowhead2"><b><%=Languages.getString("jsp.admin.saleslimit.salesLimitMonday",SessionData.getLanguage()).toUpperCase()%></b></td><td class="rowhead2">&nbsp;&nbsp;</td>
				<td class="rowhead2" ><b><%=Languages.getString("jsp.admin.saleslimit.salesLimitTuesday",SessionData.getLanguage()).toUpperCase()%></b></td><td class="rowhead2">&nbsp;&nbsp;</td>
				<td class="rowhead2"><b><%=Languages.getString("jsp.admin.saleslimit.salesLimitWednesday",SessionData.getLanguage()).toUpperCase()%></b></td><td class="rowhead2" >&nbsp;&nbsp;</td>
				<td class="rowhead2"><b><%=Languages.getString("jsp.admin.saleslimit.salesLimitThursday",SessionData.getLanguage()).toUpperCase()%></b></td><td class="rowhead2">&nbsp;&nbsp;</td>
				<td class="rowhead2"><b><%=Languages.getString("jsp.admin.saleslimit.salesLimitFriday",SessionData.getLanguage()).toUpperCase()%></b></td><td class="rowhead2">&nbsp;&nbsp;</td>
				<td class="rowhead2"><b><%=Languages.getString("jsp.admin.saleslimit.salesLimitSaturday",SessionData.getLanguage()).toUpperCase()%></b></td><td class="rowhead2">&nbsp;&nbsp;</td>
				<td class="rowhead2"><b><%=Languages.getString("jsp.admin.saleslimit.salesLimitSunday",SessionData.getLanguage()).toUpperCase()%></b></td><td class="rowhead2">&nbsp;&nbsp;</td>
			</tr>
			<tr class="main">
				<td><b><%=Languages.getString("jsp.admin.saleslimit.maxsaleslimit",SessionData.getLanguage())%></b></td>
				<td></td>
				<td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitMonday()))%></td><td></td>
				<td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitTuesday()))%></td><td></td>
				<td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitWednesday()))%></td><td></td>
				<td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitThursday()))%></td><td></td>
				<td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitFriday()))%></td><td></td>
				<td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitSaturday()))%></td><td></td>
				<td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitSunday()))%></td>
			</tr>
<%
			//Now determine the current day in order to generate the correct HTML padding to make data look nice
			int nDayToPrint = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
			int nDayCounter = 2;
			String sRowPadding = "";

			//We start at 2 because our first day of week is Monday=2,
			//and since we need to print from Monday then we change the expected value of Sunday from 1 to 8
			if ( nDayToPrint == 1 )
			{
				nDayToPrint = 8;
			}

			for ( ; nDayCounter < nDayToPrint; nDayCounter++ )
			{
				sRowPadding += "<td></td><td></td>";
			}
%>
			<tr class="main">
				<td><i><%=Languages.getString("jsp.admin.saleslimit.effectiveCreditLimit",SessionData.getLanguage())%></i></td><td></td>
				<%=sRowPadding%><td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitLiabilityLimit()))%></td>
			</tr>
			<tr class="main">
				<td><i><%=Languages.getString("jsp.admin.saleslimit.currentVolumeNotice",SessionData.getLanguage())%></i></td><td></td>
				<%=sRowPadding%><td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitRunnningLiability()))%></td>
			</tr>
			<tr class="main">
				<td><b><%=Languages.getString("jsp.admin.saleslimit.remainingAmount",SessionData.getLanguage())%></b></td><td></td>
				<%=sRowPadding%><td align="center"><%=NumberUtil.formatCurrency(Double.toString(Merchant.getSalesLimitLiabilityLimit() - Merchant.getSalesLimitRunnningLiability()))%></td><td></td>
			</tr>
			<tr><td colspan="15" align="center"><hr></td></tr>
			<tr class="main"><td><b><%=Languages.getString("jsp.admin.saleslimit.temporaryExtension",SessionData.getLanguage())%></b></td></tr>
			<tr class="main"><td colspan="9"><%=Languages.getString("jsp.admin.saleslimit.amount",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;<input type="text" id="txtSalesLimitExtension" value="0" size="5"></td></tr>
			<tr class="main">
				<td colspan="9"><input type="button" value="<%=Languages.getString("jsp.admin.saleslimit.grantTemporaryExtension",SessionData.getLanguage())%>" onclick="doSalesLimitExtension()"></td>
			</tr>
		</table>
	</td></tr>
<%
		}
%>
</table>
<script>
function doSalesLimitEdit()
{
	$("#divSalesLimits").html('<br><span class="main"><%=Languages.getString("jsp.admin.saleslimit.loadingSalesLimits",SessionData.getLanguage())%></span>');
	window.setTimeout("$('#divSalesLimits').load('admin/customers/saleslimits.jsp?action=edit&merchantId=<%=Merchant.getMerchantId()%>&Random=" + Math.random() + "')", 1000);
}

function doSalesLimitExtension()
{
	var TempExtensionCtl = document.getElementById("txtSalesLimitExtension");
	var SalesLimitLiabilityLimit = parseFloat(<%=Merchant.getSalesLimitLiabilityLimit()%>);
	var LiabilityLimit = parseFloat(<%=Merchant.getLiabilityLimit()%>);
	var NumExpression = new RegExp(/^[0-9\.]+$/);
	var disableValidation = <%=disableValidateSaleLimits%>;

	if ( !NumExpression.test(TempExtensionCtl.value) )
	{
		alert("<%=Languages.getString("jsp.admin.saleslimit.enterValidTemporaryExtension",SessionData.getLanguage())%>");
		window.setTimeout("document.getElementById('" + TempExtensionCtl.id + "').focus();document.getElementById('" + TempExtensionCtl.id + "').select();", 50);
		return;
	}

	if (!disableValidation && (!( TempExtensionCtl.value > 0 && (SalesLimitLiabilityLimit + parseFloat(TempExtensionCtl.value)) <= LiabilityLimit )))
	{
		alert("<%=Languages.getString("jsp.admin.saleslimit.enterValidTemporaryExtensionLimit",SessionData.getLanguage())%>" + Math.round((LiabilityLimit - SalesLimitLiabilityLimit)*100)/100);
		window.setTimeout("document.getElementById('" + TempExtensionCtl.id + "').focus();document.getElementById('" + TempExtensionCtl.id + "').select();", 50);
		return;
	}
	
	if ( confirm("<%=Languages.getString("jsp.admin.saleslimit.confirmTemporaryExtension",SessionData.getLanguage())%>") )
	{
		$("#divSalesLimits").html('<br><span class="main"><%=Languages.getString("jsp.admin.saleslimit.loadingSalesLimits",SessionData.getLanguage())%></span>');
		window.setTimeout("$('#divSalesLimits').load('admin/customers/saleslimits.jsp?action=extension&amount=" + TempExtensionCtl.value + "&merchantId=<%=Merchant.getMerchantId()%>&Random=" + Math.random() + "')", 1000);
	}
}
</script>
<%
	}//End of if ( request.getParameter("action").equals("show") )
	else if ( request.getParameter("action").equals("edit") ) //If the user asks to edit the Sales Limits
	{
		LogChanges.setSession(SessionData);
		LogChanges.setContext(application);
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTYPE), (Merchant.getSalesLimitType() == null)?"OFF":Merchant.getSalesLimitType());
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITLIABILITYLIMIT), String.valueOf(Merchant.getSalesLimitLiabilityLimit()));
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITMONDAY), String.valueOf(Merchant.getSalesLimitMonday()));
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTUESDAY), String.valueOf(Merchant.getSalesLimitTuesday()));
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITWEDNESDAY), String.valueOf(Merchant.getSalesLimitWednesday()));
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTHURSDAY), String.valueOf(Merchant.getSalesLimitThursday()));
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITFRIDAY), String.valueOf(Merchant.getSalesLimitFriday()));
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITSATURDAY), String.valueOf(Merchant.getSalesLimitSaturday()));
		LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITSUNDAY), String.valueOf(Merchant.getSalesLimitSunday()));
%>
<table>
	<tr class="main">
		<td>
			<label><input type="checkbox" id="salesLimitEnable" onclick="SwitchSalesLimit(this);" <%=(Merchant.getSalesLimitType() == null)?"":"CHECKED"%>><%=Languages.getString("jsp.admin.saleslimit.enableSalesLimit",SessionData.getLanguage())%></label>
		</td>
	</tr>
	<tr>
		<td id="tdSalesLimit" class="formArea2" colspan="2">
			<table>
				<tr class="main" id="trSalesLimit" style="display:none;">
					<td nowrap>
						<%=Languages.getString("jsp.admin.saleslimit.salesLimitType",SessionData.getLanguage())%>&nbsp;&nbsp;
						<select id="salesLimitType">
							<option value="DAILY"><%=Languages.getString("jsp.admin.saleslimit.salesLimitTypeDAILY",SessionData.getLanguage())%></option>
						</select>
					</td>
				</tr>
				<tr class="main" id="trSalesLimitDaily" style="display:none;">
					<td nowrap colspan="2">
						<table cellspacing="0">
							<tr  class="rowhead2">
								<td class="rowhead2"><%=Languages.getString("jsp.admin.saleslimit.salesLimitMonday",SessionData.getLanguage()).toUpperCase()%></td><td class="rowhead2">&nbsp;</td>
								<td class="rowhead2"><%=Languages.getString("jsp.admin.saleslimit.salesLimitTuesday",SessionData.getLanguage()).toUpperCase()%></td><td class="rowhead2">&nbsp;</td>
								<td class="rowhead2"><%=Languages.getString("jsp.admin.saleslimit.salesLimitWednesday",SessionData.getLanguage()).toUpperCase()%></td><td class="rowhead2">&nbsp;</td>
								<td class="rowhead2"><%=Languages.getString("jsp.admin.saleslimit.salesLimitThursday",SessionData.getLanguage()).toUpperCase()%></td><td class="rowhead2">&nbsp;</td>
								<td class="rowhead2"><%=Languages.getString("jsp.admin.saleslimit.salesLimitFriday",SessionData.getLanguage()).toUpperCase()%></td><td class="rowhead2">&nbsp;</td>
								<td class="rowhead2"><%=Languages.getString("jsp.admin.saleslimit.salesLimitSaturday",SessionData.getLanguage()).toUpperCase()%></td class="rowhead2"><td>&nbsp;</td>
								<td class="rowhead2"><%=Languages.getString("jsp.admin.saleslimit.salesLimitSunday",SessionData.getLanguage()).toUpperCase()%></td><td>&nbsp;</td>
							</tr>
							<tr>
								<td><input type="text" id="salesLimitMonday" value="<%=NumberUtil.formatAmount(Double.toString(Merchant.getSalesLimitMonday()))%>" maxlength="7" size="5"></td><td></td>
								<td><input type="text" id="salesLimitTuesday" value="<%=NumberUtil.formatAmount(Double.toString(Merchant.getSalesLimitTuesday()))%>" maxlength="7" size="5"></td><td></td>
								<td><input type="text" id="salesLimitWednesday" value="<%=NumberUtil.formatAmount(Double.toString(Merchant.getSalesLimitWednesday()))%>" maxlength="7" size="5"></td><td></td>
								<td><input type="text" id="salesLimitThursday" value="<%=NumberUtil.formatAmount(Double.toString(Merchant.getSalesLimitThursday()))%>" maxlength="7" size="5"></td><td></td>
								<td><input type="text" id="salesLimitFriday" value="<%=NumberUtil.formatAmount(Double.toString(Merchant.getSalesLimitFriday()))%>" maxlength="7" size="5"></td><td></td>
								<td><input type="text" id="salesLimitSaturday" value="<%=NumberUtil.formatAmount(Double.toString(Merchant.getSalesLimitSaturday()))%>" maxlength="7" size="5"></td><td></td>
								<td><input type="text" id="salesLimitSunday" value="<%=NumberUtil.formatAmount(Double.toString(Merchant.getSalesLimitSunday()))%>" maxlength="7" size="5"></td>
							</tr>
<%
		if ( Merchant.getSalesLimitType() != null )
		{
%>
							<tr class="main">
								<td colspan="13"><br/><%=Languages.getString("jsp.admin.saleslimit.noticechangenextcycle",SessionData.getLanguage())%></td>
							</tr>
<%
		}
%>
						</table>
						<input type="hidden" id="realCreditLimit" value="<%=Merchant.getLiabilityLimit()%>">
					</td>
				</tr>
<%
	if(disableValidateSaleLimits)
	{	 
%>				
				<tr>
					<td align="center">
						<br/><%=Languages.getString("jsp.admin.saleslimit.sharedBalanceNotice",SessionData.getLanguage())%>
					</td>
				</tr>
<%
	}
%>
				<tr>
					<td align="center">
						<input type="button" value="<%=Languages.getString("jsp.admin.saleslimit.save",SessionData.getLanguage())%>" onclick="doSalesLimitSave()">
						&nbsp;
						<input type="button" value="<%=Languages.getString("jsp.admin.saleslimit.cancel",SessionData.getLanguage())%>" onclick="doSalesLimitCancel()">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script>
function SwitchSalesLimit(chkSalesLimitType)
{
	document.getElementById("tdSalesLimit").className = (chkSalesLimitType.checked)?"formArea2":"";
	document.getElementById("trSalesLimit").style.display = (chkSalesLimitType.checked)?"block":"none";
	document.getElementById("trSalesLimitDaily").style.display = (chkSalesLimitType.checked)?"block":"none";
}

function doSalesLimitSave()
{
	var sURL = "&merchantId=<%=Merchant.getMerchantId()%>";

	if ( document.getElementById('salesLimitEnable').checked )
	{
		if ( !ValidateDailySalesLimit(document.getElementById('salesLimitMonday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitMonday",SessionData.getLanguage())%>') ) return;
		if ( !ValidateDailySalesLimit(document.getElementById('salesLimitTuesday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitTuesday",SessionData.getLanguage())%>') ) return;
		if ( !ValidateDailySalesLimit(document.getElementById('salesLimitWednesday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitWednesday",SessionData.getLanguage())%>') ) return;
		if ( !ValidateDailySalesLimit(document.getElementById('salesLimitThursday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitThursday",SessionData.getLanguage())%>') ) return;
		if ( !ValidateDailySalesLimit(document.getElementById('salesLimitFriday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitFriday",SessionData.getLanguage())%>') ) return;
		if ( !ValidateDailySalesLimit(document.getElementById('salesLimitSaturday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitSaturday",SessionData.getLanguage())%>') ) return;
		if ( !ValidateDailySalesLimit(document.getElementById('salesLimitSunday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitSunday",SessionData.getLanguage())%>') ) return;
		sURL += "&salesLimitType=" + document.getElementById('salesLimitType').value;
		sURL += "&salesLimitMonday=" + document.getElementById('salesLimitMonday').value;
		sURL += "&salesLimitTuesday=" + document.getElementById('salesLimitTuesday').value;
		sURL += "&salesLimitWednesday=" + document.getElementById('salesLimitWednesday').value;
		sURL += "&salesLimitThursday=" + document.getElementById('salesLimitThursday').value;
		sURL += "&salesLimitFriday=" + document.getElementById('salesLimitFriday').value;
		sURL += "&salesLimitSaturday=" + document.getElementById('salesLimitSaturday').value;
		sURL += "&salesLimitSunday=" + document.getElementById('salesLimitSunday').value;
		$("#divSalesLimits").html('<br><span class="main"><%=Languages.getString("jsp.admin.saleslimit.loadingSalesLimits",SessionData.getLanguage())%></span>');
		window.setTimeout("$('#divSalesLimits').load('admin/customers/saleslimits.jsp?action=save" + sURL + "&Random=" + Math.random() + "')", 1000);
	}
	else
	{
		sURL += "&salesLimitType=";
		sURL += "&salesLimitMonday=0";
		sURL += "&salesLimitTuesday=0";
		sURL += "&salesLimitWednesday=0";
		sURL += "&salesLimitThursday=0";
		sURL += "&salesLimitFriday=0";
		sURL += "&salesLimitSaturday=0";
		sURL += "&salesLimitSunday=0";
		$("#divSalesLimits").html('<br><span class="main"><%=Languages.getString("jsp.admin.saleslimit.loadingSalesLimits",SessionData.getLanguage())%></span>');
		window.setTimeout("$('#divSalesLimits').load('admin/customers/saleslimits.jsp?action=save" + sURL + "&Random=" + Math.random() + "')", 1000);
	}
}

function doSalesLimitCancel()
{
	$("#divSalesLimits").html('<br><span class="main"><%=Languages.getString("jsp.admin.saleslimit.loadingSalesLimits",SessionData.getLanguage())%></span>');
	window.setTimeout("$('#divSalesLimits').load('admin/customers/saleslimits.jsp?action=show&merchantId=<%=Merchant.getMerchantId()%>&Random=" + Math.random() + "')", 1000);
}

function ValidateDailySalesLimit(DailyLimitCtl, sDayText)
{
	var NumExpression = new RegExp(/^[0-9\.]+$/);
	var limit = parseFloat(document.getElementById('realCreditLimit').value);
	var disableValidation = <%=disableValidateSaleLimits%>;
	
	if(disableValidation) return true;				
	
	
	// Disable validation when the Merchatn

	if ( !NumExpression.test(DailyLimitCtl.value) )
	{
		var str = '<%=Languages.getString("jsp.admin.saleslimit.salesLimitInvalid",SessionData.getLanguage())%>';
		str = str.replace("_DAY_", sDayText);
		alert(str);
		window.setTimeout("document.getElementById('" + DailyLimitCtl.id + "').focus();document.getElementById('" + DailyLimitCtl.id + "').select();", 50);
		return false;
	}

	if ( parseFloat(DailyLimitCtl.value) > limit )
	{
		var str = '<%=Languages.getString("jsp.admin.saleslimit.salesLimitLowerThanLimit",SessionData.getLanguage())%>';
		str = str.replace("_VALUE_", limit);
		str = str.replace("_DAY_", sDayText);
		alert(str);
		window.setTimeout("document.getElementById('" + DailyLimitCtl.id + "').focus();document.getElementById('" + DailyLimitCtl.id + "').select();", 50);
		return false;
	}

	return true;
}

window.setTimeout("document.getElementById('salesLimitEnable').onclick()", 500);
</script>
<%
	} //End of else if ( request.getParameter("action").equals("edit") )
%>
