<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.util.*,	
                 com.debisys.customers.*,
                 com.debisys.reports.pojo.*,	
                 com.debisys.languages.Languages,
                 com.debisys.utils.ColumnReport,
                 com.debisys.schedulereports.ScheduleReport" %>
                 
<%@page import="com.debisys.users.User"%>
<%
int section=2;
int section_page=4;
String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>

<script charset="utf-8" type="text/javascript" src="/support/includes/jquery.js" language="JavaScript"></script>
<script charset="utf-8" type="text/javascript" src="/support/includes/jquery-ui.js" language="JavaScript"></script>

<script type="text/javascript" charset="utf-8">
	function redirectParentPage(){	 
	  window.opener.location = "<%= DebisysConstants.PAGE_TO_SCHEDULE_REPORTS %>";
	  window.close();	 
	}
</script>


<%
	Vector vecSearchResults = new Vector();
	Hashtable searchErrors = null;
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 25;
	int intPageCount = 1;
	String merchantId = "";
	String businessName = "";
	ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
	ArrayList<String> titles = new ArrayList<String>();
	String keyLanguage = "jsp.admin.customers.merchants_credit_history.title";
	String downloadParam = request.getParameter("download");
	
	int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays( DebisysConstants.SC_MER_CRED_HISTORY, application );
	boolean isInternational = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
										DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	
	
	boolean bIsExternalRep = com.debisys.users.User.isExternalRepAllowedEnabled(SessionData);
	boolean bShowLabelLimitDays = false;

	if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
	{
		SessionData.setProperty("iso_id", (new User()).getISOId(DebisysConstants.MERCHANT, Merchant.getMerchantId()));
		SessionData.setProperty("iso_id", SessionData.getUser().getIsoId());
	}


	if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
	{
	  merchantId = request.getParameter("merchantId");
	}
	else
	{
	  merchantId = SessionData.getProperty("ref_id");
	  Merchant.setMerchantId(merchantId);
	}

	String strUrlParams = "?merchantId=" + merchantId + "&startDate=" + Merchant.getStartDate() + "&endDate=" + Merchant.getEndDate();
		
		
	if (merchantId != null)
	{
		  Merchant.getMerchant(SessionData, application);	
		  businessName = Merchant.getBusinessName();
		  if (request.getParameter("page") != null)
		  {
		    try
		    {
		      intPage=Integer.parseInt(request.getParameter("page"));
		    }
		    catch(NumberFormatException ex)
		    {
		      intPage = 1;
		    }
		  }
	
		  if (intPage < 1)
		  {
		    intPage=1;
		  }
	  
	  	  SessionData.setProperty("reportId", DebisysConstants.SC_MER_CRED_HISTORY);
	      titles.add( Languages.getString(keyLanguage,SessionData.getLanguage()) );
	      titles.add( businessName );
	      
		  if(request.getParameter("user") == null)
		  {
			if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{  
				// If columns change remember to change getUserMerchantCreditsMx
			    vecSearchResults = Merchant.getMerchantCreditsMx(intPage, intPageSize, SessionData, application, false);
			}
			else
			{						      
			    headers = RepReports.getHeadersMerchantsCreditsHistory(SessionData, application, bIsExternalRep);  
			    
			    String sc = request.getParameter("sheduleReport");
				if ( sc != null && sc.equals("y")  )
				{	  		
				  		//SCHEDULE REPORT AND REDIRECT	  		
				  		Merchant.getMerchantCredits(intPage, intPageSize, SessionData, application, false, DebisysConstants.SCHEDULE_REPORT, headers, titles);	
				  		ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
						if (  scheduleReport != null  )
						{				
							scheduleReport.setStartDateFixedQuery( Merchant.getStartDate() );
							scheduleReport.setEndDateFixedQuery( Merchant.getEndDate() );
							scheduleReport.setTitleName( keyLanguage ); 
							scheduleReport.setAdditionalData(businessName);   
						}	
						SessionData.setProperty("start_date", Merchant.getStartDate());
			      		SessionData.setProperty("end_date", Merchant.getEndDate());			
				  		%>
				  		<script type="text/javascript" charset="utf-8">
							$(document).ready(function() 
								{
									redirectParentPage();						
								}
							);	
						</script>	  		
				  		<% 
			   }
			   else if ( downloadParam != null )
			   {
			     Merchant.getMerchantCredits(intPage, intPageSize, SessionData, application, false, DebisysConstants.DOWNLOAD_REPORT, headers, titles);		  		  		
			  	 response.sendRedirect( Merchant.getStrUrlLocation() );
			  	 
			   }	
			   else
			   {
                                vecSearchResults = Merchant.getMerchantCredits(intPage, intPageSize, SessionData, application, false, DebisysConstants.EXECUTE_REPORT, headers, titles);                                
			   }  
			}
		  } 
		  else 
		  {
		  	String startDate = request.getParameter("startDate");
		  	String endDate = request.getParameter("endDate");
		  	int daysBetween = DateUtil.getDaysBetween(endDate, startDate);		
			if (daysBetween > limitDays)
			{
				bShowLabelLimitDays = true;
			}				
			if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) 
			{  
			   // If columns change remember to change getMerchantCreditsMx
			   vecSearchResults = Merchant.getUserMerchantCreditsMx(intPage, intPageSize, SessionData, application, false);
			}
			else
			{
				if (downloadParam != null) {
					headers = RepReports.getHeadersMerchantsCreditsHistory(SessionData, application, bIsExternalRep);
					String url = Merchant.getUserMerchantCreditDownload(SessionData, application, headers, titles, false);
					response.sendRedirect(url);
				}
  
			   vecSearchResults = Merchant.getUserMerchantCredits(intPage, intPageSize, SessionData, application, false);
			}
			strUrlParams += "&user=YES";
		  }  
	    
	      if ( vecSearchResults != null && vecSearchResults.size()>0 )
	      {
			  intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
			  vecSearchResults.removeElementAt(0);
			  if (intRecordCount>0 && intRecordCount != intPageSize )
			  {		  	  
			      intPageCount = (intRecordCount / intPageSize) + 1;
			      if ((intPageCount * intPageSize) + 1 >= intRecordCount)
			      {
			        intPageCount++;
			      }
			  }
		  }
	 }
	 else
	 {
	  searchErrors = Merchant.getErrors();
	 }

%>
<html>
  <head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString(keyLanguage,SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="1700" background="../../images/top_blue.gif">
	<tr>
    <td width="18" height="20" align="left"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" align="left" width="2000"><%=Languages.getString("jsp.admin.customers.merchants_credit_history.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20" align="right"><img src="../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
  <tr>
    <td class="main">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" class="main">
<table width="400">
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
</table>
               </td>
              </tr>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
	<form name="mainform" id="mainform" method="post" action="merchants_credit_history.jsp<%=strUrlParams%>" onSubmit="scroll();">
	
            <table width="70%" border="0" cellspacing="0" cellpadding="2">
            	 <tr>
	            	<td><%=businessName%></td>
	            </tr> 	
            	<%if (bShowLabelLimitDays){ %>
            	 <tr>
            	  <td>
            	 	<Font style="color: red">
                	* <%=Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage())%>
                                                <%=" "+limitDays+" "%>
                                                <%=Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage())%>
                 	</Font> 
            	  </td>
            	 </tr>                 
                 <%}%>  
            
            <tr>
            	<td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage())+"." + Languages.getString("jsp.admin.displaying", new Object []{Integer.toString(intPage), Integer.toString(intPageCount-1)},SessionData.getLanguage())%></td>
            </tr>
            <tr>
              <td align="right" class="main" nowrap="nowrap">
              <%              
              if (intPage > 1)
              {
                out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams +"&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams +"&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams + "&page=" + i + "\">" + i + "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams + "&page=" + (intPage+1) + "\">"+Languages.getString("jsp.admin.next",SessionData.getLanguage())+"&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"merchants_credit_history.jsp" + strUrlParams + "&page=" + (intPageCount-1) + "\">"+Languages.getString("jsp.admin.last",SessionData.getLanguage())+"</a>");
              }

              %>
              </td>
            </tr>
            </table>
            <table cellspacing="1" cellpadding="2" width="70%">
            <tr>
            <%
               if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {  
            	   out.println("<td class=rowhead2>#</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.user",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.user.dba",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.desc",SessionData.getLanguage()).toUpperCase() + "</td>");
                    
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.bank",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.reference_number",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.deposit_date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2 style=\"width:300px;\">" + Languages.getString("jsp.admin.customers.merchants_credit_history.concept",SessionData.getLanguage()).toUpperCase() + "</td>");
                    
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.paymentType",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.new_avail_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.commission",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.net_payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    if(bIsExternalRep) {
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.extern.RepresentativeType",SessionData.getLanguage()).toUpperCase() + "</td>");
                    }
               }else{
            	    out.println("<td class=rowhead2>#</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.date",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.user",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.user.dba",SessionData.getLanguage()) + "</td>");
                    out.println("<td class=rowhead2 style=\"width:300px;\">" + Languages.getString("jsp.admin.customers.merchants_credit_history.desc",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.payment",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.new_avail_credit",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.commission",SessionData.getLanguage()).toUpperCase() + "</td>");
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.merchants_credit_history.net_payment",SessionData.getLanguage()).toUpperCase() + "</td>"); 
                    if(bIsExternalRep) {           
                    out.println("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.extern.RepresentativeType",SessionData.getLanguage()).toUpperCase() + "</td>");
                    }
               }
            %> 
            </tr>
            <%
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  int count = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {  
                        out.println("<tr class=row" + intEvenOdd +">" +
                        		"<td>" + count+ "</td>" +   
                                    "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                    "<td>" + vecTemp.get(1) + "</td>" +
                                    "<td>" + vecTemp.get(11) + "</td>" +
                                    "<td>" + vecTemp.get(2) + "</td>" +
                                    "<td>" + vecTemp.get(3) + "</td>" +
                                    "<td>" + vecTemp.get(4) + "</td>" +
                                    // Date modified for localization - SW  
                                    "<td>" + vecTemp.get(5).toString() + "</td>" +
                                    "<td>" + vecTemp.get(6) + "</td>" +
                                    "<td>" + vecTemp.get(7) + "</td>" +
                                    "<td>" + vecTemp.get(12) + "</td>" + //paymentType 
                                    "<td>" + vecTemp.get(8) + "</td>" +
                                    "<td>" + vecTemp.get(9) + "</td>" + // Commission comes formated from the data retriver method
                                    "<td>" + vecTemp.get(10) + "</td>");
                                    if(bIsExternalRep) {
                                    out.println("<td>" + vecTemp.get(13) + "</td>"); 
                                    }
                           out.println("</tr>");
                            //DateUtil.formatDateTime(vecTemp.get(5).toString())
                    }else{
                        out.println("<tr class=row" + intEvenOdd +">" +
                        		    "<td>" + count+ "</td>" +    
                                    "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                                    "<td>" + vecTemp.get(1) + "</td>" +
                                    "<td>" + vecTemp.get(7) + "</td>" +
                                    "<td>" + vecTemp.get(2) + "</td>" +
                                    "<td>" + vecTemp.get(3) + "</td>" +
                                    "<td>" + vecTemp.get(4) + "</td>" +
                                    // formatting of COMMISSION delayed until presentation layer
                                    "<td>" + com.debisys.utils.NumberUtil.formatAmount(vecTemp.get(5).toString()) + "%</td>" +
                                    "<td>" + vecTemp.get(6) + "</td>");
                                      if(bIsExternalRep) {
                                    out.println("<td>" + vecTemp.get(8) + "</td>");
                                    } 
                           out.println("</tr>");                      
                    }
                    count++;
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            
            <tr>
	          	<td>
	          		<input type="button" value="<%=Languages.getString("jsp.admin.reports.Print_This_Page",SessionData.getLanguage())%>" onclick="window.print()">
	          	</td>
	          	<% if ( isInternational )
	          	{%>
	          	<td>
	          		<input type="submit" name="download" value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
	          	</td>
	          	
	          	<jsp:include page="/admin/reports/schreportoption.jsp">
			  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
			 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
				</jsp:include>
				<%} %>            	
       		</tr>       		
            </table>
       </form>     

<%
}
else if (intRecordCount==0 && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.customers.merchants_credit_history.not_found",SessionData.getLanguage())+"</font>");
}
%>

</td>
      </tr>
    </table>
</td>
</tr>
</table>
</body>
</html>
