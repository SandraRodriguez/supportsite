<%@ page import="java.util.Vector,
                 com.debisys.utils.*,
                 com.debisys.languages.Languages" %>
<%
int section=2;
int section_page=15;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<html>
<head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
<%
boolean bShared = false;
if ( request.getParameter("bShared") != null )
{
	bShared = request.getParameter("bShared").equals("1");
}
else if ( (request.getParameter("bNewShared") != null) && (request.getParameter("sRepMerchantMigration") != null) )
{//Else we need to process a Merchant migration
	Rep.getRep(SessionData, application);
	Rep.doRepMerchantMigration(SessionData, application, request.getParameter("sRepMerchantMigration"), (request.getParameter("bNewShared").equals("1"))?true:false);
    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId());
    return;
}//End of else we need to process a Merchant migration
%>
    <title><%=Languages.getString("jsp.admin.customers.reps.merchantmigration.title",SessionData.getLanguage())%></title>
</head>
<body bgcolor="#ffffff" onload="window.focus()">
<%
Vector vResults = new Vector();

if ( request.getParameter("repId") != null )
{
	Rep.getRep(SessionData, application);
	vResults = Rep.getRepMerchants(SessionData, application);
}
%>
  <script>
  function ValidateLimitValue(cText)
  {
    var sText = cText.value;
    var sResult = "";
    var bHasPeriod = false;
    for ( i = 0; i < sText.length; i++ )
    {
      if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) )
      {
        sResult += sText.charAt(i);
      }
      if ( !bHasPeriod && sText.charCodeAt(i) == 46 )/*period*/
      {
        sResult += sText.charAt(i);
        bHasPeriod = true;
      }
    }
    if ( sResult.length == 0 )
    {
      sResult = "0";
    }
    if ( sText != sResult )
    {
      cText.value = sResult;
    }
  }//End of function ValidateLimitValue

  function SumLimit(nIndex)
  {
    if ( parseFloat(document.getElementById("txt_" + nIndex).value) < parseFloat(document.getElementById("run_" + nIndex).value) && <%= !(Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) && Rep.getSharedBalance().equals("1"))%>)
    {
      alert("<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.limitlowerthanrunning",SessionData.getLanguage())%>");
      document.getElementById("txt_" + nIndex).value = document.getElementById("run_" + nIndex).value;
      document.getElementById("txt_" + nIndex).focus();
      document.getElementById("txt_" + nIndex).select();
      SumTotal(true);
      return false;
    }

    SumTotal(true);
    return true;
  }//End of function SumLimit

  function SumTotal(bSilent)
  {
    var nMerchants = <%=vResults.size()%>;
    var dTotal = 0.0;
    var i = 0;
    
    for (i = 0; i < nMerchants; i++)
    {
      if ( !bSilent && (parseFloat(document.getElementById("txt_" + i).value) < parseFloat(document.getElementById("run_" + i).value))&& <%=!(Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) %> )
      {
        alert("<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.limitlowerthanrunning",SessionData.getLanguage())%>");
        document.getElementById("txt_" + i).focus();
        document.getElementById("txt_" + i).select();
        return false;
      }
      dTotal += parseFloat(document.getElementById("txt_" + i).value);
    }
    document.getElementById("txt_Total").value = FormatAmount(dTotal);

    if ( (dTotal > document.getElementById("txt_RepAvailable").value)&& <%=!(Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) %> )
    {
      alert("<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.totallimitgreaterthanrepavailable",SessionData.getLanguage())%>");
      return false;
    }


    return true;
  }//End of function SumTotal
  
  function ValidatePayment()
  {
    var nMerchants = <%=vResults.size()%>;
    var dTotal = 0.0;
    var i = 0;
    var valid = true;
    
    for (i = 0; i < nMerchants; i++)
    {
      dTotal += parseFloat(document.getElementById("txt_" + i).value);
    }
    document.getElementById("txt_Total").value = FormatAmount(dTotal);

    if ((dTotal != document.getElementById("txt_RepAvailable").value)&& <%=(Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) %>){
      alert("<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.totallimitdiferentthanrepavailable",SessionData.getLanguage())%>");
      valid = false;    
    }
    
    if (SumTotal(false)&&valid)
    {
      var nMerchants = <%=vResults.size()%>;
      var sText = "&";
      var i = 0;
      for (i = 0; i < nMerchants; i++)
      {
        sText += document.getElementById("mer_" + i).value + ":";
        if ( document.getElementById("rbtnPre_" + i).checked )
        {
          sText += "1:";
        }
        else
        {
          sText += "2:";
        }
        sText += document.getElementById("txt_" + i).value + "&";
      }
      self.opener.SetRepMerchantMigration(sText);
      window.close();
    }
  }//End of function ValidatePayment
  
  function ValidateMigration()
  {
    var nMerchants = <%=vResults.size()%>;
    var sText = "&";
    var i = 0;
    for (i = 0; i < nMerchants; i++)
    {
      sText += document.getElementById("mer_" + i).value + ":_&";
    }
    self.opener.SetRepMerchantMigration(sText);
    window.close();
  }//End of function ValidateMigration
  
  function CancelPayment()
  {
    self.opener.SetRepMerchantMigration("");
    window.close();
  }
  
  function AssignEqual()
  {
    var nMerchants = <%=vResults.size()%>;
    var i = 0;
    for (i = 0; i < nMerchants; i++)
    {
      document.getElementById("txt_" + i).value = document.getElementById("run_" + i).value;
    }
    SumTotal(false);
  }//End of function AssignEqual

	function FormatAmount(n)
	{
  		var s = "" + Math.round(n * 100) / 100;
  		var i = s.indexOf('.');
  		if (i < 0)
  		{
  		  return s + ".00";
  		}
  		var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3);
  		if (i + 2 == s.length)
  		{
  		  t += "0";
  		}
  		return t;
	}//End of function FormatAmount
	
	function SelectPrepaid()
	{
		var nMerchants = <%=vResults.size()%>;
		var i = 0;
		for (i = 0; i < nMerchants; i++)
		{
			document.getElementById("rbtnPre_" + i).checked = true;
		}
	}//End of function SelectPrepaid
	
	function SelectCredit()
	{
		var nMerchants = <%=vResults.size()%>;
		var i = 0;
		for (i = 0; i < nMerchants; i++)
		{
			document.getElementById("rbtnCre_" + i).checked = true;
		}
	}//End of function SelectCredit
  </script>
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="18" height="20"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
      <td background="../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.title",SessionData.getLanguage()).toUpperCase()%></td>
      <td width="12" height="20"><img src="../../images/top_right_blue.gif"></td>
    </tr>
    <tr>
      <td colspan="3"  bgcolor="#FFFFFF">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
          <tr>
            <td>
              <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="formArea">
<%
if (vResults.size() > 0)
{//If there are results
	double dSumLimit = 0;
	double dSumRunning = 0;
	String sPrepaid = "";
	String sCredit = "";
	if ( Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT) )
	{
		sCredit = "checked";
	}
	else
	{
		sPrepaid = "checked";
	}

	if ( !bShared )
	{
%>
					<br><span class=main><%=Languages.getString("jsp.admin.customers.reps.merchantmigration.caption",SessionData.getLanguage())%></span><br><br>
<%
	}
%>
                    <table width="100%" cellspacing="1" cellpadding="1" border="0">
                      <tr>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantid",SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantdba",SessionData.getLanguage()).toUpperCase()%></td>
<%
	if ( bShared )
	{//If we are going to enable shared balance
%>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantmigration.credittype",SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantmigration.creditlimit",SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantrunning",SessionData.getLanguage()).toUpperCase()%></td>
<%
	}
	else
	{//Else we are going to disable shared balance
%>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantpayment.merchantrunning",SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2 nowrap>
							<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.credittype",SessionData.getLanguage()).toUpperCase()%><br>
							<label for='rbtnPreMain'><input id='rbtnPreMain' type=radio name="rbtnMain" value=1 onclick="SelectPrepaid();"><%=Languages.getString("jsp.admin.customers.reps_add.credit_type1",SessionData.getLanguage())%></label>&nbsp;&nbsp;
							<label for='rbtnCreMain'><input id='rbtnCreMain' type=radio name="rbtnMain" value=2 onclick="SelectCredit();"><%=Languages.getString("jsp.admin.customers.reps_add.credit_type2",SessionData.getLanguage())%></label>
						</td>
                        <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.customers.reps.merchantmigration.creditlimit",SessionData.getLanguage()).toUpperCase()%></td>
<%
	}
%>
                      </tr>
<%
	for ( int i = 0; i < vResults.size(); i++ )
	{//For through the merchants
		Vector vTemp = (Vector)vResults.get(i);
%>
                      <tr class='main'>
                        <td class='row<%=(i % 2) + 1%>' nowrap><%=vTemp.get(0)%></td>
                        <td class='row<%=(i % 2) + 1%>' nowrap><%=vTemp.get(1)%></td>
<%
		if ( bShared )
		{//If we are enabling the shared balance
%>
                        <td class='row<%=(i % 2) + 1%>' nowrap>
<%
			if ( vTemp.get(5).equals("1"))
			{
				out.print(Languages.getString("jsp.admin.customers.reps_add.credit_type1",SessionData.getLanguage()));
			}
			else
			{
				out.print(Languages.getString("jsp.admin.customers.reps_add.credit_type2",SessionData.getLanguage()));
			}
			dSumLimit += Double.parseDouble(vTemp.get(2).toString());
			dSumRunning += Double.parseDouble(vTemp.get(3).toString());
%>
                        </td>
                        <td class='row<%=(i % 2) + 1%>' align='right' nowrap>
                          <%=NumberUtil.formatAmount(vTemp.get(2).toString())%>
                          <input id='mer_<%=i%>' type=hidden value='<%=vTemp.get(0)%>'>
                        </td>
                        <td class='row<%=(i % 2) + 1%>' align='right' nowrap><%=NumberUtil.formatAmount(vTemp.get(3).toString())%></td>
<%
		}//End of if we are enabling the shared balance
		else
		{//Else we are disabling the shared balance
%>
                        <td class='row<%=(i % 2) + 1%>' align='right' nowrap><%=NumberUtil.formatAmount(vTemp.get(3).toString())%></td>
                        <td class='row<%=(i % 2) + 1%>' nowrap>
                          <label for='rbtnPre_<%=i%>'><input id='rbtnPre_<%=i%>' type=radio name='rbtnCreditType_<%=i%>' value='1' <%=sPrepaid%>><%=Languages.getString("jsp.admin.customers.reps_add.credit_type1",SessionData.getLanguage())%></label>&nbsp;&nbsp;
                          <label for='rbtnCre_<%=i%>'><input id='rbtnCre_<%=i%>' type=radio name='rbtnCreditType_<%=i%>' value='2' <%=sCredit%>><%=Languages.getString("jsp.admin.customers.reps_add.credit_type2",SessionData.getLanguage())%></label>
                        </td>
                        <td class='row<%=(i % 2) + 1%>' align='right' nowrap>
                          <input id='txt_<%=i%>' type=text maxlength=8 size=12 value='<%=NumberUtil.formatAmount(vTemp.get(3).toString())%>' onpropertychange='ValidateLimitValue(this);' onblur='SumLimit(<%=i%>);this.value=FormatAmount(this.value);'>
                          <input id='mer_<%=i%>' type=hidden value='<%=vTemp.get(0)%>'>
                          <input id='run_<%=i%>' type=hidden value='<%=NumberUtil.formatAmount(vTemp.get(3).toString())%>'>
                        </td>
<%
		}//End of else we are disabling the shared balance
%>
                      </tr>
<%
	}//End of for through the merchants
	
	if ( bShared )
	{//If we are enabling the shared balance
		boolean bAllowLimit = false;
		boolean bAllowRunning = false;
		%>
        <tr class=main>
          <td colspan=3 align='right'><b><%=Languages.getString("jsp.admin.customers.reps.merchantmigration.total",SessionData.getLanguage())%>:</b></td>
          <td align='right' nowrap><%=NumberUtil.formatAmount(Double.toString(dSumLimit))%></td>
          <td align='right' nowrap><%=NumberUtil.formatAmount(Double.toString(dSumRunning))%></td>
        </tr>
        <tr class=main>
          <td colspan=3 align='right'><b><%=Languages.getString("jsp.admin.customers.reps.merchantmigration.totalrep",SessionData.getLanguage())%>:</b></td>
<%
		if ( Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) )
		{
%>
          <td align='right' nowrap><%=Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage())%></td>
          <td align='right' nowrap><%=Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage())%></td>
<%
		}
		else
		{
%>
          <td align='right' nowrap><%=NumberUtil.formatAmount(Rep.getCreditLimit())%></td>
          <td align='right' nowrap><%=NumberUtil.formatAmount(Rep.getRunningLiability())%></td>
<%
		}
%>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr class=main>
          <td colspan=5 align='right'>
<%
		if ( (dSumLimit <= Double.parseDouble(Rep.getCreditLimit())) || Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) )
		{
			bAllowLimit = true;
			out.print("<span style='color:green;'><b>" + Languages.getString("jsp.admin.customers.reps.merchantmigration.limitok",SessionData.getLanguage()) + "</b></span>");
		}
		else
		{
			bAllowLimit = false;
			out.print("<span style='color:red;'><b>" + Languages.getString("jsp.admin.customers.reps.merchantmigration.limitbad",SessionData.getLanguage()) + "</b></span>");
		}
%>
          </td>
        </tr>
        <tr class=main>
          <td colspan=5 align='right'>
<%
		if ( (dSumRunning <= Double.parseDouble(Rep.getRunningLiability())) || Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) || Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID))
		{
			bAllowRunning = true;
			out.print("<span style='color:green;'><b>" + Languages.getString("jsp.admin.customers.reps.merchantmigration.runningok",SessionData.getLanguage()) + "</b></span>");
		}
		else
		{
			bAllowRunning = false;
			out.print("<span style='color:red;'><b>" + Languages.getString("jsp.admin.customers.reps.merchantmigration.runningbad",SessionData.getLanguage()) + "</b></span>");
		}
%>
          </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
          <td colspan=5 align='center'>
<%
		if ( bAllowLimit && bAllowRunning )
		{
%>
            <input id='btnConvert' type=button value='<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.convert",SessionData.getLanguage())%>' onclick='ValidateMigration();'>&nbsp;&nbsp;&nbsp;
<%
		}
%>
            <input id='btnCancel' type=button value='<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.cancel",SessionData.getLanguage())%>' onclick='CancelPayment();'>
          </td>
        </tr>
      </table>
<%
	}//End of if we are enabling the shared balance
	else
	{//Else we are disabling the shared balance
%>
                      <tr class=main>
                        <td colspan=5 align='right' nowrap>
                          <b><%=Languages.getString("jsp.admin.customers.reps.merchantmigration.totalassignedlimit",SessionData.getLanguage())%>:</b>&nbsp;&nbsp;&nbsp;
                          <input id='txt_Total' type=text size=12 value='0' readonly>
                        </td>
                      </tr>
                      <tr class=main>
                        <td colspan=5 align='right' nowrap>
                          <b><%=Languages.getString("jsp.admin.customers.reps_info.credit_avail",SessionData.getLanguage())%>:</b>&nbsp;&nbsp;&nbsp;
<%
		if ( Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) )
		{
%>
                          <%=Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage())%>
                          <input id='txt_RepAvailable' type=hidden value='1000000000'>
<%
		}
		else
		{
%>
                          <input id='txt_RepAvailable' type=text size=12 value='<%=NumberUtil.formatAmount(Rep.getAvailableCredit())%>' readonly>
<%
		}
%>
                          <script>SumTotal(false);</script>
                        </td>
                      </tr>
                      <tr><td>&nbsp;</td></tr>
                      <tr>
                        <td colspan=5 align='center'>
                          <input id='btnConvert' type=button value='<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.convert",SessionData.getLanguage())%>' onclick='ValidatePayment();'>&nbsp;&nbsp;&nbsp;
                          <input id='btnCancel' type=button value='<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.cancel",SessionData.getLanguage())%>' onclick='CancelPayment();'>
                          &nbsp;&nbsp;&nbsp;<input id='btnAssign' type=button value='<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.assign",SessionData.getLanguage())%>' onclick='AssignEqual();'>
                        </td>
                      </tr>
                    </table>
<%
	}//End of else we are disabling the shared balance
}//End of if there are results
else
{
	if ( request.getParameter("bShared") != null )
	{
%>
                    <script>
                      //self.opener.SetRepMerchantPayment("&");
                      alert('<%=Languages.getString("jsp.admin.customers.reps.merchantmigration.nomerchants",SessionData.getLanguage())%>');
                      window.close();                    
                    </script>
<%
	}
}
%>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
