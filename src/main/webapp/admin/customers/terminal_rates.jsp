<%@page import="com.debisys.utils.*,
				com.debisys.languages.Languages,
				com.debisys.terminals.Terminal,
				com.debisys.properties.Properties,
				java.util.*,java.util.ArrayList,
				com.debisys.users.User" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:setProperty name="Terminal" property="*"/>
<jsp:setProperty name="RatePlan" property="*"/>
<jsp:setProperty name="Merchant" property="*"/>
<%
int section = 2;
int section_page = 11;
String ratePlanIDValue = RatePlan.getRatePlanId(request.getParameter("siteId"));
/*
 * Alfred A. - Added dummy strings for the sms and web service names before initialization because the sms
 * window would appear always when the jsp page first loaded.
 */
String sPCTerminalID = "";
String sSMSTerminalID = "SMST1";
String sSMS2TerminalID = "SMST2";
String sSMS3TerminalID = "SMST3";
String sSMS4TerminalID="SMST4";
String sSMS5TerminalID="SMST5";
String sMicroBrowserID = "";
String sWebServiceTerminalID="WebTest1";
String sWebService2TerminalID="WebTest2";

String hiddenTrMarketPlaceRow = "hidden";
String disableControlsMB = "";
boolean asignoTerminal = false;
Vector<Vector<Vector<String>>> vISORatePlan = new Vector<Vector<Vector<String>>>();
Vector vRepRatePlan = new Vector();
Vector vMPTerminalTypes = new Vector();

String sAccessLevel = SessionData.getProperty("access_level");
String strRefId = SessionData.getProperty("ref_id");
String typeRatePlan=""; 
vMPTerminalTypes = com.debisys.terminals.Terminal.getMarketplaceTerminalType();

boolean permissionToTerminalMappingsAndAccessLevel = SessionData.checkPermission(DebisysConstants.PERM_SETUP_TERMINAL_MAPPING) && 
														sAccessLevel.equals(DebisysConstants.ISO);
														
boolean hasPermissionManage_New_Rateplans = SessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS);

// TODO: check if the terminal is disabled
	response.setCharacterEncoding("utf-8");
	Vector vRatePlan = new Vector();
	String sNewPrefix = (request.getParameter("sNew")!=null)?request.getParameter("sNew"):"";
	if ( request.getParameter("PlanType").equals("REP") ){
		typeRatePlan="REP";
		RatePlan.setRepRatePlanId(request.getParameter("sRateID"));
		vRatePlan = RatePlan.getMerchantRatePlan(SessionData);

%>
<table width="100">
	<tr>
		<td class="rowhead2" width="100" nowrap="nowrap"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.total",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.rep",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.merchant",SessionData.getLanguage()).toUpperCase()%></td>
	</tr>
<%
		Iterator it = vRatePlan.iterator();
		int intEvenOdd = 1;
		String strProductId = "";
		double dblTotalRate = 0;
		double dblRepRate = 0;
		double dblMerchantRate = 0;
		int icount=0;
		while (it.hasNext()){
			icount++;
			Vector vecTemp = (Vector)it.next();
			strProductId = vecTemp.get(1).toString();
			dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
			dblRepRate = 0; //Double.parseDouble(vecTemp.get(3).toString());
			dblMerchantRate = dblTotalRate - dblRepRate;
			out.print("<tr class=row" + intEvenOdd +">" +
				"<td><input type=hidden name="+sNewPrefix+"productId value=" + strProductId + ">" +strProductId+ "</td>" +
				"<td nowrap=\"nowrap\">" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
				"<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\""+sNewPrefix+"total_" + strProductId + "\" id=\""+sNewPrefix+"total_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblTotalRate)) + "\" readonly size=3></td>" +
				"<td align=left><input type=text name=\""+sNewPrefix+"rep_" + strProductId + "\" id=\""+sNewPrefix+"rep_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblRepRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'"+sNewPrefix+"');\" onBlur=\"return validate(this);\">");
			out.print("</td>");
			out.print("<td align=left><input style=\"color:#");
			//parature tkt 9949440 forcing some rates rounding
			dblMerchantRate = NumberUtil.roundRate(dblMerchantRate, DebisysConfigListener.getRatesDecimalZeroes(application));
			if ((dblMerchantRate < 0) || dblMerchantRate > dblTotalRate){
				out.print("ff0000");
			}else{
				out.print("0000FF");
			}
			out.println(";background:#C0C0C0;\" type=text name=\""+sNewPrefix+"remaining_" + strProductId + "\" id=\""+sNewPrefix+"remaining_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblMerchantRate)) + "\" readonly size=3>");
			out.print("</td></tr>");
			intEvenOdd = (intEvenOdd == 1)?2:1;
		}
		vRatePlan.clear();
%>
</table>
<label><input type="checkbox" name="saveRates" value="y">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.save",SessionData.getLanguage())%></label>
<% //End of if we must render a RepRate
	}
	else if ( request.getParameter("PlanType").equals("ISO") ) 
	{
		// Must render a ISO Rate
		String strDistChainType = SessionData.getProperty("dist_chain_type");
		String strAccessLevel = SessionData.getProperty("access_level");
		RatePlan.setRatePlanId(request.getParameter("sRateID"));
		vRatePlan = RatePlan.getProductsIsoRatePlan(SessionData);
%>
<table width="100">
	<tr>
		<td class="rowhead2" width="100" nowrap="nowrap"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
		<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.total",SessionData.getLanguage()).toUpperCase()%></td>
<%
		if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
		<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.iso_percent",SessionData.getLanguage()).toUpperCase().replaceAll(" ", "&nbsp;")%></td>
<%
		}
		if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
%>
		<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.agent_percent",SessionData.getLanguage()).toUpperCase().replaceAll(" ", "&nbsp;")%></td>
<%
		}
		if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>
		<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.subagent_percent",SessionData.getLanguage()).toUpperCase().replaceAll(" ", "&nbsp;")%></td>
<%
		}
%>
		<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.rep_percent",SessionData.getLanguage()).toUpperCase().replaceAll(" ", "&nbsp;")%></td>
		<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.merchant_percent",SessionData.getLanguage()).toUpperCase().replaceAll(" ", "&nbsp;")%></td>
	</tr>
<%
		Iterator it = vRatePlan.iterator();
		int intEvenOdd = 1;
		String strProductId = "";
		double dblTotalRate = 0;
		double dblISORate = 0;
		double dblAgentRate = 0;
		double dblSubAgentRate = 0;
		double dblRepRate = 0;
		double dblMerchantRate = 0;
		int icount=0;
	
		while (it.hasNext()){
			icount++;
			Vector vecTemp = (Vector)it.next();
			strProductId = vecTemp.get(1).toString();
			dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
			dblISORate = Double.parseDouble(vecTemp.get(3).toString());
			dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
			dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());
			dblRepRate = Double.parseDouble(vecTemp.get(6).toString());
			dblMerchantRate = Double.parseDouble(vecTemp.get(7).toString());
			out.print("<tr class=row" + intEvenOdd +">" +
				"<td><input type=hidden name="+sNewPrefix+"productId value=" + strProductId + ">" +strProductId+ "</td>" +
				"<td nowrap=\"nowrap\">" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
				"<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\""+sNewPrefix+"total_" + strProductId + "\" id=\""+sNewPrefix+"total_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblTotalRate)) + "\" readonly size=3></td>");
			if (strAccessLevel.equals(DebisysConstants.ISO)){
				out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\""+sNewPrefix+"iso_" + strProductId + "\" id=\""+sNewPrefix+"iso_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblISORate)) + "\" readonly size=3></td>");
			}
			if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
				out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\""+sNewPrefix+"agent_" + strProductId + "\" id=\""+sNewPrefix+"agent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblAgentRate)) + "\" readonly size=3></td>");
			}
			if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\""+sNewPrefix+"subagent_" + strProductId + "\" id=\""+sNewPrefix+"subagent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate)) + "\" readonly size=3></td>");
			}
			out.print("<td align=left><input type=text name=\""+sNewPrefix+"rep_" + strProductId + "\" id=\""+sNewPrefix+"rep_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblRepRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'"+sNewPrefix+"');\" onBlur=\"return validate(this);\"></td>");
			out.print("<td align=left><input style=\"color:#");
			//parature tkt 9949440 forcing some rates rounding
			dblMerchantRate = NumberUtil.roundRate(dblMerchantRate, DebisysConfigListener.getRatesDecimalZeroes(application));
			if ((dblMerchantRate < 0) || dblMerchantRate > dblTotalRate){
				out.print("ff0000");
			}else{
				out.print("0000FF");
			}
			out.println(";background:#C0C0C0;\" type=text name=\""+sNewPrefix+"remaining_" + strProductId + "\" id=\""+sNewPrefix+"remaining_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblMerchantRate)) + "\" readonly size=3>");
			out.print("</td></tr>");
			intEvenOdd = (intEvenOdd == 1)?2:1;
		}
		vRatePlan.clear();
%>
</table>
<%
	}//End of if we must render an ISORate
%>
<script type="text/javascript">
	$("#btnSubmit").removeAttr('disabled');		
</script>