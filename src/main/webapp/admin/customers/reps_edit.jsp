<%@page import="net.emida.supportsite.dto.InvoiceType"%>
<%@page import="net.emida.supportsite.dto.RepInvoicePaymentType"%>
<%@page import="net.emida.supportsite.dao.InvoicePaymentTypesDao"%>
<%@page import="net.emida.supportsite.dto.InvoicePaymentType"%>
<%@ page import="com.debisys.customers.Rep,
                 java.net.URLEncoder,
                 com.debisys.utils.*,
                 java.util.*,
                 com.debisys.utils.TimeZone,
                 com.debisys.tools.s2k,
                 java.text.SimpleDateFormat,
                 com.debisys.properties.Properties,
                 net.emida.supportsite.dto.InvoicePaymentType,
                 java.io.IOException" %>




<%
int section=2;
int section_page=16;
String instance = DebisysConfigListener.getInstance(application);
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request"/>
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges"
	scope="request" />
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
    if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ) {
%>
<%@ include file="/WEB-INF/jspf/admin/customers/reps/repInvoicePaymentTypesCommonCode.jspf" %>
<%}%>
<%

/******************************************************************************************/
/******************************************************************************************/
/*GEOLOCATION FEATURE*/
boolean hasPermissionGeolocation = SessionData.checkPermission(DebisysConstants.PERM_GEOLOCATION_FEAUTURE);

int graceDaysLeft = 0, graceDays;
String installationDate;

installationDate = Properties.getPropertyValue(instance, "global","address.installationDate") + "";
try{
	graceDays = Integer.parseInt(Properties.getPropertyValue(instance, "global","address.daysOfGrace"));
}catch(Exception ex){
	graceDays = 0;
}
if(installationDate.equals("")){
	graceDaysLeft = 0;
}else{
	SimpleDateFormat sdf= new SimpleDateFormat("yyyy/MM/dd");
	java.util.Date d = null;
	try{
		d = sdf.parse(installationDate);
		Date today = new Date();
		int diff = (int)((long)(today.getTime() - d.getTime())/(1000*60*60*24));
		graceDaysLeft = graceDays - diff;
	}catch(Exception ex){
		//TODO: installationDate no se ha establecido en la tabla de properties
	}
}
/*INIT Internationalitation*/
String labelValidatingAddressInformation = Languages.getString("jsp.admin.geolocation.validatingAddress",SessionData.getLanguage());
String labelInvalidInput = Languages.getString("jsp.admin.geolocation.invalidInput",SessionData.getLanguage());
String labelKeyBad = Languages.getString("jsp.admin.geolocation.invalidKey",SessionData.getLanguage());
String labelAddressNotFound = Languages.getString("jsp.admin.geolocation.invalidInputBad",SessionData.getLanguage());
String labelInputWarnning1 = Languages.getString("jsp.admin.geolocation.warnning1",SessionData.getLanguage());
String labelInputWarnning2 = Languages.getString("jsp.admin.geolocation.warnning2",SessionData.getLanguage());
String labelInputWarnning3 = Languages.getString("jsp.admin.geolocation.warnning3",SessionData.getLanguage());
String labelInputWarnning4 = Languages.getString("jsp.admin.geolocation.warnning4",SessionData.getLanguage());
String labelInputWarnning5 = Languages.getString("jsp.admin.geolocation.warnning5",SessionData.getLanguage());
String labelCurrentAddress = Languages.getString("jsp.admin.geolocation.addressIs",SessionData.getLanguage());
String labelSuggestion1 = Languages.getString("jsp.admin.geolocation.suggestions1",SessionData.getLanguage());
String labelSuggestion2 = Languages.getString("jsp.admin.geolocation.suggestions2",SessionData.getLanguage());
String labelApply = Languages.getString("jsp.admin.geolocation.apply",SessionData.getLanguage());
String labelCancel = Languages.getString("jsp.admin.geolocation.cancel",SessionData.getLanguage());
String labelNotConnect = Languages.getString("jsp.admin.geolocation.notConnect",SessionData.getLanguage());
/*END Internationalitation*/

/*GEOLOCATION FEATURE*/
/******************************************************************************************/
/******************************************************************************************/


Hashtable repErrors = null;
int    intTabIndex = 1;
if (DebisysConfigListener.getDebugJS(application)) {
				String vars = "\"submitted=" + request.getParameter("submitted") + "\"";
				out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
				vars = "\"city="  + Rep.getCity() + "\"";
				out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
			}
if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").length() > 0) )
{
  if ( (request.getParameter("mxContactAction").equals("ADD")) && (session.getAttribute("vecContacts") != null) )
  {
    Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
  }
  else if ( request.getParameter("mxContactAction").equals("SAVE") )
  {
    Vector vTemp = new Vector();
    vTemp.add(request.getParameter("contactTypeId"));
    vTemp.add(request.getParameter("contactName"));
    vTemp.add(request.getParameter("contactPhone"));
    vTemp.add(request.getParameter("contactFax"));
    vTemp.add(request.getParameter("contactEmail"));
    vTemp.add(request.getParameter("contactCellPhone"));
    vTemp.add(request.getParameter("contactDepartment"));
    Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
    if ( (request.getParameter("mxContactIndex") == null) || request.getParameter("mxContactIndex").equals("") )
    {
      Rep.getVecContacts().add(vTemp);
    }
    else
    {
      Rep.getVecContacts().setElementAt(vTemp, Integer.parseInt(request.getParameter("mxContactIndex")));
    }
    session.setAttribute("vecContacts", Rep.getVecContacts());
  }
  else if ( request.getParameter("mxContactAction").equals("EDIT") )
  {
    Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
    Vector vec = (Vector)Rep.getVecContacts().elementAt(Integer.parseInt(request.getParameter("mxContactIndex")));
    Rep.setContactTypeId(Integer.parseInt(vec.get(0).toString()));
    Rep.setContactName(vec.get(1).toString());
    Rep.setContactPhone(vec.get(2).toString());
    Rep.setContactFax(vec.get(3).toString());
    Rep.setContactEmail(vec.get(4).toString());
    Rep.setContactCellPhone(vec.get(5).toString());
    Rep.setContactDepartment(vec.get(6).toString());
  }
  else if ( request.getParameter("mxContactAction").equals("DELETE") )
  {
    Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
    Rep.getVecContacts().removeElementAt(Integer.parseInt(request.getParameter("mxContactIndex")));
    session.setAttribute("vecContacts", Rep.getVecContacts());
  }
  else if ( (request.getParameter("mxContactAction").equals("CANCEL")) && (session.getAttribute("vecContacts") != null) )
  {
    Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
  }
}
else if (request.getParameter("submitted") != null)
{
  try
  {
    if (request.getParameter("submitted").equals("y"))
    {
        if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
        {//If the code reachs here is because there is at least one contact and the session var exists
        if (DebisysConfigListener.getDebugJS(application)) {
				String vars = "\"repType=" + Rep.getRepCreditType() + "\"";
				out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
				vars = "\"sharedBalance="  + Rep.getSharedBalance() + "\"";
				out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
			}
          Vector vTmp = (Vector)session.getAttribute("vecContacts");
          Rep.setVecContacts(vTmp);
          vTmp = (Vector)vTmp.get(0);
          Rep.setContactName(vTmp.get(1).toString());
          Rep.setContactFirst(vTmp.get(1).toString());
          Rep.setContactMiddle("_");
          Rep.setContactLast(".");
          Rep.setContactPhone(vTmp.get(2).toString());
          Rep.setContactFax(vTmp.get(3).toString());
          Rep.setContactEmail(vTmp.get(4).toString());
          Rep.setContactCellPhone(vTmp.get(5).toString());
          Rep.setContactDepartment(vTmp.get(6).toString());
        }
        boolean validUpdate = Rep.validateUpdate(SessionData, application);
         if (DebisysConfigListener.getDebugJS(application)) {
				String vars = "\"valid=" + validUpdate + "\"";
				out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
			}
        if(validUpdate)
        {
            if (( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) &&
                    (request.getParameter("repInvoiceTypesList") != null))
            {//If when deploying in Mexico
                String jsonString = request.getParameter("repInvoiceTypesList");
                this.updateRepIvoicePaymentTyes(jsonString, Long.valueOf(Rep.getRepId()));
            }
        	// DBSY-1072 eAccount Interface
        	if(request.getAttribute("internalChildrenExist") != null)
        		Rep.setInternalChildrenExist(Integer.parseInt(request.getAttribute("internalChildrenExist").toString()));

	       Rep.setDEntityAccountType(request.getParameter("disabledentityAccountType"));
           Rep.updateRep(SessionData, application);
           if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
           {
             session.removeAttribute("vecContacts");
           }
           response.sendRedirect("/support/admin/customers/reps_info.jsp?message=1&repId=" + Rep.getRepId());
        }
        else
        {
        	if(Rep.getsalesmanid().length()>0){
        		String strRefId = SessionData.getProperty("ref_id");
        		Rep.setsalesmanname(Rep.getsalesmannamebyid(Rep.getsalesmanid(),strRefId));
        	}
           repErrors = Rep.getErrors();
        }
    }

  }
  catch (Exception e)
  {
  }

}
else
{
    Rep.getRep(SessionData, application);
    if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
    {//If when deploying in Mexico
      session.setAttribute("vecContacts", Rep.getVecContacts());
    }

  	/*************************************************/
  	/*#DBSY-1051 - EMUNOZ - AUDIT*/
    LogChanges.setSession(SessionData);
    LogChanges.setContext(application);
    LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE_REP), String.valueOf(Rep.getPaymentType()));
    LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR_REP), String.valueOf(Rep.getProcessType()));
    LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA_REP), String.valueOf(Rep.getRoutingNumber()));
    LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER_REP), String.valueOf(Rep.getAccountNumber()));
    LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID_REP), String.valueOf(Rep.getTaxId()));
   	/*END #DBSY-1051 - EMUNOZ- AUDIT*/
    /*************************************************/

    // DBSY-1072 eAccounts Interface
    LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_REP), String.valueOf(Rep.getEntityAccountType()));

    if (Rep.isError())
    {
      response.sendRedirect("/support/message.jsp?message=4");
      return;
    }
}
%>
<%@ include file="/includes/header.jsp" %>
<script language="JavaScript" src="includes/rep_validations.js"></script>
<%
	//s2k loaded style and javascript for DBSY-931 System 2000 Tools
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If when deploying in International and user is an ISO level
%>
<script type="text/javascript" src="includes/s2k/salesmannameid.js"></script>
<%}%>





<link rel="stylesheet" type="text/css" href="css/popUp.css" title="style">
<style>
	.sep{clear:both; float:none;}
	.address{padding: 10px;}
	input{font-size: 10pt;}
	button{font-size: 10pt;}
	.address-option{border: 1px dotted gray;}
	.address-map{float: right; border: 1px dotted gray;}
	.address-info{max-width: 330px; witdh: 330px; float: left; text-align: left; padding: 5px; border: 1px dotted gray; overflow: auto;}
	.address-message{align: center; font-size: 12px; font-weight:bold;}
	.address-suggestions{clear:both; overflow: auto; padding-top:20px;}
	.address-buttons{align:center;}
</style>
<script language="JavaScript" src="includes/popUp.js"></script>
<script language="JavaScript" src="includes/AddressValidation.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8">
<%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && hasPermissionGeolocation ){
%>

	$(function () {

		//******Internationalitation******************//
	    labelValidatingAddressInformation = '<%=labelValidatingAddressInformation%>';
		labelInvalidInput = '<%=labelInvalidInput%>';
		labelKeyBad = '<%=labelKeyBad%>';
		labelAddressNotFound = '<%=labelAddressNotFound%>';
		labelInputWarnning1 = '<%=labelInputWarnning1%>';
		labelInputWarnning2 = '<%=labelInputWarnning2%>';
		labelInputWarnning3 = '<%=labelInputWarnning3%>';
		labelInputWarnning4 = '<%=labelInputWarnning4%>';
		labelInputWarnning5 = '<%=labelInputWarnning5%>';
		labelCurrentAddress = '<%=labelCurrentAddress%>';
		labelSuggestion1 = '<%=labelSuggestion1%>';
		labelSuggestion2 = '<%=labelSuggestion2%>';
		labelApply = '<%=labelApply%>';
		labelCancel = '<%=labelCancel%>';
		labelNotConnect = '<%=labelNotConnect%>';
	    //******Internationalitation******************//

		graceDays = '<%=graceDaysLeft%>';
		popAddress = new PopUp('addressValidation', 'Address validation', 600, 400);
		$('input[name=address], input[name=city], input[name=county], input[name=state], input[name=zip], input[name=latitude], input[name=longitude]').change(function(){
			$('#addressValidationStatus').val('0');
		});
		$('#submitbutton').click(function(){
                                        
			if($('#addressValidationStatus').val() === 0){
				validateAddress('admin/tools/address.jsp'
					,$('input[name=address]').val()
					,$('input[name=city]').val()
					,$('input[name=state]').val()
					,$('input[name=zip]').val()
					,function(data){
						if ( data !== null){
							$('input[name=address]').val(data[0]);
							$('input[name=city]').val(data[1]);
							//$('input[name=county]').val(data[2]);
							$('input[name=state]').val(data[3]);
							$('input[name=zip]').val(data[4]);
							$('input[name=latitude]').val(data[5]);
							$('input[name=longitude]').val(data[6]);
							$('span[name=latvalue]').text(data[5]);
							$('span[name=longvalue]').text(data[6]);
							$('#addressValidationStatus').val('1');
						}
						$('#formRep').submit();
					},
					null);
			}else{
				//if(check_form(document.rep)){
					$('#formRep').submit();
				//}
			}
			return false;
		});
		$('#cancel').click(function(){history.go(-1);});
	});
<%
}else{
%>

	$(function () {
		$('#submitbutton').click(function(){
                    <%if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {%>
                        addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
                    <%}%>
			$('#formRep').submit();
		});
		$('#cancel').click(function(){history.go(-1);});
	});
<%
}
%>

</script>



<script language="JavaScript">

var form = "";
var submitted = false;
var error = false;
var error_message = "";

function check_input(field_name, field_size, message) {
  if (formRep.elements[field_name] && (formRep.elements[field_name].type !== "hidden")) {
    var field_value = formRep.elements[field_name].value;

    if (field_value === '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_form(form_name) {
  if (submitted === true) {
    alert("<%= Languages.getString("jsp.admin.customers.reps_add.jsmsg1",SessionData.getLanguage()) %>");
    return false;
  }

  error = false;
  form = form_name;
  error_message = "<%= Languages.getString("jsp.admin.customers.reps_add.jsmsg2",SessionData.getLanguage()) %>";

  check_input("businessName", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_business_name",SessionData.getLanguage()) %>");
  check_input("address", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_street_address",SessionData.getLanguage()) %>");
  <%
  if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
  {
%>
  check_input("city", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_city",SessionData.getLanguage()) %>");
  check_input("state", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_state",SessionData.getLanguage()) %>");
  check_input("zip", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_zip",SessionData.getLanguage()) %>");

    var isCorrectAba = verifyAba(9,'/^[0-9]{9}$/',"<%=Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage())%>");
	if(isCorrectAba === false){
	    error = true;
		error_message = error_message + "* "+"<%=Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage())%>"+" \n";
	}

	var isCorrectAccount = verifyAccount(20,"<%=Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage())%>");
	if(isCorrectAccount === false){
	    error = true;
		error_message = error_message + "* "+"<%=Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage())%>"+" \n";
	}
<%
  }
  else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
  {
%>
  check_input("country", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_country",SessionData.getLanguage())%>");
<%
  }
	//s2k javascript check values for DBSY-931 System 2000 Tools
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If deploying in International and user is an ISO
%>
   var name = form.salesmanid.options[form.salesmanname.selectedIndex].value;
   var id = form.salesmanid.options[form.salesmanid.selectedIndex].value;
    // skip check if both empty
   if( id.length >0  || name.length >0 ){
   		if( id === "" || id === null ){
   			  check_input("salesmanid", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_salesmanid",SessionData.getLanguage())%>");
   		}
   		else if( name === "" || name === null ){
   			  check_input("salesmanname", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_salesmanname",SessionData.getLanguage())%>");
   		}
   }
<%}
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
   { //If when deploying in Mexico
%>
        addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
  check_input("legalBusinessname", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_legalBusinessname",SessionData.getLanguage())%>");

  if ( !document.getElementById('chkBillingAddress'.checked) ) {
    check_input("mailAddress", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_billing_address",SessionData.getLanguage())%>");
    check_input("mailCountry", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_billing_country",SessionData.getLanguage())%>");
  }
  check_input("contactData", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact_data",SessionData.getLanguage())%>");
  check_input("taxId", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_taxid",SessionData.getLanguage())%>");
  check_input("creditLimit", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_creditlimit",SessionData.getLanguage())%>");
<%
   } //End of if when deploying in Mexico
   else
   {
%>
  check_input("contactFirst", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_contact_first",SessionData.getLanguage())%>");
  check_input("contactLast", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_contact_last",SessionData.getLanguage())%>");
  check_input("contactPhone", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_phone",SessionData.getLanguage())%>");

<%
   }

   // DBSY-1072 eAccounts Interface
   if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE))
   {
%>
       check_input("entityAccountType", 1, "<%=Languages.getString("jsp.admin.customers.error_entityAccountType",SessionData.getLanguage())%>");

       if (error === true) {
          alert(error_message);
          return false;
       }

       var updateChildren = true;
       var hasIntChildren = form.elements['internalChildrenExist'].value;
	   var acctType = form.elements['entityAccountType'].value;

       if (hasIntChildren === '1' && acctType === '1') {
          updateChildren = confirm("<%=Languages.getString("jsp.admin.internalToExternalAcctTypeWarning",SessionData.getLanguage())%>");
       }

       if(!updateChildren) {
          return false;
	   }
<%
   }
   //END DBSY-1072
%>

   if (error === true) {
    alert(error_message);
    return false;
  } else {
    //document.formRep.submit.disabled = true;
    submitted = true;
    return true;
  }
}

function validate(c)
{
	if (isNaN(c.value))
	{
		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
		c.focus();
		return (false);

	}
	else
	{
		if (c.value < 0)
		{

	    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
	    	c.focus();
	    	return (false);
		}
		else
		{
			c.value = formatAmount(c.value);
		}
	}
}


function formatAmount(n)
{
		var s = "" + Math.round(n * 100) / 100
		var i = s.indexOf('.')
		if (i < 0) return s + ".00"
		var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3)
		if (i + 2 == s.length) t += "0"
		return t
}

function validateInteger(c) {
if ( c.value.length > 0 ) {
	if (isNaN(c.value)) {
		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
		c.focus();
            c.select();
		return (false);
    } else if (c.value < 0) {
    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
    	c.focus();
            c.select();
    	return (false);
}
}
}//End of function validateInteger

function validateIntegerGreaterThanX(c, x) {
if ( c.value.length > 0 ) {
	if (isNaN(c.value)) {
		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
		c.focus();
            c.select();
		return (false);
    } else if (c.value <= x) {
            var s = '<%=Languages.getString("jsp.admin.errorIntegerGreatherThanX",SessionData.getLanguage())%>' + x;
    	alert(s);
    	c.focus();
            c.select();
    	return (false);
}
}
}//End of function validateIntegerGreaterThanX

function ValidateTextAreaLength(c, x) {
if ( c.value.length > x ) {
  alert('<%=Languages.getString("jsp.admin.errorTextLongerThanX",SessionData.getLanguage())%>' + x);
  c.focus();
  c.select();
  return false;
}
}//End of function ValidatePhysAddress

function ValidateRegExp(c, e) {
var exp = new RegExp(e);
//exp.compile(e);
if ( !exp.test(c.value) && (c.value.length > 0) ) {
  alert('<%=Languages.getString("jsp.admin.errorExpressionInvalid",SessionData.getLanguage())%>');
  c.focus();
  c.select();
  return false;
}
return true;
}//End of function ValidateRegExp

  function verifyAba(lengthText,expression, textInfo){

	var optionAbaCk = document.getElementById('optionAba').checked;
  	if(optionAbaCk === true){
  		return true;
  	}
  	var valueAba = document.getElementById('routingNumber').value;
  	//var isValid = verifyNumericText(valueAba,'/^[0-9]{'+lengthText+'}$/');
  	var isValid = verifyNumericText(valueAba,expression);
  	var isValidLength = false;
  	var isNoAllZero = false;
  	var isValidSQL = false;

  	if ( isValid === true && valueAba.length === 9 ) {
  		isValidLength = true;
  		isNoAllZero = verifyNoAllZero(valueAba);
  		if(isNoAllZero === true){
  			isValidSQL = validationSQL(valueAba);
  		}
  	}

  	if(isValid === true && isValidLength === true && isNoAllZero === true && isValidSQL === true){
  		var  div_info = document.getElementById("div_info_aba");
  		div_info.style.color="black";
  		div_info.innerHTML = "";
  		return true;
  	}
  	else{
  		var  div_info = document.getElementById("div_info_aba");
  		div_info.style.width="200px";
  		div_info.style.color="red";
		div_info.innerHTML = "";
		var newdiv=document.createElement("div");
		var newtext=document.createTextNode(textInfo);
		newdiv.appendChild(newtext); //append text to new div
		div_info.appendChild(newdiv);
		return false;
  	}
  }

  function verifyAccount(lengthText, textInfo){
  	var optionAbaCk = document.getElementById('optionAccount').checked;
  	if(optionAbaCk === true){
  		return true;
  	}
  	var valueAccount = document.getElementById('accountNumber').value;
  	var isValid = verifyNumericText(valueAccount,'/^[0-9]{1,'+lengthText+'}$/');
  	var isValidLength = false;
  	var isNoAllZero = false;
  	if ( isValid === true && valueAccount.length <= 20 ) {
  		isValidLength = true;
  		isNoAllZero = verifyNoAllZero(valueAccount);
  	}

  	if(isValid === true && isValidLength === true && isNoAllZero === true){
  		var  div_info = document.getElementById("div_info_account");
  		div_info.style.color="black";
  		div_info.innerHTML = "";
  		return true;
  	}
  	else{
  		var  div_info = document.getElementById("div_info_account");
  		div_info.style.width="200px";
  		div_info.style.color="red";
		div_info.innerHTML = "";
		var newdiv=document.createElement("div");
		var newtext=document.createTextNode(textInfo);
		newdiv.appendChild(newtext); //append text to new div
		div_info.appendChild(newdiv);
		return false;
  	}

  }

  // EXEC THE REGULAR EXPRESSIONS
  function verifyNumericText(text,exp){
    exp = exp.replace("/", "");
	exp = exp.replace("/", "");
	exp = exp.replace(/\\/g, "\\");
  	var patt1 = new RegExp(exp);
	var result = patt1.exec(text);
	if (result!= null){
		return true;
	}
	return false;
  }

  // VERIFY THAT AN TEXT NOT CONTAIN ALL ZEROS
  function verifyNoAllZero(text){
  	var i=0;
  	for(i = 0; i < text.length ; i++){
  		if(text.charAt(i) != '0'){
  			return true;
  		}
  	}
  	return false;
  }

  //VALIDATION FOR ABA MERCHANT WITH FORMAT OF THE DB
  function validationSQL(text){


 	var digit1 = parseInt(text.charAt(0))*3;
 	var digit2 = parseInt(text.charAt(1))*7;
 	var digit3 = parseInt(text.charAt(2))*1;
 	var digit4 = parseInt(text.charAt(3))*3;
 	var digit5 = parseInt(text.charAt(4))*7;
 	var digit6 = parseInt(text.charAt(5))*1;
 	var digit7 = parseInt(text.charAt(6))*3;
 	var digit8 = parseInt(text.charAt(7))*7;
 	var digit9 = parseInt(text.charAt(8));

  	var sumDigits = digit1+digit2+digit3+ digit4+ digit5+digit6+ digit7+digit8 ;
  	var mod10 = sumDigits % 10;
  	var totalOP = 10-mod10;
  	var resultDigit = getRightDigits(totalOP,1);
  	if(resultDigit == digit9){
  		return true;
  	}
  	else{
  		return false;
  	}
  }

  // GET AN NUMBER DIGITS THE ONE NUMBER
  function getRightDigits(num,digits){
  	var i=0;
  	var numString = num.toString();
  	var realNumStr = '';
  	for(i = (numString.length-digits); i < numString.length ; i++){
 		realNumStr = realNumStr+numString.charAt(i);
  	}
  	var realNum = parseInt(realNumStr);
  	return realNum;
  }

  function changeCheckAba(){
  	var optionAbaCk = document.getElementById('optionAba').checked;
  	if(optionAbaCk == true){
  		var div_info = document.getElementById("div_info_aba");
  		div_info.style.color="black";
  		div_info.innerHTML = "";

  		var valueAba = document.getElementById('routingNumber');
  		valueAba.value = 'NA';
  		valueAba.disabled=true;
  	}
  	else{
  		var valueAba = document.getElementById('routingNumber');
  		valueAba.value = '';
  		valueAba.disabled=false;
  	}
  }

  function changeCheckAccount(){
  	var optionAbaCk = document.getElementById('optionAccount').checked;
  	if(optionAbaCk == true){
  		var div_info = document.getElementById("div_info_account");
  		div_info.style.color="black";
  		div_info.innerHTML = "";

  		var valueAccount = document.getElementById('accountNumber');
  		valueAccount.value = 'NA';
  		valueAccount.disabled=true;
  	}
  	else{
  		var valueAccount = document.getElementById('accountNumber');
  		valueAccount.value = '';
  		valueAccount.disabled=false;
  	}
  }


function EnableBillingAddress(bChecked) {
document.getElementById('mailAddress').readOnly = bChecked;
document.getElementById('mailColony').readOnly = bChecked;
document.getElementById('mailDelegation').readOnly = bChecked;
document.getElementById('mailCity').readOnly = bChecked;
document.getElementById('mailZip').readOnly = bChecked;
document.getElementById('mailState').disabled = bChecked;
document.getElementById('mailCounty').readOnly = bChecked;
document.getElementById('mailCountry').disabled = bChecked;
if (bChecked) {
    document.getElementById('mailAddress').value = document.getElementById('address').value;
    document.getElementById('mailColony').value = document.getElementById('physColony').value;
    document.getElementById('mailDelegation').value = document.getElementById('physDelegation').value;
    document.getElementById('mailCity').value = document.getElementById('city').value;
    document.getElementById('mailZip').value = document.getElementById('zip').value;
    document.getElementById('mailState').value = document.getElementById('state').value;
    document.getElementById('mailCounty').value = document.getElementById('physCounty').value;
    document.getElementById('mailCountry').value = document.getElementById('country').value;
}
else {
    document.getElementById('mailAddress').value = '';
    document.getElementById('mailColony').value = '';
    document.getElementById('mailDelegation').value = '';
    document.getElementById('mailCity').value = '';
    document.getElementById('mailState').value = '';
    document.getElementById('mailZip').value = '';
    document.getElementById('mailCounty').value = '';
    document.getElementById('mailCountry').value = '';
}
}//End of function EnableBillingAddress

//-->

  //DTU-369 Payment Notifications
  function chkPaymentNoti()
  {
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
		<%
		if (Rep.isPaymentNotificationSMS())
		{
		%>
		 	valuePayNotiSMSChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiSMSChk.checked=false;
		<%
		}
		%>
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
		<%
		if (Rep.isPaymentNotificationMail())
		{
		%>
		 	valuePayNotiMailChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiMailChk.checked=false;
		<%
		}
		%>
  		var valuePayNotiChk = document.getElementById('paymentNotifications');
		<%
		if (Rep.isPaymentNotifications())
		{
		%>
		 	valuePayNotiChk.checked=true;
  			valuePayNotiSMSChk.disabled=false;
  			valuePayNotiMailChk.disabled=false;
		<%
		}
		else
		{
		%>
			valuePayNotiChk.checked=false;
			valuePayNotiSMSChk.checked=false;
			valuePayNotiMailChk.checked=false;
			valuePayNotiSMSChk.disabled=true;
			valuePayNotiMailChk.disabled=true;
		<%
		}
		%>
  }
  function changePayNoti()
  {
  	var payNoti = document.getElementById('paymentNotifications').checked;
  	if(payNoti == true)
	{
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  		valuePayNotiSMSChk.checked=false;
  		valuePayNotiSMSChk.disabled=false;
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  		valuePayNotiMailChk.checked=false;
  		valuePayNotiMailChk.disabled=false;
  	}
  	else
	{
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  		valuePayNotiSMSChk.checked=false;
  		valuePayNotiSMSChk.disabled=true;
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  		valuePayNotiMailChk.checked=false;
  		valuePayNotiMailChk.disabled=true;
  	}
  }

  function changePayNotiSMS()
  {
  	var payNotiSMS = document.getElementById('paymentNotificationSMS').checked;
  	var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  	if(payNotiSMS == true)
	{
  		valuePayNotiMailChk.checked=false;
  	}
  	else
	{
  		valuePayNotiMailChk.checked=true;
  	}
  }

  function changePayNotiMail()
  {
  	var payNotiMail = document.getElementById('paymentNotificationMail').checked;
  	var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  	if(payNotiMail == true)
	{
  		valuePayNotiSMSChk.checked=false;
  	}
  	else
	{
  		valuePayNotiSMSChk.checked=true;
  	}
  }

//DTU-480 Low Balance Alert Message SMS Mail
    function chkBalanceNoti()
  	{
		var valueBalNotiValue = document.getElementById('balanceNotificationValue');
		var valueBalNotiDays = document.getElementById('balanceNotificationDays');
		  	
    	var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		<%
		if (Rep.isBalanceNotificationTypeValue())
		{
		%>
		 	valueBalNotiValueChk.checked=true;
		 	valueBalNotiValue.disabled=false;
		<%
		}
		else
		{
		%>
			valueBalNotiValueChk.checked=false;
			valueBalNotiValue.disabled=true;
		<%
		}
		%>
    	var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		<%
		if (Rep.isBalanceNotificationTypeDays())
		{
		%>
		 	valueBalNotiDaysChk.checked=true;
		 	valueBalNotiDays.disabled=false;
		<%
		}
		else
		{
		%>
			valueBalNotiDaysChk.checked=false;
			valueBalNotiDays.disabled=true;
		<%
		}
		%>
  		var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		<%
		if (Rep.isBalanceNotificationSMS())
		{
		%>
		 	valueBalNotiSMSChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiSMSChk.checked=false;
		<%
		}
		%>
  		var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		<%
		if (Rep.isBalanceNotificationMail())
		{
		%>
		 	valueBalNotiMailChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiMailChk.checked=false;
		<%
		}
		%>
  		var valueBalNotiChk = document.getElementById('balanceNotifications');
		<%
		if (Rep.isBalanceNotifications())
		{
		%>
		 	            valueBalNotiChk.checked=true;	
 			            valueBalNotiSMSChk.disabled=false;
  			            valueBalNotiMailChk.disabled=false;
						valueBalNotiValueChk.disabled=false;
						valueBalNotiDaysChk.disabled=false;
		<%
		}
		else
		{
		%>
			            valueBalNotiChk.checked=false;
			            valueBalNotiSMSChk.disabled=true;
			            valueBalNotiSMSChk.checked = false;
			            valueBalNotiMailChk.disabled=true;
			            valueBalNotiMailChk.checked = false;
						valueBalNotiValueChk.disabled=true;
						valueBalNotiValueChk.checked = false;
						valueBalNotiDaysChk.disabled=true;
						valueBalNotiDaysChk.checked = false;
						valueBalNotiValue.disabled=true;
						valueBalNotiDays.disabled=true;	
		<%
		}
		%>
  }  
  
				function changeBalNoti() {
		        var balNoti = document.getElementById('balanceNotifications').checked;
		        if (balNoti == true) {
		            var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		            valueBalNotiDaysChk.checked = false;
		            valueBalNotiDaysChk.disabled = false;
		            var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		            valueBalNotiValueChk.checked = false;
		            valueBalNotiValueChk.disabled = false;				
		            var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		            valueBalNotiSMSChk.checked = false;
		            valueBalNotiSMSChk.disabled = false;
		            var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		            valueBalNotiMailChk.checked = false;
		            valueBalNotiMailChk.disabled = false;
					var valueBalNotiValue = document.getElementById('balanceNotificationValue');
					var valueBalNotiDays = document.getElementById('balanceNotificationDays');
					valueBalNotiValue.disabled=true;
					valueBalNotiDays.disabled=true;		            
		        }
		        else {
		            var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		            valueBalNotiDaysChk.checked = false;
		            valueBalNotiDaysChk.disabled = true;
		            var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		            valueBalNotiValueChk.checked = false;
		            valueBalNotiValueChk.disabled = true;
		            var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		            valueBalNotiSMSChk.checked = false;
		            valueBalNotiSMSChk.disabled = true;
		            var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		            valueBalNotiMailChk.checked = false;
		            valueBalNotiMailChk.disabled = true;
					var valueBalNotiValue = document.getElementById('balanceNotificationValue');
					var valueBalNotiDays = document.getElementById('balanceNotificationDays');
					valueBalNotiValue.disabled=true;
					valueBalNotiDays.disabled=true;			            
		        }
		    }

              function changeBalNotiValue()
              {
  	            var balNotiValue = document.getElementById('balanceNotificationTypeValue').checked;
				var valueBalNotiValue = document.getElementById('balanceNotificationValue');
  	            if(balNotiValue == true)
	            {
					valueBalNotiValue.disabled=false;
  	            }
  	            else
	            {
					valueBalNotiValue.disabled=true;
  	            }
              }
  
              function changeBalNotiDays()
              {
  	            var balNotiDays = document.getElementById('balanceNotificationTypeDays').checked;
				var valueBalNotiDays = document.getElementById('balanceNotificationDays');					
  	            if(balNotiDays == true)
	            {
					valueBalNotiDays.disabled=false;						
				}
				else
				{
					valueBalNotiDays.disabled=true;						
				}
              }	
              
		    function changeBalNotiSMS() 
		    {
		        var balNotiSMS = document.getElementById('balanceNotificationSMS').checked;
		        var valueBalNotiMail = document.getElementById('balanceNotificationMail');
		        if (balNotiSMS == true) 
		        {
		            valueBalNotiMail.checked = false;
		        }
		        else 
		        {
		            valueBalNotiMail.checked = true;
		        }
		    }

		    function changeBalNotiMail() 
		    {
		        var balNotiMail = document.getElementById('balanceNotificationMail').checked;
        		var valueBalNotiSMS = document.getElementById('balanceNotificationSMS');		        
		        if (balNotiMail == true) 
		        {
		    
		            valueBalNotiSMS.checked = false;
		        }
		        else 
		        {
		            valueBalNotiSMS.checked = true;
		        }
		    } 

  </script>

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.reps_edit.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#ffffff" class="formArea2">

              <table border="0" width="100%" cellpadding="0" cellspacing="0" align="left">
                <tr>
                  <td>
                  <form id="formRep" name="formRep" method="post" action="admin/customers/reps_edit.jsp" onSubmit="return check_form(formRep);">
                      <table width="100%">

                        <input type="hidden" name="repId" value="<%=Rep.getRepId()%>">
<%
									if (DebisysConfigListener.getDebugJS(application)) {
										String vars = "\"repType=" + Rep.getRepCreditType() + "\"";
										out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
										vars = "\"sharedBalance="  + Rep.getSharedBalance() + "\"";
										out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
									}
                          if (repErrors != null)
                          {
                            out.println("<tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                                    "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

                            Enumeration enum1 = repErrors.keys();

                            while (enum1.hasMoreElements())
                            {
                              String strKey   = enum1.nextElement().toString();
                              String strError = (String)repErrors.get(strKey);

                              if (strError != null && !strError.equals(""))
                              {
                                out.println("<li>" + strError);
                              }
                            }

                            out.println("</font></td></tr>");
                          }
%>
                          <tr>
                            <td class="formAreaTitle2">
                              <%= Languages.getString("jsp.admin.customers.reps_add.company_information",SessionData.getLanguage()) %>
                            </td>
                          </tr>
                          <tr>
                            <td class="formArea2">
                              <table>
<%
                                if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(
                                        DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT))
                                {
%>
									<tr>
										<td></td>
										<td>
<%
									if ( Rep.hasNewRatePlanMerchants() )
									{
%>
                                      <img src='images/risk-icon.png'>&nbsp;<span class='main'><%=Languages.getString("jsp.admin.rateplans.WarningMoveActor",SessionData.getLanguage())%></span>
<%
									}
%>
										</td>
									</tr>
                                  <tr>
                                    <td class="main">
                                      <b><%= Languages.getString("jsp.admin.customers.reps_add.subagent",SessionData.getLanguage()) %>:</b>
                                    </td>
                                    <td nowrap class="main">
                                      <input type="text" name="repName" value="<%= Rep.getRepName(Rep.getParentRepId()) %>" size="30" readonly>
<%
                                      if (repErrors != null && repErrors.containsKey("parentRepId"))
                                      {
                                        out.print("<font color=ff0000>*</font>");
                                      }
%>
                                      <input type="button" name="repSelect" value="<%= Languages.getString("jsp.admin.customers.reps_add.select_subagent",SessionData.getLanguage()) %>" onClick="window.open('/support/admin/customers/subagent_selector.jsp?search=y','repSelect','width=500,height=500,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
                                    </td>
                                  </tr>
                                  <input type="hidden" name="parentRepId" value="<%= Rep.getParentRepId() %>">
                                   <input type="hidden" name="oldParentRepId" value="<%= Rep.getOldParentRepId() %>">
<%
                                }
                                else
                                if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
                                {
%>
                                  <tr>
                                    <td class="main">
                                      <b><%= Languages.getString("jsp.admin.customers.reps_add.subagent",SessionData.getLanguage()) %>:</b>
                                    </td>
                                    <td nowrap class="main">
                                      <%= SessionData.getProperty("company_name") %>
                                    </td>
                                  </tr>
                                  <input type="hidden" name="parentRepId" value="<%= SessionData.getProperty("ref_id") %>">
<%
                                }
                                else
                                if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(
                                        DebisysConstants.DIST_CHAIN_3_LEVEL)))
                                {
%>
                                  <tr>
                                    <td class="main">
                                      <b><%= Languages.getString("jsp.admin.customers.reps_add.iso",SessionData.getLanguage()) %>:</b>
                                    </td>
                                    <td nowrap class="main">
                                      <%= SessionData.getProperty("company_name") %>
                                    </td>
                                  </tr>
                                  <input type="hidden" name="parentRepId" value="<%= SessionData.getProperty("ref_id") %>">
<%
                                }
%>
                                <%
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
   {//If when deploying in Mexico
       
%>
                                <tr>
                                  <td class="main">
                                    <b><%= Languages.getString("jsp.admin.customers.reps_edit.legal_businessname",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap class="main">
                                    <input type="text" name="legalBusinessname" value="<%= Rep.getLegalBusinessname() %>" size="20" maxlength="50" tabIndex="<%= intTabIndex++ %>">
                                  <%
                                    if (repErrors != null && repErrors.containsKey("legalBusinessname"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
                                   %>
                                   </td>
                                </tr>
  <%}%>
                                <tr>
                                  <td class="main">
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.business_name",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap class="main">
                                    <input type="text" name="businessName" value="<%= Rep.getBusinessName() %>" size="20" maxlength="50" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("businessName"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
<%
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
   {//If when deploying in Mexico
%>
                                                <TR>
                                                <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.businesstype",SessionData.getLanguage())%>:</B></TD>
                                                <TD NOWRAP CLASS="main">
                                                    <SELECT NAME="businessTypeId">
<%
  Vector vecBusinessTypes = com.debisys.customers.Merchant.getBusinessTypes(customConfigType);
  Iterator itBusinessTypes = vecBusinessTypes.iterator();
  while (itBusinessTypes.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itBusinessTypes.next();
    String strBusinessTypeId = vecTemp.get(0).toString();
    String strBusinessTypeCode = vecTemp.get(1).toString();
    if ( (request.getParameter("businessTypeId") != null) && (strBusinessTypeId.equals(request.getParameter("businessTypeId"))) )
        out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\" SELECTED>" + strBusinessTypeCode + "</OPTION>");
    else if ( strBusinessTypeId.equals(Rep.getBusinessTypeId()) )
        out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\" SELECTED>" + strBusinessTypeCode + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\">" + strBusinessTypeCode + "</OPTION>");
  }
%>
                                                    </SELECT>
                                                </TD>
                                                </TR>
                                                <TR>
                                                <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment",SessionData.getLanguage())%>:</B></TD>
                                                <TD NOWRAP CLASS="main">
                                                    <SELECT NAME="merchantSegmentId">
<%
  Vector vecMerchantSegments = com.debisys.customers.Merchant.getMerchantSegments();
  Iterator itMerchantSegments = vecMerchantSegments.iterator();
  while (itMerchantSegments.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itMerchantSegments.next();
    String strMerchantSegmentId = vecTemp.get(0).toString();
    String strMerchantSegmentCode = vecTemp.get(1).toString();
    if ( (request.getParameter("merchantSegmentId") != null) && strMerchantSegmentId.equals(request.getParameter("merchantSegmentId")))
      out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
    else if ( strMerchantSegmentId.equals(Integer.toString(Rep.getMerchantSegmentId())) )
      out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
  }
%>
                                                    </SELECT>
                                                </TD>
                                                </TR>
                                                <TR>
                                                    <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.businesshours",SessionData.getLanguage())%>:</B></TD>
                                                    <TD NOWRAP CLASS="main">
                                                        <INPUT  TYPE="text" NAME="businessHours" VALUE="<%=Rep.getBusinessHours()%>" SIZE="20" MAXLENGTH="50">
                                                    </TD>
                                                </TR>
<%
   } //End of if when deploying in Mexico
%>
								<TR>
									<TD CLASS="main">
										<B><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZone",SessionData.getLanguage())%>:</B>
									</TD>
									<TD NOWRAP CLASS="main">
										<SELECT NAME="timeZoneId">
<%
										Iterator<Vector<String>> itList = TimeZone.getTimeZoneList().iterator();
										while ( itList.hasNext() )
										{
											Vector vItem = itList.next();
											String sSelected = "";
											if (vItem.get(0).toString().equals(Integer.toString(Rep.getTimeZoneId())))
											{
												sSelected = "SELECTED";
											}
%>
											<OPTION VALUE="<%=vItem.get(0).toString()%>" <%=sSelected%>><%=vItem.get(2).toString()%> [<%=vItem.get(1).toString()%>]</OPTION>
<%
											vItem.clear();
											vItem = null;
										}
										itList = null;
%>
										</SELECT>
									</TD>
								</TR>
								<TR>
									<TD CLASS="main">&nbsp;</TD>
									<TD CLASS="main">
										<FONT COLOR="red"><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZoneWarning",SessionData.getLanguage())%></FONT>
									</TD>
								</TR>
	<%
	// DBSY-931 System 2000 Tools s2k
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If when deploying in International and user is an ISO level
		String strRefId = SessionData.getProperty("ref_id");
%>
<tr>
<TD CLASS="main">
<B><%= Languages.getString("jsp.admin.tools.s2k.addedit.terms",SessionData.getLanguage())%>:</B>
</TD>
 <TD NOWRAP CLASS="main">
 <INPUT  TYPE="text" NAME="terms" VALUE="<%=Rep.getterms()%>" SIZE="6" MAXLENGTH="4">
</TD>
</tr>
<tr>
<TD CLASS="main">
<B><%= Languages.getString("jsp.admin.customers.salesmanid",SessionData.getLanguage())%>:</B>
									</TD>
									<TD NOWRAP CLASS="main">

<%
					Vector salesman = new s2k().gets2ksalesman(SessionData.getProperty("iso_id"));
%>
	<select name="salesmanid" id ="salesmanid" onchange="loadid('<%=strRefId%>');" tabIndex="<%= intTabIndex++ %>" >
                             <%
                                out.println("<OPTION VALUE=\"\" >" + "</OPTION>");
                               for ( int v=0 ; v<salesman.size();v++){
                                String temp = ((Vector)salesman.get(v)).get(0).toString();
								out.println("<OPTION VALUE=\"" + temp );
								if(Rep.getsalesmanid().equals(temp))
								 out.println("\" selected >" +  temp  + "</OPTION>");
								else if(temp.equals(request.getParameter("salesmanid")))
								 	out.println("\" selected >" +  temp  + "</OPTION>");
								else
								 out.println("\"  >" +  temp  + "</OPTION>");
								 }
								  %>
                              </select>

<B>     <%= Languages.getString("jsp.admin.customers.salesmanname",SessionData.getLanguage())%>:</B>
     <select name="salesmanname"  id="salesmanname"  onchange="loadname('<%=strRefId%>');" tabIndex="<%= intTabIndex++ %>" >
                             <%
                          out.println("<OPTION VALUE=\"\" >" + "</OPTION>");
                                for ( int v=0 ; v<salesman.size();v++){
                                String temp = ((Vector)salesman.get(v)).get(1).toString();
								out.println("<OPTION VALUE=\"" + temp );
								if(Rep.getsalesmanname().equals(temp))
								 	out.println("\" selected >" +  temp  + "</OPTION>");
								else if(temp.equals(request.getParameter("salesmanname")))
								 	out.println("\" selected >" +  temp  + "</OPTION>");
								else
								 	out.println("\"  >" +  temp  + "</OPTION>");
								 } %>
                              </select>
                                  </td>
                              </tr>
          <%}

		// DBSY-1072 eAccounts Interface
		if(SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
%>
														<tr>
															<td class="main">
																<b><%= Languages.getString("jsp.admin.customers.entity_account_type",SessionData.getLanguage())%>:</b>
															</td>
<%
			// Determine if a warning needs to be displayed if current entity to External when
			// there are Internal subsidiaries
			int internalChildrenExist = EntityAccountTypes.EntityHasInternalChildren(Rep.getRepId(), DebisysConstants.REP);

			// Determine if the parent account is External/Internal
			// If External, child should be External and no changes are allowed without first changing the parent
			boolean changeAllowed = EntityAccountTypes.SubsidiaryCanChange(Rep.getParentRepId());
			boolean is3level = false;
			int acctType = Rep.getEntityAccountType();
			if(strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(
	                    DebisysConstants.DIST_CHAIN_3_LEVEL)
	           )
			{
				is3level = true;
			}
			if(changeAllowed)
			{
				Hashtable<Integer, String> ht_accountTypes = EntityAccountTypes.getAccountTypes();
%>
															<td>
																<select id="entityAccountType" name="entityAccountType">
<%
				for (Enumeration<Integer> e = ht_accountTypes.keys(); e.hasMoreElements();) {
					int type_id = e.nextElement();
					if(type_id == acctType) {
						out.println("<option value=\"" + type_id + "\" selected>" + ht_accountTypes.get(type_id) + "</option>");
					} else {
			    		out.println("<option value=\"" + type_id + "\">" + ht_accountTypes.get(type_id) + "</option>");
					}
				}
%>
																</select>
																<input type="hidden" id="disabledentityAccountType" name="disabledentityAccountType" value="" />
																<input type="hidden" id="internalChildrenExist" name="internalChildrenExist" value="<%=internalChildrenExist%>" />
															</td>
<%
			} else {
%>

															<td>
															<%if(is3level){ %>
																<select id="entityAccountType" name="entityAccountType" >
															<%}else{%>
																<select id="entityAccountType" name="entityAccountType" disabled=true >
															<%}
				Hashtable<Integer, String> ht_accountTypes = EntityAccountTypes.getAccountTypes();
				int old_type = -1;
				for (Enumeration<Integer> e = ht_accountTypes.keys(); e.hasMoreElements();) {
					int type_id = e.nextElement();
					if(type_id == acctType) {
						old_type = type_id;
						out.println("<option value=\"" + type_id + "\" selected>" + ht_accountTypes.get(type_id) + "</option>");
					} else {
			    		out.println("<option value=\"" + type_id + "\">" + ht_accountTypes.get(type_id) + "</option>");
					}
				}
%>
																</select>
																<input type="hidden" id="disabledentityAccountType" name="disabledentityAccountType" value="1" />
																<input type="hidden" id="internalChildrenExist" name="internalChildrenExist" value="<%=internalChildrenExist%>" />
															</td>
<%
			}
%>
														</tr>
<%
		}
%>
<script type="text/javascript">
function addToParent(repId, repName,isUnlimited,CanChangeAccountType)
{
  document.formRep.parentRepId.value = repId;
  document.formRep.repName.value = repName;
  <%
   // DBSY-1072 eAccounts Interface
   if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)){
%>

										var combo = document.getElementById('entityAccountType');
										if(CanChangeAccountType !== null){
											if(!CanChangeAccountType){
											      combo.disabled = true;
											      combo.options.selectedIndex ="0";
											      var temp = document.getElementById('disabledentityAccountType');
											      temp.value = "1";
											}else
												{
												combo.disabled = false;
												combo.options.selectedIndex ="0";
												var temp = document.getElementById('disabledentityAccountType');
											    temp.value = "";
											}

										}else
										{
											combo.disabled = false;
											combo.options.selectedIndex ="0";
											var temp = document.getElementById('disabledentityAccountType');
											temp.value = "";
										}

							<%}%>
}
</script>
                              </table>
                            </td>
                          </tr>
                          
                                        <%
                                            // Invoicing payment types for Mexico reps
                                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                // Get all the information required
                                                pageContext.setAttribute("invoicingView", "EDIT");
                                                pageContext.setAttribute("invoicePaymentTypesSectionTitle", Languages.getString("jsp.admin.customers.reps.InvoiceTitle",SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicingTypeLabel", Languages.getString("jsp.admin.customers.reps.InvoiceType",SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicePaymentTypeNameColumTitle", Languages.getString("jsp.admin.customers.reps.InvoicePaymentType",SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicePaymentTypeAccountColumTitle", Languages.getString("jsp.admin.customers.reps.InvoicePaymentAccountNumber",SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicePaymentTypeImageEditLabel", Languages.getString("jsp.admin.customers.reps.InvoiceEdit", SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicePaymentTypeImageDeleteLabel", Languages.getString("jsp.admin.customers.reps.InvoiceDelete", SessionData.getLanguage()));
                                                pageContext.setAttribute("addInvoicePaymentTypeButtonLabel", Languages.getString("jsp.admin.customers.reps.InvoiceAddPaymentType",SessionData.getLanguage()));                                                
                                                List<InvoiceType>invoiceTypes = InvoicePaymentTypesDao.getAllInvoiceTypes();
                                                pageContext.setAttribute("invoiceTypes", invoiceTypes);  
                                                String jsonString = request.getParameter("repInvoiceTypesList");
                                                Map<String, RepInvoicePaymentType> clientPaymentTypesMap = getClientPaymentTypes(jsonString);
                                                List<RepInvoicePaymentType> repInvoicePaymentTypes = null;
                                                // If there are values comming from the client they are assumed to be the last ones to be shown
                                                if(clientPaymentTypesMap != null){
                                                    repInvoicePaymentTypes = new ArrayList<RepInvoicePaymentType>(clientPaymentTypesMap.values());
                                                }else{
                                                    repInvoicePaymentTypes = InvoicePaymentTypesDao.getAllRepInvoicePaymentTypes(Long.valueOf(Rep.getRepId()));
                                                }
                                                setJsInvoicingGlobals(out, SessionData.getLanguage(), Rep.getRepId(), repInvoicePaymentTypes);
                                                setJsPaymentTypes(out);                                                
                                                pageContext.setAttribute("repInvoicePaymentTypes", repInvoicePaymentTypes);
                                        %>       
                                            <link href="css/admin/customers/reps/repInvoicePaymentTypes.css" type="text/css" rel="StyleSheet" />
                                            <script src="js/admin/customers/reps/repInvoicePaymentTypes.js" type="text/javascript"></script>
                                            <%@ include file="/WEB-INF/jspf/admin/customers/reps/repInvoicePaymentTypesView.jspf" %>                                            
                                        <%
                                            }
                                        %>
                          <tr>
                            <td class="formAreaTitle2">
                              <br>
                              <%= Languages.getString("jsp.admin.customers.reps_add.address",SessionData.getLanguage()) %>
                            </td>
                          </tr>
                          <tr>
                            <td class="formArea2">
                              <table width="100">
                                <tr class="main">
                                  <td>
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.street_address",SessionData.getLanguage()) %>:</b>
                                  </td>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
               						<TD COLSPAN="4" NOWRAP><TEXTAREA id="address" NAME="address" ROWS="2" COLS="50" ONKEYUP="if (this.value.length > 50) { this.value = this.value.substring(0, 50); }" ONBLUR="ValidateTextAreaLength(this, 50);"><%=Rep.getAddress()%></TEXTAREA><%if (repErrors != null && repErrors.containsKey("physAddress")) out.print("<font color=ff0000>*</font>");%></TD>
<%
  } else { //Else if when deploying in others
%>
                                  <td nowrap>
                                    <input type="text" name="address" value="<%= Rep.getAddress() %>" size="50" maxlength="50" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("address"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
<%
  }
%>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                    </TR>
                                                    <TR CLASS="main">
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage())%>:</TD>
               						<TD NOWRAP><INPUT TYPE="text" id="physColony" NAME="physColony" VALUE="<%=Rep.getPhysColony()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage())%>:</TD>
               						<TD NOWRAP><INPUT TYPE="text" id="physDelegation" NAME="physDelegation" VALUE="<%=Rep.getPhysDelegation()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
<%
  } //End of if when deploying in Mexico
%>
                                </tr>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                    <TR CLASS="main">
               						<TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())%>:</TD>
                                                        <TD NOWRAP><INPUT TYPE="text" id="city" NAME="city" VALUE="<%=Rep.getCity()%>" SIZE="20" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("city")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage())%></TD>
                                                        <TD NOWRAP><INPUT TYPE="text" id="zip" NAME="zip" VALUE="<%=Rep.getZip()%>" SIZE="20" MAXLENGTH="5" TABINDEX="<%=intTabIndex++%>" ONBLUR="ValidateRegExp(this, '[0-9]{5}');"><%if (repErrors != null && repErrors.containsKey("zip")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><B><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())%></B></TD>
                                                        <TD NOWRAP>
                                                            <SELECT NAME="state" id="state" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
  Iterator itStates = vecStates.iterator();
  while (itStates.hasNext()) {
	    Vector vecTemp = null;
	    vecTemp = (Vector)itStates.next();
	    String strStateId = vecTemp.get(0).toString();
	    String strStateName = vecTemp.get(1).toString();
	    if ( (request.getParameter("state") != null) && (strStateId.equals(request.getParameter("state"))) )
	      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
	    else if ( strStateId.equals(Rep.getState()) )
	      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
	    else
	      out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
	  }
%>
                                                            </SELECT>
                                                        </TD>
                                                    </TR>
              						<tr class="main">
               						<td><%=Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage())%>:</td>
               						<td nowrap><input type="text" id="physCounty" name="physCounty" value="<%=Rep.getPhysCounty()%>" size="20" maxlength="20" tabIndex="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("physCounty")) out.print("<font color=ff0000>*</font>");%></td>
              						</tr>
<%
  } //End of if when deploying in Mexico
  else { //Else other deployments
%>
                                <tr class="main">
                                  <td nowrap>
<%
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                                    {
                                      out.println("<b>" + Languages.getString("jsp.admin.customers.reps_add.city",SessionData.getLanguage()) + ":</b>")
                                              ;
                                    }
                                    else
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                    {
                                      out.println(Languages.getString("jsp.admin.customers.reps_add.city",SessionData.getLanguage()) + ":");
                                    }
%>
                                  </td>
                                  <td nowrap>
                                    <input type="text" id="city" name="city" value="<%= Rep.getCity() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("city"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }

                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                                    {
                                      out.println("<b>" + Languages.getString("jsp.admin.customers.reps_add.state_domestic",SessionData.getLanguage())
                                              + ":</b>");
                                    }
                                    else
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                    {
                                      out.println(Languages.getString("jsp.admin.customers.reps_add.state_international",SessionData.getLanguage()) +
                                              ":");
                                    }
%>
                                    <input type="text" id="state" name="state" value="<%= Rep.getState() %>" size="3" maxlength="2" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("state"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
<%
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                                    {
                                      out.println("<b>" + Languages.getString("jsp.admin.customers.reps_add.zip_domestic",SessionData.getLanguage()) +
                                              ":</b>");
                                    }
                                    else
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                    {
                                      out.println(Languages.getString("jsp.admin.customers.reps_add.zip_international",SessionData.getLanguage()) + ":"
                                              );
                                    }
%>
                                    <input type="text" id="zip" name="zip" value="<%= Rep.getZip() %>" size="10" maxlength="10" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("zip"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
<%
  } //End of else other deployments
%>
                                <tr class="main">
                                  <td>
<%
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                    {
                                      out.println("<b>");
                                    }
%>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.country",SessionData.getLanguage()) %>:
                                  </td>

                             <td nowrap>

<%
					     String[][] countries = CountryInfo.getAllCountries(CountryInfo.getIso(SessionData.getProperty("ref_id"),SessionData.getProperty("ref_type")));
					if(countries[0][0].length() > 0 ){
					%>

	<select id="country" name="country" tabIndex="<%= intTabIndex++ %>" >
                             <%
                             if(countries.length == 1){
                                out.println("<OPTION VALUE=\"" + countries[0][0].toString() + "\" SELECTED>" + countries[0][1].toString()  + "</OPTION>");
                             }
                             else{
                              out.println("<OPTION VALUE=\"\">" + "" + "</OPTION>");
                               for ( int v=0 ; v<countries.length;v++){
                                	if ( countries[v][0].toString().equals(Rep.getCountry().toString()) )
											out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\" SELECTED>" + countries[v][1].toString()  + "</OPTION>");
								  	else
											out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\">" + countries[v][1].toString()  + "</OPTION>");

                             }
                             }
 %>
                              </select>
<%
                                   }
                                   else
                                   {
                                   		out.print("<font color=ff0000>Database Error Country must be set for ISO</font>");
                                   }
                                    if (repErrors != null && repErrors.containsKey("country"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
                                <%
								if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && hasPermissionGeolocation ){
								%>
								<tr class="main">
									<td><%=Languages.getString("jsp.admin.customers.merchants_edit.latitude",SessionData.getLanguage())%>:</td>
									<td><input name="latitude" type="hidden" value="<%=Rep.getLatitude()%>"><span name="latvalue"><%=Rep.getLatitude()%></span></td>

								</tr>
								<tr class="main">
									<td><%=Languages.getString("jsp.admin.customers.merchants_edit.longitude",SessionData.getLanguage())%>:</td>
									<td><input name="longitude" type="hidden" value="<%=Rep.getLongitude()%>"><span name="longvalue"><%=Rep.getLongitude()%></span></td>
								</tr>
								<%
								}
								else{
								%>
									<tr>
										<td>
											<input name="latitude"  type="hidden" value="<%=Rep.getLatitude()%>">
											<input name="longitude" type="hidden" value="<%=Rep.getLongitude()%>">
										</td>
									</tr>
								<%
								}
								%>
                              </table>
                            </td>
                          </tr>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
      					<TR>
      						<TD CLASS="formAreaTitle2">
                                                    <BR>
                                                    <%=Languages.getString("jsp.admin.customers.merchants_edit.billingaddress",SessionData.getLanguage())%>&nbsp;
                                                    <BR>
                                                    <INPUT TYPE="checkbox" ID="chkBillingAddress" NAME="chkBillingAddress" <%=((request.getParameter("chkBillingAddress") != null)?"CHECKED":"")%> ONCLICK="EnableBillingAddress(this.checked);"><LABEL FOR="chkBillingAddress"><%=Languages.getString("jsp.admin.customers.merchants_edit.billingaddresstitle",SessionData.getLanguage())%></LABEL>
      						</TD>
      					</TR>
     					<tr>
                                            <td class="formArea2">
                                                <table width="100">
                                                    <tr class="main">
                                                        <td><b><%=Languages.getString("jsp.admin.customers.merchants_edit.street_address",SessionData.getLanguage())%>:</b></td>
               						<TD COLSPAN="4" NOWRAP><TEXTAREA ID="mailAddress" NAME="mailAddress" ROWS="2" COLS="50" TABINDEX="<%=intTabIndex++%>" ONPROPERTYCHANGE="if (this.value.length > 50) { this.value = this.value.substring(0, 50); }" ONBLUR="ValidateTextAreaLength(this, 50);"><%=Rep.getMailAddress()%></TEXTAREA><%if (repErrors != null && repErrors.containsKey("billingAddress")) out.print("<font color=ff0000>*</font>");%></TD>
                                                    </TR>
                                                    <TR CLASS="main">
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage())%>:</TD>
               						<TD NOWRAP><INPUT TYPE="text" ID="mailColony" NAME="mailColony" VALUE="<%=Rep.getMailColony()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage())%>:</TD>
               						<TD NOWRAP><INPUT TYPE="text" ID="mailDelegation" NAME="mailDelegation" VALUE="<%=Rep.getMailDelegation()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                    </TR>
                                                    <TR CLASS="main">
               						<TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())%>:</TD>
                                                        <TD NOWRAP><INPUT TYPE="text" ID="mailCity" NAME="mailCity" VALUE="<%=Rep.getMailCity()%>" SIZE="20" MAXLENGTH="30" TABINDEX="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("billingCity")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage())%>:</TD>
                                                        <TD NOWRAP><INPUT TYPE="text" ID="mailZip" NAME="mailZip" VALUE="<%=Rep.getMailZip()%>" SIZE="20" MAXLENGTH="5" TABINDEX="<%=intTabIndex++%>" ONBLUR="ValidateRegExp(this, '[0-9]{5}');"><%if (repErrors != null && repErrors.containsKey("billingZip")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><B><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())%></B></TD>
                                                        <TD NOWRAP>
                                                            <SELECT ID="mailState" NAME="mailState" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
  Iterator itStates = vecStates.iterator();
  while (itStates.hasNext()) {
	    Vector vecTemp = null;
	    vecTemp = (Vector)itStates.next();
	    String strStateId = vecTemp.get(0).toString();
	    String strStateName = vecTemp.get(1).toString();
	    if ( (request.getParameter("mailState") != null) && (strStateId.equals(request.getParameter("mailState"))) )
	      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
	    else if ( strStateId.equals(Rep.getMailState()) )
	      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
	    else
	      out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
	  }
%>
                                                            </SELECT>
                                                        </TD>
                                                    </TR>
              						<tr class="main">
               						<td><%=Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage())%>:</td>
               						<td nowrap><input type="text" id="mailCounty" name="mailCounty" value="<%=Rep.getMailCounty()%>" size="20" maxlength="20" tabIndex="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("billingCounty")) out.print("<font color=ff0000>*</font>");%></td>
              						</tr>
              						<tr class="main">
               						<td><B><%=Languages.getString("jsp.admin.customers.merchants_edit.country",SessionData.getLanguage())%>:</B></td>
               						<td nowrap>

<%
					if(countries[0][0].length() > 0 ){
					%>

	<select id="mailCountry" name="mailCountry" tabIndex="<%= intTabIndex++ %>" >
                             <%
                             if(countries.length == 1){
                                out.println("<OPTION VALUE=\"" + countries[0][0].toString() + "\" SELECTED>" + countries[0][1].toString()  + "</OPTION>");
                             }
                             else{
                              out.println("<OPTION VALUE=\"\">" + "" + "</OPTION>");
                               for ( int v=0 ; v<countries.length;v++){
                                	if ( countries[v][0].toString().equals(Rep.getMailCountry().toString()) )
											out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\" SELECTED>" + countries[v][1].toString()  + "</OPTION>");
								  	else
											out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\">" + countries[v][1].toString()  + "</OPTION>");

                             }
                             }
 %>
                              </select>
<%
                                   }
                                   else
                                   {
                                   		out.print("<font color=ff0000>Database Error Country must be set for ISO</font>");
                                   }
                                    if (repErrors != null && repErrors.containsKey("billingCountry"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
              						</tr>
            					</table>
					          </td>
					      </tr>
<%
   if ( request.getParameter("chkBillingAddress") != null )
       out.print("<SCRIPT>EnableBillingAddress(true);</SCRIPT>");
   //else
       //out.print("<SCRIPT>EnableBillingAddress(false);</SCRIPT>");
  } //End of if when deploying in Mexico
%>
                          <tr>
                            <td class="formAreaTitle2">
                              <br>
                              <%= Languages.getString("jsp.admin.customers.reps_add.contact_information",SessionData.getLanguage()) %>
                            </td>
                          </tr>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  {//If when deploying in Mexico
%>
                                        <TR>
                                            <TD CLASS="formArea2">
                                                <INPUT TYPE="hidden" ID="mxContactAction" NAME="mxContactAction" VALUE="">
                                                <TABLE WIDTH="100%" CELLSPACING="1" CELLPADDING="1" BORDER="0">
                                                    <TR>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contacttype",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contact",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.phone",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.fax",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.email",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.cellphone",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.department",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"></TD>
                                                    </TR>
<%
    if ( Rep.getVecContacts().size() > 0 )
    {//If there are contacts to show
%>
                                                    <TR>
                                                      <TD>
                                                        <INPUT TYPE="hidden" ID="mxContactIndex" NAME="mxContactIndex" VALUE="">
                                                        <DIV STYLE="display:none;"><INPUT TYPE="text" NAME="contactData" VALUE="<%=Rep.getVecContacts().size()%>"></DIV>
                                                        <SCRIPT>
                                                            function mxEditContact()
                                                            {
                                                                addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
                                                              document.getElementById('mxContactAction').value = "EDIT";
                                                              document.formRep.onsubmit = new Function("return true;");
                                                              document.formRep.submitbutton.disabled = true;
                                                            }
                                                            function mxDeleteContact()
                                                            {
                                                                addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
                                                              document.getElementById('mxContactAction').value = "DELETE";
                                                              document.formRep.onsubmit = new Function("return true;");
                                                              document.formRep.submitbutton.disabled = true;
                                                            }
                                                        </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
      int intEvenOdd = 1;
      int contactIndex = 0;
      Vector vecContacts = Rep.getVecContacts();
      Iterator itContacts = vecContacts.iterator();
      while (itContacts.hasNext())
      {
        Vector vecTemp = null;
        vecTemp = (Vector)itContacts.next();
%>
                                                    <TR CLASS="row<%=intEvenOdd%>">
<%
        if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("EDIT")) && (request.getParameter("mxContactIndex") != null) && (contactIndex == Integer.parseInt(request.getParameter("mxContactIndex"))) )
        {//If we are editing an item
%>
                                                        <TD>
                                                          <SCRIPT>document.getElementById("mxContactIndex").value = "<%=contactIndex%>";</SCRIPT>
                                                          <SELECT NAME="contactTypeId">
<%
          Vector vecContactTypes = com.debisys.customers.Merchant.getContactTypes();
          Iterator itContactTypes = vecContactTypes.iterator();
          while (itContactTypes.hasNext())
          {
            vecTemp = null;
            vecTemp = (Vector)itContactTypes.next();
            String strContactTypeId = vecTemp.get(0).toString();
            String strContactTypeCode = vecTemp.get(1).toString();
            if (strContactTypeId.equals(Integer.toString(Rep.getContactTypeId())))
            {
              out.println("<OPTION VALUE=\"" + strContactTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
            }
            else
            {
              out.println("<OPTION VALUE=\"" + strContactTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
            }
          }
%>
                                                          </SELECT>
                                                        </TD>
                                                        <TD><INPUT TYPE="text" NAME="contactName" VALUE="<%=Rep.getContactName()%>" SIZE="15" MAXLENGTH="50"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactPhone" VALUE="<%=Rep.getContactPhone()%>" SIZE="15" MAXLENGTH="15"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactFax" VALUE="<%=Rep.getContactFax()%>" SIZE="15" MAXLENGTH="15"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactEmail" VALUE="<%=Rep.getContactEmail()%>" SIZE="15" MAXLENGTH="200"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactCellPhone" VALUE="<%=Rep.getContactCellPhone()%>" SIZE="15" MAXLENGTH="50"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactDepartment" VALUE="<%=Rep.getContactDepartment()%>" SIZE="15" MAXLENGTH="50"></TD>
<%
        }//End of if we are editing an item
        else
        {//Else just show the item data
%>
                                                        <TD><%=com.debisys.customers.Merchant.getContactTypeById(vecTemp.get(0).toString(),SessionData)%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(1).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(2).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(3).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(4).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(5).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(6).toString())%></TD>
                                                        <TD>
                                                            <TABLE>
                                                                <TR>
                                                                    <TD>
                                                                      <INPUT TYPE="image" TITLE="<%=Languages.getString("jsp.admin.customers.merchants_edit.editcontact",SessionData.getLanguage())%>" SRC="images/icon_edit.gif" ONCLICK="document.getElementById('mxContactIndex').value = '<%=contactIndex%>';mxEditContact();">
                                                                    </TD>
                                                                    <TD>&nbsp;</TD>
                                                                    <TD>
                                                                      <INPUT TYPE="image" TITLE="<%=Languages.getString("jsp.admin.customers.merchants_edit.deletecontact",SessionData.getLanguage())%>" SRC="images/icon_delete.gif" ONCLICK="document.getElementById('mxContactIndex').value = '<%=contactIndex%>';mxDeleteContact();">
                                                                    </TD>
                                                                </TR>
                                                            </TABLE>
                                                        </TD>
<%
        }//End of else just show the item data
%>
                                                    </TR>
<%
        intEvenOdd = ((intEvenOdd == 1)?2:1);
        contactIndex++;
      }
    }//End of if there are contacts to show
    else if ( (request.getParameter("mxContactAction") == null) ||
              (request.getParameter("mxContactAction") != null) && !request.getParameter("mxContactAction").equals("ADD")
            )
    {//Else show a message of no contacts
%>
                                                    <TR><TD CLASS="main" COLSPAN="8" ALIGN="center"><%=Languages.getString("jsp.admin.customers.merchants_edit.nocontacts",SessionData.getLanguage())%></TD></TR>
                                                    <TR><TD CLASS="main" COLSPAN="8" ALIGN="center"><DIV STYLE="display:none;"><INPUT TYPE="text" NAME="contactData" VALUE=""></DIV></TD></TR>
<%
    }//End of else show a message of no contacts

    if ( (request.getParameter("mxContactAction") == null) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("SAVE"))) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("CANCEL"))) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("DELETE")))
       )
    {//If we need to show the ADD button
%>
                                                    <TR>
                                                      <TD CLASS="main" COLSPAN="8" ALIGN="right">
                                                        <INPUT TYPE="button" ID="mxBtnAddContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.addcontact",SessionData.getLanguage())%>" ONCLICK="mxAddContact();">
                                                        <SCRIPT>
                                                            function mxAddContact()
                                                            {
                                                                addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
                                                              document.getElementById('mxContactAction').value = "ADD";
                                                              document.getElementById('mxBtnAddContact').disabled = true;
                                                              document.formRep.onsubmit = new Function("return true;");
                                                              document.formRep.submitbutton.click();
                                                              document.formRep.submitbutton.disabled = true;
                                                            }
                                                        </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
    }//End of if we need to show the ADD button
    else if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("ADD")) )
    {//Else if we need to show the row for a new Contact
%>
                                                    <TR CLASS="main">
                                                        <TD>
                                                          <DIV STYLE="display:none;"><INPUT TYPE="text" NAME="contactData" VALUE="<%=((Rep.getVecContacts().size() == 0)?"":"1")%>"></DIV>
                                                          <SELECT NAME="contactTypeId">
<%
      Vector vecContactTypes = com.debisys.customers.Merchant.getContactTypes();
      Iterator itContactTypes = vecContactTypes.iterator();
      while (itContactTypes.hasNext())
      {
        Vector vecTemp = null;
        vecTemp = (Vector)itContactTypes.next();
        String strContactTypeId = vecTemp.get(0).toString();
        String strContactTypeCode = vecTemp.get(1).toString();
        if (strContactTypeId.equals(request.getParameter("contactTypeId")))
        {
          out.println("<OPTION VALUE=\"" + strContactTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
        }
        else
        {
          out.println("<OPTION VALUE=\"" + strContactTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
        }
      }
%>
                                                          </SELECT>
                                                        </TD>
                                                        <TD><INPUT TYPE="text" NAME="contactName" VALUE="<%=Rep.getContactName()%>" SIZE="15" MAXLENGTH="50"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactPhone" VALUE="<%=Rep.getContactPhone()%>" SIZE="15" MAXLENGTH="15"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactFax" VALUE="<%=Rep.getContactFax()%>" SIZE="15" MAXLENGTH="15"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactEmail" VALUE="<%=Rep.getContactEmail()%>" SIZE="15" MAXLENGTH="200"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactCellPhone" VALUE="<%=Rep.getContactCellPhone()%>" SIZE="15" MAXLENGTH="50"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactDepartment" VALUE="<%=Rep.getContactDepartment()%>" SIZE="15" MAXLENGTH="50"></TD>
                                                    </TR>
<%
    }//End of else if we need to show the row for a new Contact

    if ( ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("ADD"))) ||
         ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("EDIT")))
       )
    {//If we need to show the SAVE-CANCEL button
%>
                                                    <TR>
                                                      <TD CLASS="main" COLSPAN="8" ALIGN="right">
                                                        <INPUT TYPE="button" ID="mxBtnSaveContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.savecontact",SessionData.getLanguage())%>" ONCLICK="mxSaveContact();">
                                                        <INPUT TYPE="button" ID="mxBtnCancelContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.cancelsavecontact",SessionData.getLanguage())%>" ONCLICK="mxCancelSaveContact();">
                                                        <SCRIPT>
                                                            function mxSaveContact()
                                                            {
                                                              error = false;
                                                              form = document.rep;
                                                              error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";
                                                              check_input("contactName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact",SessionData.getLanguage())%>");
                                                              check_input("contactPhone", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_phone",SessionData.getLanguage())%>");
                                                              if ( error )
                                                              {
                                                                alert(error_message);
                                                                return false;
                                                              }
                                                              addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
                                                              document.getElementById('mxContactAction').value = "SAVE";
                                                              document.getElementById('mxBtnSaveContact').disabled = true;
                                                              document.formRep.onsubmit = new Function("return true;");
                                                              document.formRep.submitbutton.click();
                                                              document.formRep.submitbutton.disabled = true;
                                                            }

                                                            function mxCancelSaveContact()
                                                            {
                                                                addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
                                                              document.getElementById('mxContactAction').value = "CANCEL";
                                                              document.getElementById('mxBtnSaveContact').disabled = true;
                                                              document.getElementById('mxBtnCancelContact').disabled = true;
                                                              document.formRep.onsubmit = new Function("return true;");
                                                              document.formRep.submitbutton.click();
                                                              document.formRep.submitbutton.disabled = true;
                                                            }
                                                        </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
    }//End of if we need to show the SAVE-CANCEL button
%>
                                                </TABLE>
                                            </TD>
                                        </TR>
                                        <INPUT type="hidden" id="hidSharedBalance" name="sharedBalance" value="<%=Rep.getSharedBalance()%>"/>
                                        <INPUT type="hidden" id="hidRepCreditType" name="repCreditType" value="<%=Rep.getRepCreditType()%>"/>
				     						<tr>
					        					<td class="formArea2">
					        						<table width="100">
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="smsNotificationPhone" id="smsNotificationPhone" value="<%=Rep.getSmsNotificationPhone()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="mailNotificationAddress" id="mailNotificationAddress" value="<%=Rep.getMailNotificationAddress()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
													</tr>												
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" onClick="changePayNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" onClick="changePayNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" onClick="changePayNotiMail()" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" onClick="changeBalNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" onClick="changeBalNotiValue()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" onClick="changeBalNotiDays()" />
														</td>
													</tr>													
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationValue" id="balanceNotificationValue" value="<%=Rep.getBalanceNotificationValue()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationDays" id="balanceNotificationDays" value="<%=Rep.getBalanceNotificationDays()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" onClick="changeBalNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" onClick="changeBalNotiMail()" />
														</td>
													</tr>
<%
   }
%>	
													</table>
						            				<script>chkPaymentNoti()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>chkBalanceNoti()</script>
<%
   }
%>													
												</td>
											</tr>
<%
  }
  else
  {//Else when deploying in others
%>
                          <tr>
                            <td class="formArea2">
                              <table width="100">
                                <tr class="main">
                                  <td nowrap>
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.contact",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap>
                                    <table border=0>
                                      <tr class=main>
                                        <td>
                                          <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
                           				<%= Languages.getString("jsp.admin.customers.reps_add.contact_first",SessionData.getLanguage()) %>
                    		        	<% } else{%>
            		                	<%= Languages.getString("jsp.admin.customers.reps_add.contact_first_international",SessionData.getLanguage()) %>
			                            <%}%>
			                            </td>
                                        <td>
                                          <%= Languages.getString("jsp.admin.customers.reps_add.contact_middle",SessionData.getLanguage()) %>
                                        </td>
                                        <td>
                                          <%= Languages.getString("jsp.admin.customers.reps_add.contact_last",SessionData.getLanguage()) %>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <input type="text" name="contactFirst" value="<%= Rep.getContactFirst() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                          if (repErrors != null && repErrors.containsKey("contactFirst"))
                                          {
                                            out.print("<font color=ff0000>*</font>");
                                          }
%>
                                        </td>
                                        <td>
                                          <input type="text" name="contactMiddle" value="<%= Rep.getContactMiddle() %>" size="2" maxlength="1" tabIndex="<%= intTabIndex++ %>">
<%
                                          if (repErrors != null && repErrors.containsKey("contactMiddle"))
                                          {
                                            out.print("<font color=ff0000>*</font>");
                                          }
%>
                                        </td>
                                        <td>
                                          <input type="text" name="contactLast" value="<%= Rep.getContactLast() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                          if (repErrors != null && repErrors.containsKey("contactLast"))
                                          {
                                            out.print("<font color=ff0000>*</font>");
                                          }
%>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr class="main">
                                  <td>
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.phone",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap>
                                    <input type="text" name="contactPhone" value="<%= Rep.getContactPhone() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("contactPhone"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
                                <tr class="main">
                                  <td>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.fax",SessionData.getLanguage()) %>:
                                  </td>
                                  <td nowrap>
                                    <input type="text" name="contactFax" value="<%= Rep.getContactFax() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("contactFax"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
                                <tr class="main">
                                  <td>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.email",SessionData.getLanguage()) %>:
                                  </td>
                                  <td nowrap>
                                    <input type="text" name="contactEmail" value="<%= Rep.getContactEmail() %>" size="20" maxlength="200" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("contactEmail"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="smsNotificationPhone" id="smsNotificationPhone" value="<%=Rep.getSmsNotificationPhone()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="mailNotificationAddress" id="mailNotificationAddress" value="<%=Rep.getMailNotificationAddress()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
													</tr>												
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" onClick="changePayNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" onClick="changePayNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" onClick="changePayNotiMail()" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" onClick="changeBalNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" onClick="changeBalNotiValue()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" onClick="changeBalNotiDays()" />
														</td>
													</tr>													
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationValue" id="balanceNotificationValue" value="<%=Rep.getBalanceNotificationValue()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationDays" id="balanceNotificationDays" value="<%=Rep.getBalanceNotificationDays()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" onClick="changeBalNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" onClick="changeBalNotiMail()" />
														</td>
													</tr>
<%
   }
%>		
						            					</table>
						            					<script>chkPaymentNoti()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>chkBalanceNoti()</script>
<%
   }
%>														
                            </td>
                          </tr>
<%
  }//End of else when deploying in others
%>

			  <%
              if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES))
              {
              %>

						<tr>
      						<td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.common_info_edit.payment_information",SessionData.getLanguage())%></td>
      					</tr>
      					<tr>
	        					<td class="formArea2">
	        						<table width="100">
              						<tr class="main">
              						 <td nowrap>
              						   <%=Languages.getString("jsp.admin.customers.common_info_edit.payment_type",SessionData.getLanguage())%>
              						 </td>
               						 <td nowrap>
               						  <SELECT NAME="paymentType">
               						   <%
										  Vector vecPaymentTypes = com.debisys.customers.Rep.getPaymentTypes();
										  Iterator itPaymentTypes = vecPaymentTypes.iterator();
										  boolean flag_payment=false;
										  while (itPaymentTypes.hasNext()) {
										    Vector vecTemp = null;
										    vecTemp = (Vector)itPaymentTypes.next();
										    String strPaymentTypeId = vecTemp.get(0).toString();
										    String strPaymentTypeCode = vecTemp.get(1).toString();
										    if ( (request.getParameter("paymentType") != null) && (strPaymentTypeId.equals(request.getParameter("paymentType"))) ){
										        flag_payment = true;
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
										    }
										    else if ( strPaymentTypeId.equals(Integer.toString(Rep.getPaymentType())) ){
										        flag_payment = true;
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
										    }
										    else
										      out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\">" + strPaymentTypeCode + "</OPTION>");
										  }
										  if ( !flag_payment ){
										   %>
										      <OPTION VALUE="-1" SELECTED>--</OPTION>
										   <%
										  }
										%>

               						  </SELECT>
               						  </TD>

               						  <td nowrap>
               						  <% if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
						                		   <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type",SessionData.getLanguage())%>
						                		   <% } else{%>
						                		   <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type_international",SessionData.getLanguage())%>
						               <%}%>
              						 </td>
               						 <td nowrap>
               						  <SELECT NAME="processType">
               						     <%
										  Vector vecProcessTypes = com.debisys.customers.Rep.getProcessorTypes();
										  Iterator itProcessTypes = vecProcessTypes.iterator();
										  boolean flag_process=false;
										  while (itProcessTypes.hasNext()) {
										    Vector vecTemp = null;
										    vecTemp = (Vector)itProcessTypes.next();
										    String strProcessTypeId = vecTemp.get(0).toString();
										    String strProcessTypeCode = vecTemp.get(1).toString();

										    if ( (request.getParameter("processType") != null) && (strProcessTypeId.equals(request.getParameter("processType"))) ){
										        flag_process = true;
										        out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
										    }
										    else if ( strProcessTypeId.equals(Integer.toString(Rep.getProcessType())) ){
										        flag_process = true;
										        out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
										    }
										    else
										      out.println("<OPTION VALUE=\"" + strProcessTypeId + "\">" + strProcessTypeCode + "</OPTION>");
										  }
										  if ( !flag_process ){
										   %>
										      <OPTION VALUE="-1" SELECTED>--</OPTION>
										   <%
										  }
										%>
               						  </SELECT>
               						  </TD>
               						  </tr>
               						</table>
               			 </tr>
              <%
			  }
              %>
                          <tr>
                            <td class="formAreaTitle2">
                              <br>
                              <%= Languages.getString("jsp.admin.customers.reps_add.banking_information",SessionData.getLanguage()) %>
                            </td>
                          </tr>
                          <tr>
                            <td class="formArea2">
                              <table width="100">
                                <tr class="main">
                                  <td nowrap>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
    out.print("<B>" + Languages.getString("jsp.admin.customers.merchants_edit.tax_id",SessionData.getLanguage()) + ":</B>");
%>
               						</TD><td nowrap><input type="text" name="taxId" value="<%=Rep.getTaxId()%>" size="15" maxlength="15" tabIndex="<%=intTabIndex++%>" ONBLUR="ValidateRegExp(this, '[A-Z]{3,4}-[0-9]{6}-[A-Z0-9]{3}')"><%if (repErrors != null && repErrors.containsKey("taxId")) out.print("<font color=ff0000>*</font>");%>
                                                        &nbsp;<%=Languages.getString("jsp.admin.customers.merchants_edit.sample_taxid",SessionData.getLanguage())%>
               						</td>
              						</tr>
<%
  } else { //Else when deploying in others
    out.print(Languages.getString("jsp.admin.customers.reps_add.tax_id_international",SessionData.getLanguage()) + ":");
%>
               						</TD><td nowrap>
                                    <input type="text" name="taxId" value="<%= Rep.getTaxId() %>" size="15" maxlength="12" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("taxId"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
<%
  }
%>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  {//If when deploying in Mexico
%>
                                        <TR CLASS="main">
                                            <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.bankname",SessionData.getLanguage())%>:&nbsp;</TD>
                                            <TD><INPUT TYPE="text" NAME="bankName" VALUE="<%=Rep.getBankName()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("bankName")) out.print("<font color=ff0000>*</font>");%></TD>
                                        </TR>
                                        <TR CLASS="main">
                                            <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.accounttype",SessionData.getLanguage())%>:&nbsp;</TD>
                                            <TD>
                                                <SELECT NAME="accountTypeId" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
  Iterator itAccountTypes = vecAccountTypes.iterator();
  while (itAccountTypes.hasNext()) {
	    Vector vecTemp = null;
	    vecTemp = (Vector)itAccountTypes.next();
	    String strAccountTypeId = vecTemp.get(0).toString();
	    String strAccountTypeCode = vecTemp.get(1).toString();
	    if ( (request.getParameter("accountTypeId") != null) && (strAccountTypeId.equals(request.getParameter("accountTypeId"))) )
	      out.println("<OPTION VALUE=\"" + strAccountTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
	    else if (strAccountTypeId.equals(Integer.toString(Rep.getAccountTypeId())))
	      out.println("<OPTION VALUE=\"" + strAccountTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
	    else
	      out.println("<OPTION VALUE=\"" + strAccountTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
	                            }
%>
                                                </SELECT>
                                            </TD>
                                        </TR>
<%
  } //End of if when deploying in Mexico
%>
                                <tr class="main">
                                  <td nowrap>
                                    <% if ( (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) ){%>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.routing_number",SessionData.getLanguage()) %>
								<%}else{%>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.routing_number_international",SessionData.getLanguage()) %>
									<%}%>:
                                  </td>
                                  <td nowrap>
                                    <%String textErrorAba = Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage()); %>
                                    <input type="text" id="routingNumber" name="routingNumber" value="<%= Rep.getRoutingNumber() %>" size="15"
                                    maxlength="9" tabIndex="<%= intTabIndex++ %>"

                                    <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                    	<%String regExpresion="[0-9]{9}";%>
											onKeyUp="verifyAba(9,'<%=regExpresion%>','<%=textErrorAba%>');"
										<%if(Rep.getRoutingNumber().startsWith("NA")){ %>
										 	disabled = "false";
										<%} %>
									<%} %>
                                    >
<%
                                    if (repErrors != null && repErrors.containsKey("routingNumber"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                  <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                  <td>
                                     <%String textInfoAba = Languages.getString("jsp.admin.customers.field_no_applicable.info_field_no_applicable",SessionData.getLanguage()); %>
                                     <input type="checkbox" id="optionAba" name="optionAba" value="optionAba"
                                      onClick="changeCheckAba()"
                                     <%if(Rep.getRoutingNumber().startsWith("NA")){ %>
										checked
									 <%} %>
                                       >
                                     <%=textInfoAba%>
	                              </td>
	                              <td><div id="div_info_aba" name="div_info_aba" ></div></td>
	                              <%} %>
                                </tr>
                                <tr class="main">
                                  <td>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.account_number",SessionData.getLanguage()) %>:
                                  </td>
                                  <td nowrap>
                                    <%String textErrorAccount = Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage()); %>
                                    <input type="text" id="accountNumber" name="accountNumber" value="<%= Rep.getAccountNumber() %>"
                                    size="15" maxlength="20" tabIndex="<%= intTabIndex++ %>"
                                    <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                    	onKeyUp="verifyAccount(20,'<%=textErrorAccount%>');"
                                    <%}%>
                                    <%if(Rep.getRoutingNumber().startsWith("NA")){ %>
										disabled = "false";
									<%} %>
                                    >
<%
                                    if (repErrors != null && repErrors.containsKey("accountNumber"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                  <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                  <td>
                                      <%String textInfoAccount = Languages.getString("jsp.admin.customers.field_no_applicable.info_field_no_applicable",SessionData.getLanguage()); %>
                                      <input type="checkbox" id="optionAccount" name="optionAccount" value="optionAccount"
                                        onClick="changeCheckAccount()"
                                      <%if(Rep.getRoutingNumber().startsWith("NA")){ %>
										checked
									  <%} %>
                                      ><%=textInfoAccount%>
                                      </td>
									<td><div id="div_info_account" name="div_info_account" ></div></td>
								<%} %>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        <tr>
                          <td class="formAreaTitle2" align="center">
                            <input type="hidden" name="submitted" value="y">

                            <input type="submit" name="submitbutton" tabIndex="<%= intTabIndex++ %>" value="<%= Languages.getString("jsp.admin.customers.reps_edit.submit",SessionData.getLanguage()) %>">
                            <input type="button" value="<%= Languages.getString("jsp.admin.cancel",SessionData.getLanguage()) %>" onClick="history.go(-1)" tabIndex="<%= intTabIndex++ %>">
                            <input type="hidden" id="addressValidationStatus" name="addressValidationStatus"  value="<%=Rep.getAddressValidationStatus()%>">

                          </td>
                        </tr>

                    </table>
                    </form>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>