<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.util.Hashtable,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%
	int section=14;
	int section_page=3;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:setProperty name="User" property="*"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
	Hashtable<String, String> userErrors = new Hashtable<String, String>();

	// Form has been submitted
	if(request.getParameter("submitted") != null) 
	{
		if (request.getParameter("username") != null && request.getParameter("email") != null && request.getParameter("passwordId") != null)
		{
			User.setpasswordId(request.getParameter("passwordId"));
			User.setUsername(request.getParameter("username"));
			User.setPassword(request.getParameter("password"));
            User.setEmail(request.getParameter("email"));
			if (User.validateEmail(SessionData))
			{
				User.updateEmail(SessionData, application);
			}
            else if (User.isError())
            {
              	Hashtable<String, String> tempErrors = User.getErrors();
              
              	if (tempErrors.containsKey("username"))
              	{
                	userErrors.put("username", tempErrors.get("username").toString());
              	}
              	if (tempErrors.containsKey("password"))
              	{
                	userErrors.put("password",tempErrors.get("password").toString());
              	}
              	if (tempErrors.containsKey("email"))
              	{
                	userErrors.put("email",tempErrors.get("email").toString());
              	}
            }
		}
		
		if(userErrors.size() == 0) {
	        User.setpasswordId("");
	        User.setUsername("");
	        User.setPassword("");
	        User.setEmail("");
		}
	}

	if(request.getParameter("submitted") == null || userErrors.size() > 0) 
	{
		String username = User.getUserLogonID(SessionData);
		String password = User.getUserLogonPassword(SessionData);
		String email="";
		if(userErrors.size() > 0)
			 email=request.getParameter("email");
		else
			 email=User.getUserLogonEmail(SessionData);
			
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<table width=100% border=0>
	              			<tr class="main">
	               				<td nowrap valign="top"><%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.email.instructions",SessionData.getLanguage())%></td>
	               			</tr>
	               		</table>
       					<form name="carrierUsersInfoForm" method="post" action="admin/customers/carrier_users_info_emailEdit.jsp" >
       					<table border="0" width="100%" cellpadding="0" cellspacing="0">
				     		<tr>
					        	<td>
				          			<table border=0>
										<tr class=main>
											<td><%=Languages.getString("jsp.admin.customers.carrier_users.username",SessionData.getLanguage())%> </td>
											<td>
												<input type="text" readonly="readonly" id="username" name="username" value="<%=username%>"/>
											</td>
<%
		if(userErrors.size() > 0)
		{
			if(userErrors.get("email").length() > 0) 
			{
				out.println("<td><font color=\"red\">" + userErrors.get("email").toString() + "</font></td>");
			}
		}
%>											
										</tr>
										<tr class=main>
											<td><%=Languages.getString("jsp.admin.customers.carrier_users.password",SessionData.getLanguage())%> </td>
											<td>
												<input readOnly="readOnly" type="password" id="password" name="password" value="<%=password%>"/>
											</td>
										</tr>
										
										<tr class=main>
											<td><%=Languages.getString("jsp.admin.customers.user_logins.email",SessionData.getLanguage())%> </td>
											<td>
												<input type="text" id="email" name="email" <%if(email!=null){ %>value="<%=email%>"<%}%>/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td>
							          			<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.updatebutton",SessionData.getLanguage())%>" />
							          			<input type="hidden" id="submitted" name="submitted" value="1" />
									    		<input type="hidden" id="refId" name="refId" value="<%=SessionData.getProperty("ref_id")%>">
							          			<input type="hidden" id="passwordId" name="passwordId" value="<%=request.getParameter("passwordId")%>"/>
									    		<input type="hidden" id="refType" name="refType" value="1">
											    <input type="hidden" id="status" name="status" value="1">
							          			</form>
											</td>
											<td>
												<form name="cancelForm" method="get" action="admin/customers/carrier_users.jsp" >
													<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.cancelbutton",SessionData.getLanguage())%>">
													<input type="hidden" id="search" name="search" value="y">
												</form>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
	} else {
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000" background="images/top_blue.gif">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td width="970" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.title",SessionData.getLanguage())%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<form name="backToCarrierUsers" action="admin/customers/carrier_users.jsp"> 
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center" class="main"><%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.CarrierAccountModifiedSuccessfully",SessionData.getLanguage())%>!</td>
							</tr>
							<tr>
					    		<td align="center">
					   				<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.BackToCarrierUsers",SessionData.getLanguage())%>" />
									<input type="hidden" id="search" name="search" value="y">
					   			</td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
	}
%>
<%@ include file="/includes/footer.jsp" %>