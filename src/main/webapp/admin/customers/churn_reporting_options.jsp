<%@ page import="java.util.Vector,
				 java.util.List,
				 com.debisys.users.User,
				 java.util.Iterator,
				 java.util.Hashtable,
				 com.debisys.utils.*,
				 com.debisys.utils.TimeZone,
				 com.debisys.utils.DebisysConfigListener,
				 com.debisys.properties.RepProperty, java.text.DecimalFormat" %>
<%
int section=4;
int section_page=9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%

String rep_id = request.getParameter("repId");
String cancel = request.getParameter("cancel");
String tools = "" + request.getParameter("tools");

/* Load properties */
boolean weekly = RepProperty.getProperty(Long.parseLong(rep_id), RepProperty.CHURN_REPORTING_OPTIONS_WEEKLY, "disabled").equals("enabled");
String wthreshold = RepProperty.getProperty(Long.parseLong(rep_id), RepProperty.CHURN_REPORTING_OPTIONS_WEEKLY_THRESHOLD, "50");
String wsentto = RepProperty.getProperty(Long.parseLong(rep_id), RepProperty.CHURN_REPORTING_OPTIONS_WEEKLY_SENT_TO, Rep.getRepEmailById(rep_id));
boolean monthly = RepProperty.getProperty(Long.parseLong(rep_id), RepProperty.CHURN_REPORTING_OPTIONS_MONTHLY,"disabled").equals("enabled"); 
String mthreshold = RepProperty.getProperty(Long.parseLong(rep_id), RepProperty.CHURN_REPORTING_OPTIONS_MONTHLY_THRESHOLD, "50");
String msentto = RepProperty.getProperty(Long.parseLong(rep_id), RepProperty.CHURN_REPORTING_OPTIONS_MONTHLY_SENT_TO, Rep.getRepEmailById(rep_id));
boolean merchdisabled = RepProperty.getProperty(Long.parseLong(rep_id), RepProperty.CHURN_REPORTING_OPTIONS_DISABLED_MERCHANTS, "disabled").equals("enabled");

wthreshold = new DecimalFormat("#0.00").format(Double.valueOf(wthreshold));
mthreshold = new DecimalFormat("#0.00").format(Double.valueOf(mthreshold));

String msg = null;
if(cancel != null){
	if(tools.equals("1")){
		response.sendRedirect("../tools/tools.jsp");
	}else{
		response.sendRedirect("reps_info.jsp?repId=" + rep_id);
	}
}else{
	String save = request.getParameter("save");
	if(save != null){
		weekly = ("" + request.getParameter("weekly")).equals("1");
		wthreshold = "" + request.getParameter("weeklythreshold");
		wsentto = "" + request.getParameter("weeklysentto");
		monthly = ("" + request.getParameter("monthly")).equals("1");
		mthreshold = "" + request.getParameter("monthlythreshold");
		msentto = ("" + request.getParameter("monthlysentto")).trim();
		merchdisabled = ("" + request.getParameter("merchdisabled")).equals("Y");
		if(rep_id != null && rep_id.length()>0){
			RepProperty.saveOptionsForChurnReport(Long.parseLong(rep_id), weekly, Double.parseDouble(wthreshold), wsentto.trim(), monthly, Double.parseDouble(mthreshold), msentto, merchdisabled);
			msg = Languages.getString("jsp.admin.reports.churn.saveok",SessionData.getLanguage());
		}else{
			msg = Languages.getString("jsp.admin.reports.churn.invalidrepid",SessionData.getLanguage());
		}
	}
}
%>
<%@ include file="/includes/header.jsp" %>
<style>
	.reportForm table{border:0;border-collapse:collapse;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
	.reportForm td{padding:4px; height:28;vertical-align:text-top;}
</style>
<%
if(msg != null){
	%><div style="color:red;"><big><%=msg %></big></div><br><%
}
%>
<script>
	function trim (str){
		return str.replace(/^\s+/g,'').replace(/\s+$/g,'')
	}
	function isAnEmail(emailAddress){
		var pattern = /^([\w\-]+\.?)+[\w\-]+@([\w\-]+\.?)+[\w\-]+\.\w{2,4}$/i;
		return pattern.test(emailAddress);
	}
	function validateEmailList(str, label, required){
		str = trim(str);
		if(str.length > 0){
			var email = str.split(',');
			for (var i = 0; i < email.length; i++) {
				if (!isAnEmail(trim(email[i]))) {
					alert(label)
					return false;
				}
			}
		}else{
			if(required){
				alert(label);
				return false;
			}
		}
		return true;
	}
	function validateForm(){
		var ok = validateEmailList(document.getElementById("weeklysentto").value,	
				'<%=Languages.getString("jsp.admin.reports.churn.errorsendtow",SessionData.getLanguage())%>',
				document.getElementById("wenabled").checked); 
		ok = ok && validateEmailList(document.getElementById("monthlysentto").value, 
				'<%=Languages.getString("jsp.admin.reports.churn.errorsendtom",SessionData.getLanguage())%>',
				document.getElementById("menabled").checked);
		ok = ok && validateThreshold(document.getElementById("wthreshold").value, 
				'<%=Languages.getString("jsp.admin.reports.churn.errorwthreshold",SessionData.getLanguage())%>');
		ok = ok && validateThreshold(document.getElementById("mthreshold").value, 
				'<%=Languages.getString("jsp.admin.reports.churn.errormthreshold",SessionData.getLanguage())%>');
		return ok;
	}
	function validateThreshold(val, msg){
		val = trim(val);
		if(!validThreshold(val)){
			alert(msg);
			return false;
		}
		return true;
	}
	function validThreshold(num){
		var pattern = /^\d{1,3}(\.\d{1,2})?$/i;
		if(pattern.test(num)){
			num = num * 1;
			return (num >= 0.0 && num <= 100.0)
		}
		return false;
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.churn.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3" class="formArea2">
			<form class="reportForm" name="mainform" method="get" action="admin/customers/churn_reporting_options.jsp" onsubmit="return validateForm();">
				<input type="hidden" name="repId" value="<%=rep_id %>">
				<table>
					<tr>
						<td class="main"><%=Languages.getString("jsp.admin.reports.churn.weekly",SessionData.getLanguage())%>:</td>
						<td>
							<input name="weekly" type="radio" value="0" <%=weekly?"":"checked"%>><%=Languages.getString("jsp.admin.reports.churn.disabled",SessionData.getLanguage())%> 
							<input id="wenabled" name="weekly" type="radio" value="1" <%=weekly?"checked":""%>><%=Languages.getString("jsp.admin.reports.churn.enabled",SessionData.getLanguage())%>
						</td>
					</tr>
					<tr>
						<td class="main"><%=Languages.getString("jsp.admin.reports.churn.threshold",SessionData.getLanguage())%></td>
						<td><input id="wthreshold" name="weeklythreshold" type="text" value="<%=wthreshold %>" maxlength="5" size="5" style="text-align: right;"></td>
					</tr>
					<tr>
						<td><%=Languages.getString("jsp.admin.reports.churn.sentto",SessionData.getLanguage())%></td>
						<td><input id="weeklysentto" name="weeklysentto" type="text" size="80" value="<%=wsentto %>"></td>
					</tr>
					<tr>
						<td class="main"><%=Languages.getString("jsp.admin.reports.churn.monthly",SessionData.getLanguage())%>:</td>
						<td>
							<input name="monthly" type="radio" value="0" <%=monthly?"":"checked"%>><%=Languages.getString("jsp.admin.reports.churn.disabled",SessionData.getLanguage())%> 
							<input id="menabled" name="monthly" type="radio" value="1" <%=monthly?"checked":""%>><%=Languages.getString("jsp.admin.reports.churn.enabled",SessionData.getLanguage())%>
						</td>
					</tr>
					<tr>
						<td class="main"><%=Languages.getString("jsp.admin.reports.churn.threshold",SessionData.getLanguage())%></td>
						<td><input id="mthreshold" name="monthlythreshold" type="text" value="<%=mthreshold %>" maxlength="5" size="5" style="text-align: right;"></td>
					</tr>
					<tr>
						<td class="main"><%=Languages.getString("jsp.admin.reports.churn.sentto",SessionData.getLanguage())%></td>
						<td><input id="monthlysentto" name="monthlysentto" type="text" size="80" value="<%=msentto %>"></td>
					</tr>

					<tr>
						<td class="main"><%=Languages.getString("jsp.admin.reports.churn.merchdisabled",SessionData.getLanguage())%></td>
						<td><input name="merchdisabled" type="checkbox" value="Y" <%=merchdisabled?"checked":"" %>></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<input class="main" type="submit" name="save" value="<%=Languages.getString("jsp.admin.reports.churn.saveoptions",SessionData.getLanguage())%>">
							<input class="main" type="submit" name="cancel" value="<%=Languages.getString("jsp.admin.reports.churn.cancel",SessionData.getLanguage())%>">
							<input class="main" type="hidden" name="tools" value="<%=tools%>">
						</td>
					</tr>
					<tr>
						<td colspan="2" class="main">
							<small><%=Languages.getString("jsp.admin.reports.churn.note1",SessionData.getLanguage())%></small><br>
							<small><%=Languages.getString("jsp.admin.reports.churn.note3",SessionData.getLanguage())%></small>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>