<%@ page import="com.debisys.languages.Languages"%> 
<%@ page import="com.debisys.utils.DebisysConfigListener,
                 com.debisys.languages.Languages"%>
<%
int section=2;
int section_page=3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="page"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
String action = request.getParameter("action");
String value = request.getParameter("value");
String reason = request.getParameter("reason");
String validateTable = request.getParameter("validateTable");
String result;
String deploymentType = DebisysConfigListener.getDeploymentType(application);
if(Merchant.getMerchantId() != null && !Merchant.getMerchantId().equals("")){
	if(action.equals("cancel")){
		Merchant.updateMerchantCancelledStatus(SessionData, application,  Merchant.getMerchantId(), value.equals("1"), reason);
		if( value.equals("1")){
			%>CANCELLED<%
		}else{
			%>RESTORED<%
		}
	}else if(action.equals("disable")){
		if(validateTable !=null && validateTable.equals("1") && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) ){
			int rta = Merchant.validateMerchantInDisabledMerchants(SessionData, application, Merchant.getMerchantId());
			if(rta>=1){
				%><%=Merchant.getMerchantId()%><%
			}
			else if(rta == 0 ){
				%>NO_IN_TABLE<%
			}
		}
		else{
			Merchant.updateMerchantStatus(SessionData, application, Merchant.getMerchantId(), value.equals("1"), reason);
			if(value.equals("1")){
				%>DISABLED<%
			}else{
				%>ENABLED<%
			}
		}
		
	}
}else{
	%>ERROR<%
}
%>