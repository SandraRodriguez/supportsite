<%@ page import="com.debisys.terminalTracking.*,
                 com.debisys.languages.*"%>
<%@page import="com.debisys.utils.StringUtil"%>

<jsp:useBean id="CPhysicalTerminal" class="com.debisys.terminalTracking.CPhysicalTerminal" scope="session"/>
<jsp:setProperty name="CPhysicalTerminal" property="col"/>
<jsp:setProperty name="CPhysicalTerminal" property="sort"/>
<%
int section = 2;
int section_page = 11;

String serialNumber = "";
String lotNumber = "";
String applicationType = "";
String brandId = "";
String modelId = "";
String statusId = "";
String startDate = "";
String endDate = "";
boolean submitted = false;

int intPage = 1;
int intPageCount = 1;
int intPageSize = 100;

if (request.getParameter("submittedITS") != null)
{
  try
    {
      if (request.getParameter("submittedITS").equals("y"))
    {
        submitted = true;
        serialNumber = (request.getParameter("serialNumberH") != null)?request.getParameter("serialNumberH"):"";
        lotNumber = (request.getParameter("lotNumberH") != null)?request.getParameter("lotNumberH"):"";
        applicationType = (request.getParameter("applicationTypeH") != null)?request.getParameter("applicationTypeH"):"";
        brandId = (request.getParameter("brandIdH") != null)?request.getParameter("brandIdH"):"";
        modelId = (request.getParameter("modelIdH") != null)?request.getParameter("modelIdH"):"";
        statusId = (request.getParameter("statusIdH") != null)?request.getParameter("statusIdH"):"";
        startDate = (request.getParameter("startDateH") != null)?request.getParameter("startDateH"):"";
        endDate = (request.getParameter("endDateH") != null)?request.getParameter("endDateH"):"";
  CPhysicalTerminal.setSerialNumberH("");
  CPhysicalTerminal.setApplicationTypeH("");
  CPhysicalTerminal.setLotNumberH("");
  CPhysicalTerminal.setBrandIdH("");
  CPhysicalTerminal.setStatusIdH("");
  CPhysicalTerminal.setModelIdH("");
  CPhysicalTerminal.setStartDateH("");
  CPhysicalTerminal.setEndDateH("");
    }
    }
  catch (Exception e){}
}
else
{
  CPhysicalTerminal.setSerialNumberH("");
  CPhysicalTerminal.setApplicationTypeH("");
  CPhysicalTerminal.setLotNumberH("");
  CPhysicalTerminal.setBrandIdH("");
  CPhysicalTerminal.setStatusIdH("");
  CPhysicalTerminal.setModelIdH("");
  CPhysicalTerminal.setStartDateH("");
  CPhysicalTerminal.setEndDateH("");
}
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%
  response.setCharacterEncoding("utf-8");
%>
<html>
<head>
  <link href="../../default.css" type="text/css" rel="stylesheet">
  <script type="text/javascript" src="/support/includes/jquery.js"></script>
  <script type="text/javascript" src="/support/includes/ajax.js"></script>
  <script type="text/javascript" SRC="/support/includes/funcionesValidacion.js"></script>
  <script type="text/javascript">

function saveCurrentFieldValues()
{
  document.getElementById('serialNumberH').value = document.getElementById('serial').value
  document.getElementById('lotNumberH').value = document.getElementById('txtLotNumber').value
  document.getElementById('applicationTypeH').value = document.getElementById('ddlApplicationType').value
  document.getElementById('brandIdH').value = document.getElementById('ddlBrand').value
  document.getElementById('modelIdH').value = document.getElementById('ddlModel').value
  document.getElementById('statusIdH').value = document.getElementById('hStatus').value
  document.getElementById('startDateH').value = document.getElementById('txtStartDate').value
  document.getElementById('endDateH').value = document.getElementById('txtEndDate').value 
  //document.mainform.submit();
}


function enviarDatos(btn)
   {   
          saveCurrentFieldValues();
            if(validacion(btn.form)){
                btn.form.action="/support/admin/customers/physterm_selector.jsp";
            }
            else{
                return false;
            }
       btn.form.submit();
   }  
   
function validacion(form)
   {
     var alpha = 0;
     var errors = "";
     
     if ( document.getElementById('txtStartDate').value.length != 0 || document.getElementById('txtEndDate').value.length != 0 )
     {
      if ( document.getElementById('txtStartDate').value.length == 0 )
        {
         errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminals.emptystartdate",SessionData.getLanguage())%>\n';
         alpha++;
        }
		     if ( document.getElementById('txtEndDate').value.length == 0 )
		     {
		        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminals.emptyenddate",SessionData.getLanguage())%>\n';
		        alpha++;
		     }        
     }   
            
          
    if (alpha != 0) {
        alert(errors);
        return false;
    }
    else {      
        return true;
    }      
   }

    function SetSize()
    {
      var nWidth = document.getElementById("oBody").scrollWidth;
      if ( nWidth > screen.width )
      {
        nWidth = screen.width * 0.9;
      }
      var nHeight = document.getElementById("oBody").scrollHeight;
      if ( nHeight > screen.height )
      {
        nHeight = screen.height * 0.9;
      }
      window.resizeTo(nWidth, nHeight);
      window.moveTo((screen.width - nWidth)/2, (screen.height - nHeight)/2);
    }
  </script>
</head>
<body id="oBody" bgcolor="#ffffff" _onload="SetSize()">
<table width="100%"><tr><td align=center>
<form name="mainform" method="post" action="/support/admin/customers/physterm_selector.jsp">
<input type="hidden" id="submittedITS" name="submittedITS" value="y"> 
<input type="hidden" id="serialNumberH" name="serialNumberH" value="">
<input type="hidden" id="lotNumberH" name="lotNumberH" value="">
<input type="hidden" id="applicationTypeH" name="applicationTypeH" value="">
<input type="hidden" id="brandIdH" name="brandIdH" value="">
<input type="hidden" id="modelIdH" name="modelIdH" value="">
<input type="hidden" id="statusIdH" name="statusIdH" value="">
<input type="hidden" id="startDateH" name="startDateH" value="">
<input type="hidden" id="endDateH" name="endDateH" value="">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td background="/support/images/top_blue.gif" width="1%" align="left"><img src="/support/images/top_left_blue.gif" width="18" height="20"></td>
    <td background="/support/images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.title",SessionData.getLanguage())%></b></td>
    <td background="/support/images/top_blue.gif" width="1%" align="right"><img src="/support/images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
    <td colspan=4>
      <table width="100%" border="0" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0">
        <tr>
          <td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
          <td align="center" valign="top" bgcolor="#FFFFFF">
            <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
              <tr><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                  <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.serial",SessionData.getLanguage())%>:</td>
                  <%
                    if(SessionData.getUser().isQcommBusiness() == true)
                    {
                      out.print("<td><input type=text class=\"plain\" id=\"serial\" name=\"serial\" maxlength=\"26\" onkeypress=\"return validaNumYTexto(event);\" value=\"" + StringUtil.toString(CPhysicalTerminal.getSerialNumberH()) + "\"></td>");
                    }
                    else
                    {
                      out.print("<td><input type=text class=\"plain\" id=\"serial\" name=\"serial\" maxlength=\"25\" onkeypress=\"return validaNumYTexto(event);\" value=\"" + StringUtil.toString(CPhysicalTerminal.getSerialNumberH()) + "\"></td>");
                    }
                  %>
                  
                  <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.lotNumber",SessionData.getLanguage())%>:</td>
                  <td><input type=text class="plain" name="lotNumber" id="txtLotNumber" onkeypress="return validaNumYTexto(event);" value="<%=StringUtil.toString(CPhysicalTerminal.getLotNumberH())%>"></td>                                                                
              </tr> 
              <tr>
                                                            <td>&nbsp;</td>
                  <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.applicationType",SessionData.getLanguage())%>:</td>
                  <td>
                    <select name="applicationType" id="ddlApplicationType">
                                        <%
                                        
                                          CApplicationType[] appTypeList = CApplicationType.getAllApplicationTypes();
                                          boolean applicationTypeSelected = false;
                                          for(CApplicationType appType:appTypeList)
                                          {
                                            if((CPhysicalTerminal.getApplicationTypeH() != "") && (CPhysicalTerminal.getApplicationTypeH().equals(String.valueOf(appType.getApplicationTypeId()))))
                                            {
                                              out.println("<option value=" + appType.getApplicationTypeId() +" selected>" +appType.getApplicationTypeDescription() + "</option>");
                                              applicationTypeSelected = true;
                                            }
                                            else
                                            {
                                              out.println("<option value=" + appType.getApplicationTypeId() +">" +appType.getApplicationTypeDescription() + "</option>");
                                            }
                                          }
                                          if(!applicationTypeSelected)
                                          {
                                            out.println("<option value=\"-1\" selected>" + Languages.getString("jsp.admin.transactions.inventoryTerminals.optionAll",SessionData.getLanguage()) + "</option>");
                                          }
                                          
                                        %>
                                    </select>
                               </td>
              </tr>  
              <tr>
                                                            <td>&nbsp;</td>
                  <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.brand",SessionData.getLanguage())%>:</td>
                  <td>
                    <select name="brand" id="ddlBrand" onchange="onBrandChange()">
                                        <%
                                          CTerminalBrand[] brandList = CTerminalBrand.getBrandList();
                                          CTerminalBrand selectedBrand = null;
                                          out.println("<option value=\"-1\" selected>" + Languages.getString("jsp.admin.transactions.inventoryTerminals.optionAll",SessionData.getLanguage()) + "</option>");
                                          for(CTerminalBrand currentBrand:brandList)
                                          {
                                            if((CPhysicalTerminal.getBrandIdH() != "") && (CPhysicalTerminal.getBrandIdH().equals(String.valueOf(currentBrand.getBrandId()))))
                                            {
                                              out.println("<option value=" + currentBrand.getBrandId() +" selected>" + currentBrand.getBrandName() + "</option>");
                                              selectedBrand = currentBrand;
                                              selectedBrand.loadBrandModels();
                                            }
                                            else
                                            {
                                              out.println("<option value=" + currentBrand.getBrandId() +">" + currentBrand.getBrandName() + "</option>");
                                            }
                                          }
                                        %>
                                       
                                    </select>
                                                            </td>
                                                            
                  <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.model",SessionData.getLanguage())%>:</td>
                  <td>
                    <select name="model" id="ddlModel">
                    
                                    <option value="-1" selected><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.optionAll",SessionData.getLanguage())%></option>
                                    <%
                                      if(selectedBrand != null)
                                      {
                                        for(CTerminalModel currentModel: selectedBrand.getModelList())
                                        {
                                          out.println("<option value=" + currentModel.getModelId() +" "+(CPhysicalTerminal.getModelIdH().equals(currentModel.getModelId())? "selected": "")+">" + currentModel.getModelName() + "</option>");
                                        }
                                      }
                                    %>
                                    </select>
                                </td>
              </tr>  
              <tr>
                <td><input type="hidden" id="hStatus" name="status" value="<%=CTerminalStatus.getTerminalStatusByCode(CTerminalStatus.READY_CODE).getTerminalSatusId()%>"></td>
              </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.admittanceDate",SessionData.getLanguage())%>:</strong></td>
                                                        </tr>
              <tr>
                                                            <td>&nbsp;</td>
                  <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.startDate",SessionData.getLanguage())%>:</td>
                  
                  <td><input class="plain" id="txtStartDate" name="startDate" value="<%=StringUtil.toString(CPhysicalTerminal.getStartDateH())%>" readonly=true size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="/support/admin/calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a></td>
              </tr>
              <tr>
                                                            <td></td>                                                            
                  <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.endDate",SessionData.getLanguage())%>:</td>
                  <td> <input class="plain" id="txtEndDate" name="endDate" value="<%=StringUtil.toString(CPhysicalTerminal.getEndDateH())%>" readonly=true size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="/support/admin/calendar/calbtn.gif" width="34" height="22" border="0" alt=""></a></td>
              </tr>

              <tr><td><br></td></tr>
              <tr align="center">
                                                                <td>&nbsp;</td>
                                                                <td align=center colspan="4">
                                                                  <input id="btnSearch" type="button" value="<%=Languages.getString("jsp.admin.transactions.inventoryTerminals.search",SessionData.getLanguage())%>" onclick="enviarDatos(this)">
                                                                </td>
              </tr>
              <tr><td><br></td></tr>
            </table>
          </td>
          <td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
        </tr>
        <tr>
          <td height="1" bgcolor="#003082" colspan="3"><img src="/support/images/trans.gif" height="1"></td>
              </tr>
      </table>
    </td>
  </tr>
  <%
  String paramSerialNumber = (serialNumber.equals(""))? null: serialNumber;
  CPhysicalTerminal.setSerialNumberH(paramSerialNumber);
  long paramApplicationType = (applicationType.equals(""))? -1: Long.valueOf(applicationType);
  CPhysicalTerminal.setApplicationTypeH(Long.toString(paramApplicationType));
  String paramLotNumber = (lotNumber.equals(""))? null: lotNumber;
  CPhysicalTerminal.setLotNumberH(paramLotNumber);
  long paramBrandId = (brandId.equals(""))? -1: Long.valueOf(brandId);
  CPhysicalTerminal.setBrandIdH(Long.toString(paramBrandId));
  long paramStatusId = (statusId.equals(""))? CTerminalStatus.getTerminalStatusByCode(CTerminalStatus.READY_CODE).getTerminalSatusId(): Long.valueOf(statusId);
  CPhysicalTerminal.setStatusIdH(Long.toString(paramStatusId));
  long paramModelId = (modelId.equals(""))? -1: Long.valueOf(modelId);
  CPhysicalTerminal.setModelIdH(Long.toString(paramModelId));
  String paramStartDate = (startDate.equals(""))? null: startDate;
  CPhysicalTerminal.setStartDateH(paramStartDate);
  String paramEndDate = (endDate.equals(""))? null: endDate;
  CPhysicalTerminal.setEndDateH(paramEndDate);
  
  if (request.getParameter("page") != null) {
    try {
      intPage = Integer.parseInt(request.getParameter("page"));
    } catch(NumberFormatException ex) {
      intPage = 1;
    }
  }

  if (intPage < 1) {
    intPage = 1;
  }

  CPhysicalTerminal[] terminalList = 
    CPhysicalTerminal.getTerminalList(paramSerialNumber,
                      paramApplicationType, 
                      paramLotNumber, 
                      paramBrandId,
                      paramModelId, 
                      paramStatusId, 
                      paramStartDate,
                      paramEndDate, 
                      intPageSize,
                      intPage);
  
  int intRecordCount = 0;
  if ((terminalList != null) && (terminalList.length > 0)) {
    intRecordCount = Integer.parseInt("" + terminalList[0].getRecordCount());
  }
  
  if (intRecordCount > 0) {
    intPageCount = (intRecordCount / intPageSize);
    if ((intPageCount * intPageSize) < intRecordCount) {
      intPageCount++;
    }
  }
  
  if ((terminalList != null) && (terminalList.length > 0)) {
  %>
  <tr>
    <td colspan="3" align="right">
<%
if (intPage > 1) {
  out.println("<a href=\"/support/admin/customers/physterm_selector.jsp?submittedITS=y&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
  out.println("<a href=\"/support/admin/customers/physterm_selector.jsp?submittedITS=y&page=" + (intPage - 1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
}

int intLowerLimit = intPage - 12;
int intUpperLimit = intPage + 12;

if (intLowerLimit < 1) {
  intLowerLimit=1;
  intUpperLimit = 25;
}

for (int j = intLowerLimit; j <= intUpperLimit && j <= intPageCount; j++) {
  if (j == intPage) {
    out.println("<font color=#ff0000>" + j + "</font>&nbsp;");
  } else {
    out.println("<a href=\"/support/admin/customers/physterm_selector.jsp?submittedITS=y&page=" + j + "\">" + j + "</a>&nbsp;");
  }
}

if (intPage <= (intPageCount - 1)) {
  out.println("<a href=\"/support/admin/customers/physterm_selector.jsp?submittedITS=y&page=" + (intPage + 1) + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
  out.println("<a href=\"/support/admin/customers/physterm_selector.jsp?submittedITS=y&page=" + (intPageCount) + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
}

%>
</td>
  </tr>
  <tr>
    <td colspan="3">
      <span class=main><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.selectserialtitle",SessionData.getLanguage())%></span>
      <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
        <tr>
          <td class="main"><br>
          
            <table width="100%" cellspacing="1" cellpadding="1" border="0"id="t1">
                    <thead>
                      <tr>
                        <!--  class="SectionTopBorder" ...  class="sort-table" ... NOt using the JS sorting :( -->
                <%
                 String strSortURL = "/support/admin/customers/physterm_selector.jsp?submittedITS=y";
                 %>
                        <td class=rowhead2>#</td>
                      <td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.serial",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=1&sort=1"><img src="/support/images/down.gif" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=1&sort=2"><img src="/support/images/up.gif" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.brand",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=2&sort=1"><img src="/support/images/down.gif" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=2&sort=2"><img src="/support/images/up.gif" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.model",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=3&sort=1"><img src="/support/images/down.gif" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=3&sort=2"><img src="/support/images/up.gif" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.applicationType",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=4&sort=1"><img src="/support/images/down.gif" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=4&sort=2"><img src="/support/images/up.gif" height=11 width=11 border=0></a></td>
                      <td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.primaryCommType",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=5&sort=1"><img src="/support/images/down.gif" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=5&sort=2"><img src="/support/images/up.gif" height=11 width=11 border=0></a></td>
                      <td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.admittanceDate",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=6&sort=1"><img src="/support/images/down.gif" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=6&sort=2"><img src="/support/images/up.gif" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.lotNumber",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=7&sort=1"><img src="/support/images/down.gif" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=7&sort=2"><img src="/support/images/up.gif" height=11 width=11 border=0></a></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventoryTerminals.status",SessionData.getLanguage()).toUpperCase()%></td>
                    </tr>
                  </thead>
                  <%
                    
                    int rowBackGround = 1;
                    int intCounter = 1;
                    for(CPhysicalTerminal currentTerminal: terminalList)
                    {
                                  currentTerminal.loadTerminalParts();
                                  String sValue = currentTerminal.getTerminalId() + "#" + currentTerminal.getSerialNumber() + "#";
													        for ( CTerminalPart currentPart:currentTerminal.getAssignedParts().values() )
													        {
													          if (currentPart.getPartType().getPartTypeCode().equals(CTerminalPartType.SIMCARD_CODE))
													          {
													            CSimCard simCardObj = (CSimCard)currentPart;
													            sValue += simCardObj.getPhoneNumber() + "#";
													            sValue += simCardObj.getMobileSuscriberId();
													            break;
													          }
													        }
                                  out.println("<tr class=row" + rowBackGround +">" +
                                      "<td>" + (intCounter++) + "</td>" +
                                      "<td><a href=\"javascript:void(0)\" onclick=\"self.opener.parent.SetPhysicalTerminal('" + sValue + "#');self.close();\">" + currentTerminal.getSerialNumber() + "</td>" +
                                      "<td>" + currentTerminal.getBrand().getBrandName() + "</td>" +
                                      "<td>" + currentTerminal.getModel().getModelName() + "</a></td>" +
                                      "<td>" + currentTerminal.getApplicationType().getApplicationTypeDescription() + "</td>" +
                                      "<td>" + currentTerminal.getPrimaryCommType().getCommunicationTypeDescription() + "</td>" +
                                      "<td>" + currentTerminal.getInventoryAdmitanceDate() + "</td>" +
                                      "<td>" + currentTerminal.getLotNumber() + "</td>" +
                                      "<td>" + currentTerminal.getCurrentStatus().getTerminalStatusDescription() + "</td>" +
                                        "</tr>");       
                                  rowBackGround = (rowBackGround == 1)? 2: 1; 
                    }                   
                  %>
                  </table>  
          <br>
          <br><!-- 
            <SCRIPT type="text/javascript">
                var stT1 = new SortROC(document.getElementById("t1"),
                ["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
            </SCRIPT>  -->          
          </td>
        </tr>
      </table>
    </td>
  </tr> 
  <%
  } // END if((partList != null) && (partList.length > 0))
  else if(submitted)
  {
    out.println("<tr><td>");
      out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
      out.println("</td></tr>");
  }
  %>    
</table>
</form>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="/support/admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</td></tr></table>
</body>
</html>
