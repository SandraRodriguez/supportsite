<%@page import="com.debisys.bizUtil.City"%>
<%@page import="com.debisys.bizUtil.District"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page 
    import="com.debisys.customers.*,
                 com.debisys.users.User,
                 java.net.URLEncoder,
                 com.debisys.utils.*,
                 java.util.*,
                 com.debisys.terminals.Terminal,
                 javax.mail.Session,
                 javax.mail.internet.MimeMessage,
                 javax.mail.Message,
                 javax.mail.internet.InternetAddress,
                 javax.mail.Transport,
                 javax.mail.MessagingException,
                 com.debisys.customers.CreditTypes,
                 com.debisys.utils.TimeZone,
                 com.debisys.tools.s2k,
                 java.text.SimpleDateFormat,
                com.debisys.properties.Properties,
                com.debisys.reports.PropertiesByFeature" pageEncoding="ISO-8859-1"%>

<%
int section=2;
int section_page=9;
boolean isIsoPrepaid = false;
String instance = DebisysConfigListener.getInstance(application);
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="ValidateEmail" class="com.debisys.utils.ValidateEmail" scope="page"/>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:setProperty name="Terminal" property="*"/>
<jsp:setProperty name="Merchant" property="*"/>
<jsp:setProperty name="RatePlan" property="*"/>
<jsp:setProperty name="User" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%!String disabledText = "";
  String styleCreditLimitBox = "style=\"display:block;\"";
  String screditLimitInputValue = "0";%>
<%
Rep objRep = new Rep();
Rep dataRepresentant = null;
if (objRep.getRepresentantByRepId(SessionData.getProperty("ref_id")) != null) {
    dataRepresentant = new Rep().getRepresentantByRepId(SessionData.getProperty("ref_id"));
}
boolean isDomestic = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
/******************************************************************************************/
/******************************************************************************************/
/*GEOLOCATION FEATURE*/
boolean hasPermissionGeolocation = SessionData.checkPermission(DebisysConstants.PERM_GEOLOCATION_FEAUTURE);
int graceDaysLeft = 0, graceDays = 0;
String installationDate;

installationDate = Properties.getPropertyValue(instance, "global","address.installationDate") + "";
try{
  graceDays = Integer.parseInt(Properties.getPropertyValue(instance, "global","address.daysOfGrace"));
}catch(Exception ex){
  graceDays = 0;
}
if(installationDate.equals("")){
  graceDaysLeft = 0;
}else{
  SimpleDateFormat sdf= new SimpleDateFormat("yyyy/MM/dd");
  java.util.Date d = null;
  try{
    d = sdf.parse(installationDate);
    Date today = new Date();
    int diff = (int)((long)(today.getTime() - d.getTime())/(1000*60*60*24));
    graceDaysLeft = graceDays - diff;
  }catch(Exception ex){
    //TODO: installationDate no se ha establecido en la tabla de properties
  }
}
/*INIT Internationalitation*/
String labelValidatingAddressInformation = Languages.getString("jsp.admin.geolocation.validatingAddress",SessionData.getLanguage());
String labelInvalidInput = Languages.getString("jsp.admin.geolocation.invalidInput",SessionData.getLanguage());
String labelKeyBad = Languages.getString("jsp.admin.geolocation.invalidKey",SessionData.getLanguage());
String labelAddressNotFound = Languages.getString("jsp.admin.geolocation.invalidInputBad",SessionData.getLanguage());
String labelInputWarnning1 = Languages.getString("jsp.admin.geolocation.warnning1",SessionData.getLanguage());
String labelInputWarnning2 = Languages.getString("jsp.admin.geolocation.warnning2",SessionData.getLanguage());
String labelInputWarnning3 = Languages.getString("jsp.admin.geolocation.warnning3",SessionData.getLanguage());
String labelInputWarnning4 = Languages.getString("jsp.admin.geolocation.warnning4",SessionData.getLanguage());
String labelInputWarnning5 = Languages.getString("jsp.admin.geolocation.warnning5",SessionData.getLanguage());
String labelCurrentAddress = Languages.getString("jsp.admin.geolocation.addressIs",SessionData.getLanguage());
String labelSuggestion1 = Languages.getString("jsp.admin.geolocation.suggestions1",SessionData.getLanguage());
String labelSuggestion2 = Languages.getString("jsp.admin.geolocation.suggestions2",SessionData.getLanguage());
String labelApply = Languages.getString("jsp.admin.geolocation.apply",SessionData.getLanguage());
String labelCancel = Languages.getString("jsp.admin.geolocation.cancel",SessionData.getLanguage());
String labelNotConnect = Languages.getString("jsp.admin.geolocation.notConnect",SessionData.getLanguage());
/*END Internationalitation*/
/*GEOLOCATION FEATURE*/

HashMap propertiesValues = new HashMap();
String latitudeValue       = "latitude";    
String longitudeValue      = "longitude";
String zoomValue           = "zoom";
/******************************************************************************************/
/******************************************************************************************/

int intTabIndex = 1;
String strMessage = request.getParameter("message");
if (strMessage == null)
{
  strMessage="";
}
Hashtable merchantErrors = null;
Hashtable userErrors = null;
Hashtable hashRepRates = new Hashtable();
boolean merchantAdded = false;
String allowExternalRepCheck = "";
String allowExternalOutsideHierarchyCheck="";

boolean bExternalRep=com.debisys.users.User.isExternalRepAllowedEnabled(SessionData);
boolean bMerchantAssignment=SessionData.checkPermission(DebisysConstants.PERM_CAN_SET_MERCHANT_ASSIGNMENT);
boolean addJavaScriptCode = true;

if ( request.getParameter("addMerchant") != null && request.getParameter("addMerchant").length() > 0)
{
  session.removeAttribute("merchantInvoicePaymentsArray");
}


if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").length() > 0) )
{
  if ( (request.getParameter("mxContactAction").equals("ADD")) && (session.getAttribute("vecContacts") != null) )
  {
    Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
  }
  else if ( request.getParameter("mxContactAction").equals("SAVE") )
  {
    Vector vTemp = new Vector();
    vTemp.add(request.getParameter("contactTypeId"));
    vTemp.add(request.getParameter("contactName"));
    vTemp.add(request.getParameter("contactPhone"));
    vTemp.add(request.getParameter("contactFax"));
    vTemp.add(request.getParameter("contactEmail"));
    vTemp.add(request.getParameter("contactCellPhone"));
    vTemp.add(request.getParameter("contactDepartment"));
    if ( session.getAttribute("vecContacts") != null )
    {
        Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
    }
    if ( (request.getParameter("mxContactIndex") == null) || request.getParameter("mxContactIndex").equals("") )
    {
        Merchant.getVecContacts().add(vTemp);
    }
    else
    {
        Merchant.getVecContacts().setElementAt(vTemp, Integer.parseInt(request.getParameter("mxContactIndex")));
    }
    session.setAttribute("vecContacts", Merchant.getVecContacts());
  }
  else if ( request.getParameter("mxContactAction").equals("EDIT") )
  {
    Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
    Vector vec = (Vector)Merchant.getVecContacts().elementAt(Integer.parseInt(request.getParameter("mxContactIndex")));
    Merchant.setContactTypeId(Integer.parseInt(vec.get(0).toString()));
    Merchant.setContactName(vec.get(1).toString());
    Merchant.setContactPhone(vec.get(2).toString());
    Merchant.setContactFax(vec.get(3).toString());
    Merchant.setContactEmail(vec.get(4).toString());
    Merchant.setContactCellPhone(vec.get(5).toString());
    Merchant.setContactDepartment(vec.get(6).toString());
  }
  else if ( request.getParameter("mxContactAction").equals("DELETE") )
  {
    Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
    Merchant.getVecContacts().removeElementAt(Integer.parseInt(request.getParameter("mxContactIndex")));
    session.setAttribute("vecContacts", Merchant.getVecContacts());
  }
  else if ( (request.getParameter("mxContactAction").equals("CANCEL")) && (session.getAttribute("vecContacts") != null) )
  {
    Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
  }
  else if ( request.getParameter("mxContactAction").equals("--") )
  {
    Merchant.setVecContacts((Vector)session.getAttribute("vecContacts"));
  } 
  /***********************************************************/
  /***********************************************************/
  // PUT HERE VALIDATION TO PaymentTypeForInvoiceGeneration AND AccountNumberForInvoiceGeneration
  String paymentTypeForInvoiceGenerationVal = request.getParameter("paymentTypeForInvoiceGeneration");
  if ( paymentTypeForInvoiceGenerationVal != null ) {
    //Merchant.setPaymentTypeForInvoiceGeneration(paymentTypeForInvoiceGenerationVal);
  }
  String accountNumberForInvoiceGenerationVal = request.getParameter("accountNumberForInvoiceGeneration");
  if ( accountNumberForInvoiceGenerationVal != null ){
    //Merchant.setAccountNumberForInvoiceGeneration(accountNumberForInvoiceGenerationVal);
  }
  /***********************************************************/
  /***********************************************************/
}
else if (request.getParameter("submitted") != null)
{
  try
  {
    if (request.getParameter("submitted").equals("y"))
    {

        if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
        {//If the code reachs here is because there is at least one contact and the session var exists
          Vector vTmp = (Vector)session.getAttribute("vecContacts");
          Merchant.setVecContacts(vTmp);
          vTmp = (Vector)vTmp.get(0);
          Merchant.setContactName(vTmp.get(1).toString());
          Merchant.setContactPhone(vTmp.get(2).toString());
          Merchant.setContactFax(vTmp.get(3).toString());
          Merchant.setContactEmail(vTmp.get(4).toString());
          Merchant.setContactCellPhone(vTmp.get(5).toString());
          Merchant.setContactDepartment(vTmp.get(6).toString());
        }
        boolean bUser = true;
        if (!isDomestic){
            bUser = User.validateAddUpdateUser(SessionData, getServletContext());//A variable is used here to show all the errors at once because if merchant fails, the validation of user won't run
        }
        if ( Merchant.validate(SessionData, hashRepRates, application) && bUser )
        {
            if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_MAXIMUM_SALES_LIMIT) && Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_CREDIT))
            {
              Merchant.setMaximumSalesLimit(request);
            }
            Merchant.setDEntityAccountType(request.getParameter("disabledentityAccountType"));
            allowExternalRepCheck = request.getParameter("allowExternalReps");
            if(allowExternalRepCheck != null && allowExternalRepCheck.equals("Yes")){
                Merchant.updateMerchantPermissions(true, Merchant.flagExternalRepsAllowed);
            }else{
                Merchant.updateMerchantPermissions(false, Merchant.flagExternalRepsAllowed);
            }
            allowExternalOutsideHierarchyCheck = request.getParameter("allowMerchantAssignment");
            if(allowExternalOutsideHierarchyCheck != null && allowExternalOutsideHierarchyCheck.equals("Yes")){
                Merchant.updateMerchantExternalOutsideHierarchy(true, Merchant.flagMerchantAssignmentAllowed);
            }else{
                Merchant.updateMerchantExternalOutsideHierarchy(false, Merchant.flagMerchantAssignmentAllowed);
            }
            if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
            {
                String paymentTypeInvoice = request.getParameter("paymentTypeForInvoiceGeneration");
                if ( paymentTypeInvoice != null)
                {
                    String[] paymentCodes = paymentTypeInvoice.split("-");
                    //Merchant.setPaymentTypeForInvoiceGeneration(paymentCodes[0]);
                    if ( paymentCodes!=null && ( paymentCodes[1].equals("C") || paymentCodes[1].equals("N/A") ) )
                    {

                    }
                    else
                    {
                        String accountInvoice = request.getParameter("accountNumberForInvoiceGeneration");
                    }
                }
                else
                {
                  //Merchant.setPaymentTypeForInvoiceGeneration("-1");      
                }
            }
            else
            {
              //Merchant.setPaymentTypeForInvoiceGeneration("-1");      
            }

            Merchant.addMerchant(SessionData, application, request);
            CustomerSearch cs = new CustomerSearch();
            cs.setCriteria(Merchant.getMerchantId());
            Vector v = (Vector)cs.searchMerchant(1, 1, SessionData, application).get(1);
            User.setStatus(1);
            User.setRefId(v.get(0).toString());
            User.setRefType(DebisysConstants.PW_REF_TYPE_MERCHANT);
            if (!isDomestic){
                User.addUser(SessionData,application,false);
                for (int j = 1; j <= 19; j++)
                {
                    if (request.getParameter("perm" + "_" + j) != null)
                    {
                      User.setPermissionTypeId(Integer.toString(j));
                      User.addUserPermission();
                    }
                }
            }
            merchantAdded = true;
            if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
            {
                session.removeAttribute("vecContacts");
                session.removeAttribute("merchantInvoicePaymentsArray");
            }
            try
            {
                // 2009-04-30 FB.Mail notification auto fill removed because ticket  5545-7483655
                String mailNotification = StringUtil.toString(request.getParameter("mailNotification")).trim();
                if(!mailNotification.equals(""))
                {
                  String mailHost = DebisysConfigListener.getMailHost(application);
                  String accessLevel = SessionData.getProperty("access_level");
                  String companyName = SessionData.getProperty("company_name");
                  String userName = SessionData.getProperty("username");
                  Merchant.sendMailNotification(mailHost,accessLevel, companyName,userName, User.getUsername(), User.getPassword(), mailNotification, application);
                  addJavaScriptCode = false;
                }
            }
            catch (MessagingException e)
            {
              String s = e.toString();
            }
        }
        else
        {
          merchantErrors = Merchant.getErrors();
          userErrors = User.getErrors();
        }
    }
  }
  catch (Exception e)
  {
    String s = e.toString();
    merchantErrors = new Hashtable();
    merchantErrors.put("error1", s);
    userErrors = new Hashtable();
    userErrors.put("error1", s);
  }
}
else
{
  if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
  {
    session.removeAttribute("vecContacts");
  }
}
%>

<%@ include file="/includes/header.jsp" %>
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<%
  //s2k loaded style and javascript for DBSY-931 System 2000 Tools
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If when deploying in International and user is an ISO level
%>
  <script type="text/javascript" src="includes/s2k/salesmannameid.js"></script>
<%}%>


<%
    boolean isInternational = customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
        &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
    Long isoID = Long.parseLong(SessionData.getUser().getISOId(strAccessLevel,SessionData.getUser().getRefId()));
    List<District> districts=null;
    if (isInternational){
        districts = District.findByCountryByIso(isoID);
        if (districts==null){
            isInternational=false;
        }
    }
   boolean permLocalizeMerchant = SessionData.checkPermission(DebisysConstants.PERM_ALLOW_LOCALIZE_MERCHANT_ON_MAP);
   if ( permLocalizeMerchant && merchantErrors == null) {
    String featureCode = "MerchantMapLocator";
    ArrayList<PropertiesByFeature> propertiesByFeature = PropertiesByFeature.findPropertiesByFeature(featureCode);
 
    propertiesValues = new HashMap();
    propertiesValues.put(latitudeValue, "25.05"); 
    propertiesValues.put(longitudeValue, "-77.33");
    propertiesValues.put(zoomValue, "11");
    for( PropertiesByFeature property : propertiesByFeature)
    {
        if ( propertiesValues.containsKey( property.getProperty() ))
        {
            propertiesValues.put( property.getProperty(), property.getValue());
        }
    }     
%>
        <link rel="stylesheet" type="text/css" href="css/maps.css" title="style">
        
<%}%>
<link rel="stylesheet" type="text/css" href="css/popUp.css" title="style">
<style>
  .sep{clear:both; float:none;}
  .address{padding: 10px;}
  input{font-size: 10pt;}
  button{font-size: 10pt;}
  .address-option{border: 1px dotted gray;}
  .address-map{float: right; border: 1px dotted gray;}
  .address-info{max-width: 330px; witdh: 330px; float: left; text-align: left; padding: 5px; border: 1px dotted gray; overflow: auto;}
  .address-message{align: center; font-size: 12px; font-weight:bold;}
  .address-suggestions{clear:both; overflow: auto; padding-top:20px;}
  .address-buttons{align:center;}
</style>
<script language="JavaScript" src="includes/popUp.js"></script>
<script language="JavaScript" src="includes/AddressValidation.js"></script>
<script language="JavaScript" src="includes/merchants_validations.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8">
<%
    Enumeration enumErr = null;
    StringBuilder valError = new StringBuilder();
    if (merchantErrors!=null){
        enumErr = merchantErrors.keys();
        while ( enumErr.hasMoreElements() )
        {
          String strKey = enumErr.nextElement().toString();
          String strError = (String) merchantErrors.get(strKey);
          if (strError != null && !strError.equals(""))
          {
            valError.append("valControls.push( {'control':'"+strKey+"', 'messages': ['"+strError+"']});");        
          }
        }
    }
%>
    $(function () {
        debugger;
<%    
    
    if (valError.toString().length()>0){
        out.println(valError);
        out.println("showValidationMessages(true); showMessagesValidations(); valControls=[];");        
    }
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
%>

    //******Internationalitation******************//
      labelValidatingAddressInformation = '<%=labelValidatingAddressInformation%>';
    labelInvalidInput = '<%=labelInvalidInput%>';
    labelKeyBad = '<%=labelKeyBad%>';
    labelAddressNotFound = '<%=labelAddressNotFound%>';
    labelInputWarnning1 = '<%=labelInputWarnning1%>';
    labelInputWarnning2 = '<%=labelInputWarnning2%>';
    labelInputWarnning3 = '<%=labelInputWarnning3%>';
    labelInputWarnning4 = '<%=labelInputWarnning4%>';
    labelInputWarnning5 = '<%=labelInputWarnning5%>';
    labelCurrentAddress = '<%=labelCurrentAddress%>';
    labelSuggestion1 = '<%=labelSuggestion1%>';
    labelSuggestion2 = '<%=labelSuggestion2%>';
    labelApply = '<%=labelApply%>';
    labelCancel = '<%=labelCancel%>';
    labelNotConnect = '<%=labelNotConnect%>';
      //******Internationalitation******************//
    graceDays = '<%=graceDaysLeft%>';
    validationRequired = true;
    popAddress = new PopUp('addressValidation', 'Address validation', 600, 400);
    $('input[name=physAddress], input[name=physCity], input[name=physCounty], input[name=physState], input[name=physZip], input[name=latitude], input[name=longitude]').change(function(){
      $('#addressValidationStatus').val('0');
    });
    $('#Buttonsubmit').click(function(){
      if($('#addressValidationStatus').val() === 0){
        //alert("Debug: "+$('#addressValidationStatus').val());
        validateAddress('admin/tools/address.jsp'
          ,$('input[name=physAddress]').val()
          ,$('input[name=physCity]').val()
          ,$('input[name=physState]').val()
          ,$('input[name=physZip]').val()
          ,function(data){
            if ( data !== null ){
              $('input[name=physAddress]').val(data[0]);
              $('input[name=physCity]').val(data[1]);
              $('input[name=physCounty]').val(data[2]);
              $('input[name=physState]').val(data[3]);
              $('input[name=physZip]').val(data[4]);
              $('input[name=latitude]').val(data[5]);
              $('input[name=longitude]').val(data[6]);
              $('span[name=latvalue]').text(data[5]);
              $('span[name=longvalue]').text(data[6]);
              $('#addressValidationStatus').val('1');
            }
            //$('#formMerchant').submit();
            check_form(formMerchant);
          }
          ,null);
      }else{
        //$('#formMerchant').submit();
        check_form(formMerchant);
      }
      return false;
    });
    $('#cancel').click(function(){history.go(-1);});
  
<%
}else{
%>

    $("#timeZoneWarning").css("font-size", "14px");  
    $("#timeZoneWarning").css("font-weight", "bold");  
    $("#timeZoneWarning").css("font-style", "italic");  
    $('#Buttonsubmit').click(function(){
      //$('#formMerchant').submit();
      check_form(formMerchant);
      showMessagesValidations();
    });
    $('#cancel').click(function(){history.go(-1);});        
 
<%
}
%>
    });
</script>


<script language="JavaScript">

var valControls=[];

function showMessagesValidations() {
  if (valControls.length>0){
    var element = $("#divMessages");
    var intWidth = $(window).innerWidth();
    var intHeight = $(window).innerHeight();
    var xCenter = (intWidth / 2)+100;
    var yCenter = intHeight / 2;
    element.css('position', 'fixed' );  
    element.css('top', 'auto' );  
    element.css('bottom', yCenter+'px' );  
    element.css('right', '20px' );  
    element.css('left', xCenter+'px' );  
    element.css('background-color', 'white' );                 
    element.css('border-radius', '15px' );  
    element.css('border', 'solid' );  
    element.css("font-size", "11px");
    element.css("opacity", "0.5");
    element.css("filter:", "alpha(opacity=50)");    
  }
}
window.onscroll = showMessagesValidations;
window.onresize =  showMessagesValidations;

var form = "";
var submitted = false;
var error = false;
var error_message = "";

    
function check_input(field_name, field_size, message) {
    if(form.elements[field_name] === undefined){
    	return;
    }
  var isShow = (form.elements[field_name].type !== "hidden");  
  var isDisplay = (form.elements[field_name].style.display !== "none" ||
                    form.elements[field_name].style.display === "");  
  
  if (form.elements[field_name] && (isShow && isDisplay) ){
    var field_value=null;      
    var isSelect = false;
    if($("#"+field_name).is("select")) {
        isSelect = true;
        var selected_option = $('#'+field_name+' option:selected');
        if (selected_option.text().length>0 ){
            field_value = selected_option.val();
        } else{
            field_value='';
        }
    }    
    else if($("#"+field_name).is("input") || $("#"+field_name).is("textarea")) {
        field_value = form.elements[field_name].value;
    }    

    if (field_value === undefined || field_value ===null || field_value === '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      error = true;    
      
      var arr = jQuery.grep(valControls, function( n, i ) {
        return ( n.control === field_name );
      });
      
      if (arr.length===0){
        valControls.push( {'control':field_name, 'messages': [message]});
        if (isSelect || $("#"+field_name).is("textarea")){
          $("#"+field_name).after('<span id=span_'+field_name+'>*</span>');
          var spn = $("#span_"+field_name);
            if (spn!==null){
              spn.css("color", "red");   
              spn.css("font-weight", "bold");
              spn.css("font-size", "25px");
            }
        } else{
            var spanControl="";
            var spn;
            spanControl = field_name;
            if (field_name === "contactData"){
                spanControl = "contactDataH"; //span_Label"+field_name;  //contactDataH              
                $("input[id='"+spanControl+"']").after("<span id='s"+spanControl+"' name='s"+spanControl+"'>*</span>");
                spn = $("#s"+spanControl);
            }else{
                    $("input[name="+spanControl+"]").after('<span id=span_'+spanControl+'>*</span>');
                    spn = $("#span_"+spanControl);
                }
          if (spn!==null){
            spn.css("color", "red");  
            spn.css("font-weight", "bold");
            spn.css("font-size", "25px");
          }
        }
      } else if (arr.length===1){
        jQuery(valControls).each(function (index){
            if(valControls[index].control === field_name){                
                var addMessage = true;
                $.each(valControls[index].messages, function(i, msg){
                    if (msg===message){
                        addMessage = false;
                    }                    
                });
                if (addMessage){
                    valControls[index].messages.push(message);
                }else{
                    return false;
                }
            }
        });
      }
    } else{        
        jQuery(valControls).each(function (index){
            if(valControls[index].control === field_name){
                
                $.each(valControls[index].messages, function(i, msg){
                    if(message === msg ){
                        valControls[index].messages.splice(i,1); // This will remove the object 
                        return false;
                    }
                });
                if (valControls[index].messages.length===0){
                    valControls.splice(index,1); // This will remove the object 
                    return false; // This will stop the execution of jQuery each loop.
                }
            }
        });
       $("#span_"+field_name).remove();
    }
  } else{
        jQuery(valControls).each(function (index){
            if(valControls[index].control === field_name){
                
                $.each(valControls[index].messages, function(i, msg){
                    if(message === msg){
                        valControls[index].messages.splice(i,1); // This will remove the object 
                        return false;
                    }
                });
                if (valControls[index].messages.length===0){
                    valControls.splice(index,1); // This will remove the object 
                    return false; // This will stop the execution of jQuery each loop.
                }
            }
        });
       $("#span_"+field_name).remove();
  }
}

function validate_select_processType(field_name, field_size, message) {
  if (form.elements[field_name] && (form.elements[field_name].type !== "hidden")) {
    var field_value = form.elements[field_name].value;

    if (field_value === '' || field_value === "-1") {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}
/*
function validate_select(field_name, message) {
  if (form.elements[field_name] && (form.elements[field_name].type !== "hidden")) {    
    var field_value = form.elements[field_name].value;

    if (field_value === '' || field_value === "-1"  || field_value === "0") {
        error_message = error_message + "* " + message + "\n";
        error = true;        
    }
  }
}

function check_inputClassificationDesc(message) {
  if (form.elements["merchantClassificationOtherDesc"] && form.elements["merchantClassificationOtherDesc"].type !== "hidden" && document.getElementById("merchantClassificationOtherDescDiv").style.display !== "none") {
    var field_value = form.elements["merchantClassificationOtherDesc"].value;

    if (field_value === '' || field_value.length > 150) {
      error_message = error_message + "* " + message + "\n";
      error = true;      
    }
  }
}*/
var emailPattern = '^[_a-zA-Z0-9-\\/]+(\\.[_a-zA-Z0-9-\\/]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$';
        
function check_form(form_name) {
  var creditTypeD = $("#lstMerchantType").val();
  if(confirmSetTestAccount() === false){
    return false;
  }  
  if (submitted === true) {
    alert("<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg1",SessionData.getLanguage())%>");
    return false;
  }
  error = false;
  form = form_name;
  error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";
  
<%
   //if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { 
%>
  check_input("repName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_rep_name",SessionData.getLanguage())%>");
<%
   //} 
%>

  check_input("businessName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_business_name",SessionData.getLanguage())%>");
  check_input("dba", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_dba",SessionData.getLanguage())%>");
          
  
<%
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
  //check_input("businessHours", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_businesshours",SessionData.getLanguage())%>");
<%
   } //End of if when deploying in Mexico
%>
  check_input("physAddress", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_street_address",SessionData.getLanguage())%>");
  <%
    String mandatoryMerchantClassification = DebisysConfigListener.getMandatoryMerchantClassification(application); 
    
  if (mandatoryMerchantClassification.trim().equals("1")){%>
    check_input("merchantClassification", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_merchantClassification",SessionData.getLanguage())%>");
    check_input("merchantClassificationOtherDesc", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_merchantClassificationOtherDesc",SessionData.getLanguage())%>");
    //validate_select("merchantClassification", "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_merchantClassification",SessionData.getLanguage())%>");
    //check_inputClassificationDesc("<%=Languages.getString("jsp.admin.customers.merchants_edit.error_merchantClassificationOtherDesc",SessionData.getLanguage())%>");<%
    }
    %>
   
  <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) || deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
  {
  %>
  check_input("physCity", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_city",SessionData.getLanguage())%>");
  check_input("physState", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_state",SessionData.getLanguage())%>");
  check_input("physZip", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_zip",SessionData.getLanguage())%>");
  
    if (creditTypeD !== "<%= DebisysConstants.MERCHANT_TYPE_PREPAID %>") {
        check_input("accountNumber", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_account_number",SessionData.getLanguage())%>");
        check_input("routingNumber", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_routing_number",SessionData.getLanguage())%>");
    }
        
  <%
  } 
  %>
  check_input("physCountry", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_country",SessionData.getLanguage())%>");

<%
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
    if ( !document.getElementById('chkBillingAddress'.checked) ) {
      check_input("mailAddress", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_billing_address",SessionData.getLanguage())%>");
      check_input("mailCountry", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_billing_country",SessionData.getLanguage())%>");
    }
    check_input("contactData", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact_data",SessionData.getLanguage())%>");
    check_input("taxId", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_taxid",SessionData.getLanguage())%>");      

<%
   } //End of if when deploying in Mexico
   else
   {
%>

  check_input("contactName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact",SessionData.getLanguage())%>");
  check_input("contactPhone", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_phone",SessionData.getLanguage())%>");  
  
<%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){%>
    var emailNot = document.getElementById('contactEmail');
    if (emailNot!==null){
        ValidateRegExp(emailNot, emailPattern, true);
        check_input("contactEmail", 3, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact_Email",SessionData.getLanguage())%>");                 
    }     
<%}%>
    
    
<%
   }


   if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES)){
    %>
    validate_select_processType("processType", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_processType",SessionData.getLanguage())%>");
   <%
   }

  // DBSY-1072 eAccounts Interface
    if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)){
%>
      check_input("entityAccountType", 1, "<%=Languages.getString("jsp.admin.customers.error_entityAccountType",SessionData.getLanguage())%>");
<%
    }

  //s2k javascript check values for DBSY-931 System 2000 Tools
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If deploying in International and user is an ISO
%>
   var name = form.salesmanid.options[form.salesmanname.selectedIndex].value;
   var id = form.salesmanid.options[form.salesmanid.selectedIndex].value;
    // skip check if both empty
   //if( id.length >0  || name.length >0 ){
      //if( id === "" || id === null ){
          check_input("salesmanid", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_salesmanid",SessionData.getLanguage())%>");
      //}
      //else if( name === "" || name === null ){
      //    check_input("salesmanname", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_salesmanname",SessionData.getLanguage())%>");
      //}
   //}
<%}
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) || deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
   {
%>
    if (document.getElementById('monthlyFeeThreshold')!==null){
        validateMonthlyFee(document.getElementById('monthlyFeeThreshold'));
    }
    validateCreditLimit(document.getElementById('creditLimit'));
    if ( (document.getElementById('lstMerchantType').value === <%=DebisysConstants.MERCHANT_TYPE_CREDIT%>) && document.getElementById('salesLimitEnable').checked )
    {
        ValidateDailySalesLimit(document.getElementById('salesLimitMonday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitMonday",SessionData.getLanguage())%>');
        ValidateDailySalesLimit(document.getElementById('salesLimitTuesday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitTuesday",SessionData.getLanguage())%>');
        ValidateDailySalesLimit(document.getElementById('salesLimitWednesday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitWednesday",SessionData.getLanguage())%>');
        ValidateDailySalesLimit(document.getElementById('salesLimitThursday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitThursday",SessionData.getLanguage())%>');
        ValidateDailySalesLimit(document.getElementById('salesLimitFriday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitFriday",SessionData.getLanguage())%>');
        ValidateDailySalesLimit(document.getElementById('salesLimitSaturday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitSaturday",SessionData.getLanguage())%>');
        ValidateDailySalesLimit(document.getElementById('salesLimitSunday'), '<%=Languages.getString("jsp.admin.saleslimit.salesLimitSunday",SessionData.getLanguage())%>');
    }

<%
   }
%>
//Changed by LF on 2006/07/04 because the validation for TaxID was already added and also a language resource already exists
//changed by jay on 6/22/06
//since it should be required for mexico and domestic but not international
<%
    if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) ) {
%>
        check_input("taxId", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_tax_id",SessionData.getLanguage())%>");
        if (creditTypeD !== "<%= DebisysConstants.MERCHANT_TYPE_PREPAID %>") {

            var isCorrectAba = verifyAba('/^[0-9]{9}$/',"<%=Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage())%>");
            if(isCorrectAba === false){
              error = true;
              error_message = error_message + "* "+"<%=Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage())%>"+" \n";
            }

            var isCorrectAccount = verifyAccount(20,"<%=Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage())%>");
            if(isCorrectAccount === false){
              error = true;
              error_message = error_message + "* "+"<%=Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage())%>"+" \n";
            }
        }
<%
    } else{
    %>
        check_input("username", 3, "<%=Languages.getString("com.debisys.users.error_username_length",SessionData.getLanguage())%>");
        var passwordControl = document.getElementById('password'); 
        if (passwordControl!==null){
            var passPattern = "^(?=.{8,})(?=.*[!@#\\$%\\^&\\*\\(\\)\\+=\\|;'\"{}<>\\.\\?\\-_\\\\/:,~`])(?=.*\\d.*\\d.*\\d)(?=.*[A-Z]).*$";
            ValidateRegExp(passwordControl, passPattern, true);
            check_input("password", 1, "<%=Languages.getString("jsp.admin.passwordChangeError",SessionData.getLanguage())%>");            
        }
    <%
    }
%>
  
  //required for every deployment
    var emailNot = document.getElementById('mailNotification');
    if (emailNot!==null){
        ValidateRegExp(emailNot, emailPattern, true);
        check_input("mailNotification", 3, "<%=Languages.getString("jsp.admin.emailError",SessionData.getLanguage())%>");                 
    }     
    
    if (error === true)
    {   
        showValidationMessages(true);        
        return false;
    }
    else
    {
        showValidationMessages(false);
        <%
        if ( !deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) ) {
        %>
        var dataFieldsValues = $('#formMerchant').serializeArray();    
        var pageJSP = "admin/customers/merchants/validateFieldsByEntityType.jsp";
        $.post(pageJSP, dataFieldsValues,
            function (data){
                $.each(data, function(i, item){
                    valControls.push( {'control':i, 'messages': [item]});                    
                });
                if (valControls.length>0){
                    showValidationMessages(true);
                    valControls = [];                    
                } else{
                    showValidationMessages(false);                    
                    document.formMerchant.Buttonsubmit.disabled = true;
                    submitted = true;                    
                    $('#formMerchant').submit();                    
                }
                
            }
        );
        <%
        } else{
        %>
            showValidationMessages(false);                    
            document.formMerchant.Buttonsubmit.disabled = true;
            submitted = true;                    
            $('#formMerchant').submit(); 
        <%
        }
        %>
     }
}

  function showValidationMessages(showMessages){
    var divControl = "divMessages";    
    $("#"+divControl+" ul").remove();
    if (showMessages){        
        $("#"+divControl).append('<ul></ul>');
        $.each(valControls, function(i, item){
            $.each(item.messages, function(i, message){
                $("#"+divControl+" ul").append('<li>'+message+'</li>');    
            });            
        });    
        $("#"+divControl).show();
        var controlFocus = $("#"+valControls[0].control);
        controlFocus.focus(8);    
    } else{
        $("#"+divControl).hide();
    }
  }

    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode !== 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;
        return true;
    }    
    
  function validateInteger(c) {
    if ( c.value.length > 0 ) {
        if (isNaN(c.value)) {
            //alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
            //c.focus();
            //c.select();
            return (false);
        } else if (c.value < 0) {
            alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
            //c.focus();
            //c.select();
            return (false);
        }
    }
  }

  function validateMonthlyFee(c) {
    var montlhyFeeValidationMessage = "<%=Languages.getString("com.debisys.terminals.error_monthly_fee",SessionData.getLanguage())%> ";
    if ( c.value.length > 0 && parseFloat(c.value) !== '0' ) {                      
        if (isNaN(c.value)) {              
            c.value = '';
            check_input(c.id, 1, montlhyFeeValidationMessage);
            c.value = '0';
            error = true;
        } else if (parseFloat(c.value) < 0) {
            c.value = '';            
            check_input(c.id, 1, montlhyFeeValidationMessage);
            c.value = '0';            
            error = true;
        } else {
            check_input(c.id, 0, montlhyFeeValidationMessage);            
        }
    } else {
        check_input(c.id, 1, montlhyFeeValidationMessage);            
    }
  }
  
  function validateCreditLimit(c) {
    if (parseInt(document.getElementById('lstMerchantType').value) === 3) { //unlimited case
        // (see function checkCreditLimitTR)
    } else {
        var preFixCredit = "<%=Languages.getString("jsp.admin.customers.merchantsDashBoard.creditlimitamount",SessionData.getLanguage())%> ";
        var isNaNMessage = preFixCredit+"<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>";
        var lengthMessage = preFixCredit+"<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>";
        if ( c.value.length > 0 ) {                      
          if (isNaN(c.value)) {              
            c.value = '';
            check_input("creditLimit", 1, isNaNMessage);
            c.value = '0';
            c.focus();            
            c.select();
            error = true;
          } else if (parseInt(c.value) < 0) {
            c.value = '';            
            check_input("creditLimit", 1, lengthMessage);
            c.value = '0';
            //alert(s);
            c.focus();
            c.select();
            error = true;
          } else{            
            check_input("creditLimit", 0, isNaNMessage);
            check_input("creditLimit", 0, lengthMessage);            
          }
        }
    }
  }

  
  function ValidateRegExp(c, e, isRequired) {
    var exp = new RegExp(e);
    if ( !exp.test(c.value) && (c.value.length > 0 || isRequired) ) {
        c.value='';
        return false;
    } else{
        //check_input(c.id, 1, "FAIL "+c.id);
    }
    return true;
  }

  function verifyAba(expression, textInfo){
    var optionAbaCk = document.getElementById('optionAba').checked;
    if(optionAbaCk === true){
        return true;
    }
    var valueAba = document.getElementById('routingNumber').value;
    var isValid = verifyNumericText(valueAba,expression);
    var isValidLength = false;
    var isNoAllZero = false;
    var isValidSQL = false;

    if ( isValid === true && valueAba.length === 9 ) {
        isValidLength = true;
        isNoAllZero = verifyNoAllZero(valueAba);
        if(isNoAllZero === true){
            isValidSQL = validationSQL(valueAba);
        }
    }

    if(isValid === true && isValidLength === true && isNoAllZero === true && isValidSQL === true){
        var  div_info = document.getElementById("div_info_aba");
        div_info.style.color="black";
        div_info.innerHTML = "";
        return true;
    }
    else{
        var  div_info = document.getElementById("div_info_aba");
        div_info.style.width="200px";
        div_info.style.color="red";
        div_info.innerHTML = "";
        var newdiv=document.createElement("div");
        var newtext=document.createTextNode(textInfo);
        newdiv.appendChild(newtext); //append text to new div
        div_info.appendChild(newdiv);
        return false;
    }
  }

  function verifyAccount(lengthText, textInfo){
    var optionAbaCk = document.getElementById('optionAccount').checked;
    if(optionAbaCk === true){
      return true;
    }
    var valueAccount = document.getElementById('accountNumber').value;
    var isValid = verifyNumericText(valueAccount,'/^[0-9]{1,'+lengthText+'}$/');
    var isValidLength = false;
    var isNoAllZero = false;
    if ( isValid === true && valueAccount.length <= 20 ) {
      isValidLength = true;
      isNoAllZero = verifyNoAllZero(valueAccount);
    }

    if(isValid === true && isValidLength === true && isNoAllZero === true){
      var  div_info = document.getElementById("div_info_account");
      div_info.style.color="black";
      div_info.innerHTML = "";
      return true;
    }
    else{
      var  div_info = document.getElementById("div_info_account");
      div_info.style.width="200px";
      div_info.style.color="red";
    div_info.innerHTML = "";
    var newdiv=document.createElement("div");
    var newtext=document.createTextNode(textInfo);
    newdiv.appendChild(newtext); //append text to new div
    div_info.appendChild(newdiv);
    return false;
    }

  }

  // EXEC THE REGULAR EXPRESSIONS
  function verifyNumericText(text,exp){
    exp = exp.replace("/", "");
    exp = exp.replace("/", "");
    exp = exp.replace(/\\/g, "\\");
    var patt1 = new RegExp(exp);
    var result = patt1.exec(text);
    if (result !== null){
        return true;
    }
    return false;
  }

  // VERIFY THAT AN TEXT NOT CONTAIN ALL ZEROS
  function verifyNoAllZero(text){
    var i=0;
    var countZeros = 0;
    for(i = 0; i < text.length ; i++){
      if(text.charAt(i) !== '0'){
        return true;
      }
    }
    return false;
  }

  //VALIDATION FOR ABA MERCHANT WITH FORMAT OF THE DB
  function validationSQL(text){

    var digit1 = parseInt(text.charAt(0))*3;
    var digit2 = parseInt(text.charAt(1))*7;
    var digit3 = parseInt(text.charAt(2))*1;
    var digit4 = parseInt(text.charAt(3))*3;
    var digit5 = parseInt(text.charAt(4))*7;
    var digit6 = parseInt(text.charAt(5))*1;
    var digit7 = parseInt(text.charAt(6))*3;
    var digit8 = parseInt(text.charAt(7))*7;
    var digit9 = parseInt(text.charAt(8));

      var sumDigits = digit1+digit2+digit3+ digit4+ digit5+digit6+ digit7+digit8 ;
      var mod10 = sumDigits % 10;
      var totalOP = 10-mod10;
      var resultDigit = getRightDigits(totalOP,1);

      if(resultDigit === digit9){
        return true;
      }
      else{
        return false;
      }
  }

  // GET AN NUMBER DIGITS THE ONE NUMBER
  function getRightDigits(num,digits){
    var i=0;
    var numString = num.toString();
    var realNumStr = '';
    for(i = (numString.length-digits); i < numString.length ; i++){
    realNumStr = realNumStr+numString.charAt(i);
    }
    var realNum = parseInt(realNumStr);
    return realNum;
  }

  function changeCheckAba(){
    var optionAbaCk = document.getElementById('optionAba').checked;
    if(optionAbaCk === true){
      var div_info = document.getElementById("div_info_aba");
      div_info.style.color="black";
      div_info.innerHTML = "";

      var valueAba = document.getElementById('routingNumber');
      valueAba.value = 'NA';
      valueAba.disabled=true;
    }
    else{
      var valueAba = document.getElementById('routingNumber');
      valueAba.value = '';
      valueAba.disabled=false;
    }
  }

  function changeCheckAccount(){
    var optionAbaCk = document.getElementById('optionAccount').checked;
    if(optionAbaCk === true){
      var div_info = document.getElementById("div_info_account");
      div_info.style.color="black";
      div_info.innerHTML = "";

      var valueAccount = document.getElementById('accountNumber');
      valueAccount.value = 'NA';
      valueAccount.disabled=true;
    }
    else{
      var valueAccount = document.getElementById('accountNumber');
      valueAccount.value = '';
      valueAccount.disabled=false;
    }
  }

  function EnableBillingAddress(bChecked) {
    document.getElementById('mailAddress').readOnly = bChecked;
    document.getElementById('mailColony').readOnly = bChecked;
    document.getElementById('mailDelegation').readOnly = bChecked;
    document.getElementById('mailCity').readOnly = bChecked;
    document.getElementById('mailZip').readOnly = bChecked;
    document.getElementById('mailState').disabled = bChecked;
    document.getElementById('mailCounty').readOnly = bChecked;
    document.getElementById('mailCountry').disabled = bChecked;
    if (bChecked) {
        document.getElementById('mailAddress').value = document.getElementById('physAddress').value;
        document.getElementById('mailColony').value = document.getElementById('physColony').value;
        document.getElementById('mailDelegation').value = document.getElementById('physDelegation').value;
        document.getElementById('mailCity').value = document.getElementById('physCity').value;
        document.getElementById('mailZip').value = document.getElementById('physZip').value;
        document.getElementById('mailState').value = document.getElementById('physState').value;
        document.getElementById('mailCounty').value = document.getElementById('physCounty').value;
        document.getElementById('mailCountry').value = document.getElementById('physCountry').value;
    }
    else {
        document.getElementById('mailAddress').value = '';
        document.getElementById('mailColony').value = '';
        document.getElementById('mailDelegation').value = '';
        document.getElementById('mailCity').value = '';
        document.getElementById('mailZip').value = '';
        document.getElementById('mailCounty').value = '';
        document.getElementById('mailCountry').value = '';
    }
  }//End of function EnableBillingAddress

  function SetRepSharedBalance(nValue, nCreditType) {
    if ( nValue === 1 )
    {
      document.getElementById('creditLimit').value = 0.1;
      try {
        document.getElementById('lstMerchantType').style.display = "none";
        document.getElementById('lblSharedBalance').style.display = "block";
        document.getElementById('trCreditLimit').style.display = "none";
      } catch (err) { }
    }
    else
    {
        document.getElementById('creditLimit').value = "";
        try {
        document.getElementById('lstMerchantType').style.display = "block";
        document.getElementById('lblSharedBalance').style.display = "none";
        document.getElementById('trCreditLimit').style.display = "block";
      } catch(err) { }

    }
    document.getElementById('sharedBalance').value = nValue;
  }//End of function SetRepSharedBalance

    function ValidateDailySalesLimit(DailyLimitCtl, sDayText){
      var NumExpression = new RegExp(/^[0-9\.]+$/);
      var limit = parseFloat(document.getElementById('creditLimit').value);
      if ( isNaN(limit) )
      {
        limit = 0;
      }

      if ( !NumExpression.test(DailyLimitCtl.value) )
      {
        var str = '<%=Languages.getString("jsp.admin.saleslimit.salesLimitInvalid",SessionData.getLanguage())%>';
        str = str.replace("_DAY_", sDayText);
        error_message += "* " + str + "\n";;
        error = true;
      }

      if ( parseFloat(DailyLimitCtl.value) > limit )
      {
        var str = '<%=Languages.getString("jsp.admin.saleslimit.salesLimitLowerThanLimit",SessionData.getLanguage())%>';
        str = str.replace("_VALUE_", limit);
        str = str.replace("_DAY_", sDayText);
        error_message += "* " + str + "\n";;
        error = true;
      }
    }
  

 //DTU-369 Payment Notifications
  function changePayNoti(){
    var payNoti = document.getElementById('paymentNotifications').checked;
    if(payNoti === true)
    {
      var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
      valuePayNotiSMSChk.checked=false;
      valuePayNotiSMSChk.disabled=false;
      var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
      valuePayNotiMailChk.checked=false;
      valuePayNotiMailChk.disabled=false;
    }
    else
    {
      var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
      valuePayNotiSMSChk.checked=false;
      valuePayNotiSMSChk.disabled=true;
      var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
      valuePayNotiMailChk.checked=false;
      valuePayNotiMailChk.disabled=true;
    }
  }

  function changePayNotiSMS(){
    var payNotiSMS = document.getElementById('paymentNotificationSMS').checked;
    var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
    if(payNotiSMS === true)
  {
      valuePayNotiMailChk.checked=false;
    }
    else
  {
      valuePayNotiMailChk.checked=true;
    }
  }

  function changePayNotiMail(){
    var payNotiMail = document.getElementById('paymentNotificationMail').checked;
    var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
    if(payNotiMail === true)
  {
      valuePayNotiSMSChk.checked=false;
    }
    else
  {
      valuePayNotiSMSChk.checked=true;
    }
  }

    //DTU-480: Low Balance Alert Message SMS and or email
    function changeBalNoti() {
            var balNoti = document.getElementById('balanceNotifications').checked;
            if (balNoti === true) {
                var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
                valueBalNotiDaysChk.checked = false;
                valueBalNotiDaysChk.disabled = false;
                var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
                valueBalNotiValueChk.checked = false;
                valueBalNotiValueChk.disabled = false;        
                var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
                valueBalNotiSMSChk.checked = false;
                valueBalNotiSMSChk.disabled = false;
                var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
                valueBalNotiMailChk.checked = false;
                valueBalNotiMailChk.disabled = false;
          var valueBalNotiValue = document.getElementById('balanceNotificationValue');
          var valueBalNotiDays = document.getElementById('balanceNotificationDays');
          valueBalNotiValue.disabled=true;
          valueBalNotiDays.disabled=true;               
            }
            else {
                var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
                valueBalNotiDaysChk.checked = false;
                valueBalNotiDaysChk.disabled = true;
                var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
                valueBalNotiValueChk.checked = false;
                valueBalNotiValueChk.disabled = true;
                var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
                valueBalNotiSMSChk.checked = false;
                valueBalNotiSMSChk.disabled = true;
                var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
                valueBalNotiMailChk.checked = false;
                valueBalNotiMailChk.disabled = true;
          var valueBalNotiValue = document.getElementById('balanceNotificationValue');
          var valueBalNotiDays = document.getElementById('balanceNotificationDays');
          valueBalNotiValue.disabled=true;
          valueBalNotiDays.disabled=true;                 
            }
        }

    function changeBalNotiValue(){
        var balNotiValue = document.getElementById('balanceNotificationTypeValue').checked;
        var valueBalNotiValue = document.getElementById('balanceNotificationValue');
        if(balNotiValue === true){
            valueBalNotiValue.disabled=false;
        }
        else{
            valueBalNotiValue.disabled=true;
        }
    }
  
    function changeBalNotiDays(){
        var balNotiDays = document.getElementById('balanceNotificationTypeDays').checked;
        var valueBalNotiDays = document.getElementById('balanceNotificationDays');          
        if(balNotiDays === true){
          valueBalNotiDays.disabled=false;            
        }
        else
        {
          valueBalNotiDays.disabled=true;           
        }
    } 
              
    function changeBalNotiSMS(){
        var balNotiSMS = document.getElementById('balanceNotificationSMS').checked;
        var valueBalNotiMail = document.getElementById('balanceNotificationMail');
        if (balNotiSMS === true){
            valueBalNotiMail.checked = false;
        }
        else{
            valueBalNotiMail.checked = true;
        }
    }

    function changeBalNotiMail(){
        var balNotiMail = document.getElementById('balanceNotificationMail').checked;
        var valueBalNotiSMS = document.getElementById('balanceNotificationSMS');            
        if (balNotiMail === true) 
        {

            valueBalNotiSMS.checked = false;
        }
        else 
        {
            valueBalNotiSMS.checked = true;
        }
    }
        
    function merchantClassificationCheck(){        
        var merchantClassificationId = document.getElementById('merchantClassification');
         $.getJSON("/support/includes/ajax.jsp?class=com.debisys.customers.Merchant&method=getMerchantClassificationByIdAjax&value=" + merchantClassificationId.value + "&rnd=" + Math.random(),
            function(data){
              $.each(data.items, function(i, item){
                    if(item.hasClassificationDescription === true){
                      document.getElementById('merchantClassificationOtherDescDiv').style.display="inline";
                      document.getElementById('merchantClassificationOtherDesc').style.display="inline";
                    }
                    else{
                      document.getElementById('merchantClassificationOtherDescDiv').style.display="none";
                      document.getElementById('merchantClassificationOtherDesc').style.display="none";
                    }
              });

            });
    }       


</script>


<table border="0" cellpadding="0" cellspacing="0" width="750">
  <tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
    <td colspan="3" bgcolor="#ffffff" class="formArea2">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
  <tr>
    <td class="main">
<%if (!merchantAdded)
{
  %>

<br>
<%=Languages.getString("jsp.admin.customers.merchants_add.add_instructions",SessionData.getLanguage())%>
<br><br>
<%}%>
      <table border="0" width="100%" cellpadding="0" cellspacing="0" align="left">
      <tr>
          <td>

<%if (!merchantAdded)
{
%>
<table width="100%">
<form id="formMerchant" name="formMerchant" method="post" action="admin/customers/merchants_add2.jsp" >
<%

if (merchantErrors != null)
{
    out.println("<tr class=main><td align=left ><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
    Enumeration enum1=merchantErrors.keys();
    while(enum1.hasMoreElements())
    {
        String strKey = enum1.nextElement().toString();
        String strError = (String) merchantErrors.get(strKey);
        if (strError != null && !strError.equals(""))
        {
            out.println("<li>" + strError + "</li>");
        }
    }
    enumErr = userErrors.keys();
    while ( enumErr.hasMoreElements() )
    {
        String strKey = enumErr.nextElement().toString();
        String strError = (String) userErrors.get(strKey);
        if (strError != null && !strError.equals(""))
        {
            out.println("<li>" + strError + "</li>");
        }
    }

  out.println("</font></td></tr>");
}


%>

                <tr>
                  <td class="formAreaTitle2"><%=Languages.getString("jsp.admin.customers.merchants_edit.company_information",SessionData.getLanguage())%></td>
                </tr>
                <tr>
                 <td class="formArea2">
                    <table>

                     <%
                     if (!strAccessLevel.equals(DebisysConstants.REP)){
                     %>
                        <tr>
                            <td class="main">
                                <b>
                                <%=Languages.getString("jsp.admin.customers.merchants_edit.sales_rep",SessionData.getLanguage())%>:
                            </td>
                            <td nowrap class="main">
                                <input type="text" name="repName" id="repName" value="<%=Merchant.getMerchantRepName(Merchant.getRepId())%>" size="30" readonly>
<%  
                                    if (merchantErrors != null && merchantErrors.containsKey("repId")) out.print("<font color=ff0000>*</font>");
%>
                                <input type="button" name="repSelect" id="repSelect" value="<%=Languages.getString("jsp.admin.customers.merchants_edit.select_rep",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/rep_selector.jsp?search=y','repSelect','width=700,height=500,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
                                <input type="hidden" name="repId" value="<%=Merchant.getRepId()%>">
                            </td>
                        </tr>

                        <%}
                          else /** if (strAccessLevel.equals(DebisysConstants.AGENT)
                              || strAccessLevel.equals(DebisysConstants.SUBAGENT)) **/
                          {
                          %>
                        <tr>
                            <td class="main">
                                <b>
                                    <%=Languages.getString("jsp.admin.customers.merchants_edit.sales_rep",SessionData.getLanguage())%>:
                            </td>
                            <td nowrap class="main">
                                <input type="text" name="repName" id="repName" value="<%=dataRepresentant.getBusinessName()%>" size="30" maxlength="200" readonly>
<%  
                                if (merchantErrors != null && merchantErrors.containsKey("repId")) out.print("<font color=ff0000>*</font>");
%>
                                <input type="hidden" name="repId" value="<%=SessionData.getProperty("ref_id")%>">
                            </td>
                        </tr>
                          <%
                          }
                        %>
                          <input type="hidden" name="repRatePlanId" value="<%=Merchant.getRepRatePlanId()%>">
                          <input type="hidden" name="merchantId" value="<%=Merchant.getMerchantId()%>">


                        <%
                        boolean permissionEnableEditMerchantAccountExecutives= SessionData.checkPermission(DebisysConstants.PERM_Enable_Edit_Merchant_Account_Executives);
                        if (permissionEnableEditMerchantAccountExecutives)
                        {
                          //Long isoID = Long.parseLong(SessionData.getUser().getISOId(strAccessLevel,SessionData.getUser().getRefId()));
                          ArrayList<com.debisys.tools.AccountExecutive> executives = com.debisys.tools.AccountExecutive.findExecutivesByActor(isoID,request,true,SessionData);
                          StringBuffer optionsExecutives = new StringBuffer();
                          for(com.debisys.tools.AccountExecutive account : executives)
                          {
                            optionsExecutives.append("<option value='"+account.getId()+"'>"+account.getNameExecutive()+"</option>");
                          }
                          optionsExecutives.append("<option value='0' selected >-----</option>");
                        %>


                        <tr>
                          <td class="main" nowrap>
                           <b><%=Languages.getString("jsp.admin.reports.tools.accountexecutives.label",SessionData.getLanguage())%> : <b/>
                          </td>
                          <td class="main" nowrap>
                           <select id="executiveAccountId" name="executiveAccountId">
                            <%=optionsExecutives.toString()%>
                           </select>
                          </td>
                        </tr>
                        <%}%>



                        <tr>
                          <td class="main"><b><%=Languages.getString("jsp.admin.customers.merchants_edit.business_name",SessionData.getLanguage())%>:</td><td nowrap class="main"><input type="text" name="businessName" id="businessName" value="<%=Merchant.getBusinessName()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("businessName")) out.print("<font color=ff0000>*</font>");%></td>
                        </tr>
                        <tr>
                          <td class="main"><b><%=Languages.getString("jsp.admin.customers.merchants_edit.dba",SessionData.getLanguage())%>:</td><td nowrap class="main"><input type="text" id="dba" name="dba" value="<%=Merchant.getDba()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("dba")) out.print("<font color=ff0000>*</font>");%></td>
                        </tr>
                                                <%if (!isDomestic){%>
                        <tr>
                          <td class="main"><b><%=Languages.getString("jsp.admin.customers.merchants_edit.merchantOwnerId",SessionData.getLanguage())%>:</td>
                          <td nowrap class="main"><input oncopy="return false" onpaste="return false" onkeypress="return isNumber(event);"                                         
                                type="number" id="merchantOwnerId" name="merchantOwnerId" value="<%=Merchant.getMerchantOwnerId()%>" 
                                onKeyDown="if(this.value.length>12) return false;"
                                min="1" max="999999999999" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("merchantOwnerId")) out.print("<font color=ff0000>*</font>");%></td>
                        </tr>
                                                <%}%>
                <%  if (SessionData.checkPermission(DebisysConstants.PERM_REGULATORY_FEE)) {

                  Vector vecSearchResults2 = new Vector();
                  vecSearchResults2 = Merchant.merchantRegulatoryFees(SessionData, application);
                  Iterator it2 = vecSearchResults2.iterator();

                  String selected = "";

                  %>
                 <tr>
                  <td class="main">
                    <b><%=Languages.getString("jsp.admin.customers.merchants_edit.regulatoryFee",SessionData.getLanguage())%>:
                  </td>
                  <td nowrap class="main">
                  <%if (!vecSearchResults2.isEmpty()) { %>
                    <select name="regulatoryFeeId">
                    <%
                      while (it2.hasNext())
                                {
                                  Vector vecTemp2 = null;
                                  vecTemp2 = (Vector) it2.next();
                                  if(Integer.toString(Merchant.getRegulatoryFeeId()).equals(vecTemp2.get(0))){
                                    selected="selected";
                                  }
                                  else{
                                    selected = "";
                                  }

                        out.println("<OPTION value=\""+vecTemp2.get(0)+ "\" " + " " +selected +">"+vecTemp2.get(1) + " ("+vecTemp2.get(0) + ")");

                         }
                            out.println("</SELECT>");
                            vecSearchResults2.clear();
                          %>
                    </select>
                  <%} %>
                  </td>
                 </tr>
                <% } %>
<%
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                <TR>
                                                <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.businesstype",SessionData.getLanguage())%>:</B></TD>
                                                <TD NOWRAP CLASS="main">
                                                    <SELECT NAME="businessTypeId" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecBusinessTypes = com.debisys.customers.Merchant.getBusinessTypes(customConfigType);
  Iterator itBusinessTypes = vecBusinessTypes.iterator();
  while (itBusinessTypes.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itBusinessTypes.next();
    String strBusinessTypeId = vecTemp.get(0).toString();
    String strBusinessTypeCode = vecTemp.get(1).toString();
    if (strBusinessTypeId.equals(request.getParameter("businessTypeId")))
      out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\" SELECTED>" + strBusinessTypeCode + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\">" + strBusinessTypeCode + "</OPTION>");
  }
%>
                                                    </SELECT>
                                                </TD>
                                                </TR>
                                                <TR>
                                                <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment",SessionData.getLanguage())%>:</B></TD>
                                                <TD NOWRAP CLASS="main">
                                                    <SELECT NAME="merchantSegmentId" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecMerchantSegments = com.debisys.customers.Merchant.getMerchantSegments();
  Iterator itMerchantSegments = vecMerchantSegments.iterator();
  while (itMerchantSegments.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itMerchantSegments.next();
    String strMerchantSegmentId = vecTemp.get(0).toString();
    String strMerchantSegmentCode = vecTemp.get(1).toString();
    if (strMerchantSegmentId.equals(request.getParameter("merchantSegmentId")))
      out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
  }
%>
                                                    </SELECT>
                                                </TD>
                                                </TR>
                                                <TR>
                                                    <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.businesshours",SessionData.getLanguage())%>:</B></TD>
                                                    <TD NOWRAP CLASS="main">
                                                        <INPUT  TYPE="text" NAME="businessHours" VALUE="<%=Merchant.getBusinessHours()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>">
                                                    </TD>
                                                </TR>

                                                 <tr>
                          <td class="main"><b><%=Languages.getString("jsp.admin.customers.merchants_add.Region",SessionData.getLanguage())%></td>
                                                          <td nowrap class="main">
                                                              <select name="region" TABINDEX="<%=intTabIndex++%>">
               <%
                  Vector vecRegions = com.debisys.customers.Merchant.getRegions(customConfigType);
                  Iterator itRegion = vecRegions.iterator();
                  while (itRegion.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector)itRegion.next();
                    String strRegId = vecTemp.get(0).toString();
                    String strRegName = vecTemp.get(1).toString();
                    if ( (request.getParameter("region") != null) && (strRegId.equals(request.getParameter("region"))) )
                    {
                      out.println("<OPTION VALUE=\"" + strRegId + "\" SELECTED>" + strRegName + "</OPTION>");
                    }
                    else if ( strRegId.equals( (new Integer(Merchant.getRegion())).toString() ) )
                    {
                        out.println("<OPTION VALUE=\"" + strRegId + "\" SELECTED>" + strRegName + "</OPTION>");
                    }
                    else
                    {
                      out.println("<OPTION VALUE=\"" + strRegId + "\">" + strRegName + "</OPTION>");
                    }
                  }
               %>
                                                              </select>
                                                          </td>
                        </tr>
                        <%if (false){ %>
                                <tr>
                          <td class="main"><b><%=Languages.getString("jsp.admin.customers.merchants_add.Invoice_type",SessionData.getLanguage())%></td>
                                      <td nowrap class="main">
                <!-- invoice field refactored DBSY-570 -->
                                <select id="invoice" name="invoice" TABINDEX="<%=intTabIndex++%>">
                                <%
                                  Vector vecInvoiceTypes = Merchant.getInvoiceTypes();
                          Iterator itInvoiceType = vecInvoiceTypes.iterator();
                                  while(itInvoiceType.hasNext()){
                                    Vector vecTemp = null;
                                    vecTemp = (Vector)itInvoiceType.next();
                                    String strInvoiceTypeId = vecTemp.get(0).toString();
                                  String strInvoiceTypeName = vecTemp.get(1).toString();
                                  String strSelected = (strInvoiceTypeId.equals(Integer.toString(Merchant.getInvoice())))?"SELECTED":"";
                                    out.println("<option value=\""+strInvoiceTypeId+"\" "+strSelected+">"+strInvoiceTypeName+"</option>");
                                  }
                                 %>
                                </select>
                      <!-- END -- invoice field refactored DBSY-570 -->
                                       </td>
                        </tr>
                <%}%>

  



                    <!-- New fields DBSY-570 -->
                        
                        <tr>
                          <td class="main"><b><%=Languages.getString("jsp.admin.customers.merchants_add.control_number",SessionData.getLanguage())%>:</b></td>
                          <td class="main" nowrap>
                            <input type="text" name="controlNumber" size="20" value="<%=(Merchant.getControlNumber()==null)?"":Merchant.getControlNumber()%>"
                        TABINDEX="<%=intTabIndex++%>"/>
                          </td>
                        </tr>
                    <!-- END New fields DBSY-570 -->

<%
   } //End of if when deploying in Mexico
%>
                <tr>
                          <td class="main"><b><%=Languages.getString("jsp.admin.customers.merchants_add.merchant_classification",SessionData.getLanguage())%>:</b></td>
                          <td class="main" nowrap>
                            <select id="merchantClassification" name="merchantClassification" onchange="merchantClassificationCheck()" TABINDEX="<%=intTabIndex++%>">
                            <option value="0"></option>
                            <%
                              Vector vecMerchantClassifications = Merchant.getMerchantClassifications(SessionData);
                      Iterator itMerchantClassifications = vecMerchantClassifications.iterator();
                              while(itMerchantClassifications.hasNext()){
                                Vector vecTemp = null;
                                vecTemp = (Vector)itMerchantClassifications.next();
                                String strClassifId = vecTemp.get(0).toString();
                              String strClassifName = vecTemp.get(1).toString();
                              String strSelected = (strClassifId.equals(Integer.toString(Merchant.getMerchantClassification())))?"SELECTED":"";
                                out.println("<option value=\""+strClassifId+"\" "+strSelected+">"+strClassifName+"</option>");

                              }
                             %>
                            </select>
            <div style="<%=(Merchant.getMerchantClassificationOtherDesc()==null || Merchant.getMerchantClassificationOtherDesc().trim().equals(""))?"display:none":"display:inline"%>" id="merchantClassificationOtherDescDiv">
                <b id="merchantClassificationOtherDescB"><%=Languages.getString("jsp.admin.customers.merchants_edit.merchantClassificationOtherDesc",SessionData.getLanguage())%></b>
                <input type="text" id="merchantClassificationOtherDesc" name="merchantClassificationOtherDesc" size="30" value="<%=(Merchant.getMerchantClassificationOtherDesc()==null)?"":Merchant.getMerchantClassificationOtherDesc()%>"
                       style="<%=(Merchant.getMerchantClassificationOtherDesc()==null || Merchant.getMerchantClassificationOtherDesc().trim().equals(""))?"display:none":"display:inline"%>"
                TABINDEX="<%=intTabIndex++%>" />
            </div>
                          </td>
                          
                        </tr>
                        
                <TR>
                  <TD CLASS="main">
                    <B><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZone",SessionData.getLanguage())%>:</B>
                  </TD>
                  <TD NOWRAP CLASS="main">
                    <SELECT NAME="timeZoneId">
<%
                    String sDefaultTimeZone = Merchant.getTimeZoneId() == 0?null:Integer.toString(Merchant.getTimeZoneId());
                    if ( sDefaultTimeZone == null ) { sDefaultTimeZone = TimeZone.getDefaultTimeZone().get(0).toString(); }
                    Iterator<Vector<String>> itList = TimeZone.getTimeZoneList().iterator() ;
                    while ( itList.hasNext() )
                    {
                      Vector vItem = itList.next();
                      String sSelected = "";
                      if (vItem.get(0).toString().equals(sDefaultTimeZone))
                      {
                        sSelected = "SELECTED";
                      }
%>
                      <OPTION VALUE="<%=vItem.get(0).toString()%>" <%=sSelected%>><%=vItem.get(2).toString()%> [<%=vItem.get(1).toString()%>]</OPTION>
<%
                      vItem.clear();
                      vItem = null;
                    }
                    itList = null;
%>
                    </SELECT>
                  </TD>
                </TR>
                <TR>
                  <TD CLASS="main">&nbsp;</TD>
                  <TD CLASS="main" id="timeZoneWarning">
                    <%= Languages.getString("jsp.admin.customers.merchants_edit.timeZoneWarning",SessionData.getLanguage())%>
                  </TD>
                </TR>


        <%
        boolean hasPermissionManage_New_Rateplans = SessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS);
        if (hasPermissionManage_New_Rateplans)
        {
                                    String defaultNewRatePlans = "";
                                    if (isDomestic){
                                        defaultNewRatePlans="checked";
                                    }
                                
        %>
            <TR>
                <TD CLASS="main">
                  <B><%= Languages.getString("jsp.admin.customers.merchants_info.rateplan",SessionData.getLanguage())%>:</B>
                </TD>
                <TD NOWRAP CLASS="main">
                                                                    <input id="useRatePlanModel" name="useRatePlanModel" type="checkbox" checked="checked" <%=defaultNewRatePlans%>    />

                </TD>
            </TR>
        <%
        }
        %>



<%
  // DBSY-931 System 2000 Tools s2k
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If when deploying in International and user is an ISO level
    String strRefId = SessionData.getProperty("ref_id");
%>
<tr>
<TD CLASS="main">
<B><%= Languages.getString("jsp.admin.tools.s2k.addedit.terms",SessionData.getLanguage())%>:</B>
</TD>
 <TD NOWRAP CLASS="main">
 <INPUT  TYPE="text" NAME="terms" VALUE="<%=Merchant.getterms()%>" SIZE="6" MAXLENGTH="4">
</TD>
</tr>
<tr>
<TD CLASS="main">
<B><%= Languages.getString("jsp.admin.customers.salesmanid",SessionData.getLanguage())%>:</B>
                  </TD>
                  <TD NOWRAP CLASS="main">

<%
          Vector salesman = new s2k().gets2ksalesman(SessionData.getProperty("iso_id"));
%>
  <select name="salesmanid" id ="salesmanid" onchange="loadid('<%=strRefId%>');" tabIndex="<%= intTabIndex++ %>" >
                             <%
                                out.println("<OPTION VALUE=\"\" >" + "</OPTION>");
                               for ( int v=0 ; v<salesman.size();v++){
                                String temp = ((Vector)salesman.get(v)).get(0).toString();
                                String selected="";
                                   if ( temp.equals(request.getParameter("salesmanid")) )
                                    selected ="SELECTED";

                out.println("<OPTION VALUE=\"" + temp
                + "\" "+ selected +" >"
                 +  temp  + "</OPTION>");
                 }
                  %>
                              </select>

<B>     <%= Languages.getString("jsp.admin.customers.salesmanname",SessionData.getLanguage())%>:</B>
     <select name="salesmanname"  id="salesmanname"  onchange="loadname('<%=strRefId%>');" tabIndex="<%= intTabIndex++ %>" >
                             <%
                          out.println("<OPTION VALUE=\"\" >" + "</OPTION>");
                                for ( int v=0 ; v<salesman.size();v++){
                                String temp = ((Vector)salesman.get(v)).get(1).toString();
                                 String selected="";
                                   if ( temp.equals(request.getParameter("salesmanname")) )
                                    selected ="SELECTED";

                out.println("<OPTION VALUE=\"" + temp
                + "\" "+ selected +" >"
                 +  temp  + "</OPTION>");
                 }
 %>
                              </select>
                                  </td>
                              </tr>
          <%}

  // DBSY-1072 eAccounts Interface
  if(SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
%>
                        <tr>
                          <td class="main">
                            <b><%= Languages.getString("jsp.admin.customers.entity_account_type",SessionData.getLanguage())%>:</b>
                          </td>
<%
    Hashtable<Integer, String> ht_accountTypes = EntityAccountTypes.getAccountTypes();
%>
                          <td>
                          <input type="hidden" id="disabledentityAccountType" name="disabledentityAccountType" value="" />
                            <select id="entityAccountType" name="entityAccountType">
<%
      for (Enumeration<Integer> e = ht_accountTypes.keys(); e.hasMoreElements();) {
        int type_id = e.nextElement();
        out.println("<option value=\"" + type_id + "\" " + (type_id == 1?"selected":"") + ">" + ht_accountTypes.get(type_id) + "</option>");
      }
%>
                            </select>
                          </td>
                        </tr>
<%
  }
        if(bExternalRep && !strAccessLevel.equals(DebisysConstants.REP))
        {
%>
                <tr>
                          <td class="main">
                            <b><%= Languages.getString("jsp.admin.customers.allowExternalReps",SessionData.getLanguage())%>:</b>
                          </td>
                          <td>
                          <input type="checkbox" name="allowExternalReps" value="Yes" <%=(Merchant.getMerchantPermissions() == 1)?"checked":""%> /> <br />

                          </td>
                </tr>

         <%}
 if(bMerchantAssignment && ( strAccessLevel.equals(DebisysConstants.ISO) ||  strAccessLevel.equals(DebisysConstants.AGENT))){ %>
  <tr>
                          <td class="main">
                            <b><%= Languages.getString("jsp.admin.customers.allowMerchantAssignment",SessionData.getLanguage())%>:</b>
                          </td>
                          <td>
                          <input type="checkbox" name="allowMerchantAssignment" value="Yes" <%=(Merchant.getMerchantExternalOutsideHierarchy() == 1)?"checked":""%> /> <br />

                          </td>
                </tr>

<%}%>
                      </table>
                    </td>
                </tr>
                
                
                
 <%
 if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ) { //If when deploying in Mexico
  
%>
    <tr>
      <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add.Invoice_title",SessionData.getLanguage())%></td>
    </tr>
    <tr>
      <td>
        <jsp:include page="merchants/invoicing.jsp">            
          <jsp:param value="<%=customConfigType%>" name="customConfigType"/>
        </jsp:include>
      </td>
    </tr>
                      
<%}%> 

                <tr>
                  <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_edit.address",SessionData.getLanguage())%></td>
                </tr>
        <%
        if ( permLocalizeMerchant )
        {   
            HashMap parametersName = new HashMap();
            String sessionLanguage = SessionData.getUser().getLanguageCode();
            String viewMerchantMapping = "viewMerchantMapping";
            ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport(ResourceReport.RESOURCE_MERCHANT_MAP_LOCATOR, sessionLanguage, viewMerchantMapping);
                    
            parametersName.put(viewMerchantMapping, "viewMerchantMapping");
            for( ResourceReport resource : resources)
            {
                if ( parametersName.containsKey( resource.getKeyMessage() ) )
                {
                    parametersName.put( resource.getKeyMessage(), resource.getMessage());            
                }        
            }             
        %>
            <tr align="center">
                <td colspan="2" align="left">
                    <input id="btnViewMerchantMapping" type="button" onclick="loadSetupMap();" value="<%=parametersName.get(viewMerchantMapping)%>">                                            
                </td>
            </tr>                                            
        <%
        }
        %>
                <tr>
                    <td class="formArea2">
                      <table width="100">                                                                                            
                      <tr class="main">
                          <td><b><%=Languages.getString("jsp.admin.customers.merchants_edit.street_address",SessionData.getLanguage())%>:</b></td>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
        <TD COLSPAN="4" NOWRAP><textarea id="physAddress" name="physAddress" rows="2" cols="50" maxlength="50" tabindex="<%=intTabIndex++%>" ><%=Merchant.getPhysAddress()%></textarea><%if (merchantErrors != null && merchantErrors.containsKey("physAddress")) out.print("<font color=ff0000>*</font>");%></TD>
<%
  } else { //Else if when deploying in others
%>
                          <td nowrap><input type="text" id="physAddress" name="physAddress" value="<%=Merchant.getPhysAddress()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("physAddress")) out.print("<font color=ff0000>*</font>");%></td>
<%
  }
%>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                    </TR>
                                                    <TR CLASS="main">
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage())%>:</TD>
                          <TD NOWRAP><INPUT TYPE="text" id="physColony" NAME="physColony" VALUE="<%=Merchant.getPhysColony()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage())%>:</TD>
                          <TD NOWRAP><INPUT TYPE="text" id="physDelegation" NAME="physDelegation" VALUE="<%=Merchant.getPhysDelegation()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
<%
  } //End of if when deploying in Mexico
%>
                                                    </TR>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                    <TR CLASS="main">
                        <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())%>:</TD>
                        <TD NOWRAP><INPUT TYPE="text" id="physCity" NAME="physCity" VALUE="<%=Merchant.getPhysCity()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("physCity")) out.print("<font color=ff0000>*</font>");%></TD>
                        <TD>&nbsp;</TD>
                        <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage())%></TD>
                        <TD NOWRAP><INPUT TYPE="text" id="physZip" name="physZip" VALUE="<%=Merchant.getPhysZip()%>" SIZE="20" MAXLENGTH="5" TABINDEX="<%=intTabIndex++%>" 
                                                    ONBLUR="ValidateRegExp(this, '[0-9]{5}');">
                            <%if (merchantErrors != null && merchantErrors.containsKey("physZip")) out.print("<font color=ff0000>*</font>");%></TD>
                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><B><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())%></B></TD>
                                                        <TD NOWRAP>
                                                            <SELECT id="physState" NAME="physState" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
  Iterator itStates = vecStates.iterator();
  while (itStates.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itStates.next();
    String strStateId = vecTemp.get(0).toString();
    String strStateName = vecTemp.get(1).toString();
    if ( strStateId.equals(request.getParameter("physState")) )
      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
  }
%>
                                                            </SELECT>
                                                        </TD>
                                                    </TR>
<%
  } //End of if when deploying in Mexico
  else { //Else other deployments
%>
                                                    <TR CLASS="main">
                          <TD NOWRAP>
<%
  if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
    out.println("<b>"+Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())+":</b>");
  else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
    out.println(Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())+":");
%>
                          </TD>
                <TD NOWRAP>

                        <%                           
                        if (!isInternational){
                        %>
                        <INPUT TYPE="text" id="physCity" NAME="physCity" VALUE="<%=Merchant.getPhysCity()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("physCity")) out.print("<font color=ff0000>*</font>");%>                            

                        <%
                            } else{
                                StringBuilder optCities = new StringBuilder();
                                optCities.append("<option>"+Merchant.getPhysCity()+"</option>");
                        %>
                            <select id="physCity" name="physCity"  >
                                <%=optCities.toString()%>
                            </select>                            
                            
                        <%
                        }
                        %>




                    
                </TD>
                <TD>&nbsp;</TD>
                <TD NOWRAP>
<%
    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
    {
        out.println("<b>"+Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())+":</b>");
%>
    </TD>
    <TD NOWRAP>
      <SELECT id="physState" name="physState" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
  Iterator itStates = vecStates.iterator();
  while (itStates.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itStates.next();
    String strStateId = vecTemp.get(2).toString();
    String strStateName = vecTemp.get(2).toString();
    if ( strStateId.equals(request.getParameter("physState")) )
      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
  }
%>
      </SELECT>
    </TD>
    <TD>&nbsp;</TD>
    <TD NOWRAP>
<%
  }//END Release 16.5 Add by YH
  else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
  {
    out.println(Languages.getString("jsp.admin.customers.merchants_edit.state_international",SessionData.getLanguage())+":");
%>
    </TD>
    <TD NOWRAP>
      <INPUT TYPE="text" id="physState" name="physState" value="<%=Merchant.getPhysState()%>" size="3" maxlength="2" TABINDEX="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("physState")) out.print("<font color=ff0000>*</font>");%>
    </TD>
    <TD>&nbsp;</TD>
    <TD NOWRAP>
<%
  }
  if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
    out.println("<b>"+Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic",SessionData.getLanguage())+":</b>");
  else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
    out.println(Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage()) + ":");
%>
                          </TD>
                                                        <TD NOWRAP>
                                                            <INPUT TYPE="text" id="physZip" NAME="physZip" VALUE="<%=Merchant.getPhysZip()%>" SIZE="7" MAXLENGTH="10" TABINDEX="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("physZip")) out.print("<font color=ff0000>*</font>");%>
                                                        </TD>
                                                    </TR>
<%
  } //End of else other deployments
%>
            <tr class="main">
                <td><%=Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage())%>:</td>
                <td nowrap>

<%   
                        
                        if (!isInternational){
                        %>
                            <input id="physCounty" type="text" name="physCounty" value="<%=Merchant.getPhysCounty()%>" size="20" maxlength="20" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("physCounty")) out.print("<font color=ff0000>*</font>");%>
                        <%
                            } else{                                
                                StringBuilder optCounties = new StringBuilder();
                                for(District district : districts){                                    
                                    optCounties.append("<option value='"+district.getName()+"' >"+district.getName()+"</option>");
                                }
                                
                                List<City> cities = City.findAllCities(isoID);
                                StringBuilder tmpCities = new StringBuilder();
                                tmpCities.append("let citiesArray = [");
                                for(City city : cities){
                                    String ci = "{ name: '"+city.getName()+"',  districtName: '"+city.getDistrictName()+"' }";
                                    tmpCities.append(ci+ ",");
                                }
                                String cc = tmpCities.toString().substring(0, tmpCities.toString().length()-1);
                                cc = cc + "];";
                                
                        %>
                            <select id="physCounty" name="physCounty" onchange="populateCities();"  >
                                <%=optCounties.toString()%>
                            </select>                            
                            <script language="JavaScript" type="text/javascript" charset="utf-8">
                                <%=cc%>
                                function populateCities(){                                    
                                    var districtName = $( "#physCounty option:selected" ).val();    
                                    //$("#physCounty").val(districtName);
                                    var selectedCities = citiesArray.filter(x => x.districtName === districtName);
                                    var options = $("#physCity");
                                    options.empty();                                    
                                    $.each(selectedCities, function(i, item) {                                        
                                        options.append($("<option  />").val(item.name).text(item.name));
                                    });                                       
                                }
                                $(document).ready(function() {
                                    populateCities();                                                                       
                                });
                            </script>                                
                        <%
                        }
                        %>
                        </td>


                
            </tr>
                          <tr class="main">
                          <td><%
                           if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                           {
                             out.println("<b>");
                           }
                           %><%=Languages.getString("jsp.admin.customers.merchants_edit.country",SessionData.getLanguage())%>:</td>
                          <td nowrap>

<%
            String[][] countries = CountryInfo.getAllCountries(CountryInfo.getIso(SessionData.getProperty("ref_id"),SessionData.getProperty("ref_type")));
            if(countries[0][0].length() > 0 ){
          %>

  <select name="physCountry" id="physCountry" tabIndex="<%= intTabIndex++ %>" >
                             <%
                             if(countries.length == 1){
                                out.println("<OPTION VALUE=\"" + countries[0][0].toString() + "\" SELECTED>" + countries[0][1].toString()  + "</OPTION>");
                             }
                             else{
                              out.println("<OPTION VALUE=\"\" SELECTED>" + "" + "</OPTION>");
                               for ( int v=0 ; v<countries.length;v++)
                out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\">" + countries[v][1].toString()  + "</OPTION>");
                             }
 %>
                              </select>
<%
                                   }
                                   else
                                   {
                                      out.print("<font color=ff0000>Database Error Country must be set for ISO</font>");
                                   }
                                    if (merchantErrors != null && merchantErrors.containsKey("physCountry"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td></tr>
              <%
                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                {
                  out.println("<tr class=\"main\">");
                  out.println("<td>" + Languages.getString("jsp.admin.customers.merchants_edit.route",SessionData.getLanguage()) + ":</td>");
                  out.println("<td nowrap>");

                  out.println("<SELECT ID=\"merchantRouteId\" NAME=\"merchantRouteId\" TABINDEX=\"" + intTabIndex++ + "\">");
                  out.println("<OPTION VALUE=\"-1\" SELECTED> </OPTION>");
                  Vector vecMerchantRoutes = com.debisys.customers.Merchant.getRoutes();
                  Iterator itMerchantRoutes = vecMerchantRoutes.iterator();
                  while (itMerchantRoutes.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector)itMerchantRoutes.next();
                    String strMerchantRouteId = vecTemp.get(0).toString();
                    String strMerchantRouteName = vecTemp.get(1).toString();
                    if (strMerchantRouteId.equals(request.getParameter("merchantRouteId")))
                      out.println("<OPTION VALUE=\"" + strMerchantRouteId + "\" SELECTED>" + strMerchantRouteName + "</OPTION>");
                    else
                      out.println("<OPTION VALUE=\"" + strMerchantRouteId + "\">" + strMerchantRouteName + "</OPTION>");
                  }
                  out.println("</SELECT>");

                  out.println("</td>");
                  out.println("</tr>");
                }
                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                {
                %>
                <tr class="main">
                  <td><%=Languages.getString("jsp.admin.customers.merchants_edit.latitude",SessionData.getLanguage())%>:</td>
                  <td><input name="latitude" type="hidden" value="<%=Merchant.getLatitude()%>"><span name="latvalue"><%=Merchant.getLatitude()%></span></td>
                  <td><%=Languages.getString("jsp.admin.customers.merchants_edit.longitude",SessionData.getLanguage())%>:</td>
                  <td><input name="longitude" type="hidden" value="<%=Merchant.getLongitude()%>"><span name="longvalue"><%=Merchant.getLongitude()%></span></td>
                </tr>
                <%
                }
                                                                if ( permLocalizeMerchant ){
                                                                %>    
                                                                    <input id="latitude" name="latitude" type="hidden" value="<%=Merchant.getLatitude()%>" />
                                                                    <input id="longitude" name="longitude" type="hidden" value="<%=Merchant.getLongitude()%>" />
                                                                <%  
                                                                }
                                                                %>                                                               
                                                    </table>
                    </td>
                </tr>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
            <TR>
                  <TD CLASS="formAreaTitle2">
                      <BR>
                  <%=Languages.getString("jsp.admin.customers.merchants_edit.billingaddress",SessionData.getLanguage())%>&nbsp;
                  </TD>
                </TR>
                <TR>
                  <TD nowrap CLASS="main">
                                                    <b><INPUT TYPE="checkbox" ID="chkBillingAddress" NAME="chkBillingAddress" <%=((request.getParameter("chkBillingAddress") != null)?"CHECKED":"")%> ONCLICK="EnableBillingAddress(this.checked);"><LABEL FOR="chkBillingAddress"><%=Languages.getString("jsp.admin.customers.merchants_edit.billingaddresstitle",SessionData.getLanguage())%></LABEL>
                                                    </td>
                  </tr>
                                                   <TR>
                                                                <td nowrap class="main">
                                                              <% if ( Merchant.getPrintBillingAddress() )
                                                                 {%>
                                                                      <b><input type="checkbox" name="printBillingAddress" checked>
                                                               <%}
                                                                 else
                                                                 {
                                                                 %>
                                                                      <b><input type="checkbox" name="printBillingAddress">
                                                               <%}%>
                                                               <%=Languages.getString("jsp.admin.customers.merchants_add.printBillingAddress",SessionData.getLanguage())%>
                                                         </td>
                                                     </tr>


              <tr>
                                            <td class="formArea2">
                                                <table width="100">
                                                    <tr class="main">
                                                        <td><b><%=Languages.getString("jsp.admin.customers.merchants_edit.street_address",SessionData.getLanguage())%>:</b></td>
                          <TD COLSPAN="4" NOWRAP><textarea id="mailAddress" name="mailAddress" rows="2" cols="50" maxlength="50" tabindex="<%=intTabIndex++%>" ><%=Merchant.getMailAddress()%></textarea><%if (merchantErrors != null && merchantErrors.containsKey("billingAddress")) out.print("<font color=ff0000>*</font>");%></TD>
                                                    </TR>
                                                    <TR CLASS="main">
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage())%>:</TD>
                          <TD NOWRAP><INPUT TYPE="text" ID="mailColony" NAME="mailColony" VALUE="<%=Merchant.getMailColony()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage())%>:</TD>
                          <TD NOWRAP><INPUT TYPE="text" ID="mailDelegation" NAME="mailDelegation" VALUE="<%=Merchant.getMailDelegation()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                    </TR>
                                                    <TR CLASS="main">
                          <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())%>:</TD>
                                                        <TD NOWRAP><INPUT TYPE="text" ID="mailCity" NAME="mailCity" VALUE="<%=Merchant.getMailCity()%>" SIZE="20" MAXLENGTH="30" TABINDEX="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("billingCity")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage())%>:</TD>
                        <TD NOWRAP><INPUT TYPE="text" ID="mailZip" NAME="mailZip" VALUE="<%=Merchant.getMailZip()%>" SIZE="20" MAXLENGTH="5" TABINDEX="<%=intTabIndex++%>" 
                            ONBLUR="ValidateRegExp(this, '[0-9]{5}');"><%if (merchantErrors != null && merchantErrors.containsKey("billingZip")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><B><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())%></B></TD>
                                                        <TD NOWRAP>
                                                            <SELECT ID="mailState" NAME="mailState" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
  Iterator itStates = vecStates.iterator();
  while (itStates.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itStates.next();
    String strStateId = vecTemp.get(0).toString();
    String strStateName = vecTemp.get(1).toString();
    if (strStateId.equals(request.getParameter("mailState")))
      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
  }
%>
                                                            </SELECT>
                                                        </TD>
                                                    </TR>
                          <tr class="main">
                          <td><%=Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage())%>:</td>
                          <td nowrap><input type="text" id="mailCounty" name="mailCounty" value="<%=Merchant.getMailCounty()%>" size="20" maxlength="20" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("billingCounty")) out.print("<font color=ff0000>*</font>");%></td>
                          </tr>
                          <tr class="main">
                          <td><B><%=Languages.getString("jsp.admin.customers.merchants_edit.country",SessionData.getLanguage())%>:</B></td>
                          <td nowrap>

<%
          if(countries[0][0].length() > 0 ){
          %>


  <select id="mailCountry" name="mailCountry" tabIndex="<%= intTabIndex++ %>" >
                             <%
                             if(countries.length == 1){
                                out.println("<OPTION VALUE=\"" + countries[0][0].toString() + "\" SELECTED>" + countries[0][1].toString()  + "</OPTION>");
                             }
                             else{
                              out.println("<OPTION VALUE=\"\" SELECTED>" + "" + "</OPTION>");
                               for ( int v=0 ; v<countries.length;v++)
                out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\">" + countries[v][1].toString()  + "</OPTION>");
                             }
 %>
                              </select>
<%
                                   }
                                   else
                                   {
                                      out.print("<font color=ff0000>Database Error Country must be set for ISO</font>");
                                   }
                                    if (merchantErrors != null && merchantErrors.containsKey("billingCountry"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td></tr>

                      </table>



                    </td>
                </tr>

<%
   if ( request.getParameter("chkBillingAddress") != null )
       out.print("<SCRIPT>EnableBillingAddress(true);</SCRIPT>");
   else
       out.print("<SCRIPT>EnableBillingAddress(false);</SCRIPT>");
  } //End of if when deploying in Mexico
%>
                <tr>
                  <td class="formAreaTitle2">
                    <span id="span_LabelcontactData" name="span_LabelcontactData"><%=Languages.getString("jsp.admin.customers.merchants_edit.contact_information",SessionData.getLanguage())%>
                    </span>
                    <input type="hidden" id="contactDataH" name="contactDataH"></input>
                  </td>
                  
                </tr>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  {//If when deploying in Mexico
%>
                                        <TR>
                                            <TD CLASS="formArea2">
                                                <INPUT TYPE="hidden" ID="mxContactAction" NAME="mxContactAction" VALUE="">
                                                <TABLE WIDTH="100%" CELLSPACING="1" CELLPADDING="1" BORDER="0">
                                                    <TR>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contacttype",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contact",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.phone",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.fax",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.email",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.cellphone",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.department",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"></TD>
                                                    </TR>
<%
    if ( Merchant.getVecContacts() != null && Merchant.getVecContacts().size() > 0 )
    {//If there are contacts to show
%>
                                                    <TR>
                                                      <TD>
                                                        <INPUT TYPE="hidden" ID="mxContactIndex" NAME="mxContactIndex" VALUE="">
                                                        <DIV STYLE="display:none;">
                                                            <input type="text" id="contactData" name="contactData" value="<%=Merchant.getVecContacts().size()%>"></input>
                                                        </DIV>
                                                        <SCRIPT>
                                                            function mxEditContact()
                                                            {
                                                              document.getElementById('mxContactAction').value = "EDIT";
                                                              document.formMerchant.onsubmit = new Function("return true;");
                                                              document.formMerchant.Buttonsubmit.disabled = true;
                                                            }
                                                            function mxDeleteContact()
                                                            {
                                                              document.getElementById('mxContactAction').value = "DELETE";
                                                              document.formMerchant.onsubmit = new Function("return true;");
                                                              document.formMerchant.Buttonsubmit.disabled = true;
                                                            }
                                                        </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
      int intEvenOdd = 1;
      int contactIndex = 0;
      Vector vecContacts = Merchant.getVecContacts();
      Iterator itContacts = vecContacts.iterator();
      while (itContacts.hasNext())
      {
        Vector vecTemp = null;
        vecTemp = (Vector)itContacts.next();
%>
                                                    <TR CLASS="row<%=intEvenOdd%>">
<%
        if ( (request.getParameter("mxContactAction") != null) && 
            (request.getParameter("mxContactAction").equals("EDIT")) && 
              (request.getParameter("mxContactIndex") != null) && 
                (contactIndex == Integer.parseInt(request.getParameter("mxContactIndex"))) )
        {//If we are editing an item
%>
                                                        <TD>
                                                          <SCRIPT>document.getElementById("mxContactIndex").value = "<%=contactIndex%>";</SCRIPT>
                                                          <SELECT NAME="contactTypeId" id="contactTypeId" TABINDEX="<%=intTabIndex++%>">
<%
          Vector vecContactTypes = com.debisys.customers.Merchant.getContactTypes();
          Iterator itContactTypes = vecContactTypes.iterator();
          while (itContactTypes.hasNext())
          {
            vecTemp = null;
            vecTemp = (Vector)itContactTypes.next();
            String strContactTypeId = vecTemp.get(0).toString();
            String strContactTypeCode = vecTemp.get(1).toString();
            if (strContactTypeId.equals(Integer.toString(Merchant.getContactTypeId())))
            {
              out.println("<OPTION VALUE=\"" + strContactTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
            }
            else
            {
              out.println("<OPTION VALUE=\"" + strContactTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
            }
          }
%>
                                                          </SELECT>
                                                        </TD>
                                                        <TD><INPUT TYPE="text" NAME="contactName" id="contactName" VALUE="<%=Merchant.getContactName()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactPhone" id="contactPhone" VALUE="<%=Merchant.getContactPhone()%>" SIZE="15" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactFax" id="contactFax" VALUE="<%=Merchant.getContactFax()%>" SIZE="15" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactEmail" id="contactEmail" VALUE="<%=Merchant.getContactEmail()%>" SIZE="15" MAXLENGTH="200" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactCellPhone" id="contactCellPhone" VALUE="<%=Merchant.getContactCellPhone()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactDepartment" id=contactDepartment VALUE="<%=Merchant.getContactDepartment()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
<%
        }//End of if we are editing an item
        else
        {//Else just show the item data
%>
                                                        <TD><%=com.debisys.customers.Merchant.getContactTypeById(vecTemp.get(0).toString(),SessionData)%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(1).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(2).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(3).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(4).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(5).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(6).toString())%></TD>
                                                        <TD>
                                                            <TABLE>
                                                                <TR>
                                                                    <TD>
                                                                      <INPUT TYPE="image" TITLE="<%=Languages.getString("jsp.admin.customers.merchants_edit.editcontact",SessionData.getLanguage())%>" SRC="images/icon_edit.gif" ONCLICK="document.getElementById('mxContactIndex').value = '<%=contactIndex%>';mxEditContact();">
                                                                    </TD>
                                                                    <TD>&nbsp;</TD>
                                                                    <TD>
                                                                      <INPUT TYPE="image" TITLE="<%=Languages.getString("jsp.admin.customers.merchants_edit.deletecontact",SessionData.getLanguage())%>" SRC="images/icon_delete.gif" ONCLICK="document.getElementById('mxContactIndex').value = '<%=contactIndex%>';mxDeleteContact();">
                                                                    </TD>
                                                                </TR>
                                                            </TABLE>
                                                        </TD>
<%
        }//End of else just show the item data
%>
                                                    </TR>
<%
        intEvenOdd = ((intEvenOdd == 1)?2:1);
        contactIndex++;
      }
    }//End of if there are contacts to show
    else if ( (request.getParameter("mxContactAction") == null) ||
              (request.getParameter("mxContactAction") != null) && !request.getParameter("mxContactAction").equals("ADD")
            )
    {//Else show a message of no contacts
%>
                                                    <TR><TD CLASS="main" COLSPAN="8" ALIGN="center"><%=Languages.getString("jsp.admin.customers.merchants_edit.nocontacts",SessionData.getLanguage())%></TD></TR>
                                                    <TR><TD CLASS="main" COLSPAN="8" ALIGN="center"><DIV STYLE="display:none;"><INPUT TYPE="text" NAME="contactData" VALUE=""></DIV></TD></TR>
<%
    }//End of else show a message of no contacts

    if ( (request.getParameter("mxContactAction") == null) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("SAVE"))) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("CANCEL"))) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("DELETE"))) 
          //|| request.getParameter("invoicingAction") != null
          || request.getParameter("mxContactAction").equals("--")
       )
    {//If we need to show the ADD button
%>
                                                    <TR>
                                                      <TD CLASS="main" COLSPAN="8" ALIGN="right">
                                                        <INPUT TYPE="button" ID="mxBtnAddContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.addcontact",SessionData.getLanguage())%>" ONCLICK="mxAddContact();">
                                                        <SCRIPT>
                                                            function mxAddContact()
                                                            {
                                                              document.getElementById('mxContactAction').value = "ADD";
                                                              document.getElementById('mxBtnAddContact').disabled = true;
                                                              $('#formMerchant').submit();
                                                              //document.formMerchant.onsubmit = new Function("return true;");
                                                              //document.formMerchant.Buttonsubmit.click();
                                                              //document.formMerchant.Buttonsubmit.disabled = true;
                                                            }
                                                        </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
    }//End of if we need to show the ADD button
    else if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("ADD")) )
    {//Else if we need to show the row for a new Contact
%>
                                                    <TR CLASS="main">
                                                        <TD>
                                                          <DIV STYLE="display:none;"><INPUT TYPE="text" id="contactData" NAME="contactData" VALUE="<%=((Merchant.getVecContacts().size() == 0)?"":"1")%>"></DIV>
                                                          <SELECT NAME="contactTypeId" id="contactTypeId" TABINDEX="<%=intTabIndex++%>">
<%
      Vector vecContactTypes = com.debisys.customers.Merchant.getContactTypes();
      Iterator itContactTypes = vecContactTypes.iterator();
      while (itContactTypes.hasNext())
      {
        Vector vecTemp = null;
        vecTemp = (Vector)itContactTypes.next();
        String strContactTypeId = vecTemp.get(0).toString();
        String strContactTypeCode = vecTemp.get(1).toString();
        if (strContactTypeId.equals(request.getParameter("contactTypeId")))
        {
          out.println("<OPTION VALUE=\"" + strContactTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
        }
        else
        {
          out.println("<OPTION VALUE=\"" + strContactTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
        }
      }
%>
                                                          </SELECT>
                                                        </TD>
                                                        <TD><INPUT TYPE="text" NAME="contactName" id="contactName" VALUE="<%=Merchant.getContactName()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactPhone" id="contactPhone" VALUE="<%=Merchant.getContactPhone()%>" SIZE="15" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactFax" id="contactFax" VALUE="<%=Merchant.getContactFax()%>" SIZE="15" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactEmail" id="contactEmail" VALUE="<%=Merchant.getContactEmail()%>" SIZE="15" MAXLENGTH="200" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactCellPhone" id="contactCellPhone" VALUE="<%=Merchant.getContactCellPhone()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactDepartment" id="contactDepartment" VALUE="<%=Merchant.getContactDepartment()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                    </TR>
<%
    }//End of else if we need to show the row for a new Contact

    if ( ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("ADD"))) ||
         ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("EDIT")))
       )
    {//If we need to show the SAVE-CANCEL button
%>
                                                    <TR>
                                                      <TD CLASS="main" COLSPAN="8" ALIGN="right">
                                                        <INPUT TYPE="button" ID="mxBtnSaveContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.savecontact",SessionData.getLanguage())%>" ONCLICK="mxSaveContact();">
                                                        <INPUT TYPE="button" ID="mxBtnCancelContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.cancelsavecontact",SessionData.getLanguage())%>" ONCLICK="mxCancelSaveContact();">
    <SCRIPT>
        function mxSaveContact()
        {
          error = false;
          form = document.formMerchant;
          error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";
          check_input("contactName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact",SessionData.getLanguage())%>");
          check_input("contactPhone", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_phone",SessionData.getLanguage())%>");
          if ( error )
          {
            showMessagesValidations();
            showValidationMessages(true); 
            //alert(error_message);
            return false;
          }

          document.getElementById('mxContactAction').value = "SAVE";
          document.getElementById('mxBtnSaveContact').disabled = true;
          $('#formMerchant').submit();
          //document.formMerchant.onsubmit = new Function("return true;");
          //document.formMerchant.Buttonsubmit.click();
          //document.formMerchant.Buttonsubmit.disabled = true;
        }

        function mxCancelSaveContact()
        {
          document.getElementById('mxContactAction').value = "CANCEL";
          document.getElementById('mxBtnSaveContact').disabled = true;
          document.getElementById('mxBtnCancelContact').disabled = true;
          $('#formMerchant').submit();
          //document.formMerchant.onsubmit = new Function("return true;");
          //document.formMerchant.Buttonsubmit.click();
          //document.formMerchant.Buttonsubmit.disabled = true;
        }
    </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
    }//End of if we need to show the SAVE-CANCEL button
%>
                                                </TABLE>
                                            </TD>
                                        </TR>

                      <tr>
                          <td class="formArea2">
                            <table width="100">
                          <tr class="main" nowrap>
                            <td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="smsNotificationPhone" id="smsNotificationPhone" value="<%=Merchant.getSmsNotificationPhone()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
                          </tr>
                          <tr class="main" nowrap>
                            <td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="mailNotificationAddress" id="mailNotificationAddress" value="<%=Merchant.getMailNotificationAddress()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
                          </tr>                       
                          <tr>
                            <td class="main" nowrap>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
                            </td>
                            <td nowrap class="main">
                              <input id="paymentNotifications" name="paymentNotifications" type="checkbox" onClick="changePayNoti()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap>
                              <%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" onClick="changePayNotiSMS()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" onClick="changePayNotiMail()" />
                            </td>
                          </tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
                          <tr>
                            <td class="main" nowrap>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
                            </td>
                            <td>
                              <input id="balanceNotifications" name="balanceNotifications" type="checkbox" onClick="changeBalNoti()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap>
                              <%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
                            </td>
                          </tr> 
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" onClick="changeBalNotiValue()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" onClick="changeBalNotiDays()" />
                            </td>
                          </tr>                         
                          <tr>
                            <td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationValue" id="balanceNotificationValue" value="<%=Merchant.getBalanceNotificationValue()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
                          </tr>
                          <tr>
                            <td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationDays" id="balanceNotificationDays" value="<%=Merchant.getBalanceNotificationDays()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
                          </tr>                         
                          <tr>
                            <td class="main" nowrap>
                              <%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" onClick="changeBalNotiSMS()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" onClick="changeBalNotiMail()" />
                            </td>
                          </tr>
<%
   }
%>    
                        </table>
                        <script>changePayNoti();</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
        <script>changeBalNoti()</script>
<%
   }
%>                        
                      </td>
                    </tr>
<%
  }
  else
  {//Else when deploying in others
%>
                <tr>
                    <td class="formArea2">
                      <table width="100">
                          <tr class="main">
                          <td nowrap><b><%=Languages.getString("jsp.admin.customers.merchants_edit.contact",SessionData.getLanguage())%>:</td><td nowrap><input type="text" id="contactName" name="contactName" value="<%=Merchant.getContactName()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("contactName")) out.print("<font color=ff0000>*</font>");%></td>
                          </tr>
                          <tr class="main">
                          <td><b><%=Languages.getString("jsp.admin.customers.merchants_edit.phone",SessionData.getLanguage())%>:</td>
                          <td nowrap>
                            <input type="text" id="contactPhone" name="contactPhone" value="<%=Merchant.getContactPhone()%>" size="15" maxlength="15" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("contactPhone")) out.print("<font color=ff0000>*</font>");%>
                            
                          </td>
                          </tr>
                          <tr class="main">
                      <td><%=Languages.getString("jsp.admin.customers.merchants_edit.fax",SessionData.getLanguage())%>:</td>
                      <td nowrap><input type="text" id="contactFax" name="contactFax" value="<%=Merchant.getContactFax()%>" size="15" maxlength="15" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("contactFax")) out.print("<font color=ff0000>*</font>");%></td>
                          </tr>
                          <tr class="main">
                      <td><%=Languages.getString("jsp.admin.customers.merchants_edit.email",SessionData.getLanguage())%>:</td>
                      <td nowrap><input type="text" id="contactEmail" name="contactEmail" value="<%=Merchant.getContactEmail()%>" size="20" maxlength="200" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("contactEmail")) out.print("<font color=ff0000>*</font>");%></td>
                  </tr>
                          <tr class="main" nowrap>
                            <td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td>
                            <td nowrap><input type="text" name="smsNotificationPhone" id="smsNotificationPhone" value="<%=Merchant.getSmsNotificationPhone()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
                          </tr>
                          <tr class="main" nowrap>
                            <td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td>
                            <td nowrap><input type="text" name="mailNotificationAddress" id="mailNotificationAddress" value="<%=Merchant.getMailNotificationAddress()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
                          </tr>                       
                          <tr>
                            <td class="main" nowrap>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
                            </td>
                            <td nowrap class="main">
                              <input id="paymentNotifications" name="paymentNotifications" type="checkbox" onClick="changePayNoti()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap>
                              <%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" onClick="changePayNotiSMS()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" onClick="changePayNotiMail()" />
                            </td>
                          </tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
                          <tr>
                            <td class="main" nowrap>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
                            </td>
                            <td nowrap class="main">
                              <input id="balanceNotifications" name="balanceNotifications" type="checkbox" onClick="changeBalNoti()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap>
                              <%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
                            </td>
                          </tr> 
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" onClick="changeBalNotiValue()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" onClick="changeBalNotiDays()" />
                            </td>
                          </tr>                         
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationValue" id="balanceNotificationValue" value="<%=Merchant.getBalanceNotificationValue()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationDays" id="balanceNotificationDays" value="<%=Merchant.getBalanceNotificationDays()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
                          </tr>                         
                          <tr>
                            <td class="main" nowrap>
                              <%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" onClick="changeBalNotiSMS()" />
                            </td>
                          </tr>
                          <tr>
                            <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
                            <td>
                              <input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" onClick="changeBalNotiMail()" />
                            </td>
                          </tr>
<%
   }
%>  
                      </table>
                      <script>changePayNoti();</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
        <script>changeBalNoti();</script>
<%
   }
%>                
                    </td>
                </tr>
<%
  }//End of else when deploying in others
%>


  <%
  //2009-08-04  FB - Ticket 5545-8845699

  User tmpUser = new User();
  String strIsoId = tmpUser.getISOId(SessionData.getProperty("access_level"), SessionData.getProperty("ref_id"));
  isIsoPrepaid = Rep.getIsoWebPermission(DebisysConstants.WEB_PERMISSION_US_PREPAID, strIsoId);
  %>



        <%




              if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES))
              {
              %>
            <tr>
                  <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.common_info_edit.payment_information",SessionData.getLanguage())%></td>
                </tr>
                <tr>
                    <td class="formArea2">
                      <table width="100">
                          <tr class="main">
                           <td nowrap>
                             <%=Languages.getString("jsp.admin.customers.common_info_edit.payment_type",SessionData.getLanguage())%>
                           </td>
                           <td nowrap>
                            <SELECT NAME="paymentType">
                             <%
                      Vector vecPaymentTypes = com.debisys.customers.Merchant.getPaymentTypes();
                      Iterator itPaymentTypes = vecPaymentTypes.iterator();
                      boolean flag_payment=false;

                      while (itPaymentTypes.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector)itPaymentTypes.next();
                        String strPaymentTypeId = vecTemp.get(0).toString();
                        String strPaymentTypeCode = vecTemp.get(1).toString();
                        if ( (request.getParameter("paymentType") != null) && (strPaymentTypeId.equals(request.getParameter("paymentType"))) ){
                            flag_payment = true;
                            out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
                        }
                        else if ( strPaymentTypeId.equals(Integer.toString(Merchant.getPaymentType())) ){
                            flag_payment = true;
                            out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
                        }
                        else{
                          if ( Merchant.getPaymentType() == 0 && strPaymentTypeId.equals(Integer.toString(2)) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)){
                            out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
                          }
                          else if (Merchant.getPaymentType() == 0 && strPaymentTypeId.equals(Integer.toString(3)) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
                            out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
                          }
                          else{
                              out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" >" + strPaymentTypeCode + "</OPTION>");
                          }

                        }
                      }
                      if ( !flag_payment ){
                       %>
                          <OPTION VALUE="-1" >--</OPTION>
                       <%
                      }
                    %>

                            </SELECT>
                            </TD>

                            <td nowrap>
                            <b>
                            <% if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
                                   <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type",SessionData.getLanguage())%>
                                   <% } else{%>
                                   <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type_international",SessionData.getLanguage())%>
                           <%}%>
                           </b>
                           </td>
                           <td nowrap>
                            <SELECT NAME="processType" id="processType">
                               <%
                      Vector vecProcessTypes = com.debisys.customers.Merchant.getProcessorTypes();
                      Iterator itProcessTypes = vecProcessTypes.iterator();
                      boolean flag_process=false;
                      boolean isSelectProcess = false;
                      while (itProcessTypes.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector)itProcessTypes.next();
                        String strProcessTypeId = vecTemp.get(0).toString();
                        String strProcessTypeCode = vecTemp.get(1).toString();

                        if ( (request.getParameter("processType") != null) && (strProcessTypeId.equals(request.getParameter("processType"))) ){
                            flag_process = true;
                            out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
                        }
                        else if ( strProcessTypeId.equals(Integer.toString(Merchant.getProcessType())) ){
                            flag_process = true;
                            out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
                        }
                        else{
                          String processorDefaultId = Merchant.getProcessDefaultValueId(application);
                          if(processorDefaultId.trim().equals(strProcessTypeId)){
                            isSelectProcess = true;
                            out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
                          }
                          else{
                            out.println("<OPTION VALUE=\"" + strProcessTypeId + "\">" + strProcessTypeCode + "</OPTION>");
                          }
                          }
                      }
                      if ( !flag_process){
                          %>
                            <OPTION VALUE="-1" <%if(!isSelectProcess){%>SELECTED<%}%>>--</OPTION>
                       <%
                      }
                    %>
                            </SELECT>

                            <%
                                      if (merchantErrors != null && merchantErrors.containsKey("processType")){
                                        out.print("<font color=ff0000>*</font>");
                                      }
                    %>
                            </TD>

                            </tr>

                          </table>
                    </tr>
        <%
        }
        %>
      

<%                
            String merchantTypePrepaid = "", merchantTypeCredit = "", merchantTypeUnlimited = "";

            if (Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_PREPAID)) {
              merchantTypePrepaid = " selected";
            } else if (Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_CREDIT)) {
              merchantTypeCredit = " selected";
            } else if (Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED)) {
              merchantTypeUnlimited = " selected";
            } else {
                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                    merchantTypeCredit = " selected";
                } else {
                    merchantTypePrepaid = " selected";
                }
            }

            // DBSY-804
              // Get ISO of Merchant and then get credit type to determine if we need to force a
              // flexible credit type
              // ref_id = ISO ID
            String isoid = SessionData.getProperty("iso_id");
            String isoCredType = Rep.getISOCreditType(isoid);

            Hashtable<String, String> options =
                                                CreditTypes.getAvailableOptionsForMerchant(isoCredType, customConfigType, deploymentType,
                                                new String[]{merchantTypePrepaid, merchantTypeCredit, merchantTypeUnlimited});

            List<Boolean> otherOptions = CreditTypes.getFormOptionsMerchant(customConfigType, deploymentType, Merchant.getMerchantType(), isoCredType);
            boolean b8_hideCreditLimitBox = otherOptions.get(8);
            // if we got PREPAID and no other options means: either DOMESTIC+PREPAID or INTL+FLEX - disable the combo and the button!
            if (options.keySet().size() == 1 && options.containsKey(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                disabledText = "disabled=\"true\"";
            }

            
            if (!deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
%>
                <tr>
                    <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_edit.terminal_information",SessionData.getLanguage())%></td>
                </tr>
                <tr>
                    <td class="formArea2" valign=top>
                        <br>
                        <table width="100">
                            <tr class=main>
                                <td valign=top>
                                    <table width="100">
<%
                                        if ( (!customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))) {//If when not deploying in Mexico
%>
                                            <tr class=main>
                                                <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.monthly_fee_threshold",SessionData.getLanguage())%></td>
                                                <td><input type="text" id="monthlyFeeThreshold" name="monthlyFeeThreshold" value="<%=Merchant.getMonthlyFeeThreshold()%>" size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("monthlyFeeThreshold")) out.print("<font color=ff0000>*</font>");%>(<%=Languages.getString("jsp.admin.optional",SessionData.getLanguage())%>)</td>
                                            </tr>
<%
                                        } //End of if when not deploying in Mexico
            } else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
%>
                <tr>
                    <td class="formAreaTitle2"><br>
                        <%= Languages.getString("jsp.admin.customers.merchants_edit.terminal_information", SessionData.getLanguage()) %>
                    </td>
                </tr>
                <tr>
                    <td class="formArea2">
                        <table width="100">
                            <tr class="main">
                                <td nowrap>
                                    <%= Languages.getString("jsp.admin.customers.merchants_info.merchant_type", SessionData.getLanguage()) %> :
                                </td>
                                <td nowrap>
                                    <div id="lbCredit">
                                        <label id="showCreditType"></label>
                                    </div>
                                    <div id="lbCreditTx" style="display:none;">
                                        <select style="display:block;" <%=disabledText%> id="lstMerchantType" name="merchantType">
<%
                                        for (String option: options.keySet()) {
                                            String valueText = "jsp.admin.customers.merchants_info.credit_type" + option;
                %>
                                            <option value=<%=option%> <%=options.get(option)%>><%=Languages.getString(valueText,SessionData.getLanguage())%></option>
                <%
                                        }
                %>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
<%
            }
%>

            <script type="text/javascript">
                function addToParent(repId, repName, bIsShared, bIsUnlimited, CanChangeAccountType, creditType) {
                    var Prepaid   = '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type1",SessionData.getLanguage())%>';
                    var Credit   = '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type2",SessionData.getLanguage())%>';
                    var Unlimited = '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage())%>';
                    $("#lbCredit").hide(); $("#lbCreditTx").hide();
    
                    document.formMerchant.repId.value = repId;
                    document.formMerchant.repName.value = repName;
                    
                    var labelCreditType = ((creditType == "<%= DebisysConstants.REP_CREDIT_TYPE_PREPAID %>") 
                                    ? Prepaid
                                    : ((creditType == "<%= DebisysConstants.REP_CREDIT_TYPE_CREDIT %>") 
                                        ? Credit
                                        : Unlimited ));
                                         
                    if (labelCreditType != Unlimited) {
                        $("#lbCredit").show();
                        $("#showCreditType").text(labelCreditType);
                    } else {
                        $("#lbCreditTx").show();
                    }

<%
                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
%>
                        $("#lstMerchantType").val(creditType);
<%
                    } else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
%> 
                        if (!bIsUnlimited) {
                            var combo = document.getElementById('lstMerchantType');
                            var r = -1;
                            for (i = 0; i < combo.length; i++) {
                                if (combo.options[i].text === '<%=Languages.getString("jsp.admin.customers.merchants_info.credit_type3",SessionData.getLanguage())%>') {
                                    r = i;
                                }
                            }
                            if (r !== -1) {
                                combo.remove(r);
                            }
                        } else {
                            var combo = document.getElementById('lstMerchantType');
                            var r = -1;
                            for (i = 0; i < combo.length; i++) {
                                if (combo.options[i].text === '<%=Languages.getString("jsp.admin.customers.merchants_info.credit_type3",SessionData.getLanguage())%>') {
                                    r = i;
                                }
                            }
<%
                            if (disabledText.equals("")) { // this will skip the addition of unlimited option when : INTL+FLEX or DOMESTIC+PrepaidPrepaid
%>
                                if (r === -1) { // meaning there is no 'Unlimited' Option
                                    var y=document.createElement('option');
                                    y.text='<%=Languages.getString("jsp.admin.customers.merchants_info.credit_type3",SessionData.getLanguage())%>';
                                    y.value='3';
                                    try {
                                        combo.add(y, null); // standards compliant; doesn't work in IE
                                    } catch(ex) {
                                        combo.add(y); // IE only
                                    }
                                    for (i=0;i<combo.length;i++) {
                                        if (combo.options[i].text === '<%=Languages.getString("jsp.admin.customers.merchants_info.credit_type3",SessionData.getLanguage())%>') {
                                            combo.selectedIndex=i;
                                        }
                                    }
                                }
<%
                            }
%>
                        }
                        if ( document.formMerchant.creditLimit !== null ) {
<%
                            if(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
%>
                                document.formMerchant.creditLimit.value = ((bIsShared)?"0":"1");
<%
                            }
%>
                        }
                        checkCreditLimitTR();
<%
   // DBSY-1072 eAccounts Interface
                        if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
%>
                            var combo = document.getElementById('entityAccountType');
                            if(CanChangeAccountType !== null) {
                                if(!CanChangeAccountType) {
                                    combo.disabled = true;
                                    combo.options.selectedIndex ="0";
                                    var temp = document.getElementById('disabledentityAccountType');
                                    temp.value = "1";
                                } else {
                                    combo.disabled = false;
                                    combo.options.selectedIndex ="0";
                                    var temp = document.getElementById('disabledentityAccountType');
                                    temp.value = "";
                                }
                            } else {
                                combo.disabled = false;
                                combo.options.selectedIndex ="0";
                                var temp = document.getElementById('disabledentityAccountType');
                                temp.value = "";
                            }
<%
                        }
                    }
%>
                }

        function checkCreditLimitTR()
        {
          var nSharedBalance = 0;
          if ( document.getElementById('sharedBalance') !== null )
          {
            nSharedBalance = document.getElementById('sharedBalance').value;
          }

          if (document.getElementById('lstMerchantType').value === '<%=DebisysConstants.MERCHANT_TYPE_UNLIMITED%>' || nSharedBalance === 1)
          {
            document.getElementById('trCreditLimit').style.display = "none";
            document.getElementById('creditLimit').value = "0";
            document.getElementById('optionAba').disabled=true;
            document.getElementById('optionAccount').disabled=true;
          }
          else
          {
            if (document.getElementById('lstMerchantType').value === '<%=DebisysConstants.MERCHANT_TYPE_CREDIT%>')
            {
<%
  if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_MAXIMUM_SALES_LIMIT) )
  {
%>
              document.getElementById('trSalesLimitEnable').style.display = 'block';
<%
  }
%>
                document.getElementById('optionAba').disabled=true;
                document.getElementById('optionAccount').disabled=true;
            }
            else
            {
              document.getElementById('salesLimitEnable').checked = false;
              document.getElementById('salesLimitEnable').onclick();
              document.getElementById('trSalesLimitEnable').style.display = 'none';
                if (document.getElementById('optionAba') !== null)
                {
                    document.getElementById('optionAba').disabled=false;
                    document.getElementById('optionAba').checked=true;
                }
                if (document.getElementById('optionAccount') !== null)
                {
                    document.getElementById('optionAccount').disabled=false;
                    document.getElementById('optionAccount').checked=true;
                }                                                       
                document.getElementById('routingNumber').value="NA";
                if (document.getElementById('accountNumber') !== null)
                {
                    document.getElementById('accountNumber').value="NA";
                }
                                                            
            }
            document.getElementById('trCreditLimit').style.display = "block";
            document.getElementById('creditLimit').value = "0";
                                                
          }
        }
      </script>
      
<%
        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
%>
            <input type="hidden" name="merchantType" id="lstMerchantType"/>
<%
        } else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
%>
            <tr class=main style="display:block;">
                <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_info.merchant_type",SessionData.getLanguage())%></td>
                <td nowrap>
                    <select style="display:block;" <%=disabledText%> id="lstMerchantType" name="merchantType" onchange="checkCreditLimitTR();">
<%
                        for (String option: options.keySet()) {
                            String valueText = "jsp.admin.customers.merchants_info.credit_type" + option;
%>
                            <option value=<%=option%> <%=options.get(option)%>><%=Languages.getString(valueText,SessionData.getLanguage())%></option>
<%
                        }
%>
                    </select>
<%
                    if (!disabledText.equals("")) { //if theres disabled select, we need hidden input
%>
                        <input type="hidden" name="merchantType" id="lstMerchantType" value="<%=DebisysConstants.MERCHANT_TYPE_PREPAID%>"/>
<%
                    }

                    if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                        out.print("<SPAN ID='lblSharedBalance' STYLE='display:none;'><B>" + Languages.getString("jsp.admin.customers.merchants_info.credit_type4",SessionData.getLanguage()) + "</B></SPAN>");
                    }
                    if (merchantErrors != null && merchantErrors.containsKey("merchantType")) out.print("<font color=ff0000>*</font>");
                    out.print("</td></tr>");

                    if (b8_hideCreditLimitBox) {
                        styleCreditLimitBox = "style=\"display:none;\"";
                        screditLimitInputValue = "0";
                    } else {
                        styleCreditLimitBox = "style=\"display:block;\"";
                        screditLimitInputValue = (Merchant.getCreditLimit() != null) ? Merchant.getCreditLimit() : "";
                    }
%>
                    <tr id="trCreditLimit" class="main" <%out.println(styleCreditLimitBox);%>>
                        <td nowrap>
<%
                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico OR Domestic
%>
                                <B><%=Languages.getString("jsp.admin.customers.merchants_info.credit_limit",SessionData.getLanguage())%>:</B>&nbsp;</td>
                                <td>
                                    <input id="creditLimit" type="text" name="creditLimit" value="<%=screditLimitInputValue%>" size="8" maxlength="15" tabIndex="<%=intTabIndex++%>" >
                                    <INPUT TYPE="hidden" id="sharedBalance" NAME="sharedBalance" VALUE="<%=Merchant.getSharedBalance()%>">
                                    <SCRIPT>
                                        SetRepSharedBalance(<%=Merchant.getSharedBalance()%>);
                                    </SCRIPT>
<%
                            }
                                if ( !customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && !SessionData.getUser().isQcommBusiness() ) {//Else if when deploying in others and not QCOMM
                                    if (!disabledText.equals("")) { // If is Prepaid no show credit limit field
%>
                                        <input type="hidden" id="creditLimit" name="creditLimit" size="8" maxlength="15">
<%
                                    } else {
%>
                                        <%=Languages.getString("jsp.admin.customers.merchants_info.credit_limit",SessionData.getLanguage())%>
                                </td>
                                <td>
                                    <input type="text" id="creditLimit" name="creditLimit" value="<%=Merchant.getCreditLimit()%>" size="8" maxlength="15" tabIndex="<%=intTabIndex++%>">
<%
                                        if (merchantErrors != null && merchantErrors.containsKey("creditLimit")) out.print("<font color=ff0000>*</font>");
                                    }
                                } else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && SessionData.getUser().isQcommBusiness()) { // last case: when QComm business
%>
                                    <input type="hidden" id="creditLimit" name="creditLimit" size="8" maxlength="15">
<%
                                }
                                disabledText = ""; // Resetting the disabledText variable to fix the sticky behavior
%>
                                </td>
                    </tr>
<%
        }
    styleCreditLimitBox = "style=\"display:block;\"";
    if ( false && deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) ) 
    {
%>
<tr class="main">
  <td nowrap>
    <script>
    function TogglePINCache(bValue)
    {
      if ( bValue )
      {
        document.getElementById("trPINCache").style.visibility = "visible";
      }
      else
      {
        document.getElementById("trPINCache").style.visibility = "hidden";
        document.getElementById("reservedPINCacheBalance").value = "0";
      }
    }
    </script>
    <%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincache",SessionData.getLanguage())%>
  </td>
  <td nowrap>
    <label><input type="radio" name="pinCacheEnabled" value="true" <%=(Merchant.isPinCacheEnabled())?"checked":""%> 
                  onpropertychange="TogglePINCache(true);" onclick="TogglePINCache(true);">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincacheenabled",SessionData.getLanguage())%></label>&nbsp;&nbsp;&nbsp;<label><input type="radio" name="pinCacheEnabled" value="false" <%=(Merchant.isPinCacheEnabled())?"":"checked"%> onpropertychange="TogglePINCache(false);" onclick="TogglePINCache(false);">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincachedisabled",SessionData.getLanguage())%></label>
  </td>
</tr>
<tr id="trPINCache" class="main" style="visibility:hidden;">
  <td nowrap>
    <script>
    function FilterReservedPINCacheBalance(ctl)
    {
      var sText = ctl.value;
      var sResult = "";
      for ( i = 0; i < sText.length; i++ )
      {
        if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) )
        {
          sResult += sText.charAt(i);
        }
      }
      if ( sResult.length === 0 )
      {
        sResult = "0";
      }
      if ( sResult > 100 )
      {
        sResult = 100;
      }
      if ( sText !== sResult )
      {
        ctl.value = sResult;
      }
    }
    </script>
    <%=Languages.getString("jsp.admin.customers.merchants_info.reservedpincachebalance",SessionData.getLanguage())%>
  </td>
  <td nowrap>
    <input type="text" id="reservedPINCacheBalance" name="reservedPINCacheBalance" value="<%=Merchant.getReservedPINCacheBalance()%>" size="15" maxlength="3" onpropertychange="FilterReservedPINCacheBalance(this)" onblur="FilterReservedPINCacheBalance(this)">%&nbsp;(<%=Languages.getString("jsp.admin.customers.merchants_info.reservedpincachebalancenotice",SessionData.getLanguage()) + Merchant.getGlobalReservedPINCacheBalance()%>%)<%if (merchantErrors != null && (merchantErrors.containsKey("reservedPINCacheBalance") || merchantErrors.containsKey("globalPINCacheBalance") || merchantErrors.containsKey("invalidGlobalPINCacheBalance"))) out.print("<font color=ff0000>*</font>");%>
  </td>
</tr>
<%
  }
%>
  <tr id="trSalesLimitEnable" class="main" style="display:none;">
    <td nowrap colspan="2">
      <label><input type="checkbox" id="salesLimitEnable" name="salesLimitEnable" onclick="SwitchSalesLimit(this);" <%=(Merchant.getSalesLimitEnable().equals(""))?"":"CHECKED"%>><%=Languages.getString("jsp.admin.saleslimit.enableSalesLimit",SessionData.getLanguage())%></label>
      <script>
      function SwitchSalesLimit(chkSalesLimitType)
      {
        document.getElementById("tdSalesLimit").className = (chkSalesLimitType.checked)?"formArea2":"";
        document.getElementById("trSalesLimit").style.display = (chkSalesLimitType.checked)?"block":"none";
        document.getElementById("trSalesLimitDaily").style.display = (chkSalesLimitType.checked)?"block":"none";
      }
      </script>
    </td>
  </tr>
  <tr>
    <td id="tdSalesLimit" class="" colspan="2">
      <table>
        <tr class="main" id="trSalesLimit" style="display:none;">
          <td nowrap>
            <%=Languages.getString("jsp.admin.saleslimit.salesLimitType",SessionData.getLanguage())%>&nbsp;&nbsp;
            <select name="salesLimitType">
              <option value="DAILY"><%=Languages.getString("jsp.admin.saleslimit.salesLimitTypeDAILY",SessionData.getLanguage())%></option>
            </select>
          </td>
        </tr>
        <tr class="main" id="trSalesLimitDaily" style="display:none;">
          <td nowrap colspan="2">
            <table>
              <tr class="main">
                <td><%=Languages.getString("jsp.admin.saleslimit.salesLimitMonday",SessionData.getLanguage())%></td><td>&nbsp;</td>
                <td><%=Languages.getString("jsp.admin.saleslimit.salesLimitTuesday",SessionData.getLanguage())%></td><td>&nbsp;</td>
                <td><%=Languages.getString("jsp.admin.saleslimit.salesLimitWednesday",SessionData.getLanguage())%></td><td>&nbsp;</td>
                <td><%=Languages.getString("jsp.admin.saleslimit.salesLimitThursday",SessionData.getLanguage())%></td><td>&nbsp;</td>
                <td><%=Languages.getString("jsp.admin.saleslimit.salesLimitFriday",SessionData.getLanguage())%></td><td>&nbsp;</td>
                <td><%=Languages.getString("jsp.admin.saleslimit.salesLimitSaturday",SessionData.getLanguage())%></td><td>&nbsp;</td>
                <td><%=Languages.getString("jsp.admin.saleslimit.salesLimitSunday",SessionData.getLanguage())%></td>
              </tr>
              <tr>
                <td><input type="text" id="salesLimitMonday" name="salesLimitMonday" value="<%=Merchant.getSalesLimitMonday()%>" maxlength="5" size="5"></td><td></td>
                <td><input type="text" id="salesLimitTuesday" name="salesLimitTuesday" value="<%=Merchant.getSalesLimitTuesday()%>" maxlength="5" size="5"></td><td></td>
                <td><input type="text" id="salesLimitWednesday" name="salesLimitWednesday" value="<%=Merchant.getSalesLimitWednesday()%>" maxlength="5" size="5"></td><td></td>
                <td><input type="text" id="salesLimitThursday" name="salesLimitThursday" value="<%=Merchant.getSalesLimitThursday()%>" maxlength="5" size="5"></td><td></td>
                <td><input type="text" id="salesLimitFriday" name="salesLimitFriday" value="<%=Merchant.getSalesLimitFriday()%>" maxlength="5" size="5"></td><td></td>
                <td><input type="text" id="salesLimitSaturday" name="salesLimitSaturday" value="<%=Merchant.getSalesLimitSaturday()%>" maxlength="5" size="5"></td><td></td>
                <td><input type="text" id="salesLimitSunday" name="salesLimitSunday" value="<%=Merchant.getSalesLimitSunday()%>" maxlength="5" size="5"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</td>
</tr>
</table>

                    </td>
                </tr>
                <tr>

                <tr>
                  <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_edit.banking_information",SessionData.getLanguage())%></td>
                </tr>
                <tr>
                    <td class="formArea2">
                      <table width="100">
                      <jsp:include page="merchants_test_account.jsp"/>
                          <tr class="main">
                          <TD NOWRAP>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
    out.print("<B>" + Languages.getString("jsp.admin.customers.merchants_edit.tax_id",SessionData.getLanguage()) + ":</B>");
%>
                          </TD><td nowrap><input type="text" name="taxId" ID="taxId" value="<%=Merchant.getTaxId()%>" size="15" maxlength="15" tabIndex="<%=intTabIndex++%>" 
                                ONBLUR="ValidateRegExp(this, '[A-Z]{3,4}-[0-9]{6}-[A-Z0-9]{3}')"><%if (merchantErrors != null && merchantErrors.containsKey("taxId")) out.print("<font color=ff0000>*</font>");%>
                                                        &nbsp;<%=Languages.getString("jsp.admin.customers.merchants_edit.sample_taxid",SessionData.getLanguage())%>
                          </td>
                          </tr>
<%
  }
  else //Else when deploying in others
  {
    if(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
    {
      out.print("<B>" + Languages.getString("jsp.admin.customers.merchants_edit.tax_id_international",SessionData.getLanguage()) + ":</B>");
    }
    else
    {
      out.print(Languages.getString("jsp.admin.customers.merchants_edit.tax_id_international",SessionData.getLanguage()) + ":");
    }
%>
                          </TD><td nowrap>
                            <input type="text" id="taxId" name="taxId" 
                              <%if(Merchant.isAchAccount()){ %> readonly="readonly" <%}%>
                            value="<%=Merchant.getTaxId()%>" size="15" maxlength="11" tabIndex="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("taxId")) out.print("<font color=ff0000>*</font>");%></td>
                          </tr>
<%
  }
%>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  {//If when deploying in Mexico
%>
                                        <TR CLASS="main">
                                            <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.bankname",SessionData.getLanguage())%>:&nbsp;</TD>
                                            <TD><INPUT TYPE="text" NAME="bankName" id="bankName" VALUE="<%=Merchant.getBankName()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"><%if (merchantErrors != null && merchantErrors.containsKey("bankName")) out.print("<font color=ff0000>*</font>");%></TD>
                                        </TR>
                                        <TR CLASS="main">
                                            <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.accounttype",SessionData.getLanguage())%>:&nbsp;</TD>
                                            <TD>
                                                <SELECT NAME="accountTypeId" id="accountTypeId" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
  Iterator itAccountTypes = vecAccountTypes.iterator();
  while (itAccountTypes.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itAccountTypes.next();
    String strAccountTypeId = vecTemp.get(0).toString();
    String strAccountTypeCode = vecTemp.get(1).toString();
    if (strAccountTypeId.equals(request.getParameter("accountTypeId")))
      out.println("<OPTION VALUE=\"" + strAccountTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strAccountTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
  }
%>
                                                </SELECT>
                                            </TD>
                                        </TR>
<%
  } //End of if when deploying in Mexico
%>
                          <tr class="main">
                                                        <td nowrap>
                                                        <%
                                                            if(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                                {
                                  out.print("<B>" + Languages.getString("jsp.admin.customers.merchants_edit.routing_number",SessionData.getLanguage()) + ":</B>");
                                }
                                else
                                {
                                  if ( (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) )
                                    out.print(Languages.getString("jsp.admin.customers.merchants_edit.routing_number",SessionData.getLanguage()) + ":");
                                  else
                                    out.print(Languages.getString("jsp.admin.customers.merchants_edit.routing_number_international",SessionData.getLanguage()) + ":");

                                }

                                              String readOnly="";
                                              String regExpresion="";
                                              String maxlength="";

                                              if(Merchant.isAchAccount())
                                              {
                                                readOnly="readonly='readonly'";
                                              }
                                              if ( (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) )
                                              {
                                                regExpresion="[0-9]{18}";
                                                maxlength="18";
                                              }
                                              else if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) )
                                              {
                                                regExpresion="[0-9]{9}";
                                                maxlength="9";
                                              }


                                                        %>
                                                        </td>
                                                        <td nowrap>
                                                          <%String textErrorAba = Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage()); %>
                                                            <input type="text" id="routingNumber" name="routingNumber" "<%=readOnly%>"
                                                            value="<%=Merchant.getRoutingNumber()%>" size="15" maxlength="<%=maxlength%>"
                                                            <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                onKeyUp="verifyAba('<%=regExpresion%>','<%=textErrorAba%>');"
                              <%} %>
                                                            TABINDEX="<%=intTabIndex++%>"
                                                            <%if(Merchant.getRoutingNumber().startsWith("NA")){ %>
                                disabled = "false";
                              <%} %>
                                                            >
                                              <%if (merchantErrors != null && merchantErrors.containsKey("routingNumber")) out.print("<font color=ff0000>*</font>");%>
                                                        </td>
                                                        <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                                        <td nowrap>
                                                            <%String textInfoAba = Languages.getString("jsp.admin.customers.field_no_applicable.info_field_no_applicable",SessionData.getLanguage()); %>
                                                          <input type="checkbox" id="optionAba" name="optionAba"
                                                            value="optionAba" onClick="changeCheckAba()"
                                                            <%if(Merchant.getRoutingNumber().startsWith("NA")){ %>
                                     checked
                                  <%} %>
                                
                                                          ><%=textInfoAba%>
                                                        </td>
                                                        <td><div id="div_info_aba" name="div_info_aba" ></div></td>
                                                        <%} %>
                          </tr>
                          <tr class="main">
                      <td>
                        <%
                                                    if(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                            {
                              out.print("<B>" + Languages.getString("jsp.admin.customers.merchants_edit.account_number",SessionData.getLanguage()) + ":</B>");
                            }
                            else
                            {
                              out.print(Languages.getString("jsp.admin.customers.merchants_edit.account_number",SessionData.getLanguage()) + ":");
                            }
                        %>

                      </td>
                      <%if(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){ %>
                        <td nowrap>
                        <%String textErrorAccount = Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage()); %>
                          <input type="text" id="accountNumber" name="accountNumber"
                            <%if(Merchant.isAchAccount()){ %> readonly="readonly" <%}%>
                          value="<%=Merchant.getAccountNumber()%>" size="15" maxlength="20"
                          <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                          onKeyUp="verifyAccount(20,'<%=textErrorAccount%>');"
                          <%} %>
                          <%if(Merchant.getAccountNumber().startsWith("NA")){ %>
                            disabled = "false";
                          <%} %>
                          tabIndex="<%=intTabIndex++%>">
                          <%if (merchantErrors != null && merchantErrors.containsKey("accountNumber")) out.print("<font color=ff0000>*</font>");%>
                          </td>

                          <td nowrap>
                                                    <%String textInfoAccount = Languages.getString("jsp.admin.customers.field_no_applicable.info_field_no_applicable",SessionData.getLanguage()); %>
                                                      <input type="checkbox" id="optionAccount" name="optionAccount"
                                                      value="optionAccount" disabled="true" onClick="changeCheckAccount()"
                                                      <%if(Merchant.getAccountNumber().startsWith("NA")){ %>
                                   checked
                                <%} %>
                                                      ><%=textInfoAccount%>
                                                  </td>
                          <td><div id="div_info_account" name="div_info_account" ></div></td>
                      <%}
                      else{ %>

                        <td nowrap>
                        <input type="text" id="accountNumber" name="accountNumber"
                          <%if(Merchant.isAchAccount()){ %> readonly="readonly" <%}%>
                        value="<%=Merchant.getAccountNumber()%>" size="15" maxlength="20" tabIndex="<%=intTabIndex++%>">
                        <%if (merchantErrors != null && merchantErrors.containsKey("accountNumber")) out.print("<font color=ff0000>*</font>");%>
                              <%} %>
                          </tr>
                  <%
                  if(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                  {
                    if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_BANKING_ACCOUNT_TYPE_FOR_MERCHANT))
                    {
                  %>
                      <tr CLASS="main">
                                            <td NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.accounttype",SessionData.getLanguage())%>:&nbsp;</TD>
                                            <td>
                                                <select NAME="accountTypeId" id="accountTypeId" TABINDEX="<%=intTabIndex++%>">
                        <%
                          Vector vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
                          Iterator itAccountTypes = vecAccountTypes.iterator();
                          while (itAccountTypes.hasNext()) {
                            Vector vecTemp = null;
                            vecTemp = (Vector)itAccountTypes.next();
                            String strAccountTypeId = vecTemp.get(0).toString();
                            String strAccountTypeCode = vecTemp.get(1).toString();
                            if (strAccountTypeId.equals(request.getParameter("accountTypeId")))
                              out.println("<OPTION VALUE=\"" + strAccountTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
                            else
                              out.println("<OPTION VALUE=\"" + strAccountTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
                          }
                        %>
                                                </select>
                                            </td>
                                        </tr>
                                     <%}%>   
                    <tr class="main">
                      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankAddress",SessionData.getLanguage())%>:</td><td><input type="text" name="bankAddress" id="bankAddress" value="<%=Merchant.getBankAddress()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
                      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankCity",SessionData.getLanguage())%>:</td><td><input type="text" name="bankCity" id="bankCity" value="<%=Merchant.getBankCity()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
                    </tr>
                    <tr class="main">
                      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankState",SessionData.getLanguage())%>:</td><td><input type="text" name="bankState" id="bankState" value="<%=Merchant.getBankState()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
                      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankZip",SessionData.getLanguage())%>:</td><td><input type="text" name="bankZip" id="bankZip" value="<%=Merchant.getBankZip()%>" size="20" maxlength="10" tabIndex="<%=intTabIndex++%>"></td>
                    </tr>
                    <tr class="main">
                      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankContactName",SessionData.getLanguage())%>:</td><td><input type="text" name="bankContactName" id="bankContactName" value="<%=Merchant.getBankContactName()%>" size="20" maxlength="200" tabIndex="<%=intTabIndex++%>"></td>
                      <td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.bankContactPhone",SessionData.getLanguage())%>:</td><td><input type="text" name="bankContactPhone" id="bankContactPhone" value="<%=Merchant.getBankContactPhone()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
                    </tr>
                  <%
                    }
                  %>
                      </table>
                    </td>
                </tr>
                <%                 
                if ( !isDomestic ){
                %>
                <tr id="TRloginInformationTitle">
                  <td id="TDloginInformationTitle" class="formAreaTitle2"><br>
                <% if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
        <%=Languages.getString("jsp.admin.customers.merchants_edit.login_information",SessionData.getLanguage())%>
        <% } else{%>
        <%=Languages.getString("jsp.admin.customers.merchants_edit.login_information_international",SessionData.getLanguage())%>
        <%}%>
                  </td>
                </tr>
                <tr>
                  <td class="formArea2" valign=top>
                    <br>
                    <table>
                      <tr class=main>
                        <td valign=top>
                          <table>
                            <tr class=main>
                              <td nowrap><%=Languages.getString("jsp.admin.customers.user_logins.username",SessionData.getLanguage())%></td>
                              <td>&nbsp;&nbsp;<input type="text" id="username" name="username" value="<%=User.getUsername()%>" size="15" maxlength="50" tabIndex="<%=intTabIndex++%>"><%if (userErrors != null && userErrors.containsKey("username")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
                              <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp;<%=Languages.getString("jsp.admin.customers.user_logins.password",SessionData.getLanguage())%></td>
                              <td>&nbsp;&nbsp;<input type="text" id="password" name="password" value="<%=User.getPassword()%>" size="15" maxlength="50" tabIndex="<%=intTabIndex++%>"><%if (userErrors != null && userErrors.containsKey("password")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
                            </tr>
                            <tr>
                              <td colspan=4>
<%
  {
    Vector vecAssignablePermissions = User.getAssignablePermissions(SessionData, DebisysConstants.MERCHANT);
    if (vecAssignablePermissions != null && vecAssignablePermissions.size() > 0)
    {
      Iterator it = vecAssignablePermissions.iterator();
      while (it.hasNext())
      {
        Vector vecTemp = null;
        vecTemp = (Vector) it.next();
        String tmpPermTypeId = vecTemp.get(0).toString();
        String codeResource = "jsp.admin_" + vecTemp.get(1).toString().replaceAll("/", "_").replaceAll(" ","_") + "_Code";
        String languageResourceByCode = Languages.getString(codeResource, SessionData.getLanguage());
        //out.println("["+codeResource+"]");
%>
                                <span class=main>
                                  <label>
                                    <input type=checkbox name="perm_<%=tmpPermTypeId%>" value=y <%=(request.getParameter("perm_" + tmpPermTypeId) != null)?"checked":""%>>&nbsp;
                                    <%=languageResourceByCode%>
                                  </label>
                                </span><br>
<%
      }
    }
  }
%>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <%}%>
                <tr>
                  <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_edit.mailnotification",SessionData.getLanguage())%></td>
                </tr>
                <tr>
                  <td class="formArea2" valign=top>
                    <br>
                    <table>
                      <tr class=main>
                        <td valign=top>
                          <table>
                            <tr class=main><td nowrap><%=Languages.getString("jsp.admin.customers.merchants.mailnotice",SessionData.getLanguage())%></td></tr>
                            <tr class=main>
                              <td nowrap>
                                <!-- 2009-04-30 FB.Mail notification auto fill removed because ticket  5545-7483655 -->
                                  <input type="text" name="mailNotification" id="mailNotification" value="" size="80" maxlength="256" tabIndex="<%=intTabIndex++%>">
                                </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>         
                <tr>
                    <td class="formAreaTitle2" align="center">
                        <input type="hidden" id="submitted" name="submitted" value="y">
                        <input type=button value="<%=Languages.getString("jsp.admin.cancel",SessionData.getLanguage())%>" onClick="history.go(-1)" tabIndex="<%=intTabIndex++%>">
                        <%
                        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && hasPermissionGeolocation ){
                        %>
                        <input id="addressValidationStatus" name="addressValidationStatus" type="hidden" value="<%=Merchant.getAddressValidationStatus()%>"/>
                        <%}%>
                        <input type="button" id="Buttonsubmit" name="Buttonsubmit" value="<%=Languages.getString("jsp.admin.customers.merchants_add.submit",SessionData.getLanguage())%>" tabIndex="<%=intTabIndex++%>"/>
                    </td>
                </tr>

            </form>
</table>


<%
  }
  else
  {
%>
<table width="100%" align=left>
                <tr>
                  <td class="formAreaTitle2"><%=Languages.getString("jsp.admin.customers.merchants_add.success",SessionData.getLanguage())%></td>
                </tr>
                <tr>
                 <td class="main">
                  <br>
                  <%=Languages.getString("jsp.admin.customers.merchants_add.do_next",SessionData.getLanguage())%>:<br>
                  <%if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT))
                  {%>
                  <li><a href="admin/customers/merchants_add2.jsp?repId=<%=Merchant.getRepId()%>&repRatePlanId=<%=Merchant.getRepRatePlanId()%>"><%=Languages.getString("jsp.admin.customers.merchants_add.option1",SessionData.getLanguage())%></a></li>
                  <%}
                  else if (strAccessLevel.equals(DebisysConstants.REP))
                  {%>
                  <li><a href="admin/customers/merchants_add2.jsp?repRatePlanId=<%=Merchant.getRepRatePlanId()%>"><%=Languages.getString("jsp.admin.customers.merchants_add.option2",SessionData.getLanguage())%></a></li>
                  <%}%>
                  <li><a href="admin/customers/merchants_add1.jsp"><%=Languages.getString("jsp.admin.customers.merchants_add.option3",SessionData.getLanguage())%></a></li>
                  <li><a href="admin/customers/merchants.jsp?search=y"><%=Languages.getString("jsp.admin.customers.merchants_add.option4",SessionData.getLanguage())%></a></li>
                  <li><a href="admin/customers/merchants_info.jsp?merchantId=<%=Merchant.getMerchantId()%>"><%=Languages.getString("jsp.admin.customers.merchants_add.option5",SessionData.getLanguage())%></a></li>
                  <li><a href="admin/customers/terminal.jsp?merchantId=<%=Merchant.getMerchantId()%>&repId=<%=Merchant.getRepId()%>"><%=Languages.getString("jsp.admin.customers.merchants_info.add_new_terminal",SessionData.getLanguage())%></a></li>
                  <br><br><br>
                 </td>
                </tr>
</table>
<%}%>
<script>
    var showMap = (localStorage["mapFlag"] === "true") ? true : false;
    
$(document).ready( function() {
    
    var Prepaid   = '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type1",SessionData.getLanguage())%>';
    var Credit   = '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type2",SessionData.getLanguage())%>';
    var Unlimited = '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage())%>';
    
<%
    if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_MAXIMUM_SALES_LIMIT) ) {
%>
        if ( document.getElementById("salesLimitEnable") !== null ) {
            document.getElementById("salesLimitEnable").onclick();
        }
<%
    }

    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
%>
        if ( document.getElementById('lstMerchantType') !== null ) {
            document.getElementById('lstMerchantType').onchange();
        }
<%
    } else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && strAccessLevel.equals(DebisysConstants.REP)) {
%>
        var labelCreditType = (("<%= dataRepresentant.getRepCreditType() %>" == "<%= DebisysConstants.REP_CREDIT_TYPE_PREPAID %>") 
                                    ? Prepaid
                                    : (("<%= dataRepresentant.getRepCreditType() %>" == "<%= DebisysConstants.REP_CREDIT_TYPE_CREDIT %>") 
                                        ? Credit
                                        : Unlimited ));  
        $("#showCreditType").text(labelCreditType);
<%
    }
%>
});
  
    function loadSetupMap() {
        
        if (showMap) {
            latitudeMap   = <%=propertiesValues.get(latitudeValue)%>;
            longitudeMap = <%=propertiesValues.get(longitudeValue)%>; 
            zoomPoint    = <%=propertiesValues.get(zoomValue)%>; 
        } else {
            latitudeMap = parseFloat(localStorage["latitudeMap"]);
            longitudeMap = parseFloat(localStorage["longitudeMap"]);
            localStorage.removeItem("latitudeMap");
            localStorage.removeItem("longitudeMap");
        }        
        
        $('#map').show();
        initMap(showMap);           
        $('#mainTableSupport').hide();
        $('#divPrintBanner').hide();
    }
    
    if (showMap) {
        loadSetupMap();
        showMap = false;
        localStorage.removeItem("mapFlag");
    }
</script>
</td>
<td id="messagesZone" style="width: 150px;">
    <div id="divMessages" style="width: 550px;">
       
    </div>    
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

    

<%@ include file="/includes/footer.jsp" %>
