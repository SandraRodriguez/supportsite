<%@ page import="com.debisys.utils.*,
         java.util.*"%>
<%
    int section = 2;
    int section_page = 17;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="page"/>
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%    
Vector vTmp = new Vector();
    if (NumberUtil.isNumeric(Rep.getPaymentAmount()) && NumberUtil.isNumeric(Rep.getCommission())) {
        if (Rep.getAssignedCreditSubAgent(SessionData) > (Rep.getCurrentSubAgentCreditLimit(SessionData) + Double.parseDouble(Rep.getPaymentAmount()))
                && !Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
            response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=4");
            return;
        } else {

            if ((!Rep.getOldRepCreditType().equals(Rep.getRepCreditType()) && !Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))) {
                if (!Rep.checkChildCreditType()) {
                    //error - merchants under must be all prepaid or unlimited
                    response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=5");
                    return;
                } else {
                    if (Rep.checkAgentCreditLimit(SessionData, application)) {
                        Rep.makeSubAgentPayment(SessionData, application, false);
                        response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId());
                        return;
                    }
                }
            } else if (!Rep.getOldRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) && Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                if (!Rep.checkAgentCreditType(SessionData, application)) {
                    response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=15");
                    return;
                } else {
                    Rep.makeSubAgentPayment(SessionData, application, false);
                    response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId());
                    return;
                }
            } else {
                String redirectUrl = "subagents_info.jsp?repId=" + Rep.getRepId();
                if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    if (Rep.checkAgentCreditLimit(SessionData, application)) {                        
                        try {
                            vTmp = Rep.doPaymentSubagentMX(SessionData, application);
                        } catch (Exception e) {
                            vTmp.add(999);
                        }
                        if (vTmp.get(0).toString().equals("0")) {
                            response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=10");
                            return;
                        } else {
                            response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=12&code=" + vTmp.get(0));
                            return;
                        }
                    } else {
                        response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=16");
                        return;
                    }
                } else {
                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {

                        if (Rep.checkAgentCreditLimit(SessionData, application)) {
                            
                            try {
                                vTmp = Rep.doPaymentSubagentMX(SessionData, application);
                            } catch (Exception e) {
                                vTmp.add(999);
                            }
                            if (vTmp.get(0).toString().equals("0")) {
                                response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=14");
                                return;
                            } else {
                                response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=13&code=" + vTmp.get(0));
                                return;
                            }

                        } else {
                            response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=16");
                            return;
                        }
                    } else {
                        if (Rep.checkAgentCreditLimit(SessionData, application)) {
                            try {
                                vTmp = Rep.doPaymentSubagentMX(SessionData, application);
                            } catch (Exception e) {
                                vTmp.add(999);
                            }
                            if (vTmp.get(0).toString().equals("0")) {
                                response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=14");
                                return;
                            } else {
                                response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=13&code=" + vTmp.get(0));
                                return;
                            }
                        } else {
                            response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=16");
                            return;
                        }
                    }
                }
                /*
                if (Rep.getErrors() != null) {

                    Enumeration enum1 = Rep.getErrors().keys();
                    if (enum1.hasMoreElements()) {
                        String strKey = enum1.nextElement().toString();
                        String strError = (String) Rep.getErrors().get(strKey);
                        redirectUrl = redirectUrl + "&message=" + strError;
                    }

                }
                response.sendRedirect(redirectUrl);
                return;
                */
                
            }
        } // End of Check for assigned credit
    }// end of isNumeric If
    else {
        response.sendRedirect("subagents_info.jsp?repId=" + Rep.getRepId() + "&message=2");
        return;
    }

%>