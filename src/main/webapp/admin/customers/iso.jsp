<%@ page
	import="com.debisys.customers.CustomerSearch,java.util.HashMap,java.util.Vector,java.util.Iterator,java.net.URLEncoder,com.debisys.utils.HTMLEncoder"%>
<%
	int section = 2;
	int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="CustomerSearch"
	class="com.debisys.customers.CustomerSearch" scope="request" />
<jsp:setProperty name="CustomerSearch" property="*" />
<%@ include file="/includes/security.jsp"%>

<%
	String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
	Vector vecSearchResults = new Vector();
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;
	
	if (request.getParameter("search") != null)
	{
		
		if (request.getParameter("page") != null)
		{
			try
			{
				intPage = Integer.parseInt(request.getParameter("page"));
			}
			catch (NumberFormatException ex)
			{
				intPage = 1;
			}
		}
		
		if (intPage < 1)
		{
			intPage = 1;
		}
		
		vecSearchResults = CustomerSearch.searchRep(intPage, intPageSize, SessionData,DebisysConstants.REP_TYPE_ISO_5_LEVEL);
				
		intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
		vecSearchResults.removeElementAt(0);
		if (intRecordCount > 0)
		{
			intPageCount = (intRecordCount / intPageSize) + 1;
			if ((intPageCount * intPageSize) + 1 >= intRecordCount)
			{
				intPageCount++;
			}
		}
		
	}
%>
<%@ include file="/includes/header.jsp"%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" width="2000"
			class="formAreaTitle">
			&nbsp;<%=Languages.getString("jsp.admin.customers.iso.title",SessionData.getLanguage()).toUpperCase() + " "
					+ SessionData.getProperty("company_name").toUpperCase()%></td>
		<td width="12" height="20">
			<img src="images/top_right_blue.gif">
		</td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				align=center>
				<tr>
					<td>
						<form name="entityform" method="get"
							action="admin/customers/iso.jsp">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="formArea2">
										<br>
										<table border=0>
											<tr class="main">
												<td nowrap valign="top" width="100" colspan=2><%=Languages.getString("jsp.admin.narrow_results",SessionData.getLanguage())%>:
												</td>
											</tr>
											<tr class=main>
												<td valign="top" nowrap colspan=2>
													<input name="criteria" type="text" size="40" maxlength="64">
													<br>
													(<%=Languages.getString("jsp.admin.customers.agents.search_instructions",SessionData.getLanguage())%>)
												</td>
												<td valign="top" align="left">
													<input type="hidden" name="search" value="y">
													<input type="submit" name="submit"
														value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
												</td>
											</tr>
											</form>


											<!-- START REMOVING MCR -->
											<!-- 
            <%if (SessionData.checkPermission(DebisysConstants.PERM_AGENTS))
			{%> 
              <tr>
              <td colspan=2 align=left>
                  <form method="get" action="admin/customers/agents_add.jsp"><input type="submit" name="addAgent" value="<%=Languages.getString("jsp.admin.customers.agents.add_agent",SessionData.getLanguage())%>"></form>
              </td>
              </tr>
             <%}%>
            </table>-->
											<!-- END REMOVING MCR -->
											<%
												if (vecSearchResults != null && vecSearchResults.size() > 0)
												{
											%>
											<table width="100%" cellspacing="1" cellpadding="1"
												border="0">
												<tr>
													<td class="main" colspan=7><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage())
						+ " "%>
														<%
															if (!CustomerSearch.getCriteria().equals(""))
																{
																	out.println(Languages.getString("jsp.admin.for",SessionData.getLanguage()) + " \""
																			+ HTMLEncoder.encode(CustomerSearch.getCriteria()) + "\"");
																}
														%>.
														<%=Languages.getString("jsp.admin.displaying", new Object[]
				{ Integer.toString(intPage), Integer.toString(intPageCount - 1) },SessionData.getLanguage())%><br>
														<%
															if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
																{
														%>
														<%=Languages.getString("jsp.admin.customers.agents.nav_instructions",SessionData.getLanguage())%>
														<%
															}
																else
																{
														%>
														<%=Languages
									.getString("jsp.admin.customers.agents.nav_instructions_international",SessionData.getLanguage())%>
														<%
															}
														%>
													</td>
												</tr>
												<tr>
													<td align=right class="main" nowrap colspan=7>
														<%
															if (intPage > 1)
																{
																	out.println("<a href=\"admin/customers/iso.jsp?search="
																			+ URLEncoder.encode(request.getParameter("search"), "UTF-8")
																			+ "&criteria="
																			+ URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")
																			+ "&page=1&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8")
																			+ "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8")
																			+ "\">" + Languages.getString("jsp.admin.first",SessionData.getLanguage()) + "</a>&nbsp;");
																	out.println("<a href=\"admin/customers/iso.jsp?search="
																			+ URLEncoder.encode(request.getParameter("search"), "UTF-8")
																			+ "&criteria="
																			+ URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page="
																			+ (intPage - 1) + "&col="
																			+ URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort="
																			+ URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">&lt;&lt;"
																			+ Languages.getString("jsp.admin.previous",SessionData.getLanguage()) + "</a>&nbsp;");
																}
																int intLowerLimit = intPage - 12;
																int intUpperLimit = intPage + 12;
																
																if (intLowerLimit < 1)
																{
																	intLowerLimit = 1;
																	intUpperLimit = 25;
																}
																
																for (int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
																{
																	if (i == intPage)
																	{
																		out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
																	}
																	else
																	{
																		out.println("<a href=\"admin/customers/iso.jsp?search="
																				+ URLEncoder.encode(request.getParameter("search"), "UTF-8")
																				+ "&criteria="
																				+ URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")
																				+ "&page=" + i + "&col="
																				+ URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort="
																				+ URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" + i
																				+ "</a>&nbsp;");
																	}
																}
																
																if (intPage < (intPageCount - 1))
																{
																	out.println("<a href=\"admin/customers/iso.jsp?search="
																			+ URLEncoder.encode(request.getParameter("search"), "UTF-8")
																			+ "&criteria="
																			+ URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page="
																			+ (intPage + 1) + "&col="
																			+ URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort="
																			+ URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">"
																			+ Languages.getString("jsp.admin.next",SessionData.getLanguage()) + "&gt;&gt;</a>&nbsp;");
																	out.println("<a href=\"admin/customers/iso.jsp?search="
																			+ URLEncoder.encode(request.getParameter("search"), "UTF-8")
																			+ "&criteria="
																			+ URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page="
																			+ (intPageCount - 1) + "&col="
																			+ URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort="
																			+ URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">"
																			+ Languages.getString("jsp.admin.last",SessionData.getLanguage()) + "</a>");
																}
														%>
													</td>
												</tr>
												<tr>
													<td class=rowhead2><%=Languages.getString("jsp.admin.customers.business_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;
														<a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=1&sort=1"><img
																src="images/down.png" height=11 width=11 border=0>
														</a><a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=1&sort=2"><img
																src="images/up.png" height=11 width=11 border=0>
														</a>
													</td>
													<td class=rowhead2><%=Languages.getString("jsp.admin.customers.iso.iso_id",SessionData.getLanguage()).toUpperCase()%>&nbsp;
														<a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=2&sort=1"><img
																src="images/down.png" height=11 width=11 border=0>
														</a><a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=2&sort=2"><img
																src="images/up.png" height=11 width=11 border=0>
														</a>
													</td>
													<td class=rowhead2><%=Languages.getString("jsp.admin.customers.city",SessionData.getLanguage()).toUpperCase()%>&nbsp;
														<a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=3&sort=1"><img
																src="images/down.png" height=11 width=11 border=0>
														</a><a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=3&sort=2"><img
																src="images/up.png" height=11 width=11 border=0>
														</a>
													</td>
													<td class=rowhead2><%=Languages.getString("jsp.admin.customers.state",SessionData.getLanguage()).toUpperCase()%>&nbsp;
														<a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=4&sort=1"><img
																src="images/down.png" height=11 width=11 border=0>
														</a><a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=4&sort=2"><img
																src="images/up.png" height=11 width=11 border=0>
														</a>
													</td>
													<td class=rowhead2><%=Languages.getString("jsp.admin.customers.contact",SessionData.getLanguage()).toUpperCase()%>&nbsp;
														<a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=5&sort=1"><img
																src="images/down.png" height=11 width=11 border=0>
														</a><a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=5&sort=2"><img
																src="images/up.png" height=11 width=11 border=0>
														</a>
													</td>
													<td class=rowhead2><%=Languages.getString("jsp.admin.customers.phone",SessionData.getLanguage()).toUpperCase()%>&nbsp;
														<a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=6&sort=1"><img
																src="images/down.png" height=11 width=11 border=0>
														</a><a
															href="admin/customers/iso.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=6&sort=2"><img
																src="images/up.png" height=11 width=11 border=0>
														</a>
													</td>
													<td class=rowhead2>
														&nbsp;
													</td>
												</tr>
												<%
													Iterator it = vecSearchResults.iterator();
														int intEvenOdd = 1;
														while (it.hasNext())
														{
															Vector vecTemp = null;
															vecTemp = (Vector) it.next();
															//out.println("<tr class=row" + intEvenOdd +">" +
															//           "<td><a href=\"admin/customers/agents_info.jsp?repId=" + vecTemp.get(1) + "\">" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</a>");
															
															///REMOVE															
															if (SessionData.checkPermission(DebisysConstants.PERM_TO_VIEW_ISO_INFORMATION))
															{
																out.println("<tr class=row" + intEvenOdd + ">"
																		+ "<td><a href=\"admin/customers/iso_info.jsp?repId="
																		+ vecTemp.get(1) + "\">"
																		+ HTMLEncoder.encode(vecTemp.get(0).toString()) + "</a>");
																
															}
															else
															{
																out.println("<tr class=row" + intEvenOdd + ">" + "<td>" + vecTemp.get(0));
																
															}
																		
															out.println("</td>" + "<td nowrap>" + vecTemp.get(1) + "</td>" + "<td nowrap>"
																	+ vecTemp.get(2) + "</td>" + "<td>" + vecTemp.get(3) + "</td>" + "<td>"
																	+ vecTemp.get(4) + " " + vecTemp.get(5) + "</td>" + "<td>"
																	+ vecTemp.get(6) + "</td>" + "<td>");
															if (SessionData.checkPermission(DebisysConstants.PERM_LOGINS))
															{
																out.print("<a href=\"admin/customers/user_logins.jsp?refId="
																		+ vecTemp.get(1) + "&refType="
																		+ DebisysConstants.PW_REF_TYPE_CARRIER
																		+ "\"><img src=images/icon_user.png border=0 alt=\""
																		+ Languages.getString("jsp.admin.customers.edit_logins",SessionData.getLanguage())
																		+ "\"></a>");
															}
															out.print("<a href=\"javascript:\" onclick=\"OpenTrx("
																	+ vecTemp.get(1) + ", '" + vecTemp.get(0).toString().replaceAll("'", "")
																	+ "');\"><img src=images/icon_dollar.png border=0 alt=\""
																	+ Languages.getString("jsp.admin.customers.dollarsign",SessionData.getLanguage()) + "\"></a>");
															String sChildLevel = "agents";
															if ( strAccessLevel.equals(DebisysConstants.CARRIER) )
															{
																if ( vecTemp.get((CustomerSearch.isSearchNumNewModelsPlans()?10:9)).equals("2") )//Whether is 3 or 5 level
																{
																	sChildLevel = "reps";
																}
															}
															out.print("<a href=\"admin/customers/" + sChildLevel + ".jsp?search=y&repId="
																	+ vecTemp.get(1) + "\"><img src=images/lupa.png border=0 alt=\""
																	+ Languages.getString("jsp.admin.customers.drilldown",SessionData.getLanguage()) + "\"></a></td>"
																	+ "</tr>");
															if (intEvenOdd == 1)
															{
																intEvenOdd = 2;
															}
															else
															{
																intEvenOdd = 1;
															}
															
														}
														vecSearchResults.clear();
												%>
											</table>
											<form id="frmOpenTrx" action="admin/transactions/transactions.jsp" method="post">
												<input type="hidden" id="inRepID" name="repId">
												<input type="hidden" id="inRepName" name="repName">
											</form>
											<script>
											function OpenTrx(nRepID, sRepName)
											{
												$("#inRepID").val(nRepID);
												$("#inRepName").val(sRepName);
												$("#frmOpenTrx").submit();
											}
											</script>

											<%
												}
												else
													if (intRecordCount == 0 && request.getParameter("search") != null)
													{
														out.println("<br><br><font color=ff0000>"
																+ Languages.getString("jsp.admin.customers.isos.not_found",SessionData.getLanguage())
																+ "</font>");
													}
											%>
											</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp"%>