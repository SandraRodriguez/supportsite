<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%
	int section=14;
	int section_page=6;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:setProperty name="User" property="*"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_products.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<table width=100% border=0>
							<tr class="main">
	               				<td nowrap valign="top"><%=Languages.getString("jsp.admin.customers.carrier_users_products.instructions",SessionData.getLanguage())%></td>
	               			</tr>
	               		</table>
						<table>
       						<tr>
								<td align="center">
									<form name="entityform" method="get" action="admin/customers/carrier_users_products.jsp" >
										<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users.EditCarrierUserProducts",SessionData.getLanguage())%>">
										<input type="hidden" id="passwordId" name="passwordId" value="<%=request.getParameter("passwordId")%>" />
									</form>
								</td>
							</tr>
						</table>
       					
<%
	// Store this in case there are products for that carrier user
	StringBuffer strTableBuff = new StringBuffer("");
	strTableBuff.append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr>");
	strTableBuff.append("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.carrier_users_products.provider",SessionData.getLanguage()) + "</td>");
	strTableBuff.append("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.carrier_users_products.product_id",SessionData.getLanguage()) + "</td>");
	strTableBuff.append("<td class=rowhead2>" + Languages.getString("jsp.admin.customers.carrier_users_products.product_name",SessionData.getLanguage()) + "</td>");
	strTableBuff.append("</tr>");
	
	boolean resultFound = false;
	Vector<Vector<String>> vecProductResults = User.getProducts();
	int intEvenOdd = 1;
	String providerID = "";
	Iterator<Vector<String>> itProducts = vecProductResults.iterator();
		
	// Basically get a list of all of the products and organize them by provider.
	// Each product will have a flag indicating if it is allowed for the carrier user
	// 0 - not allowed
	// 1 - allowed
	// Show them in a list sorted by provider, with each product having a checkbox
	while (itProducts.hasNext())
	{
		Vector<String> vecProducts = null;
		vecProducts = (Vector<String>) itProducts.next();
		String selected = "";
		
		// Only show the product if the carrier user is allowed to use it
		if(vecProducts.get(5).equals("1")) {
			resultFound = true;
			if(resultFound == true && strTableBuff != null)
			{
				out.println(strTableBuff.toString());
				strTableBuff = null;
			}
			
			// Getting the first product/provider
			if(providerID.equals("")) {
				providerID = vecProducts.get(0);
				out.println("<tr class=row" + intEvenOdd +">");
				out.println("<td>");
				out.println(vecProducts.get(1) + " (" + vecProducts.get(0) + ")");
				out.println("</td>");
				out.println("<td align=\"center\">--");
				out.println("</td>");
				out.println("<td align=\"center\">--");
				out.println("</td>");
				out.println("</tr>");
				if (intEvenOdd == 1)
		    		intEvenOdd = 2;
		  		else
		    		intEvenOdd = 1;
			}
			
			// Finished one set of products for a specific provider, on to the next one
			if(!vecProducts.get(0).equals(providerID)) {
				providerID = vecProducts.get(0);
				out.println("<tr class=row" + intEvenOdd +">");
				out.println("<td>");
				out.println(vecProducts.get(1) + " (" + vecProducts.get(0) + ")");
				out.println("</td>");
				out.println("<td align=\"center\">--");
				out.println("</td>");
				out.println("<td align=\"center\">--");
				out.println("</td>");
				out.println("</tr>");
				if (intEvenOdd == 1)
		    		intEvenOdd = 2;
		  		else
		    		intEvenOdd = 1;
			}
			
			out.println("<tr class=row" + intEvenOdd +">");
			out.println("<td>");
			out.println("</td>");
			out.println("<td>");
			out.println(vecProducts.get(2));
			out.println("</td>");
			out.println("<td>");
			out.println(vecProducts.get(3) + " (" + vecProducts.get(4) + ")");
			out.println("</td>");
			out.println("</tr>");
	
			if (intEvenOdd == 1)
	    		intEvenOdd = 2;
	  		else
	    		intEvenOdd = 1;
		}
	}
	
	if(resultFound)
	{
		out.println("</table>");
	} else {
%>
						<table width="100%">
			               	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%
	}
%>			
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>