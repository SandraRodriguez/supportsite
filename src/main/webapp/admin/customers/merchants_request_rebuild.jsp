<%@ page import="com.debisys.customers.Merchant,java.net.URLEncoder,com.debisys.utils.HTMLEncoder,com.debisys.utils.NumberUtil,java.util.*"%>
<%
	int section = 2;
	int section_page = 19;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request" />
<jsp:useBean id="TerminalVersions" class="com.debisys.terminals.TerminalVersions" scope="request" />
<jsp:useBean id="ValidateEmail" class="com.debisys.utils.ValidateEmail" scope="request" />
<jsp:useBean id="Properties" class="com.debisys.properties.Properties" scope="request" />
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>


<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%

    /*
    * This page just sends a mail to customer service for rebuild the terminal desired.
    */

	StringBuffer optionsTerminalType = new StringBuffer(1000);
	StringBuffer optionsTerminalVersions = new StringBuffer(1000);
	
	boolean finishOk=false;

    String messages="";
	String currentTypeDescription = "";
	String siteId = "";
	String type = "";
    String merchantId="";
	String selected_type = "";
    String email="";
    String selected_type_desc="";
    
	Vector terminalTypes = Terminal.getTerminalTypeForRequest(SessionData,application);
	Vector terminalVersionByTerminalType = null;

	siteId = request.getParameter("siteId");
	type = request.getParameter("type");
	merchantId = request.getParameter("merchantId");
	selected_type = request.getParameter("selected_type");
    email = request.getParameter("email");
    selected_type_desc = request.getParameter("selected_type_desc");
      
      
        
	String siteIdLanguage = Languages.getString("jsp.merchant.requestrebuil.text5",SessionData.getLanguage());
	String currentTypeLanguage = Languages.getString("jsp.merchant.requestrebuil.text6",SessionData.getLanguage());
	String updateAddressOnlyLanguage  = Languages.getString("jsp.merchant.requestrebuil.text7",SessionData.getLanguage());
	String termTypeLanguage = Languages.getString("jsp.merchant.requestrebuil.text8",SessionData.getLanguage());
	String termVersionLanguage = Languages.getString("jsp.merchant.requestrebuil.text9",SessionData.getLanguage());
	String notesLanguage = Languages.getString("jsp.merchant.requestrebuil.text10",SessionData.getLanguage());
	String fromLanguage = Languages.getString("jsp.merchant.requestrebuil.text11",SessionData.getLanguage());
	String toLanguage = Languages.getString("jsp.merchant.requestrebuil.text12",SessionData.getLanguage());
	String sendEmailLanguage = Languages.getString("jsp.merchant.requestrebuil.text13",SessionData.getLanguage());
	String goToTerminalsLanguage = Languages.getString("jsp.merchant.requestrebuil.text14",SessionData.getLanguage());
	String sepateEmailsLanguage = Languages.getString("jsp.merchant.requestrebuil.text15",SessionData.getLanguage());
	String requestRebuildTitleLanguage = Languages.getString("jsp.merchant.requestrebuil.text16",SessionData.getLanguage());
	String customerSupportEmailLanguage = Languages.getString("jsp.merchant.requestrebuil.text17",SessionData.getLanguage());
	String fromValidate = Languages.getString("jsp.merchant.requestrebuil.text18",SessionData.getLanguage());
	String terminalTypeValidate = Languages.getString("jsp.merchant.requestrebuil.text19",SessionData.getLanguage());
			
	String instance = DebisysConfigListener.getInstance(this.getServletContext());		
	String customerServiceEmail = Properties.getPropertieByName(instance,"customerServiceMail");		
	
	String intranetCustomerSetup = Properties.getPropertieByName(instance,"intranetCustomerSetup");	
			
	if (selected_type != null) 
	{
	    ///////////////////////////////////////////////////////////////////////////////////////////
	    //when terminal type combo box change this code will be fill terminal software combo.//////
	    ///////////////////////////////////////////////////////////////////////////////////////////
		terminalVersionByTerminalType = TerminalVersions.getTerminalVersionsByTerminalType(SessionData,application,selected_type);
		Iterator itTerminalTypes = terminalVersionByTerminalType.iterator();
		while (itTerminalTypes.hasNext()) 
		{
			Vector vecTemp = (Vector) itTerminalTypes.next();
			String value = vecTemp.get(0).toString();
			
			optionsTerminalVersions.append("<option  value=\""+ value + "\">" + value+ "</option>");
			 
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////
	}
	

	if (request.getMethod().equals("POST")) 
	{
		String mailHost = DebisysConfigListener.getMailHost(application);
		String bodyText = request.getParameter("bodyText");
		String from = request.getParameter("from");
		String mails = request.getParameter("mails");
		
		mails= mails+";"+from;
		
		String addressOnly = request.getParameter("addressOnly");
		String terminalVersions = request.getParameter("terminalVersions");
		try
		{
		 	String additionalComments = Languages.getString("jsp.admin.reports.pinreturn_search.additionalcomments",SessionData.getLanguage());
			String text1 = Languages.getString("jsp.merchant.requestrebuil.text1",SessionData.getLanguage());
			String text2 = Languages.getString("jsp.merchant.requestrebuil.text2",SessionData.getLanguage());
			String requestRebuildForSite = Languages.getString("jsp.merchant.requestrebuil.text3",SessionData.getLanguage());
			String text4 = Languages.getString("jsp.merchant.requestrebuil.text4",SessionData.getLanguage());
						  
			StringBuffer bodyMessage = new StringBuffer(5000);
			String styleFont="FONT-WEIGHT: bold;FONT-SIZE: 12pt;";
			String styleFontBody="font-size: 10pt;";		
			String styleFontTitles="font-size: 11pt;";		
			String styleTable= "BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;";
			String styleTable1= styleTable+"BACKGROUND-COLOR: #f1f9fe;width:700px;";
			String styleTable2=styleTable+"BACKGROUND-COLOR: #ffffff;width:98%;";
			String bodyColor="#ffffff";
			
			bodyMessage.append("<body bgcolor=\""+bodyColor+"\">");
			bodyMessage.append("<table cellpadding=\"2\" cellspacing=\"2\" style=\""+styleTable1+"\">");
			bodyMessage.append(" <tr>");
			bodyMessage.append("   <td><b>"+requestRebuildForSite+" "+siteId+" </b></td>");
			bodyMessage.append("  </tr>");
			bodyMessage.append("  <tr>");
			bodyMessage.append("   <td style=\""+styleFont+"\"><br><br>"+additionalComments+"</td>");
			bodyMessage.append("  </tr>");
			bodyMessage.append("  <tr>");
			bodyMessage.append("   <td align=center>");
			bodyMessage.append("      <table cellpadding=\"2\" cellspacing=\"2\" style=\""+styleTable2+"\">");
			bodyMessage.append("       <tr>");
			bodyMessage.append("          <td style=\""+styleFontBody+"\" colspan=\"1\" >"+bodyText+"</td>");
			bodyMessage.append("        </tr>  ");
			
			if (addressOnly==null)
			{
				bodyMessage.append("        <tr>");
				bodyMessage.append("            <td style=\""+styleFont+"\">");  
				bodyMessage.append("                 <table cellpadding=\"0\" cellspacing=\"0\" style=\""+styleTable2+"\">");
				bodyMessage.append("			       <tr>");
				bodyMessage.append("			          <td style=\""+styleFontTitles+"\" nowrap><b>"+text1+"</b></td>");
				bodyMessage.append("	                  <td style=\""+styleFontTitles+"\" nowrap>"+selected_type_desc+"</td>");
				bodyMessage.append("	               </tr>");
				if (terminalVersions!=null && terminalVersions!="null")
				{
				  bodyMessage.append("	               <tr>");
				  bodyMessage.append("	                  <td style=\""+styleFontTitles+"\" nowrap><b>"+text2+"</b></td>");
				  bodyMessage.append("	                  <td style=\""+styleFontTitles+"\" nowrap>"+terminalVersions+"</td>");
				  bodyMessage.append("	               </tr>");
				}     
				bodyMessage.append("                 </table>"); 
				bodyMessage.append("             </td>");
				bodyMessage.append("         </tr>");
			}     
			bodyMessage.append("      </table>");
			bodyMessage.append("   </td>");
			bodyMessage.append("  </tr>");
			bodyMessage.append("  <tr>");
			bodyMessage.append("  <td style=\""+styleFont+"\" colspan=\"2\">");
			bodyMessage.append("    <a href=\""+intranetCustomerSetup+"/merchants/m_editTerminals.asp?site_id="+siteId+"&merch_id="+merchantId+"\">"+text4+"</a>");
			bodyMessage.append("   </td>");
			bodyMessage.append("  </tr>");
			bodyMessage.append("</table>");
			bodyMessage.append("</body>");

		    String note = Languages.getString("jsp.admin.customers.merchants_request_rebuild.note",SessionData.getLanguage());
		    Terminal.setMerchantId(merchantId);
		    Terminal.setSiteId(siteId);
		    Vector vecTerminalInfo = Terminal.getTerminalInfo();
		    note = note.replace("{1}", siteId);
		    if(selected_type_desc != null){
		    	note = note.replace("{2}", selected_type_desc);
		    }else{
		    	note = note.replace("{2}", "");
		    }
		    if(terminalVersions != null){
		    	note = note.replace("{3}", terminalVersions);
		    }else{
		    	note = note.replace("{3}", "");
		    }
		    note += ". " + bodyText;
		    if(note.length() > 255){
		    	note.substring(0,254);
		    }
		    Merchant.addMerchantNote(Long.parseLong(merchantId), note, SessionData.getUser().getUsername());

		    ValidateEmail.sendMail(requestRebuildForSite+" "+siteId,mailHost,mails,from,bodyMessage.toString(),instance);
		    messages = Languages.getString("jsp.admin.contact.index.success",SessionData.getLanguage());
		    
		    finishOk = true;
		}
		catch(Exception e)
		{
			messages = e.getMessage();
		   //out.println(e+" <br>");
		}
	}
	else 
	{
		//System.out.println("GET");
	}
	if(messages == null){messages = "";}
    boolean findType=false;
	Iterator itTerminalTypes = terminalTypes.iterator();
	while (itTerminalTypes.hasNext()) {
		Vector vecTemp = (Vector) itTerminalTypes.next();
		String strTerminalType = vecTemp.get(0).toString();
		String strDescription = vecTemp.get(1).toString();
		if (!strTerminalType.equals(type)) 
		{
			if(selected_type!=null && selected_type.equals(strTerminalType)){
			  optionsTerminalType.append("<option selected value=\""+ strTerminalType + "\">" + strDescription+ "</option>");
			  findType=true;
			}
			else{
			 optionsTerminalType.append("<option  value=\""+ strTerminalType + "**"+strDescription+"\">" + strDescription+ "</option>");
			}
		} 
		else 
		{
			currentTypeDescription = strDescription;
		}
	}
	
	if (!findType){
	  optionsTerminalType .append("<option selected  value=\"-1\">- - - -</option>");
	}
%>

<script type="text/javascript">

 function showTerminalVersion(value)
 {
    
  var selected_type = document.getElementById("selected_type");
  var chains= value.split("**");
  //alert(chains[0]);
  selected_type.value = chains[0];
  
  var selected_type_desc = document.getElementById("selected_type_desc");
  selected_type_desc.value = chains[1];
  //alert(selected_type_desc.value);
  
  document.form_rebuild.method="get";
  document.form_rebuild.submit();
 }
 
 
 function sendEmail()
 {
   var from = document.getElementById("from");
   var selected_type_desc = document.getElementById("selected_type_desc");
   
   if (from.value.length > 0 && selected_type_desc.value.length > 0)
   {
      submitForm();
   }
   else
   {
     var addressOnly = document.getElementById("addressOnly");
     if (addressOnly.checked==false)
     {
	     //alert(selected_type_desc.value);
	     if (selected_type_desc.value == "null")
	     {
	       alert("<%=terminalTypeValidate%>");
	       var terminalType = document.getElementById("terminalType");
	       terminalType.focus();
	       return false;
	     }
	     if (from.value.length == 0)
	     {
	       alert("<%=fromValidate%>");
	       from.focus();
	       return false;
	     }
     }
     else{
       if (from.value.length == 0)
	   {
	       alert("<%=fromValidate%>");
	       from.focus();
	       return false;
	   }
	   else
	   {
	     submitForm();
	   }       
     }
   }   
 }
 
 function submitForm(){
    document.form_rebuild.method="post";  
    document.form_rebuild.submit();
 }
 
 function disabledOthersControls(value)
 {
    var addressOnly = document.getElementById("addressOnly");
    var terminalType = document.getElementById("terminalType");
    var terminalVersions = document.getElementById("terminalVersions");
    
    var body = document.getElementById("bodyText");
    
    if (addressOnly.checked==true)
    {
      terminalType.disabled=true;
      terminalVersions.disabled=true;
      body.value = "<%=updateAddressOnlyLanguage%>";
    }
    else
    {
      terminalType.disabled=false;
      terminalVersions.disabled=false;
      body.value = "";
    }    
 }
</script>

<table id="masterTable" border="0" width="100%"	class="formAreaTitle">
	<tr>
	  <td>
		<table id="titleMaster" border="0" cellpadding="0" cellspacing="0" width="750">
	        <tr>
	          <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	          <td background="images/top_blue.gif" width="2000" class="formAreaTitle">
	            &nbsp;
	             <%=requestRebuildTitleLanguage.toUpperCase()%>
	          </td>
	          <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	        </tr>
	      </table>
	    </td>  
	</tr>
	<tr>
	  <td>
	    <a id="goback" name="goback" class="showLink" href="admin/customers/merchants_info.jsp?merchantId=<%= merchantId %>"><%=goToTerminalsLanguage%></a>
	  </td>	  
	</tr>
	<tr>
		<td colspan="3" bgcolor="#ffffff">
			<table id="dataMaster" border="0" cellpadding="5" cellspacing="0" width="100%" height="250px" bgcolor="#ffffff">
				<tr>
					<td class="formArea2" align="left">
						<form name="form_rebuild" method="post" action="admin/customers/merchants_request_rebuild.jsp">
						  <%  
						      String height="761";
						      if (finishOk){
						         height="261";
						      }						      					  
						  %>
							<table border="0" width="683" cellpadding="0" cellspacing="15" height="<%=height%>">
							    <tr>
									<td class="formAreaTitle2" align="center" colspan="2">
										<%=messages%>
									</td>									
								</tr>
								
							<%  if (!finishOk){ %>							
							
							    
							    
								<tr>
									<td class="formAreaTitle2" align="right">
										<%=siteIdLanguage%>
									</td>
									<td class="formArea2" align="left" >
										<%=siteId%>
									</td>
								</tr>

								<tr>
									<td class="formAreaTitle2" align="right">
										<%=currentTypeLanguage%>
									</td>
									<td class="formArea2" align="left">
										<%=currentTypeDescription%>
									</td>
								</tr>

								<tr>
									<td class="formAreaTitle2" align="right">
										<%=updateAddressOnlyLanguage%>
									</td>
									<td class="formArea2" align="left">
										<input id="addressOnly" name="addressOnly" type="checkbox" onclick="disabledOthersControls(this.value);" >
									</td>
								</tr>
								<tr>
									<td class="formAreaTitle2" align="right">
										<%=termTypeLanguage%>
									</td>
									<td class="formArea2" align="left">
										<select id="terminalType" name="terminalType" onchange="showTerminalVersion(this.value);">
											<%=optionsTerminalType%>
										</select>

									</td>
								</tr>

								<tr>
									<td class="formAreaTitle2" align="right">
										<%=termVersionLanguage%>
									</td>
									<td class="formArea2" align="left">
										<select id="terminalVersions" name="terminalVersions">
											<%=optionsTerminalVersions%>
										</select>
									</td>
								</tr>
								<tr>
									<td class="formAreaTitle2" align="right">
										<%=notesLanguage%>
									</td>
									<td class="formArea2" align="left">
									    <table width="460" height="273">
									       <tr>
									          <td>
									            <textarea id="bodyText" name="bodyText" cols="50" rows="15"></textarea>
									          </td>
									          <td></td>
									       </tr>					    
									    </table>
									</td>
								</tr>
								<tr>
								  <td class="formAreaTitle2" align="right"><%=customerSupportEmailLanguage %></td>
								  <td class="formArea2" align="left"><%=customerServiceEmail%></td>
								</tr>
								<tr>
									<td class="formAreaTitle2" align="right">
										<%=toLanguage%>
									</td>
									<td class="formArea2" align="left">
									    <table>
									       <tr>
									          <td>
									            <textarea id="mails" name="mails" cols="30" rows="5" ><%=email%></textarea>
									          </td>									          
									       </tr>
									       <tr>
									          <td  class="main"><%=sepateEmailsLanguage %></td>
									       </tr>						    
									    </table>
									</td>
								</tr>
								<tr>
									<td class="formAreaTitle2" align="right">
										<%=fromLanguage%>
									</td>
									<td class="formArea2" align="left">
									    <table>
									       <tr>
									          <td>
									            <input type="text" id="from" name="from" value="<%=email%>" />
									          </td>									          
									       </tr>
									       <tr>
									          <td  class="main"></td>
									       </tr>						    
									    </table>
									</td>
								</tr>
								
								<tr>
									<td class="main" align="center" colspan="2">
										<input type="button" value="<%=sendEmailLanguage%>" onclick="sendEmail();" />
										<input id="siteId" name="siteId" type="hidden" value="<%=siteId%>">
										<input id="type" name="type" type="hidden" value="<%=type%>">
										<input id="selected_type" name="selected_type" type="hidden">
										<input id="selected_type_desc" name="selected_type_desc" type="hidden" value="<%=selected_type_desc%>">
										<input id="merchantId" name="merchantId" value="<%=merchantId%>" type="hidden">
										<input id="email" name="email" value="<%=email%>" type="hidden">
										
									</td>
								</tr>
							 <% }
							    else
							    {
							        String urlBack = request.getRequestURL().toString().replaceFirst("merchants_request_rebuild.jsp","merchants_info.jsp")+"?merchantId="+merchantId;
     							    //System.out.println(urlBack);
							   %>
							    <script type="text/javascript">
							        function goBackTerminals(){
							          window.location="<%=urlBack%>";
							        }
							        setTimeout("goBackTerminals()",2000);							        
							    </script>
						    <% }%>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<%@ include file="/includes/footer.jsp"%>