<%@ page import="java.util.*,com.debisys.properties.Properties,
                 com.debisys.languages.*,
                 com.debisys.utils.LogChanges,
                 com.debisys.reports.PropertiesByFeature" %>
<%@page import="com.debisys.utils.StringUtil"%>
<%@page import="com.debisys.terminals.TerminalRegistration"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="ValidateEmail" class="com.debisys.utils.ValidateEmail" scope="page"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
<jsp:setProperty name="Terminal" property="*"/>
<%
int section = 2;
int section_page = 11;
LogChanges.setSession(SessionData);
LogChanges.setContext(application);
%>
<%@ include file="/includes/security.jsp" %>
<script type="text/javascript" src="/support/js/terminal.js"></script>
<script type="text/javascript" src="/support/js/jquery.js"></script>

<html>
	<head>
		<link href="../../default.css" type="text/css" rel="stylesheet">
		<script>
			function SetFrameSize(){
  				self.parent.SetFrameSize(document.getElementById("oBody").scrollWidth, document.getElementById("oBody").scrollHeight);
			}
			function SetMinSize(){
	  			self.parent.SetFrameSize(1250, 101);
			}
		</script>
	</head>
	<body id="oBody" bgcolor="#ffffff" onload="window.setTimeout('SetFrameSize()', 500)" onunload="SetMinSize()">
<%
Hashtable<String,String> terminalErrors = new Hashtable<String,String>();
if ( request.getParameter("save") != null ){
	if ( !Terminal.isRegistrationCodeUnique(StringUtil.toString(request.getParameter("edit")))){
		terminalErrors.put("registrationCode", Languages.getString("jsp.admin.customers.terminal.invalidregistrationcode",SessionData.getLanguage()));
	}
	
	if(Terminal.existRegistrationCode(application, SessionData, request)){
		terminalErrors.put("registrationCodeAlreadyExist", Languages.getString("jsp.admin.customers.terminal.alreadyexistregcode",SessionData.getLanguage()));
	}

	if (!Terminal.isUserIDUnique(StringUtil.toString(request.getParameter("edit")))){
		terminalErrors.put("userIdUnique", Languages.getString("jsp.admin.customers.terminal.uniqueuserid",SessionData.getLanguage()));
	}
	if ( !(Terminal.isChkRegCode() || Terminal.isChkCertificate() || Terminal.isChkIPRestriction()) ){
		//terminalErrors.put("securityRequired", Languages.getString("jsp.admin.customers.terminal.securityrequired",SessionData.getLanguage()));
	}
    if(! Terminal.validateRegCodeSendOptions()){
        terminalErrors.put("sendOptionRequired", Languages.getString("jsp.admin.customers.terminal.enforce_userRegCodeSendOptions",SessionData.getLanguage()));
    }

    if(Terminal.isChkRegCode() && Terminal.isChkConfirmationByEmail() && !ValidateEmail.isValidEmail(Terminal.getTxtConfirmationEMail())){
        terminalErrors.put("confirmationMailSyntaxError", Languages.getString("jsp.admin.customers.terminal.wrongSyntax_inputConfirmationEmail",SessionData.getLanguage()));
    }

    String phoneNumber = Terminal.getTxtConfirmationPhoneNumber();
    if(Terminal.isChkRegCode() && Terminal.isChkConfirmationBySMS() && phoneNumber != null && !phoneNumber.matches("\\d{9,14}")){
        terminalErrors.put("confirmationSmsSyntaxError", Languages.getString("jsp.admin.customers.terminal.wrongSyntax_inputConfirmationSms",SessionData.getLanguage()));
    }

	if(request.getParameter("mailNotificationtwo") == null || request.getParameter("mailNotificationtwo").equals("")) {
		terminalErrors.put("mailRequired", Languages.getString("jsp.admin.customers.terminal.mailrequired",SessionData.getLanguage()));
	}
	ValidateEmail.setStrEmailAddress(request.getParameter("mailNotificationtwo"));
	if(!request.getParameter("mailNotificationtwo").equals("") && !ValidateEmail.validateEmailList()){
		terminalErrors.put("mailSyntaxisError", Languages.getString("jsp.admin.customers.terminal.mailSyntaxisError",SessionData.getLanguage()));
	}
	if ( terminalErrors.size() == 0 ){
		if ( request.getParameter("save").length() > 0 ){
                        Terminal.updatePCTerminalRegistration(application, SessionData, request);
			Merchant.setMerchantId(Terminal.getMerchantId(Terminal.getSiteId()));
			Merchant.getMerchant(SessionData,application);
			String mailNotification = StringUtil.toString(request.getParameter("mailNotificationtwo")).trim();
			String sAccessLevel = SessionData.getProperty("access_level");
			String strRefId = SessionData.getProperty("ref_id");
			String typeRatePlan=""; 
			String mailHost = DebisysConfigListener.getMailHost(application);
			String userName = SessionData.getProperty("username");
			String companyName = SessionData.getProperty("company_name");
			String dba = Merchant.getDba();
			String contactEmail = Merchant.getContactEmail();
			String monthlyFee = Merchant.getMonthlyFee();
			Boolean PCTerm=Boolean.TRUE;
			String sURL="";
			String sTerminalDesc="";
			Vector vTerminalTypes = Terminal.getTerminalTypes_Combo(request.getParameter("terminalClass"));
			//String termTypeUI="";
			String terminalVersionForPcTerminal ="";
			String termTypeUI = request.getParameter("current_term_version");	
			Terminal.setMerchantId(Terminal.getMerchantId(Terminal.getSiteId()));
			Terminal.setSiteId(Terminal.getSiteId());
			if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
				Vector vecTerminalInfo = Terminal.getTerminalInfoMx();
				if (vecTerminalInfo != null && vecTerminalInfo.size() > 0){
					Terminal.setTerminalType(vecTerminalInfo.get(1).toString());
				}
			}else{
				Vector vecTerminalInfo = Terminal.getTerminalInfo();
				Terminal.setTerminalType(vecTerminalInfo.get(1).toString());
			}
			Iterator it = vTerminalTypes.iterator();
			while (it.hasNext()){
				Vector vTmp = (Vector) it.next();
				if (vTmp.get(0).toString().equals(Terminal.getTerminalType())){
					if (!vTmp.get(1).toString().equals("PC Terminal")){
						PCTerm=Boolean.FALSE;
					}else{
						Iterator itBrands = Terminal.getPCTerminalBrands(strRefId).iterator();
						while (itBrands.hasNext()){
							Vector vTemp = (Vector) itBrands.next();
							if (vTemp.get(0).toString().trim().equals(Terminal.getPcTermBrand())){
								if(termTypeUI.equalsIgnoreCase("V001")){
									sURL = (Terminal.isChkCertificate())? 
											Terminal.getBrandingProperties(Terminal.getPcTermBrand(), "PCTermSecureURL"): 
											Terminal.getBrandingProperties(Terminal.getPcTermBrand(), "PCTermURL")+"?b="+Terminal.getPcTermBrand();
								}else{
									sURL = (Terminal.isChkCertificate())? vTemp.get(3).toString().trim(): vTemp.get(2).toString().trim();
								}
								break;
							}
						}
					}
					sTerminalDesc = "[" + vTmp.get(0) + "] " + vTmp.get(1);
					break;
				}
			}
			String instance = DebisysConfigListener.getInstance(application);
			String urlDownCert = "";
			String initType = SessionData.getProperty("terminal_type");
			//if(termTypeUI.equalsIgnoreCase("V001") && !termTypeUI.equals(initType) && Terminal.isChkCertificate()){
			if(termTypeUI.equalsIgnoreCase("V001") && Terminal.isChkCertificate()){
				urlDownCert = Properties.getPropertyValue(instance, "global","downloadCertUrl");
			}
			//SessionData.setProperty("terminal_type", "");
			if(!mailNotification.equals("")){
				try{
                                    
                                    /*
                                    Terminal.sendMailNotificationUpdatePCTerm(mailHost,sAccessLevel, strRefId,companyName, userName,dba,
                                                contactEmail,monthlyFee,request.getParameter("terminalClass"), mailNotification,PCTerm,sURL,sTerminalDesc, 
                                                !DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO), 
                                                urlDownCert, true, application);
                                    */
                                    Terminal.sendMailNotificationUpdatePCTerm1(mailHost,sAccessLevel, strRefId,companyName, userName,dba,
                                                contactEmail,monthlyFee,request.getParameter("terminalClass"), mailNotification,PCTerm,sURL,sTerminalDesc, 
                                                !DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO), 
                                                urlDownCert, true, application, SessionData.getLanguage());
				}catch(Exception e){
					//can't connect to smtp host
					e.printStackTrace();
				}
			}
			response.sendRedirect("/support/admin/customers/pcterm_users.jsp?siteId=" + Terminal.getSiteId()+ "&terminalVersionForPcTerminal="+request.getParameter("terminalVersionForPcTerminal"));
		}else{
			Terminal.addPCTerminalRegistration(application, SessionData, request);
			
			Merchant.setMerchantId(Terminal.getMerchantId(Terminal.getSiteId()));
			Merchant.getMerchant(SessionData,application);
			String mailNotification = StringUtil.toString(request.getParameter("mailNotificationtwo")).trim();
			String sAccessLevel = SessionData.getProperty("access_level");
			String strRefId = SessionData.getProperty("ref_id");
			String typeRatePlan=""; 
			String mailHost = DebisysConfigListener.getMailHost(application);
			String userName = SessionData.getProperty("username");
			String companyName = SessionData.getProperty("company_name");
			String dba = Merchant.getDba();
			String contactEmail = Merchant.getContactEmail();
			String monthlyFee = Merchant.getMonthlyFee();
			Boolean PCTerm=Boolean.TRUE;
			String sURL="";
			String sTerminalDesc="";
			Vector vTerminalTypes = Terminal.getTerminalTypes_Combo(request.getParameter("terminalClass"));
			//String termTypeUI="";
			String terminalVersionForPcTerminal ="";
			String termTypeUI = request.getParameter("current_term_version");
			Terminal.setMerchantId(Terminal.getMerchantId(Terminal.getSiteId()));
			Terminal.setSiteId(Terminal.getSiteId());
			if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
				Vector vecTerminalInfo = Terminal.getTerminalInfoMx();
				if (vecTerminalInfo != null && vecTerminalInfo.size() > 0){
					Terminal.setTerminalType(vecTerminalInfo.get(1).toString());
				}
			}else{
				Vector vecTerminalInfo = Terminal.getTerminalInfo();
				Terminal.setTerminalType(vecTerminalInfo.get(1).toString());
			}				
				
			Iterator it = vTerminalTypes.iterator();
			while (it.hasNext()){
				Vector vTmp = (Vector) it.next();
				if (vTmp.get(0).toString().equals(Terminal.getTerminalType())){
					if (!vTmp.get(1).toString().equals("PC Terminal")){
						PCTerm=Boolean.FALSE;
					}else{
						Iterator itBrands = Terminal.getPCTerminalBrands(strRefId).iterator();
						while (itBrands.hasNext()){
							Vector vTemp = (Vector) itBrands.next();
							if (vTemp.get(0).toString().trim().equals(Terminal.getPcTermBrand())){
								if(termTypeUI.equalsIgnoreCase("V001")){
									sURL = (Terminal.isChkCertificate())? 
											Terminal.getBrandingProperties(Terminal.getPcTermBrand(), "PCTermSecureURL"): 
											Terminal.getBrandingProperties(Terminal.getPcTermBrand(), "PCTermURL")+"?b="+Terminal.getPcTermBrand();
								}else{
									sURL = (Terminal.isChkCertificate())? vTemp.get(3).toString().trim(): vTemp.get(2).toString().trim();
								}
								break;
							}
						}
					}
					sTerminalDesc = "[" + vTmp.get(0) + "] " + vTmp.get(1);
					break;
				}
			}
			String instance = DebisysConfigListener.getInstance(application);
			String urlDownCert = "";
			String initType = SessionData.getProperty("terminal_type");
			//if(termTypeUI.equalsIgnoreCase("V001") && !termTypeUI.equals(initType) && Terminal.isChkCertificate()){
			if(termTypeUI.equalsIgnoreCase("V001") && Terminal.isChkCertificate()){
				urlDownCert = Properties.getPropertyValue(instance, "global","downloadCertUrl");
			}
			//SessionData.setProperty("terminal_type", "");
			if(!mailNotification.equals("")){
				try{
					Terminal.sendMailNotificationUpdatePCTerm(mailHost,sAccessLevel, strRefId,companyName, userName,dba,contactEmail,monthlyFee, 
					request.getParameter("terminalClass"), mailNotification,PCTerm,sURL,sTerminalDesc, !DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO), urlDownCert, true, application);
				}catch(Exception e){
					//can't connect to smtp host
					e.printStackTrace();
				}
			}
		}
		terminalErrors = Terminal.getErrors();			  
	}
}//End of if ( request.getParameter("save") != null )

if ( StringUtil.toString(request.getParameter("add")).length() > 0 || StringUtil.toString(request.getParameter("edit")).length() > 0 || terminalErrors.size() > 0){
	if (terminalErrors.size() > 0){
		out.println("<table><tr class=main><td><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
		Enumeration<String> enum1 = terminalErrors.keys();
		while (enum1.hasMoreElements()){
			String strKey = enum1.nextElement().toString();
			String strError = (String)terminalErrors.get(strKey);
			if (strError != null && !strError.equals("")){
				out.println("<li>" + strError + "</li>");
			}
		}
		out.println("</font></td></tr></table>");    
	}

	Vector vResults = null;
	if ( request.getParameter("edit") != null ){
		vResults = (Vector)Terminal.getPCTerminalRegistrations(StringUtil.toString(request.getParameter("edit")), application).get(0);
		if ( request.getParameter("save") == null ){
			Terminal.setTxtRegistration(vResults.get(2).toString());
			Terminal.setPcTermUsername(vResults.get(5).toString());
			Terminal.setPcTermPassword(vResults.get(6).toString());
			Terminal.setChkRegCode(vResults.get(9).toString().equals("-1")?true:false);
			Terminal.setPcTermBrand(vResults.get(10).toString());
			Terminal.setChkIPRestriction(vResults.get(11).toString().equals("-1")?true:false);
			Terminal.setTxtIPAddress(StringUtil.toString(vResults.get(12).toString()));
			Terminal.setTxtSubnetMask(StringUtil.toString(vResults.get(25).toString()));
			Terminal.setTxtLoginErrorCount(StringUtil.toString(vResults.get(14).toString()));
			Terminal.setDdlRecordStatus(StringUtil.toString(vResults.get(13).toString()));
			Terminal.setChkCertificate(vResults.get(26).toString().equals("-1")?true:false);
			Terminal.setChkAllowCertificate(vResults.get(30).toString().equals("-1")?true:false);
			Terminal.setTxtAllowCertificate(StringUtil.toString(vResults.get(31).toString()));
            Terminal.setChkConfirmationByUser(vResults.get(33).toString().equals("-1")?true:false);
            Terminal.setChkConfirmationByEmail(vResults.get(34).toString().equals("-1")?true:false);
            Terminal.setChkConfirmationBySMS(vResults.get(35).toString().equals("-1")?true:false);
            Terminal.setTxtConfirmationEMail(StringUtil.toString(vResults.get(36).toString()));
            Terminal.setTxtConfirmationPhoneNumber(StringUtil.toString(vResults.get(37).toString()));

			LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_USERNAME, Terminal.getPcTermUsername());
			LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_PASSWORD, Terminal.getPcTermPassword());
			LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_ENFORCECERT, String.valueOf(Terminal.isChkCertificate()));
			LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_CERTINSTALL, String.valueOf(Terminal.isChkAllowCertificate()));
			LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_CERTREMAINING, Terminal.getTxtAllowCertificate());
			LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_ENFORCEREGCODE, String.valueOf(Terminal.isChkRegCode()));
			LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_REGCODE, Terminal.getTxtRegistration());
		}
		vResults.set(4, (StringUtil.toString(vResults.get(4).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(4).toString()));
		vResults.set(7, (StringUtil.toString(vResults.get(7).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(7).toString()));
		vResults.set(15, (StringUtil.toString(vResults.get(15).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(15).toString()));
		vResults.set(16, (StringUtil.toString(vResults.get(16).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(16).toString()));
		vResults.set(17, (StringUtil.toString(vResults.get(17).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(17).toString()));
		vResults.set(18, (StringUtil.toString(vResults.get(18).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(18).toString()));
		vResults.set(27, (StringUtil.toString(vResults.get(27).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(27).toString()));
		vResults.set(28, (StringUtil.toString(vResults.get(28).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(28).toString()));
		vResults.set(29, (StringUtil.toString(vResults.get(29).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(29).toString()));
		vResults.set(32, (StringUtil.toString(vResults.get(32).toString()).length() == 0)?"N/A":StringUtil.toString(vResults.get(32).toString()));
	}else{
		LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_USERNAME, "");
		LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_PASSWORD, "");
		LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_ENFORCECERT, "");
		LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_CERTINSTALL, "");
		LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_CERTREMAINING, "");
		LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_ENFORCEREGCODE, "");
		LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_REGCODE, "");
		vResults = new Vector();
		for ( int i = 0; i < 33; i++ ){
			vResults.add("");
		}
	}
%>
		<script>
			var error = false;
			var error_message = "";
			function check_input(field_name, field_size, message){
				if (document.getElementById(field_name) && (document.getElementById(field_name).type != "hidden")){
					var field_value = document.getElementById(field_name).value;
					if (field_value == '' || field_value.length < field_size){
						error_message = error_message + "* " + message + "\n";
						error = true;
					}
				}
			}//End of function check_input
			function check_form(){
				error = false;
				error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";
				check_input("pcTermBrand", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_brand",SessionData.getLanguage())%>");
				check_input("pcTermUsername", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_username",SessionData.getLanguage())%>");
				check_input("pcTermPassword", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_password",SessionData.getLanguage())%>");
<%
	if ( request.getParameter("edit") == null ){
%>
				if ( document.getElementById("chkManualRegCode").checked ){
					check_input("txtRegistration", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_regcode",SessionData.getLanguage())%>");
				}
<%
	}
%>
				if ( document.getElementById("chkIPRestriction").checked ){
					check_input("txtIPAddress", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_ipaddress",SessionData.getLanguage())%>");
					check_input("txtSubnetMask", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_subnetmask",SessionData.getLanguage())%>");
				}
				if ( document.getElementById("chkAllowCertificate").checked ){
					check_input("txtAllowCertificate", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_certamount",SessionData.getLanguage())%>");
				}
				if (error == true){
					alert(error_message);
					return false;
				}else{
					var ctl = document.getElementById("btnSubmit");
					if ( ctl != null ){
						ctl.disabled = true;
					}    
					ctl = document.getElementById("btnCancel");
					if ( ctl != null ){
						ctl.disabled = true;
					}    
					return true;
				}
			}//End of function check_form
		</script>
		<form action="/support/admin/customers/pcterm_users.jsp" method="post" onsubmit="return check_form(this);">
			<table width="100%">
				<tr><td></td><td>&nbsp;</td><td></td><td>&nbsp;&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.pc_term_brand",SessionData.getLanguage())%></td>
					<td></td>
					<td>
						<select id="pcTermBrand" name="pcTermBrand">
<%
	String strRefId = SessionData.getProperty("ref_id");
	Iterator itBrands = Terminal.getPCTerminalBrands(strRefId).iterator();
	out.println("<option value=''>" + Languages.getString("jsp.admin.customers.terminal.pc_term_brand_select",SessionData.getLanguage()) + "</option>");
	while ( itBrands.hasNext() ){
		Vector vTemp = (Vector)itBrands.next();
		String sConfigID = vTemp.get(0).toString().trim();
		String sName = vTemp.get(1).toString().trim();
		if (sConfigID.equals(Terminal.getPcTermBrand())){
			out.println("<option value=\"" + sConfigID + "\" selected>" + sName + "</option>");
		}else{
			out.println("<option value=\"" + sConfigID + "\">" + sName + "</option>");
		}
	}
        String language = "1";
        if( SessionData.getLanguage().equals("spanish") ){
            language = "2";
        } 
        String reportCode = "RecoveryPassword";
        String properyNameFeature = "RECOVERY_PASSWORD_SUPPORT_LABEL_"+language;
        HashMap<String, PropertiesByFeature> propertiesByFeature = PropertiesByFeature.findPropertyByCode(reportCode, properyNameFeature);
        String resetAnswersLabel = "Reset answers of the questions that are used in password recovery?";
        if ( propertiesByFeature.get(properyNameFeature) != null ){
            resetAnswersLabel = propertiesByFeature.get(properyNameFeature).getValue();
        }
%>
						</select>
					</td>
                                        <td></td>
                                        <td></td>
                                        
				</tr>
                                
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pc_term_username",SessionData.getLanguage())%></td>
					<td></td>
<%
/*LogChanges.setSession(SessionData);
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_USERNAME), Terminal.getPcTermUsername(), Terminal.getSiteId());
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_PASSWORD), Terminal.getPcTermPassword(), Terminal.getSiteId());
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_ENFORCECERT), String.valueOf(Terminal.isChkCertificate()), Terminal.getSiteId());
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CERTINSTALL), String.valueOf(Terminal.isChkAllowCertificate()), Terminal.getSiteId());
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CERTREMAINING), Terminal.getTxtAllowCertificate(), Terminal.getSiteId());
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_ENFORCEREGCODE), String.valueOf(Terminal.isChkRegCode()), Terminal.getSiteId());
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_REGCODE), Terminal.getTxtRegistration(), Terminal.getSiteId());
*/
%>
					<td nowrap="nowrap"><input type="text" ID="pcTermUsername" name="pcTermUsername" value="<%=Terminal.getPcTermUsername()%>" size="20" maxlength="15"></td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pc_term_password",SessionData.getLanguage())%></td>
					<td></td>
					<td>
                                            <input type="password" ID="pcTermPassword" name="pcTermPassword" value="<%=Terminal.getPcTermPasswordDecrypt(application)%>" size="20" maxlength="15" onblur="return ValidatePCTermPassword(this);">
                                            <p class="errorLabelPass"><%=Languages.getString("jsp.admin.customers.terminal.invalid.password1",SessionData.getLanguage())%></p>
						<script>
							function ValidatePCTermPassword(c){
                                                            function ValidatePCTermPassword(c) {
                                                              var exp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,12}$");
                                                               if (!exp.test(c.value))
                                                                {
                                                                  alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidpassword",SessionData.getLanguage())%>');
                                                                  window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
                                                                  return false;
                                                                }
                                                                return true;
                                                            }
							}
						</script>
					</td>                                        
				</tr>
<%
	if ( request.getParameter("edit") != null ){
%>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.loginerrorcount",SessionData.getLanguage())%></td>
					<td></td>
					<td>
						<input type="text" id="txtLoginErrorCount" name="txtLoginErrorCount" value="<%=Terminal.getTxtLoginErrorCount()%>" size="20" maxlength="3" onblur="ValidateLoginErrorCount(this)">
						<script>
							function ValidateLoginErrorCount(c){
								var exp = new RegExp("^([0-9])+$");
								if ( !exp.test(c.value) && (c.value.length > 0) ){
									alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidcertificateamount",SessionData.getLanguage())%>');
									window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
									return false;
								}
								if ( c.value.length > 0 ){
									if ( c.value != "<%=Terminal.getTxtLoginErrorCount()%>" && c.value > 0 ){
										alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidloginerrorcount",SessionData.getLanguage())%>');
										window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
										return false;
									}
								}
								return true;
							}//End of function ValidateLoginErrorCount
						</script>
					</td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.recordstatus",SessionData.getLanguage())%></td>
					<td></td>
					<td>
						<select name="ddlRecordStatus">
							<option value="" <%=(Terminal.getDdlRecordStatus().equals(""))?"selected":""%>>ACTIVE</option>
							<option value="1" <%=(Terminal.getDdlRecordStatus().equals("1"))?"selected":""%>>DISABLED</option>
						</select>
					</td>
				</tr>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.lastloginattempt",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(15).toString())%></b></td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.lastloginsuccess",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(16).toString())%></b></td>
				</tr>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.lastloginattemptip",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(17).toString())%></b></td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.lastloginsuccessip",SessionData.getLanguage())%></td>
					<td></td>
					<td>
						<b><%=StringUtil.toString(vResults.get(18).toString())%></b>
						<input type="hidden" name="chkManualRegCode" value="true">
					</td>
				</tr>


                <tr><td colspan="7"><hr width="70%"></td></tr>
				<tr class="main"><td colspan="3"><label><%=Languages.getString("jsp.admin.customers.terminal.enforce_regcode",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkRegCode" name="chkRegCode" onclick="document.getElementById('fieldsetEnforceConfirmationByUser').disabled = !this.checked;"></label></td></tr>

                 <tr class="main">
                    <td colspan="7">
                        <fieldset id="fieldsetEnforceConfirmationByUser" disabled style="margin:7px;">
                            <legend><%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationOptions",SessionData.getLanguage())%></legend>
                            <table>
                                <tr class="main">
                                    <td>
                                        <input type="checkbox" id="chkConfirmationByEmail" name="chkConfirmationByEmail"  onclick="document.getElementById('txtConfirmationEMail').disabled = !this.checked;" >&nbsp;
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationByEmail",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_inputConfirmationEmail",SessionData.getLanguage())%>
                                        <input type="text" id="txtConfirmationEMail" name="txtConfirmationEMail" value="<%=Terminal.getTxtConfirmationEMail()%>" size="50" maxlength="100"  disabled="disabled">
                                    </td>
                                </tr>
                                <%
                                    String supportInstance = DebisysConfigListener.getInstance(application);
                                    String enableSmsNotification = Properties.getPropertyValue(supportInstance, "global", "EnableSmsNotification");
                                    boolean isActiveSmsNotification = (enableSmsNotification != null && !enableSmsNotification.isEmpty() && enableSmsNotification.equalsIgnoreCase("true")) ? true : false;
                                    if(isActiveSmsNotification){
                                %>
                                <tr class="main">
                                    <td>
                                        <input type="checkbox" id="chkConfirmationBySMS" name="chkConfirmationBySMS" onclick="document.getElementById('txtConfirmationPhoneNumber').disabled = !this.checked;" >&nbsp;
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationBySMS",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_inputConfirmationPhoneNumber",SessionData.getLanguage())%>
                                        <input type="text" id="txtConfirmationPhoneNumber" name="txtConfirmationPhoneNumber" value="<%=Terminal.getTxtConfirmationPhoneNumber()%>" size="40" maxlength="50" disabled="disabled" >
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                                <tr class="main">
                                    <td colspan="2">
                                        <input type="checkbox" id="chkConfirmationByUser" name="chkConfirmationByUser">&nbsp;
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationByMerchant",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>

                        </fieldset>
                    </td>
                </tr>

<%
	}else{
%>

				<tr><td colspan="7"><hr width="70%"></td></tr>
				<tr class="main"><td colspan="3"><label><%=Languages.getString("jsp.admin.customers.terminal.enforce_regcode",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkRegCode" name="chkRegCode" onclick="document.getElementById('fieldsetEnforceConfirmationByUser').disabled = !this.checked;"></label></td></tr>
                <tr class="main">
                    <td colspan="7">
                        <fieldset id="fieldsetEnforceConfirmationByUser" disabled style="margin:7px;">
                            <legend><%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationOptions",SessionData.getLanguage())%></legend>
                            <table>
                                <tr class="main">
                                    <td>
                                        <input type="checkbox" id="chkConfirmationByEmail" name="chkConfirmationByEmail"  onclick="document.getElementById('txtConfirmationEMail').disabled = !this.checked;" >&nbsp;
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationByEmail",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_inputConfirmationEmail",SessionData.getLanguage())%>
                                        <input type="text" id="txtConfirmationEMail" name="txtConfirmationEMail" value="<%=Terminal.getTxtConfirmationEMail()%>" size="50" maxlength="100"  disabled="disabled">
                                    </td>
                                </tr>
                                <%
                                    String supportInstance = DebisysConfigListener.getInstance(application);
                                    String enableSmsNotification = Properties.getPropertyValue(supportInstance, "global", "EnableSmsNotification");
                                    boolean isActiveSmsNotification = (enableSmsNotification != null && !enableSmsNotification.isEmpty() && enableSmsNotification.equalsIgnoreCase("true")) ? true : false;
                                    if(isActiveSmsNotification){
                                %>
                                <tr class="main">
                                    <td>
                                        <input type="checkbox" id="chkConfirmationBySMS" name="chkConfirmationBySMS" onclick="document.getElementById('txtConfirmationPhoneNumber').disabled = !this.checked;" >&nbsp;
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationBySMS",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_inputConfirmationPhoneNumber",SessionData.getLanguage())%>
                                        <input type="text" id="txtConfirmationPhoneNumber" name="txtConfirmationPhoneNumber" value="<%=Terminal.getTxtConfirmationPhoneNumber()%>" size="40" maxlength="50" disabled="disabled" >
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                                <tr class="main">
                                    <td colspan="2">
                                        <input type="checkbox" id="chkConfirmationByUser" name="chkConfirmationByUser">&nbsp;
                                        <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationByMerchant",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>

                        </fieldset>
                    </td>
                </tr>
				<tr class="main">
					<td colspan="7">
						<label><%=Languages.getString("jsp.admin.customers.terminal.enter_manual_code",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkManualRegCode" name="chkManualRegCode" onclick="document.getElementById('txtRegistration').disabled = !this.checked;"></label>
						&nbsp;&nbsp;<%=Languages.getString("jsp.admin.customers.terminal.enter_manual_code_summary",SessionData.getLanguage())%>
						</td>
				</tr>
<%
	}
%>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationcode",SessionData.getLanguage())%></td>
					<td></td>
					<td>
					<input type="text" ID="txtRegistration" name="txtRegistration" <%=(request.getParameter("edit") != null)?"":"disabled"%> value="<%=Terminal.getTxtRegistration()%>" size="20" maxlength="15">
                                        <input type="hidden" id="CurrentTxtRegistration" name="CurrentTxtRegistration" value="<%=Terminal.getTxtRegistration()%>">
					</td>
				</tr>
<%
	if ( request.getParameter("edit") != null ){
%>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationattempt",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(8).toString())%></b></td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registration_ok",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=vResults.get(3).equals("1")?Languages.getString("jsp.admin.customers.terminal.pc_term_yes",SessionData.getLanguage()):Languages.getString("jsp.admin.customers.terminal.pc_term_no",SessionData.getLanguage())%></b></td>
				</tr>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationdate",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(4).toString())%></b></td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationip",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(7).toString())%></b></td>
				</tr>
<%
	}
%>
				<tr><td colspan="7"><hr width="70%"></td></tr>
				<tr class="main"><td colspan="7"><label><%=Languages.getString("jsp.admin.customers.terminal.enforce_iprestriction",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkIPRestriction" name="chkIPRestriction" onclick="document.getElementById('txtIPAddress').disabled = document.getElementById('txtSubnetMask').disabled = !this.checked;"></label>&nbsp;&nbsp;<%=Languages.getString("jsp.admin.customers.terminal.enforce_iprestriction_staticip",SessionData.getLanguage())%></td></tr>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.ipaddress",SessionData.getLanguage())%></td>
					<td></td>
					<td>
						<input type="text" ID="txtIPAddress" name="txtIPAddress" value="<%=Terminal.getTxtIPAddress()%>" size="20" maxlength="15" disabled="disabled" onblur="return ValidateIPAddress(this);">
						<script>
							function ValidateIPAddress(c){
								var exp = new RegExp("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
								if ( !exp.test(c.value) && (c.value.length > 0) ){
									alert('<%=Languages.getString("jsp.admin.invalidipaddress",SessionData.getLanguage())%>');
									window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
									return false;
								}
								if ( c.value.length > 0 ){
									var v = c.value.split(".");
									for (var s in v){
										if ( parseInt(v[s]) > 255 ){
											alert('<%=Languages.getString("jsp.admin.invalidipaddress",SessionData.getLanguage())%>');
											window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
											return false;
										}
									}
								}
								return true;
							}//End of function ValidateIPAddress
						</script>
					</td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.subnetmask",SessionData.getLanguage())%></td>
					<td></td>
					<td>
						<input type="text" ID="txtSubnetMask" name="txtSubnetMask" value="<%=Terminal.getTxtSubnetMask()%>" size="20" maxlength="19" disabled="disabled" onblur="return ValidateSubnetMask(this);">
						<script>
							function ValidateSubnetMask(c){
								var exp = new RegExp("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})(\\/(\\d{1,3}))*$");
								if ( !exp.test(c.value) && (c.value.length > 0) ){
									alert('<%=Languages.getString("jsp.admin.invalidsubnetmask",SessionData.getLanguage())%>');
									window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
									return false;
								}
								if ( c.value.length > 0 ){
									var v = c.value.split("/");
									v = v[0].split(".");
									for (var s in v){
										if ( parseInt(v[s]) > 255 ){
											alert('<%=Languages.getString("jsp.admin.invalidsubnetmask",SessionData.getLanguage())%>');
											window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
											return false;
										}
									}
								}
								return true;
							}//End of function ValidateSubnetMask
						</script>
					</td>
				</tr>
				<!-- There is a script for hiding this element -->
				<tr><td colspan="7"><hr width="70%"><input type="hidden" name="terminalVersionForPcTerminal" id = "terminalVersionForPcTerminal" value="<%=request.getParameter("terminalVersionForPcTerminal")%>"></td></tr>
                <tr class="main" id="enforceIsAdminCheck"><td colspan="3"><label id="idLabelIsAdmin"><%=Languages.getString("jsp.admin.customers.terminal.pcterminaluser.isAdmin",SessionData.getLanguage())%>&nbsp;
                	<input type="checkbox" id="chkIsAdmin" name="chkIsAdmin" class="chkIsAdmin" <%=(vResults.size()>=37 && vResults.get(38).toString().equalsIgnoreCase("1")) ? "checked=1":""%>></label></td></tr>
                <tr><td colspan="7"><hr width="70%"></td></tr>
				<tr class="main"><td colspan="3"><label><%=Languages.getString("jsp.admin.customers.terminal.enforce_certificate",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkCertificate" name="chkCertificate"></label></td></tr>
				<tr class="main"><td colspan="3"><label><%=Languages.getString("jsp.admin.customers.terminal.allow_certificate_install",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkAllowCertificate" name="chkAllowCertificate" onclick="document.getElementById('txtAllowCertificate').disabled = !this.checked;"></label></td></tr>
				<tr class="main">
					<td colspan="3">
						<%=Languages.getString("jsp.admin.customers.terminal.certificate_amount",SessionData.getLanguage())%>&nbsp;
						<input type="text" ID="txtAllowCertificate" name="txtAllowCertificate" value="<%=Terminal.getTxtAllowCertificate()%>" size="5" maxlength="3" disabled="disabled" onblur="ValidateTxtAllowCertificate(this)">
<%
	if ( request.getParameter("edit") != null ){
%>
						<script>
							function ValidateTxtAllowCertificate(c){
								var exp = new RegExp("^([0-9])+$");
								if ( !exp.test(c.value) && (c.value.length > 0) ){
									alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidcertificateamount",SessionData.getLanguage())%>');
									window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
									return false;
								}
								return true;
							}//End of function ValidateTxtAllowCertificate
						</script>
<%
	}else{
%>
						<script>
							function ValidateTxtAllowCertificate(c){
								var exp = new RegExp("^([0-9])+$");
								if ( !exp.test(c.value) && (c.value.length > 0) ){
									alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidcertificateamount",SessionData.getLanguage())%>');
									window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
									return false;
								}
								return true;
							}
						</script>
<%
	}
%>
					</td>
				</tr>
<%
	if ( request.getParameter("edit") != null ){
%>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.certinstalledcount",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(32).toString())%></b></td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.lastrequestdate",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(29).toString())%></b></td>
				</tr>
				<tr class="main">
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.laststatus",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(27).toString())%></b></td>
					<td></td>
					<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.laststatusdate",SessionData.getLanguage())%></td>
					<td></td>
					<td><b><%=StringUtil.toString(vResults.get(28).toString())%></b></td>
				</tr>

                                <tr><td colspan="7"><hr width="70%"></td></tr>
                                <tr class="main">                                    
                                    <td nowrap="nowrap" ><%=resetAnswersLabel%></td> 
                                    <td align="center"><input type="checkbox" ID="resetAnswers" name="resetAnswers" /></td>
                                </tr>
				<tr class="main">
					<td colspan="3">
						<%=Languages.getString("jsp.admin.customers.merchants_edit.mailnotification",SessionData.getLanguage())%>&nbsp;
						<input type="text" id="mailNotificationtwo" name="mailNotificationtwo" size="80" maxlength="256" value="<%=StringUtil.toString(request.getParameter("mailNotificationtwo")) %>" />
					</td>
				</tr>  
<%
	}else{
%>
				<tr class="main">
					<td colspan="3">
						<%=Languages.getString("jsp.admin.customers.merchants_edit.mailnotification",SessionData.getLanguage())%>&nbsp;
						<input type="text" id="mailNotificationtwo" name="mailNotificationtwo" size="80" maxlength="256" value="<%=StringUtil.toString(request.getParameter("mailNotificationtwo")) %>" />
					</td>
				</tr> 
<%
	}
%>
			</table>
<%
	if ( Terminal.isChkRegCode() ){
		out.print("<script>document.getElementById('chkRegCode').click();</script>");
	}
	if ( Terminal.isChkManualRegCode() ){
		out.print("<script>document.getElementById('chkManualRegCode').click();</script>");
	}
	if ( Terminal.isChkIPRestriction() ){
		out.print("<script>document.getElementById('chkIPRestriction').click();</script>");
	}
	if ( Terminal.isChkCertificate() ){
		out.print("<script>document.getElementById('chkCertificate').click();</script>");
	}
	if ( Terminal.isChkAllowCertificate() ){
		out.print("<script>document.getElementById('chkAllowCertificate').click();</script>");
	}
    if ( Terminal.isChkConfirmationByUser() ){
        out.print("<script>document.getElementById('chkConfirmationByUser').click();</script>");
    }
    if ( Terminal.isChkConfirmationByEmail() ){
        out.print("<script>document.getElementById('chkConfirmationByEmail').click();</script>");
    }
    if ( Terminal.isChkConfirmationBySMS() ){
        out.print("<script>document.getElementById('chkConfirmationBySMS').click();</script>");
    }
	String terminalVersionForPcTerminal ="";
	terminalVersionForPcTerminal = SessionData.getProperty("terminal_type");
	if ( SessionData.getUser().isQcommBusiness() && (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) && strAccessLevel.equals(DebisysConstants.ISO) ){
%>
			<input type="hidden" name="terminalClass" value="QComm">
<%
	} 
	if (!(SessionData.getUser().isQcommBusiness() && (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) && strAccessLevel.equals(DebisysConstants.ISO))){
%>
			<input type="hidden" name="terminalClass" value="Debisys">    
<%
	}
%>
			<input type="hidden" id="current_term_version" name="current_term_version" value="<%=terminalVersionForPcTerminal%>">
			<br><br>
			<input type="hidden" name="save" value="<%=StringUtil.toString(request.getParameter("edit"))%>">
			<input type="hidden" name="siteId" value="<%=Terminal.getSiteId()%>">
<%
	if ( StringUtil.toString(request.getParameter("edit")).length() == 0 ){
%>
			<input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.customers.terminal.add_pc_term",SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
<%
	}else{
%>
			<input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.customers.terminal.save_pc_term",SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
			<input type="hidden" name="edit" value="<%=StringUtil.toString(request.getParameter("edit"))%>">
<%
	}
%>
		</form>
		<form action="/support/admin/customers/pcterm_users.jsp" method="post">
			<input type="hidden" name="siteId" value="<%=Terminal.getSiteId()%>">
                        <input type="hidden" name="terminalVersionForPcTerminal" value="<%=request.getParameter("terminalVersionForPcTerminal")%>">
			<input id="btnCancel" type="submit" value="<%=Languages.getString("jsp.admin.cancel",SessionData.getLanguage())%>">
		</form>
<%
}else{//Else show results
	Vector vResults = Terminal.getPCTerminalRegistrations("", application);
	if ( vResults.size() > 0 ){
%>
		<table>
			<tr class="rowhead2">
				<td nowrap="nowrap">ID</td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.pc_term_brand",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pc_term_username",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.enforce_regcode",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationcode",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registration_ok",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationdate",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationip",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationattempt",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.logincount",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.iprestricted",SessionData.getLanguage()).toUpperCase()%></td>
				<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.certificaterestricted",SessionData.getLanguage()).toUpperCase()%></td>
				<td align="center" nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.pcterm_status",SessionData.getLanguage()).toUpperCase()%></td>
                                <%if(request.getParameter("terminalVersionForPcTerminal") != null && request.getParameter("terminalVersionForPcTerminal").equalsIgnoreCase("NG")){%>
                                    <td align="center" nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.isAdmin",SessionData.getLanguage()).toUpperCase()%></td>
                                <%}%>
			</tr>
<%
		Iterator it = vResults.iterator();
		int nEvenOdd = 1;
		while ( it.hasNext() ){
			Vector vTmp = (Vector)it.next();
%>
			<tr class="row<%=nEvenOdd%>">
				<td nowrap="nowrap"><a href="/support/admin/customers/pcterm_users.jsp?edit=<%=vTmp.get(0)%>&siteId=<%=Terminal.getSiteId()%>&terminalVersionForPcTerminal=<%=request.getParameter("terminalVersionForPcTerminal")%>"><%=vTmp.get(0)%></a></td>
				<td align="center" nowrap="nowrap"><%=vTmp.get(10)%></td>
				<td nowrap="nowrap"><%=vTmp.get(5)%></td>
				<td align="center" nowrap="nowrap"><%=vTmp.get(9).equals("-1")?Languages.getString("jsp.admin.customers.terminal.pc_term_yes",SessionData.getLanguage()):Languages.getString("jsp.admin.customers.terminal.pc_term_no",SessionData.getLanguage())%></td>
				<td nowrap="nowrap"><%=vTmp.get(2)%></td>
				<td align="center" nowrap="nowrap"><%=vTmp.get(3).equals("1")?Languages.getString("jsp.admin.customers.terminal.pc_term_yes",SessionData.getLanguage()):Languages.getString("jsp.admin.customers.terminal.pc_term_no",SessionData.getLanguage())%></td>
				<td nowrap="nowrap"><%=vTmp.get(4)%></td>
				<td nowrap="nowrap"><%=vTmp.get(7)%></td>
				<td nowrap="nowrap"><%=vTmp.get(8)%></td>
				<td nowrap="nowrap"><%=vTmp.get(14)%></td>
				<td align="center" nowrap="nowrap"><%=vTmp.get(11).equals("-1")?Languages.getString("jsp.admin.customers.terminal.pc_term_yes",SessionData.getLanguage()):Languages.getString("jsp.admin.customers.terminal.pc_term_no",SessionData.getLanguage())%></td>
				<td align="center" nowrap="nowrap"><%=vTmp.get(26).equals("-1")?Languages.getString("jsp.admin.customers.terminal.pc_term_yes",SessionData.getLanguage()):Languages.getString("jsp.admin.customers.terminal.pc_term_no",SessionData.getLanguage())%></td>
				<td align="center" nowrap="nowrap">
<%
                                if ( vTmp.get(3).equals("1") ){
                                        out.print(Languages.getString("jsp.admin.customers.terminal.pc_term_yes",SessionData.getLanguage()));
                                }else if ( Integer.parseInt(vTmp.get(8).toString()) > 3 ){
                                        out.print(Languages.getString("jsp.admin.customers.terminal.pc_term_no",SessionData.getLanguage()));
                                }else{
                                        out.print(Languages.getString("jsp.admin.customers.terminal.pc_term_pending",SessionData.getLanguage()));
                                }
%>
				</td>
                                <%if(request.getParameter("terminalVersionForPcTerminal")!= null && request.getParameter("terminalVersionForPcTerminal").equalsIgnoreCase("NG")){%>
                                <td align="center" nowrap="nowrap"><%=vTmp.get(38).equals("1")?Languages.getString("jsp.admin.customers.terminal.pc_term_yes",SessionData.getLanguage()):Languages.getString("jsp.admin.customers.terminal.pc_term_no",SessionData.getLanguage())%></td>
                                <%}%>
			</tr>
<%
			vTmp = null;
			nEvenOdd = (nEvenOdd == 1)?2:1;
		}
		it = null;
%>
		</table>
<%
    }else{
%>
		<table><tr><td class="main"><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></td></tr></table>
<%
	}
	vResults = null;
%>
		<br>
		<br>
		<form action="/support/admin/customers/pcterm_users.jsp?terminalVersionForPcTerminal=<%=request.getParameter("terminalVersionForPcTerminal")%>" method="post">
			<input type="hidden" name="add" value="0">
			<input type="hidden" name="siteId" value="<%=Terminal.getSiteId()%>">
			<input type="submit" value="<%=Languages.getString("jsp.admin.customers.terminal.add_pc_term",SessionData.getLanguage())%>">
		</form>
<%
}//End of else show results
%>
	<script type="text/javascript">
		var terminalVersion = "<%= request.getParameter("terminalVersionForPcTerminal") %>";
		if(terminalVersion !== 'NG'){
			$("#chkIsAdmin").hide();
			$('label#idLabelIsAdmin').hide();
		}
	</script>
	</body>
</html>
