<%@ page import="com.debisys.utils.*, com.debisys.languages.Languages,
				com.debisys.terminals.Terminal,
				java.util.*,java.util.ArrayList,
				com.debisys.users.User"%>
<%@page import="java.io.PrintWriter"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:setProperty name="Terminal" property="*"/>
<%
int section = 2;
int section_page = 11;
%>
<%@ include file="/includes/security.jsp" %>
<%
String sAccessLevel = SessionData.getProperty("access_level");
String product_id = request.getParameter("product_id");
String site_id = request.getParameter("siteid");
int intEvenOdd = 1;
int icount=0;
%>
<html>
<head>
	<title><%=Languages.getString("jsp.admin.reports.feeTiersTable.Title",SessionData.getLanguage())%> <%=product_id%></title>
	<link href="../../default.css" type="text/css" rel="stylesheet">
</head>
<body bgcolor="#ffffff">
<%if(strAccessLevel.equals(DebisysConstants.ISO)){
	if(product_id.length() > 0){
		Vector vecProductFeeTiers = Terminal.getProductFeeTiers(product_id, site_id);
		Iterator it = vecProductFeeTiers.iterator();
	%>
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td width="18" height="20">
					<img width="18" height="20" src="/support/images/top_left_blue.gif"/>
				</td>
				<td class="formAreaTitle" width="3000" background="/support/images/top_blue.gif"><%=Languages.getString("jsp.admin.reports.feeTiersTable.Title",SessionData.getLanguage()).toUpperCase()%> <%=product_id%></td>
			</tr>
			<tr>
				<td colspan="2">
					<br>
					<strong style="font-size:12px;"> <%=Languages.getString("jsp.admin.reports.feeTiersTable.note",SessionData.getLanguage())%></strong>
					<br><br>
				</td>
			</tr>
			<tr>
				<td class="main" bgcolor="#ffffff" colspan="3">
					<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
						<tr>
							<td class="formArea2">	
								<table class="formArea2" width="98%">
									<tr class="rowhead2">
										<td><%=Languages.getString("jsp.admin.reports.feeTiersTable.Vendor",SessionData.getLanguage()).toUpperCase()%></td>
										<td><%=Languages.getString("jsp.admin.reports.feeTiersTable.ProductPrice",SessionData.getLanguage()).toUpperCase()%></td>
										<td><%=Languages.getString("jsp.admin.reports.feeTiersTable.Broker",SessionData.getLanguage()).toUpperCase()%></td>
										<td><%=Languages.getString("jsp.admin.reports.feeTiersTable.Subbroker1",SessionData.getLanguage()).toUpperCase()%></td>
										<td><%=Languages.getString("jsp.admin.reports.feeTiersTable.Subbroker2",SessionData.getLanguage()).toUpperCase()%></td>
										<td><%=Languages.getString("jsp.admin.reports.feeTiersTable.Subbroker3",SessionData.getLanguage()).toUpperCase()%></td>
										<td><%=Languages.getString("jsp.admin.reports.feeTiersTable.Merchant",SessionData.getLanguage()).toUpperCase()%></td>
									</tr> 
<%
		while (it.hasNext()){
			icount++;
			Vector vecTemp = null;
			vecTemp = (Vector) it.next();
			out.print("<tr class=row" + intEvenOdd +">");
			for(int i=0;i<vecTemp.size();i++){
				out.print("<td>"+vecTemp.get(i).toString()+"</td>");
			}
			out.print("</tr>");
			if (intEvenOdd == 1){
				intEvenOdd = 2;
			}else{
				intEvenOdd = 1;
			}
		}//End of while (it.hasNext())
		vecProductFeeTiers.clear();
%>								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
<%	}else{%>
		<b>Product Id is not valid!</b>
<%	}
}else{%>
	<b>You don't have enough permissions to see this table!</b>
<%
}%>
</body>
</html>