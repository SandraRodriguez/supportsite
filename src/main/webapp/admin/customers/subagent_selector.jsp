<%@ page import="com.debisys.utils.EntityAccountTypes,
				 com.debisys.customers.CustomerSearch,
                 java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.languages.Languages" %>
<%
int section=2;
int section_page=10;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<html>
<head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title>Debisys</title>
<script language="JavaScript">
<!--
// Add the selected items in the parent by calling method of parent
function selectSubAgent(repId, repName, isUnlimited,CanChangeAccountType,creditType) {
self.opener.addToParent(repId, repName, isUnlimited,CanChangeAccountType,creditType);
window.close();
}
//-->
</script>

</head>
<body bgcolor="#ffffff" onload="window.focus()">
<%
Vector vecSearchResults = new Vector();
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String strParentRepName = "";

if (request.getParameter("search") != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }


  vecSearchResults = CustomerSearch.searchRep(intPage, intPageSize, SessionData, DebisysConstants.REP_TYPE_SUBAGENT);
  intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
  vecSearchResults.removeElementAt(0);
  if (intRecordCount>0)
  {
    intPageCount = (intRecordCount / intPageSize) + 1;
    if ((intPageCount * intPageSize) + 1 >= intRecordCount)
    {
      intPageCount++;
    }
  }
}
%>
<table border="0" cellpadding="0" cellspacing="0" width="485">
	<tr>
    <td width="18" height="20"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
    <td background="../../images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.subagent_selector.select_subagent",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="entityform" method="get" action="/support/admin/customers/subagent_selector.jsp" >
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
          <br>
	          <table border=0>
              <tr class="main">
               <td nowrap valign="top" width="100" colspan=2><%=Languages.getString("jsp.admin.narrow_results",SessionData.getLanguage())%>:</td>
               </tr>
               <tr class=main>
               <td valign="top" nowrap colspan=2>
               <input name="criteria" type="text" size="40" maxlength="64"><br>
               (<%=Languages.getString("jsp.admin.customers.subagents.search_instructions",SessionData.getLanguage())%>)
               </td>
               <td valign="top" align="left">
                  <input type="hidden" name="search" value="y">
                  <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
               </td>
              </tr>
              </form>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" cellspacing="1" cellpadding="1" border="0">
            <tr><td class="main" colspan=7><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
            <%
              if (!CustomerSearch.getCriteria().equals("")) {out.println(Languages.getString("jsp.admin.for",SessionData.getLanguage()) + " \"" + HTMLEncoder.encode(CustomerSearch.getCriteria()) + "\"");}
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount-1)},SessionData.getLanguage())%><br><%=Languages.getString("jsp.admin.customers.rep_selector.instructions",SessionData.getLanguage())%></td></tr>
            <tr>
              <td align=right class="main" nowrap colspan=7>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"/support/admin/customers/subagent_selector.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=1&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"/support/admin/customers/subagent_selector.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + (intPage-1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"/support/admin/customers/subagent_selector.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + i + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">" + i + "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"/support/admin/customers/subagent_selector.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + (intPage+1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">"+Languages.getString("jsp.admin.next",SessionData.getLanguage())+"&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"/support/admin/customers/subagent_selector.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&page=" + (intPageCount-1) + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8") + "\">"+Languages.getString("jsp.admin.last",SessionData.getLanguage())+"</a>");
              }

              %>
              </td>
            </tr>
            <tr>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.business_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="/support/admin/customers/subagent_selector.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=1&sort=1"><img src="../../images/down.png" height=11 width=11 border=0></a><a href="/support/admin/customers/subagent_selector.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=1&sort=2"><img src="../../images/up.png" height=11 width=11 border=0></a></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.contact",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="/support/admin/customers/subagent_selector.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=5&sort=1"><img src="../../images/down.png" height=11 width=11 border=0></a><a href="/support/admin/customers/subagent_selector.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=5&sort=2"><img src="../../images/up.png" height=11 width=11 border=0></a></td>
            	<td class=rowhead2><%=Languages.getString("jsp.admin.customers.subagents.sub_agent_id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="/support/admin/customers/subagent_selector.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=2&sort=1"><img src="../../images/down.png" height=11 width=11 border=0></a><a href="/support/admin/customers/subagent_selector.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=2&sort=2"><img src="../../images/up.png" height=11 width=11 border=0></a></td>
            </tr>
            <%
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    String creditType = (String)vecTemp.get(8);
                    boolean unlimitedParent = false;
                    if (creditType != null) {
                    	unlimitedParent = creditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED);
                    }
                    
					// Determine if the parent account is External/Internal
					// If External, child should be created as External
					boolean changeAllowed = EntityAccountTypes.SubsidiaryCanChange(String.valueOf(vecTemp.get(1)));
					
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td><a href=\"javascript:selectSubAgent('" + vecTemp.get(1) + "','" + vecTemp.get(0).toString().replaceAll("'","\\\\'") + "',"+ unlimitedParent + ","+changeAllowed+ ","+creditType+");\">" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</a></td>" +
                                "<td nowrap>" + vecTemp.get(4) + " " + vecTemp.get(5) + "</td>" +
                                "<td nowrap>" + vecTemp.get(1) + "</td>" +
                        "</tr>");
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>

<%
}
else if (intRecordCount==0 && request.getParameter("search") != null)
{
 out.println("<br><br><font color=ff0000>" +Languages.getString("jsp.admin.customers.subagents.not_found",SessionData.getLanguage())+ "</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
