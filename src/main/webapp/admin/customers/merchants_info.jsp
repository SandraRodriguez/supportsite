<%@ page import="java.util.*,
				com.debisys.utils.*,
				com.debisys.customers.CreditTypes,
				com.debisys.utils.TimeZone,
				com.debisys.customers.MerchantInvoicePayments,
				com.debisys.pincache.PcReservedBalance,
				com.debisys.pincache.Pincache,
				com.debisys.languages.Languages,
				com.debisys.terminals.Terminal" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.tools.AccountExecutive"%>
<%@page import="com.debisys.users.User"%>
<%
	int section      = 2;
	int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Merchant" property="*" />
<%@ include file="/includes/security.jsp"%>
<%-- rbuitrago: declaring vector reference to spare us a database call --%>
<%
Vector<Vector<String>> vecTerminals = new Vector<Vector<String>>();
Merchant.getMerchant(SessionData,application);
String entityId = Merchant.getMerchantId();
/******************************************************************************************/
/******************************************************************************************/
/*GEOLOCATION FEATURE*/
boolean hasPermissionGeolocation = SessionData.checkPermission(DebisysConstants.PERM_GEOLOCATION_FEAUTURE);
/******************************************************************************************/
/******************************************************************************************/
/*GEOLOCATION FEATURE*/

boolean hasPermissionManage_New_Rateplans = SessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS);
boolean bExternalRep=com.debisys.users.User.isExternalRepAllowedEnabled(SessionData);
boolean bMerchantAssignment=SessionData.checkPermission(DebisysConstants.PERM_CAN_SET_MERCHANT_ASSIGNMENT);
String iso_id = SessionData.getUser().getIsoId();
if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) ){
	SessionData.setProperty("iso_id", (new User()).getISOId(DebisysConstants.MERCHANT, entityId));
	SessionData.setProperty("iso_id", iso_id);
}

String strSite = request.getParameter("siteid");
if (strSite == null){
	strSite = "";
}
String action = "" + request.getParameter("action");
if(action.equals("setStatus")){
	String status = request.getParameter("status");
	if(status == null){
		status = "0";
	}
	String reason = request.getParameter("reason");
	if (strSite == null){
		strSite = "";
	}
	Merchant.setTerminalStatus(SessionData, application, Long.parseLong(strSite), Integer.parseInt(status), reason);
	String note;
	if(status.equals("0")){
		note = Languages.getString("com.debisys.terminals.jsp.terminaldisabled",SessionData.getLanguage()) + ". SiteID: " + strSite + ". " + reason ;
	}else{
		note = Languages.getString("com.debisys.terminals.jsp.terminalenabled",SessionData.getLanguage()) + ". SiteID: " + strSite + ". " + reason ;
	}
	Merchant.addMerchantNote(Long.parseLong(entityId), note, SessionData.getUser().getUsername());
	response.resetBuffer();
	response.flushBuffer();
	return;
}
String strMessage = request.getParameter("message");
if (strMessage == null){
	strMessage = "";
}
String strPhysicalTerminalSerial = request.getParameter("physicalTerminalSerial");
if (strPhysicalTerminalSerial == null){
	strPhysicalTerminalSerial = "";
}
//HashMap entityErrors = null;

if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
	Merchant.getMerchantMx(SessionData, application);
}
if (Merchant.isError()){
	response.sendRedirect("/support/message.jsp?message=4");
	return;
}
boolean permissionTerminalLabel = SessionData.checkPermission(DebisysConstants.PERM_ADD_EDIT_TERMINAL_LABEL);
boolean permissionTerminalAssoc = SessionData.checkPermission(DebisysConstants.PERM_ALLOW_MANAGE_TERMINAL_ASSOCIATIONS);
%>
<%@ include file="/includes/header.jsp"%>
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery.numeric.js"></script>
<script type="text/javascript" src="/support/js/merchants_info.js"></script>
<%if (permissionTerminalLabel){%>
    <style type="text/css">
        .linkAssociate {
            font-size: 13px;
            color: #2C0973;
            font-family:  Arial, Helvetica, sans-serif;
            text-decoration: none;
        }  
    </style>
<%}%>
<script type="text/javascript">     
	current_enabled = null;
	merchantId = '<%= entityId %>';
	terminaslsIsDisableMsg = "<%=Languages.getString("jsp.admin.customers.terminals.isdisabled",SessionData.getLanguage())%>";
	jsmsg1 = "<%=Languages.getString("jsp.admin.customers.merchants_info.jsmsg1",SessionData.getLanguage()) %>";
	jsmsg2 = "<%=Languages.getString("jsp.admin.customers.merchants_info.jsmsg2",SessionData.getLanguage()) %>";
	jsmsg3 = "<%=Languages.getString("jsp.admin.customers.merchants_info.jsmsg3",SessionData.getLanguage()) %>";
	jsmsg4 = "<%=Languages.getString("jsp.admin.customers.merchants_info.jsmsg4",SessionData.getLanguage()) %>";
	jsmsg5 = "<%=Languages.getString("jsp.admin.customers.merchants_info.jsmsg5",SessionData.getLanguage())%>";
	jsmsg6 = "<%=Languages.getString("jsp.admin.customers.merchants_info.jsmsg6",SessionData.getLanguage())%>";
	jsmsg7 = "<%=Languages.getString("jsp.admin.customers.merchants_info.jsmsg7",SessionData.getLanguage())%>";
	jsmsg8 = "<%=Languages.getString("jsp.admin.customers.merchants_info.jsmsg8",SessionData.getLanguage())%>";
	jsmsg9 = "<%= Languages.getString("jsp.admin.customers.merchants_info.jsmsg9",SessionData.getLanguage()) %>";
	jsmsg10 = " - <%= Languages.getString("jsp.admin.customers.merchants_info.jsmsg10",SessionData.getLanguage()) %>";
	errorMsg = "<%=Languages.getString("jsp.admin.pincache.requesterror",SessionData.getLanguage())%>";
	depInfo = "<%=DebisysConfigListener.getDeploymentType(application)+ "_" + DebisysConfigListener.getCustomConfigType(application)%>";
	saveButText = "<%=Languages.getString("jsp.admin.pincache.ok",SessionData.getLanguage())%>";
	cancelButText = "<%=Languages.getString("jsp.admin.pincache.cancel",SessionData.getLanguage())%>";
	rbDialogTitle = "<%=Languages.getString("jsp.admin.reservedbalance.title",SessionData.getLanguage())%>";
	toenable = "<%=Languages.getString("jsp.admin.pincache.toenable",SessionData.getLanguage())%>";
	todisable = "<%=Languages.getString("jsp.admin.pincache.todisable",SessionData.getLanguage())%>";
	
	function validate(c){
		if (isNaN(c.value)){
			alert('<%= Languages.getString("jsp.admin.error2",SessionData.getLanguage()) %>');
			c.focus();
			return (false);
		}else{
			c.value = formatAmount(c.value);
		}
	}

	function ValidatePrepaidLimitMX(cText){
		if ( (cText.value < 0) && (Math.abs(cText.value) > (<%=Merchant.getLiabilityLimit()%> - <%=Merchant.getRunningLiability()%>)) && (<%=Merchant.getRunningLiability()%> > 0) ){
			alert("<%=Languages.getString("jsp.admin.customers.reps_info.paymentgreaterthanavailable",SessionData.getLanguage())%>");
			cText.focus();
			cText.select();
			return;
		}
		if ( (cText.value < 0) && (Math.abs(cText.value) > <%=Merchant.getLiabilityLimit()%>) ){
			alert("<%=Languages.getString("jsp.admin.customers.reps_info.absnegativepaymentgreaterthanlimit",SessionData.getLanguage())%>");
			cText.focus();
			cText.select();
		}
	}//End of function ValidatePrepaidLimitMX

	function ValidateUnlimitedMX(cText){
		if ( Math.abs(cText.value) > <%=Merchant.getRunningLiability()%> ){
			alert("<%=Languages.getString("jsp.admin.customers.reps_info.unlimitedpaymentgreaterthanrunning",SessionData.getLanguage())%>");
			cText.focus();
			cText.select();
		}
	}//End of function ValidateUnlimitedMX

	function ValidateCreditLimitMX(cText){
		if ( (cText.value > 0) && (<%=Merchant.getRunningLiability()%> === 0) ){
			alert("<%=Languages.getString("jsp.admin.customers.reps_info.paymentnegative",SessionData.getLanguage())%>");
			cText.focus();
			cText.select();
			return;
		}
		if ( cText.value > <%=Merchant.getRunningLiability()%> ){
			alert("<%=Languages.getString("jsp.admin.customers.reps_info.unlimitedpaymentgreaterthanrunning",SessionData.getLanguage())%>");
			cText.focus();
			cText.select();
		}
	}//End of function ValidateCreditLimitMX
</script>
<script type="text/javascript">
  //DTU-369 Payment Notifications
  function disableChk()
  {
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
		<%
		if (Merchant.isPaymentNotificationSMS())
		{
		%>
		 	valuePayNotiSMSChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiSMSChk.checked=false;
		<%
		}
		%>
		valuePayNotiSMSChk.disabled=true;
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
		<%
		if (Merchant.isPaymentNotificationMail())
		{
		%>
		 	valuePayNotiMailChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiMailChk.checked=false;
		<%
		}
		%>
  		valuePayNotiMailChk.disabled=true;
  		var valuePayNotiChk = document.getElementById('paymentNotifications');
		<%
		if (Merchant.isPaymentNotifications())
		{
		%>
		 	valuePayNotiChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiChk.checked=false;
			valuePayNotiSMSChk.checked=false;
			valuePayNotiMailChk.checked=false;
		<%
		}
		%>
  		valuePayNotiChk.disabled=true;
  }
  
  //DTU-480: Low Balance Alert Message SMS and/or email
  function disableBalChk()
  {
    	var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		<%
		if (Merchant.isBalanceNotificationTypeValue())
		{
		%>
		 	valueBalNotiValueChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiValueChk.checked=false;
		<%
		}
		%>
		valueBalNotiValueChk.disabled=true;
    	var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		<%
		if (Merchant.isBalanceNotificationTypeDays())
		{
		%>
		 	valueBalNotiDaysChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiDaysChk.checked=false;
		<%
		}
		%>
		valueBalNotiDaysChk.disabled=true;		
  		var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		<%
		if (Merchant.isBalanceNotificationSMS())
		{
		%>
		 	valueBalNotiSMSChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiSMSChk.checked=false;
		<%
		}
		%>
		valueBalNotiSMSChk.disabled=true;
  		var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		<%
		if (Merchant.isBalanceNotificationMail())
		{
		%>
		 	valueBalNotiMailChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiMailChk.checked=false;
		<%
		}
		%>
  		valueBalNotiMailChk.disabled=true;
  		var valueBalNotiChk = document.getElementById('balanceNotifications');
		<%
		if (Merchant.isBalanceNotifications())
		{
		%>
		 	valueBalNotiChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiChk.checked=false;
			valueBalNotiSMSChk.checked=false;
			valueBalNotiMailChk.checked=false;
			valueBalNotiValueChk.checked=false;
			valueBalNotiDaysChk.checked=false;
		<%
		}
		%>
  		valueBalNotiChk.disabled=true;
  }  
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td colspan="3">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="18" height="20"><img src="images/top_left_blue.gif"
						width="18" height="20">
					</td>
					<td background="images/top_blue.gif" width="5000"
						class="formAreaTitle">&nbsp; <%= Languages.getString("jsp.admin.customers.merchants_info.merchant_detail",SessionData.getLanguage()).toUpperCase() %>
					</td>
					<td width="12" height="20"><img
						src="images/top_right_blue.gif">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#ffffff">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
				<tr bgcolor="ffffff">
					<td align=center class=main>
						<img src="images/information.png" border="0" valign="middle">
						<font color="ff0000">
<%
if (strMessage.equals("2")){
	out.println(Languages.getString("jsp.admin.customers.merchants_info.merchant_update_success",SessionData.getLanguage()));
}else if (strMessage.equals("5")){
	out.println(Languages.getString("jsp.admin.customers.merchants_info.terminal_update_success",SessionData.getLanguage()) + "<br><br>" 
		+ Languages.getString("jsp.admin.customers.merchants_info.terminal_update_success_replication",SessionData.getLanguage()));
}else if (strMessage.equals("6")){
	out.println(Languages.getString("jsp.admin.customers.merchants_info.terminal_add_success",SessionData.getLanguage()) + "  "+strSite);
}else if (strMessage.equals("7")){
	out.println(Languages.getString("jsp.admin.customers.merchants_info.terminal_add_success",SessionData.getLanguage()) + "  "+strSite);
	out.println(Languages.getString("jsp.admin.customers.merchants_info.physicalTerminalAssigned",SessionData.getLanguage()) + "  "+strPhysicalTerminalSerial);
}else if (strMessage.equals("8")){
	out.println(Languages.getString("jsp.admin.customers.merchants_info.terminal_update_success",SessionData.getLanguage()) + "  "+strSite);
	out.println(Languages.getString("jsp.admin.customers.merchants_info.physicalTerminalAssigned",SessionData.getLanguage()) + "  "+strPhysicalTerminalSerial+"<br><br>" + Languages.getString(
		"jsp.admin.customers.merchants_info.terminal_update_success_replication",SessionData.getLanguage()));
}else if ( strMessage.equals("22") ){
	out.println(Languages.getString("jsp.admin.customers.merchants_info.paymenterror",SessionData.getLanguage()) + request.getParameter("code"));
}else if ( strMessage.equals("23") ){
	out.println(Languages.getString("jsp.admin.customers.merchants_info.paymentsuccessful",SessionData.getLanguage()));
}else if ( strMessage.equals("25") ){
	out.println(Languages.getString("jsp.admin.customers.merchants_info.terminalStatusUpdated",SessionData.getLanguage()));
}else if (strMessage.equals("26")){
	out.println("" + Languages.getString("jsp.admin.saleslimit.globalLimitLowerDailyLimit",SessionData.getLanguage()));
}
%>
						</font>
					</td>
				</tr>
				<tr>
					<td class="formArea2">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<table border="0" width="50" cellpadding="0" cellspacing="0">
										<tr>
											<%
if (SessionData.checkPermission(DebisysConstants.PERM_MERCHANTS)) {
%>
											<td>
												<form method="post"
													action="admin/customers/merchants_edit.jsp?merchantId=<%= Merchant.getMerchantId() %>"
													onsubmit="DisableButtons();">
													<input type="hidden" name="merchantId"
														value="<%= entityId %>"> <input
														id="btnEditMerchant" type="submit" name="submit"
														value="<%= Languages.getString("jsp.admin.customers.merchants_info.edit_merchant_info",SessionData.getLanguage()) %>">
												</form></td>
<%
}
if (SessionData.checkPermission(DebisysConstants.PERM_NOTES) && !strAccessLevel.equals(DebisysConstants.MERCHANT)) {//removed: &&  !strAccessLevel.equals(DebisysConstants.REP)
%>
											<td>
												<form>
													<input type="button" name="view_merchant_notes"
														value="<%= Languages.getString("jsp.admin.customers.merchants_info.view_merchant_notes",SessionData.getLanguage()) %>"
														onClick="window.open('/support/admin/customers/merchants_notes.jsp?merchantId=<%= Merchant.getMerchantId() %>','merchantNotes','width=650,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
												</form>
											</td>
<%
}
if (SessionData.checkPermission(DebisysConstants.PERM_ADD_NOTES) && !strAccessLevel.equals(DebisysConstants.MERCHANT)) {//removed:  &&  !strAccessLevel.equals(DebisysConstants.REP)
	String businessName = Merchant.getBusinessName().replace("'","");
%>
											<td>
												<form>
													<input type="button" name="add_merchant_notes"
														value="<%= Languages.getString("jsp.admin.customers.merchants_info.add_merchant_notes",SessionData.getLanguage()) %>"
														onClick="window.open('/support/admin/customers/merchants_notes_add.jsp?merchantId=<%=Merchant.getMerchantId()%>&businessName=<%= businessName %>','merchantNotes','width=400,height=300,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
												</form>
											</td>
<%
}
%>

										</tr>
									</table>
									<table width="100%">
										<tr>
											<td class="formAreaTitle2"><%= Languages.getString("jsp.includes.menu.merchant_info",SessionData.getLanguage()) %>
											</td>
										</tr>
										<tr>
											<td class="formArea2">
												<table>
													<tr>
														<td class="main"><b><%= Languages.getString("jsp.admin.customers.merchants_info.rep_name",SessionData.getLanguage()) %>:</b>
														</td>
														<td class="main"><%= Merchant.getRepName() %></td>
													</tr>
													<tr>
														<td class="main"><b><%= Languages.getString("jsp.admin.customers.merchants_info.business_name",SessionData.getLanguage()) %>:</b>
														</td>
														<td class="main"><%= Merchant.getBusinessName() %></td>
													</tr>
													
<%
boolean isDomestic = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
boolean permissionEnableEditMerchantAccountExecutives= SessionData.checkPermission(DebisysConstants.PERM_Enable_Edit_Merchant_Account_Executives);
if (permissionEnableEditMerchantAccountExecutives){
	AccountExecutive account = AccountExecutive.findExecutiveById(Merchant.getExecutiveAccountId(),SessionData);
	String nameExecutive =  (account.getNameExecutive()!=null?account.getNameExecutive():"");
%>
													<tr id="ERIControl">
														<td class="main"><b><%=Languages.getString("jsp.admin.reports.tools.accountexecutives.label",SessionData.getLanguage())%>:</b>
														</td>
														<td class="main"><%= nameExecutive %></td>
													</tr>
<%
}
%>
													<tr>
														<td class="main"><b><%= Languages.getString("jsp.admin.customers.merchants_info.dba",SessionData.getLanguage()) %>:</b>
														</td>
														<td class="main"><%= Merchant.getDba() %></td>
													</tr>
											
<%
if (SessionData.checkPermission(DebisysConstants.PERM_REGULATORY_FEE)) {
%>
													<tr>
														<td class="main"><b><%=Languages.getString("jsp.admin.customers.merchants_edit.regulatoryFee",SessionData.getLanguage())%>:</b>
														</td>
														<td nowrap="nowrap" class="main"><%= Merchant.getRegulatoryFeeName() %>
														</td>
													</tr>
<%
}
if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
	//If when deploying in Mexico
%>
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.businesstype",SessionData.getLanguage()) %>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main">
<%
	Vector   vecBusinessTypes = com.debisys.customers.Merchant.getBusinessTypes(customConfigType);
	Iterator itBusinessTypes  = vecBusinessTypes.iterator();
	while (itBusinessTypes.hasNext()) {
		Vector vecTemp = null;
		vecTemp = (Vector)itBusinessTypes.next();
		String strBusinessTypeId   = vecTemp.get(0).toString();
		String strBusinessTypeCode = vecTemp.get(1).toString();
		if (strBusinessTypeId.equals(Merchant.getBusinessTypeId())) {
			out.println(strBusinessTypeCode);
			break;
		}
	}
%>
														</td>
													</tr>
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment",SessionData.getLanguage()) %>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main">
<%
	Vector   vecMerchantSegments = com.debisys.customers.Merchant.getMerchantSegments();
	Iterator itMerchantSegments  = vecMerchantSegments.iterator();
	while (itMerchantSegments.hasNext()) {
		Vector vecTemp = null;
		vecTemp = (Vector)itMerchantSegments.next();
		String strMerchantSegmentId   = vecTemp.get(0).toString();
		String strMerchantSegmentCode = vecTemp.get(1).toString();
		if (strMerchantSegmentId.equals(Integer.toString(Merchant.getMerchantSegmentId()))) {
			out.println(Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" +
			strMerchantSegmentCode,SessionData.getLanguage()));
			break;
		}
	}
%>
														</td>
													</tr>
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.businesshours",SessionData.getLanguage()) %>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main"><%= Merchant.getBusinessHours() %>
														</td>
													</tr>
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.Region",SessionData.getLanguage()) %>:</b>
														</td>
<%
	Vector vecRegions = com.debisys.customers.Merchant.getRegions(customConfigType);
	Iterator itRegion = vecRegions.iterator();
	String region="";
	while (itRegion.hasNext()){
		Vector vecTemp = null;
		vecTemp = (Vector)itRegion.next();
		String strRegId = vecTemp.get(0).toString();
		String strRegName = vecTemp.get(1).toString();
		if ( strRegId.equals( Integer.toString(Merchant.getRegion())) ){
			region =  strRegName;
		}
	}
%>

														<td nowrap="nowrap" CLASS="main"><%=region%></td>
													</tr>
													<!-- new fields DBSY-570. added by JA-->
													
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.control_number",SessionData.getLanguage())%>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main"><%=(Merchant.getControlNumber() == null)?"":Merchant.getControlNumber()%>
														</td>
													</tr>																																	
													<!-- END new fields DBSY-570-->
<%
}//End deploy Mexico 
%>
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.merchant_classification",SessionData.getLanguage())%>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main">
														<%
															Iterator itMerchClassifications = Merchant.getMerchantClassifications(SessionData).iterator();
															while(itMerchClassifications.hasNext()){
																Vector vecTemp = (Vector)itMerchClassifications.next();
																String merchClassificationID = vecTemp.get(0).toString();
																String merchClassificationValue = vecTemp.get(1).toString();
																if(merchClassificationID.equals( String.valueOf(Merchant.getMerchantClassification()))){
																	out.println(merchClassificationValue);
																}
															}
														%>
														<div style="<%=(Merchant.getMerchantClassificationOtherDesc()==null || Merchant.getMerchantClassificationOtherDesc().trim().equals(""))?"display:none":"display:inline"%>" id="merchantClassificationOtherDescDiv">
					      					      		<b id="merchantClassificationOtherDescB">   <%=Languages.getString("jsp.admin.customers.merchants_edit.merchantClassificationOtherDesc",SessionData.getLanguage())%>:</b>
					      					      		<%=(Merchant.getMerchantClassificationOtherDesc()==null)?"":Merchant.getMerchantClassificationOtherDesc()%>
														</div>
														</td>
													</tr>
													
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.control_number",SessionData.getLanguage())%>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main"><%=(Merchant.getControlNumber() == null)?"":Merchant.getControlNumber()%>
														</td>
													</tr>	


<% if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && !Merchant.getAchSchedule().equals("") && !Merchant.getAchScheduleType().equals("")) {
%>
													<tr>
														<td class="main"><b><%= Languages.getString("jsp.admin.customers.merchants_info.ach_schedule",SessionData.getLanguage()) %>:</b>
														</td>
														<td nowrap="nowrap" class="main">
<%
	if (Merchant.getAchScheduleType().equalsIgnoreCase("m")) {
		out.print(Languages.getString("jsp.admin.customers.merchants_info.ach_schedule_type1",SessionData.getLanguage()) +
			" - " + Merchant.getAchSchedule());
	} else if (Merchant.getAchScheduleType().equalsIgnoreCase("d")) {
		out.print(Languages.getString("jsp.admin.customers.merchants_info.ach_schedule_type2",SessionData.getLanguage()) +
			" - " + Merchant.getAchSchedule());
	}
%>
														</td>
													</tr>
<%
}
%>
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZone",SessionData.getLanguage())%>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main"><%=TimeZone.getTimeZoneName(Merchant.getTimeZoneId())%></td>
													</tr>
<%
// DBSY-931 System 2000 Tools s2k
if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
		&&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
		&& strAccessLevel.equals(DebisysConstants.ISO)
		&& SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
	) { //If when deploying in International and user is an ISO level
%>
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.tools.s2k.addedit.terms",SessionData.getLanguage())%>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main"><%=Merchant.getterms()%></td>
													</tr>
													<tr>
														<td CLASS="main"><b><%= Languages.getString("jsp.admin.customers.salesmanid",SessionData.getLanguage())%>:</b>
														</td>
														<td nowrap="nowrap" CLASS="main"><%=Merchant.getsalesmanid()%>
															<b> <%= Languages.getString("jsp.admin.customers.salesmanname",SessionData.getLanguage())%>:
														</b> <%=Merchant.getsalesmanname()%></td>
													</tr>
<%
}

// DBSY-1072 eAccounts Interface
if(SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
%>
													<tr>
														<td class="main"><b><%=Languages.getString("jsp.admin.customers.entity_account_type",SessionData.getLanguage())%>:</b>
														</td>
														<td class="main"><%=EntityAccountTypes.GetEntityAccountTypeDesc(Merchant.getEntityAccountType())%></td>
													</tr>
<%
}
if(bExternalRep) {
%>
													<tr>
														<td class="main"><b><%=Languages.getString("jsp.admin.customers.allowExternalReps",SessionData.getLanguage())%>:</b>
														</td>
														<td class="main"><%=(Merchant.getMerchantPermissions() == 1)?"Yes":"No"%></td>
													</tr>
<%
}
 if(bMerchantAssignment && ( strAccessLevel.equals(DebisysConstants.ISO) ||  strAccessLevel.equals(DebisysConstants.AGENT))){ 
%>
													<tr>
														<td class="main"><b><%=Languages.getString("jsp.admin.customers.allowMerchantAssignment",SessionData.getLanguage())%>:</b>
														</td>
														<td class="main"><%=(Merchant.getMerchantExternalOutsideHierarchy() == 1)?"Yes":"No"%></td>
													</tr>
	
<%
}
if (hasPermissionManage_New_Rateplans){
	String labelUseRatePlanModel = Languages.getString("jsp.admin.customers.merchants_info.merchantTypeModel",SessionData.getLanguage());
	if (Merchant.isUseRatePlanModel()){
		labelUseRatePlanModel = labelUseRatePlanModel+ " "+ Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelISO",SessionData.getLanguage());
	}else{
		labelUseRatePlanModel = labelUseRatePlanModel+ " "+ Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelEmida",SessionData.getLanguage());
	}
%>
													<tr>
														<td CLASS="main" colspan="2"><b><%=labelUseRatePlanModel%></b>
															<br /> <%=Merchant.getWarningChangeModel() %></td>

													</tr>
<%
}
%>
												</table></td>
										</tr>
<%
if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
	//If when deploying in Mexico
%>
										<tr>
											<td class="formAreaTitle2"><%=Languages.getString("jsp.admin.customers.merchants_add.Invoice_title",SessionData.getLanguage())%>
											</td>
										</tr>	
										<tr>
											<td class="formArea2" >
												<table>
													<tr class="main">
														<td><b><%=Languages.getString("jsp.admin.customers.merchants_edit.Invoice_type",SessionData.getLanguage())%>:</b>
														</td>
														<td nowrap="nowrap" >
<%
	boolean showInvoiceTypes = false;
	Iterator itInvoiceTypes = Merchant.getInvoiceTypes().iterator();
	while(itInvoiceTypes.hasNext()){
		Vector vecTemp = (Vector)itInvoiceTypes.next();
		String strInvoiceTypeId = vecTemp.get(0).toString();
		String strInvoiceTypeValue = vecTemp.get(1).toString();
		String strCodeType = vecTemp.get(2).toString();
		if(strInvoiceTypeId.equals(String.valueOf(Merchant.getInvoice()))){
			System.out.println(strCodeType);
			if (!strCodeType.equals("NINV")){
				showInvoiceTypes = true;
			}
			out.println(strInvoiceTypeValue);
		}
	}
%>
														</td>
													</tr>
<%
 if ( showInvoiceTypes ){ 
	ArrayList<MerchantInvoicePayments> merchantPaymentsTypes = MerchantInvoicePayments.findMerchantInvoicePayments(Merchant.getMerchantId());
	if ( merchantPaymentsTypes != null && merchantPaymentsTypes.size() > 0){														
%>
													<tr>
														<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_add.paymentTypeInvoice",SessionData.getLanguage())%></td>
														<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_add.paymentAccountInvoiceNumber",SessionData.getLanguage())%></td>
													</tr>
<% 
		int classTr=1;
		for(MerchantInvoicePayments mMerchantInvoicePayments : merchantPaymentsTypes){
%>
													<tr class="row<%=classTr%>">
														<td><%=mMerchantInvoicePayments.getDescription()%></td>
														<td><%=mMerchantInvoicePayments.getAccountNumber()%></td>
													</tr>
<% 
			if ( classTr == 1){
				classTr = 2;
			}else{
				classTr = 1;}
			}
		}
	}
%>
												</table>
											</td>
										</tr>
<%
}
%>
										<tr>
											<td class="formAreaTitle2"><br> <%= Languages.getString("jsp.admin.customers.merchants_info.address",SessionData.getLanguage()) %>
											</td>
										</tr>
										<tr>
											<td class="formArea2">
												<table width="100">
													<tr class="main">
<%
if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
	//If when deploying in Mexico
%>
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.street",SessionData.getLanguage()) %>:</b>
														</td>
														<td colspan="4"><%= Merchant.getPhysAddress() %></td>
<%
} else {
	//Else when deploying in others
%>
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.street",SessionData.getLanguage()) %>:</b>
														</td>
														<td nowrap="nowrap"><%= Merchant.getPhysAddress() %>
														</td>
<%
}
%>
													</tr>
<%
if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
	//If when deploying in Mexico
%>
													<tr CLASS="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getPhysColony() %></td>
														<td>&nbsp;</td>
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getPhysDelegation() %></td>
													</tr>
													<tr CLASS="main">
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getPhysCity() %></td>
														<td>&nbsp;</td>
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getPhysZip() %></td>
														<td>&nbsp;</td>
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage()) %></b></td>
														<td nowrap="nowrap">
<%
	Vector   vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
	Iterator itStates  = vecStates.iterator();
	while (itStates.hasNext()) {
		Vector vecTemp = null;
		vecTemp = (Vector)itStates.next();
		String strStateId   = vecTemp.get(0).toString();
		String strStateName = vecTemp.get(1).toString();
		if (strStateId.equals(Merchant.getPhysState())) {
			out.println(strStateName);
			break;
		}
	}
%>
														</td>
													</tr>
<%
} else {
	//Else when deploying in others
%>
													<tr class="main">
														<td nowrap="nowrap"><b>
<%
	if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
		out.println(Languages.getString("jsp.admin.customers.merchants_info.city_state_zip",SessionData.getLanguage()) + ":");
	} else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
		out.println(Languages.getString("jsp.admin.customers.merchants_info.city_province_postal_code",SessionData.getLanguage()) + ":");
	}
%>
															</b></td>
														<td nowrap="nowrap"><%= Merchant.getPhysCity() + ", " + Merchant.getPhysState() + " " + Merchant.getPhysZip() %>
														</td>
													</tr>
<%
}
%>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.county",SessionData.getLanguage()) %>:</b>
														</td>
														<td nowrap="nowrap"><%= Merchant.getPhysCounty() %></td>
													</tr>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.country",SessionData.getLanguage()) %>:</b>
														</td>
														<td nowrap="nowrap"><%= CountryInfo.getAllCountriesID().get(Integer.parseInt(Merchant.getPhysCountry())) %>
														</td>
													</tr>

<%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && hasPermissionGeolocation){
%>
													<tr class="main">
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.latitude",SessionData.getLanguage())%>:</td>
														<td nowrap="nowrap"><%=Merchant.getLatitude()%></td>
													</tr>
													<tr class="main">
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.longitude",SessionData.getLanguage())%>:</td>
														<td nowrap="nowrap"><%=Merchant.getLongitude()%></td>
													</tr>
<%
}
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
	out.println("<tr class=\"main\">");
	out.println("<td><b>" + Languages.getString("jsp.admin.customers.merchants_edit.route",SessionData.getLanguage()) + ":</td></b>");
	out.println("<td nowrap>");
	out.println(Merchant.getMerchantRouteName());
	out.println("</td>");
	out.println("</tr>");
}
%>
												</table></td>
										</tr>
<%
if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
	//If when deploying in Mexico
%>
										<tr>
											<td CLASS="formAreaTitle2"><bR> <%= Languages.getString("jsp.admin.customers.merchants_edit.billingaddress",SessionData.getLanguage()) %>
											</td>
										</tr>
										<tr>
											<td class="formArea2">
												<table width="100">
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_edit.street_address",SessionData.getLanguage()) %>:</b></td>
														<td COLSPAN="4"><%= Merchant.getMailAddress() %></td>
													</tr>
													<tr CLASS="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getMailColony() %></td>
														<td>&nbsp;</td>
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getMailDelegation() %></td>
													</tr>
													<tr CLASS="main">
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getMailCity() %></td>
														<td>&nbsp;</td>
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getMailZip() %></td>
														<td>&nbsp;</td>
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage()) %></b></td>
														<td nowrap="nowrap">
<%
	Vector<Vector<String>> vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
	Iterator<Vector<String>> itStates  = vecStates.iterator();
	while (itStates.hasNext()) {
		Vector<String> vecTemp = (Vector<String>)itStates.next();
		String strStateId   = vecTemp.get(0).toString();
		String strStateName = vecTemp.get(1).toString();
		if (strStateId.equals(Merchant.getMailState())) {
			out.println(strStateName);
			break;
		}
	}
%>
														</td>
													</tr>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getMailCounty() %></td>
													</tr>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_edit.country",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= CountryInfo.getAllCountriesID().get(Integer.parseInt(Merchant.getMailCountry())) %></td>
													</tr>
													<tr class="main">
														<td COLSPAN="8"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.printBillingAddress",SessionData.getLanguage()) %>:</b></td>
														<td><%= Merchant.getPrintBillingAddress()?Languages.getString("jsp.admin.customers.merchants_edit.printBillingAddress_Y",SessionData.getLanguage()): Languages.getString("jsp.admin.customers.merchants_edit.printBillingAddress_N",SessionData.getLanguage())%></td>
													</tr>
												</table></td>
										</tr>
<%
}
%>
										<tr>
											<td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_info.contact.title",SessionData.getLanguage())%></td>
										</tr>
										<tr>
											<td class="formArea2">
<%
if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
%>
												<table WIDTH="100%" CELLSPACING="1" CELLPADDING="1"
													BORDER="0">
													<tr>
														<td CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contacttype",SessionData.getLanguage()).toUpperCase()%></td>
														<td CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contact",SessionData.getLanguage()).toUpperCase()%></td>
														<td CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.phone",SessionData.getLanguage()).toUpperCase()%></td>
														<td CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.fax",SessionData.getLanguage()).toUpperCase()%></td>
														<td CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.email",SessionData.getLanguage()).toUpperCase()%></td>
														<td CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.cellphone",SessionData.getLanguage()).toUpperCase()%></td>
														<td CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.department",SessionData.getLanguage()).toUpperCase()%></td>
													</tr>
<%
	int intEvenOdd = 1;
	Vector vecContacts = Merchant.getVecContacts();
	Iterator itContacts = vecContacts.iterator();
	while (itContacts.hasNext()){
		Vector vecTemp = null;
		vecTemp = (Vector)itContacts.next();
%>
													<tr CLASS="row<%=intEvenOdd%>">
														<td><%=com.debisys.customers.Merchant.getContactTypeById(vecTemp.get(0).toString(),SessionData)%></td>
														<td><%=StringUtil.toString(vecTemp.get(1).toString())%></td>
														<td><%=StringUtil.toString(vecTemp.get(2).toString())%></td>
														<td><%=StringUtil.toString(vecTemp.get(3).toString())%></td>
														<td><%=StringUtil.toString(vecTemp.get(4).toString())%></td>
														<td><%=StringUtil.toString(vecTemp.get(5).toString())%></td>
														<td><%=StringUtil.toString(vecTemp.get(6).toString())%></td>
													</tr>
<%
		intEvenOdd = ((intEvenOdd == 1)?2:1);
	}
%>
												</table>
												<table width="100">
												<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Merchant.getSmsNotificationPhone())%></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Merchant.getMailNotificationAddress())%></td>
													</tr>												
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox"  />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox"  />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" />
														</td>
													</tr>													
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><%=Merchant.getBalanceNotificationValue()%></td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><%=Merchant.getBalanceNotificationDays()%></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" />
														</td>
													</tr>
<%
   }
%>
												</table> <script>disableChk()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>disableBalChk()</script>
<%
   }
%>													
<%
}else{
%>
												<table>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.contact",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getContactName() %></td>
													</tr>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.email",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getContactEmail() %></td>
													</tr>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.phone",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.formatPhone(application, Merchant.getContactPhone()) %></td>
													</tr>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.fax",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%=  Merchant.formatPhone(application, Merchant.getContactFax()) %></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Merchant.getSmsNotificationPhone())%></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Merchant.getMailNotificationAddress())%></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" value="<%=Merchant.isPaymentNotifications()%>"  />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" value="<%=Merchant.isPaymentNotificationSMS()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" value="<%=Merchant.isPaymentNotificationMail()%>" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" value="<%=Merchant.isBalanceNotifications()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" value="<%=Merchant.isBalanceNotificationTypeValue()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" value="<%=Merchant.isBalanceNotificationTypeDays()%>" />
														</td>
													</tr>													
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><%=Merchant.getBalanceNotificationValue()%></td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><%=Merchant.getBalanceNotificationDays()%></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" value="<%=Merchant.isBalanceNotificationSMS()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" value="<%=Merchant.isBalanceNotificationMail()%>" />
														</td>
													</tr>
<%
   }
%>
												</table>
												<script>disableChk()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>disableBalChk()</script>
<%
   }
%>													
<%
}
%>
											</td>
										</tr>
<%
if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES)){
%>
										<tr>
											<td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.common_info_edit.payment_information",SessionData.getLanguage())%></td>
										</tr>
										<tr>
											<td class="formArea2">
												<table width="100">
													<tr class="main">
														<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.common_info_edit.payment_type",SessionData.getLanguage())%></td>
														<td nowrap="nowrap">
															<select name="paymentType" disabled="disabled">
<%
	Vector vecPaymentTypes = com.debisys.customers.Merchant.getPaymentTypes();
	Iterator itPaymentTypes = vecPaymentTypes.iterator();
	boolean flag_payment=false;
	while (itPaymentTypes.hasNext()) {
		Vector vecTemp = null;
		vecTemp = (Vector)itPaymentTypes.next();
		String strPaymentTypeId = vecTemp.get(0).toString();
		String strPaymentTypeCode = vecTemp.get(1).toString();
		if ( (request.getParameter("paymentType") != null) && (strPaymentTypeId.equals(request.getParameter("paymentType"))) ){
			flag_payment = true;
			out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
		}else if ( strPaymentTypeId.equals(Integer.toString(Merchant.getPaymentType())) ){
			flag_payment = true;
			out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
		}else{
			out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\">" + strPaymentTypeCode + "</OPTION>");
		}
	}
	if ( !flag_payment ){
%>
																<OPTION VALUE="-1" SELECTED="selected">--</OPTION>
<%
	}
%>

															</SELECT>
														</td>
														<td nowrap="nowrap">
<% 
	if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
%>
															<%=Languages.getString("jsp.admin.customers.common_info_edit.process_type",SessionData.getLanguage())%>
<% 
	}else{
%>
															<%=Languages.getString("jsp.admin.customers.common_info_edit.process_type_international",SessionData.getLanguage())%>
<%
	}
%>
														</td>
														<td nowrap="nowrap"><SELECT NAME="processType" disabled="disabled">
																<%
	Vector vecProcessTypes = com.debisys.customers.Merchant.getProcessorTypes();
	Iterator itProcessTypes = vecProcessTypes.iterator();
	boolean flag_process=false;
	while (itProcessTypes.hasNext()) {
		Vector vecTemp = null;
		vecTemp = (Vector)itProcessTypes.next();
		String strProcessTypeId = vecTemp.get(0).toString();
		String strProcessTypeCode = vecTemp.get(1).toString();
		if ( (request.getParameter("processType") != null) && (strProcessTypeId.equals(request.getParameter("processType"))) ){
			flag_process = true;
			out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
		}else if ( strProcessTypeId.equals(Integer.toString(Merchant.getProcessType())) ){
			flag_process = true;
			out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
		}else{
			out.println("<OPTION VALUE=\"" + strProcessTypeId + "\">" + strProcessTypeCode + "</OPTION>");
		}
	}
	if ( !flag_process ){
%>
																<OPTION VALUE="-1" SELECTED="selected">--</OPTION>
<%
	}
%>
															</SELECT>
														</td>
													</tr>
												</table>
											</td>
										</tr>
<%
}
//Monthly Fee Sales Threshold
if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP)) {
	if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
		//If when deploying in Mexico
%>
										<tr>
											<td class="formAreaTitle2"><br> <%= Languages.getString("jsp.admin.customers.merchants_edit.banking_information",SessionData.getLanguage()) %></td>
										</tr>
										<tr>
											<td class="formArea2">
												<table width="100">
													<tr class="main">
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.tax_id",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getTaxId() %></td>
													</tr>
													<tr CLASS="main">
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.bankname",SessionData.getLanguage()) %>:</b></td>
														<td><%= Merchant.getBankName() %></td>
													</tr>
													<tr CLASS="main">
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.accounttype",SessionData.getLanguage()) %>:</b></td>
														<td>
															<%
		Vector   vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
		Iterator itAccountTypes  = vecAccountTypes.iterator();
		while (itAccountTypes.hasNext()) {
			Vector vecTemp = null;
			vecTemp = (Vector)itAccountTypes.next();
			String strAccountTypeId   = vecTemp.get(0).toString();
			String strAccountTypeCode = vecTemp.get(1).toString();
			if (strAccountTypeId.equals(Integer.toString(Merchant.getAccountTypeId()))) {
				out.println(Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" +
				strAccountTypeCode,SessionData.getLanguage()));
				break;
			}
		}
%>
														</td>
													</tr>
													<tr class="main">
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.routing_number",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getRoutingNumber() %></td>
													</tr>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_edit.account_number",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getAccountNumber() %></td>
													</tr>
												</table>
											</td>
										</tr>
<%
	}else{
%>
										<tr>
											<td class="formAreaTitle2"><br> <%= Languages.getString("jsp.admin.customers.merchants_info.terminal_information",SessionData.getLanguage()) %></td>
										</tr>
										<tr>
											<td class="formArea2">
												<table>
													<tr class="main">
														<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.monthly_fee_threshold",SessionData.getLanguage()) %>:</b></td>
														<td nowrap="nowrap"><%= Merchant.getMonthlyFeeThreshold() %></td>
													</tr>
												</table>
											</td>
										</tr>
<%
	}
}//End of if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) ||
//strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP))

if ( SessionData.checkPermission(DebisysConstants.PERM_EDIT_TERMINAL_OPS_TIME) ){
%>
										<tr>
											<td class="formAreaTitle2"><br> <%=Languages.getString("jsp.admin.terminalops.title",SessionData.getLanguage()) %></td>
										</tr>
										<tr>
											<td class="formArea2">
												<div id="divTerminalOps">
													<br>
													<span class="main"><%=Languages.getString("jsp.admin.terminalops.loading",SessionData.getLanguage())%></span>
												</div>
											</td>
										</tr>
<%
}
//iso rep agent and subagents can set credit limits now based on permissions
// rbuitrago: checking for any Debisys' terminals for this merchant
vecTerminals = com.debisys.customers.Merchant.getTerminalsCountEnabledWithTerminalOf(entityId);
boolean havingDebisysTerminals = false;
boolean havingQCommTerminals = false;
for (Vector<String> entry : vecTerminals) {
	if (DebisysConfigListener.getDebugJS(application)) {
		String fullEntry = entry.get(0) + ";" +  entry.get(1) + ";" + entry.get(2) + ";" + entry.get(3) + ";" +  entry.get(4) + ";" + entry.get(5) + ";" + entry.get(6);
		String vars = "\"Full Entry=" + fullEntry + " - havingQCommTerminals=" + havingQCommTerminals + "\"";
		out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
	}
	if ("Debisys".equalsIgnoreCase("" + entry.get(4))) {
		havingDebisysTerminals = true;
	}
	if ("QComm".equalsIgnoreCase("" + entry.get(4))) {
		havingQCommTerminals = true;
	}
}
if (DebisysConfigListener.getDebugJS(application)) {
	String vars = "\"havingDebisysTerminals=" + havingDebisysTerminals + " - havingQCommTerminals=" + havingQCommTerminals + " -Entries=" + vecTerminals.size() + "\"";
	out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
}
%>
<%-- rbuitrago: this section header is to be shown anyway so I cut it off the if-else .. --%>
										<tr>
											<td class="formAreaTitle2"><br> <%= Languages.getString("jsp.admin.customers.merchants_info.merchant_type",SessionData.getLanguage()) %></td>
										</tr>
										<%
if ((deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && SessionData.getUser().isQcommBusiness()) && havingQCommTerminals){
%>
										<tr>
											<td class="formArea2">
												<table>
													<tr class="main">
														<td><%= Languages.getString("jsp.admin.customers.merchants_info.credit_limir_instructions_qcomm",SessionData.getLanguage()) %></td>
													</tr>
												</table></td>
										</tr>
<%
}
if (havingDebisysTerminals || !(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && SessionData.getUser().isQcommBusiness())){
	if (SessionData.checkPermission(DebisysConstants.PERM_CREDIT_LIMITS)){
		// rbuitrago: to get to the end of this if (and the beginning of its else), browse until next match of next line.
		//allow all others to view credit limits
		String liabilityLimit   = Merchant.getLiabilityLimit();
		String runningLiability = Merchant.getRunningLiability();
		String noCreditCheck    = Merchant.getNoCreditCheck();
		double availableCredit  = Double.parseDouble(liabilityLimit) - Double.parseDouble(runningLiability);
%>
										<tr>
											<td class="formArea2">
												<table>
													<tr class="main">
														<td><%= Languages.getString("jsp.admin.customers.merchants_info.credit_limit_notice_debisys",SessionData.getLanguage()) %></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="formArea2">
												<table>
													<tr class="main">
														<td>
<%!
		List<Boolean> otherOptions = null;
		boolean showApplyPaymentOption, showPaymentOptions, showUpdateCreditLimitOption, showAdditionalPaymentOptions, disableCreditTypeControls, hideFormTermSalesResetLimit, hideBtnChangeType, displayInstructions1 = false;
		String disabledText = "";
		String disabledTextReset = "";
%> <%
		String isoid = SessionData.getProperty("iso_id");
		String isoCredType = Rep.getISOCreditType(isoid);
		otherOptions = CreditTypes.getFormOptionsMerchant(customConfigType, deploymentType, Merchant.getMerchantType(), isoCredType);
		showApplyPaymentOption = otherOptions.get(0);
                if ( Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_CREDIT) && isDomestic){
                    showApplyPaymentOption = false;
                }
		showPaymentOptions = otherOptions.get(1);
		showUpdateCreditLimitOption = otherOptions.get(2);
		showAdditionalPaymentOptions = otherOptions.get(3);
		disableCreditTypeControls = otherOptions.get(4);
		hideFormTermSalesResetLimit = otherOptions.get(5);
		hideBtnChangeType = otherOptions.get(6);
		displayInstructions1 = otherOptions.get(7);
		if (displayInstructions1) {
			out.println(Languages.getString("jsp.admin.customers.merchants_info.credit_limit_instructions1",SessionData.getLanguage()));
		}
%>
														</td>
													</tr>
<%
		if (strMessage.equals("9")){
			out.println("<font color=ff0000>Merchant can not be unlimited because rep is credit/prepaid.</font><br>");
		}
%>
													<form name="creditLimitType" method="post" action="admin/customers/merchants_update_credit_limit_type.jsp" onsubmit="return checkMerchantType();">
													<tr class="main">
														<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_info.credit_limit_type",SessionData.getLanguage()) %>:</b></td>
													</tr>
													<tr>
														<td>
															<table>
																<tr class="main">
																	<td>
<%
							// DBSY-804
							// Get ISO of Merchant and then get credit type to determine if we need to force a
							// flexible credit type
							// ref_id = ISO ID
%> <%
		if (DebisysConfigListener.getDebugJS(application)) {
			String vars = "\"isoID=" + isoid + " - ref_id=" + SessionData.getProperty("ref_id") + "\"";
			out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
		}
		if (DebisysConfigListener.getDebugJS(application)) {
			String vars = "\"merchantType=" + Merchant.getMerchantType() + " - isoCredType=" + isoCredType + "\"";
			out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
			vars = "\"showApplyPaymentOption=" + showApplyPaymentOption
				+ " - paymentoptions=" + showPaymentOptions
				+ " - showUpdateCreditLimitOption=" + showUpdateCreditLimitOption
				+ " - Additionalpaymentoptions=" + showAdditionalPaymentOptions
				+ " - disableCreditTypeControls=" + disableCreditTypeControls
				+ " - hideFormTermSalesResetLimit=" + hideFormTermSalesResetLimit
				+ " - hideBtnChangeType=" + hideBtnChangeType + "\"";
			out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
		}
		if (disableCreditTypeControls) {
			disabledText = "disabled=\"true\"";
		}
		if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_SHARED)) ) {
			//If the merchant is of Shared Balance
			%> &nbsp;<SPAN><%=Languages.getString("jsp.admin.customers.merchants_info.credit_type4",SessionData.getLanguage())%></SPAN><%
		} else {
			//Else if the merchant is not of Shared Balance
			String merchantTypePrepaid   = "";
			String merchantTypeCredit    = "";
			String merchantTypeUnlimited = "";
			boolean limitToPrepaid = false;
			if ( Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_PREPAID) ){
				merchantTypePrepaid = "selected";
				if((customConfigType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))){
					limitToPrepaid = true;
				}
			}else if ( Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_CREDIT) ){
				merchantTypeCredit = "selected";
			}else if ( Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED) ){
				merchantTypeUnlimited = "selected";
			}else{
				if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
					merchantTypeCredit = " selected";
				}else{
					merchantTypePrepaid = " selected";
				}
			}
%>
																		<script type="text/javascript">
																			function checkCreditLimitTBody() {
																				if (document.getElementById('lstMerchantType').value == <%=DebisysConstants.MERCHANT_TYPE_UNLIMITED%>) {
																					document.getElementById('tBodySalesNCreditLimit').style.display = "none";
																				} else {
																					document.getElementById('tBodySalesNCreditLimit').style.display = "block";
																				}
																				if (document.getElementById('lstMerchantType').value == <%=DebisysConstants.MERCHANT_TYPE_PREPAID%>) {
																					document.getElementById('btnResetLimit').style.display = "none";
																				}
																				else{
																					document.getElementById('btnResetLimit').style.display = "block";
																				}
																			}
																		</script>

<%
			// Disable the credit type dropdown and force "Prepaid" if ISO of this merchant is Flex.
			// Basically, merchant credit type cannot be changed.
			Hashtable<String, String> options =
			CreditTypes.getAvailableOptionsForMerchant(isoCredType, customConfigType, deploymentType,
			new String[]{merchantTypePrepaid, merchantTypeCredit, merchantTypeUnlimited});
			if (DebisysConfigListener.getDebugJS(application)) {
				String vars = "\"";
				for (String opt: options.keySet()) {
					vars += "[" + opt + "]=[" + options.get(opt) + "] ";
				}
				vars += "\"";
				out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
			}
			if(!Pincache.hasPinesAssigned(Merchant.getMerchantId())){
%>
																		<select style="display:block;" <%=disabledText%> id="lstMerchantType" name="merchantType" onchange="checkCreditLimitTBody();">
<%
				disabledTextReset = merchantTypePrepaid;
				for (String option: options.keySet()) {
					String valueText = "jsp.admin.customers.merchants_info.credit_type" + option;
%>
																			<option value="<%=option%>" <%=options.get(option)%>><%=Languages.getString(valueText,SessionData.getLanguage())%></option>
<%
				}
%>
																		</select>
<%
			}else{
				%> &nbsp;<SPAN><%=Languages.getString("jsp.admin.customers.merchants_info.credit_type" + Merchant.getMerchantType(),SessionData.getLanguage())%></SPAN><%
			}
		}//End of else if the merchant is not of Shared Balance
%>
																	</td>
																	<td>
																		<input type="hidden" name="merchantId"
																			value="<%= Merchant.getMerchantId() %>"> <input
																			type="hidden" name="repId"
																			value="<%= Merchant.getRepId() %>"> <input
																			id="lblChangeDescription" type="hidden"
																			name="paymentDescription" value=""> <input
																			id="lblCurrentMerchantType" type="hidden"
																			name="currentMerchantType"
																			value="<%=Merchant.getMerchantType()%>">
<%
			if (!hideBtnChangeType && !Pincache.hasPinesAssigned(Merchant.getMerchantId())) {
%>
																		<input <%=disabledText%> id="btnChangeType" type="submit" value="<%=Languages.getString("jsp.admin.customers.reps_info.changecredittype",SessionData.getLanguage())%>">
<%
			}
										//disabledText = ""; //this eliminates sticky behavior (Strings are immutable)
%>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													</form>
													<tr class="main">
														<td nowrap="nowrap">
															<table cellpadding="0" cellspacing="0" border="0">
																<form name="reset" method="post" action="admin/customers/merchants_reset_balance.jsp" onSubmit="return checkRunningLiability();">
																	<input type="hidden" name="submitted" value="y">
																	<input type="hidden" name="merchantId"
																		value="<%= Merchant.getMerchantId() %>"> <input
																		type="hidden" name="merchantType"
																		value="<%= Merchant.getMerchantType() %>"> <input
																		type="hidden" name="runningLiability"
																		value="<%= runningLiability %>">
<%
		String s_styleForDailySalesLimit = "";
		if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_MAXIMUM_SALES_LIMIT)){
			s_styleForDailySalesLimit = "style=\"display:block;\"";
		}else{
			s_styleForDailySalesLimit = "style=\"display:none;\"";
		}
		String s_styleFormTermSalesResetLimit = "style=\"display:block;\"";
		if (hideFormTermSalesResetLimit) {
			s_styleFormTermSalesResetLimit = "style=\"display:none;\"";
		}
%>
																	<tbody id="tBodySalesNCreditLimit" <%=s_styleFormTermSalesResetLimit%>>
																		<tr class="main">
																			<td valign="top" nowrap="nowrap" align="left"><br>
																				<b><%= Languages.getString("jsp.admin.customers.merchants_info.terminal_sales_since_last_adjustment",SessionData.getLanguage()) %>:</b>
																				<%= NumberUtil.formatCurrency(runningLiability) %>
<% 
		if(disabledTextReset.contains("selected") || disabledText.equals("")){
%>
																				<input style="display:none;" id="btnResetLimit" type="submit" name="submit" value="Reset"> 
<%	
		}else if(disabledTextReset.equals("")){
%>
																				<input id="btnResetLimit" type="submit" name="submit" value="Reset">
<%
		}
		disabledText = ""; //this eliminates sticky behavior (Strings are immutable)
%>
																				<br> <br></td>

																		</tr>
																	</tbody>
																</form>
<%
		if (showUpdateCreditLimitOption){
%>
																<form name="creditLimit" method="post" action="admin/customers/merchants_update_credit_limit.jsp" onSubmit="return checkCreditLimit();">
																	<tr class="main">
<%
			// added rbuitrago: For Prepaid Merchants the Credit Limit shouldn't be shown
			if (!Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_PREPAID)) {
%>
																		<td><%= Languages.getString("jsp.admin.customers.merchants_info.credit_limit_desc",SessionData.getLanguage()) %><b><%= Languages.getString("jsp.admin.customers.merchants_info.available_credit",SessionData.getLanguage()) %>:</b>
<%
				out.println(NumberUtil.formatCurrency(Double.toString(availableCredit)));
%>
																		</td>
<%
			}
%>
																	</tr>
																	<tr class="main">
																		<td valign="top" nowrap="nowrap" align="left">
																			<br>
																			<input type="hidden" name="merchantId" value="<%= Merchant.getMerchantId() %>">
																			<input type="hidden" name="repId" value="<%= Merchant.getRepId() %>">
<%
			if (strMessage.equals("3")){
				out.println("<font color=ff0000>" + Languages.getString("jsp.admin.error2",SessionData.getLanguage()) + "</font><br>");
			}else if (strMessage.equals("4")){
				out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.merchants_info.error2",SessionData.getLanguage()) + "</font><br>");
			}
%>
																			<b><%= Languages.getString("jsp.admin.customers.merchants_info.credit_limit",SessionData.getLanguage()) %>:</b>
																			<input type="text" name="creditLimit" value="<%= NumberUtil.formatAmount(liabilityLimit) %>" size="8" maxlength="20">
																			<input id="btnUpdateCreditLimit" type="submit" name="submit" value="<%= Languages.getString("jsp.admin.customers.merchants_info.update",SessionData.getLanguage()) %>">
																		</td>
																	</tr>
																</form>
<%
		}//End of if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
		//Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_CREDIT))
%>
																</tbody>
																<tbody id="DailySalesLimitBody" <%=s_styleForDailySalesLimit%> >
<%
		if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_MAXIMUM_SALES_LIMIT)){
%>
																	<tr>
																		<td><div id="divSalesLimits">
																				<br>
																				<span class="main"><%=Languages.getString("jsp.admin.saleslimit.loadingSalesLimits",SessionData.getLanguage())%></span>
																			</div>
																		</td>
																	</tr>
<%
		}
%>
																</tbody>
															</table>
<%
		if (strMessage.equals("10")) {
			out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.merchants_info.error10",SessionData.getLanguage()) + "</font><br>");
		}
%>
														</td>
													</tr>
												</table>
											</td>
										</tr>
<%
		if(showApplyPaymentOption ){//&& Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_PREPAID)
%>

										<tr>
											<td class="formAreaTitle2"><br> <%= Languages.getString("jsp.admin.customers.reps_info.apply_payment",SessionData.getLanguage()) %></td>
										</tr>
										<tr>
											<td class="formArea2">
												<table>
													<form name="payment" method="post" action="admin/customers/merchants_apply_payment.jsp" onSubmit="<%=((customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))?"return CheckPaymentMx();":"return checkPayment();")%>">
														<input type="hidden" name="repId" value="<%= Merchant.getRepId() %>">
														<tr class="main">
															<td nowrap="nowrap">
																<table cellspacing="0" cellpadding="0" border="0">
																	<tr class="main">
																		<td>
																			<table cellspacing="0" cellpadding="0" border="0">
																				<tr class="main">
																					<td><b><%= Languages.getString("jsp.admin.customers.merchants_info.available_credit",SessionData.getLanguage())%>:</b>
<%
			if (Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED)){
				out.println(Languages.getString("jsp.admin.customers.merchants_info.unlimited",SessionData.getLanguage()));
			}else{
				out.println(NumberUtil.formatCurrency(Double.toString(availableCredit)));
			}
%>
																					</td>

																				</tr>
<%
			if (!Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED)){
%>
																				<input type="hidden" name="submitted" value="y" />
																				<input type="hidden" name="merchantId" value="<%= Merchant.getMerchantId() %>" />
																				<tr class="main">
																					<td>
<%
				if (strMessage.equals("1")){
					out.println("<font color=ff0000>" + Languages.getString("jsp.admin.error2",SessionData.getLanguage())+ "</font>");
				} else if (strMessage.equals("7")) {
					out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.merchants_info.error3",SessionData.getLanguage()) + "</font>");
				} else if (strMessage.equals("8")) {
					out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.merchants_info.error4",SessionData.getLanguage()) + "</font>");
				} else if (strMessage.equals("21")) {
					out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.merchants_info.error5",SessionData.getLanguage()) + "</font>");
				}

%>
																					</td>
																				</tr>
																				<tr class="main">
																					<td nowrap="nowrap">
																						<table cellpadding="0" cellspacing="0">
																							<tr class="main">
<%
				if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) || deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) ||customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
					String sScript = "";
					String sBlurScript = "this.value=formatAmount(this.value);";
					if ( Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_PREPAID) ){
						sScript = "ValidatePaymentValueMX(this, true);calculateMx();";
						sBlurScript += "ValidatePrepaidLimitMX(this);";
					}else if ( Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_CREDIT) ){
						sScript = "ValidatePaymentValueMX(this, true);calculateMx();";
						sBlurScript += "ValidatePrepaidLimitMX(this);ValidateCreditLimitMX(this);";
					}else{
						sScript = "ValidatePaymentValueMX(this, false);calculateMx();";
						sBlurScript += "ValidateUnlimitedMX(this);";
					}
%>
																								<label><input type="radio"
																									name="commissionChecked" value="1"
																									checked="checked"
																									onClick="return checkCommissionType();">&nbsp;
																									<%=Languages.getString("jsp.admin.customers.merchants_info.commissionPercentage",SessionData.getLanguage())%></label>
																								<br>
																								<label><input type="radio"
																									name="commissionChecked" value="2"
																									onClick="return checkCommissionType();">&nbsp;
																									<%=Languages.getString("jsp.admin.customers.merchants_info.commissionValue",SessionData.getLanguage())%></label>
																								<br>
																								<td align="center"><b><%=Languages.getString("jsp.admin.customers.merchants_info.gross_paymentMx",SessionData.getLanguage())%></b>&nbsp;
																									<input type="text" name="paymentAmount"
																									value="0.00" size=7
																									onpropertychange="<%=sScript%>"
																									onblur="<%=sBlurScript%>"></td>
<%
				} else {
					out.println("<td align=center><b>" + Languages.getString("jsp.admin.customers.merchants_info.gross_payment",SessionData.getLanguage()) +
						"</b>&nbsp;<input type=text name=\"paymentAmount\" value=\"0.00\" size=7 onKeyUp=\"calculate(this);\" onBlur=\"return validate(this);\"></td>");
				}
%>
																								<td align="center">&nbsp; <b>-</b> &nbsp;</td>
<%
				if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
					out.println("<td align=center><b>" + Languages.getString(
						"jsp.admin.customers.merchants_info.commission",SessionData.getLanguage()) +
						"</b>&nbsp;<input type=text name=\"commission\" value=\"0.00\" size=3 onpropertychange=\"FilterCommission(this);calculateMx();\"></td>");
				}else{
					out.println("<td align=center><b>" + Languages.getString(
						"jsp.admin.customers.merchants_info.commission",SessionData.getLanguage()) +
						"</b>&nbsp;<input type=text name=\"commission\" value=\"0\" size=3 onKeyUp=\"calculate(this);\" onBlur=\"return validate(this);\"><b>%</b></td>");
				}
%>
																								<td align="center">&nbsp; <b>=</b> &nbsp;</td>
																								<td align="center"><input type="text"
																									name="netPaymentAmount" value="0.00" size="7"
																									readonly="readonly"
																									style="color:#0000FF;background:#C0C0C0;">&nbsp;
																									<b><%= Languages.getString("jsp.admin.customers.merchants_info.net_payment",SessionData.getLanguage()) %></b>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<tr class="main">
																					<td>
																						<table>
																							<tr class="main">
																								<td><b><%=Languages.getString("jsp.admin.customers.merchants_info.deposit_date",SessionData.getLanguage())%>:
																								</b>
																								</td>
																								<td><input class="plain" name="depositDate"
																									value="" size="12"> <a
																									href="javascript:void(0)"
																									onclick="if(self.gfPop)gfPop.fStartPop(document.payment.depositDate, document.payment.endDate);return false;"
																									HIDEFOCUS><img name="popcal"
																										align="absmiddle"
																										src="admin/calendar/calbtn.png" width="34"
																										height="22" border="0" alt="">
																								</a></td>
																							</tr>

																						<%
				if (strMessage.equals("20")) {
					out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.merchants_info.validateDateError",SessionData.getLanguage()) + "</font>");
				}
				if (showPaymentOptions) {
					// formerly depositDate calendar (and validation) was exclusive to MX.
					// The calendar must be shown everywhere, but using it is not mandatory. The default is current date.
					out.println("<tr class=main><td><input type=\"hidden\" class=\"plain\" name=\"endDate\" value=\""+ SessionData.getProperty("end_date") + "\" size=\"12\"></td></tr>");

					// out.println("<tr class=main><td><b>" + Languages.getString(
					// 	"jsp.admin.customers.merchants_info.bank",SessionData.getLanguage()) +
					// ": </b></td><td><input type=text name=\"bank\" value=\"\" size=20 maxlength=50></td></tr>");

					// For MX only: Bank info, ref number, paymentType.
					if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
						//R27. DBSY-586. Added by jacuna
						Vector vecISOBanks = Merchant.getISOBanks(SessionData);
						StringBuffer strBanks = new StringBuffer();
						strBanks.append("<option value=\"\"></option>");
						if (vecISOBanks.size()>0) {
							Iterator itBanks = vecISOBanks.iterator();
							while(itBanks.hasNext()) {
								Vector vecTemp = (Vector)itBanks.next();
								strBanks.append("<option value=\""+vecTemp.get(0).toString()+"\">"+vecTemp.get(1).toString()+"</option>");
							}
						} else {
							Vector vecDefaultBank = Merchant.getDefaultBanks(SessionData);
							Iterator itDefaultBank = vecDefaultBank.iterator();
							while(itDefaultBank.hasNext()) {
								Vector vecTemp = (Vector)itDefaultBank.next();
								strBanks.append("<option value=\""+vecTemp.get(0).toString()+"\">"+vecTemp.get(1).toString()+"</option>");
							}
						}
						out.println("<tr class=main><td><b>"+Languages.getString("jsp.admin.customers.merchants_info.bank",SessionData.getLanguage())+
									": </b></td><td><SELECT name=\"bank_id\">"+strBanks.toString()+"</SELECT></td></tr>");
						//End. R27. DBSY-586. Added by jacuna
					} // end if in MX
					if (showAdditionalPaymentOptions) {
						out.println("<tr class=main><td><b>" + Languages.getString(
							"jsp.admin.customers.merchants_info.ref_number",SessionData.getLanguage()) +
							": </b></td><td><input type=text name=\"refNumber\" value=\"\" size=20 maxlength=50></td></tr>");
						//R27. DBSY-586. Added by jacuna
						Vector vecPaymentTypes = Merchant.getMerchantCreditPaymentTypes();
						Iterator itPayment = vecPaymentTypes.iterator();
						StringBuffer strPaymentValues = new StringBuffer();
						while ( itPayment.hasNext()) {
							Vector vecTemp = (Vector) itPayment.next();
							strPaymentValues.append("<option value=\""+vecTemp.get(0).toString()+"\">"+vecTemp.get(1).toString()+"</option>");
						}
						out.println("<tr class=main><td><b>" +
							Languages.getString("jsp.admin.customers.merchants_info.merchantPaymentType",SessionData.getLanguage()) +"</b></td><td><select name=\"merchantCreditPaymentType\">"+
							strPaymentValues.toString()+"</select></td></tr>");
						//End. R27. DBSY-586. Added by jacuna
						out.println("<tr class=main><td><b>" +
						Languages.getString("jsp.admin.customers.merchants_info.comments",SessionData.getLanguage()) +
							": </b></td><td><textarea type=text name=\"paymentDescription\" value=\"\" size=20 maxlength=255/ rows=6></textarea>("
							+ Languages.getString("jsp.admin.optional",SessionData.getLanguage()) + ")</td></tr></table>");
					} else {
						out.println("<tr class=main><td><b>" + Languages.getString(
							"jsp.admin.customers.merchants_info.description",SessionData.getLanguage()) +
							":</b><input type=text name=\"paymentDescription\" value=\"\" size=20>(" +
							Languages.getString("jsp.admin.optional",SessionData.getLanguage()) + ")</td></tr></table>");
					}
				}
%>
																							</tr>
																							<tr class="main">
																								<td>
																									<input type="hidden" name="liabilityLimit"
																										value="<%= Merchant.getLiabilityLimit() %>">
																									<input type="hidden" name="runningLiability" value="<%= Merchant.getRunningLiability() %>">
																									<input type="hidden" name="merchantType" value="<%= Merchant.getMerchantType() %>">
<%
				String sDisabled = "";
				if ( com.debisys.customers.Merchant.getTerminals(Merchant.getMerchantId()).size() == 0 ){
					sDisabled = "DISABLED";
				}
%>
																									<input id="btnApplyPayment" type="submit" name="submit1"
																										<%=sDisabled%> value="<%= Languages.getString("jsp.admin.customers.merchants_info.apply_payment",SessionData.getLanguage()) %>">
																									&nbsp;&nbsp;
																									<input type="button" name="view_history"
																										value="<%= Languages.getString("jsp.admin.customers.merchants_info.view_history",SessionData.getLanguage()) %>"
																										onClick="window.open('/support/admin/customers/merchants_credit_history.jsp?merchantId=<%= Merchant.getMerchantId() %>','merchantHistory','width=1020,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
<%
				int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays("merchants_info", application);
%>
																									* <%=Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage())%>
																									<%=" "+limitDays+" "%> <%=Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage())%>
																								</td>
																							</tr>
<%
			}//End of if (!Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED))
			else{
%>
																							<tr class="main">
																								<td>
																									<!-- <input type="button" name="view_history" value="<%= Languages.getString("jsp.admin.customers.merchants_info.view_history",SessionData.getLanguage()) %>" onClick="window.open('/support/admin/customers/merchants_credit_history.jsp?merchantId=<%= Merchant.getMerchantId() %>','merchantHistory','width=1020,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">-->
																								</td>
																							</tr>
<%
			}
%>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</form>
<%
		}//End of if (showApplyPaymentOption)
	}//End of if (SessionData.checkPermission(DebisysConstants.PERM_CREDIT_LIMITS))
	else{//Else user has no permissions
		//allow all others to view credit limits
		//to view available credit and payment history
		String liabilityLimit   = Merchant.getLiabilityLimit();
		String runningLiability = Merchant.getRunningLiability();
		double availableCredit  = Double.parseDouble(liabilityLimit) - Double.parseDouble(runningLiability);
		//international
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
%>
														<tr>
															<td class="formAreaTitle2"><br> <%= Languages.getString("jsp.admin.customers.merchants_info.credit_limits",SessionData.getLanguage()) %></td>
														</tr>
														<tr>
															<td class="formArea2">
																<table>
																	<tr class="main">
																		<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_info.available_credit",SessionData.getLanguage()) %>:</b>
<%
			if (Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED)) {
				out.println(Languages.getString("jsp.admin.customers.merchants_info.unlimited",SessionData.getLanguage()));
			} else {
				if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_SHARED) ){
					//out.println(NumberUtil.formatCurrency(Double.toString(availableCredit)));
				}
				else{
					out.println(NumberUtil.formatCurrency(Double.toString(availableCredit)));
				}	
			}
%>
																			<br><br>
																			<input type="button" name="view_history"
																				value="<%= Languages.getString("jsp.admin.customers.merchants_info.view_history",SessionData.getLanguage()) %>"
																				onClick="window.open('/support/admin/customers/merchants_credit_history.jsp?merchantId=<%= Merchant.getMerchantId() %>','merchantHistory','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
<%
			//End of if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
		}else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
%>
														<tr>
															<td class="formAreaTitle2"><br> <%= Languages.getString("jsp.admin.customers.merchants_info.credit_limits",SessionData.getLanguage()) %>
															</td>
														</tr>
														<tr>
															<td class="formArea2">
																<table>
																	<tr class="main">
																		<td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_info.available_credit",SessionData.getLanguage()) %>:</b>
<%
			out.println(NumberUtil.formatCurrency(Double.toString(availableCredit)));
%>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
<%
		}//End of if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
	}//End of else user has no permissions
	//NOT USED ANY MORE BUT JUST IN CASE....
        boolean showPinCache = false;
        //NOT USED ANY MORE BUT JUST IN CASE....
	if (showPinCache && Merchant.isUseRatePlanModel()
                && !Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_SHARED)
                    && !Merchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED)
			&& SessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)){
		double availableCredit  = Double.parseDouble(Merchant.getLiabilityLimit()) - Double.parseDouble(Merchant.getRunningLiability());
%>
    <tr>
            <td>
                    <script>
                            var merchantID = '<%=Merchant.getMerchantId()%>';
                            maxBalance = <%=Double.toString(availableCredit)%>;
                    </script>
                    <div class="ui-widget" id="searchcriteria">
                            <div class="ui-widget-header"><strong><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincache",SessionData.getLanguage())%></strong></div>
                            <div class="ui-widget-content">
                                    <p>
                                            <label>
                                                    <input id="enablePINCache" type="radio" name="enablePINCache" value="true" <%=(Merchant.isPinCacheEnabled())?"checked":""%>>
                                                    &nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincacheenabled",SessionData.getLanguage())%>
                                            </label>
                                            &nbsp;&nbsp;&nbsp;
                                            <label>
                                                    <input type="radio" name="enablePINCache" value="false" <%=Merchant.isPinCacheEnabled()?"":"checked"%>>
                                                    &nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincachedisabled",SessionData.getLanguage())%>
                                            </label>
                                    </p>
                                    <p id="pincacheInfo">
                                            <strong><%=Languages.getString("jsp.admin.reservedbalance.title",SessionData.getLanguage())%></strong>
                                            <span id="reservedBalance" width="50px"><%=NumberUtil.formatCurrency(String.valueOf(PcReservedBalance.getRBByMerchantId(Merchant.getMerchantId())))%></span>
                                            <button id="rbReserve"><%=Languages.getString("jsp.admin.reservedbalance.reserve",SessionData.getLanguage())%></button>
                                            <button id="rbReturn"><%=Languages.getString("jsp.admin.reservedbalance.return",SessionData.getLanguage())%></button>
                                            <div id="rbReserveDialog">
                                                    <label><%=Languages.getString("jsp.admin.reservedbalance.toreserve",SessionData.getLanguage())%></label><br>
                                                    <input type="text" class="numeric text ui-widget-content ui-corner-all" id="valueToReserve" name="valueToReserve" maxlength="4" value="0" />
                                            </div>
                                            <div id="rbReturnDialog">
                                                    <label><%=Languages.getString("jsp.admin.reservedbalance.toreturn",SessionData.getLanguage())%></label>
                                                    <input type="text" class="numeric text ui-widget-content ui-corner-all" id="valueToReturn" name="valueToReturn" maxlength="4" value="0" />
                                            </div>
                                    </p>
                            </div>
                    </div>
                    <br />
            </td>
    </tr>
<%
	}
}//End of if (havingDebisysTerminals || !(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && SessionData.getUser().isQcommBusiness()))
%>
													<tr>
														<td class="formAreaTitle2"><br> <%= Languages.getString("jsp.admin.customers.merchants_info.terminals",SessionData.getLanguage()) %></td>
													</tr>
													<tr>
														<td class="formArea2">
<%
if (SessionData.checkPermission(DebisysConstants.PERM_TERMINALS) && !Merchant.isAchAccount()) {
%>
												<div align="right">
													<a class="addTerminalBtn" href="admin/customers/terminal.jsp?repId=<%= Merchant.getRepId() %>&merchantId=<%= Merchant.getMerchantId() %>">
														<%= Languages.getString("jsp.admin.customers.merchants_info.add_new_terminal",SessionData.getLanguage()) %>
													</a>
												</div>

<%
}
%>									
														
															<table width="100%" border=0 class=main
																id="terminalsTable" cellspacing="1" cellpadding="1">
																<tr>
																	<td class="rowhead2">#</td>
																	<td class="rowhead2">&nbsp; <%= Languages.getString("jsp.admin.customers.merchants_info.site_id",SessionData.getLanguage()).toUpperCase() %></td>
																	<%--rbuitrago: rowhead2er and new row created for "terminal of" --%>
																	<td class="rowhead2">&nbsp; <%= Languages.getString("jsp.admin.customers.merchants_info.terminal_of",SessionData.getLanguage()).toUpperCase() %></td>
																	<td class="rowhead2">&nbsp; <%= Languages.getString("jsp.admin.customers.merchants_info.terminal_desc",SessionData.getLanguage()).toUpperCase() %></td>
<%if ( permissionTerminalLabel ) {
%>
                                <td class="rowhead2">&nbsp;<%= Languages.getString("jsp.admin.customers.merchants_info.terminal_Label",SessionData.getLanguage()).toUpperCase() %></td>
<%
}
if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
%>
																	<td class="rowhead2">&nbsp; <%= Languages.getString("jsp.admin.customers.merchants_info.terminal_fee",SessionData.getLanguage()).toUpperCase() %></td>
<%
}
if (SessionData.checkPermission(DebisysConstants.PERM_CLERK_CODES)) {
%>
																	<td class="rowhead2">&nbsp; <%= Languages.getString("jsp.admin.customers.merchants_info.edit_clerk_codes",SessionData.getLanguage()).toUpperCase() %></td>
<%
}
%>
																	<td class="rowhead2">&nbsp; <%= Languages.getString("jsp.admin.customers.merchants_info.clerk_report",SessionData.getLanguage()).toUpperCase() %></td>
<%
String messRequestRebuild = Languages.getString("jsp.merchant.requestrebuil.text20",SessionData.getLanguage());
//Vector vecTerminalTypes = com.debisys.terminals.Terminal.getTerminalTypeForRequest(SessionData,application);
if ( strAccessLevel.equals(DebisysConstants.CARRIER) ){
	if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_DISABLE_TERMINALS)) {
		out.print("<td class=rowhead2>&nbsp;" + Languages.getString("jsp.admin.customers.merchants_info.enable",SessionData.getLanguage()).toUpperCase() + "</td>");
	}
}else{
	if (SessionData.checkPermission(DebisysConstants.PERM_TERMINALS)) {
		out.print("<td class=rowhead2>&nbsp;" + Languages.getString("jsp.admin.customers.merchants_info.edit_terminal",SessionData.getLanguage()).toUpperCase() + "</td>");
		out.print("<td class=rowhead2>&nbsp;" + Languages.getString("jsp.admin.customers.merchants_info.enable",SessionData.getLanguage()).toUpperCase() + "</td>");
		out.print("<td class=rowhead2>&nbsp;"+messRequestRebuild+"</td>");
	} else if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_TERMINAL_RATES)) {
		out.print("<td class=rowhead2>&nbsp;" + Languages.getString("jsp.admin.customers.merchants_info.view_terminal_rates",SessionData.getLanguage()).toUpperCase() + "</td>");
		out.print("<td class=rowhead2>&nbsp;" + Languages.getString("jsp.admin.customers.merchants_info.view_terminal_info",SessionData.getLanguage()).toUpperCase() + "</td>");
	} else{
		out.print("<td class=rowhead2>&nbsp;" + Languages.getString("jsp.admin.customers.merchants_info.view_terminal_products",SessionData.getLanguage()) + "</td>");
		out.print("<td class=rowhead2>&nbsp;" + Languages.getString("jsp.admin.customers.merchants_info.view_terminal_info",SessionData.getLanguage()).toUpperCase() + "</td>");
	}
	if (strAccessLevel.equals(DebisysConstants.MERCHANT) && SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION)){
		out.print("<td class=rowhead2>&nbsp;" + Languages.getString("jsp.admin.rateplans.tsorder",SessionData.getLanguage()).toUpperCase() + "</td>");
	}
	if (Merchant.isPinCacheEnabled() && SessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE_ADV)) {
	%>
																	<td class="rowhead2">&nbsp; <%= Languages.getString("jsp.admin.pincache.title",SessionData.getLanguage())%></td>
	<%
	}
}
%>
																</tr>
<%
boolean showWarnnig=false;
Iterator it = vecTerminals.iterator();
int icount=0;

while (it.hasNext()){
	icount++;
	Vector vecTemp = null;
	vecTemp = (Vector)it.next();
	com.debisys.customers.Merchant.printlnLogMsg("Terminal ["+vecTemp.get(0).toString()+"] added from merchant_info ");
	Vector vTmp = new Vector();
	boolean tenabled = vecTemp.get(5).toString().equals("1");
	String ratePlan = vecTemp.get(6).toString();
	Integer productsOnTerminalRates = Integer.parseInt(vecTemp.get(3).toString());
	String riskIcon="";
	if (Merchant.isUseRatePlanModel() && ratePlan=="" ){
		riskIcon ="<img src='/support/images/riskSite.jpg' />";
		showWarnnig=true;
	}else if ( !Merchant.isUseRatePlanModel() && productsOnTerminalRates==0 ){
		riskIcon ="<img src='/support/images/riskSite.jpg' />";
		showWarnnig=true;
	}
%>
																<tr class="row1 <%=(tenabled?"":"silvered") %>">
																	<td>&nbsp;<%=icount %>&nbsp;</td>
																	<td><%=vecTemp.get(0)%> <%=riskIcon%></td>
<%
	if ( vecTemp.get(2).toString().equals("40")  
                || vecTemp.get(2).toString().equals(DebisysConstants.SMS_PHONE_KIOSK_MERCHANT_TERMINAL_TYPE) 
                || vecTemp.get(2).toString().equals(DebisysConstants.SMS_PRINTER_TERMINAL_TYPE) 
                || vecTemp.get(2).toString().equals(DebisysConstants.SMS_TABLET_TERMINAL_TYPE) 
                || vecTemp.get(2).toString().equals(DebisysConstants.SMS_QUICK_SELL_TERMINAL_TYPE)
                || vecTemp.get(2).toString().equals(DebisysConstants.USSD_INFO_PYME_KIOSKO_TERMINAL_TYPE)
                || vecTemp.get(2).toString().equals(DebisysConstants.SMS_3G_PRINTER_TERMINAL_TYPE)){
		vTmp = com.debisys.customers.Merchant.getTerminalRegistration(vecTemp.get(0).toString());
	}
	// rbuitrago: prints terminal_of value
	out.println("<td nowrap>&nbsp;" + vecTemp.get(4) + "</td>");
        String terminalDescriptionValue = vecTemp.get(1).toString();
	if ( vTmp.size() == 0 ){
            out.println("<td nowrap>&nbsp;" + terminalDescriptionValue + "</td>");
	}else if ( vTmp.size() == 1 ){
            out.println("<td nowrap>&nbsp;" + terminalDescriptionValue + " (" + vTmp.get(0) + ")</td>");
	}else{
            out.println("<td nowrap>&nbsp;" + terminalDescriptionValue + "<DIV STYLE='height:40px;overflow:auto;'><UL>");
            for ( int ii = 0; ii < vTmp.size(); ii++ ){
                    out.println("<LI>(" + ((Vector)vTmp.get(ii)).get(0) + ")</LI>");
            }
            out.println("</UL></DIV></td>");
	}
	if (permissionTerminalLabel) {
            String linkAssociate = "";        
            String terminalTypeId = vecTemp.get(2).toString();
            if ( strAccessLevel.equals(DebisysConstants.ISO) 
                    && permissionTerminalAssoc && (terminalTypeId.equals("26") || terminalTypeId.equals("104") || terminalTypeId.equals("105")) ){
                String associateLabel = Languages.getString("jsp.admin.customers.merchants_info.associateLabel",SessionData.getLanguage());
                if (tenabled){                    
                    String idTemp = vecTemp.get(0)+"*"+entityId+"*"+terminalTypeId+"*"+terminalDescriptionValue;
                    linkAssociate = "(<a href='javascript:void(0);' class='linkAssociate' id='"+idTemp+"'>"+associateLabel+"</a>)";
                } else{
                    linkAssociate = "";
                }
                out.println("<td nowrap>&nbsp;" + vecTemp.get(7) + " "+ linkAssociate +"</td>");
            } else{
                out.println("<td nowrap>&nbsp;" + vecTemp.get(7) + "</td>");
            }            
	}
	if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
		out.println("<td>&nbsp;<a href=admin/customers/terminal_fee.jsp?merchantId=" + Merchant.getMerchantId() +
			"&siteId=" + vecTemp.get(0) + ">" + Languages.getString("jsp.admin.customers.merchants_info.fees",SessionData.getLanguage()) +
			"</a></td>");
	}
	if (SessionData.checkPermission(DebisysConstants.PERM_CLERK_CODES)) {
		out.println("<td>&nbsp;<a id=\"clerk"+ vecTemp.get(0) +"\" "+ (tenabled?"":"style=\"display:none\"") +" href=admin/customers/merchants_clerk_codes.jsp?merchantId=" +
			Merchant.getMerchantId() + "&siteId=" + vecTemp.get(0) + ">" + Languages.getString(
			"jsp.admin.customers.merchants_info.clerk_codes",SessionData.getLanguage()) + "</a></td>");
	}
	out.println(
		"<td>&nbsp;<a href=\"javascript:void(0);\" onClick=\"javascript:window.open('/support/admin/reports/clerk/clerk_report.jsp?siteId="
		+ vecTemp.get(0) + "', 'report', 'width=750,height=600,resizable=yes,scrollbars=yes');\">" +
		Languages.getString("jsp.admin.customers.merchants_info.view",SessionData.getLanguage()) + "</a></td>");
	if ( strAccessLevel.equals(DebisysConstants.CARRIER) ){
		if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_DISABLE_TERMINALS)){
%>
																	<td>
																		<input type="checkbox" class="enableButton"
																			id="enabled<%=vecTemp.get(0)%>"
																			name="enabled<%=vecTemp.get(0)%>" value="y"
																			startvalue="<%=tenabled%>"
																			<%=(tenabled?"checked=\"checked\"":"") %>> <input
																			type="hidden" id="reason<%=vecTemp.get(0)%>"
																			name="reason<%=vecTemp.get(0)%>" value=""></td>
<%
		}
	}else{
		if (SessionData.checkPermission(DebisysConstants.PERM_TERMINALS)){
			out.print("<td>&nbsp;<a href=admin/customers/terminal.jsp?merchantId=" + Merchant.getMerchantId() + "&repId=" + Merchant.getRepId() + "&siteId=" + vecTemp.get(0) + ">" + Languages.getString("jsp.admin.customers.merchants_info.edit",SessionData.getLanguage()) + "</a></td>");
%>
																	<td>
																		<input type="checkbox" class="enableButton"
																			id="enabled<%=vecTemp.get(0)%>"
																			name="enabled<%=vecTemp.get(0)%>" value="y"
																			startvalue="<%=tenabled%>"
																			<%=(tenabled?"checked=\"checked\"":"") %>> <input
																			type="hidden" id="reason<%=vecTemp.get(0)%>"
																			name="reason<%=vecTemp.get(0)%>" value=""></td>
<%
			out.print("<td>&nbsp;<a href=admin/customers/merchants_request_rebuild.jsp?siteId="+vecTemp.get(0)+"&type="+vecTemp.get(2).toString()+"&merchantId="+Merchant.getMerchantId()+"&email="+Merchant.getContactEmail()+">"+messRequestRebuild+"</a></td>");
		}else{
			out.print("<td>&nbsp;<a href=admin/customers/merchants_view_rates.jsp?merchantId=" + Merchant.getMerchantId()
				+ "&siteId=" + vecTemp.get(0) + ">" + Languages.getString("jsp.admin.customers.merchants_info.view",SessionData.getLanguage())
				+ "</a></td>");
			out.print("<td>&nbsp;<a href=admin/customers/merchants_view_terminal_info.jsp?merchantId=" +
				Merchant.getMerchantId() + "&siteId=" + vecTemp.get(0) + ">" + Languages.getString(
				"jsp.admin.customers.merchants_info.view_info",SessionData.getLanguage()) + "</a></td>");
		}
	}
	if (strAccessLevel.equals(DebisysConstants.MERCHANT) && SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION)){
		out.print("<td>&nbsp;<a href=admin/customers/merchants_view_rates.jsp?merchantId=" + Merchant.getMerchantId()
			+ "&siteId=" + vecTemp.get(0) + "&top=1&model="+Merchant.isUseRatePlanModel()+">" + Languages.getString("jsp.admin.customers.merchants_info.view",SessionData.getLanguage())
			+ "</a></td>");
	}
	if (Terminal.IsTerminalInMBList(application, (String)vecTemp.get(2)) 
			&& Merchant.isPinCacheEnabled() && SessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE_ADV)
			&& vecTemp.get(8).equals("1") //pin cache enabled  && enabled
		) {
%>
																	<td>
																		<button class="pincacheManageBtn" data-siteid="<%=vecTemp.get(0)%>"><%= Languages.getString("jsp.admin.pincache.goadmin",SessionData.getLanguage()) %></button>
																	</td>
<%
	}else{
%>
																	<td>
																		&nbsp;
																	</td>
		
<%		
	}
%>
																</tr>
<%
	vecTemp.removeAllElements();
	vecTemp = null;
	vTmp.removeAllElements();
	vTmp = null;
	com.debisys.customers.Merchant.printlnLogMsg("Was eliminated the record the terminals from merchants_info");
}
vecTerminals.removeAllElements();
com.debisys.customers.Merchant.printlnLogMsg("Deleting ALL records of the principal terminals vector from merchants_info");
com.debisys.customers.Merchant.printlnLogMsg("Terminal SIZE Vector = "+vecTerminals.size()+" **********  END ********");
vecTerminals = null;
if ( showWarnnig ){
%>
																<tr>
																	<td colspan="6" align="left" style="color:red"><img
																		src="/support/images/riskSite.jpg" /> <%=Languages.getString("jsp.admin.customers.merchants_terminals_changeModel",SessionData.getLanguage())%>
																	</td>
																</tr>
<%
}
%>
															</table>
														</td>
													</tr>
												</table>
<%
if (SessionData.checkPermission(DebisysConstants.PERM_TERMINALS) && !Merchant.isAchAccount()) {
%>
												<div align="right">
													<a class="addTerminalBtn" href="admin/customers/terminal.jsp?repId=<%= Merchant.getRepId() %>&merchantId=<%= Merchant.getMerchantId() %>">
														<%= Languages.getString("jsp.admin.customers.merchants_info.add_new_terminal",SessionData.getLanguage()) %>
													</a>
												</div>
<%
}
%>									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script>
$(document).ready( function(){
<%
if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_MAXIMUM_SALES_LIMIT)){
%>
	window.setTimeout("$('#divSalesLimits').load('admin/customers/saleslimits.jsp?action=show&merchantId=<%=Merchant.getMerchantId()%>&Random=" + Math.random() + "')", 500);
<%
}
if ( SessionData.checkPermission(DebisysConstants.PERM_EDIT_TERMINAL_OPS_TIME) ){
%>
	$("#divTerminalOps").load("admin/customers/terminalops.jsp?action=show&merchantId=<%=Merchant.getMerchantId()%>&Random=" + Math.random());
<%
}
%>
	$('.addTerminalBtn').button({icons:{primary: 'ui-icon-plus'}}).attr('style', 'color: white;');
});
</script>
<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<div id="reasonDialog" title="<%= Languages.getString("com.debisys.terminals.jsp.terminaldisabled.reason",SessionData.getLanguage()) %>">
	<textarea id="reason" name="reason" rows="4" cols="50"></textarea>
</div>
<%if (permissionTerminalLabel){%>
    <jsp:include page="terminals/terminalAssociation.jsp" >
        <jsp:param value="<%=iso_id%>" name="iso_id"/>    
    </jsp:include>    
<%}%>
<div id="customMessage"></div>
<%@ include file="/includes/footer.jsp"%>