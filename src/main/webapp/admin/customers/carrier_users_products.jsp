	<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.util.Hashtable,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%
	int section=14;
	int section_page=5;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:setProperty name="User" property="*"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
	Hashtable userErrors = new Hashtable();

	// Form has been submitted
	if(request.getParameter("submitted") != null) {
		String[] products = request.getParameterValues("products");

		User.addCarrierUserProduct(products, request.getParameter("passwordId"));
		
		userErrors = User.getErrors();
		
		if(userErrors.size() == 0) {
     		User.setUsername("");
     		User.setPassword("");
		}
	}
	
	if(userErrors.size() > 0 || request.getParameter("submitted") == null) 
	{
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_products.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
<!-- -------------------------------------------------------------------------------------------------------------- -->			
  				<tr>
					<td class="formArea2">
						<table border=0>
	              			<tr class="main">
	               				<td nowrap valign="top"><%=Languages.getString("jsp.admin.customers.carrier_users_products.instructions",SessionData.getLanguage())%></td>
	               			</tr>
	               		</table>
       					<table border="0" width="100%" cellpadding="0" cellspacing="0" class="main">
				     		<tr>
				     			<td>
				     				<form name="productsForm" method="post" action="admin/customers/carrier_users_products.jsp">
				     				<table>
										<tr>
											<td class=rowhead3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users.productsopt",SessionData.getLanguage()).toUpperCase()%></td>
										</tr>
				     					<tr>
								        	<td>
								        		<div style="overflow:auto;width:980px;height:400px;border:1px solid #336699;padding-left:5px;font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 8pt;">
<%
		String providerID = "";
		Vector<Vector<String>> vecProductResults = User.getProducts();
		Iterator<Vector<String>> itProducts = vecProductResults.iterator();
			
		// Basically get a list of all of the products and organize them by provider.
		// Show them in a list sorted by provider, with each product having a checkbox
		while (itProducts.hasNext())
		{
			Vector<String> vecProducts = null;
			vecProducts = (Vector<String>) itProducts.next();
			String selected = "";
			
			// Getting the first product/provider
			if(providerID.equals("")) {
				providerID = vecProducts.get(0);
				out.println("<font size=\"4\" ><b>" + vecProducts.get(1) + " (" + vecProducts.get(0) + ")</b></font><br>");
			}
			
			// Finished one set of products for a specific provider, on to the next one
			if(!vecProducts.get(0).equals(providerID)) {
				providerID = vecProducts.get(0);
				out.println("<font size=\"4\" ><b>" + vecProducts.get(1) + " (" + vecProducts.get(0) + ")</b></font><br>");
			}
			
			if(vecProducts.get(5).equals("1"))
				selected = "CHECKED";
			
			out.println("<input type=\"checkbox\" id=\"products\" name=\"products\" value=\"" + vecProducts.get(0) + "_" + vecProducts.get(2) + "\" " + selected + ">&nbsp;&nbsp;" + vecProducts.get(3) + " (" + vecProducts.get(4) + ") " +vecProducts.get(2)+ " <br>");
		}
%>
												</div>
											</td>
				     					</tr>
				     					<tr>
								        	<td>
							          			<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.savebutton",SessionData.getLanguage()).toUpperCase()%>" />
							          			<input type="hidden" id="submitted" name="submitted" value="1" />
							          			<input type="hidden" id="passwordId" name="passwordId" value="<%=request.getParameter("passwordId")%>" />
											</td>
				     					</tr>
				     				</table>
				     				</form>
				     			</td>
							</tr>
						</table>
					</td>
				</tr>
<!-- --------------------------------------------------------------------------------------------------------------- -->	
<%
	}else {
		// Carrier user created successfully
%>
				<tr>
					<td class="formArea2">
						<form name="backToCarrierUsers" action="admin/customers/carrier_users.jsp"> 
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr class="main">
								<td align="center">Carrier User Products Successfully Saved!</td>
							</tr>
							<tr>
				        		<td align="center">
				       				<input type="submit" value="Back To Carrier Users" />
										<input type="hidden" id="search" name="search" value="y">
				       			</td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
<%
	}
%>			
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>