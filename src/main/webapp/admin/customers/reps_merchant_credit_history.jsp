<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.*,
                 com.debisys.languages.Languages,
                 com.debisys.utils.ColumnReport,
                 com.debisys.schedulereports.ScheduleReport" %>
<%
int section=2;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request"/>
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>

<script charset="utf-8" type="text/javascript" src="/support/includes/jquery.js" language="JavaScript"></script>
<script charset="utf-8" type="text/javascript" src="/support/includes/jquery-ui.js" language="JavaScript"></script>

<script type="text/javascript" charset="utf-8">
	function redirectParentPage(){	 
	  window.opener.location = "<%= DebisysConstants.PAGE_TO_SCHEDULE_REPORTS %>";
	  window.close();	 
	}
</script>



<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 25;
int intPageCount = 1;

ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
ArrayList<String> titles = new ArrayList<String>();
String keyLanguage = "jsp.admin.customers.reps_merchant_credit_history.title";
String businessName = ""; 
String strUrlParams = "?repId=" + Rep.getRepId();

 if (request.getParameter("repId") != null)
 {
 
 	  headers = RepReports.getHeadersRepCreditsAssigmentHistory(SessionData, application);
 	  Rep.getRep(SessionData, application);
	  businessName = Rep.getBusinessName();
	  titles.add( Languages.getString(keyLanguage,SessionData.getLanguage()) );
	  titles.add(businessName);
	  
 	  SessionData.setProperty("reportId", DebisysConstants.SC_REP_MERC_CRED_HIST);	
	  if (request.getParameter("page") != null)
	  {
		    try
		    {
		      intPage=Integer.parseInt(request.getParameter("page"));
		    }
		    catch(NumberFormatException ex)
		    {
		      intPage = 1;
		    }
		    if (intPage < 1)
		  	{
		    	intPage=1;
		  	}
	  }
	  								  
	  String sc = request.getParameter("sheduleReport");
	  if ( sc != null && sc.equals("y")  )
	  {	  		
	  		//SCHEDULE REPORT AND REDIRECT
	  		
	  		Rep.getRepMerchantCredits(intPage, intPageSize, SessionData,application, DebisysConstants.SCHEDULE_REPORT, headers, titles);	
	  		ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
			if (  scheduleReport != null  )
			{				
				scheduleReport.setStartDateFixedQuery( Rep.getStartDate() );
				scheduleReport.setEndDateFixedQuery( Rep.getEndDate() );
				scheduleReport.setTitleName( keyLanguage );  
				scheduleReport.setAdditionalData(businessName);    
			}	
			SessionData.setProperty("start_date", Rep.getStartDate());
      		SessionData.setProperty("end_date", Rep.getEndDate());			
	  		%>
	  		<script type="text/javascript" charset="utf-8">
				$(document).ready(function() 
					{
						redirectParentPage();						
					}
				);	
			</script>
	  		
	  		<% 
	  }
	  else if ( request.getParameter("download") != null )
	  {
	  	Rep.getRepMerchantCredits(intPage, intPageSize, SessionData,application, DebisysConstants.DOWNLOAD_REPORT, headers, titles);	  		
	  	response.sendRedirect( Rep.getStrUrlLocation() );
	  }	
	  else
	  {   
		  	  
		  vecSearchResults = Rep.getRepMerchantCredits(intPage, intPageSize, SessionData,application, DebisysConstants.EXECUTE_REPORT, headers, titles);
		  intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
		  vecSearchResults.removeElementAt(0);
		  if (intRecordCount>0)
		  {
		      intPageCount = (intRecordCount / intPageSize) + 1;
		      if ((intPageCount * intPageSize) + 1 >= intRecordCount)
		      {
		        intPageCount++;
		      }
		  }
	  }
 }
 else
 {
  searchErrors = Rep.getErrors();
 }

%>
<html>
  <head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title><%=Languages.getString(keyLanguage,SessionData.getLanguage())%></title>    
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="700" background="../../images/top_blue.gif">
	<tr>
    <td width="18" height="20" align="left"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" align="left" width="3000"><%=Languages.getString("jsp.admin.customers.reps_credit_history.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20" align="right"><img src="../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td class="main">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" class="main">
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
</table>
               </td>
              </tr>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
     <form name="mainform" id="mainform" method="post" action="reps_merchant_credit_history.jsp<%=strUrlParams%>" onSubmit="scroll();">     		
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
            	<td><%=businessName%></td>
            </tr>  	
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage())+"." + Languages.getString("jsp.admin.displaying", new Object []{Integer.toString(intPage), Integer.toString(intPageCount-1)},SessionData.getLanguage())%></td></tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"reps_merchant_credit_history.jsp" + strUrlParams +"&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"reps_merchant_credit_history.jsp" + strUrlParams +"&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"reps_merchant_credit_history.jsp" + strUrlParams + "&page=" + i + "\">" + i + "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"reps_merchant_credit_history.jsp" + strUrlParams + "&page=" + (intPage+1) + "\">"+Languages.getString("jsp.admin.next",SessionData.getLanguage())+"&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"reps_merchant_credit_history.jsp" + strUrlParams + "&page=" + (intPageCount-1) + "\">"+Languages.getString("jsp.admin.last",SessionData.getLanguage())+"</a>");
              }

              %>
              </td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_merchant_credit_history.date",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_merchant_credit_history.user",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_merchant_credit_history.merchant_businessname",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_merchant_credit_history.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_merchant_credit_history.desc",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_merchant_credit_history.amount",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_merchant_credit_history.new_avail_credit",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.customers.reps_merchant_credit_history.prior_avail_credit",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <%
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td>" + vecTemp.get(7) + "</td>" +
                                "<td>" + vecTemp.get(2) + "</td>" +
                                "<td>" + vecTemp.get(3) + "</td>" +
                                "<td>" + vecTemp.get(4) + "</td>" +
                                "<td>" + vecTemp.get(5) + "</td>" +
                                "<td>" + vecTemp.get(6) + "</td>" +

                        "</tr>");
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            <tr>
            	<td>
            		<input type="button" value="<%=Languages.getString("jsp.admin.reports.Print_This_Page",SessionData.getLanguage())%>" onclick="window.print()">
            	</td>
            	<td>
            		<input type="submit" name="download" value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
            	</td>
            	<jsp:include page="/admin/reports/schreportoption.jsp">
				  	  <jsp:param value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>" name="permissionEnableScheduleReports"/>
				 	  <jsp:param value="<%=SessionData.getLanguage()%>" name="language"/>
				</jsp:include>            	
            </tr>
            </table>            
		</form>
			
			
<%
}
else if (intRecordCount==0 && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.customers.reps_merchant_credit_history.not_found",SessionData.getLanguage())+"</font>");
}
%>

</td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
