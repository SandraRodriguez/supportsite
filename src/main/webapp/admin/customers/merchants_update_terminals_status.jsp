<%@ page import="com.debisys.customers.Merchant,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 java.util.*" %>
<%
int section=2;
int section_page=11;
String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
<jsp:setProperty name="Merchant" property="*" />
<%@ include file="/includes/security.jsp" %>

<%
int intTabIndex = 1;
String strMerchantId = request.getParameter("merchantId");
String strSiteId = request.getParameter("siteId");
String strStatus = request.getParameter("status");

String strMessage = request.getParameter("message");
Vector vecTerminalInfo = new Vector();
if (strMessage == null){
	strMessage="";
}
if (request.getParameter("submitted") != null){
	try{
		if (request.getParameter("submitted").equals("y")){
			Merchant.updateTerminalsStatus(SessionData,strSiteId,strMerchantId,strStatus);
			response.sendRedirect("merchants_info.jsp?merchantId=" + strMerchantId + "&message=25");
		}
	}catch (Exception e){
	}
}
%>
<%@ include file="/includes/header.jsp" %>
<script language="JavaScript"></script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.update_terminal",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#ffffff" class="main">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
				<tr>
					<td class="formArea2">
						<table border="0" width="100%" cellpadding="0" cellspacing="0" align="left">
							<tr>
								<td>
									<form name="terminal" method="post" action="admin/customers/merchants_update_terminals_status.jsp" onsubmit="return check_form(terminal);">
										<input type="hidden" name="merchantId" value="<%=strMerchantId%>">
										<input type="hidden" name="siteId" value="<%=strSiteId%>">
										<input type="hidden" name="status" value="<%=strStatus%>">
										<table width="100%">
											<tr>
<%
if(strStatus.equals("1")){
%>
												<td class="formAreaTitle" align="center"><br><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.disable_terminal",SessionData.getLanguage())%></td>
<%
}else{
%>
												<td class="formAreaTitle" align="center"><br><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.enable_terminal",SessionData.getLanguage())%></td>
<%
}
%>
											</tr>
											<tr>
												<td class="formArea2"></td>
											</tr>
											<tr>
												<td class="formAreaTitle" align="center">
													<input type="hidden" name="submitted" value="y">
													<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.update_terminal",SessionData.getLanguage())%>" tabIndex="<%=intTabIndex++%>">
													<input type="button" value="<%=Languages.getString("jsp.admin.cancel",SessionData.getLanguage())%>" onClick="history.go(-1)" tabIndex="<%=intTabIndex++%>">
												</td>
											</tr>
										</table>
									</form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>
    