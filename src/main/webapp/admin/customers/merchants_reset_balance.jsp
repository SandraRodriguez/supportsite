<%@ page import="com.debisys.utils.DebisysConfigListener"%>
<%
int section=2;
int section_page=5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="page"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
String deploymentType = DebisysConfigListener.getDeploymentType(application);
//domestic
if (deploymentType.equals("0"))
{
  Merchant.resetBalance2(SessionData);
}
else if(deploymentType.equals("1"))
{
	String strCurrencySymbol = NumberUtil.getCurrencySymbol();
	Merchant.setPaymentDescription("Balance reset to " + strCurrencySymbol + "0.00");
  	Merchant.resetBalance(SessionData);

}
response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId());
%>