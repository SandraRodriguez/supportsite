<%@ page import="com.debisys.utils.*,
                 java.util.*"%>
<%@page import="com.debisys.users.User"%>
<%
int section=2;
int section_page=6;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="page"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
  String strMerchantType = Merchant.getMerchantType();
  String deploymentType = DebisysConfigListener.getDeploymentType(application);
  String customConfigType = DebisysConfigListener.getCustomConfigType(application);
  String validateMerchantBalanceMx = DebisysConfigListener.getValidateMerchantBalanceMx(application);
	
    if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
    {
        SessionData.setProperty("iso_id", (new User()).getISOId(DebisysConstants.MERCHANT, Merchant.getMerchantId()));
        SessionData.setProperty("iso_id", SessionData.getUser().getIsoId());
    }

  boolean checkRepCreditLimitValue = Merchant.checkRepCreditLimit(SessionData, application);
  
  boolean optionValidateMmx = false;
  if(validateMerchantBalanceMx.equals(DebisysConstants.ENABLE_VALIDATION_MERCHANT_BALANCE)){
  	optionValidateMmx = checkRepCreditLimitValue;
  }
  else{
  	optionValidateMmx = true;
  }

  boolean bCommissionByValue = false;
  if ( request.getParameter("commissionChecked") != null )
  {
    bCommissionByValue = ((request.getParameter("commissionChecked").equals("1"))?false:true);
  }
  if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
  {
    if (NumberUtil.isNumeric(Merchant.getPaymentAmount()) && NumberUtil.isNumeric(Merchant.getCommission())){

      if (strMerchantType.equals(DebisysConstants.MERCHANT_TYPE_PREPAID))
      {
      //credit based merchant update
      //need to check if rep has enough credit
      //and update rep running liability
            
      if ((checkRepCreditLimitValue) ||
          (optionValidateMmx && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && checkRepCreditLimitValue) || 
          (checkRepCreditLimitValue)){
          // BEGIN MiniRelease 1.0 - Added by YH
          if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
          {
            Vector vTmp = new Vector();
            try
            {
              vTmp = Merchant.doPaymentMX(SessionData, application, bCommissionByValue);
            }
            catch (Exception e)
            {
                    vTmp.add(999);
            }
            if ( vTmp.get(0).toString().equals("0") )
            {
                response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=23");
                return;
            }
            else
            {
                response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=22&code=" + vTmp.get(0));
                return;
            }              
          }
          else 
          {
            //Para Caribe extendido . Reemplazo de la lógica de script por la del SP 
            // el metodo makePayment ocaciona errores en el calculo de los creditos para esta configuración
            Merchant.doPaymentMX(SessionData, application, bCommissionByValue);
   		
            Hashtable errors = Merchant.getErrors();
            if (errors.containsKey("paymentError"))
            {
                response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=21");
                return;
            }
          }
          Hashtable errorsI = Merchant.getErrors();
        if (errorsI.containsKey("error3"))
        {
            response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=20");
            return;
        }        
        response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId());
        return;
      }
      else
      {
        response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=10");
        return;
      }

      }
      else if (strMerchantType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT))
      {
        if (Merchant.checkRepCreditLimit(SessionData, application))
        {
         
        	  Vector vTmp = new Vector();
        	  try
        	  {
        		  vTmp = Merchant.doPaymentMX(SessionData, application, bCommissionByValue);
        	  }
        	  catch (Exception e)
        	  {
        		  vTmp.add(999);
        	  }
        	  if ( vTmp.get(0).toString().equals("0") )
        	  {
                  response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=23");
                  return;
        	  }
        	  else
        	  {
                  response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=22&code=" + vTmp.get(0));
                  return;
        	  }
      }
      else
      {
        response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=10");
        return;
      }

      }

    }
    else
    {
      response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=1");
      return;
    }
  }
  else
  {      
    if( Merchant.checkRepCreditLimit(SessionData, application) ){
        if (NumberUtil.isNumeric(Merchant.getPaymentAmount())){            
            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                Merchant.updateLiabilityLimitMx(SessionData, application, bCommissionByValue);
            }else{
                //Merchant.updateLiabilityLimit(SessionData, application);
                Vector vTmp = new Vector();
        	  try
        	  {
                    vTmp = Merchant.doPaymentMX(SessionData, application, bCommissionByValue);
        	  }
        	  catch (Exception e)
        	  {
        		  vTmp.add(999);
        	  }
        	  if ( vTmp.get(0).toString().equals("0") )
        	  {
                  response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=23");
                  return;
        	  }
        	  else
        	  {
                  response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=22&code=" + vTmp.get(0));
                  return;
        	  }
            }
            Hashtable errorsII = Merchant.getErrors();
            if (errorsII.containsKey("error3"))
            {
                response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=20");
                return;
            }                        
            response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId());
            return;
        }
        else
        {
            response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=1");
            return;
        }
    }
    else{
        response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=10");
        return;
    }
  }
%>