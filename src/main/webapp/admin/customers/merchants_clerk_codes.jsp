<%@ page
    import="com.debisys.customers.Merchant,
    java.net.URLEncoder,
    com.debisys.utils.HTMLEncoder,
    java.util.*,
    com.debisys.utils.LogChanges"%>
    <%
        int section = 2;
        int section_page = 7;

        String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
    %>
    <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
    <jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
    <jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
    <jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
    <jsp:setProperty name="Merchant" property="*" />
    <jsp:setProperty name="Terminal" property="*"/>
    <%@ include file="/includes/security.jsp"%>
    <%    LogChanges.setSession(SessionData);
        LogChanges.setContext(application);

        String strMessage = request.getParameter("message");
        Vector<Vector<String>> vecClerkCodes = new Vector<Vector<String>>();
        Hashtable merchantErrors = null;
        String descriptionError = "";
        
        boolean allowResetClerk = SessionData.checkPermission(DebisysConstants.PERM_RESET_CLERK_CODE) || SessionData.getUser().isIntranetUser();                

        Terminal.getTerminalInfo();
        if (strMessage == null) {
            strMessage = "";
        }
        vecClerkCodes = Merchant.getClerkCodes(application, SessionData);
        int sizeClerkCodes = vecClerkCodes.size();
        if (request.getParameter("submitted") != null) {
            try {
                if (request.getParameter("submitted").equals("y")) {
                    String deleteClerkCodes[] = request.getParameterValues("deleteClerkCode");
                    String newClerkCode = request.getParameter("newClerkCode");
                    String newClerkName = request.getParameter("newClerkName");
                    String newClerkLastName = "";
                    if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                        newClerkLastName = request.getParameter("newClerkLastName");
                    }
                    boolean bNewIsAdmin = request.getParameter("newIsAdmin") != null;

                    if (deleteClerkCodes != null && deleteClerkCodes.length > 0) {
                        if (deleteClerkCodes.length < sizeClerkCodes) {
                            for (int i = 0; i < deleteClerkCodes.length; i++) {
                                if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                    Merchant.deleteClerkCodeMx(application, SessionData, deleteClerkCodes[i]);
                                } else {
                                    Merchant.deleteClerkCode(application, SessionData, deleteClerkCodes[i]);
                                }
                            }
                        } else {
                            Merchant.addFieldError("errorjsp1", Languages.getString("jsp.admin.customers.merchants_info.error_2", SessionData.getLanguage()));
                        }
                    } else if (request.getParameter("mxFeeTable") == null) {
                        if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                            if (newClerkCode != null && !newClerkCode.equals("")) {
                                Merchant.addClerkCodeMx(application, SessionData, newClerkCode, newClerkName, newClerkLastName, bNewIsAdmin);
                                merchantErrors = Merchant.getErrors();
                                if (merchantErrors != null) {
                                    String strError = (String) merchantErrors.get("ErrorValidate");
                                    if (strError.equals("Error")) {
                                        out.println("<script>alert(\"" + Languages.getString("jsp.admin.customers.merchants_clerk_codes.validateInfo", SessionData.getLanguage()) + "\")</script>");
                                    }
                                }
                            }
                        } else {
                            if (newClerkCode != null && !newClerkCode.equals("")) {
                                if (newClerkName != null && !newClerkName.equals("")) {
                                    Merchant.addClerkCode(application, SessionData, newClerkCode, newClerkName, bNewIsAdmin);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("" + e);
            }
        }

        vecClerkCodes = Merchant.getClerkCodes(application, SessionData);

        if (Merchant.isError()) {
            //descriptionError = .getErors()
            Hashtable errorHashtable = Merchant.getErrors();
            Enumeration e = errorHashtable.elements();
            StringBuffer errorMerchant = new StringBuffer();
            // errorMerchant.append("<table>");

            while (e.hasMoreElements()) {
                descriptionError = descriptionError + e.nextElement() + "\n";
            }
            //response.sendRedirect("/support/message.jsp?message=4");
            //return;
        }
    %>
    <%@ include file="/includes/header.jsp" %>
    <script language="JavaScript">
        <!--
    var form = "";
        var error = false;
        var error_message = "";

        function check_input(field_name, field_size, message) {
            if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
                var field_value = form.elements[field_name].value;

                if (field_value == '' || field_value.length < field_size) {
                    error_message = error_message + "* " + message + "\n";
                    error = true;
                }
            }
        }//End of function check_input

        function check_form(form_name) {
            error = false;
            form = form_name;
            error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2", SessionData.getLanguage())%>";

        <%
            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
        %>
            check_input("newClerkName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.no_clerkname", SessionData.getLanguage())%>");
            check_input("newClerkLastName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.no_clerklastname", SessionData.getLanguage())%>");
            check_input("newClerkCode", 1, "<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.no_clerkcode", SessionData.getLanguage())%>");
        <%
            } //End of if when deploying in Mexico
        %>

            if (error == true)
            {
                alert(error_message);
                return false;
            } else
            {
                return true;
            }
        }//End of function check_form

        function validateDivClerkName() {

            var textInfo = "<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.validateNameCleck", SessionData.getLanguage())%> ";
            var idClerk = document.getElementById("newClerkCode").value;
            var nameClerk = document.getElementById("newClerkName").value;
            if (idClerk != null && idClerk != '') {
                var trimNameClerk = trimClerk(nameClerk);
                if (nameClerk != null && trimNameClerk != '') {
                    var div_info = document.getElementById("div_cleck_name");
                    div_info.style.color = "black";
                    div_info.innerHTML = "";
                    return true;
                } else {
                    var div_info = document.getElementById("div_cleck_name");
                    div_info.style.width = "200px";
                    div_info.style.color = "red";
                    div_info.innerHTML = "";
                    var newdiv = document.createElement("div");
                    var newtext = document.createTextNode(textInfo);
                    newdiv.appendChild(newtext); //append text to new div
                    div_info.appendChild(newdiv);
                    return false;
                }
            }
            return true;
        }

        function trimClerk(myString)
        {
            return myString.replace(/^\s+/g, '').replace(/\s+$/g, '');
        }

        //-->
    </script>
    <style>
        .el-dialog__header {
            display: none !important
        }
    </style>
    <%
        String path = request.getContextPath();
    %>
    <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>                                                            
    <script src="<%=path%>/js/themes/vue.js" type="text/javascript"></script>                                          

    <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="<%=path%>/css/themes/vue_upload_style.css" rel="stylesheet" type="text/css"/>                
    <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css" />               

    <script src="<%=path%>/js/sweetalert.min.js" type="text/javascript"></script>                          
    <script type="text/javascript" src="<%=path%>/includes/_lib/tinymce/tinymce.min.js"></script>

    <link rel="stylesheet" href="<%=path%>/css/element_ui.css"/>             
    <script src="<%=path%>/js/vue/element_ui_en.js" type="text/javascript"></script>       

    <div id="ccodes">
        <center>
            <div class="ajax-loading">
                <div></div>
            </div>
        </center>
        <el-dialog   
            width="28%"
            :visible.sync="dialogVisible">

            <el-alert
                v-if="isValidNotfy"
                style="background-color: #ffe0b2"
                :closable=false                
                type="error">
                
                <h4><%=Languages.getString("ss.reset.clerk.codes.warn.1", SessionData.getLanguage())%></h4>
            </el-alert>

            <el-alert
                effect="dark"
                v-if="!isValidNotfy"
                style="background-color: #ffcdd2"
                :closable=false                
                type="error">

                <h3><%=Languages.getString("ss.reset.clerk.codes.err.1", SessionData.getLanguage())%></h3>
            </el-alert>

            <div v-if="isValidNotfy">                 
                <br />
                <br />
                <h3><span class="glyphicon glyphicon-bell" aria-hidden="true"></span> <%=Languages.getString("ss.reset.clerk.codes.label.1", SessionData.getLanguage())%></h3>        
                <small><%=Languages.getString("ss.reset.clerk.codes.label.1.small", SessionData.getLanguage())%></small>
                <hr />
                <br />
                <el-radio v-if="model.smsNotificationPhone" v-model="notify" label="sms">
                    <h5><%=Languages.getString("ss.reset.clerk.codes.sms.option", SessionData.getLanguage())%>
                        <h3>{{model.smsNotificationPhone}}</h3>
                    </h5>
                </el-radio>
                <el-radio v-if="model.mailNotificationAddress" v-model="notify" label="email">
                    <h5><%=Languages.getString("ss.reset.clerk.codes.email.option", SessionData.getLanguage())%>
                        <h3>{{model.mailNotificationAddress}}</h3>
                    </h5>
                </el-radio>

            </div>



            <span slot="footer" class="dialog-footer">
                <hr />                
                <el-button @click="dialogVisible = false"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></el-button>
                <el-button v-if="isValidNotfy" @click="onReset()" type="success"><%=Languages.getString("ss.reset.clerk.codes.send", SessionData.getLanguage())%> <span class="glyphicon glyphicon-send" aria-hidden="true"></span></el-button>
            </span>
        </el-dialog>


        <table border="0" cellpadding="0" cellspacing="0" width="750">
            <tr>
                <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.title", SessionData.getLanguage()).toUpperCase()%></td>
                <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3" bgcolor="#ffffff">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
                        <tr>
                            <td class="formArea2">
                                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="main" align="center">
                                            <br><br>
                                            <%
                                                if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                            %>
                                            <form method="post" action="admin/customers/merchants_clerk_codes.jsp">
                                                <input type="hidden" name="submitted" value="y">
                                                <input type="hidden" name="mxFeeTable" value="y">
                                                <input type="hidden" name="merchantId" value="<%=Merchant.getMerchantId()%>">
                                                <input type="hidden" name="siteId" value="<%=Merchant.getSiteId()%>">
                                                <%
                                                } else {
                                                %>
                                                <form name="merchantClerkCode" method=post action="admin/customers/merchants_clerk_codes.jsp" onSubmit="return validateDivClerkName();">
                                                    <%
                                                        }
                                                    %>
                                                    <table style='margin: 0px 0px 0px 100px !important; font-size: 16px' border="0" width=<%=((customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) ? "100%" : "480")%>>
                                                        <tr>
                                                            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_code", SessionData.getLanguage()).toUpperCase()%></td>
                                                            <td class="rowhead2" nowrap><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_name", SessionData.getLanguage()).toUpperCase()%></td>
                                                            <%
                                                                //sessionUser.checkPermission(strKey)
                                                                if (Terminal.IsTerminalInMBList(application, Terminal.getTerminalType())) {
                                                            %>
                                                            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.isAdmin", SessionData.getLanguage()).toUpperCase()%></td>
                                                            <%
                                                                }
                                                            %>
                                                            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.delete", SessionData.getLanguage()).toUpperCase()%></td>                                                                      
                                                            <%
                                                                if (allowResetClerk) {
                                                            %>
                                                            <td class="rowhead2"></td>          
                                                            <%
                                                                }
                                                            %>
                                                        </tr>
                                                        <%
                                                            if ((vecClerkCodes.size() == 0) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                                out.print("<TR><TD COLSPAN=5 ALIGN=center CLASS=main>" + Languages.getString("jsp.admin.customers.merchants_clerk_codes.noclerks", SessionData.getLanguage()) + "</TD></TR>");
                                                            } else {
                                                                sizeClerkCodes = vecClerkCodes.size();
                                                                String disable_delete = "";
                                                                boolean disable_deleteSecurity = true;

                                                                if (sizeClerkCodes == 1) {
                                                                    //if we have only one clerk, we do not let delete.
                                                                    disable_deleteSecurity = false;
                                                                }

                                                                for (Vector<String> vecTemp : vecClerkCodes) {
                                                                    String clerk_code = vecTemp.get(0);

                                                                    //solo inhabilta cuando en QCOMM y la clave es 1111 
                                                                    //porque en QCOMM se crean los clerk con el 1111 fijo, luego no se puden
                                                                    //eliminar aca.
                                                                    if (clerk_code.equals("1111") && SessionData.getUser().isQcommBusiness() == true) {
                                                                        disable_delete = "disabled";
                                                                    } else {
                                                                        disable_delete = "";
                                                                    }

                                                                    if (!disable_deleteSecurity) {
                                                                        disable_delete = "disabled";
                                                                    }
                                                                    out.println("<tr>"
                                                                            + "<td>" + Merchant.getHideClerk(vecTemp.get(0).toUpperCase()) + "</td>"
                                                                            + "<td>" + vecTemp.get(1) + "</td>"
                                                                            + (Terminal.IsTerminalInMBList(application, Terminal.getTerminalType()) ? ("<td align=\"center\">" + (vecTemp.get(2).equals("1") ? Languages.getString("jsp.admin.customers.merchants_clerk_codes.isAdminYES", SessionData.getLanguage()) : Languages.getString("jsp.admin.customers.merchants_clerk_codes.isAdminNO", SessionData.getLanguage())) + "</td>") : "")
                                                                            + "<td align=center><input " + disable_delete + " type=checkbox name=deleteClerkCode value=\"" + vecTemp.get(0) + "\"></td>"
                                                                            + (allowResetClerk ? "<td align=center><el-button @click='showModal(\""+ vecTemp.get(1) +"\")' style='margin-top: 5px !important' type='warning' plain><span class='glyphicon glyphicon-new-window' aria-hidden='true'></span> " + Languages.getString("ss.reset.clerk.codes.title", SessionData.getLanguage()) + "</el-button>" : "") + "</td>"
                                                                            + "</tr>");                                                                                                                                                                                                            
                                                                }
                                                            }
                                                        %>
                                                        <%
                                                            if ((vecClerkCodes.size() > 0) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                        %>
                                                        <tr>
                                                            <td colspan="3" align="center"><input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.deleteclerks", SessionData.getLanguage())%>"></td>
                                                        </tr>

                                                        <%
                                                            }
                                                        %>
                                                        <tr>
                                                            <td colspan="3" align="center" class="errorText"> 
                                                                <%=descriptionError%>
                                                            </td>
                                                        </tr>
                                                    </table>                     
                                                    <%
                                                        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                    %>
                                                </form>
                                                <%
                                                        out.print(Languages.getString("jsp.admin.customers.merchants_clerk_codes.instructions2", SessionData.getLanguage()));
                                                    } else {
                                                        out.print(Languages.getString("jsp.admin.customers.merchants_clerk_codes.instructions", SessionData.getLanguage()));
                                                    }
                                                %>
                                                <%
                                                    if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                %>
                                                <form name="merchantClerkCode" method="post" action="admin/customers/merchants_clerk_codes.jsp" onSubmit="return check_form(merchantClerkCode);">
                                                    <%
                                                        }
                                                    %>
                                                    <table border="0" cellpadding="3" cellspacing="0" class="main">
                                                        <tr>
                                                            <% if (!SessionData.getUser().isQcommBusiness()) {%>
                                                            <td colspan="2" align="center"><b><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.enter", SessionData.getLanguage())%></b></td>
                                                                    <% } %>
                                                        </tr>
                                                        <%

                                                            LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_CLERKCODE, "");
                                                            LogChanges.set_value_for_check(LogChanges.LOGCHANGETYPE_TERM_CLERKNAME, "");

                                                            // BEGIN MiniRelease 1.0 - Added by YH
                                                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                                out.println("<tr>"
                                                                        + "<td>" + Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_nameMx", SessionData.getLanguage()) + "</td>"
                                                                        + "<td><input type=text name=newClerkName value=\"\" size=20 maxlength=16 ></td>"
                                                                        + "</tr>"
                                                                        + "<tr>"
                                                                        + "<td>" + Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_lastNameMx", SessionData.getLanguage()) + "</td>"
                                                                        + "<td><input type=text name=newClerkLastName value=\"\" size=20 maxlength=24 ></td>"
                                                                        + "</tr>"
                                                                        + "<tr>"
                                                                        + "<td>" + Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_codeMx", SessionData.getLanguage()) + "</td>"
                                                                        + "<td><input type=text name=newClerkCode value=\"\" size=20 maxlength=10></td>"
                                                                        + "</tr>");
                                                                if (Terminal.IsTerminalInMBList(application, Terminal.getTerminalType())) {
                                                        %>
                                                        <tr>
                                                            <td><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.isAdmin", SessionData.getLanguage())%></td>
                                                            <td><input type="checkbox" name="newIsAdmin"></td>
                                                        </tr>
                                                        <%
                                                            }
                                                        } else {
                                                            out.println("<tr>"
                                                                    + "<td>" + Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_code", SessionData.getLanguage()) + "</td>"
                                                                    + "<td><input type=text id=\"newClerkCode\" name=newClerkCode value=\"\" size=20 maxlength=10></td>"
                                                                    + "</tr>"
                                                                    + "<tr>"
                                                                    + "<td>" + Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_name", SessionData.getLanguage()) + "</td>"
                                                                    + "<td><input type=text id=\"newClerkName\" name=newClerkName value=\"\" size=20 maxlength=50></td><td><div id=\"div_cleck_name\" name=\"div_cleck_name\"></div></td>"
                                                                    + "</tr>");
                                                            if (Terminal.IsTerminalInMBList(application, Terminal.getTerminalType())) {
                                                        %>
                                                        <tr>
                                                            <td><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.isAdmin", SessionData.getLanguage())%></td>
                                                            <td><input type="checkbox" name="newIsAdmin"></td>
                                                        </tr>
                                                        <%
                                                                }
                                                            }
                                                            // END MiniRelease 1.0 - Added by YH
                                                        %>
                                                        <tr>
                                                            <td>
                                                                <div id="errorVal" style="display:none;">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <input type="hidden" name="merchantId" value="<%=Merchant.getMerchantId()%>">
                                                                <input type="hidden" name="siteId" value="<%=Merchant.getSiteId()%>">
                                                                <input type="hidden" name="submitted" value="y">
                                                                <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.submit", SessionData.getLanguage())%>">

                                                                <%
                                                                    if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                                        out.println("</form><form action='admin/customers/merchants_info.jsp'><input type=submit value=\"" + Languages.getString("jsp.admin.customers.merchants_clerk_codes.back", SessionData.getLanguage()) + "\">");
                                                                        out.println("<input type=hidden name=merchantId value='" + Merchant.getMerchantId() + "'>");
                                                                        out.println("</form>");
                                                                    }
                                                                %>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br><br>
                                                    </td>
                                                    </tr>
                                                    <%
                                                        if (!customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                    %>
                                                </form>
                                                <%
                                                    }
                                                %>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>

</div>
<script src="<%=path%>/js/admin/customers/merchants_clerk_codes/merchants_clerk_codes.js" type="text/javascript"></script>        
<%@ include file="/includes/footer.jsp" %>
