<%@ page import="java.util.Vector,
				 java.util.List,
                 java.util.Iterator,
                 java.util.Hashtable,
                 com.debisys.customers.CreditTypes,
                 com.debisys.utils.*,
                 com.debisys.utils.TimeZone" %>
<%
  int section      = 12;
  int section_page = 2;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
  <jsp:setProperty name="Rep" property="*" />
  <%@ include file="/includes/security.jsp" %>
  <%
   String strMessage = request.getParameter("message");

  if (strMessage == null)
  {
    strMessage = "";
  }

  //HashMap entityErrors = null;

  if (request.getParameter("submitted") != null)
  {
    try
    {
      if (request.getParameter("submitted").equals("y"))
      {}
    }
    catch (Exception e)
    {}
  }
  else
  {
    Rep.getAgent(SessionData, application);

    if (Rep.isError())
    {
      response.sendRedirect("/support/message.jsp?message=4");

      return;
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
  <%
  boolean isDomestic = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
  %>
<script language="JavaScript">
<!--    
	function checkPayment()
	{
	    var formApplyPmt = document.payment.paymentAmount.value;
    if (isNaN(formApplyPmt))
		{
			alert("<%=Languages.getString("jsp.admin.customers.reps_info.jsmsg1",SessionData.getLanguage())%>");
			document.payment.paymentAmount.focus();
      return false;
		}
		else
		{
			DisableButtons();
			return true;
		}
	}
	function checkCreditLimit()
	{
		var formCreditLimit = document.creditLimit.creditLimit.value
    if (isNaN(formCreditLimit))
		{
			alert("<%= Languages.getString("jsp.admin.customers.reps_info.jsmsg1",SessionData.getLanguage()) %>");
			document.creditLimit.creditLimit.focus();
      return false;
		}
		else
		{
			DisableButtons();
			return true;
		}
	}

    function calculate()
    {
    	if (isNaN(document.payment.commission.value)== false && isNaN(document.payment.paymentAmount.value)== false)
    	{

          var intGrossPayment = 0;
          var intNetPayment   = 0;
          var intCommissionAmount   = 0;
          var intCommissionPercent   = 0;

          intGrossPayment = document.payment.paymentAmount.value;
          intCommissionPercent   = document.payment.commission.value;
          intCommissionAmount = intGrossPayment * (intCommissionPercent * .01);
          intNetPayment = intGrossPayment - intCommissionAmount;
          document.payment.netPaymentAmount.value = formatAmount(intNetPayment);

    	}
    }

    function validate(c)
    {
    	if (isNaN(c.value))
    	{
    		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
    		c.focus();
    		return (false);
    	}
    	else
    	{
				c.value = formatAmount(c.value);
  		}
    }


	function formatAmount(n)
	{
  		var s = "" + Math.round(n * 100) / 100
  		var i = s.indexOf('.')
  		if (i < 0) return s + ".00"
  		var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3)
  		if (i + 2 == s.length) t += "0"
  		return t
	}



  function GetPaymentAmountValue()
  {
    return document.payment.paymentAmount.value;
  }

  function SetRepMerchantPayment(sText)
  {
    document.getElementById("txtRepMerchantPayment").value = sText;
    document.getElementById("btnApplyPayment").disabled = false;
    document.getElementById("btnApplyPayment").click();
    document.getElementById("btnApplyPayment").disabled = true;
  }

  function SetRepMerchantMigration(sText)
  {
    document.getElementById("txtRepMerchantMigration").value = sText;
    if ( sText.length > 0 )
    {
      document.getElementById("frmRepMerchantMigration").submit();
    }
  }//End of function SetRepMerchantMigration

  function ValidatePaymentValueMX(cText, bAllowMinus)
  {
    var sText = cText.value;
    var sResult = "";
    var bHasPeriod = false;
    var bHasMinus = false;
    for ( i = 0; i < sText.length; i++ )
    {
      if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) )
      {
        sResult += sText.charAt(i);
      }
      if ( !bHasPeriod && sText.charCodeAt(i) == 46 )/*period*/
      {
        sResult += sText.charAt(i);
        bHasPeriod = true;
      }
      if ( bAllowMinus && !bHasMinus && (sText.charCodeAt(i) == 45) && (i == 0) )/* - */
      {
        sResult += sText.charAt(i);
        bHasMinus = true;
      }
    }
    if ( sResult.length == 0 )
    {
      sResult = "0";
    }
    if ( sText != sResult )
    {
      cText.value = sResult;
    }
  }//End of function ValidatePaymentValueMX

  function ValidatePrepaidLimitMX(cText)
  {
    if ( (cText.value < 0) && (Math.abs(cText.value) > (<%=Rep.getCreditLimit()%> - <%=Rep.getRunningLiability()%>)) && (<%=Rep.getRunningLiability()%> > 0) )
    {
      alert("<%=Languages.getString("jsp.admin.customers.reps_info.paymentgreaterthanavailable",SessionData.getLanguage())%>");
      cText.focus();
      cText.select();
      return;
    }
    if ( (cText.value < 0) && (Math.abs(cText.value) > <%=Rep.getCreditLimit()%>) )
    {
      alert("<%=Languages.getString("jsp.admin.customers.reps_info.absnegativepaymentgreaterthanlimit",SessionData.getLanguage())%>");
      cText.focus();
      cText.select();
    }
  }//End of function ValidatePrepaidLimitMX

  function ValidateUnlimitedMX(cText)
  {
    if ( Math.abs(cText.value) > <%=Rep.getRunningLiability()%> )
    {
      alert("<%=Languages.getString("jsp.admin.customers.reps_info.unlimitedpaymentgreaterthanrunning",SessionData.getLanguage())%>");
      cText.focus();
      cText.select();
    }
  }//End of function ValidateUnlimitedMX

  function ValidateCreditLimitMX(cText)
  {
    if ( (cText.value > 0) && (<%=Rep.getRunningLiability()%> == 0) )
    {
      alert("<%=Languages.getString("jsp.admin.customers.reps_info.paymentnegative",SessionData.getLanguage())%>");
      cText.focus();
      cText.select();
      return;
    }
    if ( cText.value > <%=Rep.getRunningLiability()%> )
    {
      alert("<%=Languages.getString("jsp.admin.customers.reps_info.unlimitedpaymentgreaterthanrunning",SessionData.getLanguage())%>");
      cText.focus();
      cText.select();
    }
  }//End of function ValidateCreditLimitMX

  function DisableButtons()
  {
    var ctl = document.getElementById("btnUpdateCreditLimit");
    if ( ctl != null )
    {
      ctl.disabled = true;
    }
    ctl = document.getElementById("btnEditRep");
    if ( ctl != null )
    {
      ctl.disabled = true;
    }
    ctl = document.getElementById("btnChangeType");
    if ( ctl != null )
    {
      ctl.disabled = true;
    }
    ctl = document.getElementById("btnApplyPayment");
    if ( ctl != null )
    {
      ctl.disabled = true;
    }
  }
	-->
</script>

<script type="text/javascript">
  //DTU-369 Payment Notifications
  function disableChk()
  {
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
		<%
		if (Rep.isPaymentNotificationSMS())
		{
		%>
		 	valuePayNotiSMSChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiSMSChk.checked=false;
		<%
		}
		%>
		valuePayNotiSMSChk.disabled=true;
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
		<%
		if (Rep.isPaymentNotificationMail())
		{
		%>
		 	valuePayNotiMailChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiMailChk.checked=false;
		<%
		}
		%>
  		valuePayNotiMailChk.disabled=true;
  		var valuePayNotiChk = document.getElementById('paymentNotifications');
		<%
		if (Rep.isPaymentNotifications())
		{
		%>
		 	valuePayNotiChk.checked=true;
		<%
		}
		else
		{
		%>
			valuePayNotiChk.checked=false;
			valuePayNotiSMSChk.checked=false;
			valuePayNotiMailChk.checked=false;
		<%
		}
		%>
  		valuePayNotiChk.disabled=true;
  }
  
  //DTU-480: Low Balance Alert Message SMS and/or email
  function disableBalChk()
  {
    	var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		<%
		if (Rep.isBalanceNotificationTypeValue())
		{
		%>
		 	valueBalNotiValueChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiValueChk.checked=false;
		<%
		}
		%>
		valueBalNotiValueChk.disabled=true;
    	var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		<%
		if (Rep.isBalanceNotificationTypeDays())
		{
		%>
		 	valueBalNotiDaysChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiDaysChk.checked=false;
		<%
		}
		%>
		valueBalNotiDaysChk.disabled=true;		
  		var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		<%
		if (Rep.isBalanceNotificationSMS())
		{
		%>
		 	valueBalNotiSMSChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiSMSChk.checked=false;
		<%
		}
		%>
		valueBalNotiSMSChk.disabled=true;
  		var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		<%
		if (Rep.isBalanceNotificationMail())
		{
		%>
		 	valueBalNotiMailChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiMailChk.checked=false;
		<%
		}
		%>
  		valueBalNotiMailChk.disabled=true;
  		var valueBalNotiChk = document.getElementById('balanceNotifications');
		<%
		if (Rep.isBalanceNotifications())
		{
		%>
		 	valueBalNotiChk.checked=true;
		<%
		}
		else
		{
		%>
			valueBalNotiChk.checked=false;
			valueBalNotiSMSChk.checked=false;
			valueBalNotiMailChk.checked=false;
			valueBalNotiValueChk.checked=false;
			valueBalNotiDaysChk.checked=false;
		<%
		}
		%>
  		valueBalNotiChk.disabled=true;
  }  
  
</script>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td colspan="3">
        <table border="0" cellpadding="0" cellspacing="0" width="750">
          <tr>
            <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
            <td background="images/top_blue.gif" width="2000" class="formAreaTitle">
              &nbsp;
              <%= Languages.getString("jsp.admin.customers.agents_info.agent_detail",SessionData.getLanguage()).toUpperCase() %>
            </td>
            <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#ffffff">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
<%
          if (strMessage.equals("1"))
          {
            out.println("<tr bgcolor=ffffff><td align=center class=main><img src=images/information.png border=0 valign=middle><font color=000000>" + Languages.getString(
                    "jsp.admin.customers.agents_info.agent_update_success",SessionData.getLanguage()) + "</td></tr>");
          }
          else if ( strMessage.equals("10") )
          {
              out.println("<tr bgcolor=ffffff><td align=center class=main><img src=images/information.png border=0 valign=middle><font color=000000>" + Languages.getString(
              "jsp.admin.customers.agents_info.paymentsuccessful",SessionData.getLanguage()) + "</td></tr>");
          }
          else if ( strMessage.equals("11") )
          {
              out.println("<tr bgcolor=ffffff><td align=center class=main><img src=images/information.png border=0 valign=middle><font color=000000>" + Languages.getString(
              "jsp.admin.customers.agents_info.paymentrepok_SubAgentfailed",SessionData.getLanguage()) + "</td></tr>");
          }
          else if ( strMessage.equals("12") )
          {
              out.println("<tr bgcolor=ffffff><td align=center class=main><img src=images/information.png border=0 valign=middle><font color=000000>" + Languages.getString(
              "jsp.admin.customers.reps_info.paymentrepfailed",SessionData.getLanguage()) + request.getParameter("code") + "</td></tr>");
          }
           else if ( strMessage.equals("13") )
	        {
	            out.println("<tr bgcolor=ffffff><td align=center class=main><img src=images/information.png border=0 valign=middle><font color=000000>" + Languages.getString(
	            "jsp.admin.customers.merchants_info.paymenterror",SessionData.getLanguage()) + request.getParameter("code") + "</td></tr>");
	        }
	        else if ( strMessage.equals("14") )
	        {
	            out.println("<tr bgcolor=ffffff><td align=center class=main><img src=images/information.png border=0 valign=middle><font color=000000>" + Languages.getString(
	            "jsp.admin.customers.merchants_info.paymentsuccessful",SessionData.getLanguage()) + "</td></tr>");
	        }
%>
          <tr>
            <td class="formArea2">
              <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                    <table border="0" width="50" cellpadding="0" cellspacing="0">
                      <tr>
                        <td>
                          <form method="post" action="admin/customers/agents_edit.jsp?repId=<%=Rep.getRepId()%>" onsubmit="DisableButtons();">
                            <input type="hidden" name="repId" value="<%= Rep.getRepId() %>">
                            <input id="btnEditRep" type="submit" name="submit" value="<%= Languages.getString("jsp.admin.customers.agents_info.edit_agent_info",SessionData.getLanguage()) %>">
                          </form>
                        </td>
                      </tr>
                    </table>
                    <table width="100%">
                      <tr>
                        <td class="formAreaTitle2">
                          <%= Languages.getString("jsp.admin.customers.reps_add.company_information",SessionData.getLanguage()) %>
                        </td>
                      </tr>
                      <tr>
                        <td class="formArea2">
                          <table>
<%
                            if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(
                                    DebisysConstants.DIST_CHAIN_5_LEVEL)))
                            {
%>
                              <tr>
                                <td class="main">
                                  <b><%= Languages.getString("jsp.admin.customers.agents_add.iso",SessionData.getLanguage()) %>:</b>
                                </td>
                                <td nowrap class="main">
                                  <%= Rep.getRepName(Rep.getParentRepId()) %>
                                </td>
                              </tr>
<%
                            }

%>


                            </tr>
<%
                          if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                          {
                            //If when deploying in Mexico
%>
                              <tr>
                                  <td class="main">
                                    <b><%= Languages.getString("jsp.admin.customers.reps_edit.legal_businessname",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap class="main">
                                    <%= Rep.getLegalBusinessname() %>
                                  </td>
                                </tr>
<%
                            }
%>
                            <tr>
                              <td class="main">
                                <b><%= Languages.getString("jsp.admin.customers.reps_add.business_name",SessionData.getLanguage()) %>:</b>
                              </td>
                              <td nowrap class="main">
                                <%= Rep.getBusinessName() %>
                              </td>
                            </tr>
<%
                          if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                          {
                            //If when deploying in Mexico
%>
                            <TR>
                              <TD CLASS="main">
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.businesstype",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD NOWRAP CLASS="main">
<%
                                Vector   vecBusinessTypes = com.debisys.customers.Merchant.getBusinessTypes(customConfigType);
                                Iterator itBusinessTypes  = vecBusinessTypes.iterator();

                                while (itBusinessTypes.hasNext()) {
                                  Vector vecTemp = null;

                                  vecTemp = (Vector)itBusinessTypes.next();

                                  String strBusinessTypeId   = vecTemp.get(0).toString();
                                  String strBusinessTypeCode = vecTemp.get(1).toString();

                                  if (strBusinessTypeId.equals(Rep.getBusinessTypeId())) {
                                    out.println(strBusinessTypeCode);

                                    break;
                                  }
                                }
%>
                              </TD>
                            </TR>
                            <TR>
                              <TD CLASS="main">
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD NOWRAP CLASS="main">
<%
                                Vector   vecMerchantSegments = com.debisys.customers.Merchant.getMerchantSegments();
                                Iterator itMerchantSegments  = vecMerchantSegments.iterator();

                                while (itMerchantSegments.hasNext()) {
                                  Vector vecTemp = null;

                                  vecTemp = (Vector)itMerchantSegments.next();

                                  String strMerchantSegmentId   = vecTemp.get(0).toString();
                                  String strMerchantSegmentCode = vecTemp.get(1).toString();

                                  if (strMerchantSegmentId.equals(Integer.toString(Rep.getMerchantSegmentId()))) {
                                    out.println(Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" +
                                            strMerchantSegmentCode,SessionData.getLanguage()));

                                    break;
                                  }
                                }
%>
                              </TD>
                            </TR>
                            <TR>
                              <TD CLASS="main">
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.businesshours",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD NOWRAP CLASS="main">
                                <%= Rep.getBusinessHours() %>
                              </TD>
                            </TR>
<%
                          }
%>
							<TR>
								<TD CLASS="main">
									<B><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZone",SessionData.getLanguage())%>:</B>
								</TD>
								<TD NOWRAP CLASS="main"><%=TimeZone.getTimeZoneName(Rep.getTimeZoneId())%></TD>
							</TR>

							<%
  // DBSY-931 System 2000 Tools s2k
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If when deploying in International and user is an ISO level
%>
<tr>
								<TD CLASS="main">
										<B><%= Languages.getString("jsp.admin.tools.s2k.addedit.terms",SessionData.getLanguage())%>:</B>
									</TD>
									<TD NOWRAP CLASS="main"><%=Rep.getterms()%></TD>
</tr>
								<tr>
								<TD CLASS="main">
										<B><%= Languages.getString("jsp.admin.customers.salesmanid",SessionData.getLanguage())%>:</B>
									</TD>
								<TD NOWRAP CLASS="main"><%=Rep.getsalesmanid()%>
								<B>   <%= Languages.getString("jsp.admin.customers.salesmanname",SessionData.getLanguage())%>:   </B>
								<%=Rep.getsalesmanname()%></td>
								</tr>
<%
	}

	// DBSY-1072 eAccounts Interface
	if(SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
%>
								<tr>
									<td class="main">
										<b><%=Languages.getString("jsp.admin.customers.entity_account_type",SessionData.getLanguage())%>:</b>
									</td>
									<td class="main"><%=EntityAccountTypes.GetEntityAccountTypeDesc(Rep.getEntityAccountType())%></td>
								</tr>
<%
	}
%>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td class="formAreaTitle2">
                          <br>
                          <%= Languages.getString("jsp.admin.customers.reps_add.address",SessionData.getLanguage()) %>
                        </td>
                      </tr>
                      <tr>
                        <td class="formArea2">
                          <table width="100">
                            <tr class="main">
<%
                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                              //If when deploying in Mexico
%>
                              <td>
                                <b><%= Languages.getString("jsp.admin.customers.merchants_info.street",SessionData.getLanguage()) %>:</b>
                              </td>
                              <td colspan="4">
                                <%= Rep.getAddress() %>
                              </td>
<%
                            } else {
                              //Else when deploying in others
%>
                              <td>
                                <b><%= Languages.getString("jsp.admin.customers.reps_add.street_address",SessionData.getLanguage()) %>:</b>
                              </td>
                              <td nowrap>
                                <%= Rep.getAddress() %>
                              </td>
<%
                            }
%>
                            </tr>
<%
                          if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                            //If when deploying in Mexico
%>
                            <TR CLASS="main">
                              <TD>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD NOWRAP>
                                <%= Rep.getPhysColony() %>
                              </TD>
                              <TD>
                                &nbsp;
                              </TD>
                              <TD>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD NOWRAP>
                                <%= Rep.getPhysDelegation() %>
                              </TD>
                            </TR>
                            <TR CLASS="main">
                              <TD NOWRAP>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD NOWRAP>
                                <%= Rep.getCity() %>
                              </TD>
                              <TD>
                                &nbsp;
                              </TD>
                              <TD NOWRAP>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD NOWRAP>
                                <%= Rep.getZip() %>
                              </TD>
                              <TD>
                                &nbsp;
                              </TD>
                              <TD NOWRAP>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage()) %></B>
                              </TD>
                              <TD NOWRAP>
<%
                                Vector   vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
                                Iterator itStates  = vecStates.iterator();

                                while (itStates.hasNext()) {
                                  Vector vecTemp = null;

                                  vecTemp = (Vector)itStates.next();

                                  String strStateId   = vecTemp.get(0).toString();
                                  String strStateName = vecTemp.get(1).toString();

                                  if (strStateId.equals(Rep.getState())) {
                                    out.println(strStateName);

                                    break;
                                  }
                                }
%>
                              </TD>
                            </TR>
                        <tr class="main">
                          <td>
                            <b><%= Languages.getString("jsp.admin.customers.merchants_info.county",SessionData.getLanguage()) %>:</b>
                          </td>
                          <td nowrap>
                            <%= Rep.getPhysCounty() %>
                          </td>
                        </tr>
<%
                          } else {
                            //Else when deploying in others
%>
                            <tr class="main">
                              <td nowrap>
                                <b><%
                                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                                {
                                  out.println(Languages.getString("jsp.admin.customers.reps_info.city_state_zip",SessionData.getLanguage()) + ":");
                                }
                                else
                                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                {
                                  out.println(Languages.getString("jsp.admin.customers.reps_info.city_province_postal_code",SessionData.getLanguage())
                                          + ":");
                                }
%></b>
                            </td>
                            <td nowrap>
                              <%= Rep.getCity() + ", " + Rep.getState() + " " + Rep.getZip() %>
                            </td>
                          </tr>
<%
                        }
%>
                          <tr class="main">
                            <td>
<%
                              if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                              {
                                out.println("<b>");
                              }
%>
                              <%= Languages.getString("jsp.admin.customers.reps_add.country",SessionData.getLanguage()) %>:
                            </td>
                            <td nowrap>
                              <%= CountryInfo.getAllCountriesID().get(Integer.parseInt(Rep.getCountry())) %>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
<%
                  if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    //If when deploying in Mexico
%>
                    <TR>
                      <TD CLASS="formAreaTitle2">
                        <BR>
                        <%= Languages.getString("jsp.admin.customers.merchants_edit.billingaddress",SessionData.getLanguage()) %>
                      </TD>
                    </TR>
                    <tr>
                      <td class="formArea2">
                        <table width="100">
                          <tr class="main">
                            <td>
                              <b><%= Languages.getString("jsp.admin.customers.merchants_edit.street_address",SessionData.getLanguage()) %>:</b>
                            </td>
                            <TD COLSPAN="4">
                              <%= Rep.getMailAddress() %>
                            </TD>
                          </TR>
                          <TR CLASS="main">
                            <TD>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage()) %>:</B>
                            </TD>
                            <TD NOWRAP>
                              <%= Rep.getMailColony() %>
                            </TD>
                            <TD>
                              &nbsp;
                            </TD>
                            <TD>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage()) %>:</B>
                            </TD>
                            <TD NOWRAP>
                              <%= Rep.getMailDelegation() %>
                            </TD>
                          </TR>
                          <TR CLASS="main">
                            <TD NOWRAP>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage()) %>:</B>
                            </TD>
                            <TD NOWRAP>
                              <%= Rep.getMailCity() %>
                            </TD>
                            <TD>
                              &nbsp;
                            </TD>
                            <TD NOWRAP>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage()) %>:</B>
                            </TD>
                            <TD NOWRAP>
                              <%= Rep.getMailZip() %>
                            </TD>
                            <TD>
                              &nbsp;
                            </TD>
                            <TD NOWRAP>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage()) %></B>
                            </TD>
                            <TD NOWRAP>
<%
                              Vector   vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
                              Iterator itStates  = vecStates.iterator();

                              while (itStates.hasNext()) {
                                Vector vecTemp = null;

                                vecTemp = (Vector)itStates.next();

                                String strStateId   = vecTemp.get(0).toString();
                                String strStateName = vecTemp.get(1).toString();

                                if (strStateId.equals(Rep.getMailState())) {
                                  out.println(strStateName);

                                  break;
                                }
                              }
%>
                            </TD>
                          </TR>
                          <tr class="main">
                            <td>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage()) %>:</B>
                            </td>
                            <td nowrap>
                              <%= Rep.getMailCounty() %>
                            </td>
                          </tr>
                          <tr class="main">
                            <td>
                              <B><%= Languages.getString("jsp.admin.customers.merchants_edit.country",SessionData.getLanguage()) %>:</B>
                            </td>
                            <td nowrap>
                              <%= CountryInfo.getAllCountriesID().get(Integer.parseInt(Rep.getMailCountry())) %>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
<%
                  }
%>
                    <tr>
                      <td class="formAreaTitle2">
                        <br>
                        <%= Languages.getString("jsp.admin.customers.reps_add.contact_information",SessionData.getLanguage()) %>
                      </td>
                    </tr>
                    <tr>
                      <td class="formArea2">
<%
        if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
        {
%>
                                                <TABLE WIDTH="100%" CELLSPACING="1" CELLPADDING="1" BORDER="0">
                                                    <TR>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contacttype",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contact",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.phone",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.fax",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.email",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.cellphone",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.department",SessionData.getLanguage()).toUpperCase()%></TD>
                                                    </TR>
<%
          int intEvenOdd = 1;
          Vector vecContacts = Rep.getVecContacts();
          Iterator itContacts = vecContacts.iterator();
          while (itContacts.hasNext())
          {
            Vector vecTemp = null;
            vecTemp = (Vector)itContacts.next();
%>
                                                    <TR CLASS="row<%=intEvenOdd%>">
                                                        <TD><%=com.debisys.customers.Merchant.getContactTypeById(vecTemp.get(0).toString(),SessionData)%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(1).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(2).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(3).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(4).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(5).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(6).toString())%></TD>
                                                    </TR>
<%
            intEvenOdd = ((intEvenOdd == 1)?2:1);
          }
%>
                                                </TABLE>
				        						<table width="100">
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Rep.getSmsNotificationPhone())%></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Rep.getMailNotificationAddress())%></td>
													</tr>												
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox"/>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td  nowrap class="main"><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" />
														</td>
													</tr>
													<tr>
														<td  nowrap class="main"><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" />
														</td>
													</tr>													
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><%=Rep.getBalanceNotificationValue()%></td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><%=Rep.getBalanceNotificationDays()%></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" />
														</td>
													</tr>
<%
   }
%>
												</table>
												  <script>disableChk()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>disableBalChk()</script>
<%
   }
%>												  
<%
        }
        else
        {
%>
                        <table width="100">
                          <tr class="main">
                            <td nowrap>
                              <b><%= Languages.getString("jsp.admin.customers.reps_add.contact",SessionData.getLanguage()) %>:</b>
                            </td>
                            <td nowrap>
                              <%= Rep.getContactFirst() + " " + Rep.getContactMiddle() + " " + Rep.getContactLast() %>
                            </td>
                          </tr>
                          <tr class="main">
                            <td>
                              <b><%= Languages.getString("jsp.admin.customers.reps_add.phone",SessionData.getLanguage()) %>:</b>
                            </td>
                            <td nowrap>
                              <%= Rep.getContactPhone() %>
                            </td>
                          </tr>
                          <tr class="main">
                            <td>
                              <%= Languages.getString("jsp.admin.customers.reps_add.fax",SessionData.getLanguage()) %>:
                            </td>
                            <td nowrap>
                              <%= Rep.getContactFax() %>
                            </td>
                          </tr>
                          <tr class="main">
                            <td>
                              <%= Languages.getString("jsp.admin.customers.reps_add.email",SessionData.getLanguage()) %>:
                            </td>
                            <td nowrap>
                              <%= Rep.getContactEmail() %>
                            </td>
                          </tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Rep.getSmsNotificationPhone())%></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Rep.getMailNotificationAddress())%></td>
													</tr>												
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" value="<%=Rep.isPaymentNotifications()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" value="<%=Rep.isPaymentNotificationSMS()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" value="<%=Rep.isPaymentNotificationMail()%>" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" value="<%=Rep.isBalanceNotifications()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox"  value="<%=Rep.isBalanceNotificationTypeValue()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" value="<%=Rep.isBalanceNotificationTypeDays()%>" />
														</td>
													</tr>													
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><%=Rep.getBalanceNotificationValue()%></td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><%=Rep.getBalanceNotificationDays()%></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" value="<%=Rep.isBalanceNotificationSMS()%>" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" value="<%=Rep.isBalanceNotificationMail()%>" />
														</td>
													</tr>
<%
   }
%>

					</table>
					  <script>disableChk()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>disableBalChk()</script>
<%
   }
%>					  
<%
        }
%>
                      </td>
                    </tr>

              <%
              if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES))
              {
              %>

                    <tr>
      						<td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.common_info_edit.payment_information",SessionData.getLanguage())%></td>
      					</tr>
      					<tr>
	        					<td class="formArea2">
	        						<table width="100">
              						<tr class="main">
              						 <td nowrap>
              						   <%=Languages.getString("jsp.admin.customers.common_info_edit.payment_type",SessionData.getLanguage())%>
              						 </td>
               						 <td nowrap>
               						  <SELECT NAME="paymentType" disabled>
               						   <%
										  Vector vecPaymentTypes = com.debisys.customers.Rep.getPaymentTypes();
										  Iterator itPaymentTypes = vecPaymentTypes.iterator();
										  boolean flag_payment=false;
										  while (itPaymentTypes.hasNext()) {
										    Vector vecTemp = null;
										    vecTemp = (Vector)itPaymentTypes.next();
										    String strPaymentTypeId = vecTemp.get(0).toString();
										    String strPaymentTypeCode = vecTemp.get(1).toString();
										    if ( (request.getParameter("paymentType") != null) && (strPaymentTypeId.equals(request.getParameter("paymentType"))) ){
										        flag_payment = true;
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
										    }
										    else if ( strPaymentTypeId.equals(Integer.toString(Rep.getPaymentType())) ){
										        flag_payment = true;
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
										    }
										    else
										      out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\">" + strPaymentTypeCode + "</OPTION>");
										  }
										  if ( !flag_payment ){
										   %>
										      <OPTION VALUE="-1" SELECTED>--</OPTION>
										   <%
										  }
										%>

               						  </SELECT>
               						  </TD>

               						  <td nowrap>
               						  <% if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
						                		   <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type",SessionData.getLanguage())%>
						                		   <% } else{%>
						                		   <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type_international",SessionData.getLanguage())%>
						               <%}%>
              						 </td>
               						 <td nowrap>
               						  <SELECT NAME="processType" disabled>
               						     <%
										  Vector vecProcessTypes = com.debisys.customers.Rep.getProcessorTypes();
										  Iterator itProcessTypes = vecProcessTypes.iterator();
										  boolean flag_process=false;
										  while (itProcessTypes.hasNext()) {
										    Vector vecTemp = null;
										    vecTemp = (Vector)itProcessTypes.next();
										    String strProcessTypeId = vecTemp.get(0).toString();
										    String strProcessTypeCode = vecTemp.get(1).toString();

										    if ( (request.getParameter("processType") != null) && (strProcessTypeId.equals(request.getParameter("processType"))) ){
										        flag_process = true;
										        out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
										    }
										    else if ( strProcessTypeId.equals(Integer.toString(Rep.getProcessType())) ){
										        flag_process = true;
										        out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
										    }
										    else
										      out.println("<OPTION VALUE=\"" + strProcessTypeId + "\">" + strProcessTypeCode + "</OPTION>");
										  }
										  if ( !flag_process ){
										   %>
										      <OPTION VALUE="-1" SELECTED>--</OPTION>
										   <%
										  }
										%>
               						  </SELECT>
               						  </TD>
               						  </tr>
               						</table>
               			 </tr>

                    <%
					 }
					%>



<%
                    if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                    {
                      //If when deploying in Mexico
%>
                      <tr>
                        <td class="formAreaTitle2">
                          <br>
                          <%= Languages.getString("jsp.admin.customers.merchants_edit.banking_information",SessionData.getLanguage()) %>
                        </td>
                      </tr>
                      <tr>
                        <td class="formArea2">
                          <table width="100">
                            <tr class="main">
                              <TD NOWRAP>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.tax_id",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <td nowrap>
                                <%= Rep.getTaxId() %>
                              </td>
                            </tr>
                            <TR CLASS="main">
                              <TD NOWRAP>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.bankname",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD>
                                <%= Rep.getBankName() %>
                              </TD>
                            </TR>
                            <TR CLASS="main">
                              <TD NOWRAP>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.accounttype",SessionData.getLanguage()) %>:</B>
                              </TD>
                              <TD>
<%
                                Vector   vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
                                Iterator itAccountTypes  = vecAccountTypes.iterator();

                                while (itAccountTypes.hasNext()) {
                                  Vector vecTemp = null;

                                  vecTemp = (Vector)itAccountTypes.next();

                                  String strAccountTypeId   = vecTemp.get(0).toString();
                                  String strAccountTypeCode = vecTemp.get(1).toString();

                                  if (strAccountTypeId.equals(Integer.toString(Rep.getAccountTypeId()))) {
                                    out.println(Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" +
                                            strAccountTypeCode,SessionData.getLanguage()));

                                    break;
                                  }
                                }
%>
                              </TD>
                            </TR>
                            <tr class="main">
                              <td nowrap>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.routing_number",SessionData.getLanguage()) %>:</B>
                              </td>
                              <td nowrap>
                                <%= Rep.getRoutingNumber() %>
                              </td>
                            </tr>
                            <tr class="main">
                              <td>
                                <B><%= Languages.getString("jsp.admin.customers.merchants_edit.account_number",SessionData.getLanguage()) %>:</B>
                              </td>
                              <td nowrap>
                                <%= Rep.getAccountNumber() %>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
<%
                    }//End of if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) || deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                    {
%>
                      <tr>
                        <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.reps_add.credit_limit",SessionData.getLanguage())%>
<%
                      if (strMessage.equals("2"))
                      {
                        //jsp.admin.error2=Please enter a valid number.
                        out.println("<br><font color=ff0000>" + Languages.getString("jsp.admin.error2",SessionData.getLanguage()) + "</font>");
                      }
                      else if (strMessage.equals("3"))
                      {
                        //jsp.admin.customers.reps_info.error3=Please enter a value greater than or equal to 0.
                        out.println("<br><font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error3",SessionData.getLanguage()) + "</font>");
                      }
                      else if (strMessage.equals("4"))
                      {
                        //jsp.admin.customers.reps_info.error4=The credit limit must be greater than the assigned credit.
                        out.println("<br><font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error4",SessionData.getLanguage()) + "</font>");
                      }
                      else if (strMessage.equals("5"))
                      {
                        //jsp.admin.customers.reps_info.error5=A credit/prepaid rep can not have unlimited credit merchants.<br>Please change merchants to limited before continuing.
                        out.println("<br><font color=ff0000>" + Languages.getString("jsp.admin.customers.agents_info.error5",SessionData.getLanguage()) + "</font>");
                      }
%>
                        </td>
                      </tr>
                      <tr>
                        <td class="formArea2" valign=top>
                          <table width="100%">
<%
                      String merchantTypePrepaid   = "";
                      String merchantTypeCredit    = "";
                      String merchantTypeUnlimited = "";

                      if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID))
                      {
                        merchantTypePrepaid = " selected";
                      }
                      else
                      if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT))
                      {
                        merchantTypeCredit = " selected";
                      }
                      else
                      if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
                      {
                        merchantTypeUnlimited = " selected";
                      }
                      else
                      {
                        merchantTypeUnlimited = " selected";
                      }
%>

<%
	// DBSY-804
// Get the credit type here and determine if Flex
String isoid = SessionData.getProperty("ref_id");
// this is perhaps the only true case in which the ref_id
// can also be the iso_id. The other case is reps, but under 3L chains.
String isoCredType = Rep.getISOCreditType(isoid);
List<Boolean> otherOptions = CreditTypes.getFormOptionsReps(
	customConfigType, deploymentType, Rep.getRepCreditType(),
	isoCredType, new String[]{merchantTypePrepaid, merchantTypeCredit, merchantTypeUnlimited}, DebisysConstants.REP_TYPE_AGENT);

boolean showApplyPaymentOption = otherOptions.get(0);
if ( Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT) && isDomestic){
    showApplyPaymentOption = false;
}
boolean showPaymentOptions = otherOptions.get(1);
boolean showUpdateCreditLimitOption = otherOptions.get(2);
boolean disableUpdatePaymentButton = otherOptions.get(3);
boolean disableCreditTypeCombo = otherOptions.get(4);

if (DebisysConfigListener.getDebugJS(application)) {
	String vars = "\"repType=" + Rep.getRepCreditType() + " - isoCredType=" + isoCredType + "\"";
	out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
	vars = "\"showApplyPaymentOption=" + showApplyPaymentOption + " - showUpdateCreditLimitOption=" + showUpdateCreditLimitOption + " - disableUpdatePaymentButton=" + disableUpdatePaymentButton + " - disableCreditTypeCombo=" + disableCreditTypeCombo + " - showPaymentOptions=" + showPaymentOptions + "\"";
	out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
}

		if (showUpdateCreditLimitOption)
		{
%>
													<tr class=main>
													  <td nowrap><%=Languages.getString("jsp.admin.customers.reps_add.credit_limit",SessionData.getLanguage())%></td>
													  <td nowrap>
													  <form name="creditLimit" method="post" action="admin/customers/agents_update_credit_limit.jsp" onSubmit="return checkCreditLimit();">
															    <input type="hidden" name="repId" value="<%= Rep.getRepId() %>">
															     <input type="text" name="creditLimit" value="<%= NumberUtil.formatAmount(Rep.getCreditLimit()) %>" size="8" maxlength="20">
															    <input id="btnUpdateCreditLimit" type="submit" name="submit" value="<%= Languages.getString("jsp.admin.customers.reps_info.update",SessionData.getLanguage()) %>">

															</form>
                            </td>
<%
	  }


  if (strMessage.equals("6"))
	  {
	    //enter a valid number
	    out.println("<font color=ff0000>" + Languages.getString("jsp.admin.error2",SessionData.getLanguage()) + "</font><br>");
	  }
	  else if (strMessage.equals("7"))
	  {
	    //greater than or equal to 0
	    out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error3",SessionData.getLanguage()) + "</font><br>");
	  }
	  else if (strMessage.equals("8"))
	  {
	    //greater than or equal to 0
	    out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error8",SessionData.getLanguage()) + "</font><br>");
	  }
	  else if (strMessage.equals("9"))
	  {
	    //running liability less than 0
	    out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error9",SessionData.getLanguage()) + "</font><br>");
	  }

%>
                      	</td>
                    </tr>
                   	<tr class=main>
                     	<td nowrap><%=Languages.getString("jsp.admin.customers.reps_info.credit_avail",SessionData.getLanguage())%>:</td>
                      	<td>
<%
	if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
	{
	  out.print(Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage()));
	}
	else
	{
	  out.print(NumberUtil.formatCurrency(Rep.getAvailableCredit()));
	}
%>
                      	</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class=formAreaTitle2><br><%=Languages.getString("jsp.admin.customers.reps_info.credit_type",SessionData.getLanguage())%></td></tr>
		<tr><td class=formArea2 valign=top><table width='100%'>
		<tr class=main>
  			<td nowrap width=110><%=Languages.getString("jsp.admin.customers.reps_add.rep_type",SessionData.getLanguage())%>
                        </td>
                        <td>
<%
                        String s_RepCreditType =  Rep.getRepCreditType() == null || Rep.getRepCreditType().equals("") ? DebisysConstants.REP_CREDIT_TYPE_CREDIT : Rep.getRepCreditType();
                        
                        String labelCreditType = ((s_RepCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) 
                                            ? Languages.getString("jsp.admin.customers.reps_add.credit_type1", SessionData.getLanguage())
                                            : ((s_RepCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) 
                                                ? Languages.getString("jsp.admin.customers.reps_add.credit_type2", SessionData.getLanguage())
                                                : Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage()) ));

                        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
                            
                            Hashtable<String, String> creditTypeComboOptions =
                                                    CreditTypes.getAvailableOptionsForAgent(isoCredType, customConfigType, deploymentType,
                                                            new String[]{merchantTypePrepaid,merchantTypeCredit,merchantTypeUnlimited});

                            if (DebisysConfigListener.getDebugJS(application)) {
                                String vars = "\"";
                                for (String opt: creditTypeComboOptions.keySet()) {
                                    vars += opt + "=" + creditTypeComboOptions.get(opt) + " ";
                                }
                                
                                vars += "\"";
                                out.print("<Script type=\"text/javascript\">alert("+vars+")</script>");
                            }
                            String disabledText = "";
                            if (disableCreditTypeCombo) {
                                disabledText = "disabled=\"true\"";
                            }
%>
                            <form method="post" action="admin/customers/agents_make_payment.jsp" onsubmit="DisableButtons();">
                                <input type="hidden" name="sRepMerchantPayment" value="">
                                <input type="hidden" name="repId" value="<%=Rep.getRepId()%>">
                                <input type="hidden" name="oldRepCreditType" value="<%=s_RepCreditType%>">
                                <input type="hidden" name="paymentAmount" value="0.00">
                                <input type="hidden" name="commission" value="0">
                                <input type="hidden" name="netPaymentAmount" value="0.00">
                                <input type="hidden" name="paymentDescription" value="">

                                <select name="repCreditType" <%=disabledText%>>
<%
                                    for (String option: creditTypeComboOptions.keySet()) {
                                        String getString = "jsp.admin.customers.reps_add.credit_type" + option;
%>
                                        <option value=<%=option%> <%=creditTypeComboOptions.get(option)%>><%=Languages.getString(getString,SessionData.getLanguage())%></option>
<%
                                    }
%>
                                </select>
                                &nbsp;&nbsp;&nbsp;
                                <input id="btnChangeType" type="submit" <%=disabledText%> value="<%=Languages.getString("jsp.admin.customers.reps_info.changecredittype",SessionData.getLanguage())%>">
                            </form>

<%
                        } else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
%>                            
                            <%= labelCreditType %>
<%
                        }
%>   
  			</td>
                        
		</tr>
<%
		String sRepMerchantPayment = "";
		//if ( Rep.getSharedBalance().equals("1") && (Rep.getRepMerchants(SessionData, application).size() > 0) )
		//{
			//sRepMerchantPayment = "return checkRepMerchantPayment();";
		//}
		//else
		//{
			sRepMerchantPayment = "return checkPayment();";
		//}
%>
</table></td></tr>

<% 
if (SessionData.checkPermission(DebisysConstants.PERM_CREDIT_LIMITS))
{
  if(showApplyPaymentOption)
  {
%>
		<tr><td class=formAreaTitle2><br><%=Languages.getString("jsp.admin.customers.reps_info.apply_payment",SessionData.getLanguage())%></td></tr>
		<tr>
			<td class=formArea2 valign=top>
				<form name="payment" method="post" action="admin/customers/agents_make_payment.jsp" onSubmit="<%=sRepMerchantPayment%>">
				  <input id='txtRepMerchantPayment' type="hidden" name="sRepMerchantPayment" value="">
				  <input type="hidden" name="repId" value="<%=Rep.getRepId()%>">
				  <%s_RepCreditType =  Rep.getRepCreditType() == null || Rep.getRepCreditType().equals("")? DebisysConstants.REP_CREDIT_TYPE_CREDIT : Rep.getRepCreditType(); %><input type="hidden" name="oldRepCreditType" value="<%=s_RepCreditType%>">
				  <input type=hidden name="repCreditType" value="<%=Rep.getRepCreditType()%>">
						<table width='100%'>
							<tr class="main">
							 <td nowrap colspan=2>
								 <table cellpadding=0 cellspacing=0>
								  <tr class=main>
								    <td align=center nowrap>
								    	<b><%=Languages.getString("jsp.admin.customers.reps_info.gross_payment",SessionData.getLanguage())%></b>
								<%
									if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
									{
										String sScript = "";
										String sBlurScript = "this.value=formatAmount(this.value);";
										if ( Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) )
										{
											sScript = "ValidatePaymentValueMX(this, true);calculate(this);";
											sBlurScript += "ValidatePrepaidLimitMX(this);";
										}
										else if ( Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT) )
										{
											sScript = "ValidatePaymentValueMX(this, true);calculate(this);";
											sBlurScript += "ValidatePrepaidLimitMX(this);ValidateCreditLimitMX(this);";
										}
										else
										{
											sScript = "ValidatePaymentValueMX(this, false);calculate(this);";
											sBlurScript += "ValidateUnlimitedMX(this);";
										}
								%>
								    	<input type=text name="paymentAmount" value="0.00" size=7 onpropertychange="<%=sScript%>" onblur="<%=sBlurScript%>">
								<%
									}
									else
									{
								%>
								    	<input type=text name="paymentAmount" value="0.00" size=7 onKeyUp="calculate(this);" onBlur="return validate(this);">
								<%
									}
								%>
								    </td>
								    <td align=center nowrap>&nbsp;<b>-</b>&nbsp;</td>
								    <td align=center nowrap><b><%=Languages.getString("jsp.admin.customers.reps_info.commission",SessionData.getLanguage())%></b><input type=text name="commission" value="0" size=3 onKeyUp="calculate(this);" onBlur="return validate(this);"><b>%</b></td>
								    <td align=center nowrap>&nbsp;<b>=</b>&nbsp;</td>
								    <td align=center nowrap><input type=text name="netPaymentAmount" value="0.00" size=7 readonly style="color:#0000FF;background:#C0C0C0;"><b><%=Languages.getString("jsp.admin.customers.reps_info.net_payment",SessionData.getLanguage())%></b></td>
								    </tr>
								 </table>
							 </td>
							 </tr>
							 <tr class=main>
							    <td colspan=2><b><%=Languages.getString("jsp.admin.customers.reps_info.description",SessionData.getLanguage())%>:</b><input type=text name="paymentDescription" value="" size=20>(<%=Languages.getString("jsp.admin.optional",SessionData.getLanguage())%>)</td>
							 </tr>
							 <tr class=main>
							    <td colspan=2 nowrap>
							<%
								if (disableUpdatePaymentButton) {
							%>
							       <input id="btnApplyPayment" disabled="true" type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.reps_info.update",SessionData.getLanguage())%>">&nbsp;
							<%
								} else {
							%>
							       <input id="btnApplyPayment" type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.reps_info.update",SessionData.getLanguage())%>">&nbsp;
							<%
								}
							%>
							       <input type="button" name="view_history" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_history",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/agents_credit_history.jsp?repId=<%=Rep.getRepId()%>','repHistory1','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
							       <input type="button" name="view_history2" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_credit_assignment_history",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/agents_subagent_credit_history.jsp?repId=<%=Rep.getRepId()%>','repHistory2','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
							                                                      <%int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays("agents_info", application); %>
							                                                * <%=Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage())%>
							                                                <%=" "+limitDays+" "%>
							                                                <%=Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage())%>
							    </td>
							 </tr>
						</table>
			</td>
		</tr>
<% }
} %>

	<%
	}//End of if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
	%>

															</td>
														</tr>
                  </table>
												</td>
											</tr>
											<tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.reports.churn.notifications",SessionData.getLanguage()) %></td></tr>
											<tr>
												<td class="formArea2" valign="top">
													<table width="100%">
														<tr class="main">
															<td colspan="2" nowrap="nowrap">
																<a href="admin/customers/churn_reporting_options.jsp?repId=<%=Rep.getRepId()%>"><%=Languages.getString("jsp.admin.reports.churn.reportingoptions",SessionData.getLanguage())%></a>&nbsp;
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
</form>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<%@ include file="/includes/footer.jsp" %>
