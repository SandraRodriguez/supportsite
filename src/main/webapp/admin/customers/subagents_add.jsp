<%@ page import="com.debisys.utils.*,
                 com.debisys.users.User,
                 com.debisys.customers.CreditTypes,
                 java.util.*,
                 javax.mail.Session,
                 javax.mail.internet.MimeMessage,
                 javax.mail.Message,
                 javax.mail.internet.InternetAddress,
                 javax.mail.Transport,
                 javax.mail.MessagingException,
                 com.debisys.utils.TimeZone,
                 com.debisys.tools.s2k" %>
<%
  int section      = 12;
  int section_page = 5;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
  <jsp:setProperty name="Rep" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  int    intTabIndex = 1;
  String strMessage  = request.getParameter("message");

  if (strMessage == null)
  {
    strMessage = "";
  }

  Hashtable repErrors    = null;
  //Hashtable hashRepRates = new Hashtable();
  boolean   subagentAdded     = false;

  if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").length() > 0) )
  {
    if ( (request.getParameter("mxContactAction").equals("ADD")) && (session.getAttribute("vecContacts") != null) )
    {
      Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
    }
    else if ( request.getParameter("mxContactAction").equals("SAVE") )
    {
      Vector vTemp = new Vector();
      vTemp.add(request.getParameter("contactTypeId"));
      vTemp.add(request.getParameter("contactName"));
      vTemp.add(request.getParameter("contactPhone"));
      vTemp.add(request.getParameter("contactFax"));
      vTemp.add(request.getParameter("contactEmail"));
      vTemp.add(request.getParameter("contactCellPhone"));
      vTemp.add(request.getParameter("contactDepartment"));
      if ( session.getAttribute("vecContacts") != null )
      {
        Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
      }
      if ( (request.getParameter("mxContactIndex") == null) || request.getParameter("mxContactIndex").equals("") )
      {
        Rep.getVecContacts().add(vTemp);
      }
      else
      {
        Rep.getVecContacts().setElementAt(vTemp, Integer.parseInt(request.getParameter("mxContactIndex")));
      }
      session.setAttribute("vecContacts", Rep.getVecContacts());
    }
    else if ( request.getParameter("mxContactAction").equals("EDIT") )
    {
      Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
      Vector vec = (Vector)Rep.getVecContacts().elementAt(Integer.parseInt(request.getParameter("mxContactIndex")));
      Rep.setContactTypeId(Integer.parseInt(vec.get(0).toString()));
      Rep.setContactName(vec.get(1).toString());
      Rep.setContactPhone(vec.get(2).toString());
      Rep.setContactFax(vec.get(3).toString());
      Rep.setContactEmail(vec.get(4).toString());
      Rep.setContactCellPhone(vec.get(5).toString());
      Rep.setContactDepartment(vec.get(6).toString());
    }
    else if ( request.getParameter("mxContactAction").equals("DELETE") )
    {
      Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
      Rep.getVecContacts().removeElementAt(Integer.parseInt(request.getParameter("mxContactIndex")));
      session.setAttribute("vecContacts", Rep.getVecContacts());
    }
    else if ( (request.getParameter("mxContactAction").equals("CANCEL")) && (session.getAttribute("vecContacts") != null) )
    {
      Rep.setVecContacts((Vector)session.getAttribute("vecContacts"));
    }
  }
  else if (request.getParameter("submitted") != null)
  {
    try
    {
      if (request.getParameter("submitted").equals("y"))
      {
          if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
          {//If the code reachs here is because there is at least one contact and the session var exists
            Vector vTmp = (Vector)session.getAttribute("vecContacts");
            Rep.setVecContacts(vTmp);
            vTmp = (Vector)vTmp.get(0);
            Rep.setContactName(vTmp.get(1).toString());
            Rep.setContactFirst(vTmp.get(1).toString());
            Rep.setContactMiddle("_");
            Rep.setContactLast(".");
            Rep.setContactPhone(vTmp.get(2).toString());
            Rep.setContactFax(vTmp.get(3).toString());
            Rep.setContactEmail(vTmp.get(4).toString());
            Rep.setContactCellPhone(vTmp.get(5).toString());
            Rep.setContactDepartment(vTmp.get(6).toString());
          }

    	  if (Rep.validateSubAgent(SessionData, application))
        {
          Rep.setDEntityAccountType(request.getParameter("disabledentityAccountType"));
          Rep.addSubAgent(SessionData, application);

          subagentAdded = true;
          if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
          {
            session.removeAttribute("vecContacts");
          }

          try
          {
            String strSendMessage = "Added by ";

            if (strAccessLevel.equals(DebisysConstants.ISO))
            {
              strSendMessage = strSendMessage + "Iso     ";
            }
            else
            if (strAccessLevel.equals(DebisysConstants.AGENT))
            {
              strSendMessage = strSendMessage + "Agent   ";
            }
            else
            if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
            {
              strSendMessage = strSendMessage + "SubAgent";
            }

            strSendMessage = strSendMessage + ":" + SessionData.getProperty("username") + " from " + SessionData.getProperty(
                    "company_name") + '\n';
            strSendMessage = strSendMessage +
                            "Business Name    :" + Rep.getBusinessName() + '\n' +
                            "Address Info" + "\n" +
                            "----------------------" + "\n" +
                            "Address  :" + Rep.getAddress() + "\n" +
                            "City     :" + Rep.getCity() + "\n" +
                            "State    :" + Rep.getState() + "\n" +
                            "Zip      :" + Rep.getZip() + "\n" +
                            "Country  :" + Rep.getCountry() + "\n\n" +
                            "Contact Info" + "\n" +
                            "----------------------" + "\n" +
                            "Contact  :" + Rep.getContactFirst() + " " + Rep.getContactMiddle() + " " + Rep.getContactLast()+"\n" +
                            "Phone    :" + Rep.getContactPhone() + "\n" +
                            "Fax      :" + Rep.getContactFax() + "\n" +
                            "Email    :" + Rep.getContactEmail() + "\n\n" +
                            "Banking Info" + "\n" +
                            "----------------------" + "\n" +
                            "Tax Id   :" + Rep.getTaxId() + "\n" +
                            "ABA      :" + Rep.getRoutingNumber() + "\n" +
                            "Account #:" + Rep.getAccountNumber() + "\n\n";

            String               strSendSubject = "New Agent Added:" + Rep.getBusinessName();
            java.util.Properties props;

            props = System.getProperties();

            props.put("mail.smtp.host", DebisysConfigListener.getMailHost(application));

            Session     s       = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(s);
            String strSetupMail = com.debisys.utils.ValidateEmail.getSetupMail(application);
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(strSetupMail));
            message.setFrom(new InternetAddress(strSetupMail));
            message.setSubject(strSendSubject);
            message.setText(strSendMessage);
            Transport.send(message);
          }
          catch (MessagingException e)
          {
            String s = e.toString();
          }
        }
        else
        {
          repErrors = Rep.getErrors();
        }
      }
    }
    catch (Exception e)
    {
    }
  }
  else
  {
    if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
    {
      session.removeAttribute("vecContacts");
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
  <script language="JavaScript" src="includes/rep_validations.js"></script>
<%
	//s2k loaded style and javascript for DBSY-931 System 2000 Tools
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If when deploying in International and user is an ISO level
%>
	<script type="text/javascript" src="includes/s2k/salesmannameid.js"></script>
<%}%>
  <script language="JavaScript">
var form = "";
var submitted = false;
var error = false;
var error_message = "";

function check_input(field_name, field_size, message) {
  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var field_value = form.elements[field_name].value;

    if (field_value == '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_form(form_name) {
  if (submitted == true) {
    alert("<%= Languages.getString("jsp.admin.customers.reps_add.jsmsg1",SessionData.getLanguage()) %>");
    return false;
  }

  error = false;
  form = form_name;
  error_message = "<%= Languages.getString("jsp.admin.customers.reps_add.jsmsg2",SessionData.getLanguage()) %>";

  check_input("businessName", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_business_name",SessionData.getLanguage()) %>");
  check_input("address", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_street_address",SessionData.getLanguage()) %>");

<%
  if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
  {
%>
  check_input("city", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_city",SessionData.getLanguage()) %>");
  check_input("state", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_state",SessionData.getLanguage()) %>");
  check_input("zip", 1, "<%= Languages.getString("jsp.admin.customers.reps_add.error_zip",SessionData.getLanguage()) %>");
  check_input("lstSubAgentCreditType", 1, "<%=Languages.getString("jsp.admin.customers.error.required.creditType",SessionData.getLanguage())%>");

  var isCorrectAba = verifyAba(9,'/^[0-9]{9}$/',"<%=Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage())%>");
  if(isCorrectAba == false){
	error = true;
	error_message = error_message + "* "+"<%=Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage())%>"+" \n";
  }

  var isCorrectAccount = verifyAccount(20,"<%=Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage())%>");
  if(isCorrectAccount == false){
	error = true;
	error_message = error_message + "* "+"<%=Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage())%>"+" \n";
  }
<%
  }
  else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
  {
%>
  check_input("country", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_country",SessionData.getLanguage())%>");
<%
  }
	//s2k javascript check values for DBSY-931 System 2000 Tools
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If deploying in International and user is an ISO
%>
   var name = form.salesmanid.options[form.salesmanname.selectedIndex].value;
   var id = form.salesmanid.options[form.salesmanid.selectedIndex].value;
    // skip check if both empty
   if( id.length >0  || name.length >0 ){
   		if( id == "" || id == null ){
   			  check_input("salesmanid", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_salesmanid",SessionData.getLanguage())%>");
   		}
   		else if( name == "" || name == null ){
   			  check_input("salesmanname", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_salesmanname",SessionData.getLanguage())%>");
   		}
   }
<%}

   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
   { //If when deploying in Mexico
%>
  if ( !document.getElementById('chkBillingAddress'.checked) ) {
    check_input("mailAddress", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_billing_address",SessionData.getLanguage())%>");
    check_input("mailCountry", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_billing_country",SessionData.getLanguage())%>");
  }
  check_input("contactData", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact_data",SessionData.getLanguage())%>");
  check_input("taxId", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_taxid",SessionData.getLanguage())%>");
<%
   } //End of if when deploying in Mexico
   else
   {
%>
  check_input("contactFirst", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_contact_first",SessionData.getLanguage())%>");
  check_input("contactLast", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_contact_last",SessionData.getLanguage())%>");
  check_input("contactPhone", 1, "<%=Languages.getString("jsp.admin.customers.reps_add.error_phone",SessionData.getLanguage())%>");
<%
   }

   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
   { //If when deploying in Mexico
%>
  check_input("creditLimit", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_creditlimit",SessionData.getLanguage())%>");
<%
   }

   // DBSY-1072 eAccounts Interface
   if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)){
%>
       check_input("entityAccountType", 1, "<%=Languages.getString("jsp.admin.customers.error_entityAccountType",SessionData.getLanguage())%>");
<%
   }
%>

  if (error == true) {
    alert(error_message);
    return false;
  } else {
    document.subAgent.submit.disabled = true;
    submitted = true;
    return true;
  }
}

function validate(c)
{
	if (isNaN(c.value))
	{
		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
		c.focus();
		return (false);

	}
	else
	{
		if (c.value < 0)
		{

	    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
	    	c.focus();
	    	return (false);
		}
		else
		{
			c.value = formatAmount(c.value);
		}
	}
}


function formatAmount(n)
{
		var s = "" + Math.round(n * 100) / 100
		var i = s.indexOf('.')
		if (i < 0) return s + ".00"
		var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3)
		if (i + 2 == s.length) t += "0"
		return t
}

function validateInteger(c) {
if ( c.value.length > 0 ) {
	if (isNaN(c.value)) {
		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
		c.focus();
            c.select();
		return (false);
    } else if (c.value < 0) {
    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
    	c.focus();
            c.select();
    	return (false);
}
}
}//End of function validateInteger

function validateIntegerGreaterThanX(c, x) {
if ( c.value.length > 0 ) {
	if (isNaN(c.value)) {
		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
		c.focus();
            c.select();
		return (false);
    } else if (c.value <= x) {
            var s = '<%=Languages.getString("jsp.admin.errorIntegerGreatherThanX",SessionData.getLanguage())%>' + x;
    	alert(s);
    	c.focus();
            c.select();
    	return (false);
}
}
}//End of function validateIntegerGreaterThanX

function ValidateTextAreaLength(c, x) {
if ( c.value.length > x ) {
  alert('<%=Languages.getString("jsp.admin.errorTextLongerThanX",SessionData.getLanguage())%>' + x);
  c.focus();
  c.select();
  return false;
}
}//End of function ValidatePhysAddress

function ValidateRegExp(c, e) {
var exp = new RegExp(e);
//exp.compile(e);
if ( !exp.test(c.value) && (c.value.length > 0) ) {
  alert('<%=Languages.getString("jsp.admin.errorExpressionInvalid",SessionData.getLanguage())%>');
  c.focus();
  c.select();
  return false;
}
return true;
}//End of function ValidateRegExp

  function verifyAba(lengthText,expression, textInfo){

	var optionAbaCk = document.getElementById('optionAba').checked;
  	if(optionAbaCk == true){
  		return true;
  	}

  	var valueAba = document.getElementById('routingNumber').value;
  	//var isValid = verifyNumericText(valueAba,'/^[0-9]{'+lengthText+'}$/');
  	var isValid = verifyNumericText(valueAba,expression);
  	var isValidLength = false;
  	var isNoAllZero = false;
  	var isValidSQL = false;

  	if ( isValid == true && valueAba.length == 9 ) {
  		isValidLength = true;
  		isNoAllZero = verifyNoAllZero(valueAba);
  		if(isNoAllZero == true){
  			isValidSQL = validationSQL(valueAba);
  		}
  	}

  	if(isValid == true && isValidLength == true && isNoAllZero == true && isValidSQL == true){
  		var  div_info = document.getElementById("div_info_aba");
  		div_info.style.color="black";
  		div_info.innerHTML = "";
  		return true;
  	}
  	else{
  		var  div_info = document.getElementById("div_info_aba");
  		div_info.style.width="200px";
  		div_info.style.color="red";
		div_info.innerHTML = "";
		var newdiv=document.createElement("div");
		var newtext=document.createTextNode(textInfo);
		newdiv.appendChild(newtext); //append text to new div
		div_info.appendChild(newdiv);
		return false;
  	}
  }

  function verifyAccount(lengthText, textInfo){
  	var optionAbaCk = document.getElementById('optionAccount').checked;
  	if(optionAbaCk == true){
  		return true;
  	}
  	var valueAccount = document.getElementById('accountNumber').value;
  	var isValid = verifyNumericText(valueAccount,'/^[0-9]{1,'+lengthText+'}$/');
  	var isValidLength = false;
  	var isNoAllZero = false;
  	if ( isValid == true && valueAccount.length <= 20 ) {
  		isValidLength = true;
  		isNoAllZero = verifyNoAllZero(valueAccount);
  	}

  	if(isValid == true && isValidLength == true && isNoAllZero == true){
  		var  div_info = document.getElementById("div_info_account");
  		div_info.style.color="black";
  		div_info.innerHTML = "";
  		return true;
  	}
  	else{
  		var  div_info = document.getElementById("div_info_account");
  		div_info.style.width="200px";
  		div_info.style.color="red";
		div_info.innerHTML = "";
		var newdiv=document.createElement("div");
		var newtext=document.createTextNode(textInfo);
		newdiv.appendChild(newtext); //append text to new div
		div_info.appendChild(newdiv);
		return false;
  	}

  }

  // EXEC THE REGULAR EXPRESSIONS
  function verifyNumericText(text,exp){
    exp = exp.replace("/", "");
	exp = exp.replace("/", "");
	exp = exp.replace(/\\/g, "\\");
  	var patt1 = new RegExp(exp);
	var result = patt1.exec(text);
	if (result!= null){
		return true;
	}
	return false;
  }

  // VERIFY THAT AN TEXT NOT CONTAIN ALL ZEROS
  function verifyNoAllZero(text){
  	var i=0;
  	for(i = 0; i < text.length ; i++){
  		if(text.charAt(i) != '0'){
  			return true;
  		}
  	}
  	return false;
  }

  //VALIDATION FOR ABA MERCHANT WITH FORMAT OF THE DB
  function validationSQL(text){

 	var digit1 = parseInt(text.charAt(0))*3;
 	var digit2 = parseInt(text.charAt(1))*7;
 	var digit3 = parseInt(text.charAt(2))*1;
 	var digit4 = parseInt(text.charAt(3))*3;
 	var digit5 = parseInt(text.charAt(4))*7;
 	var digit6 = parseInt(text.charAt(5))*1;
 	var digit7 = parseInt(text.charAt(6))*3;
 	var digit8 = parseInt(text.charAt(7))*7;
 	var digit9 = parseInt(text.charAt(8));

  	var sumDigits = digit1+digit2+digit3+ digit4+ digit5+digit6+ digit7+digit8 ;
  	var mod10 = sumDigits % 10;
  	var totalOP = 10-mod10;
  	var resultDigit = getRightDigits(totalOP,1);

  	if(resultDigit == digit9){
  		return true;
  	}
  	else{
  		return false;
  	}
  }

  // GET AN NUMBER DIGITS THE ONE NUMBER
  function getRightDigits(num,digits){
  	var i=0;
  	var numString = num.toString();
  	var realNumStr = '';
  	for(i = (numString.length-digits); i < numString.length ; i++){
 		realNumStr = realNumStr+numString.charAt(i);
  	}
  	var realNum = parseInt(realNumStr);
  	return realNum;
  }

  function changeCheckAba(){
  	var optionAbaCk = document.getElementById('optionAba').checked;
  	if(optionAbaCk == true){
  		var div_info = document.getElementById("div_info_aba");
  		div_info.style.color="black";
  		div_info.innerHTML = "";

  		var valueAba = document.getElementById('routingNumber');
  		valueAba.value = 'NA';
  		valueAba.disabled=true;
  	}
  	else{
  		var valueAba = document.getElementById('routingNumber');
  		valueAba.value = '';
  		valueAba.disabled=false;
  	}
  }

  function changeCheckAccount(){
  	var optionAbaCk = document.getElementById('optionAccount').checked;
  	if(optionAbaCk == true){
  		var div_info = document.getElementById("div_info_account");
  		div_info.style.color="black";
  		div_info.innerHTML = "";

  		var valueAccount = document.getElementById('accountNumber');
  		valueAccount.value = 'NA';
  		valueAccount.disabled=true;
  	}
  	else{
  		var valueAccount = document.getElementById('accountNumber');
  		valueAccount.value = '';
  		valueAccount.disabled=false;
  	}
  }


function EnableBillingAddress(bChecked) {
document.getElementById('mailAddress').readOnly = bChecked;
document.getElementById('mailColony').readOnly = bChecked;
document.getElementById('mailDelegation').readOnly = bChecked;
document.getElementById('mailCity').readOnly = bChecked;
document.getElementById('mailZip').readOnly = bChecked;
document.getElementById('mailState').disabled = bChecked;
document.getElementById('mailCounty').readOnly = bChecked;
document.getElementById('mailCountry').disabled = bChecked;
if (bChecked) {
    document.getElementById('mailAddress').value = document.getElementById('address').value;
    document.getElementById('mailColony').value = document.getElementById('physColony').value;
    document.getElementById('mailDelegation').value = document.getElementById('physDelegation').value;
    document.getElementById('mailCity').value = document.getElementById('city').value;
    document.getElementById('mailZip').value = document.getElementById('zip').value;
    document.getElementById('mailState').value = document.getElementById('state').value;
    document.getElementById('mailCounty').value = document.getElementById('physCounty').value;
    document.getElementById('mailCountry').value = document.getElementById('country').value;
}
else {
    document.getElementById('mailAddress').value = '';
    document.getElementById('mailColony').value = '';
    document.getElementById('mailDelegation').value = '';
    document.getElementById('mailCity').value = '';
    document.getElementById('mailZip').value = '';
    document.getElementById('mailCounty').value = '';
    document.getElementById('mailCountry').value = '';
}
}//End of function EnableBillingAddress

//DTU-369 Payment Notifications
  function changePayNoti()
  {
  	var payNoti = document.getElementById('paymentNotifications').checked;
  	if(payNoti == true)
	{
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  		valuePayNotiSMSChk.checked=false;
  		valuePayNotiSMSChk.disabled=false;
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  		valuePayNotiMailChk.checked=false;
  		valuePayNotiMailChk.disabled=false;
  	}
  	else
	{
  		var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  		valuePayNotiSMSChk.checked=false;
  		valuePayNotiSMSChk.disabled=true;
  		var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  		valuePayNotiMailChk.checked=false;
  		valuePayNotiMailChk.disabled=true;
  	}
  }

  function changePayNotiSMS()
  {
  	var payNotiSMS = document.getElementById('paymentNotificationSMS').checked;
  	var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
  	if(payNotiSMS == true)
	{
  		valuePayNotiMailChk.checked=false;
  	}
  	else
	{
  		valuePayNotiMailChk.checked=true;
  	}
  }

  function changePayNotiMail()
  {
  	var payNotiMail = document.getElementById('paymentNotificationMail').checked;
  	var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
  	if(payNotiMail == true)
	{
  		valuePayNotiSMSChk.checked=false;
  	}
  	else
	{
  		valuePayNotiSMSChk.checked=true;
  	}
  }

//DTU-480: Low Balance Alert Message SMS and or email
				function changeBalNoti() {
		        var balNoti = document.getElementById('balanceNotifications').checked;
		        if (balNoti == true) {
		            var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		            valueBalNotiDaysChk.checked = false;
		            valueBalNotiDaysChk.disabled = false;
		            var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		            valueBalNotiValueChk.checked = false;
		            valueBalNotiValueChk.disabled = false;				
		            var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		            valueBalNotiSMSChk.checked = false;
		            valueBalNotiSMSChk.disabled = false;
		            var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		            valueBalNotiMailChk.checked = false;
		            valueBalNotiMailChk.disabled = false;
					var valueBalNotiValue = document.getElementById('balanceNotificationValue');
					var valueBalNotiDays = document.getElementById('balanceNotificationDays');
					valueBalNotiValue.disabled=true;
					valueBalNotiDays.disabled=true;		            
		        }
		        else {
		            var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
		            valueBalNotiDaysChk.checked = false;
		            valueBalNotiDaysChk.disabled = true;
		            var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
		            valueBalNotiValueChk.checked = false;
		            valueBalNotiValueChk.disabled = true;
		            var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
		            valueBalNotiSMSChk.checked = false;
		            valueBalNotiSMSChk.disabled = true;
		            var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
		            valueBalNotiMailChk.checked = false;
		            valueBalNotiMailChk.disabled = true;
					var valueBalNotiValue = document.getElementById('balanceNotificationValue');
					var valueBalNotiDays = document.getElementById('balanceNotificationDays');
					valueBalNotiValue.disabled=true;
					valueBalNotiDays.disabled=true;			            
		        }
		    }

              function changeBalNotiValue()
              {
  	            var balNotiValue = document.getElementById('balanceNotificationTypeValue').checked;
				var valueBalNotiValue = document.getElementById('balanceNotificationValue');
  	            if(balNotiValue == true)
	            {
					valueBalNotiValue.disabled=false;
  	            }
  	            else
	            {
					valueBalNotiValue.disabled=true;
  	            }
              }
  
              function changeBalNotiDays()
              {
  	            var balNotiDays = document.getElementById('balanceNotificationTypeDays').checked;
				var valueBalNotiDays = document.getElementById('balanceNotificationDays');					
  	            if(balNotiDays == true)
	            {
					valueBalNotiDays.disabled=false;						
				}
				else
				{
					valueBalNotiDays.disabled=true;						
				}
              }	
              
		    function changeBalNotiSMS() 
		    {
		        var balNotiSMS = document.getElementById('balanceNotificationSMS').checked;
		        var valueBalNotiMail = document.getElementById('balanceNotificationMail');
		        if (balNotiSMS == true) 
		        {
		            valueBalNotiMail.checked = false;
		        }
		        else 
		        {
		            valueBalNotiMail.checked = true;
		        }
		    }

		    function changeBalNotiMail() 
		    {
		        var balNotiMail = document.getElementById('balanceNotificationMail').checked;
        		var valueBalNotiSMS = document.getElementById('balanceNotificationSMS');		        
		        if (balNotiMail == true) 
		        {
		    
		            valueBalNotiSMS.checked = false;
		        }
		        else 
		        {
		            valueBalNotiSMS.checked = true;
		        }
		    } 
  </script>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" width="2000" class="formAreaTitle">
        &nbsp;
        <%= Languages.getString("jsp.admin.customers.subagents_add.title",SessionData.getLanguage()).toUpperCase() %>
      </td>
      <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#ffffff" class="formArea2">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
          <tr>
            <td class="main">
<%
              if (!subagentAdded)
              {
%>
                <br>
                <%= Languages.getString("jsp.admin.customers.subagents_add.add_instructions",SessionData.getLanguage()) %>
                <br>
                <br>
<%
              }
%>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" align="left">
                <tr>
                  <td>
<%
                    if (!subagentAdded)
                    {
%>
                      <form name="subAgent" method="post" action="admin/customers/subagents_add.jsp" onSubmit="return check_form(subAgent);">
                      <table width="100%">
<%
                          if (repErrors != null)
                          {
                            out.println("<tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                                    "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

                            Enumeration enum1 = repErrors.keys();

                            while (enum1.hasMoreElements())
                            {
                              String strKey   = enum1.nextElement().toString();
                              String strError = (String)repErrors.get(strKey);

                              if (strError != null && !strError.equals(""))
                              {
                                out.println("<li>" + strError);
                              }
                            }

                            out.println("</font></td></tr>");
                          }
%>
                          <tr>
                            <td class="formAreaTitle2">
                              <%= Languages.getString("jsp.admin.customers.reps_add.company_information",SessionData.getLanguage()) %>
                            </td>
                          </tr>
                          <tr>
                            <td class="formArea2">
                              <table>
<%
                                if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(
                                        DebisysConstants.DIST_CHAIN_5_LEVEL)))
                                {
%>
                                  <tr>
                                    <td class="main">
                                      <b><%= Languages.getString("jsp.admin.customers.subagents_add.agent",SessionData.getLanguage()) %>:</b>
                                    </td>
                                    <td nowrap class="main">
                                      <input type="text" name="parentRepName" value="<%= Rep.getRepName(Rep.getParentRepId()) %>" size="30" readonly>
<%
                                      if (repErrors != null && repErrors.containsKey("parentRepId"))
                                      {
                                        out.print("<font color=ff0000>*</font>");
                                      }
%>
                                      <input type="button" name="repSelect" value="<%= Languages.getString("jsp.admin.customers.subagents_add.select_agent",SessionData.getLanguage()) %>" onClick="window.open('/support/admin/customers/agent_selector.jsp?search=y','repSelect','width=650,height=500,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
                                      <input type="hidden" name="parentRepId" value="<%= Rep.getParentRepId() %>"\>
                                    </td>
                                  </tr>
<%
                                }
                                else if (strAccessLevel.equals(DebisysConstants.AGENT))
                                {
%>
                                  <tr>
                                    <td class="main">
                                      <b><%= Languages.getString("jsp.admin.customers.subagents_add.agent",SessionData.getLanguage()) %>:</b>
                                    </td>
                                    <td nowrap class="main">
                                      <input type="text" name="repName" value="<%= SessionData.getProperty("company_name") %>" size="30" maxlength="200" readonly>
                                      <input type="hidden" name="parentRepId" value="<%= SessionData.getProperty("ref_id") %>">
                                    </td>
                                  </tr>
<%
                                }

%>

<% if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
   {//If when deploying in Mexico
%>
                                <tr>
                                  <td class="main">
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.legal_businessname",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap class="main">
                                    <input type="text" name="legalBusinessname" value="<%=Rep.getLegalBusinessname()%>" size="20" maxlength="50" tabIndex="<%= intTabIndex++ %>">
                                  </td>
                                </tr>
<%}%>
                                <tr>
                                  <td class="main">
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.business_name",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap class="main">
                                    <input type="text" name="businessName" value="<%= Rep.getBusinessName() %>" size="20" maxlength="50" tabIndex="<%= intTabIndex++ %>">
                                    <%
                                    if (repErrors != null && repErrors.containsKey("businessName"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }%>
                                  </td>
                                </tr>

<%
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                <TR>
                                                <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.businesstype",SessionData.getLanguage())%>:</B></TD>
                                                <TD NOWRAP CLASS="main">
                                                    <SELECT NAME="businessTypeId" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecBusinessTypes = com.debisys.customers.Merchant.getBusinessTypes(customConfigType);
  Iterator itBusinessTypes = vecBusinessTypes.iterator();
  while (itBusinessTypes.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itBusinessTypes.next();
    String strBusinessTypeId = vecTemp.get(0).toString();
    String strBusinessTypeCode = vecTemp.get(1).toString();
    if (strBusinessTypeId.equals(request.getParameter("businessTypeId")))
      out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\" SELECTED>" + strBusinessTypeCode + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strBusinessTypeId + "\">" + strBusinessTypeCode + "</OPTION>");
  }
%>
                                                    </SELECT>
                                                </TD>
                                                </TR>
                                                <TR>
                                                <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment",SessionData.getLanguage())%>:</B></TD>
                                                <TD NOWRAP CLASS="main">
                                                    <SELECT NAME="merchantSegmentId" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecMerchantSegments = com.debisys.customers.Merchant.getMerchantSegments();
  Iterator itMerchantSegments = vecMerchantSegments.iterator();
  while (itMerchantSegments.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itMerchantSegments.next();
    String strMerchantSegmentId = vecTemp.get(0).toString();
    String strMerchantSegmentCode = vecTemp.get(1).toString();
    if (strMerchantSegmentId.equals(request.getParameter("merchantSegmentId")))
      out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strMerchantSegmentId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode,SessionData.getLanguage()) + "</OPTION>");
  }
%>
                                                    </SELECT>
                                                </TD>
                                                </TR>
                                                <TR>
                                                    <TD CLASS="main"><B><%=Languages.getString("jsp.admin.customers.merchants_edit.businesshours",SessionData.getLanguage())%>:</B></TD>
                                                    <TD NOWRAP CLASS="main">
                                                        <INPUT  TYPE="text" NAME="businessHours" VALUE="<%=Rep.getBusinessHours()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>">
                                                    </TD>
                                                </TR>
<%
   } //End of if when deploying in Mexico
%>
								<TR>
									<TD CLASS="main">
										<B><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZone",SessionData.getLanguage())%>:</B>
									</TD>
									<TD NOWRAP CLASS="main">
										<SELECT NAME="timeZoneId">
<%
										String sDefaultTimeZone = Rep.getTimeZoneId() == 0?null:Integer.toString(Rep.getTimeZoneId());
										if ( sDefaultTimeZone == null ) { sDefaultTimeZone = TimeZone.getDefaultTimeZone().get(0).toString(); }
										Iterator<Vector<String>> itList = TimeZone.getTimeZoneList().iterator() ;
										while ( itList.hasNext() )
										{
											Vector vItem = itList.next();
											String sSelected = "";
											if (vItem.get(0).toString().equals(sDefaultTimeZone))
											{
												sSelected = "SELECTED";
											}
%>
											<OPTION VALUE="<%=vItem.get(0).toString()%>" <%=sSelected%>><%=vItem.get(2).toString()%> [<%=vItem.get(1).toString()%>]</OPTION>
<%
											vItem.clear();
											vItem = null;
										}
										itList = null;
%>
										</SELECT>
									</TD>
								</TR>
								<TR>
									<TD CLASS="main">&nbsp;</TD>
									<TD CLASS="main">
										<FONT COLOR="red"><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZoneWarning",SessionData.getLanguage())%></FONT>
									</TD>
								</TR>
		<%
	// DBSY-931 System 2000 Tools s2k
   if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
   &&  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
   && strAccessLevel.equals(DebisysConstants.ISO)
   && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)
   ) { //If when deploying in International and user is an ISO level
		String strRefId = SessionData.getProperty("ref_id");
%>
<tr>
<TD CLASS="main">
<B><%= Languages.getString("jsp.admin.tools.s2k.addedit.terms",SessionData.getLanguage())%>:</B>
</TD>
 <TD NOWRAP CLASS="main">
 <INPUT  TYPE="text" NAME="terms" VALUE="<%=Rep.getterms()%>" SIZE="6" MAXLENGTH="4">
</TD>
</tr>
<tr>
<TD CLASS="main">
<B><%= Languages.getString("jsp.admin.customers.salesmanid",SessionData.getLanguage())%>:</B>
									</TD>
									<TD NOWRAP CLASS="main">

<%
					Vector salesman = new s2k().gets2ksalesman(SessionData.getProperty("iso_id"));
%>
	<select name="salesmanid" id ="salesmanid" onchange="loadid('<%=strRefId%>');" tabIndex="<%= intTabIndex++ %>" >
                             <%
                                out.println("<OPTION VALUE=\"\" >" + "</OPTION>");
                               for ( int v=0 ; v<salesman.size();v++){
                                String temp = ((Vector)salesman.get(v)).get(0).toString();
                                String selected="";
                                   if ( temp.equals(request.getParameter("salesmanid")) )
                                   	selected ="SELECTED";

								out.println("<OPTION VALUE=\"" + temp
								+ "\" "+ selected +" >"
								 +  temp  + "</OPTION>");
								 }
								  %>
                              </select>

<B>     <%= Languages.getString("jsp.admin.customers.salesmanname",SessionData.getLanguage())%>:</B>
     <select name="salesmanname"  id="salesmanname"  onchange="loadname('<%=strRefId%>');" tabIndex="<%= intTabIndex++ %>" >
                             <%
                          out.println("<OPTION VALUE=\"\" >" + "</OPTION>");
                                for ( int v=0 ; v<salesman.size();v++){
                                String temp = ((Vector)salesman.get(v)).get(1).toString();
                                 String selected="";
                                   if ( temp.equals(request.getParameter("salesmanname")) )
                                   	selected ="SELECTED";

								out.println("<OPTION VALUE=\"" + temp
								+ "\" "+ selected +" >"
								 +  temp  + "</OPTION>");
								 }
 %>
                              </select>
                                  </td>
                              </tr>
          <%}

	// DBSY-1072 eAccounts Interface
	if(SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
%>
												<tr>
													<td class="main">
														<b><%= Languages.getString("jsp.admin.customers.entity_account_type",SessionData.getLanguage())%>:</b>
													</td>
<%
			Hashtable<Integer, String> ht_accountTypes = EntityAccountTypes.getAccountTypes();
%>
													<td>
													<input type="hidden" id="disabledentityAccountType" name="disabledentityAccountType" value="" />
														<select id="entityAccountType" name="entityAccountType">
<%
			for (Enumeration<Integer> e = ht_accountTypes.keys(); e.hasMoreElements();) {
				int type_id = e.nextElement();
				out.println("<option value=\"" + type_id + "\" " + (type_id == 1?"selected":"") + ">" + ht_accountTypes.get(type_id) + "</option>");
			}
%>
														</select>
													</td>
												</tr>
<%
	}
%>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td class="formAreaTitle2">
                              <br>
                              <%= Languages.getString("jsp.admin.customers.reps_add.address",SessionData.getLanguage()) %>
                            </td>
                          </tr>
                          <tr>
                            <td class="formArea2">
                              <table width="100">
                                <tr class="main">
                                  <td>
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.street_address",SessionData.getLanguage()) %>:</b>
                                  </td>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
               						<TD COLSPAN="4" NOWRAP><TEXTAREA NAME="address" id="address" ROWS="2" COLS="50" TABINDEX="<%=intTabIndex++%>" ONPROPERTYCHANGE="if (this.value.length > 50) { this.value = this.value.substring(0, 50); }" ONBLUR="ValidateTextAreaLength(this, 50);"><%=Rep.getAddress()%></TEXTAREA><%if (repErrors != null && repErrors.containsKey("address")) out.print("<font color=ff0000>*</font>");%></TD>
<%
  } else { //Else if when deploying in others
%>
                                  <td nowrap>
                                    <input type="text" name="address" id="address" value="<%= Rep.getAddress() %>" size="20" maxlength="50" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("address"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
<%
  }
%>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                    </TR>
                                                    <TR CLASS="main">
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage())%>:</TD>
               						<TD NOWRAP><INPUT TYPE="text" NAME="physColony" id="physColony" VALUE="<%=Rep.getPhysColony()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage())%>:</TD>
               						<TD NOWRAP><INPUT TYPE="text" NAME="physDelegation" id="physDelegation" VALUE="<%=Rep.getPhysDelegation()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
<%
  } //End of if when deploying in Mexico
%>
                                </tr>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                    <TR CLASS="main">
               						<TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())%>:</TD>
                                                        <TD NOWRAP><INPUT TYPE="text" NAME="city" id="city" VALUE="<%=Rep.getCity()%>" SIZE="20" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("city")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage())%></TD>
                                                        <TD NOWRAP><INPUT TYPE="text" NAME="zip" id="zip" VALUE="<%=Rep.getZip()%>" SIZE="20" MAXLENGTH="5" TABINDEX="<%=intTabIndex++%>" ONBLUR="ValidateRegExp(this, '[0-9]{5}');"><%if (repErrors != null && repErrors.containsKey("zip")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><B><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())%></B></TD>
                                                        <TD NOWRAP>
                                                            <SELECT NAME="state" id="state" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
  Iterator itStates = vecStates.iterator();
  while (itStates.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itStates.next();
    String strStateId = vecTemp.get(0).toString();
    String strStateName = vecTemp.get(1).toString();
    if ( strStateId.equals(request.getParameter("state")) )
      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
  }
%>
                                                            </SELECT>
                                                        </TD>
                                                    </TR>
              						<tr class="main">
               						<td><%=Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage())%>:</td>
               						<td nowrap><input type="text" name="physCounty" id="physCounty" value="<%=Rep.getPhysCounty()%>" size="20" maxlength="20" tabIndex="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("physCounty")) out.print("<font color=ff0000>*</font>");%></td>
              						</tr>
<%
  } //End of if when deploying in Mexico
  else { //Else other deployments
%>
                                <tr class="main">
                                  <td nowrap>
<%
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                                    {
                                      out.println("<b>" + Languages.getString("jsp.admin.customers.reps_add.city",SessionData.getLanguage()) + ":</b>");
                                    }
                                    else
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                    {
                                      out.println(Languages.getString("jsp.admin.customers.reps_add.city",SessionData.getLanguage()) + ":");
                                    }
%>
                                  </td>
                                  <td nowrap>
                                    <input type="text" name="city" id ="city" value="<%= Rep.getCity() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("city"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }

                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                                    {
                                      out.println("<b>" + Languages.getString("jsp.admin.customers.reps_add.state_domestic",SessionData.getLanguage())
                                              + ":</b>");
                                    }
                                    else
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                    {
                                      out.println(Languages.getString("jsp.admin.customers.reps_add.state_international",SessionData.getLanguage()) +
                                              ":");
                                    }
%>
                                    <input type="text" name="state" id="state" value="<%= Rep.getState() %>" size="3" maxlength="2" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("state"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
<%
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                                    {
                                      out.println("<b>" + Languages.getString("jsp.admin.customers.reps_add.zip_domestic",SessionData.getLanguage()) +
                                              ":</b>");
                                    }
                                    else
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                    {
                                      out.println(Languages.getString("jsp.admin.customers.reps_add.zip_international",SessionData.getLanguage()) + ":"
                                              );
                                    }
%>
                                    <input type="text" name="zip" id="zip" value="<%= Rep.getZip() %>" size="7" maxlength="10" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("zip"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
<%
  } //End of else other deployments
%>
                                <tr class="main">
                                  <td>
<%
                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                                    {
                                      out.println("<b>");
                                    }
%>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.country",SessionData.getLanguage()) %>:
                                  </td>
                                  <td nowrap>

<%
					 String[][] countries = CountryInfo.getAllCountries(CountryInfo.getIso(SessionData.getProperty("ref_id"),SessionData.getProperty("ref_type")));
					if(countries[0][0].length() > 0 ){
					%>

	<select name="country" id="country" tabIndex="<%= intTabIndex++ %>" >
                             <%
                             if(countries.length == 1){
                                out.println("<OPTION VALUE=\"" + countries[0][0].toString() + "\" SELECTED>" + countries[0][1].toString()  + "</OPTION>");
                             }
                             else{
                              out.println("<OPTION VALUE=\"\" SELECTED>" + "" + "</OPTION>");
                               for ( int v=0 ; v<countries.length;v++)
								out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\">" + countries[v][1].toString()  + "</OPTION>");
                             }
 %>
                              </select>
<%
                                   }
                                   else
                                   {
                                   		out.print("<font color=ff0000>Database Error Country must be set for ISO</font>");
                                   }
                                    if (repErrors != null && repErrors.containsKey("country"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
      					<TR>
      						<TD CLASS="formAreaTitle2">
                                                    <BR>
                                                    <%=Languages.getString("jsp.admin.customers.merchants_edit.billingaddress",SessionData.getLanguage())%>&nbsp;
                                                    <BR>
                                                    <INPUT TYPE="checkbox" ID="chkBillingAddress" NAME="chkBillingAddress" ONCLICK="EnableBillingAddress(this.checked);" <%=((request.getParameter("chkBillingAddress") != null)?"CHECKED":"")%>><LABEL FOR="chkBillingAddress"><%=Languages.getString("jsp.admin.customers.merchants_edit.billingaddresstitle",SessionData.getLanguage())%></LABEL>
      						</TD>
      					</TR>
     					<tr>
                                            <td class="formArea2">
                                                <table width="100">
                                                    <tr class="main">
                                                        <td><b><%=Languages.getString("jsp.admin.customers.merchants_edit.street_address",SessionData.getLanguage())%>:</b></td>
               						<TD COLSPAN="4" NOWRAP><TEXTAREA ID="mailAddress" NAME="mailAddress" ROWS="2" COLS="50" TABINDEX="<%=intTabIndex++%>" ONPROPERTYCHANGE="if (this.value.length > 50) { this.value = this.value.substring(0, 50); }" ONBLUR="ValidateTextAreaLength(this, 50);"><%=Rep.getMailAddress()%></TEXTAREA><%if (repErrors != null && repErrors.containsKey("billingAddress")) out.print("<font color=ff0000>*</font>");%></TD>
                                                    </TR>
                                                    <TR CLASS="main">
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_colony",SessionData.getLanguage())%>:</TD>
               						<TD NOWRAP><INPUT TYPE="text" ID="mailColony" NAME="mailColony" VALUE="<%=Rep.getMailColony()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD><%=Languages.getString("jsp.admin.customers.merchants_edit.address_delegation",SessionData.getLanguage())%>:</TD>
               						<TD NOWRAP><INPUT TYPE="text" ID="mailDelegation" NAME="mailDelegation" VALUE="<%=Rep.getMailDelegation()%>" SIZE="20" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                    </TR>
                                                    <TR CLASS="main">
               						<TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.city",SessionData.getLanguage())%>:</TD>
                                                        <TD NOWRAP><INPUT TYPE="text" ID="mailCity" NAME="mailCity" VALUE="<%=Rep.getMailCity()%>" SIZE="20" MAXLENGTH="30" TABINDEX="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("billingCity")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.zip_international",SessionData.getLanguage())%>:</TD>
                                                        <TD NOWRAP><INPUT TYPE="text" ID="mailZip" NAME="mailZip" VALUE="<%=Rep.getMailZip()%>" SIZE="20" MAXLENGTH="5" TABINDEX="<%=intTabIndex++%>" ONBLUR="ValidateRegExp(this, '[0-9]{5}');"><%if (repErrors != null && repErrors.containsKey("billingZip")) out.print("<font color=ff0000>*</font>");%></TD>
                                                        <TD>&nbsp;</TD>
                                                        <TD NOWRAP><B><%=Languages.getString("jsp.admin.customers.merchants_edit.state_domestic",SessionData.getLanguage())%></B></TD>
                                                        <TD NOWRAP>
                                                            <SELECT ID="mailState" NAME="mailState" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
  Iterator itStates = vecStates.iterator();
  while (itStates.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itStates.next();
    String strStateId = vecTemp.get(0).toString();
    String strStateName = vecTemp.get(1).toString();
    if (strStateId.equals(request.getParameter("mailState")))
      out.println("<OPTION VALUE=\"" + strStateId + "\" SELECTED>" + strStateName + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strStateId + "\">" + strStateName + "</OPTION>");
  }
%>
                                                            </SELECT>
                                                        </TD>
                                                    </TR>
              						<tr class="main">
               						<td><%=Languages.getString("jsp.admin.customers.merchants_edit.county",SessionData.getLanguage())%>:</td>
               						<td nowrap><input type="text" id="mailCounty" name="mailCounty" value="<%=Rep.getMailCounty()%>" size="20" maxlength="20" tabIndex="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("billingCounty")) out.print("<font color=ff0000>*</font>");%></td>
              						</tr>
              						<tr class="main">
               						<td><B><%=Languages.getString("jsp.admin.customers.merchants_edit.country",SessionData.getLanguage())%>:</B></td>
               						<td nowrap>

<%
					if(countries[0][0].length() > 0 ){
					%>


	<select id="mailCountry" name="mailCountry" tabIndex="<%= intTabIndex++ %>" >
                             <%
                             if(countries.length == 1){
                                out.println("<OPTION VALUE=\"" + countries[0][0].toString() + "\" SELECTED>" + countries[0][1].toString()  + "</OPTION>");
                             }
                             else{
                              out.println("<OPTION VALUE=\"\" SELECTED>" + "" + "</OPTION>");
                               for ( int v=0 ; v<countries.length;v++)
								out.println("<OPTION VALUE=\"" + countries[v][0].toString() + "\">" + countries[v][1].toString()  + "</OPTION>");
                             }
 %>
                              </select>
<%
                                   }
                                   else
                                   {
                                   		out.print("<font color=ff0000>Database Error Country must be set for ISO</font>");
                                   }
                                    if (repErrors != null && repErrors.containsKey("billingCountry"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
              						</tr>
            					</table>
					          </td>
					      </tr>
<%
   if ( request.getParameter("chkBillingAddress") != null )
       out.print("<SCRIPT>EnableBillingAddress(true);</SCRIPT>");
   else
       out.print("<SCRIPT>EnableBillingAddress(false);</SCRIPT>");
  } //End of if when deploying in Mexico
%>
                          <tr>
                            <td class="formAreaTitle2">
                              <br>
                              <%= Languages.getString("jsp.admin.customers.reps_add.contact_information",SessionData.getLanguage()) %>
                            </td>
                          </tr>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  {//If when deploying in Mexico
%>
                                        <TR>
                                            <TD CLASS="formArea2">
                                                <INPUT TYPE="hidden" ID="mxContactAction" NAME="mxContactAction" VALUE="">
                                                <TABLE WIDTH="100%" CELLSPACING="1" CELLPADDING="1" BORDER="0">
                                                    <TR>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contacttype",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contact",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.phone",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.fax",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.email",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.cellphone",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.department",SessionData.getLanguage()).toUpperCase()%></TD>
                                                        <TD CLASS="rowhead2"></TD>
                                                    </TR>
<%
    if ( Rep.getVecContacts().size() > 0 )
    {//If there are contacts to show
%>
                                                    <TR>
                                                      <TD>
                                                        <INPUT TYPE="hidden" ID="mxContactIndex" NAME="mxContactIndex" VALUE="">
                                                        <DIV STYLE="display:none;"><INPUT TYPE="text" NAME="contactData" VALUE="<%=Rep.getVecContacts().size()%>"></DIV>
                                                        <SCRIPT>
                                                            function mxEditContact()
                                                            {
                                                              document.getElementById('mxContactAction').value = "EDIT";
                                                              document.subAgent.onsubmit = new Function("return true;");
                                                              //document.merchant.submit.click();
                                                              document.subAgent.submit.disabled = true;
                                                            }
                                                            function mxDeleteContact()
                                                            {
                                                              document.getElementById('mxContactAction').value = "DELETE";
                                                              document.subAgent.onsubmit = new Function("return true;");
                                                              //document.merchant.submit.click();
                                                              document.subAgent.submit.disabled = true;
                                                            }
                                                        </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
      int intEvenOdd = 1;
      int contactIndex = 0;
      Vector vecContacts = Rep.getVecContacts();
      Iterator itContacts = vecContacts.iterator();
      while (itContacts.hasNext())
      {
        Vector vecTemp = null;
        vecTemp = (Vector)itContacts.next();
%>
                                                    <TR CLASS="row<%=intEvenOdd%>">
<%
        if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("EDIT")) && (request.getParameter("mxContactIndex") != null) && (contactIndex == Integer.parseInt(request.getParameter("mxContactIndex"))) )
        {//If we are editing an item
%>
                                                        <TD>
                                                          <SCRIPT>document.getElementById("mxContactIndex").value = "<%=contactIndex%>";</SCRIPT>
                                                          <SELECT NAME="contactTypeId" TABINDEX="<%=intTabIndex++%>">
<%
          Vector vecContactTypes = com.debisys.customers.Merchant.getContactTypes();
          Iterator itContactTypes = vecContactTypes.iterator();
          while (itContactTypes.hasNext())
          {
            vecTemp = null;
            vecTemp = (Vector)itContactTypes.next();
            String strContactTypeId = vecTemp.get(0).toString();
            String strContactTypeCode = vecTemp.get(1).toString();
            if (strContactTypeId.equals(Integer.toString(Rep.getContactTypeId())))
            {
              out.println("<OPTION VALUE=\"" + strContactTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
            }
            else
            {
              out.println("<OPTION VALUE=\"" + strContactTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
            }
          }
%>
                                                          </SELECT>
                                                        </TD>
                                                        <TD><INPUT TYPE="text" NAME="contactName" VALUE="<%=Rep.getContactName()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactPhone" VALUE="<%=Rep.getContactPhone()%>" SIZE="15" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactFax" VALUE="<%=Rep.getContactFax()%>" SIZE="15" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactEmail" VALUE="<%=Rep.getContactEmail()%>" SIZE="15" MAXLENGTH="200" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactCellPhone" VALUE="<%=Rep.getContactCellPhone()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactDepartment" VALUE="<%=Rep.getContactDepartment()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
<%
        }//End of if we are editing an item
        else
        {//Else just show the item data
%>
                                                        <TD><%=com.debisys.customers.Merchant.getContactTypeById(vecTemp.get(0).toString(),SessionData)%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(1).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(2).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(3).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(4).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(5).toString())%></TD>
                                                        <TD><%=StringUtil.toString(vecTemp.get(6).toString())%></TD>
                                                        <TD>
                                                            <TABLE>
                                                                <TR>
                                                                    <TD>
                                                                      <INPUT TYPE="image" TITLE="<%=Languages.getString("jsp.admin.customers.merchants_edit.editcontact",SessionData.getLanguage())%>" SRC="images/icon_edit.gif" ONCLICK="document.getElementById('mxContactIndex').value = '<%=contactIndex%>';mxEditContact();">
                                                                    </TD>
                                                                    <TD>&nbsp;</TD>
                                                                    <TD>
                                                                      <INPUT TYPE="image" TITLE="<%=Languages.getString("jsp.admin.customers.merchants_edit.deletecontact",SessionData.getLanguage())%>" SRC="images/icon_delete.gif" ONCLICK="document.getElementById('mxContactIndex').value = '<%=contactIndex%>';mxDeleteContact();">
                                                                    </TD>
                                                                </TR>
                                                            </TABLE>
                                                        </TD>
<%
        }//End of else just show the item data
%>
                                                    </TR>
<%
        intEvenOdd = ((intEvenOdd == 1)?2:1);
        contactIndex++;
      }
    }//End of if there are contacts to show
    else if ( (request.getParameter("mxContactAction") == null) ||
              (request.getParameter("mxContactAction") != null) && !request.getParameter("mxContactAction").equals("ADD")
            )
    {//Else show a message of no contacts
%>
                                                    <TR><TD CLASS="main" COLSPAN="8" ALIGN="center"><%=Languages.getString("jsp.admin.customers.merchants_edit.nocontacts",SessionData.getLanguage())%></TD></TR>
                                                    <TR><TD CLASS="main" COLSPAN="8" ALIGN="center"><DIV STYLE="display:none;"><INPUT TYPE="text" NAME="contactData" VALUE=""></DIV></TD></TR>
<%
    }//End of else show a message of no contacts

    if ( (request.getParameter("mxContactAction") == null) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("SAVE"))) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("CANCEL"))) ||
        ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("DELETE")))
       )
    {//If we need to show the ADD button
%>
                                                    <TR>
                                                      <TD CLASS="main" COLSPAN="8" ALIGN="right">
                                                        <INPUT TYPE="button" ID="mxBtnAddContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.addcontact",SessionData.getLanguage())%>" ONCLICK="mxAddContact();">
                                                        <SCRIPT>
                                                            function mxAddContact()
                                                            {
                                                              document.getElementById('mxContactAction').value = "ADD";
                                                              document.getElementById('mxBtnAddContact').disabled = true;
                                                              document.subAgent.onsubmit = new Function("return true;");
                                                              document.subAgent.submit.click();
                                                              document.subAgent.submit.disabled = true;
                                                            }
                                                        </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
    }//End of if we need to show the ADD button
    else if ( (request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("ADD")) )
    {//Else if we need to show the row for a new Contact
%>
                                                    <TR CLASS="main">
                                                        <TD>
                                                          <DIV STYLE="display:none;"><INPUT TYPE="text" NAME="contactData" VALUE="<%=((Rep.getVecContacts().size() == 0)?"":"1")%>"></DIV>
                                                          <SELECT NAME="contactTypeId" TABINDEX="<%=intTabIndex++%>">
<%
      Vector vecContactTypes = com.debisys.customers.Merchant.getContactTypes();
      Iterator itContactTypes = vecContactTypes.iterator();
      while (itContactTypes.hasNext())
      {
        Vector vecTemp = null;
        vecTemp = (Vector)itContactTypes.next();
        String strContactTypeId = vecTemp.get(0).toString();
        String strContactTypeCode = vecTemp.get(1).toString();
        if (strContactTypeId.equals(request.getParameter("contactTypeId")))
        {
          out.println("<OPTION VALUE=\"" + strContactTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
        }
        else
        {
          out.println("<OPTION VALUE=\"" + strContactTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + strContactTypeCode,SessionData.getLanguage()) + "</OPTION>");
        }
      }
%>
                                                          </SELECT>
                                                        </TD>
                                                        <TD><INPUT TYPE="text" NAME="contactName" VALUE="<%=Rep.getContactName()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactPhone" VALUE="<%=Rep.getContactPhone()%>" SIZE="15" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactFax" VALUE="<%=Rep.getContactFax()%>" SIZE="15" MAXLENGTH="15" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactEmail" VALUE="<%=Rep.getContactEmail()%>" SIZE="15" MAXLENGTH="200" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactCellPhone" VALUE="<%=Rep.getContactCellPhone()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                        <TD><INPUT TYPE="text" NAME="contactDepartment" VALUE="<%=Rep.getContactDepartment()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"></TD>
                                                    </TR>
<%
    }//End of else if we need to show the row for a new Contact

    if ( ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("ADD"))) ||
         ((request.getParameter("mxContactAction") != null) && (request.getParameter("mxContactAction").equals("EDIT")))
       )
    {//If we need to show the SAVE-CANCEL button
%>
                                                    <TR>
                                                      <TD CLASS="main" COLSPAN="8" ALIGN="right">
                                                        <INPUT TYPE="button" ID="mxBtnSaveContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.savecontact",SessionData.getLanguage())%>" ONCLICK="mxSaveContact();">
                                                        <INPUT TYPE="button" ID="mxBtnCancelContact" VALUE="<%=Languages.getString("jsp.admin.customers.merchants_edit.cancelsavecontact",SessionData.getLanguage())%>" ONCLICK="mxCancelSaveContact();">
                                                        <SCRIPT>
                                                            function mxSaveContact()
                                                            {
                                                              error = false;
                                                              form = document.subAgent;
                                                              error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";
                                                              check_input("contactName", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_contact",SessionData.getLanguage())%>");
                                                              check_input("contactPhone", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.error_phone",SessionData.getLanguage())%>");
                                                              if ( error )
                                                              {
                                                                alert(error_message);
                                                                return false;
                                                              }

                                                              document.getElementById('mxContactAction').value = "SAVE";
                                                              document.getElementById('mxBtnSaveContact').disabled = true;
                                                              document.subAgent.onsubmit = new Function("return true;");
                                                              document.subAgent.submit.click();
                                                              document.subAgent.submit.disabled = true;
                                                            }

                                                            function mxCancelSaveContact()
                                                            {
                                                              document.getElementById('mxContactAction').value = "CANCEL";
                                                              document.getElementById('mxBtnSaveContact').disabled = true;
                                                              document.getElementById('mxBtnCancelContact').disabled = true;
                                                              document.subAgent.onsubmit = new Function("return true;");
                                                              document.subAgent.submit.click();
                                                              document.subAgent.submit.disabled = true;
                                                            }
                                                        </SCRIPT>
                                                      </TD>
                                                    </TR>
<%
    }//End of if we need to show the SAVE-CANCEL button
%>
                                                </TABLE>
                                            </TD>
                                        </TR>
			     						<tr>
				        					<td class="formArea2">
				        						<table width="100">
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="smsNotificationPhone" id="smsNotificationPhone" value="<%=Rep.getSmsNotificationPhone()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="mailNotificationAddress" id="mailNotificationAddress" value="<%=Rep.getMailNotificationAddress()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
													</tr>												
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" onClick="changePayNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" onClick="changePayNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" onClick="changePayNotiMail()" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" onClick="changeBalNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" onClick="changeBalNotiValue()" />
														</td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" onClick="changeBalNotiDays()" />
														</td>
													</tr>													
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationValue" id="balanceNotificationValue" value="<%=Rep.getBalanceNotificationValue()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationDays" id="balanceNotificationDays" value="<%=Rep.getBalanceNotificationDays()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" onClick="changeBalNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" onClick="changeBalNotiMail()" />
														</td>
													</tr>
<%
   }
%>		
												</table>
												<script>changePayNoti()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>changeBalNoti()</script>
<%
   }
%>												
											</td>
										</tr>
<%
  }
  else
  {//Else when deploying in others
%>
                          <tr>
                            <td class="formArea2">
                              <table width="100">
                                <tr class="main">
                                  <td nowrap>
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.contact",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap>
                                    <table border=0>
                                      <tr class=main>
                                        <td>
                                         <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
                           				<%= Languages.getString("jsp.admin.customers.reps_add.contact_first",SessionData.getLanguage()) %>
                    		        	<% } else{%>
            		                	<%= Languages.getString("jsp.admin.customers.reps_add.contact_first_international",SessionData.getLanguage()) %>
			                            <%}%>
			                            </td>
                                        <td>
                                          <%= Languages.getString("jsp.admin.customers.reps_add.contact_middle",SessionData.getLanguage()) %>
                                        </td>
                                        <td>
                                          <%= Languages.getString("jsp.admin.customers.reps_add.contact_last",SessionData.getLanguage()) %>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <input type="text" name="contactFirst" value="<%= Rep.getContactFirst() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                          if (repErrors != null && repErrors.containsKey("contactFirst"))
                                          {
                                            out.print("<font color=ff0000>*</font>");
                                          }
%>
                                        </td>
                                        <td>
                                          <input type="text" name="contactMiddle" value="<%= Rep.getContactMiddle() %>" size="2" maxlength="1" tabIndex="<%= intTabIndex++ %>">
<%
                                          if (repErrors != null && repErrors.containsKey("contactMiddle"))
                                          {
                                            out.print("<font color=ff0000>*</font>");
                                          }
%>
                                        </td>
                                        <td>
                                          <input type="text" name="contactLast" value="<%= Rep.getContactLast() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                          if (repErrors != null && repErrors.containsKey("contactLast"))
                                          {
                                            out.print("<font color=ff0000>*</font>");
                                          }
%>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr class="main">
                                  <td>
                                    <b><%= Languages.getString("jsp.admin.customers.reps_add.phone",SessionData.getLanguage()) %>:</b>
                                  </td>
                                  <td nowrap>
                                    <input type="text" name="contactPhone" value="<%= Rep.getContactPhone() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("contactPhone"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
                                <tr class="main">
                                  <td>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.fax",SessionData.getLanguage()) %>:
                                  </td>
                                  <td nowrap>
                                    <input type="text" name="contactFax" value="<%= Rep.getContactFax() %>" size="15" maxlength="15" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("contactFax"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
                                <tr class="main">
                                  <td>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.email",SessionData.getLanguage()) %>:
                                  </td>
                                  <td nowrap>
                                    <input type="text" name="contactEmail" value="<%= Rep.getContactEmail() %>" size="20" maxlength="200" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("contactEmail"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                </tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="smsNotificationPhone" id="smsNotificationPhone" value="<%=Rep.getSmsNotificationPhone()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
													</tr>
													<tr class="main" nowrap>
														<td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress",SessionData.getLanguage())%>:</td><td nowrap><input type="text" name="mailNotificationAddress" id="mailNotificationAddress" value="<%=Rep.getMailNotificationAddress()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
													</tr>												
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="paymentNotifications" name="paymentNotifications" type="checkbox" onClick="changePayNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" onClick="changePayNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" onClick="changePayNotiMail()" />
														</td>
													</tr>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
													<tr>
														<td class="main" nowrap>
															<B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications",SessionData.getLanguage())%>:</B>
														</td>
														<td nowrap class="main">
															<input id="balanceNotifications" name="balanceNotifications" type="checkbox" onClick="changeBalNoti()" />
														</td>
													</tr>
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType",SessionData.getLanguage())%>
														</td>
													</tr>	
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" onClick="changeBalNotiValue()" />
														</td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" onClick="changeBalNotiDays()" />
														</td>
													</tr>													
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationValue" id="balanceNotificationValue" value="<%=Rep.getBalanceNotificationValue()%>" size="20" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays",SessionData.getLanguage())%>:</td><td nowrap><input class="numeric" type="text" name="balanceNotificationDays" id="balanceNotificationDays" value="<%=Rep.getBalanceNotificationDays()%>" size="20" maxlength="50" tabIndex="<%=intTabIndex++%>"></td>
													</tr>													
													<tr>
														<td class="main" nowrap>
															<%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" onClick="changeBalNotiSMS()" />
														</td>
													</tr>
													<tr>
														<td  class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail",SessionData.getLanguage())%>:</td>
														<td>
															<input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" onClick="changeBalNotiMail()" />
														</td>
													</tr>
<%
   }
%>
							</table>
							<script>changePayNoti()</script>
<%
   //DTU-480: Low Balance Alert Message SMS and/or email
   if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)){
%>
				<script>changeBalNoti()</script>
<%
   }
%>							
                            </td>
                          </tr>
<%
  }//End of else when deploying in others
%>

 			  <%
              if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES))
              {
              %>
						<tr>
      						<td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.common_info_edit.payment_information",SessionData.getLanguage())%></td>
      					</tr>
      					<tr>
	        					<td class="formArea2">
	        						<table width="100">
              						<tr class="main">
              						 <td nowrap>
              						   <%=Languages.getString("jsp.admin.customers.common_info_edit.payment_type",SessionData.getLanguage())%>
              						 </td>
               						 <td nowrap>
               						  <SELECT NAME="paymentType">
               						   <%
										  Vector vecPaymentTypes = com.debisys.customers.Rep.getPaymentTypes();
										  Iterator itPaymentTypes = vecPaymentTypes.iterator();
										  boolean flag_payment=false;
										  while (itPaymentTypes.hasNext()) {
										    Vector vecTemp = null;
										    vecTemp = (Vector)itPaymentTypes.next();
										    String strPaymentTypeId = vecTemp.get(0).toString();
										    String strPaymentTypeCode = vecTemp.get(1).toString();
										    if ( (request.getParameter("paymentType") != null) && (strPaymentTypeId.equals(request.getParameter("paymentType"))) ){
										        flag_payment = true;
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
										    }
										    else if ( strPaymentTypeId.equals(Integer.toString(Rep.getPaymentType())) ){
										        flag_payment = true;
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
										    }
										    else{

										      if ( Rep.getPaymentType() == 0 && strPaymentTypeId.equals(Integer.toString(2)) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) ){
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
										      }
										      else if (Rep.getPaymentType() == 0 && strPaymentTypeId.equals(Integer.toString(3)) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ){
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\" SELECTED>" + strPaymentTypeCode + "</OPTION>");
										      }
										      else{
										        out.println("<OPTION VALUE=\"" + strPaymentTypeId + "\">" + strPaymentTypeCode + "</OPTION>");
										      }

										    }
										  }
										  if ( !flag_payment ){
										   %>
										      <OPTION VALUE="-1" >--</OPTION>
										   <%
										  }
										%>

               						  </SELECT>
               						  </TD>

               						  <td nowrap>
               						  <% if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
						                		   <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type",SessionData.getLanguage())%>
						                		   <% } else{%>
						                		   <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type_international",SessionData.getLanguage())%>
						               <%}%>
              						 </td>
               						 <td nowrap>
               						  <SELECT NAME="processType">
               						     <%
										  Vector vecProcessTypes = com.debisys.customers.Rep.getProcessorTypes();
										  Iterator itProcessTypes = vecProcessTypes.iterator();
										  boolean flag_process=false;
										  while (itProcessTypes.hasNext()) {
										    Vector vecTemp = null;
										    vecTemp = (Vector)itProcessTypes.next();
										    String strProcessTypeId = vecTemp.get(0).toString();
										    String strProcessTypeCode = vecTemp.get(1).toString();

										    if ( (request.getParameter("processType") != null) && (strProcessTypeId.equals(request.getParameter("processType"))) ){
										        flag_process = true;
										        out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
										    }
										    else if ( strProcessTypeId.equals(Integer.toString(Rep.getProcessType())) ){
										        flag_process = true;
										        out.println("<OPTION VALUE=\"" + strProcessTypeId + "\" SELECTED>" + strProcessTypeCode + "</OPTION>");
										    }
										    else
										      out.println("<OPTION VALUE=\"" + strProcessTypeId + "\">" + strProcessTypeCode + "</OPTION>");
										  }
										  if ( !flag_process ){
										   %>
										      <OPTION VALUE="-1" SELECTED>--</OPTION>
										   <%
										  }
										%>
               						  </SELECT>
               						  </TD>
               						  </tr>
               						</table>
               			 </tr>


				<%
				}
				%>

                          <tr>
                            <td class="formAreaTitle2">
                              <br>
                              <%= Languages.getString("jsp.admin.customers.reps_add.banking_information",SessionData.getLanguage()) %>
                            </td>
                          </tr>
                          <tr>
                            <td class="formArea2">
                              <table width="100">
                                <tr class="main">
               						<TD NOWRAP>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
    out.print("<B>" + Languages.getString("jsp.admin.customers.merchants_edit.tax_id",SessionData.getLanguage()) + ":</B>");
%>
               						</TD><td nowrap><input type="text" name="taxId" value="<%=Rep.getTaxId()%>" size="15" maxlength="15" tabIndex="<%=intTabIndex++%>" ONBLUR="ValidateRegExp(this, '[A-Z]{3,4}-[0-9]{6}-[A-Z0-9]{3}')"><%if (repErrors != null && repErrors.containsKey("taxId")) out.print("<font color=ff0000>*</font>");%>
                                                        &nbsp;<%=Languages.getString("jsp.admin.customers.merchants_edit.sample_taxid",SessionData.getLanguage())%>
               						</td>
              						</tr>
<%
  } else { //Else when deploying in others
    out.print(Languages.getString("jsp.admin.customers.reps_add.tax_id_international",SessionData.getLanguage()) + ":");
%>
               						</TD><td nowrap>
                                    <input type="text" name="taxId" value="<%= Rep.getTaxId() %>" size="15" maxlength="12" tabIndex="<%= intTabIndex++ %>">
<%
                                    if (repErrors != null && repErrors.containsKey("taxId"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
              						</td>
              						</tr>
<%
  }
%>
<%
  if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  {//If when deploying in Mexico
%>
                                        <TR CLASS="main">
                                            <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.bankname",SessionData.getLanguage())%>:&nbsp;</TD>
                                            <TD><INPUT TYPE="text" NAME="bankName" VALUE="<%=Rep.getBankName()%>" SIZE="15" MAXLENGTH="50" TABINDEX="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("bankName")) out.print("<font color=ff0000>*</font>");%></TD>
                                        </TR>
                                        <TR CLASS="main">
                                            <TD NOWRAP><%=Languages.getString("jsp.admin.customers.merchants_edit.accounttype",SessionData.getLanguage())%>:&nbsp;</TD>
                                            <TD>
                                                <SELECT NAME="accountTypeId" TABINDEX="<%=intTabIndex++%>">
<%
  Vector vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
  Iterator itAccountTypes = vecAccountTypes.iterator();
  while (itAccountTypes.hasNext()) {
    Vector vecTemp = null;
    vecTemp = (Vector)itAccountTypes.next();
    String strAccountTypeId = vecTemp.get(0).toString();
    String strAccountTypeCode = vecTemp.get(1).toString();
    if (strAccountTypeId.equals(request.getParameter("accountTypeId")))
      out.println("<OPTION VALUE=\"" + strAccountTypeId + "\" SELECTED>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
    else
      out.println("<OPTION VALUE=\"" + strAccountTypeId + "\">" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode,SessionData.getLanguage()) + "</OPTION>");
  }
%>
                                                </SELECT>
                                            </TD>
                                        </TR>
<%
  } //End of if when deploying in Mexico
%>
                                <tr class="main">
                                  <td nowrap>
                                    <% if ( (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) ){%>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.routing_number",SessionData.getLanguage()) %>
								<%}else{%>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.routing_number_international",SessionData.getLanguage()) %>
									<%}%>:
                                  </td>
                                  <td nowrap>
                                  <%String regExpresion="[0-9]{9}"; %>
                                  <%String textErrorAba = Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format",SessionData.getLanguage()); %>
                                    <input type="text" id="routingNumber" name="routingNumber" value="<%= Rep.getRoutingNumber() %>"
                                    size="15" maxlength="18" tabIndex="<%= intTabIndex++ %>"
                                    <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                       onKeyUp="verifyAba(9,'<%=regExpresion%>','<%=textErrorAba%>');"
                                    <%}%>
                                    >
<%
                                    if (repErrors != null && repErrors.containsKey("routingNumber"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                  <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                   	<td>
                                       <%String textInfoAba = Languages.getString("jsp.admin.customers.field_no_applicable.info_field_no_applicable",SessionData.getLanguage()); %>
                                       <input type="checkbox" id="optionAba" name="optionAba" value="optionAba" onClick="changeCheckAba()"
                                       <%if(Rep.getRoutingNumber().startsWith("NA")){ %>
																	 checked
																<%} %>
                                       ><%=textInfoAba%>
                                       </td>
                                    <td><div id="div_info_aba" name="div_info_aba" ></div></td>
                                  <%}%>
                                </tr>
                                <tr class="main">
                                  <td>
                                    <%= Languages.getString("jsp.admin.customers.reps_add.account_number",SessionData.getLanguage()) %>:
                                  </td>
                                  <td nowrap>
                                  <%String textErrorAccount = Languages.getString("jsp.admin.customers.edit_field_account.error_account_format",SessionData.getLanguage()); %>
                                    <input type="text" id="accountNumber" name="accountNumber" value="<%= Rep.getAccountNumber() %>" size="15" maxlength="20"
                                    tabIndex="<%= intTabIndex++ %>"
                                    <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                    	onKeyUp="verifyAccount(20,'<%=textErrorAccount%>');"
                                    <%} %>
                                    >
<%
                                    if (repErrors != null && repErrors.containsKey("accountNumber"))
                                    {
                                      out.print("<font color=ff0000>*</font>");
                                    }
%>
                                  </td>
                                  <%if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){%>
                                  	<td>
                                      	<%String textInfoAccount = Languages.getString("jsp.admin.customers.field_no_applicable.info_field_no_applicable",SessionData.getLanguage()); %>
                                      	<input type="checkbox" id="optionAccount" name="optionAccount" value="optionAccount" onClick="changeCheckAccount()"
                                      	<%if(Rep.getAccountNumber().startsWith("NA")){ %>
																	 checked
																<%} %>
                                      	><%=textInfoAccount%>
                                  	</td>
								  	<td><div id="div_info_account" name="div_info_account" ></div></td>
								  <%}%>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                              <td class="formAreaTitle2">
                                  <%= Languages.getString("jsp.admin.customers.reps_add.credit_limit", SessionData.getLanguage()) %>
                              </td>
                          </tr>
                          <tr>
                            <td class="formArea2" valign=top>
<%
                                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) || deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
                                    out.println("<table width=\"100\">");
                                    
                                    String merchantTypePrepaid   = "", merchantTypeCredit    = "", merchantTypeUnlimited = "";

                                    if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                                        merchantTypePrepaid = " selected";
                                    } else if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                                        merchantTypeCredit = " selected";
                                    } else if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                                        merchantTypeUnlimited = " selected";
                                    } else {
                                        merchantTypeUnlimited = " selected";
                                    }

                                    // DBSY-804
                                    // Get ISO of SubAgent and then get credit type to determine if we need to force a
                                    // flexible credit type
                                    String isoid = SessionData.getProperty("iso_id");
                                    String isoCredType = Rep.getISOCreditType(isoid);
                                    
                                    Hashtable<String, String> options =
									CreditTypes.getAvailableOptionsForSubAgent(isoCredType, customConfigType, deploymentType,
                                                                        new String[]{merchantTypePrepaid,merchantTypeCredit,merchantTypeUnlimited});
                                    String disabledText = "";
                                    List<Boolean> otherOptions = CreditTypes.getFormOptionsReps(
                                                                                customConfigType, deploymentType, Rep.getRepCreditType(), isoCredType,
                                                                                new String[]{merchantTypePrepaid, merchantTypeCredit, merchantTypeUnlimited},
                                                                                DebisysConstants.REP_TYPE_SUBAGENT);
                                    boolean hideCreditLimitBox = otherOptions.get(5);
                                   
                                    if (options.containsKey(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE)) {
                                        disabledText = "disabled=\"true\"";
                                    }
%>
                                    <script type="text/javascript">
                                        function addToParent(repId, repName, isUnlimited, CanChangeAccountType) {
                                            document.subAgent.parentRepId.value = repId;
                                            document.subAgent.parentRepName.value = repName;

                                            if (!isUnlimited) {
                                                var combo = document.getElementById('lstSubAgentCreditType');
                                                var r = -1;

                                                for (i = 0; i < combo.length; i++) {
                                                    if (combo.options[i].text == '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type3", SessionData.getLanguage())%>') {
                                                        r = i;
                                                    }
                                                }

                                                if (r != -1) {
                                                    combo.remove(r);
                                                }
                                            } else  {
<%                                                 // Only when INTL+FLEX we skip the addition of UNLIMITED option
                                                if (disabledText.equals("")) {
%>
                                                    var combo = document.getElementById('lstSubAgentCreditType');
                                                    var r = -1;

                                                    for (i=0;i<combo.length;i++) {
                                                        if (combo.options[i].text == '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage())%>') {
                                                            r = i;
                                                        }
                                                    }

                                                    if (r == -1) { // meaning there is no 'Unlimited' Option
                                                        var y=document.createElement('option');
                                                        y.text = '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage())%>';
                                                        y.value = '3';

                                                        try {
                                                            combo.add(y, null); // standards compliant; doesn't work in IE
                                                        } catch(ex) {
                                                            combo.add(y); // IE only
                                                        }

                                                        for (i = 0; i < combo.length; i++) {
                                                            if (combo.options[i].text == '<%=Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage())%>') {
                                                                combo.selectedIndex = i;
                                                            }
                                                        }
                                                    }
<%
                                                }
%>
                                            }
                                            document.getElementById('lstSubAgentCreditType').onchange();
<%
                                            if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) { // DBSY-1072 eAccounts Interface
%>
                                                var combo = document.getElementById('entityAccountType');
                                                if (CanChangeAccountType != null) {
                                                    if (!CanChangeAccountType) {
                                                        combo.disabled = true;
                                                        combo.options.selectedIndex ="0";
                                                        var temp = document.getElementById('disabledentityAccountType');
                                                        temp.value = "1";
                                                    } else {
                                                        combo.disabled = false;
                                                        combo.options.selectedIndex ="0";
                                                        var temp = document.getElementById('disabledentityAccountType');
                                                        temp.value = "";
                                                    }
                                                } else {
                                                    combo.disabled = false;
                                                    combo.options.selectedIndex ="0";
                                                    var temp = document.getElementById('disabledentityAccountType');
                                                    temp.value = "";
                                                }
<%
                                            }                                            
%>
                                        }
                                    </script>
                                    <tr class=main style="display:block;">
                                        <td nowrap><%=Languages.getString("jsp.admin.customers.reps_add.rep_type",SessionData.getLanguage())%></td>

                                        <script type="text/javascript">
                                            function checkCreditLimitBox() {
                                                if (document.getElementById('lstSubAgentCreditType').value == <%=DebisysConstants.REP_CREDIT_TYPE_UNLIMITED%>) {
                                                    document.getElementById('trCreditLimit').style.display = "none";
                                                } else {
                                                    document.getElementById('trCreditLimit').style.display = "block";
                                                }
                                            }
                                        </script>                                             
                                        <td>
                                            <select id="lstSubAgentCreditType" name="repCreditType" <%=disabledText%> onchange="checkCreditLimitBox();" tabIndex="<%= intTabIndex++ %>">
                                                <option value="">--</option>
<%
                                            for (String option: options.keySet()) {
                                                String getString = "jsp.admin.customers.reps_add.credit_type" + option;
%>
                                                <option value=<%=option%> <%=options.get(option)%>><%=Languages.getString(getString,SessionData.getLanguage())%></option>
<%
                                            }
                                            String styleCreditLimitBox = "style=\"display:block;\"";
                                            if (hideCreditLimitBox) {
                                                styleCreditLimitBox = "style=\"display:none;\"";
                                            }
%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class=main id="trCreditLimit" <%out.println(styleCreditLimitBox);%>>
                                      <td nowrap><%=Languages.getString("jsp.admin.customers.reps_add.credit_limit",SessionData.getLanguage())%></td>
                                      <td><input type="text" name="creditLimit" value="<%=Rep.getCreditLimit()%>" size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"><%if (repErrors != null && repErrors.containsKey("creditLimit")) {out.print("<font color=ff0000>&nbsp;*</font>");}%></td>
                                    </tr>
<%
                                    out.print("</tr>");
                                } //End of if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
%>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td class="formAreaTitle2" align="center">
                            <input type="hidden" name="submitted" value="y">
                            <input type="submit" name="submit" value="<%= Languages.getString("jsp.admin.customers.subagents_add.submit",SessionData.getLanguage()) %>" tabIndex="<%= intTabIndex++ %>">
                            <input type=button value="<%= Languages.getString("jsp.admin.cancel",SessionData.getLanguage()) %>" onClick="history.go(-1)" tabIndex="<%= intTabIndex++ %>">
                          </td>
                        </tr>
                      </form>
                    </table>
<%
                  }//End of if (!repAdded)
                  else
                  {//if (subagentAdded)
%>
                    <table width="100%" align=left>
                      <tr>
                        <td class="formAreaTitle2">
                          <%= Languages.getString("jsp.admin.customers.subagents_add.success",SessionData.getLanguage()) %>
                        </td>
                      </tr>
                      <tr>
                        <td class="formArea2">
                          <br>
                          <%= Languages.getString("jsp.admin.customers.reps_add.do_next",SessionData.getLanguage()) %>:
                          <br>
                           <li>
                              <a href="admin/customers/subagents_add.jsp?parentRepId=<%= Rep.getParentRepId() %>"><%= Languages.getString("jsp.admin.customers.subagents_add.option1",SessionData.getLanguage()) %></a>
                            </li>
                              <li>
                                <a href="admin/customers/subagents_add.jsp"><%= Languages.getString("jsp.admin.customers.subagents_add.option2",SessionData.getLanguage()) %></a>
                              </li>
                           <li>
                                  <a href="admin/customers/subagents.jsp?search=y"><%= Languages.getString("jsp.admin.customers.subagents_add.option3",SessionData.getLanguage()) %></a>
                                </li>
                                  <li>
                                    <a href="admin/customers/subagents_info.jsp?repId=<%= Rep.getRepId() %>"><%= Languages.getString("jsp.admin.customers.subagents_add.option4",SessionData.getLanguage()) %></a>
                                  </li>
                                    <br>
                                    <br>
                                    <br>
                                  </td>
                                </tr>
                              </table>
<%
                            }//End of if (repAdded)
%>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <%@ include file="/includes/footer.jsp" %>
