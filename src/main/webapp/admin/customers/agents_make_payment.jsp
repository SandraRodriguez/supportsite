<%@ page import="com.debisys.utils.*,
                 java.util.*"%>
<%
int section=2;
int section_page=17;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="page"/>
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>
<script language="JavaScript">
alert(<%=DebisysConfigListener.getCustomConfigType(application)%>);
</script>
<%
  if (NumberUtil.isNumeric(Rep.getPaymentAmount()) && NumberUtil.isNumeric(Rep.getCommission()))
  {
    if (Rep.getAssignedCreditAgent(SessionData) > (Rep.getCurrentAgentCreditLimit(SessionData) + Double.parseDouble(Rep.getPaymentAmount()))
        && !Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
    {
      response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId() + "&message=4");
      return;
    }
    else
    {
        if ((!Rep.getOldRepCreditType().equals(Rep.getRepCreditType()) && !Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) ||
        		(!Rep.getOldRepCreditType().equals(Rep.getRepCreditType()) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)))
        {
          if (!Rep.checkChildCreditType())
          {
            //error - merchants under must be all prepaid or unlimited
            response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId() + "&message=5");
            return;
          }
          else
          {
            Rep.makeAgentPayment(SessionData, application,false);
            response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId());
            return;
          }
        }
        else if(!Rep.getOldRepCreditType().equals(Rep.getRepCreditType()) && Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) )
        {
          Rep.makeAgentPayment(SessionData, application,false);
          response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId());
          return;
        }
	    else
	    {
	   	  String redirectUrl= "agents_info.jsp?repId=" + Rep.getRepId();
	         if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
	         {
		       	  Vector vTmp = new Vector();
		       	  try
		       	  {
		       		  vTmp = Rep.doAgentPaymentMX(SessionData, application);
		       	  }
		       	  catch (Exception e)
		       	  {
		       		  vTmp.add(999);
		       	  }
		 	 }
	         else
	         {
	             if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) )
	             {
			              Vector vTmp = new Vector();
			              try
			              {
			               vTmp = Rep.doAgentPaymentMX(SessionData, application);  
			              }
			               catch (Exception e)
			        	  {
			        		  vTmp.add(999);
			        	  }
			        	   if ( vTmp.get(0).toString().equals("0") )
			        	  {
			                  response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId() + "&message=14");
			                  return;
			        	  }
			        	  else
			        	  {
			                  response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId() + "&message=13&code=" + vTmp.get(0));
			                  return;
			        	  }
	       	  	  
	             }
	             else
	             {
	           	  Rep.makeAgentPayment(SessionData, application,false);
	             }
	         }
			if (Rep.getErrors() != null)
			{
			
			    Enumeration enum1=Rep.getErrors().keys();
			    if (enum1.hasMoreElements())
			    {
			      String strKey = enum1.nextElement().toString();
			      String strError = (String) Rep.getErrors().get(strKey);
			      redirectUrl = redirectUrl + "&message=" + strError;
			    }
			
			}
	         response.sendRedirect(redirectUrl);
	         return;
	    }
    }
  }
  else
  {
    response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId() + "&message=2");
    return;
  }

%>