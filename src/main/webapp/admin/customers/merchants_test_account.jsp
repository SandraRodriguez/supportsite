<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="com.debisys.languages.Languages,com.debisys.utils.*"%>
    
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
<jsp:setProperty name="Merchant" property="*" />

<%
	String readOnly="";
	String checkedIsTestAccountCheck = "";
	String note = Languages.getString("jsp.admin.customers.merchants_edit.is_test_account_note", SessionData.getLanguage());
	String confirmNote = Languages.getString("jsp.admin.customers.merchants_edit.is_test_account_confirm", SessionData.getLanguage());

	if(Merchant.getMerchantId() != null && !"".equals(Merchant.getMerchantId())){
		readOnly = "disabled='disabled'";
		if(Merchant.getTestAccountEnabled()){
			checkedIsTestAccountCheck = "checked='checked'";
		}
	}
%>

<script type="text/javascript" charset="utf-8">
	
	function disableBankingOptions(){
		var testAccountEnabled = document.getElementById('testAccountEnabled');
		if(testAccountEnabled.checked){
			alert('<%=note%>');
			var valueAba = document.getElementById('routingNumber');
	  		valueAba.value = 'NA';
	  		valueAba.disabled = true;
	  		document.getElementById('optionAba').disabled = true;
	  		document.getElementById('optionAba').checked = true;
	  		
	  		var valueAccount = document.getElementById('accountNumber');
	  		valueAccount.value = 'NA';
	  		valueAccount.disabled = true;
	  		document.getElementById('optionAccount').disabled = true;
	  		document.getElementById('optionAccount').checked = true;
		} else if (!testAccountEnabled.checked) {
                    if (document.getElementById('lstMerchantType').value === "<%=DebisysConstants.MERCHANT_TYPE_PREPAID%>"){
                        var valueAba = document.getElementById('routingNumber');
	  		valueAba.value = 'NA';
	  		valueAba.disabled = false;
	  		document.getElementById('optionAba').disabled = false;
	  		document.getElementById('optionAba').checked = true;
	  		
	  		var valueAccount = document.getElementById('accountNumber');
	  		valueAccount.value = 'NA';
	  		valueAccount.disabled = false;
	  		document.getElementById('optionAccount').disabled = false;
	  		document.getElementById('optionAccount').checked = true;
                    }else
                    {
			var valueAba = document.getElementById('routingNumber');
	  		valueAba.value = '';
	  		valueAba.disabled = false;
	  		document.getElementById('optionAba').disabled = true;
	  		document.getElementById('optionAba').checked = false;
	  		
	  		var valueAccount = document.getElementById('accountNumber');
	  		valueAccount.value = '';
	  		valueAccount.disabled = false;
	  		document.getElementById('optionAccount').disabled = true;
	  		document.getElementById('optionAccount').checked = false;
                    }
                    
		}
	}
	
	function confirmSetTestAccount() {
		if(document.getElementById('testAccountEnabled').checked){
			return confirm('<%=confirmNote%>');
		} else{
			return true;
		}
	}
</script>

	<tr CLASS="main">
		<td><%=Languages.getString("jsp.admin.customers.merchants_edit.is_test_account", SessionData.getLanguage()) + ":" %></td>
		<td colspan="3">
			<input id="testAccountEnabled" type="checkbox" name="testAccountEnabled" onclick="disableBankingOptions()" <%=readOnly%> <%=checkedIsTestAccountCheck%>/>
		</td>
	</tr>