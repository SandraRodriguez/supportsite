<%@ page import="com.debisys.utils.*,
         java.util.*"%>
<%
    int section = 2;
    int section_page = 17;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="page"/>
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%    

    boolean deploymentIntl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
    boolean customConfigMex = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
    boolean is3LvlChain = strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL);

    boolean wasUnlimited = Rep.getOldRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED);
    boolean isUnlimited = Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED);

    boolean changedCreditType = !Rep.getOldRepCreditType().equals(Rep.getRepCreditType());

    double assignedCredit = Rep.getAssignedCredit(SessionData);
    double currCreditLimit = Rep.getCurrentRepCreditLimit(SessionData);
    double paymtAmt = Double.parseDouble(Rep.getPaymentAmount());

    boolean isPaymentANumber = NumberUtil.isNumeric(Rep.getPaymentAmount());
    boolean isCommissionANumber = NumberUtil.isNumeric(Rep.getCommission());
    Vector vTmp = new Vector();
    if (isPaymentANumber && isCommissionANumber) {

        if (assignedCredit > (currCreditLimit + paymtAmt) && !isUnlimited) {
            response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=4");
            return;
        } else {
            if (changedCreditType && !isUnlimited) {
                if (!Rep.checkMerchantCreditType()) {
                    //error - merchants under must be all prepaid or unlimited
                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=5");
                    return;
                } else {
                    if (is3LvlChain) {
                        Rep.makePayment(SessionData, application, false);
                    } else {
                        if (Rep.checkSubAgentCreditLimit(SessionData, application)) {
                            Rep.makePayment(SessionData, application, false);
                        }
                    }

                    if (customConfigMex) {
                        if (request.getParameter("sRepMerchantPayment") != null) {
                            com.debisys.customers.Merchant m = new com.debisys.customers.Merchant();
                            m.doRepMerchantPayment(SessionData, application, request.getParameter("sRepMerchantPayment"));
                        }
                    }
                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId());
                    return;
                }
            } else if (!wasUnlimited && isUnlimited) {
                if (!is3LvlChain) {
                    if (!Rep.checkSubAgentCreditType(SessionData, application)) {
                        response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=15");
                        return;
                    } else {
                        Rep.makePayment(SessionData, application, false);
                        response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId());
                        return;
                    }
                } else { //what should happen if L3 chain?
                    Rep.makePayment(SessionData, application, false);
                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId());
                    return;
                }
            } else {
                String redirectUrl = "reps_info.jsp?repId=" + Rep.getRepId();
                if (customConfigMex) {
                    if (!is3LvlChain) {
                        if (Rep.checkSubAgentCreditLimit(SessionData, application)) {
                            //Vector vTmp = new Vector();
                            try {
                                vTmp = Rep.doPaymentMX(SessionData, application);
                            } catch (Exception e) {
                                vTmp.add(999);
                            }

                            if (vTmp.get(0).toString().equals("0")) {
                                try {
                                    if (request.getParameter("sRepMerchantPayment") != null) {
                                        com.debisys.customers.Merchant m = new com.debisys.customers.Merchant();
                                        m.doRepMerchantPayment(SessionData, application, request.getParameter("sRepMerchantPayment"));
                                    }
                                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=10");
                                    return;
                                } catch (Exception e) {
                                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=11");
                                    return;
                                }
                            } else {
                                response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=12&code=" + vTmp.get(0));
                                return;
                            }
                        } else {
                            response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=16");
                            return;
                        }
                    } else {
                        
                        try {
                            vTmp = Rep.doPaymentMX(SessionData, application);
                        } catch (Exception e) {
                            vTmp.add(999);
                        }
                        if (vTmp.get(0).toString().equals("0")) {
                            try {
                                if (request.getParameter("sRepMerchantPayment") != null) {
                                    com.debisys.customers.Merchant m = new com.debisys.customers.Merchant();
                                    m.doRepMerchantPayment(SessionData, application, request.getParameter("sRepMerchantPayment"));
                                }
                                response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=10");
                                return;
                            } catch (Exception e) {
                                response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=11");
                                return;
                            }
                        } else {
                            response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=12&code=" + vTmp.get(0));
                            return;
                        }
                    }
                } else {
                    if (deploymentIntl) {
                  //Para Caribe extendido . Reemplazo de la lógica de script por la del SP 
                        // el metodo makePayment ocaciona errores en el calculo de los creditos para esta configuración
                        if (is3LvlChain) {
                            Rep.doPaymentMX(SessionData, application);
                        } else {
                            if (Rep.checkSubAgentCreditLimit(SessionData, application)) {
                                //Vector vTmp = new Vector();
                                try {
                                    vTmp = Rep.doPaymentMX(SessionData, application);
                                } catch (Exception e) {
                                    vTmp.add(999);
                                }

                                if (vTmp.get(0).toString().equals("0")) {
                                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=14");
                                    return;
                                } else {
                                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=13&code=" + vTmp.get(0));
                                    return;
                                }

                            } else {
                                response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=16");
                                return;
                            }
                        }
                    } else {
                        if (is3LvlChain) {
                            Rep.makePayment(SessionData, application, false);
                        } else {
                            //5 levels
                            if (Rep.checkSubAgentCreditLimit(SessionData, application)) {
                                //Rep.makePayment(SessionData, application, false);
                                try {
                                    vTmp = Rep.doPaymentMX(SessionData, application);
                                } catch (Exception e) {
                                    vTmp.add(999);
                                }

                                if (vTmp.get(0).toString().equals("0")) {
                                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=14");
                                    return;
                                } else {
                                    response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=13&code=" + vTmp.get(0));
                                    return;
                                }
                            } else {
                                response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=16");
                                return;
                            }
                        }
                    }
                }

                if (Rep.getErrors() != null) {
                    Enumeration enum1 = Rep.getErrors().keys();
                    if (enum1.hasMoreElements()) {
                        String strKey = enum1.nextElement().toString();
                        String strError = (String) Rep.getErrors().get(strKey);
                        redirectUrl = redirectUrl + "&message=" + strError;
                    }
                }
                response.sendRedirect(redirectUrl);
                return;
            }
        }
    } else {
        response.sendRedirect("reps_info.jsp?repId=" + Rep.getRepId() + "&message=2");
        return;
    }

%>