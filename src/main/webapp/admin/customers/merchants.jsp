<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%
int section=2;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
Vector vecSearchResults = new Vector();
Vector vecCounts = new Vector();
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String strParentRepName = "";
if (request.getParameter("search") != null){
	session.removeAttribute("vecContacts");
    session.removeAttribute("merchantInvoicePaymentsArray");
	if (request.getParameter("page") != null){
		try{
			intPage=Integer.parseInt(request.getParameter("page"));
		}catch(NumberFormatException ex){
			intPage = 1;
		}
	}
	if (intPage < 1){
		intPage=1;
	}
	vecSearchResults = CustomerSearch.searchMerchant(intPage, intPageSize, SessionData, application);
	vecCounts = (Vector) vecSearchResults.get(0);
	intRecordCount = Integer.parseInt(vecCounts.get(0).toString());
	vecSearchResults.removeElementAt(0);
	if (intRecordCount>0){
		intPageCount = (intRecordCount / intPageSize);
		if ((intPageCount * intPageSize) < intRecordCount){
			intPageCount++;
		}
	}
	if (request.getParameter("repId") != null && !request.getParameter("repId").equals("")){
		strParentRepName = Merchant.getMerchantRepName(request.getParameter("repId")).toUpperCase();
	}else{
		strParentRepName = SessionData.getProperty("company_name").toUpperCase();
	}
}
%>
<%@ include file="/includes/header.jsp" %>
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<style>
	#popBG{z-index:2; position:absolute; left:0px; top: 0px; width: 100%; height: 1000px;background-image: url(images/popupbgpixolive.png); filter: alpha (opacity=100);}
	#pop{z-index:3; position:relative; border: 1px solid #333333; text-align:center;}
	#popContent{overflow: scroll;}
	#popClose{float:right; margin:5px; cursor:pointer; position:relative;}
	.grayLabel{color:gray; font-weight:bold;}
	.silvered{background-color:silver;}
</style>
<script type="text/javascript">
	var current_enabled;
	$(document).ready(function(){
		$.extend({URLEncode:function(c){var o='';var x=0;c=c.toString();var r=/(^[a-zA-Z0-9_.]*)/;while(x<c.length){var m=r.exec(c.substr(x));if(m !== null && m.length>1 && m[1] !== ''){o+=m[1];x+=m[1].length;}else{if(c[x] === ' ')o+='+';else{var d=c.charCodeAt(x);var h=d.toString(16);o+='%'+(h.length<2?'0':'')+h.toUpperCase();}x++;}}return o;},URLDecode:function(s){var o=s;var binVal,t;var r=/(%[^%]{2})/;while((m=r.exec(o))!== null && m.length>1 && m[1] !== ''){b=parseInt(m[1].substr(1),16);t=String.fromCharCode(b);o=o.replace(m[1],t);}return o;}});
		$('.cancelled').click(function(){
			showDisableDialog(this);
		});
		$('.disabled').click(function(){
			
			this.id = this.id.replace("disabled","");
			var url1 = 'admin/customers/merchants_update_status.jsp?merchantId='+this.id+'&action=disable&value='+"&reason=&Random=&validateTable=1";
			//window.setTimeout('$("#hola").removeAttr("disabled");', 10000);
			var obj = this;
			window.setTimeout('$("#' + this.id + '").removeAttr("disabled");', 10000);
			$.post(url1, function(responseTextt, textStatuss, XMLHttpRequest){
				var option = 2;
				responseTextt = jQuery.trim(responseTextt);
				if(responseTextt !== "NO_IN_TABLE" && 
				 	responseTextt !== "CANCELLED" &&
				 	responseTextt !== "RESTORED" &&
				 	responseTextt !== "DISABLED" &&
				 	responseTextt !== "ENABLED"){
					option = 1;
					alert('<%=Languages.getString("jsp.admin.customers.merchants.avoidEnable",SessionData.getLanguage())%>');
					$(obj).attr("checked", "checked");				
				}
				if(option === 2){
					showDisableDialog(obj);
				}
			});			
		});
		$("#popClose, #cancelDisable, #okDisable").click(
			function(){
				if(current_enabled){
					var cindex = current_enabled.replace("enabled", "");
					$("#popBG").fadeOut('fast');
					if(this.id !== "okDisable"){
						$("#" + current_enabled).attr("checked", !$("#"+current_enabled).is(':checked'));
						$("#reason" + cindex).val("");
					}else{
						var reason = $.trim($("#reason").val());
						if(reason){
							//$("#"+current_enabled).parent().parent().addClass("silvered");
							$("#reason" + cindex).val(reason);
							var action = ($("#"+current_enabled).hasClass('cancelled')?'cancel':$("#"+current_enabled).hasClass('disabled')?'disable':'');
							var url = 'admin/customers/merchants_update_status.jsp?merchantId='+$("#"+current_enabled).val()+'&action='+action+'&value='+($("#"+current_enabled).is(':checked')?'1':'0')+"&reason=" + escape(reason) + "&Random=" + Math.random();
							$("#"+current_enabled).attr('disabled', 'disabled');
							window.setTimeout('$("#' + current_enabled + '").removeAttr("disabled");', 10000);
							$.post(url, function(responseText, textStatus, XMLHttpRequest){
								if(responseText === "ERROR"){
									alert("Error!");
								}else{
									if(responseText === "CANCELLED"){
										alert('<%=Languages.getString("jsp.admin.customers.merchants.iscancelled",SessionData.getLanguage())%>');
									}else if(responseText === "DISABLED"){
										alert('<%=Languages.getString("jsp.admin.customers.merchants.isdisabled",SessionData.getLanguage())%>');
									}else if(responseText === "RESTORED"){
										//alert('<%=Languages.getString("jsp.admin.customers.merchants.isrestored",SessionData.getLanguage())%>');
									}else if(responseText === "ENABLED"){
										//alert('<%=Languages.getString("jsp.admin.customers.merchants.isenabled",SessionData.getLanguage())%>');
									}
								}
							});
						}else{
							alert("Please specify a reason");
							$("#popBG").fadeIn('fast');
							return;
						}
					}
					$("#reason").val("");
					//$("#" + current_enabled).focus();
					current_enabled = null;
				}
			}
		);
	});
	//});	
	function showDisableDialog(obj){
		//if(obj.checked == false){// && $(obj).attr("startvalue") == "true"){
			var w = $(document).width();
			var h = $(document).height(); 
			var p_w = 400;//$("#pop").width();
			var p_h = 250;//$("#pop").height();
			p_w = (p_w < w*0.8)?p_w:w*0.8;
			p_h = (p_h < h*0.6)?p_h:h*0.6;
			var cw = $(obj).position().left; //(w/2) - (p_w/2);
			var ch = $(obj).position().top; //(h/2) - (p_h/2);
			current_enabled = obj.id;
			$("#popContent").css("width", (p_w-10) + "px").css("height", (p_h-10) + "px");
			$("#pop").css("position", "absolute").css("width", p_w + "px").css("height", p_h + "px").css("left", (cw-p_w) + "px").css("top", (ch-p_h) + "px");
			$("#popBG").css("width", w + "px").css("height", h + "px").fadeIn('fast');
			$("#" + current_enabled).focus();
		//}else{
		//	current_enabled = obj.id;
		//	var cindex = current_enabled.replace("enabled", "");
		//	var url = 'admin/customers/merchants_update_status.jsp?merchantId='+$(this).val()+'&action='+action+'&value='+($(this).is(':checked')?'1':'0')+"&Random=" + Math.random()
		//	$("#reason").load("admin/customers/merchants_info.jsp?merchantId=<%= Merchant.getMerchantId() %>&action=setStatus&siteid=" + cindex + "&reason=&status=1&Random=" + Math.random());
		//	$(obj).parent().parent().removeClass("silvered");
		//	$("#clerk" + cindex ).fadeIn('fast');
		//}
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		 <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants.title",SessionData.getLanguage()).toUpperCase() + " " + strParentRepName.toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
				<tr>
					<td>
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="formArea2">
									<form name="entityform" method="get" action="admin/customers/merchants.jsp" >
										<table width="300">
											<tr class="main">
												<td nowrap="nowrap" valign="top"><%=Languages.getString("jsp.admin.narrow_results",SessionData.getLanguage())%>:</td>
												<td>
<%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
	out.println(Languages.getString("jsp.admin.customers.merchants_edit.route",SessionData.getLanguage()));					
}
%>
												</td>
											</tr>
											<tr>
												<td valign="top" nowrap="nowrap">
													<input name="criteria" type="text" size="30" maxlength="64">
<%
if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
	out.println("<td>");
	out.println("<select id=\"merchantRouteId\" name=\"merchantRouteId\">");
	out.println("<option value=\"-1\" selected>" + Languages.getString("jsp.admin.reports.all",SessionData.getLanguage()) + "</option>");
	Vector vecMerchantRoutes = com.debisys.customers.Merchant.getRoutes();
	Iterator itMerchantRoutes = vecMerchantRoutes.iterator();
	while (itMerchantRoutes.hasNext()){
		Vector vecTemp = null;
		vecTemp = (Vector)itMerchantRoutes.next();
		String strMerchantRouteId = vecTemp.get(0).toString();
		String strMerchantRouteName = vecTemp.get(1).toString();
		out.println("<option value=\"" + strMerchantRouteId + "\">" + strMerchantRouteName + "</option>");
	}
	out.println("</select>");
	out.println("</td>");
}
%>
												</td>
												<td valign="top">
													<input type="hidden" name="search" value="y">
													<input type="hidden" name="repId" value="<%=CustomerSearch.getRepId()%>">
													<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
												</td>
											</tr>
											<tr>
												<td colspan="2" class="main"><font size="1">(<%=Languages.getString("jsp.admin.customers.merchants.search_instructions",SessionData.getLanguage())%>)</font></td>
											</tr>
										</table>
									</form>
<%
if (SessionData.checkPermission(DebisysConstants.PERM_MERCHANTS)){
%>
									<table>
										<tr>
											<td colspan="3" align="left">
												<form method="get" action="admin/customers/merchants_add2.jsp">
													<input type="submit" name="addMerchant" value="<%=Languages.getString("jsp.admin.customers.merchants.add_merchant",SessionData.getLanguage())%>" onclick="showMapFlag()">
												</form>
											</td>
										</tr>
									</table>
<%
}
%>
									<table width="100%" cellspacing="1" cellpadding="1">
<%
if (vecSearchResults != null && vecSearchResults.size() > 0){
%>
										<tr>
											<td class="main" colspan="8">
												<%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
<%
	if (!CustomerSearch.getCriteria().equals("")) {
		out.println(Languages.getString("jsp.admin.for",SessionData.getLanguage()) + " \"" + HTMLEncoder.encode(CustomerSearch.getCriteria()) + "\"");
	}
%>. <br>
<%
	if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
%>
												<%=Languages.getString("jsp.admin.customers.merchants.nav_instructions",SessionData.getLanguage())%>
<%
	}else{
%>
												<%=Languages.getString("jsp.admin.customers.merchants.nav_instructions_international",SessionData.getLanguage())%>
<%
	}
%>
											</td>
										</tr>
										<tr>
											<td colspan="9" class="main" align="right">
												<br>
<%
//add page numbers here
	String baseURL = "admin/customers/merchants.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8") + "&repId=" + URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8") + "&col=" + URLEncoder.encode(CustomerSearch.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(CustomerSearch.getSort(), "UTF-8");
	out.println(DbUtil.displayNavigation(baseURL, intPageSize, intRecordCount, intPage,SessionData));
%>
											</td>
										</tr>
										<tr>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=1&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=1&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants.merchant_id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=2&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=2&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.city",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=3&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=3&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.contact",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=4&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=4&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.phone",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=5&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=5&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants.disable_date",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=6&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=6&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants.rep_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=7&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=7&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<%
	if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
%>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.route",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=8&sort=1"><img src="images/down.png" height="11" width="11" border="0"></a><a href="admin/customers/merchants.jsp?search=y&criteria=<%=URLEncoder.encode(CustomerSearch.getCriteria(), "UTF-8")%>&page=<%=intPage%>&repId=<%=URLEncoder.encode(CustomerSearch.getRepId(), "UTF-8")%>&col=8&sort=2"><img src="images/up.png" height="11" width="11" border="0"></a></td>
<% 
	}
%>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants.disabled",SessionData.getLanguage()).toUpperCase()%></td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants.cancelled",SessionData.getLanguage()).toUpperCase()%></td>
											<td class="rowhead2">&nbsp;</td>
										</tr>
<%
	Iterator it = vecSearchResults.iterator();
	int intEvenOdd = 1;
	String disabledValues = "";
	String cancelledValues = "";
	String checkboxReadOnly = "";
	if (!strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.PERM_MERCHANT_STATUS)){
		checkboxReadOnly = "readonly";
	}
	while (it.hasNext()){
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
%>
										<tr class=row<%= intEvenOdd%>>
											<td width="175">
												<a href="admin/customers/merchants_info.jsp?merchantId=<%=vecTemp.get(2)%>"><%=HTMLEncoder.encode(vecTemp.get(1).toString())%></a>
											</td>
											<td nowrap="nowrap"><%=vecTemp.get(2)%></td>
											<td nowrap="nowrap"><%=vecTemp.get(3)%></td>
											<td nowrap="nowrap"><%=vecTemp.get(4)%></td>
											<td><%=vecTemp.get(5)%></td>
											<td><%=DateUtil.formatDateTime(vecTemp.get(6).toString())%></td>
											<td nowrap="nowrap"><%=("" + vecTemp.get(7) + vecTemp.get(8))%></td>
<%
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
			out.println("<td align=center>" + vecTemp.get(10) + "</td>");
		}
%>
											<td align="center">
												<input type="checkbox" id="disabled<%=vecTemp.get(2)%>" class="disabled" value="<%=vecTemp.get(2)%>" <%=(vecTemp.get(6).equals("")?"": "checked=\"checked\"")%> <%=checkboxReadOnly%>>
											</td>
											<td align="center">
												<input type="checkbox" id="cancelled<%=vecTemp.get(2)%>" class="cancelled" value="<%=vecTemp.get(2)%>" <%=(vecTemp.get(9).equals("0")?"": "checked=\"checked\"")%> <%=checkboxReadOnly%>>
											</td>
											<td nowrap="nowrap">
<%
		if (SessionData.checkPermission(DebisysConstants.PERM_LOGINS)){
%>
												<a href="admin/customers/user_logins.jsp?refId=<%=vecTemp.get(0)%>&refType=<%=DebisysConstants.PW_REF_TYPE_MERCHANT%>">
													<img src="images/icon_user.png" border="0" alt="<%=Languages.getString("jsp.admin.customers.edit_logins",SessionData.getLanguage())%>">
												</a>
<%
		}
%>
												<a href="admin/transactions/merchants_transactions.jsp?merchantId=<%=vecTemp.get(2)%>">
													<img src="images/icon_dollar.png" border="0" alt="<%=Languages.getString("jsp.admin.customers.dollarsign",SessionData.getLanguage())%>">
												</a>

												<a href="admin/customers/merchantDashBoard.jsp?search=y&merchant_Id=<%=vecTemp.get(2)%>">
													<img src="images/mDashboard.png" border="0" alt="<%=Languages.getString("jsp.admin.customers.merchantDashboard",SessionData.getLanguage())%>">
												</a>												
<%
		if(SessionData.checkPermission(DebisysConstants.PERM_MANAGE_APPROVED_RESTRICTED_PRODUCTS)){
%>
												<a href="admin/customers/approvedRestrictedProducts.jsp?search=y&merchant_Id=<%=vecTemp.get(2)%>">
													<img src="images/green-check.png" border="0" alt="<%=Languages.getString("jsp.admin.customers.greencheck",SessionData.getLanguage())%>">
												</a>
<%
		}
%>
											</td>
										</tr>
<%
	if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }
	}
	//take the trailing comma out
	vecSearchResults.clear();
}else if (intRecordCount==0 && request.getParameter("search") != null){
%>
										<tr>
											<td class="main">
												<br><br><font color="#ff0000"><%=Languages.getString("jsp.admin.customers.merchants.not_found",SessionData.getLanguage())%></font>
											</td>
										</tr>
<%
}
%>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div id="popBG" style="display:none;">
	<div id="pop">
		<table border="5" cellpadding="0" cellspacing="0" bgcolor="white" width="100%" height="100%">
			<tr>
				<th>
					<div id="popClose"><img src="images/closebutton.png"></div>
				</th>
			</tr>
			<tr>
				<th align="left">
					<div style="padding:5px"><%= Languages.getString("com.debisys.terminals.jsp.terminaldisabled.reason",SessionData.getLanguage()) %>:</div>
				</th>
			</tr>
			<tr>
				<td align="center">
					<textarea id="reason" name="reason" rows="4" cols="50"></textarea><br>
				</td>
			</tr>
			<tr>
				<td align="right" nowrap="nowrap">
					<input type="button" id="okDisable" value="Ok">
					<input type="button" id="cancelDisable" value="Cancel">
				</td>
			</tr>
		</table>
	</div>
</div>
<script>
    function showMapFlag() {
        var showMapPermission = ("<%=SessionData.checkPermission(DebisysConstants.PERM_ALLOW_LOCALIZE_MERCHANT_ON_MAP)%>" == "true") ? true : false ;
        if (showMapPermission) {
            localStorage["mapFlag"] = true;
        }
    }
</script>
                                        
<%@ include file="/includes/footer.jsp" %>