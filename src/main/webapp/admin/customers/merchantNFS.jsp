<%@ page import="java.util.Vector,
				java.util.Calendar,
				java.text.SimpleDateFormat,
                 java.util.Iterator,
                 java.util.Hashtable,
                 java.net.URLEncoder,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.StringUtil,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%@page import="java.util.Date"%>
<jsp:useBean id="task" scope="session" class="com.debisys.utils.TaskBean"></jsp:useBean>

<%
int section=2;
int section_page=2;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*" />
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
 <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
 <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<%
Vector warningSearchResults = new  Vector();
response.setCharacterEncoding("utf-8");
Hashtable searchErrors     = null;
String    sortString       = "";
boolean   bUseTaxValue     = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns



  		
Vector vecSearchResults = new Vector();
Vector vecResNsfCount30 = new Vector();
Vector vecResNsfCount60 = new Vector();
Vector vecResNsfCount90 = new Vector();
Vector vecNsfCount30Reg = new Vector();
Vector vecCounts = new Vector();
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String strParentRepName = "";

String startDate30 ="";
String endDate30="";
String startDate60 ="";
String endDate60="";
String startDate90 ="";
String endDate90="";
String merchant_id =request.getParameter("merchant_Id");
String nsf =request.getParameter("nsf");

 if ( request.getParameter("download") != null )
  {
  	  String sURL = "";
  	  if(nsf != null && nsf.equals("nsfcount30")){ 
  	  sURL = TransactionReport.downloadMerchantNSFDashboard(application, SessionData, merchant_id, 30);
  	  }
  	  else if(nsf != null && nsf.equals("nsfcount60")){ 
  	  sURL = TransactionReport.downloadMerchantNSFDashboard(application, SessionData, merchant_id, 60);
  	  }
  	  else if(nsf != null && nsf.equals("nsfcount90")){ 
  	  sURL = TransactionReport.downloadMerchantNSFDashboard(application, SessionData, merchant_id, 90);
  	  }
      
  	  response.sendRedirect(sURL);
  	  return;
  }

if (request.getParameter("search") != null){
	if (request.getParameter("page") != null){
		try{
			intPage=Integer.parseInt(request.getParameter("page"));
		}catch(NumberFormatException ex){
			intPage = 1;
		}
	}
	if (intPage < 1){
		intPage=1;
	}
	vecSearchResults = CustomerSearch.searchMerchant(intPage, intPageSize, SessionData, application);
	vecCounts = (Vector) vecSearchResults.get(0);
	intRecordCount = Integer.parseInt(vecCounts.get(0).toString());
	vecSearchResults.removeElementAt(0);
	if (intRecordCount>0){
		intPageCount = (intRecordCount / intPageSize);
		if ((intPageCount * intPageSize) < intRecordCount){
			intPageCount++;
		}
	}
	if (request.getParameter("repId") != null && !request.getParameter("repId").equals("")){
		strParentRepName = Merchant.getMerchantRepName(request.getParameter("repId"));
	}else{
		strParentRepName = SessionData.getProperty("company_name");
	}
}




String strmsj = request.getParameter("strmsj");

double Available_Daily_Credit=0;

if (request.getParameter("search") != null)
{
  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  vecSearchResults = Merchant.getMerchantDashBoard(SessionData,application, merchant_id);

 	
  
}
%>

<%@ include file="/includes/header.jsp" %>

<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchantsDashBoard.title",SessionData.getLanguage()).toUpperCase() + " " + strParentRepName.toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
	          <% if(strmsj != null && strmsj!="") { %>
	          
	          <td nowrap valign="top" align="center" class="main"><font color=ff0000><%=Languages.getString("jsp.admin.customers.skuglue.msgupdate",SessionData.getLanguage())%></font></td>
	          
	          <%} %>

				<tr>
		         <td>
		         <FORM ACTION="admin/customers/merchantNFS.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
		         	<INPUT TYPE=hidden NAME=search VALUE="<%=request.getParameter("search")%>">
		         	<INPUT TYPE=hidden NAME=merchant_Id VALUE="<%=request.getParameter("merchant_Id")%>">
		         	<INPUT TYPE=hidden NAME=nsf VALUE="<%=request.getParameter("nsf")%>">
		         	<INPUT TYPE=hidden NAME=download VALUE="Y">
		         	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
		         </FORM>
		         </td>
		       </tr>
              
              
            </table>

<table cellspacing="1" cellpadding="1" width="100%" border="2">
            <tbody>
  
<% 

	String DATE_FORMAT2 = "MM/dd/yyyy";
    SimpleDateFormat sdf2 = new SimpleDateFormat(DATE_FORMAT2);

if(nsf != null && nsf.equals("nsfcount30")){ 
	
	vecNsfCount30Reg = Merchant.getMerchantDashBoardNSFCountReg(SessionData,application, merchant_id,30);
	int cantreg = vecNsfCount30Reg.size();
	



	%>
	   <tr><td class=rowhead2 colspan="8" class="main">
           <%=Languages.getString("jsp.admin.reports.nsf.returns",SessionData.getLanguage())%> - <%=Languages.getString("jsp.admin.customers.merchantsDashBoard.thirtydays",SessionData.getLanguage()) %> 
			</td>
	   </tr>
       <tr>
          	<td class="rowhead2">#</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.ReturnID",SessionData.getLanguage())%>&nbsp;</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.MerchantID",SessionData.getLanguage())%>&nbsp;</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.ReturnDate",SessionData.getLanguage())%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Reason",SessionData.getLanguage())%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.CompanyName",SessionData.getLanguage())%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Amount",SessionData.getLanguage())%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Description",SessionData.getLanguage())%>&nbsp;</td>
       </tr> 
	<%if(cantreg>0){
	for(int i = 0; i<cantreg; i++){ 
	Vector nsfCount =  new Vector();
       nsfCount = (Vector)vecNsfCount30Reg.get(i);
       
       Date d = (Date)nsfCount.get(2);
       String description = nsfCount.get(6).toString();
        %>

       <tr class="row">
       	<td class="main"><%=i+1%>&nbsp;</td>
       	<td class="main"><%=nsfCount.get(0)%>&nbsp;</td>
       	<td class="main"><%=nsfCount.get(1)%>&nbsp;</td>
           <td class="main"><%=sdf2.format(d)%>&nbsp;</td>
           <td class="main"><%=nsfCount.get(3)%>&nbsp;</td>
           <td class="main"><%=nsfCount.get(4)%>&nbsp;</td>
           <td class="main"><%=NumberUtil.formatCurrency(nsfCount.get(5).toString())%>&nbsp;</td>
           <td class="main"><%=description%>&nbsp;</td>
       </tr>
       <%} 
       
    } else { 
     %>
         <tr>
       	<td class="main"><%=Languages.getString("jsp.admin.customers.merchantNFS.NoReg",SessionData.getLanguage())%></td>
       	</tr>
<%	} 

	}%>   
	
<% if(nsf != null && nsf.equals("nsfcount60")){ 
	
	vecNsfCount30Reg = Merchant.getMerchantDashBoardNSFCountReg(SessionData,application, merchant_id,60);
	int cantreg = vecNsfCount30Reg.size();
	%>
	   <tr><td class=rowhead2 colspan="8" class="main">
           <%=Languages.getString("jsp.admin.reports.nsf.returns",SessionData.getLanguage())%> - <%=Languages.getString("jsp.admin.customers.merchantsDashBoard.sixtydays",SessionData.getLanguage()) %> 
			</td>
	   </tr>
       <tr>
          	<td class="rowhead2">#</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.ReturnID",SessionData.getLanguage())%>&nbsp;</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.MerchantID",SessionData.getLanguage())%>&nbsp;</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.ReturnDate",SessionData.getLanguage())%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Reason",SessionData.getLanguage())%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.CompanyName",SessionData.getLanguage())%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Amount",SessionData.getLanguage())%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Description",SessionData.getLanguage())%>&nbsp;</td>
       </tr> 
	<%if(cantreg>0){
	for(int i = 0; i<cantreg; i++){ 
	Vector nsfCount =  new Vector();
       nsfCount = (Vector)vecNsfCount30Reg.get(i);
       Date d = (Date)nsfCount.get(2);
       String description = nsfCount.get(6).toString();
        %>

       <tr class="row">
       	<td class="main"><%=i+1%>&nbsp;</td>
       	<td class="main"><%=nsfCount.get(0)%>&nbsp;</td>
       	<td class="main"><%=nsfCount.get(1)%>&nbsp;</td>
           <td class="main"><%=sdf2.format(d)%>&nbsp;</td>
           <td class="main"><%=nsfCount.get(3)%>&nbsp;</td>
           <td class="main"><%=nsfCount.get(4)%>&nbsp;</td>
           <td class="main"><%=NumberUtil.formatCurrency(nsfCount.get(5).toString())%>&nbsp;</td>
           <td class="main"><%=description%>&nbsp;</td>
       </tr>
       <%} 
       
    } else { 
     %>
         <tr>
       	<td class="main"><%=Languages.getString("jsp.admin.customers.merchantNFS.NoReg",SessionData.getLanguage())%></td>
       	</tr>
<%	} 

	}%>   
	
<% if(nsf != null && nsf.equals("nsfcount90")){ 

	vecNsfCount30Reg = Merchant.getMerchantDashBoardNSFCountReg(SessionData,application, merchant_id,90);
	int cantreg = vecNsfCount30Reg.size();
	%>
	   <tr><td class=rowhead2 colspan="8" class="main">
           <%=Languages.getString("jsp.admin.reports.nsf.returns",SessionData.getLanguage())%> - <%=Languages.getString("jsp.admin.customers.merchantsDashBoard.ninetydays",SessionData.getLanguage()).toUpperCase() %> 
			</td>
	   </tr>	
       <tr>
          	<td class="rowhead2">#</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.ReturnID",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.MerchantID",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
          	<td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.ReturnDate",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Reason",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.CompanyName",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Amount",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
            <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchantNFS.Description",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
       </tr> 
	<%if(cantreg>0){
	for(int i = 0; i<cantreg; i++){ 
	Vector nsfCount =  new Vector();
       nsfCount = (Vector)vecNsfCount30Reg.get(i);
       nsfCount = (Vector)vecNsfCount30Reg.get(i);
       Date d = (Date)nsfCount.get(2);
       String description = nsfCount.get(6).toString();
        %>

       <tr class="row">
       	<td class="main"><%=i+1%>&nbsp;</td>
       	<td class="main"><%=nsfCount.get(0)%>&nbsp;</td>
       	<td class="main"><%=nsfCount.get(1)%>&nbsp;</td>
           <td class="main"><%=sdf2.format(d)%>&nbsp;</td>
           <td class="main"><%=nsfCount.get(3)%>&nbsp;</td>
           <td class="main"><%=nsfCount.get(4)%>&nbsp;</td>
           <td class="main"><%=NumberUtil.formatCurrency(nsfCount.get(5).toString())%>&nbsp;</td>
           <td class="main"><%=description%>&nbsp;</td>
       </tr>
       <%} 
       
    }
     else { 
     %>
         <tr>
       	<td class="main"><%=Languages.getString("jsp.admin.customers.merchantNFS.NoReg",SessionData.getLanguage())%></td>
       	</tr>
<%	} 

	}%> 
	 
            </tbody></table>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp" %>
