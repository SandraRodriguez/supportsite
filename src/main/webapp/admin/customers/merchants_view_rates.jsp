<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 java.util.*" %>
<%
int section=2;
int section_page=13;
String path = request.getContextPath();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:setProperty name="Terminal" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<table border="0" cellpadding="0" cellspacing="0" width="550">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_view_rates.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#ffffff" class="main">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
  <tr>
    <td class="formArea2">

      <table border="0" width="100%" cellpadding="0" cellspacing="0" align="left">
     	<tr>
	        <td>

				<table width="80">
						<tr>
						  <td colspan="3">
						    <a href="<%=path%>/admin/customers/merchants_info.jsp">
						     <%=Languages.getString("jsp.admin.customers.merchants_info.view_info.back",SessionData.getLanguage())%>
						    </a>
						  </td>
						</tr>
      					<tr>
      						<td class="formAreaTitle">
      							<br>
      							<%=Languages.getString("jsp.admin.customers.merchants_view_rates.terminal_rates",SessionData.getLanguage())%>
<%
	if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
	{
%>
	        						<br><br>
	        						<span class="main"><b><%=Languages.getString("jsp.admin.iso_rateplan_name",SessionData.getLanguage())%>:</b> <%=Terminal.getISORatePlanNameFromTerminal()%></span>
	        						<br><br>
<%
	}
%>
      						</td>
      					</tr>
     						<tr>
	        					<td class="formArea2">
<%
	Vector vecTerminalRates = new Vector();
	String operation = request.getParameter("top");
	String upTopSellers = request.getParameter("upTopSellers");
	String merchantId = request.getParameter("merchantId");
	String siteId = request.getParameter("siteId");
	
	Boolean manageNewModel = Boolean.valueOf(request.getParameter("model"));
	
	if (upTopSellers!=null){
	  Terminal.updateTSOrderMerchant(request);
	}
	
	if (operation!=null)
	{
		if ( manageNewModel)
		{
			vecTerminalRates = Terminal.getTerminalRates(strAccessLevel);
		}
		else
		{
			vecTerminalRates = Terminal.getTopSellersForMerchanstUpdate();
		}
	}
	else
	{
	   vecTerminalRates = Terminal.getTerminalRates(strAccessLevel);
	}
		
     %>
       <form action="<%=path%>/admin/customers/merchants_view_rates.jsp" method="post">
    
         <table width="80">
            <tr>
                <td class="rowhead2" width="25"  nowrap="nowrap" >#</td>
            	<td class="rowhead2" width="100"  nowrap="nowrap" >PRODUCT ID</td>
            	<td class="rowhead2" width="100">PRODUCT</td>
              <%
              if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
              {
               out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.customers.merchants_view_rates.total",SessionData.getLanguage()).toUpperCase()+"</td>");
              }

              if (strAccessLevel.equals(DebisysConstants.ISO))
              { 
                out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                  out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
                  out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
                }
                out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
              }
              else if (strAccessLevel.equals(DebisysConstants.AGENT))
              {
                out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
                out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
                out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
              }
              else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
              {
                out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
                out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
              }
              else if (strAccessLevel.equals(DebisysConstants.REP))
              {
                out.println("<td class=rowhead2 width=75 align=center>"+Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()+"</td>");
              }
                            
              if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_TERMINAL_RATES) && operation==null)
              {
               %>
                  <td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%></td>
               <% 
              }
              if (strAccessLevel.equals(DebisysConstants.MERCHANT) && SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION) && operation!=null)
              {
               %>
                  <td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.rateplans.tsorder",SessionData.getLanguage()).toUpperCase()%></td>
               <% 
              }                
              %>
              
            </tr>

            <%
                  Iterator it = vecTerminalRates.iterator();
                  int intEvenOdd = 1;
                  String strProductId="";
                  double dblTotalRate = 0;
                  double dblIsoRate = 0;
                  double dblAgentRate = 0;
                  double dblSubAgentRate = 0;
                  double dblRepRate = 0;
                  double dblMerchantRate = 0;
				  String strTSOrder="";
				  String terminalRate="";
				  int count=1;
				  
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    strProductId = vecTemp.get(0).toString();
                    
                    if (operation==null)
					{
                     dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
                     dblIsoRate = Double.parseDouble(vecTemp.get(3).toString());
                     dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
                     dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());
                     dblRepRate = Double.parseDouble(vecTemp.get(6).toString());
                     dblMerchantRate = Double.parseDouble(vecTemp.get(7).toString());//dblTotalRate - (dblIsoRate+dblAgentRate+dblSubAgentRate+dblRepRate);`
					}
					else if (!manageNewModel) 
					{
					  strTSOrder  = vecTemp.get(2).toString();
					  if (strTSOrder.equals("0"))
					  {
					    strTSOrder="";
					  }
					  terminalRate = vecTemp.get(3).toString();
					}
					else
					{
						strTSOrder  = vecTemp.get(8).toString();
					  	if (strTSOrder.equals("0"))
					  	{
					    	strTSOrder="";
					  	}
					}
					
					out.print("<tr class=row" + intEvenOdd +">" +
                                "<td>" +count+ "</td>");
                                 
                                
                    out.print("<td>" +strProductId+ "</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(1).toString()) + "</td>");

                    if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                    {
                      out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblTotalRate))+ "</td>");
                    }
                    
                    if (strAccessLevel.equals(DebisysConstants.ISO))
                    {
                      out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblIsoRate))+ "</td>");
                      if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                      {
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblAgentRate))+ "</td>");
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate))+ "</td>");
                      }
                      out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblRepRate))+ "</td>");
                    }
                    else if (strAccessLevel.equals(DebisysConstants.AGENT))
                    {
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblAgentRate))+ "</td>");
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate))+ "</td>");
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblRepRate))+ "</td>");
                    }
                    else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
                    {
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate))+ "</td>");
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblRepRate))+ "</td>");
                    }
                    else if (strAccessLevel.equals(DebisysConstants.REP))
                    {
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblRepRate))+ "</td>");
                    }
                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_TERMINAL_RATES) && operation==null)
                    {
                        out.print("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblMerchantRate))+ "</td>");
					}
					
					if ( strAccessLevel.equals(DebisysConstants.MERCHANT) && SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION) && operation!=null)
              		{
              			String disabledTopSeller="";
              			if (manageNewModel)
              			{
              				//Not allowed to setup TopSellers(HotProducts)
              				disabledTopSeller="disabled='disabled'";
              			}
              		  %>
              		   <td>
              		     <input type="text" name="value_<%=count%>" value="<%=strTSOrder%>" size="3" maxlength="5" onblur="return validatets(this);" <%=disabledTopSeller%> >
              		     <input type="hidden" name="terminalrate_<%=count%>" value="<%=terminalRate%>">
              		   </td>
              		  <% 
              		  //out.println("<td align=left><input type=text name=\""+terminalRate+"_" + strProductId + "\" value=\"" + strTSOrder + "\" size=3 maxlength=5 onBlur=\"return validatets(this);\">");
              		              		  
              		}
              		
                    out.println("</tr>");

                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }
                    count++;
                 }
				 count--;

                  vecTerminalRates.clear();
            %>
            
            <% 
            if (strAccessLevel.equals(DebisysConstants.MERCHANT) && SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION) && operation!=null)
            {
            	if (!manageNewModel)
            	{
	              	%>
	                <tr>
	                  <td align="center" colspan="3">
	                    <input type="submit" name="upTopSellers" value="<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.update_terminal",SessionData.getLanguage())%>">
	                    <input type="hidden" name="top" value="<%=operation%>">
	                    <input type="hidden" name="merchantId" value="<%=merchantId%>">
	                    <input type="hidden" name="siteId" value="<%=siteId%>">
	                    <input type="hidden" name="countProducts" value="<%=count%>">
	                  </td>
	                </tr>
	            	<%
            	}
            }
            %>
        </table>
       
       </form>
       
       </td>
	 </tr>

</table>



</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<script type="text/javascript">
	
	function validatets(c)
    {
    	if (isNaN(c.value))
    	{
    	    c.value="";
    		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
    		c.focus();
    		return (false);

    	}
    	else
    	{
    		if (c.value < 0)
    		{
                c.value="";
		    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
		    	c.focus();
		    	return (false);
			}
			if (c.value > <%=count%>)
    		{   
    		    c.value="";
		    	alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
		    	c.focus();
		    	return (false);
			}
		}
    }
</script>

<%@ include file="/includes/footer.jsp" %>
