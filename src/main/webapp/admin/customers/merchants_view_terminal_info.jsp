<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 java.util.*" %>
<%
	int section      = 2;
	int section_page = 4;
	String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:setProperty name="Terminal" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	int intTabIndex = 1;
	String strMessage = request.getParameter("message");
	Vector vecTerminalInfo = new Vector();
	if (strMessage == null)
	{
		strMessage="";
	}
	if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
	{
    	vecTerminalInfo = Terminal.getTerminalInfoMx();
        if (vecTerminalInfo != null && vecTerminalInfo.size() > 0)
		{
        	Terminal.setAnnualInsuranceFee(vecTerminalInfo.get(2).toString());
            Terminal.setAnnualInsuranceTax(vecTerminalInfo.get(3).toString());
            Terminal.setAnnualMaintenanceFee(vecTerminalInfo.get(4).toString());
            Terminal.setAnnualMaintenanceTax(vecTerminalInfo.get(5).toString());
            Terminal.setReconnectionFee(vecTerminalInfo.get(6).toString());
            Terminal.setReconnectionTax(vecTerminalInfo.get(7).toString());
            Terminal.setAnnualAffiliationFee(vecTerminalInfo.get(8).toString());
            Terminal.setAnnualAffiliationTax(vecTerminalInfo.get(9).toString());
            /* BEGIN MiniRelease 9.0 - Added by YH */
            if(vecTerminalInfo.get(10) != null )
            {
	        	Terminal.setSerialNumber(vecTerminalInfo.get(10).toString());
            }
            else
            {
            	Terminal.setSerialNumber("");
            }
            if(vecTerminalInfo.get(11) != null )
            {
            	Terminal.setSimData(vecTerminalInfo.get(11).toString());
            }
            else
            {
            	Terminal.setSimData("");
            }
            if(vecTerminalInfo.get(12) != null )
            {
            	Terminal.setImciData(vecTerminalInfo.get(12).toString());
            }
            else
            {
            	Terminal.setImciData("");
            }
            /* END MiniRelease 9.0 - Added by YH */
		}
        else
        {
			vecTerminalInfo = Terminal.getTerminalInfo();
			if (vecTerminalInfo != null && vecTerminalInfo.size() > 0)
 			{
   				Terminal.setMonthlyFee(vecTerminalInfo.get(2).toString());
	 		}
		}
	}
    else
    {
		vecTerminalInfo = Terminal.getTerminalInfo();
        /* BEGIN MiniRelease 9.0 - Added by YH */
        if(vecTerminalInfo.get(3) != null )
        {
        	Terminal.setSerialNumber(vecTerminalInfo.get(3).toString());
        }
        else
        {
        	Terminal.setSerialNumber("");
        }
        if(vecTerminalInfo.get(4) != null )
        {
        	Terminal.setSimData(vecTerminalInfo.get(4).toString());
        }
        else
        {
        	Terminal.setSimData("");
        }
        if(vecTerminalInfo.get(5) != null )
        {
        	Terminal.setImciData(vecTerminalInfo.get(5).toString());
        }
        else
        {
        	Terminal.setImciData("");
        }
        /* END MiniRelease 9.0 - Added by YH */
        if (vecTerminalInfo != null && vecTerminalInfo.size() > 0)
        {
        	Terminal.setMonthlyFee(vecTerminalInfo.get(2).toString());
        }
	}
%>
<%@ include file="/includes/header.jsp" %>
	<table border="0" cellpadding="0" cellspacing="0" width="750">
		<tr>
	    	<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.terminal_information",SessionData.getLanguage()).toUpperCase()%></td>
    		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  		</tr>
  		<tr>
  			<td colspan="3" bgcolor="#ffffff" class="main">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
	  				<tr>
    					<td class="formArea2">
      						<table border="0" width="100%" cellpadding="0" cellspacing="0" align="left">
     						<tr>
	        					<td>
									<table width="100%">
      									<tr>
      										<td class="main"><br><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.terminal_information",SessionData.getLanguage())%></td>
      									</tr>
     									<tr>
	        								<td class="formArea2">
											<br>
												<table width="100">
													<%
														if (!customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
														{
													%>
															<tr class=main><td nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.monthly_fee",SessionData.getLanguage())%></td><td nowrap><input type="text" name="monthlyFee" value="<%=Terminal.getMonthlyFee()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td></tr>
													<%
														}
													%>
													<tr class=main>
														<td nowrap colspan=2>
    														<table class=main>
        														<tr>
            														<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.serialnumber",SessionData.getLanguage())%></td>
														            <td><input type="text" name="serialNumber" value="<%=Terminal.getSerialNumber()%>" readonly size="16" maxlength="25" tabIndex="<%=intTabIndex++%>"></td>
														        </tr>
														        <tr>
														            <td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.sim",SessionData.getLanguage())%></td>
														            <td><input type="text" name="simData" value="<%=Terminal.getSimData()%>" readonly size="16" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
														        </tr>
														        <tr>
														            <td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.imci",SessionData.getLanguage())%></td>
														            <td><input type="text" name="imciData" value="<%=Terminal.getImciData()%>" readonly size="16" maxlength="20" tabIndex="<%=intTabIndex++%>"></td>
														        </tr>
    														</table>
														</td>
													</tr>
													<%
														if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
														{
													%>
															<tr class=main>
																<td nowrap colspan=2>
    																<table class=main>	
    																	<tr>
    																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.additonal_fee",SessionData.getLanguage())%></td>
    																	    <td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.fee_title",SessionData.getLanguage())%></td>
    																	    <td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.tax_title",SessionData.getLanguage())%></td>
    																	</tr>
    																	<tr>
    																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.annual_insurance",SessionData.getLanguage())%></td>
    																	    <td><input type="text" name="annualInsuranceFee" value="<%=Terminal.getAnnualInsuranceFee()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
    																	    <td><input type="text" name="annualInsuranceTax" value="<%=Terminal.getAnnualInsuranceTax()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
    																	</tr>
    																	<tr>
    																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.annual_maintenance",SessionData.getLanguage())%></td>
    																	    <td><input type="text" name="annualMaintenanceFee" value="<%=Terminal.getAnnualMaintenanceFee()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
    																	    <td><input type="text" name="annualMaintenanceTax" value="<%=Terminal.getAnnualMaintenanceTax()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
    																	</tr>
    																	<tr>
    																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.reconnection",SessionData.getLanguage())%></td>
    																	    <td><input type="text" name="reconnectionFee" value="<%=Terminal.getReconnectionFee()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
    																	    <td><input type="text" name="reconnectionTax" value="<%=Terminal.getReconnectionTax()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
    																	</tr>
    																	<tr>
    																	    <td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.annual_affiliation",SessionData.getLanguage())%></td>
    																	    <td><input type="text" name="annualAffiliationFee" value="<%=Terminal.getAnnualAffiliationFee()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
    																	    <td><input type="text" name="annualAffiliationTax" value="<%=Terminal.getAnnualAffiliationTax()%>" readonly size="8" maxlength="15" tabIndex="<%=intTabIndex++%>"></td>
    																	</tr>
    																</table>
																</td>
															</tr>
													<%
														}
	  												%>
	  												<tr class=main>
  														<td nowrap><b><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.softwareversion",SessionData.getLanguage())%></b></td>
  														<td><%=Terminal.getSoftwareVersion()%></td>
  													</tr>
													<%
														if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
      													{ 
															if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
															{
													%>
  															<tr class=main>
  																<td nowrap><b><%=Languages.getString("jsp.admin.iso_rateplan_name",SessionData.getLanguage())%></b></td>
  																<td><%=Terminal.getISORatePlanNameFromTerminal()%></td>
  															</tr>
													<%
															}
      													}
													%>
												</table>
												<br>
												<%
  													Vector vecTerminalRates = new Vector();
  													vecTerminalRates = Terminal.getTerminalRates(strAccessLevel);
												%>
												<%
													if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
      												{ 
												%>												
	        									<table width="100">
            										<tr>
            											<td class=rowhead2 width=100 nowrap><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage()).toUpperCase()%></td>
            											<td class=rowhead2 width=100><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
              											<td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.total",SessionData.getLanguage()).toUpperCase()%></td>
              											<%
              												if (strAccessLevel.equals(DebisysConstants.ISO))
              												{ 
              											%>              
                												<td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%></td>
                												<%
                													if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                													{ 
                												%>
                														<td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%></td>
                														<td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%></td>
                												<%
                													}
              												}
              											%>
              											<td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%></td>
              											<td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%></td>
            										</tr>
            										<%
                  										Iterator it = vecTerminalRates.iterator();
                  										int intEvenOdd = 1;
                										String strProductId="";
                										double dblTotalRate = 0;
                										double dblIsoRate = 0;
                										double dblAgentRate = 0;
                										double dblSubAgentRate = 0;
                										double dblRepRate = 0;
                										double dblMerchantRate = 0;
                										while (it.hasNext())
                    									{
                    										Vector vecTemp = null;
                    										vecTemp = (Vector) it.next();
                    										strProductId = vecTemp.get(0).toString();
                    										dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
                      										dblIsoRate = Double.parseDouble(vecTemp.get(3).toString());
                      										dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
                      										dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());
                      										dblRepRate = Double.parseDouble(vecTemp.get(6).toString());
                      										//dblMerchantRate = dblTotalRate - (dblIsoRate + dblAgentRate + dblSubAgentRate + dblRepRate);
                      										dblMerchantRate = Double.parseDouble(vecTemp.get(7).toString());

//    	                									if(strAccessLevel.equals(DebisysConstants.REP)
//        	                									||strAccessLevel.equals(DebisysConstants.AGENT)
//            	            									||strAccessLevel.equals(DebisysConstants.SUBAGENT))
//                	    									{
//                    	  										dblTotalRate = dblTotalRate - (dblIsoRate + dblAgentRate + dblSubAgentRate);
//                    										}
                    										out.print("<tr class=row" + intEvenOdd +">" +
                                										"<td><input type=hidden name=productId value=" + strProductId + ">" +strProductId+ "</td>" +
                               											"<td nowrap>" + HTMLEncoder.encode(vecTemp.get(1).toString()) + "</td>" +
                                										"<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"total_"+strProductId+"\" value=\""+NumberUtil.formatAmount(Double.toString(dblTotalRate))+"\" readonly size=3></td>");
                    										if (strAccessLevel.equals(DebisysConstants.ISO))
                    										{                                   
                        										out.print("<td align=left><input type=text name=\"iso_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblIsoRate)) + "\" readonly size=3 maxlength=5 tabIndex=\""+ intTabIndex++ +"\">");
                        										out.print("</td>");
                        										if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	                        									{
    	                      										out.print("<td align=left><input type=text name=\"agent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblAgentRate)) + "\" readonly size=3 maxlength=5 tabIndex=\""+ intTabIndex++ +"\">");
        	                  										out.print("</td>");
            	              										out.print("<td align=left><input type=text name=\"subagent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate)) + "\" readonly size=3 maxlength=5 tabIndex=\""+ intTabIndex++ +"\">");
                	          										out.print("</td>");
                    	    									}
                    										}
                    										out.print("<td align=left><input type=text name=\"rep_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblRepRate)) + "\" readonly size=3 maxlength=5 tabIndex=\""+ intTabIndex++ +"\">");
                    										out.print("</td>");
                    										out.print("<td align=left><input style=\"color:#");
                   											if((dblMerchantRate < 0) || dblMerchantRate > dblTotalRate)
                    										{
                      											out.print("ff0000");
                    										}
                    										else
	                    									{	
    	                  										out.print("0000FF");
        	           										}
            	        									out.println(";background:#C0C0C0;\" type=text name=\"remaining_"+strProductId+"\" value=\""+NumberUtil.formatAmount(Double.toString(dblMerchantRate))+"\" readonly size=3>");
                	    									out.print("</td></tr>");
                    										if (intEvenOdd == 1)
                    										{
                      											intEvenOdd = 2;
                                	                        } 
                    										else 
                    										{
                      											intEvenOdd = 1;
                   											}
                  										}
                  										vecTerminalRates.clear();
            										%>
            									</table>
												<%
      												}
												%>            									
					          				</td>
					      				</tr>
      									<tr>
      										<td class="formAreaTitle" align="center"><input type=button value="<%=Languages.getString("jsp.admin.customers.merchants_info.view_info.back",SessionData.getLanguage())%>" onClick="history.go(-1)" tabIndex="<%=intTabIndex++%>"></td>
      									</tr>
									</table>
								</td>
							</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%@ include file="/includes/footer.jsp" %>