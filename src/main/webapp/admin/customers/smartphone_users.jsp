<%@ page import="java.util.*,com.debisys.properties.Properties,
         com.debisys.languages.*,
         com.debisys.utils.LogChanges,
         com.debisys.reports.PropertiesByFeature" %>
<%@page import="com.debisys.utils.StringUtil"%>
<%@page import="com.debisys.terminals.TerminalRegistration"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="ValidateEmail" class="com.debisys.utils.ValidateEmail" scope="page"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
<jsp:setProperty name="Terminal" property="*"/>
<%
    int section = 2;
    int section_page = 11;
    LogChanges.setSession(SessionData);
    LogChanges.setContext(application);
%>
<%@ include file="/includes/security.jsp" %>
<script type="text/javascript" src="/support/js/terminal.js"></script>
<script type="text/javascript" src="/support/js/jquery.js"></script>
<script>
    $( document ).ready(function() {        
        $("#chkManualRegCode").hide();        
        $("#txtRegistration").attr('readonly', true);
    });
    
    function buildRandomRegistrationCode() {  
        $.post("terminals/updateNokiaInfo.jsp?Random" + Math.random(), 
            { 
                siteId: "<%=request.getParameter("siteId")%>",                    
                regCode: true, 
                IdTerminalRegistration: 0
            }, function(data) {                
                var value = data.trim();
                $("#txtRegistration").val(value);                                                
        });		
    }
</script>

<html>
    <head>
        <link href="../../default.css" type="text/css" rel="stylesheet">
        <script>
            function SetFrameSize() {
                self.parent.SetFrameSize(document.getElementById("oBody").scrollWidth, document.getElementById("oBody").scrollHeight);
            }
            function SetMinSize() {
                self.parent.SetFrameSize(1250, 101);
            }
        </script>
    </head>
    <body id="oBody" style="background-color: #fff !important">
        <%    Hashtable<String, String> terminalErrors = new Hashtable<String, String>();
            if (request.getParameter("save") != null) {
                if (!Terminal.isRegistrationCodeUnique(StringUtil.toString(request.getParameter("edit")))) {
                    terminalErrors.put("registrationCode", Languages.getString("jsp.admin.customers.terminal.invalidregistrationcode", SessionData.getLanguage()));
                }

                if (Terminal.existRegistrationCode(application, SessionData, request)) {
                    terminalErrors.put("registrationCodeAlreadyExist", Languages.getString("jsp.admin.customers.terminal.alreadyexistregcode", SessionData.getLanguage()));
                }

                if (!Terminal.isUserIDUnique(StringUtil.toString(request.getParameter("edit")))) {
                    terminalErrors.put("userIdUnique", Languages.getString("jsp.admin.customers.terminal.uniqueuserid", SessionData.getLanguage()));
                }                
                if (!Terminal.validateRegCodeSendOptions()) {
                    terminalErrors.put("sendOptionRequired", Languages.getString("jsp.admin.customers.terminal.enforce_userRegCodeSendOptions", SessionData.getLanguage()));
                }

                if (Terminal.isChkRegCode() && Terminal.isChkConfirmationByEmail() && !ValidateEmail.isValidEmail(Terminal.getTxtConfirmationEMail())) {
                    terminalErrors.put("confirmationMailSyntaxError", Languages.getString("jsp.admin.customers.terminal.wrongSyntax_inputConfirmationEmail", SessionData.getLanguage()));
                }

                String phoneNumber = Terminal.getTxtConfirmationPhoneNumber();
                if (Terminal.isChkRegCode() && Terminal.isChkConfirmationBySMS() && phoneNumber != null && !phoneNumber.matches("\\d{9,14}")) {
                    terminalErrors.put("confirmationSmsSyntaxError", Languages.getString("jsp.admin.customers.terminal.wrongSyntax_inputConfirmationSms", SessionData.getLanguage()));
                }
                
                if (terminalErrors.size() == 0) {
                    if (request.getParameter("save").length() > 0) {
                        Terminal.updatePCTerminalRegistration(application, SessionData, request);                                               
                        response.sendRedirect("/support/admin/customers/smartphone_users.jsp?siteId=" + Terminal.getSiteId() + "&terminalVersionForPcTerminal=" + request.getParameter("terminalVersionForPcTerminal"));
                    } else {
                        Terminal.addPCTerminalRegistration(application, SessionData, request);                       
                    }
                    terminalErrors = Terminal.getErrors();
                }
            }

            if (StringUtil.toString(request.getParameter("add")).length() > 0 || StringUtil.toString(request.getParameter("edit")).length() > 0 || terminalErrors.size() > 0) {
                if (terminalErrors.size() > 0) {
                    out.println("<table><tr class=main><td><font color=ff0000>" + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");
                    Enumeration<String> enum1 = terminalErrors.keys();
                    while (enum1.hasMoreElements()) {
                        String strKey = enum1.nextElement().toString();
                        String strError = (String) terminalErrors.get(strKey);
                        if (strError != null && !strError.equals("")) {
                            out.println("<li>" + strError + "</li>");
                        }
                    }
                    out.println("</font></td></tr></table>");
                }
                
                if (StringUtil.toString(request.getParameter("add")).length() > 0) {
                    out.print("<script>buildRandomRegistrationCode();</script>");
                }                

                Vector vResults = null;
                if (request.getParameter("edit") != null) {
                    vResults = (Vector) Terminal.getPCTerminalRegistrations(StringUtil.toString(request.getParameter("edit")), application).get(0);
                    if (request.getParameter("save") == null) {
                        Terminal.setTxtRegistration(vResults.get(2).toString());
                        Terminal.setPcTermUsername(vResults.get(5).toString());
                        boolean isAdmin = (vResults.get(38).toString().equalsIgnoreCase("1") ? true : false);
                        Terminal.setChkIsAdmin(isAdmin);
                    }
                } 
        %>
        <script>
            var error = false;
            var error_message = "";
            function check_input(field_name, field_size, message) {
                if (document.getElementById(field_name) && (document.getElementById(field_name).type != "hidden")) {
                    var field_value = document.getElementById(field_name).value;
                    if (field_value == '' || field_value.length < field_size) {
                        error_message = error_message + "* " + message + "\n";
                        error = true;
                    }
                }
            }
            function check_form() {
                error = false;
                error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2", SessionData.getLanguage())%>";
                check_input("pcTermBrand", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_brand", SessionData.getLanguage())%>");
                check_input("pcTermUsername", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_username", SessionData.getLanguage())%>");
                check_input("pcTermPassword", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_password", SessionData.getLanguage())%>");
            <%
            if (request.getParameter("edit") == null) {
            %>
                    if (document.getElementById("chkManualRegCode").checked) {
                    check_input("txtRegistration", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_regcode", SessionData.getLanguage())%>");
                }
            <%
            }
            %>
                    if (document.getElementById("chkIPRestriction").checked) {
                        check_input("txtIPAddress", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_ipaddress", SessionData.getLanguage())%>");
                        check_input("txtSubnetMask", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_subnetmask", SessionData.getLanguage())%>");
                }
                    if (document.getElementById("chkAllowCertificate").checked) {
                    check_input("txtAllowCertificate", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_certamount", SessionData.getLanguage())%>");
                }
                if (error == true) {
                    alert(error_message);
                    return false;
                } else {
                    var ctl = document.getElementById("btnSubmit");
                    if (ctl != null) {
                        ctl.disabled = true;
                    }
                    ctl = document.getElementById("btnCancel");
                    if (ctl != null) {
                        ctl.disabled = true;
                    }
                    return true;
                }
            }
        </script>
        <form action="/support/admin/customers/smartphone_users.jsp" method="post" onsubmit="return check_form(this);">
            <table width="100%">
                
                <tr class="main">
                    <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl2", SessionData.getLanguage())%></td>
                    <td></td>    
                    <td nowrap="nowrap"><input type="text" ID="pcTermUsername" name="pcTermUsername" value="<%=Terminal.getPcTermUsername()%>" size="20" maxlength="15"></td>                                        
                </tr>
                <%
                    if (request.getParameter("edit") != null) {
                %>                          
                <!-- DANGER __ NO_DELETE -->
                <input type="hidden" name="chkManualRegCode" value="true">              
                <%
                } else {
                %>
                <!-- DANGER __ NO_DELETE -->
                <input type="checkbox" id="chkManualRegCode" checked="true" name="chkManualRegCode">                                                                
                <%                    
                    }
                %>
                <tr class="main">
                    <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl3", SessionData.getLanguage())%></td>
                    <td></td>
                    <td>
                        <input type="text" id="txtRegistration" name="txtRegistration" <%=(request.getParameter("edit") != null) ? "enabled" : "enabled"%> value="<%=Terminal.getTxtRegistration()%>" size="20" maxlength="15">
                        <input type="hidden" id="CurrentTxtRegistration" name="CurrentTxtRegistration" value="<%=Terminal.getTxtRegistration()%>">                        
                        <input type="button" onclick="buildRandomRegistrationCode()" value="<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl7", SessionData.getLanguage())%>">
                    </td>
                </tr>       
                
                <tr class="main">
                    <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl6", SessionData.getLanguage())%></td>
                    <td></td>
                    <td>
                        <input type="checkbox" id="chkIsAdmin" name="chkIsAdmin">
                    </td>
                </tr>                                
                
            </table>
            <%
                if (Terminal.isChkRegCode()) {
                    out.print("<script>document.getElementById('chkRegCode').click();</script>");
                }                
                if (Terminal.isChkIsAdmin()) {                    
                    out.print("<script>document.getElementById('chkIsAdmin').click();</script>");
                }
                
                String terminalVersionForPcTerminal = "";
                terminalVersionForPcTerminal = SessionData.getProperty("terminal_type");
                if (SessionData.getUser().isQcommBusiness() && (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) && strAccessLevel.equals(DebisysConstants.ISO)) {
            %>
            <input type="hidden" name="terminalClass" value="QComm">
            <%
                }
                if (!(SessionData.getUser().isQcommBusiness() && (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) && strAccessLevel.equals(DebisysConstants.ISO))) {
            %>
            <input type="hidden" name="terminalClass" value="Debisys">    
            <%
                }
            %>
            <input type="hidden" id="current_term_version" name="current_term_version" value="<%=terminalVersionForPcTerminal%>">
            <br><br>
            <input type="hidden" name="save" value="<%=StringUtil.toString(request.getParameter("edit"))%>">
            <input type="hidden" name="siteId" value="<%=Terminal.getSiteId()%>">
            <%
                if (StringUtil.toString(request.getParameter("edit")).length() == 0) {
            %>
            <input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl5", SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
            <%
            } else {
            %>
            <input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.customers.terminal.save_pc_term", SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;
            <input type="hidden" name="edit" value="<%=StringUtil.toString(request.getParameter("edit"))%>">
            <%
                }
            %>
        </form>
        <form action="/support/admin/customers/smartphone_users.jsp" method="post">
            <input type="hidden" name="siteId" value="<%=Terminal.getSiteId()%>">
            <input type="hidden" name="terminalVersionForPcTerminal" value="<%=request.getParameter("terminalVersionForPcTerminal")%>">
            <input id="btnCancel" type="submit" value="<%=Languages.getString("jsp.admin.cancel", SessionData.getLanguage())%>">
        </form>
        <%
        } else {
            Vector vResults = Terminal.getPCTerminalRegistrations("", application);
            if (vResults.size() > 0) {
        %>
        <table>
            <tr class="rowhead2">
                <td nowrap="nowrap">ID</td>                
                <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl2", SessionData.getLanguage()).toUpperCase()%></td>                
                <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl3", SessionData.getLanguage()).toUpperCase()%></td>
                <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl6", SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <%
                Iterator it = vResults.iterator();
                int nEvenOdd = 1;
                while (it.hasNext()) {
                    Vector vTmp = (Vector) it.next();
            %>
            <tr class="row<%=nEvenOdd%>">
                <td nowrap="nowrap"><a href="/support/admin/customers/smartphone_users.jsp?edit=<%=vTmp.get(0)%>&siteId=<%=Terminal.getSiteId()%>&terminalVersionForPcTerminal=<%=request.getParameter("terminalVersionForPcTerminal")%>"><%=vTmp.get(0)%></a></td>                
                <td nowrap="nowrap"><%=vTmp.get(5)%></td>                
                <td nowrap="nowrap"><%=vTmp.get(2)%></td>            
                <td nowrap="nowrap" align="center"><%=(vTmp.get(38).toString().equalsIgnoreCase("1") ? "YES" : "NO")%></td>
            </tr>
            <%
                    vTmp = null;
                    nEvenOdd = (nEvenOdd == 1) ? 2 : 1;
                }
                it = null;
            %>
        </table>
        <%
        } else {
        %>
        <table><tr><td class="main"><%=Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage())%></td></tr></table>
                <%
                    }
                    vResults = null;
                %>
        <br>
        <br>
        <form action="/support/admin/customers/smartphone_users.jsp?terminalVersionForPcTerminal=<%=request.getParameter("terminalVersionForPcTerminal")%>" method="POST">
            <input type="hidden" name="add" value="0">
            <input type="hidden" name="siteId" value="<%=Terminal.getSiteId()%>">
            <input type="submit" value="<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl5", SessionData.getLanguage())%>">
        </form>
        <%
            }
        %>        
    </body>
</html>