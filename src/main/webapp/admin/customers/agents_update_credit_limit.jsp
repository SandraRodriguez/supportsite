<%@ page import="com.debisys.utils.NumberUtil,
                 java.util.Hashtable"%>
<%
int section=12;
int section_page=4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="page"/>
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%

  if (NumberUtil.isNumeric(Rep.getCreditLimit()))
  {
    if (Rep.getAssignedCreditAgent(SessionData) > (Double.parseDouble(Rep.getCreditLimit())))
    {
      response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId() + "&message=8");
      return;
    }
    else if (Double.parseDouble(Rep.getCreditLimit()) < 0)
    {
      response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId() + "&message=6");
      return;
    }
    else
    {
            Rep.updateAgentLiabilityLimit(SessionData, application);
            response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId());
            return;
    }
  }
  else
  {
    response.sendRedirect("agents_info.jsp?repId=" + Rep.getRepId() + "&message=6");
    return;
  }

%>