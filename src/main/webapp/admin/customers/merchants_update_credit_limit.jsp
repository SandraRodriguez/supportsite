<%@ page import="com.debisys.utils.NumberUtil,
                 java.util.Hashtable"%>
<%
int section=2;
int section_page=6;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="page"/>
<jsp:setProperty name="Merchant" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
  if (NumberUtil.isNumeric(Merchant.getCreditLimit()))
  {
    if (Double.parseDouble(Merchant.getCreditLimit()) < 0)
    {
      response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=4");
      return;
    }
    else
    {
      //credit based merchant update
      //need to check if rep has enough credit
      //and update rep running liability
      if (Merchant.checkRepCreditLimit(SessionData, application))
      {
        Merchant.updateLiabilityLimit2(SessionData, application);
        response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId());
        return;
      }
      else
      {
        response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=10");
        return;
      }
    }
  }
  else
  {
    response.sendRedirect("merchants_info.jsp?merchantId=" + Merchant.getMerchantId() + "&message=3");
    return;
  }
%>