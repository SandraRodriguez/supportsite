<%@page import="com.debisys.languages.Languages"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Vector"%>
<%@page import="com.debisys.utils.NumberUtil"%>
<%
  response.setCharacterEncoding("iso-8859-1");

  int section      = 2;
  int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request" />
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request"/>
<jsp:setProperty name="Merchant" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
	if ( request.getParameter("action").equals("save") )
	{
		Merchant.saveTerminalOperationsTime(request.getParameter("day"), request.getParameter("chk").equals("1"), Integer.parseInt(request.getParameter("starth")), Integer.parseInt(request.getParameter("startm")), Integer.parseInt(request.getParameter("endh")), Integer.parseInt(request.getParameter("endm")));
		return;
	}

	Hashtable<String, Vector<Integer>> htData = Merchant.getTerminalOperationsTime();
%>
<table class="main">
	<tr><td colspan="13" align="left"><%=SessionData.getString("jsp.admin.terminalops.warning")%><br/><br/></td></tr>
	<tr>
		<td><b><%=Languages.getString("jsp.admin.terminalops.Monday",SessionData.getLanguage())%></b></td><td>&nbsp;</td>
		<td><b><%=Languages.getString("jsp.admin.terminalops.Tuesday",SessionData.getLanguage())%></b></td><td>&nbsp;</td>
		<td><b><%=Languages.getString("jsp.admin.terminalops.Wednesday",SessionData.getLanguage())%></b></td><td>&nbsp;</td>
		<td><b><%=Languages.getString("jsp.admin.terminalops.Thursday",SessionData.getLanguage())%></b></td><td>&nbsp;</td>
		<td><b><%=Languages.getString("jsp.admin.terminalops.Friday",SessionData.getLanguage())%></b></td><td>&nbsp;</td>
		<td><b><%=Languages.getString("jsp.admin.terminalops.Saturday",SessionData.getLanguage())%></b></td><td>&nbsp;</td>
		<td><b><%=Languages.getString("jsp.admin.terminalops.Sunday",SessionData.getLanguage())%></b></td>
	</tr>
	<tr>
		<td>
			<label><input type="checkbox" id="chkTerminalOpsMon" name="chkTerminalOpsMon" onclick="ToggleTerminalOps('Mon');" oldValue="<%=htData.get("MO").get(0).intValue()%>" <%=(htData.get("MO").get(0).intValue() == 1)?"checked":""%>><%=Languages.getString("jsp.admin.terminalops.enable",SessionData.getLanguage())%></label>
			<br/><br/>
			<table class="main">
				<tr><td></td><td><i><%=Languages.getString("jsp.admin.terminalops.hour",SessionData.getLanguage())%></i></td><td><i><%=Languages.getString("jsp.admin.terminalops.minute",SessionData.getLanguage())%></i></td></tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.start",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsStartHMon" name="ddlTerminalOpsStartHMon">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("MO").get(1).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsStartMMon" name="ddlTerminalOpsStartMMon">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("MO").get(2).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("MO").get(2).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.end",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsEndHMon" name="ddlTerminalOpsEndHMon">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("MO").get(3).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsEndMMon" name="ddlTerminalOpsEndMMon">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("MO").get(4).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("MO").get(4).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
			</table>
		</td><td></td>
		<td>
			<label><input type="checkbox" id="chkTerminalOpsTue" name="chkTerminalOpsTue" onclick="ToggleTerminalOps('Tue');" oldValue="<%=htData.get("TU").get(0).intValue()%>" <%=(htData.get("TU").get(0).intValue() == 1)?"checked":""%>><%=Languages.getString("jsp.admin.terminalops.enable",SessionData.getLanguage())%></label>
			<br/><br/>
			<table class="main">
				<tr><td></td><td><i><%=Languages.getString("jsp.admin.terminalops.hour",SessionData.getLanguage())%></i></td><td><i><%=Languages.getString("jsp.admin.terminalops.minute",SessionData.getLanguage())%></i></td></tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.start",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsStartHTue" name="ddlTerminalOpsStartHTue">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("TU").get(1).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsStartMTue" name="ddlTerminalOpsStartMTue">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("TU").get(2).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("TU").get(2).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.end",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsEndHTue" name="ddlTerminalOpsEndHTue">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("TU").get(3).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsEndMTue" name="ddlTerminalOpsEndMTue">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("TU").get(4).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("TU").get(4).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
			</table>
		</td><td></td>
		<td>
			<label><input type="checkbox" id="chkTerminalOpsWed" name="chkTerminalOpsWed" onclick="ToggleTerminalOps('Wed');" oldValue="<%=htData.get("WE").get(0).intValue()%>" <%=(htData.get("WE").get(0).intValue() == 1)?"checked":""%>><%=Languages.getString("jsp.admin.terminalops.enable",SessionData.getLanguage())%></label>
			<br/><br/>
			<table class="main">
				<tr><td></td><td><i><%=Languages.getString("jsp.admin.terminalops.hour",SessionData.getLanguage())%></i></td><td><i><%=Languages.getString("jsp.admin.terminalops.minute",SessionData.getLanguage())%></i></td></tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.start",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsStartHWed" name="ddlTerminalOpsStartHWed">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("WE").get(1).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsStartMWed" name="ddlTerminalOpsStartMWed">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("WE").get(2).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("WE").get(2).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.end",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsEndHWed" name="ddlTerminalOpsEndHWed">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("WE").get(3).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsEndMWed" name="ddlTerminalOpsEndMWed">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("WE").get(4).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("WE").get(4).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
			</table>
		</td><td></td>
		<td>
			<label><input type="checkbox" id="chkTerminalOpsThu" name="chkTerminalOpsThu" onclick="ToggleTerminalOps('Thu');" oldValue="<%=htData.get("TH").get(0).intValue()%>" <%=(htData.get("TH").get(0).intValue() == 1)?"checked":""%>><%=Languages.getString("jsp.admin.terminalops.enable",SessionData.getLanguage())%></label>
			<br/><br/>
			<table class="main">
				<tr><td></td><td><i><%=Languages.getString("jsp.admin.terminalops.hour",SessionData.getLanguage())%></i></td><td><i><%=Languages.getString("jsp.admin.terminalops.minute",SessionData.getLanguage())%></i></td></tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.start",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsStartHThu" name="ddlTerminalOpsStartHThu">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("TH").get(1).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsStartMThu" name="ddlTerminalOpsStartMThu">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("TH").get(2).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("TH").get(2).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.end",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsEndHThu" name="ddlTerminalOpsEndHThu">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("TH").get(3).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsEndMThu" name="ddlTerminalOpsEndMThu">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("TH").get(4).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("TH").get(4).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
			</table>
		</td><td></td>
		<td>
			<label><input type="checkbox" id="chkTerminalOpsFri" name="chkTerminalOpsFri" onclick="ToggleTerminalOps('Fri');" oldValue="<%=htData.get("FR").get(0).intValue()%>" <%=(htData.get("FR").get(0).intValue() == 1)?"checked":""%>><%=Languages.getString("jsp.admin.terminalops.enable",SessionData.getLanguage())%></label>
			<br/><br/>
			<table class="main">
				<tr><td></td><td><i><%=Languages.getString("jsp.admin.terminalops.hour",SessionData.getLanguage())%></i></td><td><i><%=Languages.getString("jsp.admin.terminalops.minute",SessionData.getLanguage())%></i></td></tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.start",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsStartHFri" name="ddlTerminalOpsStartHFri">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("FR").get(1).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsStartMFri" name="ddlTerminalOpsStartMFri">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("FR").get(2).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("FR").get(2).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.end",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsEndHFri" name="ddlTerminalOpsEndHFri">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("FR").get(3).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsEndMFri" name="ddlTerminalOpsEndMFri">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("FR").get(4).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("FR").get(4).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
			</table>
		</td><td></td>
		<td>
			<label><input type="checkbox" id="chkTerminalOpsSat" name="chkTerminalOpsSat" onclick="ToggleTerminalOps('Sat');" oldValue="<%=htData.get("SA").get(0).intValue()%>" <%=(htData.get("SA").get(0).intValue() == 1)?"checked":""%>><%=Languages.getString("jsp.admin.terminalops.enable",SessionData.getLanguage())%></label>
			<br/><br/>
			<table class="main">
				<tr><td></td><td><i><%=Languages.getString("jsp.admin.terminalops.hour",SessionData.getLanguage())%></i></td><td><i><%=Languages.getString("jsp.admin.terminalops.minute",SessionData.getLanguage())%></i></td></tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.start",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsStartHSat" name="ddlTerminalOpsStartHSat">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("SA").get(1).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsStartMSat" name="ddlTerminalOpsStartMSat">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("SA").get(2).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("SA").get(2).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.end",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsEndHSat" name="ddlTerminalOpsEndHSat">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("SA").get(3).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsEndMSat" name="ddlTerminalOpsEndMSat">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("SA").get(4).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("SA").get(4).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
			</table>
		</td><td></td>
		<td>
			<label><input type="checkbox" id="chkTerminalOpsSun" name="chkTerminalOpsSun" onclick="ToggleTerminalOps('Sun');" oldValue="<%=htData.get("SU").get(0).intValue()%>" <%=(htData.get("SU").get(0).intValue() == 1)?"checked":""%>><%=Languages.getString("jsp.admin.terminalops.enable",SessionData.getLanguage())%></label>
			<br/><br/>
			<table class="main">
				<tr><td></td><td><i><%=Languages.getString("jsp.admin.terminalops.hour",SessionData.getLanguage())%></i></td><td><i><%=Languages.getString("jsp.admin.terminalops.minute",SessionData.getLanguage())%></i></td></tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.start",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsStartHSun" name="ddlTerminalOpsStartHSun">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("SU").get(1).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsStartMSun" name="ddlTerminalOpsStartMSun">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("SU").get(2).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("SU").get(2).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
				<tr><td><%=Languages.getString("jsp.admin.terminalops.end",SessionData.getLanguage())%></td>
					<td><select id="ddlTerminalOpsEndHSun" name="ddlTerminalOpsEndHSun">
<%
	for (int i = 0; i < 24; i++)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("SU").get(3).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
%>
					</select></td>
					<td><select id="ddlTerminalOpsEndMSun" name="ddlTerminalOpsEndMSun">
<%
	for (int i = 0; i < 46; i += 15)
	{
		out.print("<option value=\"" + i + "\" " + (htData.get("SU").get(4).intValue() == i?"selected":"") + ">" + String.format("%02d", new Integer(i)) + "</option>");
	}
	out.print("<option value=\"59\" " + (htData.get("SU").get(4).intValue() == 59?"selected":"") + ">59</option>");
%>
					</select></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="13" align="center"><input type="button" id="btnSaveTerminalOps" value="<%=Languages.getString("jsp.admin.terminalops.save",SessionData.getLanguage())%>" onclick="DoSaveTerminalOps()"></td></tr>
</table>
<script>
	function ToggleTerminalOps(sDay)
	{
		var bCheck = document.getElementById("chkTerminalOps" + sDay).checked;
		document.getElementById("ddlTerminalOpsStartH" + sDay).disabled = !bCheck;
		document.getElementById("ddlTerminalOpsStartM" + sDay).disabled = !bCheck;
		document.getElementById("ddlTerminalOpsEndH" + sDay).disabled = !bCheck;
		document.getElementById("ddlTerminalOpsEndM" + sDay).disabled = !bCheck;
	}

	function DoSaveTerminalOps()
	{
		$.post("admin/customers/terminalops.jsp?action=save&merchantId=<%=Merchant.getMerchantId()%>&day=MO" + 
			"&chk=" + (document.getElementById("chkTerminalOpsMon").checked?1:0) + 
			"&starth=" + document.getElementById("ddlTerminalOpsStartHMon").value + 
			"&startm=" + document.getElementById("ddlTerminalOpsStartMMon").value + 
			"&endh=" + document.getElementById("ddlTerminalOpsEndHMon").value + 
			"&endm=" + document.getElementById("ddlTerminalOpsEndMMon").value + 
			"&Random=" + Math.random(), ProcessSave);
		$.post("admin/customers/terminalops.jsp?action=save&merchantId=<%=Merchant.getMerchantId()%>&day=TU" + 
			"&chk=" + (document.getElementById("chkTerminalOpsTue").checked?1:0) + 
			"&starth=" + document.getElementById("ddlTerminalOpsStartHTue").value + 
			"&startm=" + document.getElementById("ddlTerminalOpsStartMTue").value + 
			"&endh=" + document.getElementById("ddlTerminalOpsEndHTue").value + 
			"&endm=" + document.getElementById("ddlTerminalOpsEndMTue").value + 
			"&Random=" + Math.random(), ProcessSave);
		$.post("admin/customers/terminalops.jsp?action=save&merchantId=<%=Merchant.getMerchantId()%>&day=WE" + 
			"&chk=" + (document.getElementById("chkTerminalOpsWed").checked?1:0) + 
			"&starth=" + document.getElementById("ddlTerminalOpsStartHWed").value + 
			"&startm=" + document.getElementById("ddlTerminalOpsStartMWed").value + 
			"&endh=" + document.getElementById("ddlTerminalOpsEndHWed").value + 
			"&endm=" + document.getElementById("ddlTerminalOpsEndMWed").value + 
			"&Random=" + Math.random(), ProcessSave);
		$.post("admin/customers/terminalops.jsp?action=save&merchantId=<%=Merchant.getMerchantId()%>&day=TH" + 
			"&chk=" + (document.getElementById("chkTerminalOpsThu").checked?1:0) + 
			"&starth=" + document.getElementById("ddlTerminalOpsStartHThu").value + 
			"&startm=" + document.getElementById("ddlTerminalOpsStartMThu").value + 
			"&endh=" + document.getElementById("ddlTerminalOpsEndHThu").value + 
			"&endm=" + document.getElementById("ddlTerminalOpsEndMThu").value + 
			"&Random=" + Math.random(), ProcessSave);
		$.post("admin/customers/terminalops.jsp?action=save&merchantId=<%=Merchant.getMerchantId()%>&day=FR" + 
			"&chk=" + (document.getElementById("chkTerminalOpsFri").checked?1:0) + 
			"&starth=" + document.getElementById("ddlTerminalOpsStartHFri").value + 
			"&startm=" + document.getElementById("ddlTerminalOpsStartMFri").value + 
			"&endh=" + document.getElementById("ddlTerminalOpsEndHFri").value + 
			"&endm=" + document.getElementById("ddlTerminalOpsEndMFri").value + 
			"&Random=" + Math.random(), ProcessSave);
		$.post("admin/customers/terminalops.jsp?action=save&merchantId=<%=Merchant.getMerchantId()%>&day=SA" + 
			"&chk=" + (document.getElementById("chkTerminalOpsSat").checked?1:0) + 
			"&starth=" + document.getElementById("ddlTerminalOpsStartHSat").value + 
			"&startm=" + document.getElementById("ddlTerminalOpsStartMSat").value + 
			"&endh=" + document.getElementById("ddlTerminalOpsEndHSat").value + 
			"&endm=" + document.getElementById("ddlTerminalOpsEndMSat").value + 
			"&Random=" + Math.random(), ProcessSave);
		$.post("admin/customers/terminalops.jsp?action=save&merchantId=<%=Merchant.getMerchantId()%>&day=SU" + 
			"&chk=" + (document.getElementById("chkTerminalOpsSun").checked?1:0) + 
			"&starth=" + document.getElementById("ddlTerminalOpsStartHSun").value + 
			"&startm=" + document.getElementById("ddlTerminalOpsStartMSun").value + 
			"&endh=" + document.getElementById("ddlTerminalOpsEndHSun").value + 
			"&endm=" + document.getElementById("ddlTerminalOpsEndMSun").value + 
			"&Random=" + Math.random(), ProcessSave);
		window.setTimeout("alert('<%=Languages.getString("jsp.admin.terminalops.savemsg",SessionData.getLanguage())%>')", 1000);
	}

	function ProcessSave(sData, sStatus) {}

	$(document).ready( function() 
	{
		ToggleTerminalOps("Mon");
		ToggleTerminalOps("Tue");
		ToggleTerminalOps("Wed");
		ToggleTerminalOps("Thu");
		ToggleTerminalOps("Fri");
		ToggleTerminalOps("Sat");
		ToggleTerminalOps("Sun");
	} );
</script>
