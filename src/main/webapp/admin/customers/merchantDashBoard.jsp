<%@ page import="java.util.Vector,java.util.Calendar,java.text.SimpleDateFormat,java.util.Iterator,java.util.Hashtable,java.net.URLEncoder,com.debisys.utils.NumberUtil,com.debisys.utils.HTMLEncoder,com.debisys.utils.DbUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
	int section = 2;
	int section_page = 2;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="SKUGlueSearch" class="com.debisys.customers.SkuGlueSearch" scope="request"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
 <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
 <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

<script language="javascript"> 

function openPopUp(address, fullscreen, toolbar, location, status, menubar, scrollbars, resizable, width, height, left, top, replace){ 
     var opt = "fullscreen=" + fullscreen + 
                 ",toolbar=" + toolbar + 
                 ",location=" + location + 
                 ",status=" + status + 
                 ",menubar=" + menubar + 
                 ",scrollbars=" + scrollbars + 
                 ",resizable=" + resizable + 
                 ",width=" + width + 
                 ",height=" + height + 
                 ",left=" + left + 
                 ",top=" + top; 
     var ventana = window.open(address,"venta",opt,replace); 

}                     
//-->     
</script> 



<%
	Vector warningSearchResults = new Vector();
	response.setCharacterEncoding("utf-8");
	Hashtable searchErrors = null;
	String sortString = "";
	boolean bUseTaxValue = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns

	Vector vecSearchResults = new Vector();
	Vector vecResNsfCount30 = new Vector();
	Vector vecResNsfCount60 = new Vector();
	Vector vecResNsfCount90 = new Vector();
	Vector vecNsfCount30Reg = new Vector();
	Vector vecCounts = new Vector();
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;
	String strParentRepName = "";
	String dblTotalQty30=NumberUtil.formatCurrency("0");
	String dblTotalSales30 =NumberUtil.formatCurrency("0");
	String dblTotalQty60 = NumberUtil.formatCurrency("0");
	String dblTotalSales60 = NumberUtil.formatCurrency("0");
	String dblTotalQty90 = NumberUtil.formatCurrency("0");
	String dblTotalSales90 = NumberUtil.formatCurrency("0");

	String startDate30 = "";
	String endDate30 = "";
	String startDate60 = "";
	String endDate60 = "";
	String startDate90 = "";
	String endDate90 = "";
	
	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));

	if (request.getParameter("search") != null) {
		if (request.getParameter("page") != null) {
			try {
				intPage = Integer
						.parseInt(request.getParameter("page"));
			} catch (NumberFormatException ex) {
				intPage = 1;
			}
		}
		if (intPage < 1) {
			intPage = 1;
		}
		
		if (request.getParameter("repId") != null
				&& !request.getParameter("repId").equals("")) {
			strParentRepName = Merchant.getMerchantRepName(request
					.getParameter("repId"));
		} else {
			strParentRepName = SessionData.getProperty("company_name");
		}
	}

	String merchant_id = request.getParameter("merchant_Id");
	String nsf = request.getParameter("nsf");

	String strmsj = request.getParameter("strmsj");

	double Available_Daily_Credit = 0;

	if (request.getParameter("search") != null) {
		if (request.getParameter("page") != null) {
			try {
				intPage = Integer
						.parseInt(request.getParameter("page"));
			} catch (NumberFormatException ex) {
				intPage = 1;
			}
		}

		if (intPage < 1) {
			intPage = 1;
		}

		vecSearchResults = Merchant.getMerchantDashBoard(SessionData,
				application, merchant_id);
		vecResNsfCount30 = Merchant.getMerchantDashBoardNSFCount(
				SessionData, application, merchant_id, 30);
		vecResNsfCount60 = Merchant.getMerchantDashBoardNSFCount(
				SessionData, application, merchant_id, 60);
		vecResNsfCount90 = Merchant.getMerchantDashBoardNSFCount(
				SessionData, application, merchant_id, 90);
				

		/*****************************************************************************************/
		/*NSF Sales 30 days*/
		Vector<Vector<String>> vecResNsfSales30 = new Vector<Vector<String>>();

		String DATE_FORMAT = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Calendar hoy = Calendar.getInstance();
		endDate30 = sdf.format(hoy.getTime());
		hoy.add(hoy.DAY_OF_MONTH, -30);
		startDate30 = sdf.format(hoy.getTime());

		TransactionReport.setStartDate(startDate30);
		TransactionReport.setEndDate(endDate30);

		if (TransactionReport.validateDateRange(SessionData)) {
			String strMerchantIds[] = request
					.getParameterValues("mids");
			if (strMerchantIds != null) {
				TransactionReport.setMerchantIds(strMerchantIds);
			}
			if (DebisysConfigListener.getCustomConfigType(application)
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
					&& (request.getParameter("chkUseTaxValue") != null)) {
				bUseTaxValue = true;
				vecResNsfSales30 = TransactionReport
						.getMerchantNSFVolumeMx(SessionData,
								application, merchant_id);
			} else {
				vecResNsfSales30 = TransactionReport
						.getMerchantNSFVolume(SessionData, application,
								merchant_id);
				warningSearchResults = TransactionReport
						.getWarningMessage();
			}
		} else {
			searchErrors = TransactionReport.getErrors();
		}

		Iterator<Vector<String>> iter30 = vecResNsfSales30.iterator();
		Vector<String> vecTemp30 = null;

		while (iter30.hasNext()) {
			vecTemp30 = iter30.next();
			if (vecTemp30.get(1).equals(merchant_id)) {
				dblTotalQty30 = NumberUtil.formatCurrency(vecTemp30.get(2));
				dblTotalSales30 = NumberUtil.formatCurrency(vecTemp30.get(3));
				break;
			}
		}
		/*END NSFSales 30 days*/
		/*************/

		/*NSF Sales 60 days*/
		Vector<Vector<String>> vecResNsfSales60 = new Vector<Vector<String>>();

		hoy = Calendar.getInstance();
		endDate60 = sdf.format(hoy.getTime());
		hoy.add(hoy.DAY_OF_MONTH, -60);
		startDate60 = sdf.format(hoy.getTime());

		TransactionReport.setStartDate(startDate60);
		TransactionReport.setEndDate(endDate60);

		if (TransactionReport.validateDateRange(SessionData)) {
			String strMerchantIds[] = request
					.getParameterValues("mids");
			if (strMerchantIds != null) {
				TransactionReport.setMerchantIds(strMerchantIds);
			}
			if (DebisysConfigListener.getCustomConfigType(application)
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
					&& (request.getParameter("chkUseTaxValue") != null)) {
				bUseTaxValue = true;
				vecResNsfSales60 = TransactionReport
						.getMerchantNSFVolumeMx(SessionData,
								application, merchant_id);
			} else {
				vecResNsfSales60 = TransactionReport
						.getMerchantNSFVolume(SessionData, application,
								merchant_id);
				warningSearchResults = TransactionReport
						.getWarningMessage();
			}
		} else {
			searchErrors = TransactionReport.getErrors();
		}


		Iterator<Vector<String>> iter60 = vecResNsfSales60.iterator();
		Vector<String> vecTemp60 = null;

		while (iter60.hasNext()) {
			vecTemp60 = null;
			vecTemp60 = iter60.next();
			if (vecTemp60.get(1).equals(merchant_id)) {
				dblTotalQty60 = NumberUtil.formatCurrency(vecTemp60.get(2));
				dblTotalSales60 = NumberUtil.formatCurrency(vecTemp60.get(3));
				break;
			}
		}

		/*END NSF Sales 60 days*/

		/*NSF Sales 90 days*/
		
		Vector<Vector<String>> vecResNsfSales90 = new Vector<Vector<String>>();

		hoy = Calendar.getInstance();
		endDate90 = sdf.format(hoy.getTime());
		hoy.add(hoy.DAY_OF_MONTH, -90);
		startDate90 = sdf.format(hoy.getTime());

		TransactionReport.setStartDate(startDate90);
		TransactionReport.setEndDate(endDate90);

		if (TransactionReport.validateDateRange(SessionData)) {
			String strMerchantIds[] = request
					.getParameterValues("mids");
			if (strMerchantIds != null) {
				TransactionReport.setMerchantIds(strMerchantIds);
			}
			if (DebisysConfigListener.getCustomConfigType(application)
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
					&& (request.getParameter("chkUseTaxValue") != null)) {
				bUseTaxValue = true;
				vecResNsfSales90 = TransactionReport
						.getMerchantNSFVolumeMx(SessionData,
								application, merchant_id);
			} else {
				vecResNsfSales90 = TransactionReport
						.getMerchantNSFVolume(SessionData, application,
								merchant_id);
				warningSearchResults = TransactionReport
						.getWarningMessage();
			}
		} else {
			searchErrors = TransactionReport.getErrors();
		}

		Iterator<Vector<String>> iter90 = vecResNsfSales90.iterator();
		Vector<String> vecTemp90 = null;
		
		while (iter90.hasNext()) {
			
			vecTemp90 = iter90.next();
			if (vecTemp90.get(1).equals(merchant_id)) {
				dblTotalQty90 = NumberUtil.formatCurrency(vecTemp90.get(2));
				dblTotalSales90 = NumberUtil.formatCurrency(vecTemp90.get(3));
				break;
			}
		}


		/*END NSF Sales 90 days*/

	}
%>

<%@ include file="/includes/header.jsp" %>

<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages
					.getString("jsp.admin.customers.merchantsDashBoard.title",SessionData.getLanguage()).toUpperCase()
					+ " " + strParentRepName.toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="0" cellspacing="0" class="formArea"> 
     	<tr>
	        <td >
	          <table width="300">
	          <%
	          	if (strmsj != null && strmsj != "") {
	          %>
	          
	          <td nowrap valign="top" align="center" class="main"><font color=ff0000></font></td>
	          
	          <%
	          	          	}
	          	          %>
           

              <%
                         	if (SessionData.checkPermission(DebisysConstants.PERM_MERCHANTS)) {
                         %>
              <tr>
              <td colspan=3 align=left>
                 
              </td>
              </tr>
              <%
              	}
              %>
              
            </table>
			<% if (vecSearchResults.size()>0){ %>
						<table class="main"> 
							<tbody>
						
								 <tr>
									<td nowrap="" colspan="1" >
										<%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%>
										<br>
										<br>
										<%=Languages.getString("jsp.admin.customers.merchantsDashBoard.msgNSF",SessionData.getLanguage())%>&nbsp;
										<br>
										<br>
									</td>
								</tr>

						</tbody>
						</table>
						<table>
						<tbody>		
											<tr>
												<td nowrap="" valign="top" class="formAreaTitle2">
													<%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.merchantinfo",SessionData.getLanguage())%></td>
												<td>
													&nbsp;
												</td>
											</tr>
											<!-- <tr class="formArea" >-->
												<tr bgcolor ="#ffffff" class="main">
													<table bgcolor="#ffffff"
														 width="98%" > 
														<tr >
															<td nowrap="" class="formAreaTitle2"><%=Languages
					.getString("jsp.admin.customers.merchantsDashBoard.dba",SessionData.getLanguage())%></td>
															<td class="main">
																<label><%=vecSearchResults.get(1)%></label>
															</td>
														</tr>
														<tr >
															<td nowrap="" class="formAreaTitle2"><%=Languages
					.getString("jsp.admin.customers.merchantsDashBoard.businessname",SessionData.getLanguage())%></td>
															<td class="main">
																<label><%=vecSearchResults.get(20)%></label>
															</td>
														</tr>
														<tr >
															<td nowrap="" class="formAreaTitle2"><%=Languages
					.getString("jsp.admin.customers.merchantsDashBoard.merchantid",SessionData.getLanguage())%></td>
															<td class="main">
																<label><%=vecSearchResults.get(0)%></label>
															</td>
														</tr>
														<tr >
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.repname",SessionData.getLanguage())%></td>
															<td class="main">
																<label><%=vecSearchResults.get(3)%></label>
															</td>
														</tr>
														<tr >
															<td nowrap="" class="formAreaTitle2"><%=Languages
					.getString("jsp.admin.customers.merchantsDashBoard.repid",SessionData.getLanguage())%></td>
															<td class="main">
																<label><%=vecSearchResults.get(2)%></label>
															</td>
														</tr>
														<tr>
															<td nowrap=""  class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.merchantcredittype",SessionData.getLanguage())%></td>
															<td class="main">
																<label><%=vecSearchResults.get(5)%></label>
															</td>
														</tr>
														<%if(!vecSearchResults.get(4).equals("3")){ %>
														<%
															if (vecSearchResults.get(6) != null
																	&& vecSearchResults.get(6).equals("DAILY")) {
														%>
														<tr>
															<td nowrap=""  class="formAreaTitle2"><%=Languages
								.getString("jsp.admin.customers.merchantsDashBoard.merchantcreditlimit",SessionData.getLanguage())%></td>
															<td class="main">
																<label>
																	DAILY
																</label>
															</td>
														</tr>
														<%
															} else {
														%>
														<tr>
															<td nowrap=""  class="formAreaTitle2"><%=Languages
								.getString("jsp.admin.customers.merchantsDashBoard.merchantcreditlimit",SessionData.getLanguage())%></td>
															<td class="main">
																<label>
																	NA
																</label>
															</td>
														</tr>
														<%
															}
														}else{	
														%>
														<tr>
															<td nowrap=""  class="formAreaTitle2"><%=Languages
								.getString("jsp.admin.customers.merchantsDashBoard.merchantcreditlimit",SessionData.getLanguage())%></td>
															<td class="main">
																<label>
																	NA
																</label>
															</td>
														</tr>
														<%} %>
														
														<%if(!vecSearchResults.get(4).equals("3") && !vecSearchResults.get(4).equals("1")){ %>
														<tr>
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.creditlimitamount",SessionData.getLanguage())%></td>
															<td class="main">
																<label class="main"><%= NumberUtil.formatCurrency(vecSearchResults.get(15).toString())%></label>
															</td>
														</tr>
														<%}else{ %>
														<tr>
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.creditlimitamount",SessionData.getLanguage())%></td>
															<td class="main">
																<label class="main">NA</label>
															</td>
														</tr>														
														<%} %>
														<%if(!vecSearchResults.get(4).equals("3")){ %>
														<tr >
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.currentavailablecredit",SessionData.getLanguage())%></td>
															<td class="main">
																<label><%=NumberUtil.formatCurrency(vecSearchResults.get(19).toString())%></label>
														</tr>
														<%}else{ %>
														<tr >
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.currentavailablecredit",SessionData.getLanguage())%></td>
															<td class="main">
																<label>NA</label>
														</tr>
														<%} %>
														<%if(!vecSearchResults.get(4).equals("3")){ %>
														<tr>
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.dailysaleslimit",SessionData.getLanguage())%></td>
															<td class="main">
																<label>
																<% //if(vecSearchResults.get(4).equals("1")) {%>
																<%
																if (vecSearchResults.get(6) != null
																	&& vecSearchResults.get(6).equals("DAILY")) {
																%>
																  <%=NumberUtil.formatCurrency(vecSearchResults.get(16).toString())%>
																<% }else{%>
																 NA
																<%} %>
																
																
																</label>
															</td>	
														</tr>
														<%}else{ %>
														<tr>
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.dailysaleslimit",SessionData.getLanguage())%></td>
															<td class="main">
																<label>NA</label>
															</td>	
														</tr>
														<%} %>
														<%if(!vecSearchResults.get(4).equals("3")){ %>
														<tr>
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.availabledailycredit",SessionData.getLanguage())%></td>
															<td class="main">
																<label >
																<% //if(vecSearchResults.get(4).equals("1")) {%>
																<%
																if (vecSearchResults.get(6) != null
																	&& vecSearchResults.get(6).equals("DAILY")) {
																%>
																 <%=NumberUtil.formatCurrency(vecSearchResults.get(18).toString())%>
																<% }else{%>
																NA
																<%} %>
																</label>
															</td>
															
														</tr>
														<%}else{ %>
														<tr>
															<td nowrap="" class="formAreaTitle2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.availabledailycredit",SessionData.getLanguage())%></td>
															<td class="main">
																<label >
																NA
																</label>
															</td>
															
														</tr>
														<%} %>
														<tr>
														<td width="50"></td>
														</tr>

													</table>

												</tr>
												<tr>
												<td>
													<table width="100%" cellspacing="1" cellpadding="1"
														border="2">
														<br>
														<!--<tr>
															<td class=rowhead2 colspan=8>
																<%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.terminals",SessionData.getLanguage())%>
															</td>
														</tr>  -->
														<tr class=rowhead2>
															<td class=rowhead2>
																&nbsp;&nbsp;&nbsp;
															</td>
															<td align="right"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.thirtydays",SessionData.getLanguage())%>&nbsp;
															</td>
															<td align="right"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.sixtydays",SessionData.getLanguage())%>&nbsp;
															</td>
															<td align="right"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.ninetydays",SessionData.getLanguage())%>&nbsp;
															</td>
														</tr>
														<!-- <form method="post"  action="admin/customers/skuGlue_update_status.jsp">-->
															<div align="center">
																<tr>
																	<td class="rowhead2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.nsfcount",SessionData.getLanguage()).toUpperCase()%>&nbsp;
																	</td>
																	<td class="main" align="right"><% if(vecSearchResults.get(4).equals("1")) {%>NA <% } else {%>
																		<a style="text-decoration: underline;" target="1"  
																		href="/support/admin/customers/merchantNFS.jsp?search=y&merchant_Id=<%=merchant_id%>&nsf=nsfcount30">  
																			<%=vecResNsfCount30.get(1)%> 
																		 </a>
																		<%} %>&nbsp;
																	</td>
																	<td class="main" align="right" ><% if(vecSearchResults.get(4).equals("1")) {%>NA <% } else {%>
																		<a style="text-decoration: underline;" target="2"  
																		href="/support/admin/customers/merchantNFS.jsp?search=y&merchant_Id=<%=merchant_id%>&nsf=nsfcount60">
																		<%=vecResNsfCount60.get(1)%>
																		</a>
																		<%} %>&nbsp;
																	</td>
																	<td class="main" align="right"><% if(vecSearchResults.get(4).equals("1")) {%>NA <% } else {%>
																	   <a style="text-decoration: underline;" target="3" 
																			href="/support/admin/customers/merchantNFS.jsp?search=y&merchant_Id=<%=merchant_id%>&nsf=nsfcount90">
																		<%=vecResNsfCount90.get(1)%>
																		</a>
																		<%} %>&nbsp;
																	</td>
																</tr>
																<tr>
																	<td class="rowhead2"><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.nsfamount",SessionData.getLanguage()).toUpperCase()%>&nbsp;
																	</td>
																	<td class="main" align="right"><% if(vecSearchResults.get(4).equals("1")) {%>NA <% } else {%><%=NumberUtil.formatCurrency(vecResNsfCount30.get(2).toString())%><%} %>&nbsp;
																	</td>
																	<td class="main" align="right"><% if(vecSearchResults.get(4).equals("1")) {%>NA <% } else {%><%=NumberUtil.formatCurrency(vecResNsfCount60.get(2).toString())%><%} %>&nbsp;
																	</td>
																	<td class="main" align="right"><% if(vecSearchResults.get(4).equals("1")) {%>NA <% } else {%><%=NumberUtil.formatCurrency(vecResNsfCount90.get(2).toString())%><%} %>&nbsp;
																	</td>
																</tr>
																<!-- <form name="mainform" method="post" action="admin/reports/transactions/merchants_summary.jsp" onSubmit="scroll();"> -->
																<tr>
																	<td class=rowhead2><%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.salesvolume",SessionData.getLanguage()).toUpperCase()%>&nbsp;
																	</td>
																	<td class="main" align="right">
																	<a style="text-decoration: underline;" target="4"  
																		href="/support/admin/reports/transactions/merchants_summary.jsp?search=y&mids=<%=merchant_id%>&startDate=<%=startDate30%>&endDate=<%=endDate30%>">
																		<%=dblTotalSales30%>
																		</a>&nbsp;
																	</td>
																	<td class="main" align="right">
																	<a style="text-decoration: underline;" target="5" 
																		href="/support/admin/reports/transactions/merchants_summary.jsp?search=y&mids=<%=merchant_id%>&startDate=<%=startDate60%>&endDate=<%=endDate60%>">
																		<%=dblTotalSales60%>
																		</a>&nbsp;
																	</td>
																	<td class="main" align="right">
																	<a style="text-decoration: underline;" target="6"  
																		href="/support/admin/reports/transactions/merchants_summary.jsp?search=y&mids=<%=merchant_id%>&startDate=<%=startDate90%>&endDate=<%=endDate90%>">
																		<%=dblTotalSales90%>
																		</a>&nbsp;
																	</td>
																	<!-- admin/reports/transactions/merchants_summary.jsp -->

																</tr>
																<!--</form>  -->
															</div>
														<!-- </form> -->
													</table>
													<br />
													
													</table>
												</td>
											</tr>													

													
								<% }else {	%>
						<table class="main"> 
							<tbody>
						
								 <tr>
									<td nowrap="" colspan="1" >
										<%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%>
										<br>
										<br>
									</td>
								</tr>

						</tbody>
						</table>
						<table>
						<tbody>		
											<tr>
												<td nowrap="" valign="top" class="formAreaTitle">
													<%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.merchantinfo",SessionData.getLanguage())%></td>
												<td>
													&nbsp;
												</td>
											</tr>
											<tr>
												<td nowrap="" valign="top" class="main">
													<%=Languages
							.getString("jsp.admin.customers.merchantsDashBoard.merchantinfonotexist",SessionData.getLanguage())%></td>
												<td>
													&nbsp;
												</td>
											</tr>
						</tbody>
						</table>		
							
								
								<%} %>	
								</td>
</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp" %>
