<%@page import="com.debisys.utils.*,
				com.debisys.terminals.Terminal,
				com.debisys.properties.Properties,
				java.util.*,java.util.ArrayList,
				com.debisys.terminalTracking.*,
				com.debisys.customers.Merchant,
				com.debisys.users.User,com.debisys.terminals.JavaHandSet,
				com.debisys.terminals.CarriersMobile,
				com.debisys.pincache.PcAssignmentTemplate" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="TerminalVersions" class="com.debisys.terminals.TerminalVersions" scope="request"/>
<jsp:useBean id="ValidateEmail" class="com.debisys.utils.ValidateEmail" scope="page"/>
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
<jsp:setProperty name="Terminal" property="*"/>
<jsp:setProperty name="RatePlan" property="*"/>
<jsp:setProperty name="Merchant" property="*"/>
<%
int section = 2;
int section_page = 11;
LogChanges.setSession(SessionData);
LogChanges.setContext(application);
String ratePlanIDValue = RatePlan.getRatePlanId(request.getParameter("siteId"));
/*
 * Alfred A. - Added dummy strings for the sms and web service names before initialization because the sms
 * window would appear always when the jsp page first loaded.
 */
String sPCTerminalID = "";
String sSMSTerminalID = "SMST1";
String sSMS2TerminalID = "SMST2";
String sSMS3TerminalID = "SMST3";
String sSMS4TerminalID="SMST4";
String sSMS5TerminalID="SMST5";
String sSMS6TerminalID="SMST6";
String sMicroBrowserID = "";
String sInfopymeKioskUSSD = "";
String sWebServiceTerminalID="WebTest1";
String sWebService2TerminalID="WebTest2";
String sWebService3TerminalID="WebTest3";

String hiddenTrMarketPlaceRow = "hidden";
String disableControlsMB = "";
boolean asignoTerminal = false;
Vector<Vector<Vector<String>>> vISORatePlan = new Vector<Vector<Vector<String>>>();
Vector vRepRatePlan = new Vector();
Vector vMPTerminalTypes = new Vector();

String sAccessLevel = SessionData.getProperty("access_level");
String strRefId = SessionData.getProperty("ref_id");
String typeRatePlan=""; 
vMPTerminalTypes = com.debisys.terminals.Terminal.getMarketplaceTerminalType();

boolean permissionToTerminalMappingsAndAccessLevel = SessionData.checkPermission(DebisysConstants.PERM_SETUP_TERMINAL_MAPPING) && 
														sAccessLevel.equals(DebisysConstants.ISO);
														
boolean hasPermissionManage_New_Rateplans = SessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS);

//if ( request.getParameter("getRates") != null ){// TODO: check if the terminal is disabled

//}else{
	typeRatePlan="ISO";
//}
%>
<%@ include file="/includes/security.jsp" %>
<%
Vector vecTerminal = com.debisys.terminals.Terminal.getTerminalTypes_Combo("Debisys");
Iterator itTerminal = vecTerminal.iterator();
	
while (itTerminal.hasNext()){
	Vector vecTemp = null;
	vecTemp = (Vector) itTerminal.next();
	String strTerminalTypeId = vecTemp.get(0).toString();
	String strTerminalTypeDesc = vecTemp.get(1).toString();
		
	if ( strTerminalTypeDesc.equalsIgnoreCase("Microbrowser")){
		sMicroBrowserID = strTerminalTypeId;
	}
	if ( strTerminalTypeDesc.equalsIgnoreCase("PC Terminal") ){
		sPCTerminalID = strTerminalTypeId;
	}
	//David Castro added for SMS terminal creation
	if ( strTerminalTypeDesc.equalsIgnoreCase("SMS Phone (merchant)") ){
		sSMSTerminalID = strTerminalTypeId;
	}
	if ( strTerminalTypeDesc.equalsIgnoreCase("SMS Phone (subscriber)") ){
		sSMS2TerminalID = strTerminalTypeId;
	}
	if ( strTerminalTypeDesc.equalsIgnoreCase("Web Service") ){
		sWebServiceTerminalID = strTerminalTypeId;
	}
        if ( strTerminalTypeDesc.equalsIgnoreCase("Infopyme Kiosk USSD") ){
		sInfopymeKioskUSSD = strTerminalTypeId;
	}
	
	//Alfred A. - Added two new kiosk terminals that act as if they were webservice or sms terminals - DBSY-938
	if ( strTerminalTypeDesc.equalsIgnoreCase("Kiosk (USSD/SMS)") ){
		sSMS3TerminalID = strTerminalTypeId;
	}
	if ( strTerminalTypeDesc.equalsIgnoreCase("Kiosk (Web Service)") ){
		sWebService2TerminalID = strTerminalTypeId;
	}
	
	if ( strTerminalTypeDesc.equalsIgnoreCase("Tablet") ){
		sSMS4TerminalID = strTerminalTypeId;
	}
	if ( strTerminalTypeDesc.equalsIgnoreCase("SMS Printer") ){
		sSMS5TerminalID = strTerminalTypeId;
	}
	if ( strTerminalTypeDesc.equalsIgnoreCase("MMSPOS (Web Service)") ){
		sWebService3TerminalID = strTerminalTypeId;
	}
	if ( strTerminalTypeDesc.equalsIgnoreCase("Quick Sell App") ){
		sSMS6TerminalID = strTerminalTypeId;
	}
}

String strMessage = request.getParameter("message");
if (strMessage == null){
	strMessage="";
}
Hashtable terminalErrors = null;
Hashtable<String, Vector<String>> hashRepRates = new Hashtable<String, Vector<String>>();
Hashtable hashTSOrders = new Hashtable();
boolean terminalAdded = false;
String submitted = request.getParameter("submitted");
String terminalVersionForPcTerminal="";
boolean isEditingTerminal = false;

// Terminal information was submitted
if ( submitted!= null){
	//DBSY-814- jacuna
	if(  DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_EDIT_SALES_FORCE_ID) ){
		String strSalesForceID = (String)request.getParameter("sfid");
		Terminal.setProviderTerminalID(strSalesForceID);
	}
	terminalVersionForPcTerminal = request.getParameter("termVersion_Combo");
	try{
		if ( submitted.equals("y") && (Terminal.getSiteId().length() == 0) )
		{
				Terminal.captureActorsRates(request, strAccessLevel, hashTSOrders, strDistChainType, hashRepRates,false);
				if(( com.debisys.terminals.Terminal.IsTerminalInPhysicalList(application, StringUtil.toString(request.getParameter("terminalType")))) 
						&& (SessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)) 
						&& StringUtil.toString(request.getParameter("physicalTerminalId")).length() > 0)
				{
					CPhysicalTerminal terminalFisico = new CPhysicalTerminal();
					terminalFisico.loadTerminal(Long.parseLong(request.getParameter("physicalTerminalId")));
					Terminal.setTerminalFisico(terminalFisico);
				}
				//David Castro set terminal Group and Key for MarketPlace
				if (Terminal.isMarketPlaceTerminal(request.getParameter( "terminalType"))==true){
					Terminal.setMarketPlaceGroup(new Integer(request.getParameter("ApplicationGroup")));
					Terminal.setMarketPlaceKey(new Integer(request.getParameter("licenseKey")));
				}
				if ( Terminal.getChkRatePlan().equals("REP") ){
					Terminal.setIsoRatePlanId("");
				}else if ( Terminal.getChkRatePlan().equals("ISO") ){
					Terminal.setRepRatePlanId("");
				}else{
					Terminal.setIsoRatePlanId("");
					Terminal.setRepRatePlanId("");
				}
				if ( Terminal.validateAddTerminal(SessionData, hashRepRates, application, request) )
				{
					//JFM Se agrega el siguiente if para que la asignacion de valores se haga solo para despliegues en mexico, y el tipo sea microbrowser
					if(com.debisys.terminals.Terminal.IsTerminalInPhysicalList(application, StringUtil.toString(request.getParameter("terminalType")))
							&& (SessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)))
					{
						if((StringUtil.toString(Terminal.getSerialNumber()).equals("")) && (Terminal.getTerminalFisico() != null)){
							Terminal.setSerialNumber(Terminal.getTerminalFisico().getSerialNumber());
						}
					}
					String cachedReceiptsValue = request.getParameter("cachedReceipts");
					if(cachedReceiptsValue != null ){
						Terminal.setEnbCachedRcp(cachedReceiptsValue.equals("on")?true:false);
					}
					String termTypeUI="";
					if ((request.getParameter("termVersion_Combo") != null) && ((!request.getParameter("termVersion_Combo").equals(""))))
					{
						//only if is PCTERMINAL
						termTypeUI = request.getParameter("termVersion_Combo");
						Terminal.setTermVersion(termTypeUI);
					}
					else
					{
						//(sPCTerminalID != "") && 
						termTypeUI = request.getParameter("termVersion");
					}
					Terminal.setStatus(1);//set as 1 to create it as enabled terminal
					Terminal.addTerminalMx(application, SessionData, hashRepRates, request);

					terminalErrors = Terminal.getErrors();
					if (terminalErrors==null || terminalErrors.size()==0){
						//Add terminal configuration information
						if (Terminal.IsTerminalInMBList(application, Terminal.getTerminalType())) {
							Vector vData = null;
							vData = Terminal.getMBConfigurationDataByTerminal(Terminal.getSiteId());
							if ( vData.size() == 0 ){
								Terminal.addMBConfigurationExt(Terminal.getSiteId(), Terminal.getSiteId(),Terminal.getMerchantId(Terminal.getSiteId()),request.getParameter("MBConfigurationID"),false);
							}
						}						
						Merchant.setSiteId(Terminal.getSiteId());
						Merchant.setMerchantId(Terminal.getMerchantId(Terminal.getSiteId()));
						Merchant.addClerkCodeMx(application, SessionData, Terminal.getClerkCode(), Terminal.getClerkName(), Terminal.getClerkLastName(), true);
						terminalAdded = true;
						String mess="";
						try{
						Merchant.getMerchant(SessionData,application);
							String mailHost = DebisysConfigListener.getMailHost(application);
							String userName = SessionData.getProperty("username");
							String companyName = SessionData.getProperty("company_name");
							String dba = Merchant.getDba();
							String contactEmail = Merchant.getContactEmail();
							String monthlyFee = Merchant.getMonthlyFee();
							Boolean PCTerm=Boolean.TRUE;							
							String sURL="";
							String sTerminalDesc="";
							Boolean isJavaHandSetTerm = Boolean.FALSE;
							Boolean isSmarthPhoneAndroid= Boolean.FALSE;
							Boolean isSmartPhoneIPhone= Boolean.FALSE;
							
							Vector vTerminalTypes = Terminal.getTerminalTypes_Combo(request.getParameter("terminalClass"));
							Iterator it = vTerminalTypes.iterator();
							while (it.hasNext())
							{
								Vector vTmp = (Vector) it.next();
								if (vTmp.get(0).toString().equals(Terminal.getTerminalType()))
								{
									if (!vTmp.get(1).toString().equals("PC Terminal"))
									{
										if ( vTmp.get(1).toString().startsWith("Nokia") || vTmp.get(1).toString().startsWith("Ingenico T3") )
										{
											isJavaHandSetTerm=Boolean.TRUE;
											PCTerm=Boolean.FALSE;
											sTerminalDesc = "[" + vTmp.get(0) + "] " + vTmp.get(1);
											break;
										}
										else
										{
											PCTerm=Boolean.FALSE;
										}
										if ( vTmp.get(1).toString().startsWith("SmartPhoneAndroid") ){
											isSmarthPhoneAndroid= Boolean.TRUE;
										}
										else if ( vTmp.get(1).toString().startsWith("SmartPhoneIPhone") ){
											isSmartPhoneIPhone= Boolean.TRUE;
										}										
									}
									else
									{
										Iterator itBrands = Terminal.getPCTerminalBrands(strRefId).iterator();
										while (itBrands.hasNext()){
											Vector vTemp = (Vector) itBrands.next();
											if (vTemp.get(0).toString().trim().equals(Terminal.getPcTermBrand())){
												if(termTypeUI.equalsIgnoreCase("V001")){
													sURL = (Terminal.isChkCertificate())? 
															Terminal.getBrandingProperties(Terminal.getPcTermBrand(), "PCTermSecureURL"): 
															Terminal.getBrandingProperties(Terminal.getPcTermBrand(), "PCTermURL")+"?b="+Terminal.getPcTermBrand();
												}else{
													sURL = (Terminal.isChkCertificate())? vTemp.get(3).toString().trim(): vTemp.get(2).toString().trim();
												}
												break;
											}
										}
									}
									sTerminalDesc = "[" + vTmp.get(0) + "] " + vTmp.get(1);
									break;
								}
							}
							// 2009-04-30 FB.Mail notification auto fill removed because ticket5545-7483655 
							String mailNotification = StringUtil.toString(request.getParameter("mailNotification")).trim();
							String instance = DebisysConfigListener.getInstance(application);
							String urlDownCert = "";
							String initType = SessionData.getProperty("terminal_type");
							if(termTypeUI.equalsIgnoreCase("V001") && !termTypeUI.equals(initType) && Terminal.isChkCertificate()){
								urlDownCert = Properties.getPropertyValue(instance, "global","downloadCertUrl");
							}
							SessionData.setProperty("terminal_type", "");
							if(!mailNotification.equals(""))
							{
								Terminal.sendMailNotification(mailHost,sAccessLevel, strRefId,companyName, userName,
															  dba,contactEmail,monthlyFee,request.getParameter("terminalClass"), 
															  mailNotification,PCTerm,sURL,sTerminalDesc,
															  (!DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)),
															  urlDownCert, true,isJavaHandSetTerm.booleanValue(),
															  isSmarthPhoneAndroid.booleanValue(),isSmartPhoneIPhone.booleanValue(),instance,Terminal.getSerialNumber(), application);
								
							}
						}catch (Exception e){
							String s = e.toString();
						}
						String terminalTypePhy = StringUtil.toString(request.getParameter("terminalType"));	
						if(com.debisys.terminals.Terminal.IsTerminalInPhysicalList(application,terminalTypePhy) 
									&& (SessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)) 
									&& StringUtil.toString(request.getParameter("physicalTerminalId")).length() > 0)
						{
							response.sendRedirect("direct.jsp");
						}else{
							response.sendRedirect("direct.jsp");
						}
						return;
					}
				}else{
					terminalErrors = Terminal.getErrors();
				}
			
		}
		else if (submitted.equals("y") && (Terminal.getSiteId().length() > 0) )
		{
			//we are updating a terminal
			String rateSelector = "";
			if(request.getParameter("rateSelector") != null){
				rateSelector = request.getParameter("rateSelector");
				Terminal.captureActorsNewRates(request, strAccessLevel, hashTSOrders, strDistChainType, hashRepRates,true);
			}else{
				Terminal.captureActorsRates(request, strAccessLevel, hashTSOrders, strDistChainType, hashRepRates,true);
			}
			// ValidateMarketPlace values
			Vector vecTerminalInfo = Terminal.getTerminalInfoMx();
			if (Terminal.isMarketPlaceTerminal(vecTerminalInfo.get(1).toString()) == true) {
				Terminal.setMarketPlaceKey (new Integer(vecTerminalInfo.get(16).toString()));
				Terminal.setMarketPlaceGroup (new Integer(vecTerminalInfo.get(17).toString()));
				Terminal.setSMarketPlaceTerminal(true);
				hiddenTrMarketPlaceRow="visible";
			}
			
			Terminal.setStatus(vecTerminalInfo.get(20).toString());
			Terminal.setStatusLastChange(vecTerminalInfo.get(21).toString());
			Terminal.setRatePlan(vecTerminalInfo.get(22).toString());
			Terminal.setTerminalDescription(vecTerminalInfo.get(23).toString());

			if(Terminal.validateRates(SessionData, hashRepRates, application, request))
			{
				String serialNumberValue = request.getParameter("serialNumber");
				if(serialNumberValue != null ){
					Terminal.setSerialNumber(serialNumberValue);
				}else{
					Terminal.setSerialNumber("");
				}
				String simDataValue = request.getParameter("simData");
				if(simDataValue != null ){
					Terminal.setSimData(simDataValue);
				}else{
					Terminal.setSimData("");
				}
				String imciDataValue = request.getParameter("imciData");
				if(imciDataValue != null ){
					Terminal.setImciData(imciDataValue);
				}else{
					Terminal.setImciData("");
				}
				/* END MiniRelease 9.0 - Added by YH */
				// BEGIN Release 16.5 New fields to the terminal. Add by YH
				String termVersionValue = request.getParameter("termVersion");
				if(terminalVersionForPcTerminal != null){
					Terminal.setTermVersion(terminalVersionForPcTerminal);
				}else{
					Terminal.setTermVersion("");
				}
				String termNoteValue = request.getParameter("termNote");
				if(termNoteValue != null ){
					Terminal.setTermNote(termNoteValue);
				}else{
					Terminal.setTermNote("");
				}
				// CC Cached Receipts
				String cachedReceiptsValue = request.getParameter("cachedReceipts");
				if(cachedReceiptsValue != null ){
					Terminal.setEnbCachedRcp(cachedReceiptsValue.equals("on")?true:false);
				}else{
					Terminal.setEnbCachedRcp(false);
				} //END CC Cached Receipts
				// David Castro update terminal Marketplace values
				String termMpGroup = request.getParameter("ApplicationGroup");
				if(!termMpGroup.equals("0")){
					Terminal.setMarketPlaceGroup(new Integer(termMpGroup));
				}
				String termMpKey = request.getParameter("licenseKey");
				if(!termMpKey.equals("0")){
					Terminal.setMarketPlaceKey(new Integer(termMpKey));
				}
				String urlDownCert = "";
				
				String userNameOld = request.getParameter("javaHandSetUserNameOld");
				String passwordOld = request.getParameter("javaHandSetPasswordOld");
				String brandingOld = request.getParameter("brandingOld");
				String regCodOld = request.getParameter("txtRegistrationOld");	
							
		         String termLabelValue=request.getParameter("terminalLabel");
		        if(termLabelValue != null ){
					Terminal.setTerminalLabel(termLabelValue);
				}else{
					Terminal.setTerminalLabel("");
				}
				
				if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
				{
					Terminal.updateTerminalMx(SessionData, hashRepRates, application,request);
				}
				else
				{
					Terminal.updateTerminal(SessionData, hashRepRates, application,request);
				}
				
					          
		        
				if (request.getParameter("termVersion_Combo")!=null)
				{
					String termTypeUI = request.getParameter("termVersion_Combo");
					Terminal.setTermVersion(termTypeUI);
					String instance = DebisysConfigListener.getInstance(application);
					String initType = SessionData.getProperty("terminal_type");
					boolean isNokia = (Terminal.getTerminalDescription().startsWith("Nokia")?true:false);
					boolean isSmartPhoneAndroid = (Terminal.getTerminalDescription().startsWith("SmartPhoneAndroid")?true:false);
                    boolean isSmartPhoneIPhone = (Terminal.getTerminalDescription().startsWith("SmartPhoneIPhone")?true:false);
					boolean isPCTerminal = (termTypeUI.equalsIgnoreCase("V001") && !termTypeUI.equals(initType) && Terminal.hasChkCertificate(SessionData, application));
					
					if (  isPCTerminal || isNokia )
					{
						urlDownCert = Properties.getPropertyValue(instance, "global","downloadCertUrl");
						String mailNotification = StringUtil.toString(request.getParameter("mailNotification")).trim();
						String mailHost = DebisysConfigListener.getMailHost(application);
						String userName = SessionData.getProperty("username");
						String companyName = SessionData.getProperty("company_name");
						String dba = Merchant.getDba();
						String contactEmail = Merchant.getContactEmail();
						String monthlyFee = Merchant.getMonthlyFee();
						Boolean PCTerm=Boolean.TRUE;
						String sURL="";
						String sTerminalDesc="";
						Boolean sendNokiaEmail = Boolean.FALSE;
						if (isNokia)
						{
							Vector vResultsTR = (Vector)Terminal.getPCTerminalRegistrations("", application).get(0);
							Terminal.setTxtRegistration(vResultsTR.get(2).toString());
					        Terminal.setPcTermUsername(vResultsTR.get(5).toString());
					        Terminal.setPcTermPassword(vResultsTR.get(6).toString());
					        Terminal.setPcTermBrand(vResultsTR.get(10).toString());
							if ( !(Terminal.getTxtRegistration().equals(regCodOld) && 
								   Terminal.getPcTermBrand().equals(brandingOld) &&
								   Terminal.getPcTermPassword().equals(passwordOld) &&
								   Terminal.getPcTermUsername().equals(userNameOld))
							   )
							{
								sendNokiaEmail = Boolean.TRUE;
							}
						}
						
						if( (!mailNotification.equals("") && sendNokiaEmail) || (!mailNotification.equals("") && isPCTerminal) )
						{
							try{
								Terminal.sendMailNotification(mailHost,sAccessLevel, strRefId,companyName, userName,dba,contactEmail,monthlyFee, 
															  request.getParameter("terminalClass"), mailNotification,PCTerm,sURL,sTerminalDesc, 
															  !DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO), 
															  urlDownCert, false,isNokia,isSmartPhoneAndroid,isSmartPhoneIPhone,instance,Terminal.getSerialNumber(), application);
							}catch(Exception e){
								//can't connect to smtp host
							}
						}
					}
					SessionData.setProperty("terminal_type", "");
				}										
				
				Terminal.updateTSOrder(SessionData, hashTSOrders);
				Terminal t = new Terminal();
				t.setSiteId(Terminal.getSiteId());
				t.setMerchantId(t.getMerchantId(t.getSiteId()));
				Vector vData = t.getTerminalInfoMx();
				//BEGIN JFM 26-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE
				if((SessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)) 
						&& com.debisys.terminals.Terminal.IsTerminalInPhysicalList(application, vData.get(1).toString()) 
						&& (StringUtil.toString(vData.get(15).toString()).length() == 0) 
						&& (request.getParameter("physicalTerminalId") != null) 
						&& StringUtil.toString(request.getParameter("physicalTerminalId")).length() > 0)
				{
					CPhysicalTerminal terminalFisico = new CPhysicalTerminal();
					terminalFisico.loadTerminal(Long.parseLong(StringUtil.toString(request.getParameter("physicalTerminalId"))));
					Terminal.setTerminalFisico(terminalFisico);
					Terminal.assignPhysicalTerminal(SessionData.getProperty("ref_id"),SessionData);
					asignoTerminal = true;
				}
				terminalErrors = Terminal.getErrors();
				if(terminalErrors.isEmpty()){
					if(!asignoTerminal){
						response.sendRedirect("direct.jsp");
					}else{
						response.sendRedirect("direct.jsp");
					}
					return;
				}
			}else{
				terminalErrors = Terminal.getErrors();
			}
		}
	}
	catch (com.debisys.exceptions.TerminalException e){
		String s = e.toString();
		terminalErrors = Terminal.getErrors();
	}
	catch (Exception e){
		String s = e.toString();
		terminalErrors = Terminal.getErrors();
	}
	
}
else if ( Terminal.getSiteId().length() > 0 )
{
	isEditingTerminal=true;
	// Editing a terminal
	Terminal.load(SessionData, application);
	if (Terminal.isMarketPlaceTerminal(Terminal.getTerminalType()) == true){
		hiddenTrMarketPlaceRow="visible";
	}
	if (Terminal.getTerminalType().equals("26")){
		terminalVersionForPcTerminal = Terminal.getTermVersion();
		SessionData.setProperty("terminal_type", terminalVersionForPcTerminal);
	}
	String strSerialValue = "";
	String strSimValue = "";
	String strImsiValue = "";
	if(com.debisys.terminals.Terminal.IsTerminalInPhysicalList(application, Terminal.getTerminalType()) && SessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)){
		strSerialValue = ((request.getParameter("serialNumber") != null)?request.getParameter("serialNumber"):Terminal.getSerialNumber());
		disableControlsMB = "readonly";
		if(Terminal.getTerminalFisico()!=null){
			strSerialValue = Terminal.getTerminalFisico().getSerialNumber();
			Terminal.setSerialNumber(strSerialValue);
			if(Terminal.getTerminalFisico().getAssignedParts() != null){
				for(CTerminalPart currentPart:Terminal.getTerminalFisico().getAssignedParts().values()){
					if(currentPart.getPartType().getPartTypeCode().equals(CTerminalPartType.SIMCARD_CODE)){
						CSimCard simCardObj = (CSimCard)currentPart;   
						strSimValue = String.valueOf(simCardObj.getPhoneNumber());
						Terminal.setSimData(strSimValue);
						strImsiValue = String.valueOf(simCardObj.getMobileSuscriberId());
						Terminal.setImciData(strImsiValue);
						break; // Aqu� encontr� una parte de tipo SIM
					}
				}
			}
		}
	}else{
		strSerialValue = ((request.getParameter("serialNumber") != null)?request.getParameter("serialNumber"):Terminal.getSerialNumber());
		strSimValue = ((request.getParameter("simData") != null)?request.getParameter("simData"):Terminal.getSimData());
		strImsiValue = ((request.getParameter("imciData") != null)?request.getParameter("imciData"):Terminal.getImciData());
	}	
	
}//else{
	Merchant m2 = new Merchant();
	m2.setMerchantId(Terminal.getMerchantId());
	m2.getMerchant(SessionData,application);
	Terminal.setMonthlyFeeThreshold(m2.getMonthlyFeeThreshold());
	boolean merchantUseNewRatePlanModel = m2.isUseRatePlanModel();
//}

String termDisabled = "";
if (Terminal.getSiteId().length() > 0 ){
	termDisabled = (Terminal.getStatus() == 0)? " disabled ":"";
}
%>
<%@ include file="/includes/header.jsp" %>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script type="text/javascript" src="/support/includes/ajax.js"></script>
<script type="text/javascript" src="/support/js/terminal.js"></script>
<script type="text/javascript">
	function check_form(form_name){
		error = false;
		form = form_name;
		error_message = "<%=Languages.getString("jsp.admin.customers.merchants_edit.jsmsg2",SessionData.getLanguage())%>";
		check_input("terminalType", 1, "<%=Languages.getString("com.debisys.terminals.error_terminal_type",SessionData.getLanguage())%>");
		check_input("clerkName", 1, "<%=Languages.getString("jsp.admin.customers.terminal.enterclerkname",SessionData.getLanguage())%>");
		check_input("clerkCode", 1, "<%=Languages.getString("jsp.admin.customers.terminal.enterclerkcode",SessionData.getLanguage())%>");
		var obj2 = document.getElementById('mailNotificationT');
		if(obj2 && obj2.style["display"] != "none"){
			check_input("mailNotification", 1, "<%=Languages.getString("jsp.admin.customers.terminal.enterNotificationMail",SessionData.getLanguage())%>");
		}
		//Check for terminals managed by MarketPlace 
		if (sMPTerminalType == "true"){
			check_inputNotZero("ApplicationGroup", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_aplicationGroup",SessionData.getLanguage())%>");
			check_inputNotZero("licenseKey", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_licenseKey",SessionData.getLanguage())%>");
		}
<%	
// If when deploying in Mexico
if(customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
%>
// JFM Junio 24 2008 - se coloca comentario xq la validacion del serial ya no debe hacerse, este debe cargarse de la BD
		check_input("annualInsuranceFee", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.annualInsuranceFee",SessionData.getLanguage())%>");
		check_input("annualInsuranceTax", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.annualInsuranceTax",SessionData.getLanguage())%>");
		check_input("annualMaintenanceFee", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.annualMaintenanceFee",SessionData.getLanguage())%>");
		check_input("annualMaintenanceTax", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.annualMaintenanceTax",SessionData.getLanguage())%>");
		check_input("reconnectionFee", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.reconnectionFee",SessionData.getLanguage())%>");
		check_input("reconnectionTax", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.reconnectionTax",SessionData.getLanguage())%>");
		check_input("annualAffiliationFee", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.annualAffiliationFee",SessionData.getLanguage())%>");
		check_input("annualAffiliationTax", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.annualAffiliationTax",SessionData.getLanguage())%>");
		check_input("clerkLastName", 1, "<%=Languages.getString("jsp.admin.customers.terminal.enterclerklastname",SessionData.getLanguage())%>");
<%
} //End of if when deploying in Mexico
%>
		 if ( pcterminalvalidation ){
			// David Castro SMS terminals dont need Terminal branding
			if (smsvalidation != true && webServiceValidation!=true){
				check_input("pcTermBrand", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_brand",SessionData.getLanguage())%>");
			}
			check_input("pcTermUsername", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_username",SessionData.getLanguage())%>");
			check_input("pcTermPassword", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_password",SessionData.getLanguage())%>");
			if ( document.getElementById("chkManualRegCode").checked ){
				check_input("txtRegistration", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_regcode",SessionData.getLanguage())%>");
			}
			if ( document.getElementById("chkIPRestriction").checked ){
				check_input("txtIPAddress", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_ipaddress",SessionData.getLanguage())%>");
				check_input("txtSubnetMask", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_subnetmask",SessionData.getLanguage())%>");
			}
			if ( document.getElementById("chkAllowCertificate").checked ){
				check_input("txtAllowCertificate", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_certamount",SessionData.getLanguage())%>");
			}
		}
		if ( smsvalidation || webServiceValidation ){
			if (smsvalidation != true){
				check_input("pcTermUsername", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_webserviceusername",SessionData.getLanguage())%>");
				check_input("pcTermPassword", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_webservicepassword",SessionData.getLanguage())%>");
			}
			if (webServiceValidation != true){
				check_input("pcTermUsername", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_smsusername",SessionData.getLanguage())%>");
				check_input("pcTermPassword", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit.no_smspassword",SessionData.getLanguage())%>");
			}
			if ( document.getElementById("chkIPRestriction").checked ){
				check_input("txtIPAddress", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_ipaddress",SessionData.getLanguage())%>");
				check_input("txtSubnetMask", 1, "<%=Languages.getString("jsp.admin.customers.terminal.no_subnetmask",SessionData.getLanguage())%>");
			}
		}
		var termtype = 0;
		var termof = 0;
		if ( document.getElementById("terminalType") != null ){
			termtype = document.getElementById("terminalType").value;
		}
		if ( document.getElementById("terminalOf") != null ){
			termof = document.getElementById("terminalOf").value;
		}
		if ( document.getElementById("terminalType") != null )
		{
			var terminalTypeDesc = document.getElementById("terminalType").options[document.getElementById("terminalType").selectedIndex].text; 
			if ( (terminalTypeDesc.indexOf("Microbrowser") != -1) || 
				 (terminalTypeDesc.indexOf("MB ") != -1) || 
				 (terminalTypeDesc.indexOf("Nokia") != -1) )
			{
				check_input("serialNumber", 1, "<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.serialnumberrequired",SessionData.getLanguage())%>");
			}
			if ( terminalTypeDesc.indexOf("Nokia") != -1 )
			{
				check_input("javaHandSetUserName", 1, "<%=Languages.getString("jsp.admin.customers.terminal.javaUserNameReq",SessionData.getLanguage())%>");
				check_input("javaHandSetPassword", 1, "<%=Languages.getString("jsp.admin.customers.terminal.javaPasswordReq",SessionData.getLanguage())%>");
				check_input("javaHandSetBranding", 1, "<%=Languages.getString("jsp.admin.customers.terminal.javaHandsetTermBrandReq",SessionData.getLanguage())%>");
				
			}
		}
		var remaining = document.getElementsByTagName("input");
		var i=0;
		var errorrates=false;
		for(i=0; i < remaining.length; i++){
			var nameinput = remaining[i].name;
			if (nameinput.indexOf("remaining_")==0){
				if (remaining[i].value < 0){
					errorrates=true;
				}
			}
		}
		if (errorrates){
			// this action avoid send wrong rates to the server
			error=true;
			error_message = error_message + "* " + "<%=Languages.getString("jsp.admin.terminal.error_creation_rates",SessionData.getLanguage())%>" + "\n";	  
		}
		if (error == true){
			alert(error_message);
			return false;
		}else{
			var ctl = document.getElementById("btnSubmit");
			if ( ctl != null ){
				ctl.disabled = true;
			}
			return true;
		}
	} //End of function check_form
	
	var typeRatePlan="<%=typeRatePlan%>";
	
	function calculate(c,prefix){
		if (isNaN(c.value)== false){
			if (c.value >=0){
				var strFieldName = c.name;
				var aryFieldName = strFieldName.split("_");
				var strLevel = aryFieldName[0];
				var strProductId = aryFieldName[1];
				var tmpTotalRate = eval("document.getElementById('"+prefix+"total_" + strProductId +"').value;");
				
<%
if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
				if (typeRatePlan=="ISO"){
					var tmpIsoRate   = eval("document.getElementById('"+prefix+"iso_" + strProductId +"').value;");
					//alert(tmpIsoRate);
				}else{
					var tmpIsoRate   = 0;
				}
<%
}
if ((strAccessLevel.equals(DebisysConstants.ISO)||strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var tmpAgentRate=0;
				var tmpSubAgentRate=0;
				if (typeRatePlan=="ISO"){
					tmpAgentRate   = eval("document.getElementById('"+prefix+"agent_" + strProductId +"').value;");
					tmpSubAgentRate   = eval("document.getElementById('"+prefix+"subagent_" + strProductId +"').value;");
				}
<%
}
if (strAccessLevel.equals(DebisysConstants.SUBAGENT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var tmpSubAgentRate = 0;
				if (typeRatePlan=="ISO"){
					tmpSubAgentRate   = eval("document.getElementById('"+prefix+"subagent_" + strProductId +"').value;");
				}
<%
}
%>
				var tmpRepRate   = eval("document.getElementById('"+prefix+"rep_" + strProductId +"').value;");
				var intTotalRate = 0;
<%
if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
				var intIsoRate = 0;
<%
}
if ((strAccessLevel.equals(DebisysConstants.ISO)||strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var intAgentRate = 0;
				var intSubAgentRate = 0;
<%
}
if (strAccessLevel.equals(DebisysConstants.SUBAGENT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var intSubAgentRate = 0;
<%
}
%>
				var intRepRate   = 0;
				if (isNaN(tmpTotalRate) == false){
					intTotalRate = tmpTotalRate - 0;
				}
<%
if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
					if (isNaN(tmpIsoRate) == false){
						intIsoRate = tmpIsoRate - 0;
					}
<%
}
if ((strAccessLevel.equals(DebisysConstants.ISO)||strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				if (isNaN(tmpAgentRate) == false){
					intAgentRate = tmpAgentRate - 0;
				}
				
				if (isNaN(tmpSubAgentRate) == false){
					intSubAgentRate = tmpSubAgentRate - 0;
				}
<%
}
if (strAccessLevel.equals(DebisysConstants.SUBAGENT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				if (isNaN(tmpSubAgentRate) == false){
					intSubAgentRate = tmpSubAgentRate - 0;
				}
<%
}
%>
				if (isNaN(tmpRepRate) == false){
					intRepRate = tmpRepRate - 0;
				}
<%
if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var intTotalRemaining = intTotalRate - (intIsoRate+intAgentRate+intSubAgentRate+intRepRate);
<%
}else if (strAccessLevel.equals(DebisysConstants.AGENT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var intTotalRemaining = intTotalRate - (intAgentRate+intSubAgentRate+intRepRate);
<%
}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var intTotalRemaining = intTotalRate - (intSubAgentRate+intRepRate);
<%
}else if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
%>
				var intTotalRemaining = intTotalRate - (intIsoRate+intRepRate);
<%
}else if(strAccessLevel.equals(DebisysConstants.REP)){
%>
				var intTotalRemaining = intTotalRate - intRepRate;
<%
}
%>
				if (isNaN(intTotalRemaining)==false){
					var fieldName = prefix+'remaining_' + strProductId;
					document.getElementById(fieldName).value = formatAmount(intTotalRemaining);
					if((intTotalRemaining < 0) || intTotalRemaining > intTotalRate){
						document.getElementById(fieldName).style.color = '#ff0000';
						// this is the red one. BAD
					}else{
						document.getElementById(fieldName).style.color = '#33FF33';
					}
				}
			}
		}
	}
<%
if ( Terminal.getSiteId().length() == 0 ){
%>
	function calculateISORate(c){
		if (isNaN(c.value)== false){
			if (c.value >=0){
				var strFieldName = c.name;
				var aryFieldName = strFieldName.split("_");
				var strLevel     = aryFieldName[0];
				var strProductId = aryFieldName[1];
				var tmpTotalRate = eval("document.getElementById('total_" + strProductId +"').value;");
<%
	if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
				var tmpIsoRate   = eval("document.getElementById('iso_" + strProductId +"').value;");
<%
	}
	if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var tmpAgentRate   = eval("document.getElementById('agent_" + strProductId +"').value;");
				var tmpSubAgentRate   = eval("document.getElementById('subagent_" + strProductId +"').value;");
<%
	}
%>
				var tmpRepRate   = eval("document.getElementById('rep_" + strProductId +"').value;");
				var intTotalRate = 0;
<%
	if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
				var intIsoRate = 0;
<%
	}
	if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var intAgentRate = 0;
				var intSubAgentRate = 0;
<%
	}
%>
				var intRepRate   = 0;
				if (isNaN(tmpTotalRate) == false){
					intTotalRate = tmpTotalRate - 0;
				}
<%
	if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
				if (isNaN(tmpIsoRate) == false){
					intIsoRate = tmpIsoRate - 0;
				}
<%
	}
	if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				if (isNaN(tmpAgentRate) == false){
					intAgentRate = tmpAgentRate - 0;
				}
				if (isNaN(tmpSubAgentRate) == false){
					intSubAgentRate = tmpSubAgentRate - 0;
				}
<%
	}
%>
				if (isNaN(tmpRepRate) == false){
					intRepRate = tmpRepRate - 0;
				}
<%
	if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
				var intTotalRemaining = intTotalRate - (intIsoRate+intAgentRate+intSubAgentRate+intRepRate);
<%
	}else if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
%>
				var intTotalRemaining = intTotalRate - (intIsoRate+intRepRate);
<%
	}else if(strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>
				var intTotalRemaining = intTotalRate - intRepRate;
<%
	}
%>
				if (isNaN(intTotalRemaining)==false){
					var fieldName = "remaining_" + strProductId;
					document.getElementById(fieldName).value = formatAmount(intTotalRemaining);
					if((intTotalRemaining < 0) || intTotalRemaining > intTotalRate){
						document.getElementById(fieldName).style.color = '#ff0000';
					}else{
						document.getElementById(fieldName).style.color = '#33FF33';
					}
				}
			}
		}
	} //End of function calculateISORate
<%
}
%>
	function validate(c){
		if (isNaN(c.value)){
			alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
			c.focus();
			return (false);
		}else{
			if (c.value < 0){
				alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
				c.focus();
				return (false);
			}else{
				c.value = formatAmount(c.value);
			}
		}
	}//End of function validate

	function validatets(c){
		if (isNaN(c.value)){
			alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
			c.focus();
			return (false);
		}else{
			if (c.value < 0){
				alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
				c.focus();
				return (false);
			}
		}
	}

	function validateInteger(c){
		if ( c.value.length > 0 ){
			if (isNaN(c.value)){
				alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
				c.focus();
				c.select();
				return false;
			}else if (c.value < 0){
				alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
				c.focus();
				c.select();
				return false;
			}
		}
	}//End of function validateInteger

	<%
	if ( Terminal.getStatus()>0 || Terminal.getSiteId().length() == 0){
		%>
		$(document).ready(function(){
			$("#btnSubmit").removeAttr('disabled');		
		});
		<% 
	}
	else{
	%>
		$(document).ready(function(){
			//alert("NOK ");
		});
		<% 
	}
	%>
	
</script>
<%
	if(request.getParameter("domain")!=null) {
		String nss = request.getParameter("domain");
%>
<script type="application/javascript">
    window.sessionStorage['domain'] = '<%= nss%>';
</script>
<%
	}
%>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td colspan="3" bgcolor="#ffffff" class="main">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
				<tr>
					<td class="formArea">
						<table border="0" width="100%" cellpadding="0" cellspacing="0" align="left">
							<tr>
								<td>
									<form name="terminal" method="post" action="admin/customers/terminal_new.jsp" onsubmit="return check_form(terminal);">
										<table width="100%">
											<tr>
												<td class="formArea2">
													<br>
													<table width="100%">
														<tr>
															<td></td>
															<td>&nbsp;</td>
															<td></td>
															<td>&nbsp;&nbsp;</td>
															<td></td>
															<td>&nbsp;</td>
															<td></td>
														</tr>
<%
if (terminalErrors != null && terminalErrors.size()>0){
	out.println("<tr class=main><td colspan=7><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");
	Enumeration enum1 = terminalErrors.keys();
	while (enum1.hasMoreElements()){
		String strKey = enum1.nextElement().toString();
		String strError = (String)terminalErrors.get(strKey);
		if (strError != null && !strError.equals("")){
			out.println("<li>" + strError);
		}
	}
	out.println("</font></td></tr>");
	out.println("<tr><td></td><td>&nbsp;</td><td></td><td>&nbsp;&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>");
	out.println("<script type=\"application/javascript\">if(window.sessionStorage['errorCount']){window.sessionStorage['errorCount']=parseInt(window.sessionStorage['errorCount']+1);}else{window.sessionStorage['errorCount']=1;}</script>");
}


if ( Terminal.getSiteId().length() == 0 ){
%>
														<tr class="main">
<%
	if ( SessionData.getUser().isQcommBusiness() && (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) && strAccessLevel.equals(DebisysConstants.ISO) ){
%>
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_info.terminal_of",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="hidden" name="terminalClass" value="QComm">
																<select id="terminalOf" name="terminalOf" onchange="onChangeTerminalType();" <%=termDisabled %>>
																	<option value="1" <%=(StringUtil.toString(request.getParameter("terminalOf")).equals("1"))?"selected":""%>>Debisys</option>
																	<option value="2" <%=(StringUtil.toString(request.getParameter("terminalOf")).equals("2") || StringUtil.toString(request.getParameter("terminalOf")).equals(""))?"selected":""%>>QComm</option>
																</select>
															</td>
															<td></td>
<%
	}
%>
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.terminal_type",SessionData.getLanguage())%></td>
															<td></td>
															<td>
<%
	if (!(SessionData.getUser().isQcommBusiness() && (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) && strAccessLevel.equals(DebisysConstants.ISO))){
%>
																<input type="hidden" name="terminalClass" value="Debisys">
<%
	}
%>
																<select id="terminalType" name="terminalType" <%=termDisabled %>>
																	<option value=""><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.select_terminal_type",SessionData.getLanguage())%></option>
<%
	{
		// rbuitrago: usage of parameter and new method (uses new terminal_types table)
		String sClass = StringUtil.toString(request.getParameter("terminalClass"));
		if ( sClass.equals("") ){
			sClass = "Debisys";
		}
		Iterator it = com.debisys.terminals.Terminal.getTerminalTypes_Combo(sClass).iterator();
		while (it.hasNext()){
			Vector vecTemp = (Vector)it.next();
			String strTerminalTypeId = vecTemp.get(0).toString();
			String strTerminalTypeDesc = vecTemp.get(1).toString();
			if (strTerminalTypeId.equals(request.getParameter("terminalType"))){
				out.println("<option value=" + strTerminalTypeId + " selected>" + strTerminalTypeDesc + "</option>");
			}else{
				out.println("<option value=" + strTerminalTypeId + ">" + strTerminalTypeDesc + "</option>");
			}
		}
	}

%>
																</select>
<%
	if (terminalErrors != null && terminalErrors.containsKey("terminalType")) {
		out.print("<font color=ff0000>&nbsp;*</font>");
	}
%>
																<script>
																	var isWebServiceAutherticationTerminalsEnabled = <%=User.isWebServiceAutherticationTerminalsEnabled(SessionData, application) ? "true" : "false"%>;
																	function terminalType_onclick(c){
																		//DavidCastro modifying terminaltype on click to check whether is a MarketPlace license
																		var myStr = "<%=vMPTerminalTypes.toString()%>";
																		var sMBTerminals = "<%=Terminal.TerminalMBList()%>";

																		//Si la terminal seleccionada se encuentra entre la lista de terminales del MP
																		if (myStr.indexOf("["+ c.value +",") != -1){
																			document.getElementById("MarketPlaceRow").style.visibility = 'visible';
																			sMPTerminalType = "true";
																		}else{
																			document.getElementById("MarketPlaceRow").style.visibility = 'hidden' ;
																			sMPTerminalType = "false"; //Is NOT to be used under MP management
																		}

																		document.getElementById("MBConfigurationRow").style.display = (sMBTerminals.indexOf("," + c.value + ",") != -1)?'':'none';
																		
																		if ( c.value != "<%=sPCTerminalID %>"){
<%
	if (SessionData.checkPermission(DebisysConstants.PERM_ENABLED_ADD_EDIT_EMIDA_PCTERMINAL)){
%>
																			document.getElementById('termVersion_Combo').style.display = 'none';
																			document.getElementById('termVersion').style.display = 'inline'; 
<%
	}
%>
																			//Alfred A. - Added two new kiosk terminals that act as if they were webservice or sms terminals - DBSY-938
																			if ( c.value != "<%=sSMSTerminalID %>" && c.value != "<%=sSMS2TerminalID %>" 
                                                                                                                                                                    && c.value != "<%=sWebServiceTerminalID %>" && c.value != "<%=sSMS3TerminalID %>" 
                                                                                                                                                                    && c.value != "<%=sWebService2TerminalID %>" && c.value != "<%=sWebService3TerminalID %>" 
                                                                                                                                                                    && c.value != "<%=sSMS4TerminalID %>" && c.value != "<%=sSMS5TerminalID %>" 
                                                                                                                                                                    && c.value != "<%=sSMS6TerminalID %>" && c.value != "<%= sInfopymeKioskUSSD %>" )
                                                                                                                                                        {	// Not PCTerminal and NOT SMS terminals 
																				document.getElementById('mxTablePCData').style.display = "none";
																				document.getElementById('pcTermUsername').value = '';
																				document.getElementById('pcTermPassword').value = '';
																				pcterminalvalidation = false;	
																				smsvalidation = false;
																				webServiceValidation=false;
																			}else if(c.value == "<%=sWebServiceTerminalID%>" || c.value == "<%=sWebService3TerminalID %>" || c.value == "<%=sWebService2TerminalID %>"){
																				if(isWebServiceAutherticationTerminalsEnabled){
																					document.getElementById('mxTablePCData').style.display = "table";
																					//Hide stuff from PCterminal MxTablePCData that we don�t want
																					document.getElementById('rowEnforceRegCode').style.display = 'none';
																					document.getElementById('rowEnterRegCode').style.display = 'none';
																					document.getElementById('rowBoxEnterRegCode').style.display = 'none';
																					document.getElementById('lineBeforeEnforceCertificate').style.display = 'none';
																					document.getElementById('enforceCertificateCheck').style.display = 'none';
																					document.getElementById('allowInstallCertificateCheck').style.display = 'none';
																					document.getElementById('certificateInstallAmount').style.display = 'none';
																					document.getElementById('nameBoxPcTerm').style.display = 'none';
																					document.getElementById('lineBeforeEnforceRegCode').style.display = 'none';
																					document.getElementById('lineBeforeIPRest').style.display = 'none';
																					document.getElementById('user_id_label').style.display = 'none';
																					document.getElementById('user_name_label').style.display = 'none';
																					document.getElementById('webserviceuser_name_label').style.display = '';
																					document.getElementById('pc_term_pwd_label').style.display = 'none';
																					document.getElementById('sms_pwd_label').style.display = 'none';
																					document.getElementById('webservice_pwd_label').style.display = '';
																					document.getElementById('pc_term_title_label').style.display = 'none';
																					document.getElementById('sms_terminal_title_label').style.display = 'none';
																					document.getElementById('webservice_terminal_title_label').style.display = '';
																					webServiceValidation=true;
																					pcterminalvalidation = false;
																					smsvalidation = false;
																				}else{
																					//Alfred A. - Added a catch for web service terminals that are not authenticated - DBSY-938
																					document.getElementById('mxTablePCData').style.display = "none";
																					document.getElementById('pcTermUsername').value = '';
																					document.getElementById('pcTermPassword').value = '';
																					pcterminalvalidation = false;	
																					smsvalidation = false;
																					webServiceValidation=false;
																				}
																			}else{
																				//David Castro This are SMS terminals, It uses the same validation and table made for Pcterminal
																				document.getElementById('mxTablePCData').style.display = "table";
																				//Hide stuff from PCterminal MxTablePCData that we don�t want
																				document.getElementById('rowEnforceRegCode').style.display = 'none';
																				document.getElementById('rowEnterRegCode').style.display = 'none';
																				document.getElementById('rowBoxEnterRegCode').style.display = 'none';
																				document.getElementById('lineBeforeEnforceCertificate').style.display = 'none';
																				document.getElementById('enforceCertificateCheck').style.display = 'none';
																				document.getElementById('allowInstallCertificateCheck').style.display = 'none';
																				document.getElementById('certificateInstallAmount').style.display = 'none';
																				document.getElementById('nameBoxPcTerm').style.display = 'none';
																				document.getElementById('lineBeforeEnforceRegCode').style.display = 'none';
																				document.getElementById('lineBeforeIPRest').style.display = 'none';
																				document.getElementById('user_id_label').style.display = '';
																				document.getElementById('user_name_label').style.display = 'none';
																				document.getElementById('webserviceuser_name_label').style.display ='none';		
																				document.getElementById('pc_term_pwd_label').style.display = 'none';
																				document.getElementById('sms_pwd_label').style.display = '';
																				document.getElementById('webservice_pwd_label').style.display ='none';
																				document.getElementById('pc_term_title_label').style.display = 'none';
																				document.getElementById('sms_terminal_title_label').style.display = '';
																				document.getElementById('webservice_terminal_title_label').style.display ='none';
																				pcterminalvalidation = false;
																				smsvalidation = true;
																				webServiceValidation=false;
																			}
																		}else{
																			
																			document.getElementById('mxTablePCData').style.display = "table";//Show stuff for PCterminal MxTablePCData that we don�t want
																			document.getElementById('rowEnforceRegCode').style.display = '';
																			document.getElementById('rowEnterRegCode').style.display = '';
																			document.getElementById('rowBoxEnterRegCode').style.display = '';
																			document.getElementById('lineBeforeEnforceCertificate').style.display = '';
																			document.getElementById('enforceCertificateCheck').style.display = '';
																			document.getElementById('allowInstallCertificateCheck').style.display = '';
																			document.getElementById('certificateInstallAmount').style.display = '';
																			document.getElementById('nameBoxPcTerm').style.display = '';
																			document.getElementById('lineBeforeEnforceRegCode').style.display = '';
																			document.getElementById('lineBeforeIPRest').style.display = '';
																			document.getElementById('user_id_label').style.display = 'none';
																			document.getElementById('user_name_label').style.display = '';
																			document.getElementById('webserviceuser_name_label').style.display = 'none';
																			document.getElementById('pc_term_pwd_label').style.display = '';
																			document.getElementById('sms_pwd_label').style.display = 'none';
																			document.getElementById('webservice_pwd_label').style.display = 'none';
																			document.getElementById('pc_term_title_label').style.display = '';
																			document.getElementById('sms_terminal_title_label').style.display = 'none';
																			document.getElementById('webservice_terminal_title_label').style.display = 'none';
<%
	if (SessionData.checkPermission(DebisysConstants.PERM_ENABLED_ADD_EDIT_EMIDA_PCTERMINAL)){
%>
																			document.getElementById('termVersion_Combo').style.display = 'inline';
																			document.getElementById('termVersion').style.display = 'none';
																			//alert("terminalType_onclick PERM_ENABLED_ADD_EDIT_EMIDA_PCTERMINAL ok");
<%
	}
%>
																			pcterminalvalidation = true;
																			smsvalidation = false;
																			webServiceValidation=false;
																			
																		}
																		
																		var imciControl = document.getElementById('tdTerminalImci');
																		if ( c.value =="85") /*JAVA HANDSET specific data*/
																		{
																			//document.getElementById('tableJavaHandSet').style.display = "inline";
																			imciControl.innerHTML="<%=Languages.getString("jsp.admin.customers.terminal.phoneNumberLabel",SessionData.getLanguage())%>";
																			//$("#trTerminalCarrier").removeAttr('disabled');
																			//$("#trTerminalCarrier").css('display', 'inline');
																		}
																		else
																		{
																			//document.getElementById('tableJavaHandSet').style.display = "none";
																			imciControl.innerHTML="<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.imci",SessionData.getLanguage())%>";
																			//$("#trTerminalCarrier").attr('disabled', 'disabled');
																			//$("#trTerminalCarrier").css('display', 'none');
																		}
																		
																		/**************************************************/
																		/**************************************************/
																		populateTerminalVersionByTerminalType(c.value);
																		/**************************************************/
																		/**************************************************/
																		
																		var terminalTrackingEnabled = <%=SessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)%>;
																		var sTerminals = "<%=DebisysConfigListener.getPhysicalTerminals(application)%>";
																		if ((c.value.length > 0) && (sTerminals.indexOf(c.value) != -1) && (terminalTrackingEnabled == true)){
																			document.getElementById('physicalTerminalId').value = '';
																			document.getElementById('serialNumber').value = '';
																			document.getElementById('simData').value = '';
																			document.getElementById('imciData').value = '';
																			document.getElementById('serialNumber').disabled = true;
																			document.getElementById('simData').disabled = true;
																			document.getElementById('imciData').disabled = true;
																			document.getElementById('trSerialTracking').style.display = "inline";
																			mbvalidation = true;
																		}else{
																			document.getElementById('serialNumber').disabled = false;
																			document.getElementById('simData').disabled = false;
																			document.getElementById('imciData').disabled = false;
																			document.getElementById('trSerialTracking').style.display = "none";
																			mbvalidation = false;
																		}
<%
	if ( SessionData.checkPermission(DebisysConstants.PERM_NOTES) && strAccessLevel.equals(DebisysConstants.ISO) ){
%>
																		if ((c.value.length > 0) && (sMBTerminals.indexOf(c.value) != -1))
																		{
																			document.getElementById('cachereceipts').style.visibility = "visible";
																			document.getElementById('cachereceiptscontrol').style.visibility = "visible";
																		}
																		else
																		{
																			document.getElementById('cachereceipts').style.visibility = "hidden";
																			document.getElementById('cachereceiptscontrol').style.visibility = "hidden";
																		} 
<%
	}
%>
																	} //End of function terminalType_onclick
																	$(document).ready(function(){
																		$('#terminalType').change(function(){
																			terminalType_onclick(this);
																		});
																		terminalType_onclick($('#terminalType').get(0));
																	});
																</script>
																
															</td>
														</tr>
<%
}
%>
														<tr id="trSerialTracking" class="main" style="display:none;">
															<td colspan="6"><a href="javascript:void(0)" onclick="OpenPhysicalTerminalSelector()"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.addphysicalterminal",SessionData.getLanguage())%></a></td>
															<td>
																<input type="hidden" id="physicalTerminalId" name="physicalTerminalId">
																<script>
																	function OpenPhysicalTerminalSelector(){
																		var sURL = "/support/admin/customers/physterm_selector.jsp";
																		var sOptions = "left=" + (screen.width - (screen.width * 0.8))/2 + ",top=" + (screen.height - (screen.height * 0.4))/2 + ",width=" + (screen.width * 0.8) + ",height=" + (screen.height * 0.5) + ",location=no,menubar=no,resizable=yes,scrollbars=yes";
																		var w = window.open(sURL, "<%=Terminal.getSiteId()%>", sOptions, true);
																		w.focus();
																	}
																</script>
															</td>
														</tr>
<%
	if (isEditingTerminal){
%>
														<tr class="main">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.terminalDescription",SessionData.getLanguage())%></td>
															<td></td>
															<td colspan="6"><input id="terminalDescription" name="terminalDescription" size="35" type="text" disabled="disabled" readonly="readonly" value="<%=Terminal.getTerminalDescription()%>">
															</td>
														</tr>	
<%
	}
%>	
														<tr class="main">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.serialnumber",SessionData.getLanguage())%></td>
															<td></td>
															<td nowrap="nowrap">
<%
	System.out.println(Terminal.getSerialNumber());
	LogChanges.set_value_for_check(LogChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SERIALNO), String.valueOf(Terminal.getSerialNumber()));

	if( SessionData.getUser().isQcommBusiness() ){
%>
																<input type="text" id="serialNumber" name="serialNumber" value="<%=Terminal.getSerialNumber()%>" size="26" maxlength="26" <%=termDisabled %>>
<%
	}else{
%>
																<input type="text" id="serialNumber" name="serialNumber" value="<%=Terminal.getSerialNumber()%>" size="26" maxlength="25" <%=disableControlsMB%> <%=termDisabled %>>
<%
	}
	if (terminalErrors != null && terminalErrors.containsKey("serialNumber")){
		out.print("<font color=ff0000>&nbsp;*</font>");
	}
%>
															</td>
															<td></td>
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.term_version",SessionData.getLanguage())%></td>
															<td></td>
															<td>

<% 
	ArrayList<String> terminalVersionByTerminalType= TerminalVersions.getTerminalVersionsByTerminalTypeNewTerminal(SessionData);
	StringBuilder scritpVersion = new StringBuilder();
        int indexCounter = 0;
	for(int k=0; k<terminalVersionByTerminalType.size();k++){
            String[] infoTV = terminalVersionByTerminalType.get(k).split("#");

            if ( infoTV[0].equals("V000") ){
                if ( !deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) ){
                    //PC Term 1.0
                    scritpVersion.append("versionsByTerminalType["+indexCounter+"]='PC Term 1.0#"+infoTV[1]+"#"+infoTV[0]+"'; "+"\n");
                    indexCounter++;
                }
            }
            else  if ( infoTV[0].equals("V001") ){                
                //PC Term 2.0
                scritpVersion.append("versionsByTerminalType["+indexCounter+"]='PC Term 2.0#"+infoTV[1]+"#"+infoTV[0]+"';"+"\n");
                indexCounter++;                
            }
            else  if ( infoTV[0].equals("NG") ){                
                //PC Terminal NG
                scritpVersion.append("versionsByTerminalType["+indexCounter+"]='PC Term 4.0#"+infoTV[1]+"#"+infoTV[0]+"';"+"\n");
                indexCounter++;                
            }
            else{
                if ( !deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) ){
                    scritpVersion.append("versionsByTerminalType["+indexCounter+"]='"+infoTV[0]+"#"+infoTV[1]+"#"+infoTV[0]+"';"+"\n");
                    indexCounter++;
                }
            }
	}
	%>
	<script type="text/javascript">
		var versionsByTerminalType=new Array();		
		<%=scritpVersion.toString()%>
	</script>	
	
	<% 
	
	boolean hasVersionNameAsigned=false;
	if (SessionData.checkPermission(DebisysConstants.PERM_ENABLED_ADD_EDIT_EMIDA_PCTERMINAL))
	{
%>
		<input type="hidden" id="current_term_version" name="current_term_version" value="<%=terminalVersionForPcTerminal%>">
		<select id="termVersion_Combo" name="termVersion_Combo" onchange="checkTermVersion(this);" onclick="checkTermVersion(this);" <%=termDisabled %>>  
		</select>
		<input id="termVersion" name="termVersion" style="display: none;" type="text"  value="<%=Terminal.getTermVersion()%>" size="15" maxlength="20" <%=termDisabled %>><%=((customConfigType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))?"":"&nbsp;(" + Languages.getString("jsp.admin.optional",SessionData.getLanguage()) + ")")%>
		
		<script type="text/javascript"> populateTerminalVersionByTerminalType(<%=Terminal.getTerminalType()%>); </script>
		
<%
	}else{
%>
		<select id="termVersion_Combo" name="termVersion_Combo"  onchange="checkTermVersion(this);" style="display: none" <%=termDisabled %>>  
	    </select>
		<input id="termVersion" name="termVersion" type="text"  value="<%=Terminal.getTermVersion()%>" size="15" maxlength="20" <%=termDisabled %>><%=((customConfigType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))?"":"&nbsp;(" + Languages.getString("jsp.admin.optional",SessionData.getLanguage()) + ")")%>
<%
	}
%>
															</td>
														</tr>
														<tr id = "MarketPlaceRow" class="main" style="visibility:<%=hiddenTrMarketPlaceRow%>"><td id = "LabelApplicationGroup" nowrap="nowrap"><%=Languages.getString("jsp.admin.customer.terminal.groupcombo",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<select id="ApplicationGroup" name="ApplicationGroup" <%=termDisabled %>>
																	<option value="0"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.select_group",SessionData.getLanguage())%></option>
<%
	{
		// David Castro, Application group to install in the MB terminal
		String sClass = StringUtil.toString(request.getParameter("terminalClass"));
		if ( sClass.equals("") ){
			sClass = "Debisys";
		}
		Iterator it = com.debisys.terminals.Terminal.getMarketPlaceGroup_Combo(sClass).iterator();
		while (it.hasNext()){
			Vector vecTemp = (Vector)it.next();
			Integer MPAplicationGroupId = (Integer)vecTemp.get(0);
			String strMPAplicationGroupDesc = vecTemp.get(1).toString();
			Integer myMPGroup = Terminal.getMarketPlaceGroup();
			if (myMPGroup != null){
				if (MPAplicationGroupId.equals(myMPGroup)){
					out.println("<option value=\"" + MPAplicationGroupId + "\" selected >" + strMPAplicationGroupDesc + "</option>");
				}else{
					out.println("<option value=\"" + MPAplicationGroupId + "\">" + strMPAplicationGroupDesc + "</option>");
				}
			}
		}
	}
%>
																</select>
															</td>
															<td></td>
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customer.terminal.keycombo",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<select id="licenseKey" name="licenseKey" <%=termDisabled %>>
																	<option value="0"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.select_key",SessionData.getLanguage())%></option>
<%
	{
		// DavidCastro: Key to be used among the floating ones for the terminal. 
		String sClass = StringUtil.toString(request.getParameter("terminalClass"));
		if ( sClass.equals("") ){
			sClass = "Debisys";
		}
		Iterator it = com.debisys.terminals.Terminal.getMarketPlaceKeys_Combo(sClass).iterator();
		while (it.hasNext()){
			Vector vecTemp = (Vector)it.next();
			Integer KeysgroupId = (Integer)vecTemp.get(0);
			String strKeysDesc = vecTemp.get(1).toString();
			if (KeysgroupId.equals(Terminal.getMarketPlaceKey())){
				out.println("<option value=\"" + KeysgroupId + "\" selected >" + strKeysDesc + "</option>");
			}else{
				out.println("<option value=\"" + KeysgroupId + "\">" + strKeysDesc + "</option>");
			}
		}
	}
%>
																</select>
															</td>
														</tr>
<%
	if ( Terminal.getSiteId().length() > 0 && StringUtil.toString(Terminal.getTerminalType()).length() == 0 )
	{
		Terminal t2 = new Terminal();
		t2.setMerchantId(Terminal.getMerchantId());
		t2.setSiteId(Terminal.getSiteId());
		t2.getTerminalInfo();
		Terminal.setTerminalType(t2.getTerminalType());
	}
	if (( Terminal.getSiteId().length() == 0 || (Terminal.getSiteId().length() > 0 && Terminal.IsTerminalInMBList(application, Terminal.getTerminalType())) ) )
	{
		String sTRDisplay = SessionData.checkPermission(DebisysConstants.PERM_EDIT_MB_CONFIGURATIONS)?"":"display:none";
%>
														<tr id = "MBConfigurationRow" class="main" style="<%=sTRDisplay%>">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customer.terminal.MBConfiguration",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<select name="MBConfigurationID" <%=(SessionData.checkPermission(DebisysConstants.PERM_EDIT_MB_CONFIGURATIONS)?"":"disabled")%>>
<%
		{
			Iterator<Vector<String>> it = Terminal.getMBConfigurations().iterator();
			while (it.hasNext())
			{
				Vector<String> vecTemp = it.next();
				out.println("<option value=\"" + vecTemp.get(0) + "\" " + ( vecTemp.get(0).equals(Terminal.getMBConfigurationID())?"selected":"") + ">" + vecTemp.get(1) + "</option>");
			}
		}
%>
																</select>
															</td>
<%
		if ( Terminal.getSiteId().length() > 0 && Terminal.getMBConfigurationID() != null && SessionData.checkPermission(DebisysConstants.PERM_EDIT_MB_CONFIGURATIONS) )
		{
%>
															<td>
																<script>
																function OpenMBParams(){
																	window.open("/support/admin/tools/mbconfigurations.jsp?option=edit&mconfID=<%=Terminal.getMBConfigurationID()%>&SiteID=<%=Terminal.getSiteId()%>&mid=<%=Terminal.getMerchantId()%>", "<%=Terminal.getSiteId()%>", "left=" + ((screen.width - 550) / 2) + ", top=" + ((screen.height - 480) / 2) + ", width=550, height=480, menubar=0, toolbar=0, resizable=1, scrollbars=1");
																}
																</script>
															</td>
															<td colspan="3" nowrap="nowrap"><a href="javascript:" onclick="OpenMBParams(<%=Terminal.getSiteId()%>);"><%=Languages.getString("jsp.admin.customer.terminal.AdditionalMBParams",SessionData.getLanguage())%></a></td>
<%
		}
%>
														</tr>
<%
	}
%>
														<tr class="main">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.sim",SessionData.getLanguage())%></td>
															<td></td>
															<td><input type="text" id="simData" name="simData" value="<%=Terminal.getSimData()%>" size="26" maxlength="15" <%=disableControlsMB%> <%=termDisabled %>></td>
															<td></td>
															<td id="tdTerminalImci"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.imci",SessionData.getLanguage())%></td>
															<td></td>
															<td valign="middle"  >
															  <div >
																<input type="text" id="imciData" name="imciData" value="<%=Terminal.getImciData()%>" 
																		   size="15" maxlength="20" <%=disableControlsMB%> <%=termDisabled %>>
																 <!--  
																 <a rel="tooltip" title="IMSI(International Mobile Subscriber Identity) is a unique identification associated with all GSM and UMTS network mobile phone users.">
																 	<img  src="images/help.gif" />
																 </a>
																 -->
															  </div>													
															</td>
														</tr>
 <% 
 if ( deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
 {
 	ArrayList<CarriersMobile> carriers = CarriersMobile.getCarriers();
 	StringBuffer comboCarriersMobile = new StringBuffer();
 	boolean carrierAsigned=false;
 	for(CarriersMobile carriersMobile : carriers)
 	{
 		if ( Terminal.getCarrierId().equals(carriersMobile.getId()) )
 		{
 			comboCarriersMobile.append("<option selected value=\""+carriersMobile.getId()+"\">"+carriersMobile.getName()+"</option>");
 			carrierAsigned = true;
 		}
 		else
 		{
 			comboCarriersMobile.append("<option value=\""+carriersMobile.getId()+"\">"+carriersMobile.getName()+"</option>");
 		} 		
 	}
 	if ( !carrierAsigned ){
 		comboCarriersMobile.append("<option selected value=\"-1\">----</option>");
 	}
 	else
 	{
 		comboCarriersMobile.append("<option value=\"-1\">----</option>");
 	}
 	
 %>														
				<tr id="trTerminalCarrier" class="main" <%=termDisabled %> >
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td colspan="2"><%=Languages.getString("jsp.admin.terminal.carrier",SessionData.getLanguage())%></td>
					<td>
						<select id="carrierId" name="carrierId" >
							<%=comboCarriersMobile.toString()%>
						</select>
					</td>											 
				</tr>
<%
  }
  else{
  %>
  <tr id="trTerminalCarrier" class="main">
	<td><input type="hidden" id="carrierId" name="carrierId" value="<%=Terminal.getCarrierId()%>"/></td>
  </tr>	
  <% 
  }
  if ( Terminal.getSiteId().length() > 0 ){
%>
														<tr class="main">
															<td ><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.softwareversion",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="text" id="softwareVersion" name="softwareVersion"value="<%=Terminal.getSoftwareVersion()%>" size="26" maxlength="30" readonly="readonly" <%=termDisabled %>>
															</td>
														</tr>
<%
	}
	if ( !customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ){
%>
														<tr class="main" id="trMonthlyFee">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.monthly_fee",SessionData.getLanguage())%></td>
															<td></td>
															<td><input type="text" name="monthlyFee" value="<%=Terminal.getMonthlyFee()%>" size="6" maxlength="6" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("monthlyFee")) out.print("<font color=ff0000>&nbsp;*</font>");%>&nbsp;(<%=Languages.getString("jsp.admin.optional",SessionData.getLanguage())%>)</td>
															<td></td>
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_edit.monthly_fee_threshold",SessionData.getLanguage())%></td>
															<td></td>
															<td><input type="text" name="monthlyFeeThreshold" value="<%=Terminal.getMonthlyFeeThreshold()%>" size="8" maxlength="15" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("monthlyFeeThreshold")) out.print("<font color=ff0000>*</font>");%>&nbsp;(<%=Languages.getString("jsp.admin.optional",SessionData.getLanguage())%>)</td>
														</tr>
<%
	}
	if ( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (Terminal.getSiteId().length() == 0) ){
		String otherChars = "";//Terminal.getOtherAcceptedChars(application);
%>
														<tr><td colspan="7"><hr width="70%"></td></tr>
														<tr class="main">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_nameMx",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="text" id="clerkName" name="clerkName" value="<%=Terminal.getClerkName()%>" size="26" maxlength="16" <%=termDisabled %>><!-- onblur="ValidateClerkName(this) -->
																<script>
																	function ValidateClerkName(c){
																		var exp = new RegExp("^([a-z A-Z <%=otherChars%>])+$");
																		if ( !exp.test(c.value) && (c.value.length > 0) ){
																			alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidclerkname",SessionData.getLanguage())%>');
																			window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																			return false;
																		}
																		return true;
																	}
																</script>
															</td>
															<td></td>
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_lastNameMx",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="text" id="clerkLastName" name="clerkLastName" value="<%=Terminal.getClerkLastName()%>" size="15" maxlength="24" <%=termDisabled %>><% // onblur="ValidateClerkLastName(this) %>
																<script>
																	function ValidateClerkLastName(c){
																		var exp = new RegExp("^([a-z A-Z <%=otherChars%>])+$");
																		if ( !exp.test(c.value) && (c.value.length > 0) ){
																			alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidclerklastname",SessionData.getLanguage())%>');
																			window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																			return false;
																		}
																		return true;
																	}
																</script>
															</td>
														</tr>
														<tr class="main">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_codeMx",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="text" id="clerkCode" name="clerkCode" value="<%=Terminal.getClerkCode()%>" size="26" maxlength="10" onblur="ValidateClerkCode(this)" <%=termDisabled %>>
																<script>
																	function ValidateClerkCode(c){
																		var exp = new RegExp("^([0-9a-zA-Z])+$");
																		if ( !exp.test(c.value) && (c.value.length > 0) ){
																			alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidclerkcode",SessionData.getLanguage())%>');
																			window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																			return false;
																		}
																		return true;
																	}
																</script>
															</td>
														</tr>
<%
	}else if ( !customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (Terminal.getSiteId().length() == 0) ){
%>
														<tr><td colspan="7"><hr width="70%"></td></tr>
														<tr class="main">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_name",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="text" id="clerkName" name="clerkName" value="<%=Terminal.getClerkName()%>" size="26" maxlength="50" onblur="ValidateClerkName(this)" <%=termDisabled %>>
																<script>
																function ValidateClerkName(c){
																	var exp = new RegExp("^([a-zA-Z])+$");
																	if ( !exp.test(c.value) && (c.value.length > 0) ){
																		alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidclerkname",SessionData.getLanguage())%>');
																		window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																		return false;
																	}
																	return true;
																}
																</script>
															</td>
															<td></td>
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.clerk_code",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="text" id="clerkCode" name="clerkCode" value="<%=Terminal.getClerkCode()%>" size="15" maxlength="10" onblur="ValidateClerkCode(this)" <%=termDisabled %>>
																<script>
																	function ValidateClerkCode(c){
																		var exp = new RegExp("^([0-9a-zA-Z])+$");
																		if ( !exp.test(c.value) && (c.value.length > 0) ){
																			alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidclerkcode",SessionData.getLanguage())%>');
																			window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																			return false;
																		}
																		return true;
																	}
																</script>
															</td>
														</tr>
<%
	}
%>
<%
	if ( SessionData.checkPermission(DebisysConstants.PERM_NOTES) && strAccessLevel.equals(DebisysConstants.ISO)  ){
		
%>
														<tr class="main">
															<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.term_notes",SessionData.getLanguage())%></td>
															<td></td>
															<td><textarea rows="5" cols="20" ID="termNote" name="termNote" <%=termDisabled %>><%=Terminal.getTermNote()%></textarea>&nbsp;(<%=Languages.getString("jsp.admin.optional",SessionData.getLanguage())%>)</td>
															<td></td>
														</tr>
<%
	}
	
	if ( SessionData.checkPermission(DebisysConstants.PERM_EDIT_CACHED_RECEIPTS) && 
			strAccessLevel.equals(DebisysConstants.ISO) && 
				!merchantUseNewRatePlanModel && 
					Terminal.IsTerminalInMBList(application, Terminal.getTerminalType()) )
	{		
		List<Base> listTypes = Terminal.getReceiptTypes( SessionData.getUser().getLanguageCode() );
		
	%>
			<tr class="main">
				<td colspan="2" align="right">
					<tr>
						<td id="cachereceipts" class="main" style="visibility:<%=hiddenTrMarketPlaceRow%>">
							<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.term_cachereceipts",SessionData.getLanguage())%></td>
						<td></td>
						<td id="cachereceiptscontrol" class="main" style="visibility:<%=hiddenTrMarketPlaceRow%>">
							<input type="checkbox" Id="cachedReceipts" name="cachedReceipts" <%=Terminal.getEnbCachedRcp()==true?"checked=\"yes\"":""%> <%=termDisabled %>>
						</td>	
					</tr>
					<tr>
						<td id="cachereceiptsTypes" class="main" style="visibility:<%=hiddenTrMarketPlaceRow%>">
							<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.term_cachereceiptsType",SessionData.getLanguage())%>
						</td>
						<td></td>
						<td id="cachereceiptsTypescontrol" name="cachereceiptsTypescontrol" class="main" style="visibility:<%=hiddenTrMarketPlaceRow%>">
							<select id="cachereceiptsTypesSelect" name="cachereceiptsTypesSelect">
								<%for(Base receiptTypes : listTypes)
								  {	
								  	String receTypeId = receiptTypes.getId();
								  	String receDescr  = receiptTypes.getDescription();
								  	String receiptTypeSelected="";
								  	if ( Terminal.getCacheRecetiptType().equals(receTypeId) )
								  		receiptTypeSelected = "selected";
									%>
									<option value="<%=receTypeId%>" <%=receiptTypeSelected%> ><%=receDescr%></option>
									<%
								
								}%>
								
							</select>
						</td>	
					</tr>
				</td>
				
			</tr>
	<%	
	}
	else
	{
	  	%>
	  	<tr class="main">
			<td colspan="2" align="right">
			   <input type="hidden" Id="cachedReceipts" name="cachedReceipts"  value="<%=(Terminal.getEnbCachedRcp()==true?"on":"")%>" />
	  		   <input type="hidden" Id="cachereceiptsTypesSelect" name="cachereceiptsTypesSelect" value="<%=Terminal.getCacheRecetiptType()%>" />
	  		</td>
	  	</tr>
	  	<%
	}
	
	if(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_EDIT_SALES_FORCE_ID) ){
		String sfid = "";
		if (Terminal.getSiteId().length() > 0){
			sfid = Terminal.getSFID();
		}else if(request.getParameter("sfid") != null){
			sfid = (String)request.getParameter("sfid");
		}
%>
														<tr class="main">
															<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.editSFID",SessionData.getLanguage())%>*:</td>
															<td></td>
															<td colspan="4" align="left"><input type="text" name="sfid" value="<%=sfid%>" <%=termDisabled %>/>&nbsp;<font size="1">*<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.editSFIDNote",SessionData.getLanguage())%></font></td>
														</tr>
			<%
	}
	
	if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
%>
														<tr><td colspan="7"><hr width="70%"></td></tr>
														<tr class="main">
															<td colspan="7">
																<table class="main">
																	<tr>
																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.additonal_fee",SessionData.getLanguage())%></td>
																		<td>&nbsp;</td>
																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.fee_title",SessionData.getLanguage())%></td>
																		<td>&nbsp;</td>
																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.tax_title",SessionData.getLanguage())%></td>
																	</tr>
																	<tr>
																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.annual_insurance",SessionData.getLanguage())%></td>
																		<td></td>
																		<td><input type="text" name="annualInsuranceFee" value="<%=Terminal.getAnnualInsuranceFee()%>" size="8" maxlength="15" onblur="validateInteger(this);" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("annualInsuranceFee")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
																		<td></td>
																		<td><input type="text" name="annualInsuranceTax" value="<%=Terminal.getAnnualInsuranceTax()%>" size="8" maxlength="15" onblur="validateInteger(this);" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("annualInsuranceTax")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
																	</tr>
																	<tr>
																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.annual_maintenance",SessionData.getLanguage())%></td>
																		<td></td>
																		<td><input type="text" name="annualMaintenanceFee" value="<%=Terminal.getAnnualMaintenanceFee()%>" size="8" maxlength="15" onblur="validateInteger(this);" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("annualMaintenanceFee")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
																		<td></td>
																		<td><input type="text" name="annualMaintenanceTax" value="<%=Terminal.getAnnualMaintenanceTax()%>" size="8" maxlength="15" onblur="validateInteger(this);" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("annualMaintenanceTax")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
																	</tr>
																	<tr>
																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.reconnection",SessionData.getLanguage())%></td>
																		<td></td>
																		<td><input type="text" name="reconnectionFee" value="<%=Terminal.getReconnectionFee()%>" size="8" maxlength="15" onblur="validateInteger(this);" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("reconnectionFee")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
																		<td></td>
																		<td><input type="text" name="reconnectionTax" value="<%=Terminal.getReconnectionTax()%>" size="8" maxlength="15" onblur="validateInteger(this);" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("reconnectionTax")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
																	</tr>
																	<tr>
																		<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.annual_affiliation",SessionData.getLanguage())%></td>
																		<td></td>
																		<td><input type="text" name="annualAffiliationFee" value="<%=Terminal.getAnnualAffiliationFee()%>" size="8" maxlength="15" onblur="validateInteger(this);" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("annualAffiliationFee")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
																		<td></td>
																		<td><input type="text" name="annualAffiliationTax" value="<%=Terminal.getAnnualAffiliationTax()%>" size="8" maxlength="15" onblur="validateInteger(this);" <%=termDisabled %>><%if (terminalErrors != null && terminalErrors.containsKey("annualAffiliationTax")) out.print("<font color=ff0000>&nbsp;*</font>");%></td>
																	</tr>
																</table>
															</td>
														</tr>
<%
		} //End of if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
	if ( SessionData.checkPermission(DebisysConstants.PERM_ADD_EDIT_TERMINAL_LABEL)) {
%>
														<tr>
															<td><%=Languages.getString("jsp.admin.customers.merchants_info.terminal_Label",SessionData.getLanguage())%></td>
															<td></td>
															<td><input type="text" name="terminalLabel" value="<%=Terminal.getTerminalLabel()%>" <%=termDisabled %> size="20" maxlength="50">&nbsp;(<%=Languages.getString("jsp.admin.optional",SessionData.getLanguage())%>)</td>
															
														</tr>
<%
	}
%>
<%
	if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE) ){
		Merchant mer = new Merchant();
		mer.setMerchantId(Terminal.getMerchantId(Terminal.getSiteId()));
		mer.getMerchant(SessionData,application);
		Terminal.setTerminalId(Terminal.getTerminalIdBySite(Terminal.getSiteId()));
		if (com.debisys.terminals.Terminal.IsTerminalInMBList(application, Terminal.getTerminalType()) && mer.isPinCacheEnabled()){
			ArrayList<PcAssignmentTemplate> templates = PcAssignmentTemplate.getAssignableTemplates(Terminal.getSiteId());
%>
														<script>
															errorMsg = '<%=Languages.getString("jsp.admin.pincache.requesterror",SessionData.getLanguage())%>';
															terminal_id = '<%=Terminal.getTerminalId()%>';
															saveButText = '<%=Languages.getString("jsp.admin.pincache.ok",SessionData.getLanguage())%>';
															cancelButText = '<%=Languages.getString("jsp.admin.pincache.cancel",SessionData.getLanguage())%>';
														</script>
														<tr><td colspan="7"><hr width="70%"></td></tr>
														<tr class="main">
															<td><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincache",SessionData.getLanguage())%></td>
															<td></td>
															<td colspan="5">
																<label>
																	<input id="enablePINCache" type="radio" name="enablePINCache" value="true" <%=(Terminal.isEnablePINCache())?"checked":""%> <%=termDisabled %>>
																	&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincacheenabled",SessionData.getLanguage())%>
																</label>
																&nbsp;&nbsp;&nbsp;
																<label>
																	<input type="radio" id="disablePINCache" name="enablePINCache" value="false" <%=(Terminal.isEnablePINCache())?"":"checked"%> <%=termDisabled %>>
																	&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pincachedisabled",SessionData.getLanguage())%>
																</label>
																<div id="selectPincacheTemplate">
																	<h4><%=Languages.getString("jsp.admin.pincache.usetemplate",SessionData.getLanguage())%></h4>
<%
			PcAssignmentTemplate ptt = PcAssignmentTemplate.getByTerminalId(Terminal.getTerminalId());
			if(templates != null && !templates.isEmpty()){
%>																	<select id="pincache_template_id" name="pincache_template_id">
																		<option value=""></option>
<%
				for(PcAssignmentTemplate t: templates){
%>
																			<option value="<%=t.getId()%>" 
																			<%
																			if(t.getId() == ptt.getId()){
																				%>"selected"<%
																			}else{ 
																				%>""<%
																			}
																			%>><%=t.getName()%></option>
<%
				}
%>
																		
																	</select>
<%
			}else{
%>
																			<div class="warning">
																				<%=Languages.getString("jsp.admin.pincache.notemplates",SessionData.getLanguage())%>
																				<a id="managetemplates" href="admin/pincache/template.jsp"><%=Languages.getString("jsp.admin.pincache.templates", SessionData.getLanguage()) %></a>
																			</div>
<%
			}
%>
																</div>
															</td>
														</tr>
														<tr class="main" id="pincacheTemplate">
															<td><label><%=Languages.getString("jsp.admin.pincache.usetemplate",SessionData.getLanguage())%></label></td>
															<td colspan="3" id="pincacheTemplateName">
<%
															if(ptt != null){
																%><%=(ptt.getName() != null ? ptt.getName(): "")%><%
															}else{
%>
																<div class="warning"><%=Languages.getString("jsp.admin.pincache.notemplates",SessionData.getLanguage())%></div>
<%
															}
%>
															</td>
															<td colspan="3">
																<button id="pincacheProductList" data-templateid="<%=(ptt != null ? ptt.getId(): "")%>"><%=Languages.getString("jsp.admin.pincache.viewdetail",SessionData.getLanguage())%></button>
<%
			if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE_ADV)) {
%>
																<button id="pincacheManageBtn" data-siteid="<%=Terminal.getSiteId()%>"><%= Languages.getString("jsp.admin.pincache.goadmin",SessionData.getLanguage()) %></button>
																<button id="pincacheSyncHistory" data-siteid="<%=Terminal.getSiteId()%>"><%= Languages.getString("jsp.admin.pincache.reportsync",SessionData.getLanguage()) %></button>
<%
			}
%>
															</td>
														</tr>
<%
		}
	}
%>
													</table>
												</td>
											</tr>
										</table>

	
	<%
	session.setAttribute("terminalMappings",null);
	if ( Terminal.getSiteId().length() > 0 && permissionToTerminalMappingsAndAccessLevel )
	{
	%>
	<table align="center" width="100%">		
		<tr class="main">
			<td colspan="5">
				<jsp:include page="terminals/mapping/terminalMapping.jsp" >
					<jsp:param value="<%=strRefId%>" name="strRefId"/>
					<jsp:param value="<%=Terminal.getSiteId()%>" name="siteId"/>
				</jsp:include>		
			</td>			
		</tr>	
	</table>
		
	<%}
	else if ( Terminal.getSiteId().length() == 0 && permissionToTerminalMappingsAndAccessLevel ) 
	{
	%>
	<table align="center" width="100%">		
		<tr class="main">
			<td colspan="5">
				<jsp:include page="terminals/mapping/terminalMappingNew.jsp" >
					<jsp:param value="<%=strRefId%>" name="strRefId"/>					
				</jsp:include>		
			</td>			
		</tr>	
	</table>
		
	<%
	}%>
	
	<% 	
	if ( Terminal.getSiteId().length() == 0 )
	{
		StringBuilder comboBrandings = new StringBuilder();
		Iterator itBrands = Terminal.getPCTerminalBrands(strRefId).iterator();
		comboBrandings.append("<option value=''>" + Languages.getString("jsp.admin.customers.terminal.pc_term_brand_select",SessionData.getLanguage()) + "</option>");
		while ( itBrands.hasNext() )
		{
			Vector vTemp = (Vector)itBrands.next();
			String sConfigID = vTemp.get(0).toString().trim();
			String sName = vTemp.get(1).toString().trim();
			if (sConfigID.equals(Terminal.getPcTermBrand())){
				comboBrandings.append("<option value=\"" + sConfigID + "\" selected>" + sName + "</option>");
			}else{
				comboBrandings.append("<option value=\"" + sConfigID + "\">" + sName + "</option>");
			}
		}
		
%>
			
			
										<table id="mxTablePCData" style="display:none;" width="100%">
											<tr id="pc_term_title_label" style="display:'';"><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pcterminal_info",SessionData.getLanguage())%></td></tr>
											<tr id="sms_terminal_title_label" style="display:none;"><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.smsterminal_info",SessionData.getLanguage())%></td></tr>
											<tr id="webservice_terminal_title_label" style="display:none;"><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.webserviceterminal_info",SessionData.getLanguage())%></td></tr>
											<tr>
												<td class="formArea2">
													<br>
													<table width="100%">
														<tr><td></td><td>&nbsp;</td><td></td><td>&nbsp;&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>
														<tr class="main" id="nameBoxPcTerm">
															<td nowrap="nowrap" ><%=Languages.getString("jsp.admin.customers.terminal.pc_term_brand",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<select id="pcTermBrand" name="pcTermBrand" <%=termDisabled %>>
																	<%=comboBrandings.toString()%>
																</select>
															</td>
														</tr>
														<tr class="main" >
															<td nowrap="nowrap" ID="user_name_label" style="display:'';" > 
																<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pc_term_username",SessionData.getLanguage())%>
															</td>
															<td nowrap="nowrap" ID="user_id_label" style="display:none;" > 
																<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.sms_userId",SessionData.getLanguage())%>
															</td>
															<td nowrap="nowrap" ID="webserviceuser_name_label" style="display:none;" > 
																<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.webservice_userName",SessionData.getLanguage())%>
															</td>
															<td></td>
															<td nowrap="nowrap"><input type="text" ID="pcTermUsername" name="pcTermUsername" value="<%=Terminal.getPcTermUsername()%>" size="20" maxlength="15" <%=termDisabled %>></td>
															<td></td>
															<td nowrap="nowrap" ID="pc_term_pwd_label" style="display:'';"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pc_term_password",SessionData.getLanguage())%></td>
															<td nowrap="nowrap" ID="sms_pwd_label" style="display:'none';" ><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.sms_password",SessionData.getLanguage())%></td>
															<td nowrap="nowrap" ID="webservice_pwd_label" style="display:'none';" ><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.webservice_password",SessionData.getLanguage())%></td>
															<td></td>
															<td>
                                                                                                                            <input type="password" ID="pcTermPassword" name="pcTermPassword" value="<%=Terminal.getPcTermPassword()%>" size="20" maxlength="15" onblur="return ValidatePCTermPassword(this);" <%=termDisabled %>>
                                                                                                                            <p class="errorLabelPass"><%=Languages.getString("jsp.admin.customers.terminal.invalid.password1",SessionData.getLanguage())%></p>
																<script>
																	function ValidatePCTermPassword(c){
																		if(webServiceValidation)
																		  var exp = new RegExp("^([a-zA-Z0-9\$])+$");
																		else
																			 var exp = new RegExp("^([a-zA-Z0-9\])+$");
																		if ( !exp.test(c.value) && (c.value.length > 0) ){
																			alert('<%=Languages.getString("jsp.admin.customers.terminal.invalidpassword",SessionData.getLanguage())%>');
																			window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																			return false;
																		}
																		return true;
																	}
																</script>
															</td>
														</tr>

														<tr ><td colspan="7" ID="lineBeforeEnforceRegCode"><hr width="70%"></td></tr>
														<tr class="main" ID="rowEnforceRegCode"><td colspan="3"><label><%=Languages.getString("jsp.admin.customers.terminal.enforce_regcode",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkRegCode" name="chkRegCode" onclick="document.getElementById('fieldsetEnforceConfirmationByUser').disabled = !this.checked;" <%=termDisabled %>></label></td></tr>
                                                        <tr class="main" ID="enforceConfirmationByEmail">
                                                            <td colspan="7">
                                                                <fieldset id="fieldsetEnforceConfirmationByUser" disabled style="margin:7px;">
                                                                    <legend><%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationOptions",SessionData.getLanguage())%></legend>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="checkbox" id="chkConfirmationByEmail" name="chkConfirmationByEmail" onclick="document.getElementById('txtConfirmationEMail').disabled = !this.checked;" <%=termDisabled %>>&nbsp;
                                                                                <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationByEmail",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <%=Languages.getString("jsp.admin.customers.terminal.enforce_inputConfirmationEmail",SessionData.getLanguage())%>
                                                                                <input type="text" id="txtConfirmationEMail" name="txtConfirmationEMail" value="<%=Terminal.getTxtConfirmationEMail()%>" size="50" maxlength="100" disabled="disabled" <%=termDisabled %>>
                                                                            </td>
                                                                        </tr>
                                                                        <%
                                                                            String supportInstance = DebisysConfigListener.getInstance(application);
                                                                            String enableSmsNotification = Properties.getPropertyValue(supportInstance, "global", "EnableSmsNotification");
                                                                            boolean isActiveSmsNotification = (enableSmsNotification != null && !enableSmsNotification.isEmpty() && enableSmsNotification.equalsIgnoreCase("true")) ? true : false;
                                                                            if(isActiveSmsNotification){
                                                                        %>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="checkbox" id="chkConfirmationBySMS" name="chkConfirmationBySMS" onclick="document.getElementById('txtConfirmationPhoneNumber').disabled = !this.checked;" <%=termDisabled %>>&nbsp;
                                                                                <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationBySMS",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <%=Languages.getString("jsp.admin.customers.terminal.enforce_inputConfirmationPhoneNumber",SessionData.getLanguage())%>
                                                                                <input type="text" id="txtConfirmationPhoneNumber" name="txtConfirmationPhoneNumber" value="<%=Terminal.getTxtConfirmationPhoneNumber()%>" size="40" maxlength="50" disabled="disabled" <%=termDisabled %>>
                                                                            </td>
                                                                        </tr>
                                                                        <%
                                                                            }
                                                                        %>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <input type="checkbox" id="chkConfirmationByUser" name="chkConfirmationByUser" <%=termDisabled %>>&nbsp;
                                                                                <%=Languages.getString("jsp.admin.customers.terminal.enforce_confirmationByMerchant",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="main" ID="rowEnterRegCode">
															<td colspan="7">
																<label><%=Languages.getString("jsp.admin.customers.terminal.enter_manual_code",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkManualRegCode" name="chkManualRegCode" onclick="document.getElementById('txtRegistration').disabled = !this.checked;document.getElementById('txtRegistration').value = '';" <%=termDisabled %>></label>
																<br><i><%=Languages.getString("jsp.admin.customers.terminal.enter_manual_code_summary",SessionData.getLanguage())%></i>
															</td>
														</tr>
														<tr class="main" ID="rowBoxEnterRegCode">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.registrationcode",SessionData.getLanguage())%></td>
															<td></td>
															<td><input type="text" ID="txtRegistration" name="txtRegistration" disabled="disabled" value="<%=Terminal.getTxtRegistration()%>" size="20" maxlength="15" <%=termDisabled %>></td>
														</tr>
														<tr ID="lineBeforeIPRest"><td colspan="7" ><hr width="70%"></td></tr>
														<tr class="main">
															<td colspan="7">
																<label><%=Languages.getString("jsp.admin.customers.terminal.enforce_iprestriction",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkIPRestriction" name="chkIPRestriction" onclick="document.getElementById('txtIPAddress').disabled = document.getElementById('txtSubnetMask').disabled = !this.checked;" <%=termDisabled %>></label>
																<br><i><%=Languages.getString("jsp.admin.customers.terminal.enforce_iprestriction_staticip",SessionData.getLanguage())%></i>
															</td>
														</tr>
														<tr class="main">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.ipaddress",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="text" ID="txtIPAddress" name="txtIPAddress" value="<%=Terminal.getTxtIPAddress()%>" size="20" maxlength="15" disabled="disabled" onblur="return ValidateIPAddress(this);" <%=termDisabled %>>
																<script>
																	function ValidateIPAddress(c){
																		var exp = new RegExp("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
																		if ( !exp.test(c.value) && (c.value.length > 0) ){
																			alert('<%=Languages.getString("jsp.admin.invalidipaddress",SessionData.getLanguage())%>');
																			window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																			return false;
																		}
																		if ( c.value.length > 0 ){
																			var v = c.value.split(".");
																			for (var s in v){
																				if ( parseInt(v[s]) > 255 ){
																					alert('<%=Languages.getString("jsp.admin.invalidipaddress",SessionData.getLanguage())%>');
																					window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																					return false;
																				}
																			}
																		}
																		return true;
																	}//End of function ValidateIPAddress
																</script>
															</td>
															<td></td>
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.terminal.subnetmask",SessionData.getLanguage())%></td>
															<td></td>
															<td>
																<input type="text" ID="txtSubnetMask" name="txtSubnetMask" value="<%=Terminal.getTxtSubnetMask()%>" size="20" maxlength="19" disabled="disabled" onblur="return ValidateSubnetMask(this);" <%=termDisabled %>>
																<script>
																	function ValidateSubnetMask(c){
																		var exp = new RegExp("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})(\\/(\\d{1,3}))*$");
																		if ( !exp.test(c.value) && (c.value.length > 0) ){
																			alert('<%=Languages.getString("jsp.admin.invalidsubnetmask",SessionData.getLanguage())%>');
																			window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																			return false;
																		}
																		if ( c.value.length > 0 ){
																			var v = c.value.split("/");
																			v = v[0].split(".");
																			for (var s in v){
																				if ( parseInt(v[s]) > 255 ){
																					alert('<%=Languages.getString("jsp.admin.invalidsubnetmask",SessionData.getLanguage())%>');
																					window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
																					return false;
																				}
																			}
																		}
																		return true;
																	}//End of function ValidateSubnetMask
																</script>
															</td>
														</tr>
														<tr ID="lineBeforeEnforceCertificate"><td colspan="7"><hr width="70%"></td></tr>
                                                                                                                <tr class="main" ID="enforceIsAdminCheck"><td colspan="3"><label id="idLabelIsAdmin" style="display: none;"><%=Languages.getString("jsp.admin.customers.terminal.pcterminaluser.isAdmin",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkIsAdmin" name="chkIsAdmin" <%=termDisabled %>></label></td></tr>
														<tr ID="lineBeforeEnforceCertificate"><td colspan="7"><hr width="70%"></td></tr>
                                                                                                                <tr class="main" ID="enforceCertificateCheck"><td colspan="3"><label><%=Languages.getString("jsp.admin.customers.terminal.enforce_certificate",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkCertificate" name="chkCertificate" <%=termDisabled %>></label></td></tr>
														<tr class="main" ID="allowInstallCertificateCheck"><td colspan="3"><label><%=Languages.getString("jsp.admin.customers.terminal.allow_certificate_install",SessionData.getLanguage())%>&nbsp;<input type="checkbox" id="chkAllowCertificate" name="chkAllowCertificate" onclick="document.getElementById('txtAllowCertificate').disabled = !this.checked;" <%=termDisabled %>></label></td></tr>
														<tr class="main" ID="certificateInstallAmount">
															<td colspan="3">
																<%=Languages.getString("jsp.admin.customers.terminal.certificate_amount",SessionData.getLanguage())%>&nbsp;
																<input type="text" ID="txtAllowCertificate" name="txtAllowCertificate" value="<%=Terminal.getTxtAllowCertificate()%>" size="5" maxlength="3" disabled="disabled" <%=termDisabled %>>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
<%
		if ( Terminal.isChkRegCode()){
			out.print("<script>document.getElementById('chkRegCode').click();</script>");
		}
		if ( Terminal.isChkManualRegCode()){
			out.print("<script>document.getElementById('chkManualRegCode').click();</script>");
		}
		if ( Terminal.isChkIPRestriction() ){
			out.print("<script>document.getElementById('chkIPRestriction').click();</script>");
		}
		if ( Terminal.isChkCertificate() ){
			out.print("<script>document.getElementById('chkCertificate').click();</script>");
		}
		if ( Terminal.isChkAllowCertificate() ){
			out.print("<script>document.getElementById('chkAllowCertificate').click();</script>");
		}
		//End of if ( Terminal.getSiteId().length() == 0 )
	}else{
		//Else we are editing a terminal
		Vector vTerminalTypes = com.debisys.terminals.Terminal.getTerminalTypes_Combo("Debisys");
		Iterator it = vTerminalTypes.iterator();
		while (it.hasNext()){
			Vector vTmp = (Vector) it.next();
			
			if (vTmp.get(0).toString().equals(Terminal.getTerminalType()) && (vTmp.get(1).toString().startsWith("Nokia")
			|| vTmp.get(1).toString().equalsIgnoreCase("SmartPhoneAndroid")|| vTmp.get(1).toString().equalsIgnoreCase("SmartPhoneIPhone")))
			{
				Vector vResults = (Vector)Terminal.getPCTerminalRegistrations("", application).get(0);
				int idTerminalRegistration  = Integer.parseInt(vResults.get(0).toString());
				Terminal.setTxtRegistration(vResults.get(2).toString());
		        Terminal.setPcTermUsername(vResults.get(5).toString());
		        Terminal.setPcTermPassword(vResults.get(6).toString());
		        Terminal.setPcTermBrand(vResults.get(10).toString());
		        
				StringBuilder comboBrandings = new StringBuilder();
				Iterator itBrands = Terminal.getPCTerminalBrands(strRefId).iterator();
				comboBrandings.append("<option value=''>" + Languages.getString("jsp.admin.customers.terminal.pc_term_brand_select",SessionData.getLanguage()) + "</option>");
				while ( itBrands.hasNext() )
				{
					Vector vTemp = (Vector)itBrands.next();
					String sConfigID = vTemp.get(0).toString().trim();
					String sName = vTemp.get(1).toString().trim();
					if ( sConfigID.equals( Terminal.getPcTermBrand() ) )
					{
						comboBrandings.append("<option value=\"" + sConfigID + "\" selected>" + sName + "</option>");
					}
					else
					{
						comboBrandings.append("<option value=\"" + sConfigID + "\">" + sName + "</option>");
					}
				}
			 %>
			 	<table id="tableJavaHandset0" width="100%">
						<tr>
							<td width="100%">
								<table width="100%">
									<tr >
										<td >
											<jsp:include page="terminals/insertUpdateJavaHandSet.jsp" >
												<jsp:param value="<%=comboBrandings.toString()%>" name="comboBrandings"/>
												<jsp:param value="<%=strRefId%>" name="strRefId"/>
												<jsp:param value="<%=idTerminalRegistration%>" name="IdTerminalRegistration"/>
												<jsp:param value="1" name="show"/>
												<jsp:param value="<%=Terminal.getPcTermUsername()%>" name="userTerminal"/>
												<jsp:param value="<%=Terminal.getPcTermPassword()%>" name="passwordTerminal"/>
												<jsp:param value="<%=Terminal.getPcTermBrand()%>" name="brandTerminal"/>
												<jsp:param value="<%=Terminal.getTxtRegistration()%>" name="txtRegistration"/>
												<jsp:param value="<%=Terminal.getSiteId()%>" name="siteId"/>
											</jsp:include>											
										</td>																			
									</tr>
								</table>
							</td>
						</tr>
				</table>
				<script type="text/javascript">
				 
				 $(document).ready(function(){
				   $("#mailNotificationT").show();
				 });
				</script>
			 <% 			 
			}
			
			if (vTmp.get(0).toString().equals(Terminal.getTerminalType()) && vTmp.get(1).toString().equals("PC Terminal")){
%>
										<table width="100%">
											<tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pcterminal_info",SessionData.getLanguage())%></td></tr>
											<tr>
												<td class="formArea2">
													<br>
													<table width="100%">
														<tr class="main">
															<td>
																<iframe frameborder="0" id="ifPCTermUsers" src="/support/admin/customers/pcterm_users.jsp?siteId=<%=Terminal.getSiteId()%>&terminalVersionForPcTerminal=<%=terminalVersionForPcTerminal%>" height="101" width="1250"></iframe>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
<%			
}
                        if (vTmp.get(0).toString().equals(Terminal.getTerminalType()) && vTmp.get(1).toString().equals("SmartPhoneAndroid")) {
                            %>
                            <table>
                                <tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl1",SessionData.getLanguage())%></td></tr>
                                    <tr>
                                        <td class="formArea2">
                                            <br>
                                            <table>
                                                <tr class="main">
                                                    <td>
                                                        <iframe  style="height: 200px !important; overflow:auto;" frameborder="0" src="/support/admin/customers/smartphone_users.jsp?siteId=<%=Terminal.getSiteId()%>&terminalVersionForPcTerminal=<%=terminalVersionForPcTerminal%>" height="200" width="1000"></iframe>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                            </table>
                            <%
                        }
                        if (vTmp.get(0).toString().equals(Terminal.getTerminalType()) && vTmp.get(1).toString().equals("SmartPhoneIPhone")) {
                            %>
                            <table>
                                <tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.lbl1",SessionData.getLanguage())%></td></tr>
                                    <tr>
                                        <td class="formArea2">
                                            <br>
                                            <table>
                                                <tr class="main">
                                                    <td>
                                                        <iframe  style="height: 200px !important; overflow:auto;" frameborder="0" src="/support/admin/customers/smartphone_users.jsp?siteId=<%=Terminal.getSiteId()%>&terminalVersionForPcTerminal=<%=terminalVersionForPcTerminal%>" height="200" width="1000"></iframe>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                            </table>
                            <%
                        }
			//DJC R24 Iframe for SMS user Edition
			if (vTmp.get(0).toString().equals(Terminal.getTerminalType()) 
                               && (vTmp.get(1).toString().contains("SMS Phone ") 
                                || vTmp.get(1).toString().contains("USSD/SMS") || vTmp.get(1).toString().contains("Tablet") 
                                || vTmp.get(1).toString().contains("SMS Printer") 
                                || vTmp.get(1).toString().contains("Quick Sell App")
                                || vTmp.get(1).toString().contains("Infopyme Kiosk USSD"))){
%>
										<table width="100%">
											<tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.smsterminal_info",SessionData.getLanguage())%></td></tr>
											<tr>
												<td class="formArea2">
													<br>
													<table width="100%">
														<tr class="main">
															<td>
																<iframe frameborder="0" id="ifPCTermUsers" src="/support/admin/customers/smsterm_users.jsp?siteId=<%=Terminal.getSiteId()%>" height="100" width="1250"></iframe>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
<%
			} //End of if this terminal is of type SMS
			if (vTmp.get(0).toString().equals(Terminal.getTerminalType()) && vTmp.get(1).toString().contains("Web Service") && User.isWebServiceAutherticationTerminalsEnabled(SessionData, application)){
%>	
										<table width="100%">
											<tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.webserviceterminal_info",SessionData.getLanguage())%></td></tr>
												<tr>
													<td class="formArea2">
													<br>
													<table width="100%">
														<tr class="main">
															<td>
																<iframe frameborder="0" id="ifPCTermUsers" src="/support/admin/customers/webserviceterm_users.jsp?siteId=<%=Terminal.getSiteId()%>" height="100" width="1250"></iframe>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
<%
			}
			//DJC R24 Iframe for SMS user Edition
		}
	}//End of else we are editing a terminal
%>
										<table width="100%">
											<tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.terminalrate_info",SessionData.getLanguage())%></td></tr>
											<tr>
												<td class="formArea2">
													<br>
<%
		
		//*********************************************************
		//*********************************************************
		//Variables to manage the new rate plans model
		String RatePlanName="";
		String disableSplitsByChangeRatePlanModel="";
		Merchant merchantAux = new Merchant();
		
		String typeRatePlanName = Languages.getString("jsp.admin.iso_rateplan_name",SessionData.getLanguage());
		Boolean changeRatePlanModel=false;
		
		merchantAux.setMerchantId(Terminal.getMerchantId());
		merchantAux.getMerchant(SessionData, application);
		if ( merchantAux.isUseRatePlanModel() )
		{
			disableSplitsByChangeRatePlanModel=" disabled ";
			changeRatePlanModel=true;
			typeRatePlanName = Languages.getString("jsp.admin.rateplans.newmodel_plans",SessionData.getLanguage());
			RatePlanName="";
		}
		//*********************************************************
		//*********************************************************
	

	String repId= Terminal.getRepId();		
	String RateplanModel = Terminal.getRatePlan();
	String merchantId =	Merchant.getMerchantId();									
	// If we are creating a new terminal
	if ( Terminal.getSiteId().length() == 0 ){
%>
													<table>
														<tr class="main">
															<%if (!changeRatePlanModel)
															  {
															%>
															<td nowrap="nowrap">
																<label><%=Languages.getString("jsp.admin.customers.merchants_add.select_iso_rateplan.select",SessionData.getLanguage())%>&nbsp;
																<input type="radio" id="chkISORatePlan" name="chkRatePlan" value="ISO" onclick="SwitchRatePlan(this.value);" <%=(Terminal.getChkRatePlan().equals("ISO") || Terminal.getChkRatePlan().equals(""))?"checked":"" %> <%=termDisabled %>>
																</label>
															</td>
															<%}%>
															<td>&nbsp;</td>
															<td>
<%
		if (!changeRatePlanModel)
		{
					vISORatePlan = RatePlan.getISORatePlansG(strRefId, "0", "", null);
					if (vISORatePlan.get(2).size() > 0)
					{
%>
																<select id="isoRatePlanId" name="isoRatePlanId" style="display:<%=(Terminal.getChkRatePlan().equals("ISO"))?"inline":"none"%>;" onchange="SwitchRatePlan('ISO');" <%=termDisabled %>>
<%
			Iterator it = vISORatePlan.get(2).iterator();
			while (it.hasNext()){
				Vector vecTemp = (Vector)it.next();
				
				if ( vecTemp.get(0).equals(Terminal.getIsoRatePlanId()) ){
					out.println("<option value=" + vecTemp.get(0) +" selected>" + vecTemp.get(1) + "</option>");
				}else{
					out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
				}
			}
%>
																</select>
<%
		//End of if (vISORatePlan != null && vISORatePlan.size() > 0)
					}
					else
					{
			Vector<String> vTmp = new Vector<String>();
			vTmp.add("0");
			vTmp.add("");
			vISORatePlan = new Vector<Vector<Vector<String>>>();
			vISORatePlan.add(new Vector<Vector<String>>());
			vISORatePlan.add(new Vector<Vector<String>>());
			vISORatePlan.add(new Vector<Vector<String>>());
			vISORatePlan.get(2).add(vTmp);
%>
																<select id="isoRatePlanId" name="isoRatePlanId" style="display:<%=(Terminal.getChkRatePlan().equals("ISO"))?"inline":"none"%>;" disabled="disabled">
																	<option value="0"><%=Languages.getString("jsp.admin.customers.merchants_add.select_iso_rateplan.noplan",SessionData.getLanguage())%></option>
																</select>
				  <%}%>
															</td>
														</tr>
<%
			if ( User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) && strAccessLevel.equals(DebisysConstants.ISO) )
			{
%>
														<tr class="main">
															<td nowrap="nowrap"><label><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.select_rep_rateplan",SessionData.getLanguage())%>&nbsp;<input type="radio" id="chkRepRatePlan" name="chkRatePlan" value="REP" onclick="SwitchRatePlan(this.value);" <%=(Terminal.getChkRatePlan().equals("REP"))?"checked":""%> <%=termDisabled %>></label></td>
															<td></td>
															<td>
<%
			vRepRatePlan = RatePlan.getRepRatePlans(SessionData);
			if (vRepRatePlan != null && vRepRatePlan.size() > 0){
%>
																<select id="repRatePlanId" name="repRatePlanId" style="display:<%=(Terminal.getChkRatePlan().equals("REP") || Terminal.getChkRatePlan().equals(""))?"inline":"none"%>;" onchange="SwitchRatePlan('REP');" <%=termDisabled %>>
<%
				Iterator it = vRepRatePlan.iterator();
				while (it.hasNext()){
					Vector vecTemp = (Vector)it.next();
					if ( vecTemp.get(0).equals(Terminal.getRepRatePlanId()) ){
						out.println("<option value=" + vecTemp.get(0) +" selected>" + vecTemp.get(1) + "</option>");
					}else{
						out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
					}
				}
%>
																</select>
<%
			}else{
				Vector vTmp = new Vector();
				vTmp.add("0");
				vTmp.add("");
				vRepRatePlan = new Vector();
				vRepRatePlan.add(vTmp);
%>
																<select id="repRatePlanId" name="repRatePlanId" style="display:<%=(Terminal.getChkRatePlan().equals("REP") || Terminal.getChkRatePlan().equals(""))?"inline":"none"%>;" disabled="disabled">
																	<option value="0"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.select_rep_rateplan.noplan",SessionData.getLanguage())%></option>
																</select>
<%
			}
		}//End of if ( User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) )
%>
															</td>
														</tr>
													</table>
													<script>
														function SwitchRatePlan(sValue){
															
															$("#btnSubmit").attr('disabled', 'disabled');
															
															var sRateID = "";
															if ( sValue == "ISO" ){
																document.getElementById('isoRatePlanId').style.display = "inline";
<%
		if ( User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) && strAccessLevel.equals(DebisysConstants.ISO) ){
%>
																document.getElementById('repRatePlanId').style.display = "none";
<%
		}
%>
																sRateID = document.getElementById('isoRatePlanId').value;
															}else if ( sValue == "REP" ){
																document.getElementById('isoRatePlanId').style.display = "none";
																typeRatePlan="REP";
<%
		if ( User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) && strAccessLevel.equals(DebisysConstants.ISO) ){
%>
																document.getElementById('repRatePlanId').style.display = "inline";
<%
		}
%>
																sRateID = document.getElementById('repRatePlanId').value;
															}
															var sNew = '';
															$("#divRates").html("");
															$("#divRates").load("admin/customers/terminal_rates.jsp?PlanType=" + sValue + "&repId=" + <%=Merchant.getRepId()%> + "&merchantId=" + <%=Merchant.getMerchantId()%> + "&sRateID=" + sRateID + "&Random=" + Math.random()+"&sNew="+sNew);
														}
													</script>
													<br>
													<div id="divRates">
<%   }
	 else
	 {%>
	 			<jsp:include page="terminals/listrateplan.jsp" >
					<jsp:param value="<%=merchantId%>" name="merchantId"/>
					<jsp:param value="divNewRatesEmida" name="divName"/>
					<jsp:param value="-1" name="Rateplan"/>
					<jsp:param value="" name="RateplanModelName"/>
				</jsp:include>
				<tr>
					<td colspan="2">%
						<div id="divNewRatesEmida"></div>
					</td>
				</tr>
				
	<% 
	 }	
		// Scope limiting
		{
			if (!changeRatePlanModel)
			{
			Vector vRatePlan = new Vector();
			// Rendering a rep rate here?
			if ( (StringUtil.toString(request.getParameter("chkRatePlan")).equals("REP") || StringUtil.toString(request.getParameter("chkRatePlan")).equals(""))
					&& User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) && strAccessLevel.equals(DebisysConstants.ISO))
			{
%>
														<table width="100">
															<tr>
																<td class="rowhead2" width="100" nowrap="nowrap"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage())%></td>
																<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage())%></td>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.total",SessionData.getLanguage())%></td>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.rep",SessionData.getLanguage())%></td>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.merchant",SessionData.getLanguage())%></td>
															</tr>
<%
				if ( request.getParameter("repRatePlanId") != null ){
					RatePlan.setRepRatePlanId(request.getParameter("repRatePlanId"));
				}else{
					RatePlan.setRepRatePlanId(((Vector)vRepRatePlan.get(0)).get(0).toString());
				}
				vRatePlan = RatePlan.getProductsIsoRatePlan(SessionData); 
				Iterator it = vRatePlan.iterator();
				int intEvenOdd = 1;
				String strProductId = "";
				double dblTotalRate = 0;
				double dblRepRate = 0;
				double dblMerchantRate = 0;
				int icount=0;
				while (it.hasNext()){
					icount++;
					Vector vecTemp = (Vector)it.next();
					strProductId = vecTemp.get(1).toString();
					dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
					if (StringUtil.toString(request.getParameter("submitted")).equals("y")){
						if (request.getParameter("rep_" + strProductId) != null){
							if (NumberUtil.isNumeric(request.getParameter("rep_" + strProductId))){
								dblRepRate = Double.parseDouble(request.getParameter("rep_" + strProductId));
							}else{
								dblRepRate =0;
							}
						}
					}else{
						dblRepRate = Double.parseDouble(vecTemp.get(3).toString());
					}
					dblMerchantRate = dblTotalRate - dblRepRate;
					out.print("<tr class=row" + intEvenOdd +">" +
						"<td><input type=hidden name=productId value=" + strProductId + ">" +strProductId+ "</td>" +
						"<td nowrap=\"nowrap\">" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
						"<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"total_" + strProductId + "\" id=\"total_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblTotalRate)) + "\" readonly size=3  " + termDisabled + "></td>" +
						"<td align=left><input type=text name=\"rep_" + strProductId + "\" id=\"rep_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblRepRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'');\" onBlur=\"return validate(this);\" "+ termDisabled +">");
					if (terminalErrors != null && terminalErrors.containsKey("rep_" + strProductId)){
						out.print("<font color=ff0000>*</font>");
					}
					out.print("</td>");
					out.print("<td align=left><input style=\"color:#");
					if ((dblMerchantRate < 0) || dblMerchantRate > dblTotalRate){
						out.print("ff0000");
					}else{
						out.print("0000FF");
					}
					out.println(";background:#C0C0C0;\" type=text name=\"remaining_" + strProductId + "\" id=\"remaining_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblMerchantRate)) + "\" readonly size=3  "+ termDisabled +">");
					if (terminalErrors != null && terminalErrors.containsKey("remaining_" + strProductId)){
						out.print("<font color=ff0000>*</font>");
					}
					out.print("</td></tr>");
					intEvenOdd = (intEvenOdd == 1)?2:1;
				}
				vRatePlan.clear();
%>
														</table>
														<label><input type="checkbox" name="saveRates" value="y"  <%=termDisabled %>>&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.save",SessionData.getLanguage())%></label>
<%
			//End of if we must render a RepRate
			}else{
				//Else we must render an ISO rate
				if ( request.getParameter("isoRatePlanId") != null ){
					RatePlan.setRatePlanId(request.getParameter("isoRatePlanId"));
				}else{
					RatePlan.setRatePlanId(vISORatePlan.get(2).get(0).get(0));
				}
				vRatePlan = RatePlan.getProductsIsoRatePlan(SessionData);
%>
														<table width="100">
															<tr>
																<td class="rowhead2" width="100" nowrap="nowrap"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage())%></td>
																<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage())%></td>
																<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.total",SessionData.getLanguage())%></td>
<%
				if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
																<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.iso_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
<%
				}
				if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
%>
																<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.agent_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
<%
				}
				if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>
																<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.subagent_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
<%
				}
%>
																<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.rep_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
																<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.merchant_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
															</tr>
<%
				Iterator it = vRatePlan.iterator();
				int intEvenOdd = 1;
				String strProductId = "";
				double dblTotalRate = 0;
				double dblISORate = 0;
				double dblAgentRate = 0;
				double dblSubAgentRate = 0;
				double dblRepRate = 0;
				double dblMerchantRate = 0;
				int icount=0;
				while (it.hasNext()){
					icount++;
					Vector vecTemp = (Vector)it.next();
					strProductId = vecTemp.get(1).toString();
					dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
					dblISORate = Double.parseDouble(vecTemp.get(3).toString());
					dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
					dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());
					dblRepRate = Double.parseDouble(vecTemp.get(6).toString());
					dblMerchantRate = Double.parseDouble(vecTemp.get(7).toString());
					out.print("<tr class=row" + intEvenOdd +">" +
						"<td><input type=hidden name=productId value=" + strProductId + "  " + termDisabled + ">" +strProductId+ "</td>" +
						"<td nowrap=\"nowrap\">" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
						"<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"total_" + strProductId + "\" id=\"total_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblTotalRate)) + "\" readonly size=3 "+ termDisabled +"></td>");
					if (strAccessLevel.equals(DebisysConstants.ISO)){
						out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"iso_" + strProductId + "\" id=\"iso_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblISORate)) + "\" readonly size=3 "+ termDisabled +"></td>");
					}
					if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
						out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"agent_" + strProductId + "\" id=\"agent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblAgentRate)) + "\" readonly size=3 "+ termDisabled +"></td>");
					}
					if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
						out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"subagent_" + strProductId + "\" id=\"subagent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate)) + "\" readonly size=3 "+ termDisabled +"></td>");
					}
					out.print("<td align=left><input type=text name=\"rep_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblRepRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'');\" onBlur=\"return validate(this);\"  "+ termDisabled +"></td>");
					out.print("<td align=left><input style=\"color:#");
					if ((dblMerchantRate < 0) || dblMerchantRate > dblTotalRate){
						out.print("ff0000");
					}else{
						out.print("0000FF");
					}
					out.println(";background:#C0C0C0;\" type=text name=\"remaining_" + strProductId + "\" id=\"remaining_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblMerchantRate)) + "\" readonly size=3   "+ termDisabled +">");
					out.print("</td></tr>");
					intEvenOdd = (intEvenOdd == 1)?2:1;
				}
				vRatePlan.clear();
%>
														</table>
<%
			}//End of if we must render an ISORate
		}
		}
%>
													</div>
<%
		//End of if we are creating a new terminal
	}else{// Else we are editing a terminal
	
		Vector vecTerminalRates = Terminal.getTerminalRates(strAccessLevel);
						
		if ( merchantAux.isUseRatePlanModel() )
		{
			RatePlanName = Terminal.getTerminalRatePlanName(Terminal.getRatePlan(),SessionData);
		}
		else
		{
			RatePlanName=Terminal.getISORatePlanNameFromTerminal();
		}		
		
		
%>
													<table>
														<tr class="main">
															<td nowrap="nowrap"><b><%=typeRatePlanName%></b></td>
															<td>&nbsp;</td>
															<td><%=RatePlanName%></td>
														</tr>
													</table>

													<table>
<%
		//&& !changeRatePlanModel
		if(SessionData.checkPermission(DebisysConstants.PERM_RATEPLAN_TERMINAL_EDIT)  )
		{
%>
														<tr class="main">															
															<td nowrap="nowrap">																
																<input type="checkbox" name="rateSelector" id="rateSelector" value="1" onclick="switchRateTable();" <%=termDisabled %>><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.chkRateSelector",SessionData.getLanguage())%>
															</td>
														</tr>
<%
		}
			
		%>
													</table>
													<div id="ratePlanTable" style="display: inline;">
														<table width="100%">
															<tr>
																<td class="rowhead2" width="100" nowrap="nowrap"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage()).toUpperCase()%></td>
																<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.total",SessionData.getLanguage()).toUpperCase()%></td>
<%
		if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
		if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
%>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
		if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%></td>
																<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%></td>
<%
		boolean bTSenable = Terminal.validateTopSellersFeature(Terminal.getSiteId());
		if (bTSenable && SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION)){
			out.println("<td class=\"rowhead2\" width=\"75\" align=\"center\">" + Languages.getString("jsp.admin.rateplans.tsorder",SessionData.getLanguage()).toUpperCase() + "</td>");
		}
%>
															</tr>
<%
		Iterator it = vecTerminalRates.iterator();
		int intEvenOdd = 1;
		String strProductId="";
		double dblTotalRate = 0;
		double dblIsoRate = 0;
		double dblAgentRate = 0;
		double dblSubAgentRate = 0;
		double dblRepRate = 0;
		double dblMerchantRate = 0;
		String strTSOrder = "";
		int icount=0;
		boolean isIPP = false;
	
		while (it.hasNext()){
			icount++;
			Vector vecTemp = null;
			vecTemp = (Vector) it.next();
			strProductId = vecTemp.get(0).toString();
			dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
			isIPP = (vecTemp.get(9).toString().equals("154")); 
			if (request.getParameter("submitted") != null && request.getParameter("submitted").equals("y")){
				if (strAccessLevel.equals(DebisysConstants.ISO)){
					if (request.getParameter("iso_" + strProductId) != null && NumberUtil.isNumeric(request.getParameter("iso_" + strProductId))){
						dblIsoRate = Double.parseDouble(request.getParameter("iso_" + strProductId));
					}else{
						dblIsoRate = 0;
					}
					if (request.getParameter("agent_" + strProductId) != null && NumberUtil.isNumeric(request.getParameter("agent_" + strProductId))){
						dblAgentRate = Double.parseDouble(request.getParameter("agent_" + strProductId));
					}else{
						dblAgentRate =0;
					}
					if (request.getParameter("subagent_" + strProductId) != null && NumberUtil.isNumeric(request.getParameter("subagent_" + strProductId))){
						dblSubAgentRate = Double.parseDouble(request.getParameter("subagent_" + strProductId));
					}else{
						dblSubAgentRate =0;
					}
				}else if (strAccessLevel.equals(DebisysConstants.REP) ||strAccessLevel.equals(DebisysConstants.AGENT) ||strAccessLevel.equals(DebisysConstants.SUBAGENT)){
					dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
					dblIsoRate = Double.parseDouble(vecTemp.get(3).toString());
					dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
					dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());                      
				}
				if (request.getParameter("rep_" + strProductId) != null && NumberUtil.isNumeric(request.getParameter("rep_" + strProductId))){
					dblRepRate = Double.parseDouble(request.getParameter("rep_" + strProductId));
				}else{
					dblRepRate =0;
				}
			}else{
				dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
				dblIsoRate = Double.parseDouble(vecTemp.get(3).toString());
				dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
				dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());
				dblRepRate = Double.parseDouble(vecTemp.get(6).toString());
				strTSOrder = vecTemp.get(8).toString(); 
			}
			if (strAccessLevel.equals(DebisysConstants.ISO)){
				dblMerchantRate = dblTotalRate - (dblIsoRate + dblAgentRate + dblSubAgentRate + dblRepRate);
			}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
				dblMerchantRate = dblTotalRate - (dblAgentRate + dblSubAgentRate + dblRepRate);
			}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				dblMerchantRate = dblTotalRate - (dblSubAgentRate + dblRepRate);
			}else if (strAccessLevel.equals(DebisysConstants.REP)){
				dblMerchantRate = dblTotalRate - (dblRepRate);
			}
			//dblMerchantRate = dblTotalRate - (dblIsoRate + dblAgentRate + dblSubAgentRate + dblRepRate);
			//if(strAccessLevel.equals(DebisysConstants.REP) ||strAccessLevel.equals(DebisysConstants.AGENT) ||strAccessLevel.equals(DebisysConstants.SUBAGENT)){
			//	dblTotalRate = dblTotalRate - (dblIsoRate + dblAgentRate + dblSubAgentRate);
			//}
			String style = "";
			String strProductIdCell = strProductId ;
			if(strAccessLevel.equals(DebisysConstants.ISO) && isIPP){
				strProductIdCell = "<a href=\"javascript:void(0);\" onClick=\"javascript:window.open('/support/admin/customers/terminal_feetiers.jsp?product_id="+strProductId+"&siteid="+Terminal.getSiteId() + "', 'feetiers', 'width=750,height=600,resizable=yes,scrollbars=yes');\" style=\"text-decoration: underline;\">"+strProductId +"</a>";
				style = "style=\"background-color:#81BEF7;\"";
			}
			out.print("<tr class=row" + intEvenOdd +" "+  style +">" +
					"<td><input type=hidden name=productId value=" + strProductId + "  "+ termDisabled +">" +strProductIdCell + "</td>" +
					"<td nowrap=\"nowrap\">" + HTMLEncoder.encode(vecTemp.get(1).toString()) + "</td>" +
					"<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"total_"+strProductId+"\" id=\"total_" + strProductId + "\" value=\""+NumberUtil.formatAmount(Double.toString(dblTotalRate))+"\" readonly size=3 "+ termDisabled +"></td>");
			if (strAccessLevel.equals(DebisysConstants.ISO)){
				
					out.print("<td align=left><input "+disableSplitsByChangeRatePlanModel+" type=text name=\"iso_" + strProductId + "\" id=\"iso_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblIsoRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'');\" onBlur=\"return validate(this);\"  "+ termDisabled +">");
				if (terminalErrors != null && terminalErrors.containsKey("iso_" + strProductId)){
					out.print("<font color=ff0000>*</font>");
				}
				out.print("</td>");
				
			}
			if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
				
					out.print("<td align=left><input "+disableSplitsByChangeRatePlanModel+" type=text name=\"agent_" + strProductId + "\" id=\"agent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblAgentRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'');\" onBlur=\"return validate(this);\" "+ termDisabled +">");
				if (terminalErrors != null && terminalErrors.containsKey("agent_" + strProductId)){
					out.print("<font color=ff0000>*</font>");
				}
				out.print("</td>");
				
			}
			if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				
					out.print("<td align=left><input "+disableSplitsByChangeRatePlanModel+" type=text name=\"subagent_" + strProductId + "\" id=\"subagent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'');\" onBlur=\"return validate(this);\" "+ termDisabled +">");
				if (terminalErrors != null && terminalErrors.containsKey("subagent_" + strProductId)){
					out.print("<font color=ff0000>*</font>");
				}
				out.print("</td>");
				
			}
			out.print("<td align=left><input "+disableSplitsByChangeRatePlanModel+" type=text name=\"rep_" + strProductId + "\" id=\"rep_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblRepRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'');\" onBlur=\"return validate(this);\" "+ termDisabled +">");
			if (terminalErrors != null && terminalErrors.containsKey("rep_" + strProductId)){
				out.print("<font color=ff0000>*</font>");
			}
			out.print("</td>");
			out.print("<td align=left><input style=\"color:#");
			if((dblMerchantRate < 0) || dblMerchantRate > dblTotalRate){
				//something is wrong with the splits
				out.print("ff0000");
			}else{
				out.print("0000FF");
			}
			out.println(";background:#C0C0C0;\"  "+disableSplitsByChangeRatePlanModel+" type=text name=\"remaining_"+strProductId+"\" id=\"remaining_"+strProductId+"\" value=\""+NumberUtil.formatAmount(Double.toString(dblMerchantRate))+"\" readonly size=3  "+ termDisabled +">");
			if (bTSenable && SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION)){
				out.println("<td align=left><input "+disableSplitsByChangeRatePlanModel+" type=text name=\"termts_" + strProductId + "\" id=\"termts_" + strProductId + "\" value=\"" + strTSOrder + "\" size=3 maxlength=5 onBlur=\"return validatets(this);\" "+ termDisabled +">");
			}
			if (terminalErrors != null && terminalErrors.containsKey("remaining_" + strProductId)){
				out.print("<font color=ff0000>*</font>");
			}
			out.print("</td></tr>");
			if (intEvenOdd == 1){
				intEvenOdd = 2;
			}else{
				intEvenOdd = 1;
			}
		}//End of while (it.hasNext())
		vecTerminalRates.clear();
%>
														</table>
													</div>
													<div id="newRatePlanTable" style="display: none;">
														<table>
															<%
															if ( !merchantAux.isUseRatePlanModel() )
															{ 
															%>
															
															<tr class="main">
																<td nowrap="nowrap"><label><%=Languages.getString("jsp.admin.customers.merchants_add.select_iso_rateplan.select",SessionData.getLanguage())%>&nbsp;
																	<input type="radio" id="chkISORatePlan" name="chkRatePlan" value="ISO" onclick="SwitchRatePlan(this.value);" <%=(Terminal.getChkRatePlan().equals("ISO") || Terminal.getChkRatePlan().equals(""))?"checked":""%> <%=termDisabled %>></label>
																</td>
																<td>&nbsp;</td>
																<td>
<%
		vISORatePlan = RatePlan.getISORatePlansG(strRefId, "0", "", null);
		if (vISORatePlan.get(2).size() > 0){
%>
																	<select id="isoRatePlanId" name="isoRatePlanId" style="display:<%=(Terminal.getChkRatePlan().equals("ISO") || Terminal.getChkRatePlan().equals(""))?"inline":"none"%>;" onchange="SwitchRatePlan('ISO');" <%=termDisabled %>>
<%
			Iterator itISORatePlan = vISORatePlan.get(2).iterator();
			while (itISORatePlan.hasNext()){
				Vector vecTemp = (Vector)itISORatePlan.next();
				if ( vecTemp.get(0).equals(Terminal.getIsoRatePlanId()) ){
					out.println("<option value=" + vecTemp.get(0) +" selected>" + vecTemp.get(1) + "</option>");
				}else{
					out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
				}
			}
%>
																	</select>
<%//End of if (vISORatePlan != null && vISORatePlan.size() > 0)
		}else{
			Vector<String> vTmp = new Vector<String>();
			vTmp.add("0");
			vTmp.add("");
			vISORatePlan = new Vector<Vector<Vector<String>>>();
			vISORatePlan.add(new Vector<Vector<String>>());
			vISORatePlan.add(new Vector<Vector<String>>());
			vISORatePlan.add(new Vector<Vector<String>>());
			vISORatePlan.get(2).add(vTmp);
%>
																	<select id="isoRatePlanId" name="isoRatePlanId" style="display:<%=(Terminal.getChkRatePlan().equals("ISO") || Terminal.getChkRatePlan().equals(""))?"inline":"none"%>;" disabled="disabled">
																		<option value="0"><%=Languages.getString("jsp.admin.customers.merchants_add.select_iso_rateplan.noplan",SessionData.getLanguage())%></option>
																	</select>
<%
		}
%>
																</td>
															</tr>
<%
		if ( User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) && strAccessLevel.equals(DebisysConstants.ISO) ){
%>
															<tr class="main">
																<td nowrap="nowrap"><label><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.select_rep_rateplan",SessionData.getLanguage())%>&nbsp;
																	<input type="radio" id="chkRepRatePlan" name="chkRatePlan" value="REP" onclick="SwitchRatePlan(this.value);" <%=(Terminal.getChkRatePlan().equals("REP"))?"checked":""%><%=termDisabled %>></label>
																</td>
																<td></td>
																<td>
<%
			vRepRatePlan = RatePlan.getRepRatePlans(SessionData);
			if (vRepRatePlan != null && vRepRatePlan.size() > 0){
%>
																	<select id="repRatePlanId" name="repRatePlanId" style="display:<%=(Terminal.getChkRatePlan().equals("REP"))?"inline":"none"%>;" onchange="SwitchRatePlan('REP');" <%=termDisabled %>>
<%
				Iterator itRepRatePlan = vRepRatePlan.iterator();
				while (itRepRatePlan.hasNext()){
					Vector vecTemp = (Vector)itRepRatePlan.next();
					if (vecTemp.get(0).equals(Terminal.getRepRatePlanId())){
						out.println("<option value=" + vecTemp.get(0) +" selected>" + vecTemp.get(1) + "</option>");
					}else{
						out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
					}
				}
%>
																	</select>
<%
			}else{
				Vector vTmp = new Vector();
				vTmp.add("0");
				vTmp.add("");
				vRepRatePlan = new Vector();
				vRepRatePlan.add(vTmp);
%>
																	<select id="repRatePlanId" name="repRatePlanId" style="display:<%=(Terminal.getChkRatePlan().equals("REP"))?"inline":"none"%>;" disabled="disabled">
																		<option value="0"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.select_rep_rateplan.noplan",SessionData.getLanguage())%></option>
																	</select>
<%
			}
		}//End of if ( User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) )
%>
																</td>
															</tr>
											<%
											}else{
											%>
												<tr>
													<td>
														<jsp:include page="terminals/listrateplan.jsp" >
															<jsp:param value="<%=repId%>" name="repId"/>
															<jsp:param value="<%=merchantId%>" name="merchantId"/>
															<jsp:param value="divRates" name="divName"/>
															<jsp:param value="<%=RateplanModel%>" name="RateplanModel"/>																			
														</jsp:include>
													</td>
												</tr>
												
											<%
											}
											%>
														</table>
														<script>
															function SwitchRatePlan(sValue){
															  	$("#btnSubmit").attr('disabled', 'disabled');
															  	
																var sRateID = "";
																if ( sValue == "ISO" ){
																	document.getElementById('isoRatePlanId').style.display = "inline";
<%
		if ( User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) && strAccessLevel.equals(DebisysConstants.ISO) ){
%>
																	document.getElementById('repRatePlanId').style.display = "none";
<%
		}
%>
																	sRateID = document.getElementById('isoRatePlanId').value;
																}else if ( sValue == "REP" ){
																	document.getElementById('isoRatePlanId').style.display = "none";
																	typeRatePlan="REP";
<%
		if ( User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) && strAccessLevel.equals(DebisysConstants.ISO) ){
%>
																	document.getElementById('repRatePlanId').style.display = "inline";
<%
		}
%>
																	sRateID = document.getElementById('repRatePlanId').value;
																}else if ( sValue == "EMIDA" ){
																	
																}
																var sNew = '';
																if(document.getElementById('rateSelector').checked == true){
																	sNew = 'new';
																}
																$("#divRates").html("");
																$("#divRates").load("admin/customers/terminal_rates.jsp?PlanType=" + sValue + "&repId=" + <%=Merchant.getRepId()%> + "&merchantId=" + <%=Merchant.getMerchantId()%> + "&sRateID=" + sRateID + "&Random=" + Math.random()+"&sNew="+sNew);
															}
														</script>
														<br>
														<div id="divRates">
<%
		
		// Scope limiting
		{
			Vector vRatePlan = new Vector();
			if ( merchantAux.isUseRatePlanModel() )
			{
				//******************************************************
				//******************************************************
				//NO ACTION HERE BECAUSE THE JSP WAS INCLUDE BEFORE
				//SEE <jsp:include page="terminals/listrateplan.jsp" >
				//******************************************************
				//******************************************************
			}
			else
			{
						
			// Must render a rep rate?
			if ( (StringUtil.toString(request.getParameter("chkRatePlan")).equals("REP") ) && User.isRepRatePlanforTerminalCreationEnabled(SessionData, application) && strAccessLevel.equals(DebisysConstants.ISO))
			{
%>
															<table width="100">
																<tr>
																	<td class="rowhead2" width="100" nowrap="nowrap"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage())%></td>
																	<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage())%></td>
																	<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.total",SessionData.getLanguage())%></td>
																	<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.rep",SessionData.getLanguage())%></td>
																	<td class="rowhead2" width="75" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.merchant",SessionData.getLanguage())%></td>
																</tr>
<%
				if ( request.getParameter("repRatePlanId") != null ){
					RatePlan.setRepRatePlanId(request.getParameter("repRatePlanId"));
				}else{
					RatePlan.setRepRatePlanId(((Vector)vRepRatePlan.get(0)).get(0).toString());
				}
				vRatePlan = RatePlan.getProductsIsoRatePlan(SessionData); //getMerchantRatePlan(SessionData);
				it = vRatePlan.iterator();
				intEvenOdd = 1;
				strProductId = "";
				dblTotalRate = 0;
				dblRepRate = 0;
				dblMerchantRate = 0;
				icount=0;
				while (it.hasNext()){
					icount++;
					Vector vecTemp = (Vector)it.next();
					strProductId = vecTemp.get(1).toString();
					dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
					if (StringUtil.toString(request.getParameter("submitted")).equals("y")){
						if (request.getParameter("newrep_" + strProductId) != null){
							if (NumberUtil.isNumeric(request.getParameter("newrep_" + strProductId))){
								dblRepRate = Double.parseDouble(request.getParameter("newrep_" + strProductId));
							}else{
								dblRepRate =0;
							}
						}
					}else{
						dblRepRate = Double.parseDouble(vecTemp.get(3).toString());
					}
					dblMerchantRate = dblTotalRate - dblRepRate;
					out.print("<tr class=row" + intEvenOdd +">" +
						"<td><input type=hidden name=newproductId value=" + strProductId + "  "+ termDisabled +">" +strProductId+ "</td>" +
						"<td nowrap=\"nowrap\">" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
						"<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"newtotal_" + strProductId + "\" id=\"newtotal_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblTotalRate)) + "\" readonly size=3   "+ termDisabled +"></td>" +
						"<td align=left><input type=text name=\"newrep_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblRepRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'new');\" onBlur=\"return validate(this);\"   "+ termDisabled +">");
					if (terminalErrors != null && terminalErrors.containsKey("newrep_" + strProductId)){
						out.print("<font color=ff0000>*</font>");
					}
					out.print("</td>");
					out.print("<td align=left><input style=\"color:#");
					if ((dblMerchantRate < 0) || dblMerchantRate > dblTotalRate){
						out.print("ff0000");
					}else{
						out.print("0000FF");
					}
					out.println(";background:#C0C0C0;\" type=text name=\"newremaining_" + strProductId + "\" id=\"newremaining_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblMerchantRate)) + "\" readonly size=3  "+ termDisabled +">");
					if (terminalErrors != null && terminalErrors.containsKey("newremaining_" + strProductId)){
						out.print("<font color=ff0000>*</font>");
					}
					out.print("</td></tr>");
					intEvenOdd = (intEvenOdd == 1)?2:1;
				}
				vRatePlan.clear();
%>
															</table>
															<label><input type="checkbox" name="saveRates" value="y" <%=termDisabled %>>&nbsp;<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.save",SessionData.getLanguage())%></label>
<%
				//End of if we must render a RepRate
			}
			else //if ( StringUtil.toString(request.getParameter("chkRatePlan")).equals("ISO") )
			{   
				//Else we must render an ISO rate
				if ( request.getParameter("isoRatePlanId") != null ){
					RatePlan.setRatePlanId(request.getParameter("isoRatePlanId"));
				}else{
					RatePlan.setRatePlanId(vISORatePlan.get(2).get(0).get(0));
				}
				vRatePlan = RatePlan.getProductsIsoRatePlan(SessionData);
%>
															<table width="100">
																<tr>
																	<td class="rowhead2" width="100" nowrap="nowrap"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage())%></td>
																	<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage())%></td>
																	<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.total",SessionData.getLanguage())%></td>
<%
				if (strAccessLevel.equals(DebisysConstants.ISO)){
%>
																	<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.iso_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
<%
				}

				if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
%>
																	<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.agent_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
<%
				}

				if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>
																	<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.subagent_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
<%
				}
%>
																	<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.rep_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
																	<td class="rowhead2" width="100" align="center"><%=Languages.getString("jsp.admin.ach.summary.merchant_percent",SessionData.getLanguage()).replaceAll(" ", "&nbsp;")%></td>
																</tr>
<%
				it = vRatePlan.iterator();
				intEvenOdd = 1;
				strProductId = "";
				dblTotalRate = 0;
				double dblISORate = 0;
				dblAgentRate = 0;
				dblSubAgentRate = 0;
				dblRepRate = 0;
				dblMerchantRate = 0;
				icount=0;
				while (it.hasNext()){
					icount++;
					Vector vecTemp = (Vector)it.next();
					strProductId = vecTemp.get(1).toString();
					dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
					dblISORate = Double.parseDouble(vecTemp.get(3).toString());
					dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
					dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());
					dblRepRate = Double.parseDouble(vecTemp.get(6).toString());
					dblMerchantRate = Double.parseDouble(vecTemp.get(7).toString());
					out.print("<tr class=row" + intEvenOdd +">" +
						"<td><input type=hidden name=\"newproductId\" value=" + strProductId + "  "+ termDisabled +">" +strProductId+ "</td>" +
						"<td nowrap=\"nowrap\">" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
						"<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"newtotal_" + strProductId + "\" id=\"newtotal_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblTotalRate)) + "\" readonly size=3   "+ termDisabled +"></td>");
					
					if (strAccessLevel.equals(DebisysConstants.ISO)){
						out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"newiso_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblISORate)) + "\" readonly size=3  "+ termDisabled +"></td>");
					}
					if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)){
						out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"newagent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblAgentRate)) + "\" readonly size=3  "+ termDisabled +"></td>");
					}
					if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
						out.print("<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"newsubagent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate)) + "\" readonly size=3 "+ termDisabled +"></td>");
					}
					out.print("<td align=left><input type=text name=\"newrep_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblRepRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this,'new');\" onBlur=\"return validate(this);\" "+ termDisabled +"></td>");
					out.print("<td align=left><input style=\"color:#");
					if ((dblMerchantRate < 0) || dblMerchantRate > dblTotalRate){
						out.print("ff0000");
					}else{
						out.print("0000FF");
					}
					out.println(";background:#C0C0C0;\" type=text name=\"newremaining_" + strProductId + "\" id=\"newremaining_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblMerchantRate)) + "\" readonly size=3  "+ termDisabled +">");
					out.print("</td></tr>");
					intEvenOdd = (intEvenOdd == 1)?2:1;
				} // End while
				vRatePlan.clear();
%>
															</table>
<%
			}//End of if we must render an ISORate
		   }	
		}
%>
														</div>
													</div> 
<%
	} //End of else we are editing a terminal
%>
												</td>
											</tr>
										</table>
<%
	String strStyle= "";
	if ( Terminal.getSiteId().length() != 0 ){
		strStyle = "style=\"display:none;\"";
	}
%>
										<table id="mailNotificationT" width="100%" <%=strStyle%>>
											<tr>
												<td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.merchants_edit.mailnotification",SessionData.getLanguage())%></td>
											</tr>
											<tr>
												<td class="formArea2">
													<br>
													<table width="100%">
														<tr class="main">
															<td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.merchants.mailnotice",SessionData.getLanguage())%></td>
														</tr>
														<tr class="main">
															<td nowrap="nowrap">
																<!-- 2009-04-30 FB.Mail notification auto fill removed because ticket5545-7483655 -->
																<input type="text" id="mailNotification" name="mailNotification" value="<%=StringUtil.toString(request.getParameter("mailNotification"))%>" size="80" maxlength="256" <%=termDisabled %>>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
<%
	//}
%>
										<script>
											$(document).ready(function(){
												if ( document.getElementById('terminalOf') != null ){
													document.getElementById('terminalOf').onchange();
												}
<%
	if ( Terminal.getSiteId().length() == 0 ){
		if (!User.isRepRatePlanforTerminalCreationEnabled(SessionData, application)){
%>
												//document.getElementById('chkISORatePlan').click();
<%
		}else{
%>
												//document.getElementById('chkISORatePlan').click();
<%
		}
%>
												
												if ( document.getElementById('chkISORatePlan') != null)
												{
													document.getElementById('chkISORatePlan').click();
													
												}
												terminalType_onclick(document.getElementById('terminalType'));
<%
	}else{
		if(com.debisys.terminals.Terminal.IsTerminalInPhysicalList(application, Terminal.getTerminalType()) 
				&& Terminal.getTerminalFisico()==null 
				&& SessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS))
		{
%>
												document.getElementById('serialNumber').value = '';
												document.getElementById('simData').value = '';
												document.getElementById('imciData').value = '';
												document.getElementById('serialNumber').disabled = true;
												document.getElementById('simData').disabled = true;
												document.getElementById('imciData').disabled = true;
												document.getElementById('trSerialTracking').style.display = "inline";
												mbvalidation = true;
<%
		}
	}
%>
											});
										</script>
										<table width="100%">
											<tr>
												<td align="center">
<%
	if ( Terminal.getSiteId().length() > 0 ){
		if(Terminal.getStatus() > 0){%>
													<input type="hidden" name="siteId" value="<%=Terminal.getSiteId()%>">
													<input type="submit" id="btnSubmit" name="btnSubmit" value="<%=Languages.getString("jsp.admin.customers.merchants_edit_terminal.update_terminal",SessionData.getLanguage())%>" >&nbsp;&nbsp;
<%	
		}
	}else{
%>
													<input type="submit" id="btnSubmit" name="btnSubmit" value="<%=Languages.getString("jsp.admin.customers.merchants_add_terminal.submit",SessionData.getLanguage())%>" disabled >&nbsp;&nbsp;
<%
	}
%>
													<input type="button" value="<%=Languages.getString("jsp.admin.cancel",SessionData.getLanguage())%>" onClick="window.history.go(-1)">
													<input type="hidden" name="submitted" value="y">
													<input type="hidden" name="merchantId" value="<%=Terminal.getMerchantId()%>">
													<input type="hidden" name="repId" value="<%=Terminal.getRepId()%>">
												</td>
											</tr>
										</table>
									</form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div id="customMessage"></div>
<style>
	#PlanModelMerchant{
		width: 100% !important;
	}

	#ratePlanTable{
		display: block !important;
		height: 150px;
		overflow-y: scroll;
	}
	#divNewRatesEmida{
		height: 171px;
		overflow-y: scroll;
	}
	#divRates{
		height: 171px;
		overflow-y: scroll;
	}

	#divRates table{
		width: 100% !important;
	}
	body{
		font-family: "Roboto", "Helvetica", "Arial", sans-serif !important;
		font-weight: 300;
		line-height: 1.5em;
		background-color: #fff;
	}


	ratePlanTable::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		border-radius: 10px;
		background-color: #F5F5F5;
	}

	ratePlanTable::-webkit-scrollbar
	{
		width: 12px;
		background-color: #F5F5F5;
	}

	body::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		border-radius: 10px;
		background-color: #F5F5F5;
	}

	body::-webkit-scrollbar
	{
		width: 12px;
		background-color: #F5F5F5;
	}

	body::-webkit-scrollbar-thumb
	{
		border-radius: 10px;
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
		background-color: #555;
	}
	.ControllerStyleClass{
		width: 100% !important;
		border: none;
		background-color: #fff;
		font-family: "Roboto", "Helvetica", "Arial", sans-serif !important;
	}
	input[type="button"]{
		border: none;
		border-radius: 3px;
		position: relative;
		padding: 12px 30px;
		margin: 10px 1px;
		font-size: 12px;
		font-weight: 400;
		text-transform: uppercase;
		letter-spacing: 0;
		will-change: box-shadow, transform;
		transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
		background-color: #5cb85c;
		color: #FFFFFF;
		box-shadow: 0 2px 2px 0 rgba(92, 184, 92, 0.14), 0 3px 1px -2px rgba(92, 184, 92, 0.2), 0 1px 5px 0 rgba(92, 184, 92, 0.12);
	}
	input[type="submit"]{
		border: none;
		border-radius: 3px;
		position: relative;
		padding: 12px 30px;
		margin: 10px 1px;
		font-size: 12px;
		font-weight: 400;
		text-transform: uppercase;
		letter-spacing: 0;
		will-change: box-shadow, transform;
		transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
		background-color: #5cb85c;
		color: #FFFFFF;
		box-shadow: 0 2px 2px 0 rgba(92, 184, 92, 0.14), 0 3px 1px -2px rgba(92, 184, 92, 0.2), 0 1px 5px 0 rgba(92, 184, 92, 0.12);
	}
	input[type="text"]{
		-webkit-appearance: none;
		background-color: #fff;
		background-image: none;
		border-radius: 4px;
		border: 1px solid #dcdfe6;
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
		color: #606266;
		display: inline-block;
		font-size: inherit;
		height: 40px;
		line-height: 1;
		outline: none;
		padding: 0 15px;
		-webkit-transition: border-color 0.2s cubic-bezier(0.645, 0.045, 0.355, 1);
		transition: border-color 0.2s cubic-bezier(0.645, 0.045, 0.355, 1);
		width: 100%;
	}
	select{
		-webkit-appearance: none;
		background-color: #fff;
		background-image: none;
		border-radius: 4px;
		border: 1px solid #dcdfe6;
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
		color: #606266;
		display: inline-block;
		font-size: inherit;
		height: 40px;
		line-height: 1;
		outline: none;
		padding: 0 15px;
		-webkit-transition: border-color 0.2s cubic-bezier(0.645, 0.045, 0.355, 1);
		transition: border-color 0.2s cubic-bezier(0.645, 0.045, 0.355, 1);
		width: 100%;
	}
	.formArea2{
		font-size: 8pt;
		margin-top: 20px;
		margin-bottom: 20px;
		border: 0;
		border-top: 1px solid #eee;
		font-family: "Roboto", "Helvetica", "Arial", sans-serif !important;
		background-color: #ffffff;
	}

	hr{
		font-size: 8pt;
		margin-top: 20px;
		margin-bottom: 20px;
		border: 0;
		border-top: 1px solid #eee;
	}
	fieldset{
		font-size: 8pt;
		margin-top: 20px;
		margin-bottom: 20px;
		border: 1px solid #eee;
	}
	.main{
		font-family: "Roboto", "Helvetica", "Arial", sans-serif !important;
	}
	.formArea{
		border: none !important;
	}
</style>
