<%@ page import="com.debisys.utils.*, com.debisys.customers.Rep,
                 java.util.*"%>
<%
int section=2;
int section_page=21;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request"/>
<jsp:setProperty name="Rep" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
String strUrlParams = "?repId=" + Rep.getRepId();
String sourceId="";
String destinationId="";
double amount=0;
String description="";
 if (request.getParameter("repId") != null)
 {
     sourceId=request.getParameter("repId");
     destinationId=request.getParameter("selectedRepId");
     amount=Double.valueOf(request.getParameter("paymentAmount"));
     description=request.getParameter("paymentDescription");
 }
  boolean deploymentIntl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
  Rep rep = new Rep();
 rep.setRepId(SessionData.getProperty("ref_id"));
  boolean isUnlimited = rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED);
   
  double assignedCredit = rep.getAssignedCredit(SessionData);
  double currCreditLimit = rep.getCurrentRepCreditLimit(SessionData);
   double paymtAmt = Double.parseDouble(String.valueOf(amount)); 
   
     boolean isPaymentANumber = NumberUtil.isNumeric(String.valueOf(amount));
  //boolean isCommissionANumber =  NumberUtil.isNumeric(rep.getCommission());
  
  
  if (isPaymentANumber ) {
  
    if (assignedCredit > (currCreditLimit + paymtAmt)  && !isUnlimited) {
      response.sendRedirect("repstransfer.jsp?repId=" + rep.getRepId() + "&message=4");
      return;
    } else {        
    	  String redirectUrl= "reptransfer.jsp?repId=" + rep.getRepId();
          
              if (deploymentIntl) {
            	  //Para Caribe extendido . Reemplazo de la lógica de script por la del SP 
            	  // el metodo makePayment ocaciona errores en el calculo de los creditos para esta configuración
				
			 Vector vTmp = new Vector();
					    try { 
					      vTmp= Rep.doRepTransfer(SessionData, application,destinationId);
					    } catch (Exception e) {
					 	  vTmp.add(999);
					    }
					        	  
					    if (vTmp.get(0).toString().equals("0")) {
					       response.sendRedirect("reptransfer.jsp?repId=" + Rep.getRepId() + "&message=14");
					       return;
					    } else {
					       response.sendRedirect("reptransfer.jsp?repId=" + Rep.getRepId() + "&message=13&code=" + vTmp.get(0)+"&dbMessage="+vTmp.get(1));
					       return;
					    }
			    
				         	  	  
              } 
  
          
		if (Rep.getErrors() != null) {
			Enumeration enum1 = Rep.getErrors().keys();
			if (enum1.hasMoreElements()) {
			  String strKey = enum1.nextElement().toString();
			  String strError = (String) Rep.getErrors().get(strKey);
			  redirectUrl = redirectUrl + "&message=" + strError;
			}
		}
          response.sendRedirect(redirectUrl);
          return;
 
    }
  } else {
    response.sendRedirect("reptransfer.jsp?repId=" + rep.getRepId() + "&message=2");
    return;
  }

%>