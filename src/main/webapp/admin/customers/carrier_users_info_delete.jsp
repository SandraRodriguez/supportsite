<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.util.Hashtable,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%
	int section=14;
	int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:setProperty name="User" property="*"/>
<jsp:setProperty name="CustomerSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
	Hashtable<String, String> userErrors = new Hashtable<String, String>();

	// Form has been submitted
	if(request.getParameter("submitted") != null) {
		// delete all the carrier user products for that user
		// delete the user

		if (request.getParameter("username") != null && request.getParameter("password") != null && request.getParameter("passwordId") != null)
		{
			User.setpasswordId(request.getParameter("passwordId"));
			User.setUsername(request.getParameter("username"));
			User.setPassword(request.getParameter("password"));
			
			User.deleteCarrierUserProducts(SessionData);
			
			User.deleteUser(SessionData,application);
			userErrors = User.getErrors();
		}
	}

	if(request.getParameter("submitted") == null) 
	{
		String username = User.getUserLogonID(SessionData);
		String password = User.getUserLogonPassword(SessionData);
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_info.delete.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<table width=100% border=0>
	              			<tr class="main">
	               				<td nowrap valign="top"><%=Languages.getString("jsp.admin.customers.carrier_users_info.delete.instructions",SessionData.getLanguage())%></td>
	               			</tr>
	               		</table>
       					<form name="carrierUsersDeleteForm" method="post" action="admin/customers/carrier_users_info_delete.jsp" >
       					<table border="0" width="100%" cellpadding="0" cellspacing="0">
				     		<tr>
					        	<td>
				          			<table border=0>
										<tr class=main>
											<td><%=Languages.getString("jsp.admin.customers.carrier_users.username",SessionData.getLanguage())%>: </td>
											<td><%=username%></td>
										</tr>
									</table>
								</td>
							</tr>
				     		<tr>
				     			<td>
				     				<table>
				     					<tr>
								        	<td>
							          			<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users.deleteopt",SessionData.getLanguage())%>" />
							          			<input type="hidden" id="submitted" name="submitted" value="1" />
									    		<input type="hidden" id="refId" name="refId" value="<%=SessionData.getProperty("ref_id")%>">
							          			<input type="hidden" id="passwordId" name="passwordId" value="<%=request.getParameter("passwordId")%>"/>
									    		<input type="hidden" id="refType" name="refType" value="1">
												<input type="hidden" id="password" name="password" value="<%=password%>" />
												<input type="hidden" id="username" name="username" value="<%=username%>" />
												</form>
											</td>
					        				<td>
												<form name="cancelForm" method="get" action="admin/customers/carrier_users.jsp" >
													<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.new.cancelbutton",SessionData.getLanguage())%>">
													<input type="hidden" id="search" name="search" value="y">
												</form>
					        				</td>
				     					</tr>
				     				</table>
				     			</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
	} else {
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="970" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.customers.carrier_users_info.delete.title",SessionData.getLanguage())%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<form name="backToCarrierUsers" action="admin/customers/carrier_users.jsp"> 
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center" class="main"><%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.CarrierAccountDeletionSuccessful",SessionData.getLanguage())%>!</td>
							</tr>
							<tr>
					    		<td align="center">
					   				<input type="submit" value="<%=Languages.getString("jsp.admin.customers.carrier_users_info.edit.BackToCarrierUsers",SessionData.getLanguage())%>" />
									<input type="hidden" id="search" name="search" value="y">
					   			</td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
	}
%>
<%@ include file="/includes/footer.jsp" %>