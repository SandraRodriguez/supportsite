<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="net.emida.supportsite.dto.RepInvoicePaymentType"%>
<%@page import="net.emida.supportsite.dto.InvoiceType"%>
<%@page import="net.emida.supportsite.dao.InvoicePaymentTypesDao"%>
<%@page import="net.emida.supportsite.dao.BankDao"%>
<%@page import="net.emida.supportsite.dao.DepositTypeDao"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         com.debisys.customers.CreditTypes,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.TimeZone,
         com.debisys.languages.Languages,
         com.debisys.pincache.PcReservedBalance" %>
<%
    int section = 2;
    int section_page = 15;
    boolean isIsoPrepaid = false;
    String path = request.getContextPath();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/WEB-INF/jspf/includeJsGlobals.jspf" %>
<%@ include file="/WEB-INF/jspf/admin/customers/reps/repCommon.jspf" %>
<%
    if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ) {
%>
<%@ include file="/WEB-INF/jspf/admin/customers/reps/repInvoicePaymentTypesCommonCode.jspf" %>
<%  }%>
<link rel="stylesheet" type="text/css" href="<%=path%>/css/toastr/toastr.css" />


<%    
    // Set support site common consttanst and variables
    setJsGlobals(out, DebisysConfigListener.getDeploymentType(application), DebisysConfigListener.getCustomConfigType(application));
    setJsRepGlobals(out,  DebisysConfigListener.getLanguage(application));
    String strMessage = request.getParameter("message");
    
    if (strMessage == null) {
        strMessage = "";
    }
//HashMap entityErrors = null;
    if (request.getParameter("submitted") != null) {
        try {
            if (request.getParameter("submitted").equals("y")) {
            }
        } catch (Exception e) {
        }
    } else {
        Rep.getRep(SessionData, application);
        if (Rep.isError()) {
            response.sendRedirect("/support/message.jsp?message=4");
            return;
        }
    }
%>
<%@ include file="/includes/header.jsp" %>
<%
    boolean isDomestic = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
%>
<script language="JavaScript">
    var repCurrencyCode = '<%=Rep.getCurrencyCode()%>';
</script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery.numeric.js"></script>
<script type="text/javascript" src="/support/js/NumberUtil.js"></script>
<script type="text/javascript" src="/support/js/reps_info.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/toastr/toastr.js"></script>
<script language="JavaScript">
<!--
    function checkPayment() {
        var formApplyPmt = document.payment.paymentAmount.value;
        if (isNaN(formApplyPmt)) {
            alert("<%=Languages.getString("jsp.admin.customers.reps_info.jsmsg1", SessionData.getLanguage())%>");
            document.payment.paymentAmount.focus();
            return false;
        } else {
            DisableButtons();
            return true;
        }
    }
    function checkCreditLimit() {
        var formCreditLimit = document.creditLimit.creditLimit.value
        if (isNaN(formCreditLimit)) {
            alert("<%= Languages.getString("jsp.admin.customers.reps_info.jsmsg1", SessionData.getLanguage())%>");
            document.creditLimit.creditLimit.focus();
            return false;
        } else {
            DisableButtons();
            return true;
        }
    }
    function validate(c) {
        if (isNaN(c.value)) {
            alert('<%=Languages.getString("jsp.admin.error2", SessionData.getLanguage())%>');
            c.focus();
            return (false);
        } else {
            c.value = formatAmount(c.value);
        }
    }
    function checkRepMerchantPayment() {
        if (checkPayment()) {
            if (document.getElementById("txtRepMerchantPayment").value === "") {
                var sURL = "/support/admin/customers/reps_merchant_payment.jsp?repId=" + <%=Rep.getRepId()%> + "&amount=" + document.payment.paymentAmount.value;
                var sOptions = "left=" + (screen.width - (screen.width * 0.6)) / 2 + ",top=" + (screen.height - (screen.height * 0.4)) / 2 + ",width=" + (screen.width * 0.6) + ",height=" + (screen.height * 0.4) + ",location=no,menubar=no,resizable=yes,scrollbars=yes";
                var w = window.open(sURL, "RepMerchantPayment", sOptions, true);
                w.focus();
                return false;
            } else {
                DisableButtons();
                return true;
            }
        }
    }//End of function checkRepMerchantPayment
    function ValidatePrepaidLimitMX(cText) {
        if ((cText.value < 0) && (Math.abs(cText.value) > (<%=Rep.getCreditLimit()%> - <%=Rep.getRunningLiability()%>)) && (<%=Rep.getRunningLiability()%> > 0)) {
            alert("<%=Languages.getString("jsp.admin.customers.reps_info.paymentgreaterthanavailable", SessionData.getLanguage())%>");
            cText.focus();
            cText.select();
            return;
        }
        if ((cText.value < 0) && (Math.abs(cText.value) > <%=Rep.getCreditLimit()%>)) {
            alert("<%=Languages.getString("jsp.admin.customers.reps_info.absnegativepaymentgreaterthanlimit", SessionData.getLanguage())%>");
            cText.focus();
            cText.select();
        }
    }//End of function ValidatePrepaidLimitMX
    function ValidateUnlimitedMX(cText) {
        if (Math.abs(cText.value) > <%=Rep.getRunningLiability()%>) {
            alert("<%=Languages.getString("jsp.admin.customers.reps_info.unlimitedpaymentgreaterthanrunning", SessionData.getLanguage())%>");
            cText.focus();
            cText.select();
        }
    }//End of function ValidateUnlimitedMX
    function ValidateCreditLimitMX(cText) {
        if ((cText.value > 0) && (<%=Rep.getRunningLiability()%> === 0)) {
            alert("<%=Languages.getString("jsp.admin.customers.reps_info.paymentnegative", SessionData.getLanguage())%>");
            cText.focus();
            cText.select();
            return;
        }
        if (cText.value > <%=Rep.getRunningLiability()%>) {
            alert("<%=Languages.getString("jsp.admin.customers.reps_info.unlimitedpaymentgreaterthanrunning", SessionData.getLanguage())%>");
            cText.focus();
            cText.select();
        }
        return;
    }//End of function ValidateCreditLimitMX
-->
</script>
<script type="text/javascript">
    //DTU-369 Payment Notifications
    function disableChk()
    {
        var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
    <%
        if (Rep.isPaymentNotificationSMS()) {
    %>
        valuePayNotiSMSChk.checked = true;
    <%
    } else {
    %>
        valuePayNotiSMSChk.checked = false;
    <%
        }
    %>
        valuePayNotiSMSChk.disabled = true;
        var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
    <%
        if (Rep.isPaymentNotificationMail()) {
    %>
        valuePayNotiMailChk.checked = true;
    <%
    } else {
    %>
        valuePayNotiMailChk.checked = false;
    <%
        }
    %>
        valuePayNotiMailChk.disabled = true;
        var valuePayNotiChk = document.getElementById('paymentNotifications');
    <%
        if (Rep.isPaymentNotifications()) {
    %>
        valuePayNotiChk.checked = true;
    <%
    } else {
    %>
        valuePayNotiChk.checked = false;
        valuePayNotiSMSChk.checked = false;
        valuePayNotiMailChk.checked = false;
    <%
        }
    %>
        valuePayNotiChk.disabled = true;
    }

    //DTU-480: Low Balance Alert Message SMS and/or email
    function disableBalChk()
    {
        var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
    <%
        if (Rep.isBalanceNotificationTypeValue()) {
    %>
        valueBalNotiValueChk.checked = true;
    <%
    } else {
    %>
        valueBalNotiValueChk.checked = false;
    <%
        }
    %>
        valueBalNotiValueChk.disabled = true;
        var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
    <%
        if (Rep.isBalanceNotificationTypeDays()) {
    %>
        valueBalNotiDaysChk.checked = true;
    <%
    } else {
    %>
        valueBalNotiDaysChk.checked = false;
    <%
        }
    %>
        valueBalNotiDaysChk.disabled = true;
        var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
    <%
        if (Rep.isBalanceNotificationSMS()) {
    %>
        valueBalNotiSMSChk.checked = true;
    <%
    } else {
    %>
        valueBalNotiSMSChk.checked = false;
    <%
        }
    %>
        valueBalNotiSMSChk.disabled = true;
        var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
    <%
        if (Rep.isBalanceNotificationMail()) {
    %>
        valueBalNotiMailChk.checked = true;
    <%
    } else {
    %>
        valueBalNotiMailChk.checked = false;
    <%
        }
    %>
        valueBalNotiMailChk.disabled = true;
        var valueBalNotiChk = document.getElementById('balanceNotifications');
    <%
        if (Rep.isBalanceNotifications()) {
    %>
        valueBalNotiChk.checked = true;
    <%
    } else {
    %>
        valueBalNotiChk.checked = false;
        valueBalNotiSMSChk.checked = false;
        valueBalNotiMailChk.checked = false;
        valueBalNotiValueChk.checked = false;
        valueBalNotiDaysChk.checked = false;
    <%
        }
    %>
        valueBalNotiChk.disabled = true;
    }
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td colspan="3">
            <table border="0" cellpadding="0" cellspacing="0" width="750">
                <tr>
                    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%= Languages.getString("jsp.admin.customers.reps_info.rep_detail", SessionData.getLanguage()).toUpperCase()%></td>
                    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#ffffff">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
                <tr bgcolor="ffffff"><td align="center" class="main"><img src="images/information.png" border="0" valign="middle">
                        <font color=000000>
                        <%
                            if (strMessage.equals("1")) {
                                out.println(Languages.getString("jsp.admin.customers.reps_info.rep_update_success", SessionData.getLanguage()));
                            } else if (strMessage.equals("10")) {
                                out.println(Languages.getString("jsp.admin.customers.reps_info.paymentsuccessful", SessionData.getLanguage()));
                            } else if (strMessage.equals("11")) {
                                out.println(Languages.getString("jsp.admin.customers.reps_info.paymentrepok_merchantfailed", SessionData.getLanguage()));
                            } else if (strMessage.equals("12")) {
                                out.println(Languages.getString("jsp.admin.customers.reps_info.paymentrepfailed", SessionData.getLanguage()) + request.getParameter("code"));
                            } else if (strMessage.equals("13")) {
                                out.println(Languages.getString("jsp.admin.customers.merchants_info.paymenterror", SessionData.getLanguage()) + request.getParameter("code"));
                            } else if (strMessage.equals("14")) {
                                out.println(Languages.getString("jsp.admin.customers.merchants_info.paymentsuccessful", SessionData.getLanguage()));
                            }
                        %>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="formArea">
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table border="0" width="50" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <form method="post" action="admin/customers/reps_edit.jsp?repId=<%=Rep.getRepId()%>" onsubmit="DisableButtons();">
                                                    <input type="hidden" name="repId" value="<%= Rep.getRepId()%>">
                                                    <input id="btnEditRep" type="submit" name="submit" value="<%= Languages.getString("jsp.admin.customers.reps_info.edit_rep_info", SessionData.getLanguage())%>">
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%">
                                        <tr>
                                            <td class="formAreaTitle2"><%= Languages.getString("jsp.admin.customers.reps_add.company_information", SessionData.getLanguage())%></td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2">
                                                <table>
                                                    <%
                                                        if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                                                    %>
                                                    <tr>
                                                        <td class="main"><b><%= Languages.getString("jsp.admin.customers.reps_add.subagent", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap" class="main"><%= Rep.getRepName(Rep.getParentRepId())%></td>
                                                    </tr>
                                                    <%
                                                    } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                                    %>
                                                    <tr>
                                                        <td class="main"><b><%= Languages.getString("jsp.admin.customers.reps_add.subagent", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap" class="main"><%= SessionData.getProperty("company_name")%></td>
                                                    </tr>
                                                    <%
                                                    } else if ((strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))) {
                                                    %>
                                                    <tr>
                                                        <td class="main"><b><%= Languages.getString("jsp.admin.customers.reps_add.iso", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap" class="main"><%= SessionData.getProperty("company_name")%></td>
                                                    </tr>
                                                    <%
                                                        }
                                                        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {//If when deploying in Mexico
%>
                                                    <tr>
                                                        <td class="main"><b><%= Languages.getString("jsp.admin.customers.reps_edit.legal_businessname", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap" class="main"><%= Rep.getLegalBusinessname()%></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                    <tr>
                                                        <td class="main"><b><%= Languages.getString("jsp.admin.customers.reps_add.business_name", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap" class="main"><%= Rep.getBusinessName()%></td>
                                                    </tr>
                                                    <%
                                                        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {//If when deploying in Mexico
%>
                                                    <tr>
                                                        <td class="main"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.businesstype", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap" class="main">
                                                            <%
                                                                Vector vecBusinessTypes = com.debisys.customers.Merchant.getBusinessTypes(customConfigType);
                                                                Iterator itBusinessTypes = vecBusinessTypes.iterator();
                                                                while (itBusinessTypes.hasNext()) {
                                                                    Vector vecTemp = null;
                                                                    vecTemp = (Vector) itBusinessTypes.next();
                                                                    String strBusinessTypeId = vecTemp.get(0).toString();
                                                                    String strBusinessTypeCode = vecTemp.get(1).toString();
                                                                    if (strBusinessTypeId.equals(Rep.getBusinessTypeId())) {
                                                                        out.println(strBusinessTypeCode);
                                                                        break;
                                                                    }
                                                                }
                                                            %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap" class="main">
                                                            <%
                                                                Vector vecMerchantSegments = com.debisys.customers.Merchant.getMerchantSegments();
                                                                Iterator itMerchantSegments = vecMerchantSegments.iterator();
                                                                while (itMerchantSegments.hasNext()) {
                                                                    Vector vecTemp = null;
                                                                    vecTemp = (Vector) itMerchantSegments.next();
                                                                    String strMerchantSegmentId = vecTemp.get(0).toString();
                                                                    String strMerchantSegmentCode = vecTemp.get(1).toString();
                                                                    if (strMerchantSegmentId.equals(Integer.toString(Rep.getMerchantSegmentId()))) {
                                                                        out.println(Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + strMerchantSegmentCode, SessionData.getLanguage()));
                                                                        break;
                                                                    }
                                                                }
                                                            %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.businesshours", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap" class="main"><%= Rep.getBusinessHours()%></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                    <tr>
                                                        <td class="main">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.timeZone", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap" class="main"><%=TimeZone.getTimeZoneName(Rep.getTimeZoneId())%></td>
                                                    </tr>

                                                    <%
                                                        // DBSY-931 System 2000 Tools s2k
                                                        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                                                                && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                                                && strAccessLevel.equals(DebisysConstants.ISO)
                                                                && SessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) { //If when deploying in International and user is an ISO level
%>
                                                    <tr>
                                                        <td class="main">
                                                            <b><%= Languages.getString("jsp.admin.tools.s2k.addedit.terms", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap class="main"><%=Rep.getterms()%></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main">
                                                            <b><%= Languages.getString("jsp.admin.customers.salesmanid", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap class="main"><%=Rep.getsalesmanid()%>
                                                            <b>   <%= Languages.getString("jsp.admin.customers.salesmanname", SessionData.getLanguage())%>:   </b>
                                                            <%=Rep.getsalesmanname()%>
                                                        </td>
                                                    </tr>
                                                    <%
                                                        }

                                                        // DBSY-1072 eAccounts Interface
                                                        if (SessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                                                    %>
                                                    <tr>
                                                        <td class="main">
                                                            <b><%=Languages.getString("jsp.admin.customers.entity_account_type", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td class="main"><%=EntityAccountTypes.GetEntityAccountTypeDesc(Rep.getEntityAccountType())%></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                </table>
                                            </td>
                                        </tr>
                                        <%
                                            // Invoicing payment types for Mexico reps
                                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                // Get all the information required
                                                pageContext.setAttribute("invoicingView", "INFO");
                                                pageContext.setAttribute("invoicePaymentTypesSectionTitle", Languages.getString("jsp.admin.customers.reps.InvoiceTitle",SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicingTypeLabel", Languages.getString("jsp.admin.customers.reps.InvoiceType",SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicePaymentTypeNameColumTitle", Languages.getString("jsp.admin.customers.reps.InvoicePaymentType",SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicePaymentTypeAccountColumTitle", Languages.getString("jsp.admin.customers.reps.InvoicePaymentAccountNumber",SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicePaymentTypeImageEditLabel", Languages.getString("jsp.admin.customers.reps.InvoiceEdit", SessionData.getLanguage()));
                                                pageContext.setAttribute("invoicePaymentTypeImageDeleteLabel", Languages.getString("jsp.admin.customers.reps.InvoiceDelete", SessionData.getLanguage()));
                                                pageContext.setAttribute("addInvoicePaymentTypeButtonLabel", Languages.getString("jsp.admin.customers.reps.InvoiceAddPaymentType",SessionData.getLanguage()));
                                                InvoiceType currentRepInvoiceType = InvoicePaymentTypesDao.getInvoiceTypeById(Rep.getInvoiceType());                                                
                                                String invoicingTypeValue = (currentRepInvoiceType != null)?currentRepInvoiceType.getDescription():"";
                                                pageContext.setAttribute("invoicingTypeValue", invoicingTypeValue);
                                                String jsonString = request.getParameter("repInvoiceTypesList");
                                                Map<String, RepInvoicePaymentType> clientPaymentTypesMap = this.getClientPaymentTypes(jsonString);
                                                List<RepInvoicePaymentType> repInvoicePaymentTypes = null;
                                                // If there are values comming from the client they are assumed to be the last ones to be shown
                                                if(clientPaymentTypesMap != null){
                                                    repInvoicePaymentTypes = new ArrayList<RepInvoicePaymentType>(clientPaymentTypesMap.values());
                                                }else{
                                                    repInvoicePaymentTypes = InvoicePaymentTypesDao.getAllRepInvoicePaymentTypes(Long.valueOf(Rep.getRepId()));
                                                }
                                                pageContext.setAttribute("repInvoicePaymentTypes", repInvoicePaymentTypes);
                                        %>
                                            <%@ include file="/WEB-INF/jspf/admin/customers/reps/repInvoicePaymentTypesView.jspf" %>
                                        <%
                                            }
                                        %>

                                        <tr>
                                            <td class="formAreaTitle2">
                                                <br>
                                                <%= Languages.getString("jsp.admin.customers.reps_add.address", SessionData.getLanguage())%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2">
                                                <table width="100">
                                                    <tr class="main">
                                                        <%
                                                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) { //If when deploying in Mexico
%>
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_info.street", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td colspan="4"><%= Rep.getAddress()%></td>
                                                        <%
                                                        } else {//Else when deploying in others
%>
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.reps_add.street_address", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getAddress()%></td>
                                                        <%
                                                            }
                                                        %>
                                                    </tr>
                                                    <%
                                                        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {//If when deploying in Mexico
%>
                                                    <tr class="main">
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.address_colony", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getPhysColony()%></td>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.address_delegation", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getPhysDelegation()%></TD>
                                                    </tr>
                                                    <tr class="main">
                                                        <td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.city", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap"><%= Rep.getCity()%></td>
                                                        <td>&nbsp;</td>
                                                        <td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.zip_international", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap"><%= Rep.getZip()%></td>
                                                        <td>&nbsp;</td>
                                                        <td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.merchants_edit.state_domestic", SessionData.getLanguage())%></b></td>
                                                        <td nowrap="nowrap">
                                                            <%
                                                                Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
                                                                Iterator itStates = vecStates.iterator();
                                                                while (itStates.hasNext()) {
                                                                    Vector vecTemp = null;
                                                                    vecTemp = (Vector) itStates.next();
                                                                    String strStateId = vecTemp.get(0).toString();
                                                                    String strStateName = vecTemp.get(1).toString();
                                                                    if (strStateId.equals(Rep.getState())) {
                                                                        out.println(strStateName);
                                                                        break;
                                                                    }
                                                                }
                                                            %>
                                                        </td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td><b><%= Languages.getString("jsp.admin.customers.merchants_info.county", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap"><%= Rep.getPhysCounty()%></td>
                                                    </tr>
                                                    <%
                                                    } else {//Else when deploying in others
                                                    %>
                                                    <tr class="main">
                                                        <td nowrap="nowrap">
                                                            <b>
                                                                <%
                                                                    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                                        out.println(Languages.getString("jsp.admin.customers.reps_info.city_state_zip", SessionData.getLanguage()) + ":");
                                                                    } else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
                                                                        out.println(Languages.getString("jsp.admin.customers.reps_info.city_province_postal_code", SessionData.getLanguage()) + ":");
                                                                    }
                                                                %>
                                                            </b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getCity() + ", " + Rep.getState() + " " + Rep.getZip()%></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                    <tr class="main">
                                                        <td>
                                                            <%
                                                                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
                                                                    out.println("<b>");
                                                                }
                                                            %>
                                                            <%= Languages.getString("jsp.admin.customers.reps_add.country", SessionData.getLanguage())%>:
                                                        </td>
                                                        <td nowrap="nowrap">
                                                            <%= CountryInfo.getAllCountriesID().get(Integer.parseInt(Rep.getCountry()))%>
                                                        </td>
                                                    </tr>

                                                    <%
                                                        boolean hasPermissionGeolocation = SessionData.checkPermission(DebisysConstants.PERM_GEOLOCATION_FEAUTURE);
                                                        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && hasPermissionGeolocation) {
                                                    %>
                                                    <tr class="main">
                                                        <td><%=Languages.getString("jsp.admin.customers.merchants_edit.latitude", SessionData.getLanguage())%>:</td>
                                                        <td><%=Rep.getLatitude()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td><%=Languages.getString("jsp.admin.customers.merchants_edit.longitude", SessionData.getLanguage())%>:</td>
                                                        <td><%=Rep.getLongitude()%></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                </table>
                                            </td>
                                        </tr>
                                        <%
                                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {//If when deploying in Mexico
%>
                                        <tr>
                                            <td class="formAreaTitle2">
                                                <br>
                                                <%= Languages.getString("jsp.admin.customers.merchants_edit.billingaddress", SessionData.getLanguage())%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2">
                                                <table width="100">
                                                    <tr class="main">
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.street_address", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td colspan="4"><%= Rep.getMailAddress()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.address_colony", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getMailColony()%></td>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.address_delegation", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <TD nowrap="nowrap"><%= Rep.getMailDelegation()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td nowrap="nowrap">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.city", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getMailCity()%></td>
                                                        <td>&nbsp;</td>
                                                        <td nowrap="nowrap">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.zip_international", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getMailZip()%></td>
                                                        <td>&nbsp;</td>
                                                        <td nowrap="nowrap">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.state_domestic", SessionData.getLanguage())%></b>
                                                        </td>
                                                        <td nowrap="nowrap">
                                                            <%
                                                                Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
                                                                Iterator itStates = vecStates.iterator();
                                                                while (itStates.hasNext()) {
                                                                    Vector vecTemp = null;
                                                                    vecTemp = (Vector) itStates.next();
                                                                    String strStateId = vecTemp.get(0).toString();
                                                                    String strStateName = vecTemp.get(1).toString();
                                                                    if (strStateId.equals(Rep.getMailState())) {
                                                                        out.println(strStateName);
                                                                        break;
                                                                    }
                                                                }
                                                            %>
                                                        </td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.county", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getMailCounty()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.country", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= CountryInfo.getAllCountriesID().get(Integer.parseInt(Rep.getMailCountry()))%></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        <tr>
                                            <td class="formAreaTitle2">
                                                <br>
                                                <%= Languages.getString("jsp.admin.customers.reps_add.contact_information", SessionData.getLanguage())%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2">
                                                <%
                                                    if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                %>
                                                <table width="100%" cellspacing="1" cellpadding="1" border="0">
                                                    <tr>
                                                        <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contacttype", SessionData.getLanguage()).toUpperCase()%></td>
                                                        <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.contact", SessionData.getLanguage()).toUpperCase()%></td>
                                                        <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.phone", SessionData.getLanguage()).toUpperCase()%></td>
                                                        <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.fax", SessionData.getLanguage()).toUpperCase()%></td>
                                                        <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.email", SessionData.getLanguage()).toUpperCase()%></td>
                                                        <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.cellphone", SessionData.getLanguage()).toUpperCase()%></td>
                                                        <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_edit.department", SessionData.getLanguage()).toUpperCase()%></td>
                                                    </tr>
                                                    <%
                                                        int intEvenOdd = 1;
                                                        Vector vecContacts = Rep.getVecContacts();
                                                        Iterator itContacts = vecContacts.iterator();
                                                        while (itContacts.hasNext()) {
                                                            Vector vecTemp = null;
                                                            vecTemp = (Vector) itContacts.next();
                                                    %>
                                                    <tr class="row<%=intEvenOdd%>">
                                                        <td><%=com.debisys.customers.Merchant.getContactTypeById(vecTemp.get(0).toString(), SessionData)%></td>
                                                        <td><%=StringUtil.toString(vecTemp.get(1).toString())%></td>
                                                        <td><%=StringUtil.toString(vecTemp.get(2).toString())%></td>
                                                        <td><%=StringUtil.toString(vecTemp.get(3).toString())%></td>
                                                        <td><%=StringUtil.toString(vecTemp.get(4).toString())%></td>
                                                        <td><%=StringUtil.toString(vecTemp.get(5).toString())%></td>
                                                        <td><%=StringUtil.toString(vecTemp.get(6).toString())%></td>
                                                    </tr>
                                                    <%
                                                            intEvenOdd = ((intEvenOdd == 1) ? 2 : 1);
                                                        }
                                                    %>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2">
                                                <table width="100">
                                                    <tr class="main" nowrap>
                                                        <td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone", SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Rep.getSmsNotificationPhone())%></td>
                                                    </tr>
                                                    <tr class="main" nowrap>
                                                        <td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress", SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Rep.getMailNotificationAddress())%></td>
                                                    </tr>												
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications", SessionData.getLanguage())%>:</B>
                                                        </td>
                                                        <td nowrap class="main">
                                                            <input id="paymentNotifications" name="paymentNotifications" type="checkbox"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType", SessionData.getLanguage())%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td  nowrap class="main"><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td  nowrap class="main"><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" />
                                                        </td>
                                                    </tr>
                                                    <%
                                                        //DTU-480: Low Balance Alert Message SMS and/or email
                                                        if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)) {
                                                    %>
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications", SessionData.getLanguage())%>:</B>
                                                        </td>
                                                        <td nowrap class="main">
                                                            <input id="balanceNotifications" name="balanceNotifications" type="checkbox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType", SessionData.getLanguage())%>
                                                        </td>
                                                    </tr>	
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" />
                                                        </td>
                                                    </tr>													
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue", SessionData.getLanguage())%>:</td><td nowrap><%=Rep.getBalanceNotificationValue()%></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays", SessionData.getLanguage())%>:</td><td nowrap><%=Rep.getBalanceNotificationDays()%></td>
                                                    </tr>													
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType", SessionData.getLanguage())%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" />
                                                        </td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                </table>
                                                <script>disableChk();</script>
                                                <%
                                                    //DTU-480: Low Balance Alert Message SMS and/or email
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)) {
                                                %>
                                                <script>disableBalChk();</script>
                                                <%
                                                    }
                                                %>												
                                                <%
                                                } else {
                                                %>
                                                <table width="100">
                                                    <tr class="main">
                                                        <td nowrap="nowrap"><b><%= Languages.getString("jsp.admin.customers.reps_add.contact", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap"><%= Rep.getContactFirst() + " " + Rep.getContactMiddle() + " " + Rep.getContactLast()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td><b><%= Languages.getString("jsp.admin.customers.reps_add.phone", SessionData.getLanguage())%>:</b></td>
                                                        <td nowrap="nowrap"><%= Rep.getContactPhone()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td><%= Languages.getString("jsp.admin.customers.reps_add.fax", SessionData.getLanguage())%>:</td>
                                                        <td nowrap="nowrap"><%= Rep.getContactFax()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td><%= Languages.getString("jsp.admin.customers.reps_add.email", SessionData.getLanguage())%>:</td>
                                                        <td nowrap="nowrap"><%= Rep.getContactEmail()%></td>
                                                    </tr>
                                                    <tr class="main" nowrap>
                                                        <td><%=Languages.getString("jsp.admin.customers.merchants_edit.smsNotificationPhone", SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Rep.getSmsNotificationPhone())%></td>
                                                    </tr>
                                                    <tr class="main" nowrap>
                                                        <td><%=Languages.getString("jsp.admin.customers.merchants_edit.mailNotificationAddress", SessionData.getLanguage())%>:</td><td nowrap><%=StringUtil.toString(Rep.getMailNotificationAddress())%></td>
                                                    </tr>												
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enablePaymentNotifications", SessionData.getLanguage())%>:</B>
                                                        </td>
                                                        <td nowrap class="main">
                                                            <input id="paymentNotifications" name="paymentNotifications" type="checkbox" value="<%=Rep.isPaymentNotifications()%>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <%= Languages.getString("jsp.admin.customers.merchants_edit.paymentNotificationType", SessionData.getLanguage())%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="paymentNotificationSMS" name="paymentNotificationSMS" type="checkbox" value="<%=Rep.isPaymentNotificationSMS()%>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="paymentNotificationMail" name="paymentNotificationMail" type="checkbox" value="<%=Rep.isPaymentNotificationMail()%>" />
                                                        </td>
                                                    </tr>
                                                    <%
                                                        //DTU-480: Low Balance Alert Message SMS and/or email
                                                        if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)) {
                                                    %>
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <B><%= Languages.getString("jsp.admin.customers.merchants_edit.enableBalanceNotifications", SessionData.getLanguage())%>:</B>
                                                        </td>
                                                        <td nowrap class="main">
                                                            <input id="balanceNotifications" name="balanceNotifications" type="checkbox" value="<%=Rep.isBalanceNotifications()%>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationType", SessionData.getLanguage())%>
                                                        </td>
                                                    </tr>	
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="balanceNotificationTypeValue" name="balanceNotificationTypeValue" type="checkbox"  value="<%=Rep.isBalanceNotificationTypeValue()%>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="balanceNotificationTypeDays" name="balanceNotificationTypeDays" type="checkbox" value="<%=Rep.isBalanceNotificationTypeDays()%>" />
                                                        </td>
                                                    </tr>													
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationValue", SessionData.getLanguage())%>:</td><td nowrap><%=Rep.getBalanceNotificationValue()%></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationDays", SessionData.getLanguage())%>:</td><td nowrap><%=Rep.getBalanceNotificationDays()%></td>
                                                    </tr>													
                                                    <tr>
                                                        <td class="main" nowrap>
                                                            <%= Languages.getString("jsp.admin.customers.merchants_edit.balanceNotificationNotType", SessionData.getLanguage())%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationSms", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="balanceNotificationSMS" name="balanceNotificationSMS" type="checkbox" value="<%=Rep.isBalanceNotificationSMS()%>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="main" nowrap><%=Languages.getString("jsp.admin.customers.merchants_edit.notificationMail", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <input id="balanceNotificationMail" name="balanceNotificationMail" type="checkbox" value="<%=Rep.isBalanceNotificationMail()%>" />
                                                        </td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>										
                                                </table>
                                                <script>disableChk()</script>
                                                <%
                                                    //DTU-480: Low Balance Alert Message SMS and/or email
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS)) {
                                                %>
                                                <script>disableBalChk()</script>
                                                <%
                                                    }
                                                %>												
                                                <%
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                        <%
                                            if (SessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES)) {
                                        %>
                                        <tr>
                                            <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.common_info_edit.payment_information", SessionData.getLanguage())%></td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2">
                                                <table width="100">
                                                    <tr class="main">
                                                        <td nowrap="nowrap">
                                                            <%=Languages.getString("jsp.admin.customers.common_info_edit.payment_type", SessionData.getLanguage())%>
                                                        </td>
                                                        <td nowrap="nowrap">
                                                            <select name="paymentType" disabled="disabled">
                                                                <%
                                                                    Vector vecPaymentTypes = com.debisys.customers.Rep.getPaymentTypes();
                                                                    Iterator itPaymentTypes = vecPaymentTypes.iterator();
                                                                    boolean flag_payment = false;
                                                                    while (itPaymentTypes.hasNext()) {
                                                                        Vector vecTemp = null;
                                                                        vecTemp = (Vector) itPaymentTypes.next();
                                                                        String strPaymentTypeId = vecTemp.get(0).toString();
                                                                        String strPaymentTypeCode = vecTemp.get(1).toString();
                                                                        if ((request.getParameter("paymentType") != null) && (strPaymentTypeId.equals(request.getParameter("paymentType")))) {
                                                                            flag_payment = true;
                                                                            out.println("<option value=\"" + strPaymentTypeId + "\" selected>" + strPaymentTypeCode + "</option>");
                                                                        } else if (strPaymentTypeId.equals(Integer.toString(Rep.getPaymentType()))) {
                                                                            flag_payment = true;
                                                                            out.println("<option value=\"" + strPaymentTypeId + "\" selected>" + strPaymentTypeCode + "</option>");
                                                                        } else {
                                                                            out.println("<option value=\"" + strPaymentTypeId + "\">" + strPaymentTypeCode + "</option>");
                                                                        }
                                                                    }
                                                                    if (!flag_payment) {
                                                                %>
                                                                <option value="-1" selected="selected">--</option>
                                                                <%
                                                                    }
                                                                %>
                                                            </select>
                                                        </td>
                                                        <td nowrap="nowrap">
                                                            <%
                                                                if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                            %>
                                                            <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type", SessionData.getLanguage())%>
                                                            <%
                                                            } else {
                                                            %>
                                                            <%=Languages.getString("jsp.admin.customers.common_info_edit.process_type_international", SessionData.getLanguage())%>
                                                            <%
                                                                }
                                                            %>
                                                        </td>
                                                        <td nowrap="nowrap">
                                                            <select name="processType" disabled="disabled">
                                                                <%
                                                                    Vector vecProcessTypes = com.debisys.customers.Rep.getProcessorTypes();
                                                                    Iterator itProcessTypes = vecProcessTypes.iterator();
                                                                    boolean flag_process = false;
                                                                    while (itProcessTypes.hasNext()) {
                                                                        Vector vecTemp = null;
                                                                        vecTemp = (Vector) itProcessTypes.next();
                                                                        String strProcessTypeId = vecTemp.get(0).toString();
                                                                        String strProcessTypeCode = vecTemp.get(1).toString();
                                                                        if ((request.getParameter("processType") != null) && (strProcessTypeId.equals(request.getParameter("processType")))) {
                                                                            flag_process = true;
                                                                            out.println("<option value=\"" + strProcessTypeId + "\" selected>" + strProcessTypeCode + "</option>");
                                                                        } else if (strProcessTypeId.equals(Integer.toString(Rep.getProcessType()))) {
                                                                            flag_process = true;
                                                                            out.println("<option value=\"" + strProcessTypeId + "\" selected>" + strProcessTypeCode + "</option>");
                                                                        } else {
                                                                            out.println("<option value=\"" + strProcessTypeId + "\">" + strProcessTypeCode + "</option>");
                                                                        }
                                                                    }
                                                                    if (!flag_process) {
                                                                %>
                                                                <option value="-1" selected="selected">--</option>
                                                                <%
                                                                    }
                                                                %>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {//If when deploying in Mexico
%>
                                        <tr>
                                            <td class="formAreaTitle2">
                                                <br>
                                                <%= Languages.getString("jsp.admin.customers.merchants_edit.banking_information", SessionData.getLanguage())%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2">
                                                <table width="100">
                                                    <tr class="main">
                                                        <td nowrap="nowrap">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.tax_id", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getTaxId()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td nowrap="nowrap">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.bankname", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td><%= Rep.getBankName()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td nowrap="nowrap">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.accounttype", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td>
                                                            <%
                                                                Vector vecAccountTypes = com.debisys.customers.Merchant.getAccountTypes();
                                                                Iterator itAccountTypes = vecAccountTypes.iterator();
                                                                while (itAccountTypes.hasNext()) {
                                                                    Vector vecTemp = null;
                                                                    vecTemp = (Vector) itAccountTypes.next();
                                                                    String strAccountTypeId = vecTemp.get(0).toString();
                                                                    String strAccountTypeCode = vecTemp.get(1).toString();
                                                                    if (strAccountTypeId.equals(Integer.toString(Rep.getAccountTypeId()))) {
                                                                        out.println(Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + strAccountTypeCode, SessionData.getLanguage()));
                                                                        break;
                                                                    }
                                                                }
                                                            %>
                                                        </td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td nowrap="nowrap">
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.routing_number", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getRoutingNumber()%></td>
                                                    </tr>
                                                    <tr class="main">
                                                        <td>
                                                            <b><%= Languages.getString("jsp.admin.customers.merchants_edit.account_number", SessionData.getLanguage())%>:</b>
                                                        </td>
                                                        <td nowrap="nowrap"><%= Rep.getAccountNumber()%></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%
                                            }//End of if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
%>
                                        <tr>
                                            <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.reps_add.credit_limit", SessionData.getLanguage())%>
                                                <%
                                                    if (strMessage.equals("2")) {
                                                        //jsp.admin.error2=Please enter a valid number.
                                                        out.println("<br><font color=ff0000>" + Languages.getString("jsp.admin.error2", SessionData.getLanguage()) + "</font>");
                                                    } else if (strMessage.equals("3")) {
                                                        //jsp.admin.customers.reps_info.error3=Please enter a value greater than or equal to 0.
                                                        out.println("<br><font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error3", SessionData.getLanguage()) + "</font>");
                                                    } else if (strMessage.equals("4")) {
                                                        //jsp.admin.customers.reps_info.error4=The credit limit must be greater than the assigned credit.
                                                        out.println("<br><font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error4", SessionData.getLanguage()) + "</font>");
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2" valign="top">
                                                <table width="100%">
                                                    <%
                                                        String merchantTypePrepaid = "";
                                                        String merchantTypeCredit = "";
                                                        String merchantTypeUnlimited = "";
                                                        //if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) //only when Prepaid-Prepaid enabled this option is valid
                                                        if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                                                            merchantTypePrepaid = " selected";
                                                        } else if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                                                            merchantTypeCredit = " selected";
                                                        } else if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                                                            merchantTypeUnlimited = " selected";
                                                        } else {
                                                            merchantTypeCredit = " selected";
                                                        }
                                                        // DBSY-804
                                                        // Get the credit type here and determine if Flex
                                                        String isoid = SessionData.getProperty("iso_id");
                                                        String credType = Rep.getISOCreditType(isoid);

                                                        List<Boolean> otherOptions = CreditTypes.getFormOptionsReps(
                                                                customConfigType, deploymentType, Rep.getRepCreditType(),
                                                                credType, new String[]{merchantTypePrepaid, merchantTypeCredit, merchantTypeUnlimited}, DebisysConstants.REP_TYPE_REP);

                                                        boolean showApplyPaymentOption = otherOptions.get(0);
                                                        if ( Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT) && isDomestic){
                                                            showApplyPaymentOption = false;
                                                        }
                                                        boolean showPaymentOptions = otherOptions.get(1);
                                                        boolean showUpdateCreditLimitOption = otherOptions.get(2);
                                                        boolean disableApplyPaymentButton = otherOptions.get(3);
                                                        boolean disableCreditTypeCombo = otherOptions.get(4);

                                                        if (DebisysConfigListener.getDebugJS(application)) {
                                                            String vars = "\"repType=" + Rep.getRepCreditType() + "\"";
                                                            out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
                                                            vars = "\"showApplyPaymentOption=" + showApplyPaymentOption + " - showUpdateCreditLimitOption=" + showUpdateCreditLimitOption + " - sharedBalance=" + Rep.getSharedBalance() + " - disableCreditTypeCombo=" + disableCreditTypeCombo + "\"";
                                                            out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
                                                        }
                                                        if (showUpdateCreditLimitOption) {
                                                    %>
                                                    <tr class="main">
                                                        <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.reps_add.credit_limit", SessionData.getLanguage())%></td>
                                                        <td nowrap="nowrap">
                                                            <form name="creditLimit" method="post" action="admin/customers/reps_update_credit_limit.jsp" onSubmit="return checkCreditLimit();">
                                                                <input type="hidden" name="repId" value="<%= Rep.getRepId()%>">
                                                                <input type="text" name="creditLimit" value="<%= NumberUtil.formatAmount(Rep.getCreditLimit())%>" size="8" maxlength="20">
                                                                <input id="btnUpdateCreditLimit" type="submit" name="submit" value="<%= Languages.getString("jsp.admin.customers.reps_info.update", SessionData.getLanguage())%>">
                                                            </form>
                                                        </td>
                                                        <%
                                                            }
                                                            if (strMessage.equals("6")) {
                                                                //enter a valid number
                                                                out.println("<font color=ff0000>" + Languages.getString("jsp.admin.error2", SessionData.getLanguage()) + "</font><br>");
                                                            } else if (strMessage.equals("7")) {
                                                                //greater than or equal to 0
                                                                out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error3", SessionData.getLanguage()) + "</font><br>");
                                                            } else if (strMessage.equals("8")) {
                                                                //greater than or equal to 0
                                                                out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error8", SessionData.getLanguage()) + "</font><br>");
                                                            } else if (strMessage.equals("9")) {
                                                                //running liability less than 0
                                                                out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error9", SessionData.getLanguage()) + "</font><br>");
                                                            } else if (strMessage.equals("15")) {
                                                                //jsp.admin.customers.reps_info.error5=A credit/prepaid rep can not have unlimited credit merchants.<br>Please change merchants to limited before continuing.
                                                                out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error15", SessionData.getLanguage()) + "</font><br>");
                                                            } else if (strMessage.equals("16")) {
                                                                out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error16", SessionData.getLanguage()) + "</font><br>");
                                                            } else if (strMessage.equals("31")) {
                                                                out.println("<font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error16", SessionData.getLanguage()) + "</td></tr>");
                                                            }
                                                        %>
                                                    </tr>
                                                    <%
                                                        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && Rep.getSharedBalance().equals("1")) {
                                                    %>
                                                    <tr class="main">
                                                        <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.reps_info.runningliability", SessionData.getLanguage())%></td>
                                                        <td><%=NumberUtil.formatCurrency(Rep.getRunningLiability())%></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                    <tr class="main">
                                                        <br>
                                                        <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.reps_info.credit_avail", SessionData.getLanguage())%>:</td>
                                                        <td>
                                                            <%
                                                                if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                                                                    out.print(Languages.getString("jsp.admin.customers.reps_add.credit_type3", SessionData.getLanguage()));
                                                                } else {
                                                                    out.print(NumberUtil.formatCurrency(Rep.getAvailableCredit()));
                                                                }
                                                            %>
                                                        </td>
                                                        <%if(SessionData.checkPermission(DebisysConstants.PERM_REP_MERCHANTS_BALANCE_SUM)){ %>
                                                        
                                                        <td>
                                                            <button id="btnCheckMerchantsBalance" name="btnCheckMerchantsBalance">
                                                               <%=Languages.getString("jsp.admin.customers.reps_info.merchants_credit_avail", SessionData.getLanguage())%>
                                                            </button>                                       
                                                        </td>
                                                        <td>
                                                            <span id="spMerchantsBalance"></span>                                                   
                                                        </td>
                                                        <%}%>
                                                        
                                                    </tr>
                                                    <%
                                                        if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {//When MX, show the SharedBalance option
%>
                                                    <tr class="main">
                                                        <td nowrap="nowrap"><%=Languages.getString("jsp.admin.customers.reps.sharedbalance", SessionData.getLanguage())%></td>
                                                        <td nowrap="nowrap">
                                                            <form id='frmRepMerchantMigration' action='/support/admin/customers/reps_merchant_migration.jsp?repId=<%=Rep.getRepId()%>' method='post'>
                                                                <script>
                                                                    function EnableSharedBalance() {
                                                                        var sURL = "/support/admin/customers/reps_merchant_migration.jsp?repId=<%=Rep.getRepId()%>&bShared=1";
                                                                        var sOptions = "left=" + (screen.width - (screen.width * 0.7)) / 2 + ",top=" + (screen.height - (screen.height * 0.4)) / 2 + ",width=" + (screen.width * 0.7) + ",height=" + (screen.height * 0.4) + ",location=no,menubar=no,resizable=yes,scrollbars=yes";
                                                                        var w = window.open(sURL, "RepMerchantMigration", sOptions, true);
                                                                        w.focus();
                                                                    }
                                                                    function DisableSharedBalance() {
                                                                        var sURL = "/support/admin/customers/reps_merchant_migration.jsp?repId=<%=Rep.getRepId()%>&bShared=0";
                                                                        var sOptions = "left=" + (screen.width - (screen.width * 0.7)) / 2 + ",top=" + (screen.height - (screen.height * 0.4)) / 2 + ",width=" + (screen.width * 0.7) + ",height=" + (screen.height * 0.4) + ",location=no,menubar=no,resizable=yes,scrollbars=yes";
                                                                        var w = window.open(sURL, "RepMerchantMigration", sOptions, true);
                                                                        w.focus();
                                                                    }
                                                                </script>
                                                                <%
                                                                    String sYes = "";
                                                                    String sNo = "";
                                                                    String sText = "";
                                                                    if (Rep.getSharedBalance().equals("1")) {
                                                                        out.print("<input type='hidden' name='bNewShared' value='0'>");
                                                                        sYes = "checked";
                                                                        sText = "<a href=\"javascript:DisableSharedBalance();\">" + Languages.getString("jsp.admin.customers.reps.disableshared", SessionData.getLanguage()) + "</a>";
                                                                    } else {
                                                                        out.print("<input type='hidden' name='bNewShared' value='1'>");
                                                                        sNo = "checked";
                                                                        sText = "<a href=\"javascript:EnableSharedBalance();\">" + Languages.getString("jsp.admin.customers.reps.enableshared", SessionData.getLanguage()) + "</a>";
                                                                    }
                                                                    out.print("<label for='rbtnSharedBalanceYes'><input id='rbtnSharedBalanceYes' type=radio name=sharedBalance value='1' disabled " + sYes + ">" + Languages.getString("jsp.admin.customers.reps.sharedbalance_status_yes", SessionData.getLanguage()) + "</label>");
                                                                    out.print("<label for='rbtnSharedBalanceNo'><input id='rbtnSharedBalanceNo' type=radio name=sharedBalance value='0' disabled " + sNo + ">" + Languages.getString("jsp.admin.customers.reps.sharedbalance_status_no", SessionData.getLanguage()) + "</label>");
                                                                    out.print("&nbsp;&nbsp;" + sText);
                                                                %>
                                                                <input id='txtRepMerchantMigration' type="hidden" name="sRepMerchantMigration" value="">
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    <%
                                                        }//End of if When MX, show the SharedBalance option
                                                        if (!Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                                                    %>
                                                    <tr class="main">
                                                        <td colspan="2" nowrap="nowrap">
                                                            <br>
                                                            <script>
                                                                repId = '<%=Rep.getRepId()%>';
                                                                depInfo = '<%=DebisysConfigListener.getDeploymentType(application) + "_" + DebisysConfigListener.getCustomConfigType(application)%>';
                                                                saveButText = '<%=Languages.getString("jsp.admin.pincache.ok", SessionData.getLanguage())%>';
                                                                cancelButText = '<%=Languages.getString("jsp.admin.pincache.cancel", SessionData.getLanguage())%>';
                                                                maxBalance = <%=Rep.getAvailableCredit()%>;
                                                                rbDialogTitle = '<%=Languages.getString("jsp.admin.reservedbalance.title", SessionData.getLanguage())%>';
                                                            </script>
                                                            <b><%=Languages.getString("jsp.admin.reservedbalance.title", SessionData.getLanguage())%></b>
                                                            <span id="reservedBalance" width="50px"><%=NumberUtil.formatCurrency(String.valueOf(PcReservedBalance.getRBByRepId(Rep.getRepId())))%></span>
                                                            <button id="rbReserve"><%=Languages.getString("jsp.admin.reservedbalance.reserve", SessionData.getLanguage())%></button>
                                                            <button id="rbReturn"><%=Languages.getString("jsp.admin.reservedbalance.return", SessionData.getLanguage())%></button>
                                                            <div id="rbReserveDialog">
                                                                <label><%=Languages.getString("jsp.admin.reservedbalance.toreserve", SessionData.getLanguage())%></label><br>
                                                                <input type="text" class="numeric text ui-widget-content ui-corner-all" id="valueToReserve" name="valueToReserve">
                                                            </div>
                                                            <div id="rbReturnDialog">
                                                                <label><%=Languages.getString("jsp.admin.reservedbalance.toreturn", SessionData.getLanguage())%></label>
                                                                <input type="text" class="numeric text ui-widget-content ui-corner-all" id="valueToReturn" name="valueToReturn">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.reps_info.credit_type", SessionData.getLanguage())%></td>
                                        </tr>
                                        <tr>
                                            <td class="formArea2" valign="top">
                                                <table width='100%'>
                                                    <tr class="main">
                                                        <%
                                                            if (strMessage.equals("5")) {
                                                                //jsp.admin.customers.reps_info.error5=A credit/prepaid rep can not have unlimited credit merchants.<br>Please change merchants to limited before continuing.
                                                                out.println("<br><font color=ff0000>" + Languages.getString("jsp.admin.customers.reps_info.error5", SessionData.getLanguage()) + "</font>");
                                                            }
                                                        %>
                                                        <td nowrap="nowrap" width="110"><%=Languages.getString("jsp.admin.customers.reps_add.rep_type", SessionData.getLanguage())%></td>
                                                        <td>
<%
                                                            String s_RepCreditType = CreditTypes.defaultCreditType(deploymentType, Rep.getRepCreditType());
                                                            String labelCreditType = ((s_RepCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) 
                                                                                        ? Languages.getString("jsp.admin.customers.reps_add.credit_type1", SessionData.getLanguage())
                                                                                        : ((s_RepCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) 
                                                                                            ? Languages.getString("jsp.admin.customers.reps_add.credit_type2", SessionData.getLanguage())
                                                                                            : Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage()) ));
                                                            
                                                            
                                                            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {


                                                                if (DebisysConfigListener.getDebugJS(application)) {
                                                                    String vars = "\"isoID=" + isoid + " - ref_id=" + SessionData.getProperty("ref_id") + "\"";
                                                                    out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
                                                                }
                                                                Map<String, String> creditTypeComboOptions
                                                                        = CreditTypes.getAvailableOptionsForRep(credType, customConfigType, deploymentType, 
                                                                                new String[]{merchantTypePrepaid, merchantTypeCredit, merchantTypeUnlimited}, true);
                                                                String disabledText = "";
                                                                if (disableCreditTypeCombo) {
                                                                    disabledText = "disabled=\"true\"";
                                                                }
                                                                if (DebisysConfigListener.getDebugJS(application)) {
                                                                    String vars = "\"";
                                                                    for (String opt : creditTypeComboOptions.keySet()) {
                                                                        vars += opt + "=" + creditTypeComboOptions.get(opt) + " ";
                                                                    }
                                                                    vars += "\"";
                                                                    out.print("<Script type=\"text/javascript\">alert(" + vars + ")</script>");
                                                                }
%>
                                                                <form method="post" action="admin/customers/reps_make_payment.jsp" onsubmit="DisableButtons();">
                                                                    <input type="hidden" name="sRepMerchantPayment" value="">
                                                                    <input type="hidden" name="repId" value="<%=Rep.getRepId()%>">
                                                                    <input type="hidden" name="oldRepCreditType" value="<%=s_RepCreditType%>">
                                                                    <input type="hidden" name="paymentAmount" value="0.00">
                                                                    <input type="hidden" name="commission" value="0">
                                                                    <input type="hidden" name="netPaymentAmount" value="0.00">
                                                                    <input type="hidden" name="paymentDescription" value="">
                                                                    <select name="repCreditType" <%=disabledText%>>
                                                                        <%
                                                                            for (String option : creditTypeComboOptions.keySet()) {
                                                                                String valueText = "jsp.admin.customers.reps_add.credit_type" + option;
                                                                        %>
                                                                        <option value="<%=option%>" <%=creditTypeComboOptions.get(option)%>><%=Languages.getString(valueText, SessionData.getLanguage())%></option>
                                                                        <%
                                                                            }
                                                                        %>
                                                                    </select>&nbsp;&nbsp;&nbsp;
                                                                    <input id="btnChangeType" type="submit" <%=disabledText%> value="<%=Languages.getString("jsp.admin.customers.reps_info.changecredittype", SessionData.getLanguage())%>">
                                                                </form>                                                                
<%
                                                            } else if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
%>                            
                                                                <%= labelCreditType %>
<%
                                                            }      
%> 
                                                        </td>
                                                    </tr>
                                                    <%
                                                        String sRepMerchantPayment = "";
                                                        if (Rep.getSharedBalance().equals("1") && (Rep.getRepMerchants(SessionData, application).size() > 0)) {
                                                            sRepMerchantPayment = "return checkRepMerchantPayment();";
                                                        } else {
                                                            sRepMerchantPayment = "return checkPayment();";
                                                        }
                                                    %>
                                                    
                                                </table>
                                            </td>
                                        </tr>
                                        <%
                                            if (SessionData.checkPermission(DebisysConstants.PERM_CREDIT_LIMITS)) {
                                                if (showApplyPaymentOption ) {//&& Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
                                                    if ((customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) &&
                                                            (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID))){
                                                        // Session data comes from the main rep page where the JSPF is inserted
                                                        setRepPaymentSessionGlobals(pageContext, Rep.getRepId(),
                                                                SessionData.getUser().getUsername(), deploymentType,
                                                                customConfigType, SessionData.getLanguage());
                                                        setJsInvoicingGlobals(out, SessionData.getLanguage());
                                                        SimpleDateFormat defaultDateFromatter = new SimpleDateFormat("MM/dd/yyyy");

                                                        %>
                                                        <script src="js/regexpValidationFunctions.js" type="text/javascript"></script>
                                                        <script src="js/admin/customers/reps/applyPayment/applyPayment.js" type="text/javascript"></script>
                                                        <%@ include file="/WEB-INF/jspf/admin/customers/reps/applyPayment/repApplyPaymentCommon.jspf" %>

                                                        <tr>
                                                            <td><br></td>
                                                        </tr>
                                                        <tr>                                                            
                                                            <td class="formAreaTitle2">
                                                                ${paymentSectionTitle}                                                                                
                                                            </td>                                                                                                                
                                                        </tr>
                                                        <tr>
                                                            <td class="formArea2">
                                                                <input type="button" id="applyPaymentButton" name="applyPaymentButton" value="${applyPaymentButtonLbl}" onclick="$('#frmRepPaymentMx').dialog('open');clearRepPaymentForm();">        
                                                                <input type="button" id="viewPaymentHistory"  name="viewPaymentHistory" value="${viewPaymentHistoryLbl}" onClick="window.open('/support/admin/customers/reps_credit_history.jsp?repId=${repPaymentRepId}', 'repHistory1', 'width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
                                                                <input type="button" id = "viewCreditAssignmentHistory" name="viewCreditAssignmentHistory" value="${viewAssignedCreditHistoryLbl}" onClick="window.open('/support/admin/customers/reps_merchant_credit_history.jsp?repId=${repPaymentRepId}', 'repHistory2', 'width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
                                                                * ${paymentHistoryRangeLimitLbl}
                                                                &nbsp;${repPaymentHistoryLimitDays}&nbsp;
                                                                ${paymentHistoryRangeLimitTimeUnit}
                                                            </td>
                                                        </tr>
                                                        <%
                                                    }else{
                                        %>
                                        <tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.reps_info.apply_payment", SessionData.getLanguage())%></td></tr>
                                        <tr>
                                            <td class="formArea2" valign="top">
                                                <form name="payment" method="post" action="admin/customers/reps_make_payment.jsp" onSubmit="<%=sRepMerchantPayment%>">
                                                    <input id='txtRepMerchantPayment' type="hidden" name="sRepMerchantPayment" value="">
                                                    <input type="hidden" name="repId" value="<%=Rep.getRepId()%>">
                                                    <%s_RepCreditType = CreditTypes.defaultCreditType(deploymentType, Rep.getRepCreditType());%>
                                                    <input type="hidden" name="oldRepCreditType" value="<%=s_RepCreditType%>">
                                                    <input type="hidden" name="repCreditType" value="<%=Rep.getRepCreditType()%>">
                                                    <table width="100%">
                                                        <tr class="main">
                                                            <td nowrap="nowrap" colspan="2">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr class="main">
                                                                        <td align="center" nowrap="nowrap">
                                                                            <b><%=Languages.getString("jsp.admin.customers.reps_info.gross_payment", SessionData.getLanguage())%></b>
                                                                            <%
                                                                                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) || customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                                                    String sScript = "";
                                                                                    String sBlurScript = "this.value=formatAmount(this.value);";
                                                                                    if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                                                                                        sScript = "ValidatePaymentValueMX(this, true);calculate(this);";
                                                                                        sBlurScript += "ValidatePrepaidLimitMX(this);";
                                                                                    } else if (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                                                                                        sScript = "ValidatePaymentValueMX(this, true);calculate(this);";
                                                                                        sBlurScript += "ValidatePrepaidLimitMX(this);ValidateCreditLimitMX(this);";
                                                                                    } else {
                                                                                        sScript = "ValidatePaymentValueMX(this, false);calculate(this);";
                                                                                        sBlurScript += "ValidateUnlimitedMX(this);";
                                                                                    }
                                                                            %>
                                                                            <input type="text" name="paymentAmount" value="0.00" size="7" onpropertychange="<%=sScript%>" onblur="<%=sBlurScript%>">
                                                                            <%
                                                                            } else {
                                                                            %>
                                                                            <input type="text" name="paymentAmount" value="0.00" size="7" onKeyUp="calculate(this);" onBlur="return validate(this);">
                                                                            <%
                                                                                }
                                                                            %>
                                                                        </td>
                                                                        <td align="center" nowrap="nowrap">&nbsp;<b>-</b>&nbsp;</td>
                                                                        <td align="center" nowrap="nowrap"><b><%=Languages.getString("jsp.admin.customers.reps_info.commission", SessionData.getLanguage())%></b><input type="text" name="commission" value="0" size="3" onKeyUp="calculate(this);" onBlur="return validate(this);"><b>%</b></td>
                                                                        <td align="center" nowrap="nowrap">&nbsp;<b>=</b>&nbsp;</td>
                                                                        <td align="center" nowrap="nowrap"><input type="text" name="netPaymentAmount" value="0.00" size="7" readonly="readonly" style="color:#0000FF;background:#C0C0C0;"><b><%=Languages.getString("jsp.admin.customers.reps_info.net_payment", SessionData.getLanguage())%></b></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="main">
                                                            <td colspan="2"><b><%=Languages.getString("jsp.admin.customers.reps_info.description", SessionData.getLanguage())%>:</b><input type="text" name="paymentDescription" value="" size="20">(<%=Languages.getString("jsp.admin.optional", SessionData.getLanguage())%>)</td>
                                                        </tr>
                                                        <tr class="main">
                                                            <td colspan="2" nowrap="nowrap">
                                                                <input id="btnApplyPayment" type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.reps_info.update", SessionData.getLanguage())%>">&nbsp;
                                                                <input type="button" name="view_history" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_history", SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/reps_credit_history.jsp?repId=<%=Rep.getRepId()%>', 'repHistory1', 'width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
                                                                <input type="button" name="view_history2" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_credit_assignment_history", SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/reps_merchant_credit_history.jsp?repId=<%=Rep.getRepId()%>', 'repHistory2', 'width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">
                                                                <%int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays("reps_info", application);%>
                                                                * <%=Languages.getString("jsp.admin.customers.noteHistory1", SessionData.getLanguage())%>
                                                                <%=" " + limitDays + " "%>
                                                                <%=Languages.getString("jsp.admin.customers.noteHistory2", SessionData.getLanguage())%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </td>
                                        </tr>
                                        <%
                                                    }
                                                }
                                            }
                                            if (SessionData.checkPermission(DebisysConstants.PERM_CLERK_CODES) && !strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                        %>
                                        <tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.customers.reps_info.add_global_clerk_codes_title", SessionData.getLanguage())%></td></tr>
                                        <tr>
                                            <td class="formArea2" valign="top">
                                                <table width="100%">
                                                    <tr class="main">
                                                        <td colspan="2" nowrap="nowrap">
                                                            <a href="admin/customers/reps_clerk_codes.jsp?repId=<%=Rep.getRepId()%>"><%=Languages.getString("jsp.admin.customers.reps_info.add_global_clerk_codes", SessionData.getLanguage())%></a>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        <tr><td class="formAreaTitle2"><br><%=Languages.getString("jsp.admin.reports.churn.notifications", SessionData.getLanguage())%></td></tr>
                                        <tr>
                                            <td class="formArea2" valign="top">
                                                <table width="100%">
                                                    <tr class="main">
                                                        <td colspan="2" nowrap="nowrap">
                                                            <a href="admin/customers/churn_reporting_options.jsp?repId=<%=Rep.getRepId()%>"><%=Languages.getString("jsp.admin.reports.churn.reportingoptions", SessionData.getLanguage())%></a>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
              
<%
    if (( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) ) &&
            (Rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID))){
%>
<div id="frmRepPaymentMx" title="${formWindowTitle}" class="main" hidden="">
    <form id="paymentForm">

        <%@ include file="/WEB-INF/jspf/admin/customers/reps/applyPayment/repApplyPaymentView.jspf" %> 

    </form>
</div>
<%  }%>                                                        
                                                        
<%@ include file="/includes/footer.jsp" %>