<%@ page import="javax.mail.Session,
                 javax.mail.internet.MimeMessage,
                 javax.mail.Message,
                 javax.mail.internet.InternetAddress,
                 javax.mail.MessagingException,
                 javax.mail.Transport,
                 java.util.Hashtable,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.StringUtil,
                 com.debisys.utils.ValidateEmail,
                 com.debisys.users.User"%>
<%
int section=6;
int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ValidateEmail" class="com.debisys.utils.ValidateEmail" scope="page"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
  ValidateEmail validateEmail = new ValidateEmail();
  Hashtable hashErrors = new Hashtable();
  boolean blnSendSuccess = false;
  String strSubject = request.getParameter("subject");
  String strMessage = request.getParameter("message");
  String strContactType = request.getParameter("contactType");
  String strContact = request.getParameter("contact");

  if (request.getParameter("submit") != null)
  {
  if (strSubject == null || strSubject.equals(""))
  {
    hashErrors.put("subject","");
  }
  if (strMessage == null || strMessage.equals(""))
  {
    hashErrors.put("message","");
  }
  if (strContactType == null || strContactType.equals(""))
  {
    hashErrors.put("contactType","");
  }
    if (strContact == null || strContact.equals(""))
    {
      hashErrors.put("contact","");
    }
    else if (strContactType.equalsIgnoreCase("email"))
    {

      validateEmail.setStrEmailAddress(strContact);
      validateEmail.checkSyntax();
      if (!validateEmail.isEmailValid())
      {
        hashErrors.put("email","");
      }

  }

    if (hashErrors.size() == 0)
    {
    try
    {
      String strSendMessage = "IP Addr  :" + request.getRemoteAddr() + '\n' +
                              "Username :" + SessionData.getProperty("username") + '\n' +
                              "Name     :" + SessionData.getProperty("contact_name") + '\n' +
                              "Company  :" + SessionData.getProperty("company_name") + "\n\n" +
                              "Please contact this user by " + strContactType + " at " + strContact + "\n\nMessage Submitted:\n\n" +
                              strMessage;
      String strSendSubject = "Website Support Request:" + strSubject;

      java.util.Properties props;
      props = System.getProperties();
      props.put("mail.smtp.host", DebisysConfigListener.getMailHost(application));
      Session s = Session.getDefaultInstance(props, null);


      MimeMessage message = new MimeMessage(s);
	  if (!customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
	  {
	      if (request.getParameter("department").equals("1"))
	      {
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress("customerservice@emida.net"));
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress("cs@emida.net"));
	      }
	      else if (request.getParameter("department").equals("2"))
	      {
	    	String strSetupMail = ValidateEmail.getSetupMail(application);
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress(strSetupMail));
	      }
	      else if (request.getParameter("department").equals("3"))
	      {
//          message.setRecipient(Message.RecipientType.TO,new InternetAddress("ahuggett@emida.net"));
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress("pinreturns@emida.net"));
	      }
	      else if (request.getParameter("department").equals("4"))
	      {
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress("accounting@emida.net"));        
	      }
	      else if (request.getParameter("department").equals("5"))
	      {
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress("mis@debisys.com"));
	      }
	      else
	      {
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress("support@debisys.com"));
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress("cs@emida.net"));
	      }
      }
      else
      {
	        message.setRecipient(Message.RecipientType.TO,new InternetAddress("ops-mx@emida.net"));      
      }
      


      User user = new User();
      String strISOEmail = user.getISOEmail(SessionData);
      validateEmail.setStrEmailAddress(strISOEmail);
      validateEmail.checkSyntax();


      if (strISOEmail != null && !strISOEmail.equals("") && validateEmail.isEmailValid())
      {
        message.setRecipient(Message.RecipientType.CC,new InternetAddress(strISOEmail));
      }


            if ((strContactType != null) && (strContactType.equalsIgnoreCase("email")))
            {
              message.setFrom(new InternetAddress(strContact));
            }
            else
            {
              message.setFrom(new InternetAddress("support@debisys.com"));
            }
      message.setSubject(strSendSubject);
      message.setText(strSendMessage);
      Transport.send(message);
      blnSendSuccess = true;
    }
    catch (MessagingException e)
    {
      blnSendSuccess = false; 
      hashErrors.put("send","");
      //out.println(e.toString());
    }

    }


  }
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle"><b><%=Languages.getString("jsp.admin.contact.index.title",SessionData.getLanguage()).toUpperCase()%></b></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
 </tr>
 <tr>
 	<td colspan="3">
   	<table width="100%" border="0" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0">
 	    <tr>
 		      <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
  		    <td align="left" valign="top" bgcolor="#FFFFFF">
   			    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
    			    <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main">
                 <%if (blnSendSuccess) 
                   {
                     out.println(Languages.getString("jsp.admin.contact.index.success",SessionData.getLanguage()) + "<br><br><br><br><br><br>");
                    }
                    else
                    {
                      out.println(Languages.getString("jsp.admin.contact.index.instructions",SessionData.getLanguage()) + "<br><br>");

                      if (hashErrors != null && hashErrors.size() > 0)
                      {
                        if (hashErrors.containsKey("send"))
                        {
                          out.println("<font color=ff0000>"+Languages.getString("jsp.admin.contact.index.error1",SessionData.getLanguage())+"</font>");
                        }
                        else
                        {
                          out.println("<font color=ff0000>"+Languages.getString("jsp.index.error_input",SessionData.getLanguage())+"</font>");
                          if (hashErrors.containsKey("email"))
                          {
                          out.println("<br><font color=ff0000>"+Languages.getString("jsp.admin.contact.index.error2",SessionData.getLanguage())+"</font>");
                          }
                        }
                      }
                  %>
                  <form method="post" action="admin/contact/index.jsp">
                 <table class=main>
                 <tr>
                 <%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
                  <td class="formAreaTitle2"><%=Languages.getString("jsp.admin.contact.index.to",SessionData.getLanguage())%>:</td><td><%=strBrandedCompanyName+ " " + Languages.getString("jsp.admin.contact.index.support",SessionData.getLanguage())%></td>
				<% } else{%> 
				<td class="formAreaTitle2"><%=Languages.getString("jsp.admin.contact.index.to",SessionData.getLanguage())%>:</td><td><%=Languages.getString("jsp.admin.contact.index.support",SessionData.getLanguage())+ " " + strBrandedCompanyName %></td>
				<% }%>
                 </tr>
<%	  if (!customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
	  {
 %>                 
                 <tr>
                  <td class="formAreaTitle2"><%=Languages.getString("jsp.admin.contact.index.department",SessionData.getLanguage())%>:</td>
                  <td><select name="department">
                  <option value="1">Customer Service</option>
                  <option value="2">Merchant Setups</option>
                  <option value="3">Pin Credits</option>
                  <option value="4">Accounting</option>
                  <option value="5">System Issues(IT)</option>
                  <option value="6">General Support</option>
                  </select>
                  </td>
                 </tr>
<%} %>                 
                 <tr>
                  <td class="formAreaTitle2"><%=Languages.getString("jsp.admin.contact.index.preferred_contact_method",SessionData.getLanguage())%>:</td>
                  <td><input type=radio name=contactType value="phone" <%if (strContactType != null && (strContactType.equals("phone"))) out.println("checked");%>><%=Languages.getString("jsp.admin.contact.index.phone",SessionData.getLanguage())%>&nbsp;&nbsp;&nbsp;<input type=radio name=contactType value="email" <%if (strContactType == null || strContactType.equals("email")) out.println("checked"); %>><%=Languages.getString("jsp.admin.contact.index.email",SessionData.getLanguage())%><%if (hashErrors != null && hashErrors.containsKey("contactType")) out.print("<font color=ff0000>*</font>");%></td>
                 </tr>
                 <tr>
                 <td class="formAreaTitle2"><%=Languages.getString("jsp.admin.contact.index.email_or_phone",SessionData.getLanguage())%>:</td>
                 <td><input name="contact" type="text" size=50 value="<%=HTMLEncoder.encode(StringUtil.toString(strContact))%>"><%if (hashErrors != null && hashErrors.containsKey("contact")) out.print("<font color=ff0000>*</font>");%></td>
                 </tr>
                 </tr>
                 <tr>
                 <td class="formAreaTitle2"><%=Languages.getString("jsp.admin.contact.index.subject",SessionData.getLanguage())%>:</td>
                 <td><input name="subject" type="text" size=50 value="<%=HTMLEncoder.encode(StringUtil.toString(strSubject))%>"><%if (hashErrors != null && hashErrors.containsKey("subject")) out.print("<font color=ff0000>*</font>");%></td>
                 </tr>
                 <tr>
                 <td colspan=2 class="formAreaTitle2"><%=Languages.getString("jsp.admin.contact.index.message",SessionData.getLanguage())%>:</td>
                 </tr>
                 <tr>
                 <td colspan=2 valign=top>
                 <textarea name="message" rows="20" cols="50"><%=HTMLEncoder.encode(StringUtil.toString(strMessage))%></textarea><%if (hashErrors != null && hashErrors.containsKey("message")) out.print("<font color=ff0000>*</font>");%>
                 </td>
                 </tr>
                 <tr>
                 <td colspan=2>
                 <input type=submit name=submit value="<%=Languages.getString("jsp.admin.contact.index.submit",SessionData.getLanguage())%>">
                 </td>
                 </tr>
                 </table>
                 </form>
                 <%}%>
     				    </td>
     		        <td width="18">&nbsp;</td>
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>


<%@ include file="/includes/footer.jsp" %>





