<p>&nbsp;</p>
<p>&nbsp;<strong><br /></strong></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<table style="width: 1100px; margin-left: auto; margin-right: auto;" cellspacing="30">
<tbody>
<tr>
<td style="width: 594.117px;">
<p style="text-align: justify;"><strong>AL DAR &ldquo;CLICK&rdquo; EN EL BOT&Oacute;N &ldquo;ACEPTAR&rdquo;, USTED (EN LO SUCESIVO EL (&ldquo;DISTRIBUIDOR&rdquo;) ACEPTA INCONDICIONALMENTE QUE QUEDA OBLIGADO Y SE CONVIERTE EN UNA DE LAS PARTES DE ESTE CONTRATO DE PRESTACI&Oacute;N DE SERVICIOS DE DISTRIBUCI&Oacute;N EN VENTA DE TIEMPO AIRE (EN LO SUCESIVO EL &ldquo;CONTRATO&rdquo;) CON RECARGA DE PRODUCTOS Y SERVICIOS, S. DE R.L. DE C.V. (EN LO SUCESIVO &ldquo;MOBILMEX&rdquo;) POR LO QUE A PARTIR DE EL MOMENTO DE ACEPTACI&Oacute;N QUEDAR&Aacute; ABSOLUTAMENTE OBLIGADO A LA TOTALIDAD DE LOS T&Eacute;RMINOS Y CONDICIONES ESTIPULADOS A CONTINUACI&Oacute;N: </strong></p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><strong>Declara MOBILMEX a trav&eacute;s de su representante:</strong></p>
<p style="text-align: justify;">&nbsp;</p>
<ol>
<li style="text-align: justify;">Que es una sociedad mercantil, constituida de conformidad con las leyes mexicanas, seg&uacute;n consta en la escritura p&uacute;blica n&uacute;mero 29 de fecha 10 de junio de 2009, otorgada ante la fe del Licenciado Ernesto Ramos Cobo, Notario P&uacute;blico n&uacute;mero 38 del Distrito Notarial de Viesca, en la ciudad de Torre&oacute;n, Estado de Coahuila de Zaragoza, cuyo Primer Testimonio qued&oacute; debidamente inscrito en el Registro P&uacute;blico del Comercio del Estado de Coahuila, bajo el n&uacute;mero de folio electr&oacute;nico 10,015*2 con fecha de 8 de Julio del 2009.</li>
<li style="text-align: justify;">Que cuenta con las facultades suficientes para obligar a su representada seg&uacute;n consta en la Escritura P&uacute;blica n&uacute;mero 67, de fecha 29 de agosto de 2014; otorgada ante la fe del Licenciado Ernesto Ramos Cobo, Notario P&uacute;blico n&uacute;mero 38 del Distrito Notarial de Viesca, en la ciudad de Torre&oacute;n, Estado de Coahuila de Zaragoza, facultades que no le han sido limitadas, modificadas ni revocadas, en forma alguna.</li>
<li style="text-align: justify;">Que su Registro Federal de Contribuyentes es: RPS090610QH7.</li>
<li style="text-align: justify;">Que se&ntilde;ala como su domicilio para todos los efectos que se deriven del presente Convenio el ubicado en Monte Elbruz, n&uacute;mero 132, interior 705, Chapultepec Morales, Miguel Hidalgo, Ciudad de M&eacute;xico.</li>
</ol>
<p style="text-align: justify;">Que su representada es licenciataria de un sistema de c&oacute;mputo o plataforma tecnol&oacute;gica (en lo sucesivo, el &ldquo;Sistema&rdquo;) perteneciente a un tercero, que se compone de forma enunciativa mas no limitativa de: (i) la plataforma tecnol&oacute;gica, (ii) el software, (iii) la imagen, (iv) los logotipos, (v) la marca, (vi) los procesos, (vii) los procedimientos, y (viii) los formatos que en su conjunto permiten a Mobilmex procesar, recibir y transmitir datos, as&iacute; como de una infraestructura de red con capacidad necesaria para</p>
</td>
<td style="width: 538.883px;">
<ol>
<li style="text-align: justify;">transportar y procesar transacciones electr&oacute;nicas de diversos productos y/o servicios prepagados. y.</li>
</ol>
<p style="text-align: justify;"><strong>Declara el &ldquo;Distribuidor&rdquo;, por su propio derecho </strong></p>
<ol style="text-align: justify;">
<li>Ser una persona f&iacute;sica mayor de edad, en pleno goce de sus derechos y en consecuencia capaz de obligarse en t&eacute;rminos del Contrato.</li>
<li>Que se encuentra debidamente inscrita en el Registro Federal de Contribuyentes (R.F.C.).</li>
<li>Que cuenta con los medios necesarios para llevar a cabo la promoci&oacute;n y difusi&oacute;n de los distintos productos y servicios de prepago que ofrece &ldquo;MOBILMEX&rdquo;.</li>
<li>Que a la fecha de la firma del presente no tiene relaci&oacute;n comercial o contractual alguna, que signifique un impedimento para celebrar el presente Contrato, incluyendo sin limitaci&oacute;n alguna, relaci&oacute;n comercial o contractual de exclusividad con alguna de las compa&ntilde;&iacute;as proveedoras de servicios de telefon&iacute;a local, larga distancia y celular que operan actualmente en M&eacute;xico,</li>
<li>Es su deseo trabajar en conjunto con &ldquo;MOBILMEX&rdquo; para comunicar cualquier situaci&oacute;n que as&iacute; se requiera, y para cualquier otra situaci&oacute;n, siempre con miras a crecer la red y hacer m&aacute;s s&oacute;lida la operaci&oacute;n e integraci&oacute;n entre ambas partes,</li>
<li>Todos los movimientos efectuados en el sitio de reportes por medio de una clave confidencial ser&aacute;n responsabilidad &uacute;nica y exclusiva del &ldquo;Distribuidor&rdquo;, quien se obliga por este medio a salvaguardar la confidencialidad de dichas claves de acceso,</li>
</ol>
<p style="text-align: justify;">Expuesto lo anterior, las Partes est&aacute;n de acuerdo en sujetar el presente Contrato a las siguientes:</p>
<p style="text-align: justify;"><strong><u>PRIMERA.- </u></strong>Por medio del presente, el &ldquo;Distribuidor&rdquo; se obliga con &ldquo;MOBILMEX&rdquo; a prestar servicios de distribuci&oacute;n de los Productos y Servicios que &ldquo;MOBILMEX&rdquo; pone a su disposici&oacute;n a trav&eacute;s del Sistema y de la Red &ldquo;MOBILMEX&rdquo;.</p>
<p style="text-align: justify;"><strong><u>SEGUNDA.- VIGENCIA.</u></strong>&nbsp;El contrato tendr&aacute; una vigencia indefinida desde el momento en que el &ldquo;Distribuidor&rdquo; acepte los t&eacute;rminos y condiciones se&ntilde;alados en el cuerpo del contrato, a trav&eacute;s de los medios electr&oacute;nicos que &ldquo;MOBILMEX&rdquo; pone a su disposici&oacute;n y hasta que notifique al siguiente correo electr&oacute;nico <a href="mailto:legal@mobilmex.com"><u>legal@mobilmex.com</u></a>&nbsp;con al menos 30 (treinta) d&iacute;as naturales de anticipaci&oacute;n a la fecha programada para la terminaci&oacute;n, su deseo de dar por terminado el presente.</p>
<p style="text-align: justify;"><strong><u>TERCERA.- OBLIGACIONES DE LAS PARTES. </u></strong>De conformidad con el presente instrumento, las Partes se obligan a lo siguiente:</p>
</td>
</tr>
</tbody>
</table>
<hr style="color: black !important; border: 2px;" /><hr style="border: 1 !important; height: 1px; background-color: #dadada;" />
<p style="text-align: center;"><!-- pagebreak --></p>
<table style="width: 1200px; margin-left: auto; margin-right: auto;" cellspacing="30">
<tbody>
<tr>
<td style="width: 589.617px;">&nbsp;
<p style="text-align: justify;"><strong>3.1.- &ldquo;MOBILMEX&rdquo; se obliga a:</strong></p>
<ol style="text-align: justify;">
<li>Coadyuvar&aacute; con el &ldquo;Distribuidor&rdquo;, a fin de que pueda operar de manera regular a trav&eacute;s de un equipo de sus propios equipos;</li>
<li>Garantizar la operatividad del Sistema;</li>
<li>Proporcionar a el &ldquo;Distribuidor&rdquo;, por medio del sitio de soporte, reportes que le permitan llevar un control adecuado de la operaci&oacute;n del Sistema;</li>
<li>Otorgar a el &ldquo;Distribuidor&rdquo;, las comisiones previamente acordados y configurados por &ldquo;MOBILMEX&rdquo; en el Sistema;</li>
<li>Proporcionar al &ldquo;Distribuidor&rdquo;, materiales de promoci&oacute;n y se&ntilde;alizaci&oacute;n que identificar&aacute;n a los establecimientos pertenecientes al &ldquo;Distribuidor&rdquo; como miembros de la Red &ldquo;MOBILMEX&rdquo;, los materiales de promoci&oacute;n y se&ntilde;alizaci&oacute;n deber&aacute;n cumplir en todo momento las indicaciones que al efecto realice &ldquo;MOBILMEX&rdquo;. Todos los gastos de env&iacute;o que de materiales de promoci&oacute;n y se&ntilde;alizaci&oacute;n correr&aacute;n a cargo &uacute;nica y exclusivamente del &ldquo;Distribuidor&rdquo;, por lo que desde este momento, acepta que &ldquo;MOBILMEX&rdquo;, podr&aacute; hacer cualquier compensaci&oacute;n sobre montos, descuentos y/o cualquier cantidad que tenga a favor del &ldquo;Distribuidor&rdquo;, asimismo, el &ldquo;Distribuidor&rdquo; renuncia desde este momento a cualquier acci&oacute;n legal en contra de &ldquo;MOBILMEX&rdquo;, filiales, sus directivos, funcionarios y/o cualquier persona subordinada por cualquier descuento que se llegare a general en los t&eacute;rminos del presente inciso; y</li>
<li>Dar capacitaci&oacute;n v&iacute;a remota, bajo cualquier mecanismo de conexi&oacute;n que estime pertinente &ldquo;MOBILMEX&rdquo;, &nbsp;al personal del &ldquo;Distribuidor&rdquo; &uacute;nica y exclusivamente respecto del Sistema y de la Red &ldquo;MOBILMEX&rdquo;.</li>
<li>Emitir la factura al Distribuidor, &uacute;nica y exclusivamente en el supuesto que este la solicite en los t&eacute;rminos de la Cl&aacute;usula correspondiente.</li>
</ol>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><strong>3.2.- &nbsp;El &ldquo;Distribuidor&rdquo; se obliga a:</strong></p>
<p style="text-align: justify;">Ofrecer y realizar la totalidad de las operaciones de distribuci&oacute;n en venta de los Productos y Servicios que &ldquo;MOBILMEX&rdquo; ponga a su disposici&oacute;n a trav&eacute;s del Sistema y de la Red &ldquo;MOBILMEX&rdquo;&nbsp;y que el p&uacute;blico en general le solicite, de conformidad con los procedimientos se&ntilde;alados en el presente Contrato;</p>
<p style="text-align: justify;">Hacer uso del Sistema y de la Red &ldquo;MOBILMEX&rdquo;&nbsp;&uacute;nica y exclusivamente para llevar a cabo las operaciones de distribuci&oacute;n en venta de los Productos y Servicios de &ldquo;MOBILMEX&rdquo;, siguiendo las indicaciones contenidas en la capacitaci&oacute;n que al efecto &ldquo;MOBILMEX&rdquo; le proporcione;</p>
<p style="text-align: justify;">Supervisar que su punto de venta cobre a los usuarios finales &uacute;nicamente las cantidades autorizadas por &ldquo;MOBILMEX&rdquo;<strong><em>&nbsp;</em></strong>por las operaciones de venta de los Productos y Servicios objeto del presente Contrato, mismas que en ning&uacute;n momento deber&aacute;n ser modificadas y/o alteradas por el Distribuidor;</p>
</td>
<td style="width: 533.383px;">&nbsp;
<p style="text-align: justify;">El &ldquo;Distribuidor&rdquo; deber&aacute; mantener constante vigilancia sobre las personas que operen el Sistema (en los sucesivo, los &ldquo;<u>Operadores</u>&rdquo;), a fin de evitar cualquier tipo de conducta fraudulenta de los mismos. el &ldquo;Distribuidor&rdquo; responder&aacute; directamente y deber&aacute; cubrir a &ldquo;MOBILMEX&rdquo; los da&ntilde;os y perjuicios que se generen con motivo de los actos realizados por los Operadores.</p>
<p style="text-align: justify;">Asegurarse que cuenten con los elementos necesarios para realizar las transacciones electr&oacute;nicas, tales como conectividad a Internet, licencias de software originales, equipos de c&oacute;mputo en optimas condiciones, impresoras, etc. El Distribuidor deber&aacute; cubrir cualquier gasto que se genere a efecto de cumplir con la obligaci&oacute;n a que se refiere la presente Cl&aacute;usula, tal como los gastos por instalaciones, cableado, adaptaciones, entre otros;</p>
<p style="text-align: justify;">Asegurarse que los Operadores,&nbsp;&nbsp;as&iacute; como aquellas personas que eventualmente se agreguen o las sustituyan, tomen la capacitaci&oacute;n para el correcto funcionamiento y operaci&oacute;n&nbsp;del Sistema, a fin de que comprendan los lineamentos de operaci&oacute;n&nbsp;del Sistema y la Red &ldquo;MOBILMEX&rdquo; y cumplan con las indicaciones de la capacitaci&oacute;n;</p>
<p style="text-align: justify;">Notificar a &ldquo;MOBILMEX&rdquo; sobre cualquier anomal&iacute;a que pudiere surgir al momento de realizar las operaciones de venta de los Productos y Servicios, Deber&aacute;n seguir las indicaciones que &ldquo;MOBILMEX&rdquo; le indique respecto los materiales de promoci&oacute;n y se&ntilde;alizaci&oacute;n que identificar&aacute;n al Distribuidor perteneciente a la Red &ldquo;MOBILMEX&rdquo;;</p>
<p style="text-align: justify;">El &ldquo;Distribuidor&rdquo; deber&aacute; instalar los materiales de promoci&oacute;n y se&ntilde;alizaciones en los puntos de venta que identificar&aacute;n al Distribuidor afiliado perteneciente a la Red &ldquo;MOBILMEX&rdquo; de acuerdo a los lineamientos indicados por &ldquo;MOBILMEX&rdquo;;</p>
<p style="text-align: justify;">Solicitar la autorizaci&oacute;n por escrito de &ldquo;MOBILMEX&rdquo; para modificar y/o alterar los materiales de promoci&oacute;n y se&ntilde;alizaciones en los puntos de venta que identificar&aacute;n al Distribuidor afiliados pertenecientes a la Red &ldquo;MOBILMEX&rdquo;;</p>
<p style="text-align: justify;">Supervisar que el &nbsp;&ldquo;Distribuidor&rdquo; efect&uacute;e los dep&oacute;sitos pre-pagados suficientes para cubrir la operaci&oacute;n,</p>
<p style="text-align: justify;">Supervisar que el &ldquo;Distribuidor&rdquo; realicen por lo menos un corte de caja o reporte de fin del d&iacute;a&nbsp;por cada periodo&nbsp;de 24 horas con el objetivo de reportar cualquier diferencia existente en los montos. el &ldquo;Distribuidor&rdquo; tendr&aacute;&nbsp;un periodo de 72 (setenta y dos) horas para reportar cualquier diferencia de conciliaci&oacute;n a &ldquo;MOBILMEX&rdquo; y no tendr&aacute; la obligaci&oacute;n de aceptar reportes con diferencias en los montos no reportados en per&iacute;odos de tiempo mayores a 24 horas,</p>
<p style="text-align: justify;">Cualquier incumplimiento de las obligaciones se&ntilde;aladas en los incisos anteriores por parte del &ldquo;Distribuidor&rdquo; ser&aacute; causa de rescici&oacute;n&nbsp;inmediata del presente Contrato.</p>
<p style="text-align: justify;">Cuando solicite factura por los prepagos realizados, solicitar a MOBILMEX en los t&eacute;rminos</p>
</td>
</tr>
</tbody>
</table>
<hr style="border: 1 !important; height: 1px; background-color: #dadada;" />
<p style="text-align: center;"><!-- pagebreak --></p>
<table style="width: 1200px; margin-left: auto; margin-right: auto;" cellspacing="30">
<tbody>
<tr>
<td style="width: 591.133px;">&nbsp;
<p style="text-align: justify;">vez que el Sistema realiza el c&aacute;lculo se&ntilde;alado anteriormente, se efect&uacute;a el abono de la comisi&oacute;n que &ldquo;MOBILMEX&rdquo; deba dar al &ldquo;Distribuidor&rdquo; directamente aplic&aacute;ndola &nbsp;al saldo de su Bolsa Prepagada.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Las partes convienen que &ldquo;MOBILMEX&rdquo; podr&aacute; en todo momento ajustar el porcentaje de comisi&oacute;n que le corresponden al &ldquo;Distribuidor&rdquo; en caso de que la comisi&oacute;n que &ldquo;MOBILMEX&rdquo; abona &nbsp;por cada uno de los productos y servicios a ser comercializados, sea disminuida por parte de quien le provee dichos productos y servicios. &nbsp;Dichos ajustes en las comisiones por cada uno de los productos y servicios a ser comercializados, ser&aacute;n notificados por &ldquo;MOBILMEX&rdquo; al &ldquo;Distribuidor&rdquo; &nbsp;&uacute;nica y exclusivamente v&iacute;a correo electr&oacute;nico, a la direcci&oacute;n de correo que previamente el &ldquo;Distribuidor&rdquo; &nbsp;haya proporcionado a MOBILMEX, mediante correo electr&oacute;nico En un plazo de &nbsp;24 (veinticuatro) a 48 (cuarenta y ocho) horas en d&iacute;as h&aacute;biles de anticipaci&oacute;n. A partir de dicha notificaci&oacute;n, cualquier operaci&oacute;n de los productos y servicios por parte del &ldquo;Distribuidor&rdquo;, ser&aacute; considerada como un consentimiento t&aacute;cito de &eacute;ste y de su aceptaci&oacute;n a los nuevos porcentajes de las comisiones. El &ldquo;Distribuidor&rdquo; es responsable de verificar a trav&eacute;s del sitio de soporte que los planes de comisiones se encuentren correctamente configurados, y que el Distribuidor reciba la comisi&oacute;n correcta. En caso de discrepancia o dudas, el &ldquo;Distribuidor&rdquo; est&aacute; obligado a reportarlo a &ldquo;MOBILMEX&rdquo; de inmediato a trav&eacute;s de <em>&ldquo;Costumer Service</em>&rdquo;, para que sea canalizado al ejecutivo de venta correspondiente. &ldquo;MOBILMEX&rdquo; no har&aacute; reembolsos o reversiones si el &ldquo;Distribuidor&rdquo; &nbsp;no notifica en tiempo y forma la discrepancia, dentro de un plazo de 24 (veinte cuatro) hasta 72 (setenta y dos) horas siguientes al cambio de comisiones reportado.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><strong><u>SEXTA.- FACTURACI&Oacute;N.</u></strong>&nbsp;S&oacute;lo en el supuesto que as&iacute; lo solicite el &ldquo;Distribuidor&rdquo;, &ldquo;MOBILMEX&rdquo; entregar&aacute; la factura correspondiente a los dep&oacute;sitos realizados por concepto de prepago en un plazo no menor a las 24 horas h&aacute;biles siguientes a la fecha de solicitud, para lo cual el &ldquo;Distribuidor&rdquo; deber&aacute; solicitar v&iacute;a correo electr&oacute;nico la emisi&oacute;n de la factura correspondiente.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">Las facturas que emita &ldquo;MOBILMEX&rdquo; solo podr&aacute;n ser emitidas por los dep&oacute;sitos debidamente reportados y registrados por el &ldquo;Distribuidor&rdquo; en t&eacute;rminos del presente Contrato.</p>
<p style="text-align: justify;"><strong><u>&nbsp;</u></strong></p>
<p style="text-align: justify;"><strong><u>S&Eacute;PTIMA.- ADICI&Oacute;N DE PRODUCTOS Y SERVICIOS.</u></strong><strong>&nbsp;</strong>Al disponer de nuevos Productos y Servicios, &ldquo;MOBILMEX&rdquo; le har&aacute; llegar al &ldquo;Distribuidor&rdquo; &nbsp;&uacute;nica y exclusivamente v&iacute;a correo electr&oacute;nico, a la direcci&oacute;n de correo que previamente el &ldquo;Distribuidor&rdquo; &nbsp;haya proporcionado a MOBILMEX, &nbsp;por medio del Sistema y sus dispositivos o a trav&eacute;s del portal de Internet de la Red &ldquo;MOBILMEX&rdquo;, la informaci&oacute;n y condiciones relativas a los mismos, para la evaluaci&oacute;n de su inclusi&oacute;n en el portafolio de productos a comercializar por el &ldquo;Distribuidor&rdquo;.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">En caso de que el &ldquo;Distribuidor&rdquo; no acepte dichos Productos o Servicios, deber&aacute; notificar en un plazo no mayor a 48 (cuarenta y ocho) horas, &nbsp;su negativa por escrito a &ldquo;MOBILMEX&rdquo; a fin de que &eacute;ste inhabilite la capacidad del &ldquo;Distribuidor&rdquo; para comercializarlos.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><strong><u>OCTAVA.- CREDENCIALES.</u></strong><strong>&nbsp;</strong>Las Partes contratantes en el presente instrumento, se&ntilde;alan que para el debido</p>
</td>
<td style="width: 535.867px;">&nbsp;
<p style="text-align: justify;">cumplimiento del objeto del presente, es necesario contar con una serie de contrase&ntilde;as, las cuales se formaran por una serie de caracteres num&eacute;ricos irrepetible e individual. Contrase&ntilde;as que ser&aacute;n las siguientes:</p>
<ol style="text-align: justify;">
<li><u>C&oacute;digo de Cajero</u>.- El o los Operadores del sistema autorizados por el &ldquo;Distribuidor&rdquo; recibir&aacute;n de parte de &ldquo;MOBILMEX&rdquo; &nbsp;una contrase&ntilde;a mediante la cual podr&aacute;n hacer transacciones en el Sistema.</li>
<li><u>Nombre de Usuario y Contrase&ntilde;a para al Acceso al Sistema</u>.- En caso de que el &ldquo;Distribuidor&rdquo; solicite acceso a la interface gr&aacute;fica del Sistema y la Red &ldquo;MOBILMEX&rdquo;, &ldquo;MOBILMEX&rdquo; le otorgar&aacute; un Nombre de Usuario y una contrase&ntilde;a al efecto, bajo el entendido de que el &ldquo;Distribuidor&rdquo; ser&aacute; responsable de todos los movimientos que se efect&uacute;en en sus credenciales.</li>
<li>El &ldquo;Distribuidor&rdquo; ser&aacute; responsable del uso que los Operadores le den a las credenciales de acceso proporcionadas, &nbsp;aun cuando el manejo de dichas credenciales haya sido realizado sin el consentimiento del &ldquo;Distribuidor&rdquo; o por personas ajenas a las mismas. El &ldquo;Distribuidor&rdquo; deber&aacute; tener disponible en todo momento al menos un Operador para realizar las operaciones de distribuci&oacute;n en venta de los Productos y Servicios de &ldquo;MOBILMEX&rdquo; durante el horario de servicio en el punto de venta.</li>
</ol>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><strong><u>NOVENA.- DISPOSITIVO DE CONECTIVIDAD.</u></strong><strong>&nbsp;</strong></p>
<ol style="text-align: justify;">
<li>&ldquo;MOBILMEX&rdquo; permitir&aacute; el acceso al &ldquo;Distribuidor&rdquo; v&iacute;a Internet al Sistema y la red &ldquo;MOBILMEX&rdquo; para que desde dispositivo compatible con la plataforma de &ldquo;MOBILMEX&rdquo; &nbsp;propiedad de El &ldquo;Distribuidor&rdquo; pueda realizar la venta y prestaci&oacute;n de .los Productos y Servicios propiedad de &ldquo;MOBILMEX&rdquo;. &ldquo;MOBILMEX&rdquo; proporcionar&aacute; a El &ldquo;Distribuidor&rdquo; (i) una direcci&oacute;n URL, (ii) instrucciones para acceder al Sistema de transacciones, y (iii) credenciales &nbsp;de usuario intransferibles para la realizaci&oacute;n de las transacciones.</li>
<li>El equipo o PC, y todo lo relativo a &eacute;ste, as&iacute; como dispositivos de impresi&oacute;n ser&aacute;n responsabilidad &uacute;nica y exclusiva de El &ldquo;Distribuidor&rdquo; &ldquo;MOBILMEX&rdquo; no tendr&aacute; la obligaci&oacute;n de dar atenci&oacute;n t&eacute;cnica, asesor&iacute;a, reparaci&oacute;n, programar el equipo, ni realizar modificaci&oacute;n alguna a su configuraci&oacute;n. El &ldquo;Distribuidor&rdquo; deber&aacute; contar en todo momento con equipos de impresi&oacute;n para los comprobantes emitidos por el Sistema.</li>
</ol>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA.- RESCISI&Oacute;N Y PENA CONVENCIONAL.</u></strong><strong>&nbsp;</strong>En caso de incumplimiento de cualquiera de las obligaciones se&ntilde;aladas en el Presente Contrato por el &ldquo;Distribuidor&rdquo; en t&eacute;rminos del art&iacute;culo 1949 del C&oacute;digo Civil vigente para el Distrito Federal, podr&aacute; optar entre la recisi&oacute;n del Contrato sin necesidad de declaraci&oacute;n judicial o bien el cumplimiento forzoso del mismo.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;">En cualquiera de los dos supuestos se&ntilde;alados en el p&aacute;rrafo anterior el &ldquo;Distribuidor&rdquo; deber&aacute; pagar los da&ntilde;os y perjuicios, as&iacute; como cualquier cantidad adicional que genere su incumplimiento y/o el &nbsp;perjuicio de &ldquo;MOBILMEX&rdquo; Y/o sus filiales.</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA PRIMERA.- &nbsp;PROPIEDAD INDUSTRIAL. &nbsp;</u></strong>El &ldquo;Distribuidor&rdquo; reconoce que la propiedad industrial del Sistema, incluyendo el software, su desarrollo industrial, c&oacute;digo fuente, interfaces de programaci&oacute;n, manuales de instalaci&oacute;n, gu&iacute;as de operaci&oacute;n, y cualquier documento</p>
</td>
</tr>
</tbody>
</table>
<hr style="border: 1 !important; height: 1px; background-color: #dadada;" />
<p style="text-align: center;"><!-- pagebreak --></p>
<table style="width: 1200px; margin-left: auto; margin-right: auto;" cellspacing="30">
<tbody>
<tr>
<td style="width: 591.7px;">
<p style="text-align: justify;">generado para la operaci&oacute;n del Sistema, incluyendo sin limitaci&oacute;n alguna la marca comercial, nombre y logotipo de &ldquo;MOBILMEX&rdquo;, adem&aacute;s de cualquier otro derecho de propiedad de la Red &ldquo;MOBILMEX&rdquo; ., son propiedad exclusiva de un tercero, mismas que han sido licenciadas para uso de &ldquo;MOBILMEX&rdquo;. Adicionalmente, la Red &ldquo;MOBILMEX&rdquo; es habilitada para el uso del &ldquo;Distribuidor&rdquo; a modo de licencia, de forma no exclusiva, y podr&aacute; ser revocada en cualquier momento por &ldquo;MOBILMEX&rdquo;. A la terminaci&oacute;n del presente Contrato, el &ldquo;Distribuidor&rdquo; se obliga a eliminar cualquier software y devolver a &ldquo;MOBILMEX&rdquo; todos los materiales proporcionados en comodato y toda la documentaci&oacute;n recibida y generada como resultado de este Contrato. El presente instrumento no otorga derecho alguno a ninguna de las Partes sobre la propiedad industrial de la otra o de alg&uacute;n tercero y no debe interpretarse como una licencia sobre el uso de la misma.</p>
<p style="text-align: justify;">El &ldquo;Distribuidor&rdquo; se obliga a sacar en paz a &ldquo;MOBILMEX&rdquo; y/o a cualquiera de sus filiales por cualquier litigio o reclamaci&oacute;n que le hiciere cualquier tercero por el indebido uso del Sistema que el &ldquo;Distribuidor&rdquo; y/o sus empleados realicen &nbsp;durante la vigencia del presente Contrato y hasta por los 5 (cinco) a&ntilde;os posteriores a la vigencia del presente.</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA SEGUNDA.- MARCAS.</u></strong><strong>&nbsp;&nbsp;</strong>El &ldquo;Distribuidor&rdquo; se obliga a respetar las marcas registradas y nombres comerciales propiedad y licenciadas a &ldquo;MOBILMEX&rdquo;, as&iacute; como las marcas y/o nombres comerciales propiedad de terceros que se relacionen con los Productos y Servicios objeto del presente Contrato. El &ldquo;Distribuidor&rdquo; no podr&aacute; hacer uso de estas marcas registradas y nombres comerciales con ning&uacute;n otro fin diferente a promocionar los Productos y Servicios en el material publicitario proporcionado por &ldquo;MOBILMEX&rdquo;. El &ldquo;Distribuidor&rdquo; se obliga a no solicitar el registro de ninguna marca y/o nombre comercial relacionado con los Productos y Servicios que son objeto del presente Contrato y que son propiedad de &ldquo;MOBILMEX&rdquo; o de terceros.</p>
<p style="text-align: justify;">El &ldquo;Distribuidor&rdquo; se obliga a sacar en paz a &ldquo;MOBILMEX&rdquo; y/o a cualquiera de sus filiales por cualquier litigio o reclamaci&oacute;n que le hiciere cualquier tercero por el indebido uso de la marca que el &ldquo;Distribuidor&rdquo; y/o sus &nbsp;empleados realicen durante la vigencia del presente Contrato y hasta por los 5 (cinco) a&ntilde;os posteriores a la vigencia del presente.</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA TERCERA.- NO EXISTENCIA DE RELACI&Oacute;N LABORAL.</u></strong><strong>&nbsp;</strong>&nbsp;Las partes expresamente manifiestan que no existe relaci&oacute;n laboral o de subordinaci&oacute;n alguna entre ellas, sus empleados, funcionarios, t&eacute;cnicos y dem&aacute;s trabajadores que les presten servicios, por lo que desde este momento, se comprometen a deslindarse rec&iacute;procamente, de cualquier responsabilidad presente o futura, ya sea ante tribunales o autoridades judiciales, y sacarse en paz y a salvo de cualesquier demanda, reclamaci&oacute;n o queja que pudiera surgir con motivo de lo anterior, comprometi&eacute;ndose la parte responsable, a pagarle a la otra, los honorarios, gastos y costas que tenga que erogar con motivo de lo anterior.</p>
<p style="text-align: justify;">Asimismo, el &ldquo;Distribuidor&rdquo; deber&aacute; ser el &uacute;nico responsable por cualquier reclamaci&oacute;n que cualquier tercero entable en perjuicio de &ldquo;MOBILMEX&rdquo; y/o sus filiales.</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA CUARTA.- CONFIDENCIALIDAD.</u></strong><strong>&nbsp;</strong>Ambas partes reconocen que la informaci&oacute;n referente a: (i) el Sistema y la Red &ldquo;MOBILMEX&rdquo;, (ii) la plataforma, su operaci&oacute;n y las</p>
</td>
<td style="width: 536.3px;">
<p style="text-align: justify;">transacciones, (iii) la informaci&oacute;n obtenida con base en este Contrato, (iv) la informaci&oacute;n relativa a los clientes de &ldquo;MOBILMEX&rdquo;, (v) los informes y reportes basados en Internet o provistos por el dispositivo proporcionado por &ldquo;MOBILMEX&rdquo;, as&iacute; como (vi) la documentaci&oacute;n que las Partes se proporcionen al amparo del presente Contrato, los datos y resultados obtenidos, tendr&aacute;n el car&aacute;cter de confidencial, por lo que las Partes se obligan a guardar y mantener en absoluta confidencialidad toda la informaci&oacute;n, tangible e intangible, que llegaran a obtener con base en lo anterior, as&iacute; como los resultados y/o productos derivados de la misma. El &ldquo;Distribuidor&rdquo; reconoce la importancia de la informaci&oacute;n confidencial de &ldquo;MOBILMEX&rdquo; por lo que se obliga a no divulgar la informaci&oacute;n confidencial a terceros, y a no celebrar contrato alguno con terceros que tengan o pudieren tener acceso a informaci&oacute;n confidencial de &ldquo;MOBILMEX&rdquo;.</p>
<p style="text-align: justify;">En virtud de lo anterior, deber&aacute;n mantener para s&iacute; y su personal, el secreto a que est&aacute;n obligadas en el desarrollo de sus actividades, comprometi&eacute;ndose a utilizar la informaci&oacute;n confidencial &uacute;nicamente para la realizaci&oacute;n y cumplimiento del presente Contrato, qued&aacute;ndoles estrictamente prohibido, divulgarla por cualquier medio a terceros distintos a los indispensables para el cumplimiento de los fines del presente instrumento o darle cualquier uso diverso al establecido en este Contrato, ni siquiera a nivel curricular, salvo autorizaci&oacute;n previa y por escrito de la otra Parte.</p>
<p style="text-align: justify;">La obligaci&oacute;n de confidencialidad a que se refiere esta Cl&aacute;usula, permanecer&aacute; vigente con toda su fuerza y vigor a&uacute;n despu&eacute;s de terminada la vigencia del presente Contrato hasta por un periodo de 2 (dos) a&ntilde;os posteriores a la terminaci&oacute;n del presente instrumento, siempre y cuando la informaci&oacute;n confidencial no pase a ser del dominio p&uacute;blico.</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA QUINTA.- AVISO DE PRIVACIDAD. </u></strong>El documento que describe el tratamiento de los datos personales del &ldquo;Distribuidor&rdquo; en posesi&oacute;n de MOBILMEX y el procedimiento para acceder, rectificar, cancelar y oponerse al tratamiento de los mismos y que se ubica en el Sitio Web de MOBILMEX <a href="https://soporte.mobilmex.com:9448/support/login.jsp"><u>https://soporte.mobilmex.com:9448/support/login.jsp</u></a></p>
<p style="text-align: justify;"><strong><u>D&Egrave;CIMA SEXTA.- NO COMPETENCIA. </u></strong>El &ldquo;Distribuidor&rdquo; se obliga a no competir directa o indirectamente con &ldquo;MOBILMEX&rdquo; y a no realizar actividades o contratar servicios semejantes a los que conforman el objeto del presente Contrato. Esta obligaci&oacute;n subsiste durante la vigencia del presente Contrato y hasta transcurridos 12 meses de su terminaci&oacute;n.</p>
<p style="text-align: justify;">En caso de que el &ldquo;Distribuidor&rdquo; incumpla con la obligaci&oacute;n contra&iacute;da en la presente Cl&aacute;usula, deber&aacute; pagar las penas convencionales se&ntilde;aladas en la Cl&aacute;usula Novena de este Instrumento.</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA S&Eacute;PTIMA.- CESI&Oacute;N. </u></strong>El &ldquo;Distribuidor&rdquo; no podr&aacute; ceder, enajenar, gravar o transferir los derechos u obligaciones derivadas del presente Contrato.</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA OCTAVA.- MODIFICACIONES. &nbsp;</u></strong>El &ldquo;Distribuidor&rdquo; no podr&aacute; modificar parcial o totalmente el presente Contrato y sus Anexos en cualquier momento. Cualquier modificaci&oacute;n deber&aacute; ser previamente revisada, acordada y firmada por ambas partes. Cualquier convenio Modificatorio y/o</p>
</td>
</tr>
</tbody>
</table>
<hr style="border: 1 !important; height: 1px; background-color: #dadada;" />
<p style="text-align: center;"><!-- pagebreak --></p>
<table style="width: 1200px; margin-left: auto; margin-right: auto;" cellspacing="30">
<tbody>
<tr>
<td style="width: 592.5px;">
<p style="text-align: justify;">addendum posterior, ser&aacute; parte integral del presente Contrato.</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA NOVENA.- TERRITORIO.</u></strong><strong>&nbsp;</strong>Las PARTES expresamente convienen que el &ldquo;Distribuidor&rdquo; se abstendr&aacute; de promover la venta de los servicios y/o productos objeto del presente Contrato, fuera del Territorio de la Rep&uacute;blica Mexicana, ya sea en forma directa o a trav&eacute;s de terceros, por lo que el incumplimiento de &nbsp;dicha obligaci&oacute;n constituir&aacute; una causa de rescisi&oacute;n del mismo y pago de la pena convencional se&ntilde;alada en el cuerpo del presente instrumento.</p>
<p style="text-align: justify;"><strong><u>D&Eacute;CIMA NOVENA.- NOTIFICACIONES.- </u></strong>Las Partes han se&ntilde;alado como sus domicilios convencionales para cualquier notificaci&oacute;n que deban realizarse en los t&eacute;rminos del presente contrato, lo siguientes:</p>
<p style="text-align: justify;">&ldquo;MOBILMEX&rdquo;.</p>
<p style="text-align: justify;">El &ldquo;Distribuidor&rdquo; se considerar&aacute; como domicilio vigente para todos los efectos del presente contrato el proporcionado a los ejecutivos de MOBILMEX.</p>
<p style="text-align: justify;">Las Partes se obligan de manera rec&iacute;proca a notificar a la otra Parte de manera fehaciente y por escrito cualquier cambio en sus domicilios legales dentro de los siguientes 5 d&iacute;as naturales a que haya tenido efecto.</p>
<p style="text-align: justify;"><u>&nbsp;</u></p>
<p style="text-align: justify;"><strong><u>VIG&Eacute;SIMA PRIMERA.- JURISDICCI&Oacute;N.-</u></strong><strong>&nbsp;</strong>Para la interpretaci&oacute;n y cumplimiento del presente Contrato, las Partes est&aacute;n de acuerdo en someterse expresamente a los Tribunales de la Ciudad de M&eacute;xico, por lo que en este acto renuncian de manera expresa a cualquier fuero que pudiera corresponderles en lo futuro por raz&oacute;n de su domicilio.</p>
<p style="text-align: justify;">&nbsp;</p>
<p><strong><u>LE&Iacute;DO QUE FUE EN TODAS SUS PARTES EL CONTENIDO DEL PRESENTE CONTRATO Y CONOCIENDO LAS PARTES CONTRATANTES EL ALCANCE DE TODAS LAS DECLARACIONES Y CL&Aacute;USULAS, LO SUSCRIBEN POR DUPLICADO EN M&Eacute;XICO.</u></strong></p>
</td>
<td style="width: 537.5px;">&nbsp;</td>
</tr>
</tbody>
</table>
<table style="height: 30px; margin-left: auto; margin-right: auto;" width="1100" cellspacing="30">
<tbody>
<tr>
<td style="width: 676px; text-align: center;">
<p>MOBILMEX</p>
<p>&nbsp;</p>
<p>Representante Legal</p>
</td>
<td style="width: 676px; text-align: center;">
<p>DISTRIBUIDOR</p>
<p>&nbsp;</p>
<p>Representante Legal</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
