<%@ page language="java" import="java.util.*,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.presentation.BrandingComponent" pageEncoding="ISO-8859-1"%>
<%
int section=9;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@include file="/includes/security.jsp" %>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<jsp:useBean id="Banner" class="com.debisys.images.Banner" scope="request"/>
<jsp:setProperty name="Banner" property="*"/>
<%
Vector<Vector<String>> vecBanners = Banner.getBanners();
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="100%" class="formAreaTitle">&nbsp;BANNERS</td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3" class="formArea2">
<%
String message = request.getParameter("message");
if(message != null){
%>
			<p style="color:red"><%=message %></p>
<%
}
%>
			<p style="color:white">
				&lt;&lt; <a href="admin/brandings/branding_options.jsp"><%=Languages.getString("jsp.admin.brandings.tool",SessionData.getLanguage())%></a>
			</p>
			<form action="branding_edit.jsp?action=add" method="post">
				<table class="list-table main">
					<tr>
						<td colspan="2" align="right"><input type="submit" value="Add Branding"></td>
					</tr>
				</table>
			</form>
			<table class="list-table main">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Alternate Text</th>
					<th>Description</th>
					<th>Actions</th>
				</tr>
				<%
					if (vecBanners != null && vecBanners.size() > 0)
						{
							Iterator<Vector<String>> it = vecBanners.iterator();
							int i = 0;
							while (it.hasNext())
							{
								Vector<String> vecTemp = (Vector<String>) it.next();
								%>
									<tr class="row<%=(++i)%>">
										<td><%=vecTemp.get(0)%></td>
										<td><%=HTMLEncoder.encode(vecTemp.get(1))%></td>
										<td><%=vecTemp.get(2)%></td>
										<td><%=HTMLEncoder.encode(vecTemp.get(3))%></td>
										<td align="center">
											<a href="admin/banners/banner_edit.jsp?action=edit&bannerId=<%=vecTemp.get(0)%>">Edit</a> &nbsp;
											<a href="admin/banners/banner_assign.jsp?action=dulpicate&bannerId=<%=vecTemp.get(0)%>">Duplicate</a>
										</td>
									</tr>
								<%
								i %= 2; 
							}
							vecBanners.clear();
						}
				%>		
			</table>
		</td>
	</tr>
</table>