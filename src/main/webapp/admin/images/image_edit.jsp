<%@ page language="java" import="
         com.debisys.utils.HTMLEncoder,
         org.apache.commons.fileupload.servlet.ServletFileUpload,
         com.debisys.presentation.ImageComponent" pageEncoding="ISO-8859-1"%>
<%
    int section = 9;
    int section_page = 18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Image" class="com.debisys.images.Image" scope="request"/>
<jsp:setProperty name="Image" property="*"/>
<%@include file="/includes/security.jsp" %>
<%    ImageComponent ic = new ImageComponent(Image);
    String action;
    if (ServletFileUpload.isMultipartContent(request)) {
        ic.loadFromRequest(request, response, application, SessionData);
        if (Image.getImageId() == 0) {
            String bid = ic.getValue("imageId");
            if (bid != null && !bid.equals("")) {
                Image.setImageId(Long.parseLong(bid));
            }
        }
        action = "" + ic.getValue("action");
    } else {
        action = "" + request.getParameter("action");
    }

    String message = request.getParameter("message");
    String messageType = request.getParameter("message");

    if (action.equals("save")) {
        if (ic.save()) {
            message = "Image saved successfully.";
            messageType = "1";
        } else {
            java.util.HashMap<String, String> errorsImage = ic.getErrors();
            if (errorsImage != null) {
                java.util.Collection c = errorsImage.values();
                java.util.Iterator itr = c.iterator();
                while (itr.hasNext()) {
                    String objData = itr.next().toString();
                    if (objData != null && objData.length() > 0) {
                        System.out.println("objData " + objData);
                        message = Languages.getString(objData, SessionData.getLanguage());
                    }
                }
                //message="dddd";
            } else {
                message = "Error saving news";
            }
            messageType = "2";
        }
        response.sendRedirect("/support/admin/brandings/branding_options.jsp?action=edit&imageId=" + Image.getImageId() + "&messageType=" + messageType + "&message=" + HTMLEncoder.encode(message));
    } else {
        ic.loadFromDB();
        if (action.equals("duplicate")) {
            ic.setValue("imageId", "");
        }
    }
%>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<form name="mainform" method="post" action="admin/images/image_edit.jsp" enctype="multipart/form-data">
    <input type="hidden" name="action" value="save">
    <%
        if (message != null) {
    %>
    <p style="color:<%=(messageType.equals("1") ? "green" : "red")%>"><%=message%></p>
    <%
        }
    %>
    <table border="0" cellpadding="0" cellspacing="0" width="750">
        <tr>
            <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
            <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;IMAGE SETTINGS</td>
            <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
        </tr>
        <tr>
            <td colspan="3" class="formArea2">
                <p style="color:white">
                    &lt;&lt; <a href="admin/brandings/branding_options.jsp"><%=Languages.getString("jsp.admin.brandings.tool", SessionData.getLanguage())%></a>
                </p>
                <table class="main details-table">
                    <% ic.showFields(out, SessionData);%>
                    <tr>
                        <td colspan="3" align="center"><input type="submit" name="save" value="Save Image"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="JavaScript">

        function onchangeDivInfo(sel) {

            var select_imageId_type = sel.options[sel.selectedIndex].value;

            jQuery.ajax({
                url: 'tools/images/getSuggestedSize',
                data: 'imageTypeCode=' + select_imageId_type,
                method: 'GET',
                dataType: "text",
                async: true
            }).done(function (response) {
                $('#b_tag_info').text(response);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                $('#b_tag_info').text(errorThrown);
            });
        }

    </script>
</form>