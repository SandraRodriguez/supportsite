<%@ page import="java.util.Calendar" %>
<%@ page import="com.debisys.terminalTracking.CProvider"%>
<%@ page import="com.debisys.terminalTracking.CSimCard"%>
<%@ page import="com.debisys.terminalTracking.CTerminalPartType"%>
<%@ page import="com.debisys.terminalTracking.CTerminalPartStatus"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
int section = 9;
int section_page = 5;


// Field values
String imsi = "";
String phoneNumber = "";
String serviceProvider = "";
String activationDate = "";
String lotNumber = "";
String contractNumber = "";
String contractDate = "";
String status = "";


String mobileSubscriberId;
CTerminalPartType partType;
String strMsgPartAlreadyExists = "";
String strMsgInsertProblems = "";
String strPhoneAlreadyInUse = "";

%> 


<%@ include file="/includes/security.jsp" %>

<%
  String strMessage = request.getParameter("message");
  if (strMessage == null)
  {
    strMessage = "";
  }

if (request.getParameter("simAddSubmitted") != null)
{
  try
  {
    if (request.getParameter("simAddSubmitted").equals("y"))
    {
    	// Save current values
    	imsi = (request.getParameter("mobileSubscriberIdH") != null)?request.getParameter("mobileSubscriberIdH"):"";
    	phoneNumber = (request.getParameter("phoneNumberH") != null)?request.getParameter("phoneNumberH"):"";
    	serviceProvider = (request.getParameter("providerIdH") != null)?request.getParameter("providerIdH"):"";
    	activationDate = (request.getParameter("activationDateH") != null)?request.getParameter("activationDateH"):"";
    	lotNumber = (request.getParameter("lotNumberH") != null)?request.getParameter("lotNumberH"):"";
    	contractNumber = (request.getParameter("contractNumberH") != null)?request.getParameter("contractNumberH"):"";
    	contractDate = (request.getParameter("contractDateH") != null)?request.getParameter("contractDateH"):"";
    	status = (request.getParameter("partStatusIdH") != null)?request.getParameter("partStatusIdH"):"";
    	
    	// If phone is not being used a flag for add is set
    	boolean bPhoneAvalable = false;
    	if((phoneNumber != null) && ((phoneNumber.equals("")) || (CSimCard.getPhonesInUse(phoneNumber) == 0)))
    	{
    		bPhoneAvalable = true;
    	}
    	else // Phone already in use
    	{
    		strPhoneAlreadyInUse = Languages.getString("jsp.admin.transactions.inventorySims.PhoneAlreadyInUse",SessionData.getLanguage());
    	}

		// Check if IMSI is already in use
		boolean bImsiAvailable = false;
		partType = CTerminalPartType.getTerminalPartTypeByCode(CTerminalPartType.SIMCARD_CODE);
    	if((imsi != null) &&(CSimCard.partExists(partType.getPartTypeId(),imsi) == null))
    	{
    		bImsiAvailable = true;
    	}
    	else // imsi already in use
    	{
    		strMsgPartAlreadyExists = Languages.getString("jsp.admin.transactions.inventorySims.ImsiAlreadyInUse",SessionData.getLanguage());
    	}
    	
    	if(bPhoneAvalable && bImsiAvailable)
    	{
	    	mobileSubscriberId = imsi;
	    	
	    			
	    	CSimCard newCard = new CSimCard();
	    	
	    	newCard.setPartType(partType);
	    	newCard.setLotNumber((lotNumber != null)?lotNumber:"");
	    	CTerminalPartStatus currentStatus = null;
	    	if((status != null) && (!status.equals("")))
	    	{
	    		currentStatus =  CTerminalPartStatus.getPartStatusById(Long.valueOf(status));
	    	}
	    	else
	    	{
	    		currentStatus =  CTerminalPartStatus.getPartStatusByCode(CTerminalPartStatus.NEW_CODE);
	    	}
	    	newCard.setStatus(currentStatus);
	    	newCard.setMobileSuscriberId(mobileSubscriberId);
	    	newCard.setPhoneNumber(phoneNumber);
	    	CProvider currentProvider = CProvider.getProviderById(Long.valueOf(serviceProvider));
	    	newCard.setProvider(currentProvider);
	    	newCard.setActivationDate((activationDate != null)?activationDate:Calendar.getInstance().toString());	
	    	newCard.setContractNumber((contractNumber!= null)?contractNumber:"");
	    	newCard.setContractDate((contractDate != null)?contractDate:Calendar.getInstance().toString());
	        if(!newCard.insertNewPart(SessionData.getProperty("ref_id"),SessionData))
	        {
	        	strMsgInsertProblems = Languages.getString("jsp.admin.transactions.inventorySims.InsertionFailed",SessionData.getLanguage());
	        }
	        else
	        {
	        	response.sendRedirect("inventorySims_edit.jsp?partId=" + newCard.getPartId() + "&message=" + strMessage);
	        }
    	}
    }
  }
  catch (Exception e){
  }
}
%>

<%@ include file="/includes/header.jsp" %>

<script SRC="includes/funcionesValidacion.js" type="text/javascript"> </script>
<script type="text/javascript">   
function saveCurrentFieldValues()
{
	document.getElementById('lotNumberH').value = document.getElementById('txtLotNumber').value;
	document.getElementById('partStatusIdH').value = document.getElementById('ddlStatus').value;
	document.getElementById('mobileSubscriberIdH').value = document.getElementById('txtImsi').value;
	document.getElementById('phoneNumberH').value = document.getElementById('txtPhoneNumber').value;
	document.getElementById('providerIdH').value = document.getElementById('ddlProvider').value;
	document.getElementById('activationDateH').value = document.getElementById('txtActivationDate').value;
	document.getElementById('contractNumberH').value = document.getElementById('txtContract').value;
	document.getElementById('contractDateH').value = document.getElementById('txtContractDate').value;
}

function cancelar(form) {
      form.action = "admin/transactions/inventorySims_search.jsp";
      form.submit(); 
    } 
        
function enviarDatos(form) 
{
    if(validacion(form))
    {
        saveCurrentFieldValues();
        form.submit();
    }
}   

function validacion(form)
   {
     var alpha = 0;
     var errors = "";
     
     if ( document.getElementById('txtImsi').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyImsi",SessionData.getLanguage())%>\n';
        alpha++;
     } 
     
     if ( document.getElementById('ddlProvider').selectedIndex==null || document.getElementById('ddlProvider').selectedIndex==-1 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyCarrier",SessionData.getLanguage())%>\n';
        alpha++;
     }      
     
     if ( document.getElementById('txtActivationDate').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyActivationDate",SessionData.getLanguage())%>\n';
        alpha++;
     }  
     
     if ( document.getElementById('txtLotNumber').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyLot",SessionData.getLanguage())%>\n';
        alpha++;
     }     
     
     if ( document.getElementById('txtContract').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyContract",SessionData.getLanguage())%>\n';
        alpha++;
     } 
     
     if ( document.getElementById('txtContractDate').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyContractDate",SessionData.getLanguage())%>\n';
        alpha++;
     }     
          
    if (alpha != 0) {
        alert(errors);
        return false;
    }
    else {	  	
        return true;
    }      
   }

</script>    
<form name="mainform" method="post">
<input type="hidden" id="simAddSubmitted" name="simAddSubmitted" value="y">	
<input type="hidden" id="lotNumberH" name="lotNumberH" value="">
<input type="hidden" id="partStatusIdH" name="partStatusIdH" value="">
<input type="hidden" id="mobileSubscriberIdH" name="mobileSubscriberIdH" value="">
<input type="hidden" id="phoneNumberH" name="phoneNumberH" value="">
<input type="hidden" id="providerIdH" name="providerIdH" value="">
<input type="hidden" id="activationDateH" name="activationDateH" value="">
<input type="hidden" id="contractNumberH" name="contractNumberH" value="">
<input type="hidden" id="contractDateH" name="contractDateH" value="">

<table border="0" cellpadding="0" cellspacing="0" width="60%">

<%
        if (!strMsgPartAlreadyExists.equals(""))
        {
          out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + strMsgPartAlreadyExists + "</td></tr>");
        }
		if (!strMsgInsertProblems.equals(""))
		{
		  out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + strMsgInsertProblems + "</td></tr>");
		}
		if (!strPhoneAlreadyInUse.equals(""))
		{
		  out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + strPhoneAlreadyInUse + "</td></tr>");
		}
%>         



	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan=2>                   
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">                                                   
                                                    <form name="mainform" method="post" action="admin/transactions/inventorySims_add.jsp">
                                                        <tr><td>&nbsp;</td>
                                                            <td colspan="2" class="main">
                                                            <br>
                                                            <%= Languages.getString("jsp.admin.transactions.inventorySimsAdd.add_instructions",SessionData.getLanguage()) %>
                                                            <br>                                                                                                          
                                                        </td></tr>
                                                        <tr><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><STRONG><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.imsi",SessionData.getLanguage())%>:</STRONG></td>
                                                            <td><input type=text class="plain" name="imsi" id="txtImsi" maxlength="20" onkeypress="return validaNum(event);" value="<%=imsi%>"></td>                                                           
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.phoneNumber",SessionData.getLanguage())%>:</td>
                                                            <td><input type=text class="plain" name="phoneNumber" id="txtPhoneNumber" maxlength="15" onkeypress="return validaNum(event);" value="<%=phoneNumber%>"></td>                                                                                                                           
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.carrier",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="provider" id="ddlProvider">
							    	<%
							    		CProvider[] providerList = CProvider.getProviderList();
							    		if((providerList != null) && (providerList.length > 0))
							    		{
							    			// Selected provider
							    			long lProviderId = (serviceProvider.equals(""))?0:Long.valueOf(serviceProvider);
                                			for(CProvider currentProvider:providerList)
                                			{
                                				if(currentProvider.getProviderId() == lProviderId)
                                				{
                                					out.println("<option value=" + currentProvider.getProviderId() +" selected>" + currentProvider.getDescription() + "</option>");
                                				}
                                				else
                                				{
                                					out.println("<option value=" + currentProvider.getProviderId() +">" + currentProvider.getDescription() + "</option>");
                                				}
                                			}
							    		}
							    	%>
                                    </select>
                                </td> 
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.activationDate",SessionData.getLanguage())%>:</strong></td>
							    <td><input class="plain" id="txtActivationDate" name="activationDate" value="<%=activationDate%>" readonly=true size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.mainform.activationDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>                                                           
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.lotNumber",SessionData.getLanguage())%>:</strong></td>
                                                            <td><input type=text class="plain" name="lotNumber" id="txtLotNumber" maxlength="50" onkeypress="return validaNumYTexto(event);" value="<%=lotNumber%>"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.contract",SessionData.getLanguage())%>:</strong></td>
							    <td><input type=text class="plain" name="contract" id="txtContract" maxlength="50" onkeypress="return validaNumYTexto(event);" value="<%=contractNumber%>"></td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.contractDate",SessionData.getLanguage())%>:</strong></td>
							    <td><input class="plain" id="txtContractDate" name="contractDate" value="<%=contractDate%>" readonly=true size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.mainform.contractDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>                                                           
                                                        </tr>                                                         
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.status",SessionData.getLanguage())%>:</td>
							    <td>
							    	<select name="status" id="ddlStatus" disabled>
									<%
										long lStatus = (status.equals(""))?0:Long.valueOf(status);
										CTerminalPartStatus[] partStatusList = CSimCard.getAllowedStatus(null, null);
										if((partStatusList != null) && (partStatusList.length > 0))
										{
                                   			for(CTerminalPartStatus currentStatus:partStatusList)
                                   			{
                                   				if(currentStatus.getPartStatusId() == lStatus)
                                   				{
                                   					out.println("<option value=" + currentStatus.getPartStatusId() +" selected>" + currentStatus.getPartStatusDescription() + "</option>");
                                   				}
                                   				else
                                   				{
                                   					out.println("<option value=" + currentStatus.getPartStatusId() +">" + currentStatus.getPartStatusDescription() + "</option>");
                                   				}
                                   			}
										}
									%>
	                                </select>
	                            </td>                                                            
                                                        </tr>                                                                                                                    
                                                        
                                                        <tr><td><br></td></tr>
							<tr align="center">
                                                                <td>&nbsp;</td>
                                                                <td align=center colspan="2">
                                                                    <input type="hidden" name="simAddSubmitted" value="y">
                                                                    <input type="hidden" name="message" value="2">
                                                                    <input id="btnaddSim" type="button" value="<%= Languages.getString("jsp.admin.transactions.inventorySimsAdd.addNewSim",SessionData.getLanguage()) %>" onclick="enviarDatos(this.form)">                                                              
                                                                    <input id="btnCancel" type="button" value="<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.cancel",SessionData.getLanguage())%>" onClick="cancelar(this.form)">
                                                                </td>                                                                
							</tr>
							<tr><td><br></td></tr>                                                        
                                                    </form>
                                                 </table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
                                </tr>                                                 
                        </table>
                </td>
        </tr>
</table>
</form>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<%@ include file="/includes/footer.jsp" %>                                                   