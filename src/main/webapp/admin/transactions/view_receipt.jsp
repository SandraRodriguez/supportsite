<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 com.debisys.languages.Languages,
                 java.util.*" %>
<%
int section = 3;
int section_page = 8;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
 if ( request.getParameter("data") != null )
 {
    String dataToShow = request.getParameter("data");
%>
<HTML>
<HEAD>
    <LINK HREF="../../default.css" TYPE="text/css" REL="stylesheet">
    <TITLE><%=Languages.getString("jsp.admin.transactions.viewreceipt.receiptinfo",SessionData.getLanguage())%></TITLE>
</HEAD>
<BODY BGCOLOR="#ffffff">
<TABLE WIDTH="100%" HEIGHT="100%" STYLE="height:100%;">
    <TR>
        <TD ALIGN="center" VALIGN="middle">
            <TABLE CELLSPACING="1" CELLPADDING="1" BORDER="0">                
                <%if (dataToShow != null ){%>
                <TR>
                    <TD CLASS="rowhead2"><B></B></TD>
                    <TD CLASS="row2"><%=dataToShow%></TD>
                </TR>               
                
                <%}
                else
                {
                  out.println("<tr><td colspan=2>No information available.</td></tr>");
                }
                %>
            </TABLE>
        </TD>
    </TR>
</TABLE>
</BODY>
</HTML>
<%
 }
%>