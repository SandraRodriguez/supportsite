<%@ page import="com.debisys.terminalTracking.CApplicationType"%>
<%@ page import="com.debisys.terminalTracking.CTerminalStatus"%>
<%@ page import="com.debisys.terminalTracking.CTerminalBrand"%>
<%@ page import="com.debisys.terminalTracking.CTerminalModel"%>
<%@ page import="com.debisys.terminalTracking.CCommunicationType"%>
<%@ page import="com.debisys.terminalTracking.CPhysicalTerminal"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
int section = 9;
int section_page = 5;

String serialNumber = "";
String brandId = "";
String modelId = "";
String applicationType = "";
String softwareLicense = "";
String communicationTypeId = "";

String[] strCommTypes=null;

String primaryCommTypeId = "";
String imei = "";
String purchaseOrder = "";
String admittanceDate = "";
String newStatus = "";
String lotNumber = "";
String automaticSim = "";
String msgCreacion = "";
String tipoCommGPRS = CCommunicationType.GPRS_CODE;

boolean terminalCreada = Boolean.FALSE;
long idNuevaTerminal = 0;

String strMessage = request.getParameter("message");
if (strMessage == null)
{
    strMessage = "";
}

if (request.getParameter("submittedITA") != null)
{
  try
  {
    if (request.getParameter("submittedITA").equals("y"))
    {
        //Obtener los valores de la interfaz de usuario
        serialNumber = (request.getParameter("serialNumberH") != null)?request.getParameter("serialNumberH"):"";
        brandId = (request.getParameter("brandIdH") != null)?request.getParameter("brandIdH"):"";
        modelId = (request.getParameter("modelIdH") != null)?request.getParameter("modelIdH"):"";          
        applicationType = (request.getParameter("applicationTypeIdH") != null)?request.getParameter("applicationTypeIdH"):"";
        softwareLicense = (request.getParameter("softwareLicenseH") != null)?request.getParameter("softwareLicenseH"):"";
        communicationTypeId = (request.getParameter("communicationTypeIdH") != null)?request.getParameter("communicationTypeIdH"):"";
        //Obtener el arreglo de valores seleccionados para los tipos de comunicacion
        strCommTypes = request.getParameterValues("communicationType");        
        primaryCommTypeId = (request.getParameter("primaryCommTypeIdH") != null)?request.getParameter("primaryCommTypeIdH"):"";
        imei = (request.getParameter("imeiH") != null)?request.getParameter("imeiH"):"";
        purchaseOrder = (request.getParameter("purchaseOrderH") != null)?request.getParameter("purchaseOrderH"):"";
        admittanceDate = (request.getParameter("admittanceDateH") != null)?request.getParameter("admittanceDateH"):"";        
        newStatus = (request.getParameter("newStatusH") != null)?request.getParameter("newStatusH"):"";        
        lotNumber = (request.getParameter("lotNumberH") != null)?request.getParameter("lotNumberH"):"";
        automaticSim = (request.getParameter("automaticSimH") != null)?request.getParameter("automaticSimH"):"";
    }
    if(request.getParameter("saveTerminal").equals("y"))
    {   
    	// If not in the physical terminal table but if also the serial is not duplicated with one on the terminal table, the new terminal can be created
    	if((CPhysicalTerminal.terminalExists(serialNumber, Long.parseLong(brandId.trim())) == null) && (CPhysicalTerminal.IsSerialNumberUnique(serialNumber)))
    	{	
	        //Establecer los valores del objeto segun lo recibido en el request
	        CPhysicalTerminal terminalFisico = new CPhysicalTerminal();
	        terminalFisico.setSerialNumber(serialNumber);
	        //Brand
	        CTerminalBrand brandObj = new CTerminalBrand();
	        brandObj.loadBrand(Long.parseLong(brandId.trim()));
	        brandObj.loadBrandModels();
	        terminalFisico.setBrand(brandObj);
	        //Model
	        terminalFisico.setTerminalModel(Long.parseLong(modelId.trim()));
	        //Application Type
	        CApplicationType appObj= new CApplicationType();
	        appObj.loadAppType(Long.parseLong(applicationType.trim()));
	        terminalFisico.setApplicationType(appObj);
	        //Software License
	        terminalFisico.setSoftwareLicense(softwareLicense);
	        //Communication Type
	        terminalFisico.setCommunicationTypeSet(strCommTypes);
	        //Primary Comm Type
	        terminalFisico.setPrimaryCommType(Long.parseLong(primaryCommTypeId.trim()));
	        //IMEI
	        terminalFisico.setMobileEquipmentId(imei);
	        //Purchase Order
	        terminalFisico.setPurchaseOrder(purchaseOrder);
	        //Admittance Date
	        terminalFisico.setInventoryAdmitanceDate(admittanceDate);
	        //Status
	        CTerminalStatus statusObj = new CTerminalStatus();
	        statusObj.load(Long.parseLong(newStatus.trim()));
	        terminalFisico.setCurrentStatus(statusObj);
	        //Lot number
	        terminalFisico.setLotNumber(lotNumber);
	        	        
	    	boolean bSoftwareLicAvalable = true;
	    	if((terminalFisico.getApplicationType().getApplicationTypeCode().equals(CApplicationType.MICROBROWSER_CODE)) && 
	    	   (CPhysicalTerminal.softwareLicenseExists(terminalFisico.getSoftwareLicense())))
	    	{
	    		// Software License already in use
	    		bSoftwareLicAvalable = false;
	    		msgCreacion = Languages.getString("jsp.admin.transactions.inventoryTerminalsSoftwareLicAlreadyInUse",SessionData.getLanguage());
	    	}


	    	boolean bIMEIAvalable = true;
	    	if((!terminalFisico.getMobileEquipmentId().equals("")) && 
	    	   (terminalFisico.getPrimaryCommType().getCommunicationTypeCode().equals(CCommunicationType.GPRS_CODE)) && 
	    	   (CPhysicalTerminal.imeiExists(terminalFisico.getMobileEquipmentId())))
	    	{
	    		// Software License already in use
	    		bIMEIAvalable = false;
	    		msgCreacion = Languages.getString("jsp.admin.transactions.inventoryTerminalsImeiAlreadyInUse",SessionData.getLanguage());
	    	}

	        if(bSoftwareLicAvalable && bIMEIAvalable)
	        {                
		        //Crear la terminal
		        if(automaticSim.equals("checked"))
		        {
		            terminalCreada = terminalFisico.createNewTerminal(true, SessionData.getProperty("ref_id"),SessionData);	                   
		        }
		        else
		        {
		            terminalCreada = terminalFisico.createNewTerminal(false, SessionData.getProperty("ref_id"),SessionData);
		        }
	            //Obtener el id de la terminal que se acaba de crear y redirigir la llamada
	            if(terminalCreada)
	            {
	                idNuevaTerminal=terminalFisico.getTerminalId();                    
	                //Redirigir hacia el JSP de edicion de terminales
	                response.sendRedirect("inventoryTerminals_edit.jsp?idterminal=" + idNuevaTerminal + "&message=" + strMessage);                            
	            }
	            else
	            {
	                msgCreacion = Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.insertionFailed",SessionData.getLanguage());
	            }
	        }
    	}
    	else
    	{
        	msgCreacion = Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.alreadyExists",SessionData.getLanguage());
    	}
    }
  }
  catch (Exception e){
  }
}
%>
<%! 
boolean estaContenidoEn(long valor, String[] arreglo)
{
    if(arreglo!=null){
        for(int i=0;i<arreglo.length;i++){
            if(Long.parseLong(arreglo[i])==valor){
                return true;
            }
        }
    }
    return false;
};
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script SRC="includes/funcionesValidacion.js" type="text/javascript"> </script>
<script type="text/javascript">   
    
function saveCurrentFieldValues()
{
	document.getElementById('serialNumberH').value = document.getElementById('txtSerial').value;        
	document.getElementById('brandIdH').value = document.getElementById('ddlBrand').value;
	document.getElementById('modelIdH').value = document.getElementById('ddlModel').value;
	document.getElementById('applicationTypeIdH').value = document.getElementById('ddlApplicationType').value;
        document.getElementById('lotNumberH').value = document.getElementById('txtLotNumber').value;
        document.getElementById('softwareLicenseH').value = document.getElementById('txtSoftwareLicense').value;
        document.getElementById('communicationTypeIdH').value = document.getElementById('ddlCommunicationType').value;
        document.getElementById('primaryCommTypeIdH').value = document.getElementById('ddlPrimaryCommType').value;
        document.getElementById('imeiH').value = document.getElementById('txtImei').value;
        document.getElementById('purchaseOrderH').value = document.getElementById('txtPurchaseOrder').value;
        document.getElementById('admittanceDateH').value = document.getElementById('txtAdmittanceDate').value;                
        document.getElementById('newStatusH').value = document.getElementById('ddlStatus').value;        
        if(document.getElementById('chkAutomaticSim').checked){
            document.getElementById('automaticSimH').value = "checked";
        }
	document.mainform.submit();
}   

function cancelar(form) {
      form.action = "admin/transactions/inventoryTerminals_search.jsp";
      form.submit(); 
    }
        
function enviarDatos(form) {
      if(validacion(form)){
        form.saveTerminal.value="y";
        saveCurrentFieldValues();
      }
    }   

function validacion(form)
   {
     var alpha = 0;
     var errors = "";
     
     if ( document.getElementById('txtSerial').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptySerial",SessionData.getLanguage())%>\n';
        alpha++;
     } 
            
     if ( document.getElementById('ddlBrand').value==0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyBrand",SessionData.getLanguage())%>\n';
        alpha++;
     }  
     
     if ( document.getElementById('ddlModel').value==0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyModel",SessionData.getLanguage())%>\n';
        alpha++;
     } 
     
     if ( document.getElementById('ddlApplicationType').value==0){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyAppType",SessionData.getLanguage())%>\n';
        alpha++;
     }
     
     if ( document.getElementById('ddlCommunicationType').value==0){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyCommType",SessionData.getLanguage())%>\n';
        alpha++;
     }   
     
     if ( document.getElementById('ddlPrimaryCommType').value==0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyPrimaryCommType",SessionData.getLanguage())%>\n';
        alpha++;
     }  
     
     if ( document.getElementById('txtPurchaseOrder').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyPurchaseOrder",SessionData.getLanguage())%>\n';
        alpha++;
     }      
     
     if ( document.getElementById('txtAdmittanceDate').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyAdmittanceDate",SessionData.getLanguage())%>\n';
        alpha++;
     }     
     
     if ( document.getElementById('txtLotNumber').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyLot",SessionData.getLanguage())%>\n';
        alpha++;
     }      
     
     var selCommIndex=0;
     var selCommValues = new Array();
     var selCommTexts = new Array();
     for(i=0; i<document.getElementById('ddlCommunicationType').length; i++){
        if(document.getElementById('ddlCommunicationType').options[i].selected){
            selCommValues[selCommIndex] = document.getElementById('ddlCommunicationType').options[i].value;  
            selCommTexts[selCommIndex] = document.getElementById('ddlCommunicationType').options[i].text;              
            selCommIndex++;
        }
     }   
     
     var validaCommPrimary = 0;
     for(j=0; j<selCommValues.length ; j++){
        if(parseInt(selCommValues[j])==parseInt(document.getElementById('ddlPrimaryCommType').value)){
            validaCommPrimary=1;
        }
     }
     
     if (validaCommPrimary == 0){        
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.commNotPrimary",SessionData.getLanguage())%>\n';
        alpha++;
     }
     
     var supportGPRS = 0;          
     for(k=0; k<selCommValues.length ; k++){   
        if(selCommTexts[k]==document.getElementById('tipoCommGPRS').value){
            supportGPRS=1;
        }
     }   
     
     if (supportGPRS == 0 && document.getElementById('txtImei').value.length != 0){        
        alert("<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.imeiNotGprs",SessionData.getLanguage())%>");
        document.getElementById('txtImei').value = "";
     }     
          
    if (alpha != 0) {
        alert(errors);
        return false;
    }
    else {	  	
        return true;
    }      
   } 

</script>    
<table border="0" cellpadding="0" cellspacing="0" width="60%">
<%
        if (!msgCreacion.equals(""))
        {
          out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + msgCreacion + "</td></tr>");
        }
%>         
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		</td>
	</tr>
	<tr>
		<td colspan=2>                   
			<table width="100%" border="0" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="left" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="left">                                                   
                                                    <form name="mainform" method="post" action="/support/admin/transactions/inventoryTerminals_add.jsp">
                                                        <input type="hidden" name="submittedITA" value="y">
                                                        <input type="hidden" name="saveTerminal" value="n">
                                                        <input type="hidden" name="message" value="2">
                                                        <input type="hidden" id="tipoCommGPRS" name="tipoCommGPRS" value="<%=tipoCommGPRS%>">
                                                        <input type="hidden" id="serialNumberH" name="serialNumberH" value="">
                                                        <input type="hidden" id="brandIdH" name="brandIdH" value="">
                                                        <input type="hidden" id="modelIdH" name="modelIdH" value="">
                                                        <input type="hidden" id="applicationTypeIdH" name="applicationTypeIdH" value="">                                                        
                                                        <input type="hidden" id="softwareLicenseH" name="softwareLicenseH" value="">
                                                        <input type="hidden" id="communicationTypeIdH" name="communicationTypeIdH" value="">
                                                        <input type="hidden" id="primaryCommTypeIdH" name="primaryCommTypeIdH" value="">
                                                        <input type="hidden" id="imeiH" name="imeiH" value="">
                                                        <input type="hidden" id="purchaseOrderH" name="purchaseOrderH" value="">
                                                        <input type="hidden" id="admittanceDateH" name="admittanceDateH" value="">  
                                                        <input type="hidden" id="newStatusH" name="newStatusH" value="">  
                                                        <input type="hidden" id="lotNumberH" name="lotNumberH" value="">
                                                        <input type="hidden" id="automaticSimH" name="automaticSimH" value="">
                                                        <tr><td>&nbsp;</td>
                                                            <td colspan="2" class="main">
                                                            <br>
                                                            <%= Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.add_instructions",SessionData.getLanguage()) %>
                                                            <br>                                                                                                          
                                                        </td></tr>
                                                        <tr><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><STRONG><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.serial",SessionData.getLanguage())%>:</STRONG></td>
                                                            <% 
                                                            int maxLenSerial=0;
                                                            if(SessionData.getUser().isQcommBusiness() == true)
                                                            {
                                                                maxLenSerial=26;
                                                            }else{
                                                                maxLenSerial=25;
                                                            }                                                           
                                                            %>                                                             
                                                            <td><input type=text class="plain" id="txtSerial" name="serial" onkeypress="return validaNumYTextoYLinea(event);" value='<%=serialNumber%>' maxlength='<%=maxLenSerial%>'></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.brand",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="brand" id="ddlBrand" onchange="saveCurrentFieldValues()">
                                                                <%
                                                                        CTerminalBrand[] brandList = CTerminalBrand.getBrandList();
                                                                        CTerminalBrand selectedBrand = null;
                                                                        boolean brandSelected = false;
                                                                        for(CTerminalBrand currentBrand:brandList)
                                                                        {
                                                                                if((brandId != "") && (brandId.equals(String.valueOf(currentBrand.getBrandId()))))
                                                                                {
                                                                                        out.println("<option value=" + currentBrand.getBrandId() +" selected>" + currentBrand.getBrandName() + "</option>");
                                                                                        brandSelected = true;
                                                                                        selectedBrand = currentBrand;
                                                                                        selectedBrand.loadBrandModels();
                                                                                }
                                                                                else
                                                                                {
                                                                                        out.println("<option value=" + currentBrand.getBrandId() +">" + currentBrand.getBrandName() + "</option>");
                                                                                }
                                                                        }
                                                                        if(!brandSelected)
                                                                        {
                                                                                out.println("<option value=0 selected>" + Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.selectOption",SessionData.getLanguage()) + "</option>");
                                                                        }
                                                                %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.model",SessionData.getLanguage())%>:</strong></td>
                                                            <td>
                                                                <select name="model" id="ddlModel" >
                                                                    <option value=0 selected><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.selectOption",SessionData.getLanguage())%></option>
                                                                    <%
                                                                            if(selectedBrand != null)
                                                                            {
                                                                                for(CTerminalModel currentModel: selectedBrand.getModelList())
                                                                                {
                                                                                    if((modelId != "") && (modelId.equals(String.valueOf(currentModel.getModelId()))))
                                                                                        {
                                                                                                out.println("<option value=" + currentModel.getModelId() +" selected>" + currentModel.getModelName() + "</option>");
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                                out.println("<option value=" + currentModel.getModelId() +">" + currentModel.getModelName() + "</option>");
                                                                                        }                                                                                             
                                                                                }
                                                                            }
                                                                    %>                                                                    
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.applicationType",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="applicationType" id="ddlApplicationType">
                                                                    <%
                                                                        CApplicationType[] appTypeList = CApplicationType.getAllApplicationTypes();
                                                                        boolean applicationTypeSelected = false;
                                                                        for(CApplicationType appType:appTypeList)
                                                                        {
                                                                                if((applicationType != "") && (applicationType.equals(String.valueOf(appType.getApplicationTypeId()))))
                                                                                {
                                                                                        out.println("<option value=" + appType.getApplicationTypeId() +" selected>" +appType.getApplicationTypeDescription() + "</option>");
                                                                                        applicationTypeSelected = true;
                                                                                }
                                                                                else
                                                                                {
                                                                                        out.println("<option value=" + appType.getApplicationTypeId() +">" +appType.getApplicationTypeDescription() + "</option>");
                                                                                }
                                                                        }
                                                                        if(!applicationTypeSelected)
                                                                        {
                                                                                out.println("<option value=0 selected>" + Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.selectOption",SessionData.getLanguage()) + "</option>");
                                                                        }
                                                                    %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.softwareLicense",SessionData.getLanguage())%>:</td>
                                                            <td><input type=text class="plain" name="softwareLicense" id="txtSoftwareLicense" maxlength="50" onkeypress="return validaNumYTextoYLinea(event);" value='<%=softwareLicense%>'></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.communicationType",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="communicationType" id="ddlCommunicationType" size=5 multiple>
                                                                    <%
                                                                        CCommunicationType[] commTypeList = CCommunicationType.getAllCommunicationTypes();
                                                                        for(CCommunicationType commType:commTypeList)
                                                                        {
                                                                            if(strCommTypes!=null)
                                                                            {                                                                                                                                                                                                                                                        
                                                                                if(estaContenidoEn(commType.getCommunicationTypeId(),strCommTypes))
                                                                                {
                                                                                        out.println("<option value=" + commType.getCommunicationTypeId() +" selected>" +commType.getCommunicationTypeDescription() + "</option>");
                                                                                }
                                                                                else
                                                                                {
                                                                                        out.println("<option value=" + commType.getCommunicationTypeId() +">" +commType.getCommunicationTypeDescription() + "</option>");
                                                                                }                                                                                
                                                                            } else {
                                                                                out.println("<option value=" + commType.getCommunicationTypeId() +">" +commType.getCommunicationTypeDescription() + "</option>");
                                                                            }   
                                                                        }
                                                                    %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr> 
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.primaryCommType",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="primaryCommType" id="ddlPrimaryCommType">
                                                                    <%
                                                                        CCommunicationType[] commPrimaryList = CCommunicationType.getAllCommunicationTypes();
                                                                        boolean communicationTypeSelected = false;
                                                                        for(CCommunicationType commType:commPrimaryList)
                                                                        {
                                                                                if((primaryCommTypeId != "") && (primaryCommTypeId.equals(String.valueOf(commType.getCommunicationTypeId()))))
                                                                                {
                                                                                        out.println("<option value=" + commType.getCommunicationTypeId() +" selected>" +commType.getCommunicationTypeDescription()+ "</option>");
                                                                                        communicationTypeSelected = true;
                                                                                }
                                                                                else
                                                                                {
                                                                                        out.println("<option value=" + commType.getCommunicationTypeId() +">" +commType.getCommunicationTypeDescription() + "</option>");
                                                                                }
                                                                        }
                                                                        if(!communicationTypeSelected)
                                                                        {
                                                                                out.println("<option value=0 selected>" + Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.selectOption",SessionData.getLanguage()) + "</option>");
                                                                        }
                                                                    %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr>  
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.imei",SessionData.getLanguage())%>:</td>
                                                            <td><input type=text class="plain" name="imei" id="txtImei" maxlength="15" onkeypress="return validaNum(event);" value='<%=imei%>'></td>
                                                        </tr>    
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.purchaseOrder",SessionData.getLanguage())%>:</strong></td>
                                                            <td><input type=text class="plain" name="purchaseOrder" id="txtPurchaseOrder" maxlength="50" onkeypress="return validaNumYTexto(event);" value='<%=purchaseOrder%>'></td>
                                                        </tr>                                                        
							<tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.admittanceDate",SessionData.getLanguage())%>:</strong></td>
							    <td><input class="plain" id="txtAdmittanceDate" name="admittanceDate" readonly=true size="12" value='<%=admittanceDate%>'><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.mainform.admittanceDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
							</tr>  
                                                        <tr>
                                                            <td>&nbsp;</td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.status",SessionData.getLanguage())%>:</td>
							    <td>
							    	<select name="status" id="ddlStatus" disabled >
                                                                    <%
                                                                        CTerminalStatus[] statusList = CPhysicalTerminal.getAllowedStatus(-1,null);//Se limita para que solo obtenga el estado nuevo
                                                                        
                                                                        if(statusList!=null){
                                                                            for(CTerminalStatus statusItem:statusList)
                                                                            {
                                                                                out.println("<option value=" + statusItem.getTerminalSatusId() +" selected>" +statusItem.getTerminalStatusDescription() + "</option>");
                                                                            }                                                                                
                                                                        }                                                                            
                                                                    %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr>                                                         
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.lotNumber",SessionData.getLanguage())%>:</strong></td>
                                                            <td><input type=text class="plain" name="lotNumber" id="txtLotNumber" maxlength="50" onkeypress="return validaNumYTexto(event);" value='<%=lotNumber%>' ></td>
                                                        </tr>  
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.automaticSim",SessionData.getLanguage())%>:</td>
                                                            <td><input type=checkbox class="plain" name="automaticSim" id="chkAutomaticSim" <%=automaticSim%>></td>
                                                        </tr>                                                          
                                                        
                                                        <tr><td><br></td></tr>
							<tr align="center">
                                                                <td>&nbsp;</td>
                                                                <td align=center colspan="2">                                                                    
                                                                    <input id="btnaddTerminal" type="button" value="<%= Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.addNewTerminal",SessionData.getLanguage()) %>" onclick="enviarDatos(this.form)">                                                              
                                                                    <input id="btnCancel" type="button" value="<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.cancel",SessionData.getLanguage())%>" onClick="cancelar(this.form)">
                                                                </td>                                                                
							</tr>
							<tr><td><br></td></tr>                                                        
                                                    </form>
                                                 </table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
                                </tr>                                                 
                        </table>
                </td>
        </tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<%@ include file="/includes/footer.jsp" %>                                                   