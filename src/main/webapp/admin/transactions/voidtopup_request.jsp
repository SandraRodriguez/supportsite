<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.reports.TransactionReport" %>

<%
	int section = 9;
	int section_page = 6;
	String strTransactionNo = "";
	Vector<String> vecSearchResults = new Vector<String>();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<table border="0" cellpadding="0" cellspacing="0" width="450">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle"><b><%=Languages.getString("jsp.includes.menu.voidtopup_request",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
							<tr>
								<td class="main" align="center" nowrap><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.title",SessionData.getLanguage())%></b></td>
							</tr>
<%
	if ( request.getParameter("txtTransactionNumber") == null )
	{
%>
							<tr>
								<td class="main" align="center" nowrap><br><%=Languages.getString("jsp.admin.transactions.voidtopup_request.welcome",SessionData.getLanguage())%><br><br></td>
							</tr>
							<tr>
								<td class="main" align="center">
									<script>
									function ValidateForm()
									{
										if ( document.getElementById("txtTransactionNumber").value == "" )
										{
											alert("<%=Languages.getString("jsp.admin.transactions.voidtopup_request.transactionrequired",SessionData.getLanguage())%>");
											return false;
										}
										document.getElementById("btnSubmit").disabled = true;
										return true;
									}
									</script>
									<form name="mainform" method="post" action="/support/admin/transactions/voidtopup_request.jsp" onSubmit="return ValidateForm();">
										<table BORDER="0" CELLPADDING="4" CELLSPACING="0" height="1">
											<tr>
												<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.voidtopup_request.transaction",SessionData.getLanguage())%></td>
												<td class="main" >
													<input type=textbox class="plain" name="txtTransactionNumber" id="txtTransactionNumber" value="" size="24">
													<script>
														document.getElementById("txtTransactionNumber").focus();
													</script>
												</td>
											</tr>
											<tr>
												<input type="hidden" name="search" value="y">
												<td class="main" align="center" colspan="2"><input type=submit name="btnSubmit" id="btnSubmit" value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.submit",SessionData.getLanguage())%>"></td>
											</tr>
										</table>
									</form>
								</td>
							</tr>
<%
    }//End of if we need to show the default page
	else
	{//Else if a request has been made
	   String sTrxNo = request.getParameter("txtTransactionNumber");
	   boolean isValidTranxNo = true;	   
	   int iTrxNo = 0;
	   try{
	   	iTrxNo = Integer.parseInt(sTrxNo);
	   }catch(NumberFormatException e){
	   		isValidTranxNo = false;
	   }
	 if(!isValidTranxNo){	
%>
			<tr><td class=main align=center><br><br><%=Languages.getString("jsp.admin.transactions.voidtopup_request.invalidtrxnumber",SessionData.getLanguage())%></td></tr>
			<tr>
				<td align=center>
					<br><br>
					<form name="mainform" method="post" action="/support/admin/transactions/voidtopup_request.jsp">
						<input type=submit value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.return",SessionData.getLanguage())%>">
					</form>
				</td>
			</tr>
<%		
	 }
	 else
	 {
		vecSearchResults = TransactionReport.getVoidTopUpTransaction(SessionData, iTrxNo);
		if ( vecSearchResults.size() > 0 )
		{//If there are records for this Transaction
		
			String sku = (String)vecSearchResults.get(0);
			String terminal = (String)vecSearchResults.get(3); 
			String clerkID = (String)vecSearchResults.get(4); 
		    boolean isAvoidableSKU = TopUpVoidProcessor.isSKUAutomaticVoidable(sku);
		    
			Vector<String> duplicate = TransactionReport.getDuplicatedVoidTopUp(SessionData, iTrxNo);
			if ( duplicate.isEmpty() )
			{//If there are no duplicates
%>
							<tr>
								<td>
									<form name="mainform" id="mainform" method="post" action="/support/admin/transactions/voidtopup_request.jsp">
										<table>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.productid",SessionData.getLanguage())%></b></td>
												<td>&nbsp;</td>
												<td class=main><%=vecSearchResults.get(0)%></td>
												<td>&nbsp;&nbsp;&nbsp;</td>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.productdesc",SessionData.getLanguage())%></b></td>
												<td>&nbsp;</td>
												<td class=main><%=vecSearchResults.get(1)%></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.ani",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(2)%></td>
												<td></td>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.siteid",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(3)%><input type=hidden name="txtSiteID" value="<%=vecSearchResults.get(3)%>"></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.clerk",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(10)%></td>
												<td></td>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.control_no",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(5)%><input type=hidden name="txtControlNumber" value="<%=vecSearchResults.get(5)%>"></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.amount",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(6)%></td>
												<td></td>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.datetime",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(7)%></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.recid",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(8)%></td>
												<td></td>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.client",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(9)%></td>
											</tr>
<%
				if ( request.getParameter("submitted") == null )
				{//If we need to show the TopUp data page
%>
											<tr id="trMainButtons" style="display:inline;">
												<td colspan=3 align=center><input type=button id="btnCreateRequest" value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.createrequest",SessionData.getLanguage())%>" onclick="ShowRequestData();"></td>
												<td><br><br><br></td>
												<td colspan=3 align=center><input type=button id="btnCancel" value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.cancel",SessionData.getLanguage())%>" onclick="history.go(-1);"></td>
											</tr>
											<tr id="trRequestDataReasons" style="display:none;">
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.voidreason",SessionData.getLanguage())%></b></td>
												<td><br><br></td>
												<td colspan=5>
													<select name=ddlVoidReason>
<%
					Iterator itReasons = TransactionReport.getVoidTopUpReasons().iterator();
					while (itReasons.hasNext())
					{
						Vector vTemp = (Vector)itReasons.next();
%>
														<option value="<%=vTemp.get(0)%>"><%=vTemp.get(2)%></option>
<%
					}
					itReasons = null;
%>
													</select>
												</td>
											</tr>
											<tr id="trRequestDataComments" style="display:none;">
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.voidcomments",SessionData.getLanguage())%></b></td>
												<td></td>
												<td colspan=5>
													<textarea id="txtComments" name="txtComments" cols="30" rows="5" onblur="if (this.value.length > 512) { this.value = this.value.substring(0, 512); }"></textarea>
												</td>
											</tr>
											<tr id="trRequestDataButtons" style="display:none;">
												<td colspan=3 align=center><input type=submit id="btnSubmitRequest" value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.submit",SessionData.getLanguage())%>" onclick="this.disabled = true;this.form.submit();"></td>
												<td>
													<input type=hidden name="txtTransactionNumber" value="<%=request.getParameter("txtTransactionNumber")%>">
													<input type=hidden name="submitted" value="y">
													<script>
														function ShowRequestData()
														{
															document.getElementById("trMainButtons").style.display = "none";
															document.getElementById("trRequestDataReasons").style.display = "inline";
															document.getElementById("trRequestDataComments").style.display = "inline";
															document.getElementById("trRequestDataButtons").style.display = "inline";
														}
														function HideRequestData()
														{
															document.getElementById("trMainButtons").style.display = "inline";
															document.getElementById("trRequestDataReasons").style.display = "none";
															document.getElementById("trRequestDataComments").style.display = "none";
															document.getElementById("trRequestDataButtons").style.display = "none";
														}
													</script>
												</td>
												<td colspan=3 align=center><input type=reset id="btnCancelRequest" value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.cancel",SessionData.getLanguage())%>" onclick="HideRequestData();"></td>
											</tr>
											
<%
					if(isAvoidableSKU)out.print("<tr><td colspan=7 align=center class=main><font color=ff0000>**"+Languages.getString("jsp.admin.transactions.voidtopup_request.warningAutVoid",SessionData.getLanguage())+"</font></td></tr>");
				}//End of if we need to show the TopUp data page
				else
				{	//Apply processing for automatic void SKUs			
				    //1. test if SKU applies for automatic void
				    //boolean isAvoidableSKU = TopUpVoidProcessor.isSKUAutomaticVoidable(sku); //moved above
				    String strAut_void_response = "";
				    //2. make a call to webserver.asp to perform the void with the carrier directly 
				    if(isAvoidableSKU){
				    	String[] void_response = TopUpVoidProcessor.processAutVoid(application ,
					    															Integer.parseInt(request.getParameter("txtSiteID")),
					    															Integer.parseInt(request.getParameter("txtTransactionNumber")),
					    															clerkID);
				    	if(void_response !=null && void_response.length > 1)
				    	{
				    		strAut_void_response = void_response[1];
				    	}						
				    	if("false".equals(void_response[0])){
				    		isAvoidableSKU = false;			
				    		strAut_void_response = "<font color=ff0000>"+Languages.getString("jsp.admin.transactions.voidtopup_request.failedAutVoid",SessionData.getLanguage())
				    		+"<br>"+Languages.getString("jsp.admin.transactions.voidtopup_request.serverReponseTag",SessionData.getLanguage())+": "+strAut_void_response+"</font>";	    	
				    	}else{
				    		//more friendly message
				    		strAut_void_response = "<font color=009933>"+Languages.getString("jsp.admin.transactions.voidtopup_request.successAutVoid",SessionData.getLanguage())+"</font>";
				    	}
				    }
				    //3.  insert in voidToupRequest with status "processed"
				
				//Else show the final result page
					int nCaseID = TransactionReport.insertVoidTopUpRequest(Integer.parseInt(request.getParameter("ddlVoidReason")), 
																			Integer.parseInt(request.getParameter("txtSiteID")), 
																			Integer.parseInt(request.getParameter("txtControlNumber")), 
																			Integer.parseInt(request.getParameter("txtTransactionNumber")), 
																			request.getParameter("txtComments")
																			, isAvoidableSKU);
					if(isAvoidableSKU) strAut_void_response +=" <a href='admin/reports/transactions/voidtopup_request_summary.jsp?CaseNumber="+nCaseID+"&search=y&startDate=&statusList=&endDate=&merchantList=' target='_blank' style='text-decoration:underline;'>"+nCaseID+"</a>";
%>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.voidtopup_request.caseid",SessionData.getLanguage())%></b></td>
												<td><br><br></td>
												<td class=main><%=nCaseID%></td>
											</tr>
<%
					if(strAut_void_response!=null && strAut_void_response.length()>0){						
																	
						out.print("<tr><td colspan=7 align=center class=main>"+strAut_void_response+"</td></tr>");
					}											
%>											
											<tr><td colspan=7 align=center><br><br><input type=submit value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.return",SessionData.getLanguage())%>"></td></tr>
<%
				}//End of else show the final result page
%>
										</table>
									</form>
								</td>
							</tr>
<%
			}//End of if there are no duplicates
			else
			{//Else show the message that there are duplicates
%>
              <tr><td class=main align=center><br><br><%=Languages.getString("jsp.admin.transactions.voidtopup_request.duplicateresults",SessionData.getLanguage())%> <%=duplicate.get(0)%></td></tr>
              <tr>
                <td align=center>
                  <br><br>
                  <form name="mainform" method="post" action="/support/admin/transactions/voidtopup_request.jsp">
                    <input type=submit value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.return",SessionData.getLanguage())%>">
                  </form>
                </td>
              </tr>
<%
			}//End of else show the message that there are duplicates
		}//End of if there are records for this Transaction
		else
		{//Else show the message of no results
%>
							<tr><td class=main align=center><br><br><%=Languages.getString("jsp.admin.transactions.voidtopup_request.noresults",SessionData.getLanguage())%></td></tr>
							<tr>
								<td align=center>
									<br><br>
									<form name="mainform" method="post" action="/support/admin/transactions/voidtopup_request.jsp">
										<input type=submit value="<%=Languages.getString("jsp.admin.transactions.voidtopup_request.return",SessionData.getLanguage())%>">
									</form>
								</td>
							</tr>
<%
		}//End of else show the message of no results
	 }//End of is trx a valid number
	}//End of else if a request has been made
%>
						</table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr><td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td></tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>
