<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
Vector    vecSearchResults = new Vector();
String 	  strCardProgramIds[] = null;
String 	  strCardProgramIdsList = "";
String 	  strReportType = "";	
if (request.getParameter("search") != null)
{
	try
    {
    	if (request.getParameter("CardProgramList") != null)
    	{
    		strCardProgramIds = request.getParameterValues("CardProgramList");
    		strCardProgramIdsList = CMS.getStringValues(strCardProgramIds);
    	}
    	if (request.getParameter("searchByOption") != null)
    	{
    		strReportType = request.getParameter("searchByOption");
    	}
	
		vecSearchResults = CMS.getCurrentInventory(SessionData, strCardProgramIds, strReportType);
	}
	catch (Exception e){}  
}	    
%>
<SCRIPT LANGUAGE="JavaScript">
	function openViewCards(strCardProgramId, strExternalCompanyID, strCompanyID, strReportType)
	{
		var sURL = "/support/admin/transactions/view_cards_qty_sales.jsp?CardProgramId=" + strCardProgramId + "&ExternalCompanyID=" + strExternalCompanyID + "&CompanyID=" + strCompanyID + "&ReportType=" + strReportType;
	  	var sOptions = "left=" + (screen.width - (screen.width * 0.8))/2 + ",top=" + (screen.height - (screen.height * 0.4))/2 + ",width=" + (screen.width * 0.8) + ",height=" + (screen.height * 0.5) + ",location=no,menubar=no,resizable=yes";
  		var w = window.open(sURL, "ViewTransactions", sOptions, true);
  		w.focus();
	}
</SCRIPT>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="main"><b><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFf">

<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>					
				<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
					<tr>	
                <td align=left>
                  <form target="blank" method=post action="/support/admin/transactions/export_current_inventory.jsp">
                    <input type=hidden name="CardProgramIdsList" value="<%=strCardProgramIdsList%>">
                    <input type=hidden name="ReportType" value="<%=strReportType%>">
                    <input type=submit value="<%=Languages.getString("jsp.admin.reports.export",SessionData.getLanguage())%>">
                  </form>
                </td>
					</tr>
					<tr>
						<td class="main"><br>
				        <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
      	      			<thead>
<%
if (strReportType.equals("0"))
{
%>      	      			
      	      			<tr class="SectionTopBorder">
	      	      			<td class=rowhead2>#</td>
			            	<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.qty_stock",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.qty_assigned",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.qty_sold",SessionData.getLanguage()).toUpperCase()%></td>
            			</tr>
<%
}
else if (strReportType.equals("1"))
{
%>      	      			
      	      			<tr class="SectionTopBorder">
	      	      			<td class=rowhead2>#</td>
			            	<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.next_level_assigned",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.qty_sold",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.qty_stock",SessionData.getLanguage()).toUpperCase()%></td>
            			</tr>
<%             			
}
%> 
		            	</thead>
		            	<%
                       		int intCounter = 1;
		               		Iterator it = vecSearchResults.iterator();
		               		int intEvenOdd = 1;
		               		while (it.hasNext())
		               		{
		            	   		Vector vecTemp = null;
                    	   		vecTemp = (Vector) it.next();
                    	   		String strLink = "";
                    	   		if (strReportType.equals("0"))
                    	   		{
                    	   			if(!vecTemp.get(3).toString().equals("0"))
	                    	   		{
    	                	   			strLink = "<a href=\"javascript:void(0);\" onclick=\"openViewCards(" + vecTemp.get(4).toString() + "," + vecTemp.get(5).toString() + "," + vecTemp.get(6).toString() + "," + strReportType +");\"><span class=\"showLink\"><span class=\"showLink\">" + vecTemp.get(3).toString() + "</span></span></a>";
        	            	   		}
            	        	   		else
                	    	   		{
                    		   			strLink = vecTemp.get(3).toString();
                    		   		}
                           			out.println("<tr class=row" + intEvenOdd +">" +
                                			"<td>" + intCounter++ + "</td>" +
                                			"<td>" + vecTemp.get(0)+ "</td>" +
                                			"<td>" + vecTemp.get(1) + "</td>" +
	                                		"<td>" + vecTemp.get(2) + "</td>" +
    	                            		"<td>" + strLink + "</td>" +
        	                       			"</tr>");
                    	   		}
                    	   		else if (strReportType.equals("1"))
                    	   		{
                    	   			if(!vecTemp.get(2).toString().equals("0"))
	                    	   		{
    	                	   			strLink = "<a href=\"javascript:void(0);\" onclick=\"openViewCards(" + vecTemp.get(4).toString() + "," + vecTemp.get(5).toString() + "," + vecTemp.get(6).toString() + "," + strReportType +");\"><span class=\"showLink\"><span class=\"showLink\">" + vecTemp.get(2).toString() + "</span></span></a>";
        	            	   		}
            	        	   		else
                	    	   		{
                    		   			strLink = vecTemp.get(2).toString();
                    		   		}
                           			out.println("<tr class=row" + intEvenOdd +">" +
                                			"<td>" + intCounter++ + "</td>" +
                                			"<td>" + vecTemp.get(0)+ "</td>" +
                                			"<td>" + vecTemp.get(1) + "</td>" +
	                                		"<td>" + strLink + "</td>" +
    	                            		"<td>" + vecTemp.get(3) + "</td>" +
        	                       			"</tr>");                    	   			
                    	   		}
                           		if (intEvenOdd == 1)
                           		{
                      	      		intEvenOdd = 2;
                    	   		}	
                           		else
                          		{
                             		intEvenOdd = 1;
                          		}
	                  		}
                  	  		vecSearchResults.clear();
                 		%>         			  						
                        </table>
						<br>
						<br>
<%
if (strReportType.equals("0"))
{
%>					
						<SCRIPT type="text/javascript">
  							var stT1 = new SortROC(document.getElementById("t1"),
  							["None", "CaseInsensitiveString", "Number", "Number", "Number"],0,false,false);
						</SCRIPT>	
<%
}
else if (strReportType.equals("1"))
{
%> 											
						<SCRIPT type="text/javascript">
  							var stT1 = new SortROC(document.getElementById("t1"),
  							["None", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "Number"],0,false,false);
						</SCRIPT>	
<%             			
}
%> 						
						</td>
					</td>    	    	        	            
        	        </tr>							
<%
}
else
{
 out.println("<tr><td bgcolor=#EFF9FE class=main><br><br><font color=ff0000>"  + Languages.getString("jsp.admin.transactions.manageinventory.not_found",SessionData.getLanguage()) + "</font><br><br></td></tr>");
}
%>						
        	        <tr>
	        	        <td bgcolor=#EFF9FE class=main><a href="admin/transactions/inventory_management.jsp"><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.continue",SessionData.getLanguage())%></a><br><br></td>
        	        </tr>	
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>
