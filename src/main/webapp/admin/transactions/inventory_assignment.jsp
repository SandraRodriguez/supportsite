<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%
	int section = 9;
	int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
	String strStepAction = "STEP0";
	String strCardProgramId = "";
	String strCardProgramName = "";
	String strOriginCompanyRoleId = "";
	String strOriginCompanyId = "";
	String strOriginCompanyName = "";
	String strDestinationCompanyRoleId = "";
	String strDestinationCompanyId = "";
	String strDestinationCompanyName = "";
	String strAllocateActionId = "";
	String strAllocateActionName = "";
	String strFromRange = "";
	String strToRange = "";
	String strError  = "";
	String strCardsIds = "";	
	Vector vecResult = null;
	boolean blnAssing = false;
	if (request.getParameter("submitted") != null)
	{
		try
	    {
		    if (request.getParameter("submitted").equals("y"))
    		{
		    	if (request.getParameter("stepAction") != null)
		    	{
			    	strStepAction = request.getParameter("stepAction");
			    	if (request.getParameter("CardProgramId") != null)
			    	{
			    		strCardProgramId = request.getParameter("CardProgramId");
			    	}
			    	if (request.getParameter("CardProgramName") != null)
			    	{
			    		strCardProgramName = request.getParameter("CardProgramName");
			    	}
			    	if (request.getParameter("OriginCompanyRoleId") != null)
			    	{
			    		strOriginCompanyRoleId = request.getParameter("OriginCompanyRoleId");
			    	}
			    	if (request.getParameter("OriginCompanyId") != null)
			    	{
			    		strOriginCompanyId = request.getParameter("OriginCompanyId");
			    	}
			    	if (request.getParameter("OriginCompanyName") != null)
			    	{
			    		strOriginCompanyName = request.getParameter("OriginCompanyName");
			    	}
			    	if (request.getParameter("DestinationCompanyRoleId") != null)
			    	{
			    		strDestinationCompanyRoleId = request.getParameter("DestinationCompanyRoleId");
			    	}
			    	if (request.getParameter("DestinationCompanyId") != null)
			    	{
			    		strDestinationCompanyId = request.getParameter("DestinationCompanyId");
			    	}
			    	if (request.getParameter("DestinationCompanyName") != null)
			    	{
			    		strDestinationCompanyName = request.getParameter("DestinationCompanyName");
			    	}
			    	if (request.getParameter("AllocateActionId") != null)
			    	{
			    		strAllocateActionId = request.getParameter("AllocateActionId");
			    	}
			    	if (request.getParameter("AllocateActionName") != null)
			    	{
			    		strAllocateActionName = request.getParameter("AllocateActionName");			    	
			    	}
			    	if (request.getParameter("FromRange") != null)
			    	{
			    		strFromRange = request.getParameter("FromRange");			    	
			    	}	
			    	if (request.getParameter("ToRange") != null)
			    	{
			    		strToRange = request.getParameter("ToRange");			    	
			    	}	
			    	if (request.getParameter("CardsIds") != null)
			    	{
			    		strCardsIds = request.getParameter("CardsIds");			    	
			    	}				    	
			    	
			    	if (strStepAction.equals("STEP1"))
			    	{
			    		if(strCardProgramId.equals("-1"))
			    		{
			    			strStepAction = "STEP0";
			    			strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error",SessionData.getLanguage());
			    		}
			    	}
			    	if (strStepAction.equals("STEP2"))
			    	{
			    		if((strOriginCompanyRoleId.equals("-1"))||(strOriginCompanyId.equals("-1")))
			    		{
			    			strStepAction = "STEP1";
			    			strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error",SessionData.getLanguage());
			    		}
			    	}	
			    	if (strStepAction.equals("STEP3"))
			    	{
			    		if((strDestinationCompanyRoleId.equals("-1"))||(strDestinationCompanyId.equals("-1")))
			    		{
			    			strStepAction = "STEP2";
			    			strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error",SessionData.getLanguage());
			    		}
			    		else if(strDestinationCompanyId.equals(strOriginCompanyId))
			    		{
			    			strStepAction = "STEP2";
			    			strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error2",SessionData.getLanguage());
			    		}

			    	}			    	
			    	if (strStepAction.equals("STEP4"))
			    	{
			    		if(strAllocateActionId.equals("-1"))
			    		{
			    			strStepAction = "STEP3";
			    			strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error",SessionData.getLanguage());
			    		}

			    	}	
			    	if (strStepAction.equals("STEP5"))
			    	{
			    		//double dblFromRange = 0; 
			    		//double dblToRange = 0; 
			    		//boolean blnError = false;
			    		//try
			    		//{
			    			//dblFromRange = Double.parseDouble(strFromRange);
			    			//dblToRange = Double.parseDouble(strToRange);
			    		//}
			    		//catch (Exception e)
			    		//{
			    			//strStepAction = "STEP4";
			    			//strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error3",SessionData.getLanguage());			    			
			    			//blnError = true;
			    		//} 
						//if(!blnError)
						//{
				    		//if(dblFromRange > dblToRange)
							//{
				    			//strStepAction = "STEP4";
				    			//strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error4",SessionData.getLanguage());			    									
							//}
						//}
				    	if ((strFromRange.equals("")) || (strToRange.equals("")))
						{
				    		strStepAction = "STEP4";
				    		strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error6",SessionData.getLanguage());			    									
						}						
			    	}	
			    	if (strStepAction.equals("STEP6"))
			    	{
						Vector    vecAsingResults = new Vector();
						vecAsingResults = CMS.getAvailableItems(strCardProgramId, strOriginCompanyId,SessionData);
						if (vecAsingResults != null && vecAsingResults.size() > 0)
						{	
							Iterator it = vecAsingResults.iterator();
		               		while (it.hasNext())
		               		{
		               			Vector vecTemp = null;
		               			vecTemp = (Vector) it.next();
		               			if (request.getParameter("chk_"+ vecTemp.get(2)) != null)
		               			{
		               				strCardsIds+= vecTemp.get(2) + ",";
		               			}
		               		}
		               		strCardsIds = strCardsIds.substring(0,(strCardsIds.length() - 1));
		               		if(strCardsIds.equals(""))
		               		{
		               			strStepAction = "STEP4";
		               			strError = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error",SessionData.getLanguage());			    									
		               		}
						}
			    	}			    	
			    	if (strStepAction.equals("STEP7"))
			    	{
			    		String strExecutedBy = SessionData.getProperty("username");
						if ( (strAllocateActionId.equals("1"))||(strAllocateActionId.equals("2"))||(strAllocateActionId.equals("3"))||(strAllocateActionId.equals("5")))
						{
							vecResult = CMS.assignInventory(strAllocateActionId, strExecutedBy, strCardProgramId, strOriginCompanyId, strDestinationCompanyId);
						}
						if (strAllocateActionId.equals("4"))
						{
							vecResult = CMS.assignRange(strAllocateActionId, strExecutedBy, strCardProgramId, strOriginCompanyId, strDestinationCompanyId, strFromRange, strToRange);
						}
						if (strAllocateActionId.equals("6"))
						{
							vecResult = CMS.assignSelected(strAllocateActionId, strExecutedBy, strCardsIds, strCardProgramId, strOriginCompanyId, strDestinationCompanyId);
						}					
			    	}			    	
		    	}
		    }
  		}
		catch (Exception e){}  
	}
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="main"><b><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%" 	align="center">
					<tr>
						<td width="18">&nbsp;</td>
						<td class="main">
							<br>
							<br>
    	    	          	<form name=frmViewItems target="blank" method=post action="/support/admin/transactions/view_available_items.jsp">
								<INPUT TYPE="hidden" ID="ViewCardProgramId" NAME="ViewCardProgramId" VALUE="<%=strCardProgramId%>">							
								<INPUT TYPE="hidden" ID="ViewOriginCompanyId" NAME="ViewOriginCompanyId" VALUE="">
    						</form> 							
							<form name="frmMain" method="post" action="/support/admin/transactions/inventory_assignment.jsp">
   	        	            <input type="hidden" name="submitted" value="y">							
							<INPUT TYPE="hidden" ID="stepAction" NAME="stepAction" VALUE="<%=strStepAction%>">
							<INPUT TYPE="hidden" ID="CardProgramId" NAME="CardProgramId" VALUE="<%=strCardProgramId%>">							
							<INPUT TYPE="hidden" ID="CardProgramName" NAME="CardProgramName" VALUE="<%=strCardProgramName%>">							
							<INPUT TYPE="hidden" ID="OriginCompanyRoleId" NAME="OriginCompanyRoleId" VALUE="<%=strOriginCompanyRoleId%>">
							<INPUT TYPE="hidden" ID="OriginCompanyId" NAME="OriginCompanyId" VALUE="<%=strOriginCompanyId%>">							
							<INPUT TYPE="hidden" ID="OriginCompanyName" NAME="OriginCompanyName" VALUE="<%=strOriginCompanyName%>">							
							<INPUT TYPE="hidden" ID="DestinationCompanyRoleId" NAME="DestinationCompanyRoleId" VALUE="<%=strDestinationCompanyRoleId%>">
							<INPUT TYPE="hidden" ID="DestinationCompanyId" NAME="DestinationCompanyId" VALUE="<%=strDestinationCompanyId%>">							
							<INPUT TYPE="hidden" ID="DestinationCompanyName" NAME="DestinationCompanyName" VALUE="<%=strDestinationCompanyName%>">							
							<INPUT TYPE="hidden" ID="AllocateActionId" NAME="AllocateActionId" VALUE="<%=strAllocateActionId%>">																					
							<INPUT TYPE="hidden" ID="AllocateActionName" NAME="AllocateActionName" VALUE="<%=strAllocateActionName%>">																					
							<INPUT TYPE="hidden" ID="FromRange" NAME="FromRange" VALUE="<%=strFromRange%>">																					
							<INPUT TYPE="hidden" ID="ToRange" NAME="ToRange" VALUE="<%=strToRange%>">
							<INPUT TYPE="hidden" ID="CardsIds" NAME="CardsIds" VALUE="<%=strCardsIds%>">
								<table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
								<%
									if (strStepAction.equals("STEP0"))
									{
								%>
										<tr class="main">
											<td colspan="3">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.welcome_instructions",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
	                        		    <tr class="main">
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="cardProgramList">
            	    	                    	    <%
	            		                                Vector vecCardProgramList = CMS.getCardsProgramList(SessionData);
    	    	        	                            Iterator itCPL = vecCardProgramList.iterator();
            	    	                    	    	String strSelectedCPL = "";
            	    	                    	    	out.println("<option value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_one",SessionData.getLanguage()) + "</option>");
    		                	                        while (itCPL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempCPL = null;
                	                	                	vecTempCPL = (Vector) itCPL.next();
                	                	                	if(strCardProgramId != "")
                	    	                    	    	{
               	                	                			if(strCardProgramId.equals(vecTempCPL.get(0)))
                   	    	                    	    		{
                	                	                			strSelectedCPL = "selected";
   	                	    	                    	    	}
       	        	                	                		else
           	    	                	                		{
              		                	                			strSelectedCPL = "";
               		                	                		}
                	    	                    	    	}  
	        	                	                		else
    	    	                	                		{
        		                	                			strSelectedCPL = "";
        		                	                		}                	                	                	
                        	                		    	out.println("<option " + strSelectedCPL + " value=" + vecTempCPL.get(0) +">" + vecTempCPL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td>
        	    	        	            <td>
            	    		                	<input id="btnNext0" type="button" name="btnNext0" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.next",SessionData.getLanguage())%>" onclick="goStep1();">
            	    		                	<SCRIPT>
            	    		                		function goStep1()
                                                    {
                                                     	document.getElementById('CardProgramId').value = document.getElementById('cardProgramList').value ;
	                                                    document.getElementById('CardProgramName').value = document.getElementById('cardProgramList').options[document.getElementById('cardProgramList').selectedIndex].text;
                                                    	document.getElementById('stepAction').value = "STEP1";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>
        	        	    	       </tr>	
								<%
									}
								%>  
								<%
									if (strStepAction.equals("STEP1"))
									{
								%>
										<tr class="main">
											<td colspan="6">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.step1_instructions",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
	                        		    <tr class="main">
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_type",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="originPlayerTypeList" onchange="getCompanies();">
            	    	                    	    <%
	            		                                Vector vecOriginPlayerTypeList = CMS.getCompanyRolesList(SessionData);
    	    	        	                            Iterator itOPTL = vecOriginPlayerTypeList.iterator();
            	    	                    	    	String strSelectedOPTL = "";
            	    	                    	    	out.println("<option value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_one",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itOPTL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempOPTL = null;
                	                	                	vecTempOPTL = (Vector) itOPTL.next();
                	                	                	if(strOriginCompanyRoleId != "")
                	    	                    	    	{
                	                	                		if(strOriginCompanyRoleId.equals(vecTempOPTL.get(0)))
   	                	    	                    	    	{
       	        	                	                			strSelectedOPTL = "selected";
           	        	    	                    	    	}
               		                	                		else
               		                	                		{
               	    	            	                			strSelectedOPTL = "";
               	        	        	                		}
                	    	                    	    	} 
        		                	                		else
        		                	                		{
        	    	            	                			strSelectedOPTL = "";
        	        	        	                		}
                        	                		    	out.println("<option " + strSelectedOPTL + " value=" + vecTempOPTL.get(0) +">" + vecTempOPTL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
            	    		                	<SCRIPT>
            	    		                		function getCompanies()
                                                    {
	                                                    document.getElementById('OriginCompanyRoleId').value = document.getElementById('originPlayerTypeList').value ;
                                                    	document.getElementById('stepAction').value = "STEP1";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>	    	                        	        
    	    	                	        </td>
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_name",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="originPlayerList" id="originPlayerList">
            	    	                    	    <%
	            	    	                    	    SessionData.setProperty("CardProgramId",strCardProgramId); 
            	    	                    	    	SessionData.setProperty("OriginCompanyRoleId",strOriginCompanyRoleId); 	
            	    	                    	    	Vector vecOriginPlayerList = CMS.getCompaniesList(SessionData, true);
    	    	        	                            Iterator itOPL = vecOriginPlayerList.iterator();
            	    	                    	    	String strSelectedOPL = "";
            	    	                    	    	out.println("<option value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_one",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itOPL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempOPL = null;
                	                	                	vecTempOPL = (Vector) itOPL.next();
                	                	                	if(strOriginCompanyId != "")
                	    	                    	    	{
                	                	                		if(strOriginCompanyId.equals(vecTempOPL.get(0)))
   	                	    	                    	    	{
                	                	                			strSelectedOPL = "selected";
           	        	    	                    	    	}
               		                	                		else
               		                	                		{
               		                	                			strSelectedOPL = "";
               	        	        	                		}
                	    	                    	    	} 
        		                	                		else
        		                	                		{
        		                	                			strSelectedOPL = "";
        	        	        	                		}                	                	                	
                        	                		    	out.println("<option " + strSelectedOPL + " value=" + vecTempOPL.get(0) +">" + vecTempOPL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td>    	    	                	        
        	    	        	            <td>
            	    		                	<input id="btnBack1" type="button" name="btnBack1" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.back",SessionData.getLanguage())%>" onclick="goStep0();">
            	    		                	<SCRIPT>
            	    		                		function goStep0()
                                                    {
	                                                    document.getElementById('OriginCompanyRoleId').value = document.getElementById('originPlayerTypeList').value ;
	                                                    document.getElementById('OriginCompanyId').value = document.getElementById('originPlayerList').value ;
	                                                    document.getElementById('OriginCompanyName').value = document.getElementById('originPlayerList').options[document.getElementById('originPlayerList').selectedIndex].text;
                                                    	document.getElementById('stepAction').value = "STEP0";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>
        	    	        	            <td>
            	    		                	<input id="btnNext1" type="button" name="btnNext1" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.next",SessionData.getLanguage())%>" onclick="goStep2();">
            	    		                	<SCRIPT>
            	    		                		function goStep2()
                                                    {
	                                                    document.getElementById('OriginCompanyRoleId').value = document.getElementById('originPlayerTypeList').value ;
	                                                    document.getElementById('OriginCompanyId').value = document.getElementById('originPlayerList').value ;
	                                                    document.getElementById('OriginCompanyName').value = document.getElementById('originPlayerList').options[document.getElementById('originPlayerList').selectedIndex].text;
                                                    	document.getElementById('stepAction').value = "STEP2";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td> 
        	        	    	       </tr>
        	        	    	       <tr>	
            			                    <td  colspan="6">
				                    			<input id="btnViewItems" type="button" name="btnViewItems"  value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.view_available_items",SessionData.getLanguage())%>" onclick="viewAvailableItems();">
				                    			<SCRIPT>
           	    		                			function viewAvailableItems()
	                                                {
	   	                                                document.getElementById('ViewOriginCompanyId').value = document.getElementById('originPlayerList').value;
	   	                                                document.frmViewItems.submit();
                       	                            }
                                               	</SCRIPT>
	           			                    </td>          			                            	        	    	       
        	        	    	       </tr>
								<%
									}
								%> 
								<%
									if (strStepAction.equals("STEP2"))
									{
								%>
										<tr class="main">
											<td colspan="6">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.step2_instructions",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
	                        		    <tr class="main">
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_type",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="destinationPlayerTypeList" onchange="getDCompanies();">
            	    	                    	    <%
	            		                                Vector vecDestinationPlayerTypeList = CMS.getCompanyRolesList(SessionData);
    	    	        	                            Iterator itDPTL = vecDestinationPlayerTypeList.iterator();
            	    	                    	    	String strSelectedDPTL = "";
            	    	                    	    	out.println("<option value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_one",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itDPTL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempDPTL = null;
                	                	                	vecTempDPTL = (Vector) itDPTL.next();
                	                	                	if(strDestinationCompanyRoleId != "")
                	    	                    	    	{
               	        	        	                		if(strDestinationCompanyRoleId.equals(vecTempDPTL.get(0)))
                   		    	                    	    	{
               		                	                			strSelectedDPTL = "selected";
               	    	    	                    	    	}
           	    	                	                		else
       	        	                	                		{
   	            	                	                			strSelectedDPTL = "";
                	                	                		}
                	    	                    	    	} 
    	    	                	                		else
	        	                	                		{
            	                	                			strSelectedDPTL = "";
            	                	                		}                	                	                	
                        	                		    	out.println("<option " + strSelectedDPTL + " value=" + vecTempDPTL.get(0) +">" + vecTempDPTL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
            	    		                	<SCRIPT>
            	    		                		function getDCompanies()
                                                    {
                                                    	document.getElementById('DestinationCompanyRoleId').value = document.getElementById('destinationPlayerTypeList').value ;
                                                    	document.getElementById('stepAction').value = "STEP2";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>	    	                        	        
    	    	                	        </td>
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_name",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="destinationPlayerList">
            	    	                    	    <%
    	        	    	                    	    SessionData.setProperty("CardProgramId",strCardProgramId); 
            	    	            					SessionData.setProperty("DestinationCompanyRoleId",strDestinationCompanyRoleId);            	    	                    	    		
            	    	                    	    	Vector vecDestinationPlayerList = CMS.getCompaniesList(SessionData, false);
    	    	        	                            Iterator itDPL = vecDestinationPlayerList.iterator();
            	    	                    	    	String strSelectedDPL = "";
            	    	                    	    	out.println("<option value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_one",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itDPL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempDPL = null;
                	                	                	vecTempDPL = (Vector) itDPL.next();
                	                	                	if(strDestinationCompanyId != "")
                	    	                    	    	{
               	        	        	                		if(strDestinationCompanyId.equals(vecTempDPL.get(0)))
                   		    	                    	    	{
               	        	        	                			strSelectedDPL = "selected";
               	    	    	                    	    	}
           	    	                	                		else
       	        	                	                		{
           	    	                	                			strSelectedDPL = "";
                	                	                		}
                	    	                    	    	} 
    	    	                	                		else
	        	                	                		{
    	    	                	                			strSelectedDPL = "";
            	                	                		}                	                	                	
                        	                		    	out.println("<option " + strSelectedDPL + " value=" + vecTempDPL.get(0) +">" + vecTempDPL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td>    	    	                	        
        	    	        	            <td>
            	    		                	<input id="btnBack2" type="button" name="btnBack2" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.back",SessionData.getLanguage())%>" onclick="goStep11();">
            	    		                	<SCRIPT>
            	    		                		function goStep11()
                                                    {
	                                                    document.getElementById('DestinationCompanyRoleId').value = document.getElementById('destinationPlayerTypeList').value ;
                                            	        document.getElementById('DestinationCompanyId').value = document.getElementById('destinationPlayerList').value ;
                                                    	document.getElementById('DestinationCompanyName').value = document.getElementById('destinationPlayerList').options[document.getElementById('destinationPlayerList').selectedIndex].text;
                                                    	document.getElementById('stepAction').value = "STEP1";
                                                        document.frmMain.submit();
                                                    }
                                                </SCRIPT>
            			                    </td>
        	    	        	            <td>
            	    		                	<input id="btnNext2" type="button" name="btnNext2" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.next",SessionData.getLanguage())%>" onclick="goStep3();">
            	    		                	<SCRIPT>
            	    		                		function goStep3()
                                                    {
	                                                    document.getElementById('DestinationCompanyRoleId').value = document.getElementById('destinationPlayerTypeList').value ;
                                            	        document.getElementById('DestinationCompanyId').value = document.getElementById('destinationPlayerList').value ;
                                                    	document.getElementById('DestinationCompanyName').value = document.getElementById('destinationPlayerList').options[document.getElementById('destinationPlayerList').selectedIndex].text;
                                                    	document.getElementById('stepAction').value = "STEP3";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>            			                    
        	        	    	       </tr>	
								<%
									}
								%>	
								<%
									if (strStepAction.equals("STEP3"))
									{
								%>
										<tr class="main">
											<td colspan="6">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.step3_instructions",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
	                        		    <tr class="main">
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.action",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="actionList">
            	    	                    	    <%
	            		                                Vector vecActionList = CMS.getActionList(SessionData);
    	    	        	                            Iterator itAL = vecActionList.iterator();
            	    	                    	    	String strSelectedAL = "";
            	    	                    	    	out.println("<option " + "" + " value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_one",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itAL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempAL = null;
                	                	                	vecTempAL = (Vector) itAL.next();
                	                	                	if(strAllocateActionId != "")
                	    	                    	    	{
               	        	        	                		if(strAllocateActionId.equals(vecTempAL.get(0)))
                   		    	                    	    	{
               	        	        	                			strSelectedAL = "selected";
               	    	    	                    	    	}
           	    	                	                		else
       	        	                	                		{
           	    	                	                			strSelectedAL = "";
                	                	                		}
                	    	                    	    	} 
    	    	                	                		else
	        	                	                		{
    	    	                	                			strSelectedAL = "";
            	                	                		}                	                	                	
                        	                		    	out.println("<option " + strSelectedAL + " value=" + vecTempAL.get(0) +">" + vecTempAL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td>
        	    	        	            <td>
            	    		                	<input id="btnBack3" type="button" name="btnBack3" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.back",SessionData.getLanguage())%>" onclick="goStep22();">
            	    		                	<SCRIPT>
            	    		                		function goStep22()
                                                    {
	                                                    document.getElementById('AllocateActionId').value = document.getElementById('actionList').value ;
                                                    	document.getElementById('AllocateActionName').value = document.getElementById('actionList').options[document.getElementById('actionList').selectedIndex].text;
                                                    
                                                    	document.getElementById('stepAction').value = "STEP2";
                                                        document.frmMain.submit();
                                                    }
                                                </SCRIPT>
            			                    </td>
        	    	        	            <td>
            	    		                	<input id="btnNext3" type="button" name="btnNext3" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.next",SessionData.getLanguage())%>" onclick="goStep4();">
            	    		                	<SCRIPT>
            	    		                		function goStep4()
                                                    {
	                                                    document.getElementById('AllocateActionId').value = document.getElementById('actionList').value ;
                                                    	document.getElementById('AllocateActionName').value = document.getElementById('actionList').options[document.getElementById('actionList').selectedIndex].text;
                                                    	document.getElementById('stepAction').value = "STEP4";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>            			                    
        	        	    	       </tr>	
								<%
									}
								%>		
								<%
									if (strStepAction.equals("STEP4"))
									{
										if ( (strAllocateActionId.equals("1"))||(strAllocateActionId.equals("2"))||(strAllocateActionId.equals("3"))||(strAllocateActionId.equals("5")))
										{
								%>
										<tr class="main">
											<td colspan="6">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.step4_instructions",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
	                        		    <tr class="main">
    	                    	    	    <td class=main valign=top align="center">
												<TABLE cellSpacing="1" cellPadding="1" border="1" class="main">
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.action",SessionData.getLanguage()) %></b></TD>
														<TD><%= strAllocateActionName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()) %></b></TD>
														<TD><%= strCardProgramName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.origin",SessionData.getLanguage()) %></b></TD>
														<TD><%= strOriginCompanyName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.destination",SessionData.getLanguage()) %></b></TD>
														<TD><%= strDestinationCompanyName %></TD>
													</TR>
												</TABLE>
    	    	                	        </td>
        	    	        	            <td>
            	    		                	<input id="btnBack4" type="button" name="btnBack4" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.back",SessionData.getLanguage())%>" onclick="goStep33();">
            	    		                	<SCRIPT>
            	    		                		function goStep33()
                                                    {
                                                    	document.getElementById('stepAction').value = "STEP3";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>
        	    	        	            <td>
            	    		                	<input id="btnAssign" type="button" name="btnAssign" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.assign",SessionData.getLanguage())%>" onclick="goAssing();">
            	    		                	<SCRIPT>
            	    		                		function goAssing()
                                                    {
	                                                   	document.getElementById('stepAction').value = "STEP7";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>            			                    
        	        	    	       </tr>	
								<%
										}
										else if(strAllocateActionId.equals("4"))
										{
								%>	
										<tr class="main">
											<td colspan="6">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.step3A_instructions",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
										<TR class="main">
											<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.from",SessionData.getLanguage()) %></b></TD>
											<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.to",SessionData.getLanguage()) %></b></TD>
										</TR>
										<TR class="main">
											<TD><input type="text" name="txtFromRange" value="<%=strFromRange%>"></TD>
											<TD><input type="text" name="txtToRange" value="<%=strToRange%>"></TD>
        	    	        	            <td>
            	    		                	<input id="btnBack5" type="button" name="btnBack5" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.back",SessionData.getLanguage())%>" onclick="goStep33();">
            	    		                	<SCRIPT>
            	    		                		function goStep33()
                                                    {
                                                    	document.getElementById('FromRange').value = document.getElementById('txtFromRange').value ;
	                                                    document.getElementById('ToRange').value = document.getElementById('txtToRange').value ;
                                                    	document.getElementById('stepAction').value = "STEP3";
                                                        document.frmMain.submit();
                                                    }
                                                </SCRIPT>
            			                    </td>
        	    	        	            <td>
            	    		                	<input id="btnNext5" type="button" name="btnNext5" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.next",SessionData.getLanguage())%>" onclick="goStep5();">
            	    		                	<SCRIPT>
            	    		                		function goStep5()
                                                    {
                                                    	document.getElementById('FromRange').value = document.getElementById('txtFromRange').value ;
	                                                    document.getElementById('ToRange').value = document.getElementById('txtToRange').value ;
                                                    	document.getElementById('stepAction').value = "STEP5";
                                                        document.frmMain.submit();
                                                    }
                                                </SCRIPT>
            			                    </td>            			                    
        	        	    	       </tr>																																							      	        	    	       						        	        	    	       
								<%
										}
										else if(strAllocateActionId.equals("6"))
										{
								%>	
										<tr class="main">
											<td colspan="6" nowrap="nowrap" align="center">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.cards_found",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
										<tr class="main">
											<td>
												<%
													Vector    vecSearchResults = new Vector();
													vecSearchResults = CMS.getAvailableItems(strCardProgramId, strOriginCompanyId,SessionData);
													if (vecSearchResults != null && vecSearchResults.size() > 0)
													{
												%>						
												<td class="main"><br>
				        						<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
      	      									<thead>
      	      									<tr class="SectionTopBorder">
							      	      			<td class=rowhead2>#</td>
							      	      			<td class=rowhead2></td>
									            	<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_type",SessionData.getLanguage()).toUpperCase()%></td>
						              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_name",SessionData.getLanguage()).toUpperCase()%></td>
						              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.serial_number",SessionData.getLanguage()).toUpperCase()%></td>
						              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()).toUpperCase()%></td>
						              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.expiration_date",SessionData.getLanguage()).toUpperCase()%></td>
						              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.status",SessionData.getLanguage()).toUpperCase()%></td>
						              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program_type",SessionData.getLanguage()).toUpperCase()%></td>
						            			</tr>
								            	</thead>
								            	<%
													int intCounter = 1;
													Iterator it = vecSearchResults.iterator();
								               		int intEvenOdd = 1;
								               		while (it.hasNext())
									               		{
									               			Vector vecTemp = null;
									               			vecTemp = (Vector) it.next();
									               			out.println("<tr class=row" + intEvenOdd +">" +
									    	               		"<td>" + intCounter++ + "</td>" +
									    	               		"<td><input type=checkbox class='plain' name='chk_"+ vecTemp.get(2) +"' id='chk_"+ vecTemp.get(2) +"'></td>" +
									    	               		"<td>" + vecTemp.get(0)+ "</td>" +
									    	               		"<td>" + vecTemp.get(1) + "</td>" +
									    	               		"<td>" + vecTemp.get(2) + "</td>" +
									    	               		"<td>" + vecTemp.get(3) + "</td>" +
									    	               		"<td>" + vecTemp.get(4) + "</td>" +
									    	               		"<td>" + vecTemp.get(5) + "</td>" +
									    	               		"<td>" + vecTemp.get(6) + "</td>" +
									    	               		"</tr>");
									               		if (intEvenOdd == 1)
										               		{
										               		intEvenOdd = 2;
										               		}	
									               		else
										               		{
										               		intEvenOdd = 1;
										               		}
									               		}
								               		vecSearchResults.clear();
								               		blnAssing = true;
                 			               		%>  
                        	               		</table>
							               		<br>
							               		<br>
							               		<SCRIPT type="text/javascript">
													<!--
				  									var stT1 = new SortROC(document.getElementById("t1"),
  													["CaseInsensitiveString", "None", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Date", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
		  											-->
												</SCRIPT>	
							               		</td>
	               			               		<%
	               			               			}
								               		else
									               	{
									               		out.println("<br><br><font color=ff0000>"  + Languages.getString("jsp.admin.transactions.manageinventory.not_found",SessionData.getLanguage()) + "</font><br><br>");
									               		blnAssing = false;
										            }
	               			               		%>													
											</td>
										</tr>
	                        		    <tr class="main">
        	    	        	            <td>
            	    		                	<input id="btnBack6" type="button" name="btnBack6" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.back",SessionData.getLanguage())%>" onclick="goStep333();">
            	    		                	<SCRIPT>
            	    		                		function goStep333()
                                                    {
                                                    	document.getElementById('stepAction').value = "STEP3";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>
            			                    <% 
												if (blnAssing)
												{
											%>            			                    
        	    	        	            <td>
            	    		                	<input id="btnNext6" type="button" name="btnNext6" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.next",SessionData.getLanguage())%>" onclick="goStep6();">
            	    		                	<SCRIPT>
            	    		                		function goStep6()
                                                    {
                                                    	document.getElementById('stepAction').value = "STEP6";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td> 
            			                    <% 
												}											
											%> 
        	        	    	       </tr>																																							      	        	    	       						        	        	    	       								
								<%
										}
									}
								%>
								<%
									if (strStepAction.equals("STEP5"))
									{
								%>
										<tr class="main">
											<td colspan="6">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.step4_instructions",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
	                        		    <tr class="main">
    	                    	    	    <td class=main valign=top align="center">
												<TABLE cellSpacing="1" cellPadding="1" border="1" class="main">
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.action",SessionData.getLanguage()) %></b></TD>
														<TD><%= strAllocateActionName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()) %></b></TD>
														<TD><%= strCardProgramName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.origin",SessionData.getLanguage()) %></b></TD>
														<TD><%= strOriginCompanyName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.destination",SessionData.getLanguage()) %></b></TD>
														<TD><%= strDestinationCompanyName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.from",SessionData.getLanguage()) %></b></TD>
														<TD><%= strFromRange %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.to",SessionData.getLanguage()) %></b></TD>
														<TD><%= strToRange %></TD>
													</TR>																										
												</TABLE>
    	    	                	        </td>
        	    	        	            <td>
            	    		                	<input id="btnBack7" type="button" name="btnBack7" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.back",SessionData.getLanguage())%>" onclick="goStep3333();">
            	    		                	<SCRIPT>
            	    		                		function goStep3333()
                                                    {
                                                    	document.getElementById('stepAction').value = "STEP3";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>
        	    	        	            <td>
            	    		                	<input id="btnAssign2" type="button" name="btnAssign2" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.assign",SessionData.getLanguage())%>" onclick="goAssing2();">
            	    		                	<SCRIPT>
            	    		                		function goAssing2()
                                                    {
												    	document.getElementById('FromRange').value = <%= strFromRange %>;
	                                                    document.getElementById('ToRange').value = <%= strToRange %>;
                                                    	document.getElementById('stepAction').value = "STEP7";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>            			                    
        	        	    	       </tr>	
								<%
										}
								%>	
								<%
									if (strStepAction.equals("STEP6"))
									{
								%>
										<tr class="main">
											<td colspan="6">
												<b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.step4_instructions",SessionData.getLanguage()) %></b>								
												<br>
												<br>
											</td>
										</tr>
	                        		    <tr class="main">
    	                    	    	    <td class=main valign=top align="center">
												<TABLE cellSpacing="1" cellPadding="1" border="1" class="main">
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.action",SessionData.getLanguage()) %></b></TD>
														<TD><%= strAllocateActionName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()) %></b></TD>
														<TD><%= strCardProgramName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.origin",SessionData.getLanguage()) %></b></TD>
														<TD><%= strOriginCompanyName %></TD>
													</TR>
													<TR>
														<TD><b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.destination",SessionData.getLanguage()) %></b></TD>
														<TD><%= strDestinationCompanyName %></TD>
													</TR>
												</TABLE>
    	    	                	        </td>
        	    	        	            <td>
            	    		                	<input id="btnBack8" type="button" name="btnBack8" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.back",SessionData.getLanguage())%>" onclick="goStep33333();">
            	    		                	<SCRIPT>
            	    		                		function goStep33333()
                                                    {
                                                    	document.getElementById('stepAction').value = "STEP3";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>
        	    	        	            <td>
            	    		                	<input id="btnAssign3" type="button" name="btnAssign3" value="<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.assign",SessionData.getLanguage())%>" onclick="goAssing3();">
            	    		                	<SCRIPT>
            	    		                		function goAssing3()
                                                    {
                                                    	document.getElementById('stepAction').value = "STEP7";
                                                        document.frmMain.submit();
                                                     }
                                                </SCRIPT>
            			                    </td>            			                    
        	        	    	       </tr>	
								<%
										}
								%>	
								<%
									if (strStepAction.equals("STEP7"))
									{
								%>
	                        		    <tr class="main">
	                        		    <%
										if (vecResult != null && vecResult.size() > 0)
										{
											%>						
											<td class="main"><br>
		                        		    <b><%= Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.inventory_assignment_result",SessionData.getLanguage()) %></b><br><br>
			        						<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
  	      									<thead>
  	      									<tr class="SectionTopBorder">
  	      										<td class=rowhead2>#</td>
								            	<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.serial_number",SessionData.getLanguage()).toUpperCase()%></td>
					            			</tr>
							            	</thead>
							            	<%
												int intCounter = 1;
												Iterator it = vecResult.iterator();
							               		int intEvenOdd = 1;
							               		while (it.hasNext())
								               		{
								               			Vector vecTemp = null;
								               			vecTemp = (Vector) it.next();
								               			out.println("<tr class=row" + intEvenOdd +">" +
								    	               		"<td>" + intCounter++ + "</td>" +
								    	               		"<td>" + vecTemp.get(0)+ "</td>" +
								    	               		"</tr>");
								               		if (intEvenOdd == 1)
									               		{
									               		intEvenOdd = 2;
									               		}	
								               		else
									               		{
									               		intEvenOdd = 1;
									               		}
								               		}
							               		vecResult.clear();
							               		blnAssing = true;
             			               		%>  
                    	               		</table>
						               		<br>
						               		<br>
						               		<SCRIPT type="text/javascript">
												<!--
			  									var stT1 = new SortROC(document.getElementById("t1"),
													["CaseInsensitiveString","CaseInsensitiveString"],0,false,false);
	  											-->
											</SCRIPT>	
						               		</td>
               			               		<%											
											}										
											else
											{
												out.println("<td><br><br><font color=ff0000>"  + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.select_error5",SessionData.getLanguage()) + "</font><br><br></td>");
											}
										%> 
									   </tr>	 
									   <tr class="main"> 
										<td><a href="admin/transactions/inventory_management.jsp"><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.continue",SessionData.getLanguage())%></a></td>     	    	        	            
        	        	    	       </tr>	
								<%
										}
								%>	
									<tr class="main">
										<td colspan="6">
										<br><br><font color=ff0000><%=strError%></font><br><br>
										</td>
									</tr>																																																																      	        	    	       						
								</table>
							</form>
							<br>	
							<br>
						</td>
						<td width="18">&nbsp;</td>
					</tr>

				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<%@ include file="/includes/footer.jsp"%>
