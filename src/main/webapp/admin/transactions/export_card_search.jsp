<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
Vector    vecSearchResults = new Vector();
String    sortString       = "";
String 	  strCardProgramID = ""; 
String 	  strSerialNumber  = ""; 
String 	  strCardNumber    = ""; 
String 	  strCardTypeID    = ""; 
String 	  strStatusID      = ""; 
String 	  strCompanyRoleID = ""; 
String 	  strCompanyID     = ""; 
String 	  strCreationDate  = "";

	try
    {
    	if (request.getParameter("PlayerTypeId") != null)
    	{
    		strCompanyRoleID = request.getParameter("PlayerTypeId");
    	}
    	if (request.getParameter("CardProgramNameId") != null)
    	{
    		strCardProgramID = request.getParameter("CardProgramNameId");
    	}
    	if (request.getParameter("CardProgramTypeId") != null)
    	{
    		strCardTypeID = request.getParameter("CardProgramTypeId");
    	}
    	if (request.getParameter("StatusId") != null)
    	{
    		strStatusID = request.getParameter("StatusId");
    	}
    	if (request.getParameter("SerialNumber") != null)
    	{
    		strSerialNumber = request.getParameter("SerialNumber");
    	}
    	if (request.getParameter("CardNumber") != null)
    	{
    		strCardNumber = request.getParameter("CardNumber");
    	}
    	if (request.getParameter("PlayerId") != null)
    	{
    		strCompanyID = request.getParameter("PlayerId");
    	}    	
    	if (request.getParameter("ISOReceiptDate") != null)
    	{
    		strCreationDate = request.getParameter("ISOReceiptDate");
    	}	
		vecSearchResults = CMS.getReportCardSearch(SessionData, strCardProgramID, strSerialNumber, strCardTypeID, strStatusID, strCompanyRoleID, strCompanyID, strCreationDate);
	    String sURL = CMS.downloadCardSearchReport(application, vecSearchResults, 8);
	    response.sendRedirect(sURL);
	    return;		
	}
	catch (Exception e){}  
%>
