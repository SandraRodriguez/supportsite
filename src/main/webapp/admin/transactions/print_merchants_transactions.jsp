<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.languages.*" %>
<%
String strReport = request.getParameter("report");
int section=3;
int section_page=3;

if (strReport !=null && strReport.equals("y"))
{
  section=4;
  section_page=14;
}
else
{
  strReport = "";
}
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;

if (request.getParameter("search") != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  if (TransactionSearch.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));
    vecSearchResults = TransactionSearch.search(intPage, intPageSize, SessionData, section_page, application, (request.getParameter("chkUseTaxValue") != null));
    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize);
      if ((intPageCount * intPageSize) < intRecordCount)
      {
        intPageCount++;
      }
    }
  }
  else
  {
   searchErrors = TransactionSearch.getErrors();
  }


}
%>
<html>
<head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="../../images/top_blue.gif">
  <tr>
	  <td width="18" height="20" align=left><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
	  <td class="formAreaTitle" align=left width="3000"><%=Languages.getString("jsp.admin.reports.transactions.merchants.transactions.title",SessionData.getLanguage()).toUpperCase()%></td>
	  <td width="12" height="20" align=right><img src="../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="merchantform" method="get" action="admin/transactions/merchants_transactions.jsp" onSubmit="scroll();">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
      <%if (strReport == null || strReport.equals(""))
      {%>
     	<tr>
	        <td class="formArea2">
	          <table width="300">
              <tr class="main">
               <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
         <%}else{ %>
         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
         <%}%><td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
<tr class="main">
    <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>: <input class="plain" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.merchantform.startDate,document.merchantform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
    <td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: <input class="plain" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.merchantform.startDate,document.merchantform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
    <td class=main>
      <input type="hidden" name="search" value="y">
      <input type="hidden" name="merchantId" value="<%=TransactionSearch.getMerchantId()%>">
      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.transactions.show_transactions",SessionData.getLanguage())%>">
    </td>
</tr>
<tr>
<td colspan=3 class="main">
* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
</td>
</tr>
<%}%>
</table>
               </td>
              </tr>
              </form>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
              if (!TransactionSearch.getStartDate().equals("") && !TransactionSearch.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(TransactionSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getEndDateFormatted()));
                }
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%></td></tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }
              %>
              </td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
              <td class=rowhead2>#</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
<%
				if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
%>
  			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.phys_state",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
%>                
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.city_county",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.reference",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%></td>              
				<%//DBSY-905
				if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
					&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
				%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%></td>
					  
			        <% if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
			    	  {%>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></td>
					  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></td>
					<%}
				}%>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()).toUpperCase()%></td>
<%
				if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
				{
%>
  			  <td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_number",SessionData.getLanguage()).toUpperCase()%></td>
<%
				}
%>                 
              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_type",SessionData.getLanguage()).toUpperCase()%></td>
              <% if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{%>
					<td class=rowhead2><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage()).toUpperCase()%></td>
					 <%} %>
            </tr>
            <%
                  int intCounter = 1;
                  //intCounter = intCounter - ((intPage-1)*intPageSize);

                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  String type = "";
                  String row = "";
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    // check if type is "Return" of "Adjust", if so, display the whole row in red
                    type = (String)vecTemp.get(14);
                    if (type.equals("Return") || type.equals("Adjustment") || type.equals("Credit")||type.equals("Write Off"))
                    {
                    	row = "rowred";
                    }
                    else
                    {
                    	row = "row";
                    }

                    String sViewPIN = "", sPhysState = "", sAuthorizationNo = "";
                      sViewPIN = vecTemp.get(12).toString();

                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                    {
						sPhysState = "<td nowrap>" + vecTemp.get(16).toString() + "</td>";
                    }
                    else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                    {
						sAuthorizationNo = "<td nowrap>" + vecTemp.get(15).toString() + "</td>";
                    }                    

                    out.println("<tr class=" + row + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString())  + "</td>" +
                                "<td>" + vecTemp.get(3) + "</td>" +
                                "<td nowrap>" + vecTemp.get(4) + "</td>" +
                                sPhysState +                                
                                 //city
                                "<td nowrap>" + vecTemp.get(5) + "</td>" +
                                //clerk
                                "<td nowrap>" + vecTemp.get(6) + "</td>" +
                                "<td>" + vecTemp.get(7) + "</td>" +
                                "<td>" + vecTemp.get(8) + "</td>" +
                                //DBSY-905 Tax Calculation
                    		    //amount
                                "<td nowrap>" + vecTemp.get(9) + "</td>");
			                    if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
			                       //bonus
			                                out.println("<td nowrap>" + vecTemp.get(17) + "</td>" +
			                      //total Recharge
			                                "<td nowrap>" + vecTemp.get(18) + "</td>");
			                        if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
				    	 					{
			                     //net amount 
			                         	       out.println("<td nowrap>" + vecTemp.get(24) + "</td>" +
			                     //tax amount
			                                 "<td nowrap>" + vecTemp.get(25) + "</td>");
			                                }
			                    }
                                //bal
                                out.println("<td nowrap>" + vecTemp.get(10) + "</td>" +
                                "<td nowrap width=200>" + vecTemp.get(11) + "</td>" +
                                "<td>" + sViewPIN + "</td>" +
                                sAuthorizationNo +
                                "<td>" + type + "</td>" );
                                 if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{           
                     //DBSY-919 Invoice Number Field
                     out.println("<td>" + vecTemp.get(23) + "</td>" + "</tr>");
                     //////   
                     }
                    else
                    	 out.println("</tr>");
                     
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>
            <table id="tblClose" style="display:none;width:100%;">
              <tr><td>&nbsp;</td></tr>
              <tr><td align=center><input type=button value="<%=Languages.getString("jsp.admin.reports.closewindow",SessionData.getLanguage())%>" onclick="window.close();"></td></tr>
            </table>

<%
}
else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
  <script>
document.getElementById("tblClose").style.display = "none";
window.print();
document.getElementById("tblClose").style.display = "block";
</script>
</body>
</html>