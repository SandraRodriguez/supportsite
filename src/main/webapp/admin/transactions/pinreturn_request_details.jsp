<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.reports.TransactionReport" %>

<%int section = 9;
int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
String strResult = "";
String strControlNo = "";
String strProductId = "";
String strReasonId = "";
String strComments = "";
if (request.getParameter("search") != null)
{
	try
    {
    	if (request.getParameter("ControlNoH") != null)
    	{
    		strControlNo = request.getParameter("ControlNoH");
    	}
    	if (request.getParameter("ProductIdH") != null)
    	{
    		strProductId = request.getParameter("ProductIdH");
    	}
    	if (request.getParameter("requestReasonList") != null)
    	{
    		strReasonId = request.getParameter("requestReasonList");
    	}
    	if (request.getParameter("comments") != null)
    	{
    		strComments = request.getParameter("comments");
    	}
		
    	strResult = TransactionReport.addPINReturnRequest(strControlNo, strReasonId, strComments, strProductId);
	}
	catch (Exception e){}  
}	    
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"/></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle"><b><%=Languages.getString("jsp.admin.transactions.pinreturn_request.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>  
	<tr>
		<td colspan="3">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF" class="main">
				
<%
if (!strResult.equals("0"))
{
	out.println("<tr><td bgcolor=#FFFFFF class=main nowrap=nowrap valign=top width=\"100%\"><br><br>&nbsp;  &nbsp;  "  + Languages.getString("jsp.admin.transactions.pinreturn_request.successfully",SessionData.getLanguage()) + " <b>" + strResult + "</b><br><br></td></tr>");
}
else
{
	out.println("<tr><td bgcolor=#FFFFFF class=main nowrap=nowrap valign=top width=\"100%\"><br><br><font color=ff0000> &nbsp;&nbsp;"  + Languages.getString("jsp.admin.transactions.pinreturn_request.error_adding",SessionData.getLanguage()) + "</font><br><br></td></tr>");
}
%>							
				
        	        <tr>
	        	        <td bgcolor=#FFFFFF class=main><a href="admin/transactions/pinreturn_request.jsp"><%=Languages.getString("jsp.admin.transactions.pinreturn_request.continue",SessionData.getLanguage())%></a><br><br></td>
        	        </tr>
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>