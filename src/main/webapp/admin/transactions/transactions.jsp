<%@ page
	import="java.net.URLEncoder,
		com.debisys.utils.*,
		com.debisys.customers.Merchant,
		com.debisys.reports.TransactionReport,
		java.util.*,
		com.debisys.reports.schedule.TransactionsReport,
		com.debisys.schedulereports.ScheduleReport"%>

<%@page import="com.debisys.tools.MarkedCollection"%>
<%@page import="com.debisys.utils.TimeZone"%>
<%@page import="com.debisys.reports.TransactionReport"%>
<%@page import="com.debisys.customers.CustomerSearch"%>
<jsp:useBean id="task" scope="session"
	class="com.debisys.utils.TaskBean"></jsp:useBean>
<%
int section=3;
int section_page=1;
boolean downloadbar  = false;
boolean maxdownloadbar  = false;
int maxdownloadset=-1 ;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<jsp:useBean id="TransactionSearch"
	class="com.debisys.transactions.TransactionSearch" scope="request" />
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil"
	scope="page" />
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil"
	scope="page" />
<jsp:setProperty name="TransactionSearch" property="*" />

<jsp:useBean id="TransactionsReport"
	class="com.debisys.reports.schedule.TransactionsReport" scope="request" />
<%@ include file="/includes/security.jsp"%>

<%
String path = request.getContextPath();
Vector<Vector<Object>> vecSearchResults = null;
Hashtable searchErrors = null;
MarkedCollection<String> markedRow = null;
String markers = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String strMerchIds = "";

//DBSY-905
int imaxdownloadbar = -1;
String positionMarkers = null;
int intSectionPage = 0;
int recordcount = 0;
boolean isDownload = false;
String repId  = "";
String invoiceId = "";
String millennium_no = "";
TransactionsReport report = null;
boolean allMerchantsSelected = false;
String strMerchantIds[] = null;

List<String> exportInformation = TransactionSearch.setInformationDetail(SessionData, application);
//this flag means if the user is in International Default
boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
   									DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

 boolean showAccountIdQRCode = false;
    showAccountIdQRCode = SessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);

String reportTitleKey = "jsp.admin.reports.transactions.transactions.title";
String reportTitle = Languages.getString(reportTitleKey, SessionData.getLanguage()).toUpperCase();

   
boolean viewReferenceCard = false;
if ( isInternationalDefault )
{
   	viewReferenceCard = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);							
}

String trReportStartDate = TransactionSearch.getStartDate();
if((trReportStartDate == null) || ((trReportStartDate != null) && (trReportStartDate.trim().equals(""))))
{
	trReportStartDate = request.getParameter("startDate");
	TransactionSearch.setStartDate(trReportStartDate);	
}
String trReportEndDate = TransactionSearch.getEndDate();
if((trReportEndDate == null) || ((trReportEndDate != null) && (trReportEndDate.trim().equals(""))))
{
	trReportEndDate = request.getParameter("endDate");
	TransactionSearch.setEndDate(trReportEndDate);	
}
SessionData.setProperty("start_date", trReportStartDate);
SessionData.setProperty("end_date", trReportEndDate);


if (request.getParameter("section_page") != null)
{
	try
   	{
    	intSectionPage = Integer.parseInt(request.getParameter("section_page"));
   	}
   	catch(NumberFormatException ex)
   	{
    	response.sendRedirect(request.getHeader("referer"));
    	return;
   	}
}  

if( request.getParameter("repId") != null )
{
	repId = request.getParameter("repId");
	TransactionSearch.setRepId( repId );
}

if (request.getParameter("invoiceID") != null)
{
	invoiceId = request.getParameter("invoiceID").toString();
	TransactionSearch.setInvoiceNo(invoiceId);
}

if (request.getParameter("merchantIds") != null) 
{
    TransactionSearch.setMerchantIds(request.getParameter("merchantIds"));
}

if ( request.getParameter("millennium_no") != null )
{
	millennium_no = request.getParameter("millennium_no");
	TransactionSearch.setMillennium_No(millennium_no);
}

if (request.getParameter("page") != null)
{
	try
   	{
    	intPage = Integer.parseInt(request.getParameter("page"));
   	}
   	catch(NumberFormatException ex)
   	{
    	intPage = 1;
   	}
 }

 if (intPage < 1)
 {
   intPage = 1;
 }
 
 
 if (request.getParameter("merchId") != null)
 {
 	strMerchIds = request.getParameter("merchId");
 }


// Init tasks
if (request.getParameter("download") != null)
{
	if (request.getParameter("markers") != null) 
	{
    	positionMarkers = request.getParameter("markers"); 
    } 
       
    //DBSY-908 
    if (request.getParameter("recordcount") != null) 
    {
    	recordcount = Integer.valueOf(request.getParameter("recordcount")); 
    } 
    //DBSY-908 
    if (request.getParameter("downloadbar") != null) 
    {
    	downloadbar = Boolean.valueOf(request.getParameter("downloadbar")); 
    } 
    //DBSY-908 
    if (request.getParameter("maxdownloadbar") != null) 
    {
    	imaxdownloadbar = Integer.valueOf(request.getParameter("maxdownloadbar")); 
    } 

	if (TransactionSearch.validateDateRange(SessionData))
    {
    	String strUrlLocation = "";
   		String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
   			    
   		if ( strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)  && (request.getParameter("chkUseTaxValue") != null))
       	{
   			if(  downloadbar )
   			{
   	     		task.setRunning(true);
   	     		task.setrecordcount(recordcount);
   	     		if((imaxdownloadbar != -1) && (recordcount > imaxdownloadbar))
   	     		{
   	     			task.setmax(imaxdownloadbar);
        			task.setrecordcount(imaxdownloadbar);
        		}
   	     		task.setvar(true,SessionData, intSectionPage, application, TransactionSearch);
   		    	isDownload = true;
   			}
   			else
   			{
   	   			if(imaxdownloadbar!=-1)
   	   			{
   	     			TransactionSearch.setmax(imaxdownloadbar);
   	   			}
   		    	if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
   		    	{
	   				String sTmp = SessionData.getProperty("ref_id");
	   				SessionData.setProperty("ref_id", request.getParameter("repId"));
	   				SessionData.setProperty("access_level", DebisysConstants.ISO);
	   				SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
	   	     	 	strUrlLocation = TransactionSearch.downloadMx(SessionData, intSectionPage, application);
	   				SessionData.setProperty("ref_id", sTmp);
	   				SessionData.setProperty("access_level", DebisysConstants.CARRIER);
	   				SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
   		    	}
   		    	else if ( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER))
   		    	{
   		        	TransactionSearch.setMerchantIds(request.getParameterValues("ISOIds"));
   		        	TransactionSearch.setRepId(TransactionSearch.getMerchantIds());
   		        	TransactionSearch.setMerchantIds("");
   	     	 		strUrlLocation = TransactionSearch.downloadMx(SessionData, intSectionPage, application);
   		    	}
   		    	else
   		    	{
   	     	 		strUrlLocation = TransactionSearch.downloadMx(SessionData, intSectionPage, application);
   		    	}
        	 	response.sendRedirect(strUrlLocation);
			}
       }
       else 
       {
       		if( downloadbar )
       		{
        		task.setRunning(true);
        		task.setrecordcount(recordcount);
   	    		if((imaxdownloadbar != -1)  && (recordcount > imaxdownloadbar))
   	    		{
   	     			task.setmax(imaxdownloadbar);
        			task.setrecordcount(imaxdownloadbar);
        		}
        		task.setvar(false,SessionData, intSectionPage, application, TransactionSearch);
   				isDownload = true;
   			}
   			else
   			{
   	   			if(imaxdownloadbar!=-1)
   	   			{
   	     			TransactionSearch.setmax(imaxdownloadbar);
   	   			}
   	     			  
   		    	if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
   		    	{
	   				String sTmp = SessionData.getProperty("ref_id");
	   				SessionData.setProperty("ref_id", request.getParameter("repId"));
	   				SessionData.setProperty("repName", request.getParameter("repName"));
	   				SessionData.setProperty("access_level", DebisysConstants.ISO);
	   				SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
	   	     	 	strUrlLocation = TransactionSearch.download(SessionData, intSectionPage, application);
	   				SessionData.setProperty("ref_id", sTmp);
	   				SessionData.setProperty("access_level", DebisysConstants.CARRIER);
	   				SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
   		    	}
   		    	else if ( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER))
   		    	{
	   		        TransactionSearch.setMerchantIds(request.getParameterValues("ISOIds"));
	   		        TransactionSearch.setRepIdMultiple(TransactionSearch.getMerchantIds());
	   		        TransactionSearch.setMerchantIds("");
	   	     	 	strUrlLocation = TransactionSearch.download(SessionData, intSectionPage, application);
   		    	} else {
                            strUrlLocation = TransactionSearch.download(SessionData, intSectionPage, application);
   		    	}
        	 	response.sendRedirect(strUrlLocation);
        	}
		}   
	}
    else
    {
    	response.sendRedirect(request.getHeader("referer"));
        return;
	}
} // end : if (request.getParameter("download") != null)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
else if ( request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y") )
{
    if (TransactionSearch.validateDateRange(SessionData))
    {
	//TO SCHEDULE REPORT
	strMerchantIds = request.getParameterValues("mids");
	allMerchantsSelected = Boolean.valueOf(request.getParameter("allMerchantsSelected"));
	if((strMerchantIds == null) || 
			((strMerchantIds != null) && 
					((strMerchantIds.length == 0) || ((strMerchantIds.length > 0) && (strMerchantIds[0].trim().equals(""))))))
	{
		allMerchantsSelected = true;
	}

	report = new TransactionsReport(SessionData, application, true, trReportStartDate, trReportEndDate,
			invoiceId, request.getParameter("PINNumber"),
			false, section_page, "", request.getParameter("repId"), request.getParameter("merchId"), StringUtil.arrayToString(request.getParameterValues("mids"), ","),
			request.getParameter("transactionID"), request.getParameter("millennium_no"), viewReferenceCard, intPageSize, intPage, 
			SessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA), allMerchantsSelected);
	report.setReportTitle(reportTitle);
	if(report.scheduleReport())
	{
		ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
		if (  scheduleReport != null  )
		{
			scheduleReport.setStartDateFixedQuery( report.getStart_date());
			scheduleReport.setEndDateFixedQuery( report.getEnd_date());
			scheduleReport.setTitleName( reportTitleKey );   
		}	
		response.sendRedirect( DebisysConstants.PAGE_TO_SCHEDULE_REPORTS );
	}
    } // END : if (TransactionSearch.validateDateRange(SessionData))
    else
    {
       searchErrors = TransactionSearch.getErrors();
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
else if (request.getParameter("search") != null)
{
	strMerchantIds = request.getParameterValues("mids");
	allMerchantsSelected = Boolean.valueOf(request.getParameter("allMerchantsSelected"));
	if((strMerchantIds == null) || 
				((strMerchantIds != null) && 
						((strMerchantIds.length == 0) || ((strMerchantIds.length > 0) && (strMerchantIds[0].trim().equals(""))))))
	{
		allMerchantsSelected = true;
	}

	if (TransactionSearch.validateDateRange(SessionData))
        {
            if(strMerchIds != "")
            {
                    TransactionSearch.setMerchantIds(strMerchIds);  
            }
            else
            {
                if ( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER))
                {
                    strMerchantIds = request.getParameterValues("ISOIds");
                    TransactionSearch.setMerchantIds(strMerchantIds);
                    TransactionSearch.setRepIdMultiple(TransactionSearch.getMerchantIds());
                    strMerchantIds = null;
                    TransactionSearch.setMerchantIds("");
                }
                if (strMerchantIds != null)
                {
                    TransactionSearch.setMerchantIds(strMerchantIds);
                }  	
            }
            report = new TransactionsReport(SessionData, application, false, trReportStartDate, trReportEndDate,
				invoiceId, request.getParameter("PINNumber"),
				false, section_page, "", request.getParameter("repId"), request.getParameter("merchId"), StringUtil.arrayToString(request.getParameterValues("mids"), ","),
				request.getParameter("transactionID"), request.getParameter("millennium_no"), viewReferenceCard, intPageSize, intPage, 
				SessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA), allMerchantsSelected);
            report.setReportTitle(reportTitle);
            if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
            {
                String sTmp = SessionData.getProperty("ref_id");
                SessionData.setProperty("ref_id", request.getParameter("repId"));
                SessionData.setProperty("access_level", DebisysConstants.ISO);
                SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
                vecSearchResults = report.getTransactionResults();
                SessionData.setProperty("ref_id", sTmp);
                SessionData.setProperty("access_level", DebisysConstants.CARRIER);
                SessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_5_LEVEL);
            }
            else
            {
                vecSearchResults = report.getTransactionResults();
            }
            intRecordCount = Integer.parseInt(vecSearchResults.get(0).get(0).toString());
       	
            // DBSY 908
            // checking download bar settings if Enabled and if feature is needed.
            String download = DebisysConfigListener.getdownloadbar(application);
            if(download!=null && !download.equals(""))
            {
                try
                {
                    int downloadset = Integer.parseInt(download);
                    if( downloadset <= intRecordCount )
                    {
                        downloadbar = true;
                    }
                }
                catch(NumberFormatException e)
                {
                    downloadbar = false;
                }
            }
   		
            String maxdownload = DebisysConfigListener.getmaxdownload(application);
            if((maxdownload != null) && (!maxdownload.equals("")))
            {
                try
                {
                    maxdownloadset = Integer.parseInt(maxdownload);
                    if( maxdownloadset < intRecordCount && maxdownloadset!=0)
                    {
                        maxdownloadbar = true; 
                    }
                }
                catch(NumberFormatException e)
                {
                    maxdownloadbar = false;
                }
            }
   		
            vecSearchResults.removeElementAt(0);
            if (intRecordCount>0)
            {
                intPageCount = (intRecordCount / intPageSize);
                if ((intPageCount * intPageSize) < intRecordCount)
                {
                    intPageCount++;
                }
            }
        } // END : if (TransactionSearch.validateDateRange(SessionData))
        else
        {
           searchErrors = TransactionSearch.getErrors();
        }
} // end : if (request.getParameter("search") != null)	
else
{
	if( request.getParameter("repId") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
   	{
   		repId = request.getParameter("repId");
   		TransactionSearch.setRepId( repId );
   	}
}

%>
<%@ include file="/includes/header.jsp"%>

<c:set value="<%= exportInformation %>" var="records"></c:set> 
<c:set value='<%= Languages.getString("jsp.admin.reports.transactions.transactions.download", SessionData.getLanguage()) %>' var="downloadTitleButton"></c:set> 


<script type="text/javascript">
    var splitDetailCsv = ('${records}' != null && '${records}' != undefined) ? '${records}'.replace("[", "").replace("]", "").split(",") : "";
    var downloadTitle = ('${downloadTitleButton}' != null && '${downloadTitleButton}' != undefined) ? '${downloadTitleButton}' : "";
</script>

<link rel="stylesheet" type="text/css" href="<%=path%>/css/dataTables/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/dataTables/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/admin/transactions/transactions.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/default.css">

<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/jquery-1.12.4.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/js/admin/transactions/Transactions.js" ></script>



<SCRIPT LANGUAGE="JavaScript">
function check(){
	
 if(!IsNumeric(document.mainform.invoiceID.value)){
 	document.mainform.invoiceID.value  = "";
 	alert ("Invoice must be numberic");
	return false; 
 }
 if(!IsNumeric(document.mainform.transactionID.value)){
 	document.mainform.transactionID.value  = "";
 	alert ("Transaction ID must be numberic");
	return false; 
 }
   scroll();
   return true;
}         
function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
   }

var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2("false")', 150);
}
function openViewPIN(sTransId)
{
  var sURL = "/support/admin/transactions/view_pin.jsp?trans_id=" + sTransId;
  var sOptions = "left=" + (screen.width - (screen.width * 0.4))/2 + ",top=" + (screen.height - (screen.height * 0.3))/2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
  var w = window.open(sURL, "ViewPIN", sOptions, true);
  w.focus();
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="800">
	<c:if test="${!SessionData.user.supportSiteNgUser}">
	<tr background="images/top_blue.gif">
		<td width="18" height="20" align="left">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td class="formAreaTitle" align="left" width="2000"><%=Languages.getString("jsp.admin.reports.transactions.transactions.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20" align="right">
			<img src="images/top_right_blue.gif">
		</td>
	</tr>
	</c:if>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				align=center>
				<tr>
					<td>
						<form name="mainform" method="get"
							action="admin/transactions/transactions.jsp"
							onsubmit="return check();">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="formArea2">
										<table width="300">
											<%
	Vector<?> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
%>
											<tr class="main">
												<td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br />
													<br />
												</td>
											</tr>
											<tr class="main">
												<td nowrap valign="top">
													<%
	if (!(StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER)))
	{
		out.print(Languages.getString("jsp.admin.iso_name",SessionData.getLanguage()) + ":&nbsp;");
		if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
		{
			out.print(request.getParameter("repName"));
		}
		else
		{
			out.print(SessionData.getUser().getCompanyName());
		}
	}
%>
												</td>
												<td>
													&nbsp;
												</td>
											</tr>
											<tr class="main">
												<td nowrap valign="top">
													<%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
													<%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:
												</td>
												<%}else{ %>
												<%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:
												</td>
												<%}%>
												<td>
													&nbsp;
												</td>
											</tr>

											<tr>
												<td valign="top" nowrap>
													<table width=400>
														<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<br/>" + strError);
}

  out.println("</font></td></tr>");
}
%>
														<tr class="main">
															<td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:
															</td>
															<td>
																<input class="plain" id="startDate" name="startDate"
																	value="<%=SessionData.getProperty("start_date")%>"
																	size="12">
																<a href="javascript:void(0)"
																	onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;"
																	HIDEFOCUS><img name="popcal" align="absmiddle"
																		src="admin/calendar/calbtn.png" width="34" height="22"
																		border="0" alt=""> </a>
															</td>
														</tr>
														<tr class="main">
															<td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:
															</td>
															<td>
																<input class="plain" id="endDate" name="endDate"
																	value="<%=SessionData.getProperty("end_date")%>"
																	size="12">
																<a href="javascript:void(0)"
																	onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;"
																	HIDEFOCUS><img name="popcal" align="absmiddle"
																		src="admin/calendar/calbtn.png" width="34" height="22"
																		border="0" alt=""> </a>
															</td>
														</tr>
														<tr class="main">
															<td nowrap><%=Languages.getString("jsp.admin.reports.transaction_id",SessionData.getLanguage())%>:
															</td>
															<td>
																<input name="transactionID" id="transactionID"
																	value="<%=TransactionSearch.getTransactionID()%>">
															</td>
														</tr>

														<% if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{%>
														<tr class="main">
															<td nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>:
															</td>
															<td>
																<input name="invoiceID" id="invoiceID"
																	value="<%=TransactionSearch.getInvoiceNo()%>">
															</td>
														</tr>
														<%} 
		else
		{ %>
														<tr style="display: none;" class="main">
															<td style="display: none;" nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>:
															</td>
															<td>
																<input name="invoiceID" id="invoiceID"
																	style="display: none;"
																	value="<%=TransactionSearch.getInvoiceNo()%>">
															</td>
														</tr>
														<%
		}
		if ( !strAccessLevel.equals(DebisysConstants.CARRIER) )
		{
%>
														<tr class="main">
															<td nowrap><%=Languages.getString("jsp.admin.reports.pinreturn_search.pin",SessionData.getLanguage())%>:
															</td>
															<td>
																<input name="PINNumber" id="PINNumber"
																	value="<%=TransactionSearch.getPINNumber()%>">
															</td>
														</tr>
														<%
		}
			if ( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER))
			{
		%>
														<tr class="main">
															<td class=main valign=top nowrap><%=Languages.getString("jsp.productapproval.selectISO",SessionData.getLanguage())%></td>
															<td class=main valign=top>
																<select name="ISOIds" size="10" multiple>
																	<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
																	<%
	{
	Vector vISOs = (new CustomerSearch()).searchRep(1, Integer.MAX_VALUE, SessionData, DebisysConstants.REP_TYPE_ISO_5_LEVEL);
	if ( vISOs.size() > 1 )
	{
		vISOs.remove(0);
		Iterator it = vISOs.iterator();
		while (it.hasNext())
		{
			Vector vTemp = null;
			vTemp = (Vector) it.next();
			out.println("<option value=" + vTemp.get(1) +">" + vTemp.get(0) + "</option>");
		}
	}
	else
	{
		out.println("<option value=''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>");
	}
	}
%>
																</select>
																<br>
															</td>
														</tr>
														<%
			}
			else if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
			{	
		%>
														<tr class="main">
															<td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%></td>
															<td class=main valign=top>
																<%
				if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
				{
%>
																<input type="hidden" name="repName"
																	value="<%=request.getParameter("repName")%>">
																<input type="hidden" name="repId"
																	value="<%=request.getParameter("repId")%>">
																<%
				}
%>
																<select name="mids" id="mids" size="10" multiple>
																	<%
                                if(strMerchIds == "")
                                {
                                    out.println("<option value=\"\" selected>" + Languages.getString("jsp.admin.reports.all",SessionData.getLanguage()) + "</option>");
                                }
                                else
                                {
                                    out.println("<option value=\"\">" + Languages.getString("jsp.admin.reports.all",SessionData.getLanguage()) + "</option>");
                                }
								Vector vecMerchantList;
								String sTmp = SessionData.getProperty("ref_id");
								
								System.out.println("strAccessLevel strAccessLevel"+strAccessLevel);
								if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
								{
									TransactionReport.setCarrierId(sTmp);
									//String sTmp = SessionData.getProperty("ref_id");
									SessionData.setProperty("ref_id", request.getParameter("repId"));
									vecMerchantList = Merchant.getMerchantListReports(SessionData);
									SessionData.setProperty("ref_id", sTmp);
									TransactionReport.setCarrierId(null);
								}
								else
								{
									vecMerchantList = Merchant.getMerchantListReports(SessionData);
								}
							  	Iterator it = vecMerchantList.iterator();
							  	while (it.hasNext())
							  	{
							    	Vector vecTemp = null;
							    	vecTemp = (Vector) it.next();
                                    if(strMerchIds == vecTemp.get(0))
                                    {
							    	    out.println("<option selected value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                                    }
                                    else
                                    {
                                        out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                                    }
							  	}
							%>
																</select>
																<br>
																*<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%>
															</td>
														</tr>
														<%
			}
		%>

														<tr align=center>
															<td class=main align="center">
																<input type="hidden" name="search" value="y">
																<%
	if (!(StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER)))
	{
%>
																<input type="hidden" name="repId"
																	value="<%=TransactionSearch.getRepId()%>">
																<%
	}
%>
																<input type="submit" name="submit"
																	value="<%=Languages.getString("jsp.admin.transactions.show_transactions",SessionData.getLanguage())%>"
																	onclick="validateSchedule(0);">
															</td>
															<jsp:include page="/admin/reports/schreportoption.jsp">
																<jsp:param
																	value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>"
																	name="permissionEnableScheduleReports" />
																<jsp:param value="<%=SessionData.getLanguage()%>"
																	name="language" />
															</jsp:include>

														</tr>

													</table>
													<table width=400>
														<tr>
															<td class="main" nowrap>
																*
																<%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											</form>
										</table>
										<% if(isDownload){%>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<div>
														<iframe width=350 height=200 name="iframe" id="iframe"
															src="admin/transactions/status.jsp" frameborder="0"
															style="visibility: visible;">
														</iframe>
													</div>
												</td>
											</tr>
										</table>
										<%
new Thread(task).start();
}
          
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>

										<table width="100%" border="0" cellspacing="0" cellpadding="2">
											<tr>
												<td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
													<%
              if (!TransactionSearch.getStartDate().equals("") && !TransactionSearch.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(TransactionSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getEndDateFormatted()));
                }
            %>.
													<%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%></td>
											</tr>
											<tr>
												<td align=left class="main" nowrap>
													<% if(!isDownload){%>
													<form name="downloadform" method=post
														action="admin/transactions/transactions.jsp"
														onSubmit="scroll2();">
														<%
				if( StringUtil.toString(request.getParameter("repId")).length() == 0 && strAccessLevel.equals(DebisysConstants.CARRIER) )
				{
%>
														<input type="hidden" name="ISOIds"
															value="<%=TransactionSearch.getRepId()%>">
														<%
				}
				if( request.getParameter("repName") != null && strAccessLevel.equals(DebisysConstants.CARRIER) )
				{
%>
														<input type="hidden" name="repName"
															value="<%=request.getParameter("repName")%>">
														<input type="hidden" name="repId"
															value="<%=request.getParameter("repId")%>">
														<%
				}
%>
														<input type="hidden" name="startDate"
															value="<%=TransactionSearch.getStartDate()%>">
														<input type="hidden" name="endDate"
															value="<%=TransactionSearch.getEndDate()%>">
														<input type="hidden" name="page" value="<%=intPage%>">
														<input type="hidden" name="section_page"
															value="<%=section_page%>">
														<input type="hidden" name="download" value="y">
														<input type="hidden" id="merchantIds" name="merchantIds"
															value="<%=TransactionSearch.getMerchantIds()%>">
														<input type="hidden" name="transactionID"
															value="<%=TransactionSearch.getTransactionID()%>">
														<input type="hidden" name="invoiceID"
															value="<%=TransactionSearch.getInvoiceNo()%>">
														<input type="hidden" name="PINNumber"
															value="<%=TransactionSearch.getPINNumber()%>">
														<input type="hidden" id="markers" name="markers"
															value="<%=markers%>">
														<input type="hidden" id="recordcount" name="recordcount"
															value="<%=intRecordCount%>">
														<input type="hidden" id="downloadbar" name="downloadbar"
															value="<%=downloadbar%>">
														<INPUT TYPE=hidden NAME=allMerchantsSelected VALUE="<%=allMerchantsSelected%>">
														<% if(maxdownloadbar){ %><input type="hidden"
															id="maxdownloadbar" name="maxdownloadbar"
															value="<%=maxdownloadset%>">
														<% } %>
														<%
																if((strMerchantIds != null) && (strMerchantIds.length > 0))
																{
																	for(int i = 0;i < strMerchantIds.length;i++)
																	{
														%>
														       				<input type="hidden" name="mids" value="<%=strMerchantIds[i]%>">
														<%
														          	}
																}
														%>
														
													</form>
													<br />

													<% if(maxdownloadbar){ %>
													<FONT COLOR="red"> <%=Languages.getString("jsp.admin.warning_maxdownload",SessionData.getLanguage())%><%=maxdownloadset%><%=Languages.getString("jsp.admin.warning_secmaxdownload",SessionData.getLanguage())%>
													</font>
													<% }
              } %>
												</td>
											</tr>
										</table>
										<table>
											<tr>
												<td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>:
													<%=SessionData.getProperty("company_name")%></td>
											</tr>
										</table>
										<table id="tableTransactions" class="display compact" style="width:100%" cellspacing="1" cellpadding="2">
                                                                                    <thead class="rowhead2">
											<tr>
												<th>#</th>
												<th><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</th>
												<th><%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.city",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.county",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.reference",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%></th>

												<%

/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                	if(strAccessLevel.equals(DebisysConstants.ISO)){
               	 		if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
%>
												<th><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;
												</th>
												<th><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()) %></th>
												<%               	 	}
                	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
%>
												<th><%= Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase() %>&nbsp;
												</th>
												<th><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<%                	 }
                   	}
                   	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
%>
												<th><%= Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<%                  }
                   	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
%>
												<th><%= Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<%                 	}
                   	else if (strAccessLevel.equals(DebisysConstants.REP)){
%>
												<th><%= Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<th><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<%                  }
                   	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
%>
												<th><%= Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase() %></th>
												<%                  }
%>


												<%
				}
/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/				
%>


												<%
if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
	&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
%>
												<th><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%></th>

												<% if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
	    	  {%>
												<th><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.taxpercentage",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.taxtype",SessionData.getLanguage()).toUpperCase()%></th>
												<%}
}
%>
												<th><%=Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.description",SessionData.getLanguage()).toUpperCase()%></th>
												<th><%=Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()).toUpperCase()%></th>
												<%
				if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT))
 				{ 		//Authorization Carrier Number
%>
												<th><%=Languages.getString("jsp.admin.reports.authorization_carrier_number",SessionData.getLanguage()).toUpperCase()%></th>
												<%
				}
				if (SessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA)) { 
%>
												<th><%=Languages.getString("jsp.admin.reports.additionalData",SessionData.getLanguage()).toUpperCase()%></th>
												<%
				}
				else
				{
 %>
												<th><%=Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_type",SessionData.getLanguage()).toUpperCase()%></th>
												<%
				}				

     //Added by jacuna.R25. DBSY-525
if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) 
	&& strAccessLevel.equals(DebisysConstants.ISO)
	&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) 
{%>
												<th><%=Languages.getString("jsp.admin.reports.pin_number",SessionData.getLanguage()).toUpperCase()%></th>
												<%}
if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) 
	&& strAccessLevel.equals(DebisysConstants.ISO)
	&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) 
{%>
												<th><%=Languages.getString("jsp.admin.reports.ach_date",SessionData.getLanguage()).toUpperCase()%></th>
												<%}//End aded by jacuna.R25. DBSY-525%>
												<th><%=Languages.getString("jsp.admin.reports.transaction_type",SessionData.getLanguage()).toUpperCase()%></th>
												<% if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{%>
												<th><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage()).toUpperCase()%></th>
												<%} %>

												<%if ( viewReferenceCard ){%>
												<th><%=Languages.getString("jsp.admin.report.transactions.DTU1204",SessionData.getLanguage()).toUpperCase()%></th>
												<%}%>
                                                                                                <%if (showAccountIdQRCode) {%>
                                                                                                <th><%=Languages.getString("jsp.admin.report.transactions.reference", SessionData.getLanguage()).toUpperCase()%></th>
                                                                                                <%}%>
											</tr>
                                                                                    </thead>
                                                                                <tbody>
											<%
                  int intCounter = 1;
                  //intCounter = intCounter - ((intPage-1)*intPageSize);
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;                  
                 
                  String type = "";
                  String row = "";
                  // set all the field positions as displayed by transactions.jsp
  
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    markedRow = null;
                    markedRow = new MarkedCollection<String>(vecTemp);
                    // check if type is "Return" of "Adjust", if so, display the whole row in red
                    type = (String)vecTemp.get(15);
                    if (type.equals("Return") || type.equals("Adjustment") || type.equals("Credit")||type.equals("Write Off"))
                    {
                    	row = "rowred";
                    }
                    else
                    {
                    	row = "row";
                    }
                    String sViewPIN = "";
                    String sPinNumber = "N/A";
                    if ( SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) )
                    {
                        // LOCALIZATION - Section modified to accept just a result string with the currency amount
                        // From the DB.
                      //if the trans_type=2 which is a pin sale
                      if ((vecTemp.get(14).toString().equals("2")) && Double.parseDouble(vecTemp.get(9).toString().replaceAll(",", "")) > 0)
                      {
                        sViewPIN = "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + vecTemp.get(0).toString() + "');\"><span class=\"showLink\"><span class=\"showLink\">" + vecTemp.get(13).toString() + "</span></span></a>";
                      	sPinNumber = vecTemp.get(20).toString();
                      }
                      else
                      {
                        sViewPIN = vecTemp.get(13).toString();
                      }

                    }
                    else
                    {
                      sViewPIN = vecTemp.get(13).toString();
                    }
					String[] productDescription = vecTemp.get(11).toString().split("\\/");
					
                    out.println("<tr class=" + row + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td align=\"right\">" + markedRow.get(0)+ "</td>" +
                                "<td align=\"right\">" + markedRow.get(1) + "</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(markedRow.get(2))  + "</td>" +
                                "<td>" + markedRow.get(3) + "</td>" +
                                "<td nowrap>" + markedRow.get(4) + "</td>" +
                                 //city
                                "<td nowrap>" +  vecTemp.get(5).toString() + "</td>" +
                                 //county
                                "<td nowrap>" +  vecTemp.get(29).toString() + "</td>" +
                                //clerk
                                "<td nowrap>" + markedRow.get(6) + "</td>" +
                                "<td>" + markedRow.get(7) + "</td>" +
                                "<td>" + markedRow.get(8) + "</td>" +
			                    //DBSY-905 Tax Calculation
			                    //amount
			                                "<td nowrap align=\"right\">" + NumberUtil.formatCurrency(markedRow.get(9)) + "</td>");
			                           
			                                              //billing_typeId
				                 /* if( strAccessLevel.equals(DebisysConstants.CARRIER) )
				                   	out.println("<td nowrap>" +  vecTemp.get(34).toString() + "</td>");*/
                   	     
			                    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
			                     	if(strAccessLevel.equals(DebisysConstants.ISO)){
			                    	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
			                     		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
			                     		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //rep
			                     		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //merchant
			                     	 }
			                     	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			                     	 	out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(32).toString())+ "</td>"); //iso
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //merchant
			                     	 }
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
		                         		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(30).toString())+ "</td>"); //agente
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
		                         		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(31).toString())+ "</td>"); //subagente
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //merchant
		                         	}
		                         	else if (strAccessLevel.equals(DebisysConstants.REP)){
		                         		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(34).toString())+ "</td>"); //rep
		                          		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //merchant
		                         	}
		                         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
		                         		out.println("<td nowrap align=\"right\">" +NumberUtil.formatCurrency(vecTemp.get(33).toString())+ "</td>"); //merchant
		                         	}
		                        }  
		                    	/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			                                    
			                    if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
			                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
			                    //bonus
			                                out.println("<td nowrap align=\"right\">" + markedRow.get(18) + "</td>" +
			                    //total Recharge
			                                "<td nowrap align=\"right\">" + markedRow.get(19) + "</td>");
			                         
			                        if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
				    	 					{
				    	 		//net amount 
			                                out.println("<td nowrap align=\"right\">" + markedRow.get(25) + "</td>" +
			                    //tax amount
			                                "<td nowrap align=\"right\">" + markedRow.get(26) + "</td>" + 
			                                "<td nowrap align=\"right\">" + markedRow.get(27) + "</td>" + 
			                                "<td nowrap align=\"right\">" + markedRow.get(28) + "</td>");
			                                }
			                    }
                                //bal
                                out.println("<td nowrap align=\"right\">" + NumberUtil.formatCurrency(markedRow.get(10)) + "</td>" +
                                //product
                                "<td nowrap width=200 align=\"right\">" + vecTemp.get(11).toString() + "</td>" +
                                //product description                                
                                "<td nowrap width=200>" + vecTemp.get(12).toString() + "</td>" +
                                
                                "<td align=\"right\">" +  markedRow.mark(sViewPIN, 13) + "</td>");
                                
                                if ( SessionData.checkPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT) )
				 				{
				 					//Authorization Number
				 				 	out.println("<td nowrap>" + markedRow.get(16) + "</td>");
				 				}
				 				
                                String value="";
                                if (SessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA)) 
                                {
                                  	value = markedRow.get(23);
                                		 value = value == null || value.equals("") ? Languages.getString("jsp.admin.reports.additionalData.default",SessionData.getLanguage()) : value;
                                }
                                else
                                {
                                	value = markedRow.get(22);
                                }                                
				 				out.println("<td nowrap>" + value + "</td>");
				 				
					if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) 
						&& strAccessLevel.equals(DebisysConstants.ISO)
						&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
								out.println("<td>" + markedRow.mark(sPinNumber, 20) + "</td>"); 
                    }                    
					if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) 
						&& strAccessLevel.equals(DebisysConstants.ISO)
						&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
								out.println("<td>" + markedRow.get(21) + "</td>");
                    }                    
                                out.println("<td>" + markedRow.mark(type, 15) + "</td>" );
                     
                    if(com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
      			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
      			&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
         			{
                    //DBSY-919 Invoice Number Field
                        out.println("<td>" + markedRow.get(24) + "</td>" );
                    //////  
                    }
                    
                    if ( viewReferenceCard ){
				   		out.println("<td>" + vecTemp.get(35) + "</td>");
				   	}
                    
                    if (vecTemp.size() >= 37 && showAccountIdQRCode) {
                                        out.println("<td>" + vecTemp.get(36) + "</td>");
                    }
                    else if (showAccountIdQRCode) {
                        out.println("<td></td>");
                    }

				   		
                    out.println("</tr>");
                    
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                   	
                  vecSearchResults.clear();
                  markers = markedRow.getMarkers();
            %>
                                                                            </tbody>
                                                                        </table>

										<%
}
else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>
									</td>
								</tr>
							</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js"
	id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;">
</iframe>

<%@ include file="/includes/footer.jsp"%>