<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
String strPlayerTypeId = "";
String strPlayerId = "";
String strCardProgramTypeId = "";
String strStatusId = "";
String strSerialNumber = "";
String strStartDate = "";
String strEndDate = "";
if (request.getParameter("submitted") != null)
{
	try
    {
	    if (request.getParameter("submitted").equals("y"))
		{
	    	if (request.getParameter("PlayerTypeId") != null)
	    	{
	    		strPlayerTypeId = request.getParameter("PlayerTypeId");
	    	}
	    	if (request.getParameter("PlayerId") != null)
	    	{
	    		strPlayerId = request.getParameter("PlayerId");
	    	}	    		    	
	    	if (request.getParameter("StartDate") != null)
	    	{
	    		strStartDate = request.getParameter("StartDate");
	    	}
	    	if (request.getParameter("EndDate") != null)
	    	{
	    		strEndDate = request.getParameter("EndDate");
	    	}	    	
		}
	}
	catch (Exception e){}  
}	    
%>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.showRep.disabled = true;
  document.mainform.showRep.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="main"><b><%=Languages.getString("jsp.admin.transactions.manageinventory.card_sales_search",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
					<tr>
						<td width="18">&nbsp;</td>
						<td class="main"><br>
						<form name="mainform" method="post" action="/support/admin/transactions/card_sales_search.jsp" onSubmit="scroll();">
							<input type="hidden" name="submitted" value="y">
							<INPUT TYPE="hidden" ID="PlayerTypeId" NAME="PlayerTypeId" VALUE="<%=strPlayerTypeId%>">
							<INPUT TYPE="hidden" ID="PlayerId" NAME="PlayerId" VALUE="<%=strPlayerId%>">
							<INPUT TYPE="hidden" ID="StartDate" NAME="StartDate" VALUE="<%=strStartDate%>">							
							<INPUT TYPE="hidden" ID="EndDate" NAME="EndDate" VALUE="<%=strEndDate%>">							
							<table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
										<tr class="main">
										    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%></td> 
										    <td class=main valign=top>
										    	<input class="plain" name="txtStartDate" value="<%=strStartDate%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.txtStartDate,document.mainform.txtEndDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
										    </td>
										</tr>
										<tr class="main">
										    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%></td> 
										    <td class=main valign=top>
										    	<input class="plain" name="txtEndDate" value="<%=strEndDate%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.txtStartDate,document.mainform.txtEndDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
										    </td>
										</tr>
	                        		    <tr class="main">
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_type",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="PlayerTypeList" onchange="getCompanies();">
            	    	                    	    <%
	            		                                Vector vecPlayerTypeList = CMS.getCompanyRolesList(SessionData);
    	    	        	                            Iterator itPTL = vecPlayerTypeList.iterator();
    	    	        	                            String strSelectedPTL = "";
            	    	                    	    	out.println("<option " + "" + " value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itPTL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempPTL = null;
                	                	                	vecTempPTL = (Vector) itPTL.next();
                	                	                	if(!strPlayerTypeId.equals(""))
                	    	                    	    	{
               	                	                			if(strPlayerTypeId.equals(vecTempPTL.get(0)))
                   	    	                    	    		{
               	                	                				strSelectedPTL = "selected";
   	                	    	                    	    	}
       	        	                	                		else
           	    	                	                		{
       	        	                	                			strSelectedPTL = "";
               		                	                		}
                	    	                    	    	} 
                	                	                	else
                	                	                	{
                	                	                		strSelectedPTL = "";
                	                	                	}
                        	                		    	out.println("<option " + strSelectedPTL + " value=" + vecTempPTL.get(0) +">" + vecTempPTL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
            	    		                	<SCRIPT>
            	    		                		function getCompanies()
                                                    {
														document.getElementById('PlayerTypeId').value = document.getElementById('PlayerTypeList').value; 
														document.getElementById('PlayerId').value = document.getElementById('PlayerList').value; 
														document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
														document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;
                                                        document.mainform.submit();
                                                     }
                                                </SCRIPT>		    	                        	        
    	    	                	        </td>
    	    	                	    </tr>
	                        		    <tr class="main">    	    	                	        
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_name",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="PlayerList">
            	    	                    	    <%
            	    	                    	    	Vector vecPlayerList = CMS.getCompaniesbyRoleList(SessionData, strPlayerTypeId);
    	    	        	                            Iterator itPL = vecPlayerList.iterator();
            	    	                    	    	out.println("<option  value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itPL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempPL = null;
                	                	                	vecTempPL = (Vector) itPL.next();
                        	                		    	out.println("<option value=" + vecTempPL.get(0) +">" + vecTempPL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td> 
    	    	                	    </tr>
	                        		    <tr class="main">    	    	                	            	    	                	        
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="CardProgramList" multiple>
        	            	            		<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
            	    	                    	    <%
	            		                                Vector vecCardProgramList = CMS.getSalesCardsProgramList(SessionData);
    	    	        	                            Iterator itCPL = vecCardProgramList.iterator();
    		                	                        while (itCPL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempCPL = null;
                	                	                	vecTempCPL = (Vector) itCPL.next();
                        	                		    	out.println("<option value=" + vecTempCPL.get(0) +">" + vecTempCPL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
	    	                        	        <br>*<%=Languages.getString("jsp.admin.reports.transactions.agents.instructions",SessionData.getLanguage())%>
    	    	                	        </td>
    	    	                	    </tr>	    	    	                	    
									   <tr>
									       <td class="main">
    									     <input type="hidden" name="search" value="y">
									         <input type="submit" name="showRep" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="showReport();">
            	    		                	<SCRIPT>
            	    		                		function showReport()
                                                    {
														document.getElementById('PlayerTypeId').value = document.getElementById('PlayerTypeList').value; 
														document.getElementById('PlayerId').value = document.getElementById('PlayerList').value; 
														document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
														document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;
                                                    	document.mainform.action = '/support/admin/transactions/card_sales_search_summary.jsp';
                                                        document.mainform.submit();
                                                    }
                                                </SCRIPT>									         
									       </td>
									   </tr>        	        	    	       	        	        	    	       	
								</table>        	        	    	       					
						</form>
						<br>
						<br>
						</td>
						<td width="18">&nbsp;</td>
					</tr>

				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp"%>
