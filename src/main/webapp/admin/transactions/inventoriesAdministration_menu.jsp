<%
int section=9;
int section_page=5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>



<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" width="2000"><b><%=Languages.getString("jsp.includes.menu.tools",SessionData.getLanguage()).toUpperCase()%></b></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
 </tr>
 <tr>
 	<td colspan="3">
   	<table width="100%" border="0" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0">
 	    <tr>
 		      <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
  		    <td align="left" valign="top" bgcolor="#FFFFFF">
   			    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
    			    <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main">
				
        <table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
          <tr>
            <td VALIGN="left" WIDTH="376" height="28">
              <img border="0" src="images/analysis_cube.png" width="22" height="22">
              <font face="Arial, Helvetica, sans-serif" style="font-size: 13px; font-weight: 700" color="#2C0973">
              <%=Languages.getString("jsp.includes.menu.inventoriesAdministration.title",SessionData.getLanguage())%></font>
            </td>
          </tr>
          <tr>
            <td VALIGN="top" WIDTH="376" height="28">
              <font face="Arial, Helvetica, sans-serif" style="font-size: 13px; font-weight: 700" color="#2C0973"><a href="admin/transactions/inventoryTerminals_search.jsp" style="text-decoration: none">
              - <%=Languages.getString("jsp.includes.menu.inventoriesAdministration.terminals",SessionData.getLanguage())%></font>
            </td>
          </tr>
          <tr>
            <td VALIGN="top" WIDTH="376" height="28">
               <font face="Arial, Helvetica, sans-serif" style="font-size: 13px; font-weight: 700" color="#2C0973"><a href="admin/transactions/inventorySims_search.jsp" style="text-decoration: none">
              - <%=Languages.getString("jsp.includes.menu.inventoriesAdministration.sims",SessionData.getLanguage())%></font>
            </td>
          </tr>
</table>
     				    </td>
     		        <td width="18">&nbsp;</td>
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>


<%@ include file="/includes/footer.jsp" %>