<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
Vector    vecSearchResults = new Vector();
String 	  strCardProgramIds[] = null; 
String 	  strCardProgramList = ""; 
String 	  strPlayerTypeId = "";
String 	  strPlayerId = "";
String    strStartDate = "";
String    strEndDate = "";
String    strErrorDate = "";
String    sortString       = "";
if (request.getParameter("search") != null)
{
	try
    {
    	if (request.getParameter("CardProgramList") != null)
    	{
    		strCardProgramIds = request.getParameterValues("CardProgramList");
    		strCardProgramList = CMS.getStringValues(strCardProgramIds);
    	}
    	if (request.getParameter("PlayerTypeId") != null)
    	{
    		strPlayerTypeId = request.getParameter("PlayerTypeId");
    	}
    	if (request.getParameter("PlayerId") != null)
    	{
    		strPlayerId = request.getParameter("PlayerId");
    	}
    	if (request.getParameter("StartDate") != null)
    	{
    		strStartDate = request.getParameter("StartDate");
    	}
    	if (request.getParameter("EndDate") != null)
    	{
    		strEndDate = request.getParameter("EndDate");
    	}
    	if((!strStartDate.equals(""))||(!strEndDate.equals("")))
    	{
	    	strErrorDate = CMS.validateDateRange(strStartDate,strEndDate,SessionData);
    	}
    	if(strErrorDate.equals(""))
    	{
			vecSearchResults = CMS.getReportCardSalesSearch(SessionData, strCardProgramIds, strPlayerTypeId, strPlayerId, strStartDate, strEndDate);
    	}
	}
	catch (Exception e){}  
}	    
%>
<SCRIPT LANGUAGE="JavaScript">
	function openViewCards(strCardProgramId, strExternalCompanyID, strCompanyID, strStartDate, strEndDate, strReportType)
	{
		var sURL = "/support/admin/transactions/view_cards_qty_sales.jsp?CardProgramId=" + strCardProgramId + "&ExternalCompanyID=" + strExternalCompanyID + "&CompanyID=" +  strCompanyID + "&StartDate=" +  strStartDate + "&EndDate=" +  strEndDate + "&ReportType=" + strReportType;
	  	var sOptions = "left=" + (screen.width - (screen.width * 0.8))/2 + ",top=" + (screen.height - (screen.height * 0.4))/2 + ",width=" + (screen.width * 0.8) + ",height=" + (screen.height * 0.5) + ",location=no,menubar=no,resizable=yes";
  		var w = window.open(sURL, "ViewTransactions", sOptions, true);
  		w.focus();
	}
</SCRIPT>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="main"><b><%=Languages.getString("jsp.admin.transactions.manageinventory.card_sales_search",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF" class="main">
				
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>					

					<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
					<tr>	
	    	            <td align=left>
    	    	          	<form target="blank" method=post action="/support/admin/transactions/export_card_sales_search.jsp">
                		    	<input type=hidden name="CardProgramIdsList" value="<%=strCardProgramList%>">
								<input type=hidden name="PlayerTypeId" value="<%=strPlayerTypeId%>">
								<input type=hidden name="PlayerId" value="<%=strPlayerId%>">
								<input type=hidden name="StartDate" value="<%=strStartDate%>">
								<input type=hidden name="EndDate" value="<%=strEndDate%>">
                    			<input type=submit value="<%=Languages.getString("jsp.admin.reports.export",SessionData.getLanguage())%>">
                  			</form>
                		</td>
					</tr>
					<tr>
						<td class="main"><br>
				        <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
      	      			<thead>
      	      			<tr class="SectionTopBorder">
              			<td class=rowhead2>#</td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program_id",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%></td>
              			<% 
	              			if (strAccessLevel.equals(DebisysConstants.ISO))
    	          			{
              			%>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.iso_percent",SessionData.getLanguage()).toUpperCase()%></td>
              			<%
    	          			}
              			%>
              			<% 
	              			if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))&&
	              					(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)))
    	          			{
              			%>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.agent_percent",SessionData.getLanguage()).toUpperCase()%></td>
              			<%
    	          			}
              			%> 
              			<% 
	              			if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))
	              					||(strAccessLevel.equals(DebisysConstants.SUBAGENT))&&
	              					(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)))
    	          			{
              			%>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.subagent_percent",SessionData.getLanguage()).toUpperCase()%></td>
              			<%
    	          			}
              			%>  
              			<% 
	              			if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))
	              			||(strAccessLevel.equals(DebisysConstants.SUBAGENT))||(strAccessLevel.equals(DebisysConstants.REP)))
    	          			{
              			%>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.rep_percent",SessionData.getLanguage()).toUpperCase()%></td>
              			<%
    	          			}
              			%>               			             			             			
              			<td class=rowhead2><%=Languages.getString("jsp.admin.reports.adjustment",SessionData.getLanguage()).toUpperCase()%></td>
            			</tr>
		            	</thead>
		            	<%
                       		int intCounter = 1;
		               		Iterator it = vecSearchResults.iterator();
		               		int intEvenOdd = 1;
		               		while (it.hasNext())
		               		{
		            	   		Vector vecTemp = null;
                    	   		vecTemp = (Vector) it.next();
                    	   		String strLink = "";
                    	   		if(!vecTemp.get(1).toString().equals("8900004"))//If not LOAD
                    	   		{
                    	   			if(!vecTemp.get(2).toString().equals("0"))
                    	   			{
                    	   				strLink = "<a href=\"javascript:void(0);\" onclick=\"openViewCards(" + vecTemp.get(1).toString() + "," + vecTemp.get(9).toString() + "," + vecTemp.get(10).toString() + ",'" + vecTemp.get(11).toString() + "','" + vecTemp.get(12).toString() + "'," + "2" +");\"><span class=\"showLink\"><span class=\"showLink\">" + vecTemp.get(2).toString() + "</span></span></a>";
                    	   			}
                    	   			else
                    	   			{
                    	   				strLink = vecTemp.get(2).toString();
                    	   			}
                    	   		}
                	   			else
                	   			{
                	   				strLink = vecTemp.get(2).toString();
                	   			}
                           		out.println("<tr class=row" + intEvenOdd +">");
                           		out.println("<td>" + intCounter++ + "</td>");
                           		out.println("<td>" + vecTemp.get(0)+ "</td>");
                           		out.println("<td>" + vecTemp.get(1) + "</td>");
                           		out.println("<td>" + strLink + "</td>");
                           		out.println("<td>" + vecTemp.get(3) + "</td>");
    	              			if (strAccessLevel.equals(DebisysConstants.ISO))
        	          			{
    	              				sortString = sortString + ",\"Number\"";
    	              				out.println("<td>" + vecTemp.get(4) + "</td>");
        	          			}
    	              			if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))&&
    	              					(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)))
        	          			{
    	              				sortString = sortString + ",\"Number\"";
    	              				out.println("<td>" + vecTemp.get(5) + "</td>");
        	          			}
    	              			if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))
    	              					||(strAccessLevel.equals(DebisysConstants.SUBAGENT))&&
    	              					(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)))
        	          			{
    	              				sortString = sortString + ",\"Number\"";
    	              				out.println("<td>" + vecTemp.get(6) + "</td>");
        	          			}
    	              			if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))
    	              			||(strAccessLevel.equals(DebisysConstants.SUBAGENT))||(strAccessLevel.equals(DebisysConstants.REP)))
        	          			{
    	              				sortString = sortString + ",\"Number\"";
    	              				out.println("<td>" + vecTemp.get(7) + "</td>"); 
        	          			}
                           		out.println("<td>" + vecTemp.get(8) + "</td>" );
                                out.println("</tr>");
                           		if (intEvenOdd == 1)
                           		{
                      	      		intEvenOdd = 2;
                    	   		}	
                           		else
                          		{
                             		intEvenOdd = 1;
                          		}
	                  		}
                  	  		vecSearchResults.clear();
                 		%> 		            	
                        </table>
						<br>
						<br>
						<SCRIPT type="text/javascript">
  							var stT1 = new SortROC(document.getElementById("t1"),
  							["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "Number", <%= sortString %>, "Number"],0,false,false);
						</SCRIPT>	
						</td>
					</td>    	    	        	            
        	        </tr>							
<%
}
else
{
	if(strErrorDate.equals(""))
	{
	 	out.println("<tr><td bgcolor=#EFF9FE class=main><br><br><font color=ff0000>"  + Languages.getString("jsp.admin.transactions.manageinventory.not_found",SessionData.getLanguage()) + "</font><br><br></td></tr>");
	}
	else
	{
		out.println("<tr><td bgcolor=#EFF9FE class=main><br><br><font color=ff0000>"  + strErrorDate + "</font><br><br></td></tr>");
	}
}
%>							
				
        	        <tr>
	        	        <td bgcolor=#EFF9FE class=main><a href="admin/transactions/inventory_management.jsp"><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.continue",SessionData.getLanguage())%></a><br><br></td>
        	        </tr>
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>