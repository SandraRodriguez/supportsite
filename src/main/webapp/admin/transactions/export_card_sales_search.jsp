<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
Vector    vecSearchResults = new Vector();
String 	  strCardProgramIds[] = null; 
String 	  strCardProgramList = ""; 
String 	  strPlayerTypeId = "";
String 	  strPlayerId = "";
String    strStartDate = "";
String    strEndDate = "";

	try
    {
    	if (request.getParameter("CardProgramIdsList") != null)
    	{
    		strCardProgramList = request.getParameter("CardProgramIdsList");
    		strCardProgramIds = strCardProgramList.split(",");
    	}
    	if (request.getParameter("PlayerTypeId") != null)
    	{
    		strPlayerTypeId = request.getParameter("PlayerTypeId");
    	}
    	if (request.getParameter("PlayerId") != null)
    	{
    		strPlayerId = request.getParameter("PlayerId");
    	}
    	if (request.getParameter("StartDate") != null)
    	{
    		strStartDate = request.getParameter("StartDate");
    	}
    	if (request.getParameter("EndDate") != null)
    	{
    		strEndDate = request.getParameter("EndDate");
    	}
		vecSearchResults = CMS.getReportCardSalesSearch(SessionData, strCardProgramIds, strPlayerTypeId, strPlayerId, strStartDate, strEndDate);
		String sURL = CMS.downloadCardSearchReport(application, vecSearchResults, 8);
	    response.sendRedirect(sURL);
	    return;			
	}
	catch (Exception e){}  
%>
