<jsp:useBean id="task" scope="session"
    class="com.debisys.utils.TaskBean"/>

<HTML>
<HEAD>
    <TITLE>Downloading Large Report</TITLE>
    <% if (task.isRunning()) { %>
        <SCRIPT LANGUAGE="JavaScript">
            setTimeout("location='status.jsp'", 1000);
        </SCRIPT>
    <% } %>
</HEAD>
<BODY style="background-color:#F1F9FE;">
    <p ALIGN="CENTER">
	Please be patient while Generating report
        <% int percent = Integer.valueOf(task.getPercent()*100 / task.getsum());%>
    </p>
    <TABLE WIDTH="60%" ALIGN="CENTER" style="background-color:#F1F9FE;"
            BORDER=1 CELLPADDING=0 CELLSPACING=0> <TR><td>
    <TABLE WIDTH="100%" ALIGN="CENTER" style="background-color:#F1F9FE;"
            BORDER=0 CELLPADDING=0 CELLSPACING=0>
        <TR>
            <% for (int i = 10; i <= percent; i += 10) { %>
                <TD WIDTH="10%" BGCOLOR="#000080">&nbsp;</TD>
            <% } %>
            <% for (int i = 100; i > percent; i -= 10) { %>
                <TD WIDTH="10%">&nbsp;</TD>
            <% } %>
        </TR>
    </TABLE></td></TR>
    </TABLE>
    <TABLE WIDTH="100%" BORDER=0 CELLPADDING=0 CELLSPACING=0>
        <TR>
            <TD ALIGN="CENTER">
       			Records:   <%=task.getPercent()%> / <%= task.getsum()+1 %> <br/>
                <% if (task.isRunning()) { %>
                    Running
                <% } else { %>
                    <% if (task.isCompleted()) {
                     %>
                     <a href="<%=task.getURL()%>" >Click Here to download file</a>
                    <% } else if (!task.isStarted()) { %>
                        Not Started
                    <% } else { %>
                        Stopped
                    <% } %>
                <% } %>
            </TD>
        </TR>
        <TR>
            <TD ALIGN="CENTER">
                <BR>
                <% if (task.isRunning()) { %>
                    <FORM METHOD="GET" ACTION="stop.jsp">
                        <INPUT TYPE="SUBMIT" VALUE="Stop">
                    </FORM>
                <% } %>
            </TD>
        </TR>
    </TABLE>
</BODY>
</HTML>
