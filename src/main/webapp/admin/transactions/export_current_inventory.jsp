<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
Vector    vecSearchResults = new Vector();
String 	  strCardProgramIds[] = null; 
String 	  strCardProgramIdsList = "";
String 	  strReportType = "";	
	try
    {
    	if (request.getParameter("CardProgramIdsList") != null)
    	{
    		strCardProgramIdsList = request.getParameter("CardProgramIdsList");
    	}    	
    	if (request.getParameter("ReportType") != null)
    	{
    		strReportType = request.getParameter("ReportType");
    	}
    	strCardProgramIds = strCardProgramIdsList.split(",");
		vecSearchResults = CMS.getCurrentInventory(SessionData, strCardProgramIds, strReportType);
		String sURL = CMS.downloadCardSearchReport(application, vecSearchResults, 4);
	    response.sendRedirect(sURL);
	    return;		
	}
	catch (Exception e){}  
%>
