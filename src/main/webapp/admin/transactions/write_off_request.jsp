<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.reports.TransactionReport" %>

<%
	int section = 9;
	int section_page = 10;
	String strTransactionNo = "";
	Vector<String> vecSearchResults = new Vector<String>();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<table border="0" cellpadding="0" cellspacing="0" width="450">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle"><b><%=Languages.getString("jsp.includes.menu.write_offs",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
							<tr>
								<td class="main" align="center" nowrap><b><%=Languages.getString("jsp.admin.transactions.write_off_request.title",SessionData.getLanguage())%></b></td>
							</tr>
<%
	if ( request.getParameter("txtTransactionNumber") == null )
	{
%>
							<tr>
								<td class="main" align="center" nowrap><br><%=Languages.getString("jsp.admin.transactions.write_off_request.welcome",SessionData.getLanguage())%><br><br></td>
							</tr>
							<tr>
								<td class="main" align="center">
									<script>
									function ValidateForm()
									{
										if ( document.getElementById("txtTransactionNumber").value == "" )
										{
											alert("<%=Languages.getString("jsp.admin.transactions.write_off_request.transactionrequired",SessionData.getLanguage())%>");
											return false;
										}
										document.getElementById("btnSubmit").disabled = true;
										return true;
									}
									function writeOffWarning()
										{
									        if ( confirm("<%=Languages.getString("jsp.admin.transactions.write_off_request.warning",SessionData.getLanguage())%>") )
									        {
									          form.submit;
									      	}
										}
									</script>
									<form name="mainform" method="post" action="/support/admin/transactions/write_off_request.jsp" onSubmit="return ValidateForm();">
										<table BORDER="0" CELLPADDING="4" CELLSPACING="0" height="1">
											<tr>
												<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.write_off_request.transaction",SessionData.getLanguage())%></td>
												<td class="main" >
													<input type=textbox class="plain" name="txtTransactionNumber" id="txtTransactionNumber" value="" size="24">
													<script>
														document.getElementById("txtTransactionNumber").focus();
													</script>
												</td>
											</tr>
											<tr>
												<input type="hidden" name="search" value="y">
												<td class="main" align="center" colspan="2"><input type=submit name="btnSubmit" id="btnSubmit" value="<%=Languages.getString("jsp.admin.transactions.write_off_request.submit",SessionData.getLanguage())%>"></td>
											</tr>
										</table>
									</form>
								</td>
							</tr>
<%
    }//End of if we need to show the default page
	else
	{//Else if a request has been made
	   String sTrxNo = request.getParameter("txtTransactionNumber");
	   boolean isValidTranxNo = true;	   
	   long lTrxNo = 0;
	   try
	   {
	   	lTrxNo = Long.parseLong(sTrxNo);
	   }
	   catch(NumberFormatException e)
	   {
	   		isValidTranxNo = false;
	   }
	 if(!isValidTranxNo)
	 {	
%>
			<tr><td class=main align=center><br><br><%=Languages.getString("jsp.admin.transactions.write_off_request.invalidtrxnumber",SessionData.getLanguage())%></td></tr>
			<tr>
				<td align=center>
					<br><br>
					<form name="mainform" method="post" action="/support/admin/transactions/write_off_request.jsp">
						<input type=submit value="<%=Languages.getString("jsp.admin.transactions.write_off_request.return",SessionData.getLanguage())%>">
					</form>
				</td>
			</tr>
<%		
	 }
	 else
	 {
		vecSearchResults = TransactionReport.getWriteOffTransaction(SessionData, lTrxNo);
		if ( vecSearchResults.size() > 0 )
		{//If there are records for this Transaction
		
			String siteId = (String)vecSearchResults.get(4); 
			String clerkId = (String)vecSearchResults.get(6);
			String trxTypeId = (String)vecSearchResults.get(1);
			    
			if ( trxTypeId.equals(DebisysConstants.TRX_TYPE_SALE)||trxTypeId.equals(DebisysConstants.TRX_TYPE_RECHARGE))
			{//If the trx has a valid type
			
				String trxTypeDesc = trxTypeId.equals(DebisysConstants.TRX_TYPE_SALE)?Languages.getString("jsp.admin.transactions.write_off_request.sale",SessionData.getLanguage()):Languages.getString("jsp.admin.transactions.write_off_request.recharge",SessionData.getLanguage());
%>
							<tr>
								<td>
									<form name="mainform" id="mainform" method="post" action="/support/admin/transactions/write_off_request.jsp">
										<table cellspacing="0" cellpadding="7">
<%
										if ( request.getParameter("submitted") == null )
											{//If we need to show the Trx data page
%>
											
										   <tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.write_off_request.amount",SessionData.getLanguage())%></b></td>
												<td>&nbsp;</td>
												<td class=main><%=vecSearchResults.get(2)%></td><input type=hidden name="txtAmount" value="<%=vecSearchResults.get(2)%>"></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.write_off_request.trxId",SessionData.getLanguage())%></b></td>
												<td>&nbsp;</td>
												<td class=main><%=vecSearchResults.get(0)%></td><input type=hidden name="txtTrxID" value="<%=vecSearchResults.get(0)%>"></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.write_off_request.trxType",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(5) + " - " + trxTypeDesc%></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.write_off_request.dba",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(3)%></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.write_off_request.siteId",SessionData.getLanguage())%></b></td>
												<td></td>
												<td class=main><%=vecSearchResults.get(4)%><input type=hidden name="txtSiteID" value="<%=vecSearchResults.get(4)%>"></td>
											</tr>
											<tr>
												<td class=main><b><%=Languages.getString("jsp.admin.transactions.write_off_request.reason",SessionData.getLanguage())%></b></td>
												<td></td>
												<td colspan=5>
													<textarea id="txtReason" name="txtReason" cols="30" rows="5" onblur="if (this.value.length > 255) { this.value = this.value.substring(0, 255); }"></textarea>
												</td>
											</tr>
											<tr class=main><td align=left colspan=3><font color=ff0000><%=Languages.getString("jsp.admin.transactions.write_off_request.warningMsg",SessionData.getLanguage())%><br></font></td></tr>
											<tr>
												<script>
														function confirmWriteOff()
														{
															if ( confirm("<%=Languages.getString("jsp.admin.transactions.write_off_request.warning",SessionData.getLanguage())%>") ){
																document.mainform.submit();
															}
														}
												</script>
											
												<td colspan=3 align=center><input type=button id="btnSubmitRequest" value="<%=Languages.getString("jsp.admin.transactions.write_off_request.submit",SessionData.getLanguage())%>" onclick="confirmWriteOff();"></td>
												<td>
													<input type=hidden name="txtTransactionNumber" value="<%=request.getParameter("txtTransactionNumber")%>">
													<input type=hidden name="txtClerkID" value="<%=clerkId%>">
													<input type=hidden name="submitted" value="y">
												</td>
												<td colspan=3 align=center><input type=reset id="btnCancelRequest" value="<%=Languages.getString("jsp.admin.transactions.write_off_request.cancel",SessionData.getLanguage())%>" onclick="history.go(-1);"></td>
											</tr>			
<%
				}//End of if we need to show the data page
				else
				{	//Doing the write off
				     String str_write_off_response = "";
				     String[] write_off_response  = WriteOffProcessor.processWriteOff(application ,
					    															Integer.parseInt(request.getParameter("txtSiteID")),
					    															Integer.parseInt(request.getParameter("txtTransactionNumber")),
					    															request.getParameter("txtClerkID"));
				    	if(write_off_response !=null && write_off_response.length > 1)
				    	{
				    		str_write_off_response = write_off_response[3];
				    	}						
				    	if(!Boolean.valueOf(write_off_response[0])){
				    		if (write_off_response[2].equals(DebisysConstants.WRITE_OFF_ERROR_INVALID_TRX_TYPE)){
				    			str_write_off_response = Languages.getString("jsp.admin.transactions.write_off_request.invalidTrxType",SessionData.getLanguage());
				    		}else if (write_off_response[2].equals(DebisysConstants.WRITE_OFF_ERROR_DUPLICATE_TRX)){
				    			str_write_off_response = Languages.getString("jsp.admin.transactions.write_off_request.duplicateresults",SessionData.getLanguage());
				    		}else if (write_off_response[2].equals(DebisysConstants.WRITE_OFF_ERROR_TRX_NOT_FOUND)){
				    			str_write_off_response = Languages.getString("jsp.admin.transactions.write_off_request.noresults",SessionData.getLanguage());
				    		}else if (write_off_response[2].equals(DebisysConstants.WRITE_OFF_ERROR_GROUPED_ISO)){
				    			str_write_off_response = Languages.getString("jsp.admin.transactions.write_off_request.groupedISO",SessionData.getLanguage());
				    		}
				    		str_write_off_response = "<font color=ff0000>"+Languages.getString("jsp.admin.transactions.write_off_request.failedWriteOff",SessionData.getLanguage())
				    		+"<br>"+str_write_off_response+"</font>";	    	
				    	}else{
				    		Boolean insertWriteOffResult = TransactionReport.insertWriteOff(Integer.parseInt(request.getParameter("txtTransactionNumber")), 
																			Integer.parseInt(write_off_response[1]), 
																			request.getParameter("txtReason"), 
																			SessionData.getProperty("username"));
				    		str_write_off_response = "<font color=009933>"+Languages.getString("jsp.admin.transactions.write_off_request.successWriteOff",SessionData.getLanguage());
				    		str_write_off_response = str_write_off_response + "<b> "+ Languages.getString("jsp.admin.transactions.write_off_request.trxId",SessionData.getLanguage()) + "</b> "+ write_off_response[1] +"</font>";
				    	}

					if(str_write_off_response!=null && str_write_off_response.length()>0){						
																	
						out.print("<tr><td colspan=7 align=center class=main>" + str_write_off_response + "</td></tr>");
					}											
%>											
											<tr><td colspan=7 align=center><br><br><input type=submit value="<%=Languages.getString("jsp.admin.transactions.write_off_request.return",SessionData.getLanguage())%>"></td></tr>
<%
				}//End of else show the final result page
%>
										</table>
									</form>
								</td>
							</tr>
<%
			}//End of if is a valid type
			else
			{//Else show the message that is not a valid type of transaction
%>
              <tr><td class=main align=center><br><br><%=Languages.getString("jsp.admin.transactions.write_off_request.invalidTrxType",SessionData.getLanguage())%></td></tr>
              <tr>
                <td align=center>
                  <br><br>
                  <form name="mainform" method="post" action="/support/admin/transactions/write_off_request.jsp">
                    <input type=submit value="<%=Languages.getString("jsp.admin.transactions.write_off_request.return",SessionData.getLanguage())%>">
                  </form>
                </td>
              </tr>
<%
			}//End of else show the message that there are duplicates
		}//End of if there are records for this Transaction
		else
		{//Else show the message of no results
%>
							<tr><td class=main align=center><br><br><%=Languages.getString("jsp.admin.transactions.write_off_request.noresults",SessionData.getLanguage())%></td></tr>
							<tr>
								<td align=center>
									<br><br>
									<form name="mainform" method="post" action="/support/admin/transactions/write_off_request.jsp">
										<input type=submit value="<%=Languages.getString("jsp.admin.transactions.write_off_request.return",SessionData.getLanguage())%>">
									</form>
								</td>
							</tr>
<%
		}//End of else show the message of no results
	 }//End of is trx a valid number
	}//End of else if a request has been made
%>
						</table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr><td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td></tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>
