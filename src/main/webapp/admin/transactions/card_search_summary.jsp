<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
Vector    vecSearchResults = new Vector();
String    sortString       = "";
String 	  strCardProgramID = ""; 
String 	  strSerialNumber  = ""; 
String 	  strCardNumber    = ""; 
String 	  strCardTypeID    = ""; 
String 	  strStatusID      = ""; 
String 	  strCompanyRoleID = ""; 
String 	  strCompanyID     = ""; 
String 	  strCreationDate  = "";

if (request.getParameter("search") != null)
{
	try
    {
    	if (request.getParameter("PlayerTypeId") != null)
    	{
    		strCompanyRoleID = request.getParameter("PlayerTypeId");
    	}
    	if (request.getParameter("CardProgramNameId") != null)
    	{
    		strCardProgramID = request.getParameter("CardProgramNameId");
    	}
    	if (request.getParameter("CardProgramTypeId") != null)
    	{
    		strCardTypeID = request.getParameter("CardProgramTypeId");
    	}
    	if (request.getParameter("StatusId") != null)
    	{
    		strStatusID = request.getParameter("StatusId");
    	}
    	if (request.getParameter("SerialNumber") != null)
    	{
    		strSerialNumber = request.getParameter("SerialNumber");
    	}
    	if (request.getParameter("CardNumber") != null)
    	{
    		strCardNumber = request.getParameter("CardNumber");
    	}
    	if (request.getParameter("PlayerId") != null)
    	{
    		strCompanyID = request.getParameter("PlayerId");
    	}    	
    	if (request.getParameter("ISOReceiptDate") != null)
    	{
    		strCreationDate = request.getParameter("ISOReceiptDate");
    	}	
		vecSearchResults = CMS.getReportCardSearch(SessionData, strCardProgramID, strSerialNumber, strCardTypeID, strStatusID, strCompanyRoleID, strCompanyID, strCreationDate);
	}
	catch (Exception e){}  
}	    
%>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="main"><b><%=Languages.getString("jsp.admin.transactions.manageinventory.card_search",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFf">
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>						
				<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
				<tr>
                <td align=left>
                  <form target="blank" method=post action="/support/admin/transactions/export_card_search.jsp">
                    <input type=hidden name="PlayerTypeId" value="<%=strCompanyRoleID%>">
                    <input type=hidden name="CardProgramNameId" value="<%=strCardProgramID%>">
                    <input type=hidden name="CardProgramTypeId" value="<%=strCardTypeID%>">
                    <input type=hidden name="StatusId" value="<%=strStatusID%>">
                    <input type=hidden name="SerialNumber" value="<%=strSerialNumber%>">
                    <input type=hidden name="CardNumber" value="<%=strCardNumber%>">
                    <input type=hidden name="PlayerId" value="<%=strCompanyID%>">
                    <input type=hidden name="ISOReceiptDate" value="<%=strCreationDate%>">
                    <input type=submit value="<%=Languages.getString("jsp.admin.reports.export",SessionData.getLanguage())%>">
                  </form>
                </td>
					</tr>
					<tr>						
						<td class="main"><br>
				        <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
      	      			<thead>
      	      			<tr class="SectionTopBorder">
	      	      			<td class=rowhead2>#</td>
			            	<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_type",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_name",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program_type",SessionData.getLanguage()).toUpperCase()%></td>              				
             				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.status",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.serial_number",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.iso_receipt_date",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("?jsp.admin.transactions.manageinventory.last_assigned_date",SessionData.getLanguage()).toUpperCase()%></td>


            			</tr>
		            	</thead>
		            	<%
                       		int intCounter = 1;
		               		Iterator it = vecSearchResults.iterator();
		               		int intEvenOdd = 1;
		               		while (it.hasNext())
		               		{
		            	   		Vector vecTemp = null;
                    	   		vecTemp = (Vector) it.next();
                           		out.println("<tr class=row" + intEvenOdd +">" +
                                		"<td>" + intCounter++ + "</td>" +
                                		"<td>" + vecTemp.get(0)+ "</td>" +
                                		"<td>" + vecTemp.get(1) + "</td>" +
                                		"<td>" + vecTemp.get(2) + "</td>" +
                                		"<td>" + vecTemp.get(3) + "</td>" +
                                		"<td>" + vecTemp.get(4) + "</td>" +
                                		"<td>" + vecTemp.get(5) + "</td>" +
                                		"<td>" + vecTemp.get(6) + "</td>" +
                                		"<td>" + vecTemp.get(7) + "</td>" +
                               			"</tr>");
                           		if (intEvenOdd == 1)
                           		{
                      	      		intEvenOdd = 2;
                    	   		}	
                           		else
                          		{
                             		intEvenOdd = 1;
                          		}
	                  		}
                  	  		vecSearchResults.clear();
                 		%>         			  						
                        </table>
						<br>
						<br>
<SCRIPT type="text/javascript">
  <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Date", "Date",],0,false,false);
  -->
</SCRIPT>						
						</td>
						</td>    	    	        	            
        	        </tr>	
<%
}
else
{
 out.println("<tr><td bgcolor=#FFFFFF class=main><br><br><font color=ff0000>"  + Languages.getString("jsp.admin.transactions.manageinventory.not_found",SessionData.getLanguage()) + "</font><br><br></td></tr>");
}
%>						
        	        <tr>
	        	        <td bgcolor=#FFFFFF class=main><a href="admin/transactions/inventory_management.jsp"><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.continue",SessionData.getLanguage())%></a><br><br></td>
        	        </tr>					
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>
