<%int section = 9;
int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
String strControlNo = "";
String strPin = "";
String strSerial = "";
String strTransactionNo = "";
if (request.getParameter("submitted") != null)
{
	try
    {
	    if (request.getParameter("submitted").equals("y"))
		{
	    	if (request.getParameter("ControlNo") != null)
	    	{
	    		strControlNo = request.getParameter("ControlNo");
	    	}
	    	if (request.getParameter("Pin") != null)
	    	{
	    		strPin = request.getParameter("Pin");
	    	}	    		    	
	    	if (request.getParameter("TransactionNo") != null)
	    	{
	    		strTransactionNo = request.getParameter("TransactionNo");
	    	}	    	
		}
	}
	catch (Exception e){}  
}	    
%>
<SCRIPT LANGUAGE="JavaScript">
	var count = 0
	var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

	function scroll()
	{
  		document.mainform.showRep.disabled = true;
		document.mainform.showRep.value = iProcessMsg[count];
  		count++
  		if (count = iProcessMsg.length) count = 0
  		setTimeout('scroll()', 150);
	}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle"><b><%=Languages.getString("jsp.includes.menu.pinreturn_request",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="left" valign="top" bgcolor="#FFFFFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%" align="left">
					<tr>
						<td class="main" align="left" nowrap><b><%=Languages.getString("jsp.admin.transactions.pinreturn_request.title",SessionData.getLanguage())%></b></td>
					</tr>						
					<tr>
						<td class="main" align="left" nowrap><br><%=Languages.getString("jsp.admin.transactions.pinreturn_request.welcome",SessionData.getLanguage())%><br><br></td>
					</tr>				
					<tr>
						<td class="main">
						<form name="mainform" method="post" action="/support/admin/transactions/pinreturn_request.jsp" onSubmit="scroll();">
							<input type="hidden" name="submitted" value="y">
							<INPUT TYPE="hidden" ID="ControlNo" NAME="ControlNo" VALUE="<%=strControlNo%>">
							<INPUT TYPE="hidden" ID="Pin" NAME="Pin" VALUE="<%=strPin%>">
							<INPUT TYPE="hidden" ID="TransactionNo" NAME="TransactionNo" VALUE="<%=strTransactionNo%>">						
						<table BORDER="0" CELLPADDING="4" CELLSPACING="0" height="1">
							<tr>
								<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.pinreturn_request.control",SessionData.getLanguage())%></td>
								<td class="main"><input class="plain" name="controlNo" id="controlNo"value="" size="24"></td>								
							</tr>
							<tr>
								<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.pinreturn_request.pin",SessionData.getLanguage())%></td>
								<td class="main" ><input class="plain" name="pin" id="pin" value="" size="24"></td>								
							</tr>
							<tr>
								<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.pinreturn_request.transaction",SessionData.getLanguage())%></td>
								<td class="main" ><input class="plain" name="transactionNo" id="transactionNo" value="" size="24"></td>								
							</tr>																					
							<tr>
								<input type="hidden" name="search" value="y">
								<td class="main" align="left" colspan="2"><input type=submit name="showRep" id="showRep" value="<%=Languages.getString("jsp.admin.transactions.pinreturn_request.submit",SessionData.getLanguage())%>" onclick="showReport();"></td>
            	    		    <SCRIPT>
            	    		    	function showReport()
                                	{
										document.getElementById('ControlNo').value = document.getElementById('controlNo').value; 
										document.getElementById('Pin').value = document.getElementById('pin').value; 
										document.getElementById('TransactionNo').value = document.getElementById('transactionNo').value;
                                		document.mainform.action = '/support/admin/transactions/pinreturn_request_result.jsp';
                                		document.mainform.submit();
                                	}
                                </SCRIPT>									
							</tr>														
						</table>
						</form>
						</td>
					</tr>
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>
