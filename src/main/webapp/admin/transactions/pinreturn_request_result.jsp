<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.reports.TransactionReport" %>

<%int section = 9;
int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
Vector    vecSearchResults = new Vector();
String 	  strControlNo = ""; 
String 	  strPin = "";
String 	  strSerial= "";
String    strTransactionNo = "";
if (request.getParameter("search") != null)
{
	try
    {
    	if (request.getParameter("controlNo") != null)
    	{
    		strControlNo = request.getParameter("controlNo");
    	}
    	if (request.getParameter("pin") != null)
    	{
    		strPin = request.getParameter("pin");
    	}
    	if (request.getParameter("transactionNo") != null)
    	{
    		strTransactionNo = request.getParameter("transactionNo");
    	}
		
    	vecSearchResults = TransactionReport.getPINForRequests(SessionData, strControlNo, strPin, strTransactionNo);
	}
	catch (Exception e){}  
}	    
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

            <TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
              <TBODY>
              <TR>
                <TD align=left width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=images/top_blue.gif width="3000">&nbsp;<B><%=Languages.getString("jsp.admin.ach.summary.title",SessionData.getLanguage()).toUpperCase()%></B></TD>
                <TD align=right width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_right_blue.gif" width=18></TD></TR>
                  <TR>
                <TD colSpan=3>
                  <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
                  bgColor=#fffcdf class="backceldas">
                    <TBODY>
                    <TR class="backceldas">
                      <TD width=1 bgColor=#003082><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top align=middle bgColor=#ffffff>
                        <TABLE width="100%" border=0 
                        align=center cellPadding=2 cellSpacing=0 class="backceldas">
                          <TBODY>
                          <TR>
                            <TD width=18>&nbsp;</TD>
                            <TD class="main"><BR><BR>

<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>					

					<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
					<tr>
						<td class="main"><br><%=Languages.getString("jsp.admin.transactions.pinreturn_request.instructions",SessionData.getLanguage())%></td><br>
					</tr>
					<tr>
						<td class="main"><br>
				        <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
      	      			<thead>
      	      			<tr class="SectionTopBorder">     
              			<td class=rowhead2>#</td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.pinreturn_request.product",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.pinreturn_request.product_name",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.pinreturn_request.control_title",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.pinreturn_request.merchant_dba",SessionData.getLanguage()).toUpperCase()%></td>
<%
if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
{
%>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.pinreturn_request.client",SessionData.getLanguage()).toUpperCase()%></td>
<%
}
%>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.pinreturn_request.pin_title",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.pinreturn_request.amount",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.pinreturn_request.activation_date",SessionData.getLanguage()).toUpperCase()%></td>              			
             			</tr>
		            	</thead>   
		            	<%
                       		int intCounter = 1;
		               		Iterator it = vecSearchResults.iterator();
		               		int intEvenOdd = 1;
		               		String sClient = ""; 
		               		
		               		while (it.hasNext())
		               		{
		            	   		Vector vecTemp = null;
                    	   		vecTemp = (Vector) it.next();
                    	   		String strLink = "";
								strLink = "<a href=\"/support/admin/transactions/pinreturn_request_add.jsp?ControlNo="+ vecTemp.get(1).toString() + "&ProductId=" + vecTemp.get(0).toString() +"\"><span class=\"showLink\"><span class=\"showLink\">" + vecTemp.get(1).toString() + "</span></span></a>";
								if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
								{
									sClient = "<td>" + vecTemp.get(7) + "</td>";
								}
                           		out.println("<tr class=row" + intEvenOdd +">");
                           		out.println("<td>" + intCounter++ + "</td>");
                           		out.println("<td>" + vecTemp.get(0)+ "</td>");
                           		out.println("<td>" + vecTemp.get(5) + "</td>");
                           		out.println("<td>" + strLink + "</td>");
                           		out.println("<td>" + vecTemp.get(6) + "</td>");                           		
                           		out.println(sClient);                           		
                           		out.println("<td>" + vecTemp.get(2) + "</td>");
                           		out.println("<td>" + vecTemp.get(3) + "</td>");
                           		out.println("<td>" + vecTemp.get(4) + "</td>");
                                out.println("</tr>");
                           		if (intEvenOdd == 1)
                           		{
                      	      		intEvenOdd = 2;
                    	   		}	
                           		else
                          		{
                             		intEvenOdd = 1;
                          		}
	                  		}
                  	  		vecSearchResults.clear();
                 		%> 		            	
                        </table>
						<br>
						<br>
<%
String sSortClient = "";
if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
{
	sSortClient = ", \"CaseInsensitiveString\"";
}
%>
						<SCRIPT type="text/javascript">
  							var stT1 = new SortROC(document.getElementById("t1"),
  							["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"<%=sSortClient%>],0,false,false);
						</SCRIPT>	
						</td>
					</td>    	    	        	            
        	        </tr>							
<%
}
else
{
	out.println("<tr><td class=main width=800><br><br><font color=ff0000>"  + Languages.getString("jsp.admin.transactions.pinreturn_request.not_found",SessionData.getLanguage()) + "</font><br><br></td></tr>");
}
%>							
				
                            
                   </td>
                        <td width="18">&nbsp;<a href="admin/transactions/pinreturn_request.jsp"><%=Languages.getString("jsp.admin.transactions.pinreturn_request.continue",SessionData.getLanguage())%></a></td>
                    </tr>
                    </table>
                        <div align=right class="backceldas"><font size="1"></div>
                </td>
                <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	         </tr>
	         <tr>
		            <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
            </tr>
            </table>
        </td>
    </tr>
</table>                            


<%@ include file="/includes/footer.jsp"%>