<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle"><b><%=Languages.getString("jsp.includes.menu.manageinventory",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="left" valign="top" bgcolor="#FFFfFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%"
					align="center">
					<tr>
						<td width="18">&nbsp;</td>
						<td class="main"><br>
						<br>
						<table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
							<% 
								if(!strAccessLevel.equals(DebisysConstants.MERCHANT))
								{
							%>
							<tr>
								<td VALIGN="top" WIDTH="376" height="28">  <font face="Arial, Helvetica, sans-serif" style="font-size: 13px; font-weight: 700" color="#2C0973"><a
									href="admin/transactions/inventory_assignment.jsp"
									style="text-decoration: none"> - <%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment",SessionData.getLanguage())%></a></font>
								</td>
							</tr>
							<% 
								}
							%>
							
							<tr>
								<td VALIGN="top" WIDTH="376" height="28">  <font face="Arial, Helvetica, sans-serif" style="font-size: 13px; font-weight: 700" color="#2C0973"><a
									href="admin/transactions/card_search.jsp"
									style="text-decoration: none"> - <%=Languages.getString("jsp.admin.transactions.manageinventory.card_search",SessionData.getLanguage())%></a></font>
								</td>
							</tr>
							<tr>
								<td VALIGN="top" WIDTH="376" height="28">  <font face="Arial, Helvetica, sans-serif" style="font-size: 13px; font-weight: 700" color="#2C0973"><a
									href="admin/transactions/card_sales_search.jsp"
									style="text-decoration: none"> - <%=Languages.getString("jsp.admin.transactions.manageinventory.card_sales_search",SessionData.getLanguage())%></a></font>
								</td>
							</tr>
							<tr>
								<td VALIGN="top" WIDTH="376" height="28">  <font face="Arial, Helvetica, sans-serif" style="font-size: 13px; font-weight: 700" color="#2C0973"><a
									href="admin/transactions/current_inventory.jsp"
									style="text-decoration: none"> - <%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory",SessionData.getLanguage())%></a></font>
								</td>
							</tr>														
						</table>
						<br>
						<br>
						</td>
						<td width="18">&nbsp;</td>
					</tr>

				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>
