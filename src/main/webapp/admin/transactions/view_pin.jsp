<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 com.debisys.languages.Languages,
                 java.util.*" %>
<%
int section = 3;
int section_page = 8;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
 if ( request.getParameter("trans_id") != null )
 {
  Vector vResult = TransactionSearch.getViewPinInfo(request.getParameter("trans_id"), SessionData);
   
     
%>
<HTML>
<HEAD>
    <LINK HREF="../../default.css" TYPE="text/css" REL="stylesheet">
    <TITLE><%=Languages.getString("jsp.admin.transactions.viewpin.pininfo",SessionData.getLanguage())%></TITLE>
</HEAD>
<BODY BGCOLOR="#ffffff">
<TABLE WIDTH="100%" HEIGHT="100%" STYLE="height:100%;">
    <TR>
        <TD ALIGN="center" VALIGN="middle">
            <TABLE CELLSPACING="1" CELLPADDING="1" BORDER="0">
                <TR>
                    <TD CLASS="rowhead2" COLSPAN=2 ALIGN=center STYLE="TEXT-ALIGN:center;"><B><%=Languages.getString("jsp.admin.transactions.viewpin.pininfo",SessionData.getLanguage()).toUpperCase()%></B></TD>
                </TR>
                <%if (vResult != null && vResult.size() > 0){%>
                <TR>
                    <TD CLASS="rowhead2"><B><%=Languages.getString("jsp.admin.transactions.viewpin.controlnumber",SessionData.getLanguage()).toUpperCase()%>:</B></TD>
                    <TD CLASS="row2"><B><%=vResult.get(0).toString()%></B></TD>
                </TR>
                <TR>
                    <TD CLASS="rowhead2"><B><%=Languages.getString("jsp.admin.transactions.viewpin.pinnumber",SessionData.getLanguage()).toUpperCase()%>:</B></TD>
                    <TD CLASS="row1"><%
                  //if activation_date is null then pin is live and can be sold so do not output pin
                  if(vResult.get(5).toString().equals(""))
                  {
                   out.print("(hidden)");
                  }
                  else
                  {
                    out.print(vResult.get(1).toString());
                  }
                    
                    %></TD>
                </TR>
                <TR>
                    <TD CLASS="rowhead2"><B><%=Languages.getString("jsp.admin.transactions.viewpin.productdescription",SessionData.getLanguage()).toUpperCase()%>:</B></TD>
                    <TD CLASS="row2"><%=vResult.get(2).toString()%></TD>
                </TR>
                <TR>
                    <TD CLASS="rowhead2"><B><%=Languages.getString("jsp.admin.transactions.viewpin.productamount",SessionData.getLanguage()).toUpperCase()%>:</B></TD>
                    <TD CLASS="row1"><%=NumberUtil.formatAmount(vResult.get(3).toString())%></TD>
                </TR>
                <TR>
                    <TD CLASS="rowhead2"><B><%=Languages.getString("jsp.admin.transactions.viewpin.terminalresponse",SessionData.getLanguage()).toUpperCase()%>:</B></TD>
                    <TD CLASS="main"><%=((vResult.get(4).toString().length() > 0)?vResult.get(4).toString():Languages.getString("jsp.admin.transactions.viewpin.noresponse",SessionData.getLanguage()))%></TD>
                </TR>
                <TR>
                    <TD CLASS="main" COLSPAN=2 ALIGN="center"><DIV ID="divPrintBtn" STYLE="display:inline;"><BR><BR><A HREF="javascript:void(0);" ONCLICK="document.getElementById('divPrintBtn').style.display='none';window.print();document.getElementById('divPrintBtn').style.display='inline';"><%=Languages.getString("jsp.admin.transactions.viewpin.print",SessionData.getLanguage())%></A></DIV></TD>
                </TR>
                <%}
                else
                {
                  out.println("<tr><td colspan=2>No information available.</td></tr>");
                }
                %>
            </TABLE>
        </TD>
    </TR>
</TABLE>
</BODY>
</HTML>
<%
 }
%>