<%@ page import="com.debisys.terminalTracking.CProvider"%>
<%@ page import="com.debisys.terminalTracking.CTerminalPart"%>
<%@ page import="com.debisys.terminalTracking.CSimCard"%>
<%@ page import="com.debisys.terminalTracking.CPhysicalTerminal"%>
<%@ page import="com.debisys.terminalTracking.CTerminalStatus"%>
<%@ page import="com.debisys.terminalTracking.CTerminalPartStatus"%>
<%@ page import="java.util.Calendar" %>


<%
int section = 9;
int section_page = 5;

//Field values
String imsi = "";
String phoneNumber = "";
String serviceProvider = "";
String activationDate = "";
String lotNumber = "";
String contractNumber = "";
String contractDate = "";
String currentStatus = "";
String currentStatusValue = "";
String newStatus = "";
String changeReason = "";
String partId = ""; 
String strUpdateErrror = "";



%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>

<%
	boolean submittedForUpdate = false;
	CSimCard currentCard = new CSimCard();
	partId = (request.getParameter("partId") != null)?request.getParameter("partId"):"";
	
	if(partId.equals(""))
	{
		partId = request.getParameter("partIdH");
	}
	currentCard.loadSimCard(Long.valueOf(partId));
        currentStatusValue = String.valueOf(currentCard.getStatus().getPartStatusId());
	
	String strMessage = request.getParameter("message");
	if (strMessage == null)
	{
	  strMessage = "";
	}
        
        //Verificar si el contenido de la forma puede ser editado
        String disableControl = "";
        if(currentCard.getStatus().getPartStatusCode().equals(CTerminalPartStatus.ASSIGN_CODE))
        {
                Long terminalParentId = currentCard.getParentTerminal();	
                if(terminalParentId != null)
                {
                        CPhysicalTerminal parentTerminal = new CPhysicalTerminal();
                        if(parentTerminal.loadTerminal(terminalParentId.longValue()))
                        {
                                CTerminalStatus parentSatatus = parentTerminal.getCurrentStatus();
                                if(parentSatatus.getTerminalSatusCode().equals(CTerminalStatus.TO_MERCHANT_CODE) || parentSatatus.getTerminalSatusCode().equals(CTerminalStatus.IN_MERCHANT_CODE))
                                {
                                        //Se coloca el valor deshabilitado, para hacer que los controles de la interfaz no sean editables
                                        disableControl = "disabled";
                                }
                        }
                }
        }        

	if (request.getParameter("simEditSubmitted") != null)
	{
	  try
	  {
	    if (request.getParameter("simEditSubmitted").equals("y"))
	    {
	    	submittedForUpdate = true;
	    	// Save current values
	    	imsi = (request.getParameter("mobileSubscriberIdH") != null)?request.getParameter("mobileSubscriberIdH"):"";
	    	phoneNumber = (request.getParameter("phoneNumberH") != null)?request.getParameter("phoneNumberH"):"";
	    	serviceProvider = (request.getParameter("providerIdH") != null)?request.getParameter("providerIdH"):"";
	    	activationDate = (request.getParameter("activationDateH") != null)?request.getParameter("activationDateH"):"";
	    	lotNumber = (request.getParameter("lotNumberH") != null)?request.getParameter("lotNumberH"):"";
	    	contractNumber = (request.getParameter("contractNumberH") != null)?request.getParameter("contractNumberH"):"";
	    	contractDate = (request.getParameter("contractDateH") != null)?request.getParameter("contractDateH"):"";
	    	currentStatus = (request.getParameter("partCurrentStatusIdH") != null)?request.getParameter("partCurrentStatusIdH"):"";
                currentStatusValue = (request.getParameter("currentStatusValueH") != null)?request.getParameter("currentStatusValueH"):"";
	    	newStatus = (request.getParameter("partNewStatusIdH") != null)?request.getParameter("partNewStatusIdH"):"";
	    	changeReason = (request.getParameter("changeReasonH") != null)?request.getParameter("changeReasonH"):"";
	    	
	    	// If phone number has not changed or  changed, and the new one is not being used a flag for edit is set
	    	boolean bPhoneAvalable = false;
	    	if((phoneNumber != null) && (phoneNumber.equalsIgnoreCase(currentCard.getPhoneNumber())) ||
	    		((!phoneNumber.equalsIgnoreCase(currentCard.getPhoneNumber())) && (CSimCard.getPhonesInUse(phoneNumber) == 0)))
	    	{
	    		bPhoneAvalable = true;
	    	}
	    	else // Phone already in use
	    	{
	    		strUpdateErrror = Languages.getString("jsp.admin.transactions.inventorySims.PhoneAlreadyInUse",SessionData.getLanguage());
	    	}

	    	
    		// If IMSI has changed and is not being used or if it didn't change a flag is set
    		boolean bImsiAvailable = false;
	    	if((imsi != null) && ((imsi.equalsIgnoreCase(currentCard.getMobileSuscriberId())) ||
    			((!imsi.equalsIgnoreCase(currentCard.getMobileSuscriberId())) && (CSimCard.partExists(currentCard.getPartType().getPartTypeId(),imsi) == null))))
	    	{
	    		bImsiAvailable = true;
	    	}
	    	else // imsi already in use
	    	{
	    		strUpdateErrror = Languages.getString("jsp.admin.transactions.inventorySims.ImsiAlreadyInUse",SessionData.getLanguage());
	    	}
	    	
	    	
	    	boolean bParentTerminalMoved = false;
	    	CTerminalPartStatus newObjStatus = null;
	    	if((newStatus != null) && (!newStatus.equals("")))
	    	{
	    		newObjStatus =  CTerminalPartStatus.getPartStatusById(Long.valueOf(newStatus));
	    	}
	    	CPhysicalTerminal parentTerminal = new CPhysicalTerminal();
	    	Long parentTerminalId = currentCard.getParentTerminal();
	    	if((parentTerminalId == null) || (!parentTerminal.loadTerminal(parentTerminalId)))
	    	{
	    		parentTerminal = null;
	    	}
	    	if((newObjStatus != null) && (parentTerminal != null) && 
	    		(currentCard.getStatus().getPartStatusId() != newObjStatus.getPartStatusId()) &&
	    		((parentTerminal.getCurrentStatus().getTerminalSatusCode().equalsIgnoreCase(CTerminalStatus.IN_MERCHANT_CODE) ||
	    		(parentTerminal.getCurrentStatus().getTerminalSatusCode().equalsIgnoreCase(CTerminalStatus.TO_MERCHANT_CODE)))))
    		{
	    		bParentTerminalMoved = true;
	    		strUpdateErrror = Languages.getString("jsp.admin.transactions.inventorySimsEdit.ParentTerminalMoved",SessionData.getLanguage());
    		}
	    	if(bPhoneAvalable && bImsiAvailable && !bParentTerminalMoved)
	    	{
		    	currentCard.setMobileSuscriberId((!imsi.equals(""))?imsi:null);
		    	currentCard.setPhoneNumber((!phoneNumber.equals(""))?phoneNumber:null);
		    	CProvider currentProvider = CProvider.getProviderById(Long.valueOf(serviceProvider));
		    	currentCard.setProvider(currentProvider);
		    	currentCard.setActivationDate((activationDate != null)?activationDate:Calendar.getInstance().toString());
		    	currentCard.setLotNumber((lotNumber != null)?lotNumber:"");
		    	currentCard.setContractNumber((contractNumber!= null)?contractNumber:"");
		    	currentCard.setContractDate((contractDate != null)?contractDate:Calendar.getInstance().toString());
		    	currentCard.setStatus(newObjStatus);
		    	currentCard.setChangeReason((changeReason != null)?changeReason:"");
		    	if(currentCard.updatePart(SessionData.getProperty("ref_id")))
		    	{
		    		response.sendRedirect("inventorySims_edit.jsp?partId=" + partId+ "&message=" + strMessage);	
		    	}
		    	else
		    	{
		    		strUpdateErrror = Languages.getString("jsp.admin.transactions.inventorySims.PhoneAlreadyInUse",SessionData.getLanguage());
			        response.sendRedirect("inventorySims_edit.jsp?partId=" + partId+ "&message=" + strMessage);	        
		    	}
	    	}
	    }
	    else
	    {
	    	
	    } // END if (request.getParameter("simEditSubmitted").equals("y"))
	  }
	  catch (Exception e){
	  }
	}
%>

<%@ include file="/includes/header.jsp"%>
<script SRC="includes/funcionesValidacion.js" type="text/javascript"> </script>
<script type="text/javascript">   
function saveCurrentFieldValues()
{
	document.getElementById('mobileSubscriberIdH').value = document.getElementById('txtImsi').value;
	document.getElementById('phoneNumberH').value = document.getElementById('txtPhoneNumber').value;
	document.getElementById('providerIdH').value = document.getElementById('ddlProvider').value;
	document.getElementById('activationDateH').value = document.getElementById('txtActivationDate').value;
	document.getElementById('lotNumberH').value = document.getElementById('txtLotNumber').value;
	document.getElementById('partCurrentStatusIdH').value = document.getElementById('txtCurrentStatus').value;
	document.getElementById('partNewStatusIdH').value = document.getElementById('ddlNewStatus').value;
	document.getElementById('changeReasonH').value = document.getElementById('txtChangeReason').value;	
	document.getElementById('contractNumberH').value = document.getElementById('txtContract').value;
	document.getElementById('contractDateH').value = document.getElementById('txtContractDate').value;	
}
        
function cancelar(form) {
      form.action = "admin/transactions/inventorySims_search.jsp";
      form.submit(); 
    }               
        
function enviarDatos(form) 
{
    if(validacion(form))
    {
      	saveCurrentFieldValues();
        form.submit();
    }
}   

function validacion(form)
   {
     var alpha = 0;
     var errors = "";
     
     if ( document.getElementById('txtImsi').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyImsi",SessionData.getLanguage())%>\n';
        alpha++;
     } 
     
     if ( document.getElementById('ddlProvider').selectedIndex==null || document.getElementById('ddlProvider').selectedIndex==-1 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyCarrier",SessionData.getLanguage())%>\n';
        alpha++;
     }      
     
     if ( document.getElementById('txtActivationDate').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyActivationDate",SessionData.getLanguage())%>\n';
        alpha++;
     }  
     
     if ( document.getElementById('txtLotNumber').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyLot",SessionData.getLanguage())%>\n';
        alpha++;
     }     
     
     if ( document.getElementById('txtContract').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyContract",SessionData.getLanguage())%>\n';
        alpha++;
     } 
     
     if ( document.getElementById('txtContractDate').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.emptyContractDate",SessionData.getLanguage())%>\n';
        alpha++;
     }  
     
     if ((document.getElementById('currentStatusValueH').value != document.getElementById('ddlNewStatus').value) && document.getElementById('txtChangeReason').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyChangeReason",SessionData.getLanguage())%>\n';
        alpha++;
     }     
          
    if (alpha != 0) {
        alert(errors);
        return false;
    }
    else {	  	
        return true;
    }      
   }

</script>


<table border="0" cellpadding="0" cellspacing="0" width="60%">
	<%
        if (strMessage.equals("2"))
        {
          out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + Languages.getString(
                  "jsp.admin.transactions.inventorySimsEdit.terminal_added_success",SessionData.getLanguage()) + "</td></tr>");
        }
        else if (strMessage.equals("3"))
        {
          out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + Languages.getString(
                  "jsp.admin.transactions.inventorySimsEdit.terminal_updated_success",SessionData.getLanguage()) + "</td></tr>");
        }
		if (!strUpdateErrror.equals(""))
		{
		  out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + strUpdateErrror + "</td></tr>");
		}
%>

	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img
			src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.transactions.inventorySimsEdit.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img
			src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan=2>
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%"
					align="center">
					<form name="mainform" method="post"  action="admin/transactions/inventorySims_edit.jsp">
					<input type="hidden" id="simEditSubmitted" name="simEditSubmitted" value="y"> 
					<input type="hidden" id="mobileSubscriberIdH" name="mobileSubscriberIdH"value="<%=imsi%>"> 
					<input type="hidden" id="phoneNumberH"	name="phoneNumberH" value="<%=phoneNumber%>"> 
					<input type="hidden" id="providerIdH" name="providerIdH" value="<%=serviceProvider%>"> 
					<input type="hidden" id="activationDateH" name="activationDateH" value="<%=activationDate%>">
					<input type="hidden" id="lotNumberH" name="lotNumberH" value="<%=lotNumber%>">
					<input type="hidden" id="contractNumberH" name="contractNumberH" value="<%=contractNumber%>"> 
					<input type="hidden" id="contractDateH"	name="contractDateH" value="<%=contractDate%>"> 
					<input type="hidden" id="partCurrentStatusIdH" name="partCurrentStatusIdH" value="<%=currentStatus%>"> 
                                        <input type="hidden" id="currentStatusValueH" name="currentStatusValueH" value="<%=currentStatusValue%>">
					<input type="hidden" id="partNewStatusIdH" name="partNewStatusIdH" value="<%=newStatus%>">
					<input type="hidden" id="changeReasonH" name="changeReasonH" value="<%=changeReason%>">
					<input type="hidden" id="partIdH" name="partIdH" value="<%=currentCard.getPartId()%>">
					
					<tr>
						<td>&nbsp;</td>
                                                <%
                                                    if(disableControl.equals("")){
                                                    %>
                                                        <td colspan="2" class="main"><br><%= Languages.getString("jsp.admin.transactions.inventorySimsEdit.instructions",SessionData.getLanguage()) %><br></td>                                                    
                                                    <%
                                                    }else{
                                                    %>
                                                        <td colspan="2" class="main"><br><%= Languages.getString("jsp.admin.transactions.inventorySimsEdit.noEdit",SessionData.getLanguage()) %><br></td>                                                    
                                                    <%
                                                    }                                            
                                                %>                                                                                             
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><STRONG><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.imsi",SessionData.getLanguage())%>:</STRONG></td>
						<%
                                  		if((!submittedForUpdate) && (currentCard.getMobileSuscriberId() != null))
                                  		{
                                  			out.println("<td><input type=text class=\"plain\" name=\"txtImsi\" id=\"txtImsi\" maxlength=\"20\" value=\"" + currentCard.getMobileSuscriberId() + "\" onkeypress=\"return validaNum(event);\"" + disableControl + "></td>");
                                  		}
                                  		else
                                  		{
                                  			out.println("<td><input type=text class=\"plain\" name=\"txtImsi\" id=\"txtImsi\" maxlength=\"20\" onkeypress=\"return validaNum(event);\" value=\"" + imsi + "\"" + disableControl + "></td>");
                                  		}
                                  %>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.phoneNumber",SessionData.getLanguage())%>:</td>
						<%
                                  		if((!submittedForUpdate) && (currentCard.getPhoneNumber() != null))
                                  		{
                                  			out.println("<td><input type=text class=\"plain\" name=\"txtPhoneNumber\" id=\"txtPhoneNumber\" maxlength=\"15\" value=\"" + currentCard.getPhoneNumber() + "\" onkeypress=\"return validaNum(event);\"" + disableControl + "></td>");
                                  		}
                                  		else
                                  		{
                                  			out.println("<td><input type=text class=\"plain\" name=\"txtPhoneNumber\" id=\"txtPhoneNumber\" maxlength=\"15\" onkeypress=\"return validaNum(event);\" value=\"" + phoneNumber + "\"" + disableControl + "></td>");
                                  		}
                                  %>

					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.carrier",SessionData.getLanguage())%>:</strong></td>
						<td><select name="ddlProvider" id="ddlProvider" <%=disableControl%>>
							<%
								    		CProvider[] providerList = CProvider.getProviderList();
								    		long newProvider = (!serviceProvider.equals(""))?Long.valueOf(serviceProvider):0;
								    		if((providerList != null) && (providerList.length > 0))
								    		{
	                                			for(CProvider currentProvider:providerList)
	                                			{
	                                				if(currentCard.getProvider() != null)
	                                				{
		                                				if((!submittedForUpdate) && (currentCard.getProvider().getProviderId() == currentProvider.getProviderId()))
		                                				{
		                                					out.println("<option value=" + currentProvider.getProviderId() +" selected>" + currentProvider.getDescription() + "</option>");
		                                				}
		                                				else if(newProvider == currentProvider.getProviderId()) // If edditing and there may be a provider change
		                                				{
		                                					out.println("<option value=" + currentProvider.getProviderId() +" selected>" + currentProvider.getDescription() + "</option>");
		                                				}
		                                				else // Normal option
		                                				{
		                                					out.println("<option value=" + currentProvider.getProviderId() +">" + currentProvider.getDescription() + "</option>");
		                                				}
	                                				}
	                                				else // At least provider options must be draw
	                                				{
	                                					out.println("<option value=" + currentProvider.getProviderId() +">" + currentProvider.getDescription() + "</option>");
	                                				}
	                                			}
								    		}
								    	%>
						</select></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.activationDate",SessionData.getLanguage())%>:</strong></td>
						<%
	                              		if((!submittedForUpdate) && (!currentCard.getActivationDate(false).equals("")))
	                              		{
	                              			out.println("<td><input class=\"plain\" id=\"txtActivationDate\" name=\"txtActivationDate\" value=\"" + currentCard.getActivationDate(true) + "\" readonly=true size=\"12\"" + disableControl + "><a href=\"javascript:void(0)\" onclick=\"if(self.gfPop)gfPop.fPopCalendar(document.mainform.txtActivationDate);return false;\" HIDEFOCUS><img name=\"popcal\" align=\"absmiddle\" src=\"admin/calendar/calbtn.gif\" width=\"34\" height=\"22\" border=\"0\" alt=\"\"></a></td>");
	                              		}
	                              		else
	                              		{
	                              			out.println("<td><input class=\"plain\" id=\"txtActivationDate\" name=\"txtActivationDate\" value=\"" + activationDate + "\" readonly=true size=\"12\"" + disableControl + "><a href=\"javascript:void(0)\" onclick=\"if(self.gfPop)gfPop.fPopCalendar(document.mainform.txtActivationDate);return false;\" HIDEFOCUS><img name=\"popcal\" align=\"absmiddle\" src=\"admin/calendar/calbtn.gif\" width=\"34\" height=\"22\" border=\"0\" alt=\"\"></a></td>");
	                              		}                                    
                                    %>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.lotNumber",SessionData.getLanguage())%>:</strong></td>
						<%
	                              		if((!submittedForUpdate) && (!currentCard.getLotNumber().equals("")))
	                              		{
	                              			out.println("<td><input type=text class=\"plain\" name=\"txtLotNumber\" id=\"txtLotNumber\" maxlength=\"50\" value=\""+ currentCard.getLotNumber() + "\" onkeypress=\"return validaNumYTexto(event);\"" + disableControl + "></td>");
	                               		}
	                               		else
	                               		{
	                               			out.println("<td><input type=text class=\"plain\" name=\"txtLotNumber\" id=\"txtLotNumber\" maxlength=\"50\" onkeypress=\"return validaNumYTexto(event);\" value=\"" + lotNumber + "\"" + disableControl + "></td>");
	                               		}
	                                %>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.contract",SessionData.getLanguage())%>:</strong></td>
						<%
	                              		if((!submittedForUpdate) && (!currentCard.getContractNumber().equals("")))
	                              		{
	                              			out.println("<td><input type=text class=\"plain\" name=\"txtContract\" id=\"txtContract\" maxlength=\"50\" value=\"" + currentCard.getContractNumber() + "\"onkeypress=\"return validaNumYTexto(event);\"" + disableControl + "></td>");
	                               		}
	                               		else
	                               		{
	                               			out.println("<td><input type=text class=\"plain\" name=\"txtContract\" id=\"txtContract\" maxlength=\"50\" onkeypress=\"return validaNumYTexto(event);\" value=\"" + contractNumber + "\"" + disableControl + "></td>");
	                               		}
	                                %>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsAdd.contractDate",SessionData.getLanguage())%>:</strong></td>
						<%
                                  		if((!submittedForUpdate) && (!currentCard.getContractDate(false).equals("")))
                                  		{
                                  			out.println("<td><input class=\"plain\" id=\"txtContractDate\" name=\"txtContractDate\" value=\"" + currentCard.getContractDate(true) + "\" readonly=true size=\"12\"><a href=\"javascript:void(0)\" onclick=\"if(self.gfPop)gfPop.fPopCalendar(document.mainform.txtContractDate);return false;\" HIDEFOCUS><img name=\"popcal\" align=\"absmiddle\" src=\"admin/calendar/calbtn.png\" width=\"34\" height=\"22\" border=\"0\" alt=\"\"></a></td>");
                                  		}
                                  		else
                                  		{
                                  			out.println("<td><input class=\"plain\" id=\"txtContractDate\" name=\"txtContractDate\" value=\"" + contractDate +"\" readonly=true size=\"12\"><a href=\"javascript:void(0)\" onclick=\"if(self.gfPop)gfPop.fPopCalendar(document.mainform.txtContractDate);return false;\" HIDEFOCUS><img name=\"popcal\" align=\"absmiddle\" src=\"admin/calendar/calbtn.png\" width=\"34\" height=\"22\" border=\"0\" alt=\"\"></a></td>");
                                  		}
                                   %>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsEdit.currentStatus",SessionData.getLanguage())%>:</strong></td>
						<%
	                              		if((!submittedForUpdate) && (currentCard.getStatus() != null))
	                              		{
	                              			out.println("<td><input type=text class=\"plain\" name=\"txtCurrentStatus\" id=\"txtCurrentStatus\" value=\"" + currentCard.getStatus().getPartStatusDescription() + "\" readonly></td>");
	                               		}
	                               		else
	                               		{
	                               			out.println("<td><input type=text class=\"plain\" name=\"txtCurrentStatus\" id=\"txtCurrentStatus\" readonly value=\"" + currentStatus + "\"></td>");
	                               		}
	                                %>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySimsEdit.newStatus",SessionData.getLanguage())%>:</strong></td>
						<td>
						<%
                                                out.println("<select name=\"ddlNewStatus\" id=\"ddlNewStatus\"" + disableControl + ">");
                                        	CTerminalPartStatus[] partStatus = CTerminalPart.getAllowedStatus(currentCard.getStatus().getPartStatusId(),null);
                                        	long lNewStatus = (!newStatus.equals(""))?Long.valueOf(newStatus):0;
	                                   		if(partStatus != null)
	                                   		{
	                                   			if(currentCard.getStatus() != null)
	                                   			{
	                                   				out.println("<option value=" + currentCard.getStatus().getPartStatusId() +">" + currentCard.getStatus().getPartStatusDescription() + "</option>");
	                                   			}
	                                   			for(CTerminalPartStatus currentStatusElement:partStatus)
	                                   			{
	                                   				if(!submittedForUpdate)
	                                   				{
	                                   					out.println("<option value=" + currentStatusElement.getPartStatusId() +">" + currentStatusElement.getPartStatusDescription() + "</option>");
	                                   				}
	                                   				else if(lNewStatus == currentStatusElement.getPartStatusId())
	                                   				{
	                                   					out.println("<option value=" + currentStatusElement.getPartStatusId() +" selected>" + currentStatusElement.getPartStatusDescription() + "</option>");
	                                   				}
	                                   				else
	                                   				{
	                                   					out.println("<option value=" + currentStatusElement.getPartStatusId() +">" + currentStatusElement.getPartStatusDescription() + "</option>");
	                                   				}
	                                   			}
	                                   		}
                                                out.println("</select>");
                                    	%>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySimsEdit.changeReason",SessionData.getLanguage())%>:</td>

						<td><textarea class="main" name="txtChangeReason" id="txtChangeReason" rows=4 cols=50 <%=disableControl%>><%=changeReason%></textarea></td>
                                                
					</tr>

					<tr>
						<td><br>
						</td>
					</tr>
					<tr align="center">
						<td>&nbsp;</td>
						<td align=center colspan="2">
							<input type="hidden" name="submitted" value="y"> 
							<input type="hidden" name="message" value="3"> 
							<input id="btnSave" type="button" value="<%= Languages.getString("jsp.admin.transactions.inventorySimsEdit.save",SessionData.getLanguage()) %>" onclick="enviarDatos(this.form)" <%=disableControl%>>
							<%String strHist = "window.open('/support/admin/transactions/inventorySims_history.jsp?partId=" + partId + "','History','width=700,height=500,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');"; %>
							<input id="btnHistory" type="button" value="<%= Languages.getString("jsp.admin.transactions.inventorySimsEdit.history",SessionData.getLanguage()) %>" onclick=<% out.println(strHist); %>> 
							<input id="btnCancel" type="button" value="<%=Languages.getString("jsp.admin.transactions.inventorySimsEdit.cancel",SessionData.getLanguage())%>" onClick="cancelar(this.form)">
						</td>
					</tr>
					<tr>
						<td><br>
						</td>
					</tr>
					</form>
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" 	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;"></iframe>
<%@ include file="/includes/footer.jsp"%>
