<%@ page import="com.debisys.terminalTracking.CApplicationType"%>
<%@ page import="com.debisys.terminalTracking.CTerminalStatus"%>
<%@ page import="com.debisys.terminalTracking.CTerminalBrand"%>
<%@ page import="com.debisys.terminalTracking.CTerminalModel"%>
<%@ page import="com.debisys.terminalTracking.CTerminalPart"%>
<%@ page import="com.debisys.terminalTracking.CTerminalPartType"%>
<%@ page import="com.debisys.terminalTracking.CPhysicalTerminal"%>
<%@ page import="com.debisys.terminalTracking.CCommunicationType"%>
<%@ page import="com.debisys.terminalTracking.CSimCard"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collection"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
int section = 9;
int section_page = 5;

String strIDConsultar = "";
String serialNumber = "";
String brandId = "";
String modelId = "";
String applicationType = "";
String softwareLicense = "";
String communicationTypeId = "";

String[] strCommTypes=null;

String primaryCommTypeId = "";
String imei = "";
String purchaseOrder = "";
String admittanceDate = "";
String currentStatus = "";
String currentStatusValue = "";
String currentPrimaryComm = "";
String lotNumber = "";
String newStatus = "";
String changeReason = "";

String simCard = "";
String simCardId = "";
String assDeasSimAction = "";
String assDeasSimValue = "";
String deassignReason = "";

String msgEdicion = "";
String tipoCommGPRSId = "";
String tipoCommGPRS = CCommunicationType.GPRS_CODE;
String tipoParteSIM = CTerminalPartType.SIMCARD_CODE;
String tipoEstadoReady = CTerminalStatus.READY_CODE;

boolean terminalEditada = Boolean.FALSE;

String strMessage = request.getParameter("message");
if (strMessage == null)
{
strMessage = "";
}

if (request.getParameter("submittedITE") != null && (!strMessage.equals("3")))//Si la forma se ha redirigido a si misma - tomar los valores de la forma
{
  try
  {
    if (request.getParameter("submittedITE").equals("y"))
    {
        //Obtener los valores de la interfaz de usuario
        strIDConsultar = (request.getParameter("strIDConsultarH") != null)?request.getParameter("strIDConsultarH"):"";
        serialNumber = (request.getParameter("serialNumberH") != null)?request.getParameter("serialNumberH"):"";
        brandId = (request.getParameter("brandIdH") != null)?request.getParameter("brandIdH"):"";
        modelId = (request.getParameter("modelIdH") != null)?request.getParameter("modelIdH"):"";          
        applicationType = (request.getParameter("applicationTypeIdH") != null)?request.getParameter("applicationTypeIdH"):"";
        softwareLicense = (request.getParameter("softwareLicenseH") != null)?request.getParameter("softwareLicenseH"):"";
        communicationTypeId = (request.getParameter("communicationTypeIdH") != null)?request.getParameter("communicationTypeIdH"):"";
        //Obtener el arreglo de valores seleccionados para los tipos de comunicacion
        strCommTypes = request.getParameterValues("communicationType");        
        primaryCommTypeId = (request.getParameter("primaryCommTypeIdH") != null)?request.getParameter("primaryCommTypeIdH"):"";
        imei = (request.getParameter("imeiH") != null)?request.getParameter("imeiH"):"";
        purchaseOrder = (request.getParameter("purchaseOrderH") != null)?request.getParameter("purchaseOrderH"):"";
        admittanceDate = (request.getParameter("admittanceDateH") != null)?request.getParameter("admittanceDateH"):"";        
        lotNumber = (request.getParameter("lotNumberH") != null)?request.getParameter("lotNumberH"):"";
        currentStatus = (request.getParameter("currentStatusH") != null)?request.getParameter("currentStatusH"):"";
        currentStatusValue = (request.getParameter("currentStatusValueH") != null)?request.getParameter("currentStatusValueH"):"";
        currentPrimaryComm = (request.getParameter("currentPrimaryCommH") != null)?request.getParameter("currentPrimaryCommH"):"";
        newStatus = (request.getParameter("newStatusH") != null)?request.getParameter("newStatusH"):"";        
        changeReason = (request.getParameter("changeReasonH") != null)?request.getParameter("changeReasonH"):"";
        simCard = (request.getParameter("simCardH") != null)?request.getParameter("simCardH"):"";
        simCardId = (request.getParameter("simCardIdH") != null)?request.getParameter("simCardIdH"):"";
        assDeasSimAction = (request.getParameter("assDeasSimActionH") != null)?request.getParameter("assDeasSimActionH"):"";
        assDeasSimValue = (request.getParameter("assDeasSimValueH") != null)?request.getParameter("assDeasSimValueH"):"";
        deassignReason = (request.getParameter("deassignReasonH") != null)?request.getParameter("deassignReasonH"):"";
    }
    if(request.getParameter("editTerminal").equals("y"))
    {
    	// Get original terminal values in order to compare changed info
    	CPhysicalTerminal originalTerminal = new CPhysicalTerminal();
    	if(originalTerminal.loadTerminal(Long.valueOf(strIDConsultar)))
    	{
	        //Establecer los valores del objeto segun lo recibido en el request
	        CPhysicalTerminal terminalFisico = new CPhysicalTerminal();
	        //ID para busqueda
	        terminalFisico.setTerminalId(Long.parseLong(strIDConsultar));
	        //Serial
	        terminalFisico.setSerialNumber(serialNumber);
	        //Brand
	        CTerminalBrand brandObj = new CTerminalBrand();
	        brandObj.loadBrand(Long.parseLong(brandId.trim()));
	        brandObj.loadBrandModels();
	        terminalFisico.setBrand(brandObj);
	        //Model
	        terminalFisico.setTerminalModel(Long.parseLong(modelId.trim()));
	        //Application Type
	        CApplicationType appObj= new CApplicationType();
	        appObj.loadAppType(Long.parseLong(applicationType.trim()));
	        terminalFisico.setApplicationType(appObj);
	        //Software License
	        terminalFisico.setSoftwareLicense(softwareLicense);
	        //Communication Type
	        terminalFisico.setCommunicationTypeSet(strCommTypes);
	        //Primary Comm Type
	        terminalFisico.setPrimaryCommType(Long.parseLong(primaryCommTypeId.trim()));
	        //IMEI
	        terminalFisico.setMobileEquipmentId(imei);
	        //Purchase Order
	        terminalFisico.setPurchaseOrder(purchaseOrder);
	        //Admittance Date
	        terminalFisico.setInventoryAdmitanceDate(admittanceDate);
	        //Lot number
	        terminalFisico.setLotNumber(lotNumber);  
	        //Status
	        CTerminalStatus statusObj = new CTerminalStatus();
	        statusObj.load(Long.parseLong(newStatus.trim()));
	        terminalFisico.setCurrentStatus(statusObj);
	        // Becuase part replacement is not already done. Parts SET are the actual parts assigned
	        terminalFisico.loadTerminalParts();
	        //Motivo de la modificacion
	        terminalFisico.setStatusChangeReason(changeReason);
	        
	        boolean bSerialAvailable = true;
	        // If user changes the serial number it must not be duplicated on the physical terminal table but also on the terminals table
	        if((!originalTerminal.getSerialNumber().equals(terminalFisico.getSerialNumber())) &&
	           ((CPhysicalTerminal.terminalExists(serialNumber, Long.parseLong(brandId.trim())) != null) || 
	           (!CPhysicalTerminal.IsSerialNumberUnique(terminalFisico.getSerialNumber()))))
	        {
	        	msgEdicion = Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.alreadyExists",SessionData.getLanguage());
	        	bSerialAvailable = false;
	        }

	        // If software license changed, and the new license is already in use the edition will not be allowed
	        boolean bSoftwareLicAvalable = true;
	    	if((!originalTerminal.getSoftwareLicense().equals(terminalFisico.getSoftwareLicense())) &&
	    	   (terminalFisico.getApplicationType().getApplicationTypeCode().equals(CApplicationType.MICROBROWSER_CODE)) && 
	    	   (CPhysicalTerminal.softwareLicenseExists(terminalFisico.getSoftwareLicense())))
	    	{
	    		// Software License already in use
	    		bSoftwareLicAvalable = false;
	    		msgEdicion = Languages.getString("jsp.admin.transactions.inventoryTerminalsSoftwareLicAlreadyInUse",SessionData.getLanguage());
	    	}

	    	// If IMEI changed, and the new IMEI is already in use the edition will not be allowed
	    	boolean bIMEIAvalable = true;
	    	if((!originalTerminal.getMobileEquipmentId().equals(terminalFisico.getMobileEquipmentId())) &&
	    	   (terminalFisico.getPrimaryCommType().getCommunicationTypeCode().equals(CCommunicationType.GPRS_CODE)) && 
	    	   (CPhysicalTerminal.imeiExists(terminalFisico.getMobileEquipmentId())))
	    	{
	    		// Imei already in use
	    		bIMEIAvalable = false;
	    		msgEdicion = Languages.getString("jsp.admin.transactions.inventoryTerminalsImeiAlreadyInUse",SessionData.getLanguage());
	    	}

	        if(bSerialAvailable && bSoftwareLicAvalable && bIMEIAvalable)
	        {                	        
		        //Guardar las modificaciones a la terminal
		        if(assDeasSimAction.equals("assign")){
		            if(assDeasSimValue.equalsIgnoreCase("checked")){
		                terminalEditada = terminalFisico.updateCurrentTerminal(true,SessionData.getProperty("ref_id"),null,SessionData);                
		            }else{
		                terminalEditada = terminalFisico.updateCurrentTerminal(false,SessionData.getProperty("ref_id"),null,SessionData);                                
		            }
		        }else{
		            if(assDeasSimValue.equalsIgnoreCase("checked")){
		                terminalFisico.setPartUnassignReason(deassignReason);
		                Long idSimDesasignar = new Long(Long.parseLong(simCardId));
		                terminalEditada = terminalFisico.updateCurrentTerminal(false,SessionData.getProperty("ref_id"), idSimDesasignar,SessionData);                
		            }else{
		                terminalEditada = terminalFisico.updateCurrentTerminal(false,SessionData.getProperty("ref_id"),null,SessionData);                                                
		            }
		
		        }
		        if(terminalEditada){
		            //Redirigir el llamado a este mismo JSP, para edicion
		            response.sendRedirect("inventoryTerminals_edit.jsp?idterminal=" + terminalFisico.getTerminalId() + "&message=3");                    
		        }else{
		            msgEdicion = Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.editFailed",SessionData.getLanguage());
		        }
	        }
   		}
    }
  }
  catch (Exception e){
  }
}else{  //Si no se ha redirigido - consultar los valores del objeto en BD
    try{
        //Obtener el ID a consultar
        strIDConsultar = request.getParameter("idterminal");
        boolean cargoObjeto = Boolean.FALSE;
        //Consultar el objeto en BD
        CPhysicalTerminal terminalFisico = new CPhysicalTerminal();
        if(terminalFisico.loadTerminal(Long.parseLong(strIDConsultar)))
        {
            //Obtener del objeto cargado
            serialNumber = terminalFisico.getSerialNumber();
            brandId = String.valueOf(terminalFisico.getBrand().getBrandId());
            modelId = String.valueOf(terminalFisico.getModel().getModelId());
            applicationType = String.valueOf(terminalFisico.getApplicationType().getApplicationTypeId());
            softwareLicense = (terminalFisico.getSoftwareLicense() != null)?terminalFisico.getSoftwareLicense():"";

            //Obtener el arreglo de valores seleccionados para los tipos de comunicacion
            //CCommunicationType[] communicationTypesObj = terminalFisico.getCommunicationTypeSet();
            ArrayList communicationTypesObj = terminalFisico.getCommunicationTypeSet();
            strCommTypes = new String[communicationTypesObj.size()];
            for(int i=0;i<communicationTypesObj.size();i++){
                strCommTypes[i]=String.valueOf(((CCommunicationType)communicationTypesObj.get(i)).getCommunicationTypeId());
            }

            primaryCommTypeId = String.valueOf(terminalFisico.getPrimaryCommType().getCommunicationTypeId());
            imei = (terminalFisico.getMobileEquipmentId() != null)?terminalFisico.getMobileEquipmentId():"";
            purchaseOrder = terminalFisico.getPurchaseOrder();
            
            admittanceDate = terminalFisico.getInventoryAdmitanceDate().substring(0,10);
            admittanceDate = admittanceDate.substring(5,7) + "/" + admittanceDate .substring(8,10) + "/" + admittanceDate .substring(0,4);
            
            lotNumber = terminalFisico.getLotNumber();
            currentStatus = String.valueOf(terminalFisico.getCurrentStatus().getTerminalStatusDescription());
            currentStatusValue = String.valueOf(terminalFisico.getCurrentStatus().getTerminalSatusId());
            currentPrimaryComm = String.valueOf(terminalFisico.getPrimaryCommType().getCommunicationTypeId());
            //Se igual el estado actual al nuevo, para evitar que quede el estado vacio
            newStatus = currentStatusValue;
            //Buscar si tiene una SIM asignada
            assDeasSimAction = "assign";
            if(terminalFisico.getAssignedParts() != null)
            {
            	for(CTerminalPart currentPart:terminalFisico.getAssignedParts().values())
            	{
                    if(currentPart.getPartType().getPartTypeCode().equals(CTerminalPartType.SIMCARD_CODE))
                    {
                        CSimCard simCardObj = (CSimCard)currentPart;                                                          
                        simCard=String.valueOf(simCardObj.getMobileSuscriberId());
                        simCardId=String.valueOf(simCardObj.getPartId());
                        assDeasSimAction = "deassign";
                        break; // Aqu� encontr� una parte de tipo SIM
                    }
            	}
            }                  
        }
        
    }
	catch (Exception e)
	{
		msgEdicion = e.toString();
  	}
    
}
%>
<%! 
boolean estaContenidoEn(long valor, String[] arreglo)
{
    if(arreglo!=null){
        for(int i=0;i<arreglo.length;i++){
            if(Long.parseLong(arreglo[i])==valor){
                return true;
            }
        }
    }
    return false;
};
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script SRC="includes/funcionesValidacion.js" type="text/javascript"> </script>
<script type="text/javascript">   

function saveCurrentFieldValues()
{
	document.getElementById('serialNumberH').value = document.getElementById('txtSerial').value;        
	document.getElementById('brandIdH').value = document.getElementById('ddlBrand').value;
	document.getElementById('modelIdH').value = document.getElementById('ddlModel').value;
	document.getElementById('applicationTypeIdH').value = document.getElementById('ddlApplicationType').value;        
        document.getElementById('softwareLicenseH').value = document.getElementById('txtSoftwareLicense').value;
        document.getElementById('communicationTypeIdH').value = document.getElementById('ddlCommunicationType').value;
        document.getElementById('primaryCommTypeIdH').value = document.getElementById('ddlPrimaryCommType').value;
        document.getElementById('imeiH').value = document.getElementById('txtImei').value;
        document.getElementById('purchaseOrderH').value = document.getElementById('txtPurchaseOrder').value;
        document.getElementById('admittanceDateH').value = document.getElementById('txtAdmittanceDate').value;    
        document.getElementById('lotNumberH').value = document.getElementById('txtLotNumber').value;    
        document.getElementById('currentStatusH').value = document.getElementById('txtCurrentStatus').value;    
        document.getElementById('newStatusH').value = document.getElementById('ddlNewStatus').value;    
        document.getElementById('changeReasonH').value = document.getElementById('txtChangeReason').value;    
        document.getElementById('simCardH').value = document.getElementById('txtsimCard').value;    
        if(document.getElementById('chkDeassignSim').checked){
            document.getElementById('assDeasSimValueH').value = "checked";
        }
        if(document.getElementById('assDeasSimActionH').value=="deassign"){
            document.getElementById('deassignReasonH').value = document.getElementById('txtDeassignReason').value;    
        }
	document.mainform.submit();
}

function cancelar(form) {
      form.action = "admin/transactions/inventoryTerminals_search.jsp";
      form.submit(); 
    }
        
function enviarDatos(form) {
      if(validacion(form)){
        form.editTerminal.value="y";
        saveCurrentFieldValues();      
      }
    }   

function validacion(form)
   {
     var alpha = 0;
     var errors = "";
     
     if ( document.getElementById('txtSerial').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptySerial",SessionData.getLanguage())%>\n';
        alpha++;
     } 
            
     if ( document.getElementById('ddlBrand').value==0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyBrand",SessionData.getLanguage())%>\n';
        alpha++;
     }  
     
     if ( document.getElementById('ddlModel').value==0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyModel",SessionData.getLanguage())%>\n';
        alpha++;
     } 
     
     if ( document.getElementById('ddlApplicationType').value==0){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyAppType",SessionData.getLanguage())%>\n';
        alpha++;
     }
     
     if ( document.getElementById('ddlCommunicationType').value==0){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyCommType",SessionData.getLanguage())%>\n';
        alpha++;
     }   
     
     if ( document.getElementById('ddlPrimaryCommType').value==0){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyPrimaryCommType",SessionData.getLanguage())%>\n';
        alpha++;
     }  
     
     if ( document.getElementById('txtPurchaseOrder').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyPurchaseOrder",SessionData.getLanguage())%>\n';
        alpha++;
     }      
     
     if ( document.getElementById('txtAdmittanceDate').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyAdmittanceDate",SessionData.getLanguage())%>\n';
        alpha++;
     }     
     
     if ( document.getElementById('txtLotNumber').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyLot",SessionData.getLanguage())%>\n';
        alpha++;
     }   
     
     if ((document.getElementById('currentStatusValueH').value != document.getElementById('ddlNewStatus').value) && document.getElementById('txtChangeReason').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyChangeReason",SessionData.getLanguage())%>\n';
        alpha++;
     }
     
     if ( document.getElementById('ddlNewStatus').value==0){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyStatus",SessionData.getLanguage())%>\n';
        alpha++;
     }     
     
     var selCommIndex=0;
     var selCommValues = new Array();
     var selCommTexts = new Array();
     for(i=0; i<document.getElementById('ddlCommunicationType').length; i++){
        if(document.getElementById('ddlCommunicationType').options[i].selected){
            selCommValues[selCommIndex] = document.getElementById('ddlCommunicationType').options[i].value;  
            selCommTexts[selCommIndex] = document.getElementById('ddlCommunicationType').options[i].text;              
            selCommIndex++;
        }
     }   
     
     var validaCommPrimary = 0;
     for(j=0; j<selCommValues.length ; j++){
        if(parseInt(selCommValues[j])==parseInt(document.getElementById('ddlPrimaryCommType').value)){
            validaCommPrimary=1;
        }
     }
     
     if (validaCommPrimary == 0){        
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.commNotPrimary",SessionData.getLanguage())%>\n';
        alpha++;
     }
     
     var supportGPRS = 0;          
     for(k=0; k<selCommValues.length ; k++){   
        if(selCommTexts[k]==document.getElementById('tipoCommGPRS').value){
            supportGPRS=1;
        }
     }   
     
     if (supportGPRS == 0 && document.getElementById('txtImei').value.length != 0){        
        alert("<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.imeiNotGprs",SessionData.getLanguage())%>");
        document.getElementById('txtImei').value = "";
     }  
     
     if(document.getElementById('assDeasSimActionH').value=="deassign" && document.getElementById('chkDeassignSim').checked){
         if ( document.getElementById('txtDeassignReason').value.length == 0 ){
            errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptyDeassignReason",SessionData.getLanguage())%>\n';
            alpha++;
         }   
     }     
          
     if (alpha != 0) {
         alert(errors);
         return false;
     }
     else {	  	
         return true;
     }      
   }

</script>    
<table border="0" cellpadding="0" cellspacing="0" width="60%">
<%
        if (!msgEdicion.equals(""))
        {
          out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + msgEdicion + "</td></tr>");
        }else{      
            if (strMessage.equals("2"))
            {
              out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + Languages.getString(
                      "jsp.admin.transactions.inventoryTerminalsEdit.terminal_added_success",SessionData.getLanguage()) + "</td></tr>");
            }
            else if (strMessage.equals("3"))
            {
              out.println("<tr bgcolor=003082><td colspan='3' align=center class=main><font color=33ff33>" + Languages.getString(
                      "jsp.admin.transactions.inventoryTerminalsEdit.terminal_updated_success",SessionData.getLanguage()) + "</td></tr>");
            }
        }                    
%>    
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan=2>                   
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#fFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">                                                   
                                                    <form name="mainform" method="post" action="admin/transactions/inventoryTerminals_edit.jsp">
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td colspan="2" class="main">
                                                            <br>
                                                            <%= Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.add_instructions",SessionData.getLanguage()) %>
                                                            <br>                                                                                                          
                                                            </td>
                                                        </tr>
                                                        <tr><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><STRONG><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.serial",SessionData.getLanguage())%>:</STRONG></td>
                                                            <% 
                                                            int maxLenSerial=0;
                                                            if(SessionData.getUser().isQcommBusiness() == true)
                                                            {
                                                                maxLenSerial=26;
                                                            }else{
                                                                maxLenSerial=25;
                                                            }                                                           
                                                            %>                                                             
                                                            <td><input type=text class="plain" id="txtSerial" name="serial" onkeypress="return validaNumYTexto(event);" value='<%=serialNumber%>' maxlength='<%=maxLenSerial%>'></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.brand",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="brand" id="ddlBrand" onchange="saveCurrentFieldValues()">
                                                                <%
                                                                        CTerminalBrand[] brandList = CTerminalBrand.getBrandList();
                                                                        CTerminalBrand selectedBrand = null;
                                                                        boolean brandSelected = false;
                                                                        for(CTerminalBrand currentBrand:brandList)
                                                                        {
                                                                                if((brandId != "") && (brandId.equals(String.valueOf(currentBrand.getBrandId()))))
                                                                                {
                                                                                        out.println("<option value=" + currentBrand.getBrandId() +" selected>" + currentBrand.getBrandName() + "</option>");
                                                                                        brandSelected = true;
                                                                                        selectedBrand = currentBrand;
                                                                                        selectedBrand.loadBrandModels();
                                                                                }
                                                                                else
                                                                                {
                                                                                        out.println("<option value=" + currentBrand.getBrandId() +">" + currentBrand.getBrandName() + "</option>");
                                                                                }
                                                                        }
                                                                        if(!brandSelected)
                                                                        {
                                                                                out.println("<option value=0 selected>" + Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.selectOption",SessionData.getLanguage()) + "</option>");
                                                                        }
                                                                %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.model",SessionData.getLanguage())%>:</strong></td>
                                                            <td>
                                                                <select name="model" id="ddlModel" >
                                                                    <option value=0 selected><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.selectOption",SessionData.getLanguage())%></option>
                                                                    <%
                                                                            if(selectedBrand != null)
                                                                            {
                                                                                    for(CTerminalModel currentModel: selectedBrand.getModelList())
                                                                                    {
                                                                                            if((modelId != "") && (modelId.equals(String.valueOf(currentModel.getModelId()))))
                                                                                                {
                                                                                                        out.println("<option value=" + currentModel.getModelId() +" selected>" + currentModel.getModelName() + "</option>");
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                        out.println("<option value=" + currentModel.getModelId() +">" + currentModel.getModelName() + "</option>");
                                                                                                }   
                                                                                    }
                                                                            }
                                                                    %>
                                                                </select>
                                                            </td>    
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.applicationType",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="applicationType" id="ddlApplicationType">
                                                                    <%
                                                                        CApplicationType[] appTypeList = CApplicationType.getAllApplicationTypes();
                                                                        boolean applicationTypeSelected = false;
                                                                        for(CApplicationType appType:appTypeList)
                                                                        {
                                                                                if((applicationType != "") && (applicationType.equals(String.valueOf(appType.getApplicationTypeId()))))
                                                                                {
                                                                                        out.println("<option value=" + appType.getApplicationTypeId() +" selected>" +appType.getApplicationTypeDescription() + "</option>");
                                                                                        applicationTypeSelected = true;
                                                                                }
                                                                                else
                                                                                {
                                                                                        out.println("<option value=" + appType.getApplicationTypeId() +">" +appType.getApplicationTypeDescription() + "</option>");
                                                                                }
                                                                        }
                                                                        if(!applicationTypeSelected)
                                                                        {
                                                                                out.println("<option value=0 selected>" + Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.selectOption",SessionData.getLanguage()) + "</option>");
                                                                        }
                                                                    %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr>                                                        
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.softwareLicense",SessionData.getLanguage())%>:</td>
                                                            <td><input type=text class="plain" name="softwareLicense" id="txtSoftwareLicense" maxlength="50" onkeypress="return validaNumYTextoYLinea(event);" value='<%=softwareLicense%>'></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.communicationType",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="communicationType" id="ddlCommunicationType" size=5 multiple>
                                                                    <%
                                                                        CCommunicationType[] commTypeList = CCommunicationType.getAllCommunicationTypes();
                                                                        for(CCommunicationType commType:commTypeList)
                                                                        {
                                                                            if(strCommTypes!=null)
                                                                            {                                                                                                                                                                                                                                                        
                                                                                if(estaContenidoEn(commType.getCommunicationTypeId(),strCommTypes))
                                                                                {
                                                                                        out.println("<option value=" + commType.getCommunicationTypeId() +" selected>" +commType.getCommunicationTypeDescription() + "</option>");
                                                                                }
                                                                                else
                                                                                {
                                                                                        out.println("<option value=" + commType.getCommunicationTypeId() +">" +commType.getCommunicationTypeDescription() + "</option>");
                                                                                }                                                                                
                                                                            } else {
                                                                                out.println("<option value=" + commType.getCommunicationTypeId() +">" +commType.getCommunicationTypeDescription() + "</option>");
                                                                            }   
                                                                        }
                                                                    %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr> 
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.primaryCommType",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="primaryCommType" id="ddlPrimaryCommType">
                                                                    <%
                                                                        CCommunicationType[] commPrimaryList = CCommunicationType.getAllCommunicationTypes();
                                                                        boolean communicationTypeSelected = false;
                                                                        for(CCommunicationType commType:commPrimaryList)
                                                                        {
                                                                                if((primaryCommTypeId != "") && (primaryCommTypeId.equals(String.valueOf(commType.getCommunicationTypeId()))))
                                                                                {
                                                                                    out.println("<option value=" + commType.getCommunicationTypeId() +" selected>" +commType.getCommunicationTypeDescription()+ "</option>");
                                                                                        communicationTypeSelected = true;
                                                                                }
                                                                                else
                                                                                {
                                                                                    out.println("<option value=" + commType.getCommunicationTypeId() +">" + commType.getCommunicationTypeDescription() + "</option>");
                                                                                }
                                                                                //Identificar el id del tipo de comunicacion GPRS, para la verificacion del paso a estado ready
                                                                                if(commType.getCommunicationTypeDescription().equalsIgnoreCase(tipoCommGPRS))
                                                                                {
                                                                                    tipoCommGPRSId = String.valueOf(commType.getCommunicationTypeId());
                                                                                }
                                                                                
                                                                        }
                                                                        if(!communicationTypeSelected)
                                                                        {
                                                                                out.println("<option value=0 selected>" + Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.selectOption",SessionData.getLanguage()) + "</option>");
                                                                        }
                                                                    %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr>  
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.imei",SessionData.getLanguage())%>:</td>
                                                            <td><input type=text class="plain" name="imei" id="txtImei" maxlength="15" onkeypress="return validaNum(event);" value='<%=imei%>'></td>
                                                        </tr>    
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.purchaseOrder",SessionData.getLanguage())%>:</strong></td>
                                                            <td><input type=text class="plain" name="purchaseOrder" id="txtPurchaseOrder" maxlength="50" onkeypress="return validaNumYTexto(event);" value='<%=purchaseOrder%>'></td>
                                                        </tr>                                                        
							<tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.admittanceDate",SessionData.getLanguage())%>:</strong></td>
							    <td><input class="plain" id="txtAdmittanceDate" name="admittanceDate" readonly=true size="12" value='<%=admittanceDate%>'><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.mainform.admittanceDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
							</tr>                                                          
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.lotNumber",SessionData.getLanguage())%>:</strong></td>
                                                            <td><input type=text class="plain" name="lotNumber" id="txtLotNumber" maxlength="50" onkeypress="return validaNumYTexto(event);" value='<%=lotNumber%>' ></td>
                                                        </tr>  
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.currentStatus",SessionData.getLanguage())%>:</td>
                                                            <td><input type=text class="plain" name="currentStatus" id="txtCurrentStatus" readonly value='<%=currentStatus%>'></td>
                                                        </tr>  
                                                        <tr>                                                        
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.newStatus",SessionData.getLanguage())%>:</strong></td>
							    <td>
							    	<select name="newStatus" id="ddlNewStatus" >
                                                                    <%                                                                        
                                                                        CTerminalStatus[] statusList = CPhysicalTerminal.getAllowedStatus(Long.parseLong(currentStatusValue),null);
                                                                        if(statusList!=null){
                                                                            boolean newStatusSelected = false;
                                                                            for(CTerminalStatus newStatusType:statusList)
                                                                            {
                                                                                    if((newStatus != "") && (newStatus.equals(String.valueOf(newStatusType.getTerminalSatusId()))))
                                                                                    {
                                                                                            out.println("<option value=" + newStatusType.getTerminalSatusId() +" selected>" + newStatusType.getTerminalStatusDescription()+ "</option>");
                                                                                            newStatusSelected = true;
                                                                                    }
                                                                                    //validar, porque si es GPRS y no tiene SIM asignada, no se puede mostrar el estado READY
                                                                                    else if(!(currentPrimaryComm.equals(tipoCommGPRSId) && simCardId.equals("") && newStatusType.getTerminalStatusDescription().equalsIgnoreCase(tipoEstadoReady)))
                                                                                    {
                                                                                            out.println("<option value=" + newStatusType.getTerminalSatusId() +">" + newStatusType.getTerminalStatusDescription() + "</option>");
                                                                                    }
                                                                            }
                                                                            if(!newStatusSelected)
                                                                            {
                                                                                out.println("<option value=" + currentStatusValue +" selected>" + currentStatus + "</option>");
                                                                            }else{
                                                                                out.println("<option value=" + currentStatusValue +">" + currentStatus + "</option>");
                                                                            }                                                                                
                                                                        }                                                                            
                                                                    %>
                                                                </select>
                                                            </td>                                                            
                                                        </tr> 
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.changeReason",SessionData.getLanguage())%>:</td>
                                                            <td><textarea class="main" name="changeReason" id="txtChangeReason" rows=4 cols=50 ><%=changeReason%></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.autoSimAssig",SessionData.getLanguage())%></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.simCard",SessionData.getLanguage())%>:</td>
                                                            <td><input type=text class="plain" name="simCard" id="txtsimCard" readonly value='<%=simCard%>'></td>
                                                        </tr> 
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <%
                                                            if(assDeasSimAction.equalsIgnoreCase("assign")){
                                                            %>
                                                                <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.assignSim",SessionData.getLanguage())%>:</td>                                                            
                                                            <%                
                                                            }else if(assDeasSimAction.equalsIgnoreCase("deassign")){  
                                                            %>                                                                                                                                        
                                                                <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.deassignSim",SessionData.getLanguage())%>:</td>               
                                                            <%
                                                            }  
                                                            %>
                                                            <td><input type=checkbox class="plain" name="deassignSim" id="chkDeassignSim" <%=assDeasSimValue%>></td>
                                                        </tr>  
                                                        <%                
                                                            if(assDeasSimAction.equalsIgnoreCase("deassign")){  
                                                        %>                                                         
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.deassignReason",SessionData.getLanguage())%>:</td>                                                                                                                      
                                                                <td><textarea class="main" name="deassignReason" id="txtDeassignReason" rows=4 cols=50><%=deassignReason%></textarea></td>
                                                            </tr> 
                                                        <%
                                                            }
                                                        %>
                                                        
                                                        <tr><td><br></td></tr>
							<tr align="center">
                                                                <td>&nbsp;</td>
                                                                <td align=center colspan="2">
                                                                    <input type="hidden" name="submittedITE" value="y">
                                                                    <input type="hidden" name="message" value="">       
                                                                    <input type="hidden" name="editTerminal" value="n">
                                                                    <input type="hidden" id="tipoCommGPRS" name="tipoCommGPRS" value="<%=tipoCommGPRS%>">
                                                                    <input type="hidden" id="strIDConsultarH" name="strIDConsultarH" value="<%=strIDConsultar%>">
                                                                    <input type="hidden" id="serialNumberH" name="serialNumberH" value="">
                                                                    <input type="hidden" id="brandIdH" name="brandIdH" value="">
                                                                    <input type="hidden" id="modelIdH" name="modelIdH" value="">
                                                                    <input type="hidden" id="applicationTypeIdH" name="applicationTypeIdH" value="">                                                        
                                                                    <input type="hidden" id="softwareLicenseH" name="softwareLicenseH" value="">
                                                                    <input type="hidden" id="communicationTypeIdH" name="communicationTypeIdH" value="">
                                                                    <input type="hidden" id="primaryCommTypeIdH" name="primaryCommTypeIdH" value="">
                                                                    <input type="hidden" id="imeiH" name="imeiH" value="">
                                                                    <input type="hidden" id="purchaseOrderH" name="purchaseOrderH" value="">
                                                                    <input type="hidden" id="admittanceDateH" name="admittanceDateH" value="">                                                                        
                                                                    <input type="hidden" id="currentStatusH" name="currentStatusH" value="">
                                                                    <input type="hidden" id="currentStatusValueH" name="currentStatusValueH" value="<%=currentStatusValue%>">                                                                   
                                                                    <input type="hidden" id="currentPrimaryCommH" name="currentPrimaryCommH" value="<%=currentPrimaryComm%>">                                                                    
                                                                    <input type="hidden" id="lotNumberH" name="lotNumberH" value="">
                                                                    <input type="hidden" id="newStatusH" name="newStatusH" value="<%=newStatus%>">
                                                                    <input type="hidden" id="changeReasonH" name="changeReasonH" value="">
                                                                    <input type="hidden" id="simCardH" name="simCardH" value="">
                                                                    <input type="hidden" id="simCardIdH" name="simCardIdH" value="<%=simCardId%>">
                                                                    <input type="hidden" id="assDeasSimActionH" name="assDeasSimActionH" value="<%=assDeasSimAction%>">
                                                                    <input type="hidden" id="assDeasSimValueH" name="assDeasSimValueH" value="">
                                                                    <input type="hidden" id="deassignReasonH" name="deassignReasonH" value="">                                                                                                                                                                                                          
                                                                    <input id="btnSave" type="button" value="<%= Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.save",SessionData.getLanguage()) %>" onclick="enviarDatos(this.form)">                                                              
                                                                    <% 
                                                                        String strHist = "window.open('/support/admin/transactions/inventoryTerminals_history.jsp?idterminal=" + strIDConsultar + "','History','width=800,height=500,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');";
                                                                    %>
                                                                    <input id="btnHistory" type="button" value="<%= Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.history",SessionData.getLanguage()) %>" onclick=<% out.println(strHist); %>>                                                              
                                                                    <input id="btnCancel" type="button" value="<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsEdit.cancel",SessionData.getLanguage())%>" onClick="cancelar(this.form)">
                                                                </td>                                                                
							</tr>
							<tr><td><br></td></tr>                                                        
                                                    </form>
                                                 </table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
                                </tr>                                                 
                        </table>
                </td>
        </tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>
<%@ include file="/includes/footer.jsp" %>                                                   