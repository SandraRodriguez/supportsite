<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.reports.TransactionReport" %>
<%int section = 9;
int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
	String strControlNo = "";
	String strProductId = "";
	String strPINFormat = "";
	String strReasonId = "";
	String strComments = "";
	double dblExistentRequest =0;
	String strCheckReturnType = "";
	try
	{
		if (request.getParameter("ControlNo") != null)
		{
			strControlNo = request.getParameter("ControlNo");
			strProductId = request.getParameter("ProductId");		      
			strPINFormat = TransactionReport.getPINByControlNo(strControlNo, strProductId);
			dblExistentRequest = TransactionReport.checkExistentRequest(strControlNo);
			strCheckReturnType = TransactionReport.checkReturnTypeByProductId(strProductId);
		}
		else if (request.getParameter("submitted") != null)
		{
			try
		    {
			    if (request.getParameter("submitted").equals("y"))
				{
			    	if (request.getParameter("ReasonIdH") != null)
			    	{
			    		strReasonId = request.getParameter("ReasonIdH");
			    	}
			    	if (request.getParameter("CommentsH") != null)
			    	{
			    		strComments = request.getParameter("CommentsH");
			    	}
			    	if (request.getParameter("ControlNoH") != null)
			    	{
			    		strControlNo = request.getParameter("ControlNoH");
			    	}
			    	if (request.getParameter("ProductIdH") != null)
			    	{
			    		strProductId = request.getParameter("ProductIdH");
			    	}    		    	
			    	if (request.getParameter("PINFormatH") != null)
			    	{
			    		strPINFormat = request.getParameter("PINFormatH");
			    	}  			    	
				}
			}
			catch (Exception e){}  
		}		
	}
	catch (Exception e){}  
%>
<SCRIPT LANGUAGE="JavaScript">
	var count = 0
	var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

	function scroll()
	{
  		document.mainform.addRequest.disabled = true;
		document.mainform.addRequest.value = iProcessMsg[count];
  		count++
  		if (count = iProcessMsg.length) count = 0
  		setTimeout('scroll()', 150);
	}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle"><b><%=Languages.getString("jsp.includes.menu.pinreturn_request",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>  
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="left" valign="top" bgcolor="#FFFFFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%" align="left">
					<tr>
						<td class="main" align="left" nowrap><b><br><%=Languages.getString("jsp.admin.transactions.pinreturn_request.pin",SessionData.getLanguage())%>&nbsp;<%=strPINFormat%></b></td>
					</tr>						
					<tr>
						<td class="main" align="left">
						<form name="mainform" method="post" action="/support/admin/transactions/pinreturn_request_details.jsp" onSubmit="scroll();">
						<input type="hidden" name="submitted" value="y">
						<input TYPE="hidden" ID="ReasonIdH" NAME="ReasonIdH" VALUE="<%=strReasonId%>">
						<input TYPE="hidden" ID="ControlNoH" NAME="ControlNoH" VALUE="<%=strControlNo%>">
						<input TYPE="hidden" ID="ProductIdH" NAME="ProductIdH" VALUE="<%=strProductId%>">						
						<input TYPE="hidden" ID="CommentsH" NAME="CommentsH" VALUE="<%=strComments%>">	
						<input TYPE="hidden" ID="PINFormatH" NAME="PINFormatH" VALUE="<%=strPINFormat%>">							
											
						<table BORDER="0" CELLPADDING="4" CELLSPACING="0" height="1">
							<%
								if(strCheckReturnType == "RETURNOK")
								{
									if(dblExistentRequest == 0)
									{
							%>
										<tr>
											<td class="main" align=""left"" nowrap colspan="2"><br><%=Languages.getString("jsp.admin.transactions.pinreturn_request.request_information",SessionData.getLanguage())%><br><br></td>
										</tr>							
										<tr>
											<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.pinreturn_request.request_reason",SessionData.getLanguage())%></td>
											<td class="main">
												<select name="requestReasonList" id="requestReasonList">
							<%
													Vector vecRequestReasonList = TransactionReport.getRequestReasonList();
													Iterator itRRL = vecRequestReasonList.iterator();
													while (itRRL.hasNext())
													{
														Vector vecTempRRL = null;
														vecTempRRL = (Vector) itRRL.next();
														out.println("<option value=" + vecTempRRL.get(0) +">" + vecTempRRL.get(1) + "</option>");
													}
							%>                                	
												</select>								
											</td>								
										</tr>
										<tr>
											<td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.pinreturn_request.comments",SessionData.getLanguage())%></td>
											<td class="main" ><TEXTAREA ID="comments" NAME="comments" COLS="30" ROWS="5" ONPROPERTYCHANGE="if (this.value.length > 512) { this.value = this.value.substring(0, 512); }"></TEXTAREA></td>								
										</tr>
										<tr>
											<td class="main">
												<br><input type="hidden" name="search" value="y">
												<input type=submit name="addRequest" id="addRequest" value="<%=Languages.getString("jsp.admin.transactions.pinreturn_request.submit",SessionData.getLanguage())%>" onclick="doRequest();">
											</td>
											<td class="main">
												<br><br><input type=submit name="cancel" id="cancel" value="<%=Languages.getString("jsp.admin.transactions.pinreturn_request.cancel",SessionData.getLanguage())%>" onclick="doCancel();">
												<br><br>
											</td>
											<SCRIPT>
												function doRequest()
												{
													document.getElementById('ReasonIdH').value = document.getElementById('requestReasonList').value;
													document.getElementById('CommentsH').value = document.getElementById('comments').value; 
													document.getElementById('ControlNoH').value = document.getElementById('ControlNoH').value;
													document.getElementById('ProductIdH').value = document.getElementById('ProductIdH').value; 										
													document.getElementById('PINFormatH').value = document.getElementById('PINFormatH').value; 										
												}
												function doCancel()
												{
													document.mainform.action = '/support/admin/transactions/pinreturn_request.jsp';
													document.mainform.submit();
												}                                	
											</SCRIPT>									
										</tr>
							<%
									}
									else
									{
										if(dblExistentRequest < 0)
										{
							%>	
											<tr>
												<td class="main" nowrap><br><%=Languages.getString("jsp.admin.transactions.pinreturn_request.error_no_sale",SessionData.getLanguage())%><br><br></td>
											</tr>	
											<tr>
												<td bgcolor=#FFFFFF class=main align="left"><a href="admin/transactions/pinreturn_request.jsp"><%=Languages.getString("jsp.admin.transactions.pinreturn_request.continue",SessionData.getLanguage())%></a><br><br></td>
											</tr>												
							<%
										}
										else
										{
							%>
											<tr>
												<td class="main" nowrap><br><%=Languages.getString("jsp.admin.transactions.pinreturn_request.error_wrong_status",SessionData.getLanguage())%><br><br></td>
											</tr>	
											<tr>
												<td bgcolor=#FFFFFF class=main align="left"><a href="admin/transactions/pinreturn_request.jsp"><%=Languages.getString("jsp.admin.transactions.pinreturn_request.continue",SessionData.getLanguage())%></a><br><br></td>
											</tr>													
							<%
										}
									}
								}
								else
								{
							%>
									<tr>
										<td class="main" nowrap><br><%=Languages.getString("jsp.admin.transactions.pinreturn_request.error_return_type",SessionData.getLanguage())%><br><br></td>
									</tr>	
									<tr>
										<td bgcolor=#FFFFFF class=main align="left"><a href="admin/transactions/pinreturn_request.jsp"><%=Languages.getString("jsp.admin.transactions.pinreturn_request.continue",SessionData.getLanguage())%></a><br><br></td>
									</tr>													
							<%
								}
										
							%>												
						</table>
						</form>
						</td>
					</tr>
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>
