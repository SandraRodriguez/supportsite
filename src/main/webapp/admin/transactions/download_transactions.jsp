<%@ page import="java.util.*, com.debisys.utils.*" %>
<% session.removeAttribute("task"); %>
<jsp:useBean id="task" scope="session"
    class="com.debisys.utils.TaskBean"/>
<%
int section=3;
int section_page=4;
boolean downloadbar = false;
int maxdownloadbar = -1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
String positionMarkers = null;
int intRecordCount = 0;
int intSectionPage = 0;
int intPageSize = 50;
int recordcount = 0;

if (request.getParameter("download") != null)
{
  if (request.getParameter("section_page") != null)
  {
    try
    {
      intSectionPage=Integer.parseInt(request.getParameter("section_page"));
    }
    catch(NumberFormatException ex)
    {
     response.sendRedirect(request.getHeader("referer"));
     return;
    }
  }  
    if (request.getParameter("markers") != null) {
        positionMarkers = request.getParameter("markers"); 
    } 
    
    
      //DBSY-908 
    if (request.getParameter("recordcount") != null) {
        recordcount = Integer.valueOf(request.getParameter("recordcount")); 
    } 
      //DBSY-908 
    if (request.getParameter("downloadbar") != null) {
        downloadbar = Boolean.valueOf(request.getParameter("downloadbar")); 
    } 
      //DBSY-908 
    if (request.getParameter("maxdownloadbar") != null) {
        maxdownloadbar = Integer.valueOf(request.getParameter("maxdownloadbar")); 
    } 
    
    if (request.getParameter("merchantIds") != null) {
        TransactionSearch.setMerchantIds(request.getParameter("merchantIds"));
    }

  if (TransactionSearch.validateDateRange(SessionData))
  {
    if ( request.getParameter("millennium_no") != null )
    {
    	TransactionSearch.setMillennium_No(request.getParameter("millennium_no"));
    }
    String strUrlLocation = "";
	String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
			    
	if ( strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)  && (request.getParameter("chkUseTaxValue") != null))
    {
		if(  downloadbar ){
	     	task.setRunning(true);
	     	task.setrecordcount(recordcount);
	     	if(maxdownloadbar!=-1 && recordcount>maxdownloadbar){
	     		task.setmax(maxdownloadbar);
     			task.setrecordcount(maxdownloadbar);
     		}
	     	task.setvar(true,SessionData, intSectionPage, application, TransactionSearch);
		   new Thread(task).start();
			
			%> <jsp:forward page="status.jsp"/>
			<% 
			}
			else{
	   		 if(maxdownloadbar!=-1)
	     		TransactionSearch.setmax(maxdownloadbar);
     	 	strUrlLocation = TransactionSearch.downloadMx(SessionData, intSectionPage, application);
     	 	}
    
    }
    else 
    {
      	if( downloadbar ){
     	task.setRunning(true);
     	task.setrecordcount(recordcount);
	    if(maxdownloadbar!=-1  && recordcount>maxdownloadbar){
	     	task.setmax(maxdownloadbar);
     		task.setrecordcount(maxdownloadbar);
     	}
     	task.setvar(false,SessionData, intSectionPage, application, TransactionSearch);
		new Thread(task).start();
	   
		%> <jsp:forward page="status.jsp"/>
		<%
		}
		else{
	   		 if(maxdownloadbar!=-1)
	     			TransactionSearch.setmax(maxdownloadbar);
     	 	strUrlLocation = TransactionSearch.download(SessionData, intSectionPage, application);
     	 	}
    }   
    
    response.sendRedirect(strUrlLocation);
  }
  else
  {
     response.sendRedirect(request.getHeader("referer"));
     return;
  }
}
%>