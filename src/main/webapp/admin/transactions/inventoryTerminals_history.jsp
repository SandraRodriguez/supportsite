<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.*,
                 com.debisys.languages.Languages"%>
<%@ page import="com.debisys.terminalTracking.CPhysicalTerminal"%> 
<%@ page import="com.debisys.terminalTracking.CTerminalChange"%> 
<%@ page import="java.text.SimpleDateFormat"%> 

<%
int section = 9;
int section_page = 5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<% 
String strIDConsultar = request.getParameter("idterminal");
if (strIDConsultar == null)
{
strIDConsultar = "";
}

//Establecer el ID de busqueda
CPhysicalTerminal terminalFisico = new CPhysicalTerminal();
terminalFisico.setTerminalId(Long.parseLong(strIDConsultar));
//Obtener el conjunto de registros de cambio
CTerminalChange[] statusChangesList = terminalFisico.getTerminalChanges();
%> 

<%@ include file="/includes/security.jsp" %>
<html>
<head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <!-- <title>Debisys</title>-->
</head>
<body bgcolor="#ffffff" onload="window.focus()">    
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
                <td width="18" height="20"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
                <td background="../../images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsHistory.title",SessionData.getLanguage()).toUpperCase()%></td>
                <td width="12" height="20"><img src="../../images/top_right_blue.gif"></td>            
	</tr>
	<tr>
		<td colspan=5>
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">                                                   
                                                    <tr><td>&nbsp;</td></tr>
                                                    <tr>
                                                        <td class=rowhead2>#</td>
                                                        <td class=rowhead2><%= Languages.getString("jsp.admin.transactions.inventoryTerminalsHistory.dateChange",SessionData.getLanguage()).toUpperCase() %></td>
                                                        <td class=rowhead2><%= Languages.getString("jsp.admin.transactions.inventoryTerminalsHistory.previousState",SessionData.getLanguage()).toUpperCase() %></td>
                                                        <td class=rowhead2><%= Languages.getString("jsp.admin.transactions.inventoryTerminalsHistory.newState",SessionData.getLanguage()).toUpperCase() %></td>
                                                        <td class=rowhead2><%= Languages.getString("jsp.admin.transactions.inventoryTerminalsHistory.changeReason",SessionData.getLanguage()).toUpperCase() %></td>
                                                    </tr>
                                                    <% 
                                                        if(statusChangesList!=null && statusChangesList.length>0){
                                                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                            int intEvenOdd = 1;
                                                            
                                                            for(int i=0;i<statusChangesList.length;i++){
                                                                CTerminalChange statusChange = statusChangesList[i];                                                                
                                                                out.println("<tr class=row" + intEvenOdd +">" ); 
                                                                out.println("<td>"+ (i+1)  +"</td><td>" + dateFormat.format(statusChange.getLastChangeDate()) + "</td>"); 
                                                                if(i<statusChangesList.length-1){
                                                                    CTerminalChange previousState = statusChangesList[i+1];  
                                                                    out.println("<td nowrap>" + previousState.getStatusDescription() + "</td>"); 
                                                                }else{
                                                                    out.println("<td nowrap></td>"); 
                                                                }
                                                                out.println("<td nowrap>" + statusChange.getStatusDescription() + "</td>"); 
                                                                out.println("<td nowrap>" + statusChange.getChangeReason() + "</td>"); 
                                                                out.print("</tr>");
                                                                if (intEvenOdd == 1)
                                                                    { 
                                                                      intEvenOdd = 2;
                                                                    }
                                                                    else
                                                                    {
                                                                      intEvenOdd = 1;
                                                                    }
                                                            }
                                                        }                                                                                                        
                                                    %>

                                                    <tr><td><br></td></tr>
                                                    <tr align="center">
                                                            <td>&nbsp;</td>
                                                            <td align=center colspan="5">
                                                                <input id="btnClose" type="button" value="<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsHistory.closeWindow",SessionData.getLanguage())%>" onClick="window.close();">
                                                            </td>                                                                
                                                    </tr>
                                                    <tr><td><br></td></tr>                                                        
                                                 </table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
                                </tr>
                        </table>
                </td>
        </tr>
</table>                                               
</body>
</html>