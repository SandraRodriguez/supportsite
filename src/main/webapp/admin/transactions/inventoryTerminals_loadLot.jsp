<%@ page import="com.debisys.terminalTracking.CPhysicalTerminal"%>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page import="org.apache.commons.fileupload.FileItem"%>
<%@ page import="java.io.File"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.util.*"%>
<%@ page import="com.debisys.terminalTracking.CResultMessage"%>
<%@ page import="com.debisys.terminalTracking.CFileLoad"%>


<%
	int section = 9;
	int section_page = 5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>

<%
	String strMessage = request.getParameter("message");
	if (strMessage == null) {
		strMessage = "";
	}

	//Los envios multipart se utilizan cuando hay paso de archivos  
	if (ServletFileUpload.isMultipartContent(request)) {
		try {
			ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
			List fileItemsList = servletFileUpload.parseRequest(request);
			DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
			diskFileItemFactory.setSizeThreshold(40960); // bytes

			File repositoryPath = new File("/temp");
			diskFileItemFactory.setRepository(repositoryPath);

			String fileName = "";
			String dir = "";
			Iterator it = fileItemsList.iterator();
			String extension = ".csv";

			while (it.hasNext()) {
				FileItem item = (FileItem) it.next();
				if (!item.isFormField()) {
					Date now = new Date();
					fileName = "fileTerminal" +now.getTime()+ extension;
					dir = DebisysConfigListener
							.getDownloadPath(application);
					FileOutputStream fos = new FileOutputStream(dir
							+ "/" + fileName);
					fos.write(item.getString().getBytes());
					fos.flush();
					fos.close();
				}
			}
			CFileLoad fileLoad = new CFileLoad();
			CResultMessage msgResult = fileLoad.validateFileTerminal(application, fileName,SessionData.getProperty("ref_id"),SessionData);
			SessionData.setPropertyObj("msgResult", (CResultMessage)msgResult);
			response.sendRedirect("inventoryTerminals_loadLot.jsp");
		} catch (Exception e) {
		}
	}
%>

<%@ include file="/includes/header.jsp"%>
<script SRC="includes/funcionesValidacion.js" type="text/javascript"> </script>
<script type="text/javascript">   
        
function enviarDatos(form) {
    //alert("las validaciones estan desactivadas temporalmente");
      if(validacion(form)){
        form.submit();
      }
    }
    
function validacion(form)
   {
     var alpha = 0;
     var errors = "";
     
     if ( document.getElementById('txtFileTerminals').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.emptyFile",SessionData.getLanguage())%>\n';
        alpha++;
     } 
     
        if (alpha != 0) {
            alert(errors);
            return false;
        }
        else {	  	
            return true;
        }      
   } 
   
function validarArchivoCSV(tField)   
    {
        var file = document.getElementById('txtFileTerminals').value;
        if(!file) return;
        var ext = file.substring(file.lastIndexOf(".")+1,file.length);       
        if(!(ext=="csv" || ext=="CSV")){
                alert('<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.badFileExt",SessionData.getLanguage())%>\n');
                document.getElementById('txtFileTerminals').value='';
                document.getElementById('btnProcess').disabled = true;               
        }else{
          document.getElementById('btnProcess').disabled = false;
        }            
    }
    
</script>
<table border="0" cellpadding="0" cellspacing="0" width="60%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img
			src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages
									.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img
			src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan=3>
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%"
					align="center">
					<form name="mainform" method="post"
						action="admin/transactions/inventoryTerminals_loadLot.jsp"
						enctype="multipart/form-data">
					<tr>
						<td colspan="4" class="main"><br>
						&nbsp;<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.instructions",SessionData.getLanguage())%>
						<br>
						</td>
					</tr>
					<tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<td class="main" align="center" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.file",SessionData.getLanguage())%>:</strong></td>
					<td><input type=file class="plain" name="fileTerminals" id="txtFileTerminals" size="70" onchange="validarArchivoCSV(this);"></td>
					</tr>
					<tr>
						<td><br>
						</td>
					</tr>
					<tr align="center">
						<td align=center colspan="4"><input type="hidden"
							name="submitted" value="y"> <input id="btnProcess"
							type="button"
							value="<%= Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.process",SessionData.getLanguage()) %>"
							onclick="enviarDatos(this.form)"
							disabled=true> 
							<input id="btnCancel"
							type="button"
							value="<%=Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.cancel",SessionData.getLanguage())%>"
							onClick="history.go(-1)"></td>
					</tr>
					<tr>
						<td><br>
						</td>
					</tr>
					<%
						Object obj = SessionData.getPropertyObj("msgResult");
						CResultMessage msg = (CResultMessage) obj;
						if (msg != null ) {%>
						  <tr>
							<td class="main" align="left" colspan="4">
							<br>
							<%if (msg.isError()) {%>
								  <font color="#ff0000"><b>
			                       <%=Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.errorTitle",SessionData.getLanguage())%>
			                       </b><br>
			                       <%=Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.errorInLine",SessionData.getLanguage())%>
			                       <%=msg.getNumberLine()%>.<%=msg.getMessage()%>
								  </font>							
							<%}else {%>
							       <b><%=Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLot.successfulTitle",SessionData.getLanguage())%></b>
							       <br>	                                
								   <%=msg.getMessage()%> <%=msg.getTotalRecord()%>								
							<%}%>
							<br>
							<br>								
							</td>
						  </tr>					       
					   <%}
						msg = null;
					   %>
					</form>					
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>			
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>				
		</table>
		</td>
	</tr>
</table>

<%@ include file="/includes/footer.jsp"%>
