<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.debisys.terminalTracking.CProvider"%>
<%@ page import="com.debisys.terminalTracking.CSimCard"%>
<%@ page import="com.debisys.terminalTracking.CTerminalPart"%>
<%@ page import="com.debisys.terminalTracking.CTerminalPartStatus"%>
<%@ page import="com.debisys.utils.NumberUtil"%>
<%@ page import="java.net.URLEncoder" %>

<jsp:useBean id="CSimCard" class="com.debisys.terminalTracking.CSimCard" scope="session"/>
<jsp:setProperty name="CSimCard" property="col"/>
<jsp:setProperty name="CSimCard" property="sort"/>
<!---->
<jsp:setProperty name="CSimCard" property="mobileSubscriberIdH"/>
<jsp:setProperty name="CSimCard" property="lotNumberH"/>
<jsp:setProperty name="CSimCard" property="phoneNumberH"/>
<jsp:setProperty name="CSimCard" property="carrierIdH"/>
<jsp:setProperty name="CSimCard" property="statusIdH"/>
<jsp:setProperty name="CSimCard" property="startDateH"/>
<jsp:setProperty name="CSimCard" property="endDateH"/> 
<%
int section = 9;
int section_page = 5;

String mobileSubscriberId = ""; //international mobile subscriber ID (IMSI)
String phoneNumber = "";
String lotNumber = "";
String carrierId = "";
String statusId = "";
String startDate = "";
String endDate = "";
boolean submitted = false;
long currentPage = 0;

int intPage = 1;
int intPageCount = 1;
int intPageSize = 100;

if (request.getParameter("simSearchSubmitted") != null)
{
	try
    {
	    if (request.getParameter("simSearchSubmitted").equals("y"))
		{
	    	submitted = true;
	    	mobileSubscriberId = (request.getParameter("mobileSubscriberIdH") != null)?request.getParameter("mobileSubscriberIdH"):"";
	    	phoneNumber = (request.getParameter("phoneNumberH") != null)?request.getParameter("phoneNumberH"):"";
	    	lotNumber = (request.getParameter("lotNumberH") != null)?request.getParameter("lotNumberH"):"";
	    	carrierId = (request.getParameter("carrierIdH") != null)?request.getParameter("carrierIdH"):"";
	    	statusId = (request.getParameter("statusIdH") != null)?request.getParameter("statusIdH"):"";
	    	startDate = (request.getParameter("startDateH") != null)?request.getParameter("startDateH"):"";
   			endDate = (request.getParameter("endDateH") != null)?request.getParameter("endDateH"):"";
   			currentPage = (request.getParameter("currentPageH") != null)?Long.valueOf(request.getParameter("currentPageH")):0;
		}
    }
	catch (Exception e){}
}

%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script SRC="includes/funcionesValidacion.js" type="text/javascript"> </script>
<!---->
<script type="text/javascript">   

function saveCurrentFieldValues(currentPage)
{
	document.getElementById('mobileSubscriberIdH').value = document.getElementById('textImsi').value;
	document.getElementById('phoneNumberH').value = document.getElementById('textPhoneNumber').value;
	document.getElementById('lotNumberH').value = document.getElementById('txtLotNumber').value;
	document.getElementById('carrierIdH').value = document.getElementById('ddlCarrier').value;
	document.getElementById('statusIdH').value = document.getElementById('ddlStatus').value;
	document.getElementById('startDateH').value = document.getElementById('txtStartDate').value;
	document.getElementById('endDateH').value = document.getElementById('txtEndDate').value;
	document.getElementById('currentPageH').value = currentPage;
	document.mainform.submit();
}

function keepForm() 
{
	document.getElementById('textImsi').value = document.getElementById('mobileSubscriberIdH').value;
	document.getElementById('textPhoneNumber').value = document.getElementById('phoneNumberH').value;
	document.getElementById('txtLotNumber').value = document.getElementById('lotNumberH').value;
	document.getElementById('ddlCarrier').value = document.getElementById('carrierIdH').value;
	document.getElementById('ddlStatus').value = document.getElementById('statusIdH').value;
	document.getElementById('txtStartDate').value = document.getElementById('startDateH').value;
	document.getElementById('txtEndDate').value = document.getElementById('endDateH').value;	
}

function enviarDatos(btn)
   {   
       if(btn.id=="btnSearch"){
       		saveCurrentFieldValues(1);
            if(validacion(btn.form)){
                btn.form.action="admin/transactions/inventorySims_search.jsp";
            }
            else{
                return false;
            }
       }
       else if(btn.id=="btnAddSim"){
            btn.form.action="admin/transactions/inventorySims_add.jsp";       
       }
       else if(btn.id=="btnLoadLot"){
            btn.form.action="admin/transactions/inventorySims_loadLot.jsp";       
       }       
       btn.form.submit();       
   }  
   
function validacion(form)
   {
     var alpha = 0;
     var errors = "";
     
     if ( document.getElementById('txtStartDate').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySims.emptystartdate",SessionData.getLanguage())%>\n';
        alpha++;
     }   
            
     if ( document.getElementById('txtEndDate').value.length == 0 ){
        errors+='<%=Languages.getString("jsp.admin.transactions.inventorySims.emptyenddate",SessionData.getLanguage())%>\n';
        alpha++;
     }        
          
    if (alpha != 0) {
        alert(errors);
        return false;
    }
    else {	  	
        return true;
    }      
   }
</script>

<LINK HREF="default.css" TYPE="text/css" REL="stylesheet">
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />


<form name="mainform" method="post">
<input type="hidden" id="simSearchSubmitted" name="simSearchSubmitted" value="y">	
<input type="hidden" id="mobileSubscriberIdH" name="mobileSubscriberIdH">
<input type="hidden" id="phoneNumberH" name="phoneNumberH">
<input type="hidden" id="lotNumberH" name="lotNumberH">
<input type="hidden" id="carrierIdH" name="carrierIdH">
<input type="hidden" id="statusIdH" name="statusIdH">
<input type="hidden" id="startDateH" name="startDateH">
<input type="hidden" id="endDateH" name="endDateH">
<input type="hidden" id="currentPageH" name="currentPageH">

<table border="0" cellpadding="0" cellspacing="0" width="60%" >
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.transactions.inventorySims.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan=4>
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
							<tr><td>&nbsp;</td></tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySims.imsi",SessionData.getLanguage())%>:</td>                                                         
							    <td><input type=text class="plain" name="imsi" id="textImsi" maxlength="20" onkeypress="return validaNum(event);" value="<%=CSimCard.getMobileSubscriberIdH()%>"></td>                                                           
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySims.phoneNumber",SessionData.getLanguage())%>:</td>
							    <td><input type=text class="plain" name="phoneNumber" id="textPhoneNumber" maxlength="15" onkeypress="return validaNum(event);" value="<%=CSimCard.getPhoneNumberH()%>"></td>                                                                
							</tr> 
							<tr>
                                                            <td>&nbsp;</td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySims.lotNumber",SessionData.getLanguage())%>:</td>
							    <td><input type=text class="plain" name="lotNumber" id="txtLotNumber" onkeypress="return validaNumYTexto(event);" value="<%=CSimCard.getLotNumberH()%>"></td>                                                                
							</tr>  
							<tr>
                                                            <td>&nbsp;</td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySims.carrier",SessionData.getLanguage())%>:</td>
							    <td>
							    	<select name="carrier" id="ddlCarrier">
                                    	<option value="0"><%=Languages.getString("jsp.admin.transactions.inventorySims.optionAll",SessionData.getLanguage())%></option>
                                    	<%
                                    		CProvider[] providerList = CProvider.getProviderList();
                                    		if(providerList != null)
                                    		{
                                    			for(CProvider currentProvider:providerList)
                                    			{
                                    				out.println("<option value=" + currentProvider.getProviderId()+" "+(CSimCard.getCarrierIdH().equals(currentProvider.getProviderId())? "selected": "") +">" + currentProvider.getDescription() + "</option>");
                                    			}
                                    		}
                                    	%>
                                    </select>
                                                            </td>
                                                            
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySims.status",SessionData.getLanguage())%>:</td>
							    <td>
							    	<select name="status" id="ddlStatus">
                                        <option value="0"><%=Languages.getString("jsp.admin.transactions.inventorySims.optionAll",SessionData.getLanguage())%></option>
                                        <%
                                        	CTerminalPartStatus[] partStatus = CTerminalPartStatus.getAllPartStatus();
	                                   		if(partStatus != null)
	                                   		{
	                                   			for(CTerminalPartStatus currentStatus:partStatus)
	                                   			{
	                                   				out.println("<option value=" + currentStatus.getPartStatusId()+" "+(CSimCard.getStatusIdH().equals(currentStatus.getPartStatusId())? "selected": "")+">" + currentStatus.getPartStatusDescription() + "</option>");
	                                   			}
	                                   		}
                                    	%>
                                        
                                    </select>
                                                            </td>
							</tr>  
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td class="main" nowrap><strong><%=Languages.getString("jsp.admin.transactions.inventorySims.activationDate",SessionData.getLanguage())%>:</strong></td>
                                                        </tr>
							<tr>
                                                            <td>&nbsp;</td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySims.startDate",SessionData.getLanguage())%>:</td>
							    <td><input class="plain" id="txtStartDate" name="startDate" value="<%=CSimCard.getStartDateH()%>" readonly=true size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
							</tr>
							<tr>
                                                            <td></td>                                                            
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.transactions.inventorySims.endDate",SessionData.getLanguage())%>:</td>
							    <td> <input class="plain" id="txtEndDate" name="endDate" value="<%=CSimCard.getEndDateH()%>" readonly=true size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
							</tr>

							<tr><td><br></td></tr>
							<tr align="center">
                            <td>&nbsp;</td>
                            <td align=center colspan="4"><input id="btnSearch" type="button" value="<%=Languages.getString("jsp.admin.transactions.inventorySims.search",SessionData.getLanguage())%>" onclick="enviarDatos(this)">
                                <input id="btnAddSim" type="button" value="<%= Languages.getString("jsp.admin.transactions.inventorySims.addSim",SessionData.getLanguage()) %>" onclick="enviarDatos(this)">                                                                
                                <input id="btnLoadLot" type="button" value="<%=Languages.getString("jsp.admin.transactions.inventorySims.loadSimsLot",SessionData.getLanguage())%>" onclick="enviarDatos(this)">                                                              
                            </td>
							</tr>
							<tr><td><br></td></tr>
						</table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	            </tr>
			</table>
		</td>
	</tr>
	<% 
		long paramMobileSubscriberId = (mobileSubscriberId.equals(""))? -1: Long.valueOf(mobileSubscriberId);
		long paramPhoneNumber = (phoneNumber.equals(""))? -1: Long.valueOf(phoneNumber);
		long paramLotNumber = (lotNumber.equals(""))? -1: Long.valueOf(lotNumber);
		long paramCarrierId = (carrierId.equals(""))? -1: Long.valueOf(carrierId);
		long paramStatusId = (statusId.equals(""))? -1: Long.valueOf(statusId); 
		String paramStartDate = (startDate.equals(""))? null: startDate;
		String paramEndDate = (endDate.equals(""))? null: endDate;
		long startRecordIndex = currentPage;
		long pageLength = 100; 
		
		if (request.getParameter("page") != null) {
		  try {
		    intPage = Integer.parseInt(request.getParameter("page"));
		  } catch(NumberFormatException ex) {
		    intPage = 1;
		  }
		}

		if (intPage < 1) {
		  intPage = 1;
		}
		
		CTerminalPart[] partList = 
			CSimCard.getPartList(paramMobileSubscriberId, 
								 paramPhoneNumber, 
								 paramLotNumber, 
								 paramCarrierId, 
								 paramStatusId, 
								 paramStartDate, 
								 paramEndDate, 
								 intPageSize, 
								 intPage);
		
		
		
		int intRecordCount = 0;
		if ((partList != null) && (partList.length > 0)) {
			intRecordCount = Integer.parseInt("" + partList[0].getTotalRecords());
		}
		
		if (intRecordCount > 0) {
		  intPageCount = (intRecordCount / intPageSize);
		  if ((intPageCount * intPageSize) < intRecordCount) {
		    intPageCount++;
		  }
		}
		
		if((partList != null) && (partList.length > 0))
		{/*
			out.println("<tr><align=right class=\"main\" nowrap");
			long pageCount = partList[0].getTotalRecords() / pageLength;
			long i = 1;
			out.println("<font color=#ff0000>" + i++ + "</font>&nbsp;");
			if(pageCount > 1)
			{
				for(; i <= pageCount;i++)
				{
					out.println("<a href=\"javascript:void(0);\" onclick=\"saveCurrentFieldValues(" + i + ")\">" + i + "</a>&nbsp;");
				}
			}
			else
			{
				out.println("<a href=\"javascript:void(0);\" onclick=\"saveCurrentFieldValues(" + 1 + ")\">" + 1 + "</a>&nbsp;");
			}
			out.println("</td></tr>");*/
	%>
	<tr>
		<td colspan="3" align="right">
		<!-- 
		<td>&nbsp;</td>
	    <td class="main" nowrap><=Languages.getString("jsp.admin.transactions.inventorySims.rowsPerPageMsg",SessionData.getLanguage())%></td>                                                         
	    <td><input type=text class="plain" name="rowsxpage" id="rowsxpage" maxlength="5" onkeypress="return validaNum(event);"></td> --> 
<%
String restURL = "";//"&mobileSubscriberIdH="+mobileSubscriberId+ "&phoneNumberH="+phoneNumber+ "&lotNumberH="+lotNumber+ "&carrierIdH="+carrierId+ "&statusIdH="+statusId+ "&startDateH="+startDate+ "&endDateH="+endDate;
if (intPage > 1) {
  out.println("<a href=\"admin/transactions/inventorySims_search.jsp?simSearchSubmitted=y&page=1"+restURL+"\" >"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");//
  out.println("<a href=\"admin/transactions/inventorySims_search.jsp?simSearchSubmitted=y&page=" + (intPage - 1) + restURL+"\" >&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");//
}

int intLowerLimit = intPage - 12;
int intUpperLimit = intPage + 12;

if (intLowerLimit < 1) {
  intLowerLimit=1;
  intUpperLimit = 25;
}

for (int j = intLowerLimit; j <= intUpperLimit && j <= intPageCount; j++) {
  if (j == intPage) {
    out.println("<font color=#ff0000>" + j + "</font>&nbsp;");
  } else {
    out.println("<a href=\"admin/transactions/inventorySims_search.jsp?simSearchSubmitted=y&page=" + j + restURL+"\">" + j + "</a>&nbsp;");//
  }
}

if (intPage <= (intPageCount - 1)) {
  out.println("<a href=\"admin/transactions/inventorySims_search.jsp?simSearchSubmitted=y&page=" + (intPage + 1) + restURL+"\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");//
  out.println("<a href=\"admin/transactions/inventorySims_search.jsp?simSearchSubmitted=y&page=" + (intPageCount) +restURL+"\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");//
}

%>
		</td>
	</tr>
	<tr>
		<td colspan="3">					
			<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
				<tr>
					<td class="main"><br>
					<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
				    <table width="100%" cellspacing="1" cellpadding="1" border="0"id="t1">
      	      			<thead>
      	      				<tr>
      	      					<!--  class="SectionTopBorder" ...  class="sort-table" ... NOt using the JS sorting :( -->
 								<%
								 String strSortURL = "admin/transactions/inventorySims_search.jsp?simSearchSubmitted=y";
								 %>
		      	      			<td class=rowhead2>#</td>
				            	<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.partId",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=1&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=1&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
	              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.partType",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=2&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=2&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
	              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.mobilSubscriberId",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=3&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=3&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
	              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.phoneNumber",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=4&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=4&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>              				
	             				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.carrierId",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=5&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=5&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
				            	<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.activationDate",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=6&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=6&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
	              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.lotNumber",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=7&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=7&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
	              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.contractNumber",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=8&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=8&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
	              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.contractDate",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=9&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=9&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>             				
	             				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.currentStatus",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=10&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=10&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
	             				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.inventorySims.lastStatusChangeDate",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=11&sort=1<%=restURL%>"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=11&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
            				</tr>
            			</thead>
            			<%
            				
           					int rowBackGround = 1;
           					int intCounter = 1;
           					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
           					for(CTerminalPart currentPart: partList)
           					{
           						CSimCard currentSim = (CSimCard)currentPart;
                              		out.println("<tr class=row" + rowBackGround +">" +
                                   		"<td>" + intCounter++ + "</td>" +
                                   		"<td>" + currentSim.getPartId() + "</td>" +
                                   		"<td>" + currentSim.getPartType().getPartDescription() + "</td>" +
                                   		"<td><a href=\"admin/transactions/inventorySims_edit.jsp?partId="+currentSim.getPartId()+"\">" + currentSim.getMobileSuscriberId() + "</a></td>" +
                                   		"<td>" + currentSim.getPhoneNumber() + "</td>" +
                                   		"<td>" + currentSim.getProvider().getDescription() + "</td>" +
                                   		"<td>" + currentSim.getActivationDate(true) + "</td>" +
                                   		"<td>" + currentSim.getLotNumber() + "</td>" +
                                   		"<td>" + currentSim.getContractNumber() + "</td>" +
                                   		"<td>" + currentSim.getContractDate(true) + "</td>" +
                                   		"<td>" + currentSim.getStatus().getPartStatusDescription() + "</td>" +
                                   		"<td>" + dateFormat.format(currentSim.getLastChangeStatusDate().getTime()) + "</td>" +
                                  			"</tr>");       
                              		rowBackGround = (rowBackGround == 1)?2:1; 
           					}            				
            			%>
                    </table>  
					<br>
					<br><!-- 
						<SCRIPT type="text/javascript">
  							var stT1 = new SortROC(document.getElementById("t1"),
	  						["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
						</SCRIPT>	 -->					
					</td>
				</tr>
			</table>
		</td>
	</tr>			
	<%
	} // END if((partList != null) && (partList.length > 0))
	else if(submitted)
	{
		out.println("<tr><td>");
    	out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
    	out.println("</td></tr>");
	}
	%>
		
</table>
</form>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

