<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.showRep.disabled = true;
  document.mainform.showRep.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="main"><b><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory",SessionData.getLanguage())%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
					<tr>
						<td width="18">&nbsp;</td>
						<td class="main"><br>
						<form name="mainform" method="post" action="/support/admin/transactions/card_sales_search.jsp" onSubmit="scroll();">
							<input type="hidden" name="submitted" value="y">
							<INPUT TYPE="hidden" ID="CardProgramNameIds" NAME="CardProgramNameIds" VALUE="">
							<INPUT TYPE="hidden" ID="InventoryType" NAME="InventoryType" VALUE="">
							<table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
	                        		    <tr class="main">    	    	                	            	    	                	        
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage())%></td>
	                            		</tr>
	                            		<tr class="main">  
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="CardProgramList" multiple>
        	            	            		<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
            	    	                    	    <%
	            		                                Vector vecCardProgramList = CMS.getCardsProgramList(SessionData);
    	    	        	                            Iterator itCPL = vecCardProgramList.iterator();
    		                	                        while (itCPL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempCPL = null;
                	                	                	vecTempCPL = (Vector) itCPL.next();
                        	                		    	out.println("<option value=" + vecTempCPL.get(0) +">" + vecTempCPL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
	    	                        	                <br>*<%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.instructions",SessionData.getLanguage())%>
    	    	                	        </td>
    	    	                	    </tr>
										<tr>
											<td class=main valign=top nowrap>
												<label for=chkOwnInventory>
													<input id=chkOwnInventory type=radio class=plain name=searchByOption value=0 checked><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.own_inventory",SessionData.getLanguage())%>
												</label>
											</td>
										</tr>
										<% 
											if(!strAccessLevel.equals(DebisysConstants.MERCHANT))
											{
										%>
										<tr>
											<td class=main valign=top nowrap>
												<label for=chkDownlineInventory>
													<input id=chkDownlineInventory type=radio class=plain name=searchByOption value=1><%=Languages.getString("jsp.admin.transactions.manageinventory.current_inventory.downline_inventory",SessionData.getLanguage())%>
												</label>
											</td>
									   </tr>  
									   <% 
											}
									   %>									     	    	                	    	    	    	                	    
									   <tr>
									       <td class="main">
    									     <input type="hidden" name="search" value="y">
									         <input type="submit" name="showRep" id="showRep" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="showReport();">
            	    		                	<SCRIPT>
            	    		                		function showReport()
                                                    {
                                                    	document.mainform.action = '/support/admin/transactions/current_inventory_summary.jsp';
                                                        document.mainform.submit();
                                                    }
                                                </SCRIPT>									         
									       </td>
									   </tr>        	        	    	       	        	        	    	       	
								</table>        	        	    	       					
						</form>
						<br>
						<br>
						</td>
						<td width="18">&nbsp;</td>
					</tr>

				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp"%>