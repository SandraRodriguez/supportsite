<%@ page import="java.util.*,
         com.debisys.utils.*,
         java.net.URLEncoder,
         com.debisys.customers.Merchant,
         com.debisys.reports.TransactionReport,
         com.debisys.utils.HTMLEncoder,
         com.debisys.reports.schedule.TransactionsReport,
         com.debisys.schedulereports.ScheduleReport,
         com.debisys.reports.ReportsUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="task" scope="session" class="com.debisys.utils.TaskBean"></jsp:useBean>
<%
    String strReport = request.getParameter("report");
    int section = 3;
    boolean downloadbar = false;
    boolean maxdownloadbar = false;
    int maxdownloadset = -1;
    if (strReport != null && strReport.equals("y")) {
        section = 4;
    }
    else {
        strReport = "";
    }

    int section_page = 5;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%    Vector<Vector<Object>> vecSearchResults = new Vector<Vector<Object>>();
    Hashtable searchErrors = null;
    String merchantsFilter = null;
    int intRecordCount = 0;
    int intPage = 1;
    int intPageSize = 50;
    int intPageCount = 1;
    boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(SessionData, application);

//DBsy-905
    int imaxdownloadbar = -1;
    String positionMarkers = null;
    int intSectionPage = 0;
    int recordcount = 0;
    boolean isDownload = false;
    String strMerchantIds[] = null;
    TransactionsReport report = null;
    boolean allMerchantsSelected = false;
    String invoiceId = "";
    String reportTitleKey = "jsp.admin.reports.transactions.agents.title";
    String reportTitle = Languages.getString(reportTitleKey, SessionData.getLanguage()).toUpperCase();

//this flag means if the user is in International Default
    boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

    boolean showAccountIdQRCode = false;
    showAccountIdQRCode = SessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);

    if (request.getParameter("invoiceID") != null) {
        invoiceId = request.getParameter("invoiceID").toString();
        TransactionSearch.setInvoiceNo(invoiceId);
    }

    boolean viewReferenceCard = false;
    if (isInternationalDefault) {
        viewReferenceCard = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
    }

    String trReportStartDate = TransactionSearch.getStartDate();
    if ((trReportStartDate == null) || ((trReportStartDate != null) && (trReportStartDate.trim().equals("")))) {
        trReportStartDate = request.getParameter("startDate");
        TransactionSearch.setStartDate(trReportStartDate);
    }
    String trReportEndDate = TransactionSearch.getEndDate();
    if ((trReportEndDate == null) || ((trReportEndDate != null) && (trReportEndDate.trim().equals("")))) {
        trReportEndDate = request.getParameter("endDate");
        TransactionSearch.setEndDate(trReportEndDate);
    }
    SessionData.setProperty("start_date", trReportStartDate);
    SessionData.setProperty("end_date", trReportEndDate);

    if (request.getParameter("download") != null) {
        if (request.getParameter("section_page") != null) {
            try {
                intSectionPage = Integer.parseInt(request.getParameter("section_page"));
            } catch (NumberFormatException ex) {
                response.sendRedirect(request.getHeader("referer"));
                return;
            }
        }
        if (request.getParameter("markers") != null) {
            positionMarkers = request.getParameter("markers");
        }

        //DBSY-908 
        if (request.getParameter("recordcount") != null) {
            recordcount = Integer.valueOf(request.getParameter("recordcount"));
        }
        //DBSY-908 
        if (request.getParameter("downloadbar") != null) {
            downloadbar = Boolean.valueOf(request.getParameter("downloadbar"));
        }
        //DBSY-908 
        if (request.getParameter("maxdownloadbar") != null) {
            imaxdownloadbar = Integer.valueOf(request.getParameter("maxdownloadbar"));
        }

        if (request.getParameter("merchantIds") != null) {
            TransactionSearch.setMerchantIds(request.getParameter("merchantIds"));
        }

        if (TransactionSearch.validateDateRange(SessionData)) {
            if (request.getParameter("millennium_no") != null) {
                TransactionSearch.setMillennium_No(request.getParameter("millennium_no"));
            }
            String strUrlLocation = "";
            String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);

            if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (request.getParameter("chkUseTaxValue") != null)) {
                if (downloadbar) {
                    task.setRunning(true);
                    task.setrecordcount(recordcount);
                    if (imaxdownloadbar != -1 && recordcount > imaxdownloadbar) {
                        task.setmax(imaxdownloadbar);
                        task.setrecordcount(imaxdownloadbar);
                    }
                    task.setvar(true, SessionData, intSectionPage, application, TransactionSearch);
                    isDownload = true;
                }
                else {
                    if (imaxdownloadbar != -1) {
                        TransactionSearch.setmax(imaxdownloadbar);
                    }
                    strUrlLocation = TransactionSearch.downloadMx(SessionData, intSectionPage, application);
                    response.sendRedirect(strUrlLocation);
                }

            }
            else {
                if (downloadbar) {
                    task.setRunning(true);
                    task.setrecordcount(recordcount);
                    if (imaxdownloadbar != -1 && recordcount > imaxdownloadbar) {
                        task.setmax(imaxdownloadbar);
                        task.setrecordcount(imaxdownloadbar);
                    }
                    task.setvar(false, SessionData, intSectionPage, application, TransactionSearch);
                    isDownload = true;
                }
                else {
                    if (imaxdownloadbar != -1) {
                        TransactionSearch.setmax(imaxdownloadbar);
                    }
                    strUrlLocation = TransactionSearch.download(SessionData, intSectionPage, application);
                    response.sendRedirect(strUrlLocation);
                }
            }

        }
        else {
            response.sendRedirect(request.getHeader("referer"));
            return;
        }
    }// end : if (request.getParameter("download") != null)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
    else if (request.getParameter("sheduleReport") != null && request.getParameter("sheduleReport").equals("y")) {
        //TO SCHEDULE REPORT
        strMerchantIds = request.getParameterValues("mids");
        allMerchantsSelected = Boolean.valueOf(request.getParameter("allMerchantsSelected"));
        if ((strMerchantIds == null)
                || ((strMerchantIds != null)
                && ((strMerchantIds.length == 0) || ((strMerchantIds.length > 0) && (strMerchantIds[0].trim().equals("")))))) {
            allMerchantsSelected = true;
        }

        report = new TransactionsReport(SessionData, application, true, trReportStartDate, trReportEndDate,
                invoiceId, request.getParameter("PINNumber"),
                false, section_page, "", request.getParameter("repId"), request.getParameter("merchId"), StringUtil.arrayToString(request.getParameterValues("mids"), ","),
                request.getParameter("transactionID"), request.getParameter("millennium_no"), viewReferenceCard, intPageSize, intPage,
                SessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA), allMerchantsSelected);
        report.setReportTitle(reportTitle);
        if (report.scheduleReport()) {
            ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
            if (scheduleReport != null) {
                scheduleReport.setStartDateFixedQuery(report.getStart_date());
                scheduleReport.setEndDateFixedQuery(report.getEnd_date());
                scheduleReport.setTitleName(reportTitleKey);
            }
            response.sendRedirect(DebisysConstants.PAGE_TO_SCHEDULE_REPORTS);
        }
    }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    else if (request.getParameter("search") != null) {

        if (request.getParameter("page") != null) {
            try {
                intPage = Integer.parseInt(request.getParameter("page"));
            } catch (NumberFormatException ex) {
                intPage = 1;
            }
        }

        if (intPage < 1) {
            intPage = 1;
        }

        if (TransactionSearch.validateDateRange(SessionData)) {

            String strCSVMerchantIds = request.getParameter("csvMids");
            strMerchantIds = request.getParameterValues("mids");
            allMerchantsSelected = Boolean.valueOf(request.getParameter("allMerchantsSelected"));
            if ((strMerchantIds == null)
                    || ((strMerchantIds != null)
                    && ((strMerchantIds.length == 0) || ((strMerchantIds.length > 0) && (strMerchantIds[0].trim().equals("")))))) {
                allMerchantsSelected = true;
            }

            if (request.getParameter("invoiceID") != null) {
                invoiceId = request.getParameter("invoiceID").toString();
                TransactionSearch.setInvoiceNo(invoiceId);
            }
            if (strMerchantIds != null) {
                TransactionSearch.setMerchantIds(strMerchantIds);
            }
            else if (strCSVMerchantIds != null) {
                TransactionSearch.setMerchantIds(strCSVMerchantIds);
            }

            report = new TransactionsReport(SessionData, application, false, trReportStartDate, trReportEndDate,
                    invoiceId, request.getParameter("PINNumber"),
                    false, section_page, "", request.getParameter("repId"), request.getParameter("merchId"), StringUtil.arrayToString(request.getParameterValues("mids"), ","),
                    request.getParameter("transactionID"), request.getParameter("millennium_no"), viewReferenceCard, intPageSize, intPage,
                    SessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA), allMerchantsSelected);
            report.setReportTitle(reportTitle);

            vecSearchResults = report.getResults();
    //vecSearchResults = TransactionSearch.search(intPage, intPageSize, SessionData, section_page, application, false);

            intRecordCount = Integer.parseInt(vecSearchResults.get(0).get(0).toString());

        // DBSY 908
            // checking download bar settings if Enabled and if feature is needed.
            String download = DebisysConfigListener.getdownloadbar(application);
            if (download != null && !download.equals("")) {
                try {
                    int downloadset = Integer.parseInt(download);
                    if (downloadset <= intRecordCount) {
                        downloadbar = true;
                    }
                } catch (NumberFormatException e) {
                    downloadbar = false;
                }
            }

            String maxdownload = DebisysConfigListener.getmaxdownload(application);
            if (maxdownload != null && !maxdownload.equals("")) {
                try {
                    maxdownloadset = Integer.parseInt(maxdownload);
                    if (maxdownloadset < intRecordCount && maxdownloadset != 0) {
                        maxdownloadbar = true;
                    }
                } catch (NumberFormatException e) {
                    maxdownloadbar = false;
                }
            }
            vecSearchResults.removeElementAt(0);
            if (intRecordCount > 0) {
                intPageCount = (intRecordCount / intPageSize);
                if ((intPageCount * intPageSize) < intRecordCount) {
                    intPageCount++;
                }
            }
        }
        else {
            searchErrors = TransactionSearch.getErrors();
        }

    }
%>
<%@ include file="/includes/header.jsp" %>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT LANGUAGE="JavaScript">
    function check() {

        if (!IsNumeric(document.mainform.invoiceID.value)) {
            document.mainform.invoiceID.value = "";
            alert("Invoice must be numberic");
            return false;
        }
        if (!IsNumeric(document.mainform.transactionID.value)) {
            document.mainform.transactionID.value = "";
            alert("Transaction ID must be numberic");
            return false;
        }
        scroll();
        return true;
    }
    function IsNumeric(sText)
    {
        var ValidChars = "0123456789.";
        var IsNumber = true;
        var Char;


        for (i = 0; i < sText.length && IsNumber == true; i++)
        {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1)
            {
                IsNumber = false;
            }
        }
        return IsNumber;

    }
    var count = 0
    var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> > ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ", "< <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> < ", "# <%=Languages.getString("jsp.admin.processing", SessionData.getLanguage())%> # ");

    function scroll()
    {
        document.mainform.submit.disabled = true;
        document.mainform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll()', 150);
    }

    function scroll2()
    {
        document.downloadform.submit.disabled = true;
        document.downloadform.submit.value = iProcessMsg[count];
        count++
        if (count = iProcessMsg.length)
            count = 0
        setTimeout('scroll2("false")', 150);
    }

    function openViewPIN(sTransId)
    {
        var sURL = "/support/admin/transactions/view_pin.jsp?trans_id=" + sTransId;
        var sOptions = "left=" + (screen.width - (screen.width * 0.4)) / 2 + ",top=" + (screen.height - (screen.height * 0.3)) / 2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
        var w = window.open(sURL, "ViewPIN", sOptions, true);
        w.focus();
    }


</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
    <tr>
        <td width="18" height="20" align=left><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td class="formAreaTitle" align=left width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.agents.title", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20" align=right><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form name="mainform" method="get" action="admin/transactions/agents_transactions.jsp" onSubmit="return check();">
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <%if (strReport == null || strReport.equals("")) {%>
                                <tr>
                                    <td class="formArea2">
                                        <table width="300">
                                            <%
                                                Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
                                            %>
                                            <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
                                            <tr class="main">
                                                <td nowrap valign="top">
                                                    <%if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {%>
                                                    <%=Languages.getString("jsp.admin.select_date_range", SessionData.getLanguage())%>:</td>
                                                    <%}
                         else {%>
                                                    <%=Languages.getString("jsp.admin.select_date_range_international", SessionData.getLanguage())%>:</td>
                                                <%}%><td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td valign="top" nowrap>
                                                    <table width=400>
                                                        <%
                                                            if (searchErrors != null) {
                                                                out.println("<tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1", SessionData.getLanguage()) + ":<br>");
                                                                Enumeration enum1 = searchErrors.keys();
                                                                while (enum1.hasMoreElements()) {
                                                                    String strKey = enum1.nextElement().toString();
                                                                    String strError = (String) searchErrors.get(strKey);
                                                                    out.println("<li>" + strError);
                                                                }

                                                                out.println("</font></td></tr>");
                                                            }
                                                        %>
                                                        <tr class="main">
                                                            <td nowrap><%=Languages.getString("jsp.admin.start_date", SessionData.getLanguage())%>:</td><td><input class="plain" id="startDate" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12"><a href="javascript:void(0)" onclick="if (self.gfPop)
                                                                        gfPop.fStartPop(document.mainform.startDate, document.mainform.endDate);
                                                                    return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                                        </tr>
                                                        <tr class="main">
                                                            <td nowrap><%=Languages.getString("jsp.admin.end_date", SessionData.getLanguage())%>: </td><td> <input class="plain" id="endDate" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12"><a href="javascript:void(0)" onclick="if (self.gfPop)
                                                                        gfPop.fEndPop(document.mainform.startDate, document.mainform.endDate);
                                                                    return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                                                        </tr>

                                                        <tr class="main">
                                                            <td nowrap><%=Languages.getString("jsp.admin.reports.transaction_id", SessionData.getLanguage())%>: </td><td><input name="transactionID" id="transactionID" value="<%=TransactionSearch.getTransactionID()%>"></td> 
                                                        </tr>
                                                        <tr class="main">
                                                            <td nowrap><%=Languages.getString("jsp.admin.reports.pinreturn_search.pin", SessionData.getLanguage())%>: </td><td><input name="PINNumber" id="PINNumber" value="<%=TransactionSearch.getPINNumber()%>"></td> 
                                                        </tr>

                                                        <% if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
                                                                    && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                                                    && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {%>
                                                        <tr class="main">
                                                            <td nowrap><%=Languages.getString("jsp.admin.reports.invoiceno", SessionData.getLanguage())%>: </td><td><input name="invoiceID" id="invoiceID" value="<%=TransactionSearch.getInvoiceNo()%>"></td> 
                                                        </tr>
                                                        <%}
                else {%>
                                                        <tr style="display:none;" class="main">
                                                            <td style="display:none;" nowrap><%=Languages.getString("jsp.admin.reports.invoiceno", SessionData.getLanguage())%>: </td><td><input name="invoiceID" id="invoiceID" style="display:none;"  value="<%=TransactionSearch.getInvoiceNo()%>"></td> 
                                                        </tr>
                                                        <%} %>

                                                        <%
                                                            if (!strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                                        %>
                                                        <tr>
                                                            <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.merchants.option", SessionData.getLanguage())%></td>
                                                            <td class=main valign=top>
                                                                <select name="mids" id="mids" size="10" multiple>
                                                                    <option value="" selected><%=Languages.getString("jsp.admin.reports.all", SessionData.getLanguage())%></option>
                                                                    <%
                                                                            //Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);

                                                                        Vector vecMerchantList;

                                                                        String sTmp = SessionData.getProperty("ref_id");

                                                                        if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                                                                            TransactionReport.setCarrierId(sTmp);
                                                                            SessionData.setProperty("ref_id", request.getParameter("repId"));
                                                                            vecMerchantList = Merchant.getMerchantListReports(SessionData);
                                                                            SessionData.setProperty("ref_id", sTmp);
                                                                            TransactionReport.setCarrierId(null);

                                                                        }
                                                                        else {
                                                                            vecMerchantList = Merchant.getMerchantListReports(SessionData);
                                                                        }

                                                                        Iterator it = vecMerchantList.iterator();
                                                                        while (it.hasNext()) {
                                                                            Vector vecTemp = null;
                                                                            vecTemp = (Vector) it.next();
                                                                            out.println("<option value=" + vecTemp.get(0) + ">" + vecTemp.get(1) + "</option>");
                                                                        }

                                                                    %>
                                                                </select>
                                                                <br>
                                                                *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions", SessionData.getLanguage())%>
                                                            </td>
                                                        </tr>
                                                        <%
                                                            }
                                                        %>
                                                        <tr> 
                                                            <td class=main>
                                                                <input type="hidden" name="search" value="y">
                                                                <input type="hidden" name="repId" value="<%=TransactionSearch.getRepId()%>">      
                                                                <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.transactions.show_transactions", SessionData.getLanguage())%>">
                                                            </td>
                                                            <jsp:include page="/admin/reports/schreportoption.jsp">
                                                                <jsp:param
                                                                value="<%=SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS)%>"
                                                                    name="permissionEnableScheduleReports" />
                                                                <jsp:param value="<%=SessionData.getLanguage()%>"
                                                                           name="language" />
                                                            </jsp:include>

                                                        </tr>
                                                        <tr>
                                                            <td colspan=3 class="main">
                                                                * <%=Languages.getString("jsp.admin.reports.general", SessionData.getLanguage())%>
                                                            </td>
                                                        </tr>
                                                        <%}%>
                                                    </table>
                                                </td>
                                            </tr>
                                            </form>
                                        </table>
                                        <% if (isDownload) {%>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr><td>
                                                    <div><iframe width=350 height=200 name="iframe" id="iframe" src="admin/transactions/status.jsp" frameborder="0" style="visibility:visible;">
                                                        </iframe></div></td></tr></table><%
                                                                new Thread(task).start();
                                                            }
                                                            if (vecSearchResults != null && vecSearchResults.size() > 0) {
                                                        %>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                            <%
                                                if (strReport.equals("y")) {
                                                    Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
                                            %>
                                            <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote", SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
                                                    <%
                                                        }
                                                    %>
                                            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found", SessionData.getLanguage()) + " "%><%
                                                if (!TransactionSearch.getStartDate().equals("") && !TransactionSearch.getEndDate().equals("")) {
                                                    out.println(" " + Languages.getString("jsp.admin.from", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to", SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getEndDateFormatted()));
                                                }
                                                    %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{Integer.toString(intPage), Integer.toString(intPageCount)}, SessionData.getLanguage())%></td></tr>
                                            <tr><td class="main">
                                                    <%=Languages.getString("jsp.admin.iso_name", SessionData.getLanguage())%>:&nbsp;<%=SessionData.getUser().getCompanyName()%>
                                                </td></tr>
                                            <tr>
                                                <td align=left class="main" nowrap>

                                                    <%if (!isDownload) {%> 
                                                    <form name="downloadform" method=post action="admin/transactions/agents_transactions.jsp" onSubmit="scroll2();" >
                                                        <input type="hidden" name="startDate" value="<%=TransactionSearch.getStartDate()%>">
                                                        <input type="hidden" name="endDate" value="<%=TransactionSearch.getEndDate()%>">
                                                        <input type="hidden" name="page" value="<%=intPage%>">
                                                        <input type="hidden" name="section_page" value="<%=section_page%>">
                                                        <input type="hidden" name="repId" value="<%=TransactionSearch.getRepId()%>">
                                                        <input type="hidden" id="merchantIds" name="merchantIds" value="<%=TransactionSearch.getMerchantIds()%>">
                                                        <input type="hidden" name="transactionID" value="<%=TransactionSearch.getTransactionID()%>">     
                                                        <input type="hidden" name="invoiceID" value="<%=TransactionSearch.getInvoiceNo()%>">
                                                        <input type="hidden" name="PINNumber" value="<%=TransactionSearch.getPINNumber()%>">
                                                        <input type="hidden" id="recordcount" name="recordcount" value="<%=intRecordCount%>">
                                                        <input type="hidden" id="downloadbar" name="downloadbar" value="<%=downloadbar%>">
                                                        <input type="hidden" name="download" value="y">
                                                        <%
                                                            if ((strMerchantIds != null) && (strMerchantIds.length > 0)) {
                                                                for (int i = 0; i < strMerchantIds.length; i++) {
                                                        %>
                                                        <input type="hidden" name="mids" value="<%=strMerchantIds[i]%>">
                                                        <%
                                                                }
                                                            }
                                                        %>              
                                                        <% if (maxdownloadbar) {%><input type="hidden" id="maxdownloadbar" name="maxdownloadbar" value="<%=maxdownloadset%>"><% }%>
                                                        <input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download", SessionData.getLanguage())%>">
                                                    </form>
                                                    <br/>
                                                    <% if (maxdownloadbar) {%>
                                                    <FONT COLOR="red"> <%=Languages.getString("jsp.admin.warning_maxdownload", SessionData.getLanguage())%><%=maxdownloadset%><%=Languages.getString("jsp.admin.warning_secmaxdownload", SessionData.getLanguage())%>
                                                    </font><% }
                  } %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align=right class="main" nowrap>
                                                    <%
                                                        merchantsFilter = "&mids=" + TransactionSearch.getMerchantIds();
                                                        if (intPage > 1) {
                                                            out.println("<a href=\"admin/transactions/agents_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + TransactionSearch.getRepId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=1&report=" + strReport + merchantsFilter + "\">First</a>&nbsp;");
                                                            out.println("<a href=\"admin/transactions/agents_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + TransactionSearch.getRepId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + (intPage - 1) + "&report=" + strReport + merchantsFilter + "\">&lt;&lt;Prev</a>&nbsp;");
                                                        }

                                                        int intLowerLimit = intPage - 12;
                                                        int intUpperLimit = intPage + 12;

                                                        if (intLowerLimit < 1) {
                                                            intLowerLimit = 1;
                                                            intUpperLimit = 25;
                                                        }

                                                        for (int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) {
                                                            if (i == intPage) {
                                                                out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                                                            }
                                                            else {
                                                                out.println("<a href=\"admin/transactions/agents_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + TransactionSearch.getRepId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + i + "&report=" + strReport + merchantsFilter + "\">" + i + "</a>&nbsp;");
                                                            }
                                                        }

                                                        if (intPage <= (intPageCount - 1)) {
                                                            out.println("<a href=\"admin/transactions/agents_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + TransactionSearch.getRepId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + (intPage + 1) + "&report=" + strReport + merchantsFilter + "\">Next&gt;&gt;</a>&nbsp;");
                                                            out.println("<a href=\"admin/transactions/agents_transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&repId=" + TransactionSearch.getRepId() + "&startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8") + "&page=" + (intPageCount) + "&report=" + strReport + merchantsFilter + "\">Last</a>");
                                                        }

                                                    %>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" cellspacing="1" cellpadding="2" class="sort-table">
                                            <tr>
                                                <td class=rowhead2>#</td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba", SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date", SessionData.getLanguage()).toUpperCase()%></td>

                                                <%
                                                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.phys_state", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }
                                                %>              
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.city", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.county", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.reference", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.recharge", SessionData.getLanguage()).toUpperCase()%></td>   

                                                <%
                                                    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                                %>               	  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%               	 	}
                                                else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                                %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.iso_percent", SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                	 }
                                                }
                                                else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                                %>                   	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.agent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                  }
                                                else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                                %>		              <td class=rowhead2><%= Languages.getString("jsp.admin.subagent_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                 	}
                                                else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                                %>					  <td class=rowhead2><%= Languages.getString("jsp.admin.rep_percent", SessionData.getLanguage()).toUpperCase()%></td>	
                                                <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                  }
                                                else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                                %>                    <td class=rowhead2><%= Languages.getString("jsp.admin.merchant_percent", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%                  }
                                                %>				


                                                <%
                                                    }
                                                    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                                %>


                                                <%//DBSY-905
                                                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                                            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.bonus", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.total_recharge", SessionData.getLanguage()).toUpperCase()%></td>

                                                <% if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.netAmount", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxAmount", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxpercentage", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.taxtype", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%}
                                            }%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.balance", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.product", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.description", SessionData.getLanguage()).toUpperCase()%></td>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%             if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT)) { 		//Authorization Carrier Number
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_carrier_number", SessionData.getLanguage()).toUpperCase()%></td>					  
                                                <%
                                                    }

                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
                                                            && strAccessLevel.equals(DebisysConstants.ISO)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.pin_number", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }
                                                %>
                                                <%
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
                                                            && strAccessLevel.equals(DebisysConstants.ISO)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ach_date", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }
                                                %> 
                                                <%
                                                    if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                %>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_number", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%
                                                    }
                                                %>                
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_type", SessionData.getLanguage()).toUpperCase()%></td>

                                                <%   if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                                            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.reports.invoiceno", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%} %>

                                                <%if (viewReferenceCard) {%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.DTU1204", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%}%>
                                                <%if (showAccountIdQRCode) {%>
                                                <td class=rowhead2><%=Languages.getString("jsp.admin.report.transactions.reference", SessionData.getLanguage()).toUpperCase()%></td>
                                                <%}
                                                if (isIntlAndHasDataPromoPermission) {
%>
                                                    <td class="rowhead2"><%=Languages.getString("jsp.reports.reports.transactions.form.table.column.name.promoData", SessionData.getLanguage()).toUpperCase()%></td>
<%                                                  
                                                }
%>
                                            </tr>
                                            <%
                                                int intCounter = 1;
                                                //intCounter = intCounter - ((intPage-1)*intPageSize);

                                                Iterator it = vecSearchResults.iterator();
                                                int intEvenOdd = 1;
                                                String type = "";
                                                String row = "";
                                                while (it.hasNext()) {
                                                    Vector vecTemp = null;
                                                    vecTemp = (Vector) it.next();
                                                    // check if type is "Return" of "Adjust", if so, display the whole row in red
                                                    type = (String) vecTemp.get(15);
                                                    if (type.equals("Return") || type.equals("Adjustment") || type.equals("Credit") || type.equals("Write Off")) {
                                                        row = "rowred";
                                                    }
                                                    else {
                                                        row = "row";
                                                    }

                                                    String sViewPIN = "", sPhysState = "", sAuthorizationNo = "";
                                                    String sPinNumber = "N/A";
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)) {
                                                        //if the trans_type=2 which is a pin sale
                                                        String strCurrencySymbol = NumberUtil.getCurrencySymbol();
                                                        if (vecTemp.get(14).toString().equals("2") && Double.parseDouble(vecTemp.get(9).toString().replaceAll(strCurrencySymbol, "")) > 0) {
                                                            sViewPIN = "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + vecTemp.get(0).toString() + "');\"><span class=\"showLink\">" + vecTemp.get(13).toString() + "</span></a>";
                                                            sPinNumber = vecTemp.get(20).toString();
                                                        }
                                                        else {
                                                            sViewPIN = vecTemp.get(13).toString();
                                                        }
                                                    }
                                                    else {
                                                        sViewPIN = vecTemp.get(13).toString();
                                                    }
                                                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                        sPhysState = "<td nowrap>" + vecTemp.get(17).toString() + "</td>";
                                                    }
                                                    else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                        sAuthorizationNo = "<td nowrap>" + vecTemp.get(16).toString() + "</td>";
                                                    }

                                                    out.println("<tr class=" + row + intEvenOdd + ">"
                                                            + "<td>" + intCounter++ + "</td>"
                                                            + "<td>" + vecTemp.get(0) + "</td>"
                                                            + "<td>" + vecTemp.get(1) + "</td>"
                                                            + "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString()) + "</td>"
                                                            + "<td>" + vecTemp.get(3) + "</td>"
                                                            + "<td nowrap>" + vecTemp.get(4).toString() + "</td>"
                                                            + sPhysState
                                                            + //city
                                                            "<td nowrap>" + vecTemp.get(5).toString() + "</td>"
                                                            + //county
                                                            "<td nowrap>" + vecTemp.get(29).toString() + "</td>"
                                                            + //clerk
                                                            "<td nowrap>" + vecTemp.get(6) + "</td>"
                                                            + "<td>" + vecTemp.get(7) + "</td>"
                                                            + "<td>" + vecTemp.get(8) + "</td>"
                                                            + //DBSY-905 Tax Calculation
                                                            //amount
                                                            "<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>");

                                                    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
                                                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                                                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(32).toString()) + "</td>"); //iso
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(34).toString()) + "</td>"); //rep
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //merchant
                                                            }
                                                            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(32).toString()) + "</td>"); //iso
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(30).toString()) + "</td>"); //agente
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(31).toString()) + "</td>"); //subagente
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(34).toString()) + "</td>"); //rep
                                                                out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //merchant
                                                            }
                                                        }
                                                        else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(30).toString()) + "</td>"); //agente
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(31).toString()) + "</td>"); //subagente
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(34).toString()) + "</td>"); //rep
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //merchant
                                                        }
                                                        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(31).toString()) + "</td>"); //subagente
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(34).toString()) + "</td>"); //rep
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //merchant
                                                        }
                                                        else if (strAccessLevel.equals(DebisysConstants.REP)) {
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(34).toString()) + "</td>"); //rep
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //merchant
                                                        }
                                                        else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                                                            out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(33).toString()) + "</td>"); //merchant
                                                        }
                                                    }
                                                    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/

                                                    if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                                            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                                                        //bonus
                                                        out.println("<td nowrap>" + vecTemp.get(18) + "</td>"
                                                                + //total Recharge
                                                                "<td nowrap>" + vecTemp.get(19) + "</td>");
                                                        if (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                                                            //net amount 
                                                            out.println("<td nowrap>" + vecTemp.get(25) + "</td>"
                                                                    + //tax amount
                                                                    "<td nowrap>" + vecTemp.get(26) + "</td>"
                                                                    + "<td nowrap>" + vecTemp.get(27) + "%</td>"
                                                                    + "<td nowrap>" + vecTemp.get(28) + "</td>");
                                                        }
                                                    }
                                                    //bal
                                                    out.println("<td nowrap>" + NumberUtil.formatCurrency(vecTemp.get(10).toString()) + "</td>"
                                                            + //product
                                                            "<td nowrap width=200>" + vecTemp.get(11) + "</td>"
                                                            + //product description                                
                                                            "<td nowrap width=200>" + vecTemp.get(12) + "</td>"
                                                            + "<td>" + sViewPIN + "</td>");
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT)) {
                                                        //Authorization Number
                                                        out.println("<td nowrap>" + vecTemp.get(16) + "</td>");
                                                    }
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
                                                            && strAccessLevel.equals(DebisysConstants.ISO)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                        out.println("<td>" + sPinNumber + "</td>");
                                                    }
                                                    if (SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
                                                            && strAccessLevel.equals(DebisysConstants.ISO)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                                        out.println("<td>" + vecTemp.get(21) + "</td>");
                                                    }
                                                    out.println(sAuthorizationNo
                                                            + "<td>" + type + "</td>");

                                                    if (com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application)
                                                            && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                                            && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                                                        //DBSY-919 Invoice Number Field
                                                        out.println("<td>" + vecTemp.get(24) + "</td>");
                                                        //////  
                                                    }

                                                    if (viewReferenceCard) {
                                                        out.println("<td>" + vecTemp.get(35) + "</td>");
                                                    }
                                                    if (vecTemp.size() >= 37 && showAccountIdQRCode) { // label QRCode
                                                        out.println("<td>" + vecTemp.get(36) + "</td>");
                                                    }
                                                    else if (showAccountIdQRCode) {
                                                        out.println("<td></td>");
                                                    }
                                                    if (isIntlAndHasDataPromoPermission) out.println("<td>" + ((vecTemp.size() >= 38) ? vecTemp.get(38) : "")  + "</td>");
                                                    
                                                    out.println("</tr>");

                                                    if (intEvenOdd == 1) {
                                                        intEvenOdd = 2;
                                                    }
                                                    else {
                                                        intEvenOdd = 1;
                                                    }

                                                }
                                                vecSearchResults.clear();
                                            %>
                                        </table>

                                        <%
                                            }
                                            else if (intRecordCount == 0 && request.getParameter("search") != null && searchErrors == null) {
                                                out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found", SessionData.getLanguage()) + "</font>");
                                            }
                                        %>
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>