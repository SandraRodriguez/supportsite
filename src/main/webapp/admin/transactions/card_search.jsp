<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
String strPlayerTypeId = "";
String strPlayerId = "";
String strCardProgramNameId = "";
String strCardProgramTypeId = "";
String strStatusId = "";
String strSerialNumber = "";
String strISOReceiptDate = "";
if (request.getParameter("submitted") != null)
{
	try
    {
	    if (request.getParameter("submitted").equals("y"))
		{
	    	if (request.getParameter("PlayerTypeId") != null)
	    	{
	    		strPlayerTypeId = request.getParameter("PlayerTypeId");
	    	}
	    	if (request.getParameter("CardProgramNameId") != null)
	    	{
	    		strCardProgramNameId = request.getParameter("CardProgramNameId");
	    	}
	    	if (request.getParameter("CardProgramTypeId") != null)
	    	{
	    		strCardProgramTypeId = request.getParameter("CardProgramTypeId");
	    	}
	    	if (request.getParameter("StatusId") != null)
	    	{
	    		strStatusId = request.getParameter("StatusId");
	    	}
	    	if (request.getParameter("SerialNumber") != null)
	    	{
	    		strSerialNumber = request.getParameter("SerialNumber");
	    	}
	    	if (request.getParameter("PlayerId") != null)
	    	{
	    		strPlayerId = request.getParameter("PlayerId");
	    	}	    	
	    	if (request.getParameter("ISOReceiptDate") != null)
	    	{
	    		strISOReceiptDate = request.getParameter("ISOReceiptDate");
	    	}
		}
	}
	catch (Exception e){}  
}	    
%>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.showRep.disabled = true;
  document.mainform.showRep.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif"
			width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="main"><b><%=Languages.getString("jsp.admin.transactions.manageinventory.card_search",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFF">
				<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
					<tr>
						<td width="18">&nbsp;</td>
						<td class="main"><br>
						<form name="mainform" method="post" action="/support/admin/transactions/card_search.jsp"  onSubmit="scroll();">
							<input type="hidden" name="submitted" value="y">
							<INPUT TYPE="hidden" ID="PlayerTypeId" NAME="PlayerTypeId" VALUE="<%=strPlayerTypeId%>">
							<INPUT TYPE="hidden" ID="PlayerId" NAME="PlayerId" VALUE="<%=strPlayerId%>">
							<INPUT TYPE="hidden" ID="CardProgramNameId" NAME="CardProgramNameId" VALUE="<%=strCardProgramNameId%>">							
							<INPUT TYPE="hidden" ID="CardProgramTypeId" NAME="CardProgramTypeId" VALUE="<%=strCardProgramTypeId%>">
							<INPUT TYPE="hidden" ID="StatusId" NAME="StatusId" VALUE="<%=strStatusId%>">							
							<INPUT TYPE="hidden" ID="SerialNumber" NAME="SerialNumber" VALUE="<%=strSerialNumber%>">							
							<INPUT TYPE="hidden" ID="ISOReceiptDate" NAME="ISOReceiptDate" VALUE="<%=strISOReceiptDate%>">							
							<table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
	                        		    <tr class="main">
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_type",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="PlayerTypeList" onchange="getCompanies();">
            	    	                    	    <%
	            		                                Vector vecPlayerTypeList = CMS.getCompanyRolesList(SessionData);
    	    	        	                            Iterator itPTL = vecPlayerTypeList.iterator();
    	    	        	                            String strSelectedPTL = "";
            	    	                    	    	out.println("<option " + "" + " value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itPTL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempPTL = null;
                	                	                	vecTempPTL = (Vector) itPTL.next();
                	                	                	if(!strPlayerTypeId.equals(""))
                	    	                    	    	{
               	                	                			if(strPlayerTypeId.equals(vecTempPTL.get(0)))
                   	    	                    	    		{
               	                	                				strSelectedPTL = "selected";
   	                	    	                    	    	}
       	        	                	                		else
           	    	                	                		{
       	        	                	                			strSelectedPTL = "";
               		                	                		}
                	    	                    	    	} 
                	                	                	else
                	                	                	{
                	                	                		strSelectedPTL = "";
                	                	                	}
                        	                		    	out.println("<option " + strSelectedPTL + " value=" + vecTempPTL.get(0) +">" + vecTempPTL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
            	    		                	<SCRIPT>
            	    		                		function getCompanies()
                                                    {
														document.getElementById('PlayerTypeId').value = document.getElementById('PlayerTypeList').value; 
														document.getElementById('PlayerId').value = document.getElementById('PlayerList').value; 
														document.getElementById('CardProgramNameId').value = document.getElementById('CardProgramList').value;
														document.getElementById('CardProgramTypeId').value = document.getElementById('CardProgramTypeList').value; 
														document.getElementById('StatusId').value = document.getElementById('StatusList').value; 
														document.getElementById('SerialNumber').value = document.getElementById('txtSerialNumber').value; 
														document.getElementById('ISOReceiptDate').value = document.getElementById('txtISOReceiptDate').value; 
                                                        document.mainform.submit();
                                                     }
                                                </SCRIPT>		    	                        	        
    	    	                	        </td>
    	    	                	    </tr>
	                        		    <tr class="main">    	    	                	        
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_name",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="PlayerList">
            	    	                    	    <%
            	    	                    	    	Vector vecPlayerList = CMS.getCompaniesbyRoleList(SessionData, strPlayerTypeId);
    	    	        	                            Iterator itPL = vecPlayerList.iterator();
            	    	                    	    	out.println("<option  value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itPL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempPL = null;
                	                	                	vecTempPL = (Vector) itPL.next();
                        	                		    	out.println("<option value=" + vecTempPL.get(0) +">" + vecTempPL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td> 
    	    	                	    </tr>
	                        		    <tr class="main">    	    	                	            	    	                	        
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="CardProgramList">
            	    	                    	    <%
	            		                                Vector vecCardProgramList = CMS.getCardsProgramList(SessionData);
    	    	        	                            Iterator itCPL = vecCardProgramList.iterator();
    	    	        	                            String strSelectedCPL = "";
    	    	        	                            out.println("<option  value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage()) + "</option>");
    		                	                        while (itCPL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempCPL = null;
                	                	                	vecTempCPL = (Vector) itCPL.next();
                	                	                	if(!strCardProgramNameId.equals(""))
                	    	                    	    	{
               	                	                			if(strCardProgramNameId.equals(vecTempCPL.get(0)))
                   	    	                    	    		{
               	                	                				strSelectedCPL = "selected";
   	                	    	                    	    	}
       	        	                	                		else
           	    	                	                		{
       	        	                	                			strSelectedCPL = "";
               		                	                		}
                	    	                    	    	} 
                	                	                	else
                	                	                	{
                	                	                		strSelectedCPL = "";
                	                	                	}
                        	                		    	out.println("<option " + strSelectedCPL + " value=" + vecTempCPL.get(0) +">" + vecTempCPL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td>
    	    	                	    </tr>
	                        		    <tr class="main">    	    	                	            	    	                	        
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program_type",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="CardProgramTypeList">
            	    	                    	    <%
	            		                                Vector vecCardProgramsTypesList = CMS.getCardProgramsTypesList();
    	    	        	                            Iterator itCPTL = vecCardProgramsTypesList.iterator();
    	    	        	                            String strSelectedCPTL = "";
            	    	                    	    	out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itCPTL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempCPTL = null;
                	                	                	vecTempCPTL = (Vector) itCPTL.next();
                	                	                	if(!strCardProgramTypeId.equals(""))
                	    	                    	    	{
               	                	                			if(strCardProgramTypeId.equals(vecTempCPTL.get(0)))
                   	    	                    	    		{
               	                	                				strSelectedCPTL = "selected";
   	                	    	                    	    	}
       	        	                	                		else
           	    	                	                		{
       	        	                	                			strSelectedCPTL = "";
               		                	                		}
                	    	                    	    	} 
                	                	                	else
                	                	                	{
                	                	                		strSelectedCPTL = "";
                	                	                	}
                        	                		    	out.println("<option " + strSelectedCPTL + " value=" + vecTempCPTL.get(0) +">" + vecTempCPTL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td>  
    	    	                	    </tr>
	                        		    <tr class="main">    	    	                	            	    	                	          	    	                	        
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.status",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
        	            	            		<select name="StatusList">
            	    	                    	    <%
	            		                                Vector vecStatusList = CMS.getStatusList();
    	    	        	                            Iterator itSL = vecStatusList.iterator();
    	    	        	                            String strSelectedSL = "";
    	    	        	                            out.println("<option  value=" + "-1" +">" + Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage()) + "</option>");
            	    	                    	    	while (itSL.hasNext())
	        	                	                    {
                	                	                	Vector vecTempSL = null;
                	                	                	vecTempSL = (Vector) itSL.next();
                	                	                	if(!strStatusId.equals(""))
                	    	                    	    	{
               	                	                			if(strStatusId.equals(vecTempSL.get(0)))
                   	    	                    	    		{
               	                	                				strSelectedSL = "selected";
   	                	    	                    	    	}
       	        	                	                		else
           	    	                	                		{
       	        	                	                			strSelectedSL = "";
               		                	                		}
                	    	                    	    	} 
                	                	                	else
                	                	                	{
                	                	                		strSelectedSL = "";
                	                	                	}
                        	                		    	out.println("<option " + strSelectedSL + " value=" + vecTempSL.get(0) +">" + vecTempSL.get(1) + "</option>");
                            	        	    		}
	                            	    	     	%>                                	
	    	                        	        </select>
    	    	                	        </td>  
    	    	                	    </tr>
	                        		    <tr class="main">    	    	                	            	    	                	          	    	                	           	    	                	        
	                            		    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.serial_number",SessionData.getLanguage())%></td>
    	                    	    	    <td class=main valign=top>
	    	                    	    	    <input class="plain" name="txtSerialNumber" value="<%=strSerialNumber%>" size="50">
    	                    	    	    </td> 
    	    	                	    </tr>
	                        		    <tr class="main">    	    	                	            	                    	    	       	                    	    	    
    										<td  class=main valign=top nowrap><%=Languages.getString("jsp.admin.transactions.manageinventory.iso_receipt_date",SessionData.getLanguage())%></td>
											<td class=main valign=top>
												<input class="plain" name="txtISOReceiptDate" value="<%=strISOReceiptDate%>" size="12">
												<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.txtISOReceiptDate,document.mainform.txtISOReceiptDate);return false;" HIDEFOCUS>
												<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
    	    	                	        </td>    	    	                	           	    	                	            	    	                	        
        	        	    	       </tr>
									   <tr>
									       <td class="main">
    									     <input type="hidden" name="search" value="y">
									         <input type="submit" name="showRep" id="showRep" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>" onclick="showReport();">
            	    		                	<SCRIPT>
            	    		                		function showReport()
                                                    {
														document.getElementById('PlayerTypeId').value = document.getElementById('PlayerTypeList').value; 
														document.getElementById('PlayerId').value = document.getElementById('PlayerList').value; 
														document.getElementById('CardProgramNameId').value = document.getElementById('CardProgramList').value;
														document.getElementById('CardProgramTypeId').value = document.getElementById('CardProgramTypeList').value; 
														document.getElementById('StatusId').value = document.getElementById('StatusList').value; 
														document.getElementById('SerialNumber').value = document.getElementById('txtSerialNumber').value; 
														document.getElementById('ISOReceiptDate').value = document.getElementById('txtISOReceiptDate').value;                                                     
                                                    	document.mainform.action = '/support/admin/transactions/card_search_summary.jsp';
                                                        document.mainform.submit();
                                                    }
                                                </SCRIPT>									         
									       </td>
									   </tr>        	        	    	       	        	        	    	       	
								</table>        	        	    	       					
						</form>
						<br>
						<br>
						</td>
						<td width="18">&nbsp;</td>
					</tr>

				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp"%>
