<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.languages.Languages,
                 com.debisys.transactions.CMS" %>

<%int section = 9;
int section_page = 3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData"
	scope="session" />
<%@ include file="/includes/security.jsp"%>
<%
Vector    vecSearchResults = new Vector();
String strCardProgramId = "";
String strExternalCompanyID = "";
String strCompanyID = "";
String strReportType = "";
String strStartDate = "";
String strEndDate = "";
	try
    {
    	if (request.getParameter("CardProgramId") != null)
    	{
    		strCardProgramId = request.getParameter("CardProgramId");
    	}
    	if (request.getParameter("ExternalCompanyID") != null)
    	{
    		strExternalCompanyID = request.getParameter("ExternalCompanyID");
    	}
    	if (request.getParameter("CompanyID") != null)
    	{
    		strCompanyID = request.getParameter("CompanyID");
    	}
    	if (request.getParameter("ReportType") != null)
    	{
    		strReportType = request.getParameter("ReportType");
    	}
    	if (request.getParameter("StartDate") != null)
    	{
    		strStartDate = request.getParameter("StartDate");
    	}
    	if (request.getParameter("EndDate") != null)
    	{
    		strEndDate = request.getParameter("EndDate");
    	}
    	
		vecSearchResults = CMS.getCurrentCardsQtySales(SessionData, strCardProgramId, strExternalCompanyID, strCompanyID, strStartDate, strEndDate, strReportType);
	}
	catch (Exception e){}  
%>
<HTML>
<HEAD>
    <LINK HREF="../../default.css" TYPE="text/css" REL="stylesheet">
    <LINK href="../../includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <TITLE><%=Languages.getString("jsp.admin.transactions.manageinventory.card_search",SessionData.getLanguage())%></TITLE>
</HEAD>
<BODY BGCOLOR="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="750" align="center">
	<tr>
		<td width="18" height="20"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
		<td background="../../images/top_blue.gif" width="3000" class="main"><b><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.cards_found",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="../../images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="1" bgcolor="#003082"><img src="../../images/trans.gif"
					width="1"></td>
				<td align="center" valign="top" bgcolor="#FFFFFF">
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>					<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
					<tr>						
						<td class="main"><br>
						<SCRIPT SRC="../../includes/sortROC.js" type="text/javascript"></SCRIPT>
				        <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
      	      			<thead>
      	      			<tr class="SectionTopBorder">
	      	      			<td class=rowhead2>#</td>
			            	<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_type",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.company_name",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.card_program_type",SessionData.getLanguage()).toUpperCase()%></td>              				
             				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.status",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.serial_number",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("jsp.admin.transactions.manageinventory.iso_receipt_date",SessionData.getLanguage()).toUpperCase()%></td>
              				<td class=rowhead2><%=Languages.getString("?jsp.admin.transactions.manageinventory.last_assigned_date",SessionData.getLanguage()).toUpperCase()%></td>


            			</tr>
		            	</thead>
		            	<%
                       		int intCounter = 1;
		               		Iterator it = vecSearchResults.iterator();
		               		int intEvenOdd = 1;
		               		while (it.hasNext())
		               		{
		            	   		Vector vecTemp = null;
                    	   		vecTemp = (Vector) it.next();
                           		out.println("<tr class=row" + intEvenOdd +">" +
                                		"<td>" + intCounter++ + "</td>" +
                                		"<td>" + vecTemp.get(0)+ "</td>" +
                                		"<td>" + vecTemp.get(1) + "</td>" +
                                		"<td>" + vecTemp.get(2) + "</td>" +
                                		"<td>" + vecTemp.get(3) + "</td>" +
                                		"<td>" + vecTemp.get(4) + "</td>" +
                                		"<td>" + vecTemp.get(5) + "</td>" +
                                		"<td>" + vecTemp.get(6) + "</td>" +
                                		"<td>" + vecTemp.get(7) + "</td>" +
                               			"</tr>");
                           		if (intEvenOdd == 1)
                           		{
                      	      		intEvenOdd = 2;
                    	   		}	
                           		else
                          		{
                             		intEvenOdd = 1;
                          		}
	                  		}
                  	  		vecSearchResults.clear();
                 		%>         			  						
                        </table>
						<br>
						<br>
						<SCRIPT type="text/javascript">
							<!--
  							var stT1 = new SortROC(document.getElementById("t1"),
	  						["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Date", "Date"],0,false,false);
	  						-->
						</SCRIPT>						
						</td>
<%
}
else
{
 	out.println("<td class=main><br><br><font color=ff0000>"  + Languages.getString("jsp.admin.transactions.manageinventory.not_found",SessionData.getLanguage()) + "</font><br><br></td>");
}
%>						
						<td width="18">&nbsp;</td>
					</tr>
				</table>
				</td>
				<td width="1" bgcolor="#003082"><img src="../../images/trans.gif"
					width="1"></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#003082" colspan="3"><img
					src="../../images/trans.gif" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</BODY>
</HTML>

