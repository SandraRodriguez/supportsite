<%@ page import="com.debisys.users.User,
				com.debisys.languages.Languages,
				com.debisys.utils.*,
				com.debisys.customers.Rep"%> 
<% 
int section=1;
int section_page=1;
String path = request.getContextPath();

%>

<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:setProperty name="User" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery.numeric.js"></script>
<script type="text/javascript" src="/support/js/NumberUtil.js"></script>

<%@ include file="/WEB-INF/jspf/includeJsGlobals.jspf" %>
<%@ include file="/WEB-INF/jspf/admin/customers/reps/repCommon.jspf" %>
<link rel="stylesheet" type="text/css" href="<%=path%>/css/toastr/toastr.css" />
<%
    setJsGlobals(out, DebisysConfigListener.getDeploymentType(application), DebisysConfigListener.getCustomConfigType(application));
setJsRepGlobals(out,  DebisysConfigListener.getLanguage(application));

String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);		    
boolean bPasswordChangeResult = false;
boolean bPasswordIsOld = false;
Hashtable userErrors = new Hashtable();

//String email=User.getEmailConfigured(SessionData.getProperty("username"));
boolean bIsMobileAuthenticationOn=false;
bIsMobileAuthenticationOn= SessionData.checkPermission(DebisysConstants.PERM_MOBILE_AUTHENTICATION);
boolean biSTransferReps=false;
biSTransferReps= SessionData.checkPermission(DebisysConstants.PERM_ALLOW_TRANSFER_TO_REPS);

boolean showExpiryMessage=false;
 int remainingDays=(Integer)request.getSession().getAttribute("expiryDiffDays");
String userNameData = ((SessionData != null && SessionData.getUser() != null && SessionData.getUser().getName() != null &&
        !SessionData.getUser().getName().isEmpty()) ? SessionData.getUser().getName() : null);
String userIdData = ((SessionData != null && SessionData.getUser() != null && SessionData.getUser().getUsername() != null &&
        !SessionData.getUser().getUsername().isEmpty()) ? SessionData.getUser().getUsername() : null);
 if(remainingDays<=7 && remainingDays!=0)
 {
     showExpiryMessage=true;
 }


if((request.getParameter("password") != null) && (request.getParameter("password").length() > 0)){
	if ( request.getParameter("password").equals(SessionData.getProperty("password")) ){
		bPasswordIsOld = true;
	}else{
		bPasswordChangeResult = User.changePassword(request.getParameter("password"), SessionData.getProperty("username"),SessionData,application);
	}
}

userErrors = User.getErrors();
boolean errorAnswers = false;
boolean saveAnswers = false;
if((request.getParameter("submit_answers") != null) && (request.getParameter("submit_answers").length() > 0)){
	saveAnswers = true;
	Vector<String> answers = new Vector<String>();
	for(int i=0; i< 4; i++){
		String ans = request.getParameter("answer"+i);
		if(ans==null){
			errorAnswers = true;
			break;
		}else{
			answers.add(ans);
		}
	}
	if(!errorAnswers){
		errorAnswers = !SessionData.getUser().saveAnswers(answers);
	}
}
%>
<table cellSpacing="0" cellPadding="0" width="750" border="0">
	<c:if test="${!SessionData.user.supportSiteNgUser}">
	<tr>
		<td align="left" width="1%" background="images/top_blue.gif">
			<IMG height="20" src="images/top_left_blue.gif" width="18">
		</td>
		<td class="formAreaTitle" background="images/top_blue.gif" width="3000">
			&nbsp;<B><%=Languages.getString("jsp.admin.index.title",SessionData.getLanguage()).toUpperCase()%></B>
		</td>
		<td align="right" width="1%" background="images/top_blue.gif">
			<IMG height="20" src="images/top_right_blue.gif" width="18">
		</td>
	</tr>
	</c:if>
	<tr>
		<td colSpan="3">
			<table width="100%" border="0" cellPadding="0" cellSpacing="0" bgColor="#fffcdf" class="backceldas">
				<tr class="backceldas">
					<td width="1" bgColor="#003082">
						<IMG src="images/trans.gif" width="1">
					</td>
					<td vAlign="top" align="left" bgColor="#ffffff">
						<table width="100%" border="0" align="left" cellPadding="2" cellSpacing="0" class="backceldas">
							<tr>
								<td width="18">&nbsp;</td>
								<td class="main" align="left">
									<BR><BR>
<%
if(SessionData.getProperty("name")!=null && !SessionData.getProperty("name").equals("")){
%>
									<%=Languages.getString("jsp.admin.support.hello",SessionData.getLanguage())%>
									<b class="main"><%=" " + SessionData.getProperty("name")%>!</b><br>
<%
} else {
%>
									<%=Languages.getString("jsp.admin.support.hello",SessionData.getLanguage())%>
									<b class="main"><%=" " +SessionData.getProperty("company_name")%>!</b><br>
<%
}
%>
<%
if( strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
%>
									<%=Languages.getString("jsp.admin.index.welcome_message",SessionData.getLanguage())%>
									<%=Languages.getString("jsp.admin.index.welcome_messageadd",new Object[]{ strBrandedCompanyName },SessionData.getLanguage())%><br><br>
<%
}else{
%>
									<%=Languages.getString("jsp.admin.index.welcome_message_international",SessionData.getLanguage())%>
									<%=Languages.getString("jsp.admin.index.welcome_message_internationaladd",new Object[]{ strBrandedCompanyName },SessionData.getLanguage())%>
									<br><br>
<%
}
%>
									<%=Languages.getString("jsp.admin.index.contact_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("contact_name")%><br>
									<%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%><br>
<%
if((deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && strAccessLevel.equals(DebisysConstants.REP)) 
        || (strAccessLevel.equals(DebisysConstants.REP) )){
	Rep rep = new Rep();
	rep.setRepId(SessionData.getProperty("ref_id"));
	rep.getRep(SessionData, application);
	out.print(Languages.getString("jsp.admin.customers.reps_info.credit_avail",SessionData.getLanguage()) + ":");
	if (rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)){
		out.print(Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage()));
	}else{
		out.print(NumberUtil.formatCurrency(rep.getAvailableCredit()));
%>
    <br><br>
    <input type="button" name="view_history" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_credit_assignment_history",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/reps_merchant_credit_history.jsp?repId=<%=SessionData.getProperty("ref_id")%>','repHistory','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
<%
	}
%>
    <br><br>
    <input type="button" name="view_history" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_history",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/reps_credit_history.jsp?repId=<%=rep.getRepId()%>','repHistory1','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
    
    <%if(SessionData.checkPermission(DebisysConstants.PERM_REP_MERCHANTS_BALANCE_SUM)){ %>        
        <script>
            $(document).ready(function () {
                $('#btnCheckMerchantsBalance').on('click', function (event) {
                    event.preventDefault();
                    jQuery.ajax({
                        url: 'customers/reps/repMerchantBalanceSum',
                        data: 'repId=<%=SessionData.getProperty("ref_id")%>',
                        method: 'GET',
                        dataType: "text",
                        async: true
                    }).done(function (response) {

                        $('#spMerchantsBalance').text(response);

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        var errorMsg = repMerchantBalanceSumError + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
                        toastr.error(errorMsg, repMerchantBalanceSumErrorTitle, {positionClass: "toast-top-center"});
                    });
                });            
            });


        </script>
    
        <br><br>
        <div id="divMerchantsBalanceSum">
            <button id="btnCheckMerchantsBalance" name="btnCheckMerchantsBalance">
               <%=Languages.getString("jsp.admin.customers.reps_info.merchants_credit_avail", SessionData.getLanguage())%>
            </button>                                       
            <span id="spMerchantsBalance"></span>
        </div>
    <%}%>
           
<%
}

if(deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
    && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) 
        && strAccessLevel.equals(DebisysConstants.REP) && biSTransferReps )
{
        Rep rep = new Rep();
	rep.setRepId(SessionData.getProperty("ref_id"));
	rep.getRep(SessionData, application);

%>
<br><br>
    <%if(rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)){%>
        <input type="button" name="transfer_reps" value="<%=Languages.getString("jsp.admin.customers.reps_info.reps.payment.reps",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/reptransfer.jsp?repId=<%=rep.getRepId()%>','repTransfers','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;

    <%
    }
}
if((deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && strAccessLevel.equals(DebisysConstants.AGENT)) 
|| (strAccessLevel.equals(DebisysConstants.AGENT) )){
	Rep rep = new Rep();
	rep.setRepId(SessionData.getProperty("ref_id"));
	rep.getAgent(SessionData, application);
	out.print(Languages.getString("jsp.admin.customers.reps_info.credit_avail",SessionData.getLanguage()) + ":");
	if (rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)){
		out.print(Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage()));
	}else{
		out.print(NumberUtil.formatCurrency(rep.getAvailableCredit()));
%>
    <br><br>
    <input type="button" name="view_history" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_credit_assignment_history",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/agents_subagent_credit_history.jsp?repId=<%=SessionData.getProperty("ref_id")%>','repHistory','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
<%
}
%>
    <br><br>
    <input type="button" name="view_history" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_history",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/agents_credit_history.jsp?repId=<%=rep.getRepId()%>','repHistory1','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
<%
}

if((deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && strAccessLevel.equals(DebisysConstants.SUBAGENT)) 
    || (strAccessLevel.equals(DebisysConstants.SUBAGENT) )){
	Rep rep = new Rep();
	rep.setRepId(SessionData.getProperty("ref_id"));
	rep.getSubAgent(SessionData, application);
	out.print(Languages.getString("jsp.admin.customers.reps_info.credit_avail",SessionData.getLanguage()) + ":");
	if (rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)){
		out.print(Languages.getString("jsp.admin.customers.reps_add.credit_type3",SessionData.getLanguage()));
	}else{
		out.print(NumberUtil.formatCurrency(rep.getAvailableCredit()));
%>
    <br><br>
    <input type="button" name="view_history" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_credit_assignment_history",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/subagents_rep_credit_history.jsp?repId=<%=SessionData.getProperty("ref_id")%>','repHistory','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
<%
	}
%>
    <br><br>
    <input type="button" name="view_history" value="<%=Languages.getString("jsp.admin.customers.reps_info.view_history",SessionData.getLanguage())%>" onClick="window.open('/support/admin/customers/subagents_credit_history.jsp?repId=<%=rep.getRepId()%>','repHistory1','width=725,height=600,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');">&nbsp;
<%
}
%>
									<br><br>
 <%
                                 if(showExpiryMessage){
 %>
                                   <div class="rowred2" style="padding-left:18px;">
                                    <%=String.format(Languages.getString("jsp.admin.support.expiryDiffDays",SessionData.getLanguage()),remainingDays)%>
                                   <b class="main"></b><br>
                                   </div>
 <%
 }
 %>   									
									<a id="lnk_UpdatePassword" href="javascript:void(0);" onclick="document.getElementById('txtPasswordValue').value='';TogglePasswordDisplay(true);"><%=Languages.getString("jsp.admin.updatepassword",SessionData.getLanguage())%></a><br><br>
									<div id="div_UpdatePassword" style="display:none;">
										<script>
											function TogglePasswordDisplay(bShow){
												if ( bShow ){
													document.getElementById('div_UpdatePassword').style.display = 'inline';
													document.getElementById('lnk_UpdatePassword').style.display = 'none';
													document.getElementById('lblEnterPassword').style.display = 'inline';
													document.getElementById('lblPasswordIsOld').style.display = 'none';
													document.getElementById('lblPasswordNoMatch').style.display = 'none';
													document.getElementById('lblMessage').style.display = 'none';
													document.getElementById('txtPasswordValue').focus();
												}else{
													document.getElementById('div_UpdatePassword').style.display = 'none';
													document.getElementById('lnk_UpdatePassword').style.display = 'inline';
													document.getElementById('txtPasswordValue').value = '';
													document.getElementById('txtPasswordConfirm').value = '';
													document.getElementById('lblMessage').style.display = 'inline';
												}
											}

											function ValidatePassword(){
                                                                                                        var userId = "<%=userIdData%>";
                                                                                                        var userName = "<%=userNameData%>";
                                                                                                         var isPasswordWithoutId = ("<%= pageContext.getServletContext().getAttribute(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM) %>" == 1) ? true : false; 
                                                                                                        
												if ( document.getElementById('txtPasswordValue').value != '' ){
													if ( document.getElementById('txtPasswordValue').value == document.getElementById('txtPasswordConfirm').value ){
<%
if(bIsMobileAuthenticationOn){
%>
														if(!checkMobilePassword(document.getElementById('txtPasswordValue').value)) {
															document.getElementById('lblEnterPassword').style.display = 'none';
															document.getElementById('lblPasswordIsOld').style.display = 'none';
															document.getElementById('lblPasswordNoMatch').style.display = 'none';
															document.getElementById('lblPasswordNotStrong').style.display = 'inline';
															return false;
														}else{
															return true;
														}
<%
}else{
%>
														if(!checkPassword(document.getElementById('txtPasswordValue').value)) {
															document.getElementById('lblEnterPassword').style.display = 'none';
															document.getElementById('lblPasswordIsOld').style.display = 'none';
															document.getElementById('lblPasswordNoMatch').style.display = 'none';
															document.getElementById('lblPasswordNotStrong').style.display = 'inline';
															return false;
														}else{
                                                                                                                    if (isPasswordWithoutId && passwordContainsId(userId.replace(/\s/g, '').toUpperCase(), 
                                                                                                                        userName.replace(/\s/g, '').toUpperCase(), $('#txtPasswordValue').val().toUpperCase())) {
                                                                                                                        hideMsgs();
                                                                                                                        $("#lblErrorPasswordContainsUserId").show();
                                                                                                                        return false;
                                                                                                                    } else {
                                                                                                                        return true;
                                                                                                                    }
														}
<%
}
%>
													}// end of match
													  document.getElementById('lblEnterPassword').style.display = 'none';
													  document.getElementById('lblPasswordIsOld').style.display = 'none';
													  document.getElementById('lblPasswordNoMatch').style.display = 'inline';
													  document.getElementById('lblPasswordNotStrong').style.display = 'none';
													  return false;
												}// end of null check
												document.getElementById('lblEnterPassword').style.display = 'inline';
												document.getElementById('lblPasswordIsOld').style.display = 'none';
												document.getElementById('lblPasswordNoMatch').style.display = 'none';
												document.getElementById('lblPasswordNotStrong').style.display = 'none';
												return false;
											}// end of function

											function checkPassword(str){
											   var regExp=/^(?=.{8,})(?=.*\d.*\d.*\d)(?=.*[!@#\$%\^&\*\(\)\+=\|;'"{}<>\.\?\-_\\/:,~`])(?=.*[A-Z]).*$/;
												// var re = /^(?=.{8,})(?=.*\\d){3,}(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/;
												return regExp.test(str);
											}

											function checkMobilePassword(str){
												var regExpM=/^([a-zA-Z0-9_-]){3,}$/;
												return regExpM.test(str);
											}
                                                                                        
                                                                                        function hideMsgs(){
                                                                                            $('#lblPasswordIsOld').hide();
                                                                                            $('#lblEnterPassword').hide();
                                                                                            $('#lblPasswordNoMatch').hide();
                                                                                            $('#lblPasswordNotStrong').hide();
                                                                                            $('#lblPasswordMobNotStrong').hide();
                                                                                            $('#lblErrorPasswordContainsUserId').hide();
                                                                                        }
                                                                                        
                                                                                        function passwordContainsId(userDni, userNameM, stringToCompare) {
                                                                                            var consecutive_not_allowed = parseFloat("<%= DebisysConstants.CHAR_LENGTH_NOT_ALLOWED%>");
                                                                                            var partName;

                                                                                            if (stringToCompare != undefined && stringToCompare != null && 
                                                                                                stringToCompare.length >= consecutive_not_allowed) {

                                                                                                for (var i = 0; i < stringToCompare.length; i++) {
                                                                                                    partName = stringToCompare.substring(i, (consecutive_not_allowed + i));
                                                                                                    if (partName.length == consecutive_not_allowed) {
                                                                                                        if (userDni.includes(partName) || userNameM.includes(partName)) {
                                                                                                            return true;
                                                                                                        } else if ((stringToCompare.length - (consecutive_not_allowed - 1)) == (i + 1) &&
                                                                                                            partName[consecutive_not_allowed - 1] == stringToCompare[stringToCompare.length - 1] ) {
                                                                                                            return false;
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            return false;
                                                                                        }
										</script>
										<form action="admin/index.jsp" method="post" onsubmit="return ValidatePassword();">
											<table class="main">
												<tr>
													<td><%=Languages.getString("jsp.admin.newpassword",SessionData.getLanguage())%></td>
													<td>&nbsp;</td>
													<td><%=Languages.getString("jsp.admin.confirmpassword",SessionData.getLanguage())%></td>
													<td>&nbsp;</td>
													<!-- <td><%=Languages.getString("jsp.admin.customers.user_logins.email",SessionData.getLanguage())%></td><td>&nbsp;</td> -->
												</tr>
												<tr>
													<td><input ID="txtPasswordValue" TYPE="PASSWORD" NAME="password" value="" MAXLENGTH="50" SIZE="20"></td><td></td>
													<td><input ID="txtPasswordConfirm" TYPE="PASSWORD" MAXLENGTH="50" SIZE="20"></td><td></td>
													<td><input TYPE="SUBMIT" VALUE="<%=Languages.getString("jsp.admin.changepassword",SessionData.getLanguage())%>"></td><td>&nbsp;</td>
													<td><input TYPE="BUTTON" VALUE="<%=Languages.getString("jsp.admin.cancelchangepassword",SessionData.getLanguage())%>" ONCLICK="TogglePasswordDisplay(false);"></td>
												</tr>
												<tr><td>&nbsp;</td></tr>
												<tr>
													<td COLSPAN="7">
														<font color="#ff0000">
															<b>
																<span ID="lblEnterPassword" STYLE="display:inline;"><%=Languages.getString("jsp.admin.enterpassword",SessionData.getLanguage())%></span>
																<span ID="lblPasswordIsOld" STYLE="display:none;"><%=Languages.getString("jsp.admin.passwordisold",SessionData.getLanguage())%></span>
																<span ID="lblPasswordNoMatch" STYLE="display:none;"><%=Languages.getString("jsp.admin.passwordnomatch",SessionData.getLanguage())%></span>
																<span ID="lblPasswordNotStrong" STYLE="display:none;"><%=Languages.getString("jsp.admin.passwordChangeError",SessionData.getLanguage())%></span>
																<span ID="lblPasswordMobNotStrong" STYLE="display:none;"><%=Languages.getString("jsp.admin.passwordMobChangeError",SessionData.getLanguage())%></span>
																<span ID="lblErrorPasswordContainsUserId" STYLE="display:none;"><%=Languages.getString("com.debisys.users.error_password_contains_login_name",SessionData.getLanguage())%></span>
															</b>
														</font>
													</td>
												</tr>
											</table>
										</form>
									</div>
<%
if ( (request.getParameter("password") != null) && (request.getParameter("password").length() > 0) ){
	if ( bPasswordChangeResult ){
%>
									<br>
									<span ID="lblMessage" STYLE="display:inline;">
										<font color="FF0000">
											<b><%=Languages.getString("jsp.admin.passwordchangesuccess",SessionData.getLanguage())%></b>
										</font>
									</span>
									<br>
<%
	}else if ( !bPasswordIsOld ){
            if (userErrors != null && userErrors.containsKey("login")) {
%>
									<br>
									<span ID="lblMessage" STYLE="display:inline;">
										<font color="FF0000"><b><%=Languages.getString("com.debisys.users.error_password_match",SessionData.getLanguage())%></b></font>
									</span>
									<br>
<%
            }else{ %>
                                                                         <br>
									<span ID="lblMessage" STYLE="display:inline;">
										<font color="FF0000"><b><%=Languages.getString("jsp.admin.passwordchangefailed",SessionData.getLanguage())%></b></font>
									</span>
									<br>
    <%  }
	}else{
%>
									<span ID="lblMessage" STYLE="display:none;" /><BR>
<%
	}
}else{
%>
									<span ID="lblMessage" STYLE="display:none;" /><BR>
<%
}
%>
<%
if ( bPasswordIsOld ){
%>
									<script>
										TogglePasswordDisplay(true);
										document.getElementById('lblEnterPassword').style.display = 'none';
										document.getElementById('lblPasswordIsOld').style.display = 'inline';
										document.getElementById('lblPasswordNoMatch').style.display = 'none';
									</script>
<%
}
%>
								</td>
								<td width="18">&nbsp;</td>
							</tr>
							<tr>
								<td width="18">&nbsp;</td>
								<td class="main" align="left">
									<hr>
									<a id="lnk_UpdateAnswers" href="javascript:void(0);"><%=Languages.getString("jsp.answers.update",SessionData.getLanguage())%></a><br><br>
									<div id="answers">
										<form id="answersForm" action="admin/index.jsp" method="post" onsubmit="return ValidateAnswers();">
											<table class="main">
<%
	Hashtable<String, String> answers = SessionData.getUser().getAnswers();
	for(int i=0; i< 4; i++){
		String ans = answers.get(""+i);
		if(ans==null){
			ans="";
		}
%>
												<tr>
													<td><%=Languages.getString("jsp.answers.security_question" + i,SessionData.getLanguage())%></td>
													<td><input type="text" maxLength="64" style="width:90%;" id="answer<%=i%>" name="answer<%=i%>" data-old="<%=ans%>" value="<%=ans%>"/></td>
												</tr>
<%
	}
%>

												<tr>
													<td>&nbsp;</td>
													<td>
														<input type="hidden" name="submit_answers" value="submit_answers">
														<input type="submit" value="<%=Languages.getString("jsp.answers.change",SessionData.getLanguage())%>">
														<input type="submit" value="<%=Languages.getString("jsp.answers.cancel",SessionData.getLanguage())%>" onclick="return answersCancel();">
													</td>
												</tr>
												<tr>
													<td colspan="100%">
														<font color="#ff0000">
															<b id="answermsgs">
<%
if(saveAnswers){
	if(errorAnswers){
		%><div class="error"><%=Languages.getString("jsp.answers.save_error",SessionData.getLanguage())%></div><%
	}else{
		%><div class="success"><%=Languages.getString("jsp.answers.save_ok",SessionData.getLanguage())%></div><%
	}
}
%>
															</b>
														</font>
													</td>
												</tr>
											</table>
										</form>
									</div>
								</td>
								<td width="18">&nbsp;</td>
							</tr>
						</table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td width="1" bgColor="#003082"><img src="images/trans.gif" width="1"></td>
					<td vAlign="top" align="left" bgColor="#ffffff">
						<div align=right class="backceldas">
							<jsp:include page="/includes/version.jsp"></jsp:include>							
						</div>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
				</tr>				
			</table>
		</td>
	</tr>
</table>
<script>
	function answersCancel(){
		$('#answers').hide();
		for(var i=0;i<4;i++){
			$('#answer'+i).val($('#answer'+i).data('old'));
		}
		return false;
	}
	$(document).ready(function(){
<%
	if(!saveAnswers){
%>
		$('#answers').hide();
<%
}
%>
		$('#lnk_UpdateAnswers').click(function(){$('#answers').show();});
		$('#answersForm').submit(function(){
			$('#answermsgs').html('');
			for(var i=0;i<4;i++){
				if($('#answer'+i).val() == ''){
					$('#answermsgs').html('<div class="error"><%=Languages.getString("jsp.answers.not_empty",SessionData.getLanguage())%></div>');
					return false;
				}
			}
			$('#answers').hide();
			return true;
		});
	});
</script>
<%
if (strAccessLevel.equals(DebisysConstants.ISO)) {
	%><%@ include file="/includes/bulletins.jsp" %><%
}%>
<%
if ( !strAccessLevel.equals(DebisysConstants.CARRIER) ) {
	if ((!domainName.equalsIgnoreCase("econnectprepaid.com") || strAccessLevel.equals(DebisysConstants.ISO)) && !SessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
		%><%@ include file="/includes/useful_docs.jsp" %><%
	}
}
%>
<%@ include file="/includes/footer.jsp" %>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/toastr/toastr.js"></script>
