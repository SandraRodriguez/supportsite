<%@ page import="com.debisys.languages.Languages"%> 
<%
int section=1;
int section_page=3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Document" class="com.debisys.document.Document" scope="request"/>
<jsp:setProperty name="Document" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
String action = ""+request.getParameter("action");
String value = ""+request.getParameter("value");
boolean op= false;
//boolean delete = false;
/*if(action.equals("approved")){
	op=true;
}

if(action.equals("delete")){
	delete = true;
}*/

String reason = request.getParameter("reason");
String result;
String id =  request.getParameter("documentId");

String mailHost = DebisysConfigListener.getMailHost(application);
String accessLevel = SessionData.getProperty("access_level"); 
String companyName = SessionData.getProperty("company_name");
String userName = SessionData.getProperty("username");

String email = Document.returnMailDocumentID(SessionData, application,  Long.parseLong(id));
String name = Document.returnNameDocumentID(SessionData, application,  Long.parseLong(id));
Document.setEmail(email);
Document.setName(name);


if(id != null && !id.equals("")){

	if(action.equals("approved")){
		reason=null;
		
		Document.updateDocumentApprovalStatus(SessionData, application,  Long.parseLong(id), action, reason);
        
	    try{
	    if(!email.equals("")){
	    Document.setMessage(Languages.getString("jsp.admin.document.documentrepository.docapproved",SessionData.getLanguage()));
	    Document.sendMailNotification(mailHost,accessLevel, companyName,userName,"approved",SessionData);
	    }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		%>APPROVED<%
	}else if(action.equals("denied")){
		Document.setMessage(Languages.getString("jsp.admin.document.documentrepository.docdenied",SessionData.getLanguage()));
		reason = Languages.getString("jsp.admin.document.documentrepository.reason",SessionData.getLanguage()) + ". " +reason;
		Document.updateDocumentApprovalStatus(SessionData, application, Long.parseLong(id), action, reason);
	    try{
	    if(!email.equals("")){
	    Document.setReason(reason);
	    Document.sendMailNotification(mailHost,accessLevel, companyName,userName,"denied",SessionData);
	    }
		}
		catch(Exception e){
			e.printStackTrace();
		}
			%>DISABLED<%
	}else if(action.equals("delete")){
		Document.setMessage(Languages.getString("jsp.admin.document.documentrepository.docdelete",SessionData.getLanguage()));
		//Document.updateDocumentVisible(SessionData, application, Long.parseLong(id));
		Document.updateDocumentApprovalStatus(SessionData, application, Long.parseLong(id), action, reason);
		    %>DELETE<%
	}
	else if(action.equals("cat")){
			String category = request.getParameter("cat");
			String indicator =  request.getParameter("ind");
			if(category != null && !category.equals("")){
				if(SessionData.getProperty("categorySelected")==null || SessionData.getProperty("categorySelected").equals("")){
					SessionData.setProperty("categorySelected",category);
					SessionData.setProperty("indexSelected",indicator);
				}
				else if(!SessionData.getProperty("categorySelected").equals(category)){
					if(indicator.equals("0")){
						SessionData.setProperty("categorySelected","");
						SessionData.setProperty("indexSelected","0");
					}else{
						SessionData.setProperty("categorySelected",category);
						SessionData.setProperty("indexSelected",indicator);
					}
					
				}
			}
		    %><%
	}
}else if(action.equals("redirect")){
	 	response.sendRedirect("documentApproval.jsp?search=y&criteria=&page=2&mess=");
}
else{
	%>ERROR<%
}
%>