<%@ page language="java" import="com.debisys.languages.Languages,
                 org.apache.commons.fileupload.servlet.ServletFileUpload,
                 com.debisys.presentation.DocumentComponent" pageEncoding="ISO-8859-1"%>
<%
int section=1;
int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Document" class="com.debisys.document.Document" scope="request"/>
<jsp:setProperty name="Document" property="*"/>
<%@include file="/includes/security.jsp" %>
<%
Document.setSessionData(SessionData);
Document.setStrAccessLevel(SessionData.getProperty("access_level"));
Document.setStrDistChainType(SessionData.getProperty("dist_chain_type"));
Document.setStrRefId(SessionData.getProperty("ref_id"));
Document.setIso_default(SessionData.getProperty("iso_default"));
DocumentComponent ic = new DocumentComponent(Document);


String action = "" + request.getParameter("action");

String message =request.getParameter("message");

if(SessionData.getUser() !=null){
	
	//loginLabel=SessionData.getUser().getUsername();
	SessionData.getUser().getUsername();
	SessionData.getUser().getUserLogonID(SessionData);
	SessionData.getProperty("iso_id");

}

String name = "";


if (ServletFileUpload.isMultipartContent(request)) {
	ic.loadFromRequest(request, response, application,SessionData);

	if(Document.getDocumentId() == 0){
		String bid = ic.getValue("documentId");
		if(bid != null && !bid.equals("")){
			Document.setDocumentId(Long.parseLong(bid));
		}
	}
	action = "" + ic.getValue("action");

}else{
	action = "" + request.getParameter("action");
}


String messageType = request.getParameter("message");

if(action.equals("save")){
	//ic.setValue("folderPath", application.getRealPath(application.getAttribute("documentRepositoryPath") + SessionData.getProperty("ref_id")));
	ic.setValue("folderPath", application.getAttribute("documentRepositoryPath") + "/" + SessionData.getProperty("ref_id"));
	//ic.setValue("contextPath", application.getAttribute("documentRepositoryPath") + SessionData.getProperty("ref_id"));
	ic.setValue("contextPath", SessionData.getProperty("ref_id"));
	ic.setValue("user", SessionData.getUser().getUsername());
	if(ic.save(SessionData)>0){
	
		message = Languages.getString("jsp.admin.document.documentrepository.rtaupload",SessionData.getLanguage());
		
		//enviar correo
		String mailHost = DebisysConfigListener.getMailHost(application);
	    String accessLevel = SessionData.getProperty("access_level"); 
	    String companyName = SessionData.getProperty("company_name");
	    String userName = SessionData.getProperty("username");
	    try{
	    Document.sendMailNotification(mailHost,accessLevel, companyName,userName,"upload",SessionData);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		message =  Languages.getString("jsp.admin.document.documentrepository.successsave",SessionData.getLanguage());;
		messageType = "1";
		response.sendRedirect("documentApproval.jsp?search=y&id="+ Document.getDocumentId()+"&mess="+message);
	}else{
		//message = "Error saving news";
		messageType = "2";
	}
	//response.sendRedirect("/support/admin/documents/document.jsp?action=save&imageId="+ Document.getDocumentId() + "&messageType=" + messageType + "&message=" + HTMLEncoder.encode(message));
}
%>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<style>
	.details-table .checkList {
		border: none;
	}
	.details-table .checkList li{
		float: left;
	}
</style>

<form name="mainform" method="post" action="admin/documents/document.jsp" enctype="multipart/form-data">
<input type="hidden" name="action" value="save"/>
<input type="hidden" name="isoId" value="<%=SessionData.getProperty("iso_id")%>">
<input type="hidden" name="repId" value="<%=SessionData.getProperty("ref_id")%>">
<input type="hidden" name="user" value="<%=SessionData.getUser().getUserLogonID(SessionData)%>">
<input type="hidden" name="opt" value="save">

<%
if(message != null){
%>
		<p style="color:<%=(messageType.equals("1")? "green":"red")%>"><%=message %></p>
<%
}
%>
	<table border="0" cellpadding="0" cellspacing="0" width="750">
		<tr>
			<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.tools.documentSettings",SessionData.getLanguage()).toUpperCase()%></td>
			<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
		</tr>
		<tr>
			<td colspan="3">
				<p style="color:white">
					
				</p>
				<%
String drep =  SessionData.getProperty("drep");
if (SessionData.checkPermission(DebisysConstants.PERM_TO_DOCUMENT_MANAGEMENT) || (drep !=null && drep.equals("ok"))){
%>
				<table class="main details-table">
					<td colspan="3" align="left"><%=Languages.getString("jsp.admin.document.label.formatInformation",SessionData.getLanguage())%></td>
					
					<% ic.showFields(out,SessionData); %>
					<tr>
						<td colspan="3" align="center"><%=Languages.getString("jsp.admin.document.label.information",SessionData.getLanguage())%></td>
					</tr>
					<tr>
						<td colspan="3" align="center"><input type="submit" name="save" value="<%=Languages.getString("jsp.admin.document.documentrepository.savebutton",SessionData.getLanguage())%>"></td>
					</tr>
				</table>
<%
}else { %>
				<p class="main">
					<%=Languages.getString("jsp.admin.document.documentrepository.toolNoPermission",SessionData.getLanguage())%>
				</p>
<%} %>					
			</td>
		</tr>
	</table>
</form>