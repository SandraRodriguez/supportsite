<%@ page import="java.io.*" %>
<jsp:useBean id="Document" class="com.debisys.document.Document" scope="request"/>
<jsp:setProperty name="Document" property="*"/>
<% String idfile = request.getParameter("idfile");
String filename="";
try {
	if(idfile!=null && !idfile.equals(""))
	{
	  	 String file = application.getAttribute("documentRepositoryPath") + Document.searchPathDocument(Long.valueOf(idfile));
	  	 System.out.println("--->> Doc Repository: " + file);
	     File f = new File(file);
	    	     
	     if(file.endsWith(".docx")){
	      response.setHeader("Content-type","application/vnd.openxmlformats-officedocument.wordprocessingml.document"); 
	   	 }
	   	 else if(file.endsWith(".doc")){
	     	 response.setHeader("Content-type","application/vnd.ms-word");
	     }
	     else if(file.endsWith(".pdf")){
	     	 response.setHeader("Content-type","application/octet-stream");
	     }
	     else if(file.endsWith(".xls")){
	     	 response.setHeader("Content-type","application/vnd.ms-excel");
	     }
	     else if(file.endsWith(".xlsx")){
	     	 response.setHeader("Content-type","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); 
	     }
	     else if(file.endsWith(".pptx")){
	     	 response.setHeader("Content-type","application/vnd.openxmlformats-officedocument.presentationml.presentation"); 
	     }
	     else if(file.endsWith(".ppt")){
	     	 response.setHeader("Content-type","application/vnd.ms-powerpoint"); 
	     }
	     
	     String disHeader = "attachment; filename="+f.getName();
	     filename=file;
   	     response.setHeader("content-disposition", disHeader);
   	     
   	     response.setCharacterEncoding("UTF-8");
    	     
		 File fileToDownload = new File(filename);
		   FileInputStream fileInputStream = new
		      FileInputStream(fileToDownload);
		       OutputStream out1 = response.getOutputStream();
		        
		 
		        
		 byte[] buf = new byte[1024];

         /*int count = 0;
         while ((count = fileInputStream.read(buf) ) >= 0) {
            out1.write(buf, 0, count);
            
         }*/
         int bytesRead = fileInputStream.read(buf);
         while (bytesRead >= 0) {
		   if (bytesRead > 0)
			 out1.write(buf, 0, bytesRead);
			 bytesRead = fileInputStream.read(buf);
		 }
		
         out1.flush();
         out1.close();       
         fileInputStream.close();
         buf=null;
         fileInputStream=null;
         out1=null;


	  }
	 }
catch(Exception e) // file IO errors
  {
  e.printStackTrace();
}
out.clear();
out = pageContext.pushBody(); 

%>