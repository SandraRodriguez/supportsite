<%@ page import="java.util.Vector,java.io.*,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.DbUtil" %>
<%
int section=2;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CustomerSearch" class="com.debisys.customers.CustomerSearch" scope="request"/>
<jsp:useBean id="Merchant" class="com.debisys.customers.Merchant" scope="request"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:useBean id="Document" class="com.debisys.document.Document" scope="request"/>
<jsp:setProperty name="Document" property="*"/>

<%@ include file="/includes/security.jsp" %>

<%

String message ="";
if(!Document.getMessage().equals("")){
	message= Document.getMessage();
}
else{
 message =request.getParameter("mess");
}

String criteria = request.getParameter("criteria");
String category = request.getParameter("categoryId");

String businessName = request.getParameter("business_name");
String rep_id = request.getParameter("rep_id");

String id = request.getParameter("id");

if(criteria!=null && !criteria.equals("")){
	Document.setCriteria(criteria);
}
if(category!=null && !category.equals("")){
	Document.setCategory(category);
}
if(businessName!=null && !businessName.equals("")){
	Document.setBusinessName(businessName);
}
if(rep_id!=null && !rep_id.equals("")){
	Document.setRep_id_find(rep_id);
}
if(id!=null && !id.equals(""))
{
	Document.setDocumentId(Long.parseLong(id));
}


Vector vecSearchResults = new Vector();
Vector vecCounts = new Vector();
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 20;
int intPageCount = 1;
String strParentRepName = "";
if (request.getParameter("search") != null){
	if (request.getParameter("page") != null){
		try{
			intPage=Integer.parseInt(request.getParameter("page"));
		}catch(NumberFormatException ex){
			intPage = 1;
		}
	}
	if (intPage < 1){
		intPage=1;
	}
	vecSearchResults = Document.searchDocument(intPage, intPageSize, SessionData, application);
	vecCounts = (Vector) vecSearchResults.get(0);
	intRecordCount = Integer.parseInt(vecCounts.get(0).toString());
	vecSearchResults.removeElementAt(0);
	if (intRecordCount>0){
		intPageCount = (intRecordCount / intPageSize);
		if ((intPageCount * intPageSize) < intRecordCount){
			intPageCount++;
		}
	}

}


%>
<%@ include file="/includes/header.jsp" %>
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<style>
	#popBG{z-index:2; position:absolute; left:0px; top: 0px; width: 100%; height: 1000px;background-image: url(images/popupbgpixolive.png); filter: alpha (opacity=50);}
	#pop{z-index:3; position:relative; border: 1px solid #333333; text-align:center;}
	#popContent{overflow: scroll;}
	#popClose{float:right; margin:5px; cursor:pointer; position:relative;}
	.grayLabel{color:gray; font-weight:bold;}
	.silvered{background-color:silver;}
</style>
<script type="text/javascript">

function opcion(obj)
{ 

 ventana=confirm('<%=Languages.getString("jsp.admin.document.documentrepository.warningdelete",SessionData.getLanguage())%>'); 
 var page = ''+<%=request.getParameter("page")%>;
 if (ventana) {
 	
 	var str1 = obj.value;//document.getElementById("deleteDocument").value;
 	var current=str1.substring(0,str1.indexOf('-'));
    var actionn =str1.substring(str1.indexOf('-')+1,str1.length);
    var url = 'admin/documents/documentUpdateStatus.jsp?documentId='+current+'&action='+actionn+'&value='+current;
   $.post(url, function(responseText, textStatus, XMLHttpRequest){
								if(responseText.match("ERROR") == "ERROR"){
									alert("Error!");
								}else{
									if(responseText.match("DELETE") == "DELETE"){
										alert('<%=Languages.getString("jsp.admin.document.documentrepository.docdelete",SessionData.getLanguage())%>');
										location.reload(true);
									}
								}
								
							});
						
	location.reload(true);							
}
 else {
 	var str1 = obj.value;//document.getElementById("deleteDocument").value;
 	var current=str1.substring(0,str1.indexOf('-'));
    var actionn =str1.substring(str1.indexOf('-')+1,str1.length);
   
 }                                    
} 

function opcionApproved(obj)
{ 
 var page = ''+<%=intPage%>;
 
 	ventana=confirm('<%=Languages.getString("jsp.admin.document.documentrepository.warningapproved",SessionData.getLanguage())%>'); 
 if (ventana) {
 	var str1 = obj.value;
 	var current=str1.substring(0,str1.indexOf('-'));
    var actionn =str1.substring(str1.indexOf('-')+1,str1.length);
    var url = 'admin/documents/documentUpdateStatus.jsp?documentId='+current+'&action='+actionn+'&value='+current;
	$.post(url, function(responseText, textStatus, XMLHttpRequest){
								if(responseText.match("ERROR") == "ERROR"){
									alert("Error!");
								}else{
									if(responseText.match("DELETE") == "DELETE"){
										alert('<%=Languages.getString("jsp.admin.document.documentrepository.docdelete",SessionData.getLanguage())%>');
										location.reload(true);
									}
									else {if(responseText.match("APPROVED") == "APPROVED"){
										//alert('<%=Languages.getString("jsp.admin.document.documentrepository.docapproved",SessionData.getLanguage())%>');
										//location.reload(true);
										
									}
									}
								}
								
							});
							
	alert('<%=Languages.getString("jsp.admin.document.documentrepository.docapproved",SessionData.getLanguage())%>');
	location.reload(true);
							
}
 else {
 	var str1 = document.getElementById("approvedDocument").value;
 	var current=str1.substring(0,str1.indexOf('-'));
    var actionn =str1.substring(str1.indexOf('-')+1,str1.length);
    
 }                                    
} 

	var current_enabled;
	var page = ''+<%=request.getParameter("page")%>;
	$(document).ready(function(){
		$.extend({URLEncode:function(c){var o='';var x=0;c=c.toString();var r=/(^[a-zA-Z0-9_.]*)/;while(x<c.length){var m=r.exec(c.substr(x));if(m!=null && m.length>1 && m[1]!=''){o+=m[1];x+=m[1].length;}else{if(c[x]==' ')o+='+';else{var d=c.charCodeAt(x);var h=d.toString(16);o+='%'+(h.length<2?'0':'')+h.toUpperCase();}x++;}}return o;},URLDecode:function(s){var o=s;var binVal,t;var r=/(%[^%]{2})/;while((m=r.exec(o))!=null && m.length>1 && m[1]!=''){b=parseInt(m[1].substr(1),16);t=String.fromCharCode(b);o=o.replace(m[1],t);}return o;}});
		$('.cancelled, .disabled').click(function(){
			showDisableDialog(this);
		});
		$("#popClose, #cancelDisable, #okDisable").click(
			function(){
				if(current_enabled){
				   $("#popBG").fadeOut('fast');
				   
				   var str =$("#"+current_enabled).val();
			
				  current_enabled=str.substring(0,str.indexOf('-'));
				  var action =str.substring(str.indexOf('-')+1,str.length);

					if(this.id != "okDisable"){
						$("#reason").val("");
					}else{
						var reason = $.trim($("#reason").val())
						///var action="disable";
						
						if(reason){
							$("#reason").val(reason);
							var url = 'admin/documents/documentUpdateStatus.jsp?documentId='+current_enabled+'&action='+action+'&value='+current_enabled+"&reason=" + reason + "&Random=" + Math.random();
							$.post(url, function(responseText, textStatus, XMLHttpRequest){
								if(responseText.match("ERROR") == "ERROR"){
									alert("Error!");
								}else{
									if(responseText.match("APPROVED") == "APPROVED"){
										alert('<%=Languages.getString("jsp.admin.document.documentrepository.docapproved",SessionData.getLanguage())%>');
										document.all("change").innerText = "<%=Languages.getString("jsp.admin.document.documentrepository.docapproved",SessionData.getLanguage())%>";
										location.reload(true);
										<%//message = Languages.getString("jsp.admin.document.documentrepository.docapproved",SessionData.getLanguage());%>
									}else if(responseText.match("DISABLED") == "DISABLED"){
										alert('<%=Languages.getString("jsp.admin.document.documentrepository.docdenied",SessionData.getLanguage())%>');
										<%//message = Languages.getString("jsp.admin.document.documentrepository.docdenied",SessionData.getLanguage());%>
										document.all("change").innerText = "<%=Languages.getString("jsp.admin.document.documentrepository.docdenied",SessionData.getLanguage())%>";
										location.reload(true);
										
									}
								}
								
							});
						
							//location.reload(true);
						
						}else{
							alert("Please specify a reason");
							$("#popBG").fadeIn('fast');
							return;
						}
					}
					$("#reason").val("");
					//$("#" + current_enabled).focus();
					current_enabled = null;
				}
			}
		);
	});
	//});	
	function showDisableDialog(obj){
		//if(obj.checked == false){// && $(obj).attr("startvalue") == "true"){
			var w = $(document).width();
			var h = $(document).height(); 
			var p_w = 400;//$("#pop").width();
			var p_h = 250;//$("#pop").height();
			p_w = (p_w < w*0.8)?p_w:w*0.8;
			p_h = (p_h < h*0.6)?p_h:h*0.6;
			var cw = $(obj).position().left; //(w/2) - (p_w/2);
			var ch = $(obj).position().top; //(h/2) - (p_h/2);
			current_enabled = obj.id;
			$("#popContent").css("width", (p_w-10) + "px").css("height", (p_h-10) + "px");
			$("#pop").css("position", "absolute").css("width", p_w + "px").css("height", p_h + "px").css("left", (cw-p_w) + "px").css("top", (ch-p_h) + "px");
			$("#popBG").css("width", w + "px").css("height", h + "px").fadeIn('fast');
			$("#" + current_enabled).focus();
	}
</script>

    <link href="Debisys_Home_files/default.css" type="text/css" rel="stylesheet">
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.tools.documentSettings",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
				<tr>
					<td>
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="formArea">
									<form name="entityform" method="get" action="admin/documents/documentApproval.jsp" >
										<table width="300">
										
										 <%
										 String strRefId = SessionData.getProperty("ref_id");
										 String iso_default = application.getAttribute("iso_default").toString();
										 String drep =  SessionData.getProperty("drep");
										  if(strRefId.equals(iso_default)){
										 //if (drep !=null && drep.equals("ok")){
										 %>
											<tr class="main">
													<td nowrap="nowrap" valign="top"><%=Languages.getString("jsp.admin.document.documentrepository.businessname",SessionData.getLanguage())%>:</td>
													<td nowrap="nowrap" valign="top"><%=Languages.getString("jsp.admin.document.documentrepository.repId",SessionData.getLanguage())%>:
													</td>
											</tr>		
										
											<tr class="main">
												<td valign="top" nowrap="nowrap">
													<input name="business_name" type="text" size="30" maxlength="64" >
												</td>
												<td valign="top" nowrap="nowrap">
													<input name="rep_id" type="text" size="30" maxlength="64" >
												</td>
	
											</tr>		
										<%} %>											
											<tr class="main">
												<td nowrap="nowrap" valign="top"><%=Languages.getString("jsp.admin.document.documentrepository.documentname",SessionData.getLanguage())%>:</td>
												<td>

<%
//if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
	out.println(Languages.getString("jsp.admin.document.documentrepository.category",SessionData.getLanguage()));					
//}
%>
												</td>
											</tr>
											<tr class="main">
												<td valign="top" nowrap="nowrap">
													<input name="criteria" type="text" size="30" maxlength="64" >
<%
//if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
	out.println("<td>");
	out.println("<select id=\"categoryId\" name=\"categoryId\">");
	out.println("<option value=\"-1\" selected>" + Languages.getString("jsp.admin.reports.all",SessionData.getLanguage()) + "</option>");
	Vector categories = Document.getCategories();
	Iterator itx = categories.iterator();
	
	while (itx.hasNext()){
		Vector vecTemp = null;
	    vecTemp = (Vector)itx.next();
	    String categoryId = vecTemp.get(0).toString();
	    String description = vecTemp.get(1).toString();
	    String descriptionSpanish= vecTemp.get(2).toString();
	    String categoryToSelect =""; 
	     if(SessionData.getLanguage().equals("english")){
	     categoryToSelect= description;
	     }
	     else{
	     categoryToSelect=descriptionSpanish;
	     }
	    out.println("<option value=\"" + categoryId + "\">" + categoryToSelect + "</option>");
	}
	out.println("</select>");
	out.println("</td>");
//}
%>
												</td>
												<td valign="top">
													<input type="hidden" name="search" value="y">
													<input type="hidden" name="repId" value="<%=Document.getRepId()%>">
													<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
												</td>
											</tr>
											<tr>
												<td colspan="2"><font size="1"></font></td>
											</tr>
										</table>
									</form>
									
									<table>
										<tr>
											<td colspan="3" align="left">
												<form method="get" action="admin/documents/document.jsp">
													<input type="submit" name="addDocument" value="<%=Languages.getString("jsp.admin.document.documentrepository.addNewDocument",SessionData.getLanguage())%>">
												</form>
											</td>
										</tr>
									</table>
									

									<table width="100%" cellspacing="1" cellpadding="1">
<%
if (vecSearchResults != null && vecSearchResults.size() > 0){
%>
										<tr>
											<td class="main" colspan="8">
												<%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
<%
	if (Document.getCriteria() != null && !Document.getCriteria().equals("")) {
		out.println(Languages.getString("jsp.admin.for",SessionData.getLanguage()) + " \"" + HTMLEncoder.encode(Document.getCriteria()) + "\"");
	}
%> <br>
											</td>
										</tr>
										<tr>
											<td colspan="10" class="main" align="center">
												<br>
												<p style="color:green")%><label id="change"></label></p>
												<%if(message !=null && !message.equals("")){%>
													<p style="color:green")%><%= message%></p>
												<% } %>
											</td>
										</tr>		
										<tr>
											<td colspan="10" class="main" align="right">
												<br>
												
												
<%
//add page numbers here
	//String baseURL = "admin/documents/documentApproval.jsp?search=" + request.getParameter("search") + "&criteria=" Document.getCriteria() + "&repId=" + Document.getRepId() + "&col=" + Document.getCol() + "&sort=" + Document.getSort()";
	String baseURL = "admin/documents/documentApproval.jsp?search=" + request.getParameter("search") + "&criteria="+criteria ;
	out.println(DbUtil.displayNavigation(baseURL, intPageSize, intRecordCount, intPage,SessionData));
%>
											</td>
										</tr>
										<tr>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.repId",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.businessname",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.category",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.documentname",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.visible",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.status",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.description",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.createdBy",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.lastUpdate",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
<%
if (SessionData.checkPermission(DebisysConstants.PERM_TO_DOCUMENT_MANAGEMENT)){
%>
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.actions",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
<% 
if (SessionData.getUser().isIntranetUser()
		&& SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
				DebisysConstants.INTRANET_PERMISSION_MANAGE_DOCUMENTS)){
%>											
											<td class="rowhead2"><%=Languages.getString("jsp.admin.document.documentrepository.additionalactions",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
<%
}
} %>											
											
										</tr>

<%
	Iterator it = vecSearchResults.iterator();
	String disabledValues = "";
	String cancelledValues = "";
	String checkboxReadOnly = "";
	if (!strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.PERM_MERCHANT_STATUS)){
		checkboxReadOnly = "readonly";
	}
	int intEvenOdd = 1;
	while (it.hasNext()){
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		
		//URL fileUrl = new File(vecTemp.get(8).toString()).toURI().toURL();
		//URL springUrl = new FileSystemResource(vecTemp.get(8).toString()).getURL();
		//System.out.println(fileUrl);
		//System.out.println(springUrl);
				   
		
%>
										<tr class="row<%= intEvenOdd%>">
											<td nowrap="nowrap">&nbsp;
												<%=vecTemp.get(1)%>&nbsp;
											</td>
											<td nowrap="nowrap">&nbsp;<%=vecTemp.get(11)%>&nbsp;</td>
											<td nowrap="nowrap">&nbsp;<%=vecTemp.get(14)%>&nbsp;</td>
											<td nowrap="nowrap"><a href="admin/documents/documentDownload.jsp?idfile=<%=vecTemp.get(0)%>" target="_blank" onClick="window.open(this.href, this.target, 'width=0,height=0'); return false;" onmouseover="status='';return true"><%=vecTemp.get(2)%></a></td>
											<td nowrap="nowrap">&nbsp;<%if(vecTemp.get(7).equals("false")){%><%=Languages.getString("jsp.admin.document.documentrepository.fieldnovisible",SessionData.getLanguage())%><%}
																			   else if(vecTemp.get(7).equals("true")){%><%=Languages.getString("jsp.admin.document.documentrepository.fieldvisible",SessionData.getLanguage())%><%} 
																			   %>&nbsp;
																			   </td>
											<td nowrap="nowrap">&nbsp;<%if(vecTemp.get(13).equals("1")){%><%=Languages.getString("jsp.admin.document.documentrepository.approved",SessionData.getLanguage())%><%}
																			   else if(vecTemp.get(13).equals("2")){%><%=Languages.getString("jsp.admin.document.documentrepository.pending",SessionData.getLanguage())%><%} 
																			   else if(vecTemp.get(13).equals("0")){%><%=Languages.getString("jsp.admin.document.documentrepository.denied",SessionData.getLanguage())%><%} %>
																			   &nbsp;
																			   </td>
											<td nowrap="nowrap" width="500">&nbsp;<%=vecTemp.get(3)%>&nbsp;</td>
											<td nowrap="nowrap">&nbsp;<%=vecTemp.get(4)%>&nbsp;</td>
											<td nowrap="nowrap">&nbsp;<%=vecTemp.get(10)%>&nbsp;</td>
											
<%
		//if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
			//out.println("<td align=center>" + vecTemp.get(10) + "</td>");
		//}
%>
											<td nowrap="nowrap">
												<a href="admin/documents/updateDocument.jsp?id=<%=vecTemp.get(0)%>"> 
													<img src="images/yellowedit.png" border="0" alt="<%=Languages.getString("jsp.admin.document.documentrepository.editDocument",SessionData.getLanguage())%>" >
												</a> 

												<input name="deleteDocument" src="images/reddelete.png" type="image" 
												id="<%=vecTemp.get(0)%>-<%=vecTemp.get(13)+"delete"%>" value="<%=vecTemp.get(0)%>-delete" onclick="opcion(this)" alt="<%=Languages.getString("jsp.admin.document.documentrepository.deleteDocument",SessionData.getLanguage())%>"/>
											</td>
<%
	if (SessionData.getUser().isIntranetUser()
		&& SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
				DebisysConstants.INTRANET_PERMISSION_MANAGE_DOCUMENTS)){
		String disabled="";
		if(vecTemp.get(13).equals("0")){
		disabled = "disabled=disabled";
		}else{
		disabled = "";
		}
%>					
											
											<td nowrap="nowrap" align="center">
										      <input src="images/graydelete.png" type="image" id="<%=vecTemp.get(0)%>-<%=vecTemp.get(13)+"denied"%>" class="disabled" 
												value="<%=vecTemp.get(0)%>-denied" <%=disabled %> alt="<%=Languages.getString("jsp.admin.document.documentrepository.deniedDocument",SessionData.getLanguage())%>"/>
												
<%
		
		if(vecTemp.get(13).equals("1")){
		disabled = "disabled=disabled";
		}else{
		disabled = "";
		}
%>
										<input name="<%=vecTemp.get(0)%>" src="images/approved-icon-big.png" type="image" id="<%=vecTemp.get(13)%>"  
										value="<%=vecTemp.get(0)%>-approved" <%=disabled %> onclick="opcionApproved(this)" alt="<%=Languages.getString("jsp.admin.document.documentrepository.approvalDocument",SessionData.getLanguage())%>" />
<%
	}
%>
											</td>
										</tr>
<%
                    if (intEvenOdd == 1)
                    { 
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }
	}
	//take the trailing comma out
	vecSearchResults.clear();
}else if (intRecordCount==0 && request.getParameter("search") != null){
%>
										<tr>
											<td class="main">
												<br><br><font color="#ff0000"><%=Languages.getString("jsp.admin.document.documentrepository.not_found",SessionData.getLanguage())%></font>
											</td>
										</tr>
<%
}
%>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div id="popBG" style="display:none;">
	<div id="pop">
		<table border="5" cellpadding="0" cellspacing="0" bgcolor="white" width="100%" height="100%">
			<tr>
				<th>
					<div id="popClose"><img src="images/closebutton.png"></div>
				</th>
			</tr>
			<tr>
				<th align="left">
					<div style="padding:5px"><%= Languages.getString("com.debisys.terminals.jsp.terminaldisabled.reason",SessionData.getLanguage()) %>:</div>
				</th>
			</tr>
			<tr>
				<td align="center">
					<textarea id="reason" name="reason" rows="4" cols="50"></textarea><br>
				</td>
			</tr>
			<tr>
				<td align="right" nowrap="nowrap">
					<input type="button" id="okDisable" value="Ok">
					<input type="button" id="cancelDisable" value="Cancel">
				</td>
			</tr>
		</table>
	</div>
</div>
<%@ include file="/includes/footer.jsp" %>