<%@ page language="java" import="java.util.*,
                 com.debisys.utils.HTMLEncoder,com.debisys.languages.Languages,
                 org.apache.commons.fileupload.servlet.ServletFileUpload,
                 com.debisys.presentation.DocumentComponent" pageEncoding="ISO-8859-1"%>
                 
<%@ page import="com.debisys.terminalTracking.CPhysicalTerminal"%>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page import="org.apache.commons.fileupload.FileItem"%>
<%@ page import="java.io.File"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.util.*"%>
<%@ page import="com.debisys.terminalTracking.CResultMessage"%>
<%@ page import="com.debisys.terminalTracking.CFileLoad"%>  
<%@ page import="com.debisys.document.Document"%> 
          
<%
int section=1;
int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Document" class="com.debisys.document.Document" scope="request"/>
<jsp:setProperty name="Document" property="*"/>
<%@include file="/includes/security.jsp" %>
<%
Document.setSessionData(SessionData);
Document.setStrAccessLevel(SessionData.getProperty("access_level"));
Document.setStrDistChainType(SessionData.getProperty("dist_chain_type"));
Document.setStrRefId(SessionData.getProperty("ref_id"));
Document.setIso_default(SessionData.getProperty("iso_default"));
DocumentComponent ic = new DocumentComponent(Document);
ic.disableField("doc");
ic.isEnabled("name");
ic.setValue("folderPath", application.getRealPath(application.getAttribute("documentRepositoryPath") + SessionData.getProperty("ref_id")));
String action = "" + request.getParameter("action");

if(SessionData.getUser() !=null){
	SessionData.getUser().getUsername();
	SessionData.getProperty("iso_id");
}
String message =request.getParameter("message");
String messageType =request.getParameter("messageType");
String id =request.getParameter("id");
String status = request.getParameter("status");
if(action.equals("update")){
	if(id!=null && !id.equals("")){
		Document.setDocumentId(Long.parseLong(id));
	}
	
	String op = request.getParameter("visible");
	if(op!=null && !op.equals("")){
		if(op.equals("1")){
			ic.setValue("visible",true);	
		}
		else{
			ic.setValue("visible",false);
		}
	}
	
	
	if(id!=null && !id.equals("")){
	ic.setValue("user", SessionData.getUser().getUsername());
	Document.setApprovalStatus(Integer.parseInt(status));
	
	if (SessionData.getUser().isIntranetUser()
		&& SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
				DebisysConstants.INTRANET_PERMISSION_MANAGE_DOCUMENTS)){
			ic.setUserIntranet("true");	
	}			
   	if(ic.save(SessionData)>0){
   		message = Languages.getString("jsp.admin.document.documentrepository.docupdate",SessionData.getLanguage());
		messageType = "1";
		//Document.setDocumentId(Long.parseLong(id));
		
		
		//enviar correo
		String mailHost = DebisysConfigListener.getMailHost(application);
	    String accessLevel = SessionData.getProperty("access_level"); 
	    String companyName = SessionData.getProperty("company_name");
	    String userName = SessionData.getProperty("username");
	    try{
	    Document.sendMailNotification(mailHost,accessLevel, companyName,userName,"updated",SessionData);
		}
		catch(Exception e){
			e.printStackTrace();
		}		
		
		
		response.sendRedirect("documentApproval.jsp?search=y&id="+ Document.getDocumentId()+"&mess="+message);
	}else{
		//message = "Error updating news";
		messageType = "2";
		Document.setDocumentId(Long.parseLong(id));
		//ic.loadFromDB();
		ic.disableField("doc");
		//response.sendRedirect("updateDocument.jsp?id="+ Document.getDocumentId() + "&messageType=" + messageType + "&message=" + HTMLEncoder.encode(message));
		//ic.disableField("doc");
		ic.loadDefaults();
	}
	

	}
	
}else{
	if( id != null && !id.equals("")){
		Document.setDocumentId(Long.parseLong(id));
		
	}
	ic.loadFromDB();
	
}
%>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<style>
	.details-table .checkList {
		border: none;
	}
	.details-table .checkList li{
		float: left;
	}
</style>
<%if (SessionData.getUser().isIntranetUser()
		&& SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
				DebisysConstants.INTRANET_PERMISSION_MANAGE_DOCUMENTS)){%>
<script language="JavaScript">
	$(document).ready(function(){
		$(".checkList li input[name=chain]").attr("disabled", "true");
	});
</script>
<%} %>
<form name="mainform" method="post" action="admin/documents/updateDocument.jsp?id=<%=Document.getDocumentId()%>">
<input type="hidden" name="action" value="update">
<input type="hidden" name="isoId" value=<%=SessionData.getProperty("iso_id")%>>
<input type="hidden" name="repId" value=<%=SessionData.getProperty("ref_id")%>>
<input type="hidden" name="user" value=<%=SessionData.getUser().getUsername()%>>
<input type="hidden" name="opt" value="update">
<input type="hidden" name="id" value="<%=id%>">
<input type="hidden" name="status" value="<%=Document.getApprovalStatus()%>">

<%
if(message != null){
%>
			<p style="color:<%=(messageType.equals("1")? "green":"red")%>"><%=message%></p>
<%
}
%>
	<table border="0" cellpadding="0" cellspacing="0" width="750">
		<tr>
			<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.tools.documentSettings",SessionData.getLanguage()).toUpperCase()%></td>
			<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
		</tr>
		<tr>
			<td colspan="3">
				<p class="main">
					
				</p>
				
<%
String drep =  SessionData.getProperty("drep");
if (SessionData.checkPermission(DebisysConstants.PERM_TO_DOCUMENT_MANAGEMENT) || (drep !=null && drep.equals("ok"))){
%>
	<%if(id != null && !id.equals("0") && !id.equals("")) {%>
				<table class="main details-table">
					<tr>
						<td colspan="3" align="left"><%=Languages.getString("jsp.admin.document.label.formatInformation",SessionData.getLanguage())%></td>
					</tr>
					
					<% ic.showFields(out,SessionData); %>
					
					<tr>
						<td colspan="3" align="left"><B><%=Languages.getString("jsp.admin.document.documentrepository.status",SessionData.getLanguage())%> </B>
						<%int st = Document.getApprovalStatus(); %> 
						<%if(st == 1){%><%=Languages.getString("jsp.admin.document.documentrepository.approved",SessionData.getLanguage())%><%}
						  else if (st == 0){%><%=Languages.getString("jsp.admin.document.documentrepository.denied",SessionData.getLanguage())%><%} 
						  else if (st == 2){%><%=Languages.getString("jsp.admin.document.documentrepository.pending",SessionData.getLanguage())%><%}
						  %>
						</td>
					</tr>

					<tr>
						<td colspan="3" align="left"><B><%=Languages.getString("jsp.admin.document.label.document",SessionData.getLanguage())%> </B> 
						<a href="admin/documents/documentDownload.jsp?idfile=<%=Document.getDocumentId()%>" target="_blank" onClick="window.open(this.href, this.target, 'width=300,height=400'); return false;" onmouseover="status='';return true"><%=Document.getName() %></a>
						</td>
					</tr>
					
					<tr>
						<td colspan="3" align="center"><%=Languages.getString("jsp.admin.document.label.information",SessionData.getLanguage())%></td>
					</tr>
					<tr>
						<td colspan="3" align="center"><input type="submit" name="update" value="<%=Languages.getString("jsp.admin.document.documentrepository.updatebutton",SessionData.getLanguage())%>"></td>
					</tr>
				</table>
	<%}else{%>
				<p class="main">
					<%=Languages.getString("jsp.admin.document.documentrepository.toolError",SessionData.getLanguage())%>
				</p>
	<%}%> 				
<%
}else { %>
				<p class="main">
					<%=Languages.getString("jsp.admin.document.documentrepository.toolNoPermission",SessionData.getLanguage())%>
				</p>
<%} %>				
			</td>
		</tr>
	</table>
</form>