
<%@page import="java.util.regex.Pattern"%>
<%@ page import="java.util.Hashtable,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.utils.DebisysConfigListener,
                 com.debisys.languages.Languages,
                 java.util.Vector, java.util.Iterator
                 "%>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:setProperty name="User" property="*"/>
<%
int section=1;
int section_page=2;
String strDistChainType = "";
String strAccessLevel = "";  
SessionData.setIsChangePassword(true);
if(SessionData.getUser()==null){
	String l = request.getParameter("l");
	if(l!=null && !l.equals("")){
		SessionData.setLanguage(l);
	}else{
		SessionData.setLanguage(application.getAttribute("language").toString());
	}
	
}

boolean bPasswordChangeResult = false;
boolean bPasswordIsOld = false;
boolean bIsMobileAuthenticationOn=false;
bIsMobileAuthenticationOn= SessionData.checkPermission(DebisysConstants.PERM_MOBILE_AUTHENTICATION);
String email="";
String refId="";
String refType="";
String passwordId="";
Vector userResults = new Vector();
userResults= User.getUserDetails(SessionData.getProperty("username"));
String userNameData = ((SessionData != null && SessionData.getUser() != null && SessionData.getUser().getName() != null &&
        !SessionData.getUser().getName().isEmpty()) ? SessionData.getUser().getName() : null);
String userIdData = ((SessionData != null && SessionData.getUser() != null && SessionData.getUser().getUsername() != null &&
        !SessionData.getUser().getUsername().isEmpty()) ? SessionData.getUser().getUsername() : null);
if (userResults !=null && userResults.size() > 0){
	email=(String)userResults.get(0);
	refId=(String)userResults.get(1);
	refType=(String)userResults.get(2);
	passwordId=(String)userResults.get(3);
}

boolean errorAnswers = false;
boolean saveAnswers = false;
if ( (request.getParameter("password") != null) && (request.getParameter("password").length() > 0) ){
	if ( request.getParameter("password").equals(SessionData.getProperty("password")) ){
		bPasswordIsOld = true;
	}else{
		bPasswordChangeResult = User.updatePasswordLoginDate(SessionData,request.getParameter("password"),request.getParameter("email"), SessionData.getProperty("username"),request.getParameter("refId"),request.getParameter("refType"),application,passwordId);
		SessionData.setIsChangePassword(false);
		if(request.getParameter("setAnswers") != null && request.getParameter("setAnswers").equals("1")){
			Vector<String> answers = new Vector<String>();
			for(int i=0; i< 4; i++){
				String ans = request.getParameter("answer"+i);
				if(ans==null){
					errorAnswers = true;
					break;
				}else{
					answers.add(ans);
				}
			}
			SessionData.getUser().saveAnswers(answers);
		}
		response.sendRedirect("/support/admin/index.jsp");
	}
}
%>
<%@ include file="/includes/header.jsp" %> 
	<table cellSpacing=0 cellPadding=0 width=450 border=0>
		<tr>
			<td align="left" width="1%" background="images/top_blue.gif">
				<img height=20 src="images/top_left_blue.gif" width=18>
			</td>
			<td class="formAreaTitle" background="images/top_blue.gif" class="formAreaTitle" width="2000">
				&nbsp;<%=Languages.getString("jsp.updatePassword.title",SessionData.getLanguage()).toUpperCase()%>
			</td>
			<td align="right" width="1%" background="images/top_blue.gif">
				<img height="20" src="images/top_right_blue.gif" width="18">
			</td>
		</tr>
		<tr>
			<td bgColor="#ffffff" colSpan="3">
				<table cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td class=formArea>
							<form id="changePass" action="admin/changePassword.jsp" method="post">
								<table width="100%">
									<tr class="main">
										<td noWrap style="padding-left:18px;">
											<table class="main">
												<tr>
													<td><%=Languages.getString("jsp.admin.newpassword",SessionData.getLanguage())%></td>
													<td>&nbsp;</td>
													<td><%=Languages.getString("jsp.admin.confirmpassword",SessionData.getLanguage())%></td><td>&nbsp;</td>
													<td><%=Languages.getString("jsp.admin.customers.user_logins.email",SessionData.getLanguage())%></td><td>&nbsp;</td>
												</tr>
												<tr>
													<td>
														<input type=hidden id=refId name=refId value="<%=refId%>">
														<input type=hidden id=refType name=refType value="<%=refType%>">
														<input id="txtPasswordValue" type="PASSWORD"  name="password" maxlength="50" size="20">
													</td>
													<td></td>
													<td><input id="txtPasswordConfirm" type="PASSWORD" maxlength="50" size="20"></td>
													<td></td>
													<td><input id="txtEmail" type="text" name="email" <%if(email!=null){ %>value="<%=email%>"<%}%> maxlength="50" SIZE="20"></td>
													<td></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td COLSPAN="7"><FONT COLOR="#ff0000"><B>
														<span id="lblEnterPassword" style="display:inline;"><%=Languages.getString("jsp.admin.enterpassword",SessionData.getLanguage())%><br></span>
														<span id="lblEnterNewPassword" style="display:inline;"><%=Languages.getString("jsp.admin.passwordisold",SessionData.getLanguage())%><br></span>
														<span id="lblPasswordNoMatch" style="display:none;"><%=Languages.getString("jsp.admin.passwordnomatch",SessionData.getLanguage())%><br></span>
														<span id="lblPasswordNotStrong" style="display:none;"><%=Languages.getString("jsp.admin.passwordChangeError",SessionData.getLanguage())%><br></span>
														<span id="lblPasswordMobNotStrong" style="display:none;"><%=Languages.getString("jsp.admin.passwordMobChangeError",SessionData.getLanguage())%><br></span>
														<span id="lblEmailError" style="display:none;"><%=Languages.getString("jsp.admin.contact.index.error2",SessionData.getLanguage())%><br></span>
                                                                                                                <span id="lblErrorPasswordContainsUserId" style="display:none;"><%=Languages.getString("com.debisys.users.error_password_contains_login_name", SessionData.getLanguage())%><br></span>
														</B></FONT>
													</td>
												</tr>
												<tr><td colspan="100%"><hr></td></TR>
												<tr>
													<td colspan="100%"><%=Languages.getString("jsp.answers.securityquestions",SessionData.getLanguage())%></td>
												</tr>
<%
	if(User != null && !User.hasAnswersConfigured(SessionData.getUser().getUsername())){
		Hashtable<String, String> answers = SessionData.getUser().getAnswers();
%>
		<tr>
			<td colspan="100%"><input type="hidden" name="setAnswers" value="1"></td>
		</tr>
<%

		for(int i=0; i< 4; i++){
			String ans = answers.get(""+i);
			if(ans==null){
				ans="";
			}
			%>
			<tr>
				<td colspan="3"><%=Languages.getString("jsp.answers.security_question" + i,SessionData.getLanguage())%></td>
				<td colspan="5"><input type="text" maxLength="64" style="width:90%;" id="answer<%=i%>" name="answer<%=i%>" data-old="<%=ans%>" value="<%=ans%>"/></td>
			</tr>
<%
		}
	}
%>
												<tr>
													<td colspan="100%">
														<font color="#ff0000">
															<b id="answermsgs"></b>
														</font>
													</td>
												</tr>
												<tr><td colspan="100%"><hr></td></TR>
												<tr>
													<td colspan="6"></td>
													<td><input type="submit" value="<%=Languages.getString("jsp.admin.changepassword",SessionData.getLanguage())%>"></td>
													<td>&nbsp;</td>
												</tr>
												<tr><td colspan="100%">&nbsp;</td></TR>
											</table>
										</td>
										<td width="306"></td>
									</tr>
									<tr class="main">
									<td style="padding-left:18px;"></td>
										<td></td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<script>
	function ValidatePassword()	{
            var userId = "<%=userIdData%>";
            var userName = "<%=userNameData%>";
            var isPasswordWithoutId = ("<%= pageContext.getServletContext().getAttribute(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM) %>" == 1) ? true : false; 
            
		if ($('#txtPasswordValue').val() != ''){
			if($('#txtPasswordValue').val() == $('#txtPasswordConfirm').val()){
<%
if(bIsMobileAuthenticationOn){
%>
				if(checkMobilePassword($('#txtPasswordValue').val())){
					return true;
				}else{
					$('#lblPasswordMobNotStrong').show();
					return false;
				}
<%
}else{
%>	
				if(checkPassword($('#txtPasswordValue').val())) {
                                    if (isPasswordWithoutId && passwordContainsId(userId.replace(/\s/g, '').toUpperCase(), 
                                        userName.replace(/\s/g, '').toUpperCase(), $('#txtPasswordValue').val().toUpperCase()) ) {
                                        $("#lblErrorPasswordContainsUserId").show();
                                        return false;
                                    } else {
                                        return true;
                                    }
				}else{
					$('#lblPasswordNotStrong').show();
					return false;
				}
<%
}
%>
			}// end of match
			else{
				$('#lblPasswordNoMatch').show();
				return false;
			}
		}// end of null check
		else{
			$('#lblEnterPassword').show();
			return false;
		}
	}// end of function
	
	function checkPassword(str){
		var regx=/^(?=.{8,})(?=.*\d.*\d.*\d)(?=.*[!@#\$%\^&\*\(\)\+=\|;'"{}<>\.\?\-_\\/:,~`])(?=.*[A-Z]).*$/;
		return regx.test(str);
	}
	
	function checkMobilePassword(str){
		var regx=/^([a-zA-Z0-9_-]){3,}$/;
		return regx.test(str);
	}
	
	function checkEmail(str){
		var regx = /^([\w\-]+\.?)+[\w\-]+@([\w\-]+\.?)+[\w\-]+\.\w{2,4}$/i;
		 return regx.test(str);
	}
	
	function hideMsgs(){
		$('#lblEmailError').hide();
		$('#lblEnterPassword').hide();
		$('#lblEnterNewPassword').hide();
		$('#lblPasswordNoMatch').hide();
		$('#lblPasswordNotStrong').hide();
                $('#lblErrorPasswordContainsUserId').hide();
		$('#answermsgs').html('');
	}

	function validateForm(){
		var ok= true;
		hideMsgs();
		
		if(!ValidatePassword()){
			ok = false;
		}
		// email validation
		if(!checkEmail($('#txtEmail').val())){
			$('#lblEmailError').show();
			ok = false;
		}

		// answer validation
		for(var i=0;i<4;i++){
			if($('#answer'+i).val() == ''){
				$('#answermsgs').html('<div class="error"><%=Languages.getString("jsp.answers.not_empty",SessionData.getLanguage())%></div>');
				ok = false;
			}
		}
		
		if(ok) $('#answers').hide();
		
		return ok;		
	}
        
        function passwordContainsId(userDni, userNameM, stringToCompare) {
            var consecutive_not_allowed = parseFloat("<%= DebisysConstants.CHAR_LENGTH_NOT_ALLOWED%>");
            var partName;
            
            if (stringToCompare != undefined && stringToCompare != null && 
                stringToCompare.length >= consecutive_not_allowed) {
            
                for (var i = 0; i < stringToCompare.length; i++) {
                    partName = stringToCompare.substring(i, (consecutive_not_allowed + i));
                    if (partName.length == consecutive_not_allowed) {
                        if (userDni.includes(partName) || userNameM.includes(partName)) {
                            console.log(partName);
                            return true;
                        } else if ((stringToCompare.length - (consecutive_not_allowed - 1)) == (i + 1) &&
                            partName[consecutive_not_allowed - 1] == stringToCompare[stringToCompare.length - 1] ) {
                            return false;
                        }
                    }
                }
            }
            return false;
        }

	$(document).ready(function(){
		$('#changePass').submit(validateForm);
		$('#txtPasswordValue').val('');
		$('#txtPasswordValue').focus();
		hideMsgs();
<%
if(bPasswordIsOld){
%>
		$('#lblEnterNewPassword').show();
<%
}else{
%>
		$('#lblEnterPassword').show();
<%
}
%>
	});
</script>
<%@ include file="/includes/footer.jsp" %>