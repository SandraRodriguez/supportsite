<%@ 
	page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.DebisysConfigListener,
                 com.debisys.utils.NumberUtil" 
%>

<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>

<%
	int section=10;
	int section_page=1;
%>
	<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
	<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
	<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ 
	include file="/includes/security.jsp" 
%>
<%
	String strIsoID = "-1";
	if (request.getParameter("submitted") != null)
	{
		try
	    {
		    if (request.getParameter("submitted").equals("y"))
    		{
		    	if (request.getParameter("IsoID") != null)
		    	{
		    		strIsoID = request.getParameter("IsoID");
                    User = com.debisys.users.User.getInfoUserMultiIso(strIsoID);            
                    boolean isIntranetUser = SessionData.getUser().isIntranetUser();
                                        
                    if ( isIntranetUser )
                    {
                    	SessionData.setProperty("ref_id", strIsoID);
                    	SessionData.setProperty("iso_id", strIsoID);
                    	if (User.validateLogin(application, SessionData, true))
                    	{
                             SessionData.setProperty("ref_type", User.getRefType());
                             SessionData.setProperty("access_level", User.getAccessLevel());
                             SessionData.setProperty("dist_chain_type", User.getDistChainType());
                    	}
                    }
                    else
                    {                    	 
	                     if (User.validateLogin(application, SessionData, true))
	                     {                                        
                             String last_lang = SessionData.getLanguage();
                             
                             SessionData.clear();
                             SessionData.setProperty("username", User.getUsername());
   					         SessionData.setProperty("password",User.getPassword());
                             SessionData.setProperty("ref_id", User.getRefId());
                             SessionData.setProperty("ref_type", User.getRefType());
                             SessionData.setProperty("access_level", User.getAccessLevel());
                             //determines 3 level or 5 level layout
                             SessionData.setProperty("dist_chain_type", User.getDistChainType());
                             SessionData.setProperty("company_name", User.getCompanyName());
                             SessionData.setProperty("contact_name", User.getContactName());
                             SessionData.setProperty("ip_address", request.getRemoteAddr());
                             SessionData.setProperty("iso_id", User.getISOId(User.getAccessLevel(),User.getRefId()));
                             SessionData.setProperty("datasource", User.getDatasource());
                             User.setPermissions(SessionData);
                             SessionData.setIsLoggedIn(true);
                             SessionData.setUser(User); 
                             SessionData.setLanguage(last_lang);
                             response.sendRedirect(request.getContextPath()+"/admin/index.jsp");             
                             return;                                        
                   		}                                  
                    }                                                           
                }
                SessionData.removeProperty("CurrentRatePlanActor");
                SessionData.removeProperty("RatePlanActors");
		    }
  		}
		catch (Exception e){}  
	}		    		
%>
<%@ 
	include file="/includes/header.jsp" 
%>

<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    	<td width="18" height="20" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	    <td class="formAreaTitle" align="left" width="2000"><%=Languages.getString("jsp.admin.manageiso.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20" align="right"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
				<tr>
			    	<td>
	    				<form name="frmMain" id="frmMain" method="post" action="/support/admin/multiiso/multi.jsp" >
	    				<input type="hidden" id="submitted" name="submitted" value="y">	
						<input type="hidden" id="IsoID" name="IsoID" value="<%=strIsoID%>">
      					<table border="0" width="100%" cellpadding="0" cellspacing="0">
     						<tr>
	        					<td class="formArea2">
	          						<table width="300">
               							<tr>
               								<td valign="top" nowrap>
												<table width=400>
	                            		    		<tr class="main">
	                            		    			<td>
	                            		    				<%=Languages.getString("jsp.admin.ach.summary.iso",SessionData.getLanguage())%>
	                            		    			</td>
    	                    	    	    			<td colspan=3>	 	                            		    		   		
        	            	            					<select id="isoList" name="isoList">
				            	    	                    <%
            					    	                    	Vector vecIsoList = TransactionSearch.getIsoList();
    	    	        	    				                Iterator itIL = vecIsoList.iterator();
				            	    	                    	while (itIL.hasNext())
	        					                	            {
                	            				    	        	Vector vecTempIL = null;
                	            				    	        	vecTempIL = (Vector) itIL.next();
				                        	                		out.println("<option value=" + vecTempIL.get(0) +">" + vecTempIL.get(1) + "</option>");
               				            	        	    	}
                            					    	    %>                                 	
	    	                	        	        		</select>	                            		    		   		
        	    	                	        		</td>
	    	    	                	        	</tr>
													<tr class="main">
    													<td colspan=4>
      														<input type="hidden" name="search" id="search" value="y">
									         				<input type="submit" name="changeIso" id="changeIso" value="<%=Languages.getString("jsp.admin.manageiso.cmd",SessionData.getLanguage())%>" onclick="chgIso();">
            	    		    	            			<SCRIPT>
            	    		        	        				function chgIso()
                                        	            		{
	                                        	            		document.getElementById('IsoID').value = document.getElementById('isoList').value ;
                                                    	    		//document.frmMain.submit();//This line is causing a double http request
                                                     			}
                                                			</SCRIPT>	 									         				
    													</td>														
													</tr>
												</table>
		               						</td>
              							</tr>
            						</table>
            					</td>
            				</tr>	
            			</table>
            		 </form>
            	  </td>
               </tr>	
           </table>
		</td>
	</tr>
</table>
<%@ 
	include file="/includes/footer.jsp" 
%>