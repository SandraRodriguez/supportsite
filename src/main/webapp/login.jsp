<%@ page import="java.util.Hashtable,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.utils.DebisysConfigListener,
                 com.debisys.languages.Languages,
                 com.debisys.utils.Announcements,
                 java.util.Vector, java.util.Iterator
                 "%>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:setProperty name="User" property="*"/>
<script>
<%
	String langua="";
	String pathContp = request.getContextPath();
	String newloc= pathContp+"/servlet/Languages?lan=";
	String newloc_1=pathContp+"/servlet/Languages?lan=";
	String newloc_2="&page="+request.getContextPath() + request.getServletPath();
%>
function changeLanguage(obj){
	if(obj.value=="spanish"){
<%
	langua="spanish";
	newloc+=langua+newloc_2;
	System.out.println("newloc_spanish" + newloc);
%>
		window.location = "<%=newloc%>";
	}else if(obj.value=="english"){
<%
	langua="english";
	newloc_1+=langua+newloc_2;
	System.out.println("newloc_english" + newloc_1);
%>

		window.location = "<%=newloc%>";
	}
}
</script>
<%
	Vector<Vector<String>> vecAnnouncements = Announcements.getAvailableAnnouncements(request);
	if(SessionData.getUser()==null){
		if(SessionData.getLanguage().equals("1")){
			SessionData.setLanguage(application.getAttribute("language").toString());
		}
	}
		
		if ( SessionData.isLoggedIn() )
		{
		 //out.println("this user is already logged.");
	 	 response.sendRedirect("/support/admin/index.jsp");
	 	 return;
	}
		
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
	int section = 1;
	int section_page = 1;
	//String s = request.getServerPort() + " " + request.getLocalPort() + " " + request.getRemotePort();
	Hashtable userErrors = (Hashtable)request.getSession().getAttribute("userErrors");
	if (userErrors != null)
	{
		System.out.println("errors jsp");
	}
	//else
	//{
		request.getSession().removeAttribute("userErrors");
	//}
	
	
	Boolean bcaptcha = (Boolean)request.getSession().getAttribute("captchaError");
	boolean captchaError = (bcaptcha != null && bcaptcha == true);
	Boolean showCaptcha = (Boolean)request.getSession().getAttribute("showCaptcha");
	
	String strDistChainType = "";
	String strAccessLevel = "";
	String strUrl = request.getParameter("url");

%>
<%@ include file="/includes/header.jsp" %>
<div id="menu" style="width:500px;float:left;">
               
	<table cellSpacing="0" cellPadding="0" width="450" border="0">
		<tbody>
			<tr>
				<td align="left" width="1%" background="images/top_blue.gif">
					<img height="20" src="images/top_left_blue.gif" width="18">
				</td>
				<td class="formAreaTitle" background="images/top_blue.gif" width="2000">
					&nbsp;
					<b class="formAreaTitle"><%=Languages.getString("jsp.index.login",SessionData.getLanguage()).toUpperCase()%></b>
				</td>
				<td align="right" width="1%" background="images/top_blue.gif">
					<img height="20" src="images/top_right_blue.gif" width="18">
				</td>
			</tr>
			<tr>
				<td bgColor="#5F8716" colSpan="3">
					<table cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tbody>
							<tr>
								<td>
									<form name="userform" action="<%=path%>/servlet/SecurityLoginExtern" method="post">
										<input type="hidden" value="y" name="submit_login">
	<%
		if (strUrl != null && !strUrl.equals("") && !strUrl.equalsIgnoreCase("null")){
			out.print("<input type=\"hidden\" name=\"url\" value=\"" +strUrl+ "\">");
		}
	%>
										<table cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tbody>
												<tr>
													<td class="formArea2">
														<div class="mainpad">
															<br/>
															<%=Languages.getString("jsp.index.instructions",SessionData.getLanguage())%>
														</div>
														<br/>
	<%
		if (userErrors != null){
	%>
														<div class="rowred2" style="padding-left:18px;word-wrap: break-word;">
															<%=(userErrors.containsKey("login")?userErrors.get("login"):Languages.getString("jsp.index.error_input",SessionData.getLanguage()))%>
														</div>
	<%
		}
		if (captchaError){
	%>
														<div class="rowred2" style="padding-left:18px;">
															<%=Languages.getString("jsp.index.captcha_error",SessionData.getLanguage())%>
														</div>
	<%
		}
	%>
														<table width="100%">
															<tbody>
																<tr class="main">
																	<td noWrap="nowrap" width="130" style="padding-left:18px;">
																		<%=Languages.getString("jsp.index.username",SessionData.getLanguage())%>:
																	</td>
																	<td width="306">
																		<input maxLength="64" size="30" name="username" value="<%=User.getUsername()%>"/>
	<%
		if (userErrors != null && userErrors.containsKey("username")){
			out.print("<font color=ff0000>*</font>");
		}
	%>
																		&nbsp;&nbsp;&nbsp;
																		<img height="18" alt="<%=Languages.getString("jsp.index.username",SessionData.getLanguage())%>" src="images/help.gif" width="18" border="0"/>
																	</td>
																</tr>
																<tr class="main">
																	<td style="padding-left:18px;">
																		<%=Languages.getString("jsp.index.password",SessionData.getLanguage())%>:
																	</td>
																	<td>
																		<input type="password" maxLength="64" size="30" name="password"/>
	<%
		if (userErrors != null && userErrors.containsKey("password")){
			out.print("<font color=ff0000>*</font>");
		}
	%>
																		&nbsp;&nbsp;&nbsp;
																		<img height="18" alt="<%=Languages.getString("jsp.index.password",SessionData.getLanguage())%>" src="images/help.gif" width="18" border="0"/>
																	</td>
																</tr>
																<tr class="main">
																	<td style="padding-left:18px;">
																		&nbsp;
																	</td>
																	<td align="right" style="text-decoration: underline;">
																		<a href="admin/forgot_password.jsp"><%=Languages.getString("jsp.index.forgot_password",SessionData.getLanguage())%></a>
																	</td>
																</tr>															
	<%
		if(showCaptcha != null && showCaptcha == true){
	 %>
																<tr class="main">
																	<td style="padding-left:18px;" colspan="2">
																		<%=Languages.getString("jsp.index.captcha_desc",SessionData.getLanguage())%>
																		<br />
																	</td>
																</tr>
																<tr class="main">
																	<td>&nbsp;</td>
																	<td>
																		<img src="simpleCaptcha.png" id="captcha">
																	</td>
																</tr>
																<tr class="main">
																	<td style="padding-left:18px;">
																		<%=Languages.getString("jsp.index.captcha_answer",SessionData.getLanguage())%>:
																	</td>
																	<td>
																		<input type="text" size="30" maxLength="10" name="answer_captcha" />
																	</td>
																</tr>
	<%
		}
	 %>
															</tbody>
														</table>
														<table width="100%">
															<tbody>
																<tr class="main">
																	<td>
																		<center>
																			<input type="submit" value="<%=Languages.getString("jsp.index.login",SessionData.getLanguage())%>" name="submit">
																		</center>
																	</td>
																</tr>
															</tbody>
														</table>
														<br><br>
														
													</td>
												</tr>
											</tbody>
										</table>
									</form>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div id="announcementsRight" style="width:55%;float:left;">

	<table width="100%">
		<tbody>
			<tr class="main">
				<td>
					<%
						for(Vector<String> vec: vecAnnouncements){
							if(vec.get(0).equalsIgnoreCase("right")){%>
								<%=vec.get(1)%>
							<%}
						}
					 %>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div id="announcementsBottom" style="clear:both; resize: both;">
<table width="100%">
		<tbody>
			<tr class="main">
				<td>
					<%
						for(Vector<String> vec: vecAnnouncements){
							if(vec.get(0).equalsIgnoreCase("bottom")){%>
								<%=vec.get(1)%>
							<%}
						}
					 %>
				</td>
			</tr>
		</tbody>
	</table>

</div>
<%@ include file="/includes/footer.jsp" %>
