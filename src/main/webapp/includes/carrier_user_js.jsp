<!-- 
// All Javascript used on pages for the Carrier User reports will be lcoated here.
// Make it a JSP so you load all the stuff
 -->
<script>
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll(){
	document.mainform.submit.disabled = true;
	document.mainform.submit.value = iProcessMsg[count];
	count++;
	if (count = iProcessMsg.length){ 
		count = 0;
	}
	setTimeout('scroll()', 150);
}

function scroll2(){
	document.downloadform.submit.disabled = true;
	document.downloadform.submit.value = iProcessMsg[count];
	count++;
	if (count = iProcessMsg.length){ 
		count = 0;
	}
	setTimeout('scroll2()', 150);
}

function PrintReport(){
	var divBanner = document.getElementById("divPrintBanner");
	var divMenu = document.getElementById("divPrintMenu");
	var divButton = document.getElementById("divPrintButton");
	
	if ( divBanner != null ){
		divBanner.style.display = "none";
	}
	if ( divMenu != null ){
		divMenu.style.display = "none";
	}
	divButton.style.display = "none";
	window.print();
	divButton.style.display = "inline";
	if ( divMenu != null ){
		divMenu.style.display = "inline";
	}
	if ( divBanner != null ){
		divBanner.style.display = "inline";
	}
}

function FilterMinAmount(ctl){
	var sText = ctl.value;
	var sResult = "";
	var bHasDot = false;

	if ( sText.length > 0 ){
		for ( i = 0; i < sText.length; i++ ){
			if ( (sText.charAt(i) == '.') && (i != 0) && !bHasDot ){
				sResult += sText.charAt(i);
				bHasDot = true;
			}else if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) ){
				sResult += sText.charAt(i);
			}
		}
		if ( sResult == "" ){
			document.getElementById("txtMaxAmount").value = "";
			document.getElementById("txtMaxAmount").disabled = true;
		}else{
			document.getElementById("txtMaxAmount").value = sResult;
			document.getElementById("txtMaxAmount").disabled = false;
		}
		if ( sText != sResult ){
			ctl.value = sResult;
		}
	}else{
		document.getElementById("txtMaxAmount").value = "";
		document.getElementById("txtMaxAmount").disabled = true;
	}
	return true;
}//End of function FilterMinAmount

function ValidateMinAmount(ctl){
	var sText = ctl.value;
	if ( isNaN(parseFloat(ctl.value)) ){
		ctl.value = "";
		sText = ctl.value;
	}

	if ( sText.length > 0 ){
		if ( parseFloat(sText) == 0 ){
			alert("<%=Languages.getString("jsp.admin.reports.transactions.minamount.zero",SessionData.getLanguage())%>");
			return false;
		}
		if ( (document.getElementById("txtMaxAmount").value != "") && !isNaN(parseFloat(document.getElementById("txtMaxAmount").value)) ){
			if ( parseFloat(sText) > parseFloat(document.getElementById("txtMaxAmount").value) ){
				alert("<%=Languages.getString("jsp.admin.reports.transactions.minamount.validate",SessionData.getLanguage())%>");
				return false;
			}
		}else{
			document.getElementById("txtMaxAmount").value = sText;
			document.getElementById("txtMaxAmount").disabled = false;
		}
	}else{
		document.getElementById("txtMaxAmount").value = "";
		document.getElementById("txtMaxAmount").disabled = true;
	}
	return true;
}//End of function ValidateMinAmount

function FilterMaxAmount(ctl){
	var sText = ctl.value;
	var sResult = "";
	var bHasDot = false;

	if ( sText.length > 0 ){
		for ( i = 0; i < sText.length; i++ ){
			if ( (sText.charAt(i) == '.') && (i != 0) && !bHasDot ){
				sResult += sText.charAt(i);
				bHasDot = true;
			}else if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) ){
				sResult += sText.charAt(i);
			}
		}
		if ( sText != sResult ){
			ctl.value = sResult;
		}
	}
	return true;
}//End of function FilterMaxAmount

function ValidateMaxAmount(ctl){
	var sText = ctl.value;
	if ( isNaN(parseFloat(ctl.value)) ){
		ctl.value = "";
		sText = ctl.value;
	}

	if ( sText.length > 0 ){
		if ( parseFloat(sText) == 0 ){
			alert("<%=Languages.getString("jsp.admin.reports.transactions.maxamount.zero",SessionData.getLanguage())%>");
			return false;
		}
		if ( parseFloat(sText) < parseFloat(document.getElementById("txtMinAmount").value) ){
			alert("<%=Languages.getString("jsp.admin.reports.transactions.maxamount.validate",SessionData.getLanguage())%>");
			return false;
		}
	}
	return true;
}//End of function ValidateMaxAmount

function ValidateSubmit(){
	if ( !ValidateMinAmount(document.getElementById("txtMinAmount")) ){
		return false;
	}
	if ( !ValidateMaxAmount(document.getElementById("txtMaxAmount")) ){
		return false;
	}
	scroll();
	return true;
}

function filterByErrors(){
	window.location="/support/admin/reports/carrier_user/transactions/transaction_error.jsp?filter=errors&startDate=" + document.getElementById("startDate").value + "&endDate=" + document.getElementById("endDate").value;
}

function filterByMerchants(){
	window.location="/support/admin/reports/carrier_user/transactions/transaction_error.jsp?filter=merchants&startDate=" + document.getElementById("startDate").value + "&endDate=" + document.getElementById("endDate").value;
}

// Used in products_transactions.jsp
function openViewPIN(sTransId){
	var sURL = "/support/admin/transactions/view_pin.jsp?trans_id=" + sTransId;
	var sOptions = "left=" + (screen.width - (screen.width * 0.4))/2 + ",top=" + (screen.height - (screen.height * 0.3))/2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
	var w = window.open(sURL, "ViewPIN", sOptions, true);
	w.focus();
}

// show/hide divs used in carrier user transaction.jsp
function showHideRow(id){ 
	if (document.getElementById){ 
		obj = document.getElementById(id); 
		if (obj.style.display == "none"){ 
			obj.style.display = ""; 
		} else { 
			obj.style.display = "none"; 
		} 	
	} 
} 
</script>