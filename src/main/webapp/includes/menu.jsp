<%@page import="com.debisys.utils.DebisysConstants, com.debisys.languages.Languages"%>
<%@page import="com.debisys.users.SessionData"%>
<%@page import="com.debisys.presentation.Menu, com.debisys.presentation.MenuOption"%>
<%
StringBuffer optionTools= new StringBuffer("");
Menu m = new Menu(application, SessionData, section, strDistChainType, section_page, deploymentType, serverName);
%>
<script>
	$(document).ready(function(){
		$('.helpLink').click(function(){
			window.open('/support/admin/help/index.jsp#<%=section%>', 'help', 'width=520,height=600,resizable=0,scrollbars=yes');
		});
	});
</script>
<table id="mainTableSupport" width="100%" class="mtable fondosceldas">
	<tr>
		<td vAlign=top>
			<c:if test="${!SessionData.user.supportSiteNgUser}">
<%
if(m.getMenuOptions() != null && !m.getMenuOptions().isEmpty()){
%>
				<div id="mainmenu">
					<ul>
<%
	for(MenuOption op: m.getMenuOptions()){
%>
							<li class="<%=op.getSclass()%>">
								<a href="<%=op.getUrl()%>"><%=op.getLabel()%></a> 
							</li>
<%
	}
%>
					</ul>
				</div> 
<%
}
if(m.getSubMenuOptions() != null &&  !m.getSubMenuOptions().isEmpty()){
%>
				<div id="mainmenu" class="submenu">
					<ul>
						<li><div style="height: 32px; width: <%=m.getOffSet()%>px;"></div></li>
<%
	for(MenuOption op: m.getSubMenuOptions()){
%>
							<li class="<%=op.getSclass()%>">
								<a href="<%=op.getUrl()%>"><%=op.getLabel()%></a> 
							</li>
<%
	}
%>

					</ul>
				</div>
<%
	}
%>
		</c:if>
			<table class="ControllerStyleClass" cellSpacing="5" cellPadding="5" align="left" border="0">
				<tr align="left">
					<td style="color: f5f5f5; BACKGROUND-COLOR: f5f5f5" vAlign="top">
						<!--end menu--><!--end header-->

