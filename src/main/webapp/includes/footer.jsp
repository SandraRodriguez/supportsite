<%@page import="com.debisys.utils.DebisysConstants"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@ page import="java.util.Calendar" %>
<!--begin footer-->
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		&nbsp;&nbsp;&nbsp;
<%
//LF - Var "deploymentType" was changed to a direct call in order to allow conditional including of this file
if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
	int year = Calendar.getInstance().get(Calendar.YEAR);
%>

		<span class="copyrigth" nowrap>Copyright � Emida <%=year%></span>
		<script src="includes/urchin.js" type=text/javascript></script>
		<script type=text/javascript>
			_uacct = "UA-742998-2";
			urchinTracker();
		</script>
<%
}
%>		
	<jsp:include page="/includes/version.jsp"></jsp:include>
<script>

    /*This is a necessary script to clean cookies only the first time
     because Wildfly does not hold session attributes*/

    var read_cookie = function(name){
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
    var create_cookie = function(name, value, days){
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }
    if(!read_cookie("gga")){
        /* if this is first time, delete google analytics cookies to be created again*/
        create_cookie("__utma", "", -1);
        create_cookie("__utmb", "", -1);
        create_cookie("__utmc", "", -1);
        create_cookie("__utmz", "", -1);
        create_cookie("__utmt", "", -1);

        /* Create a this cookie to control above behavior*/
        create_cookie("gga", "1", 3000);
    }

</script>
	</body>
</html>


<!--end footer-->

