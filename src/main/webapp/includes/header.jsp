<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.HashMap"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="com.debisys.utils.DebisysConfigListener,
				com.debisys.utils.ConfigHelper,
				com.debisys.languages.Languages, java.util.Vector,
				java.util.Iterator,java.net.URLEncoder,java.util.Map"%>
<%
ConfigHelper ch = new ConfigHelper(request);
String serverName = ch.getServerName();
String baseHref = ch.getBaseHref();
String domainName = ch.getDomainName();
String serverPort = ch.getServerPort();
String requestUrl = ch.getRequestUrl();
String imageName = ch.getImageName();
String headerBackground = ch.getHeaderBackground();
String strBrandedCompanyName = ch.getBrandedCompanyName();
//requestUrl comes either as     http://    or     https://
String strProtocol = "http";
if(requestUrl.indexOf(':') >= 0){
	strProtocol = requestUrl.substring(0, requestUrl.indexOf(':')).toLowerCase();
}
String deploymentType = DebisysConfigListener.getDeploymentType(application);
String customConfigType = DebisysConfigListener.getCustomConfigType(application);
// ---> removed  <! DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
%>
<!DOCTYPE HTML>
<!--begin header-->
<html>
<head>
	<base href="<%=strProtocol%>://<%=baseHref%>/support/">
	<link href="default.css" type="text/css" rel="stylesheet">
	<link href="css/themes/emida-green/jquery-ui.css" type="text/css" rel="stylesheet">
	<title><%=strBrandedCompanyName%></title>
	<script language="JavaScript" src="/support/includes/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script language="JavaScript" src="/support/includes/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="/support/includes/SSWSCheckboxes.js"></script>
<%
if((section==4 && (section_page==1 || section_page==4))){
%>
		<script type="text/javascript" src="/support/includes/CollapsibleGroupPanel.js"></script>
		<link rel=StyleSheet href="/support/css/CollapsibleGroupPanel.css" type="text/css"></link>
<%
}
// DBSY-890 SW
// Show the javascript for the carrier user forms if the permission is checked.
if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)){
	if(section==14 && section_page==48){
%>
	<script type="text/javascript" src="/support/includes/XmlGetEntities.js"></script><%
	}else if(section==14 && (section_page==7 || section_page==8 || section_page==12  || section_page==14  || section_page==18)  ){%>
	<script type="text/javascript" src="/support/includes/CollapsibleGroupPanel.js"></script>
	<link rel=StyleSheet href="/support/css/CollapsibleGroupPanel.css" type="text/css"></link>
<%
	}
%>
	
	<script type="text/javascript">
		<%@ include file="/includes/carrier_user_js.jsp" %>
	</script>
<%
}
%>
	<script type="text/javascript">
		function changeLanguage(obj){
			//$.post('<%=request.getContextPath()%>/servlet/Languages?lan=' + obj + '&Random=' + Math.random(), ProcessBrowse);
			$("#divLanguage").load('admin/language.jsp?lan='+obj+'&Random=' + Math.random(), ProcessChangeLanguage);
		}
		function ProcessChangeLanguage(sData, sStatus){
			window.location.reload(true);
		}
	</script>
</head>
<body bgColor="#ffffff">
	<div id="divLanguage" class="main"></div>
	<div id="divPrintBanner" style="display: inline">
	<c:if test="${!SessionData.user.supportSiteNgUser}">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/banner.gif" bgcolor="#ffffff" class="fondogris" <%=headerBackground%>>
					<tr>
						<td width="100%" height="58">
							<table valign="middle" align="left" border="0" cellpadding="0" cellspacing="0" >
								<tr>
									<td colspan="2" align="left"><a href="<%=strProtocol%>://<%=serverName%>:<%=serverPort%>/support"><img src="images/<%=imageName%>" alt="<%=SessionData.getString("jsp.includes.header.home"/*,SessionData.getLanguage()*/)%>" border="0"></a></td>
								</tr>
								<tr>
<%
try{
	if ( request.getSession()!=null && request.getSession().getAttribute("SessionData")!=null ){
		if(SessionData.getUser()==null){
			if(SessionData.getLanguage().equals("1")){
				SessionData.setLanguage(application.getAttribute("language").toString());
			}
		}
%>

									<td style="font-size: 8pt;" class="float">&nbsp;&nbsp;<%=Languages.getString("jsp.admin.document.support.language",SessionData.getLanguage())%>: 
<%  
		String pathCont = request.getContextPath();
		String codeLanguage = "";
		com.debisys.users.SessionData sessionUser_ = (com.debisys.users.SessionData)request.getSession().getAttribute("SessionData");
		String lang=  sessionUser_.getLanguage();
		if(lang.equals("spanish")){
			codeLanguage="1";
		}else{
			codeLanguage="0";
		}
		Vector languages = Languages.searchLanguages();
		Iterator it_ = languages.iterator();
		while (it_.hasNext()) {
			Vector vecTemp_ = null;
			vecTemp_ = (Vector)it_.next();
			String language="";
			if(!codeLanguage.equals("1")){
				language=vecTemp_.get(1).toString();
			}else{
				language=vecTemp_.get(2).toString();
			}
%>
										<input type="hidden" value="<%=language%>" id="language" name="language"/>
										<!-- <a href="<%=pathCont%>/servlet/Languages?lan=<%=URLEncoder.encode(vecTemp_.get(1).toString().toLowerCase().trim(), "UTF-8")%>&page=<%=URLEncoder.encode(request.getContextPath() + request.getServletPath()+"?"+request.getQueryString(), "UTF-8") %>" id="<%=language%>"><%=language%></a> -->
										<a href="javascript:" id="<%=language%>" name="<%=language%>" onclick="changeLanguage('<%=URLEncoder.encode(vecTemp_.get(1).toString().toLowerCase().trim(), "UTF-8")%>');"><%=language%></a>
<%
			if(it_.hasNext()){
%>
										|
<%
			}
		}
%>
									</td>
<%
	}
	if ( request.getSession()!=null && request.getSession().getAttribute("SessionData")!=null ){
		com.debisys.users.SessionData sessionUser = (com.debisys.users.SessionData)request.getSession().getAttribute("SessionData");
		if (sessionUser!=null && sessionUser.getUser()!=null){
			String loginLabel = sessionUser.getProperty("username");
			String intranetSite = (String)request.getSession().getServletContext().getAttribute("intranet-site");
			String intranetPortSite = (String)request.getSession().getServletContext().getAttribute("intranet-port-site");
%>
									<td align="left" nowrap="nowrap">
										<div>
											<table id="tblH004" width="100%" height="20" border="0" cellpadding="5" cellspacing="0">
												<tr>
													<td style="font-size: 8pt;" class="float" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=loginLabel%> | ID = <%=sessionUser.getProperty("iso_id")%> | 
<%
			if (sessionUser.getUser().isIntranetUser()){
%>
														<a href="http://<%=intranetSite%>:<%=intranetPortSite%>/default.asp"><%=Languages.getString("jsp.includes.header.go_intranet",SessionData.getLanguage())%></a>
<%
			}
%>
													</td>
												</tr>
											</table>
										</div>
									</td>
<%
		}
	}else{
		request.getSession().getAttribute("SessionData");
	}
}catch(Exception e){
    if(!response.isCommitted())
    {
	response.sendRedirect("/support/message.jsp?message=3");
    }
}
%>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</c:if>
</div>
<%
    com.debisys.users.SessionData sessionUser = (com.debisys.users.SessionData)request.getSession().getAttribute("SessionData");
    String warnning1 = "warnning1";
    String enterYouAddress = "enterYouAddress";        
    String map_Establishments = "establishments";
    String map_Addresses = "addresses";
    String map_Geocodes = "geocodes";    
    String map_SaveButton = "save";
    String map_CloseButton = "close";
    String map_All = "all";
    
    if ( sessionUser.getUser() != null )
    {
        HashMap parametersName = new HashMap();
        String sessionLanguage = sessionUser.getUser().getLanguageCode();
        ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport(ResourceReport.RESOURCE_MERCHANT_MAP_LOCATOR, sessionLanguage, null);
        //parametersName.put(viewMerchantMapping, "viewMerchantMapping");
        parametersName.put(warnning1, "warnning1");
        parametersName.put(enterYouAddress, "enterYouAddress!");
        parametersName.put(map_Establishments, "Establishments!");
        parametersName.put(map_Addresses, "Addresses!");
        parametersName.put(map_Geocodes, "Geocodes!");        
        parametersName.put(map_SaveButton, "Save!");
        parametersName.put(map_CloseButton, "Close!");
        parametersName.put(map_All, "All!");

        for( ResourceReport resource : resources)
        {
            if ( parametersName.containsKey( resource.getKeyMessage() ) )
            {
                parametersName.put( resource.getKeyMessage(), resource.getMessage());            
            }        
        } 
%>        
        <input id="pac-input" class="controls" type="text" placeholder="<%= parametersName.get(enterYouAddress) %>" maxlength="450" style= "display: none;">
        <input id="warnningLabel" class="controls" type="button" style='font-size:11px;display: none;' value='<%= parametersName.get(warnning1) %>' >
        <img id="warnningLabelImg" src="images/warning_blink.gif" height="15px" width="15px" style= "display: none;"> 
        <div id="type-selector" class="controls" style= "display: none;">            
            <input type="radio" name="type" id="changetype-all" checked="checked">
            <label for="changetype-all"><%= parametersName.get(map_All) %></label>

            <input type="radio" name="type" id="changetype-establishment">
            <label for="changetype-establishment" id="map_Establishments"><%= parametersName.get(map_Establishments) %></label>

            <input type="radio" name="type" id="changetype-address">
            <label for="changetype-address" id="map_Addresses"><%= parametersName.get(map_Addresses) %></label>

            <input type="radio" name="type" id="changetype-geocode">
            <label for="changetype-geocode" id="map_Geocodes"><%= parametersName.get(map_Geocodes) %></label>
        </div>
        
        <input id="pacClose" class="controls" type="button" value="<%= parametersName.get(map_CloseButton) %>" style= "display: none;">
        <input id="pacSave"  class="controls" type="button" value="<%= parametersName.get(map_SaveButton) %> " style= "display: none;">        
        <input id="latitudeTemp" type="hidden" value="-1" />
        <input id="longitudeTemp" type="hidden" value="-1" />
        <input id="latitudeCurrent" type="hidden" value="-1" />
        <input id="longitudeCurrent" type="hidden" value="-1" />
        <div id="map" style= "display: none;"></div>
        <script type="text/javascript" src="/support/js/merchantMapping.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap"></script>
    <%}%>        
<%@ include file="/includes/menu.jsp" %>
<!--end header-->
