 /*==============================================================================
   #                            EMIDA TECHNOLOGIES
   #
   # AUTOR:                     JOHN MOLANO  (jmolano@emida.net)
   # DESCRIPCION:               AQUI SE ENCUENTRAN LAS FUNCIONES JAVA SCRIPT
   #                            NECESARIAS PARA VALIDACIONES EN PAGINAS JSP
   # FECHA CREACION:            JUNIO 10 DE 2008
   #==============================================================================*/
   
   function validaNumYTextoYLinea(elEvento)
   {   
       var e = elEvento || window.event;
       var caracter = e.charCode || e.keyCode;
       return (isDigit(caracter) || isAlpha(caracter) || isSpecialChar(caracter,e) || isLine(caracter,e));
   }

   function validaNumYTexto(elEvento)
   {   
       var e = elEvento || window.event;
       var caracter = e.charCode || e.keyCode;
       return (isDigit(caracter) || isAlpha(caracter) || isSpecialChar(caracter,e));
   }

   function validaNum(elEvento)
   {   
       var e = elEvento || window.event;
       var caracter = e.charCode || e.keyCode;
       return (isDigit(caracter) || isSpecialChar(caracter,e));
   }
   
   function isDigit(myCharCode)
   {  
      if((myCharCode > 47) && (myCharCode <  58))
      {
         return true;
      }   
      return false;
   }
   
   function isAlpha(myCharCode)
   {   
      if(((myCharCode > 64) && (myCharCode <  91)) || ((myCharCode > 96) && (myCharCode < 123)))
      {
         return true;
      }   
      return false;
   }
   
   function isSpecialChar(myCharCode,event)
   {
      if(((myCharCode == 13) || (myCharCode == 8) || (myCharCode == 9) || (myCharCode == 37) ||
        (myCharCode == 39) || (myCharCode == 46)) && !event.shiftKey )
      {
         return true;
      }   
      return false;    
   }

   function isLine(myCharCode)
   {   
      if(myCharCode == 45)
      {
         return true;
      }   
      return false;
   }
