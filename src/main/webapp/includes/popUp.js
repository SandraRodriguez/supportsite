function PopUp(popUpName, popUpTitle, popUpWidth, popUpHeight){
	this.name = popUpName +'PopUp';
	this.title = popUpTitle;
	this.onHide = null;
	this.onShow = null;
	this.width = popUpWidth;
	this.height = popUpHeight;
	
	var popUpExqueleton = '<div id="'+ this.name +'" class="popUpBG"><div class="popUpBody"><div class="popUpHeader"><div class="popUpTitle">'+ this.title +'</div><img class="popUpClose" src="images/closebutton.png"></div><div class="popUpContent"></div></div></div>';
	$(document.body).append(popUpExqueleton);
	$('#'+this.name).hide();
}
PopUp.prototype.show = function(){
	$('#'+this.name +" .popUpClose").click(this, this.hide);

	var w = $(window).width();
	var h = $(window).height();
	var p_w = this.width;//$("#pop").width();//width: 50%
	var p_h = this.height;//$("#pop").height();
	//p_w = (p_w < w*0.8) ? p_w : w*0.8;
	//p_h = (p_h < h*0.6) ? p_h : h*0.6;
	var left = (w/2) - (p_w/2);
	var top = (h/2) - (p_h/2);// + $(window).scrollTop();
		
	$('#'+this.name)
		.css("overflow", "hidden")
		.css("position","absolute")
		.css("width", "110%")
		.css("height", "110%")
		.css("left", "0px")
		.css("top", "0px")
		.css('z-index', 9999)
		.fadeIn('fast');
	$('#'+this.name + ' .popUpBody')
		.css("position","absolute")
		.css("width", p_w + "px")
		.css("height", p_h + "px")
		.css("left", left + "px")
		.css("top", top + "px")
		.css("margin", "auto");
		
	if(this.onShow != null){
		this.onShow();
	}
};
PopUp.prototype.hide = function(event){
	if(event){
		if(event.data){
			$('#'+event.data.name).hide();
			if(event.data.onHide != null){
				event.data.onHide();
			}
		}
	}else{
		$('#'+this.name).hide();
		if(this.onHide != null){
			this.onHide();
		}
	}
};
PopUp.prototype.setContent = function(content){
	$('#'+this.name +" .popUpContent").html(content);
}
PopUp.prototype.destructor=function(){
	$('#'+this.name).hide().remove();
}