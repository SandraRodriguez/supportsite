 

function loadXmlGetEntities(){
		if(validation2 && Isolevel) {
			importSubAgentXML();
		}else if(Isolevel) {
			importISOXML();
		}else if(Agentlevel) {
			importAgentXML();
		}else if(SubAgentlevel) {
			importSubAgentXML();
		}else if(RepLevel) {
			importRepXML();
		}
}
/*
 * Alfred A./DBSY-1082 - Generic xmlDoc loading depending on the active browser
 */
function createXMLDoc(){
	var tmpdoc;
	if(window.ActiveXObject){
		tmpdoc = new ActiveXObject("Microsoft.XMLDOM");
	}else if(document.implementation && document.implementation.createDocument){
		tmpdoc = document.implementation.createDocument("", "", null);
	}else{
		alert('Form is not supported in your browser');
		return;
	}
	return tmpdoc;
}

//Alfred A./DBSY-1082 - Generic function to generate an array of values from a form.
function returnOptionsValues(formOptions){
	var paramArray = new Array();
	arrayCounter = 0;
	for(var i = 0;i<formOptions.length;i++){
		if(formOptions.options[i].value != 0 && formOptions.options[i].value != -1){
			paramArray[arrayCounter] = formOptions.options[i].value;
			arrayCounter++;
		}
	}
	return paramArray;
}

function generateURL(mainURL, additionalStringParam, paramArray, singleParam){
	var url = null;
	if(paramArray.length != 0){
		url = mainURL+paramArray[0];
		for(var i = 1; i < paramArray.length; i++){
			if(i < 100)
				url = url + additionalStringParam + paramArray[i];
		}
	}else{
		url = mainURL+singleParam;
	}
	return url;
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the ISO version.
 */
function importISOXML(){
	var xmlDoc = createXMLDoc();
	xmlDoc.async = false;
	var isoParam = ref_id;
	var url = "admin/reports/transactions/data_daily_liability.jsp?isoParam="+isoParam;
	var loaded = xmlDoc.load(url);
	if(loaded){
		createAgentDropDown(xmlDoc);
	}
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the Agent version.
 */
function importAgentXML(){
	var xmlDoc = createXMLDoc();
	xmlDoc.async = false;
	var agentParam = null;
	var agentParamArray = new Array();
	//Makes sure we take our own id if we're an agent.
	if(Agentlevel){
		agentParam = ref_id;
	}else{
		agentParam = document.mainform.AgentTypes.value;
		if(agentParam == 0){
			agentParamArray = returnOptionsValues(document.mainform.AgentTypes);
		}
	}
	if(agentParamArray.length > 100){
		var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&agentTooLong="+ref_id;
	}else{
		var url = generateURL("admin/reports/transactions/data_daily_liability.jsp?a=", "&a=", agentParamArray, agentParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createSubAgentDropDown(xmlDoc);
	}
}


/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the SubAgent version.
 */
function importSubAgentXML(){
	var xmlDoc = createXMLDoc();
	xmlDoc.async = false;
	var subAgentParam = null;
	var subAgentParamArray = new Array();
	if(validation2 && Isolevel){
		subAgentParam = ref_id;
	}else if(SubAgentlevel){
		subAgentParam = ref_id;
	}
	else{
		subAgentParam = document.mainform.SubAgentTypes.value;
		if(subAgentParam == 0){
			subAgentParamArray = returnOptionsValues(document.mainform.SubAgentTypes);
		}
	}
	if(subAgentParamArray.length > 100){
		if(!validation2 && Isolevel){//You're an iso, and you've got a lot of subagents.  Agent value needs to be passed as well
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&subAgentTooLong="+document.mainform.AgentTypes.value+"&agentTooLong="+ref_id;
		}else if(Agentlevel){//Basically means you're an agent at this point, and you had tons of subagents underneath that are going to error when creating the reps dropdown.
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&subAgentTooLong="+ref_id;
		}
	}else{
		var url = generateURL("admin/reports/transactions/data_daily_liability.jsp?a=", "&a=", subAgentParamArray, subAgentParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createRepDropDown(xmlDoc);
	} else {
                    // display error message
                var errorMsg = null;
                if (xmlDoc.parseError && xmlDoc.parseError.errorCode != 0) {
                    errorMsg = "XML Parsing Error: " + xmlDoc.parseError.reason
                              + " at line " + xmlDoc.parseError.line
                              + " at position " + xmlDoc.parseError.linepos;
                }
                else {
                    if (xmlDoc.documentElement) {
                        if (xmlDoc.documentElement.nodeName == "parsererror") {
                            errorMsg = xmlDoc.documentElement.childNodes[0].nodeValue;
                        }
                    }
                }
                if (errorMsg) {
                    alert (errorMsg);
                }
                else {
                    alert ("There was an error while loading XML file!");
                }
            }
}

/*
 * Alfred A./DBSY-1082 - Creates a new XML object to be loaded with the XML content from a URL
 * to populate the forms on this page.  This is the Rep version.
 */
function importRepXML(){
	var xmlDoc = createXMLDoc();
	xmlDoc.async = false;
	var repParam = null;
	var repParamArray = new Array();
	//Makes sure we take our own id if we're a rep.
	if(RepLevel){
		repParam = ref_id;
	}else{
		repParam = document.mainform.RepTypes.value;
		if(repParam == 0){
			repParamArray = returnOptionsValues(document.mainform.RepTypes);
		}
	}
	//Needs to handle iso, agent, and subagent here.
	if(repParamArray.length > 100){
		if(!validation2 && Isolevel){//You're an iso, and you've got a lot of subagents.  Agent value needs to be passed as well
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+document.mainform.SubAgentTypes.value+"&subAgentTooLong="+document.mainform.AgentTypes.value+"&agentTooLong="+ref_id;
		}else if(Agentlevel){//Basically means you're an agent at this point, and you had tons of subagents underneath that are going to error when creating the reps dropdown.
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+document.mainform.SubAgentTypes.value+"&subAgentTooLong="+ref_id;
		}else if(SubAgentlevel){
			var url = "admin/reports/transactions/data_daily_liability.jsp?a="+"-1"+"&repTooLong="+ref_id;
		}
	}else{
		var url = generateURL("admin/reports/transactions/data_daily_liability.jsp?r=", "&r=", repParamArray, repParam);
	}
	var loaded = xmlDoc.load(url);
	if(loaded){
		createMerchantList(xmlDoc);
	}
}

/*
 * Alfred A./DBSY-1082 - Function that dynamically clear out form locations that
 * no longer need to be filled in anymore.
 */
function clearOptions(levelToClear){
	if(levelToClear == 0){
		var agentDropDown = document.mainform.SubAgentTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
	}else if(levelToClear == 1){
		var agentDropDown2 = document.mainform.RepTypes;
		for(var a = agentDropDown2.options.length;a>=0;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 2){
		var agentDropDown2 = document.mainform.mids;
		for(var a = agentDropDown2.options.length;a>=1;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 3){
		var agentDropDown = document.mainform.RepTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
		var agentDropDown2 = document.mainform.mids;
		for(var a = agentDropDown2.options.length;a>=1;a--){
			agentDropDown2.options[a] = null;
		}
	}else if(levelToClear == 4){
		var agentDropDown = document.mainform.SubAgentTypes;
		for(var a = agentDropDown.options.length;a>=0;a--){
			agentDropDown.options[a] = null;
		}
		var agentDropDown2 = document.mainform.RepTypes;
		for(var a = agentDropDown2.options.length;a>=0;a--){
			agentDropDown2.options[a] = null;
		}
		var agentDropDown3 = document.mainform.mids;
		for(var a = agentDropDown3.options.length;a>=1;a--){
			agentDropDown3.options[a] = null;
		}
	}
}

/*
 * Alfred A./DBSY-1082 - Generic function that takes in a form and XML data,
 * which will then populate that form with the data.
 */
function genericFillIn(formType, x){

	formType.options[0] = new Option(LangAssign, 0);
	var dropDownCounter = 0;
	for (i=0;i<x.length;i++)
	{
		if(ref_id == x[i].getAttribute("val")){
  			continue;
  		}
		var name = x[i].firstChild.nodeValue;
		var value = x[i].getAttribute("val");
  		
  		formType.options[dropDownCounter+1] = new Option(name, value);
  		dropDownCounter++;
	}
}

// Alfred A./DBSY-1082 - These 3 functions merely set up the generic fill-in above.
function createAgentDropDown(xmlDoc){
	genericFillIn(document.mainform.AgentTypes, xmlDoc.getElementsByTagName("display"));
	importAgentXML();
}

function createSubAgentDropDown(xmlDoc){
	clearOptions(4);
	genericFillIn(document.mainform.SubAgentTypes, xmlDoc.getElementsByTagName("display"));
	importSubAgentXML();
}

function createRepDropDown(xmlDoc){
	clearOptions(3);
	genericFillIn(document.mainform.RepTypes, xmlDoc.getElementsByTagName("display"));
	importRepXML();
}

/*
 * Alfred A./DBSY-1082 - Slightly different function that functions like the generic fill in above, 
 * but also gives a hidden attribute a value in case the user has selected the ALL option in Merchants.
 */
function createMerchantList(xmlDoc){
	clearOptions(2);
	var agentDropDown = document.mainform.mids;
	var x = xmlDoc.getElementsByTagName("display");
	var allValues = new Array();
	var dropDownCounter = 0;
	for (i=0;i<x.length;i++)
	{
		if(ref_id == x[i].getAttribute("val")){
  			continue;
  		}
		var name = x[i].firstChild.nodeValue;
		var value = x[i].getAttribute("val");
		allValues[i] = value;
  		
  		agentDropDown.options[dropDownCounter+1] = new Option(name, value);
  		dropDownCounter++;
	}
	
	document.mainform.AllValues.value = allValues.join(":");
}




