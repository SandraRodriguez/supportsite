<%@ page import="com.debisys.utils.DebisysConstants"%>

<%@page import="java.util.StringTokenizer"%>
<%@page import="java.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.debisys.users.SessionData"%><!--usefuldocs.jsp START-->


<jsp:useBean id="Document" class="com.debisys.document.Document" scope="request"/>
<jsp:setProperty name="Document" property="*"/>
<br>
  <TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
              <TBODY>
              <TR>
                <TD align=left width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=images/top_blue.gif>&nbsp;<%=Languages.getString("jsp.includes.useful_docs.title",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD align=right width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_right_blue.gif" width=18></TD></TR>
                  
              <TR>
                <TD colSpan=3>
                  <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
                  bgColor=#fffcdf class="backceldas">
                    <TBODY>
                    <TR>
                      <TD width=1 bgColor=#003082><IMG 
                        src="images/trans.gif" width=1> </TD>
                      <TD vAlign=top align=middle bgColor=#FFFFFF>
                        <TABLE width="100%" border=0 id="categorias"
                        align=center cellPadding=2 cellSpacing=0 class="backceldas"><!--downloadable-->                  
			            <TBODY>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript">
function opcion(obj)
{ 
//alert(document.getElementById('categoryId').options[document.getElementById('categoryId').selectedIndex].value);
var cat=document.getElementById('categoryId').options[document.getElementById('categoryId').selectedIndex].text;
var tex= document.getElementById('categoryId').selectedIndex;
var ind = obj.value;
//var ind=document.getElementById('categoryId').options[document.getElementById('categoryId').selectedIndex].value; 
var url = 'admin/documents/documentUpdateStatus.jsp?documentId=1&action=cat&cat='+cat+'&ind='+ind;
$.post(url);

<%

	
	String a =SessionData.getProperty("categorySelected");
	String index = SessionData.getProperty("indexSelected");
	if(a!=null && !a.equals("") && index!=null && !index.equals("")){
	%>
		document.getElementById('categoryId').selectedIndex="<%=index%>";
	<%}else{
	SessionData.setProperty("categorySelected","");
	SessionData.setProperty("indexSelected","0");
	%>document.getElementById('categoryId').selectedIndex="0";
	<%}
	
%>
//document.getElementById("categorias").refresh();
location.reload(true);
}

</script>

<form name="categories">	
					<input type="hidden" id="" />
	  		  	 	 
<tr>
<td width="18">&nbsp; </td><!--link to downloadable -->
<td valign="top" class="main">
<br><br></td>
<td width="18">&nbsp; </td><!--brief description of downloadable-->
<td valign="top" class="main" align="right"><%=Languages.getString("jsp.admin.document.documentrepository.filterby",SessionData.getLanguage())%>
 		 <%
 		 String optInicial="";
 		 if(SessionData.getLanguage().equals("english")){
	     optInicial= "All";
	     }
	     else{
	     optInicial= "Todos";
	     } %>
	     
					&nbsp;<SELECT NAME="categoryId" id="categoryId" onchange="opcion(this);"><OPTION VALUE="0" onclick="opcion(this);"><%=optInicial%></OPTION>
  	  		  	 	 

					


	
<%

	Vector categories = Document.searchDocumentByCategory(SessionData,application,"");
	Iterator it = categories.iterator();

	  while (it.hasNext()) {
	    Vector vecTemp = null;
	    vecTemp = (Vector)it.next();
	    String categoryId = vecTemp.get(0).toString();
	    String description = vecTemp.get(1).toString();
	    String descriptionSpanish= vecTemp.get(2).toString();
	    String categoryToSelect =""; 
	     if(SessionData.getLanguage().equals("english")){
	     categoryToSelect= description;
	     }
	     else{
	     categoryToSelect=descriptionSpanish;
	     }
	   	if(SessionData.getProperty("categorySelected")!=null && !SessionData.getProperty("categorySelected").equals("") ){
			
			if(SessionData.getProperty("categorySelected").equals(description) || SessionData.getProperty("categorySelected").equals(descriptionSpanish)){
				 out.println("<OPTION VALUE=\"" + categoryId + "\" SELECTED>" +categoryToSelect + "</OPTION>");
			}
			else{
			 	out.println("<OPTION VALUE=\"" + categoryId + "\">" + categoryToSelect + "</OPTION>");
			}
	    } 
	    else{
		     if (categoryId.equals(request.getParameter("categoryId")))
		      out.println("<OPTION VALUE=\"" + categoryId + "\" SELECTED>" + categoryToSelect + "</OPTION>");
		    else
		      out.println("<OPTION VALUE=\"" + categoryId + "\">" + categoryToSelect + "</OPTION>");
		 	 }		
	    }

	   
%>	
	</SELECT>
		&nbsp;
		</td>
</tr>		   
	
	
	<%
	  Iterator newIT =  categories.iterator();
	  	 while (newIT.hasNext()) {
	  		 Vector vecTemp = null;
	  		  vecTemp = (Vector)newIT.next();
	  		   String categoryId = vecTemp.get(0).toString();
	  		   String description = vecTemp.get(1).toString();
			   String descriptionSpanish= vecTemp.get(2).toString();
			   String categoryToSelect =""; 
			   if(SessionData.getLanguage().equals("english")){
			     categoryToSelect= description;
			   }
			   else{
			   	 categoryToSelect=descriptionSpanish;
			   }	  		   
	  		   
	  		    Vector documents=null;
	  		   if(SessionData.getProperty("categorySelected")!=null && !SessionData.getProperty("categorySelected").equals("") ){
	  		     documents = Document.searchDocumentByCategory(SessionData,application,index);
	  		   }
	  		   else{
	  		   	 documents = Document.searchDocumentByCategory(SessionData,application,categoryId);
	  		   }
	  		  
	  		   
	  		   %>
	  		   	 <% if(SessionData.getProperty("categorySelected")!=null && !SessionData.getProperty("categorySelected").equals("") ){ 
  		  	 	    if(SessionData.getProperty("categorySelected").equals(description) || SessionData.getProperty("categorySelected").equals(descriptionSpanish)){%>
  		  	 	    
  		  	 	    <tr>
					<td width="18">&nbsp; </td><!--link to downloadable -->
					<td valign="top" class="main" align="left">
					<B>
	  		  	 	 <%=Languages.getString("jsp.admin.document.documentrepository.category",SessionData.getLanguage())%>:  
	  		  	 	 <%=categoryToSelect.toUpperCase()%></B><br><br></td>
					<td width="18">&nbsp; </td><!--brief description of downloadable-->	 
							
					<!--brief description of downloadable-->
					<td class="main" valign="top" align="left">
							
					</td>
					</tr>
  		  	 	 <%} 
  		  	 	 }
 				 else { 
  		  	 	 %>
  		  	 	    <tr>
					<td width="18">&nbsp; </td><!--link to downloadable -->
					<td valign="top" class="main" align="left">
					<B>
	  		  	 	 <%=Languages.getString("jsp.admin.document.documentrepository.category",SessionData.getLanguage())%>:  
	  		  	 	 <%=categoryToSelect.toUpperCase()%></B><br><br></td>
					<td width="18">&nbsp; </td><!--brief description of downloadable-->	 
						<!--brief description of downloadable-->
						<td class="main" valign="top" align="left">
								
						</td>
						</tr>	  		  	 	 
  		  	 	 <%} %>
	
	  		   <%
	  		   Iterator itdoc = documents.iterator();
	  		  	 while (itdoc.hasNext()) {
	  		  	 	 Vector vecTempDoc = null;
	  		  	 	 vecTempDoc = (Vector)itdoc.next();
	  		  	 	 %>
	  		  	 	 	
	  		  	 	 	
                              
		  		  	 	<% if(SessionData.getProperty("categorySelected")!=null && !SessionData.getProperty("categorySelected").equals("") ){ 
  		  	 	    		if(SessionData.getProperty("indexSelected").equals(categoryId)){%> 
  		  	 	    		

							<tr>
							<td width="18">&nbsp; </td><!--link to downloadable -->
							<td valign="top" class="main" align="left">
							<a class="textprin" href="admin/documents/documentDownload.jsp?idfile=<%=vecTempDoc.get(0).toString()%>" target="_blank" onClick="window.open(this.href, this.target, 'width=300,height=400'); return false;" onmouseover="status='';return true"><%= vecTempDoc.get(1).toString()%> </a><br><br></td>
							<td width="18">&nbsp; </td><!--brief description of downloadable-->	  	 	    		 	
  		  	 	    		<% 
			  		  	 	 }
			  		  	 	 }
			 				 else { 
			  		  	 	 %>
							<tr>
							<td width="18">&nbsp; </td><!--link to downloadable -->
							<td valign="top" class="main" align="left">
							<a class="textprin" href="admin/documents/documentDownload.jsp?idfile=<%=vecTempDoc.get(0).toString()%>" target="_blank" onClick="window.open(this.href, this.target, 'width=300,height=400'); return false;" onmouseover="status='';return true"><%= vecTempDoc.get(1).toString()%> </a><br><br></td>
							<td width="18">&nbsp; </td><!--brief description of downloadable-->	  	 	    		 	
		  		  	 		 <%} %>
		  		  	 		    


						<!--brief description of downloadable-->
						
						<% if(SessionData.getProperty("categorySelected")!=null && !SessionData.getProperty("categorySelected").equals("") ){ 
  		  	 	    		if(SessionData.getProperty("indexSelected").equals(categoryId)){%> 
  		  	 	    		<td class="main" valign="top" align="left">
  		  	 	    			<%= vecTempDoc.get(2).toString()%>
  		  	 	    		</td>
							</tr>
  		  	 	    		<% 
			  		  	 	 }
			  		  	 	 }
			 				 else { 
			 				 %>
			 				 <td class="main" valign="top" align="left">
			  		  	 	 <%= vecTempDoc.get(2).toString()%>
			  		  	 	 </td>
							</tr>
			  		  	 	 <%} %>
			  		  	 	 	

					<%
	  		  	 	 }
	  		  	 %><%
	  		   
	  		   
	  	}	   
	  		   
  		 
	 %>
</form>
</table>

</td>
<td width="1" bgcolor="#003082">
<img src="images/trans.gif" width="1">
</td>
</tr>
<tr>
<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
</tr>
</table>
</td>
</table>
<!--usefuldocs.jsp END-->
