function SortROCRows(iTab, iSortTyps, iSortClm, iSortTypClm, iAsync, row1a, row2a) {
	this.sortColumn = iSortClm;
	this.descending = iSortTypClm;
	this.bAsync = iAsync;
	this.element = iTab;
	this.tHead = iTab.tHead;
	this.tBody = iTab.tBodies[0];
	this.row1 = row1a;
	this.row2 = row2a;
	this.document = iTab.ownerDocument || iTab.document;
	var oThis = this;
	this._headerOnclick = function(e) {
		oThis.headerOnclick(e);
	};
	// only for IE
	var win = this.document.defaultView || this.document.parentWindow;
	this._onunload = function() {
		oThis.destroy();
	};
	if (win && typeof win.attachEvent != "undefined") {
		win.attachEvent("onunload", this._onunload);
	}
	this.initHeader(iSortTyps || []);
}

SortROCRows.gecko = navigator.product == "Gecko";
SortROCRows.msie = /msie/i.test(navigator.userAgent);
SortROCRows.removeBeforeSort = SortROCRows.gecko;
SortROCRows.prototype.onsort = function() {
	var rows = this.tBody.rows;
	var l = rows.length;
	var rowEl;
	var cellEl;
	for ( var i = 0; i < l; i++) {
		recreateAltColor(rows[i], i % 2 ? this.row2 : this.row1);
		rowEl = rows[i];
		cellEl = rowEl.cells[0];
		while (cellEl.lastChild != null) {
			cellEl.removeChild(cellEl.lastChild);
		}
		cellEl.appendChild(document.createTextNode(i + 1));
	}
};

function recreateAltColor(el, iClass) {
	el.className = iClass;
}

// default false - asce, true - desc
SortROCRows.prototype.defaultDescending = false;
SortROCRows.prototype._sortTypeInfo = {};

SortROCRows.prototype.initHeader = function(iSortTyps) {
	var cells = this.tHead.rows[0].cells;
	var l = cells.length;
	var img, c;
	for ( var i = 0; i < l; i++) {
		c = cells[i];
		if (iSortTyps[i] != null && iSortTyps[i] != "None") {
			img = this.document.createElement("IMG");
			img.src = "/support/images/trans.gif";
			c.appendChild(img);
			if (iSortTyps[i] != null)
				c._sortType = iSortTyps[i];
			if (typeof c.addEventListener != "undefined")
				c.addEventListener("click", this._headerOnclick, false);
			else if (typeof c.attachEvent != "undefined")
				c.attachEvent("onclick", this._headerOnclick);
			else
				c.onclick = this._headerOnclick;
		} else {
			c.setAttribute("_sortType", iSortTyps[i]);
			c._sortType = "None";
		}
	}
	this.updateArrows();
};

// remove arrows and events
SortROCRows.prototype.uninitHeader = function() {
	var cells = this.tHead.rows[0].cells;
	var l = cells.length;
	var c;
	for ( var i = 0; i < l; i++) {
		c = cells[i];
		if (c._sortType != null && c._sortType != "None") {
			c.removeChild(c.lastChild);
			if (typeof c.removeEventListener != "undefined")
				c.removeEventListener("click", this._headerOnclick, false);
			else if (typeof c.detachEvent != "undefined")
				c.detachEvent("onclick", this._headerOnclick);
			c._sortType = null;
			c.removeAttribute("_sortType");
		}
	}
};

SortROCRows.prototype.updateArrows = function() {
	var cells = this.tHead.rows[0].cells;
	var l = cells.length;
	var img;
	for ( var i = 0; i < l; i++) {
		if (cells[i]._sortType != null && cells[i]._sortType != "None") {
			img = cells[i].lastChild;
			if (i == this.sortColumn)
				img.className = "sort-arrow "
						+ (this.descending ? "descending" : "ascending");
			else
				img.className = "sort-arrow";
		}
	}
};

SortROCRows.prototype.headerOnclick = function(e) {
	// find TD element
	var el = e.target || e.srcElement;
	while (el.tagName != "TD")
		el = el.parentNode;

	this.asyncSort(SortROCRows.msie ? SortROCRows.getCellIndex(el) : el.cellIndex);
};
// IE returns wrong cellIndex when columns are hidden
SortROCRows.getCellIndex = function(oTd) {
	var cells = oTd.parentNode.childNodes
	var l = cells.length;
	var i;
	for (i = 0; cells[i] != oTd && i < l; i++)
		;
	return i;
};

SortROCRows.prototype.getSortType = function(nColumn) {
	var cell = this.tHead.rows[0].cells[nColumn];
	var val = cell._sortType;
	if (val != "")
		return val;
	return "String";
};

// only nColumn is required
// if bDescending is left out the old value is taken into account
// if sSortType is left out the sort type is found from the sortTypes array

SortROCRows.prototype.sort = function(nColumn, bDescending, sSortType) {
	if (sSortType == null)
		sSortType = this.getSortType(nColumn);

	// exit if None
	if (sSortType == "None")
		return;

	if (bDescending == null) {
		if (this.sortColumn != nColumn)
			this.descending = this.defaultDescending;
		else
			this.descending = !this.descending;
	} else
		this.descending = bDescending;

	this.sortColumn = nColumn;

	if (typeof this.onbeforesort == "function")
		this.onbeforesort();

	var f = this.getSortFunction(sSortType, nColumn);
	var a = this.getCache(sSortType, nColumn);
	var tBody = this.tBody;

	a.sort(f);

	if (this.descending)
		a.reverse();

	if (SortROCRows.removeBeforeSort) {
		// remove from doc
		var nextSibling = tBody.nextSibling;
		var p = tBody.parentNode;
		p.removeChild(tBody);
	}

	// insert in the new order
	var l = a.length;
	for ( var i = 0; i < l; i++)
		tBody.appendChild(a[i].element);

	if (SortROCRows.removeBeforeSort) {
		// insert into doc
		p.insertBefore(tBody, nextSibling);
	}

	this.updateArrows();

	this.destroyCache(a);

	if (typeof this.onsort == "function")
		this.onsort();
};

SortROCRows.prototype.asyncSort = function(nColumn, bDescending, sSortType) {
	var oThis = this;
	this._asyncsort = function() {
		oThis.sort(nColumn, bDescending, sSortType);
	};
	window.setTimeout(this._asyncsort, 1);
};

SortROCRows.prototype.getCache = function(sType, nColumn) {
	var rows = this.tBody.rows;
	var l = rows.length;
	var a = new Array(l);
	var r;
	for ( var i = 0; i < l; i++) {
		r = rows[i];
		a[i] = {
			value :this.getRowValue(r, sType, nColumn),
			element :r
		};
	}
	;
	return a;
};

SortROCRows.prototype.destroyCache = function(oArray) {
	var l = oArray.length;
	for ( var i = 0; i < l; i++) {
		oArray[i].value = null;
		oArray[i].element = null;
		oArray[i] = null;
	}
};

SortROCRows.prototype.getRowValue = function(oRow, sType, nColumn) {
	// if we have defined a custom getRowValue use that
	if (this._sortTypeInfo[sType] && this._sortTypeInfo[sType].getRowValue)
		return this._sortTypeInfo[sType].getRowValue(oRow, nColumn);

	var s;
	var c = oRow.cells[nColumn];
	if (typeof c.innerText != "undefined")
		s = c.innerText;
	else
		s = SortROCRows.getInnerText(c);
	return this.getValueFromString(s, sType);
};

SortROCRows.getInnerText = function(oNode) {
	var s = "";
	var cs = oNode.childNodes;
	var l = cs.length;
	for ( var i = 0; i < l; i++) {
		switch (cs[i].nodeType) {
		case 1: // ELEMENT_NODE
			s += SortROCRows.getInnerText(cs[i]);
			break;
		case 3: // TEXT_NODE
			s += cs[i].nodeValue;
			break;
		}
	}
	return s;
};

SortROCRows.prototype.getValueFromString = function(sText, sType) {
	if (this._sortTypeInfo[sType])
		return this._sortTypeInfo[sType].getValueFromString(sText);
	return sText;
	/*
	 * switch (sType) { case "Number": return Number(sText); case
	 * "CaseInsensitiveString": return sText.toUpperCase(); case "Date": var
	 * parts = sText.split("-"); var d = new Date(0); d.setFullYear(parts[0]);
	 * d.setDate(parts[2]); d.setMonth(parts[1] - 1); return d.valueOf(); }
	 * return sText;
	 */
};

SortROCRows.prototype.getSortFunction = function(sType, nColumn) {
	if (this._sortTypeInfo[sType])
		return this._sortTypeInfo[sType].compare;
	return SortROCRows.basicCompare;
};

SortROCRows.prototype.destroy = function() {
	this.uninitHeader();
	var win = this.document.parentWindow;
	if (win && typeof win.detachEvent != "undefined") { // only IE needs this
		win.detachEvent("onunload", this._onunload);
	}
	this._onunload = null;
	this.element = null;
	this.tHead = null;
	this.tBody = null;
	this.document = null;
	this._headerOnclick = null;
	this.sortTypes = null;
	this._asyncsort = null;
	this.onsort = null;
};

// Adds a sort type to all instance of SortROC
// sType : String - the identifier of the sort type
// fGetValueFromString : function ( s : string ) : T - A function that takes a
// string and casts it to a desired format. If left out the string is just
// returned
// fCompareFunction : function ( n1 : T, n2 : T ) : Number - A normal JS sort
// compare function. Takes two values and compares them. If left out less than,
// <, compare is used
// fGetRowValue : function( oRow : HTMLTRElement, nColumn : int ) : T - A
// function
// that takes the row and the column index and returns the value used to
// compare.
// If left out then the innerText is first taken for the cell and then the
// fGetValueFromString is used to convert that string the desired value and type

SortROCRows.prototype.addSortType = function(sType, fGetValueFromString,
		fCompareFunction, fGetRowValue) {
	this._sortTypeInfo[sType] = {
		type :sType,
		getValueFromString :fGetValueFromString || SortROCRows.idFunction,
		compare :fCompareFunction || SortROCRows.basicCompare,
		getRowValue :fGetRowValue
	};
};

// this removes the sort type from all instances of SortROC
SortROCRows.prototype.removeSortType = function(sType) {
	delete this._sortTypeInfo[sType];
};

SortROCRows.basicCompare = function compare(n1, n2) {
	if (n1.value < n2.value)
		return -1;
	if (n2.value < n1.value)
		return 1;
	return 0;
};

SortROCRows.idFunction = function(x) {
	return x;
};

SortROCRows.toUpperCase = function(s) {
	return s.toUpperCase();
};

SortROCRows.toDate = function(s) {

	var d = new Date();
	if (s != "") {
		var parts = s.split(" ");
		var sDate = parts[0];
		var sTime = parts[1];
		var sAmPm = parts[2];
		// first work on the date
		// 09/08/2005
		parts = sDate.split("/")
		d.setMonth(parseInt(parts[0], 10) - 1);
		d.setDate(parseInt(parts[1], 10));
		d.setFullYear(parts[2]);

		if (sTime != null) {
			// now the time
			// 10:30
			// convert it to military
			parts = sTime.split(":")
			var sHour = parseInt(parts[0], 10);

			if (sAmPm == "PM") {
				if (sHour != 12) {
					sHour = sHour + 12
				}
			} else if (sAmPm == "AM") {
				if (sHour == 12) {
					sHour = 0;
				}
			}

			d.setHours(parseInt(sHour, 10));
			d.setMinutes(parseInt(parts[1], 10));
			d.setSeconds(0);
		}

	} else {
		d.setMonth(1);
		d.setDate(1);
		d.setFullYear(1900);

	}
	return d.valueOf();
};
SortROCRows.toNumber = function(s) {
	var temp = replaceComa(s);
	//temp = temp.replace("$", "");
	temp = temp.replace(/[^\d\.-]/g, ""); // FB.2008-11-06 Regular expression added to remove currency format from strings
	temp = temp.replace("%", "");
	if (isNaN(temp)) {
		temp = 0;
	}

	return Number(temp);
}

function replaceComa(str) {
	var split = str.split(",");
	var result = split.join("");
	return result;
}

function isEmpty(val) {
    var strRE = /^[\s ]*$/gi; //this one fails val.match(/^s+$/);
    if (strRE.test(val) || val == '') {
        return true;
    } else {
        return false;
    }   
}

// add sort types
SortROCRows.prototype.addSortType("CaseInsensitiveString", SortROCRows.toUpperCase);
SortROCRows.prototype.addSortType("Date", SortROCRows.toDate);
SortROCRows.prototype.addSortType("Number", SortROCRows.toNumber);
SortROCRows.prototype.addSortType("String");
