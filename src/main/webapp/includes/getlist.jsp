<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport" %>
<%
String action = request.getParameter("action");
String list = request.getParameter("list");
String id = request.getParameter("id");
String output = "";
if(action.equals("agent")){
	output = TransactionReport.getSubAgentList(id,list) ;
}
else if(action.equals("subagent")){
	output = TransactionReport.getRepList(list);
}
else if(action.equals("reps")){
	output = TransactionReport.getMerchantsList(list);
}
else if(action.equals("ratePlans")){
	output = Merchant.getRatePlansByRepIds(list);
}
else if(action.equals("ratePlansFromMerchants")){
	output = Merchant.getRatePlansByMerchants(list);
}

if(output=="")
 output="NOMATCH";
out.print(output);
%>