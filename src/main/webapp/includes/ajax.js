function onChangeTerminalType()
{
  var terminalOf = $("#terminalOf").val();
  var terminalType =  $("#terminalType");
  terminalType.empty();
  
  if(terminalOf == '1')
  {
    $("#trMonthlyFee").show();
    if ( document.getElementById("terminalClass") != null )
    {
      document.getElementById("terminalClass").value = "Debisys";
    }
  }
  else
  {
    $("#trMonthlyFee").hide();
    if ( document.getElementById("terminalClass") != null )
    {
      document.getElementById("terminalClass").value = "QComm";
    }
  }
  
  $.getJSON("/support/includes/ajax.jsp?class=com.debisys.terminals.Terminal&method=getTerminalTypesAjax&value="+terminalOf,
          function(data)
          {
            $.each(data.items, function(i,item)
            {
              var option = $("<option>");
              if(item.terminal_type == '')
              {
               option.attr("selected", "selected");
              }
              
              option.text(item.description);
              option.val(item.terminal_type);
              option.appendTo(terminalType);
            });
          });
  //$("#divMonthly").show();
  terminalType_onclick(document.getElementById("terminalType"));
}

function onBrandChange()
{
  var cBrand = $("#ddlBrand").val();
  var cModel =  $("#ddlModel");
  cModel.empty();
  
  
  $.getJSON("/support/includes/ajax.jsp?class=com.debisys.terminals.Terminal&method=getPhysicalTerminalModels&value=" + cBrand,
          function(data)
          {
            $.each(data.items, function(i,item)
            {
              var option = $("<option>");
              if(item.model == '')
              {alert(item.name);
               option.attr("selected", "selected");
              }
              
              option.text(item.name);
              option.val(item.model);
              option.appendTo(cModel);
            });
            document.getElementById("ddlModel").value = "-1";
            
          });
}


function onCountryChange()
{
  var cCountrySel = $("#countries").val();
  var cModel =  $("#ddlModel");
  cModel.empty();
  $.getJSON("/support/includes/ajax.jsp?class=com.debisys.tools.CrossBorderExchangeRate&method=getDestinationCurrency&value=" + cCountrySel,
          function(data)
          {
              var option = $("<option>");
              if(data.currency == '')
              {
               option.attr("selected", "selected");
              }
              
              option.text(data.currency);
              option.val(data.currency);
              option.appendTo(cModel);
             });
 }