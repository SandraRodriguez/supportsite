function doClick(buttonName, e) {
	// the purpose of this function is to allow the enter key to
	// point to the correct button to click.
	var key;

	if (window.event)
		key = window.event.keyCode; // IE
	else
		key = e.which; // firefox

	if (key == 13) {
		// Get the button the user wants to have clicked
		var btn = document.getElementById(buttonName);
		if (btn != null) { // If we find the button click it
			btn.click();
			event.keyCode = 0
		}
	}
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

function hov(loc,cls) {   
 if(loc.className) {  
    loc.className=cls;   
    }
 }   

function isoddeven(number){
			if (number === 0)
			{
			return 1;
			}
			else if (number%2)
			{
			return 2;
			}
			else
			{
			return 1;
			}
}

function GetXmlHttpObject()
{
		var xmlHttp=null;
		try
		 {
		 // Firefox, Opera 8.0+, Safari
		 xmlHttp=new XMLHttpRequest();
		 }
		catch (e)
		 {
		 //Internet Explorer
		 try
		  {
		  xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		  }
		 catch (e)
		  {
		  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 }
		return xmlHttp;
}
function mainsalesman(action,isoid,sid,sn,oldsid,oldsn)
{ 	
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp===null)
			 {
				 alert ("Browser does not support HTTP Request");
				 return ;
			 }
			//Add salesman
			var url="includes/s2k/s2ksalesmanfunctions.jsp";	
				url = url + "?v=" + Math.random()*Math.random() +"&t=" + Math.random()*Math.random() 
				+ "&action="+ action +"&isoid="+ isoid + "&sid="+sid + "&sn="+sn;
			if(action.indexOf("edit")>=0){
				url = url + "&oldsid=" +  oldsid + "&oldsn=" + oldsn;
				xmlHttp.onreadystatechange=editrowresponseback ; 
			}
			else if(action.indexOf("add")>=0){
			xmlHttp.onreadystatechange=addrowresponseback ; 
			}
			else if(action.indexOf("delete")>=0){
			xmlHttp.onreadystatechange=deleterowresponseback;
			}
			xmlHttp.open("GET",url,true);
			xmlHttp.send(null);
}


function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
 }
 
 function cancel(){

 			var ISOID = document.getElementById("ISOID").value;
 			var cancel_button = document.getElementById('cancelbutton');
  			cancel_button.style.visibility = 'hidden'; 
 			document.getElementById("addbutton").value =document.getElementById("add").value;
			document.getElementById("addbutton").onclick = function(){addRow(ISOID);};
			document.getElementById("sid").value ="";
			document.getElementById("sn").value = "";
 }
 function editRow(isoId,sid, sn){
		newsid = document.getElementById("sid").value;
		newsn = document.getElementById("sn").value;
		
			if(newsid == sid && newsn == sn  ){
				var cancel_button = document.getElementById('cancelbutton');
  				cancel_button.style.visibility = 'hidden'; 
				document.getElementById("addbutton").value =document.getElementById("add").value;
				document.getElementById("addbutton").onclick = function(){addRow(isoId);};
				document.getElementById("sid").value ="";
				document.getElementById("sn").value = "";
				return false; 
			}
			else if( newsid === null || newsid === ""  )
			{
				alert ("Salesman ID must be filled");
				return false; 
			}
			else if(  newsn === null || newsn === ""  )
			{
				alert ("Salesman Name must be filled");
				return false; 
			}
			mainsalesman('edit',isoId,newsid, newsn,sid,sn);
			return true;
		}
	function addRow(isoId) {
		var sid = document.getElementById("sid").value;
		var sn = document.getElementById("sn").value;
			if( sid === null || sid === ""  )
			{
				alert ("Salesman ID must be filled");
				return false; 
			}
			else if(  sn === null || sn === ""  )
			{
				alert ("Salesman Name must be filled");
				return false; 
			}
			mainsalesman('add',isoId,sid, trim(sn),null,null);
			return true;
		}
	
	function edit(isoId,sid,sn) {
			var cancel_button = document.getElementById('cancelbutton');
  			cancel_button.style.visibility = 'visible'; 
			document.getElementById("addbutton").value =document.getElementById("editbutton").value;
			document.getElementById("addbutton").onclick = function(){editRow( isoId,sid,sn,false )};
			document.getElementById("sid").value = sid;
			document.getElementById("sn").value = sn;
	}
	
	function deletesalesman(isoId,sid,sn) {
			if( document.getElementById("addbutton").value == document.getElementById("editbutton").value){
				alert("Deleting is not allowed while editing. Please click on save or cancel first!");
				return false;
			}
			mainsalesman('delete',isoId,sid,sn,null,null);
	}
	
	function deleterowresponseback(){
		if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 
	 
	 var response =  xmlHttp.responseText ; 
	 if( response.indexOf("Malformed URL Exception")>=0 ){
				alert ("Malformed URL Exception, Please contact customer support.");
			}
			else if(response.indexOf("Error")>=0){
				alert ("Unknown database error, Please contact customer support.");
			}else if(response.indexOf("SUCCESS")>=0){
			
			
			if(response.indexOf("//")>=0)
			{
				try {  
			             var table = document.getElementById("dataTable");
			          	 var mySplitResult = response.split("//");
						 var oldsid = mySplitResult[1];  
			             var rowCount = table.rows.length;  
			             var x  = 0;
			             for(var i=0; i<rowCount; i++) {  
			
			                 var row = table.rows[i]; 
			                 if(x == 1){
							 row.className="rowwrap" + isoddeven(i+1);
							 } 
			                 var tsid = row.cells[0].innerHTML;   
				
			                 if(tsid.toString() == oldsid.toString() ) {  
									table.deleteRow(i);
									x = 1;
									i--;
									rowCount--;
								}  
											   }  
			          		}catch(e) {  
			                 alert(e);  
			           } 
			}
			else {
			alert("Error while deleting row! Please refresh page, and try again.")
			}
			
			}
	 }
	}

	function editrowresponseback(){
		if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 
	 var response =  xmlHttp.responseText ; 
			
			if( response.indexOf("Malformed URL Exception")>=0 ){
				alert ("Malformed URL Exception, Please contact customer support.");
			}
			else if(response.indexOf("Error")>=0){
				alert ("Unknown database error, Please contact customer support.");
			}
			else if(response.indexOf("SALESMAN ID EXISTS")>=0){
				alert ("Error, Salesman ID already exists.");
			} 
			else if(response.indexOf("SUCCESS")>=0){
			var description = "";
			
			var newsid = document.getElementById("sid").value;
			var sn = document.getElementById("sn").value;
			var ISOID = document.getElementById("ISOID").value;
			
			if(response.indexOf("//")>=0){
			var mySplitResult = response.split("//");
			oldsid = mySplitResult[1];
			}
			
			 try {  

             var table = document.getElementById("dataTable");  

             var rowCount = table.rows.length;  
             
             for(var i=0; i<rowCount; i++) {  

                 var row = table.rows[i];  
				
                 var tsid = row.cells[0].innerHTML;   

                 if(tsid.toString() == oldsid.toString() ) {  
					row.cells[0].innerHTML = newsid;
					row.cells[1].innerHTML = sn;
					row.cells[2].innerHTML = "<img src=\"images/icon_edit.gif\" onclick=\"edit('" 
					+  ISOID + "','" + newsid + "','" + sn + "')\"  border=0 alt=\"Edit\" title=\"Edit\" />";
					row.cells[3].innerHTML = "<img src=\"images/icon_delete.gif\" onclick=\"deletesalesman('" 
					+  ISOID + "','" + newsid + "','" + sn + "')\"  border=0 alt=\"Delete\" title=\"Delete\" />";
					}  
             }  
          }catch(e) {  
                 alert(e);  
           }  
           		var cancel_button = document.getElementById('cancelbutton');
  				cancel_button.style.visibility = 'hidden';
				document.getElementById("addbutton").value =document.getElementById("add").value;
				document.getElementById("addbutton").onclick = function(){addRow(ISOID);};
				document.getElementById("sid").value ="";
				document.getElementById("sn").value = "";
	 }
	 
	 }
	 }

function addrowresponseback(){
		if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 
			var response =  xmlHttp.responseText ; 
			
			if( response.indexOf("Malformed URL Exception")>=0 ){
				alert ("Malformed URL Exception, Please contact customer support.");
			}
			else if(response.indexOf("Error")>=0){
				alert ("Unknown database error, Please contact customer support.");
			}
			else if(response.indexOf("SALESMAN ID EXISTS")>=0){
				alert ("Error, Salesman ID already exists.");
			} 
			else if(response.indexOf("SUCCESS")>=0){
				
			var table = document.getElementById("dataTable");
			
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			
			row.className="rowwrap" + isoddeven(rowCount+1);
			var cell1 = row.insertCell(0);
			
			var sid = document.getElementById("sid").value;
			var sn = document.getElementById("sn").value;
			var ISOID = document.getElementById("ISOID").value;
			
			var element1 = document.createTextNode ( sid );
			cell1.appendChild(element1);
			
			
			var cell2 = row.insertCell(1);
			var element2 = document.createTextNode ( sn );
			cell2.appendChild(element2);
			
			
			
			var cell3 = row.insertCell(2);
			var element3 = document.createElement("img");
			element3.src = "images/icon_edit.gif";
			element3.alt = "Edit";
			element3.title = "Edit";
			cell3.className = "tablebutton";
			cell3.style.textalign="center";
			element3.onclick = function() {edit(ISOID,sid,sn);};
			cell3.appendChild(element3);
			
			var cell4 = row.insertCell(3);
			var element4 = document.createElement("img");
			element4.src = "images/icon_delete.gif";
			cell4.className = "tablebutton";
			element4.alt = "Delete";
			element4.title = "Delete";
			element4.onclick = function() {deletesalesman(ISOID,sid,sn);} ;
			cell4.appendChild(element4);
			cell4.style.textalign="center";
			
			document.getElementById("sid").value ="";
			document.getElementById("sn").value="";
			}
		}
}
