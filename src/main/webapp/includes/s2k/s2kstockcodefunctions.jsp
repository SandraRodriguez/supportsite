<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.tools.s2k" %>
<%
	String action = request.getParameter("action");
	String isoid = request.getParameter("isoid");
	String output ="";
	if (action.equals("addstockcode")) {
		String productid = request.getParameter("productid");
		String stockcode = request.getParameter("stockcode");
		boolean exTax = false;
		String excludeTax = request.getParameter("excludeTax");
		if (excludeTax != null && !excludeTax.equals("")) {
			exTax = Boolean.parseBoolean(excludeTax);
		}
		int result = new s2k().addstoccode(isoid, productid, stockcode, exTax);
		if (result == 3) {
			output = "PID NOT FOUND";
		} else if (result == 4) {
			output = "PID EXISTS";
		} else if (result == 5) {
			String description = new s2k().getdescription(productid);
			if (description != null) {
				output = "SUCCESS//" + description;
			} else
				output = "SUCCESS";

		} else {
			output = "Error";
		}
	} else if (action.equals("editstockcode")) {
		String productid = request.getParameter("productid");
		String stockcode = request.getParameter("stockcode");
		String pid = request.getParameter("oldpid");
		String sc = request.getParameter("oldsc");
		boolean exTax = false;
		String excludeTax = request.getParameter("excludeTax");
		if (excludeTax != null && !excludeTax.equals("")) {
			exTax = Boolean.parseBoolean(excludeTax);
		}
		int result = new s2k().editstoccode(isoid, productid, stockcode, pid, sc, exTax);
		if (result == 3) {
			output = "PID NOT FOUND";
		} else if (result == 4) {
			output = "PID EXISTS";
		} else if (result == 5) {
			if (pid != productid) {
				String description = new s2k().getdescription(productid);
				if (description != null)
					output = "SUCCESS//" + pid + "//" + description;
				else
					output = "SUCCESS//" + pid;
			} else
				output = "SUCCESS//" + pid;
		} else {
			output = "Error";
		}
	} else if (action.equals("deletestockcode")) {
		String productid = request.getParameter("productid");
		String stockcode = request.getParameter("stockcode");
		int result = new s2k().deletestockcode(isoid, productid, stockcode);
		if (result == 5) {
			output = "SUCCESS//" + productid;
		} else {
			output = "Error";
		}
	}

	if (output == "")
		output = "Malformed URL Exception";
	out.print(output);
%>