function trim(stringToTrim) {
	return stringToTrim.replace(/^\s\s*/, '').replace(/\s\s*$/, '');                                                                                                                                                       
}
function unselectall(){
try{

var editbutton = document.getElementById('editbutton');
var deletebutton = document.getElementById('deletebutton');
editbutton.disabled = true;   	
deletebutton.disabled = true; 
var odd=2;
var table = document.getElementById('dataTable1');
var rowCount = table.rows.length;
for(var i=0;i<rowCount;i++){
	 var Class = table.rows[i].className;
	 if(Class == "rowselected") {
	 	table.rows[i].className = "row" + odd;
	 }
	 if(odd==1)
	 	odd = 2;
	 else
	 	odd=1;
}
}catch(e) {  
              alert(e);  
            } 
}


function removeatts() {
try{
var table = document.getElementById('dataTable2');
var rowCount = table.rows.length;
for(var i=1;i<rowCount;i++){
	 var cell = table.rows[i].cells[1];
	 cell.innerHTML ="";
}
}catch(e) {  
              alert(e);  
            } 
 }

function GetXmlHttpObject()
{
		var xmlHttp=null;
		try
		 {
		 // Firefox, Opera 8.0+, Safari
		 xmlHttp=new XMLHttpRequest();
		 }
		catch (e)
		 {
		 //Internet Explorer
		 try
		  {
		  xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		  }
		 catch (f)
		  {
		  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 }
		return xmlHttp;
}

function loadallatts() {

 if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
 { 
	 try{
	 var response =  xmlHttp.responseText ; 
	 
	 if(response.indexOf("ERROR")>=0){
				alert("Unknown database error, Please contact customer support.");
	 }else
	 {
			//successful
			var result = response.split('=/=');
			var table = document.getElementById('dataTable2');
			var rowCount = table.rows.length;
			for(var i=1;i<rowCount;i++){
			var cell = table.rows[i].cells[1];
	 		cell.innerHTML = result[i-1];
			}
	  }

	  }catch(e) {  
              alert(e);  
            } 
 }
 
}

function loadatts(jobid,accesslevel,isoid) {
 xmlHttp=GetXmlHttpObject();
			if (xmlHttp===null)
			 {
				 alert("Browser does not support HTTP Request");
				 return ;
			 }
			var url="includes/s2k/s2kattfunctions.jsp";
			url = url + "?action=loadv=" + Math.random()*Math.random() +"&t=" + Math.random()*Math.random()+ "&jobid="+ jobid + "&isoid="+ isoid + "&accesslevel="+ accesslevel ;
			xmlHttp.onreadystatechange=loadallatts ;
			xmlHttp.open("GET",url,true);
			xmlHttp.send(null);
 }
 

function selected(loc,num) {
try{
var editbutton = document.getElementById('editbutton');
var deletebutton = document.getElementById('deletebutton');

  var thisClass = loc.className;  
	if(thisClass!="rowselected"){
	 unselectall();
     loc.className="rowselected";
     var cell2 = (loc.cells[2].textContent || loc.cells[2].innerText);
     if( cell2 == 'Scheduled')
     	editbutton.disabled = false;   	
     if( cell2 != 'Processing')
     	deletebutton.disabled = false; 
     var jobid = loc.cells[0].innerHTML;
     var accesslevel = document.getElementById('accesslevel').value;
     var isoid = document.getElementById('isoid').value; 
  	 editbutton.onclick = new Function("edit(" + jobid + ")");
  	 deletebutton.onclick = new Function("del(" + jobid + ")");
  	 loadatts(jobid,accesslevel,isoid);
     }
    else{
     removeatts();
     loc.className="rowwraphover" + num; 
     editbutton.disabled = true; 
     deletebutton.disabled = true; 
    }
  }catch(e) {  
              alert(e);  
            } 
 }


function change(loc,num) {  
  var thisClass = loc.className;  
	if(thisClass!="rowselected") {
     loc.className="rowwraphover" + num; 
     }  
 }   
function back(loc,num) { 
  var thisClass = loc.className;  
	if(thisClass!="rowselected"){  
     loc.className="row"+ num;   
     }
 }   
function really_over(src) {
  if (!window.event) return true;
  var event = window.event;
  var from = event.fromElement;
  var to = event.toElement;
  return ( to == src || src.contains(to) ) && !src.contains(from) && src != from;
}

function really_out(src) {
  if (!window.event) return true;
  var event = window.event;
  var from = event.fromElement;
  var to = event.toElement;
  return (src == from || src.contains(from)) && !src.contains(to) && src != to;
}

function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {// If Internet Explorer, return version number
        return true;
    }
    else {
        return false;
    }
}

function add() {

    if (isInternetExplorer()) {
        window.location.href = "addedittasks.jsp";
    }
    else {
        window.location.href = "admin/tools/addedittasks.jsp";
    }
}
function del(jobid) {
    if (isInternetExplorer()) {
        window.location.href = "s2ktaskscheduler.jsp?action=delete&jobid=" + jobid;
    }
    else {
        window.location.href = "admin/tools/s2ktaskscheduler.jsp?action=delete&jobid=" + jobid;
    }
}
function edit(jobid) {

    if (isInternetExplorer()) {
        window.location.href = "addedittasks.jsp?action=edit&jobid=" + jobid;
    }
    else {
        window.location.href = "admin/tools/addedittasks.jsp?action=edit&jobid=" + jobid;
    }

}

function getRatePlans() {

    var repIds = $('#rids').val();
    
    if(repIds.toString().startsWith('all')){
        var currentSeletedValues = $.map($('#rids option'), function (e) {return e.value;}); 
        repIds = currentSeletedValues.toString().replace("all,", "");        
    }
    
    $.ajax({
        type: "POST",
        async: false,
        data: {list: repIds + '', action: 'ratePlans'},
        url: "includes/getlist.jsp",
        success: function (data) {
            var array_data = String($.trim(data)).split("|");
            $('#s2kRatePlans').empty();
            $('#s2kRatePlans').append('<option value="" selected>ALL</option>');

            if (array_data[0] !== 'NOMATCH') {
                for (var i = 0; i < array_data.length; i++) {
                    if (array_data[i].toString().length > 0) {
                        var rateplanData = String($.trim(array_data[i])).split("#");
                        $('#s2kRatePlans').append('<option value="' + rateplanData[0] + '">' + rateplanData[1] + '</option>');
                    }
                }
            }
        }
    });
}

function getRatePlansForMerchants(){
    var merchantsIds = $('#mids').val();
    
    if(merchantsIds.toString().startsWith('all')){
        var currentSeletedValues = $.map($('#mids option'), function (e) {return e.value;}); 
        merchantsIds = currentSeletedValues.toString().replace("all,", "");
    }
    
    $.ajax({
        type: "POST",
        async: false,
        data: {list: merchantsIds + '', action: 'ratePlansFromMerchants'},
        url: "includes/getlist.jsp",
        success: function (data) {
            var array_data = String($.trim(data)).split("|");
            $('#s2kRatePlans').empty();
            $('#s2kRatePlans').append('<option value="" selected>ALL</option>');

            if (array_data[0] !== 'NOMATCH') {
                for (var i = 0; i < array_data.length; i++) {
                    if (array_data[i].toString().length > 0) {
                        var rateplanData = String($.trim(array_data[i])).split("#");
                        $('#s2kRatePlans').append('<option value="' + rateplanData[0] + '">' + rateplanData[1] + '</option>');
                    }
                }
            }
        }
    });
}

