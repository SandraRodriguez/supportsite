function doClick(buttonName, e) {
	// the purpose of this function is to allow the enter key to
	// point to the correct button to click.
	var key;

	if (window.event)
		key = window.event.keyCode; // IE
	else
		key = e.which; // firefox

	if (key == 13) {
		// Get the button the user wants to have clicked
		var btn = document.getElementById(buttonName);
		if (btn != null) { // If we find the button click it
			btn.click();
			event.keyCode = 0
		}
	}
}
    
function clickExcludeTax(){
	var excludeTaxCheck = document.getElementById("excludeTax");
	if(excludeTaxCheck.checked){
		document.getElementById("excludeTaxText").value = "yes";
	}
}
    
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

function hov(loc,cls) {   
 if(loc.className) {  
    loc.className=cls;   
    }
 }   

function isoddeven(number){
			if (number === 0)
			{
			return 1;
			}
			else if (number%2)
			{
			return 2;
			}
			else
			{
			return 1;
			}
}

function GetXmlHttpObject()
{
		var xmlHttp=null;
		try
		 {
		 // Firefox, Opera 8.0+, Safari
		 xmlHttp=new XMLHttpRequest();
		 }
		catch (e)
		 {
		 //Internet Explorer
		 try
		  {
		  xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		  }
		 catch (e)
		  {
		  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 }
		return xmlHttp;
}

function stockcode(action, isoid, productid, sc, oldpid, oldsc, excludeTax) {
	xmlHttp = GetXmlHttpObject();
	if (xmlHttp === null) {
		alert("Browser does not support HTTP Request");
		return;
	}
	// Add stockcode
	var url = "includes/s2k/s2kstockcodefunctions.jsp";
	url = url + "?v=" + Math.random() * Math.random() + "&t=" + Math.random()
			* Math.random() + "&action=" + action + "&isoid=" + isoid
			+ "&productid=" + productid + "&stockcode=" + sc + "&excludeTax=" + excludeTax;
	if (action.indexOf("editstockcode") >= 0) {
		url = url + "&oldpid=" + oldpid + "&oldsc=" + oldsc + "&excludeTax=" + excludeTax;
		xmlHttp.onreadystatechange = editrowresponseback;
	} else if (action.indexOf("addstockcode") >= 0) {
		xmlHttp.onreadystatechange = addrowresponseback;
	} else if (action.indexOf("deletestockcode") >= 0) {
		xmlHttp.onreadystatechange = deleterowresponseback;
	}
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}


function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
 }
 
 function cancel(){

 			var ISOID = document.getElementById("ISOID").value;
 			var cancel_button = document.getElementById('cancelbutton');
  			cancel_button.style.visibility = 'hidden'; 
 			document.getElementById("addbutton").value =document.getElementById("add").value;
			document.getElementById("addbutton").onclick = function(){addRow(ISOID);};
			document.getElementById("pid").value ="";
			document.getElementById("sc").value = "";
 }
 
 function editRow(isoId,pid, sc, excludeTax) {
		newpid = document.getElementById("pid").value;
		newsc = document.getElementById("sc").value;
		var newExcludeTax = document.getElementById("excludeTax").checked;
		
		if(newpid == pid && newsc == sc && newExcludeTax == excludeTax ){
			var cancel_button = document.getElementById('cancelbutton');
			cancel_button.style.visibility = 'hidden'; 
			document.getElementById("addbutton").value =document.getElementById("add").value;
			document.getElementById("addbutton").onclick = function(){addRow(isoId);};
			document.getElementById("pid").value ="";
			document.getElementById("sc").value = "";
			return false; 
		}
		else if( newpid === null || newpid === ""  )
		{
			alert ("Product ID must be filled");
			return false; 
		}
		else if(  newsc === null || newsc === ""  )
		{
			alert ("Stock Code must be filled");
			return false; 
		}
		else if(!IsNumeric(newpid)){
		 	newpid = "";
		 	alert ("Product ID must be numberic");
			return false; 
		 }
		stockcode('editstockcode',isoId,newpid, newsc,pid,sc, newExcludeTax);
		return true;
	}
	
	function addRow(isoId) {
		debugger;
		var pid = document.getElementById("pid").value;
		var sc = document.getElementById("sc").value;
		var excludeTax = document.getElementById("excludeTax").checked;
		if( pid === null || pid === ""  )
		{
			alert ("Product ID must be filled");
			return false; 
		}
		else if(  sc === null || sc === ""  )
		{
			alert ("Stock Code must be filled");
			return false; 
		}
		else if(!IsNumeric(pid)){
		 	document.getElementById("pid").value = "";
		 	alert ("Product ID must be numberic");
			return false; 
		 }
		stockcode('addstockcode',isoId,pid, trim(sc),null,null, excludeTax);
		return true;
	}
	
	function editstockcode(isoId, pid, sc, excludeTax) {
		var cancel_button = document.getElementById('cancelbutton');
		cancel_button.style.visibility = 'visible'; 
		document.getElementById("addbutton").value =document.getElementById("editbutton").value;
		document.getElementById("addbutton").onclick = function(){editRow( isoId,pid,sc, excludeTax )};
		document.getElementById("pid").value = pid;
		document.getElementById("sc").value = sc;
		var isExclude = excludeTax === 'true' ? true : false;
		document.getElementById("excludeTax").checked = isExclude;
	}
	
	function deletestockcode(isoId,pid,sc) {
			if( document.getElementById("addbutton").value == document.getElementById("editbutton").value){
				alert("Deleting is not allowed while editing. Please click on save or cancel first!")
				return false;
			}
			stockcode('deletestockcode',isoId,pid, sc,null,null);
	}

	function deleterowresponseback(){
		if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 
	 
	 var response =  xmlHttp.responseText ; 
	 if( response.indexOf("Malformed URL Exception")>=0 ){
				alert ("Malformed URL Exception, Please contact customer support.");
			}
			else if(response.indexOf("Error")>=0){
				alert ("Unknown database error, Please contact customer support.");
			}else if(response.indexOf("SUCCESS")>=0){
			
			
			if(response.indexOf("//")>=0)
			{
				try {  
			             var table = document.getElementById("dataTable");
			          	 var mySplitResult = response.split("//");
						 var oldpid = mySplitResult[1];  
			             var rowCount = table.rows.length;  
			             var x  = 0;
			             for(var i=0; i<rowCount; i++) {  
			
			                 var row = table.rows[i]; 
			                 if(x == 1){
							 row.className="rowwrap" + isoddeven(i+1);
							 } 
			                 var tpid = row.cells[0].innerHTML;   
				
			                 if(tpid.toString() == oldpid.toString() ) {  
									table.deleteRow(i);
									x = 1;
									i--;
									rowCount--;
								}  
											   }  
			          		}catch(e) {  
			                 alert(e);  
			           } 
			}
			else {
			alert("Error while deleting row! Please refresh page, and try again.")
			}
			
			}
	 }
	}

function editrowresponseback() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		var response = xmlHttp.responseText;

		if (response.indexOf("Malformed URL Exception") >= 0) {
			alert("Malformed URL Exception, Please contact customer support.");
		} else if (response.indexOf("Error") >= 0) {
			alert("Unknown database error, Please contact customer support.");
		} else if (response.indexOf("PID NOT FOUND") >= 0) {
			alert("Error, Product ID is not valid, Please insert an existing product ID.");
		} else if (response.indexOf("PID EXISTS") >= 0) {
			alert("Error, Product ID already exists.");
		} else if (response.indexOf("SUCCESS") >= 0) {
			var description = "";

			var newpid = document.getElementById("pid").value;
			var sc = document.getElementById("sc").value;
			var ISOID = document.getElementById("ISOID").value;
			var excludeTax = document.getElementById("excludeTax").checked;
			var excludeTaxText = document.getElementById("excludeTaxText").value;

			if (response.indexOf("//") >= 0) {
				var mySplitResult = response.split("//");
				oldpid = mySplitResult[1];
				if (mySplitResult.length > 2) {
					description = mySplitResult[2];
				}
			}

			try {

				var table = document.getElementById("dataTable");

				var rowCount = table.rows.length;

				for (var i = 0; i < rowCount; i++) {

					var row = table.rows[i];

					var tpid = row.cells[0].innerHTML;

					if (tpid.toString() == oldpid.toString()) {
						row.cells[0].innerHTML = newpid;
						row.cells[1].innerHTML = description;
						row.cells[2].innerHTML = sc;
						row.cells[3].innerHTML = excludeTaxText;
						row.cells[4].innerHTML = "<img src=\"images/icon_edit.gif\" onclick=\"editstockcode('"
								+ ISOID + "','"	+ newpid + "','" + sc + "', '" + excludeTax + "')\" " 
								+ " border=0 alt=\"Edit\" title=\"Edit\" />";
						row.cells[5].innerHTML = "<img src=\"images/icon_delete.gif\" onclick=\"deletestockcode('"
								+ ISOID	+ "','"
								+ newpid + "','"
								+ sc + "')\"  border=0 alt=\"Delete\" title=\"Delete\" />";
					}
				}
			} catch (e) {
				alert(e);
			}
			var cancel_button = document.getElementById('cancelbutton');
			cancel_button.style.visibility = 'hidden';
			document.getElementById("addbutton").value = document
					.getElementById("add").value;
			document.getElementById("addbutton").onclick = function() {
				addRow(ISOID);
			};
			document.getElementById("pid").value = "";
			document.getElementById("sc").value = "";
		}

	}
}

function addrowresponseback() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		var response = xmlHttp.responseText;

		if (response.indexOf("Malformed URL Exception") >= 0) {
			alert("Malformed URL Exception, Please contact customer support.");
		} else if (response.indexOf("Error") >= 0) {
			alert("Unknown database error, Please contact customer support.");
		} else if (response.indexOf("PID NOT FOUND") >= 0) {
			alert("Error, Product ID is not valid, Please insert an existing product ID.");
		} else if (response.indexOf("PID EXISTS") >= 0) {
			alert("Error, Product ID already exists.");
		} else if (response.indexOf("SUCCESS") >= 0) {
			var description = "";
			if (response.indexOf("//") >= 0) {
				var mySplitResult = response.split("//");
				description = mySplitResult[1];
			}
			var table = document.getElementById("dataTable");

			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);

			row.className = "rowwrap" + isoddeven(rowCount + 1);
			var cell1 = row.insertCell(0);

			var pid = document.getElementById("pid").value;
			var sc = document.getElementById("sc").value;
			var ISOID = document.getElementById("ISOID").value;
			var excludeTax = document.getElementById("excludeTax").checked;
			var excludeTaxText = document.getElementById("excludeTaxText").value;

			var element1 = document.createTextNode(pid);
			cell1.appendChild(element1);

			var cell2 = row.insertCell(1);
			var element2 = document.createTextNode(description);
			cell2.appendChild(element2);

			var cell3 = row.insertCell(2);

			var element3 = document.createTextNode(sc);
			cell3.appendChild(element3);

			var cellTax = row.insertCell(3)
			var elementTax = document.createTextNode(excludeTaxText);
			cellTax.appendChild(elementTax)

			var cell4 = row.insertCell(4);
			var element4 = document.createElement("img");
			element4.src = "images/icon_edit.gif";
			element4.alt = "Edit";
			element4.title = "Edit";
			cell4.className = "tablebutton";
			cell4.style.textalign = "center";
			element4.onclick = function() {
				editstockcode(ISOID, pid, sc, excludeTax.toString());
			};
			cell4.appendChild(element4);

			var cell5 = row.insertCell(5);
			var element5 = document.createElement("img");
			element5.src = "images/icon_delete.gif";
			cell5.className = "tablebutton";
			element5.alt = "Delete";
			element5.title = "Delete";
			element5.onclick = function() {
				deletestockcode(ISOID, pid, sc);
			};
			cell5.appendChild(element5);
			cell5.style.textalign = "center";

			document.getElementById("pid").value = "";
			document.getElementById("sc").value = "";
		}
	}
}