var form = "";
var submitted = false;
var error = false;
var error_message = "";
var valControls=[];

function showMessagesValidations() {
  if (valControls.length>0){
    var element = $("#divMessages");
    var intWidth = $(window).innerWidth();
    var intHeight = $(window).innerHeight();
    var xCenter = (intWidth / 2)+100;
    var yCenter = intHeight / 2;
    element.css('position', 'fixed' );  
    element.css('top', 'auto' );  
    element.css('bottom', yCenter+'px' );  
    element.css('right', '20px' );  
    element.css('left', xCenter+'px' );  
    element.css('background-color', 'white' );                 
    element.css('border-radius', '15px' );  
    element.css('border', 'solid' );  
    element.css("font-size", "11px");
    element.css("opacity", "0.5");
    element.css("filter:", "alpha(opacity=50)");    
  }
}
window.onscroll = showMessagesValidations;
window.onresize =  showMessagesValidations;

function check_input(field_name, field_size, message) {
    
  var isShow = (form.elements[field_name].type !== "hidden");  
  var isDisplay = (form.elements[field_name].style.display !== "none" ||
                    form.elements[field_name].style.display === "");  
  
  if (form.elements[field_name] && (isShow && isDisplay) ){
    var field_value=null;      
    var isSelect = false;
    if($("#"+field_name).is("select")) {
        isSelect = true;
        var selected_option = $('#'+field_name+' option:selected');
        if (selected_option.text().length>0 ){
            field_value = selected_option.val();
        } else{
            field_value='';
        }
    }    
    else if($("#"+field_name).is("input") || $("#"+field_name).is("textarea")) {
        field_value = form.elements[field_name].value;
    }    

    if (field_value === undefined || field_value ===null || field_value === '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      error = true;    
      
      var arr = jQuery.grep(valControls, function( n, i ) {
        return ( n.control === field_name );
      });
      
      if (arr.length===0){
        valControls.push( {'control':field_name, 'messages': [message]});
        if (isSelect){
          $("#"+field_name).after('<span id=span_'+field_name+'>*</span>');
          var spn = $("#span_"+field_name);
            if (spn!==null){
              spn.css("color", "red");   
              spn.css("font-weight", "bold");
              spn.css("font-size", "25px");
            }
        } else{
          $("input[name="+field_name+"]").after('<span id=span_'+field_name+'>*</span>');
          var spn = $("#span_"+field_name);
          if (spn!==null){
            spn.css("color", "red");  
            spn.css("font-weight", "bold");
            spn.css("font-size", "25px");
          }
        }
      } else if (arr.length===1){
        jQuery(valControls).each(function (index){
            if(valControls[index].control === field_name){                
                var addMessage = true;
                $.each(valControls[index].messages, function(i, msg){
                    if (msg===message){
                        addMessage = false;
                    }                    
                });
                if (addMessage){
                    valControls[index].messages.push(message);
                }else{
                    return false;
                }
            }
        });
      }
    } else{        
        jQuery(valControls).each(function (index){
            if(valControls[index].control === field_name){
                
                $.each(valControls[index].messages, function(i, msg){
                    if(message === msg ){
                        valControls[index].messages.splice(i,1); // This will remove the object 
                        return false;
                    }
                });
                if (valControls[index].messages.length===0){
                    valControls.splice(index,1); // This will remove the object 
                    return false; // This will stop the execution of jQuery each loop.
                }
            }
        });
       $("#span_"+field_name).remove();
    }
  } else{
        jQuery(valControls).each(function (index){
            if(valControls[index].control === field_name){
                
                $.each(valControls[index].messages, function(i, msg){
                    if(message === msg){
                        valControls[index].messages.splice(i,1); // This will remove the object 
                        return false;
                    }
                });
                if (valControls[index].messages.length===0){
                    valControls.splice(index,1); // This will remove the object 
                    return false; // This will stop the execution of jQuery each loop.
                }
            }
        });
       $("#span_"+field_name).remove();
  }
}

function showValidationMessages(showMessages){
    var divControl = "divMessages";    
    $("#"+divControl+" ul").remove();
    if (showMessages){        
        $("#"+divControl).append('<ul></ul>');
        $.each(valControls, function(i, item){
            $.each(item.messages, function(i, message){
                $("#"+divControl+" ul").append('<li>'+message+'</li>');    
            });            
        });    
        $("#"+divControl).show();
        var controlFocus = $("#"+valControls[0].control);
        setTimeout(function(){             
            controlFocus.focus(); 
        }, 200);
            
    } else{
        $("#"+divControl).hide();
    }
}
  
/*
function check_input(field_name, field_size, message) {
	if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
		var field_value = form.elements[field_name].value;
		if (field_value == '' || field_value.length < field_size) {
			error_message = error_message + "* " + message + "\n";
			error = true;
		}
	}
}*/

function validate_select_processType(field_name, field_size, message) {
	if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
		var field_value = form.elements[field_name].value;
		if (field_value == '' || field_value == "-1") {
			error_message = error_message + "* " + message + "\n";
			error = true;
		}
	}
}

function verifyAba(lengthText,expression, textInfo){
		
	var optionAbaCk = false;
	var objAba = document.getElementById('optionAba');
	if ( objAba != null ){
	 optionAbaCk = document.getElementById('optionAba').checked;
	}
	
	if( optionAbaCk == true || objAba == null ){
		return true;
	}	
	var valueAba = document.getElementById('routingNumber').value;
	//var isValid = verifyNumericText(valueAba,'/^[0-9]{'+lengthText+'}$/');
	var isValid = verifyNumericText(valueAba,expression);
	var isValidLength = false;
	var isNoAllZero = false;
	var isValidSQL = false;
	
	if ( isValid == true && valueAba.length == 9 ) {
		isValidLength = true;
		isNoAllZero = verifyNoAllZero(valueAba);
		if(isNoAllZero == true){
			isValidSQL = validationSQL(valueAba);
		}
	}
	
	if(isValid == true && isValidLength == true && isNoAllZero == true && isValidSQL == true){		
		var  div_info = document.getElementById("div_info_aba");
		div_info.style.color="black";
		div_info.innerHTML = "";
		return true;
	}else{
		var  div_info = document.getElementById("div_info_aba");
		if ( div_info != null ){
			div_info.style.width="200px";
			div_info.style.color="red";
			div_info.innerHTML = "";			
			var newdiv=document.createElement("div");
			var newtext=document.createTextNode(textInfo);
			newdiv.appendChild(newtext); //append text to new div
			div_info.appendChild(newdiv);
		}
		return false;
	}
}

function verifyAccount(lengthText, textInfo){
	
	var optionAbaCk = false
	var objAccount = document.getElementById('optionAccount');
	if ( objAccount != null ){
		optionAbaCk = document.getElementById('optionAccount').checked;
	}
	if ( optionAbaCk == true || objAccount == null ){
		return true;
	}
	var valueAccount = document.getElementById('accountNumber').value;
	var isValid = verifyNumericText(valueAccount,'/^[0-9]{1,'+lengthText+'}$/');
	var isValidLength = false;
	var isNoAllZero = false;
	if ( isValid == true && valueAccount.length <= 20 ) {
		isValidLength = true;
		isNoAllZero = verifyNoAllZero(valueAccount);
	}
	if(isValid == true && isValidLength == true && isNoAllZero == true){
		var  div_info = document.getElementById("div_info_account");
		div_info.style.color="black";
		div_info.innerHTML = "";
		return true;
	}else{
		var  div_info = document.getElementById("div_info_account");
		if ( div_info!= null){
			div_info.style.width="200px";
			div_info.style.color="red";
			div_info.innerHTML = "";
			var newdiv=document.createElement("div");
			var newtext=document.createTextNode(textInfo);
			newdiv.appendChild(newtext); //append text to new div
			div_info.appendChild(newdiv);
		}
		return false;
	}
}

// EXEC THE REGULAR EXPRESSIONS
function verifyNumericText(text,exp){
	exp = exp.replace("/", "");
	exp = exp.replace("/", "");
	exp = exp.replace(/\\/g, "\\");
	var patt1 = new RegExp(exp);
	var result = patt1.exec(text);
	if (result!= null){
		return true;
	}
	return false;
}

// VERIFY THAT AN TEXT NOT CONTAIN ALL ZEROS 
function verifyNoAllZero(text){
	var i=0;
	for(i = 0; i < text.length ; i++){	
		if(text[i] != '0'){
			return true;
		}
	}
	return false;
}

//VALIDATION FOR ABA MERCHANT WITH FORMAT OF THE DB
function validationSQL(text){
	var digit1 = parseInt(text.charAt(0))*3;
	var digit2 = parseInt(text.charAt(1))*7;
	var digit3 = parseInt(text.charAt(2))*1;
	var digit4 = parseInt(text.charAt(3))*3;
	var digit5 = parseInt(text.charAt(4))*7;
	var digit6 = parseInt(text.charAt(5))*1;
	var digit7 = parseInt(text.charAt(6))*3;
	var digit8 = parseInt(text.charAt(7))*7;
	var digit9 = parseInt(text.charAt(8));
	var sumDigits = digit1+digit2+digit3+ digit4+ digit5+digit6+ digit7+digit8 ; 
	var mod10 = sumDigits % 10;
	var totalOP = 10-mod10;
	var resultDigit = getRightDigits(totalOP,1);
	if(resultDigit == digit9){
		return true;
	}else{
		return false;
	}
}

// GET AN NUMBER DIGITS THE ONE NUMBER
function getRightDigits(num,digits){
	var i=0;
	var numString = num.toString();
	var realNumStr = '';
	for(i = (numString.length-digits); i < numString.length ; i++){
		realNumStr = realNumStr+numString.charAt(i);
	}
	var realNum = parseInt(realNumStr);
	return realNum;
}

function changeCheckAba(){
	var optionAbaCk = document.getElementById('optionAba').checked;
	if(optionAbaCk == true){
		var div_info = document.getElementById("div_info_aba");
		div_info.style.color="black";
		div_info.innerHTML = "";
		var valueAba = document.getElementById('routingNumber');
		valueAba.value = 'NA';
		valueAba.disabled=true;
	}else{
		var valueAba = document.getElementById('routingNumber');
		valueAba.value = '';
		valueAba.disabled=false;
	}
}

function changeCheckAccount(){
	var optionAbaCk = document.getElementById('optionAccount').checked;
	if(optionAbaCk == true){
		var div_info = document.getElementById("div_info_account");
		div_info.style.color="black";
		div_info.innerHTML = "";
		var valueAccount = document.getElementById('accountNumber');
		valueAccount.value = 'NA';
		valueAccount.disabled=true;
	}else{
		var valueAccount = document.getElementById('accountNumber');
		valueAccount.value = '';
		valueAccount.disabled=false;
	}
}

function EnableBillingAddress(bChecked) {
	document.getElementById('mailAddress').readOnly = bChecked;
	document.getElementById('mailColony').readOnly = bChecked;
	document.getElementById('mailDelegation').readOnly = bChecked;
	document.getElementById('mailCity').readOnly = bChecked;
	document.getElementById('mailZip').readOnly = bChecked;
	document.getElementById('mailState').disabled = bChecked;
	document.getElementById('mailCounty').readOnly = bChecked;
	document.getElementById('mailCountry').disabled = bChecked;
	if (bChecked) {
		document.getElementById('mailAddress').value = document.getElementById('physAddress').value;
		document.getElementById('mailColony').value = document.getElementById('physColony').value;
		document.getElementById('mailDelegation').value = document.getElementById('physDelegation').value;
		document.getElementById('mailCity').value = document.getElementById('physCity').value;
		document.getElementById('mailZip').value = document.getElementById('physZip').value;
		document.getElementById('mailState').value = document.getElementById('physState').value;
		document.getElementById('mailCounty').value = document.getElementById('physCounty').value;
		document.getElementById('mailCountry').value = document.getElementById('physCountry').value;
	}else{
		document.getElementById('mailAddress').value = '';
		document.getElementById('mailColony').value = '';
		document.getElementById('mailDelegation').value = '';
		document.getElementById('mailCity').value = '';
		document.getElementById('mailZip').value = '';
		document.getElementById('mailCounty').value = '';
		document.getElementById('mailCountry').value = '';
	}
}//End of function EnableBillingAddress

function showRepSelector(){
	var valueTypeRatePlanModel = $("#typeRatePlanModel").val();
	var optionOpen = "width=700,height=500,screenX=0,screenY=0,top=0,left=0,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes";
	window.open("/support/admin/customers/rep_selector.jsp?search=y&showLabelManagePlanModel="+valueTypeRatePlanModel,"repSelect",optionOpen);
}

function warningModel(isChecked){
	//isChecked,divId,merchantId,model
	var UseRatePlanModelValue = document.getElementById("UseRatePlanModelValue").value;
	var warningModel = document.getElementById("warningModel");
	$("#terminaslOut").show();
	//$("#"+divId).show();	
	//$("#"+divId).load("admin/customers/terminals/terminals_out_rateplan.jsp?merchantId="+merchantId+"&model="+model+"&Random=" + Math.random());
	//alert(isChecked+" "+UseRatePlanModelValue);
	if ( UseRatePlanModelValue==1 && isChecked ){
		$("#terminaslOut").hide();
	}		
	if ( UseRatePlanModelValue==0 && !isChecked ){
		$("#terminaslOut").hide();
	}
}

function TogglePINCache(bValue) {
	if (bValue) {
		document.getElementById("trPINCache").style.visibility = "visible";
	} else {
		document.getElementById("trPINCache").style.visibility = "hidden";
	}
}

function FilterReservedPINCacheBalance(ctl) {
	var sText = ctl.value;
	var sResult = "";
	for (i = 0; i < sText.length; i++) {
		if ((sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57)) {
			sResult += sText.charAt(i);
		}
	}
	if (sResult.length == 0) {
		sResult = "0";
	}
	if (sResult > 100) {
		sResult = 100;
	}
	if (sText != sResult) {
		ctl.value = sResult;
	}
}

function mxSaveContact(text1, text2, text3){
    error = false;
    form = document.formMerchant;
    error_message = text1;
    debugger;
    check_input("contactName", 1, text2);
    check_input("contactPhone", 1, text3);    
    if ( error ){    
        showMessagesValidations();
        showValidationMessages(true);
        //alert(error_message);
        return false;
    }
    document.getElementById('mxContactAction').value = "SAVE";
    document.getElementById('mxBtnSaveContact').disabled = true;
    document.formMerchant.onsubmit = new Function("return true;");
    document.formMerchant.Buttonsubmit.click();
    document.formMerchant.Buttonsubmit.disabled = true;
}

function mxCancelSaveContact(){
	document.getElementById('mxContactAction').value = "CANCEL";
	document.getElementById('mxBtnSaveContact').disabled = true;
	document.getElementById('mxBtnCancelContact').disabled = true;
	document.formMerchant.onsubmit = new Function("return true;");
	document.formMerchant.Buttonsubmit.click();
	document.formMerchant.Buttonsubmit.disabled = true;
}

function changeLabelModelRatePlan(labelModel,typeModel){
	$("#labelUseRatePlanModel").text(labelModel);										
	$("#typeRatePlanModel").val(typeModel);											
}

function showChangeRatePlanWindow(merchantId,repId){
	var leftVal = (800 - screen.width) / 2;
	var topVal = (500 - screen.height) / 2;
	var optionOnClick="width=400,height=400,screenX=10,screenY=10,top="+topVal+",left="+leftVal+",directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes";
	var typeRatePlanModel = $("#typeRatePlanModel").val();
	var url = "/support/admin/customers/terminals/change_model.jsp?merchantId="+merchantId+"&repId="+repId+"&useRatePlan="+typeRatePlanModel;
	wopen(url,"changeModel",600,500);
}

function wopen(url, name, w, h){
	// Fudge factors for window decoration space.
	// In my tests these work well on all platforms & browsers.
	w += 32;
	h += 96;
	wleft = (screen.width - w) / 2;
	wtop = (screen.height - h) / 2;
	// IE5 and other old browsers might allow a window that is
	// partially offscreen or wider than the screen. Fix that.
	// (Newer browsers fix this for us, but let's be thorough.)
	if (wleft < 0) {
		w = screen.width;
		wleft = 0;
	}
	if (wtop < 0) {
		h = screen.height;
		wtop = 0;
	}
	var win = window.open(url, name,
			'width=' + w + ', height=' + h + ', ' +
			'left=' + wleft + ', top=' + wtop + ', ' +
			'location=no, menubar=no, ' +
			'status=no, toolbar=no, scrollbars=no, resizable=no');
	// Just in case width and height are ignored
	win.resizeTo(w, h);
	// Just in case left and top are ignored
	win.moveTo(wleft, wtop);
	win.focus();
}

function mxEditContact(){
	document.getElementById('mxContactAction').value = "EDIT";
	document.formMerchant.onsubmit = new Function("return true;");
	document.formMerchant.Buttonsubmit.disabled = true;	
}

function mxDeleteContact(){
	document.getElementById('mxContactAction').value = "DELETE";
	document.formMerchant.onsubmit = new Function("return true;");
	document.formMerchant.Buttonsubmit.disabled = true;
}

function mxAddContact(){
	document.getElementById('mxContactAction').value = "ADD";
	document.getElementById('mxBtnAddContact').disabled = true;
	document.formMerchant.onsubmit = new Function("return true;");
	document.formMerchant.Buttonsubmit.click();
	document.formMerchant.Buttonsubmit.disabled = true;
}