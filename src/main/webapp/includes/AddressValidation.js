//------------------------------------------------------------------------------
var $xml;
var popAddress;
var validationRequired = false;
var graceDays = 0;

/*INIT Internationalitation DEFAULT VALUES*/
var labelValidatingAddressInformation = "Validating address information. Please wait...";
var labelInvalidInput = "Invalid input";
var labelKeyBad = "Invalid License Key";
var labelAddressNotFound = "Input Address is not found";
var labelInputWarnning1 = "Input Address is DPV confirmed for all components";
var labelInputWarnning2 = "Input Address is found, but not DPV confirmed";
var labelInputWarnning3 = "Input Address Primary number is DPV confirmed. Secondary number is present but not DPV confirmed";
var labelInputWarnning4 = "Input address Primary number is DPV confirmed, Secondary number is missing";
var labelInputWarnning5 = "Canadian address on input. Verified on City level only.";
var labelCurrentAddress = "Your current address is:";
var labelSuggestion1 = "No suggestions found for your address, please check it, fix it and try again";
var labelSuggestion2 = "No additional suggestions found for your address, please check it, fix it and try again";
var labelApply = "Apply";
var labelCancel = "Cancel";
var labelNotConnect = "Can't connect to the address validation service";				
/*END Internationalitation */

function getAddressOption(primary, cityName, county, stateAbbreviation, zipcode, lat, lon, checked){
	return '<div class="address-option"><input name="address-option" type="radio" value="'+primary+'|'+cityName+'|'+county+'|'+stateAbbreviation+'|'+zipcode+'|'+lat+'|'+lon+'"'+(checked? ' checked="cheked"':'')+'>'+primary + ' ' + cityName + ' ' + stateAbbreviation + ' ' + zipcode+'</div>';
}

function validateAddress(urlService, address, city, state, zipcode, applyCallback, cancelCallback){
	var daysOfGrace = true;
	var firm = '';
	$xml = '';
	popAddress.setContent('<div style="padding:50px"><img src="../images/global-wait.gif">'+labelValidatingAddressInformation+'</div>');
	popAddress.show();
	//alert("AddressValidation.js ["+urlService+"]");
	$.ajax({url: urlService+'?action=VerifyAddressAdvanced&FirmOrRecipient='+firm
		+'&PrimaryAddressLine='+address
		+'&SecondaryAddressLine='
		+'&Urbanization='
		+'&CityName='+city
		+'&State='+state
		+'&ZipCode='+zipcode 
		+'&random='+Math.random()*1000000000000000000000,
		success: function(data) {
			//$xml = $($.parseXML(data));
			$xml = $(data);
		},
		async: false
	});
	
	if($xml != ''){
		var $aux = $xml.find("ReturnCode");
		var returnCode = $aux.text();
		var showInfo = true;
		var showMap = true;
		var showSuggestions = true;
		switch(returnCode){
			case '1':
				msg = labelInvalidInput;
				showInfo = false;
				showSuggestions = false;
				break;
			case '2':
				msg = labelKeyBad;
				showInfo = false;
				showSuggestions = false;
				break;
			case '10':
				msg = labelAddressNotFound;
				showInfo = false;
				showSuggestions = false;
				break;
			case '100':
				msg = labelInputWarnning1;
				break;
			case '101':
				msg = labelInputWarnning2;				
				break;
			case '102':
				msg = labelInputWarnning3;
				break;
			case '103':
				msg = labelInputWarnning4;
				break;
			case '200':
				msg = labelInputWarnning5;
				break;
		}
		
		var $aux;
		var addressmap = '';
		var info = '';
		
		$aux = $xml.find("StateAbbreviation");
		var stateAbbreviation = $aux.text();
		
		$aux = $xml.find("CityName");
		var cityName = $aux.text();

		$aux = $xml.find("County");
		var county = $aux.text();
		
		var primary, zipcode, lat, lon;	
	
		if(showInfo){
			$aux = $xml.find("PrimaryDeliveryLine");
			primary = $aux.text();
			$aux = $xml.find("PavResponse > ZipCode");
			zipcode = $aux.text();		
			if(showMap){
				$aux = $xml.find("GeoLocationInfo AvgLatitude");
				lat = ($aux.length > 0) ? $aux.text() : '0';
				$aux = $xml.find("GeoLocationInfo AvgLongitude");
				lon = ($aux.length > 0) ? $aux.text() : '0';
				addressmap = '<div class="address-map" ><img src="http://maps.googleapis.com/maps/api/staticmap?center='+lat+','+lon+'&zoom=12&size=200x280&markers=color:blue%7Clabel:S%7C'+lat+','+lon+'&sensor=false"></div>';
			}
			info = '<strong>'+labelCurrentAddress+'</strong><br>'+getAddressOption(primary, cityName, county, stateAbbreviation, zipcode, lat, lon, true);
		}
		
		var suggestions = labelSuggestion1; //'No suggestions found for your address, please check it, fix it and try again';
		if(showSuggestions){
			$matches = $xml.find("MultipleMatches AddressInfo");
			if($matches.length > 0){
				suggestions = '<strong>Suggestions:</strong><br>';
				for(var i=0; i < $matches.length; i++){
					$aux = $($matches[i]).find("PrimaryHigh");
					primary = $aux.text() + ' ';	
					$aux = $($matches[i]).find("StreetName");
					primary += $aux.text() + ' ';			
					$aux = $($matches[i]).find("Suffix");
					primary += $aux.text();
					$aux = $($matches[i]).find("AddressInfo > ZipCode");
					zipcode = $aux.text()
					suggestions += getAddressOption(primary, cityName, county, stateAbbreviation, zipcode, lat, lon, false);
				}
			}else{
				suggestions = labelSuggestion2; //'No additional suggestions found for your address, please check it, fix it and try again';
			}
		}

		var content = '<div class="address"><div class="address-message">'+msg+'</div>'+addressmap+'<div class="address-info"><div class="current-address">'+info+'</div><div class="address-suggestions">'+suggestions+'</div></div></div><div class="sep"/><div class="address-buttons">';
		
		if (!validationRequired){
			if( showInfo == true ){
				content += '<button id="applyButton">'+labelApply+'</button>';
			}
			else if ( graceDays>0 && showInfo == false ){
				content += '<button id="applyButton">'+labelApply+'</button>';
			}
		}
		else{
			if( showInfo == true ){
				content += '<button id="applyButton">'+labelApply+'</button>';
			}
		}
		content += '<button id="cancelButton">'+labelCancel+'</button></div>';
		popAddress.setContent(content);
		$('#applyButton').click(function(){
			popAddress.hide();
			var data = null;
			if ( $('input[name=address-option]').val() != null ){ 
				data = ($('input[name=address-option]').val()).split("|");
			}
			//alert(data);
			//delete popAddress;
			if ( applyCallback ){
				applyCallback(data);
			}
		});
		$('#cancelButton').click(function(){
			popAddress.hide();
			//delete popAddress;
			if(cancelCallback){
				cancelCallback();
			}
		});
		//popAddress.show();
	}else{
		alert(labelNotConnect);
		popAddress.hide();
		//delete popAddress;
	}
}
//------------------------------------------------------------------------------
