 /*==============================================================================
   #                            EMIDA TECHNOLOGIES
   #
   # AUTHOR:                    Steven Wright  (swright@emida.net)
   # DESCRIPTION:               This Javascript checks whether the username and 
   #                            password are numbers fr mobile authentication.
   # CREATION DATE:             February 27, 2009
   #==============================================================================*/
   
  function CheckUncheckAll(Ids, permName, doCheck, doc)
  {
      var tokens = Ids.split('_');
      var permTokens = permName.split('_');
    
      for(i=1;i<tokens.length;i=i+1)
      {
          if(tokens[i] != "25" && tokens[i] != "5")
          {
              if(permTokens.length == 2)
              {
                  if(doCheck == "Y")
                      doc.getElementById(permTokens[0] + "_" + tokens[i]).checked = true;
                  else if(doCheck == "N")
                      doc.getElementById(permTokens[0] + "_" + tokens[i]).checked = false;

              }
              else if(permTokens.length == 3)
              {
                  if(doCheck == "Y")
                      doc.getElementById(permTokens[0] + "_" + permTokens[1] + "_" + tokens[i]).checked = true;
                  else if(doCheck == "N")
                      doc.getElementById(permTokens[0] + "_" + permTokens[1] + "_" + tokens[i]).checked = false;
              }
          }
      }
  }
  
  function EnableDisableAll(Ids, permName, doEnable, doc)
  {
      var tokens = Ids.split('_');
      var permTokens = permName.split('_');
    
      for(i=1;i<tokens.length;i=i+1)
      {
          if(tokens[i] != "25" && tokens[i] != "5")
          {
              if(permTokens.length == 2)
              {
                  if(doEnable == "Y")
                      doc.getElementById(permTokens[0] + "_" + tokens[i]).disabled = false;
                  else if(doEnable == "N")
                      doc.getElementById(permTokens[0] + "_" + tokens[i]).disabled = true;

              }
              else if(permTokens.length == 3)
              {
                  if(doEnable == "Y")
                      doc.getElementById(permTokens[0] + "_" + permTokens[1] + "_" + tokens[i]).disabled = false;
                  else if(doEnable == "N")
                      doc.getElementById(permTokens[0] + "_" + permTokens[1] + "_" + tokens[i]).disabled = true;
              }
          }
      }
  }

  function MobileAuthentication(doc, mobileAuthChkbox, otherIds)
  {   
      //var elementPass = doc.getElementById('password');
      var elementUser;
      var elementPass;
      var tokens = mobileAuthChkbox.split('_');
      var IDTokens = otherIds.split('_');

      // ie perm_25
      if(tokens.length == 2)
      {
          elementPass = doc.getElementById('password').value;
          elementUser = doc.getElementById('username').value;
      }
      // ie perm_821_25
      else if(tokens.length == 3)
      {
          elementPass = doc.getElementById('password_' + tokens[1]).value;
          elementUser = doc.getElementById('username_' + tokens[1]).value;
      }
      
      if(doc.getElementById(mobileAuthChkbox).checked == true)
      {
          if (isNaN(elementPass) == true || isNaN(elementUser) == true)
          {
              doc.getElementById(mobileAuthChkbox).checked = false;
              alert("In order to enable to Mobile Authentication permission," + '\n' +
              "the username and password can only be numbers.");
          }
          else
          {
              CheckUncheckAll(otherIds, mobileAuthChkbox, "N", doc);
              EnableDisableAll(otherIds, mobileAuthChkbox, "N", doc);
          }
      }
      else
      {
          EnableDisableAll(otherIds, mobileAuthChkbox, "Y", doc);
      }
  }
  
  function checkMobileAuth(textVal, mobAuthName, otherIds, doc)
  {
      var numeric = textVal;
      var isNumeric = "1";
      var tokens = mobAuthName.split('_');
      var IDTokens = otherIds.split('_');
      
      for(var j=0;j<numeric.length;j=j+1)
      {
          var alphaa = numeric.charAt(j);
          var hh = alphaa.charCodeAt(0);
          
          if((hh > 47 && hh < 58))
          {
            // Do nothing
          }
          else
          {
              isNumeric = "0";
          }
      }
      
      if(isNumeric == "0")
      {
          if(doc.getElementById(mobAuthName).checked)
          {
              doc.getElementById(mobAuthName).checked = false;
              EnableDisableAll(otherIds, mobAuthName, "Y", document);
          }
      }
  }