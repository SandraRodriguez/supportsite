$(document).ready(function(){
	$('.ColorField').attr('widt', '0').ColorPicker({
		flat: true,
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).ColorPickerHide();
			$(el).children('input')
				.val('#' + hex)
				.css('backgroundColor', '#' + hex)
				.each(function(index, el){updateColorField(el);});
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor($(this).children('input').first().val());
		},
		onShow: function () {
			$(this).children('.colorpicker').stop().animate({height: $(this).attr('widt')=='1' ? 0 : 173}, 500);
			return false;
		}
	}).bind('keyup', function(){
		$(this).ColorPickerSetColor($(this).children('input').first().val());
		$(this).children('input').each(function(index, el){updateColorField(el);});
	}).ColorPickerHide();
	$('.ColorField').children('.colorpicker').css("height",0);
	$('.ColorField').children('input')
		.bind('change', function(){
			updateColorField(this);
		})
		.click(function() {
			$(this).parent().ColorPickerSetColor($(this).val());
			$(this).parent().children('.colorpicker').stop().animate({height: $(this).parent().attr('widt')=='1' ? 0 : 173}, 500);
			$(this).parent().attr('widt', $(this).parent().attr('widt')=='0' ? '1' : '0');
			updateColorField(this);
		})
		.each(function(index, el){updateColorField(el);});

	$('img[alt=HelpToolTip]').hover(
		function(){
			$(this).next().css("left", $(this).offset().left + this.width).show();
			ensureVisible($(this).next().get()[0]);
		}, function(){
			$(this).next().hide();
		}
	);
	$('input[class=image_file]').change(
		function(){
			$($('input[name='+($(this).attr('name')).replace('_file','')+'_id]').get()[0]).val("");
			//previewImage(this, document.getElementById($(this).attr('name')+'_view'));
		}
	);
	$('.formComponent').submit(function(){
			return $(this).children('input').each(function (){
				validate(this);
			});
		}
	);
	$('input.integer').numeric();
	$('input.phone').phone();
	$('input.dateField').datepicker({changeMonth: true, changeYear: true});
	$('input.timeField').datetimepicker({hourGrid: 4,minuteGrid: 10, ampm: true});
	$('input.datetimeField').datetimepicker({showSecond: true,timeFormat: 'hh:mm:ss', changeMonth: true, changeYear: true});
	$('input.datetimeField, input.dateField, input.timeField').click(function(){ 
		if(this.chidden == 1){
			$(this).datepicker('hide');
			this.chidden = 0;
		}else{
			$(this).datepicker('show');
			this.chidden = 1;
		}
	});
	$('.datetimeButton, .dateButton, .timeButton').click(function(){
		var obj = $('#'+$(this).attr('idc')).get()[0]; 
		if(obj.chidden == 1){
			$(obj).datepicker('hide');
			obj.chidden = 0;
		}else{
			$(obj).datepicker('show');
			obj.chidden = 1;
		}
	});
        
        var controlImgWhite = $("#LogoImageWhite_id").val();
        if ( controlImgWhite !== null){            
            $.ajax({
                type: "POST",
                async: true,
                data: {objectId: controlImgWhite, action: 'getImageBytesById'},
                url: "admin/brandings/images_branding.jsp",
                success: function (data) {
                    var array_data = String($.trim(data));
                    if (array_data !== null && array_data !== 'null') {
                        //$('#fieldsetImage').css("display", "block");
                        document.getElementById("controlImgWhite").src = "data:image/png;base64," + array_data;
                    }
                }
            });
        }
        var controlImgBlack = $("#LogoImageBlack_id").val();
        if ( controlImgWhite !== null){
            $.ajax({
                type: "POST",
                async: true,
                data: {objectId: controlImgBlack, action: 'getImageBytesById'},
                url: "admin/brandings/images_branding.jsp",
                success: function (data) {
                    var array_data = String($.trim(data));
                    if (array_data !== null && array_data !== 'null') {
                        //$('#fieldsetImage').css("display", "block");
                        document.getElementById("controlImgBlack").src = "data:image/png;base64," + array_data;
                    }
                }
            });
        }
        
});

function updateColorField(eventObj){
	$(eventObj).css('backgroundColor', eventObj.value);
	var rgbstring = eventObj.style.backgroundColor;
	var strings = rgbstring.substring(4,rgbstring.length-1).split(",");
	var triplet = [];
	for(var i=0; i<3; i+=1) {
		triplet[i] = parseInt(strings[i]);
	}
	var newtriplet = [];// black or white:
	var total = 0;
	for (var i=0; i<triplet.length; i+=1){
		total += triplet[i];
	} 
	eventObj.style.color = (total > (3*256/2))? "rgb(0,0,0)" : "rgb(255,255,255)";
}
function ensureVisible(obj){
	var v = {
		x: $(window).scrollLeft(),
		y: $(window).scrollTop(),
		cx: $(window).width(),
		cy: $(window).height()
	};
	if (v.x + v.cx < obj.offsetLeft + obj.offsetWidth) {
		$(obj).animate({left: (v.x + v.cx) - (obj.offsetWidth - 20) + 'px'}, 500);
	}
	if (v.y + v.cy < obj.offsetTop + obj.offsetHeight) {
		$(obj).animate({top: (v.y + v.cy) - (obj.offsetHeight + 20) + 'px'}, 500);
	}
}

/*function previewImage(what, preview) {
	var extn = what.value.split("."); 
	extn = extn[extn.length - 1];
	if (extn.length > 1) { 
		if ((extn == "jpg") || (extn == "JPG") || (extn == "png") || (extn == "PNG") || (extn == "gif") || (extn == "GIF") || (extn == "jpeg") || (extn == "JPEG")) { 
			var filesize = 0;
			var filename = what.value.split("/"); 
			filename = filename[filename.length - 1];
			if($.browser.msie) {
				preview.src=escape('file:///'+ what.value).replace(/%5C/g, "/").replace(/%3A/g, ":");
			}else if($.browser.safari) {
				preview.src=what.value;
			}else{
				preview.src=what.files[0].getAsDataURL();
				filesize = what.files[0].fileSize;
				filename = what.files[0].fileName
			}
			var img2 = new Image();
			img2.src = preview.src;
			$(what).parent().children('input[name='+$(what).attr('name')+'_name'+']').val(filename);
			//alert("Real size: " + img2.width + " x " + img2.height + ", Size:" + filesize);
			return true;
		}else{
			alert("Invalid Image type."); 
		}
	}else{
		alert("Invalid Image type."); 
	}
	return false;
}*/

function validate(obj){
	var ok = true;
	if(obj != null){
		var rules = obj.rules;
		if(rules){
			alert(rules);
		}
	}
	/*var img2 = new Image();
	img2.src = $('input[class=image_file]').get()[0].files[0].getAsDataURL();
	img.src = img2.src;
	alert("Real size: " + img2.width + " x " + img2.height + ", Size:" + this.files[0].fileSize);	*/
	return ok;
}
/*-------------------------------*/
/*
 * Allows only valid characters to be entered into input boxes.
 * Note: does not validate that the final text is a valid number
 * (that could be done by another script, or server-side)
 *
 * @name     numeric
 * @param    decimal      Decimal separator (e.g. '.' or ',' - default is '.'). Pass false for integers
 * @param    callback     A function that runs if the number is not valid (fires onblur)
 * @author   Sam Collett (http://www.texotela.co.uk)
 * @example  $(".numeric").numeric();
 * @example  $(".numeric").numeric(",");
 * @example  $(".numeric").numeric(null, callback);
 *
 */
(function($) {
	$.fn.numeric = function(decimal, callback){
		decimal = (decimal === false) ? "" : decimal || ".";
		callback = typeof callback == "function" ? callback : function(){};
		return this.data("numeric.decimal", decimal).data("numeric.callback", callback).keypress($.fn.numeric.keypress).blur($.fn.numeric.blur);
	}
	$.fn.numeric.keypress = function(e){
		var decimal = $.data(this, "numeric.decimal");
		var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		// allow enter/return key (only when in an input box)
		if(key == 13 && this.nodeName.toLowerCase() == "input"){
			return true;
		}else if(key == 13){
			return false;
		}
		var allow = false;
		// allow Ctrl+A
		if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) return true;
		// allow Ctrl+X (cut)
		if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) return true;
		// allow Ctrl+C (copy)
		if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) return true;
		// allow Ctrl+Z (undo)
		if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) return true;
		// allow or deny Ctrl+V (paste), Shift+Ins
		if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */|| (e.shiftKey && key == 45)) return true;
		// if a number was not pressed
		if(key < 48 || key > 57)
		{
			/* '-' only allowed at start */
			if(key == 45 && this.value.length == 0) return true;
			/* only one decimal separator allowed */
			if(decimal && key == decimal.charCodeAt(0) && this.value.indexOf(decimal) != -1){
				allow = false;
			}
			// check for other keys that have special purposes
			if(	key != 8 /* backspace */ && key != 9 /* tab */ && key != 13 /* enter */ && key != 35 /* end */ && key != 36 /* home */ && key != 37 /* left */ && key != 39 /* right */ && key != 46 /* del */){
				allow = false;
			}else{
				// for detecting special keys (listed above)
				// IE does not support 'charCode' and ignores them in keypress anyway
				if(typeof e.charCode != "undefined"){
					// special keys have 'keyCode' and 'which' the same (e.g. backspace)
					if(e.keyCode == e.which && e.which != 0){
						allow = true;
						// . and delete share the same code, don't allow . (will be set to true later if it is the decimal point)
						if(e.which == 46) allow = false;
					}else if(e.keyCode != 0 && e.charCode == 0 && e.which == 0){// or keyCode != 0 and 'charCode'/'which' = 0
						allow = true;
					}
				}
			}
			// if key pressed is the decimal and it is not already in the field
			if(decimal && key == decimal.charCodeAt(0)){
				allow = (this.value.indexOf(decimal) == -1);
			}
		}else{
			allow = true;
		}
		return allow;
	}
	$.fn.numeric.blur = function(){
		var decimal = $.data(this, "numeric.decimal");
		var callback = $.data(this, "numeric.callback");
		var val = $(this).val();
		if(val != ""){
			var re = new RegExp("^\\d+$|\\d*" + decimal + "\\d+");
			if(!re.exec(val)){
				callback.apply(this);
			}
		}
	}
	$.fn.removeNumeric = function(){
		return this.data("numeric.decimal", null).data("numeric.callback", null).unbind("keypress", $.fn.numeric.keypress).unbind("blur", $.fn.numeric.blur);
	}
	//---------------------------------------------------------------------------------------------
	$.fn.phone = function(hyphen, callback){
		hyphen = (hyphen === false) ? "" : hyphen || "-";
		callback = typeof callback == "function" ? callback : function(){};
		return this.data("phone.hyphen", hyphen).data("phone.callback", callback).keypress($.fn.phone.keypress).blur($.fn.phone.blur);
	}
	$.fn.phone.keypress = function(e){
		var hyphen = $.data(this, "phone.hyphen");
		var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		// allow enter/return key (only when in an input box)
		if(key == 13 && this.nodeName.toLowerCase() == "input"){
			return true;
		}else if(key == 13){
			return false;
		}
		var allow = false;
		// allow Ctrl+A
		if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) return true;
		// allow Ctrl+X (cut)
		if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) return true;
		// allow Ctrl+C (copy)
		if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) return true;
		// allow Ctrl+Z (undo)
		if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) return true;
		// allow or deny Ctrl+V (paste), Shift+Ins
		if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */|| (e.shiftKey && key == 45)) return true;
		// if a number was not pressed
		if(key < 48 || key > 57)
		{
			/* '-' only allowed at start */
			if(key == 45 && this.value.length == 0) return true;
			/* only one hyphen separator allowed */
			if(hyphen && key == hyphen.charCodeAt(0) && this.value.indexOf(hyphen) != -1){
				allow = false;
			}
			// check for other keys that have special purposes
			if(	key != 8 /* backspace */ && key != 9 /* tab */ && key != 13 /* enter */ && key != 35 /* end */ && key != 36 /* home */ && key != 37 /* left */ && key != 39 /* right */ && key != 46 /* del */){
				allow = false;
			}else{
				// for detecting special keys (listed above)
				// IE does not support 'charCode' and ignores them in keypress anyway
				if(typeof e.charCode != "undefined"){
					// special keys have 'keyCode' and 'which' the same (e.g. backspace)
					if(e.keyCode == e.which && e.which != 0){
						allow = true;
						// . and delete share the same code, don't allow . (will be set to true later if it is the hyphen point)
						if(e.which == 46) allow = false;
					}else if(e.keyCode != 0 && e.charCode == 0 && e.which == 0){// or keyCode != 0 and 'charCode'/'which' = 0
						allow = true;
					}
				}
			}
			// if key pressed is the hyphen and it is not already in the field
			if(hyphen && key == hyphen.charCodeAt(0)){
				allow = (this.value.indexOf(hyphen) == -1);
			}
		}else{
			allow = true;
		}
		return allow;
	}
	$.fn.phone.blur = function(){
		var hyphen = $.data(this, "phone.hyphen");
		var callback = $.data(this, "phone.callback");
		var val = $(this).val();
		if(val != ""){
			var re = new RegExp("^\\d+$|\\d*" + hyphen + "\\d+");
			if(!re.exec(val)){
				callback.apply(this);
			}
		}
	}
	$.fn.removePhone = function(){
		return this.data("phone.hyphen", null).data("phone.callback", null).unbind("keypress", $.fn.phone.keypress).unbind("blur", $.fn.phone.blur);
	}	
})(jQuery);