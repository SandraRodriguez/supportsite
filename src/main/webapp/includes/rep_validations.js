
//////////////////////////////////////////////////////
//ADD GENERAL VALIDATIONS HERE
//////////////////////////////////////////////////////

$(document).ready(function()
	{		
	$("#balanceNotificationValue").keydown(function(event) 
			{
		        // Allow: backspace, delete, tab, escape, and enter
		        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.key == '.' ||  event.key == 'Decimal' ||
		        		event.keyCode == 190 || event.keyCode == 110 || event.char == '.' ||
		             // Allow: Ctrl+A
		            (event.keyCode == 65 && event.ctrlKey === true) ||
		             // Allow: home, end, left, right
		            (event.keyCode >= 35 && event.keyCode <= 39)) 
		        {

		        	if ((event.key == '.') && ($(this).val().length == 0))
		        	{
		        		event.preventDefault();
		        	}
		        	else if ((event.key == '.') && ($(this).val().indexOf(".") !== -1))
		        	{
		        		event.preventDefault();
		        	}
					else if ((event.keyCode == 190) && ($(this).val().length == 0))
		        	{
		        		event.preventDefault();
		        	}
		        	else if ((event.keyCode == 190) && ($(this).val().indexOf(".") !== -1))
		        	{
		        		event.preventDefault();
		        	}	
					else if ((event.keyCode == 110) && ($(this).val().length == 0))
		        	{
		        		event.preventDefault();
		        	}
		        	else if ((event.keyCode == 110) && ($(this).val().indexOf(".") !== -1))
		        	{
		        		event.preventDefault();
		        	}			        	
					else if ((event.key == 'Decimal') && ($(this).val().length == 0))
		        	{
		        		event.preventDefault();
		        	}
		        	else if ((event.key == 'Decimal') && ($(this).val().indexOf(".") !== -1))
		        	{
		        		event.preventDefault();
		        	}	
					else if ((event.char == '.') && ($(this).val().length == 0))
		        	{
		        		event.preventDefault();
		        	}
		        	else if ((event.char == '.') && ($(this).val().indexOf(".") !== -1))
		        	{
		        		event.preventDefault();
		        	}						
		        	else
		        	{
		                 // let it happen, don't do anything
		                 return;
		        	}
		        }
		        else {
		            // Ensure that it is a number and stop the keypress
		            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
		                event.preventDefault();
		            }
		        }
	    	});
		
		$("#balanceNotificationDays").keydown(function(event) 
				{
			        // Allow: backspace, delete, tab, escape, and enter
			        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
			             // Allow: Ctrl+A
			            (event.keyCode == 65 && event.ctrlKey === true) ||
			             // Allow: home, end, left, right
			            (event.keyCode >= 35 && event.keyCode <= 39)) {
			                 // let it happen, don't do anything
			                 return;
			        }
			        else {
			            // Ensure that it is a number and stop the keypress
			            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
			                event.preventDefault();
			            }
			        }
		    	});
	});	