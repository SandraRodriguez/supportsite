function getSelectedOptions(sdd) {
			var sdValues = [];
			for(var i = 0; i < sdd.options.length; i++) {
			if(sdd.options[i].selected == true) {
			sdValues.push(sdd.options[i].value);
			}
		}
		return sdValues;
}
function check(){
	scroll();
	return true;
}


function GetXmlHttpObject()
{
		var xmlHttp=null;
		try
		 {
		 // Firefox, Opera 8.0+, Safari
		 xmlHttp=new XMLHttpRequest();
		 }
		catch (e)
		 {
		 //Internet Explorer
		 try
		  {
		  xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		  }
		 catch (e)
		  {
		  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 }
		return xmlHttp;
}
function showSub(id, action)
{ 	

	var oList; 
	if(action == 'agent')
		oList = getSelectedOptions(document.mainform.selectagentslist); 
	else if(action == 'subagent')	
		oList = getSelectedOptions(document.mainform.subagentslist); 
	else if(action == 'reps')
		oList = getSelectedOptions(document.mainform.repslist); 
	
	var list = "";
		if(oList.length > 0){
			for( var f =0; f < oList.length;f++){
				if(list=="")
				list = oList[f];
				else
				list = list + "," + oList[f];	  
		}
		}
	if(list.length > 0 &&  list.indexOf("all")==-1)
	{
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			 {
				 alert ("Browser does not support HTTP Request");
				 return ;
			 }
			if(action == 'agent'){
			//AGENT
			var url="includes/getlist.jsp";
				url = url + "?action=agent&list=" + list.toString() +"&id=" + id ;
			xmlHttp.onreadystatechange=agentstateChanged ;
			xmlHttp.open("GET",url,true);
			xmlHttp.send(null);
			}
			else if(action == 'subagent'){
			//Sub-Agent
			var url="includes/getlist.jsp";
				url = url + "?action=subagent&list=" + list.toString() +"&id=" + id ;
			xmlHttp.onreadystatechange=subagentstateChanged ;
			xmlHttp.open("GET",url,true);
			xmlHttp.send(null);
			}
			else if(action == 'reps'){
			//Reps
			var url="includes/getlist.jsp";
				url = url + "?action=reps&list=" + list.toString() +"&id=" + id ;
			xmlHttp.onreadystatechange=repsstateChanged ;
			xmlHttp.open("GET",url,true);
			xmlHttp.send(null);
			}
	}
	else{
	
		if(action ==  'agent'){
				hideallafterAgent();
		}
		else if(action == 'subagent'){ 
				hideallaftersub();
		}
		else if(action == 'reps'){
				hideallafterrep();
		}
	}
}

function hideallafterrep(){

		document.mainform.merchantslist.style.display = 'none';		
		document.getElementById('merchantslistbox').style.display = 'none';
}
function hideallaftersub(){
		document.mainform.merchantslist.style.display = 'none';
		document.mainform.repslist.style.display = 'none';	
		document.getElementById('merchantslistbox').style.display = 'none';
		document.getElementById('repslistbox').style.display = 'none';
}

function hideallafterAgent(){
	document.mainform.merchantslist.style.display = 'none';
	document.mainform.repslist.style.display = 'none';
	document.mainform.subagentslist.style.display = 'none';
				
	document.getElementById('merchantslistbox').style.display = 'none';
	document.getElementById('repslistbox').style.display = 'none';
	document.getElementById('subagentslistbox').style.display = 'none';
}
function repsstateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 
			var response =  xmlHttp.responseText ; 
			
			cadd  =  document.mainform.merchantslist;
			
			if(response.length > 0 && response.indexOf("NOMATCH")==-1){
			    	var strar = response.toString().split(':');
	 				document.getElementById('merchantslist').length = 0;
	 				
	 				// add default "ALL"
	 				var OptNew = document.createElement('option');
			                OptNew.value  = "all";
			                OptNew.text   = "ALL";
			                try {
			                     cadd.add(OptNew, null); // standards compliant; doesn't work in IE
			                }
			                catch(ex) {
			                    cadd.add(OptNew); // IE only
			                }
			    	var i = 0 ;
			    	for ( i = 0	 ; i < strar.length-1 ; i++){
			    	if(strar[i].toString().length>0){
						  	var st = strar[i].toString().split(';');
						  	var OptNew = document.createElement('option');
			                OptNew.value  = st[0].toString();
			                OptNew.text   = st[1].toString();
			                try {
			                     cadd.add(OptNew, null); // standards compliant; doesn't work in IE
			                }
			                catch(ex) {
			                    cadd.add(OptNew); // IE only
			                }
			                }
						}
					cadd.style.display = 'block';	
					document.getElementById('merchantslistbox').style.display = 'block';
                                        document.getElementById('ratePlanListbox').style.display = 'block';
				 }
				 else
				 {
					hideallafterrep();
				 } 
	}
}
function subagentstateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 
			var response =  xmlHttp.responseText ; 
			
			cadd  =  document.mainform.repslist;
			
			if(response.length > 0 && response.indexOf("NOMATCH")==-1){
			    	var strar = response.toString().split(':');
	 				document.getElementById('repslist').length = 0;
	 				hideallafterrep();
	 				// add default "ALL"
	 				var OptNew = document.createElement('option');
			                OptNew.value  = "all";
			                OptNew.text   = "ALL";
			                try {
			                     cadd.add(OptNew, null); // standards compliant; doesn't work in IE
			                }
			                catch(ex) {
			                    cadd.add(OptNew); // IE only
			                }
			    	var i = 0 ;
			    	for ( i = 0	 ; i < strar.length-1 ; i++){
			    	if(strar[i].toString().length>0){
						  	var st = strar[i].toString().split(';');
						  	var OptNew = document.createElement('option');
			                OptNew.value  = st[0].toString();
			                OptNew.text   = st[1].toString();
			                try {
			                     cadd.add(OptNew, null); // standards compliant; doesn't work in IE
			                }
			                catch(ex) {
			                    cadd.add(OptNew); // IE only
			                }
			                }
						}
					cadd.style.display = 'block';
					document.getElementById('repslistbox').style.display = 'block';	
				 }
				 else
				 {
					hideallaftersub();
				 } 
	}
}
function agentstateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 
			var response =  xmlHttp.responseText ; 
			cadd  =  document.mainform.subagentslist;
			if(response.length > 0 && response.indexOf("NOMATCH")==-1){
			    	var strar = response.toString().split(':');
	 				document.getElementById('subagentslist').length = 0;
	 				hideallaftersub();
	 				// add default "ALL"
	 				var OptNew = document.createElement('option');
			                OptNew.value  = "all";
			                OptNew.text   = "ALL";
			                try {
			                     cadd.add(OptNew, null); // standards compliant; doesn't work in IE
			                }
			                catch(ex) {
			                    cadd.add(OptNew); // IE only
			                }
			    	var i = 0 ;
			    	for ( i = 0	 ; i < strar.length-1 ; i++){
			    	if(strar[i].toString().length>0){
						  	var st = strar[i].toString().split(';');
						  	var OptNew = document.createElement('option');
			                OptNew.value  = st[0].toString();
			                OptNew.text   = st[1].toString();
			                try {
			                     cadd.add(OptNew, null); // standards compliant; doesn't work in IE
			                }
			                catch(ex) {
			                    cadd.add(OptNew); // IE only
			                }
			                }
						}
					cadd.style.display = 'block';
					document.getElementById('subagentslistbox').style.display = 'block';	
				 }
				 else
				 {
				 	hideallafterAgent();
				 } 
	}
}
