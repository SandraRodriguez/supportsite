<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ page import="com.debisys.utils.DebisysConstants"%>
<%@ page import="com.debisys.languages.Languages"%>
<%
int section=1;
int section_page=2;
String strDistChainType = "";
String strAccessLevel = "";  
%>
<%@ include file="/includes/header.jsp" %>
<% 
if(SessionData.getUser()==null){
	String l = request.getParameter("l");
	if(l!=null && !l.equals("")){
		SessionData.setLanguage(l);
	}else{
		SessionData.setLanguage(application.getAttribute("language").toString());
	}
	
}%>
            <TABLE cellSpacing=0 cellPadding=0 width=450 border=0>
              <TBODY>
              <TR>
                <TD align="left" width="1%" 
                  background="images/top_blue.gif"><IMG height=20 
                  src="images/top_left_blue.gif" width=18></TD>
                <TD class="formAreaTitle" 
                  background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.message.title",SessionData.getLanguage()).toUpperCase()%></TD>
                <TD align="right" width="1%" 
                  background="images/top_blue.gif"><IMG height="20" 
                  src="images/top_right_blue.gif" width="18"></TD></TR>
              <TR>
              <TD bgColor=#ffffff colSpan=3>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TBODY>
                    <TR>
                      <TD>
                       <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
						 <TBODY>
								  <TR>
									<TD class=formArea>
 										<TABLE width="100%">
			                                <TBODY>
			                                <TR class="main">
			                                <TD noWrap style="padding-left:18px;">
						                 <br><br>
						                  <font color=ff0000>
						                  <%
						
						                    int intErrorMessage = 0;
						                    String strErrorMessage = request.getParameter("message");
						                    String strUrl = request.getParameter("url");
						                    if (strErrorMessage != null && !strErrorMessage.equals(""))
						                    {
							                    try
							                    {
							                      intErrorMessage = Integer.parseInt(request.getParameter("message"));
							                    }
							                    catch(Exception ex)
							                    {
							                      intErrorMessage = 0;
							                    }
							
							                    switch (intErrorMessage)
							                    {
							                      case 0:
							                        //do nothing
							                        break;
							                      case 1:
							                        out.println(Languages.getString("jsp.message.msg1",SessionData.getLanguage()));
							                        break;
							                      case 2:
							                        out.println(Languages.getString("jsp.message.msg2",SessionData.getLanguage()));
							                        break;
							                      case 3:
							                        out.println(Languages.getString("jsp.message.msg3",SessionData.getLanguage()));
							                        break;
							                      case 4:
							                        out.println(Languages.getString("jsp.message.msg4",SessionData.getLanguage()));
							                        break;
												  case 5:
							                        out.println(Languages.getString("jsp.message.msg5",SessionData.getLanguage()));
							                        break;	
							                      case 6:
							                        out.println(Languages.getString("jsp.message.msg6",SessionData.getLanguage()));
							                        break;	
							                      case 7:
							                        out.println(Languages.getString("jsp.message.msg7",SessionData.getLanguage()));
							                        break;
							                      default:
							                        break;
							                    }
						                    }
						
						                  %>
						                  </font>
						                  <br><br>			                                
			                                
			                                </TD>
			                                <TD width="306"></TD></TR>
			                                <TR class="main">
                               				 <TD style="padding-left:18px;">    </TD>
                               				 <TD></TD></TR></TBODY></TABLE>                   
              						</TD></TR>
              						
              			</TBODY></TABLE>			
		
 	</table>
 </td>
</tr>
</table>


<%@ include file="/includes/footer.jsp" %>


