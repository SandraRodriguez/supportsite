<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=windows-1252">
    <meta http-equiv="refresh" content="20; URL=http://soporte.puntoflex.com">
    <link href="default.css" type="text/css" rel="stylesheet">
    <title>&iexcl;Nuestro web site se ha movido!</title>
</head>
<body>
    <p align="center">
        &nbsp;<img src="images/puntoflex_logo.gif" alt="Inicio" border="0">
    </p>

    <h2 align="center">
        &iexcl;Nuestro web site se ha movido!
    </h2>

    <p align="center">
        <br>
        Porfavor tome nota de la nueva ubicaci&oacute;n de nuestro sitio de soporte.
        <br>
        Si no es direccionado en un momento de forma autom&aacute;tica, haga click en
        <br>
        el siguiente link:
        <br>
        <a href="http://soporte.puntoflex.com/">http://soporte.puntoflex.com/</a>
        <br>
        <br>
        El sitio actual ser&aacute; deshabilitado durante el mes de Noviembre.
        <br>
        Porfavor tome nota, y si tiene cualquier problema, env&iacute;e un email a:
        <br>
        <a href="mailto:OPS-MX@emida.net">OPS-MX@emida.net</a>
        <br>
        <br>
        Gracias.
    </p>
</body>
</html>
