
$(document).ready(function () {

    $("#contantsTabl").on('click', '.btnSelect', function () {
        var currentRow = $(this).closest("tr");

        var id = currentRow.find("td:eq(0)").text();
        var col1 = currentRow.find("td:eq(1)").text();
        var col2 = currentRow.find("td:eq(2)").text();
        var col3 = currentRow.find("td:eq(3)").text();

        $('.contantsTabl').fadeOut(1);
        $('.btnEmd').fadeOut(1);
        $('.modifica').show(1);

        document.getElementById("id").value = id;
        document.getElementById("name").value = col1;
        document.getElementById("value").value = col2;
        document.getElementById("description").value = col3;
                
        if(col1 === 'clientId' || col1 === 'email') {
            document.getElementById("value").type = "text";
        } else {
            document.getElementById("value").type = "password";
        }        
    });

    $("#fee").keyup(function () {
        var $this = $(this);
        $this.val($this.val().replace(/[^\d.]/g, ''));
    });

    $("#txtaddAmount").keyup(function () {
        var $this = $(this);
        $this.val($this.val().replace(/[^\d.]/g, ''));
    });

    $('#editpp').submit(function () {
        var options = $('#selectamounts option');
        var values = $.map(options, function (option) {
            return option.value;
        });
        document.getElementById("amountsAllowed").value = values.toString();

        var num = parseFloat(document.getElementById('fee').value);
        var new_num = num.toFixed(2);
        document.getElementById("fee").value = new_num;
    });

    $('#addpp').submit(function () {
        var num = parseFloat(document.getElementById('fee').value);
        var new_num = num.toFixed(2);
        document.getElementById("fee").value = new_num;
    });

    $('.es1 input').keyup(function () {
        var empty = false;
        $('.es1 input').each(function () {
            if ($(this).val().length === 0) {
                empty = true;
            }
        });
        if (empty) {
            $('.es2 input').attr('disabled', 'disabled');
        } else {
            $('.es2 input').removeAttr('disabled');
        }
    });

});

function addAmount() {
    var num = parseFloat(document.getElementById('txtaddAmount').value);
    var new_num = num.toFixed(2);
    document.getElementById('txtaddAmount').value = new_num;

    if (new_num !== '0') {
        $('#selectamounts').append($('<option>', {
            value: new_num,
            text: new_num
        }));
    }
    document.getElementById("txtaddAmount").value = '0';

    var usedNames = {};
    $("select[name='selectamounts'] > option").each(function () {
        if (usedNames[this.text]) {
            alert('Is already registered.!');
            $(this).remove();
        } else {
            usedNames[this.text] = this.value;
        }
    });

    var options = $('#selectamounts option');

    var values = $.map(options, function (option) {
        if (option.value !== 'NaN') {
            return option.value;
        }
    });

    document.getElementById("amountsAllowed").value = values.toString();
}

function removeAmount() {
    $("#selectamounts option:selected").remove();

    var options = $('#selectamounts option');

    var values = $.map(options, function (option) {
        return option.value;
    });

    document.getElementById("amountsAllowed").value = values.toString();
}
