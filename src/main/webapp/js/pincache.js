function updateTotalPines($templateBox){
	var count = 0;
	$templateBox.find('.productBox tr').each(function(){
		if($(this).children('td').size() > 2){
			count += parseInt($(this).children('td').get(1).innerHTML);
		}
	});
	$templateBox.find('.totalPines').text(count);
}
function removeTemplate($button){
	var $tempblock = $button.parent().parent();
	$.getJSON("/support/includes/ajax.jsp?class=com.debisys.pincache.Pincache&method=removeTemplateAjax&value=" + $button.data("templateid") +"&rnd=" + Math.random(),
		function(data){
			var cl = 'info';
			if(data.status == 'error'){
				cl = 'error';
			}else{
				cl = 'success';
				$tempblock.prev('h3').remove();
				$tempblock.remove();
			}
			$('#customMessage').html('<div class="'+cl+'">'+data.msgs.join('<br />')+'</div>');
			$('#customMessage').dialog('open');
		}
	).error(function (){
		$('#customMessage').html('<div class="error">Error</div>');
		$('#customMessage').dialog('open');		
	});
}
function removeProduct($button){
	var $tempblock = $button.parent().parent();
	$.getJSON("/support/includes/ajax.jsp?class=com.debisys.pincache.Pincache&method=removeProductAjax&value=" + $button.data("templateid") + "_" + $button.data("productid") +"&rnd=" + Math.random(),
		function(data){
			var cl = 'info';
			if(data.status == 'error'){
				cl = 'error';
			}else{
				cl = 'success';
				var $templateBox = $tempblock.parent().parent().parent(); 
				$tempblock.remove();
				updateTotalPines($templateBox);
			}
			$('#customMessage').html('<div class="'+cl+'">'+data.msgs.join('<br />')+'</div>');
			$('#customMessage').dialog('open');
		}
	).error(function (){
		$('#customMessage').html('<div class="error">Error</div>');
		$('#customMessage').dialog('open');		
	});
}
$(function() {
	$(".accordion" ).accordion({
		heightStyle: "content",
		header: "h3",
		collapsible: true,
		alwaysOpen: false,
		collapsible: true,
		active: false,
		icons: {
			header: "ui-icon-circle-arrow-e",
			activeHeader: "ui-icon-circle-arrow-s"
		}
	});
	$('#expand-all-templates').button({icons: {primary: 'ui-icon-plus'}}).click(function(){
		$('.templateBox').slideDown()
			.attr('aria-expanded', 'true').attr('aria-hidden', 'false')
			.addClass('ui-accordion-content-active')
			.prev('h3')
				.addClass('ui-accordion-header-active')
				.addClass('ui-state-active')
				.addClass(' ui-corner-top')
				.removeClass('ui-corner-all');
	});
	$('#collapse-all-templates').button({icons: {primary: 'ui-icon-minus'}}).click(function(){
		$('.templateBox').slideUp()
			.attr('aria-expanded', 'false').attr('aria-hidden', 'true')
			.removeClass('ui-accordion-content-active')
			.prev('h3')
				.removeClass('ui-accordion-header-active')
				.removeClass('ui-state-active')
				.removeClass('ui-corner-top')
				.addClass('ui-corner-all');
	});	
	$( "#add-template-form" ).dialog({
		autoOpen: false,
		height: 380,
		width: 400,
		modal: true,
		buttons: [
			{
				text: saveButtonText,
				click: function() {
					/// TODO: validate if empty
					$( this ).dialog( "close" );
					$("#template-form").submit();
				}
			},{
				text: cancelButtonText,
				click: function() {$( this ).dialog( "close" );}
			}
		],
		close: function() {}
	});
	$( "#add-product-form" ).dialog({
		autoOpen: false,
		height: 380,
		width: 400,
		modal: true,
		buttons: [
			{
				text: saveButtonText,
				click: function() {
					/// TODO: validate if empty
					$(this).dialog( "close" );
					$(this).children('form').submit();
				}
			},{
				text: cancelButtonText,
				click: function() {$( this ).dialog( "close" );}
			}
		],
		close: function() {}
	});
	$( "#edit-product-form").dialog({
		autoOpen: false,
		height: 380,
		width: 400,
		modal: true,
		buttons: [
			{
				text: saveButtonText,
				click: function() {
					$(this).dialog( "close" );
					$(this).children('form').submit();
				}
			},{
				text: cancelButtonText,
				click: function() {$( this ).dialog( "close" );}
			}
		],
		close: function(){}
	});	
	$("#a-quantity, #e-quantity").spinner({ max: 999, min: 0}).numeric(false, function() { this.value = ""; this.focus(); });
	$("#representative_id").change(function(){
		$("#rateplan_id").html('');
		$.getJSON("/support/includes/ajax.jsp?class=com.debisys.pincache.PcAssignmentTemplate&method=getApplicableRatePlansByRepAjax&value=" + $("#representative_id").val() + "&rnd=" + Math.random(),
			function(data){
				$("#rateplan_id").html('<option value=""></option>');
				$.each(data.items, function(i, item){
					var option = $("<option>");
					option.val(item[0]);
					option.text(item[1]);
					option.appendTo($("#rateplan_id"));
				});
			}
		);
	});
	
	/* Buttons */
	$("#add-template").button({
		icons: {primary: "ui-icon-plus"}
	}).click(function() {
		$( "#add-template-form" ).dialog( "open" );
	});
	$(".add-product").button({
		icons: {primary: "ui-icon-plus"}
	}).click(function() {
		$.getJSON("/support/includes/ajax.jsp?class=com.debisys.pincache.PcAssignmentTemplateProduct&method=getPincacheableProductsAjax&value=" + $(this).data("rateplanid") + '_' +$(this).data("templateid") + "&rnd=" + Math.random(),
				function(data){
					$("#a-product_id").html('<option value=""></option>');
					$.each(data.items, function(i, item){
						var option = $("<option>");
						option.val(item[0]);
						option.text(item[1]);
						option.appendTo($("#a-product_id"));
					});
				}
			);			
		$("#add-product-form" ).dialog( "open" );
		$("#a-template_id").val($(this).data("templateid"));
		$("#a-quantity").val(1);
	});
	$(".delete-template").button({
		icons: {primary: "ui-icon-trash"}
	}).click(function() {
		var $button = $(this); 
		$('#confirmDialog')
			.dialog('option', 'title', $(this).text())
			.dialog('option', 'buttons', [
				{
					text: okButtonText,
					click: function() {
						$(this).dialog('close');
						removeTemplate($button);
					}
				},{
					text: cancelButtonText,
					click: function() {
						$(this).dialog("close");
					}
				}
			])
			.dialog("open");		
	});
	$(".delete-product").button({
		icons: {primary: "ui-icon-trash"}
	}).click(function() {
		var $button = $(this); 
		$('#confirmDialog')
			.dialog('option', 'title', $(this).text())
			.dialog('option', 'buttons', [
				{
					text: okButtonText,
					click: function() {
						$(this).dialog('close');
						removeProduct($button);
					}
				},{
					text: cancelButtonText,
					click: function() {
						$(this).dialog("close");
					}
				}
			])
			.dialog("open");
	});
	$(".edit-product").button({
		icons: {primary: "ui-icon-pencil"}
	}).click(function() {
		$("#edit-product-form" ).dialog( "open" );
		$("#e-template_id").val($(this).data("templateid"));
		$("#e-product_id").val($(this).data("productid"));
		$("#e-product_id_label").text($('#'+$(this).data("templateid")+'-'+$(this).data("productid")+ "-description").text());
		$("#e-quantity").val($('#'+$(this).data("templateid")+'-'+$(this).data("productid")+ "-quantity").text());
	});	
	$("#confirmDialog").dialog({modal: true, autoOpen: false, title: "", width: 400, position: [300, 250]});
	$('#customMessage').dialog({autoOpen: false,modal: true,buttons: {OK: function() {$(this).dialog('close');}}, close: function() {$('#customMessage').html('');}});
});