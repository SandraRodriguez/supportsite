new Vue({
    el: '#ach-statements',
    data: {
        view: 'init',
        list: [],        

        model: {
            id: null,
            header_image_id: null,
            title: null,                  
            footer_image_id: null,
            footer_line1: null,
            footer_line2: null,
            email_from: null,
            email_subject: null,
            email_body: null
        },

        basic_rules: {
            title: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ],                   
            footer_line1: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ],
            footer_line2: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ],            
            image_line: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ],
            email_from: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ],
            email_subject: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ]
        },        
        

        dialogImageUrl: '',
        dialogVisible: false,
        requestType: '',
        isValidForm: false
    },
    mounted: function () {
        this.getAll();           
    },
    methods: {
        validateForm() {
            this.$refs['form'].validate((valid) => {
                if (valid) {
                    this.isValidForm = true;
                } else {
                    this.$notify.error({
                        title: 'Error',
                        message: 'Invalid Setting. Please try again'
                    });
                    this.isValidForm = false;
                }
            });
        },  
        handlePictureCardPreview(file) {
            this.dialogImageUrl = file.url;
            this.dialogVisible = true;
        },
        getAll() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/ach-statement/all',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.list = data;
            });
        },
        clean() {
            this.promoModel = {
                description: null,
                thresholds: []
            };
        },
        cancelAction() {
            this.view = 'init';
        },
        handleEdit(row) {
            this.getById(row.id);
            this.view = 'edit';
            this.requestType = 'update';
            this.view = 'add';
        },
        getById(id) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/ach-statement/getById?id=' + id,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.model = data;
                setContent(self.model.email_body);
            });
        },
        showAddForm() {
            this.requestType = 'add';
            this.view = 'add';
            this.clean();

        },
        handleRemove() {            
            this.confirmAction();
        },
        remove() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'DELETE',
                contentType: 'application/json',
                url: '/support/tools/ach-statement/remove?id=' + self.model.id,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.showOKMessage();
            });
        },
        save() {
            this.model.email_body = getTinyContent();
            this.validateForm();

            if (this.isValidForm) {
                if (this.$refs.uploaderhead.uploadFiles.length > 0) {
                    this.model.header_image_id = this.$refs.uploaderhead.uploadFiles[0].response;
                }
                if (this.$refs.uploaderfoot.uploadFiles.length > 0) {
                    this.model.footer_image_id = this.$refs.uploaderfoot.uploadFiles[0].response;
                }

                let self = this;
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: '/support/tools/ach-statement/' + self.requestType,
                    data: JSON.stringify(self.model),
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    self.showOKMessage();
                });
            }
        },
        tableRowClassName( {row, rowIndex}) {
            if (row.enabled === false) {
                return 'danger-row';
            }
            return '';
        },
        showOKMessage() {
            swal({
                title: "Good job!",
                text: " ",
                icon: "success",
                timer: 1500,
                buttons: false
            });
            setTimeout(function () {
                window.location.href = "/support/tools/ach-statement";
            }, 1600);
        },
        confirmAction() {
            let self = this;
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this setting!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    self.remove();
                }
            });
        }
    }
});