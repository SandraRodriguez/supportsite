new Vue({
    el: '#instant-spiff',
    data: {
        view: 'init',
        list: [],

        model: {
            id: null,
            iso_id: 0,
            iso_name: null,
            rtrs: [],
            status: true
        },
        rtr: {
            status: false
        },
        aux: {
            activateProducts: [],
            spiffsProducts: []
        },
        basic_rules: {
            title: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ],
            footer_line1: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ],
            footer_line2: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ],
            image_line: [
                {required: true, message: 'Please enter a valid value', trigger: 'change'}
            ]
        },

        dialogImageUrl: '',
        dialogVisible: false,
        requestType: '',
        isValidForm: false,
        active: 0,
        isos: [],
        rtrs: [],
        activProducts: [],
        spiffs: [],

        searchsetting: '',
        searchrtr: '',
        searchactiv: '',
        searchspiff: '',
        dialogTableVisible: false,
        showActiv: false,
        showSpiff: false,
        settings: [],

        currentSpiff: {
            day_to_start_pay: 0,
            day_to_end_pay: 0
        },
        addSpiff: false,
        lblnext: '',
        addDynamicActivSku: false,
        addDynamicSpiffSku: false,
        tmp: {
            activateProducts: {}
        },
        labels: {},
        startDaySugest: 0,
        isOKtoSave: false,
        disableEditStart: true
    },
    mounted: function () {
        this.getAll();
        this.getLabels();
    },
    methods: {
        buildSugestStartDay() {
            var data = this.aux.spiffsProducts;
            if (this.aux.spiffsProducts.length > 0) {
                console.log('pasa por ++');
                this.startDaySugest = Math.max.apply(Math, data.map(function (o) {
                    return o.day_to_end_pay;
                }));
                this.disableEditStart = true;
            } else {
                this.startDaySugest = 0;
                this.disableEditStart = false;
            }
        },
        resetCurrentSpiff() {
            this.currentSpiff = {};
            this.addSpiff = false;
        },
        tableRowClassName( {row, rowIndex}) {
            console.log(rowIndex);
            if (rowIndex === 1) {
                return 'warning-row';
            } else if (rowIndex === 3) {
                return 'success-row';
            }
            return '';
        },
        updateGeneralSetting() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'PUT',
                contentType: 'application/json',
                url: '/support/tools/instant-spiff-setting/update',
                data: JSON.stringify(self.model),
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.addDynamicActivSku = false;
                self.aux.activateProducts.push(self.tmp.activateProducts);
            });
        },
        switchSetting() {
            if (this.view === 'update') {
                this.updateGeneralSetting();
            }
        },
        parseDt(date) {
            return moment(date, "x").format("MM-DD-YYYY hh:mm:ss");
        },
        addDynamicActProduct() {
            this.tmp.activateProducts.id_super = this.aux.id;
            let self = this;

            if (this.view === 'add') {
                /*
                 this.rtr.activateProducts = this.aux.activateProducts;                
                 this.model.rtrs.push(this.rtr);
                 
                 this.model.rtrs.activateProducts.push(this.tmp.activateProducts)
                 self.addDynamicActivSku = false;
                 */
            } else {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: '/support/tools/instant-spiff-setting/activate/add',
                    data: JSON.stringify(self.tmp.activateProducts),
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    self.addDynamicActivSku = false;
                    self.getActivateSkuById(self.aux.id);
                });
            }
        },
        clearCurrentSpiff() {
            this.addSpiff = true;
            this.currentSpiff = {
                day_to_start_pay: 0,
                day_to_end_pay: 0
            };
        },
        onAddSpiff() {
            if (this.view === 'add') {
                if (this.currentSpiff.product_id && this.currentSpiff.day_to_start_pay && this.currentSpiff.day_to_end_pay) {
                    this.aux.spiffsProducts.push(this.currentSpiff);
                    this.addSpiff = false;
                }
            } else {
                if (this.aux.id) {
                    this.currentSpiff.id_super = this.aux.id;
                    let self = this;
                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'POST',
                        contentType: 'application/json',
                        url: '/support/tools/instant-spiff-setting/spiff/add',
                        data: JSON.stringify(self.currentSpiff),
                        error: function (jqXHR, exception) {
                            $(".ajax-loading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        self.addDynamicSpiffSku = false;
                        self.getSpiffSkuById(self.aux.id);
                        self.clearCurrentSpiff();
                    });
                } else {
                    if (this.currentSpiff.product_id && this.currentSpiff.day_to_start_pay && this.currentSpiff.day_to_end_pay) {
                        this.aux.spiffsProducts.push(this.currentSpiff);
                        this.addSpiff = false;
                    }
                }
            }
        },
        onUpdateSpiff() {
            if (this.view === 'add') {
                if (this.currentSpiff.product_id && this.currentSpiff.day_to_start_pay && this.currentSpiff.day_to_end_pay) {
                    this.aux.spiffsProducts.push(this.currentSpiff);
                    this.addSpiff = false;
                }
            } else {
                this.currentSpiff.id_super = this.aux.id;
                let self = this;
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'PUT',
                    contentType: 'application/json',
                    url: '/support/tools/instant-spiff-setting/spiff/add',
                    data: JSON.stringify(self.currentSpiff),
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    self.addDynamicSpiffSku = false;
                    self.getSpiffSkuById(self.aux.id);
                    self.clearCurrentSpiff();
                });
            }
        },
        addRtrSync() {
            this.rtr.id_super = this.model.id;
            this.rtr.activateProducts = this.aux.activateProducts;
            this.rtr.spiffsProducts = this.aux.spiffsProducts;
            this.dialogTableVisible = false;

            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: '/support/tools/instant-spiff-setting/rtr/add',
                data: JSON.stringify(self.rtr),
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.getById(self.model.id);
            });
        },
        updateRtrStatus(model) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'PUT',
                contentType: 'application/json',
                url: '/support/tools/instant-spiff-setting/rtr/update',
                data: JSON.stringify(model),
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.getById(self.model.id);
            });
        },
        addRtr() {
            this.rtr.activateProducts = this.aux.activateProducts;
            this.rtr.spiffsProducts = this.aux.spiffsProducts;
            this.model.rtrs.push(this.rtr);
            this.dialogTableVisible = false;
        },
        next() {
            this.active++;
            if (this.active === 1) {
                if (!this.rtr.product_id) {
                    this.lblnext = this.labels.lblNext;
                    this.active--;
                } else {
                    this.findProductsBy(20);
                    this.lblnext = this.labels.lblNext;
                }
            }
            if (this.active === 2) {
                if (this.aux.activateProducts.length > 0) {
                    this.findProductsBy(18);
                    this.lblnext = this.labels.lblSave;
                } else {
                    this.active--;
                }
            }
            if (this.active === 3) {
                if (this.aux.spiffsProducts.length > 0) {
                    if (this.view === 'add') {
                        this.addRtr();
                    }
                    if (this.view === 'update') {
                        this.addRtrSync();
                    }

                    this.active = 0;
                    this.dialogTableVisible = false;
                    this.lblnext = this.labels.lblNext;
                    ;
                } else {
                    this.active--;
                }
            }
        },
        back() {
            this.active--;
            if (this.active === 0) {
                this.lblnext = this.labels.lblNext;
            }
            if (this.active === 1) {
                this.lblnext = this.labels.lblNext;
            }
            if (this.active === 2) {
                this.lblnext = this.labels.lblSave;
            }
        },
        filterRTR(query, item) {
            return item.key.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
        validateForm() {
            this.$refs['form'].validate((valid) => {
                if (valid) {
                    this.isValidForm = true;
                } else {
                    this.$notify.error({
                        title: 'Error',
                        message: 'Invalid Setting. Please try again'
                    });
                    this.isValidForm = false;
                }
            });
        },
        handlePictureCardPreview(file) {
            this.dialogImageUrl = file.url;
            this.dialogVisible = true;
        },
        showModalAddProduct() {
            this.dialogTableVisible = true;
            this.active = 0;
            this.addSpiff = false;
            this.findProductsBy(3);
            this.rtr = {
                settingModel: {},
                activateProducts: [],
                spiffsProducts: [],
                status: true
            };
            this.aux = {
                activateProducts: [],
                spiffsProducts: []
            };
            this.showActiv = false;
            this.showSpiff = false;
        },
        getIsos(isEdit) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/instant-spiff-setting/getIsos?isoId=' + self.model.iso_id + "&isEdit=" + isEdit,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.isos = data;
            });
        },
        findProductsBy(transType) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/instant-spiff-setting/findProductsBy?transType=' + transType,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                if (transType === 3) {
                    self.rtrs = data;
                } else if (transType === 18) {
                    self.spiffs = data;
                } else if (transType === 20) {
                    self.activProducts = data;
                }
            });
        },
        getAll() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/instant-spiff-setting/all',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.settings = data;
            });
        },
        clear() {
            this.model = {
                id: null,
                iso_id: 0,
                rtrs: [],
                status: true
            };
            this.rtr = {
                status: false
            };
            this.aux = {
                activateProducts: [],
                spiffsProducts: []
            };
            this.showActiv = false;
            this.showSpiff = false;
        },
        cancelAction() {
            this.view = 'init';
            this.getAll();
        },

        getById(id) {
            this.clear();
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/instant-spiff-setting/getById?id=' + id,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.model = data;
                self.view = 'update';

                self.getIsos(true);
            });
        },
        getActivateSkuById(id) {
            this.addDynamicActivSku = false;
            this.addDynamicSpiffSku = false;
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/instant-spiff-setting/activate/filterBy?id_super=' + id,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.aux.activateProducts = data;
            });
        },
        getSpiffSkuById(id) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/instant-spiff-setting/spiff/filterBy?id_super=' + id,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.aux.spiffsProducts = data;
            });
        },
        showAddForm() {
            this.view = 'add';
            this.getIsos(false);
            this.clear();
        },
        handleEdit(index, model) {
            this.showActiv = true;
            this.showSpiff = true;

            if (this.view === 'add') {
                this.aux.activateProducts = this.model.rtrs[index].activateProducts;
                this.aux.spiffsProducts = this.model.rtrs[index].spiffsProducts;
            } else {
                this.aux.id = model.id;
                this.getActivateSkuById(model.id);
                this.getSpiffSkuById(model.id);
            }
        },
        handleRemove(index, model) {
            this.confirmAction('rtr', index, model);
            this.confirmAction('setting', index, model);
        },
        remove(index, model) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'DELETE',
                contentType: 'application/json',
                url: '/support/tools/instant-spiff-setting/remove?id=' + model.id,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.showOKMessage();
                self.settings.splice(index, 1);
            });
        },
        removeRtrSku(index, model) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'DELETE',
                contentType: 'application/json',
                url: '/support/tools/instant-spiff-setting/rtr/remove?id=' + model.id,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.handleEdit(index, model);
                self.model.rtrs.splice(index, 1);
            });
        },
        removeActSku(index, model) {
            let self = this;
            swal({
                title: self.labels.rmWarning,
                text: self.labels.rmWarningSubtitle,
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'DELETE',
                        contentType: 'application/json',
                        url: '/support/tools/instant-spiff-setting/activate/remove?id=' + model.id,
                        error: function (jqXHR, exception) {
                            $(".ajax-loading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        self.aux.activateProducts.splice(index, 1);
                    });
                }
            });
        },
        removeSpiff(index, model) {
            let self = this;
            swal({
                title: self.labels.rmWarning,
                text: self.labels.rmWarningSubtitle,
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'DELETE',
                        contentType: 'application/json',
                        url: '/support/tools/instant-spiff-setting/spiff/remove?id=' + model.id,
                        error: function (jqXHR, exception) {
                            $(".ajax-loading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        self.aux.spiffsProducts.splice(index, 1);
                    });
                }
            });
        },
        save() {
            var option = 'add';
            if (this.view === 'update') {
                option = 'update';
            }

            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: '/support/tools/instant-spiff-setting/' + option,
                data: JSON.stringify(self.model),
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.showOKMessage();
            });

        },
        tableRowClassName( {row, rowIndex}) {
            if (row.enabled === false) {
                return 'danger-row';
            }
            return '';
        },
        showOKMessage() {
            swal({
                title: this.labels.okMessage,
                text: " ",
                icon: "success",
                timer: 1500,
                buttons: false
            });
            setTimeout(function () {
                window.location.href = "/support/tools/instant-spiff-setting";
            }, 1600);
        },
        confirmAction(flow, index, model) {
            let self = this;
            swal({
                title: self.labels.rmWarning,
                text: self.labels.rmWarningSubtitle,
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    if (flow === 'rtr') {
                        self.model.rtrs.splice(index, 1);
                    } else if (flow === 'activation') {
                    } else if (flow === 'spiff') {
                        self.remove();
                    } else if (flow === 'setting') {
                        self.remove(index, model);
                    }
                }
            });
        },
        showNotify() {
            let self = this;
            this.$notify({
                title: self.labels.lblsuccess,
                message: self.labels.success_message,
                type: 'success'
            });
        },
        getLabels() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/instant-spiff-setting/labels',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.labels = data;
                self.lblnext = self.labels.lblNext;
            });
        }
    },
    computed: {
        computedSearchSetting: function () {
            let self = this;
            return this.settings.filter(data => !self.searchsetting
                        || data.iso_name.toLowerCase().includes(self.searchsetting.toLowerCase())
                        || data.iso_id.toString().includes(self.searchsetting.toLowerCase())
            );
        },

        computedSearchRTR: function () {
            let self = this;
            return this.model.rtrs.filter(data => !self.searchrtr
                        || data.name.toLowerCase().includes(self.searchrtr.toLowerCase())
                        || data.product_id.toString().includes(self.searchrtr.toLowerCase())
            );
        },

        computeActivSearch: function () {
            let self = this;
            return this.aux.activateProducts.filter(data => !self.searchactiv
                        || data.name.toLowerCase().includes(self.searchactiv.toLowerCase())
                        || data.product_id.toString().includes(self.searchactiv.toLowerCase())
            );
        },

        computedSearchSpiff: function () {
            let self = this;
            return this.aux.spiffsProducts.filter(data => !self.searchspiff
                        || data.name.toLowerCase().includes(self.searchspiff.toLowerCase())
                        || data.product_id.toString().includes(self.searchspiff.toLowerCase())
            );
        }
    },
    watch: {
        currentSpiff: function (model) {
            this.buildSugestStartDay();
            this.currentSpiff.day_to_start_pay = this.startDaySugest + 1;
            this.currentSpiff.day_to_end_pay = (this.startDaySugest + 5);
        },
        'model.iso_id': function (content) {
            if (this.model.iso_id > 0) {
                this.isOKtoSave = true;
            }
        }
    }
});