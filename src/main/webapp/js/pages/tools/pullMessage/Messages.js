
function Messages() {
    var Instance = this;
    var fmRetrieveMessages;
    var tableMessages;
    var numberOfPage;

    Instance.init = function () {
        $(document).ready(function () {

            numberOfPage = 1;
            $("#messageStartDate").datepicker({
                dateFormat: 'yy/mm/dd'
            });
            $("#messageEndDate").datepicker({
                dateFormat: 'yy/mm/dd'
            });
            
            selectTerminals();
            validateForm();
            
            tableMessages = $('#tableMessages')
                .DataTable({
                    drawCallback: function( settings ) {
                        var pageNumber = $('#tableMessages').DataTable().page.info();
                        if ((pageNumber.page + 1) != 1) {
                            $("#tableMessages_previous").removeClass("disabled");
                        } 
                        $("#tableMessages_next").removeClass("disabled");
                    },
                    info: false,
                    searching: false,
                    bLengthChange: false,
                    pageLength: 50,
                    pagingType: "simple"
                }
            );
            
            $("#fmPullMessages").submit(function (event) {
                if (typeof fmRetrieveMessages !== "undefined") {
                    numberOfPage = 1;
                    showTable(1);
                } else {
                    return false;
                }
                event.preventDefault(); // Don't refresh page
            });
        });
    };
    
    $("#btnDownloadMessages").on("click", function(event) {
        validateForm();
        if (fmRetrieveMessages.numberOfInvalids() === 0) {
            var url = "/support/tools/pullMessage/messages/downloadReportMessages";
            event.preventDefault();
            window.location.href = url;
        }
    });
    
    var showTable = function (numberPage) {
        validateForm();
        var rowsNumber = 0;
        if (fmRetrieveMessages.numberOfInvalids() === 0) {
            $("#divTableMessages").show(); // Show table
            var jsonData = {};
            if ( $.fn.DataTable.isDataTable('#tableMessages') ) $('#tableMessages').DataTable().destroy();

            jsonData["terminalTypeId"] = $("#selectTerminals").val();
            jsonData["startDate"] = $("#messageStartDate").val();
            jsonData["endDate"] = $("#messageEndDate").val();
            jsonData["statusMessage"] = $("#selectMessageStatus").val();

            tableMessages = $('#tableMessages').DataTable({
                drawCallback: function( settings ) {
                    if (numberPage > 1) {
                        $("#tableMessages_previous").removeClass("disabled");
                        $('.paginate_button.previous', this.api().table().container()).on('click', function() {
                            numberOfPage -= 1;
                            showTable(numberOfPage);
                        });
                    }
                    if (rowsNumber > 0) {
                        $("#tableMessages_next").removeClass("disabled"); // Show Next button pagination
                        $('.paginate_button.next', this.api().table().container()).on('click', function() { // Handle click event next button
                            numberOfPage += 1;
                            showTable(numberOfPage);
                        });
                        $("#divDownloadMessages").show();  // Show Download File button
                    } else {
                        $("#divDownloadMessages").hide();  // Hide Download File button
                    }
                },
                info: false,
                searching: false,
                bLengthChange: false,
                pageLength: 50,
                pagingType: "simple",
                async: true,
                "ajax" : {
                    "url": "/support/tools/pullMessage/messages/getAllMessages",
                    "type": "POST",
                    "data": function() {
                        if (numberPage === null || numberPage === undefined || numberPage === "" || typeof numberPage === undefined) {
                            numberPage = $('#tableMessages').DataTable().page.info();
                            numberPage = (numberPage.page + 1);
                        }
                        jsonData["page"] = numberPage;
                        return jsonData;
                    },
                    "dataSrc": function ( json ) {
                        if (json !== undefined) rowsNumber = json.length
                        return json;
                    }
                },
                "columns": [
                    {mData: "terminalType", title: tableTerminalTypeColumnTitle},
                    {mData: "siteId", title: tableTerminalIdColumnTitle},
                    {mData: "message", title: tableMessageColumnTitle},
                    {
                        mData: "messageRead", 
                        title: tableMessageReadColumnTitle,
                        render: function (data, type, row) {
                            if (data == 0) {
                                return tableLabelColumnNotRead
                            } else {
                                return tableLabelColumnRead
                            }
                        }
                    },
                    {mData: "dateWhenMessageWasRead", title: tableDateWhenMessageWasReadColumnTitle}
                ]
            });
        }
    };
    
    var validateForm = function() {
        fmRetrieveMessages = $("#fmPullMessages").validate({
            errorClass: 'errorLabelField',
            rules: {
                messageStartDate: {
                    required: true,
                    dateISO: true
                },
                messageEndDate: {
                    required: true,
                    dateISO: true,
                    greaterThanStartDate: "#messageStartDate"
                },
                selectTerminals: {
                    required: true,
                    selectValidate: true
                },
                selectMessageStatus: {
                    required: true,
                    selectValidate: true
                }
            },
            messages: {
                messageStartDate: {
                    required: formErrorRequiredInitialDate,
                    dateISO: formErrorInvalidInitialDate
                },
                messageEndDate: {
                    required: formErrorRequiredFinalDate,
                    dateISO: formErrorInvalidFinalDate,
                    greaterThanStartDate: formErrorlessThanFinalDate
                },
                selectTerminals: {
                    required: formErrorRequiredTerminalType,
                    selectValidate: formErrorInvalidTerminalType
                },
                selectMessageStatus: {
                    required: formErrorRequiredMessageStatus,
                    selectValidate: formErrorInvalidMessageStatus
                }
            }
        });

        jQuery.validator.addMethod("selectValidate", function (value, element) {
            return value !== null && value !== undefined && value !== "" && typeof value !== undefined;
        }, "Select not allowed");

        jQuery.validator.addMethod("greaterThanStartDate", function (value, element, initialDate) {
            var initDate = new Date(($(initialDate).val()) + " 00:00:00");
            var endDate = new Date((value) + " 23:59:59");
            if (!/Invalid|NaN/.test(endDate)) {
                return endDate > initDate;
            }
            return isNaN(value) && isNaN($(initialDate).val()) || (Number(value) > Number($(initialDate).val()));
        }, "Must be greater than {0}.");  
    };

    var selectTerminals = function () {
        $("#selectTerminals").empty(); // Reset field
        $("#selectTerminals").append($('<option>', {value: ""})); // Add empty option
        
        $.ajax({
            url: '/support/tools/pullMessage/messages/getAvailableTerminals',
            method: 'POST',
            async: true
        }).done(function (response) {
            if (response !== undefined && response !== null) {
                response.forEach(function(value, key) {
                    terminalTypeId = (value.terminalTypeId !== null && value.terminalTypeId !== undefined) ? value.terminalTypeId : "";
                    terminalName = (value.terminalName !== null && value.terminalName !== undefined) ? value.terminalName : "";
                    $("#selectTerminals").append($('<option>', {
                            value: terminalTypeId,
                            text: terminalName
                    }));
                });
            }
        }).error(function (error) {
            console.log(error);
        });
    };
    
    var clearFields = function () {
        $('#fmPullMessages').find("input[type=text], input[type=number], textarea, select").val("");
    };

    Instance.init();
}

var message = new Messages();