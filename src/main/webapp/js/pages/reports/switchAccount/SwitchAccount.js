function SwitchAccount() {
    var Instance = this;
    var numberOfPage;
    var fmValidate;
    var dataTable;
    
    Instance.init = function () {
        $(document).ready(function () {
            
            numberOfPage = 1;
            
            $("#startDate").datepicker({
                dateFormat: 'yy/mm/dd'
            });
            $("#endDate").datepicker({
                dateFormat: 'yy/mm/dd'
            });
            
            $("#fmSwitchAccount").submit(function(event) {
                if ((typeof fmValidate !== "undefined") && (fmValidate.numberOfInvalids() === 0)) {
                    getTerminals();
                } else {
                    return false;
                }
                event.preventDefault(); // Don't refresh page
            });
            
        });
    };
    
    fmValidate = $("#fmSwitchAccount").validate({
        errorClass: 'errorLabelField',
        rules: {
            startDate: {
                required: true
            },
            endDate: {
                required: true,
                greaterThanStartDate: "#startDate"
            },
            terminalId: {
                required: function(element) {
                    terminalT = $("#terminalId").val();
                    merchantT = $("#merchantId").val();
                    return (validateField(merchantT) ? false : (validateField(terminalT) ? false : true));
                }
            },
            merchantId: {
                required: function(element) {
                    terminalT = $("#terminalId").val();
                    merchantT = $("#merchantId").val();
                    return (validateField(terminalT) ? false : (validateField(merchantT) ? false : true));
                }
            }
        },
        messages: {
            startDate: {
                required: formErrorRequiredInitialDate
            },
            endDate: {
                required: formErrorRequiredFinalDate,
                greaterThanStartDate: formErrorlessThanFinalDate
            },
            terminalId: {
                required: formErrorRequiredTerminalId
            },
            merchantId: {
                required: formErrorRequiredMerchantId
            }
        }
    });
    
    $.validator.addMethod("greaterThanStartDate", function (value, element, initialDate) {
        var initDate = new Date(($(initialDate).val()) + " 00:00:00");
        var endDate = new Date((value) + " 23:59:59");
        if (!/Invalid|NaN/.test(endDate)) {
            return endDate > initDate;
        }
        return isNaN(value) && isNaN($(initialDate).val()) || (Number(value) > Number($(initialDate).val()));
    }, "Must be greater than {0}.");  
    
    var getTerminals = function() {
        $("#divTableSwitchAccount").show(); // Show table
        setDefaultTimeDatePicker();
        buildDataTable();
        clearFields();
    };
    
    var validateField = function(field) {
        return ((field !== 'undefined') && (field != "") && (field != null));
    };
    
    var setDefaultTimeDatePicker = function() {
        $("#startDate").val($("#startDate").val() + ' ' + '00:00:00');
        $("#endDate").val($("#endDate").val() + ' ' + '23:59:00');
    };
    
    var buildDataTable = function() {
        var uri = "/support/reports/switchAccount/getAllTerminals";
        var rowsNumber = 0;
        var dataForm = $("#fmSwitchAccount").serialize();
        if ( $.fn.DataTable.isDataTable('#tableSwitchAccount') ) $('#tableSwitchAccount').DataTable().destroy();
        
        dataTable = $('#tableSwitchAccount').DataTable({
            info: false,
            searching: false,
            bLengthChange: false,
            async: true,
            dom: 'Bfrtip',
            buttons: [
                { extend: 'csvHtml5', text: tableDownloadButton }
            ],
            "ajax" : {
                "url": uri,
                "type": "POST",
                "data": function() {
                    return dataForm;
                },
                "dataSrc": function ( json ) {
                    if (json !== undefined) rowsNumber = json.length
                    return json;
                }
            },
            "columns": [
                {mData: "ownerDba", title: tableOwnerDbaColumnTitle},
                {mData: "ownerMerchantId", title: tableOwnerMerchantIdColumnTitle},
                {mData: "ownerTerminalId", title: tableOwnerTerminalIdColumnTitle},
                {mData: "ownerUser", title: tableOwnerUserColumnTitle},
                {mData: "ownerTerminalType", title: tableOwnerTerminalTypeColumnTitle},
                {mData: "ownerTerminalLabel", title: tableOwnerTerminalLabelColumnTitle},
                {mData: "ownerTerminalCreate", title: tableOwnerTerminalDateColumnTitle},
                {mData: "ownerTerminalBranding", title: tableOwnerTerminalBrandingColumnTitle},
                {
                    mData: "ownerTerminalVersion", 
                    title: tableOwnerTerminalVersionColumnTitle,
                    render: function (data, type, row) {
                        if (data == 'V001') {
                            return tableVersionPcTerm2ColumnTitle
                        } else if (data == 'NG') {
                            return tableVersionPcTerm4ColumnTitle
                        } else {
                            return data
                        }
                        
                    }
                },
                {mData: "associatedDate", title: tableAssociatedDateColumnTitle},
                {mData: "dba", title: tableAssociatedDbaColumnTitle},
                {mData: "merchantId", title: tableAssociatedMerchantIdColumnTitle},
                {mData: "associatedTerminalId", title: tableAssociatedTerminalIdColumnTitle},
                {mData: "terminalType", title: tableAssociatedTerminalTypeColumnTitle},
                {mData: "terminalLabel", title: tableAssociatedTerminalLabelColumnTitle},
                {mData: "terminalBranding", title: tableAssociatedTerminalBrandingColumnTitle},
                {
                    mData: "terminalVersion", 
                    title: tableAssociatedTerminalVersionColumnTitle,
                    render: function (data, type, row) {
                        if (data == 'V001') {
                            return tableVersionPcTerm2ColumnTitle
                        } else if (data == 'NG') {
                            return tableVersionPcTerm4ColumnTitle
                        } else {
                            return data
                        }
                    }
                },
                {mData: "terminalCreate", title: tableAssociatedTerminalDateColumnTitle}
            ]
        });
    };
    
    var clearFields = function () {
        $('#fmSwitchAccount').find("input[type=text]").val("");
    };
    
    Instance.init();
}

var switchAccount = new SwitchAccount();