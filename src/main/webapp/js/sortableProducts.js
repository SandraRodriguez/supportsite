new Vue({
    el: '#sort-products',
    data: {
        categories: [],
        carriers: [],
        products: [],

        showProducts: false,
        showCarriers: false,
        ismoved: false,

        breadcrumb: ' ',
        searchCategory: '',
        searchCarrier: '',
        searchProduct: '',
        orderby: '',

        category: {},
        currentCarrier: {}
    },
    mounted: function () {
        this.getCategories();
    },
    methods: {
        getCategories() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/sortableProducts/categories',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.categories = data;
            });
        },
        getCarriers(category) {
            this.products = [];
            this.showProducts = false;
            this.breadcrumb = category.description;
            this.category = category;
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                data: {
                    productType: category.id
                },
                url: '/support/tools/sortableProducts/carriers',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.carriers = data;
                if (data.length > 0) {
                    self.showCarriers = true;
                } else {
                    self.showCarriers = false;
                }
            });
        },
        getProducts(carrier) {
            this.breadcrumb = this.category.description + ' / ' + carrier.name;
            this.ismoved = false;
            this.orderby = '--';
            this.currentCarrier = carrier;
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                data: {
                    productType: self.category.id,
                    carrierId: carrier.id
                },
                url: '/support/tools/sortableProducts/products',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.products = data;
                if (data.length > 0) {
                    self.showProducts = true;
                } else {
                    self.showProducts = false;
                }
            });
        },
        getProductsOrderedBy() {
            if (this.orderby !== '--') {
                this.ismoved = true;
                let self = this;
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    data: {
                        productType: self.category.id,
                        carrierId: self.currentCarrier.id,
                        orderBy: self.orderby
                    },
                    url: '/support/tools/sortableProducts/productsOrdered',
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    self.products = data;
                    if (data.length > 0) {
                        self.showProducts = true;
                    } else {
                        self.showProducts = false;
                    }
                });
            }
        },
        updateProductPosition() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: '/support/tools/sortableProducts/updateProductPosition',
                data: JSON.stringify(self.products),
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.ismoved = false;
                self.getProducts(self.currentCarrier);
                self.showSweet(true, data);
            });
        },
        onMove() {
            this.ismoved = true;
        },
        resetDragAction() {
            this.getProducts(this.currentCarrier);
        },
        showSweet(success, content) {
            if (success) {
                $.sweetModal({
                    content: content,
                    icon: $.sweetModal.ICON_SUCCESS,
                    theme: $.sweetModal.THEME_MIXED,
                    type: $.sweetModal.TYPE_MODAL,
                    showCloseButton: false,
                    timeout: 1000,
                    width: '30%'
                });
            } else {
                $.sweetModal({
                    content: content,
                    icon: $.sweetModal.ICON_ERROR,
                    theme: $.sweetModal.THEME_MIXED,
                    type: $.sweetModal.TYPE_MODAL,
                    showCloseButton: false,
                    timeout: 1000,
                    width: '30%'
                });
            }
        }
    },
    computed: {
        categoriesComputed: function () {
            let self = this;
            return this.categories.filter((category) => (category.description.toLowerCase().indexOf(self.searchCategory) !== -1
                        || category.description.toUpperCase().indexOf(self.search) !== -1));
        },
        carriersComputed: function () {
            let self = this;
            return this.carriers.filter((carrier) => (carrier.name.toLowerCase().indexOf(self.searchCarrier) !== -1
                        || carrier.name.toUpperCase().indexOf(self.search) !== -1));
        },
        productsComputed: function () {
            let self = this;
            return this.products.filter((product) => (product.description.toLowerCase().indexOf(self.searchProduct) !== -1
                        || product.description.toUpperCase().indexOf(self.searchProduct) !== -1
                        || product.id.toString().toUpperCase().indexOf(self.searchProduct) !== -1));
        },
        dragOptions() {
            return {
                animation: 0,
                ghostClass: 'ghost'
            };
        }
    }
});