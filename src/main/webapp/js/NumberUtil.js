/* 
 * EMIDA all rights reserved 1999-2025.
 */


/* global Intl */

function formatCurrency(value, locale, currency){
    var retValue = value.toString();
    
    if((value !== undefined) && (locale !== undefined) && (currency !== undefined)){
        formatter = new Intl.NumberFormat(locale, {
          style: 'currency',
          currency: currency,
          minimumFractionDigits: 2
        });
        
        retValue = formatter.format(value);
    }
    return retValue;
}
