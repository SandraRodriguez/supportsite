new Vue({
    el: '#credentials-providers',
    data: {
        rules: {
            providerId: [
                {required: true, message: 'Please provider is required, pick one', trigger: 'change'}                
            ],            
            user: [
                {required: true, message: 'Please user is required', trigger: 'change'},
                {min: 1, max: 40, message: 'Length should be 1 to 40 chars', trigger: 'change'}
            ],
            password: [
                {required: true, message: 'Please password is required', trigger: 'change'},
                {min: 1, max: 40, message: 'Length should be 1 to 40 chars', trigger: 'change'}
            ]
        },
        allProviders: [],
        providers: [],
        prefix: [],
        viewCredentialsTable: false,
        dialogVisible: false,
        formCredentials: {
            id: '',
            user: '',
            password: '',
            providerId: ''
        },
        index: 1,
        disabledProvidersControl: true        
    },
    mounted: function () {
        this.findAllProviders();
    },
    methods: {
        findAllProviders() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/toolsCredentials/credentialsByProvider/list',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                if (data.success) {
                    self.providers = data.providers;
                    self.viewCredentialsTable = false;
                    if (self.providers.length > 0) {
                        self.viewCredentialsTable = true;
                        self.rules.providerId[0].message = data.providerMesValidator;
                        self.rules.user[0].message = data.providerMesUser;
                        self.rules.user[1].message = data.lengthMessRange;                        
                        self.rules.password[0].message = data.providerMesPass;
                        self.rules.password[1].message = data.lengthMessRange;                                                
                    }
                    $.each(self.providers, function (key, value) {
                        var details = [];                        
                        if (value.status === 1) {
                            value.status = true;
                        } else {
                            value.status = false;
                        }
                        value.id = self.index;
                        self.index++;
                        $.each(value.details, function (key, valueDetail) {
                            if (valueDetail.idNewCredentialDetail !== null) {
                                valueDetail.id = self.index;
                                valueDetail.providerName = value.name;
                                valueDetail.providerId = value.providerId;
                                //valueDetail.status = false;
                                if (valueDetail.statusDetail === 1) {
                                    valueDetail.statusDetail = true;
                                } else {
                                    valueDetail.statusDetail = false;
                                }
                                details.push(valueDetail);
                                self.index++;
                            }
                        });
                        value.children = details;
                        console.log(key, value);
                    });
                    self.allProviders = data.allProviders;
                }
            });
        },
        openNew() {
            let self = this;
            self.formCredentials.id = -1;
            self.formCredentials.user = '';
            self.formCredentials.password = '';
            self.formCredentials.providerId = null;
            self.dialogVisible = true;
            self.disabledProvidersControl = false;
        },
        upateInsertCredentialDetail() {
            let self = this;
            var validationOk = false;
            this.$refs['credentialForm'].validate((valid) => {
                if (valid) {
                    validationOk = true;
                } else {
                  return false;
                }
            });
            if (validationOk) {                
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: '/support/toolsCredentials/credentialsByProvider/save',
                    data: JSON.stringify(self.formCredentials),
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    debugger;
                    if (data.length > 0) {
                        self.$notify.error({
                            title: 'Error',
                            message: data
                        });
                    } else {
                        self.dialogVisible = false;
                        self.findAllProviders();
                    }

                });
            }
        },
        handleEdit(index, rowCell) {            
            this.formCredentials.id = rowCell.idNewCredentialDetail;            
            this.formCredentials.user = rowCell.user;
            this.formCredentials.password = rowCell.password;
            this.formCredentials.providerName = rowCell.providerName;
            this.formCredentials.providerId = rowCell.providerId;
            this.formCredentials.statusDetail = rowCell.statusDetail;
            this.statusDetail = rowCell.status;
            this.dialogVisible = true;
            this.disabledProvidersControl = true;

        },
        updateStatusProvider(rowStatus) {
            let self = this;
            var validationOk = true;
            if (validationOk) {
                var status;
                if (rowStatus.status) {
                    status = 1;
                } else {
                    status = 0;
                }
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: '/support/toolsCredentials/credentialsByProvider/statusProvider',
                    data: JSON.stringify({id: rowStatus.idNewProvider, status: status}),
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    self.findAllProviders();
                });
            }
        },
        updateStatusDetailCredential(rowStatusDetail) {
            let self = this;
            var validationOk = true;
            if (validationOk) {
                var status;
                if (rowStatusDetail.statusDetail) {
                    status = 1;
                } else {
                    status = 0;
                }
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: '/support/toolsCredentials/credentialsByProvider/statusCredential',
                    data: JSON.stringify({id: rowStatusDetail.idNewCredentialDetail, status: status}),
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    self.findAllProviders();
                });
            }
        }
    },
    computed: {

    }
});