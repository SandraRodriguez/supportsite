var errorMsg = '';
var form = "";
var submitted = false;
var error = false;
var error_message = "";
var smsvalidation = false;
var pcterminalvalidation = false;
var mbvalidation = false;
var webServiceValidation=false;
var sMPTerminalType="";
var terminal_id = '';
var saveButText = '';
var cancelButText = '';

function updateRatePlanEmida(planid,divName,merchantId){
	$("#"+divName).html("");
	$("#"+divName).show();
	var EmidaRatePlanValue = document.getElementById("EmidaRatePlanValue");
	EmidaRatePlanValue.value = planid;
	//alert(EmidaRatePlanValue.value);
	if(planid != ""){
		$("#btnSubmit").removeAttr('disabled');
	}
	$("#"+divName).load("admin/customers/terminals/listplan.jsp?plan="+planid+"&merchantId="+merchantId+"&Random=" + Math.random());
}
	
function unassignPincacheTemplate(){
	$.getJSON("/support/includes/ajax.jsp?class=com.debisys.pincache.PcTerminalTemplate&method=removeAssignmentAjax&value=" + terminal_id + "&rnd=" + Math.random(),
		function(data){
			var cl = 'info';
			if(data.status == 'error'){
				cl = 'error';
			}else{
				cl = 'success';
				$('#pincacheTemplate').hide();
				$('#pincacheTemplateName').html('');
			}
			$('#customMessage').html('<div class="'+cl+'">'+data.msgs.join('<br />')+'</div>');
			$('#customMessage').dialog('open');
		}
	).error(function(){
		$('#customMessage').html('<div class="error">'+errorMsg+'</div>');
		$('#customMessage').dialog('open');
	});
}
function assignPincacheTemplate(){
	var template_id = $('#pincache_template_id').val();
	if(template_id != undefined && template_id != ''){
		$.getJSON("/support/includes/ajax.jsp?class=com.debisys.pincache.PcTerminalTemplate&method=createAssignmentAjax&value=" + terminal_id + "_" + template_id +"&rnd=" + Math.random(),
			function(data){
				var cl = 'info';
				if(data.status == 'error'){
					cl = 'error';
					$('#disablePINCache').attr('checked',true);
				}else{
					cl = 'success';
					$('#pincacheTemplateName').html($('#pincache_template_id option:selected').text());
					$('#pincacheProductList').data('templateid', $('#pincache_template_id').val());
					$('#pincacheTemplate').show();
				}
				$('#selectPincacheTemplate').dialog('close');
				$('#customMessage')
					.html('<div class="'+cl+'">'+data.msgs.join('<br />')+'</div>')
					.dialog('option','height', 300)
					.dialog('option','width', 300)
					.dialog('open');
			}
		).error(function(){
			$('#customMessage').html('<div class="error">'+errorMsg+'</div>');
			$('#customMessage').dialog('open');
		});
	}else{
		$('#disablePINCache').attr('checked',true);
	}
}

function SetFrameSize(nWidth, nHeight){
	if ( nWidth > 100 ){
		document.getElementById("ifPCTermUsers").width = nWidth;
	}
	if ( nHeight > 100 ){
		document.getElementById("ifPCTermUsers").height = nHeight;
	}
}

function switchRateTable(){
	if(document.getElementById('ratePlanTable').style.display == 'none'){
		if ( document.getElementById('newRatePlanTable')!=null){
			document.getElementById('newRatePlanTable').style.display = 'none';
		}
		document.getElementById('ratePlanTable').style.display = 'inline';
	}else{
		document.getElementById('ratePlanTable').style.display = 'none';
		if ( document.getElementById('newRatePlanTable')!=null){
			document.getElementById('newRatePlanTable').style.display = 'inline';
		}
	} 	
}

function formatAmount(n){
	var s = "" + Math.round(n * 100) / 100;
	var i = s.indexOf('.');
	if (i < 0){
		return s + ".00";
	}
	var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3);
	if (i + 2 == s.length){
		t += "0";
	}
	return t;
}//End of function formatAmount

function deactivateSerialNumber(s1,s2){
	window.open('/support/admin/customers/terminal_sn_deactivation.jsp?serialNumber='+s1+'&siteId='+s2, 'report', 'width="75"0,height=600,resizable=yes,scrollbars=yes');	
}

function SetPhysicalTerminal(cValue){
	try{
		var sValue = cValue + "###";
		document.getElementById('physicalTerminalId').value = '';
		document.getElementById('serialNumber').value = '';
		document.getElementById('simData').value = '';
		document.getElementById('imciData').value = '';
		var vData = sValue.split("#");
		document.getElementById('physicalTerminalId').value = vData[0];
		document.getElementById('serialNumber').value = vData[1];
		document.getElementById('simData').value = vData[2];
		document.getElementById('imciData').value = vData[3];
	}catch (e){}
}

function populateTerminalVersionByTerminalType(terminalType){
	$("#termVersion_Combo").html("");
	var hasVersions=false;
	var select = document.getElementById("termVersion_Combo");
	for(var i=0; i < versionsByTerminalType.length; i++){
		var valuesVersion = versionsByTerminalType[i].split("#");
		if ( valuesVersion[1] == terminalType ){
			var newOpt = new Option(valuesVersion[0], valuesVersion[2]);
			if ( document.getElementById('termVersion').value==valuesVersion[2]){
				newOpt.selected=true;
			}
			select.options[select.options.length] = newOpt;										
			hasVersions=true;
		}
	}
	if ( hasVersions ){
		document.getElementById('termVersion_Combo').style.display = 'inline';
		document.getElementById('termVersion').style.display = 'none';
		
	}else{
		document.getElementById('termVersion_Combo').style.display = 'none';
		document.getElementById('termVersion').style.display = 'inline';
	}
	
	var termVer = select[0];
	if (typeof(termVer)!== "undefined" && termVer.value === 'NG'){
        $("#chkIsAdmin").show();
        $("#idLabelIsAdmin").show();
    } else {
    	$("#chkIsAdmin").hide();
        $("#idLabelIsAdmin").hide();
    }
}


function checkTermVersion(obj){
	var obj2 = document.getElementById('mailNotificationT');
	if(obj && obj2){
		var old_val = document.getElementById('current_term_version');
		if(old_val != null && old_val.value != 'V001' && old_val.value != 'NG'){
			if(obj[obj.selectedIndex].value === 'V001'){
				obj2.style["display"]  = "";
                                $("#chkIsAdmin").hide();
                                $("#idLabelIsAdmin").hide();
                                $('#chkIsAdmin').prop('checked', false);
                                
                                
			}
                        else if (obj[obj.selectedIndex].value === 'NG'){
                            $("#chkIsAdmin").show();
                            $("#idLabelIsAdmin").show();
                        }
                        else{
                            obj2.style["display"] = "none";
                            $("#chkIsAdmin").hide();
                            $("#idLabelIsAdmin").hide();
                            $('#chkIsAdmin').prop('checked', false);
                            
                            $('#ifPCTermUsers').contents().find('chkIsAdmin').hide();
                            $('#ifPCTermUsers').contents().find('chkIsAdmin').prop('checked', false);
                            
			}
		} else{
            var valSelectVersion = $('#termVersion_Combo').val();
            if(valSelectVersion === 'NG'){
                $('#ifPCTermUsers').contents().find('label#idLabelIsAdmin').show();
                $('#ifPCTermUsers').contents().find('input#chkIsAdmin').show();
            }
            else{
                $('#ifPCTermUsers').contents().find('label#idLabelIsAdmin').hide();
                $('#ifPCTermUsers').contents().find('input#chkIsAdmin').hide();
                $('#ifPCTermUsers').contents().find('input#chkIsAdmin').prop('checked', false);
            }
            
        }
	}
}

function check_input(field_name, field_size, message){
	if (document.getElementById(field_name) && (document.getElementById(field_name).type != "hidden")){
		var field_value = document.getElementById(field_name).value;
		if (field_value == '' || field_value.length < field_size){
			error_message = error_message + "* " + message + "\n";
			error = true;
		}
	}
}//End of function check_input

function check_inputNotZero(field_name, field_size, message){
	if (document.getElementById(field_name) && (document.getElementById(field_name).type != "hidden")){
		var field_value = document.getElementById(field_name).value;
		if (field_value == '' || field_value.length < field_size || field_value == 0){
			error_message = error_message + "* " + message + "\n";
			error = true;
		}
	}
}//End of function check_input

$(document).ready(function(){
	$('#selectPincacheTemplate').dialog({
		autoOpen: false,
		height: 250,
		width: 300,
		modal: true,
		buttons: [
			{
				text: saveButText,
				click: function() {
					assignPincacheTemplate();
					$(this).dialog('close');
				}
			},{
				text: cancelButText,
				click: function() {
					$( this ).dialog('close');
					$('#disablePINCache').attr('checked',true);
				}
			}
		],
		close: function() {}
	});
	$('#customMessage').dialog({
		autoOpen: false,
		modal: true,
		buttons: {OK: function() {$(this).dialog('close');}},
		close: function() {
			$('#customMessage').html('');
		}
	});
	$('input[name=enablePINCache]').change(function(){
		//if($(this).is(':checked')){
		if($(this).val() == 'true' && $(this).is(':checked')){
			//$('#pincacheTemplate').show();
			$('#selectPincacheTemplate').dialog('open');
		}else{
			unassignPincacheTemplate();
		}
	});
	$('#pincacheProductList').button({icons:{primary: 'ui-icon-suitcase'}})
		.click(function(){
			if($('#pincacheProductList').data('templateid')){
				$('#customMessage')
					.dialog('option','height', 600)
					.dialog('option','width', 500)
					.load('/support/admin/pincache/products.jsp?template_id=' + $('#pincacheProductList').data('templateid'), function(data){
						$(this).html(data);
						$(this).dialog('open');
					});
			}
			return false;
		});
	
	if($('#enablePINCache').val() == 'true' && $('#enablePINCache').is(':checked')){
		$('#pincacheTemplate').show();
	}else{
		$('#pincacheTemplate').hide();
	}
	$('#pincacheManageBtn')
		.button({icons:{primary:'ui-icon-bullet'}})
		.click(function(){
			window.open('/support/admin/pincache/manage_pins.jsp?site_id='+$(this).data('siteid'), '_self');
			return false;
		});
	$('#pincacheSyncHistory')
		.button({icons:{primary:'ui-icon-refresh'}})
		.click(function(){
			var titlebtn = $(this).text(); 
			//window.open(, '_self');
			$('#customMessage').load('/support/admin/reports/pincache/sync.jsp?dialogMode=1&site_id='+$(this).data('siteid'), function(data){
				$(this)
					.html(data)
					.dialog('option','title', titlebtn)
					.dialog('option','height', 600)
					.dialog('option','width', 800)
					.dialog('open');
				});
			return false;
		});
	/*$('#terminalType').change(function(){
		terminalType_onclick(this);
	});
	terminalType_onclick($('#terminalType').get(0));*/
});
