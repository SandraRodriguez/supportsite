/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {// If Internet Explorer, return version number
        return true;
    }
    else {
        return false;
    }
}

function newPaymentWizard() {
    if (isInternetExplorer()) {
        window.location.href = "paymentWizardAddEdit.jsp?action=insert";
    }
    else {
        window.location.href = "admin/tools/depositTypes/paymentWizardAddEdit.jsp?action=insert";
    }
}

function editPaymentWizard(id) {
    if (isInternetExplorer()) {
        window.location.href = "paymentWizardAddEdit.jsp?action=edit&id=" + id + "";
    }
    else {
        window.location.href = "admin/tools/depositTypes/paymentWizardAddEdit.jsp?action=edit&id=" + id + "";
    }
}

function goToPaymentWizard() {
    if (isInternetExplorer()) {
        window.location.href = "paymentWizardList.jsp";
    }
    else {
        window.location.href = "admin/tools/depositTypes/paymentWizardList.jsp";
    }
}

function selectImage(paymentWizardId) {

    $('#fieldsetImage').css("display", "none");
    $.ajax({
        type: "POST",
        async: false,
        data: {paymentWizardId: paymentWizardId, action: 'getImageBytesByPaymentWizardId'},
        url: "admin/tools/depositTypes/depositTypesProcessDB.jsp",
        success: function (data) {
            var array_data = String($.trim(data));
            if (array_data !== null && array_data !== 'null') {
                $('#fieldsetImage').css("display", "block");
                document.getElementById("imagenPreview").src = "data:image/png;base64," + array_data;
            }
        }
    });

    openDialog('dialogUploadImage', '');
    $("#paymentWizardId").val(paymentWizardId);
}

function openDialog(dialogId, msg) {

    $('#' + dialogId).dialog({
        title: msg,
        maxWidth: 700,
        maxHeight: 500,
        width: 700,
        height: 500,
        modal: true
    });
}


function changeDepositTypes() {
    var banksDepositTypeSelect = $('#banksDepositTypesList').val();
    if (banksDepositTypeSelect === '-1') {
        return;
    }

    $('#depositTypeList').html('');

    $.ajax({
        type: "POST",
        async: false,
        data: {action: 'getDepositTypes', bankId: banksDepositTypeSelect},
        url: "admin/tools/depositTypes/depositTypesProcessDB.jsp",
        success: function (data) {

            var arrayDepositTypes = String($.trim(data)).split("\n");
            for (var i = 0; i < arrayDepositTypes.length; i++) {
                if (arrayDepositTypes[i] !== '') {
                    var depositTypesData = String($.trim(arrayDepositTypes[i])).split("|");
                    $('#depositTypeList').append('<option value="' + depositTypesData[0] + '" selected="selected">' + depositTypesData[1] + '</option>');
                }
            }
        }
    });
}

function spinnerNumeric() {
    $("input:text.spinner").keydown(function (event) { // fields with class numeric
        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190) {
            event.preventDefault();
        }
        //if a decimal has been added, disable the "."-button

    });
}


function validateFormPaymentWizard(errorDuplicate) {

    removeRedBorder();
    var hasErrors = false;

    var description = $("#description").val();
    var banksDepositTypesList = $("#banksDepositTypesList").val();
    var depositTypeList = $("#depositTypeList").val();

    if (description.trim() === '') {
        $("#description").css("border", "1px solid red");
        hasErrors = true;
    }
    if (banksDepositTypesList === '-1') {
        $("#banksDepositTypesList").css("border", "1px solid red");
        hasErrors = true;
    }
    if (depositTypeList === '-1') {
        $("#depositTypeList").css("border", "1px solid red");
        hasErrors = true;
    }

    // Validating fields
    var fieldLabel = $("#fieldLabel").val();
    var fieldPrefix = $("#fieldPrefix").val();
    var helpText = $("#helpText").val();

    if (fieldLabel.trim() === '') {
        $("#fieldLabel").css("border", "1px solid red");
        hasErrors = true;
    }
    if (fieldPrefix.trim() === '') {
        $("#fieldPrefix").css("border", "1px solid red");
        hasErrors = true;
    }
    if (helpText.trim() === '') {
        $("#helpText").css("border", "1px solid red");
        hasErrors = true;
    }
    
    var idPaymentWizard = $("#idPaymentWizard").val();
    if (!hasErrors) {
        $.ajax({
            type: "POST",
            async: false,
            data: {idPaymentWizard: idPaymentWizard, selectedBank: banksDepositTypesList, depositTypeSelected: depositTypeList, action: 'isBankDepositTypeDuplicate'},
            url: "admin/tools/depositTypes/depositTypesProcessDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data));
                if (array_data === 'true') {
                    alert(errorDuplicate);
                    hasErrors = true;
                }
            }
        });
    }


    return !hasErrors;

}

function removeRedBorder() {
    $("#description").css('border', '');
    $("#banksDepositTypesList").css('border', '');
    $("#depositTypeList").css('border', '');
    $("#fieldLabel").css('border', '');
    $("#fieldPrefix").css('border', '');
    $("#helpText").css('border', '');
}

function deletePaymentWizard(id, message, messageNotCanDelete) {

    var canDelete = true;
    if (canDelete) {
        if (confirm(message)) {
            if (isInternetExplorer()) {
                window.location.href = "depositTypesProcessDB.jsp?action=removeDepositType&id=" + id + "";
            }
            else {
                window.location.href = "admin/tools/depositTypes/depositTypesProcessDB.jsp?action=removePaymentWizard&id=" + id + "";
            }
        }

    }
}

function erasePrefixValue() {
    $('#fieldPrefix').val('');
}

function resizeInputType() {

    var sizeInput = $('#spinnerP').val();
    $('#fieldPrefix').attr('maxlength', sizeInput);
}


function validateInputType(evt) {

    var fieldType = $('input[name=fieldType]:checked').val();
    if (fieldType === 'N') {
        return isNumberKey(evt);
    }
    else if (fieldType === 'AN') {
        return isAlphaNumberKeySensitiveCase(evt);
    }
    else if (fieldType === 'A') {
        return isAlphabetic(evt);
    }
    else if (fieldType === 'F') {
        return true;
    }
}

function isAlphabetic(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //if (charCode > 31 && (charCode < 48 || charCode > 57))
    if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode === 190 || charCode === 8)
        return true;

    return false;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //if (charCode > 31 && (charCode < 48 || charCode > 57))
    if ((charCode < 48 || charCode > 57) && charCode !== 46 && charCode !== 110 && charCode !== 190 && charCode !== 8)
        return false;

    return true;
}

function isAlphaNumberKeySensitiveCase(evt) {
    var result = false;
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (!isNumberKey(evt)) {
        result = isAlphabetic(evt);
    } else {
        result = true;
    }

    return result;
}