function validateRegEx(regex, inputField)
{
    var inputValue = inputField.value;
    var retValue = false;
    if ((inputField !== null) && (regex !== null)) {
        // See if the input data validates OK
        if (regex.test(inputValue))
        {
            retValue = true;
        }
        // else : response already false
    }
    return retValue;
}


function isEmpty(inputField)
{
    // See if the input value contains any text
    return !validateRegEx(/.+/, inputField);
}

function isValidLength(minLength, maxLength, inputField)
{
    // See if the input value contains at least minLength but no more than maxLength characters
    return validateRegEx(new RegExp("^.{" + minLength + "," + maxLength + "}$"),
            inputField);
}

function validateZipCode(inputField)
{
    // First see if the input value contains data
    if (!isEmpty(inputField))
        return false;

    // Then see if the input value is a ZIP code
    return validateRegEx(/^\d{5}$/, inputField);
}

function validateDate(inputField)
{
    // First see if the input value contains data
    if (isEmpty(inputField))
        return false;

    // Then see if the input value is a date
    return validateRegEx(/^\d{2}\/\d{2}\/(\d{2}|\d{4})$/,
            inputField);
}


function validateEmail(inputField)
{
    // First see if the input value contains data
    if (!isEmpty(inputField))
        return false;

    // Then see if the input value is an email address
    return validateRegEx(/^[\w\.-_\+]+@[\w-]+(\.\w{2,3})+$/,
            inputField);
}


function setErrorStyle(inputField, helpField, errorMessage, setError, focusOnError)
{
    if (focusOnError === null) {
        focusOnError = false;
    }

    if ((inputField !== null) && (setError !== null)) {
        if (setError === true) {
            inputField.style.color = "red";
            if (helpField !== null)
                helpField.innerHTML = errorMessage;
            if (focusOnError) {
                inputField.focus();
            }
        } else {
            if (helpField !== null)
                helpField.innerHTML = "";
            inputField.style.color = "black";
        }
    }
}

function validateNumericRegExp(inputField, regex)
{
    var inputValue = inputField.value;
    var retValue = false;
    if ((inputField !== null) && (regex !== null)) {
        // See if the input data validates OK
        if (regex.test(inputValue))
        {
            retValue = true;
        }
        // else : response already false
    }
    return retValue;
}


function isPositiveNumber(inputField)
{
    return validateNumericRegExp(inputField, /^\d*\.{0,1}\d+$/);
}

function isCurrency(inputField)
{
    return validateNumericRegExp(inputField, /(?=.)^\$?-?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,4})?$/);
}

function hideElement(element, hide) {
    if (element !== null) {
        if (hide) {
            element.style.display = "none";
        } else {
            element.style.display = "inline";
        }
    }
}