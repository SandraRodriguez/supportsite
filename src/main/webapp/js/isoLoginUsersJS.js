

function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {// If Internet Explorer, return version number
        return true;
    }
    else {
        return false;
    }
}

function editIsoLoginUsers(id) {

    $('#spanWarningMessages').text('');
    showWindowsCharge();
    if (isInternetExplorer()) {
        window.location.href = "isoLoginList.jsp?action=edit&isoUserId=" + id + "";
    }
    else {
        window.location.href = "admin/tools/isoLogin/isoLoginList.jsp?action=edit&isoUserId=" + id + "";
    }
    showForm();

}

function showWindowsCharge() {
    document.getElementById('div_show_charge').style.display = "block";
}

function hideWindowsCharge() {
    $('#div_show_charge').css("display", "none");
}

function newIsoLogin() {
    $('#spanWarningMessages').text('');
    removeRedBorder();
    $("#isoList option").prop('selected', false);
    $("#userId").val('');
    $("#password").val('');
    showForm();
}

function showForm() {
    $('#div_form_isoLogin').css("display", "block");
}

function hideForm() {
    $('#div_form_isoLogin').css("display", "none");
}

function deleteIsoLogin(id, message, messageNotCanDelete) {

    showWindowsCharge();
    if (confirm(message)) {
        if (isInternetExplorer()) {
            window.location.href = "isoLoginList.jsp?action=removeIsoLogin&isoUserId=" + id + "";
        }
        else {
            window.location.href = "admin/tools/isoLogin/isoLoginList.jsp?action=removeIsoLogin&isoUserId=" + id + "";;
        }
    }
    else {
        hideWindowsCharge();
    }   
    
}

function goBack(msg) {
    if (isInternetExplorer()) {
        window.location.href = "isoLoginList.jsp" ;
    }
    else {
        window.location.href = "admin/tools/isoLogin/isoLoginList.jsp";
    }
}

function validateForm(message, action, id) {
    
    removeRedBorder();
    var hasErrors = false;

    if ($('#isoList').val() === '-1') {
        $("#isoList").css("border", "1px solid red");
        hasErrors = true;
    }

    var userId = $('#userId').val();
    if (userId === null || userId.trim()==='') {
        $("#userId").css("border", "1px solid red");
        hasErrors = true;
    }
    
    var password = $('#password').val();
    if (password === null || password.trim() === '') {
        $("#password").css("border", "1px solid red");
        hasErrors = true;
    }
    
    if(!hasErrors){
        var user = $('#userId').val();
        $.ajax({
            type: "POST",
            async: false,
            data: {id: id, action: action, user: user},
            url: "admin/tools/isoLogin/isoLoginDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data));
                if (array_data === 'false') {
                    alert(message);
                    hasErrors = true;
                }
            }
        });
    }
    
    return !hasErrors;
}

function changeRepsByIso() {
    var isoSelected = $('#isoList').val();
    if (isoSelected === '-1') {
        return;
    }

    $('#defaultRepsList').html('');

    $.ajax({
        type: "POST",
        async: false,
        data: {action: 'getRepsByIso', isoId: isoSelected},
        url: "admin/tools/isoLogin/isoLoginDB.jsp",
        success: function (data) {

            var arrayRepsList = String($.trim(data)).split("\n");
            for (var i = 0; i < arrayRepsList.length; i++) {
                if (arrayRepsList[i] !== '') {
                    var currentRep = String($.trim(arrayRepsList[i])).split("|");
                    $('#defaultRepsList').append('<option value="' + currentRep[0] + '" selected="selected">' + currentRep[1] + '</option>');
                }
            }
        }
    });
}


function removeRedBorder() {
    $("#isoList").css('border', '');
    $("#userId").css('border', '');
    $("#password").css('border', '');
}



