/* global applyPaymentButtonLbl, cancelButtonLbl, CUSTOM_CONFIG_TYPE_MEXICO, currentCustomConfigType, repMerchantBalanceSumError, repMerchantBalanceSumErrorTitle, DEPLOYMENT_INTERNATIONAL, currentDeploymentType, repCurrencyCode */

var repId = 0;
var depInfo = '';
var saveButText = '';
var cancelButText = '';
var maxBalance = 0;
var rbDialogTitle = '';

function reserveBalance(rep_id, val, isReturn) {
    $.getJSON('/support/includes/ajax.jsp?class=com.debisys.pincache.PcReservedBalance&method=repReserveAjax&value=' + rep_id + '_' + val + '_' + depInfo + (isReturn ? '_true' : '_false') + '&rnd=' + Math.random(),
            function (data) {
                var cl = 'info';
                if (data.status === 'error') {
                    cl = 'error';
                } else {
                    cl = 'success';
                    $('#customMessage').dialog('option', 'buttons', {
                        Ok: function () {
                            location.reload();
                        }
                    });
                }
                $('#customMessage').html('<div class="' + cl + '">' + data.msgs.join('<br />') + '</div>');
                $('#customMessage').dialog('open');
            }
    ).error(function () {
        $('#customMessage').html('<div class="error">Error</div>');
        $('#customMessage').dialog('open');
    });
}

$(document).ready(function () {
    $('#customMessage').dialog({
        autoOpen: false,
        modal: true,
        buttons: {OK: function () {
                $(this).dialog('close');
            }},
        close: function () {
            $('#customMessage').html('');
        }
    });
    $('#rbReserveDialog').dialog({
        title: rbDialogTitle,
        autoOpen: false,
        height: 250,
        width: 300,
        modal: true,
        buttons: [
            {
                text: saveButText,
                click: function () {
                    reserveBalance(repId, $('#valueToReserve').val(), false);
                    $(this).dialog('close');
                }
            }, {
                text: cancelButText,
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $('#valueToReserve').val('');
        }
    });
    $('#rbReturnDialog').dialog({
        title: rbDialogTitle,
        autoOpen: false,
        height: 250,
        width: 300,
        modal: true,
        buttons: [
            {
                text: saveButText,
                click: function () {
                    reserveBalance(repId, $('#valueToReturn').val(), true);
                    $(this).dialog('close');
                }
            }, {
                text: cancelButText,
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $('#valueToReturn').val('');
        }
    });
    $('#rbReserve').button({incons: {primary: 'ui-icon-arrowreturn-1-w'}})
            .click(function () {
                $('#rbReserveDialog').dialog('open');
                return false;
            }
            );
    $('#rbReturn').button({incons: {primary: 'ui-icon-radio-off'}})
            .click(function () {
                $('#rbReturnDialog').dialog('open');
                return false;
            }
            );
    $('#valueToReserve, #valueToReturn')
            .spinner({max: maxBalance, min: 1.00})
            .numeric({decimal: '.', negative: false}, function () {
                this.value = "";
                this.focus();
            });

    // Precondition : applyPayment.js already included    
    if (currentCustomConfigType === CUSTOM_CONFIG_TYPE_MEXICO) {
        $("#frmRepPaymentMx").dialog({
            modal: true,
            autoOpen: false,
            maxWidth: 700,
            maxHeight: 900,
            width: 500,
            height: 500,
            position: ['middle', 20],
            buttons: [{text: applyPaymentButtonLbl, click: function () {
                        $("#paymentForm").submit();
                    }},
                {text: cancelButtonLbl, click: function () {
                        clearRepPaymentForm();
                        $(this).dialog("close");
                    }}]
        });
    }

    $('#btnCheckMerchantsBalance').on('click', function (event) {
        event.preventDefault();
        jQuery.ajax({
            url: 'customers/reps/repMerchantBalanceSum',
            data: 'repId=' + repId,
            method: 'GET',
            dataType: "text",
            async: true
        }).done(function (response) {
            
            $('#spMerchantsBalance').text(response);

        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorMsg = repMerchantBalanceSumError + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            toastr.error(errorMsg, repMerchantBalanceSumErrorTitle, {positionClass: "toast-top-center"});
        });
    });

});

function formatAmount(n) {
    var s = "" + Math.round(n * 100) / 100;
    var i = s.indexOf('.');
    if (i < 0)
        return s + ".00";
    var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3);
    if (i + 2 === s.length)
        t += "0";
    return t;
}

function GetPaymentAmountValue() {
    return document.payment.paymentAmount.value;
}

function SetRepMerchantPayment(sText) {
    document.getElementById("txtRepMerchantPayment").value = sText;
    document.getElementById("btnApplyPayment").disabled = false;
    document.getElementById("btnApplyPayment").click();
    document.getElementById("btnApplyPayment").disabled = true;
}

function SetRepMerchantMigration(sText) {
    document.getElementById("txtRepMerchantMigration").value = sText;
    if (sText.length > 0) {
        document.getElementById("frmRepMerchantMigration").submit();
    }
}//End of function SetRepMerchantMigration

function ValidatePaymentValueMX(cText, bAllowMinus) {
    var sText = cText.value;
    var sResult = "";
    var bHasPeriod = false;
    var bHasMinus = false;
    for (i = 0; i < sText.length; i++) {
        if ((sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57)) {
            sResult += sText.charAt(i);
        }
        if (!bHasPeriod && sText.charCodeAt(i) === 46) {/*period*/
            sResult += sText.charAt(i);
            bHasPeriod = true;
        }
        if (bAllowMinus && !bHasMinus && (sText.charCodeAt(i) === 45) && (i === 0))/* - */ {
            sResult += sText.charAt(i);
            bHasMinus = true;
        }
    }
    if (sResult.length === 0) {
        sResult = "0";
    }
    if (sText !== sResult) {
        cText.value = sResult;
    }
}//End of function ValidatePaymentValueMX

function calculate() {
    if (isNaN(document.payment.commission.value) === false && isNaN(document.payment.paymentAmount.value) === false) {
        var intGrossPayment = 0;
        var intNetPayment = 0;
        var intCommissionAmount = 0;
        var intCommissionPercent = 0;
        intGrossPayment = document.payment.paymentAmount.value;
        intCommissionPercent = document.payment.commission.value;
        intCommissionAmount = intGrossPayment * (intCommissionPercent * .01);
        intNetPayment = intGrossPayment - intCommissionAmount;
        document.payment.netPaymentAmount.value = formatAmount(intNetPayment);
    }
}

function DisableButtons() {
    var ctl = document.getElementById("btnUpdateCreditLimit");
    if (ctl !== null) {
        ctl.disabled = true;
    }
    ctl = document.getElementById("btnEditRep");
    if (ctl !== null) {
        ctl.disabled = true;
    }
    ctl = document.getElementById("btnChangeType");
    if (ctl !== null) {
        ctl.disabled = true;
    }
    ctl = document.getElementById("btnApplyPayment");
    if (ctl !== null) {
        ctl.disabled = true;
    }
}



