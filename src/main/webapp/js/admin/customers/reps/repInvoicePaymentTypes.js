/* 
 * EMIDA all rights reserved 1999-2019.
 */


/* global invoycePaymentTypes, invoiceSaveLabel, invoicePaymentCancelLabel, invoiceAddPaymentLabel, repId, repInvoicePaymentTypes, invoiceEditLabel, invoiceDeleteLabel, invoicePaymentAccountWarning */

var currentEditPaymentId = '';
var currentEditPaymentRowIndex = -1;
var isEdditing = false;


$(document).ready(function () {
    onSelectChangeInvoice();

});


function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

function generateGuid() {
    return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
}

function clearRepInvoiceTable() {
    var invoicePaymentsTable = document.getElementById("tableInvoicingPaymentType");
    if (invoicePaymentsTable) {
        if (invoicePaymentsTable.rows.length > 2) {
            for (i = (invoicePaymentsTable.rows.length - 2); i >= 1; i--) {
                invoicePaymentsTable.deleteRow(i);
            }
        }
    }
}

function onSelectChangeInvoice()
{
    var selected = $("#invoiceType option:selected");
    if ((selected !== null) && (selected.attr("id") !== "NINV"))
    {
        $("#tableInvoicingPaymentType").css('display', 'inline');
    } else {
        $("#tableInvoicingPaymentType").css('display', 'none');
        repInvoicePaymentTypes = {};
        addInvoicingTypesResult();
        clearRepInvoiceTable();
    }
}

/**
 * Creates a combo box using the invoycePaymentTypes values
 * @returns {generateInvoicePaymentsSelect.selectList|Element}
 */
function generateInvoicePaymentsSelect()
{
    var selectList = null;
    if (invoycePaymentTypes) {
        selectList = document.createElement("select");
        selectList.id = "invoicePaymentTypes";
        selectList.name = "invoicePaymentTypes";
        selectList.onchange = validateAccountNumber;
        for (i = 0; i < invoycePaymentTypes.length; i++) {
            var currentOption = document.createElement("option");
            currentOption.value = invoycePaymentTypes[i].id;
            currentOption.id = invoycePaymentTypes[i].code;
            if (invoycePaymentTypes[i].code === 'N/A') {
                currentOption.selected = true;
            }
            currentOption.text = invoycePaymentTypes[i].description;
            selectList.appendChild(currentOption);
        }
    }
    return selectList;
}


function generateAccountWarningSpan() {
    var accountSpanWarning = document.createElement("span");
    accountSpanWarning.id = "accountSpanWarning";
    accountSpanWarning.name = "accountSpanWarning";
    accountSpanWarning.style.color = "red";
    accountSpanWarning.innerHTML = invoicePaymentAccountWarning;
    hideElement(accountSpanWarning, true);
    return accountSpanWarning;
}


/**
 * Creates an input field to capture payment types account numbers
 * @param {type} accountNumber
 * @returns {generateInvoicePaymentAccountInput.accountInput|Element}
 */
function generateInvoicePaymentAccountInput(accountNumber) {
    var accountInput = document.createElement("input");
    accountInput.id = "paymentTypeAccount";
    accountInput.name = "paymentTypeAccount";
    accountInput.type = "text";
    accountInput.maxLength = 4;
    accountInput.size = 5;
    accountInput.value = accountNumber;
    accountInput.onblur = validateAccountNumber;
    accountInput.onkeyup = validateAccountNumber;
    accountInput.onkeypress = function () {
        return ((event.charCode >= 48) && (event.charCode <= 57));
    };
    return accountInput;
}

function onEditImageClick(rowIndex, paymentId) {
    cancelEdit();
    removeAddPaymentTypeButtonRow();
    editInvoicingPayment(rowIndex,
            paymentId);
    addInvoicingTypesResult();
}

function onDeleteImageClick(rowIndex, paymentId) {
    cancelEdit();
    deleteInvoicingPayment(rowIndex,
            paymentId);
    showAddPaymentTypeButtonRow();
    addInvoicingTypesResult();
}

function generateEditButtonsTable(rowIndex, paymentId) {
    var buttonsTable = document.createElement("table");
    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    var editImage = document.createElement('img');
    editImage.title = invoiceEditLabel;
    editImage.src = 'images/icon_edit.gif';
    editImage.onclick = function () {
        onEditImageClick(rowIndex, paymentId);
    };
    td1.appendChild(editImage);
    tr.appendChild(td1);

    var td2 = document.createElement('td');
    td2.innerHTML = '&nbsp;';
    tr.appendChild(td2);

    var td3 = document.createElement('td');
    var deleteImage = document.createElement('img');
    deleteImage.title = invoiceDeleteLabel;
    deleteImage.src = 'images/icon_delete.gif';
    deleteImage.onclick = function () {
        onDeleteImageClick(rowIndex, paymentId);
    };
    td3.appendChild(deleteImage);
    tr.appendChild(td3);
    buttonsTable.appendChild(tr);

    return buttonsTable;
}

function removeRowCells(currentRow) {
    if (currentRow) {
        for (i = (currentRow.cells.length - 1); i >= 0; i--) {
            currentRow.deleteCell(i);
        }
    }
}

function removePaymentEditRowCells() {
    var currentEditRow = document.getElementById(currentEditPaymentId);
    if (isEdditing && currentEditRow) {
        removeRowCells(currentEditRow);
    }
}

function cancelEdit() {

    if (isEdditing) {
        removePaymentEditRowCells();
        showPaymentTypeRow(currentEditPaymentId, currentEditPaymentRowIndex);
    }
}

function generateSaveCancelButtonsTable(isUpdating, paymentId, rowIndex) {
    var savePaymentTypeButton = document.createElement("button");
    savePaymentTypeButton.id = "savePaymentTypeButton";
    savePaymentTypeButton.name = "savePaymentTypeButton";
    var saveButtonText = document.createTextNode(invoiceSaveLabel);
    savePaymentTypeButton.appendChild(saveButtonText);

    savePaymentTypeButton.onclick = function () {
        validateAccountNumber();
        updatePaymentTypeRecord(paymentId, rowIndex);
        showAddPaymentTypeButtonRow();
        addInvoicingTypesResult();
    };

    var cancelPaymentTypeButton = document.createElement("button");
    cancelPaymentTypeButton.id = "cancelPaymentTypeButton";
    cancelPaymentTypeButton.name = "cancelPaymentTypeButton";
    var cancelButtonText = document.createTextNode(invoicePaymentCancelLabel);
    cancelPaymentTypeButton.appendChild(cancelButtonText);
    if (!isUpdating) {
        cancelPaymentTypeButton.onclick = function () {
            cancelEdit();
            removePaymentEditRowCells();
            showAddPaymentTypeButtonRow();
        };
    } else {
        cancelPaymentTypeButton.onclick = function () {
            cancelEdit();
            showAddPaymentTypeButtonRow();
        };
    }

    var buttonsTable = document.createElement("table");
    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    td1.appendChild(savePaymentTypeButton);
    tr.appendChild(td1);

    var td2 = document.createElement('td');
    td2.appendChild(cancelPaymentTypeButton);
    tr.appendChild(td2);

    buttonsTable.appendChild(tr);
    return buttonsTable;
}

function showPaymentTypeRow(paymentTypeId, rowIndex) {
    var invoicePaymentsTable = document.getElementById("tableInvoicingPaymentType");
    var repInvoicePaymentType = repInvoicePaymentTypes[paymentTypeId];
    if ((invoicePaymentsTable) && (repInvoicePaymentType)) {
        var paymentTypeTableRow = invoicePaymentsTable.rows[rowIndex];
        if (paymentTypeTableRow) {
            paymentTypeTableRow.id = repInvoicePaymentType.id;
            removeRowCells(paymentTypeTableRow);
            showPaymentRecordRow(repInvoicePaymentType, paymentTypeTableRow.rowIndex);
        }
    }
}

/**
 * This function will add a row at the end of the invoice payment types table
 * with a button for adding new payment types
 * @returns {undefined}
 */
function showAddPaymentTypeButtonRow() {
    var invoicePaymentsTable = document.getElementById("tableInvoicingPaymentType");
    if (invoicePaymentsTable) {
        var addNewPaymentTypeRow = document.getElementById("addNewPaymentTypeRow");
        if (addNewPaymentTypeRow === null) {
            // Add new row with button
            var addNewPaymentTypeRow = invoicePaymentsTable.insertRow(invoicePaymentsTable.rows.length);
            addNewPaymentTypeRow.id = "addNewPaymentTypeRow";
            addNewPaymentTypeRow.insertCell(0);
            addNewPaymentTypeRow.insertCell(1);

            var addPaymentTypeButton = document.createElement("button");
            addPaymentTypeButton.id = "btnAddInvoicingPaymentType";
            addPaymentTypeButton.name = "btnAddInvoicingPaymentType";
            var addButtonText = document.createTextNode(invoiceAddPaymentLabel);
            addPaymentTypeButton.appendChild(addButtonText);
            addPaymentTypeButton.onclick = showNewPaymentTypeRow;

            var buttonCell = addNewPaymentTypeRow.insertCell(2);
            buttonCell.appendChild(addPaymentTypeButton);
        }
    }
}

function removeAddPaymentTypeButtonRow() {
    var addNewPaymentTypeRow = document.getElementById("addNewPaymentTypeRow");
    if (addNewPaymentTypeRow) {
        addNewPaymentTypeRow.parentNode.removeChild(addNewPaymentTypeRow);
    }
}


function validateAccountNumber() {
    var paymentTypeAccountInput = document.getElementById("paymentTypeAccount");
    var invoicePaymentTypesSelect = document.getElementById("invoicePaymentTypes");
    var retValue = true;
    if (paymentTypeAccountInput && invoicePaymentTypesSelect) {
        if (invoicePaymentTypesSelect.selectedIndex !== -1) {
            var accountSpanWarning = document.getElementById("accountSpanWarning");
            var savePaymentTypeButton = document.getElementById("savePaymentTypeButton");
            if ((accountSpanWarning) && (savePaymentTypeButton)) {
                if (invoicePaymentTypesSelect[invoicePaymentTypesSelect.selectedIndex].id === 'C'){
                    paymentTypeAccountInput.disabled = true;
                    hideElement(accountSpanWarning, true);
                    savePaymentTypeButton.disabled = false;                    
                }else if ((invoicePaymentTypesSelect[invoicePaymentTypesSelect.selectedIndex].id !== 'C') &&
                        ((isEmpty(paymentTypeAccountInput)) || (!validateRegEx(/\b\d{4,4}\b/, paymentTypeAccountInput)))) {
                    paymentTypeAccountInput.disabled = false;
                    hideElement(accountSpanWarning, false);
                    savePaymentTypeButton.disabled = true;
                    retValue = false;
                } else {
                    paymentTypeAccountInput.disabled = false;
                    hideElement(accountSpanWarning, true);
                    savePaymentTypeButton.disabled = false;
                    // retValue already true
                }
            }
        }
    }
    return retValue;
}


/**
 * This function will show a new row at the end of the invoice payment types 
 * table with two buttons, one for saving current invoice payment type data
 * and another for canceling the add operation
 * @returns {undefined}
 */
function showNewPaymentTypeRow() {
    removeAddPaymentTypeButtonRow();
    var invoicePaymentsTable = document.getElementById("tableInvoicingPaymentType");
    if (invoicePaymentsTable) {
        var newRowIndex = invoicePaymentsTable.rows.length;
        var addNewPaymentTypeRow = invoicePaymentsTable.insertRow(newRowIndex);
        var recordId = generateGuid();
        addNewPaymentTypeRow.id = recordId;
        var paymentTypeCell = addNewPaymentTypeRow.insertCell(0);
        paymentTypeCell.appendChild(generateInvoicePaymentsSelect());
        var accountNoCell = addNewPaymentTypeRow.insertCell(1);
        var accountInput = generateInvoicePaymentAccountInput("");
        accountNoCell.appendChild(accountInput);
        var accountSpanWarning = generateAccountWarningSpan();
        accountNoCell.appendChild(accountSpanWarning);

        var buttonsCell = addNewPaymentTypeRow.insertCell(2);
        var buttonsTable = generateSaveCancelButtonsTable(false, recordId,
                newRowIndex);

        currentEditPaymentId = recordId;
        currentEditPaymentRowIndex = newRowIndex;
        isEdditing = true;

        buttonsCell.appendChild(buttonsTable);
    }
}

/**
 * 
 * @param {type} id
 * @param {type} localRepId
 * @param {type} typeId
 * @param {type} typeDescription
 * @param {type} accountNumber
 * @returns {RepInvoicePaymentType}
 */
function  RepInvoicePaymentType(id, localRepId, typeId, typeDescription, accountNumber) {
    this.id = id;
    this.repId = localRepId;
    this.typeId = typeId;
    this.typeDescription = typeDescription;
    this.accountNumber = accountNumber;
}

/**
 * 
 * @param {int} tableRowIndex
 * @param {int} paymentId
 * @returns {undefined}
 */
function editInvoicingPayment(tableRowIndex, paymentId) {
    var invoicePaymentsTable = document.getElementById("tableInvoicingPaymentType");
    if (invoicePaymentsTable) {
        var currentRow = invoicePaymentsTable.rows[tableRowIndex];
        removeRowCells(currentRow);
        // Get rep Payment type record
        var repPaymentType = repInvoicePaymentTypes[paymentId];
        // Put edit fields to the row
        // Input type select for invoice payment types
        var paymentTypeCell = currentRow.insertCell(0);
        var invoicePaymentSelect = generateInvoicePaymentsSelect();
        invoicePaymentSelect.value = repPaymentType.typeId;
        paymentTypeCell.appendChild(invoicePaymentSelect);
        // Account number
        var accountNumberCell = currentRow.insertCell(1);
        var accountInput = generateInvoicePaymentAccountInput(repPaymentType.accountNumber);
        accountNumberCell.appendChild(accountInput);
        var accountSpanWarning = generateAccountWarningSpan();
        accountNumberCell.appendChild(accountSpanWarning);

        // Buttons
        var editButtonsCell = currentRow.insertCell(2);
        var buttonsTable = generateSaveCancelButtonsTable(true, paymentId,
                tableRowIndex);
        editButtonsCell.appendChild(buttonsTable);
        currentEditPaymentId = paymentId;
        currentEditPaymentRowIndex = tableRowIndex;
        isEdditing = true;
    }
}

/**
 * 
 * @param {type} tableRowIndex
 * @param {type} paymentTypeId
 * @returns {undefined}
 */
function deleteInvoicingPayment(tableRowIndex, paymentTypeId) {
    delete repInvoicePaymentTypes[paymentTypeId];
    var invoicePaymentsTable = document.getElementById("tableInvoicingPaymentType");
    if (invoicePaymentsTable) {
        invoicePaymentsTable.deleteRow(tableRowIndex);
    }
}

/**
 * 
 * @param {type} newInvoicePaymentType
 * @param {type} rowIndex
 * @returns {undefined}
 */
function showPaymentRecordRow(newInvoicePaymentType, rowIndex) {
    var invoicePaymentsTable = document.getElementById("tableInvoicingPaymentType");
    if ((newInvoicePaymentType) && (invoicePaymentsTable) && (rowIndex)) {
        var lastRowClass = invoicePaymentsTable.rows[rowIndex - 1].className;
        var newRowClass = 'row1';
        if (lastRowClass === 'row1') {
            newRowClass = 'row2';
        }
        var newPaymentTypeRow = invoicePaymentsTable.rows[rowIndex];
        if (!newPaymentTypeRow) {
            newPaymentTypeRow = invoicePaymentsTable.insertRow(rowIndex);
        }
        newPaymentTypeRow.className = newRowClass;
        newPaymentTypeRow.id = newInvoicePaymentType.id;
        var paymentTypeCell = newPaymentTypeRow.insertCell(0);
        paymentTypeCell.innerHTML = newInvoicePaymentType.typeDescription;
        paymentTypeCell.className = 'simpleCenteredText';

        var accountNumberCell = newPaymentTypeRow.insertCell(1);
        accountNumberCell.innerHTML = newInvoicePaymentType.accountNumber;
        accountNumberCell.className = 'simpleCenteredText';

        var buttonsCell = newPaymentTypeRow.insertCell(2);
        buttonsCell.align = 'center';

        var buttonsTable = generateEditButtonsTable(newPaymentTypeRow.rowIndex,
                newInvoicePaymentType.id);
        buttonsCell.appendChild(buttonsTable);
    }
}


/**
 * From the input values in the table, generates a rep invoice type payment record
 * @param {type} paymentId
 * @returns {RepInvoicePaymentType}
 */
function getRepInvoicePaymentTypeFromTable(paymentId) {
    var invoicePaymentTypes = document.getElementById("invoicePaymentTypes");
    var selectedPayment = invoicePaymentTypes.options[invoicePaymentTypes.selectedIndex];

    var paymentTypeAccount = document.getElementById("paymentTypeAccount");
    var tableInvoicePaymentType = new RepInvoicePaymentType(paymentId, repId, selectedPayment.value,
            selectedPayment.textContent, paymentTypeAccount.value);
    return tableInvoicePaymentType;
}


/**
 * Updates or add a record to the dictionary from the data found int the table
 * @param {type} paymentId
 * @returns {undefined}
 */
function addUpdatePaymentTypeRecord(paymentId)
{
    var updatedInvoicePaymentType = getRepInvoicePaymentTypeFromTable(paymentId);
    if (updatedInvoicePaymentType.id in repInvoicePaymentTypes) { // edditing record
        var currentPaymentType = repInvoicePaymentTypes[updatedInvoicePaymentType.id];
        if (currentPaymentType) {
            currentPaymentType.repId = updatedInvoicePaymentType.repId;
            currentPaymentType.typeId = updatedInvoicePaymentType.typeId;
            currentPaymentType.typeDescription = updatedInvoicePaymentType.typeDescription;
            currentPaymentType.accountNumber = updatedInvoicePaymentType.accountNumber;
        }
    } else { // Adding record
        repInvoicePaymentTypes[updatedInvoicePaymentType.id] = updatedInvoicePaymentType;
    }
    return updatedInvoicePaymentType;
}

function updatePaymentTypeRecord(paymentId, rowIndex) {
    // Get new record values
    var updatedInvoicePaymentType = addUpdatePaymentTypeRecord(paymentId);
    removePaymentEditRowCells();
    showPaymentRecordRow(updatedInvoicePaymentType, rowIndex);
}

function addInvoicingTypesResult() {
    if (repInvoicePaymentTypes) {
        var repInvoiceTypesList = document.getElementById("repInvoiceTypesList");
        repInvoiceTypesList.value = JSON.stringify(repInvoicePaymentTypes);
    }
}