/* 
 * EMIDA all rights reserved 1999-2019.
 */


/* global   invalidCommissionValueMsg, invalidPaymentAmountMsg, sessionLanguage,defaultErrorMessage,
 paymentResultDialogTitle, invalidDepositDateLbl, emptyDocumentNumberLbl, todayLbl, previousShortLbl, nextShortLbl,
 repDayLongSu, repDayLongMo, repDayLongTu, repDayLongWe, repDayLongTh, repDayLongFr, repDayLongSa,
 repDaySu, repDayMo, repDayTu, repDayWe, repDayTh, repDayFr, repDaySa,
 repMonthJan, repMonthFeb, repMonthMar, repMonthApr, repMonthMay, repMonthJun, repMonthJul, repMonthAug, repMonthSep, repMonthOct, repMonthNov, repMonthDec,
 repMonthShrtJan, repMonthShrtFeb, repMonthShrtMar, repMonthShrtApr, repMonthShrtMay, repMonthShrtJun, repMonthShrtJul, repMonthShrtAug, repMonthShrtSep, repMonthShrtOct, repMonthShrtNov, repMonthShrtDec
 
 
 */

$(document).ready(function () {

    $("#paymentResultDialog").dialog({
        modal: true,
        autoOpen: false,
        buttons: {
            Ok: function () {
                $(this).dialog("close");                
            }
        }
    });

    // paymentForm only exists when addign reps
    $("#paymentForm").submit(function (event) {
        event.preventDefault();
        if (validatePaymentForm()) {
            
            jQuery.ajax({
                url: 'admin/customers/repPaymentProcess.jsp',
                method: 'POST',
                data: $('#paymentForm').serialize()
            }).done(function (response) {
                var isPaymentSuccess = false;
                if (response.resultCode === "000") {
                    $('#paymentResultDialogIcon').addClass("ui-icon-check");
                    $('#paymentResultDialogMessage').text(response.resultMessage);
                    isPaymentSuccess = true;
                } else {
                    $('#paymentResultDialogIcon').addClass("ui-icon-notice");
                    $('#paymentResultDialogMessage').text(response.resultMessage);
                }
                $('#paymentResultDialog').dialog("open");
                if(isPaymentSuccess){
                    $("#frmRepPaymentMx").dialog( "close" );
                }
            }).fail(function () {
                $('#paymentResultDialogIcon').addClass("ui-icon-notice");
                $('#paymentResultDialogMessage').text(defaultErrorMessage);
                $('#paymentResultDialog').dialog("open");
            });
        }
    });

    var regional = (sessionLanguage === "spanish") ? 'es' : 'en';
    $.datepicker.regional[regional] = {
        clearText: 'Effacer', clearStatus: '',
        closeText: '',
        prevText: "&#x3C;" + previousShortLbl,
        nextText: nextShortLbl + "&#x3E;",
        currentText: todayLbl,
        monthNames: [repMonthJan, repMonthFeb, repMonthMar, repMonthApr, repMonthMay, repMonthJun, repMonthJul, repMonthAug, repMonthSep, repMonthOct, repMonthNov, repMonthDec],
        monthNamesShort: [repMonthShrtJan, repMonthShrtFeb, repMonthShrtMar, repMonthShrtApr, repMonthShrtMay, repMonthShrtJun, repMonthShrtJul, repMonthShrtAug, repMonthShrtSep, repMonthShrtOct, repMonthShrtNov, repMonthShrtDec],
        dayNames: [repDayLongSu, repDayLongMo, repDayLongTu, repDayLongWe, repDayLongTh, repDayLongFr, repDayLongSa],
        dayNamesShort: [repDaySu, repDayMo, repDayTu, repDayWe, repDayTh, repDayFr, repDaySa],
        dayNamesMin: [repDaySu, repDayMo, repDayTu, repDayWe, repDayTh, repDayFr, repDaySa],
        weekHeader: "Sm",
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
    };
    $.datepicker.setDefaults($.datepicker.regional[regional]);


    $("#depositDate").datepicker({
        maxDate: '0'}
    );

    //$("#depositDate").change(isDepositDateValueOk);
});


function clearNetPaymentValues() {
    // Clear payment formula values
    $('#netPaymentValue').text("");
    $('#netPaymentCommission').text("0");
    $('#netPaymentTotal').text("0");
    // Clear hidden fields
    $('#finalPaymentAmount').val("0");
    $('#finalAmount').val("0");
    $('#finalCommission').val("0");
}


function clearRepPaymentForm() {
    $('#commissionByPercentage').prop('checked', true);
    $('#commissionValue').val("0");
    $('#commissionValueError').text("");
    $('#paymentValue').val("");
    $('#paymentValueError').text("");
    clearNetPaymentValues();
    $('#depositDate').datepicker('setDate', null);
    $('#depositDateError').text("");
    $('#receiptType').prop('selectedIndex', 0);
    $('#banks').prop('selectedIndex', 0);
    $('#documentNumber').val("");
    $('#documentNumberError').text("");
    onBankChange();
    $('#accountNumber').val("");
    $('#accountNumberError').text("");
    $('#paymentComments').val("");
    $('#paymentResultMessage').text("");
    $('#paymentResultMessage').removeClass();    
}


/*
 * Validates that the commission value is numeric
 * and if a percentage is selected, the value must be less than 100
 * @returns {undefined}
 **/
function isCommissionValueOk()
{
    var commissionValueField = document.getElementById("commissionValue");
    var errorField = document.getElementById("commissionValueError");
    var retValue = false;

    if ((!isEmpty(commissionValueField) && (isCurrency(commissionValueField))))
    {
        var commissionValue = parseFloat($('#commissionValue').val());
        var isTypePercentaje = ($('#commissionByPercentage').attr("checked") === "checked");

        if ((commissionValue < 0.0) ||
                (isTypePercentaje && (commissionValue >= 100.0))) {
            setErrorStyle(commissionValueField, errorField, invalidCommissionValueMsg,
                    true, true);
        } else {
            setErrorStyle(commissionValueField, errorField, '',
                    false, false);
        }
        retValue = true;
    } else {
        setErrorStyle(commissionValueField, errorField, invalidCommissionValueMsg,
                true, true);
    }
    return retValue;
}

function isPaymentValueOk()
{
    var paymentValueField = document.getElementById("paymentValue");
    var errorField = document.getElementById("paymentValueError");
    var retValue = false;

    if ((!isEmpty(paymentValueField) && (isCurrency(paymentValueField))))
    {
        var paymentValue = parseFloat($('#paymentValue').val());
        
        if (paymentValue === 0.0) {
            clearRepPaymentForm();
        } else {
            setErrorStyle(paymentValueField, errorField, '',
                    false, false);
        }
        retValue = true;
    } else {
        setErrorStyle(paymentValueField, errorField, invalidPaymentAmountMsg,
                true, true);
    }
    return retValue;
}


function isDepositDateValueOk()
{
    var depositDateField = document.getElementById("depositDate");
    var errorField = document.getElementById("depositDateError");
    var retValue = false;

    if (validateDate(depositDateField))
    {
        setErrorStyle(depositDateField, errorField, '',
                false, false);
        retValue = true;
    } else {
        setErrorStyle(depositDateField, errorField, invalidDepositDateLbl,
                true, true);
    }
    return retValue;
}


function isDocumentNumberValueOk()
{
    var documentNumberField = document.getElementById("documentNumber");
    var errorField = document.getElementById("documentNumberError");
    var retValue = false;

    if (!isEmpty(documentNumberField))
    {
        setErrorStyle(documentNumberField, errorField, '',
                false, false);
        retValue = true;
    } else {
        setErrorStyle(documentNumberField, errorField, emptyDocumentNumberLbl,
                true, true);
    }
    return retValue;
}

/**
 * Pre: Commission amount and payment amount are already valid
 */
function calculateNetAmount() {
    if (isCommissionValueOk() && isPaymentValueOk()) {
        var commissionValue = parseFloat($('#commissionValue').val());
        var isCommissionTypePercentaje = ($('#commissionByPercentage').attr("checked") === "checked");
        var paymentValue = parseFloat($('#paymentValue').val());

        var newCommissionValue = (isCommissionTypePercentaje) ? paymentValue * (commissionValue / 100) : commissionValue;
        var commissionPercentage = (isCommissionTypePercentaje) ? commissionValue : (commissionValue / paymentValue);

        // Set view values
        $('#netPaymentValue').text(paymentValue.toFixed(2).toString());
        $('#netPaymentCommission').text(newCommissionValue.toFixed(2).toString());
        $('#netPaymentTotal').text((paymentValue - newCommissionValue).toFixed(2).toString());
        // rep credits assumes the commission value always as percentage
        $('#finalAmount').val(paymentValue.toString());
        $('#finalCommission').val(commissionPercentage.toString());
        return true;
    } else {
        clearNetPaymentValues();
        return false;
    }
}

function validatePaymentForm() {
    if (($('#repPaymentRepId').val !== '') & (calculateNetAmount()) & (isDepositDateValueOk()) & (isDocumentNumberValueOk())) {
        return true;
    } else {
        return false;
    }
}

function onBankChange() {
    var $banksSelect = $('#banks');
    var postParams = "action=getDepositTypesByBankId&bankIds=" + $banksSelect.val();
    $.post("admin/customers/repPaymentProcess.jsp", postParams
            , function (result) {
                var $depositTypesSelect = $('#depositTypes');
                $depositTypesSelect.find('option').remove();
                result.forEach(function (depositType) {
                    $depositTypesSelect.append('<option id="' + depositType.depositCode + '"  value="' + depositType.id + '">' + ((sessionLanguage === "spanish") ? depositType.descriptionSpanish : depositType.descriptionEnglish) + '</option>');
                });
            });
}