/* 
 * EMIDA all rights reserved 1999-2019.
 */


/* global REP_CREDIT_TYPE_UNLIMITED, REP_CREDIT_TYPE_CREDIT, REP_CREDIT_TYPE_FLEXIBLE, REP_CREDIT_TYPE_PREPAID, CUSTOM_CONFIG_TYPE_MEXICO, currentCustomConfigType */


var form = "";
var submitted = false;
var error = false;
var error_message = "";


$(document).ready(function(){
    checkCreditLimitBox();
});



function check_input(field_name, field_size, message) {
    if (form.elements[field_name] && (form.elements[field_name].type !== "hidden"))
    {
        var field_value = form.elements[field_name].value;

        if (field_value === '' || field_value.length < field_size) {
            error_message = error_message + "* " + message + "\n";
            error = true;
        }
    }
}

function formatAmount(n)
{
    var s = "" + Math.round(n * 100) / 100;
    var i = s.indexOf('.');
    if (i < 0) {
        return s + ".00";
    }
    var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3);
    if (i + 2 === s.length) {
        t += "0";
    }
    return t;
}

function verifyAba(lengthText, expression, textInfo) {

    var optionAbaCk = document.getElementById('optionAba').checked;
    if (optionAbaCk === true) {
        return true;
    }

    var valueAba = document.getElementById('routingNumber').value;
    var isValid = verifyNumericText(valueAba, expression);
    var isValidLength = false;
    var isNoAllZero = false;
    var isValidSQL = false;

    if (isValid === true && valueAba.length === 9) {
        isValidLength = true;
        isNoAllZero = verifyNoAllZero(valueAba);
        if (isNoAllZero === true) {
            isValidSQL = validationSQL(valueAba);
        }
    }

    if (isValid === true && isValidLength === true && isNoAllZero === true && isValidSQL === true) {
        var div_info = document.getElementById("div_info_aba");
        div_info.style.color = "black";
        div_info.innerHTML = "";
        return true;
    } else {
        var div_info = document.getElementById("div_info_aba");
        div_info.style.width = "200px";
        div_info.style.color = "red";
        div_info.innerHTML = "";
        var newdiv = document.createElement("div");
        var newtext = document.createTextNode(textInfo);
        newdiv.appendChild(newtext); //append text to new div
        div_info.appendChild(newdiv);
        return false;
    }
}

function verifyAccount(lengthText, textInfo) {
    var optionAbaCk = document.getElementById('optionAccount').checked;
    if (optionAbaCk === true) {
        return true;
    }
    var valueAccount = document.getElementById('accountNumber').value;
    var isValid = verifyNumericText(valueAccount, '/^[0-9]{1,' + lengthText + '}$/');
    var isValidLength = false;
    var isNoAllZero = false;
    if (isValid === true && valueAccount.length <= 20) {
        isValidLength = true;
        isNoAllZero = verifyNoAllZero(valueAccount);
    }

    if (isValid === true && isValidLength === true && isNoAllZero === true) {
        var div_info = document.getElementById("div_info_account");
        div_info.style.color = "black";
        div_info.innerHTML = "";
        return true;
    } else {
        var div_info = document.getElementById("div_info_account");
        div_info.style.width = "200px";
        div_info.style.color = "red";
        div_info.innerHTML = "";
        var newdiv = document.createElement("div");
        var newtext = document.createTextNode(textInfo);
        newdiv.appendChild(newtext); //append text to new div
        div_info.appendChild(newdiv);
        return false;
    }

}

// EXEC THE REGULAR EXPRESSIONS
function verifyNumericText(text, exp) {
    exp = exp.replace("/", "");
    exp = exp.replace("/", "");
    exp = exp.replace(/\\/g, "\\");
    var patt1 = new RegExp(exp);
    var result = patt1.exec(text);
    if (result !== null) {
        return true;
    }
    return false;
}

// VERIFY THAT AN TEXT NOT CONTAIN ALL ZEROS
function verifyNoAllZero(text) {
    var i = 0;
    for (i = 0; i < text.length; i++) {
        if (text.charAt(i) !== '0') {
            return true;
        }
    }
    return false;
}

//VALIDATION FOR ABA MERCHANT WITH FORMAT OF THE DB
function validationSQL(text) {

    var digit1 = parseInt(text.charAt(0)) * 3;
    var digit2 = parseInt(text.charAt(1)) * 7;
    var digit3 = parseInt(text.charAt(2)) * 1;
    var digit4 = parseInt(text.charAt(3)) * 3;
    var digit5 = parseInt(text.charAt(4)) * 7;
    var digit6 = parseInt(text.charAt(5)) * 1;
    var digit7 = parseInt(text.charAt(6)) * 3;
    var digit8 = parseInt(text.charAt(7)) * 7;
    var digit9 = parseInt(text.charAt(8));

    var sumDigits = digit1 + digit2 + digit3 + digit4 + digit5 + digit6 + digit7 + digit8;
    var mod10 = sumDigits % 10;
    var totalOP = 10 - mod10;
    var resultDigit = getRightDigits(totalOP, 1);

    if (resultDigit === digit9) {
        return true;
    } else {
        return false;
    }
}

// GET AN NUMBER DIGITS THE ONE NUMBER
function getRightDigits(num, digits) {
    var i = 0;
    var numString = num.toString();
    var realNumStr = '';
    for (i = (numString.length - digits); i < numString.length; i++) {
        realNumStr = realNumStr + numString.charAt(i);
    }
    var realNum = parseInt(realNumStr);
    return realNum;
}

function changeCheckAba() {
    var optionAbaCk = document.getElementById('optionAba').checked;
    if (optionAbaCk === true) {
        var div_info = document.getElementById("div_info_aba");
        div_info.style.color = "black";
        div_info.innerHTML = "";

        var valueAba = document.getElementById('routingNumber');
        valueAba.value = 'NA';
        valueAba.disabled = true;
    } else {
        var valueAba = document.getElementById('routingNumber');
        valueAba.value = '';
        valueAba.disabled = false;
    }
}

function changeCheckAccount() {
    var optionAbaCk = document.getElementById('optionAccount').checked;
    if (optionAbaCk === true) {
        var div_info = document.getElementById("div_info_account");
        div_info.style.color = "black";
        div_info.innerHTML = "";

        var valueAccount = document.getElementById('accountNumber');
        valueAccount.value = 'NA';
        valueAccount.disabled = true;
    } else {
        var valueAccount = document.getElementById('accountNumber');
        valueAccount.value = '';
        valueAccount.disabled = false;
    }
}

function EnableBillingAddress(bChecked) {
    document.getElementById('mailAddress').readOnly = bChecked;
    document.getElementById('mailColony').readOnly = bChecked;
    document.getElementById('mailDelegation').readOnly = bChecked;
    document.getElementById('mailCity').readOnly = bChecked;
    document.getElementById('mailZip').readOnly = bChecked;
    document.getElementById('mailState').disabled = bChecked;
    document.getElementById('mailCounty').readOnly = bChecked;
    document.getElementById('mailCountry').disabled = bChecked;
    if (bChecked) {
        document.getElementById('mailAddress').value = document.getElementById('address').value;
        document.getElementById('mailColony').value = document.getElementById('physColony').value;
        document.getElementById('mailDelegation').value = document.getElementById('physDelegation').value;
        document.getElementById('mailCity').value = document.getElementById('city').value;
        document.getElementById('mailZip').value = document.getElementById('zip').value;
        document.getElementById('mailState').value = document.getElementById('state').value;
        document.getElementById('mailCounty').value = document.getElementById('physCounty').value;
        document.getElementById('mailCountry').value = document.getElementById('country').value;
    } else {
        document.getElementById('mailAddress').value = '';
        document.getElementById('mailColony').value = '';
        document.getElementById('mailDelegation').value = '';
        document.getElementById('mailCity').value = '';
        document.getElementById('mailZip').value = '';
        document.getElementById('mailCounty').value = '';
        document.getElementById('mailCountry').value = '';
    }
}//End of function EnableBillingAddress

//DTU-369 Payment Notifications
function changePayNoti()
{
    var payNoti = document.getElementById('paymentNotifications').checked;
    if (payNoti === true)
    {
        var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
        valuePayNotiSMSChk.checked = false;
        valuePayNotiSMSChk.disabled = false;
        var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
        valuePayNotiMailChk.checked = false;
        valuePayNotiMailChk.disabled = false;
    } else
    {
        var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
        valuePayNotiSMSChk.checked = false;
        valuePayNotiSMSChk.disabled = true;
        var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
        valuePayNotiMailChk.checked = false;
        valuePayNotiMailChk.disabled = true;
    }
}

function changePayNotiSMS()
{
    var payNotiSMS = document.getElementById('paymentNotificationSMS').checked;
    var valuePayNotiMailChk = document.getElementById('paymentNotificationMail');
    if (payNotiSMS === true)
    {
        valuePayNotiMailChk.checked = false;
    } else
    {
        valuePayNotiMailChk.checked = true;
    }
}

function changePayNotiMail()
{
    var payNotiMail = document.getElementById('paymentNotificationMail').checked;
    var valuePayNotiSMSChk = document.getElementById('paymentNotificationSMS');
    if (payNotiMail === true)
    {
        valuePayNotiSMSChk.checked = false;
    } else
    {
        valuePayNotiSMSChk.checked = true;
    }
}

//DTU-480: Low Balance Alert Message SMS and or email
function changeBalNoti() {
    var balNoti = document.getElementById('balanceNotifications').checked;
    if (balNoti === true) {
        var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
        valueBalNotiDaysChk.checked = false;
        valueBalNotiDaysChk.disabled = false;
        var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
        valueBalNotiValueChk.checked = false;
        valueBalNotiValueChk.disabled = false;
        var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
        valueBalNotiSMSChk.checked = false;
        valueBalNotiSMSChk.disabled = false;
        var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
        valueBalNotiMailChk.checked = false;
        valueBalNotiMailChk.disabled = false;
        var valueBalNotiValue = document.getElementById('balanceNotificationValue');
        var valueBalNotiDays = document.getElementById('balanceNotificationDays');
        valueBalNotiValue.disabled = true;
        valueBalNotiDays.disabled = true;
    } else {
        var valueBalNotiDaysChk = document.getElementById('balanceNotificationTypeDays');
        valueBalNotiDaysChk.checked = false;
        valueBalNotiDaysChk.disabled = true;
        var valueBalNotiValueChk = document.getElementById('balanceNotificationTypeValue');
        valueBalNotiValueChk.checked = false;
        valueBalNotiValueChk.disabled = true;
        var valueBalNotiSMSChk = document.getElementById('balanceNotificationSMS');
        valueBalNotiSMSChk.checked = false;
        valueBalNotiSMSChk.disabled = true;
        var valueBalNotiMailChk = document.getElementById('balanceNotificationMail');
        valueBalNotiMailChk.checked = false;
        valueBalNotiMailChk.disabled = true;
        var valueBalNotiValue = document.getElementById('balanceNotificationValue');
        var valueBalNotiDays = document.getElementById('balanceNotificationDays');
        valueBalNotiValue.disabled = true;
        valueBalNotiDays.disabled = true;
    }
}

function changeBalNotiValue()
{
    var balNotiValue = document.getElementById('balanceNotificationTypeValue').checked;
    var valueBalNotiValue = document.getElementById('balanceNotificationValue');
    if (balNotiValue === true)
    {
        valueBalNotiValue.disabled = false;
    } else
    {
        valueBalNotiValue.disabled = true;
    }
}

function changeBalNotiDays()
{
    var balNotiDays = document.getElementById('balanceNotificationTypeDays').checked;
    var valueBalNotiDays = document.getElementById('balanceNotificationDays');
    if (balNotiDays === true)
    {
        valueBalNotiDays.disabled = false;
    } else
    {
        valueBalNotiDays.disabled = true;
    }
}

function changeBalNotiSMS()
{
    var balNotiSMS = document.getElementById('balanceNotificationSMS').checked;
    var valueBalNotiMail = document.getElementById('balanceNotificationMail');
    if (balNotiSMS === true)
    {
        valueBalNotiMail.checked = false;
    } else
    {
        valueBalNotiMail.checked = true;
    }
}

function changeBalNotiMail()
{
    var balNotiMail = document.getElementById('balanceNotificationMail').checked;
    var valueBalNotiSMS = document.getElementById('balanceNotificationSMS');
    if (balNotiMail === true)
    {

        valueBalNotiSMS.checked = false;
    } else
    {
        valueBalNotiSMS.checked = true;
    }
}

function mxEditContact()
{
    addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
    document.getElementById('mxContactAction').value = "EDIT";
    document.rep.onsubmit = new Function("return true;");
    document.rep.submit.disabled = true;
}

function mxDeleteContact()
{
    addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
    document.getElementById('mxContactAction').value = "DELETE";
    document.rep.onsubmit = new Function("return true;");
    document.rep.submit.disabled = true;
}

function mxAddContact()
{
    document.getElementById('mxContactAction').value = "ADD";
    document.getElementById('mxBtnAddContact').disabled = true;
    addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
    document.rep.onsubmit = new Function("return true;");
    document.rep.submit.click();
    document.rep.submit.disabled = true;
}

function mxCancelSaveContact()
{
    document.getElementById('mxContactAction').value = "CANCEL";
    document.getElementById('mxBtnSaveContact').disabled = true;
    document.getElementById('mxBtnCancelContact').disabled = true;
    document.rep.onsubmit = new Function("return true;");
    addInvoicingTypesResult(); // repInvoicePaymentTypesCommonCode.jspf
    document.rep.submit.click();
    document.rep.submit.disabled = true;
}


function checkCreditLimitBox() {

    switch ($('#lstRepCreditType').val()) {
        case REP_CREDIT_TYPE_UNLIMITED:
            $('#trCreditLimit').hide();
            break;
        case REP_CREDIT_TYPE_CREDIT:
            $('#trCreditLimit').show();
            $('#tdMxPrepaidPayment').hide();
            $('#tdCommonPayment').show();
            break;
        case REP_CREDIT_TYPE_FLEXIBLE:
            $('#trCreditLimit').show();
            $('#tdMxPrepaidPayment').hide();
            $('#tdCommonPayment').show();
            break;
        case REP_CREDIT_TYPE_PREPAID:
            $('#trCreditLimit').show();
            if (currentCustomConfigType === CUSTOM_CONFIG_TYPE_MEXICO) {
                $('#tdMxPrepaidPayment').show();
                $('#tdCommonPayment').hide();
            } else {
                $('#tdMxPrepaidPayment').hide();
                $('#tdCommonPayment').show();
            }
            break;
    }
}
