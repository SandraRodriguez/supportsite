new Vue({
    el: '#ccodes',
    data: {
        model: {},

        dialogVisible: false,
        notify: '',
        clerkName: '',
        isValidNotfy: false
    },
    mounted: function () {
        $('.el-dialog__header').hide();

        let uri = window.location.search.substring(1);
        let params = new URLSearchParams(uri);
        let merchantId = params.get("merchantId");
        let siteId = params.get("siteId");

        localStorage.setItem('ccodemerchantId', merchantId);
        localStorage.setItem('ccodesiteId', siteId);
    },
    methods: {
        showModal(clerkNm) {                        
            var merchantId = localStorage.getItem('ccodemerchantId');
            this.clerkName = clerkNm;
            this.findBy(merchantId);
        },
        onReset() {            
            this.addForgotPasswordNotification();
            this.dialogVisible = false;
        },
        findBy(merchantId) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/admin/findMerchantBy?merchantId=' + merchantId,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.model = data;
                self.dialogVisible = true;

                if (data.smsNotificationPhone !== '' || data.mailNotificationAddress !== '') {
                    self.isValidNotfy = true;
                    if (data.mailNotificationAddress !== '') {
                        self.notify = 'email';
                    } else {
                        self.notify = 'sms';
                    }
                } else {
                    self.isValidNotfy = false;
                }
            });
        },    
        addForgotPasswordNotification() {
            let merchantId = localStorage.getItem('ccodemerchantId');
            let siteId = localStorage.getItem('ccodesiteId');
            let self = this;
            var subscriber = self.model.smsNotificationPhone;
            if(self.notify === 'email') {
                subscriber = self.model.mailNotificationAddress;
            }            
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'POST',
                url: '/support/admin/addForgotPasswordNotification',
                contentType: 'application/json',
                data: JSON.stringify({
                    entity_reset: merchantId,
                    millennium_no: siteId,
                    channel: self.notify,
                    clerkName: self.clerkName,
                    subscriber: subscriber
                }),
                async: true,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.showOKMessage();
            });
        },
        showOKMessage() {
            let merchantId = localStorage.getItem('ccodemerchantId');
            let siteId = localStorage.getItem('ccodesiteId');
            swal({
                title: "Good job!",
                text: " ",
                icon: "success",
                timer: 1500,
                buttons: false
            });
            setTimeout(function () {
                window.location.href = '/support/admin/customers/merchants_clerk_codes.jsp?merchantId=' + merchantId + '&siteId=' + siteId;
            }, 1600);
        }
    }
});