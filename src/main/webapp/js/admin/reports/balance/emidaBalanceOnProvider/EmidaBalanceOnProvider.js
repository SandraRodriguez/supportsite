/* 
 * EMIDA all rights reserved 1999-2019.
 */

// Check call to setJsInvoicingGlobals : these are javascrip global variables taken from the resource bundle
/* global noProviderSelectedError, invalidDates, repApplyLbl, repCancelLbl, repFromLbl, repToLbl, repCustomLbl, repDaySu, repDayMo, repDayTu, repDayWe, repDayTh, repDayFr, repDaySa, repMonthJan, repMonthFeb, repMonthMar, repMonthMay, repMonthApr, repMonthJun, repMonthJul, repMonthAug, repMonthSep, repMonthOct, repMonthNov, repMonthDec, tblSearchLbl, tblNoDataLbl, tblNoticeContractsShowingLbl, tblNoticeContractsOfLbl, tblNoticeContractsRegisterLbl, tblFirstLbl, tblNextLbl, tblPreviousLbl, tblLastLbl */
function dateToYMD(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d) + '/' + '' + y;
}

var fromDate = null;
var toDate = null;



function getProviderList() {
    var selectedProviders = [];
    if ($('#providerListBox').val() !== null) {
        $("#providerListBox :selected").each(function () {
            selectedProviders.push($(this).val());
        });
    }
    return selectedProviders;
}


function areFormParametersValid() {
    var retValue = true;
    if ($('#providerListBox').val() === null) {
        $("#errorMessageIdSpan").text(noProviderSelectedError);
        $("#errorMessageIdDiv").show();
        retValue = false;
    } else if (fromDate === 'null' || toDate === 'null') {
        $("#errorMessageIdSpan").text(invalidDates);
        $("#errorMessageIdDiv").show();
        retValue = false;
    } else {
        $("#errorMessageIdSpan").text('');
        $("#errorMessageIdDiv").hide();
    }
    return retValue;
}

/**
 * Funtion to be used when parameters change, so current data corresponds to 
 * a different filter values
 * @returns void
 */
function clearData() {
    $(".tblEmidaBalance").hide();
    $(".btnReportDownloader").hide();
}


$(document).ready(function () {
    $('#providerListBox').multiSelect({
        selectableHeader: "<input type='text' class='search-input' autocomplete='off'>",
        selectionHeader: "<input type='text' class='search-input' autocomplete='off'>",
        afterInit: function (ms) {
            var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
        },
        afterDeselect: function () {
            clearData();
        }
    });

    $(".logtx").hide();
    $(".btnsearch").prop('disabled', true);
    $('#tblEmidaBalanceId').DataTable();
    $(".divErrMessage").hide();

    fromDate = dateToYMD(new Date());
    toDate = dateToYMD(new Date());

    $(".tblEmidaBalance").hide();
    $(".btnReportDownloader").hide();

    $(".btnsearch").click(function () {
        if (areFormParametersValid()) {
            getEmidaBalanceOnProviderResult();
        } //else : Error messages shown during validation
    });

    $(".btnReportDownloader").click(function () {

        if (areFormParametersValid()) {
            var url = '/support/reports/balance/emidaBalanceOnProvider/download?';
            url += 'startDate=' + fromDate;
            url += '&endDate=' + toDate;
            var providerList = getProviderList();
            var arrayLength = providerList.length;
            if (arrayLength > 0) {
                for (var i = 0; i < arrayLength; i++) {
                    url += '&providerList=' + providerList[i];
                }
            } else {
                url += '&providerList=';
            }
            window.location.href = url;
        }
    }
    );
    $("#activlink").click(function (event) {
        event.preventDefault();
    });
});



// Datetime picker
$(document).ready(function () {
    $('input[name="daterange"]').daterangepicker({
        "showDropdowns": true,
        "showWeekNumbers": true,
        "locale": {
            "format": "MM/DD/YYYY",
            "separator": " - ",
            "applyLabel": repApplyLbl,
            "cancelLabel": repCancelLbl,
            "fromLabel": repFromLbl,
            "toLabel": repToLbl,
            "customRangeLabel": repCustomLbl,
            "daysOfWeek": [
                repDaySu,
                repDayMo,
                repDayTu,
                repDayWe,
                repDayTh,
                repDayFr,
                repDaySa
            ],
            "monthNames": [
                repMonthJan,
                repMonthFeb,
                repMonthMar,
                repMonthApr,
                repMonthMay,
                repMonthJun,
                repMonthJul,
                repMonthAug,
                repMonthSep,
                repMonthOct,
                repMonthNov,
                repMonthDec
            ],
            "firstDay": 1
        },
        endDate: moment(),
        maxDate: moment()
    }, function (start, end, label) {
        fromDate = start.format('MM/DD/YYYY');
        toDate = end.format('MM/DD/YYYY');
        if ((fromDate === null && toDate === null) || ($('#providerListBox').val() === null)) {
            $(".btnsearch").prop('disabled', false);
        }        
    });
});

function  buildTable(data) {
    $('#tblEmidaBalanceId').DataTable({
        "aaData": data,
        "bPaginate": true,
        "bLengthChange": false,
        "bDestroy": true,
        "paging": false,
        "iDisplayLength": 10,
        searching: true,
        aoColumns: [
            {"mData": 'balanceQueryDate', sDefaultContent: "--", sClass: "alignCenter", sWidth: "140px"},
            {"mData": 'providerId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "150px"},
            {"mData": 'providerName', sDefaultContent: "--", sClass: "alignCenter", sWidth: "150px"},
            {"mData": 'operatorName', sDefaultContent: "--", sClass: "alignCenter", sWidth: "150px"},
            {mRender: function (data, type, row) {
                    var value = row.balance;
                    return '$' + parseFloat(value).toFixed(2);
                }, sDefaultContent: "--", sClass: "alignRight", sWidth: "150px"
            }
        ],
        "aaSorting": [[1, "desc"]],
        "oLanguage": {
            "sSearch": tblSearchLbl,
            "sZeroRecords": tblNoDataLbl,
            "sInfo": tblNoticeContractsShowingLbl +
                    " _START_ - _END_ " +
                    tblNoticeContractsOfLbl +
                    " _TOTAL_ " + tblNoticeContractsRegisterLbl,
            "sInfoEmpty": tblNoticeContractsShowingLbl + " 0 - 0 " +
                    tblNoticeContractsOfLbl + " 0 " + tblNoticeContractsRegisterLbl,
            "sInfoFiltered": "",
            "oPaginate": {
                "sFirst": tblFirstLbl,
                "sNext": tblNextLbl,
                "sPrevious": tblPreviousLbl,
                "sLast": tblLastLbl
            }
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).on('click', function () {
                id = aData.id;
                viewEditHoldBackRequest(aData);
            });
        }
    });
}


function getEmidaBalanceOnProviderResult() {
    $(".ajax-loading").show();

    var selectedProviders = getProviderList();
    var result = $.ajax({
        type: 'POST',
        timeout: 10000,
        data: {
            startDate: fromDate,
            endDate: toDate,
            providerList: selectedProviders
        },
        url: '/support/reports/balance/emidaBalanceOnProvider/grid',
        async: true
    }).done(function (msg) {
        $(".ajax-loading").hide();
    });
    result.success(function (data, selectedProviders, headers, config) {
        onSuccessResponse(data);
    });
}

function onSuccessResponse(data) {
    if (data.length === 0) {
        $(".tblEmidaBalance").hide();
        $(".btnReportDownloader").hide();
        $('#tblEmidaBalanceId').dataTable().fnClearTable();
        $(".divErrMessage").show();
        $(".ui-datepicker-trigger").css("vertical-align", "top");
    } else {
        $(".btnReportDownloader").show();
        $(".tblEmidaBalance").show();
        $(".divErrMessage").hide();
        $('#tblEmidaBalanceId').dataTable().fnDestroy();
        buildTable(data);
    }
}




