function hideElement(element, hide) {
    if (element !== null) {
        if (hide) {
            element.style.display = "none";
        }
        else {
            element.style.display = "inline";
        }
    }
}


function validateMerchantId(errorText) {
    var merchantIdField = document.getElementById("txtMerchantId");
    var merchantIdHelpField = document.getElementById("merchantIdHelp");
    var retValue = true;
    if (isPositiveNumber(merchantIdField)) {
        setErrorStyle(merchantIdField, merchantIdHelpField, errorText, false, false);
        document.getElementById("btnSearch").disabled = false;
        retValue = true;
    }
    else {
        if (isEmpty(merchantIdField))
        {
            setErrorStyle(merchantIdField, merchantIdHelpField, errorText, false, false);
            document.getElementById("btnSearch").disabled = false;
            retValue = true;
        }
        else
        {
            setErrorStyle(merchantIdField, merchantIdHelpField, errorText, true, false);
            document.getElementById("btnSearch").disabled = true;
            retValue = false;
        }
    }
    return retValue;
}



function validateInteger(sourceField, helpField, errorText)
{
    var retValue = true;
    if (isPositiveNumber(sourceField)) {
        setErrorStyle(sourceField, helpField, errorText, false, false);
        document.getElementById("btnSearch").disabled = false;
        retValue = true;
    }
    else {
        if (isEmpty(sourceField))
        {
            setErrorStyle(sourceField, helpField, errorText, false, false);
            document.getElementById("btnSearch").disabled = false;
            retValue = true;
        }
        else
        {
            setErrorStyle(sourceField, helpField, errorText, true, false);
            document.getElementById("btnSearch").disabled = true;
            retValue = false;
        }
    }
    return retValue;
}

function disableNonReqIdControls(disable)
{
    var startDate = document.getElementById('startDate');
    var endDate = document.getElementById('endDate');
    var merchantId = document.getElementById('txtMerchantId');
    var cmbPaymentStatus = document.getElementById('paymentStatusList');
    var cmbBanks = document.getElementById('bankList');
    var documentId = document.getElementById('txtDocumentId');
    var amount = document.getElementById('txtAmount');
    var siteId = document.getElementById('txtSiteId');

    if(disable)
    {
        startDate.disabled = true;
        endDate.disabled = true;
        merchantId.disabled = true;
        cmbPaymentStatus.disabled = true;
        cmbBanks.disabled = true;
        documentId.disabled = true;
        amount.disabled = true;
        siteId.disabled = true;
    }
    else
    {
        startDate.disabled = false;
        endDate.disabled = false;
        merchantId.disabled = false;
        cmbPaymentStatus.disabled = false;
        cmbBanks.disabled = false;
        documentId.disabled = false;
        amount.disabled = false;
        siteId.disabled = false;        
    }
}

function validateRequestId(errorText)
{
    var requestIdField = document.getElementById("txtRequestId");
    var requestIdHelpField = document.getElementById("requestIdHelp");
    var retValue = true;
    
    if(validateInteger(requestIdField, requestIdHelpField, errorText, false))
    {
        retValue = true;
    }
    else
    {
        retValue = false;
    }
    if(retValue && (!isEmpty(requestIdField)))
    {
        disableNonReqIdControls(true);
    }
    else
    {
        disableNonReqIdControls(false);
    }
    return retValue;
}

function validateSiteId(errorText)
{
    var siteIdField = document.getElementById("txtSiteId");
    var siteIdHelpField = document.getElementById("siteIdHelp");
    return validateInteger(siteIdField, siteIdHelpField, errorText, false);
}


function validateAmount(errorText)
{
    var amountField = document.getElementById("txtAmount");
    var amountHelpField = document.getElementById("amountHelp");
    var retValue = true;

    if (isCurrency(amountField, false)) {
        setErrorStyle(amountField, amountHelpField, errorText, false, false);
        document.getElementById("btnSearch").disabled = false;
        retValue = true;
    }
    else {
        if (isEmpty(amountField, false))
        {
            setErrorStyle(amountField, amountHelpField, errorText, false, false);
            document.getElementById("btnSearch").disabled = false;
            retValue = true;
        }
        else
        {
            setErrorStyle(amountField, amountHelpField, errorText, true, false);
            document.getElementById("btnSearch").disabled = true;
            retValue = false;
        }
    }
    return retValue;
}


function handleEntitySelection()
{
    var chkBoxMerchant = document.getElementById('chkUseMerchantId');
    var chkBoxChain = document.getElementById('chkUseEntityChain');
    var divMerchant = document.getElementById("merchantIdDiv");
    var divEntityCombos = document.getElementById("entityCombos");
    if ((chkBoxMerchant !== null) && (chkBoxChain !== null) &&
            (divMerchant !== null) && (divEntityCombos))
    {
        if (chkBoxMerchant.checked)
        {
            hideElement(divMerchant, false);
            hideElement(divEntityCombos, true);
        }
        else if (chkBoxChain.checked)
        {
            $("#txtMerchantId").val('');
            hideElement(divEntityCombos, false);
            hideElement(divMerchant, true);
        }
    }
}



function validateAllControls()
{
    return (validateMerchantId('') &&
            validateRequestId('') &&
            validateSiteId('') &&
            validateAmount(''));
}


// Because there are validators by control, this function is intended
// for checking the user selected at least a useful filter
function areFormFieldsFilled(errorMessage)
{
    var retValue = true;

    var startDate = document.getElementById('startDate');
    var endDate = document.getElementById('endDate');
    var merchantId = document.getElementById('txtMerchantId');
    var currentEntityType = document.getElementById('currentEntityType');
    var currentSelectedIds = document.getElementById('currentSelectedIds');
    var cmbPaymentStatus = document.getElementById('paymentStatusList');
    var cmbBanks = document.getElementById('bankList');
    var requestId = document.getElementById('txtRequestId');
    var documentId = document.getElementById('txtDocumentId');
    var amount = document.getElementById('txtAmount');
    var siteId = document.getElementById('txtSiteId');
    var formHelp = document.getElementById('formHelp');
    if (formHelp)
    {
        if (startDate && endDate && merchantId && currentEntityType &&
                currentSelectedIds && cmbPaymentStatus && cmbBanks &&
                requestId && documentId && amount && siteId)
        {
            if (((startDate.value === '') && (endDate.value === '') &&
                    (merchantId.value === '') && (currentEntityType.value === '') &&
                    (currentSelectedIds.value === '') && (cmbPaymentStatus.value === '-1') &&
                    (cmbBanks.value === '-1') && (requestId.value === '') &&
                    (documentId.value === '') && (amount.value === '') &&
                    (siteId.value === '')) || (!validateAllControls()))
            {
                formHelp.innerHTML = errorMessage;
                alert(errorMessage);
                document.getElementById('search').value = '';
                document.getElementById("btnSearch").disabled = true;
                retValue = false;
            }
            else
            {
                formHelp.innerHTML = "";
                document.getElementById('search').value = 'y';
            }
        }
        else
        {
            formHelp.innerHTML = "";
            document.getElementById('search').value = 'y';
        }
    }
    return retValue;
}


function clearControls()
{
    $("#startDate").val('');
    $("#endDate").val('');
    $("input[type=text]").val('');
    $("#paymentStatusList").val("-1");
    $("#bankList").val("-1");
}




	