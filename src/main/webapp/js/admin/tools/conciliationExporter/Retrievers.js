/* 
 * EMIDA all rights reserved 1999-2019.
 */

/* global retrieverThId, retrieverThClass, retrieverThDescription, retrieverThColumnHeaders, retrieverThDataSourceName, retrieverThStartDate, retrieverThEndDate, retrieverThUseFixedDates, retrieverThUseDateRange, retrieverThRangeDays, retrieverThIsPartialQuery, retrieverThDetailQuery, retrieverThFileNameDateFormat, retrieverThActions, retrieverFormInvalidDescriptionLabel, retrieverFormInvalidClassLabel, retrieverFormInvalidDatasourceNameLabel, retrieverFormInvalidRangeDays, retrieverFormInvalidDetailQuery, retrieverFormInvalidFileNameDateFormat, retrieverFormAddTitle, repApplyLbl, repCancelLbl, repFromLbl, repToLbl, repCustomLbl, repDaySu, repDayMo, repDayTu, repDayWe, repDayTh, repDayFr, repDaySa,
 repMonthJan, repMonthFeb, repMonthMar, repMonthApr, repMonthMay, repMonthJun, repMonthJul, repMonthAug, repMonthSep, repMonthOct, repMonthNov, repMonthDec, retrieverInsertionFailMsg, retrieverDeletionFailMsg, retrieverList, retrieverMap, retrieverFormFixedDatesEmptyLabel, retrieverFormInvalidFixedDatesLabel, retrieverFormEditTitle, MAX_COLUMN_TEXT_LENGTH, tblNavFirstLbl, tblNavPrevLbl, tblNavNextLbl, tblNavLastLbl, tblNavInfoLbl, tblNavInfoEmptyLbl, retrieverListLoadFailMsg
 */

function Retrievers() {

    const DEFAULT_DATE_FORMAT = 'MM/DD/YYYY';
    var retrieversTable;
    var editingRetriever = false;
    var retrieverFixedStartDate = null;
    var retrieverFixedEndDate = null;
    var Instance = this;
    Instance.init = function () {

        $(document).ready(function () {
            Instance.loadRetrievers();
            Instance.loadRetrieversTable();
            $('#btnAddRetriever').on('click', function (event) {
                event.preventDefault();
                Instance.clearRetrieverForm();
                editingRetriever = false;
                $('#addEditRetrieverFormTitle').html(retrieverFormAddTitle);
                $('#addEditRetrieverBoostrapForm').modal('toggle');
            });
            $('#addEditRetrieverForm').submit(function (e) {
                e.preventDefault();
                var retValue = false;
                if (validateRetrieverForm()) {
                    $('#addEditRetrieverBoostrapForm').modal('hide');
                    if (!editingRetriever) {
                        Instance.insertNewRetriever();
                    } else {
                        Instance.editRetriever();
                    }
                    Instance.clearRetrieverForm();
                    retValue = true;
                }
                return retValue;
            });
        });
    };

    Instance.loadRetrievers = function () {
        jQuery.ajax({
            url: 'tools/conciliationExporter/getAllExportRetrievers',
            method: 'GET',
            dataType: "json",
            async: true
        }).done(function (response) {
            if (response) {
                retrieverList = response;
                retrieverMap = {};
                retrieverList.forEach(function (currentRetriever) {
                    retrieverMap[currentRetriever.id] = currentRetriever;
                });
                Instance.updateRetrieverTableData();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorMsg = retrieverListLoadFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    };

    Instance.updateRetrieverTableData = function () {
        retrieversTable.clear();
        retrieversTable.rows.add(retrieverList);
        retrieversTable.draw();
    };

    Instance.useFixedDatesOnClick = function () {
        if ($('#chkbRetrieverUseFixedDates').is(":checked")) {
            $('#txtRetrieverStartDate').removeAttr("disabled");
            $('#txtRetrieverEndDate').removeAttr("disabled");
            $('#chkbRetrieverUseDateRange').attr('disabled', 'disabled')
        } else {
            $('#txtRetrieverStartDate').attr('disabled', 'disabled');
            $('#txtRetrieverEndDate').attr('disabled', 'disabled');
            $('#chkbRetrieverUseDateRange').removeAttr("disabled");
        }
    };

    Instance.useDateRangeOnClick = function () {
        if ($('#chkbRetrieverUseDateRange').is(":checked")) {
            $('#txtRetrieverRangeDays').removeAttr("disabled");
            $('#chkbRetrieverUseFixedDates').attr('disabled', 'disabled')
        } else {
            $('#txtRetrieverRangeDays').attr('disabled', 'disabled');
            $('#chkbRetrieverUseFixedDates').removeAttr("disabled");
        }
    };
    
    Instance.useStartDateOnClick = function () {
    	
    };

    Instance.editRetrieverClick = function (retrieverId) {
        var retriever = retrieverMap[retrieverId];
        currentRecordId = retriever.id;
        editingRetriever = true;
        Instance.clearRetrieverForm();
        $('#addEditRetrieverFormTitle').html(retrieverFormEditTitle);
        $('#txtRetrieverId').val(currentRecordId);
        $('#txtRetrieverDescription').val(retriever.description);
        $('#txtRetrieverClass').val(retriever.retrieverClass);
        $('#txtRetrieverColumnHeaders').val(retriever.columnHeaders);
        $('#txtRetrieverDataSourceName').val(retriever.dataSourceName);
        $('#chkbRetrieverUseFixedDates').prop("checked", retriever.useFixedDates);
        if (retriever.useFixedDates) {
            retrieverFixedStartDate = moment(retriever.startDate);
            retrieverFixedEndDate = moment(retriever.endDate);
            $('#txtRetrieverStartDate').val(retrieverFixedStartDate.format(DEFAULT_DATE_FORMAT));
            $('#txtRetrieverEndDate').val(retrieverFixedEndDate.format(DEFAULT_DATE_FORMAT));
        }
        Instance.useFixedDatesOnClick();
        $('#chkbRetrieverUseDateRange').prop("checked", retriever.useDateRange);
        $('#txtRetrieverRangeDays').val(retriever.rangeDays);
        $('#txtRetrieverDetailQuery').val(retriever.detailQuery);
        $('#chkbRetrieverIsPartialQuery').prop("checked", retriever.partialQuery);
        Instance.useDateRangeOnClick();
        $('#txtRetrieverFileNameDateFormat').val(retriever.fileNameDateFormat);
        $('#addEditRetrieverBoostrapForm').modal('toggle');
        $('#chkbRetrieverUseStartDate').prop("checked", retriever.useStartDate);
        Instance.useStartDateOnClick();
    };

    Instance.clearRetrieverForm = function () {
        $('#txtRetrieverId').val("");
        $('#txtRetrieverDescription').val("");
        $('#retrieverDescriptionValueError').text("");
        $('#txtRetrieverClass').val("");
        $('#retrieverClassValueError').text("");
        $('#txtRetrieverColumnHeaders').val("");
        $('#retrieverColumnHeadersValueError').text("");
        $('#txtRetrieverDataSourceName').val("");
        $('#retrieverDataSourceNameValueError').text("");
        $('#chkbRetrieverUseFixedDates').attr("checked", false);
        $('#txtRetrieverStartDate').val("");
        $('#txtRetrieverEndDate').val("");
        $('#txtRetrieverStartDate').attr('disabled', 'disabled');
        $('#txtRetrieverEndDate').attr('disabled', 'disabled');
        $('#retrieverFixedDatesRangeValueError').text("");
        $('#chkbRetrieverUseDateRange').attr("checked", false);
        $('#txtRetrieverRangeDays').attr('disabled', 'disabled');
        $('#txtRetrieverRangeDays').val('');
        $('#retrieverRangeDaysValueError').text("");
        $('#txtRetrieverDetailQuery').val("");
        $('#retrieverDetailQueryValueError').text("");
        $('#chkbRetrieverIsPartialQuery').attr("checked", false);
        $('#txtRetrieverFileNameDateFormat').val("");
        $('#retrieverFileNameDateFormatValueError').text("");
    };

    function validateRetrieverForm() {
        var retValue = (Instance.isValidRetrieverDescription() &
                Instance.isValidRetrieverClass() &
                Instance.isValidRetrieverDataSourceName() &
                Instance.isValidFixedDates() &
                Instance.isValidRangeDays() &
                Instance.isValidRetrieverDetailQuery() &
                Instance.isValidRetrieverFileNameDateFormat());
        return retValue;
    }

    Instance.isValidRetrieverDescription = function ()
    {
        var retrieverDescriptionField = document.getElementById("txtRetrieverDescription");
        var errorField = document.getElementById("retrieverDescriptionValueError");
        var retValue = checkFieldEmpty(retrieverDescriptionField, errorField, retrieverFormInvalidDescriptionLabel);
        return retValue;
    };

    Instance.isValidRetrieverClass = function ()
    {
        var retrieverClassField = document.getElementById("txtRetrieverClass");
        var errorField = document.getElementById("retrieverClassValueError");
        var retValue = checkFieldEmpty(retrieverClassField, errorField, retrieverFormInvalidClassLabel);
        return retValue;
    };

    Instance.isValidRetrieverDataSourceName = function ()
    {
        var dataSourceNameField = document.getElementById("txtRetrieverDataSourceName");
        var errorField = document.getElementById("retrieverDataSourceNameValueError");
        var retValue = checkFieldEmpty(dataSourceNameField, errorField, retrieverFormInvalidDatasourceNameLabel);
        return retValue;
    };

    Instance.isValidFixedDates = function ()
    {
        if ($('#chkbRetrieverUseFixedDates').is(":checked")) {
            var startDateField = document.getElementById("txtRetrieverStartDate");
            var endDateField = document.getElementById("txtRetrieverEndDate");
            var errorField = document.getElementById("retrieverFixedDatesRangeValueError");
            if (!checkFieldEmpty(startDateField, errorField, retrieverFormFixedDatesEmptyLabel)) {
                return false;
            }
            if (!checkFieldEmpty(endDateField, errorField, retrieverFormFixedDatesEmptyLabel)) {
                return false;
            }

            if ((!validateRegEx(/^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/, startDateField)) ||
                    (!validateRegEx(/^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/, endDateField))) {
                setErrorStyle(endDateField, errorField, retrieverFormInvalidFixedDatesLabel,
                        true, true);
                return false;
            } else {
                setErrorStyle(endDateField, errorField, '',
                        false, false);
            }

            var mStartDate = moment(startDateField.value);
            var mEndDate = moment(endDateField.value);
            if (mStartDate.isAfter(mEndDate))
            {
                setErrorStyle(endDateField, errorField, retrieverFormInvalidFixedDatesLabel,
                        true, true);
            } else {
                setErrorStyle(endDateField, errorField, '',
                        false, false);
            }
        }
        return true;
    };

    Instance.isValidRangeDays = function ()
    {
        var retValue = false;
        if ($('#chkbRetrieverUseDateRange').is(':checked')) {
            var rangeDaysField = document.getElementById("txtRetrieverRangeDays");
            var errorField = document.getElementById("retrieverRangeDaysValueError");
            if (!isEmpty(rangeDaysField))
            {
                var paymentValue = parseInt($('#txtRetrieverRangeDays').val());
                if ((isNaN(paymentValue)) || (paymentValue <= 0)) {
                    setErrorStyle(errorField, errorField, retrieverFormInvalidRangeDays,
                            true, true);
                } else {
                    setErrorStyle(errorField, errorField, '',
                            false, false);
                    retValue = true;
                }

            } else {
                setErrorStyle(errorField, errorField, retrieverFormInvalidRangeDays,
                        true, true);
            }
        } else {
            retValue = true;
        }
        return retValue;
    };

    Instance.isValidRetrieverDetailQuery = function ()
    {
        var retrieverDetailQueryField = document.getElementById("txtRetrieverDetailQuery");
        var errorField = document.getElementById("retrieverDetailQueryValueError");
        var retValue = checkFieldEmpty(retrieverDetailQueryField, errorField, retrieverFormInvalidDetailQuery);
        return retValue;
    };

    Instance.isValidRetrieverFileNameDateFormat = function ()
    {
        var retrieverFileNameDateFormat = document.getElementById("txtRetrieverFileNameDateFormat");
        var errorField = document.getElementById("retrieverFileNameDateFormatValueError");
        var retValue = checkFieldEmpty(retrieverFileNameDateFormat, errorField, retrieverFormInvalidFileNameDateFormat);
        return retValue;
    };

    Instance.loadRetrieversTable = function () {
        retrieversTable = $('#retrieversTable').DataTable({
            searching: false,
            bLengthChange: false,
            autoWidth: false,
            rowId: 'id',
            data: retrieverList,
            columns: [
                {
                    title: retrieverThId,
                    data: 'id', 
                    visible: false
                },
                {
                    title: retrieverThClass,
                    data: 'retrieverClass',
                    visible: false
                },
                {
                    title: retrieverThDescription,
                    data: 'description', order: 'asc'
                },
                {
                    title: retrieverThColumnHeaders,
                    data: 'columnHeaders',
                    render: function (data, type, row) {
                        if (row.columnHeaders.length > MAX_COLUMN_TEXT_LENGTH) {
                            return row.columnHeaders.substring(0, MAX_COLUMN_TEXT_LENGTH);
                        } else {
                            return row.columnHeaders;
                        }
                    }
                },
                {
                    title: retrieverThDataSourceName,
                    data: 'dataSourceName'
                },
                {
                    title: retrieverThUseFixedDates,
                    data: 'useFixedDates',
                    className: "dt-center",
                    render: function (data, type, row) {
                        var checkBoxId = 'useFixedDatesChkb_' + row.id;
                        var strChkBControl = '<input id="' + checkBoxId + '" name="' + checkBoxId + '" type="checkbox" disabled="disabled"';
                        if (row.useFixedDates) {
                            strChkBControl += ' checked="checked"';
                        }
                        strChkBControl += ' />';
                        return  strChkBControl;
                    }
                },
                {
                    title: retrieverThStartDate,
                    data: 'startDate',
                    render: function (data, type, row) {
                        if (row.useFixedDates) {
                            var mStartDate = moment(row.startDate);
                            return mStartDate.format(DEFAULT_DATE_FORMAT);
                        } else {
                            return "";
                        }
                    }
                },
                {
                    title: retrieverThEndDate,
                    data: 'endDate',
                    render: function (data, type, row) {
                        if (row.useFixedDates) {
                            var mEndDate = moment(row.endDate);
                            return mEndDate.format(DEFAULT_DATE_FORMAT);
                        } else {
                            return "";
                        }
                    }
                },
                {
                    title: retrieverThUseDateRange,
                    data: 'useDateRange',
                    className: "dt-center",
                    render: function (data, type, row) {
                        var checkBoxId = 'useDateRangeChkb_' + row.id;
                        var strChkBControl = '<input id="' + checkBoxId + '" name="' + checkBoxId + '" type="checkbox" disabled="disabled"';
                        if (row.useDateRange) {
                            strChkBControl += ' checked="checked"';
                        }
                        strChkBControl += ' />';
                        return  strChkBControl;
                    }
                },
                {
                    title: retrieverThRangeDays,
                    data: 'rangeDays'
                },
                {
                    title: retrieverThIsPartialQuery,
                    data: 'partialQuery',
                    className: "dt-center",
                    render: function (data, type, row) {
                        var checkBoxId = 'partialQueryChkb_' + row.id;
                        var strChkBControl = '<input id="' + checkBoxId + '" name="' + checkBoxId + '" type="checkbox" disabled="disabled"';
                        if (row.partialQuery) {
                            strChkBControl += ' checked="checked"';
                        }
                        strChkBControl += ' />';
                        return  strChkBControl;
                    }

                },
                {
                    title: retrieverThDetailQuery,
                    data: 'detailQuery',
                    render: function (data, type, row) {
                        if (row.detailQuery.length > MAX_COLUMN_TEXT_LENGTH) {
                            return row.detailQuery.substring(0, MAX_COLUMN_TEXT_LENGTH);
                        } else {
                            return row.detailQuery;
                        }
                    }
                },
                {
                    title: retrieverThFileNameDateFormat,
                    data: 'fileNameDateFormat'
                },
                {
                    title: retrieverThActions,
                    data: '',
                    render: function (data, type, row) {
                        var actionColumnButtons = '<a onclick=retrieversController.editRetrieverClick(&#39;' + row.id + '&#39;) class="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true">&nbsp;&nbsp;</span></a>';
                        actionColumnButtons += '<a onclick="confirmRecordDeletion(&#39;' + row.id + '&#39;)" class="delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
                        return  actionColumnButtons;
                    }
                }
            ],
            language: {
                "info": tblNavInfoLbl,
                "infoEmpty": tblNavInfoEmptyLbl,
                paginate: {
                    first: tblNavFirstLbl,
                    previous: tblNavPrevLbl,
                    next: tblNavNextLbl,
                    last: tblNavLastLbl
                }
            }
        });
        retrieversTable.order([2, 'asc']).draw();
    };

    Instance.insertNewRetriever = function () {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/insertRetriever',
            method: 'POST',
            dataType: "json",
            data: $('#addEditRetrieverForm').serialize()
        }).done(function (response) {
            if (response.resultCode === "000") {
                Instance.loadRetrievers();
            } else {
                errorMsg = retrieverInsertionFailMsg + " " + response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            errorMsg = retrieverInsertionFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    };

    Instance.editRetriever = function () {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/editRetriever',
            method: 'POST',
            dataType: "json",
            data: $('#addEditRetrieverForm').serialize()
        }).done(function (response) {
            if (response.resultCode === "000") {
                Instance.loadRetrievers();
            } else {
                errorMsg = response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            errorMsg = jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    };

    Instance.deleteRetrieverRecord = function () {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/deleteRetriever/' + currentRecordId,
            method: 'DELETE',
            dataType: "json"
        }).done(function (response) {
            if (response.resultCode === "000") {
                Instance.loadRetrievers();
            } else {
                errorMsg = retrieverDeletionFailMsg + " " + response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            errorMsg = retrieverDeletionFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    };


    Instance.init();
}
;
var retrieversController = new Retrievers();


