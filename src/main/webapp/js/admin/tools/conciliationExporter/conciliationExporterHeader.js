/* 
 * EMIDA all rights reserved 1999-2019.
 */


/* global repTypesLoadFailMsg, retrieverListLoadFailMsg, fileExporterListLoadFailMsg, exportTaskListLoadFailMsg, errorMsgTitle */

const MAX_COLUMN_TEXT_LENGTH = 40;
var currentRecordId = '';

var exportTaskList;
var exportTaskMap;

var retrieverList;
var retrieverMap;

var exporterList;
var exporterMap;

var entityTypes;

function confirmRecordDeletion(id) {
    currentRecordId = id;
    $('#deleteRecordModal').modal('show');
}


function checkFieldEmpty(field, errorField, errorMsg) {
    var retValue = false;

    if (isEmpty(field))
    {
        setErrorStyle(field, errorField, errorMsg,
                true, true);
    } else {
        setErrorStyle(field, errorField, '',
                false, false);
        retValue = true;
    }
    return retValue;
}



function loadEntityTypes() {
    jQuery.ajax({
        url: '/support/tools/conciliationExporter/getAllRepTypes',
        method: 'GET',
        dataType: "json",
        async: true
    }).done(function (response) {
        entityTypes = response;
    }).fail(function (jqXHR, textStatus, errorThrown) {
        var errorMsg = repTypesLoadFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
        showErrorAlert(errorMsg);
    });
}

function showErrorAlert(errorMessage){
    toastr.error(errorMessage, errorMsgTitle, { positionClass: "toast-top-center"});
}

$(document).ready(function () {
    loadEntityTypes();
//    $.holdReady(true);
//    $.when(loadRetrievers(), loadFileExporters(), loadExportTasks(), loadEntityTypes()).done(function () {
//        $.holdReady(false);
//    });
});

