/* 
 * EMIDA all rights reserved 1999-2019.
 */

/* global fileExporterFormAddTitle, fileExporterFormInvalidDescriptionLabel, fileExporterFormInvalidClassLabel, fileExporterFileNameFormatValueError, fileExporterFormInvalidTargetFilePathLabel, fileExporterFormInvalidRecordFormatLabel, fileExporterFormInvalidRecordSeparatorLabel, fileExporterFormInvalidFileNameFormatLabel, JSON2, fileExporterInsertionFailMsg, acceptLabel, fileExporterDeletionFailMsg, fileExporterThId, fileExporterThClass, fileExporterThActions, fileExporterThFooterFormat, fileExporterThRecordSeparator, fileExporterThRecordFormat, fileExporterThHeaderFormat, fileExporterThTargetFilePath, fileExporterThFileNameFormat, fileExporterThDescription, exporterList, exporterMap, fileExporterFormEditTitle, tblNavInfoLbl, tblNavInfoEmptyLbl, tblNavFirstLbl, tblNavPrevLbl, tblNavNextLbl, tblNavLastLbl, errorMsgTitle, fileExporterListLoadFailMsg */

function Exporters() {
    var fileExportersTable;
    var editingFileExporter = false;
    var Instance = this;

    Instance.init = function () {
        

        $(document).ready(function () {
            loadFileExporters();
            loadExporterTable();

            $('#btnAddExporter').on('click', function (event) {
                event.preventDefault();
                Instance.clearFileExporterForm();
                editingFileExporter = false;
                $('#addEditFileExporterFormTitle').html(fileExporterFormAddTitle);
                $('#addEditFileExporterBoostrapForm').modal('toggle');
            });


            $('#addEditFileExporterForm').submit(function (e) {
                e.preventDefault();
                var retValue = false;
                if (Instance.validateFileExporterForm()) {
                    $('#addEditFileExporterBoostrapForm').modal("hide");
                    if (!editingFileExporter) {
                        Instance.insertNewFileExporter();
                    } else {
                        Instance.editFileExporter();
                    }
                    Instance.clearFileExporterForm();
                    retValue = true;
                }
                return retValue;
            });
        });
    };

    function loadFileExporters() {
        jQuery.ajax({
            url: 'tools/conciliationExporter/getAllFileExporters',
            method: 'GET',
            dataType: "json",
            async: true
        }).done(function (response) {
            if (response) {
                exporterList = response;
                exporterMap = {};
                exporterList.forEach(function (currentExporter) {
                    exporterMap[currentExporter.id] = currentExporter;
                });
                Instance.updateFileExportersTableData();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorMsg = fileExporterListLoadFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    }

    Instance.updateFileExportersTableData = function () {
        fileExportersTable.clear();
        fileExportersTable.rows.add(exporterList);
        fileExportersTable.draw();
    };


    Instance.editFileExporterClick = function (fileExporterId) {
        var fileExporter = exporterMap[fileExporterId];
        currentRecordId = fileExporter.id;
        editingFileExporter = true;
        Instance.clearFileExporterForm();
        $('#addEditFileExporterFormTitle').html(fileExporterFormEditTitle);
        $('#txtFileExporterId').val(currentRecordId);
        $('#txtFileExporterDescription').val(fileExporter.description);
        $('#txtFileExporterClass').val(fileExporter.fileExporterClass);
        $('#txtFileExporterFileNameFormat').val(fileExporter.fileNameFormat);
        $('#txtFileExporterTargetFilePath').val(fileExporter.targetFilePath);
        $('#txtFileExporterHeaderFormat').val(fileExporter.headerFormat);
        $('#txtFileExporterRecordFormat').val(fileExporter.recordFormat);
        $('#txtFileExporterRecordSeparator').val(fileExporter.recordSeparator);
        $('#txtFileExporterFooterFormat').val(fileExporter.footerFormat);
        $('#addEditFileExporterBoostrapForm').modal('show');
    };

    Instance.validateFileExporterForm = function () {
        var retValue = (Instance.isValidFileExporterDescription() &
                Instance.isValidFileExporterClass() &
                Instance.isValidFileExporterFileNameFormat() &
                Instance.isValidFileExporterTargetFilePath() &
                Instance.isValidFileExporterRecordFormat() &
                Instance.isValidFileExporterRecordSeparator());
        return retValue;
    };

    Instance.isValidFileExporterDescription = function ()
    {
        var fileExporterDescriptionField = document.getElementById("txtFileExporterDescription");
        var errorField = document.getElementById("fileExporterDescriptionValueError");
        var retValue = checkFieldEmpty(fileExporterDescriptionField, errorField, fileExporterFormInvalidDescriptionLabel);
        return retValue;
    };

    Instance.isValidFileExporterClass = function ()
    {
        var fileExporterClassField = document.getElementById("txtFileExporterClass");
        var errorField = document.getElementById("fileExporterClassValueError");
        var retValue = checkFieldEmpty(fileExporterClassField, errorField, fileExporterFormInvalidClassLabel);
        return retValue;
    };

    Instance.isValidFileExporterFileNameFormat = function ()
    {
        var fileExporterFileNameFormatField = document.getElementById("txtFileExporterFileNameFormat");
        var errorField = document.getElementById("fileExporterFileNameFormatValueError");
        var retValue = checkFieldEmpty(fileExporterFileNameFormatField, errorField, fileExporterFormInvalidFileNameFormatLabel);
        return retValue;
    };

    Instance.isValidFileExporterTargetFilePath = function ()
    {
        var fileExporterTargetFilePathField = document.getElementById("txtFileExporterTargetFilePath");
        var errorField = document.getElementById("fileExporterTargetFilePathValueError");
        var retValue = checkFieldEmpty(fileExporterTargetFilePathField, errorField, fileExporterFormInvalidTargetFilePathLabel);
        return retValue;
    };

    Instance.isValidFileExporterRecordFormat = function ()
    {
        var fileExporterRecordFormatField = document.getElementById("txtFileExporterRecordFormat");
        var errorField = document.getElementById("fileExporterRecordFormatValueError");
        var retValue = checkFieldEmpty(fileExporterRecordFormatField, errorField, fileExporterFormInvalidRecordFormatLabel);
        return retValue;
    };

    Instance.isValidFileExporterRecordSeparator = function ()
    {
        var fileExporterRecordSeparator = document.getElementById("txtFileExporterRecordSeparator");
        var errorField = document.getElementById("fileExporterRecordSeparatorValueError");
        var retValue = checkFieldEmpty(fileExporterRecordSeparator, errorField, fileExporterFormInvalidRecordSeparatorLabel);
        return retValue;
    };

    Instance.clearFileExporterForm = function () {
        $('#txtFileExporterId').val("");
        $('#txtFileExporterDescription').val("");
        $('#fileExporterDescriptionValueError').text("");
        $('#txtFileExporterClass').val("");
        $('#fileExporterClassValueError').text("");
        $('#txtFileExporterFileNameFormat').val("");
        $('#fileExporterFileNameFormatValueError').text("");
        $('#txtFileExporterTargetFilePath').val("");
        $('#fileExporterTargetFilePathValueError').text("");
        $('#txtFileExporterHeaderFormat').val("");
        $('#txtFileExporterRecordFormat').val("");
        $('#fileExporterRecordFormatValueError').text("");
        $('#txtFileExporterRecordSeparator').val("");
        $('#fileExporterRecordSeparatorValueError').text("");
        $('#txtFileExporterFooterFormat').val("");
    };

    function loadExporterTable() {
        fileExportersTable = $('#fileExportersTable').DataTable({
            searching: false,
            bLengthChange: false,
            data: exporterList,
            columns: [
                {
                    title: fileExporterThId,
                    data: 'id', visible: false
                },
                {
                    title: fileExporterThClass,
                    data: 'fileExporterClass'
                },
                {
                    title: fileExporterThDescription,
                    data: 'description', order: 'asc'
                },
                {
                    title: fileExporterThFileNameFormat,
                    data: 'fileNameFormat'
                },
                {
                    title: fileExporterThTargetFilePath,
                    data: 'targetFilePath'
                },
                {
                    title: fileExporterThHeaderFormat,
                    data: 'headerFormat'
                },
                {
                    title: fileExporterThRecordFormat,
                    data: 'recordFormat'
                },
                {
                    title: fileExporterThRecordSeparator,
                    data: 'recordSeparator'
                },
                {
                    title: fileExporterThFooterFormat,
                    data: 'footerFormat'
                },
                {
                    title: fileExporterThActions,
                    data: '',
                    render: function (data, type, row) {
                        var actionColumnButtons = '<a onclick=exportersController.editFileExporterClick(&#39;' + row.id + '&#39;) class="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true">&nbsp;&nbsp;</span></a>';
                        actionColumnButtons += '<a onclick="confirmRecordDeletion(&#39;' + row.id + '&#39;)" class="delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
                        return  actionColumnButtons;
                    }
                }
            ],
            language: {
                "info": tblNavInfoLbl,
                "infoEmpty": tblNavInfoEmptyLbl,
                paginate: {
                    first: tblNavFirstLbl,
                    previous: tblNavPrevLbl,
                    next: tblNavNextLbl,
                    last: tblNavLastLbl
                }
            }
        });
        fileExportersTable.order([2, 'asc']).draw();
    }

    Instance.insertNewFileExporter = function () {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/insertFileExporter',
            method: 'POST',
            dataType: "json",
            data: $('#addEditFileExporterForm').serialize()
        }).done(function (response) {
            if (response.resultCode === "000") {
                loadFileExporters();
            } else {
                errorMsg = response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            errorMsg = jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });

    };

    Instance.editFileExporter = function () {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/editFileExporter',
            method: 'POST',
            dataType: "json",
            data: $('#addEditFileExporterForm').serialize()
        }).done(function (response) {
            if (response.resultCode === "000") {
                loadFileExporters();
            } else {
                errorMsg = response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            errorMsg = jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });

    };


    Instance.deleteFileExporterRecord = function () {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/deleteFileExporter/' + currentRecordId,
            method: 'DELETE',
            dataType: "json"
        }).done(function (response) {
            if (response.resultCode === "000") {
                loadFileExporters();
            } else {
                errorMsg = fileExporterDeletionFailMsg + " " + response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).error(function (jqXHR, textStatus, errorThrown) {
            errorMsg = fileExporterDeletionFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    };

    Instance.init();
}
;

var exportersController = new Exporters();
