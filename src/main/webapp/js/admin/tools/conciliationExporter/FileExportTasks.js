/* 
 * EMIDA all rights reserved 1999-2019.
 */

/* 
 */

/* global exportTaskThId, exportTaskThDescription, exportTaskThActions, exportTaskThTaskEnabled, exportTaskThQueryProcessorClass, exportTaskThExporter, exportTaskThRetriever, exportTaskThEntityType, exportTaskThEntityName, exportTaskThEntityId, exporterMap, retrieverMap, entityTypes, exportTaskFormInvalidDescriptionLabel, exportTaskFormAddTitle, exportTaskFormInvalidEntityIdLabel, exportTaskFormInvalidEntityNameLabel, exportTaskFormInvalidEntityTypeLabel, exportTaskFormInvalidRetrieverLabel, exportTaskFormInvalidFileExporterLabel, exportTaskFormInvalidPreprocessClassLabel, exportTaskList, retrieversTable, exportTaskMap, exportTaskFormEditTitle, exportTaskInsertionFailMsg, exportTaskEditFailMsg, exportTaskDeletionFailMsg, retrieverList, exporterList, tblNavInfoLbl, tblNavInfoEmptyLbl, tblNavFirstLbl, tblNavPrevLbl, tblNavNextLbl, tblNavLastLbl, errorMsgTitle, merchantLoadFailMsg, repLoadFailMsg, isoLoadFailMsg, exportTaskListLoadFailMsg */
function FileExportTasks() {


    var exportTasksTable;
    var editingExportTask = false;

    var Instance = this;

    Instance.init = function () {

        $(document).ready(function () {
            Instance.loadExportTasks();
            loadExportTasksTable();

            $('#btnAddExportTask').on('click', function (event) {
                event.preventDefault();
                Instance.clearExportTaskForm();
                fillIsosSelect();
                updateRetrieverSelect();
                updateFileExporterSelect();
                editingExportTask = false;
                $('#addEditExportTaskFormTitle').html(exportTaskFormAddTitle);
                $('#addEditExportTaskBoostrapForm').modal('toggle');
            });



            $('#addEditExportTaskForm').submit(function (e) {
                e.preventDefault();
                var retValue = false;
                if (Instance.validateExportTaskForm()) {
                    $('#addEditExportTaskBoostrapForm').modal('hide');
                    $("#txtExportTaskEntityId").removeAttr('disabled');
                    $("#txtExportTaskEntityName").removeAttr('disabled');
                    if (!editingExportTask) {
                        insertNewExportTask();
                    } else {
                        editExportTask();
                    }
                    Instance.clearExportTaskForm();
                    retValue = true;
                }
                return retValue;
            });
        });
    };

    Instance.loadExportTasks = function () {
        jQuery.ajax({
            url: 'tools/conciliationExporter/getExportTasks',
            method: 'GET',
            dataType: "json",
            async: true
        }).done(function (response) {
            if (response) {
                exportTaskList = response;
                exportTaskMap = {};
                exportTaskList.forEach(function (currentExportTask) {
                    exportTaskMap[currentExportTask.id] = currentExportTask;
                });
                Instance.updateExportTaskTableData();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorMsg = exportTaskListLoadFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    };


    Instance.updateExportTaskTableData = function () {
        exportTasksTable.clear();
        exportTasksTable.rows.add(exportTaskList);
        exportTasksTable.draw();
    };

    function fillIsosSelect() {
        jQuery.ajax({
            url: '/support/util/entityLocator/getIsos',
            method: 'GET',
            dataType: "json"
        }).done(function (response) {
            $('#selectIso').empty();
            $('#selectIso').append('<option value="1" selected="selected">--</option>;');
            response.forEach(function (currentIso) {
                $('#selectIso').append($('<option>', {
                    value: currentIso.entityId,
                    text: currentIso.entityName,
                    entityType: currentIso.entityType
                }));
            });
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorMsg = isoLoadFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    }

    Instance.onSelectedIso = function (selectedOption) {
        if (selectedOption.attributes.entitytype.value === '2') { // 3 Level Iso
            $('#selectAgent').empty();
            $('#selectAgentDiv').hide();
            $('#selectSubAgent').empty();
            $('#selectSubAgentDiv').hide();
            fillRepChildsSelect(selectedOption.value, '#selectRep');
        } else if (selectedOption.attributes.entitytype.value === '3') { // 5 Level Iso
            $('#selectAgent').empty();
            $('#selectAgentDiv').show();
            $('#selectSubAgent').empty();
            $('#selectSubAgentDiv').show();
            fillRepChildsSelect(selectedOption.value, '#selectAgent');
        }
        $('#txtExportTaskEntityId').val(selectedOption.value);
        $('#txtExportTaskEntityName').val(selectedOption.label);
        $('#txtExportTaskEntityTypeId').val(selectedOption.attributes.entitytype.value);
    };

    Instance.onSelectedAgent = function (selectedOption) {
        $('#selectSubAgent').empty();
        fillRepChildsSelect(selectedOption.value, '#selectSubAgent');
        $('#txtExportTaskEntityId').val(selectedOption.value);
        $('#txtExportTaskEntityName').val(selectedOption.label);
        $('#txtExportTaskEntityTypeId').val(selectedOption.attributes.entitytype.value);
    };

    Instance.onSelectedSubAgent = function (selectedOption) {
        $('#selectRep').empty();
        fillRepChildsSelect(selectedOption.value, '#selectRep');
        $('#txtExportTaskEntityId').val(selectedOption.value);
        $('#txtExportTaskEntityName').val(selectedOption.label);
        $('#txtExportTaskEntityTypeId').val(selectedOption.attributes.entitytype.value);
    };

    function fillRepChildsSelect(parentId, selector) {
        jQuery.ajax({
            url: '/support/util/entityLocator/getRepsChildsByParentId',
            method: 'GET',
            dataType: "json",
            data: {'parentId': parentId}
        }).done(function (response) {
            $(selector).empty();
            $(selector).append('<option value="1" selected="selected">--</option>;');
            response.forEach(function (currentRep) {
                $(selector).append($('<option>', {
                    value: currentRep.entityId,
                    text: currentRep.entityName,
                    entityType: currentRep.entityType
                }));
            });
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorMsg = repLoadFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    }

    Instance.onSelectedRep = function (selectedOption) {
        $('#selectMerchant').empty();
        fillMerchantSelect(selectedOption.value, '#selectMerchant');
        $('#txtExportTaskEntityId').val(selectedOption.value);
        $('#txtExportTaskEntityName').val(selectedOption.label);
        $('#txtExportTaskEntityTypeId').val(selectedOption.attributes.entitytype.value);
    };

    function fillMerchantSelect(parentId, selector) {
        jQuery.ajax({
            url: '/support/util/entityLocator/getMerchantsByRepId',
            method: 'GET',
            dataType: "json",
            data: {'repId': parentId}
        }).done(function (response) {
            $(selector).empty();
            $(selector).append('<option value="1" selected="selected">--</option>;');
            response.forEach(function (currentMerchant) {
                $(selector).append($('<option>', {
                    value: currentMerchant.entityId,
                    text: currentMerchant.entityName,
                    entityType: currentMerchant.entityType
                }));
            });
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorMsg = merchantLoadFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    }

    Instance.onSelectedMerchant = function (selectedOption) {
        $('#txtExportTaskEntityId').val(selectedOption.value);
        $('#txtExportTaskEntityName').val(selectedOption.label);
        $('#txtExportTaskEntityTypeId').val(selectedOption.attributes.entitytype.value);
    };



    Instance.validateExportTaskForm = function () {
        var retValue = (Instance.isValidExportTaskDescription() &
                Instance.isValidExportTaskEntityId() &
                Instance.isValidExportTaskEntityName() &
                Instance.isValidEntityTypeId() &
                Instance.isValidExportTaskRetrieverId() &
                Instance.isValidExportTaskFileExporterId() &
                Instance.isValidExportTaskPreprocessClass());
        return retValue;
    };

    Instance.isValidExportTaskDescription = function ()
    {
        var retrieverDescriptionField = document.getElementById("txtExportTaskDescription");
        var errorField = document.getElementById("exportTaskDescriptionValueError");
        var retValue = checkFieldEmpty(retrieverDescriptionField, errorField, exportTaskFormInvalidDescriptionLabel);
        return retValue;
    };

    Instance.isValidExportTaskEntityId = function ()
    {
        var retrieverClassField = document.getElementById("txtExportTaskEntityId");
        var errorField = document.getElementById("exportTaskEntityIdValueError");
        var retValue = checkFieldEmpty(retrieverClassField, errorField, exportTaskFormInvalidEntityIdLabel);
        return retValue;
    };

    Instance.isValidExportTaskEntityName = function ()
    {
        var dataSourceNameField = document.getElementById("txtExportTaskEntityName");
        var errorField = document.getElementById("exportTaskEntityNameValueError");
        var retValue = checkFieldEmpty(dataSourceNameField, errorField, exportTaskFormInvalidEntityNameLabel);
        return retValue;
    };

    Instance.isValidEntityTypeId = function ()
    {
        var dataSourceNameField = document.getElementById("txtExportTaskEntityTypeId");
        var errorField = document.getElementById("exportTaskEntityTypeIdValueError");
        var retValue = checkFieldEmpty(dataSourceNameField, errorField, exportTaskFormInvalidEntityTypeLabel);
        return retValue;
    };

    Instance.isValidExportTaskRetrieverId = function ()
    {
        var retrieverDetailQueryField = document.getElementById("selectExportTaskRetrieverId");
        var errorField = document.getElementById("exportTaskRetrieverIdValueError");
        var retValue = checkFieldEmpty(retrieverDetailQueryField, errorField, exportTaskFormInvalidRetrieverLabel);
        return retValue;
    };

    Instance.isValidExportTaskFileExporterId = function ()
    {
        var retrieverDetailQueryField = document.getElementById("selectExportTaskFileExporterId");
        var errorField = document.getElementById("exportTaskFileExporterIdValueError");
        var retValue = checkFieldEmpty(retrieverDetailQueryField, errorField, exportTaskFormInvalidFileExporterLabel);
        return retValue;
    };

    Instance.isValidExportTaskPreprocessClass = function ()
    {
        var retrieverFileNameDateFormat = document.getElementById("txtExportTaskQueryPreprocessorClass");
        var errorField = document.getElementById("exportTaskQueryPreprocessorClassValueError");
        var retValue = checkFieldEmpty(retrieverFileNameDateFormat, errorField, exportTaskFormInvalidPreprocessClassLabel);
        return retValue;
    };



    Instance.editExportTaskClick = function (taskExporterId) {
        var exporterTask = exportTaskMap[taskExporterId];

        currentRecordId = exporterTask.id;
        editingExportTask = true;
        Instance.clearExportTaskForm();
        updateRetrieverSelect();
        updateFileExporterSelect();
        $('#txtExportTaskId').val(currentRecordId);
        $('#txtExportTaskDescription').val(exporterTask.description);
        $('#txtExportTaskEntityId').val(exporterTask.entityId);
        $('#txtExportTaskEntityName').val(exporterTask.entityName);
        $('#txtExportTaskEntityTypeId').val(exporterTask.entityTypeId);
        $('#selectExportTaskRetrieverId').val(exporterTask.retrieverId).change();
        $('#selectExportTaskFileExporterId').val(exporterTask.fileExporterId).change();
        $('#txtExportTaskQueryPreprocessorClass').val(exporterTask.queryPreprocessorClass);
        $('#chkbExportTaskEnabled').prop("checked", exporterTask.taskEnabled);
        $('#addEditExportTaskFormTitle').html(exportTaskFormEditTitle);
        $('#addEditExportTaskBoostrapForm').modal('show');
    };

    Instance.clearExportTaskForm = function () {
        $('#txtExportTaskId').val("");
        $('#txtExportTaskDescription').val("");
        $('#selectAgent').empty();
        $('#selectAgentDiv').hide();
        $('#selectSubAgent').empty();
        $('#selectSubAgentDiv').hide();
        $('#selectRep').empty();
        $('#selectMerchant').empty();
        $('#exportTaskDescriptionValueError').text("");
        $('#txtExportTaskEntityId').val("");
        $("#txtExportTaskEntityId").attr('disabled', 'disabled');
        $('#exportTaskEntityIdValueError').text("");
        $('#txtExportTaskEntityName').val("");
        $("#txtExportTaskEntityName").attr('disabled', 'disabled');
        $('#exportTaskEntityNameValueError').text("");
        $('#txtExportTaskEntityTypeId').val("");
        $('#exportTaskEntityTypeIdValueError').text("");
        $('#exportTaskRetrieverIdValueError').text("");
        $('#exportTaskFileExporterIdValueError').text("");
        $('#txtExportTaskQueryPreprocessorClass').val("");
        $('#exportTaskQueryPreprocessorClassValueError').text("");
        $('#chkbExportTaskEnabled').attr("checked", false);
    };

    function loadExportTasksTable() {
        exportTasksTable = $('#exportTasksTable').DataTable({
            searching: false,
            bLengthChange: false,
            autoWidth: false,
            rowId: 'id',
            data: exportTaskList,
            columns: [
                {
                    title: exportTaskThId,
                    data: 'id', 
                    visible: false
                },
                {
                    title: exportTaskThDescription,
                    data: 'description'
                },
                {
                    title: exportTaskThEntityId,
                    data: 'entityId', order: 'asc'
                },
                {
                    title: exportTaskThEntityName,
                    data: 'entityName',
                    width: 200
                },
                {
                    title: exportTaskThEntityType,
                    data: 'entityTypeId',
                    render: function (data, type, row) {
                        if (entityTypes) {
                            return entityTypes[row.entityTypeId].description;
                        } else {
                            return row.entityTypeId;
                        }
                    }
                },
                {
                    title: exportTaskThRetriever,
                    data: 'retrieverId',
                    render: function (data, type, row) {
                        if (retrieverMap) {
                            return retrieverMap[row.retrieverId].description;
                        } else {
                            return row.retrieverId;
                        }
                    }
                },
                {
                    title: exportTaskThExporter,
                    data: 'fileExporterId',
                    render: function (data, type, row) {
                        if (exporterMap) {
                            return exporterMap[row.fileExporterId].description;
                        } else {
                            return row.fileExporterId;
                        }
                    }
                },
                {
                    title: exportTaskThQueryProcessorClass,
                    data: 'queryPreprocessorClass'
                },

                {
                    title: exportTaskThTaskEnabled,
                    data: 'taskEnabled',
                    className: "dt-center",
                    render: function (data, type, row) {
                        var checkBoxId = 'useDateRangeChkb_' + row.id;
                        var strChkBControl = '<input id="' + checkBoxId + '" name="' + checkBoxId + '" type="checkbox" disabled="disabled"';
                        if (row.taskEnabled) {
                            strChkBControl += ' checked="checked"';
                        }
                        strChkBControl += ' />';
                        return  strChkBControl;
                    }
                },

                {
                    title: exportTaskThActions,
                    data: '',
                    render: function (data, type, row) {
                        var actionColumnButtons = '<a onclick="exportTasksController.editExportTaskClick(&#39;' + row.id + '&#39;)" class="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true">&nbsp;&nbsp;</span></a>';
                        actionColumnButtons += '<a onclick="confirmRecordDeletion(&#39;' + row.id + '&#39;)" class="delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
                        return  actionColumnButtons;
                    }
                }
            ],
            language: {
                "info": tblNavInfoLbl,
                "infoEmpty": tblNavInfoEmptyLbl,
                paginate: {
                    first: tblNavFirstLbl,
                    previous: tblNavPrevLbl,
                    next: tblNavNextLbl,
                    last: tblNavLastLbl
                }
            }
        });
        exportTasksTable.order([2, 'asc']).draw();
    }

    function insertNewExportTask() {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/insertEntityExportTask',
            method: 'POST',
            dataType: "json",
            data: $('#addEditExportTaskForm').serialize()
        }).done(function (response) {
            if (response.resultCode === "000") {
                Instance.loadExportTasks();
            } else {
                errorMsg = exportTaskInsertionFailMsg + " " + response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            errorMsg = exportTaskInsertionFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });

    }

    function editExportTask() {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/editEntityExportTask',
            method: 'POST',
            dataType: "json",
            data: $('#addEditExportTaskForm').serialize()
        }).done(function (response) {
            if (response.resultCode === "000") {
                Instance.loadExportTasks();
            } else {
                errorMsg = exportTaskEditFailMsg + " " + response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            errorMsg = exportTaskEditFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });

    }

    Instance.deleteExportTaskRecord = function () {
        var errorMsg = "";
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/deleteExportTask/' + currentRecordId,
            method: 'DELETE',
            dataType: "json"
        }).done(function (response) {
            if (response.resultCode === "000") {
                Instance.loadExportTasks();
            } else {
                errorMsg = exportTaskDeletionFailMsg + " " + response.resultCode + " " + response.resultMessage;
                showErrorAlert(errorMsg);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            errorMsg = exportTaskDeletionFailMsg + " " + jqXHR.status + " " + textStatus + " " + errorThrown;
            showErrorAlert(errorMsg);
        });
    };




    function updateRetrieverSelect() {
        $('#selectExportTaskRetrieverId').empty();
        retrieverList.forEach(function (currentRetriever) {
            $('#selectExportTaskRetrieverId').append($('<option>', {
                value: currentRetriever.id,
                text: currentRetriever.description
            }));
        });
    }


    function updateFileExporterSelect() {
        $('#selectExportTaskFileExporterId').empty();
        exporterList.forEach(function (currentExporter) {
            $('#selectExportTaskFileExporterId').append($('<option>', {
                value: currentExporter.id,
                text: currentExporter.description
            }));
        });
    }

    Instance.init();
}
;

var exportTasksController = new FileExportTasks();