/* 
 * EMIDA all rights reserved 1999-2019.
 */


$(document).ready(function () {

    $('#deleteRecordModal').submit(function (e) {
        e.preventDefault();
        deleteRecord();
    });
});

/* global retrieversController, exportersController, exportTasksController */

function deleteRecord() {
    $('#deleteRecordModal').modal('hide');
    // Check what tab is active, that means on what entity the deletion will happen    
    var currentTabId = $("ul#exporterTabs li.active a").attr('id');
    switch (currentTabId) {
        case 'exportTasksTab':
            exportTasksController.deleteExportTaskRecord();
            break;
        case 'retrieversTab':
            retrieversController.deleteRetrieverRecord();
            break;
        case 'fileExportersTab':
            exportersController.deleteFileExporterRecord();
            break;
        case 'fileTransferTasksTab':
            fileTransferTasks.deleteRetrieverRecord();
            break;
    }
    return true;
}




