
/* global fileTransferThDescription, fileTransferThFileNamePattern, fileTransferThTransferredFiles, fileTransferThProcessedFolder, fileTransferThRetriesOnTransferFail, fileTransferThSourceFolder, fileTransferThTargetFolder, fileTransferThTaskEnabled, retrieverThActions, fileTransferThTaskTypeId, fileTransferThConnectionId, tblNavInfoLbl, tblNavInfoEmptyLbl, tblNavFirstLbl, tblNavPrevLbl, tblNavNextLbl, tblNavLastLbl, modalErrorRequiredTransferType, modalErrorInvalidTransferType, modalErrorRequiredConnectionType, modalErrorInvalidConnectionType, modalErrorRequiredCheckTransferredFiles, modalErrorInvalidCheckTransferredFiles, modalErrorRequiredTaskEnabled, modalErrorInvalidTaskEnabled, modalErrorRequiredDescription, modalErrorRequiredFileNamePattern, modalErrorRequiredProcessedFolder, modalErrorRequiredRetriesOnTransferFail, modalErrorRequiredSourceFolder, modalErrorRequiredTargetFolder, fileTransferThNoConnectionTypes, modalErrorRetriesOnTransferMinLength, fileTransferThGeneralError, fileTransferThSuccess, currentRecordId, fileTransferThNoTransferTypes, MAX_COLUMN_TEXT_LENGTH */

function FileTransferTasks() {

    var Instance = this;
    var fmNewTransfer;
    var tableFileTransferTask;

    Instance.init = function () {
        $(document).ready(function () {

            tableFileTransferTask = $('#fileTransferTasksTable').DataTable({
                info: false,
                searching: false,
                bLengthChange: false,
                pageLength: 10,
                "ajax": {
                    "url": "/support/tools/conciliationExporter/getAllTransferTypesConnection",
                    "type": "GET",
                    "dataSrc": ""
                },
                "columns": [
                    {mData: "description", title: fileTransferThDescription},
                    {mData: "fileTransferTaskTypeName", title: fileTransferThTaskTypeId},
                    {mData: "fileTransferConnectionName", title: fileTransferThConnectionId},
                    {mData: "fileNamePattern", title: fileTransferThFileNamePattern},
                    {
                        data: "sourceFolder",
                        title: fileTransferThSourceFolder,
                        render: function (data, type, row) {
                            if (row.sourceFolder.length > MAX_COLUMN_TEXT_LENGTH) {
                                return row.sourceFolder.substring(0, MAX_COLUMN_TEXT_LENGTH);
                            } else {
                                return row.sourceFolder;
                            }
                        }
                    },
                    {
                        data: "targetFolder",
                        title: fileTransferThTargetFolder,
                        render: function (data, type, row) {
                            if (row.targetFolder.length > MAX_COLUMN_TEXT_LENGTH) {
                                return row.targetFolder.substring(0, MAX_COLUMN_TEXT_LENGTH);
                            } else {
                                return row.targetFolder;
                            }
                        }
                    },
                    {
                        data: "processedFolder",
                        title: fileTransferThProcessedFolder,
                        render: function (data, type, row) {
                            if (row.processedFolder.length > MAX_COLUMN_TEXT_LENGTH) {
                                return row.processedFolder.substring(0, MAX_COLUMN_TEXT_LENGTH);
                            } else {
                                return row.processedFolder;
                            }
                        }
                    },
                    {
                        data: "checkTransferredFiles",
                        title: fileTransferThTransferredFiles,
                        className: "dt-center",
                        render: function (data, type, row) {
                            var checkBoxId = 'checkTRansferredFilesChkb_' + row.id;
                            var strChkBControl = '<input id="' + checkBoxId + '" name="' + checkBoxId + '" type="checkbox" disabled="disabled"';
                            if (row.checkTransferredFiles) {
                                strChkBControl += ' checked="checked"';
                            }
                            strChkBControl += ' />';
                            return  strChkBControl;
                        }
                    },
                    {mData: "retriesOnTransferFail", title: fileTransferThRetriesOnTransferFail},
                    {
                        data: "taskEnabled",
                        title: fileTransferThTaskEnabled,
                        className: "dt-center",
                        render: function (data, type, row) {
                            var checkBoxId = 'taskEnabledChkb_' + row.id;
                            var strChkBControl = '<input id="' + checkBoxId + '" name="' + checkBoxId + '" type="checkbox" disabled="disabled"';
                            if (row.taskEnabled) {
                                strChkBControl += ' checked="checked"';
                            }
                            strChkBControl += ' />';
                            return  strChkBControl;
                        }
                    },
                    {
                        title: retrieverThActions,
                        render: function (data, type, row) {
                            var actionColumnButtons = '<button id="editTransferRow" class="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true">&nbsp;&nbsp;</span></button>';
                            actionColumnButtons += '<button onclick="confirmRecordDeletion(&#39;' + row.id + '&#39;)" id="deleteTransferRow" class="delete"><span class="glyphicon glyphicon-trash" aria-hidden="true">&nbsp;&nbsp;</span></button>';
                            return  actionColumnButtons;
                        }
                    }
                ],
                language: {
                    "info": tblNavInfoLbl,
                    "infoEmpty": tblNavInfoEmptyLbl,
                    paginate: {
                        first: tblNavFirstLbl,
                        previous: tblNavPrevLbl,
                        next: tblNavNextLbl,
                        last: tblNavLastLbl
                    }
                }
            });

            fmNewTransfer = $("#fmFileTransfer").validate({
                errorClass: 'errorLabelField',
                rules: {
                    fileTransferTaskTypeId: {
                        required: true,
                        selectValidate: true
                    },
                    fileTransferConnectionId: {
                        required: true,
                        selectValidate: true
                    },
                    taskEnabled: {
                        required: true,
                        selectValidate: true
                    },
                    description: {
                        required: true
                    },
                    fileNamePattern: {
                        required: true
                    },
                    processedFolder: {
                        required: true
                    },
                    retriesOnTransferFail: {
                        required: true,
                        retriesOnTransferValidate: true
                    },
                    sourceFolder: {
                        required: true
                    },
                    targetFolder: {
                        required: true
                    }
                },
                messages: {
                    fileTransferTaskTypeId: {
                        required: modalErrorRequiredTransferType,
                        selectValidate: modalErrorInvalidTransferType
                    },
                    fileTransferConnectionId: {
                        required: modalErrorRequiredConnectionType,
                        selectValidate: modalErrorInvalidConnectionType
                    },
                    taskEnabled: {
                        required: modalErrorRequiredTaskEnabled,
                        selectValidate: modalErrorInvalidTaskEnabled
                    },
                    description: {
                        required: modalErrorRequiredDescription
                    },
                    fileNamePattern: {
                        required: modalErrorRequiredFileNamePattern
                    },
                    processedFolder: {
                        required: modalErrorRequiredProcessedFolder
                    },
                    retriesOnTransferFail: {
                        required: modalErrorRequiredRetriesOnTransferFail,
                        retriesOnTransferValidate: modalErrorRetriesOnTransferMinLength
                    },
                    sourceFolder: {
                        required: modalErrorRequiredSourceFolder
                    },
                    targetFolder: {
                        required: modalErrorRequiredTargetFolder
                    }
                },
                highlight: function (element) {
                    $(element).parent().removeClass('errorLabelField');
                }
            });

            jQuery.validator.addMethod("selectValidate", function (value, element) {
                return value !== null && value !== undefined && value !== "" && typeof value !== undefined;
            }, "Select not allowed");

            jQuery.validator.addMethod("retriesOnTransferValidate", function (value, element) {
                return (value >= 0 && value <= 10);
            }, "Select not allowed");

            $('#updateFileTransferModal').on('hidden.bs.modal', function () {
                clearFields(); // Set default values
                $('#fmFileTransfer')[0].reset(); // Reset form
                $("#transactionType").val("");
            });


            $("#fmFileTransfer").submit(function (event) {
                if (typeof fmNewTransfer !== "undefined") {
                    if (fmNewTransfer.numberOfInvalids() === 0) {
                        if ($("#transactionType").val() === "ADD") {
                            insertFileTransferTask(event);
                        } else if ($("#transactionType").val() === "EDIT") {
                            editFileTransferTask();
                        }

                        clearFields(); // Set default values
                        fmNewTransfer.resetForm(); // Hide error messages
                        $('#updateFileTransferModal').modal('hide'); // close modal
                        tableFileTransferTask.ajax.reload(); // Refresh table
                        $("#transactionType").val(""); // Set default Id value
                    }
                } else {
                    return false;
                }
                event.preventDefault(); // Don't refresh page
            });

            $('#fileTransferTasksTable tbody').on('click', '#editTransferRow', function () {
                $("#transactionType").val("EDIT");
                loadModal();
                var data = tableFileTransferTask.row($(this).parents('tr')).data();
                $("#fileTransferTaskTypeId").val(data.fileTransferTaskTypeId);
                $("#fileTransferConnectionId").val(data.fileTransferConnectionId);
                $('#checkTransferredFiles').prop("checked", data.checkTransferredFiles);
                $('#taskEnabled').prop("checked", data.taskEnabled);
                $("#description").val(data.description);
                $("#fileNamePattern").val(data.fileNamePattern);
                $("#processedFolder").val(data.processedFolder);
                $("#retriesOnTransferFail").val(data.retriesOnTransferFail);
                $("#sourceFolder").val(data.sourceFolder);
                $("#targetFolder").val(data.targetFolder);
                $("#id").val(data.id);
            });
        });
    };

    Instance.showModal = function () {
        $("#transactionType").val("ADD");
        loadModal();
    };

    Instance.closeModal = function () {
        $('#updateFileTransferModal').modal('hide');
    };

    var insertFileTransferTask = function (event) {
        $.ajax({
            url: '/support/tools/conciliationExporter/insertFileTransferTaskTypeConnection',
            method: 'POST',
            async: true,
            data: $('#fmFileTransfer').serialize()
        }).done(function (response) {
            if (response !== undefined && response !== null) {
                if (!response.error) {
                    tableFileTransferTask.ajax.reload();
                } else {
                    showResponse(false, "Error", response.resultMessage);
                }
            } else {
                showResponse(false, "Error", ((response.resultMessage !== undefined && typeof response.resultMessage !== "undefined") ? response.resultMessage : ""));
            }
        }).error(function (error) {
            console.log(error);
            showResponse(false, "Error", fileTransferThGeneralError);
        });
    };

    var editFileTransferTask = function (event) {
        $.ajax({
            url: '/support/tools/conciliationExporter/editFileTransferTaskTypeConnection',
            method: 'POST',
            async: true,
            data: $('#fmFileTransfer').serialize()
        }).done(function (response) {
            if (response !== undefined && response !== null) {
                if (!response.error) {
                    tableFileTransferTask.ajax.reload();
                } else {
                    showResponse(false, "Error", response.resultMessage);
                }
            } else {
                showResponse(false, "Error", ((response.resultMessage !== undefined && typeof response.resultMessage !== "undefined") ? response.resultMessage : ""));
            }
        }).error(function (error) {
            console.log(error);
            showResponse(false, "Error", fileTransferThGeneralError);
        });
    };


    Instance.deleteRetrieverRecord = function () {
        jQuery.ajax({
            url: '/support/tools/conciliationExporter/deleteFileTransferTaskTypeConnection/' + currentRecordId,
            method: 'DELETE',
            dataType: "json",
            async: false
        }).done(function (response) {
            if (response !== undefined && response !== null) {
                if (!response.error) {
                } else {
                    showResponse(false, "Error", response.resultMessage);
                }
            } else {
                showResponse(false, "Error", ((response.resultMessage !== undefined && typeof response.resultMessage !== "undefined") ? response.resultMessage : ""));
            }
        }).error(function (error) {
            showResponse(false, "Error", fileTransferThGeneralError);
        });
        tableFileTransferTask.ajax.reload(); // Refresh table
    };

    var getFileTransferTaskTypes = function () {
        $("#fileTransferTaskTypeId").empty(); // Reset field
        $("#fileTransferTaskTypeId").append($('<option>', {value: ""})); // Add empty option

        $.ajax({
            url: '/support/tools/conciliationExporter/getAllFileTransferTaskTypes',
            method: 'GET',
            async: false
        }).done(function (response) {
            if (response !== undefined && response !== null) {
                // Setting to select in modal
                response.forEach(function (value, key) {
                    idType = (value.id !== null && value.id !== undefined) ? value.id : "";
                    nameType = (value.name !== null && value.name !== undefined) ? value.name : "";

                    $("#fileTransferTaskTypeId").append($('<option>', {
                        value: idType,
                        text: nameType
                    }));
                });
            } else {
                showResponse(false, "Error", fileTransferThNoTransferTypes);
            }
        }).error(function (error) {
            console.log(error);
            showResponse(false, "Error", fileTransferThNoTransferTypes);
        });
    };

    var getFileTransferConnection = function () {
        $("#fileTransferConnectionId").empty(); // Reset field
        $("#fileTransferConnectionId").append($('<option>', {value: ""})); // Add empty option

        $.ajax({
            url: '/support/tools/conciliationExporter/getAllFileTransferConnections',
            method: 'GET',
            async: false
        }).done(function (response) {
            if (response !== undefined && response !== null) {
                response.forEach(function (value, key) { // Setting to select in modal
                    idType = (value.id !== null && value.id !== undefined) ? value.id : "";
                    descriptionType = (value.description !== null && value.description !== undefined) ? value.description : "";

                    $("#fileTransferConnectionId").append($('<option>', {
                        value: idType,
                        text: descriptionType
                    }));
                });
            } else {
                showResponse(false, "Error", fileTransferThNoConnectionTypes);
            }
        }).error(function (error) {
            console.log(error);
            showResponse(false, "Error", fileTransferThNoConnectionTypes);
        });
    };

    var showResponse = function (messageType, title, message) {
        if (messageType) {
            toastr.success(message, title, {positionClass: "toast-top-center"});
        } else {
            toastr.error(message, title, {positionClass: "toast-top-center"});
        }
    };

    var loadModal = function () {
        getFileTransferTaskTypes();
        getFileTransferConnection();
        $('#updateFileTransferModal').modal('show');
    };

    var clearFields = function () {
        $('#fmFileTransfer').find("input[type=text], input[type=number], textarea, select").val("");
    };

    Instance.init();
}
;

var fileTransferTasks = new FileTransferTasks();
