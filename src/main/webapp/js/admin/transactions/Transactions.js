
function Transactions() {
    var Instance = this;

    Instance.init = function () {
        $(document).ready(function () {
            
            var actualDate = new Date();
            actualDate = actualDate.getTime();
            
            $('#tableTransactions').DataTable({
                dom: 'Bfr<ip<t>f>',
                buttons: [{
                    extend : 'csvHtml5',
                    text : downloadTitle,
                    title: 'debisys_' + actualDate,
                    filename: 'debisys_' + actualDate,
                    customize : function(csv) {
                        
                        var split_CopyCsv = csv.split("\n");
                        var arrayDetail = new Array();
                        var split_csv = new Array();
                        var copyDetailCsv = clone(splitDetailCsv);
                        
                        copyDetailCsv.splice(5, 0, ""); // Add line break
                        
                        copyDetailCsv.forEach(function(index, value) { // this variable copyDetailCsv declared in transactions.jsp has information detail for csv.
                            if (value != 0 && value != 3 && value != 4) {
                                var titleFilter = copyDetailCsv[value];
                                var valueFilter = copyDetailCsv[value + 1];
                                arrayDetail.push(("\"" + (titleFilter != undefined ? titleFilter.trim() : "") + "\"") + "," + ("\"" + (valueFilter != undefined ? valueFilter.trim() : "") + "\""));
                                copyDetailCsv.splice((value + 1), 1);
                            } else {
                                arrayDetail.push("\"" + index.trim() + "\"");
                            }
                        });
                        
                       function clone(obj) {
                            if (null == obj || "object" != typeof obj) return obj;
                            var copy = obj.constructor();
                            for (var attr in obj) {
                                if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
                            }
                            return copy;
                        }
                        
                        split_csv = arrayDetail; // Add filter csv information
                        split_csv.push(""); // Add empty row
                        split_csv.push.apply(split_csv, split_CopyCsv); // Add csv to detail filter
                        csv = split_csv.join("\n");
                        return csv; 
                    }
                }],
                stripeClasses: ['stripe1','stripe2'],
                bInfo : false,
                searching : false,
                pageLength: 50
            });
        });
    };
    Instance.init();
}

var transactions = new Transactions();