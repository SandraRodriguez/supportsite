

function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {      // If Internet Explorer, return version number
        return true;
    }
    else {
        return false;
    }
}

function editDepositType(id) {
    showWindowsCharge();
    if (isInternetExplorer()) {
        window.location.href = "depositTypesList.jsp?action=edit&id=" + id + "";
    }
    else {
        window.location.href = "admin/tools/depositTypes/depositTypesList.jsp?action=edit&id=" + id + "";
    }
    showForm();
}

function showWindowsCharge() {
    document.getElementById('div_show_charge').style.display = "block";
}

function hideWindowsCharge() {
    $('#div_show_charge').css("display", "none");
}

function newDepositType() {
    $('#descriptionEng').val('');
    $('#descriptionSpa').val('');
    $('#depositCode').val('');
    $('#action').val('insertDeposit');
    showForm();
}

function showForm() {
    $('#div_form').css("display", "block");
}

function hideForm() {
    $('#div_form').css("display", "none");
}

function deleteDepositType(id, message, messageNotCanDelete) {

    var canDelete= true;
    $.ajax({
        type: "POST",
        async: false,
        data: {depositTypeId: id, action: 'canDeleteDepositType'},
        url: "admin/tools/depositTypes/depositTypesProcessDB.jsp",
        success: function (data) {
            var array_data = String($.trim(data));
            if (array_data === 'false') {
                alert(messageNotCanDelete);
                canDelete = false;
            }
        }
    });
    if(canDelete){
        showWindowsCharge();
        if (confirm(message)) {
            if (isInternetExplorer()) {
                window.location.href = "depositTypesList.jsp?action=removeDepositType&id=" + id + "";
            }
            else {
                window.location.href = "admin/tools/depositTypes/depositTypesList.jsp?action=removeDepositType&id=" + id + "";
            }
        }
        else {
            hideWindowsCharge();
        }
    }
}

function goBack(msg) {
    if (isInternetExplorer()) {
        window.location.href = "depositTypesList.jsp?msg=" + msg;
    }
    else {
        window.location.href = "admin/tools/depositTypes/depositTypesList.jsp?msg=" + msg;
    }
}

function validateForm(errorDuplicate) {
    removeRedBorder();
    var hasErrors = false;
    var descriptionEng = $("#descriptionEng").val();
    var descriptionSpa = $("#descriptionSpa").val();    
    var depositCode    = $("#depositCode").val();    
    if (descriptionEng.trim() === '' || descriptionSpa.trim() === '' || depositCode.trim() === '' ) {
        markErrorFields();
        hasErrors = true;
    }
    debugger;
    if (!hasErrors) {
        $.ajax({
            type: "POST",
            async: false,
            data: {descriptionEng: descriptionEng, 
                   descriptionSpa: descriptionSpa,                    
                   action: 'verifyDuplicateDepositType'},
            url: "admin/tools/depositTypes/depositTypesProcessDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data));               
                if (array_data === 'true') {
                    alert(errorDuplicate);
                    hasErrors = true;
                }
            }
        });
    }

    return !hasErrors;
}

function markErrorFields() {
    var descriptionEng = $("#descriptionEng").val();
    var descriptionSpa = $("#descriptionSpa").val();
    var depositCode    = $("#depositCode").val();    
    if (descriptionEng.trim() === '') {
        $("#descriptionEng").css("border", "1px solid red");
    }
    if (descriptionSpa.trim() === '') {
        $("#descriptionSpa").css("border", "1px solid red");
    }
    if (depositCode.trim() === '') {
        $("#depositCode").css("border", "1px solid red");
    }

}

function removeRedBorder() {
    $("#descriptionEng").css('border', '');
    $("#descriptionSpa").css('border', '');
}



