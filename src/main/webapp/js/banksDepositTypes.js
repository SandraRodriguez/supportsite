

function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {// If Internet Explorer, return version number
        return true;
    }
    else {
        return false;
    }
}

function editBanksDepositType(id, messageNotCanEdit) {

    var canDelete = true;

    if (canDelete) {
        showWindowsCharge();
        if (isInternetExplorer()) {
            window.location.href = "banksDepositTypes.jsp?action=edit&bankId=" + id + "";
        }
        else {
            window.location.href = "admin/tools/depositTypes/banksDepositTypes.jsp?action=edit&bankId=" + id + "";
        }
        showForm();
    }
}

function showWindowsCharge() {
    document.getElementById('div_show_charge').style.display = "block";
}

function hideWindowsCharge() {
    $('#div_show_charge').css("display", "none");
}

function newDepositType() {
    removeRedBorder();
    $("#selectBanks option").prop('selected', false);
    $("#selectDepositTypes option").prop('selected', false);
    showForm();
}

function showForm() {
    $('#div_form_banks').css("display", "block");
}

function hideForm() {
    $('#div_form_banks').css("display", "none");
}

function deleteBanksDepositType(id, message, messageNotCanDelete) {

    var canDelete = true;

    $.ajax({
        type: "POST",
        async: false,
        data: {bankId: id, action: 'canDeleteBankDepositType'},
        url: "admin/tools/depositTypes/depositTypesProcessDB.jsp",
        success: function (data) {
            var array_data = String($.trim(data));
            if (array_data === 'false') {
                alert(messageNotCanDelete);
                canDelete = false;
            }
        }
    });

    if (canDelete) {
        showWindowsCharge();
        if (confirm(message)) {
            if (isInternetExplorer()) {
                window.location.href = "banksDepositTypes.jsp?action=removeBanksDepositType&bankId=" + id + "";
            }
            else {
                window.location.href = "admin/tools/depositTypes/banksDepositTypes.jsp?action=removeBanksDepositType&bankId=" + id + "";
            }
        }
        else {
            hideWindowsCharge();
        }
    }
}

function goBack(msg) {
    if (isInternetExplorer()) {
        window.location.href = "banksDepositTypes.jsp?msg=" + msg;
    }
    else {
        window.location.href = "admin/tools/depositTypes/banksDepositTypes.jsp?msg=" + msg;
    }
}

function validateForm(messageDuplicate) {
    removeRedBorder();

    var hasErrors = false;

    if ($('#selectBanks').val() === '-1') {
        $("#selectBanks").css("border", "1px solid red");
        hasErrors = true;
    }

    var values = $('#selectDepositTypes').val();
    if (values === null) {
        $("#selectDepositTypes").css("border", "1px solid red");
        hasErrors = true;
    }
    
    return !hasErrors;
}


function removeRedBorder() {
    $("#selectBanks").css('border', '');
    $("#selectDepositTypes").css('border', '');
}



