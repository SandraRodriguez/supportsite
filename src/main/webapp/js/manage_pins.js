var saveButtonText = '';
var cancelButtonText = '';
var okButtonText = '';
var differentError = '';
var changeStatustext = '';
var filterText = '';

function changeStatus(toStatus){
	var pcasid = $('#changeDialog').data('pcasid');
	var note = $('#noteTxt').val();
	if(pcasid != null && pcasid != undefined && pcasid != '' && toStatus > 0){
		if(note != null && note != undefined && note != '' && toStatus > 0){
			$('#changeDialog').dialog('close');
			// TODO: add wait dialog
			$.getJSON("/support/includes/ajax.jsp?class=com.debisys.pincache.Pincache&method=changePINStatusAjax&value=" + pcasid + "_" + toStatus + "_" + note +"&rnd=" + Math.random(),
					function(data){
						$('#changeDialog').dialog('close');
						$('#customMessage')
							.html('<div class="'+data.status+'">'+data.msgs.join('<br />')+'</div>')
							.dialog('option','height', 400)
							.dialog('option','width', 800)
							.dialog('option','buttons', { Ok: function(){location.reload();}})
							.dialog('open');
					}
				).error(function(){
					$('#customMessage').html('<div class="error">'+errorMsg+'</div>');
					$('#customMessage').dialog('open');
				});
			$('#changeDialog').data('pcasid', '');
		}else{
			$('#noteTxtLbl').attr('style', 'color:red');
			$('#changeDialog').parent().effect('shake', { times:5 }, 400);
		}
	}else{
		$('#changeDialog').dialog('close');
		$('#changeDialog').data('pcasid', '');
	}
}
$(document).ready( function(){
	$('#customMessage').dialog({
		autoOpen: false,
		height: 250,
		width: 300,
		modal: true,
		title: 'Error',
		buttons: [{
			text: okButtonText,
			click: function(){
				$('#customMessage').dialog('close');
			}
		}],
	});	
	$('#pinToStatus3')
		.button({icons:{primary:'ui-icon-check'}})
		.click(function(){
			changeStatus(3);
		});
	$('#pinToStatus5')
		.button({icons:{primary:'ui-icon-alert'}})
		.click(function(){
			changeStatus(5);
		});
	$('#allpins').change(function(){
		if($(this).is(':checked')){
			$('#pinTable').find('input[type=checkbox]').attr('checked', 'checked');
		}else{
			$('#pinTable').find('input[type=checkbox]').removeAttr('checked');
		}
	});
	$('.selectAllStatus2').button({icons: {primary: 'ui-icon-check'}}).click(function(){
		$('#pinTable').find('input[type=checkbox]').removeAttr('checked').each(function(){
			if($(this).parent().parent().data('status') == '2'){
				$(this).attr('checked', 'checked');	
			}
		});
	});
	$('.selectAllStatus5').button({icons: {primary: 'ui-icon-check'}}).click(function(){
		$('#pinTable').find('input[type=checkbox]').removeAttr('checked').each(function(){
			if($(this).parent().parent().data('status') == '5'){
				$(this).attr('checked', 'checked');	
			}
		});
	});
	$('.selectAllStatus8').button({icons: {primary: 'ui-icon-check'}}).click(function(){
		$('#pinTable').find('input[type=checkbox]').removeAttr('checked').each(function(){
			if($(this).parent().parent().data('status') == '8'){
				$(this).attr('checked', 'checked');	
			}
		});
	});
	$('#changeDialog').dialog({
		autoOpen: false,
		height: 400,
		width: 400,
		modal: true,
		title: changeStatustext,
		buttons: [{
			text: cancelButtonText,
			click: function(){
				$('#changeDialog').dialog('close');
			}
		}],
	});
	$('.changeStatus').button().click(function(){
		$('#changeDialog').data('pcasid', $(this).parent().parent().data('pcasid'));
		var s = $(this).parent().parent().data('status');
		if(s == '2'){
			$('#pinToStatus5').button({ disabled: false });
			$('#pinToStatus3').button({ disabled: false });
			$('#changeDialog').dialog('open');
		}else if(s == '5'){
			$('#pinToStatus5').button({ disabled: true });
			$('#pinToStatus3').button({ disabled: false });
			$('#changeDialog').dialog('open');
		}else if(s == '8'){
			$('#pinToStatus5').button({ disabled: true });
			$('#pinToStatus3').button({ disabled: false });
			$('#changeDialog').dialog('open');
		}
	});
	$('.changeStatusSelected').button().click(function(){
		var commonStatus = 0;
		var pcasid = '';
		$('#pinTable').find('input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				if(commonStatus == 0){
					commonStatus = $(this).parent().parent().data('status');
				}else{
					if(commonStatus != $(this).parent().parent().data('status')){
						commonStatus = 'error';
					}
				}
				if(pcasid != ''){
					pcasid += '@';
				}
				pcasid += $(this).parent().parent().data('pcasid');
			}
		});
		if(commonStatus == '2'){
			$('#pinToStatus5').button({ disabled: false });
			$('#pinToStatus3').button({ disabled: false });
			$('#changeDialog').dialog('open');
		}else if(commonStatus == '5'){
			$('#pinToStatus5').button({ disabled: true });
			$('#pinToStatus3').button({ disabled: false });
			$('#changeDialog').dialog('open');
		}else if(commonStatus == '8'){
			$('#pinToStatus5').button({ disabled: true });
			$('#pinToStatus3').button({ disabled: false });
			$('#changeDialog').dialog('open');
		}else{
			commonStatus = 'error';
		}
		if(commonStatus != 'error'){
			$('#changeDialog').data('pcasid', pcasid);
		}else{
			$('#customMessage').html('<div class="error">'+differentError+'</div>').dialog('open');
		}
	});
	$('#searchBySiteIdBtn').button({icons:{primary:'ui-icon-search'}});
	var oTable = $('#pinTable').dataTable({
		"sPaginationType": "full_numbers",
		"oLanguage": {
			"sSearch": filterText,
			"sFirst": "First page",
			"sLast": "Last page",
			"sNext": "Next page",
			"sPrevious": "Previous page",
			"sInfoFiltered": " - filtering from _MAX_ records"
		}		
	});
});