

function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {// If Internet Explorer, return version number
        return true;
    }
    else {
        return false;
    }
}

function editIsoDomains(id) {

    $('#spanWarningMessages').text('');
    showWindowsCharge();
    if (isInternetExplorer()) {
        window.location.href = "isoDomainList.jsp?action=edit&idRow=" + id + "";
    }
    else {
        window.location.href = "admin/tools/smsInventory/isoDomainList.jsp?action=edit&idRow=" + id + "";
    }
    showForm();

}

function showWindowsCharge() {
    document.getElementById('div_show_charge').style.display = "block";
}

function hideWindowsCharge() {
    $('#div_show_charge').css("display", "none");
}

function newIsoDomain() {
    $('#spanWarningMessages').text('');
    removeRedBorder();
    $("#isoList option").prop('selected', false);
    $("#domain").val('');
    showForm();
}

function showForm() {
    $('#div_form_isoDomain').css("display", "block");
}

function hideForm() {
    $('#div_form_isoDomain').css("display", "none");
}

function deleteIsoDomain(id, message, messageNotCanDelete) {

    showWindowsCharge();
    if (confirm(message)) {
        if (isInternetExplorer()) {
            window.location.href = "isoDomainList.jsp?action=removeIsoDomain&idRow=" + id + "";
        }
        else {
            window.location.href = "admin/tools/smsInventory/isoDomainList.jsp?action=removeIsoDomain&idRow=" + id + "";;
        }
    }
    else {
        hideWindowsCharge();
    }   
    
}

function goBack(msg) {
    if (isInternetExplorer()) {
        window.location.href = "isoDomainList.jsp" ;
    }
    else {
        window.location.href = "admin/tools/smsInventory/isoDomainList.jsp";
    }
}

function validateForm(message, action, id) {
    
    removeRedBorder();
    var hasErrors = false;

    if ($('#isoList').val() === '-1') {
        $("#isoList").css("border", "1px solid red");
        hasErrors = true;
    }

    var domain = $('#domain').val();
    if (domain === null || domain.trim()==='') {
        $("#domain").css("border", "1px solid red");
        hasErrors = true;
    }
    
    if(!hasErrors){
        var user = $('#domain').val();
        $.ajax({
            type: "POST",
            async: false,
            data: {id: id, action: action, user: user},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data));
                if (array_data === 'false') {
                    alert(message);
                    hasErrors = true;
                }
            }
        });
    }
    
    return !hasErrors;
}

function changeSiteIdByIso() {
    var isoSelected = $('#isoList').val();
    if (isoSelected === '-1') {
        return;
    }

    $('#defaultSiteId').html('');

    $.ajax({
        type: "POST",
        async: false,
        data: {action: 'getSiteIdsByIso', isoId: isoSelected},
        url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
        success: function (data) {

            var existData = false;
            var arrayRepsList = String($.trim(data)).split("\n");
            for (var i = 0; i < arrayRepsList.length; i++) {
                if (arrayRepsList[i] !== '') {
                    existData = true;
                    var currentRep = String($.trim(arrayRepsList[i])).split("|");
                    $('#defaultSiteId').append('<option value="' + currentRep[0] + '" selected="selected">' + currentRep[1] + '</option>');
                }
            }
            if(existData === false){
                $('#defaultSiteId').append('<option value="0" selected="selected"></option>');
            }
        }
    });
}


function removeRedBorder() {
    $("#isoList").css('border', '');
    $("#domain").css('border', '');
}



