/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var marker;
var markers = [];
var map = null;
var latitudeMap = -1;
var longitudeMap = -1;
var zoomPoint = 12;
var merchantId = -1;
var hideWarnning1 = 0;
//means if we are editing or creating a new entity
//typeAction=1 editing
//typeAction=0 creating
var typeAction = 0;

function initMap(merchantMap) {
    if (latitudeMap === -1)
        return;
    if (map !== null) {
        return;
    }   
    var mapInput = document.getElementById('map');
    map = new google.maps.Map(mapInput, {
        center: {lat: latitudeMap, lng: longitudeMap},
        zoom: zoomPoint
    });
    
        
    if (merchantMap != undefined && merchantMap != null && 
        merchantMap == false) {
        addMarker({lat: latitudeMap, lng: longitudeMap});
    }
    
    mapInput.style.display = '';
    
    var warnningLabelImg = document.getElementById('warnningLabelImg');

    var input = document.getElementById('pac-input');
    input.style.display = '';
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    if (hideWarnning1 === 0) {
        warnningLabelImg.style.display = '';
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(warnningLabelImg);
    }

    var warnningLabel = document.getElementById('warnningLabel');

    var types = document.getElementById('type-selector');
    //types.style.display = '';
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(types);

    if (typeAction === 1) {
        var pacSave = document.getElementById('pacSave');
        if (pacSave !== null) {
            pacSave.style.cursor = 'pointer';
            pacSave.style.display = '';
            pacSave.addEventListener('click', function () {
                saveMerchant();
            });
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(pacSave);
        }
    }
    var pacClose = document.getElementById('pacClose');
    if (pacClose !== null) {
        pacClose.style.cursor = 'pointer';
        pacClose.style.display = '';
        pacClose.addEventListener('click', function () {
            var latitudeT = document.getElementById('latitudeTemp').value;
            var longitudeT = document.getElementById('longitudeTemp').value;
            
            localStorage["latitudeMap"] = latitudeT;
            localStorage["longitudeMap"] = longitudeT;
            
            if (typeAction === 0) {//if creating entity                
                if (latitudeT === "-1" || longitudeT === "-1") {
                    window.location.href = "/support/admin/customers/merchants.jsp?search=y";
                }
            } else {
                latitudeT = document.getElementById('latitudeCurrent').value;
                longitudeT = document.getElementById('longitudeCurrent').value;
                if (latitudeT === "-1" || longitudeT === "-1") {
                    window.location.href = "/support/admin/customers/merchants_info.jsp?merchantId=" + merchantId;
                }
            }
            $('#map').hide();
            $('#mainTableSupport').show();
            $('#divPrintBanner').show();

        });
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(pacClose);
    }

    if (hideWarnning1 === 0) {
        warnningLabel.style.display = '';
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(warnningLabel);
    }


    var infowindow = new google.maps.InfoWindow();
    marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });


    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', function () {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(12);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
        setLocationMerchant(place.geometry.location.lat(), place.geometry.location.lng());
    });



    google.maps.event.addListener(map, 'click', function (event) {
        marker.setVisible(false);
        addMarker(event.latLng);
        map.panTo(event.latLng);
    });

    function addMarker(location) {
        var latitude = "";
        var longitude = ""
            
        marker = new google.maps.Marker({
            position: location,
            map: map
        });
        marker.setVisible(true);
        
        if (location != undefined && location != null) {
            if (location.lat != undefined && location.lat != null &&
                typeof location.lat == "number") {
                latitude = location.lat;
                longitude = location.lng;
            } else if (location.lat() != undefined && location.lat() != null &&
                    typeof location.lat() == "number" ) {
                latitude = location.lat();
                longitude = location.lng();
            }
        }
        
        setAllMap(null);
        markers.push(marker);
        setLocationMerchant(latitude, longitude);
    }

    function setLocationMerchant(latitude, longitude) {
        document.getElementById('latitudeTemp').value = latitude;
        document.getElementById('longitudeTemp').value = longitude;
        document.getElementById('latitude').value = latitude;
        document.getElementById('longitude').value = longitude;
    }

    function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
        markers = [];
    }

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
    function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function () {
            autocomplete.setTypes(types);
        });
    }

    setupClickListener('changetype-all', []);
    //setupClickListener('changetype-address', ['address']);
    //setupClickListener('changetype-establishment', ['establishment']);
    //setupClickListener('changetype-geocode', ['geocode']);

    function putMarker() {
        var longitudeCurrent = document.getElementById('longitudeCurrent').value;
        var latitudeCurrent = document.getElementById('latitudeCurrent').value;
        if (latitudeCurrent != "-1" && longitudeCurrent != "-1") {
            var merchantLatLong = new google.maps.LatLng(latitudeCurrent, longitudeCurrent);
            var marker1 = new google.maps.Marker({
                position: merchantLatLong,
                map: map
            });
            marker.setVisible(false);
            setAllMap(null);
            markers.push(marker1);
            map.panTo(merchantLatLong);
        }
    }
    if (typeAction === 1) {
        putMarker();
    }
}

function saveMerchant() {
    var merchantId = document.getElementById('merchantId').value;
    var latitude = document.getElementById('latitudeTemp').value;
    var longitude = document.getElementById('longitudeTemp').value;
    var url = "/support/admin/customers/merchants/updateMerchant.jsp";
    if (latitude !== "-1" && longitude !== "-1") {
        $.ajax({
            type: "POST",
            async: false,
            data: {merchantId: merchantId, latitude: latitude, longitude: longitude},
            url: url,
            success: function (data) {
                //var dataTrim = data;
                $('#map').hide();
                $('#mainTableSupport').show();
                $('#divPrintBanner').show();
            }
        });
    } else {
        var warnningLabel = document.getElementById('warnningLabel');
        alert(warnningLabel.value);
    }
}

function goMapAdd() {
    $('#map').show();
    initMap(true);
    $('#mainTableSupport').hide();
    $('#divPrintBanner').hide();
}