function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {      // If Internet Explorer, return version number
        return true;
    }
    else {                 // If another browser, return 0
        return false;
    }
}
function goAssingInventory(edit) {
    if (isInternetExplorer()) {
        window.location.href = "assignInventory.jsp" + edit;
    }
    else {
        window.location.href = "admin/tools/smsInventory/assignInventory.jsp" + edit;
    }
}

function goAssingInventoryShowList() {
    if (isInternetExplorer()) {
        window.location.href = "assignInventoryShowList.jsp";
    }
    else {
        window.location.href = "admin/tools/smsInventory/assignInventoryShowList.jsp";
    }
}

function goToTools() {
    if (isInternetExplorer()) {
        window.location.href = "../tools.jsp";
    }
    else {
        window.location.href = "admin/tools/tools.jsp";
    }
}

function assignBatch(id) {
    if (isInternetExplorer()) {
        window.location.href = "assignInventoryToMerchant.jsp?batchId=" + id;
    }
    else {
        window.location.href = "admin/tools/smsInventory/assignInventoryToMerchant.jsp?batchId=" + id;
    }
}

function editBatch(id, code) {
    if (isInternetExplorer()) {
        window.location.href = "simInventoryEditForm.jsp?batchId=" + id + "&batchCode=" + code;
    }
    else {
        window.location.href = "admin/tools/smsInventory/simInventoryEditForm.jsp?batchId=" + id + "&batchCode=" + code;
    }
}



function closePopupCarriers() {
    $("#popUpDivCarriers").hide();
}
function closePopupTypes() {
    $("#popUpDivTypes").hide();
}

function closePopupProducts() {
    $("#popUpDivProducts").hide();
}

function checkAllCarriers(element, message) {
    if (element.checked === true) {
        if (confirm(message)) {
            for (var i = 0; i < count; i++) {
                $("#simsCarrier_" + i).val($("#simProviderGenericList option:selected").text());
                $("#simsCarrierHidden_" + i).val($("#simProviderGenericList").val());
                
                $("#simsProduct_" + i).val('');
                $("#simsProductHidden_" + i).val('');
            }
        }
    }
    element.checked = false;
}

function checkAllTypes(element, message) {
    if (element.checked === true) {
        if (confirm(message)) {
            for (var i = 0; i < count; i++) {
                $("#simsType_" + i).val($("#simTypeGenericList option:selected").text());
                $("#simsTypeHidden_" + i).val($("#simTypeGenericList").val());
            }
        }
    }
    element.checked = false;
}

function checkAllProducts(element, message) {
    if (element.checked === true) {
        if (confirm(message)) {
            for (var i = 0; i < count; i++) {
                
                $("#simsCarrier_" + i).val($("#simProviderGenericList option:selected").text());
                $("#simsCarrierHidden_" + i).val($("#simProviderGenericList").val());
                
                
                var productId = $("#simProductGenericList").val();
                if(productId === 0){
                    productId = '';
                }
                
                $("#simsProduct_" + i).val($("#simProductGenericList option:selected").text());
                $("#simsProductHidden_" + i).val(productId);
                $("#carrierForProductHidden_" + i).val($("#simProviderGenericList").val());
                
            }
        }
    }
    element.checked = false;
}


function onchangeRadioOld() {
    $("#batchSelected").val("");
    $("#batchSelected").prop("disabled", false);
}
function onchangeRadioNew() {
    $("#batchSelected").val("");
    $("#batchSelected").css("border", "");
    $("#batchSelected").prop("disabled", true);
}

function viewDataFormat() {
    var text = "<img src=\"images/simInventoryUpload.png\" >";
    $("#pDialogMessage").html(text);

    var opt = {
        autoOpen: false,
        modal: true,
        width: 1050,
        height: 400,
        title: 'Data Format CSV File',
        html: text
    };

    $(document).ready(function () {
        $("#dialog-message").dialog(opt).dialog("open");
    });
}

function downloadCSV(elem) {
    if (isInternetExplorer()) {
        elem.setAttribute("href", 'documents/SimInventoryFormat.csv');
    }
    else {
        elem.setAttribute("href", 'documents/SimInventoryFormat.csv');
    }
    elem.click();
}

function changeSiteIdSelectByMerchantId(merchantId, merchanDBA) {
    
    $('#comboDefaultSiteId').html('');
    $.ajax({
        type: "POST",
        async: false,
        data: {action: 'getSiteIdsByMerchantId', merchantId: merchantId},
        url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
        success: function (data) {

            var existData = false;
            var arrayRepsList = String($.trim(data)).split("\n");
            
            $('#comboDefaultSiteId').append('<option value="0" selected="selected"></option>');
            for (var i = 0; i < arrayRepsList.length; i++) {
                if (arrayRepsList[i] !== '') {
                    existData = true;
                    var currentRep = String($.trim(arrayRepsList[i])).split("|");
                    $('#comboDefaultSiteId').append('<option value="' + currentRep[0] + '">' + currentRep[1] + '</option>');
                }
            }
            if(existData === true){
                var opt = {
                        autoOpen: false,
                        modal: true,
                        width: 550,
                        height: 200,
                        title: 'Select Default Site Id for (' +merchanDBA+ ')'
                    };
                    $(document).ready(function () {
                        $("#dialog-message").dialog(opt).dialog("open");
                    });
            }
        }
    });
}