var errorMsg = '';
var depInfo = '';
var saveButText = '';
var cancelButText = '';
var maxBalance = 0;
var rbDialogTitle = '';
var current_enabled;
var merchantId = '';
var terminaslsIsDisableMsg = '';
var jsmsg1 = '';
var jsmsg2 = '';
var jsmsg3 = '';
var jsmsg4 = '';
var jsmsg5 = '';
var jsmsg6 = '';
var jsmsg7 = '';
var jsmsg8 = '';
var jsmsg9 = '';
var jsmsg10 = '';
var toenable = '';
var todisable = '';


function checkRunningLiability(){
	if (document.reset.runningLiability.value == '0.00'){
		alert(jsmsg1);
		return false;
	}else{
		if(confirm(jsmsg2)){
			DisableButtons();
			return true;
		}else{
			return false;
		}
	}
}

function checkPayment(){
	var formApplyPmt = document.payment.paymentAmount.value;
	if (formApplyPmt == 0.00){
		alert(jsmsg3);
		document.payment.paymentAmount.focus();
		return false;
	}else if (isNaN(formApplyPmt)){
		alert(jsmsg4);
		document.payment.paymentAmount.focus();
		return false;
	}else{
		DisableButtons();
		return true;
	}
}

function CheckPaymentMx(){
	var formApplyPmt = document.payment.paymentAmount.value;
	if (formApplyPmt == 0.00){
		alert(jsmsg3);
		document.payment.paymentAmount.focus();
		return false;
	}else if (isNaN(formApplyPmt)){
		alert(jsmsg4);
		document.payment.paymentAmount.focus();
		return false;
	}else{
		DisableButtons();
		return true;
	}
}


function checkCreditLimit(){
	var formCreditLimit = document.creditLimit.creditLimit.value;
	if (isNaN(formCreditLimit)){
		alert(jsmsg4);
		document.creditLimit.creditLimit.focus();
		return false;
	}else{
		DisableButtons();
		return true;
	}
}

function checkCommissionType(){
	document.payment.commission.value = "0.00";
	document.payment.netPaymentAmount.value = document.payment.paymentAmount.value;
}

function checkMerchantType(){
	if ( document.getElementById("lstMerchantType").value != document.getElementById("lblCurrentMerchantType").value ){
		if ( confirm(jsmsg5)){
			var nValue = document.getElementById("lstMerchantType").value;
			if ( nValue == 1 ){
				document.getElementById("lblChangeDescription").value = jsmsg6;
			}else if ( nValue == 2 ){
				document.getElementById("lblChangeDescription").value = jsmsg7;
			}else if ( nValue == 3 ){
				document.getElementById("lblChangeDescription").value = jsmsg8;
			}
			DisableButtons();
			return true;
		}else{
			document.getElementById("lstMerchantType").value = document.getElementById("lblCurrentMerchantType").value;
			return false;
		}
	}else{
		return false;
	}
}//End of function checkMerchantType

function alertUser(){
	if (!document.payment.unlimitedCredit.checked){
		if(confirm(jsmsg9)){
			document.payment.noCreditCheck.value = 0;
			document.payment.paymentDescription.value += jsmsg10;
			document.payment.submit();
		}else{
			document.payment.unlimitedCredit.checked = true;
		}
	}
	return true;
}

function calculate(){
	if (isNaN(document.payment.commission.value)== false && isNaN(document.payment.paymentAmount.value)== false){
		var intGrossPayment = 0;
		var intNetPayment   = 0;
		var intCommissionAmount   = 0;
		var intCommissionPercent   = 0;

		intGrossPayment = document.payment.paymentAmount.value;
		intCommissionPercent   = document.payment.commission.value;
		intCommissionAmount = intGrossPayment * (intCommissionPercent * .01);
		intNetPayment = intGrossPayment - intCommissionAmount;
		document.payment.netPaymentAmount.value = formatAmount(intNetPayment);
	}
}

function calculateMx(){
	if (isNaN(document.payment.commission.value)== false && isNaN(document.payment.paymentAmount.value)== false){
		var intGrossPayment = 0;
		var intNetPayment   = 0;
		var intCommissionAmount   = 0;
		var intCommissionPercent   = 0;
		intGrossPayment = document.payment.paymentAmount.value;
		intCommissionPercent   = document.payment.commission.value;
		if (document.payment.commissionChecked[0].checked){
			intCommissionAmount = intGrossPayment * (intCommissionPercent * .01);
		}else if (document.payment.commissionChecked[1].checked){
			intCommissionAmount = intCommissionPercent;
		}
		intNetPayment = intGrossPayment - intCommissionAmount;
		document.payment.netPaymentAmount.value = formatAmount(intNetPayment);
	}
}

function formatAmount(n){
	var s = "" + Math.round(n * 100) / 100;
	var i = s.indexOf('.');
	if (i < 0) return s + ".00";
	var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3);
	if (i + 2 == s.length) t += "0";
	return t;
}

function FilterTotalPayment(cText){
	var sText = cText.value;
	var sResult = "";
	var bHasDot = false;
	for ( i = 0; i < sText.length; i++ ){
		if ( (sText.charAt(i) == '-') && (i == 0) ){
			sResult += sText.charAt(i);
		}else if ( (sText.charAt(i) == '.') && (i != 0) && !bHasDot ){
			sResult += sText.charAt(i);
			bHasDot = true;
		}else if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) ){
			sResult += sText.charAt(i);
		}
	}
	if ( sResult.length == 0 ){
		sResult = formatAmount(0);
	}
	if ( sText != sResult ){
		cText.value = sResult;
	}
}//End of function FilterTotalPayment

function FilterCommission(cText){
	var sText = cText.value;
	var sResult = "";
	var bHasDot = false;
	for ( i = 0; i < sText.length; i++ ){
		if ( (sText.charAt(i) == '.') && (i != 0) && !bHasDot ){
			sResult += sText.charAt(i);
			bHasDot = true;
		}else if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) ){
			sResult += sText.charAt(i);
		}
	}
	if ( sResult.length == 0 ){
		sResult = formatAmount(0);
	}
	if ( (sResult > 100) && document.payment.commissionChecked[0].checked ){
		sResult = formatAmount(100);
	}
	if ( sText != sResult ){
		cText.value = sResult;
	}
}//End of function FilterCommission


function ValidatePaymentValueMX(cText, bAllowMinus){
	var sText = cText.value;
	var sResult = "";
	var bHasPeriod = false;
	var bHasMinus = false;
	for ( i = 0; i < sText.length; i++ ){
		if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) ){
			sResult += sText.charAt(i);
		}
		if ( !bHasPeriod && sText.charCodeAt(i) == 46 )/*period*/{
			sResult += sText.charAt(i);
			bHasPeriod = true;
		}
		if ( bAllowMinus && !bHasMinus && (sText.charCodeAt(i) == 45) && (i == 0) )/* - */{
			sResult += sText.charAt(i);
			bHasMinus = true;
		}
	}
	if ( sResult.length == 0 ){
		sResult = "0";
	}
	if ( sText != sResult ){
		cText.value = sResult;
	}
}//End of function ValidatePaymentValueMX
	
function DisableButtons(){
	var ctl = document.getElementById("btnEditMerchant");
	if ( ctl != null ){
		ctl.disabled = true;
	}
	ctl = document.getElementById("btnChangeType");
	if ( ctl != null ){
		ctl.disabled = true;
	}
	ctl = document.getElementById("btnResetLimit");
	if ( ctl != null ){
		ctl.disabled = true;
	}
	ctl = document.getElementById("btnUpdateCreditLimit");
	if ( ctl != null ){
		ctl.disabled = true;
	}
	ctl = document.getElementById("btnUpdateLimit");
	if ( ctl != null ){
		ctl.disabled = true;
	}
	ctl = document.getElementById("btnApplyPayment");
	if ( ctl != null ){
		ctl.disabled = true;
	}
	ctl = document.getElementById("btnAddTerminal");
	if ( ctl != null ){
		ctl.disabled = true;
	}
	ctl = document.getElementById("btnViewTerminal");
	if ( ctl != null ){
		ctl.disabled = true;
	}
}//End of function DisableButtons

function reserveBalance(merchant_id, val, isReturn){
	$.getJSON('/support/includes/ajax.jsp?class=com.debisys.pincache.PcReservedBalance&method=merchantReserveAjax&value='+merchant_id+'_'+val+'_'+depInfo+(isReturn ? '_true' : '_false')+'&rnd='+Math.random(),
		function(data){
			var cl = 'info';
			if(data.status == 'error'){
				cl = 'error';
			}else{
				cl = 'success';
				$('#customMessage').dialog('option', 'buttons', {
					Ok:function(){ location.reload();}
				});
			}
			$('#customMessage').html('<div class="'+cl+'">'+data.msgs.join('<br />')+'</div>');
			$('#customMessage').dialog('open');
		}
	).error(function(){
		$('#customMessage').html('<div class="error">'+errorMsg+'</div>');
		$('#customMessage').dialog('open');
	});
}
function enableMerchantPincache(merchant_id, isEnable){
	$('#customMessage')
	.html('<div class="warning">'+(isEnable ? toenable : todisable)+'</div>')
	.dialog('option','height', 300)
	.dialog('option','width', 300)
	.dialog('option','buttons', [
		{
			text: saveButText,
			click: function(){
				$('#customMessage')
					.dialog('close')
					.dialog('option', 'buttons', {OK: function() {$(this).dialog('close');}});
				$.getJSON("/support/includes/ajax.jsp?class=com.debisys.pincache.Pincache&method=enableMerchantAjax&value="+merchant_id+ (isEnable ? "_true" : "_false") + '_' + depInfo + '&rnd=' + Math.random(),
				function(data){
					var cl = 'info';
					if(data.status == 'error'){
						cl = 'error';
						$('#enablePINCache[value='+(isEnable?'false':'true')+']').attr('checked',true);
					}else{
						cl = 'success';
						if(isEnable){
							$('#pincacheInfo').show();
						}else{
							$('#pincacheInfo').hide();
						}
					}
					$('#customMessage')
						.html('<div class="'+cl+'">'+data.msgs.join('<br />')+'</div>')
						.dialog('option','height', 300)
						.dialog('option','width', 300)
						.dialog('option', 'buttons', {OK: function(){location.reload();}})
						.dialog('open');
						}
					).error(function(){
						$('#customMessage').html('<div class="error">'+errorMsg+'</div>');
						$('#customMessage').dialog('option', 'buttons', {OK: function(){location.reload();}}).dialog('open');
					});
				}
		},{
			text: cancelButText,
			click: function() {
				$(this).dialog('close');
				$('#enablePINCache[value='+(isEnable?'false':'true')+']').attr('checked',true);
			}
		}
		]
	)
	.dialog('open');
}
$.extend({URLEncode:function(c){var o='';var x=0;c=c.toString();var r=/(^[a-zA-Z0-9_.]*)/;while(x<c.length){var m=r.exec(c.substr(x));if(m!=null && m.length>1 && m[1]!=''){o+=m[1];x+=m[1].length;}else{if(c[x]==' ')o+='+';else{var d=c.charCodeAt(x);var h=d.toString(16);o+='%'+(h.length<2?'0':'')+h.toUpperCase();}x++;}}return o;},URLDecode:function(s){var o=s;var binVal,t;var r=/(%[^%]{2})/;while((m=r.exec(o))!=null && m.length>1 && m[1]!=''){b=parseInt(m[1].substr(1),16);t=String.fromCharCode(b);o=o.replace(m[1],t);}return o;}});
function saveReason(ok){
	if(current_enabled){
		var cindex = current_enabled.replace("enabled", "");
		if(!ok){
			$("#" + current_enabled).attr("checked", !$("#"+current_enabled).is(':checked'));
			$("#clerk" + cindex ).fadeIn('fast');
			$("#reason" + cindex).val("");
		}else{
			var reason = $.trim($("#reason").val());
			if(reason){
				if($("#"+current_enabled).is(':checked')){
					$("#"+current_enabled).parent().parent().removeClass("silvered");
					$("#clerk" + cindex ).fadeIn('fast');
				}else{
					$("#"+current_enabled).parent().parent().addClass("silvered");
					$("#clerk" + cindex ).fadeOut('fast');
					alert(terminaslsIsDisableMsg);
				}
				$("#reason" + cindex).val(reason);
				$("#reason").load("admin/customers/merchants_info.jsp?merchantId="+ merchantId +"&action=setStatus&siteid="+ cindex + "&status="+ ($("#"+current_enabled).is(':checked')?"1":"0") +"&reason=" + $.URLEncode(reason) + "&Random=" + Math.random());
			}else{
				alert("Please specify a reason");
				return;
			}
		}
		$("#reason").val("");
		$("#" + current_enabled).focus();
		current_enabled = null;
	}
	$('#reasonDialog').dialog('close');
}
$(document).ready(function(){
	$('button, input[type=button], input[type=submit]').button();
	$('#customMessage').dialog({
		autoOpen: false,
		modal: true,
		buttons: {OK: function() {$(this).dialog('close');}},
		close: function() {
			$('#customMessage').html('');
		}
	});
	$('#rbReserveDialog').dialog({
		title: rbDialogTitle,
		autoOpen: false,
		height: 250,
		width: 300,
		modal: true,
		buttons: [
			{
				text: saveButText,
				click: function(){
					reserveBalance(merchantId, $('#valueToReserve').val(), false);
					$(this).dialog('close');
				}
			},{
				text: cancelButText,
				click: function() {$( this ).dialog('close');}
			}
		],
		close: function() {
			$('#valueToReserve').val('0');
		}
	});	
	$('#rbReturnDialog').dialog({
		title: rbDialogTitle,
		autoOpen: false,
		height: 250,
		width: 300,
		modal: true,
		buttons: [
			{
				text: saveButText,
				click: function(){
					reserveBalance(merchantId, $('#valueToReturn').val(), true);
					$(this).dialog('close');
				}
			},{
				text: cancelButText,
				click: function() {$( this ).dialog('close');}
			}
		],
		close: function() {
			$('#valueToReturn').val('0');
		}
	});
	$('#rbReserve').button({incons:{primary: 'ui-icon-arrowreturn-1-w'}})
		.click(function(){
			$('#rbReserveDialog').dialog('open');
			return false;
		}
	);
	$('#rbReturn').button({incons:{primary: 'ui-icon-radio-off'}})
		.click(function(){
			$('#rbReturnDialog').dialog('open');
			return false;
		}
	);
	$('#valueToReserve, #valueToReturn')
	.spinner({ max: maxBalance, min: 1.00})
	.numeric({ decimal : '.', negative : false }, function(){
		this.value = "0";
		this.focus();
	});
	$('input[name=enablePINCache]').change(function(){
		if($(this).val() == 'true' && $(this).is(':checked')){
			enableMerchantPincache(merchantID, true);
		}else{
			enableMerchantPincache(merchantID, false);
		}
	});																					
	if($('#enablePINCache').val() == 'true' && $('#enablePINCache').is(':checked')){
		$('#pincacheInfo').show();
	}else{
		$('#pincacheInfo').hide();
	}	
	$('.pincacheManageBtn')
	.button({icons:{primary:'ui-icon-bullet'}})
	.click(function(){
		window.open('/support/admin/pincache/manage_pins.jsp?site_id='+$(this).data('siteid'), '_self');
		return false;
	});
	$('#reasonDialog').dialog({
		autoOpen: false,
		height: 300,
		width: 400,
		modal: true,
		buttons: [{
			text: saveButText,
			click: function(){saveReason(true);}
		},{
			text: cancelButText,
			click: function(){saveReason(false);}
		}]
	});
	$('.enableButton').click(function(){
		current_enabled = $(this).attr('id');
		$('#reasonDialog').dialog('open');
	});
});
