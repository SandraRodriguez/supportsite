/**
 * Created by andresdkm on 24/04/17.
 */
new Vue({
    el: '#colors-page',
    components: {
        'material-picker': VueColor.Photoshop,

    }, data: {
        baseColors: {
            hex: $("#BaseColorHidden").val(),
        },
        inverseColors: {
            hex: $("#ReverseColorHidden").val(),
        },
        styleTop: {
            width: '600px',
            height: '20px',
            backgroundColor: '',
        },
        styleSide: {
            width: '70px',
            backgroundColor: '',
            height: '267px',
            zIndex: 9,
            position: 'absolute',
        },
        styleChars: {
            width: '500px',
            height: '237px',
            zIndex: 1,
            position: 'absolute',
            top: '31px',
        }

    },
    mounted: function (){     
        debugger;
        this.styleTop.backgroundColor = $("#ReverseColorHidden").val();
        this.styleSide.backgroundColor = $("#BaseColorHidden").val();
        $("#controlColors").hide(0);
        $(".vue-color__photoshop__actions").hide(1);
        $(".vue-color__photoshop__previews").hide(1);        
    },
    methods: {
        
        selectGreen()
        {
            this.baseColors = {
                hex: '#279566',
                hsv: {
                    h: 154.36363636363635,
                    s: 0.5851,
                    v: 0.3686,
                    a: 1
                },
                rgba: {
                    r: 39,
                    g: 149,
                    b: 102,
                    a: 1
                },
                a: 1
            };
            this.inverseColors = {
                hex: '#ffffff',
                hsv: {
                    h: 0,
                    s: 0,
                    v: 0,
                    a: 1
                },
                rgba: {
                    r: 255,
                    g: 255,
                    b: 1255,
                    a: 1
                },
                a: 1
            };
            changeColorTemplate("#279566", "#ffffff");            
        },
        selectPurple()
        {
            this.baseColors = {
                hex: '#6D45BC',
                hsv: {
                    h: 260.1680672268908,
                    s: 0.4703,
                    v: 0.5039,
                    a: 1
                },
                rgba: {
                    r: 109,
                    g: 69,
                    b: 188,
                    a: 1
                },
                a: 1
            };
            this.inverseColors = {
                hex: '#ffffff',
                hsv: {
                    h: 0,
                    s: 0,
                    v: 0,
                    a: 1
                },
                rgba: {
                    r: 255,
                    g: 255,
                    b: 1255,
                    a: 1
                },
                a: 1
            };
            changeColorTemplate("#6D45BC", "#ffffff");
        },
        selectRed()
        {
            this.baseColors = {
                hex: '#D6494B',
                hsv: {
                    h: 359.1489361702128,
                    s: 0.6322000000000003,
                    v: 0.5627,
                    a: 1
                },
                rgba: {
                    r: 214,
                    g: 73,
                    b: 75,
                    a: 1
                },
                a: 1
            };
            this.inverseColors = {
                hex: '#ffffff',
                hsv: {
                    h: 0,
                    s: 0,
                    v: 0,
                    a: 1
                },
                rgba: {
                    r: 255,
                    g: 255,
                    b: 1255,
                    a: 1
                },
                a: 1
            };
            changeColorTemplate("#D6494B", "#ffffff");
        },
        selectYellow()
        {
            this.baseColors = {
                hex: '#fbc02d',
                hsv: {
                    h: 42.81553398058252,
                    s: 0.9625999999999998,
                    v: 0.5803,
                    a: 1
                },
                rgba: {
                    r: 251,
                    g: 192,
                    b: 45,
                    a: 1
                },
                a: 1
            };
            this.inverseColors = {
                hex: '#ffffff',
                hsv: {
                    h: 0,
                    s: 0,
                    v: 0,
                    a: 1
                },
                rgba: {
                    r: 255,
                    g: 255,
                    b: 1255,
                    a: 1
                },
                a: 1
            };
            changeColorTemplate("#fbc02d", "#ffffff");
        },
        onChangeBase (val) {
            this.baseColors = val;            
            changeColorTemplate(this.baseColors.hex, this.inverseColors.hex);
        },        
        onChangeInverse(val){
            this.inverseColors = val;
            changeColorTemplate(this.baseColors.hex, this.inverseColors.hex);
        },
        cancelColors(){            
            $("#controlColors").hide(2);
        },
        resetColors(){
            this.styleTop.backgroundColor = $("#ReverseColor").val();
            this.styleSide.backgroundColor = $("#BaseColor").val();            
        },
        saveColors(){
            $('input[name="BaseColor"]').val(this.baseColors.hex);            
            $('input[name="ReverseColor"]').val(this.inverseColors.hex);             
        }
    },
    watch: {
        baseColors: function (newValue, oldValue) {
            this.styleSide.backgroundColor = newValue.hex;
            this.styleChars.backgroundColor = 'rgba(' + newValue.rgba.r + ',' + newValue.rgba.g + ',' + newValue.rgba.b + ',' + 0.6 + ')';
        }
        ,
        inverseColors: function (newValue, oldValue) {
            this.styleTop.backgroundColor = newValue.hex;
        }
    }
})