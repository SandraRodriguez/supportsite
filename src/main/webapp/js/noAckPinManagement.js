new Vue({
    el: '#mvack-transactions',
    data: {
        ackPins: [],

        search: '',
        selectedPin: '',
        searchCategory: '',
        errMessage: '',
        startDt: '',
        endDt: '',
        endObject: '',

        isValidDt: false,
        isSearch: false,

        lang: {},
        ackpin: {},
        currentAckPin: {}
    },
    mounted: function () {
        this.getLang();
    },
    methods: {
        buildDateComponent() {
            let self = this;
            $('input[name="startDt"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "autoApply": true,
                "locale": {
                    "format": "MM/DD/YYYY",
                    "separator": " - ",
                    "applyLabel": self.lang.reportsapply,
                    "cancelLabel": self.lang.reportscancel,
                    "fromLabel": self.lang.reportsfrom,
                    "toLabel": self.lang.reportsto,
                    "customRangeLabel": self.lang.reportscustom,
                    "daysOfWeek": [
                        self.lang.reportsdaySu,
                        self.lang.reportsdayMo,
                        self.lang.reportsdayTu,
                        self.lang.reportsdayWe,
                        self.lang.reportsdayTh,
                        self.lang.reportsdayFr,
                        self.lang.reportsdaySa
                    ],
                    "monthNames": [
                        self.lang.reportsmonthNameJan,
                        self.lang.reportsmonthNameFeb,
                        self.lang.reportsmonthNameMar,
                        self.lang.reportsmonthNameApr,
                        self.lang.reportsmonthNameMay,
                        self.lang.reportsmonthNameJun,
                        self.lang.reportsmonthNameJul,
                        self.lang.reportsmonthNameAug,
                        self.lang.reportsmonthNameSep,
                        self.lang.reportsmonthNameOct,
                        self.lang.reportsmonthNameNov,
                        self.lang.reportsmonthNameDec
                    ],
                    "firstDay": 1
                },
                endDate: moment(),
                maxDate: moment()
            }, function (start, end, label) {
                $('.endDt').trigger("click");
                self.startDt = start.format('YYYY-MM-DD');
            });
            $('input[name="endDt"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "autoApply": true,
                "locale": {
                    "format": "MM/DD/YYYY",
                    "separator": " - ",
                    "applyLabel": self.lang.reportsapply,
                    "cancelLabel": self.lang.reportscancel,
                    "fromLabel": self.lang.reportsfrom,
                    "toLabel": self.lang.reportsto,
                    "customRangeLabel": self.lang.reportscustom,
                    "daysOfWeek": [
                        self.lang.reportsdaySu,
                        self.lang.reportsdayMo,
                        self.lang.reportsdayTu,
                        self.lang.reportsdayWe,
                        self.lang.reportsdayTh,
                        self.lang.reportsdayFr,
                        self.lang.reportsdaySa
                    ],
                    "monthNames": [
                        self.lang.reportsmonthNameJan,
                        self.lang.reportsmonthNameFeb,
                        self.lang.reportsmonthNameMar,
                        self.lang.reportsmonthNameApr,
                        self.lang.reportsmonthNameMay,
                        self.lang.reportsmonthNameJun,
                        self.lang.reportsmonthNameJul,
                        self.lang.reportsmonthNameAug,
                        self.lang.reportsmonthNameSep,
                        self.lang.reportsmonthNameOct,
                        self.lang.reportsmonthNameNov,
                        self.lang.reportsmonthNameDec
                    ],
                    "firstDay": 1
                },
                endDate: moment(),
                maxDate: moment()
            }, function (start, end, label) {
                self.endDt = end.format('YYYY-MM-DD');
                self.endObject = end;
            });
        },
        getLang() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/noAckPinManagement/langs',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.lang = JSON.parse(data);
                self.buildDateComponent();
            });
        },
        chargePinAmount(transactionId) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'POST',
                data: {
                    transactionId: transactionId
                },
                url: '/support/tools/noAckPinManagement/chargePinAmount',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.showSweet(true);
                if (self.startDt !== null || self.startDt !== '' || self.endDt !== null || self.endDt !== '') {
                    self.getAckPins();
                }
            });
        },
        returnPin(pin) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'POST',
                data: {
                    pin: pin
                },
                url: '/support/tools/noAckPinManagement/returnPin',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.showSweet(true);
                if (self.startDt !== null || self.startDt !== '' || self.endDt !== null || self.endDt !== '') {
                    self.getAckPins();
                }
            });
        },
        autitPinView(controlNumber, pin) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'POST',
                data: {
                    controlNumber: controlNumber
                },
                url: '/support/tools/noAckPinManagement/autitPinView',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                self.selectedPin = pin;
                $('#viewpin').modal('toggle');
            });
        },
        getAckPins() {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                data: {
                    startDt: self.startDt,
                    endDt: self.endDt
                },
                url: '/support/tools/noAckPinManagement/getAckPins',
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                if (data.length > 0) {
                    self.ackPins = data;
                    self.isSearch = true;
                    self.ackpinsComputed = data;
                } else {
                    self.ackPins = [];
                    self.ackpinsComputed = [];
                }
                self.buildAckTable();
            });
        },
        getPinIsolatedHistory(id) {
            let self = this;
            $(".ajax-loading").show();
            var result = $.ajax({
                type: 'GET',
                url: '/support/tools/noAckPinManagement/getPinIsolatedHistory/' + id,
                error: function (jqXHR, exception) {
                    $(".ajax-loading").hide();
                }
            }).done(function (msg) {
                $(".ajax-loading").hide();
            });
            result.success(function (data, status, headers, config) {
                $('#cm-rmodal').modal('toggle');
                self.buildHistoryTable(data);
            });
        },
        denegar() {
            alert('denegar');
        },
        buildAckTable() {
            let self = this;
            $('#tblackpins').dataTable().fnDestroy();
            $('#tblackpins').DataTable({
                aaData: self.ackpinsComputed,
                bLengthChange: false,
                bDestroy: true,
                bSortable: false,
                info: true,
                paging: true,
                searching: false,
                aoColumns: [
                    {mRender: function (data, type, row) {
                            return row.date.substring(0, 22);
                        }, sDefaultContent: "--", sClass: "alignCenter"
                    },
                    {"mData": 'recId', sDefaultContent: "--", sClass: "alignCenter"},
                    {"mData": 'merchantId', sDefaultContent: "--", sClass: "alignCenter"},
                    {"mData": 'millenniumNo', sDefaultContent: "--", sClass: "alignCenter"},
                    {"mData": 'providerName', sDefaultContent: "--", sClass: "alignCenter"},
                    {"mData": 'productDescription', sDefaultContent: "--", sClass: "alignCenter"},
                    {mRender: function (data, type, row) {
                            return '<p style="cursor: pointer" class="glyphicon glyphicon-eye-open" aria-hidden="true"></p>';
                        }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "60px"
                    },
                    {mRender: function (data, type, row) {
                            if (row.status === 'PROCESSED') {
                                return '<span style="cursor: pointer" class="glyphicon glyphicon-play" aria-hidden="true"></span>';
                            } else {
                                return '--';
                            }
                        }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "60px"
                    },
                    {mRender: function (data, type, row) {
                            if (row.status === 'PROCESSED') {
                                return '<span style="cursor: pointer" class="glyphicon glyphicon-play" aria-hidden="true"></span>';
                            }
                        }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "60px"
                    },
                    {mRender: function (data, type, row) {
                            return '<span style="cursor: pointer" class="glyphicon glyphicon-list" aria-hidden="true"></span>';
                        }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "60px"
                    }
                ],
                "oLanguage": {
                    "sSearch": this.lang.jspadmingenericLabelsearch,
                    "sZeroRecords": this.lang.jspadmingenericLabelsearchempty,
                    "sInfo": this.lang.jspadmingenericLabeltableshowing +
                            " _START_ - _END_ " +
                            this.lang.jspadmingenericLabeltableof +
                            " _TOTAL_ " + this.lang.jspadmingenericLabeltableentries,
                    "sInfoFiltered": "",
                    "oPaginate": {
                        "sNext": this.lang.jspadmingenericLabelnext,
                        "sPrevious": this.lang.jspadmingenericLabelback
                    }
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).on('click', 'td', function () {
                        var column = $(this).index();
                        if (column === 6) {
                            self.viewPin(aData.controlNumber, aData.pin);
                        }
                        if (column === 7) {
                            if (aData.status === 'PROCESSED') {
                                self.chargePinAmount(aData.recId);
                            }
                        }
                        if (column === 8) {                            
                            self.returnPin(aData.pin);
                        }
                        if (column === 9) {
                            self.getPinIsolatedHistory(aData.id);
                        }
                    });
                }
            });
        },
        buildHistoryTable(content) {
            $('#tblhistory').dataTable().fnDestroy();
            $('#tblhistory').DataTable({
                aaData: content,
                bLengthChange: false,
                bDestroy: true,
                bSortable: false,
                info: true,
                paging: true,
                searching: false,
                aoColumns: [
                    {"mData": 'comment', sDefaultContent: "--", sClass: "alignLeft"},
                    {mRender: function (data, type, row) {
                            return row.date.substring(0, 22);
                        }, sDefaultContent: "--", sClass: "alignCenter"
                    }
                ],
                "oLanguage": {
                    "sSearch": this.lang.jspadmingenericLabelsearch,
                    "sZeroRecords": this.lang.jspadmingenericLabelsearchempty,
                    "sInfo": this.lang.jspadmingenericLabeltableshowing +
                            " _START_ - _END_ " +
                            this.lang.jspadmingenericLabeltableof +
                            " _TOTAL_ " + this.lang.jspadmingenericLabeltableentries,
                    "sInfoFiltered": "",
                    "oPaginate": {
                        "sNext": this.lang.jspadmingenericLabelnext,
                        "sPrevious": this.lang.jspadmingenericLabelback
                    }
                }
            });
        },
        showSweet(success) {
            if (success) {
                $.sweetModal({
                    icon: $.sweetModal.ICON_SUCCESS,
                    theme: $.sweetModal.THEME_MIXED,
                    type: $.sweetModal.TYPE_MODAL,
                    showCloseButton: false,
                    timeout: 1000,
                    width: '30%'
                });
            } else {
                $.sweetModal({
                    icon: $.sweetModal.ICON_ERROR,
                    theme: $.sweetModal.THEME_MIXED,
                    type: $.sweetModal.TYPE_MODAL,
                    showCloseButton: false,
                    timeout: 1000,
                    width: '30%'
                });
            }
        },
        viewPin(vcontrolNumber, vpin) {
            this.autitPinView(vcontrolNumber, vpin);
        },
        validateDt() {
            if (this.startDt !== null || this.startDt !== '' || this.endDt !== null || this.endDt !== '') {
                var duration = moment.duration(this.endObject.diff(this.startDt));
                var diffDays = parseInt(duration.asDays());
                if (diffDays > 182) {
                    this.isValidDt = false;
                    this.isSearch = false;
                    this.errMessage = this.lang.ackmanagementmainwarn1;
                } else {
                    this.isValidDt = true;
                }
            }
        }
    },
    computed: {
        ackpinsComputed: function () {
            let self = this;
            return this.ackPins.filter((element) => (element.productDescription.toLowerCase().indexOf(self.search) !== -1
                        || element.productDescription.toUpperCase().indexOf(self.search) !== -1
                        || element.providerName.toLowerCase().indexOf(self.search) !== -1
                        || element.providerName.toUpperCase().indexOf(self.search) !== -1
                        || element.merchantId.toString().toUpperCase().indexOf(self.search) !== -1
                        || element.millenniumNo.toString().toUpperCase().indexOf(self.search) !== -1
                        || element.pin.toString().toUpperCase().indexOf(self.search) !== -1
                        || element.recId.toString().toUpperCase().indexOf(self.search) !== -1
                        ));
        }
    },
    watch: {
        search: function () {
            this.buildAckTable();
        },
        startDt: function () {
            this.validateDt();
        },
        endDt: function () {
            this.validateDt();
        }
    }
});