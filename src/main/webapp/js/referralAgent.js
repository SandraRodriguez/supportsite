

function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {      // If Internet Explorer, return version number
        return true;
    }
    else {                 // If another browser, return 0
        return false;
    }
}

function editReferralAgent(id, description) {
    showWindowsCharge();
    if (isInternetExplorer()) {
        window.location.href = "referralAgentConf.jsp?action=edit&id=" + id + "";
    }
    else {
        window.location.href = "admin/rateplans/referralAgents/referralAgentConf.jsp?action=edit&id=" + id + "";
    }
}

function showWindowsCharge() {
    //SE AGREGO EN modal.js  linea 23 t.controls.style.display="none" PARA NOVER EL ICONO DE CERRADO
    document.getElementById('div_show_charge').style.display = "block";
}

function hideWindowsCharge() {
    $('#div_show_charge').css("display", "none");
}



function newAssociation() {

    if (isInternetExplorer()) {
        window.location.href = "referralAgentConf.jsp?action=newAssociation";
    }
    else {
        window.location.href = "admin/rateplans/referralAgents/referralAgentConf.jsp?action=newAssociation";
    }
}

function goBack(msg) {
    if (isInternetExplorer()) {
        window.location.href = "referralAgentAssociationList.jsp?msg=" + msg;
    }
    else {
        window.location.href = "admin/rateplans/referralAgents/referralAgentAssociationList.jsp?msg=" + msg;
        ;
    }
}

function removeSelectItems() {
    $('#referralAgentIsos option:selected').remove();
}

$(document).ready(function () {

    $("#selectAllIsos").searchable();
    $("#selectAllProducts").searchable();

    var wrapper = $('.productsTableSelected'); //Input field wrapper
    $(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
        e.preventDefault();
        $(this).closest("tr").remove();
    });

    setNumericKey();
    $('.toolTip').hover(
            function () {
                this.tip = this.title;
                $(this).append('<div class="toolTipWrapper"><div class="toolTipTop"></div><div class="toolTipMid">' + this.tip + '</div><div class="toolTipBtm"></div></div>');
                this.title = "";
                this.width = $(this).width();
                $(this).find('.toolTipWrapper').css({left: this.width - 22});
                $('.toolTipWrapper').fadeIn(300);
            },
            function () {
                $('.toolTipWrapper').fadeOut(100);
                $(this).children().remove();
                this.title = this.tip;
            }
    );
});


function setNumericKey(){
    
    $("input:text.numeric").keydown(function (event) { // fields with class numeric
        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 110 || event.keyCode == 190) {

        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 110 || event.keyCode == 190)) {
            event.preventDefault();
        }
        //if a decimal has been added, disable the "."-button
    });
    
}


