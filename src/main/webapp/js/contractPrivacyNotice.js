var v_id = '';
var browserAgent = '';

$(document).ready(function () {
    doInit();
    doEvents();
});

function doEvents() {
    $('.changestatus').change(function () {
        if ($(this).is(":checked")) {
            $('.spstatus').removeClass('label label-danger').addClass('label label-success');
            $(".spstatus").text(getStateEnableTxt());
        } else {
            $('.spstatus').removeClass('label label-success').addClass('label label-danger');
            $(".spstatus").text(getStateDisableTxt());
        }
    });
    $("#editcap").click(function (ev) {
        ev.preventDefault();
        onEdit();
    });

    $('.lstemplate').addClass('active');

    $(".lshelp").click(function (ev) {
        ev.preventDefault();
        $('.lstemplate').removeClass('active');
        $('.lshelp').addClass('active');
        $('.divtemplates').hide(1);
        $('.divhelp').show(1);
    });
    $(".lstemplate").click(function (ev) {
        ev.preventDefault();
        $('.lshelp').removeClass('active');
        $('.lstemplate').addClass('active');
        $('.divtemplates').show(1);
        $('.divhelp').hide(1);
    });

    $(".add-confg").click(function (ev) {
        ev.preventDefault();
        doAddEvnt();
        $('.onPreview').hide(1);
        $(".btAddConf").prop("disabled", true);
        $('.onEdit').hide(1);
    });

    $(".previewbtn").click(function (ev) {
        ev.preventDefault();
        doPreview();
    });

    $(".cancelpreviewbtn").click(function (ev) {
        ev.preventDefault();

        $('.previewbtn').show(1);
        $('.cancelpreviewbtn').hide(1);

        $('.onEdit').show(1);
        $(".onPreview").html('');
    });

    $('.configName').keyup(function () {
        if ($('.description').val().length !== 0 & $('.configName').val().length !== 0) {
            $(".btAddConf").prop("disabled", false);
        } else {
            $(".btAddConf").prop("disabled", true);
        }
    });

    $('.description').keyup(function () {
        if ($('.description').val().length !== 0 & $('.configName').val().length !== 0) {
            $(".btAddConf").prop("disabled", false);
        } else {
            $(".btAddConf").prop("disabled", true);
        }
    });

    $('.objtemplate').on('change', function (ev) {
        var value = $(this).val();
        ev.preventDefault();
        innerContent('<spam>--' + value + '--</spam>');
        $('.objtemplate option').eq(0).prop('selected', true);
    });
}

function doAddEvnt() {
    $("#onAddNewConfig").show(1);
    $(".onEdit").hide(1);
    $(".lstAllTemplate").hide(1);

    $('.configName').focus();
}

function doInit() {
    onSearchFileds();
    getBrowserAgent();

    onSearchAllTemplates();

    $("#onAddNewConfig").hide(1);
    $(".onEdit").hide(1);
    $(".cancelpreviewbtn").hide(1);
    $(".divhelp").hide(1);
    $(".cnfgName").prop("disabled", true);
    $(".lstAllTemplate").show(1);
}

function getBrowserAgent() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0) { //IExplorer        
        browserAgent = 1;
    } else {
        browserAgent = 0;
    }
}

function onAddNewConfg() {
    $(".ajax-loading").show(1);
    var result = $.ajax({
        type: 'POST',
        url: '/support/tools/contractsPrivacyNotice/add',
        async: true,
        data: {
            configName: $('.configName').val(),
            description: $('.description').val()
        },
        complete: function () {
            $('.ajax-loading').hide(1);
        }
    });
    result.success(function (data) {
        if (data.length !== 0) {
            $(".configName").val('');
            $("textarea#description").val('');
            doInit();
        }
    });
}

function onSearchAllTemplates() {
    $(".ajax-loading").show(1);
    var result = $.ajax({
        type: 'GET',
        url: '/support/tools/contractsPrivacyNotice/findAllTemplates',
        async: true,
        complete: function () {
            $('.ajax-loading').hide(1);
        }
    });
    result.success(function (data) {
        if (data.length === 0) {
            $(".lstAllTemplate").hide(1);
        } else {
            $('.lstemplate span').html(data.length);
            $('.tblTemplates').DataTable({
                "aaData": data,
                "bPaginate": false,
                "bLengthChange": false,
                "bDestroy": true,
                "paging": false,
                aoColumns: [
                    {mRender: function (data, type, row) {
                            var href = '/support/tools/contractsPrivacyNotice';
                            if (browserAgent !== 0) {
                                href = "<a href='#' id='tmpL" + row.id + "'><span class='label label-default' style='cursor:pointer'>" + getEditTxt() + "</span>  -  " + row.configName + "</a>";
                                return href;
                            } else {
                                return '<span class="label label-default" style="cursor:pointer">' + getEditTxt() + '</span>    -    ' + row.configName;
                            }
                        }, sDefaultContent: "--"
                    },
                    {mRender: function (data, type, row) {
                            return row.createDt;
                        }, sDefaultContent: "--", sWidth: "110px"
                    },
                    {"mData": 'modifyDt', sDefaultContent: "--", sClass: "alignCenter", sWidth: "160px"},
                    {mRender: function (data, type, row) {
                            var href = '/support/tools/doDownloadHistoryBalance?fileName=';
                            if (row.status === 'enable') {
                                href = '<span class="label label-success">' + getStateEnableTxt() + '</span>';
                            } else {
                                href = '<span class="label label-danger">' + getStateDisableTxt() + '</span>';
                            }
                            return href;
                        }, sDefaultContent: "--", sWidth: "60px"
                    },
                    {"mData": 'description', sDefaultContent: "--", sClass: "alignCenter"}
                ],
                "aaSorting": [[1, "desc"]],
                "oLanguage": getVLang(),
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if (browserAgent === 0) {
                        $(nRow).on('click', function () {
                            v_id = aData.id;
                            onEdit(aData);
                            findTemplateById(aData.id);
                        });
                    }
                }
            });
        }
    });
}

function onUpdateTemplate() {
    $(".ajax-loading").show(1);
    var result = $.ajax({
        type: 'POST',
        url: '/support/tools/contractsPrivacyNotice/update',
        async: true,
        data: {
            id: v_id,
            configName: $('.configNameUpdate').val(),
            tempContent: getVTinyContent(),
            status: (($('.changestatus').is(":checked")) ? 'enable' : 'disable'),
            description: $('textarea#descriptionUpdate').val()
        },
        complete: function () {
            $('.ajax-loading').hide(1);

        }
    }
    );
    result.success(function (data) {
        if (data.length !== 0) {
            doInit();
        }
    });
}

function findTemplateById(id) {
    $(".ajax-loading").show(1);
    var result = $.ajax({
        type: 'GET',
        data: {
            id: id
        },
        url: '/support/tools/contractsPrivacyNotice/findTemplateById',
        async: true,
        complete: function () {
            $('.ajax-loading').hide(1);
        }
    });
    result.success(function (data) {
        if (data.length === 0) {
        } else {
            if (data.status === 'enable') {
                $('.spstatus').removeClass('label label-danger').addClass('label label-success');
            } else {
                $('.spstatus').removeClass('label label-success').addClass('label label-danger');
            }
            if (data.tempContent.length !== 0) {
                setVContent(data.tempContent);
            } else {
                setVContent('');
            }
        }
    });
}

function onSearchFileds() {
    $(".ajax-loading").show(1);
    var result = $.ajax({
        type: 'GET',
        url: '/support/tools/contractsPrivacyNotice/findConfigTemplate',
        async: true,
        complete: function () {
            $('.ajax-loading').hide(1);
        }
    });
    result.success(function (data) {
        if (data.length === 0) {
        } else {
            $("#objtemplate option").remove();
            $('#objtemplate').append($('<option>', {
                value: '--',
                text: '--'
            }));
            $.each(data, function (i, item) {
                $('#objtemplate').append($('<option>', {
                    value: item.field,
                    text: item.field
                }));
            });
        }
    });
}

function onEdit(content) {
    $('.objtemplate').prop('disabled', false);
    $('.cnfgName').prop('disabled', false);

    $('.onEdit').show(1);
    $(".lstAllTemplate").hide(1);

    $(".configNameUpdate").val(content.configName);
    $("textarea#descriptionUpdate").val(content.description);
    if (content.status === 'disable') {
        $(".spstatus").text(getStateDisableTxt());
        $('.spstatus').removeClass('label label-info').addClass('label label-danger');
        $('.changestatus').addClass('checked').prop('checked', false);
    } else {
        $(".spstatus").text(getStateEnableTxt());
        $('.changestatus').addClass('checked').prop('checked', true);
        $('.spstatus').removeClass('label label-info').addClass('label label-success');
    }
}
    