function Promolog() {

    var Instance = this;
    var numPerPage = 10;
    var numRows = 0;
    var numPagination = 0;
    
    Instance.init = function () {
        $(document).ready(function () {

            $('table#promotionsTableSummary').each(function () {
                var $table = $(this);
                numRows = $table.find('tbody tr').length;
                numPages = Math.ceil(numRows / numPerPage);
                numPagination = (numPages < 5) ? numPages : 5;
            });

            $('#promotionsTableSummary tbody').paginathing({
                perPage: 10,
                limitPagination: numPagination,
                insertAfter: '.table'
            });
        });
    };

    Instance.init();
};

var promoLog = new Promolog();