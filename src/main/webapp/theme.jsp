<%--
  Author : Franky Villadiego
  Date: 2015-07-16
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>


<link href="js/themes/bootstrap.min.css" type="text/css" rel="stylesheet">
<script src="js/themes/vue.min.js" type="text/javascript" charset="utf-8"></script>
<script  src="js/themes/vue-resource.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/themes/vue-color.min.js" type="text/javascript" charset="utf-8"></script>
<%
String labelSave = request.getParameter("labelSave");
String labelReset = request.getParameter("labelReset");
String labelCancel = request.getParameter("labelCancel");
String lblSeleColor = request.getParameter("lblSeleColor");
String themes = request.getParameter("themes");
String themesGreen = request.getParameter("themesGreen");
String themesPurple = request.getParameter("themesPurple");
String themesRed = request.getParameter("themesRed");
String themesYellow = request.getParameter("themesYellow");

String lblBaseColor = request.getParameter("lblBaseColor");
String lblReverseColor = request.getParameter("lblReverseColor");                        
String BaseColor = request.getParameter("BaseColor");                        
String ReverseColor = request.getParameter("ReverseColor");                        
%>
<input type="hidden" id="BaseColorHidden" value="<%=BaseColor%>" />
<input type="hidden" id="ReverseColorHidden" value="<%=ReverseColor%>" />
<div class="page animsition" >
    <div class="page-content container-fluid" id="colors-page">
        <div class="row">
            <div class="col-lg-12">
                <button class="btn btn-success" @click="saveColors()"><%=labelSave%></button>
                <button type="button" class="btn btn-success" @click="cancelColors()"><%=labelCancel%></button>
                <button type="button" class="btn btn-success" @click="resetColors()"><%=labelReset%></button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="row" style="color:black;">
                    <div class="col-lg-1 " style="height: 350px;">
                        <div class="top" v-bind:style="styleTop">
                        </div>
                        <div class="side" v-bind:style="styleSide">
                        </div>
                        <div class="chars" v-bind:style="styleChars"></div>
                        <img src="images/themes/preview.png" style="width: 600px;z-index: 110;position: absolute;top: -1px;"/>
                    </div>
                    <div class="col-lg-12">
                        <h5><%=themes%></h5>
                        <div class="row" style="color:black;">
                            <div class="col-lg-4" @click="selectGreen()">
                                <img src="images/themes/previewGreen.png" class="img-responsive"/>
                                <h5><%=themesGreen%></h5>
                            </div>
                            <div class="col-lg-4" @click="selectPurple()">
                                <img src="images/themes/previewPurple.png" class="img-responsive"/>
                                <h5><%=themesPurple%></h5>
                            </div>
                            <div class="col-lg-4" @click="selectRed()">
                                <img src="images/themes/previewRed.png" class="img-responsive"/>
                                <h5><%=themesRed%></h5>
                            </div>
                            <div class="col-lg-4" @click="selectYellow()">
                                <img src="images/themes/previewYellow.png" class="img-responsive"/>
                                <h5><%=themesYellow%></h5>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-6">
                <div class="row" style="color:black;" >
                    <div class="col-lg-12"  >
                        <h3><%=lblBaseColor%></h3>                        
                        <material-picker v-model="baseColors" @change-color="onChangeBase"  
                                         head="<%=lblSeleColor%>" style="width: 50%;"></material-picker>
                    </div>

                    <div class="col-lg-12">
                        <h3><%=lblReverseColor%></h3>
                        <material-picker v-model="inverseColors" @change-color="onChangeInverse"
                                         head="<%=lblSeleColor%>" style="width: 50%;"></material-picker>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<script  src="js/themes/colors.js" type="text/javascript" charset="utf-8"></script>