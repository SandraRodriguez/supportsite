<%--
  Author : Franky Villadiego
  Date: 2015-06-25
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<table border="0" cellpadding="0" cellspacing="0" width="750" style="margin: 5px">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;${Languages.getString('jsp.message.title')}</td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" class="formArea2">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td style="padding: 30px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;">
                        <font color=ff0000>
                            ${messageToPrint}
                        </font>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<c:import url="/WEB-INF/pages/footer.jsp" />