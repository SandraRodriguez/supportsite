<%-- 
    Document   : paymentOptions
    Created on : 24/01/2017, 01:38:35 PM
    Author     : janez
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.debisys.languages.Languages"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
//    int section = 4;
//    int section_page = 17;
    int section_page = 35;
    String path = request.getContextPath();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script src="js/paymentProcessorUtil.js" type="text/javascript"></script>
<html>
    <script>
        var vTam;
        window.onload = function () {
            $('.modifica').hide(0);
            var fType = "${paymentProcessor.feeType}";
            if (fType === 'Fixed') {
                document.getElementById("feeType").selectedIndex = "0";
            } else if (fType === 'Percentage') {
                document.getElementById("feeType").selectedIndex = "1";
            }

            var vStatus = "${paymentProcessor.status}";
            if (vStatus === 'ACTIVE') {
                document.getElementById("status").checked = true;
            } else if (vStatus === 'INACTIVE') {
                document.getElementById("status").checked = false;
            }

            var vdefa = "${paymentProcessor.isDefault}";
            if (vdefa === 'true') {
                document.getElementById("isDefault").checked = true;
            } else if (vdefa === 'false') {
                document.getElementById("isDefault").checked = false;
            }

            vTam = "${parametersList.size()}";
            if (vTam === '0') {
                $('.contantsTabl').hide(0);
            }

            var v_amountsAllowed = "${paymentProcessor.amountsAllowed}";
            var array = v_amountsAllowed.split(',');
            $.each(array.sort(), function (key, value) {
                $('#selectamounts').append($('<option>', {
                    value: value,
                    text: value
                }));
            });
        };

        function onUpdatePaymentProcessorParameter() {
            var fIdPaymentProcessor = "${paymentProcessor.id}";

            var vid = document.getElementById("id").value;
            var vname = document.getElementById("name").value;
            var vdescription = document.getElementById("description").value;
            var vval = document.getElementById("value").value;

            var response = confirm("¿You want to update " + vname + " constan?");
            if (response === true) {
                var urlPath = window.location.protocol + "//" + window.location.host + window.location.pathname.substring(0, 35);

                var http = new XMLHttpRequest();
                var url = urlPath + "/updatePaymentProcessorParameter/";

                var params = "id=" + vid + "&name=" + vname + "&description=" + vdescription + "&value=" + vval;
                http.open("POST", url, false);

                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                http.send(params);

                window.location.href = "/support/admin/tools/paymentOption/editPaymentProcessor/" + fIdPaymentProcessor;
            }
        }

        $(document).ready(function () {
            document.getElementById("saveconf").disabled = true;
            $(".fee").change(function () {
                document.getElementById("saveconf").disabled = false;
            });

            $(".isDefault").change(function () {
                document.getElementById("saveconf").disabled = false;
            });

            $(".feeType").change(function () {
                document.getElementById("saveconf").disabled = false;
            });
            $(".status").change(function () {
                document.getElementById("saveconf").disabled = false;
            });
        });

    </script>  

    <head>       
        <script src="js/paymentProcessorUtil.js" type="text/javascript"></script>        
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>          
        <style>
            .fondosceldas {	
                background-color: #fff !important;
            }
        </style>
    </head>

    <body>        
        <div class="container">
            <div class="row text text-muted" style="background-color: #fff; margin: 1%">                                                          

                <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                    <li role="presentation" class="active "><a style="cursor: pointer" class="active"><img src="images/analysis_cube.png" border="0" width="22" height="22"> <%=Languages.getString("jsp.tools.paymentoption.permissions.title", SessionData.getLanguage()).toUpperCase()%></a></li>                                                                                                                                                
                </ul>

                <spf:form method="POST" modelAttribute="parameterListWrapper" id="editpp" name="editpp">                                                                                                                                                            
                    <br /><br />
                    <div class="col-md-2">
                        <div class="form-group">
                            <label><%=Languages.getString("jsp.tools.paymentoption.processorname.title", SessionData.getLanguage()).toUpperCase()%></label>
                            <input class="form-control" style="width: 250px" type="text" name="platform" value="${paymentProcessor.platform}" disabled="true"/>

                            <br />
                            <label><%=Languages.getString("jsp.tools.paymentoption.fee.title", SessionData.getLanguage()).toUpperCase()%></label>
                            <input style="width: 120px" type="text" class="fee form-control" name="fee" id="fee" value="${paymentProcessor.fee}"/>
                            <br />

                            <label><%=Languages.getString("jsp.tools.paymentoption.feetype.title", SessionData.getLanguage()).toUpperCase()%></label>

                            <select class="feeType form-control" name="feeType" id="feeType" style="width: 125px">
                                <option value="Fixed">Fixed</option>
                                <option value="Percentage">Percentage</option>  
                            </select>
                        </div> 

                        <br /><br />
                        <label><input type="checkbox" name="status" id="status"/> <%=Languages.getString("jsp.tools.paymentoption.processorenable.title", SessionData.getLanguage()).toUpperCase()%></label>
                        <br />
                        <label><input type="checkbox" name="isDefault" id="isDefault"/> <%=Languages.getString("jsp.tools.paymentoption.processordefault.title", SessionData.getLanguage()).toUpperCase()%></label>
                        <strong><p style="font-size: 10px">${message}</p></strong>
                        <br />
                        <input value="Cancelar" class="btn btn-default" type="button" onclick="window.location.href = '/support/admin/tools/paymentOption/'" />
                        <input type="submit" class="btn btn-success saveconf" name="Accept"  value="Save"/>
                    </div>                                                

                    <div class="col-md-2">

                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label><%=Languages.getString("jsp.tools.dtu3165.135.add.amount", SessionData.getLanguage()).toUpperCase()%></label>
                            <input class="form-control" style="width: 120px" name="txtaddAmount" id="txtaddAmount" value="0"/>   

                            <br />
                            <input type="button" class="btn btn-default" name="Accept" onclick="addAmount()"  value="Save"/>
                            <input type="button" class="btn btn-danger" name="Accept" onclick="removeAmount()"  value="Remove"/>
                            <input type="hidden" class="btn btn-danger" name="amountsAllowed" id="amountsAllowed"/>                                                                                   
                        </div>                                                        
                    </div>  


                    <div class="col-md-2">
                        <div class="form-group">
                            <label><%=Languages.getString("jsp.tools.dtu3165.135.amounts", SessionData.getLanguage()).toUpperCase()%></label>
                            <select  class="form-control"name="selectamounts" id="selectamounts" style="width: 125px">                                            
                            </select>                                                         
                        </div>
                    </div>   
                </div>

                <hr />

                <div class="row text text-muted" style="background-color: #fff">                                                          
                    <br />

                    <div class="col-md-12">
                        <div class="form-group">
                            <table class="modifica" id="modifica" style="width: 100% !important;" >                                                                                                                                  
                                <tr>
                                    <td>   
                                        <label><%=Languages.getString("jsp.tools.paymentoption.name.title", SessionData.getLanguage()).toUpperCase()%></label>
                                        <input type="hidden" name="id" id="id"/>
                                        <input class="form-control" type="text" name="name" id="name" disabled="true"/>
                                    </td>                                
                                </tr>                                             
                                <tr>
                                    <td>   
                                        <label><%=Languages.getString("jsp.tools.paymentoption.constantvalue.title", SessionData.getLanguage()).toUpperCase()%></label>
                                        <input class="form-control value" name="value" id="value" maxlength="512"/>
                                    </td>
                                    <td>                                                                                           
                                    </td>                                                
                                </tr>
                                <tr>                                
                                    <td>                                                       
                                        <label><%=Languages.getString("jsp.tools.paymentoption.constantdescription.title", SessionData.getLanguage()).toUpperCase()%></label>
                                        <input class="form-control" type="text" name="description" id="description" disabled="true"/>
                                    </td>                                
                                </tr>                                                

                                <tr>                                        
                                    <td>
                                        <br />
                                        <input class="btn btn-default" type="button" name="Cancelar"  value="Cancel" onclick="window.location.href = '/support/admin/tools/paymentOption/editPaymentProcessor/${paymentProcessor.id}'"/>                                        
                                        <input class="btn btn-success" type="button" name="Accept"  onclick="onUpdatePaymentProcessorParameter()" value="Update"/>                                                                                                                           
                                    </td>                                    
                                </tr>                                                     
                            </table>     
                        </div>
                    </div> 

                    <spf:form  method="POST"  modelAttribute="parameterListWrapper">                                                                                         
                        <table class="table table-hover table-condensed contantsTabl" id="contantsTabl" style="width: 97% !important; margin-left: 2%">
                            <thead style="background-color: #f1f1f1">
                                <tr>  
                                    <td style="display:none;"></td>
                                    <td><%=Languages.getString("jsp.tools.paymentoption.name.title", SessionData.getLanguage())%></td>
                                    <td><%=Languages.getString("jsp.tools.paymentoption.constantvalue.title", SessionData.getLanguage())%></td>                                                 
                                    <td><%=Languages.getString("jsp.tools.paymentoption.constantdescription.title", SessionData.getLanguage())%></td>                                                                                     
                                    <td></td>
                                </tr> 
                            </thead>
                            <tbody>                                                                                  
                                <c:forEach var="paymentProcessorParameter" items="${parametersList}">                                                                
                                    <tr>
                                        <td style="display:none;">${paymentProcessorParameter.id}</td>                                                            
                                        <td>${paymentProcessorParameter.name}</td>                                                            
                                        <td style="-webkit-text-security: square">${paymentProcessorParameter.value}</td>
                                        <td>${paymentProcessorParameter.description}</td>                                                                                                                                                                                                     

                                        <td align="center">                                                                                                            
                                            <p class="btnSelect" style="cursor: pointer"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></p>                                            
                                        </td>
                                    </tr>                                                              
                                </c:forEach>
                            </tbody>
                        </table>    
                    </spf:form>                                              
                </div>

            </spf:form>

        </div>       
        <%@ include file="/includes/footer.jsp" %>
    </body>
</html>