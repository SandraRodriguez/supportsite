<%-- 
    Document   : paymentOptions
    Created on : 24/01/2017, 01:38:35 PM
    Author     : janez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<%
    int section = 9;
//    int section = 4;
//    int section_page = 17;
    int section_page = 35;
    String path = request.getContextPath();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<html>
    <head>
        <script type="text/javascript" src="/support/includes/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>    
        <script src="js/paymentProcessorUtil.js" type="text/javascript"></script>

        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>  
        <script src="<%=path%>/js/themes/vue.js" type="text/javascript"></script>                                          
        <link rel="stylesheet" href="<%=path%>/css/element_ui.css"/>             
        <script src="<%=path%>/js/vue/element_ui_en.js" type="text/javascript"></script>    

        <%    String labelAddNewCreditDebitCard = Languages.getString("jsp.tools.paymentoption.addNewCreditDebitCard", SessionData.getLanguage());
        %>

        <style>
            .fondosceldas {	
                background-color: #fff !important;
            }
        </style>               
    </head>
    <body>
        <br />
        <table border="0" cellpadding="0" cellspacing="0" width="1800" >
            <tr>
            <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                <li role="presentation" class="active "><a style="cursor: pointer" class="active"><img src="images/analysis_cube.png" border="0" width="22" height="22"> <%=Languages.getString("jsp.tools.paymentoption.permissions.title", SessionData.getLanguage()).toUpperCase()%></a></li>                                                                                                                                                
            </ul>

        </tr>
        <tr>
            <td colspan="1"  bgcolor="#FFFFFF">

                <div class="">
                    <div class="row text text-muted" style="background-color: #fff; margin-left: 1%" id="payment-processor-add">                                                          

                        <%--<spf:form method="POST" commandName="addPaymentProcessor" id="addpp" name="addpp">--%>   

                        <br />
                        <br />
                        <div class="col-md-2">
                            <div class="form-group">

                                <label><%=Languages.getString("jsp.tools.paymentoption.processorname.integrated", SessionData.getLanguage()).toUpperCase()%></label>
                                <br />
                                <el-select v-model="model.processorCode" filterable  style="margin-top: 0;">
                                    <el-option v-for="item in processorList" :key="item.key" :label="item.value" :value="item.key"></el-option>
                                </el-select>

                                <br /><br />


                                <label><%=Languages.getString("jsp.tools.paymentoption.processorname.title", SessionData.getLanguage()).toUpperCase()%></label>
                                <input class="form-control" required style="width: 250px" type="text" v-model="model.platform" name="platform" id="platform"/>

                                <br />
                                <label><%=Languages.getString("jsp.tools.paymentoption.fee.title", SessionData.getLanguage()).toUpperCase()%></label>
                                <input class="form-control"  style="width: 120px" v-model="model.fee" required name="fee" id="fee"/>
                                <br />

                                <label><%=Languages.getString("jsp.tools.paymentoption.feetype.title", SessionData.getLanguage()).toUpperCase()%></label>
                                <select class="form-control" required name="feeType" id="feeType" style="width: 125px">
                                    <option value="Fixed">Fixed</option>
                                    <option value="Percentage">Percentage</option>  
                                </select> 
                            </div> 

                            <br /><br />
                            <label><input type="checkbox" name="status" id="status"/> <%=Languages.getString("jsp.tools.paymentoption.processorenable.title", SessionData.getLanguage()).toUpperCase()%></label>
                            <br />
                            <label><input type="checkbox" name="isDefault" id="isDefault"/> <%=Languages.getString("jsp.tools.paymentoption.processordefault.title", SessionData.getLanguage()).toUpperCase()%></label>
                            <strong><p style="font-size: 10px">${message}</p></strong>
                            <br />
                            <input value="<%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage()).toUpperCase()%>" class="btn btn-default" onclick="window.location.href = '/support/admin/tools/paymentOption/'" />
                            <input v-if="model.processorCode && model.platform && model.fee" @click="save" type="button" class="btn btn-success" name="Accept"  value="<%=Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage()).toUpperCase()%>"/>

                        </div>                                                



                        <div class="col-md-2">
                            <div class="form-group">
                                <label><%=Languages.getString("jsp.tools.dtu3165.135.add.amount", SessionData.getLanguage()).toUpperCase()%></label>
                                <input class="form-control" style="width: 120px" name="txtaddAmount" id="txtaddAmount" value="0"/>   
                                <br />
                                <input type="button" class="btn btn-default" name="Accept" onclick="addAmount()"  value="<%=Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage()).toUpperCase()%>"/>
                                <input type="hidden" class="btn btn-danger" name="amountsAllowed" id="amountsAllowed"/>                                                                                   
                            </div>
                        </div>  


                        <div class="col-md-2">
                            <div class="form-group">
                                <label><%=Languages.getString("jsp.tools.dtu3165.135.amounts", SessionData.getLanguage()).toUpperCase()%></label>
                                <select  class="form-control"name="selectamounts" id="selectamounts" style="width: 125px">                                            
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <el-table :data="model.params" stripe style="overflow-x: hidden; overflow-y: scroll; height: 575px !important">
                                <el-table-column type="expand">
                                    <template slot-scope="props">
                                        <h4>Propery Value</h4>
                                        <div v-if="props.row.name === 'clientId' || props.row.name === 'email'">
                                            <el-input placeholder="--" v-model="props.row.value" maxlength="512"></el-input>
                                        </div>
                                        <div v-else>
                                            <el-input type="password" placeholder="--" v-model="props.row.value" maxlength="512"></el-input>
                                        </div>
                                        <br /><br />
                                    </template>
                                </el-table-column>

                                <el-table-column prop="name" label="Name">
                                    <template slot-scope="props">                                            
                                        <div v-if="props.row.name === 'clientId' || props.row.name === 'email'">
                                            {{props.row.name}} <i class="el-icon-unlock"></i>                                                
                                        </div>
                                        <div v-else>                                                
                                            {{props.row.name}}                                                                                           
                                        </div>                                            
                                    </template>
                                </el-table-column>
                                <el-table-column prop="description" label="Description">
                                </el-table-column>
                            </el-table>                          
                        </div>

                    </div>       

                </div>        
            </td>
        </tr>	
    </table>
    <br />
    <iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
    </iframe>
    <script src="<%=path%>/js/pages/tools/paymentProcessor/paymentProcessorAdd.js" type="text/javascript"></script>        
</body>
</html>
<%@ include file="/includes/footer.jsp" %>
