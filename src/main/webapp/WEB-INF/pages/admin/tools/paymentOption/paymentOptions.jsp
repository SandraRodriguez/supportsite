<%-- 
    Document   : paymentOptions
    Created on : 24/01/2017, 01:38:35 PM
    Author     : janez
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
    int section = 9;
//    int section = 4;
//    int section_page = 17;
    int section_page = 35;

    String labelAddNewCreditDebitCard = Languages.getString("jsp.tools.paymentoption.addNewCreditDebitCard", SessionData.getLanguage());
    String path = request.getContextPath();
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>                                        

<script>
    function onDeleteValidation(platformid, name) {
        var response = confirm("�You want to remove " + name + " platform?");
        if (response === true) {
            var urlPath = window.location.protocol + "//" + window.location.host + window.location.pathname;

            var http = new XMLHttpRequest();
            var url = urlPath + "/deletePaymentProcessor/";

            var params = "platform=" + platformid;
            http.open("POST", url, false);

            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            http.send(params);
            window.location.href = "/support/admin/tools/paymentOption/";
        }
    }
    function onEditPaymentProcessor(platformid) {
        window.location.href = "/support/admin/tools/paymentOption/editPaymentProcessor/" + platformid;
    }
</script>
<style>
    .fondosceldas {	
        background-color: #fff !important;
    }
    .btn-success {
        color: #212121;
        background-color: #afd855 !important;        
    }
</style>
<html>
    <head>       
        <script src="js/paymentProcessorUtil.js" type="text/javascript"></script>        

        <script type="text/javascript" src="/support/includes/jquery.js"></script>
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                    

        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>  

        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                        
    </head>

    <body>    
        <div class="container" style="margin: 1%">
            <div class="row text text-muted" style="background-color: #fff">  
                <div class="col-md-12">                
                    <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                        <li role="presentation" class="active "><a style="cursor: pointer" class=""><img src="images/analysis_cube.png" border="0" width="22" height="22"> <%=Languages.getString("jsp.tools.paymentoption.permissions.title", SessionData.getLanguage()).toUpperCase()%></a></li>                                                                                                                                                
                    </ul>              
                    <br />
                    <center>
                        <a class="pull-right btn btn-success" style="color: #212121;" href="/support/admin/tools/paymentOption/addPaymentProcessor?message=">
                            <%=labelAddNewCreditDebitCard%>
                        </a>
                    </center>                                       
                    <br />
                    <br /><br />                                        
                    <table class="table table-hover">
                        <thead style="background-color: #afd855; font-size: 14px">
                            <tr>
                                <th><%=Languages.getString("jsp.tools.paymentoption.name.title", SessionData.getLanguage())%></th>
                                <th><%=Languages.getString("jsp.tools.paymentoption.fee.title", SessionData.getLanguage())%></th>
                                <th><%=Languages.getString("jsp.tools.paymentoption.feetype.title", SessionData.getLanguage())%></th>
                                <th ><%=Languages.getString("jsp.tools.paymentoption.state.title", SessionData.getLanguage())%></th>
                                <th style="text-align: center !important"><%=Languages.getString("jsp.tools.paymentoption.default.title", SessionData.getLanguage())%></th>
                                <!--<th><%=Languages.getString("jsp.tools.paymentoption.option.title", SessionData.getLanguage())%></th>-->
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>                                                   

                        <c:forEach var="listValue" items="${processorList}">                                                                                                                                                                                                                
                            <tr style="font-size: 14px">
                                <td style="font-weight: bold"><a href="/support/admin/tools/paymentOption/editPaymentProcessor/${listValue.id}" style="text-decoration: underline">${listValue.platform}</a></td>
                                <td>${listValue.fee}</td>
                                <td>${listValue.feeType}</td>
                                <td>${listValue.status}</td>     
                                <c:choose>
                                    <c:when test="${listValue.isDefault == true}">
                                        <td><center><input type="radio" name="gender" value="male" checked></center></td>
                                    </c:when>    
                                    <c:otherwise>
                                    <td><center><input disabled="" type="radio" name="gender" value="male"></center></td>
                                    </c:otherwise>
                                </c:choose>                                                                                                                                                            
                            <td align="center">
                                <p style="cursor: pointer" onclick="onEditPaymentProcessor('${listValue.id}')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></p>                                            
                            </td>
                            <td align="center">
                                <p style="cursor: pointer" onclick="onDeleteValidation('${listValue.id}', '${listValue.platform}')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></p>
                            </td>
                            </tr>                                                                                                                                             
                        </c:forEach>                                                                                                                                                                                           
                    </table>  
                </div>
            </div>
        </div>
        <hr />
    </body>
    <%@ include file="/includes/footer.jsp" %>
</html>