<%-- 
    Document   : newBulkPayment
    Created on : 21-feb-2017, 17:48:24
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 15;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">        
        <script type="text/javascript" src="/support/includes/jquery.js"></script>
        <script language="JavaScript" src="/support/includes/primeui/primeui-2.0-min.js" type="text/javascript" charset="utf-8"></script>
    </head>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif" >
            <tr>                
                <td class="formAreaTitle" align="left" width="3000"><%=Languages.getString("jsp.tools.bulkpayment.permissions.run.new.title", SessionData.getLanguage()).toUpperCase()%></td>                
            </tr>
            <tr>
                <td colspan="3"  bgcolor="#FFFFFF">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">                
                        <tr>
                            <td>
                                <form method="POST" action="tools/bulkPayment/newBulkPayment" enctype="multipart/form-data">
                                    <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" >                                                                
                                        <tr>                                     
                                            <td class="formArea2">                                                                                                                                                                                            
                                                <table width="600" style="margin-left: 3%; margin-bottom: 3%; margin-top: 3%">  
                                                    <tr>
                                                        <td>   
                                                            <label><%=Languages.getString("jsp.tools.bulkpayment.upload.title", SessionData.getLanguage()).toUpperCase()%></label>
                                                        </td>
                                                        <td>                                                                                                  
                                                            <input type="file" name="xls_filename"><br>
                                                        </td>   
                                                    </tr>
                                                    <tr style="height: 25px">                                                                                                        
                                                    </tr>
                                                    <td>                                                                                                                           
                                                        <a href="/support/tools/bulkPayment?message="><< <%=Languages.getString("jsp.admin.genericLabel.back", SessionData.getLanguage()).toUpperCase()%></a>
                                                    </td>                                                                                                         
                                                    <td>                                                                    
                                                        <input type="submit" name="Accept"  value="<%=Languages.getString("jsp.tools.bulkpayment.upload.button", SessionData.getLanguage()).toUpperCase()%>"/>                                                        
                                                    </td>                                                                                                        
                                                </table>                                        
                                            </td>                                           
                                        </tr>	
                                    </table> 
                                </form>
                            </td>
                        </tr>	
                    </table>
                </td>
            </tr>
        </table>
    </body>
    <iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
    </iframe>
    <%@ include file="/includes/footer.jsp" %>
</html>
