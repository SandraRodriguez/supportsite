<%-- 
    Document   : bulkPayment
    Created on : 21-feb-2017, 13:59:39
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 15;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%    String labelAddRunNewProcess = Languages.getString("jsp.tools.bulkpayment.permissions.run.new.title", SessionData.getLanguage());
%>

<%
    String path = request.getContextPath();
%>

<style type="text/css">
    @import "<%=path%>/css/jquery.dataTables.min.css";
    @import "<%=path%>/css/loading.css";    
    .alignCenter { text-align: center; }    
</style>

<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>

<script>

    function isErr(str) {
        if (str.substring(0, 5) === 'ERROR') {
            $("#servermessage").css("color", "red");
        } else {
            $("#servermessage").css("color", "green");
        }
    }
    $(document).ready(function () {
        var str = document.getElementById("servermessage").innerText;
        isErr(str);

        $("#asearch").prop("disabled", true);
        findHistorySuggest();
        $("#tblagent").hide();
        $("#toBack").hide();
        $("#divgralInfo").hide();

        $("#asearch").click(function () {
            $("#tblResult").show();
            findHistory();
        });

        $("#doDownload").click(function (event) {
            event.preventDefault();
            var vl = document.getElementById('doDownload').value;
            alert(vl);
        });

        $(function () {
            $("#fromDate").datepicker({
                showOn: "button",
                buttonImage: "admin/calendar/calbtn.gif",
                buttonImageOnly: true,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#toDate").datepicker("option", "minDate", selectedDate);
                    $(".ui-datepicker-trigger").css("vertical-align", "top");
                }
            });
            var today = new Date();
            $("#toDate").datepicker({
                showOn: "button",
                buttonImage: "admin/calendar/calbtn.gif",
                buttonImageOnly: true,
                dateFormat: "dd/mm/yy",
                minDate: $("#fromDate").val(),
                maxDate: today,
                onClose: function (selectedDate) {
                    $("#fromDate").datepicker("option", "maxDate", selectedDate);
                    $(".ui-datepicker-trigger").css("vertical-align", "top");
                    if ($("#fromDate").val() !== '') {
                        $("#asearch").prop("disabled", false);
                    }
                }
            });


            $("#filterValue2").change(function () {
                $("#filterValue").val($("#filterValue2").val());
            });

            $(".ui-datepicker-trigger").css("vertical-align", "top");
        });

        $("#evnsch").click(function (event) {
            event.preventDefault();
            onScheduleHistoryBalance();
        });
    });

    function onScheduleHistoryBalance() {
        var result = $.ajax({
            type: 'POST',
            timeout: 10000,
            data: {
                startDt: $("#fromDate").val(),
                endDt: $("#fromDate").val()
            },
            url: '/support/reports/onScheduleHistoryBalance',
            async: true
        });
        result.success(function (data, status, headers, config) {
            if (data.length !== 0) {
                $("p#tblagentmessage").text('<%=Languages.getString("jsp.report.history.balance.scheduledmessage", SessionData.getLanguage())%>');
                $("#evnsch").hide();
                window.location.href = '/support/reports/scheduleHistorialBalanceController';
            }
        });
        result.error(function (x, t, m) {
            if (t === "timeout") {
            } else {
            }
        });
    }

    function findHistory() {
        $(".ajax-loading").show();
        $("#servermessage").hide();
        var v_fromDate = $('#fromDate').val();
        var v_toDate = $('#toDate').val();
        if (v_fromDate.length === 0) {
            $('#lblNewbp').after('<div style="color: #cd0a0a" id="lblerrfrom">** from date is Required</div>');
        }
        if (v_toDate.length === 0) {
            $('#lblNewbp').after('<div style="color: #cd0a0a" id="lblerrto">** to date is Required</div>');
        }

        var result = $.ajax({
            type: 'GET',
            timeout: 10000,
            data: {
                startDt: $("#fromDate").val(),
                endDt: $("#toDate").val()
            },
            url: '/support/tools/findPaymentsByRange',
            async: true

        }).done(function (msg) {
            $(".ajax-loading").hide();
        });
        result.success(function (data, status, headers, config) {
            if (data.length === 0) {
                $('#tblagendreport').dataTable().fnClearTable();
                $("p#totalRecords").text(data.length);
                $("p#totalRecords").attr('style', "color: #cd0a0a; display:inline");
                $("p#pfrom").text($("#fromDate").val());
                $("p#pto").text($("#toDate").val());

                $("#divtblagend").hide();
                $(".ui-datepicker-trigger").css("vertical-align", "top");
                $("#divgralInfo").show();
            } else {
                $('#tblagendreport').dataTable().fnDestroy();
                $("#divtblagend").show();
                $("#divgralInfo").show();
                $('#tblagendreport').DataTable({
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bInfo": false,
                    "bDestroy": true,
                    "paging": false,
                    "iDisplayLength": 15,
                    searching: true,
                    aoColumns: [
                        {mRender: function (data, type, row) {
                                var href = '/support/tools/doDownloadHistoryBalance?fileName=';
                                if (row.status === 'COMPLETED') {
                                    href = "<a href='" + href + row.fileName + "'>" + row.fileName + "</a>";
                                } else {
                                    href = row.fileName;
                                }
                                return row.fileName;
                            }, sDefaultContent: "--"
                        },
                        {"mData": 'loadDt', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'endProcess', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'rowsProcessed', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'status', sDefaultContent: "--", sClass: "alignCenter"}
                    ],
                    "aaSorting": [[1, "desc"]],
                    "sPaginationType": "full_numbers",
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.tools.bulkpayment.searchevt")}',
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    }
                });
                $('#tblagendreport').dataTable().fnClearTable();
                $('#tblagendreport').dataTable().fnAddData(data);

                $("p#totalRecords").text(data.length);
                $("p#pfrom").text($("#fromDate").val());
                $("p#pto").text($("#toDate").val());

                $("#tblagent").hide();
                $("#toBack").show();
            }
        });
    }

    function formatedDate(today) {
        var day = today.getDate();
        var month = today.getMonth() + 1;
        var year = today.getFullYear();

        var formatted =
                (day < 10 ? '0' : '') + day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                year;
        return formatted;
    }

    function findHistorySuggest() {
        $(".ajax-loading").show();
        var today = new Date();
        var bac = new Date();
        var day = bac.getDate();
        var mont = bac.getMonth();
        var year = bac.getFullYear();

        bac.setFullYear(year, mont, (day - 15));

        $("#fromDate").val('');
        $("#toDate").val('');

        var result = $.ajax({
            type: 'GET',
            timeout: 10000,
            data: {
                startDt: '' + formatedDate(bac) + '',
                endDt: '' + formatedDate(today) + ''
            },
            url: '/support/tools/findPaymentsByRange',
            async: true
        }).done(function (msg) {
            $(".ajax-loading").hide();
        });
        result.success(function (data, status, headers, config) {
            if (data.length === 0) {
                $("p#totalRecords").text(data.length);
                $("p#pfrom").text(formatedDate(bac));
                $("p#pto").text(formatedDate(today));

                $("#evnsch").hide();

                $("#tblResult").hide();
                $("#divtblagend").hide();
            } else {
                $("p#totalRecords").text(data.length);
                $("p#pfrom").text(formatedDate(bac));
                $("p#pto").text(formatedDate(today));

                $("#tblagent").hide();
                $("#toBack").hide();
                $("#divgralInfo").show();

                $('#tblagendreport').DataTable({
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "bDestroy": true,
                    "paging": false,
                    "iDisplayLength": 15,
                    searching: true,
                    aoColumns: [
                        {"mData": 'fileName', sDefaultContent: "--"},
                        {"mData": 'loadDt', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'endProcess', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'rowsProcessed', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'status', sDefaultContent: "--", sClass: "alignCenter"}
                    ],
                    "aaSorting": [[1, "desc"]],
                    "sPaginationType": "full_numbers",
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.tools.bulkpayment.searchevt")}',
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    }
                });
                $('#tblagendreport').dataTable().fnClearTable();
                $('#tblagendreport').dataTable().fnAddData(data);
            }
        });
    }
</script>

<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">        
    </head>
    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>
    <table border="0" cellpadding="0" cellspacing="0" width="1200" background="images/top_blue.gif" >
        <tr>                
            <td class="formAreaTitle" align="left" width="3000"><%=Languages.getString("jsp.tools.bulkpayment.permissions.title", SessionData.getLanguage()).toUpperCase()%></td>                
        </tr>            
        <tr>
            <td colspan="3"  bgcolor="#FFFFFF">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">                
                    <tr>
                        <td>
                            <form name="frmMain" id="frmMain" method="post" action="/support/admin/achBilling/summary.jsp" onSubmit="scroll();">                                                        
                                <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">                                                                
                                    <tr>                                                        
                                        <td class="formArea2">                                                                                                                    
                                            <br />                                                                                                                                       
                                            <table border="0" style="margin-left: 2%; margin-right: 2%" width ="95%" class="main" bgcolor="#FFFFFF" cellpadding="20">                                                    
                                                <tr>
                                                    <td align="left" colspan="7" class="main">
                                                        <p><%=Languages.getString("jsp.tools.bulkpayment.help.p1", SessionData.getLanguage())%></p>
                                                        <p><%=Languages.getString("jsp.tools.bulkpayment.help.p2", SessionData.getLanguage())%></p>
                                                        <ol id="instructions">
                                                            <li><%=Languages.getString("jsp.tools.bulkpayment.help.p3", SessionData.getLanguage())%>
                                                                <%
                                                                    if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                                                                %>
                                                                <a href="/support/tools/bulkPayment/downloadTemplate"><%=Languages.getString("jsp.tools.bulkpayment.help.p3.link", SessionData.getLanguage())%></a>
                                                                <%
                                                                } else {
                                                                %>
                                                                <a href="/support/tools/bulkPayment/downloadTemplate"><%=Languages.getString("jsp.tools.bulkpayment.help.p3.link", SessionData.getLanguage())%></a>
                                                                <%
                                                                    }
                                                                %>
                                                            </li>
                                                            <li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst3", SessionData.getLanguage())%>
                                                                <p>
                                                                <ol id="recommends">
                                                                    <li><span><%=Languages.getString("jsp.tools.bulkpayment.help.p4.a", SessionData.getLanguage())%></span></li>
                                                                    <li><span><%=Languages.getString("jsp.tools.bulkpayment.help.p4.b", SessionData.getLanguage())%></span></li>                                                               
                                                                    <li><span><%=Languages.getString("jsp.tools.bulkpayment.help.p4.c", SessionData.getLanguage())%></span></li>                                                                                                                                            
                                                                    <li><span><%=Languages.getString("jsp.tools.bulkpayment.help.p4.d", SessionData.getLanguage())%></span></li>
                                                                    <li><span><%=Languages.getString("jsp.tools.bulkpayment.help.p4.e", SessionData.getLanguage())%></span></li>
                                                                </ol>
                                                                </p>
                                                            <li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst3", SessionData.getLanguage())%></li>
                                                            <li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst4", SessionData.getLanguage())%></li>
                                                            <li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst5", SessionData.getLanguage())%></li>
                                                            <li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst6", SessionData.getLanguage())%></li>
                                                        </ol>
                                                    </td>
                                                </tr>
                                            </table>


                                    <center>
                                        <hr />
                                        <a href="/support/tools/bulkPayment/newBulkPayment" id="lblNewbp">
                                            <%=labelAddRunNewProcess%>
                                        </a>                                                         
                                        <p id="servermessage" style="font-size: 12px"><strong>${message}</strong></p>                                                                                        
                                    </center>
                                    <br />     

                                    <table width="100%" border="0" style="margin-bottom: 1%">                                          
                                        <tr>
                                            <td width="150"  align="center">
                                                <p id="startDtTitle" style="display:inline">
                                                    <%=Languages.getString("jsp.admin.start_date", SessionData.getLanguage()).toUpperCase()%>
                                                </p>                                            
                                            </td>                                        
                                            <td>                                                                                                  
                                                <input disabled="true" id="fromDate" name="startDate" value="" type="text" size="12" style="height: 16px;" required="true">                                       
                                            </td>
                                        </tr>                                    
                                        <tr>                                    
                                            <td width="150" align="center">
                                                <div id="idenddatetitle">
                                                    <%=Languages.getString("jsp.admin.end_date", SessionData.getLanguage()).toUpperCase()%>
                                                </div>                                        
                                            </td>                                        
                                            <td>     
                                                <div id="idenddate">
                                                    <input disabled="true" id="toDate" name="endDate" value="" type="text" size="12" style="height: 16px;" required="true">                                            
                                                    <input value="<%= Languages.getString("jsp.admin.search", SessionData.getLanguage()).toUpperCase()%>" id="asearch" type="button" size="12" style="margin-left: 2%;">                                                        
                                                </div>                                                                                        
                                            </td>
                                        </tr>                                    
                                        <tr>
                                            <td></td>                                    
                                            <td>
                                                <!--<div id="tblResult" class="pui-datatable ui-widget" style="margin-top:1%">-->                                                
                                                <div>   
                                                    <div id="divgralInfo">
                                                        <br />
                                                        <strong>
                                                            <%=Languages.getString("jsp.report.totalrecords", SessionData.getLanguage())%>
                                                            <p id="totalRecords" style="display:inline"></p>
                                                            <%=Languages.getString("jsp.admin.from", SessionData.getLanguage())%>
                                                            <p id="pfrom" style="display:inline"></p>                                                    
                                                            <%=Languages.getString("jsp.report.totalrecords.to", SessionData.getLanguage())%>
                                                            <p id="pto" style="display:inline"></p>
                                                        </strong>
                                                    </div>                                                        
                                                    <div id="divtblagend">
                                                        <table id="tblagendreport" class="table display" cellspacing="0" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th><%=Languages.getString("jsp.tools.bulkpayment.namefile.title", SessionData.getLanguage()).toUpperCase()%></th>  
                                                                    <th><%=Languages.getString("jsp.tools.bulkpayment.loadDate.title", SessionData.getLanguage()).toUpperCase()%></th>
                                                                    <th><%=Languages.getString("jsp.tools.bulkpayment.endprocess.title", SessionData.getLanguage()).toUpperCase()%></th>       
                                                                    <th><%=Languages.getString("jsp.tools.bulkpayment.rowprocesed.title", SessionData.getLanguage()).toUpperCase()%></th>
                                                                    <th><%=Languages.getString("jsp.tools.bulkpayment.status.title", SessionData.getLanguage()).toUpperCase()%></th>                                                                                   
                                                                </tr>
                                                            </thead>
                                                        </table>  
                                                    </div>                                                                                         
                                                </div>    

                                                <div id="tblagent" style="margin-top:1%">
                                                    <p id="tblagentmessage" style="display:inline; color: #cd0a0a"></p>                                                
                                                    <a href="javascript:void(0)" id="evnsch">
                                                        <%=Languages.getString("jsp.report.history.balance.click", SessionData.getLanguage()).toUpperCase()%>
                                                    </a> 
                                                    <div id="toBack">
                                                        <br />
                                                        <a href="/support/reports/scheduleHistorialBalanceController" id="evnsch"><<
                                                            <%=Languages.getString("jsp.admin.genericLabel.back", SessionData.getLanguage()).toUpperCase()%>
                                                        </a>
                                                    </div>                                                
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    </td>
                                    </tr>	
                                </table>                                                
                            </form>
                        </td>
                    </tr>	
                </table>
            </td>
        </tr>
    </table>
</body>
<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>
</html>
