<%--
  Author : Franky Villadiego
  Date: 2015-07-16
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">
    .ui-widget{
        font-size: 1.2em;
    }
    .a-link{margin:2px;}
</style>

<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;" width="960">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            ${Languages.getString('jsp.admin.reports.spiffReports.spiffDetailReport')}
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                      <label class="ui-widget"><spt:message code="jsp.admin.narrow_results" />:</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form id="frmSearch" method="get" action="customers/agents">
                            <table>
                                <tr>
                                    <td>
                                        <input id="searchCriteria" name="searchCriteria" size="40" maxlength="64" type="text"/> <br/>
                                        <label class="ui-widget"><spt:message code="jsp.admin.customers.agents.search_instructions"/></label>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <input id="submitSearch" class="ui-widget" type="submit" value="${Languages.getString('jsp.admin.search')}" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <c:if test="${PERM_AGENTS}">
                    <tr>
                        <td>
                            <form method="get" action="admin/customers/agents_add.jsp">
                                <input type="submit" value="${Languages.getString('jsp.admin.customers.agents.add_agent')}">
                            </form>
                        </td>
                    </tr>
                </c:if>
                <tr>
                    <td>
                        <br/>
                        <p class="ui-widget" style="margin: 3px;">
                            <span id="totalRecords">0</span> ${Languages.getString('jsp.admin.results_found')}
                            <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        <p class="ui-widget"><spt:message code="jsp.admin.customers.agents.nav_instructions_international"/> </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="tblResult" style="margin-top:5px;"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script>

    $(function(){

        $('#tblResult').puidatatable({
            lazy:true,
            paginator: {
                rows: 25,
                pageLinks:10,
                totalRecords: 0
            },
            customData:{},
            columns: [
                {field:'businessName', headerText: '${Languages.getString("jsp.admin.customers.business_name")}', sortable:true,
                    content : function(row){
                        var perm_agents = this.options.customData.permAgents;
                        if(perm_agents) {
                            var href = '/support/customers/agents/' + row.agentId;
                            return "<a href=" + href + ">" + row.businessName + "</a>";
                        }else{
                            return row.businessName;
                        }
                    }
                },
                {field:'agentId', headerText: '${Languages.getString("jsp.admin.customers.agents.agent_id")}', sortable:true},
                {field:'city', headerText: '${Languages.getString("jsp.admin.customers.city")}', sortable:true},
                {field:'state', headerText: '${Languages.getString("jsp.admin.customers.state")}', sortable:true},
                {field:'contact', headerText: '${Languages.getString("jsp.admin.customers.contact")}', sortable:true},
                {field:'phone', headerText: '${Languages.getString("jsp.admin.customers.phone")}', sortable:true},
                {headerStyle:'width:8%',
                    bodyStyle:'text-align:center',
                    content:function(row){
                        var perm_logins = this.options.customData.permLogins;
                        var refTypeAgent = this.options.customData.refTypeAgent;
                        var link0 = '';
                        if(perm_logins){
                            link0 = "<a class='a-link' href='admin/customers/user_logins.jsp?refId=" + row.agentId + "&refType=" + refTypeAgent + "'><img src='images/icon_user.png' /></a>";
                        }
                        var link1 = "<a class='a-link' href='admin/transactions/agents_transactions.jsp?repId=" + row.agentId + "'><img src='images/icon_dollar.png' /></a>";
                        var link2 = "<a class='a-link' href='admin/customers/subagents.jsp?search=y&repId=" + row.agentId + "'><img src='images/lupa.png' /></a>";
                        return link0 + link1 + link2;
                }}
            ],
            sortField:'businessName',
            sortOrder:'1',
            datasource: function(callback, ui){
                $('#spinner').addClass("fa fa-refresh fa-spin");
                var uri = '/support/customers/agents/grid';
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder == 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var criteria = '${searchCriteria}' + "";
                var data = {
                    "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows":rows, "totalRecords":"${totalRecords}", "searchCriteria":criteria};
                $.ajax({
                    type:"GET",
                    data:data,
                    url:uri,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        this.options.paginator.totalRecords = response.totalRecords;
                        $('#totalRecords').text(response.totalRecords);
                        this.options.customData.permAgents = response.permAgents;
                        this.options.customData.permLogins = response.permLogins;
                        this.options.customData.refTypeAgent = response.refTypeAgent;
                        callback.call(this, response.agents);
                    },
                    error:function(err){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        console.log(err);
                    }
                });
            }
        });

/*        $("#frmSearch").submit(function(e){
            e.preventDefault();
            console.log('exec...');
            $('#tblResult').puidatatable("reset");
        });*/
    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />