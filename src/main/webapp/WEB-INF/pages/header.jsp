
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE HTML>
<html>

<head>
    <base href="${strProtocol}://${baseHref}/support/">
    <title>${strBrandedCompanyName}</title>

    <!-- CSS -->
    <link href="default.css" type="text/css" rel="stylesheet">
    <link href="css/themes/emida-green/jquery-ui.css" type="text/css" rel="stylesheet">
    <link href="css/primeui-2.0-min.css" type="text/css" rel="stylesheet">
    <link href="css/font-awesome/font-awesome.min.css" type="text/css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
    <c:if test="${SessionData.user.supportSiteNgUser}">
        <link href="css/ng.css" type="text/css" rel="stylesheet">
    </c:if>

    <!-- JavaScripts -->
    <script language="JavaScript" src="/support/includes/jquery/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script language="JavaScript" src="/support/includes/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
    <script language="JavaScript" src="/support/includes/primeui/primeui-3.0.2-min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="/support/includes/SSWSCheckboxes.js"></script>

    <c:if test="{section==4 and (section_page ==1 or section_page==4)}">
        <script type="text/javascript" src="/support/includes/CollapsibleGroupPanel.js"></script>
        <link rel=StyleSheet href="/support/css/CollapsibleGroupPanel.css" type="text/css"></link>
    </c:if>

    <!-- DBSY-890 SW -->
    <!-- Show the javascript for the carrier user forms if the permission is checked. -->
    <c:if test="{PERM_VIEW_CARRIER_REPORTS}">
        <c:if test="{section==14 and section_page==48}">
            <script type="text/javascript" src="/support/includes/XmlGetEntities.js"></script>
        </c:if>
        <c:if test="{section==14 and (section_page==7 or section_page==8 or section_page==12  or section_page==14  or section_page==18)}">
            <script type="text/javascript" src="/support/includes/CollapsibleGroupPanel.js"></script>
            <link rel=StyleSheet href="/support/css/CollapsibleGroupPanel.css" type="text/css"></link>
        </c:if>
        <script type="text/javascript">
            <c:import url="carrier_user_js.jsp" />
        </script>
    </c:if>

    <script type="text/javascript">
        function changeLanguage(obj){
            $("#divLanguage").load('/support/changeLanguage?lan='+obj+'&Random=' + Math.random(), ProcessChangeLanguage);
        }
        function ProcessChangeLanguage(sData, sStatus){
            window.location.reload(true);
        }
    </script>
</head>

<body bgColor="#ffffff">
<div id="divLanguage" class="main"></div>
<div id="divPrintBanner" style="display: inline">
    <table id="tbl001" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table id="tbl002" width="100%" border="0" cellpadding="0" cellspacing="0" background="images/banner.gif" bgcolor="#ffffff" class="fondogris"${headerBackground}>
                    <tr>
                        <td width="100%" height="58">
                            <table id="tbl003" valign="middle" align="left" border="0" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td colspan="2" align="left">
                                        <a href="${strProtocol}://${serverName}:${serverPort}/support">
                                            <img src="images/${imageName}" alt="${util:langString(Languages, "jsp.includes.header.home")}" border="0">
                                        </a>
                                    </td>
                                </tr>
                                <tr>

                                    <c:if test="${sessionScope != null and sessionScope.SessionData != null}">
                                        <td id="tdLanguages" style="font-size: 8pt;" class="float">&nbsp;&nbsp;
                                            ${util:langString(Languages, "jsp.admin.document.support.language")}
                                            <c:forEach items="${languages}" var="language">
                                                <input type="hidden" value="${codeLanguage == '0' ? language.englishLabel : language.spanishLabel}" id="language" name="language"/>
                                                <a href="javascript:" id="${codeLanguage == '0' ? language.englishLabel : language.spanishLabel}" name="${codeLanguage == '0' ? language.englishLabel : language.spanishLabel}"
                                                   onclick="changeLanguage('${util:encodeUrl(util:trimLower(language.englishLabel))}');">${codeLanguage == '0' ? language.englishLabel : language.spanishLabel}</a>
                                                |
                                            </c:forEach>
                                        </td>
                                    </c:if>

                                    <c:if test="${sessionScope != null and sessionScope.SessionData != null}">
                                        <td align="left" nowrap="nowrap">
                                            <div>
                                                <table id="tbl004" width="100%" height="20" border="0" cellpadding="5" cellspacing="0">
                                                    <tr>
                                                        <td style="font-size: 8pt;" class="float" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${loginLabel} | ID = ${isoId} |
                                                            <c:if test="${isIntranetUser}">
                                                                <a href="http://${intranetSite}:${intranetPortSite}/default.asp">${util:langString(Languages, "jsp.includes.header.go_intranet")}</a>
                                                            </c:if>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </c:if>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<!--end header-->

<script>
    $(document).ready(function(){
        $('.helpLink').click(function(){
            window.open('/support/admin/help/index.jsp#${section}', 'help', 'width=520,height=600,resizable=0,scrollbars=yes');
        });
    });
</script>

<table id="tbl005" width="100%" class="mtable fondosceldas">
    <tr>
        <td vAlign=top>

            <c:if test="${menu.menuOptions != null and util:getSize(menu.menuOptions) > 0}">
                <div id="mainmenu">
                    <ul>
                        <c:forEach items="${menu.menuOptions}" var="menuOp">
                            <li class="${menuOp.sclass}">
                                <a href="${menuOp.url}">${menuOp.label}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <c:if test="${menu.subMenuOptions != null and util:getSize(menu.subMenuOptions) > 0}">
                <div id="mainmenu" class="submenu">
                    <ul>
                        <li><div style="height: 32px; width: ${menu.offSet}px;"></div></li>
                        <c:forEach items="${menu.subMenuOptions}" var="menuOp">
                            <li class="${menuOp.sclass}">
                                <a href="${menuOp.url}">${menuOp.label}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

        </td>
    </tr>
</table>








<!--end footer-->