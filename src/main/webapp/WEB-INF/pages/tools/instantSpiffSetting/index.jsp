<%-- 
    Document   : instantSpiff
    Created on : Jun 13, 2019, 9:42:09 AM
    Author     : janez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%

    int section = 9;
    int section_page = 47;

%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<!DOCTYPE html>
<html>
    <head>             
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <title><%=Languages.getString("instant.spiff.main.title", SessionData.getLanguage())%></title>                                         

        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>                                                            

        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>                 
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css" />

        <script src="<%=path%>/js/themes/vue.js" type="text/javascript"></script>                                          
        <link rel="stylesheet" href="<%=path%>/css/element_ui.css"/>     
        <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         

        <script src="<%=path%>/js/vue/element_ui_en.js" type="text/javascript"></script>    
        <script src="<%=path%>/js/sweetalert.min.js" type="text/javascript"></script>                       
  
    </head>

    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>

    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px;">
        <tbody>
            <tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="1800px"><%=Languages.getString("instant.spiff.main.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>                                                                                                                                                                                                                                                                   
                                                                    <div class="container-fluid text text-muted" id="instant-spiff">                                                                                   

                                                                        <div class="row text" style="background-color: #fff !important">
                                                                            <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                                <li role="presentation" class="lstemplate active "><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("instant.spiff.main.title", SessionData.getLanguage())%></a></li>                                                                                                                                                
                                                                            </ul>  
                                                                            <br />                                                                                                                                                                                                                                                                                                            
                                                                        </div>   

                                                                        <div v-show="view === 'init'" class="row text text-muted" style="background-color: #fff !important; width: 70%;">                                                                        

                                                                            <div class="pull-right">                                                                                
                                                                                <el-button @click="showAddForm()" class="pull-right" type="default" icon="el-icon-plus" circle></el-button>
                                                                            </div>


                                                                            <template>
                                                                                <el-input
                                                                                    style="width: 40%; margin-right: 1%; margin-top: 1%"
                                                                                    placeholder="<%=Languages.getString("instant.spiff.add.iso.plch", SessionData.getLanguage())%>"
                                                                                    prefix-icon="el-icon-search"
                                                                                    v-model="searchsetting">
                                                                                </el-input>                                                                                

                                                                                <el-table                                                                                                                          
                                                                                    style="overflow-y: scroll; height: 600px !important; margin-top: 3%"
                                                                                    :row-class-name="tableRowClassName"
                                                                                    :data="computedSearchSetting"                                                                                     
                                                                                    empty-text="<%=Languages.getString("jsp.admin.genericLabel.search.empty", SessionData.getLanguage())%>">                                                                                                                                                                            
                                                                                    <el-table-column
                                                                                        width="220px"
                                                                                        align="center"
                                                                                        label="<%=Languages.getString("instant.spiff.main.tbl.col.1", SessionData.getLanguage())%>"
                                                                                        prop="enabled">                                                                            

                                                                                        <template scope="scope">
                                                                                            <center>
                                                                                                <div>                                                                                                    
                                                                                                    <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                                                                                    {{parseDt(scope.row.update_dt)}}
                                                                                                </div>
                                                                                            </center>                                                                                            
                                                                                        </template>

                                                                                    </el-table-column>  

                                                                                    <el-table-column
                                                                                        align="left"
                                                                                        label="<%=Languages.getString("instant.spiff.main.tbl.col.2", SessionData.getLanguage())%>"
                                                                                        prop="iso_id">                                                                                       
                                                                                        <template scope="scope">                                                                                            
                                                                                            <div>                                                                                                                                                                                                        
                                                                                                {{scope.row.iso_name}} ({{scope.row.iso_id}})
                                                                                            </div>                                                                                                                                                                                   
                                                                                        </template>                                                                                        
                                                                                    </el-table-column>

                                                                                    <el-table-column
                                                                                        width="110px"
                                                                                        align="center"
                                                                                        label="<%=Languages.getString("instant.spiff.main.tbl.col.4", SessionData.getLanguage())%>"
                                                                                        prop="enabled">                                                                            

                                                                                        <template scope="scope">
                                                                                            <center v-if="scope.row.status === true">
                                                                                                <div>                                                                                                    
                                                                                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                                                                                </div>
                                                                                            </center>
                                                                                            <center v-else>
                                                                                                <div>
                                                                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                                                                </div>
                                                                                            </center>
                                                                                        </template>

                                                                                    </el-table-column>                                                                 

                                                                                    <el-table-column 
                                                                                        width="220px"
                                                                                        align="center">                                                                                            
                                                                                        <template scope="scope">                                                                                                                                                                                        
                                                                                            <el-button @click="getById(scope.row.id)" type="default" circle><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></el-button>                                                                                                                                                                                                                                                                                                  
                                                                                            <el-button @click="handleRemove(scope.$index, scope.row)" type="default" circle><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></el-button>                                                                                                                                                                                                                                                                                                  
                                                                                        </template>
                                                                                    </el-table-column>
                                                                                </el-table>
                                                                            </template>

                                                                        </div>


                                                                        <div v-show="view === 'add' || view === 'update'">
                                                                            <el-card class="box-card">   

                                                                                <button @click="view= 'init'; active = 0" type="button " class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>                                                                                


                                                                                <div style="margin-top: 1%;">
                                                                                    <div>                                                                                
                                                                                        <el-form ref="form" v-bind:rules="basic_rules" :model="model">

                                                                                            <div class="row" style="background-color: #fff">

                                                                                                <div class="col-md-2 text text-muted">        
                                                                                                    <h3><%=Languages.getString("instant.spiff.main.lbl.isoentity", SessionData.getLanguage())%></h3>    
                                                                                                    <br />                                                                                                
                                                                                                    <el-select v-model="model.iso_id"    
                                                                                                               @change="switchSetting()"
                                                                                                               filterable placeholder="Select">
                                                                                                        <el-option                                                                                                                  
                                                                                                            collapse-tags="false"
                                                                                                            v-for="item in isos"
                                                                                                            :key="item.key"
                                                                                                            :label="item.value"
                                                                                                            :value="item.key">

                                                                                                            <span style="float: left">{{ item.value }}</span>
                                                                                                            <span style="float: right; color: #8492a6">{{ item.key }}</span>

                                                                                                        </el-option>
                                                                                                    </el-select>  

                                                                                                    <br />
                                                                                                    <br />
                                                                                                    <hr />                                                                                                        
                                                                                                    <h4><%=Languages.getString("instant.spiff.main.lbl.is.enabled", SessionData.getLanguage())%></h4>    
                                                                                                    <el-switch
                                                                                                        @change="switchSetting()"
                                                                                                        active-color="#afd855"
                                                                                                        v-model="model.status">
                                                                                                    </el-switch>      
                                                                                                    <br />
                                                                                                    <small><%=Languages.getString("instant.spiff.main.lbl.is.enabled.sub", SessionData.getLanguage())%></small>


                                                                                                </div>

                                                                                                <div class="col-md-4 text text-muted">      
                                                                                                    <h3><%=Languages.getString("instant.spiff.main.lbl.rtr.products", SessionData.getLanguage())%></h3>    
                                                                                                    <br />

                                                                                                    <el-input
                                                                                                        v-if="model.rtrs.length > 0"
                                                                                                        style="width: 50%; margin-right: 1%"
                                                                                                        placeholder="<%=Languages.getString("instant.spiff.general.product.plc.search", SessionData.getLanguage())%>"
                                                                                                        prefix-icon="el-icon-search"
                                                                                                        v-model="searchrtr">
                                                                                                    </el-input>
                                                                                                    <el-button @click="showModalAddProduct()" class="pull-right" type="default" icon="el-icon-plus" circle></el-button>
                                                                                                    <br /><br />

                                                                                                    <el-table     
                                                                                                        style="width: 100%; overflow-y: scroll; height: 300px !important"
                                                                                                        :row-class-name="tableRowClassName"                                                                                                        
                                                                                                        :data="computedSearchRTR"                                                                                     
                                                                                                        empty-text="<%=Languages.getString("jsp.admin.genericLabel.search.empty", SessionData.getLanguage())%>">                                                                                                                                                                                                               
                                                                                                        <el-table-column                                                                                                            
                                                                                                            align="center"
                                                                                                            label="<%=Languages.getString("instant.spiff.general.product", SessionData.getLanguage())%>">
                                                                                                            <template scope="scope">                                                                                                                
                                                                                                                <div class="pull-left">                                                                                                    
                                                                                                                    {{scope.row.product_id}} - {{scope.row.name}}                                                                                                                    
                                                                                                                </div>                                                                                                                                                                                                                     
                                                                                                            </template>
                                                                                                        </el-table-column>                                                                                                                                                                      

                                                                                                        <el-table-column 
                                                                                                            width="220"
                                                                                                            align="center">                                                                                            
                                                                                                            <template scope="scope">                                                                                                                       
                                                                                                                <el-button @click="handleEdit(scope.$index, scope.row)" type="default" circle><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></el-button>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                                                                                                                <el-button v-if="view !== 'add'" @click="updateRtrStatus(scope.row)" circle><span v-bind:class="{'text-success': scope.row.status, 'glyphicon glyphicon-play': scope.row.status, 'text-muted': !scope.row.status, 'glyphicon glyphicon-off': !scope.row.status}" class="" aria-hidden="true"></span></el-button>                                                                                                                                                                                                                                                                                                  
                                                                                                                <!--<el-button @click="removeRtrSku(scope.$index, scope.row)" type="default" circle><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></el-button>-->
                                                                                                            </template>
                                                                                                        </el-table-column>
                                                                                                    </el-table>
                                                                                                </div>

                                                                                                <div v-if="showActiv" class="col-md-3 text text-muted">      
                                                                                                    <h3><%=Languages.getString("instant.spiff.main.lbl.activation.products", SessionData.getLanguage())%></h3>    
                                                                                                    <br />
                                                                                                    <div v-if="addDynamicActivSku">    
                                                                                                        <small><%=Languages.getString("instant.spiff.main.lbl.select.to.add", SessionData.getLanguage())%></small>
                                                                                                        <br />
                                                                                                        <el-select value-key="product_id" style="width: 100% !important; margin-bottom: 5% !important" v-model="tmp.activateProducts"
                                                                                                                   @change="addDynamicActProduct()"
                                                                                                                   filterable
                                                                                                                   placeholder="--">
                                                                                                            <el-option                                                                                    
                                                                                                                v-for="item in activProducts"
                                                                                                                :key="item.product_id"
                                                                                                                :label="item.name"
                                                                                                                :value="item">

                                                                                                                <span style="float: left">{{ item.name }}</span>
                                                                                                                <span style="margin-right: 5% !important; float: right; color: #8492a6">{{ item.product_id }}</span>                                                                                        
                                                                                                            </el-option>
                                                                                                        </el-select>  
                                                                                                        <br />                                                                                                        
                                                                                                        <el-button style="" type="default" @click="addDynamicActivSku = false; getActivateSkuById(aux.id)" round><%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></el-button>                                                                                                
                                                                                                    </div>
                                                                                                    <div v-if="!addDynamicActivSku">
                                                                                                        <el-input
                                                                                                            style="width: 50%; margin-right: 1%"
                                                                                                            placeholder="<%=Languages.getString("instant.spiff.general.product.plc.search", SessionData.getLanguage())%>"
                                                                                                            prefix-icon="el-icon-search"
                                                                                                            v-model="searchactiv">
                                                                                                        </el-input>                                                                                                    
                                                                                                        <el-button v-if="view === 'update'" @click="addDynamicActivSku = true; aux.activateProducts = []; findProductsBy(20)" class="pull-right" type="default" icon="el-icon-plus" circle></el-button>
                                                                                                        <br /><br />

                                                                                                        <el-table                                                                                                             
                                                                                                            style="width: 100%; overflow-y: scroll; height: 300px !important"
                                                                                                            :data="computeActivSearch"                                                                                     
                                                                                                            empty-text="<%=Languages.getString("jsp.admin.genericLabel.search.empty", SessionData.getLanguage())%>">                                                                                                                                                                                                               
                                                                                                            <el-table-column                                                                                                            
                                                                                                                align="center"
                                                                                                                label="<%=Languages.getString("instant.spiff.general.product", SessionData.getLanguage())%>">
                                                                                                                <template scope="scope">                                                                                                                
                                                                                                                    <div class="pull-left">                                                                                                    
                                                                                                                        {{scope.row.product_id}} - {{scope.row.name}}
                                                                                                                    </div>                                                                                                                                                                                                                     
                                                                                                                </template>
                                                                                                            </el-table-column>                                                                                                                                                             

                                                                                                            <el-table-column 
                                                                                                                width="60"
                                                                                                                align="center">                                                                                            
                                                                                                                <template scope="scope">                                                                                                                                                                                                                                                                                                        
                                                                                                                    <el-button @click="removeActSku(scope.$index, scope.row)" type="default" circle><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></el-button>                                                                                                                                                                                                                                                                                                  
                                                                                                                </template>
                                                                                                            </el-table-column>
                                                                                                        </el-table>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div v-if="showSpiff" class="col-md-3 text text-muted">      
                                                                                                    <h3><%=Languages.getString("instant.spiff.main.lbl.spiff.products", SessionData.getLanguage())%></h3>    
                                                                                                    <br />
                                                                                                    <div v-if="addDynamicSpiffSku">    


                                                                                                        <el-select value-key="product_id" style="width: 100% !important;" v-model="currentSpiff" 
                                                                                                                   @change="buildSugestStartDay"
                                                                                                                   filterable
                                                                                                                   placeholder="--">
                                                                                                            <el-option                                                                                    
                                                                                                                v-for="item in spiffs"
                                                                                                                :key="item.product_id"
                                                                                                                :label="item.name"
                                                                                                                :value="item">

                                                                                                                <span style="float: left">{{ item.name }}</span>
                                                                                                                <span style="margin-right: 7% !important; float: right; color: #8492a6">{{ item.product_id }}</span>

                                                                                                            </el-option>
                                                                                                        </el-select>   

                                                                                                        <br />
                                                                                                        <br />

                                                                                                        <div v-if="currentSpiff.day_to_start_pay">
                                                                                                            <h5><%=Languages.getString("instant.spiff.main.lbl.add.spiff.start.day", SessionData.getLanguage())%></h5>
                                                                                                            <el-input-number     
                                                                                                                :disabled="disableEditStart"
                                                                                                                style="width: 100% !important;"
                                                                                                                :min="startDaySugest" 
                                                                                                                :max="365"
                                                                                                                placeholder="0"
                                                                                                                suffix-icon="el-icon-date"
                                                                                                                v-model.number="currentSpiff.day_to_start_pay">
                                                                                                            </el-input-number>        
                                                                                                        </div>


                                                                                                        <br />
                                                                                                        <br />

                                                                                                        <div v-if="currentSpiff.day_to_end_pay">
                                                                                                            <h5><%=Languages.getString("instant.spiff.main.lbl.add.spiff.end.day", SessionData.getLanguage())%></h5>
                                                                                                            <el-input-number   
                                                                                                                style="width: 100% !important;"
                                                                                                                :min="(currentSpiff.day_to_start_pay + 1)"                                                                                                                 
                                                                                                                placeholder="0"
                                                                                                                suffix-icon="el-icon-date"
                                                                                                                v-model.number="currentSpiff.day_to_end_pay">
                                                                                                            </el-input-number>
                                                                                                        </div>


                                                                                                        <br />
                                                                                                        <br />

                                                                                                        <center>                                                                                                
                                                                                                            <el-button style="width: 30% !important" @click="onAddSpiff()" round><%=Languages.getString("instant.spiff.main.lbl.add.spiff", SessionData.getLanguage())%></el-button>                
                                                                                                            <br />
                                                                                                            <br />                                                                                                
                                                                                                            <el-button style="width: 30% !important" type="default" @click="resetCurrentSpiff();  addDynamicSpiffSku = false; getSpiffSkuById(aux.id);" round><%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></el-button>                                                                                                
                                                                                                        </center>

                                                                                                    </div>
                                                                                                    <div v-if="!addDynamicSpiffSku">
                                                                                                        <el-input
                                                                                                            v-if="aux.spiffsProducts.length > 0"
                                                                                                            style="width: 50%; margin-right: 1%"
                                                                                                            placeholder="<%=Languages.getString("instant.spiff.general.product.plc.search", SessionData.getLanguage())%>"
                                                                                                            prefix-icon="el-icon-search"
                                                                                                            v-model="searchspiff">
                                                                                                        </el-input>                                                                                                                                                                                                            
                                                                                                        <el-button  v-if="view === 'update'"  @click="addDynamicSpiffSku = true; findProductsBy(18)" class="pull-right" type="default" icon="el-icon-plus" circle></el-button>
                                                                                                        <br /><br />

                                                                                                        <el-table      
                                                                                                            style="width: 100%; overflow-y: scroll; height: 300px !important"
                                                                                                            :row-class-name="tableRowClassName"                                                                                                        
                                                                                                            :data="computedSearchSpiff"                                                                                     
                                                                                                            empty-text="<%=Languages.getString("jsp.admin.genericLabel.search.empty", SessionData.getLanguage())%>">                                                                                                                                                                                                               
                                                                                                            <el-table-column
                                                                                                                align="center"
                                                                                                                label="<%=Languages.getString("instant.spiff.general.product", SessionData.getLanguage())%>">
                                                                                                                <template scope="scope">                                                                                                                
                                                                                                                    <div class="pull-left">                                                                                                    
                                                                                                                        {{scope.row.product_id}} - {{scope.row.name}} - ({{scope.row.day_to_start_pay}}...{{scope.row.day_to_end_pay}})
                                                                                                                    </div>                                                                                                                                                                                                                     
                                                                                                                </template>
                                                                                                            </el-table-column>                                                                                                                        

                                                                                                            <el-table-column 
                                                                                                                width="120"
                                                                                                                align="center">                                                                                            
                                                                                                                <template scope="scope">                                                                                                                                                                                                                                                                                                        
                                                                                                                    <!--<el-button @click="addDynamicSpiffSku = true; currentSpiff.id = scope.row.id; findProductsBy(18)" type="default" circle><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></el-button>-->                                                                                                                                                                                                                                                                                                  
                                                                                                                    <el-button @click="removeSpiff(scope.$index, scope.row)" type="default" circle><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></el-button>                                                                                                                                                                                                                                                                                                  
                                                                                                                </template>
                                                                                                            </el-table-column>
                                                                                                        </el-table>
                                                                                                    </div>                                                                                                                
                                                                                                </div>
                                                                                            </div>
                                                                                        </el-form>                                                                            

                                                                                    </div>                                                                         

                                                                                </div>


                                                                                <hr style="margin-top: 2%" />
                                                                                <el-button @click="cancelAction()" plain><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <%=Languages.getString("jsp.admin.genericLabel.back", SessionData.getLanguage())%></el-button>                                                                                    
                                                                                <el-row v-if="view === 'add' && isOKtoSave" class="pull-right" style="margin-bottom: 2%">                                                                                    
                                                                                    <el-button @click="save()" type="success" plain><span class="glyphicon glyphicon-save" aria-hidden="true"></span> <%=Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></el-button>                                                                                
                                                                                </el-row>

                                                                                <el-row v-if="requestType === 'update'" class="pull-left" style="margin-bottom: 2%">
                                                                                    <el-button @click="handleRemove" type="default" plain><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <%=Languages.getString("jsp.admin.genericLabel.Delete", SessionData.getLanguage())%></el-button>                                                                                                                                                                                                                                                                                                  
                                                                                    <br />                                                                                    
                                                                                </el-row>                                                                                                                                                                

                                                                            </el-card>
                                                                        </div>

                                                                        <el-dialog title="" :visible.sync="dialogTableVisible" width="50%">

                                                                            <el-steps ref="step" :active="active" finish-status="success">
                                                                                <el-step icon="el-icon-s-tools" title="<%=Languages.getString("instant.spiff.main.lbl.rtr.products", SessionData.getLanguage())%>"></el-step>
                                                                                <el-step icon="el-icon-s-tools" title="<%=Languages.getString("instant.spiff.main.lbl.activation.products", SessionData.getLanguage())%>"></el-step>
                                                                                <el-step icon="el-icon-s-tools" title="<%=Languages.getString("instant.spiff.main.lbl.spiff.products", SessionData.getLanguage())%>"></el-step>
                                                                            </el-steps>

                                                                            <!--RTR Products-->
                                                                            <div v-if="active === 0">
                                                                                <br />
                                                                                <h5><%=Languages.getString("instant.spiff.main.lbl.select.to.add", SessionData.getLanguage())%></h5>                                                                                       
                                                                                <el-select @change="next" style="width: 100% !important; margin-bottom: 5% !important; height: 50px !important" v-model="rtr" 
                                                                                            filterable
                                                                                            value-key="product_id" placeholder="--">
                                                                                    <el-option                                                                                           
                                                                                        collapse-tags="false"
                                                                                        v-for="item in rtrs"
                                                                                        :key="item.product_id"
                                                                                        :label="item.name"
                                                                                        :value="item">

                                                                                        <span style="float: left">{{ item.name }}</span>
                                                                                        <span style="margin-right: 5% !important; float: right; color: #8492a6">{{ item.product_id }}</span>

                                                                                    </el-option>
                                                                                </el-select>    
                                                                                <br />
                                                                                <h5><%=Languages.getString("instant.spiff.main.lbl.is.enabled", SessionData.getLanguage())%></h5>                                                                              
                                                                                <el-switch active-color="#afd855" 
                                                                                           v-model="rtr.status">
                                                                                </el-switch>
                                                                            </div>

                                                                            <!-- Activation Products -->
                                                                            <div v-if="active === 1">
                                                                                <br />
                                                                                <h5><%=Languages.getString("instant.spiff.main.lbl.select.to.add", SessionData.getLanguage())%></h5>                                                                                       
                                                                                <el-select value-key="product_id" style="width: 100% !important; margin-bottom: 5% !important" v-model="aux.activateProducts"
                                                                                           multiple
                                                                                           filterable
                                                                                           placeholder="--">
                                                                                    <el-option                                                                                    
                                                                                        v-for="item in activProducts"
                                                                                        :key="item.product_id"
                                                                                        :label="item.name"
                                                                                        :value="item">

                                                                                        <span style="float: left">{{ item.name }}</span>
                                                                                        <span style="margin-right: 5% !important; float: right; color: #8492a6">{{ item.product_id }}</span>                                                                                        
                                                                                    </el-option>
                                                                                </el-select>    
                                                                                <br />                                                                                
                                                                            </div>

                                                                            <!-- Spiffs Products -->
                                                                            <div v-if="active === 2">
                                                                                <br />                                                                                
                                                                                <div v-if="addSpiff">                                                                                    
                                                                                    <div class="row text text-muted" style="background-color: #fff">
                                                                                        <div class="col-md-4">
                                                                                            <h5><%=Languages.getString("instant.spiff.general.product.plc.search", SessionData.getLanguage())%></h5>
                                                                                            <el-select filterable value-key="product_id" style="width: 100% !important; margin-bottom: 5% !important" v-model="currentSpiff" 
                                                                                                       @change="buildSugestStartDay"                                                                                                       
                                                                                                       placeholder="--">
                                                                                                <el-option                                                                                    
                                                                                                    v-for="item in spiffs"
                                                                                                    :key="item.product_id"
                                                                                                    :label="item.name"
                                                                                                    :value="item">

                                                                                                    <span style="float: left">{{ item.name }}</span>
                                                                                                    <span style="margin-right: 7% !important; float: right; color: #8492a6">{{ item.product_id }}</span>

                                                                                                </el-option>
                                                                                            </el-select>                                                                                                                                                                                           
                                                                                        </div>

                                                                                        <div class="col-md-3">
                                                                                            <h5><%=Languages.getString("instant.spiff.main.lbl.add.spiff.start.day", SessionData.getLanguage())%></h5>
                                                                                            <el-input-number     
                                                                                                :disabled="disableEditStart"
                                                                                                :min="startDaySugest" 
                                                                                                :max="365"
                                                                                                placeholder="0"
                                                                                                suffix-icon="el-icon-date"
                                                                                                v-model.number="currentSpiff.day_to_start_pay">
                                                                                            </el-input-number>
                                                                                        </div>

                                                                                        <div class="col-md-3">
                                                                                            <h5><%=Languages.getString("instant.spiff.main.lbl.add.spiff.end.day", SessionData.getLanguage())%></h5>
                                                                                            <el-input-number     
                                                                                                :min="(currentSpiff.day_to_start_pay + 1)" 
                                                                                                :max="365"
                                                                                                placeholder="0"
                                                                                                suffix-icon="el-icon-date"
                                                                                                v-model.number="currentSpiff.day_to_end_pay">
                                                                                            </el-input-number>                                                                                     
                                                                                        </div>  

                                                                                        <div class="col-md-2">
                                                                                            <br />
                                                                                            <br />
                                                                                            <center>                                                                                                
                                                                                                <el-button style="width: 100% !important" @click="onAddSpiff()" round><%=Languages.getString("instant.spiff.main.lbl.add.spiff", SessionData.getLanguage())%></el-button>                
                                                                                                <br />
                                                                                                <br />                                                                                                
                                                                                                <el-button style="width: 100% !important" type="default" @click="resetCurrentSpiff" round><%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></el-button>                                                                                                
                                                                                            </center>
                                                                                        </div>
                                                                                    </div>


                                                                                </div>

                                                                                <div v-if="!addSpiff">
                                                                                    <div class="row text text-muted" style="background-color: #fff">                                                                                
                                                                                        <div class="col-md-10">
                                                                                            <el-table         
                                                                                                style="width: 100%; overflow-y: scroll; height: 200px !important"
                                                                                                :row-class-name="tableRowClassName"                                                                                                   
                                                                                                :data="aux.spiffsProducts"                                                                                     
                                                                                                empty-text="<%=Languages.getString("jsp.admin.genericLabel.search.empty", SessionData.getLanguage())%>">                                                                                                                                                                                                               
                                                                                                <el-table-column
                                                                                                    align="center"
                                                                                                    label="<%=Languages.getString("instant.spiff.general.product", SessionData.getLanguage())%>">
                                                                                                    <template scope="scope">       
                                                                                                        <div class="pull-left">                                                                                                    
                                                                                                            {{scope.row.product_id}} - {{scope.row.name}} - ({{scope.row.day_to_start_pay}}...{{scope.row.day_to_end_pay}})
                                                                                                        </div>       
                                                                                                    </template>
                                                                                                </el-table-column>                                                                                                                        

                                                                                                <el-table-column 
                                                                                                    width="60"
                                                                                                    align="center">                                                                                            
                                                                                                    <template scope="scope">                                                                                                                                                                                                                                                                                                        
                                                                                                        <el-button @click="removeSpiff(scope.$index, scope.row)" type="default" circle><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></el-button>                                                                                                                                                                                                                                                                                                  
                                                                                                    </template>
                                                                                                </el-table-column>
                                                                                            </el-table>  
                                                                                        </div>
                                                                                        <div class="col-md-2">
                                                                                            <center>
                                                                                                <el-button @click="clearCurrentSpiff()" type="success" icon="el-icon-plus" circle></el-button>
                                                                                            </center>
                                                                                        </div>
                                                                                    </div>                                                                                                                                                                       
                                                                                </div>
                                                                            </div>


                                                                            <span v-if="!addSpiff" slot="footer" class="dialog-footer">                                                                                                                                                                
                                                                                <el-button v-show="active > 0" @click="back" class="pull-left" round><%=Languages.getString("jsp.admin.genericLabel.back", SessionData.getLanguage())%></el-button>                                                                                
                                                                                <el-button @click="next" class="next" class="pull-right" round>{{lblnext}}                                                                                    
                                                                                </el-button>                                                                                
                                                                            </span>

                                                                        </el-dialog>

                                                                    </div>                                                                      
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody>                        
                    </table>
                </td>
            </tr>                   
        </tbody>                    
    </table>
</body>
<script src="<%=path%>/js/pages/tools/instantSpiffSetting/instantSpiffSetting.js" type="text/javascript"></script>        

<style type="text/css">

    ::-webkit-scrollbar {
        width: 8px;
    }

    ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.3);
        border-radius: 10px;
    }

    ::-webkit-scrollbar-thumb {
        border-radius: 10px;
        -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.5);
    }
    .el-table .warning-row {
        background: #ff0033;
    }

    .el-table .success-row {
        background: #f0f9eb;
    }

</style>
</html>