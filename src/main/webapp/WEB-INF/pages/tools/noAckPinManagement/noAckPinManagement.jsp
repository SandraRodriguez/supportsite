<%-- 
    Document   : moveAckTransaction
    Created on : Sep 10, 2018, 10:08:12 AM
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title><%=Languages.getString("mv.acktransactions.title", SessionData.getLanguage())%></title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>                 
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>     

        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                        

        <script src="<%=path%>/js/themes/vue.js" type="text/javascript" charset="utf-8"></script>                

        <link href="<%=path%>/css/jquery.sweet-modal.min.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="<%=path%>/js/jquery.sweet-modal.min.js"></script>       

        <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
        <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
        <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>

    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px;">
        <tbody>
            <tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("mv.acktransactions.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>                    
                                                                    <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                        <li role="presentation" class="lstemplate active "><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("mv.acktransactions.title", SessionData.getLanguage())%></a></li>                                                                                                                                                
                                                                    </ul>  
                                                                    <br />                                                                                                                                                                                                  

                                                                    <div class="container-fluid" id="mvack-transactions">  

                                                                        <div class="row" style="background-color: #fff">

                                                                            <div class="col-md-2">                                                                                        
                                                                                <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startDt", SessionData.getLanguage())%></h5>
                                                                                <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                    <span class="input-group-addon btncls" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                    <input type="text" class="form-control startDt" v-model="startDt" name="startDt" />                                                                                                                                                                                                                      
                                                                                </div>                                                                                                                                                                                               
                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startdt.lbl1", SessionData.getLanguage())%></small>                                                                                    
                                                                                <br /><br />
                                                                                <button @click="getAckPins" v-show="isValidDt" type="button" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.genericLabel.search", SessionData.getLanguage())%></button>                                                                                    

                                                                            </div>

                                                                            <div class="col-md-2">                                                                                        
                                                                                <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.endDt", SessionData.getLanguage())%></h5>
                                                                                <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                    <span class="input-group-addon btncle" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                    <input type="text" class="form-control endDt" v-model="endDt" name="endDt" />                                                                                                                                                                                                                      
                                                                                </div>                                                                                                                                                                                               
                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startdt.lbl2", SessionData.getLanguage())%></small>                                                                                                                                                                        
                                                                            </div>                                                                                

                                                                            <div class="col-lg-8">
                                                                                <br />
                                                                                <div v-show="errMessage" class="alertmaxs">                                                                        
                                                                                    <div class="alert alert-warning">                                                                                                                         
                                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                        <span class="glyphicon glyphicon-eye-open"></span> 
                                                                                        <strong>                                                                                            
                                                                                            {{errMessage}}
                                                                                        </strong>                                                                            
                                                                                    </div>                                              
                                                                                </div>                                                                                                                                                                 
                                                                            </div>

                                                                        </div>

                                                                        <div v-show="isSearch">
                                                                            <br />
                                                                            <br />
                                                                            <div class="row card" style="background-color: #fff">
                                                                                <div>
                                                                                    <div class="col-md-2" style="margin-top: 1%">                                                                                    
                                                                                        <input v-model="search" type="text" class="form-control" id="exampleInputEmail1" placeholder="<%= Languages.getString("jsp.admin.genericLabel.search", SessionData.getLanguage())%>">
                                                                                        <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("ack.management.main.help1", SessionData.getLanguage())%></small>                                                                                    
                                                                                    </div>

                                                                                    <div class="col-md-12" style="margin-top: 1%">                                                                                    
                                                                                        <table id="tblackpins" class="table table-hover text-muted" style="width: 100% !important">                                                                                                                                                                        
                                                                                            <thead style="background-color: #f1f1f1">
                                                                                                <tr>                                                                                                                                                                        
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col1.label", SessionData.getLanguage())%></th>                                                                                                
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col2.label", SessionData.getLanguage())%></th>                                                                                               
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col3.label", SessionData.getLanguage())%></th>                                                                                                                     
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col4.label", SessionData.getLanguage())%></th>                                                                                                
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col5.label", SessionData.getLanguage())%></th>                                                                                                
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col6.label", SessionData.getLanguage())%></th>                                                                                                
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col7.label", SessionData.getLanguage())%></th>                                                                                                
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col8.label", SessionData.getLanguage())%></th>                                                                                                
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col82.label", SessionData.getLanguage())%></th>                                                                                                
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.main.tbl.col9.label", SessionData.getLanguage())%></th>                                                                                                
                                                                                                </tr>
                                                                                            </thead>                                                                                                    
                                                                                        </table>     
                                                                                        <br />
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>


                                                                        <div class="modal fade" id="cm-rmodal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper text-muted">
                                                                                <div class="modal-dialog modal-lg vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("ack.management.history.tbl.title", SessionData.getLanguage())%></h4>
                                                                                        </div>                                                                                        
                                                                                        <table id="tblhistory" class="table table-hover" style="width: 100% !important">                                                                            
                                                                                            <thead style="background-color: #f1f1f1">
                                                                                                <tr>
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.history.tbl.col1.label", SessionData.getLanguage())%></th>                                                                                    
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("ack.management.history.tbl.col2.label", SessionData.getLanguage())%></th>                                                                                    
                                                                                                </tr>
                                                                                            </thead>                                                                                                    
                                                                                        </table>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div>

                                                                        <div class="modal fade" id="viewpin" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper text-muted">
                                                                                <div class="modal-dialog modal-sm vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                                                                                            
                                                                                            <h4 class="modal-title" id="myModalLabel">PIN</h4>
                                                                                        </div>                
                                                                                        <div style="padding: 5%">
                                                                                            <center>                                                                                                                                                                                   
                                                                                                <strong>{{selectedPin}}</strong>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                                                                                            </center>
                                                                                        </div>                                                                                        
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
<script src="<%=path%>/js/noAckPinManagement.js" type="text/javascript"></script>        
<style type="text/css">
    .alignCenter { text-align: center; }
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;        
        border-radius: 5px;        
    }
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
    .sbar {        
        width: 100%;
        overflow-y: auto;
        height: 500px
    }
    .modal {
    }
    .vertical-alignment-helper {
        display:table;
        height: 100%;
        width: 100%;
    }
    .vertical-align-center {                
        display: table-cell;
        vertical-align: middle;
    }
    .modal-content {                
        width:inherit;
        height:inherit;               
        margin: 0 auto;
    }

    .modal.fade .modal-dialog {
        -webkit-transform: scale(0.1);
        -moz-transform: scale(0.1);
        -ms-transform: scale(0.1);
        transform: scale(0.1);
        top: 300px;
        opacity: 0;
        -webkit-transition: all 0.1s;
        -moz-transition: all 0.1s;
        transition: all 0.1s;
    }

    .modal.fade.in .modal-dialog {
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1);
        -webkit-transform: translate3d(0, -300px, 0);
        transform: translate3d(0, -300px, 0);
        opacity: 1;
    }
</style>
<%@ include file="/includes/footer.jsp" %>
</html>
