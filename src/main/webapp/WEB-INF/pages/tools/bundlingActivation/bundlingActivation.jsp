<%-- 
    Document   : bundlingActivation
    Created on : 11-jul-2017, 15:43:38
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 16;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<html>
    <head>        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>        
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>                                

        <style type="text/css">
            hr.message-inner-separator {
                clear: both;
                margin-top: 10px;
                margin-bottom: 13px;
                border: 0;
                height: 1px;
                background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
                background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
                background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
                background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
            }            
        </style>

        <script>

            function dateToYMD(date) {
                var d = date.getDate();
                var m = date.getMonth() + 1;
                var y = date.getFullYear();
                return (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + '' + y;
            }
            var fromDate = null;
            var toDate = null;
            var status = null;
            var id = '';
            $(document).ready(function () {
                $(".logtx").hide();
                $(".btnsearch").prop('disabled', true);
                $('#tbllstbundling').DataTable();
                $(".divErrMessage").hide();
                $(".divSuccessMessage").hide();
                $(".div-edit-status").hide();
                $(".audiv").hide();
                $('.btnOkUpdate').attr('disabled', 'disabled');
                $('input[name=radioStatus]').prop('checked', false);

                fromDate = dateToYMD(new Date());
                toDate = dateToYMD(new Date());

                findLastCompletedHoldBackRequest();
                $(".tbllstbundling").hide();
                $(".btnReportDownloader").hide();
                $('input[name="daterange"]').daterangepicker({
                    "showDropdowns": true,
                    "showWeekNumbers": true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>',
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    endDate: moment(),
                    maxDate: moment()
                }, function (start, end, label) {
                    fromDate = start.format('DD/MM/YYYY');
                    toDate = end.format('DD/MM/YYYY');
                    if ((fromDate === null && toDate === null) || status == null || status == 'null') {
                        $(".btnsearch").prop('disabled', false);
                    }
                });
                $(".btnsearch").click(function () {
                    if (status == 'null' || fromDate == 'null' || toDate == 'null') {
                        $('.lblstatus').css('background-color', 'rgba(219, 126, 126, 0.2)');
                        setTimeout(function () {
                            $('.lblstatus').css('background-color', '');
                        }, 500);
                    } else {
                        findHoldBackRequestByStatus();
                    }
                });
                $(".btnReportDownloader").click(function () {
//                    window.location.href = '/support/tools/bundlingActivation/downloadReport?startDt=' + fromDate + '&endDt=' + toDate + '&statusCode=' + status;
                    var url_download = '/support/tools/bundlingActivation/downloadReport?startDt=' + fromDate + '&endDt=' + toDate + '&statusCode=' + status;

                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'GET',
                        timeout: 10000,
                        url: url_download,
                        async: true
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        var csvContent = data;
                        var pom = document.createElement('a');
                        var blob = new Blob([csvContent], {type: 'text/csv;charset=UTF-8;'});
                        var url = URL.createObjectURL(blob);
                        pom.href = url;
                        pom.setAttribute('download', 'bundlingActivation.csv');
                        document.body.appendChild(pom);
                        pom.click();
                        setTimeout(function () {
                            document.body.removeChild(pom);
                            window.URL.revokeObjectURL(url);
                        }, 100);
                    });
                });
                $(".btnPreUpdate").click(function () {
                    onPreEditHoldBackRequest();
                });
                $(".btnOkUpdate").click(function () {
                    onEditHoldBackRequest();
                });
                $(".btnCancelUpdate").click(function () {
                    $(".div-main").show();
                    $(".tbllstbundling").show();
                    $(".div-edit-status").hide();
                    $(".audiv").hide();
                });
                $("#activlink").click(function (event) {
                    event.preventDefault();
                });
                $('textarea#auditTextArea').bind('input propertychange', function () {
                    $('.btnOkUpdate').attr('disabled', 'disabled');
                    if (this.value.length > 5) {
                        $('.btnOkUpdate').removeAttr('disabled', 'disabled');
                    }
                });
                $('input[type=radio][name=radioStatus]').change(function () {
                    var myRadio = $('input[name=radioStatus]');
                    status = myRadio.filter(':checked').val();
                    if (fromDate !== "" & toDate !== "" && status !== '' && status !== 'undefined' && status !== 'null') {
                        $(".btnsearch").prop('disabled', false);
                    }
                });
            });

            function findLastCompletedHoldBackRequest() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    timeout: 10000,
                    url: '/support/tools/bundlingActivation/findLastHoldBackRequest',
                    async: true
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function findHoldBackRequestByStatus() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    timeout: 10000,
                    data: {
                        startDt: fromDate,
                        endDt: toDate,
                        statusCode: status
                    },
                    url: '/support/tools/bundlingActivation/findHoldBackRequestByStatus',
                    async: true
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function onSuccessResponse(data) {
                if (data.length === 0) {
                    $(".tbllstbundling").hide();
                    $(".btnReportDownloader").hide();
                    $('#tbllstbundling').dataTable().fnClearTable();
                    $(".divErrMessage").show();
                    $(".ui-datepicker-trigger").css("vertical-align", "top");
                } else {
                    if (fromDate !== "" & toDate !== "" && status !== '' && status !== 'undefined' && status !== 'null') {
                        $(".btnReportDownloader").show();
                    }
                    $(".tbllstbundling").show();
                    $(".divErrMessage").hide();
                    $('#tbllstbundling').dataTable().fnDestroy();
                    buildTable(data);
                }
            }

            function  buildTable(data) {
                $('#tbllstbundling').DataTable({
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
//                    "bInfo": false,
                    "bDestroy": true,
                    "paging": true,
                    "iDisplayLength": 10,
                    searching: true,
                    aoColumns: [
                        {"mData": 'creationDate', sDefaultContent: "--", sClass: "alignCenter", sWidth: "140px"},
                        {mRender: function (data, type, row) {
                                return "<span class='glyphicon glyphicon-calendar' aria-hidden='true'></span> " + row.scheduleDate;
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "120px"
                        },
                        {"mData": 'lastExecutionDate', sDefaultContent: "--", sClass: "alignCenter", sWidth: "150px"},
                        {mRender: function (data, type, row) {
                                return "<span class='glyphicon glyphicon-new-window' aria-hidden='true'></span> <a href='javascript:;'>" + row.activationTransactionId + "</a>";
                            }, sDefaultContent: "--", sClass: "alignCenter"
                        },
                        {"mData": 'holdBackTrxId', sDefaultContent: "--", sClass: "alignCenter"},
                        {mRender: function (data, type, row) {
                                var href = '/support/tools/doDownloadHistoryBalance?fileName=';
                                if (row.status === 'PENDING') {
                                    href = '<span class="label label-warning">' + '<%=Languages.getString("jsp.tools.bundling.status.col.1", SessionData.getLanguage())%>' + '</span>'
                                }
                                if (row.status === 'COMPLETED') {
                                    href = '<span class="label label-success">' + '<%=Languages.getString("jsp.tools.bundling.status.col.2", SessionData.getLanguage())%>' + '</span>'
                                }
                                if (row.status === 'UNAVAILABLE') {
                                    href = '<span class="label label-danger">' + '<%=Languages.getString("jsp.tools.bundling.status.col.3", SessionData.getLanguage())%>' + '</span>'
                                }
                                return href;
                            }, sDefaultContent: "--", sClass: "alignCenter"
                        }
                    ],
                    "aaSorting": [[1, "desc"]],
//                    "sPaginationType": "full_numbers",
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.tools.bundling.search.event")}',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    },
                    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).on('click', function () {
                            id = aData.id;
                            viewEditHoldBackRequest(aData);
                        });
                    }
                });
            }

            function showLogTransaction(data) {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    data: {
                        holdBackTrxId: data.holdBackTrxId
                    },
                    url: '/support/tools/bundlingActivation/getHistoryTransaction',
                    async: true,
                    complete: function () {
                        $('.ajax-loading').hide(1);
                    }
                });
                result.success(function (data, status, headers, config) {
                    $("textarea#txtarealog").val(data.transText);
                    $('.txtProviderResponse').text(data.provider_response);
                });
            }

            function viewEditHoldBackRequest(object) {
                $(".div-main").hide();
                $(".tbllstbundling").hide();
                $(".div-edit-status").show();
                if (object.status === 'PENDING') {
                    $(".logtx").hide();
                    $('input[id=updateComplete]').prop('checked', false);
                    $('input[id=updatePending]').prop('checked', true);
                    $('input[id=updateUnavailable]').prop('checked', false);
                }
                if (object.status === 'COMPLETED') {
                    $(".logtx").show();
                    $('input[id=updateComplete]').prop('checked', true);
                    $('input[id=updatePending]').prop('checked', false);
                    $('input[id=updateUnavailable]').prop('checked', false);                                                                               
                    showLogTransaction(object);
                }
                if (object.status === 'UNAVAILABLE') {
                    $(".logtx").hide();
                    $('input[id=updateComplete]').prop('checked', false);
                    $('input[id=updatePending]').prop('checked', false);
                    $('input[id=updateUnavailable]').prop('checked', true);
                }
            }

            function onPreEditHoldBackRequest() {
                $(".audiv").show();
                $(".div-edit-status").hide();
                $('textarea#auditTextArea').val('');
            }

            function onEditHoldBackRequest() {
                var stUpdate = $('input[name=radioStatusUpdate]');
                var vstatus = stUpdate.filter(':checked').val();
                $(".ajax-loading").show();
                $.ajax({
                    type: 'POST',
                    data: {
                        id: id,
                        statusCode: vstatus,
                        reason: $('textarea#auditTextArea').val()
                    },
                    url: '/support/tools/bundlingActivation/updateHoldBackRequestStatus',
                    async: true,
                    complete: function () {
                        $('.ajax-loading').hide(1);
                        $('.divSuccessMessage').show(1);
                        $(".audiv").hide();
                        $(".div-edit-status").hide();
                        setTimeout(function () {
                            $('.divSuccessMessage').hide(1);
                            findHoldBackRequestByStatus();
                            $(".div-main").show();
                            $(".tbllstbundling").show();
                        }, 1500);
                    }
                });
            }
        </script>
<!--        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />-->        
        <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
        <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
        <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>             
    </head>
    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>        
    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" width="1200" style="margin-top: 1%; margin-left: 1%">
        <tbody><tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.tools.bundlingActivation.title.sub1", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td bgcolor="#7B9EBD" width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table class="table-hover" cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table table-hover">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>
                                                                    <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                        <li role="presentation" class="lstemplate active"><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.tools.bundlingActivation.title.sub1", SessionData.getLanguage())%></a></li>                                                                                                                                                
                                                                    </ul>                                                                       
                                                                    <br />
                                                                    <div class="div-main">                                                                                                                                            
                                                                        <table width="50%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="lblrd">
                                                                                        <div class="form-group">                                                                                                  
                                                                                            <label class="text-muted"><%= Languages.getString("jsp.tools.bundling.date.range", SessionData.getLanguage())%></label>                                                                                        
                                                                                            <div class="input-group input-group-sm">                                                                        
                                                                                                <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                                                                                <input type="text" class="form-control" name="daterange" />                                                                    
                                                                                            </div>                                                                                                                                                                                                                                             
                                                                                        </div>
                                                                                    </td>                                                                                
                                                                                    <td class="lblstatus">
                                                                                        <div class="form-group" style="margin-left: 5%;">                                                                                            
                                                                                            <label class="text-muted"><%= Languages.getString("jsp.tools.bundling.select.status", SessionData.getLanguage())%></label><br />                                                                                        
                                                                                            <input type="radio" name="radioStatus" value="COMPLETED" /> <span class="label label-success"><%= Languages.getString("jsp.tools.bundling.status.label.1", SessionData.getLanguage())%></span>                                                                                                                                             
                                                                                            <input type="radio" name="radioStatus" value="PENDING" /> <span class="label label-warning"><%= Languages.getString("jsp.tools.bundling.status.label.2", SessionData.getLanguage())%></span>                                                                        
                                                                                            <input type="radio" name="radioStatus" value="UNAVAILABLE" /> <span class="label label-danger"><%= Languages.getString("jsp.tools.bundling.status.label.3", SessionData.getLanguage())%></span>                                                                        
                                                                                        </div>
                                                                                    </td>                                                                                    
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>

                                                                        <ul class="pager">
                                                                            <li class="previous btnsearch" id="btnsearch" style="cursor:pointer"><a><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.search", SessionData.getLanguage())%></a></li>                                                                            
                                                                            <li class="next btnReportDownloader" style="cursor:pointer"><a><span class="glyphicon glyphicon-save" aria-hidden="true"></span> <%= Languages.getString("jsp.tools.bundling.update.download", SessionData.getLanguage())%></a></li>                                                                            
                                                                        </ul>
                                                                        <hr />
                                                                    </div>

                                                                    <div class="div-edit-status">                                                                                                                                            
                                                                        <button type="button" class="btn btn-default btnPreUpdate"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> <%= Languages.getString("jsp.tools.bundling.update.status", SessionData.getLanguage())%></button>                                                                    
                                                                        <button type="button" class="btn btn-danger btnCancelUpdate"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.cancel", SessionData.getLanguage())%></button>                                                                                                                                            
                                                                        <div class="logtx">                                                                                                                                                    
                                                                            <br />
                                                                            <b><p class="txtProviderResponse" style="font: bold"></p></b>
                                                                            <textarea id="txtarealog" class="txtarealog" style="background-color: #ececec; color: #010101; border: none; resize: none; height: 400px; width: 100%; font-family: 'Courier New'" class="text" disabled="true">                                                                            
                                                                            </textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="divErrMessage">
                                                                        <div class="alert alert-danger" role="alert">
                                                                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                                            <p id="errMessage" style="display:inline"><%= Languages.getString("jsp.tools.bundling.err.message.search", SessionData.getLanguage())%></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="divSuccessMessage">                                                                        
                                                                        <div class="alert alert-success" role="alert">
                                                                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>                                                                                
                                                                            <p id="errMessage" style="display:inline"><%= Languages.getString("jsp.tools.bundling.success.message", SessionData.getLanguage())%></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="audiv">      
                                                                        <table width="50%">
                                                                            <tbody>
                                                                                <tr>                                                                                                                                                                   
                                                                                    <td>
                                                                                        <div class="form-group">
                                                                                            <label class="text-muted"><%= Languages.getString("jsp.tools.bundling.update.status", SessionData.getLanguage())%></label><br />                                                                                                                     
                                                                                            <input type="radio" id="updateComplete" class="updateComplete" name="radioStatusUpdate" value="COMPLETED" /> <span class="label label-success"><%= Languages.getString("jsp.tools.bundling.status.label.1", SessionData.getLanguage())%></span>                                                                        
                                                                                            <input type="radio" id="updatePending" class="updatePending" name="radioStatusUpdate" value="PENDING" /> <span class="label label-warning"><%= Languages.getString("jsp.tools.bundling.status.label.2", SessionData.getLanguage())%></span>                                                                        
                                                                                            <input type="radio" id="updateUnavailable" class="updateUnavailable" name="radioStatusUpdate" value="UNAVAILABLE" /> <span class="label label-danger"><%= Languages.getString("jsp.tools.bundling.status.label.3", SessionData.getLanguage())%></span>                                                                        
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>                                                                                
                                                                            </tbody>
                                                                        </table>
                                                                        <div class="alert alert-warning">
                                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                            <span class="glyphicon glyphicon-eye-open"></span> <strong><%= Languages.getString("jsp.tools.bundling.audit.panel.title", SessionData.getLanguage())%></strong>
                                                                            <hr class="message-inner-separator">
                                                                            <p>                                                                                
                                                                                <%= Languages.getString("jsp.tools.bundling.audit.body.warning", SessionData.getLanguage())%>
                                                                            </p>
                                                                            <br />
                                                                            <textarea id="auditTextArea" style="resize: none; height: 200px; width: 100%; font-size: 16px" class="text">                                                                            
                                                                            </textarea>


                                                                            <br /><br /><br />
                                                                            <button type="button" class="btn btn-default btnOkUpdate"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <%= Languages.getString("jsp.tools.bundling.update.status", SessionData.getLanguage())%></button>                                                                    
                                                                            <button type="button" class="btn btn-danger btnCancelUpdate"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.cancel", SessionData.getLanguage())%></button>                                                                                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="tbllstbundling">
                                                                        <table id="tbllstbundling" class="table table-hover">                                                                            
                                                                            <thead>
                                                                                <tr>                                                                                    
                                                                                    <th><%= Languages.getString("jsp.tools.bundling.tbl.c1", SessionData.getLanguage())%></th>
                                                                                    <th><%= Languages.getString("jsp.tools.bundling.tbl.c2", SessionData.getLanguage())%></th>
                                                                                    <th><%= Languages.getString("jsp.tools.bundling.tbl.c3", SessionData.getLanguage())%></th>
                                                                                    <th><%= Languages.getString("jsp.tools.bundling.tbl.c4", SessionData.getLanguage())%></th>
                                                                                    <th><%= Languages.getString("jsp.tools.bundling.tbl.c5", SessionData.getLanguage())%></th>
                                                                                    <th><%= Languages.getString("jsp.tools.bundling.tbl.c6", SessionData.getLanguage())%></th>
                                                                                </tr>
                                                                            </thead>                                                                                                    
                                                                        </table>
                                                                    </div>                                                                                                                                            
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody></table>
                </td>
            </tr>     
        </tbody>
    </table>
</body>      
<%@ include file="/includes/footer.jsp" %>
</html>