<%-- 
    Document   : messages
    Created on : 4/10/2018, 05:34:40 PM
    Author     : emida
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 40;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@ include file="/WEB-INF/jspf/pages/tools/pullMessage/pullMessageGlobals.jspf" %>
<%
    String path = request.getContextPath();
    setJavaScriptGlobals(out, SessionData.getLanguage());
    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title><%=Languages.getString("jsp.admin.tools.notification.messagesTitle", SessionData.getLanguage())%></title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/themes/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/loading.css" />      
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/dataTables/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<%=path%>/glyphicons/glyphicons.min.css" /> 
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/pages/tools/pullMessage/messages.css" /> 
    </head>
    <body>
        <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px">
            <tbody>
                <tr>
                    <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                    <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.admin.tools.notification.messagesTitle", SessionData.getLanguage())%></td>
                    <td height="20"><img src="images/top_right_blue.gif"></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                            <tbody>                  
                                <tr>
                                    <td width="1"><img src="images/trans.gif" width="1"></td>
                                    <td bgcolor="#ffffff" valign="top" align="center">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="main" align="left">
                                                        <table class="table">
                                                            <tbody>                                                     
                                                                <tr>                                                           
                                                                    <td>
                                                                        <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                            <li role="presentation" class="lstemplate active cm-tab1"><a style="cursor: pointer" class="lstemplate active"><img src="images/report-icon.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.admin.tools.notification.messagesTitle", SessionData.getLanguage())%></a></li>                                                                                                                                                                                                                                                                                                             
                                                                        </ul>  
                                                                        <br/>                                                                                                                                                                                                  
                                                                        <div class="container-fluid">
                                                                            <div class="content-cm-1">
                                                                                <form id="fmPullMessages" name="fmPullMessages" class="form-horizontal" autocomplete="off" >
                                                                                    <div class="row" style="background-color: #ffffff">                                                                                
                                                                                        <div class="col-md-2">                                                                                        
                                                                                            <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startDt", SessionData.getLanguage())%></h5>
                                                                                            <div class="input-group input-group-sm">
                                                                                                <span class="input-group-addon btncls" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                                                                                <input type="text" class="form-control" id="messageStartDate" name="messageStartDate" />
                                                                                            </div>
                                                                                            <br/><br/>
                                                                                            <button type="submit" id="btnSearchMessages" class="btn btn-default btnsearch"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.genericLabel.search", SessionData.getLanguage())%></button>                                                                                    
                                                                                        </div>
                                                                                        <div class="col-md-2">                                                                                        
                                                                                            <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.endDt", SessionData.getLanguage())%></h5>
                                                                                            <div class="input-group input-group-sm">  
                                                                                                <span class="input-group-addon btncle" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                                <input type="text" class="form-control" id="messageEndDate" name="messageEndDate" />                                                                                                                                                                                                                      
                                                                                            </div>                                                                                                                                                                                               
                                                                                        </div>
                                                                                        <div class="col-md-2">                                                                                        
                                                                                            <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.terminalType.label", SessionData.getLanguage())%></h5>
                                                                                            <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                                <select id="selectTerminals" name="selectTerminals" class="form-control"></select>
                                                                                            </div>                                                                                                                                                                                               
                                                                                        </div>
                                                                                        <div class="col-md-2">                                                                                        
                                                                                            <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.messageStatus.label", SessionData.getLanguage())%></h5>
                                                                                            <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                                <select id="selectMessageStatus" name="selectMessageStatus" class="form-control">
                                                                                                    <option value=""></option>
                                                                                                    <option value="1"><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.messageStatus.option.enable", SessionData.getLanguage())%></option>
                                                                                                    <option value="0"><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.messageStatus.option.disable", SessionData.getLanguage())%></option>
                                                                                                </select>
                                                                                            </div>                                                                                                                                                                                               
                                                                                        </div>
                                                                                        <div id="divDownloadMessages" class="col-lg-8" style="display:none;">
                                                                                            <br />
                                                                                            <button id="btnDownloadMessages" type="button" class="btn btn-default btndownload pull-right"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.genericLabel.download", SessionData.getLanguage())%></button>
                                                                                        </div>
                                                                                    </div>   
                                                                                </form>
                                                                                <br><br>
                                                                                <div id="divTableMessages" class="row" style="background-color: #ffffff; display:none;">
                                                                                    <div id="tbllstbundlingdiv" class="tbllstbundlingdiv text-muted card">
                                                                                        <table id="tableMessages" class="table table-hover table-responsive">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.terminalType.column.title", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.terminalId.column.title", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.message.column.title", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.messageRead.column.title", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.dateOnMessageWasRead.column.title", SessionData.getLanguage())%></th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>   
                                                                        </div>   
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/jquery-1.12.4.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/jquery/jquery-ui.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<%=path%>/js/pages/tools/pullMessage/Messages.js"></script>                                                                            
    </body>
    <%@ include file="/includes/footer.jsp" %>
</html>
