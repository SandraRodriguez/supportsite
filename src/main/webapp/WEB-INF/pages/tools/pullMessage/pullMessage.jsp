<%-- 
    Document   : pullMessage
    Created on : 14-mar-2018, 11:34:43
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title><%=Languages.getString("jsp.tool.notification.pull.message", SessionData.getLanguage())%></title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>                              

        <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
        <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
        <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>        

        <link href="<%=path%>/css/jquery.sweet-modal.min.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="<%=path%>/js/jquery.sweet-modal.min.js"></script>                 
        <script>
            var fi = null;
            var ff = null;
            var fia = null;
            var ffa = null;
            var cuid = null;
            var isadd = false;
            $(document).ready(function () {
                $('input[name="startDta"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    showWeekNumbers: true,
                    "autoApply": true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    startDate: moment(),
                    minDate: moment()
                }, function (start, end, label) {
                    fia = start.format('YYYY-MM-DD');
                    var c = null;
                    var c2 = null;
                    if (isadd) {
                        c = $('textarea#pmtx').val();
                        c2 = $('#pmsubject').val();
                    } else {
                        c = $('textarea#pmtxu').val();
                        c2 = $('#pmsubjectu').val();
                    }
                    if (c.length <= 10 || c2.length <= 10 || fia === null || fia === '' || ffa === null || ffa === '') {
                        $('.btnaddpm').attr('disabled', true);
                        $('.btnuppm').attr('disabled', true);
                    } else {
                        if (isValidRange()) {
                            $('.btnaddpm').attr('disabled', false);
                            $('.btnuppm').attr('disabled', false);
                        } else {
                            $('.btnaddpm').attr('disabled', true);
                            $('.btnuppm').attr('disabled', true);
                        }
                    }
                });
                $('input[name="endDta"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "autoApply": true,
                    showWeekNumbers: true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    startDate: moment(),
                    minDate: moment()
                }, function (start, end, label) {
                    ffa = start.format('YYYY-MM-DD');
                    var c = null;
                    var c2 = null;
                    if (isadd) {
                        c = $('textarea#pmtx').val();
                        c2 = $('#pmsubject').val();
                    } else {
                        c = $('textarea#pmtxu').val();
                        c2 = $('#pmsubjectu').val();
                    }
                    if (c.length <= 10 || c2.length <= 10 || fia === null || fia === '' || ffa === null || ffa === '') {
                        $('.btnaddpm').attr('disabled', true);
                        $('.btnuppm').attr('disabled', true);
                    } else {
                        if (isValidRange()) {
                            $('.btnaddpm').attr('disabled', false);
                            $('.btnuppm').attr('disabled', false);
                        } else {
                            $('.btnaddpm').attr('disabled', true);
                            $('.btnuppm').attr('disabled', true);
                        }
                    }
                });
                $('input[name="startDt"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "autoApply": true,
                    showWeekNumbers: true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                }, function (start, end, label) {
                    fi = start.format('YYYY-MM-DD');
                    if (fi === null || fi === '' || ff === null || ff === '') {

                    } else {
                        getByRange();
                    }
                });

                $('input[name="endDt"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "autoApply": true,
                    showWeekNumbers: true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                }, function (start, end, label) {
                    ff = end.format('YYYY-MM-DD');
                    if (fi === null || fi === '' || ff === null || ff === '') {
                        getByRange();
                    } else {
                        getByRange();
                    }
                });
                $('#pmsubject').keyup(function () {
                    var c = $('textarea#pmtx').val();
                    var c2 = $('#pmsubject').val();
                    if (c.length <= 10 || c2.length <= 10 || fia === null || fia === '' || ffa === null || ffa === '') {
                        $('.btnaddpm').attr('disabled', true);
                    } else {
                        $('.btnaddpm').attr('disabled', false);
                    }
                });
                $('#pmtx').keyup(function () {
                    var c = $('textarea#pmtx').val();
                    var c2 = $('#pmsubject').val();
                    if (c.length <= 10 || c2.length <= 10 || fia === null || fia === '' || ffa === null || ffa === '') {
                        $('.btnaddpm').attr('disabled', true);
                    } else {
                        $('.btnaddpm').attr('disabled', false);
                    }
                });
                $('#pmsubjectu').keyup(function () {
                    var c = $('textarea#pmtxu').val();
                    var c2 = $('#pmsubjectu').val();
                    if (c.length <= 10 || c2.length <= 10 || fia === null || fia === '' || ffa === null || ffa === '') {
                        $('.btnuppm').attr('disabled', true);
                    } else {
                        $('.btnuppm').attr('disabled', false);
                    }
                });
                $('#pmtxu').keyup(function () {
                    var c = $('textarea#pmtxu').val();
                    var c2 = $('#pmsubjectu').val();
                    if (c.length <= 10 || c2.length <= 10 || fia === null || fia === '' || ffa === null || ffa === '') {
                        $('.btnuppm').attr('disabled', true);
                    } else {
                        $('.btnuppm').attr('disabled', false);
                    }
                });
                $(".btncl").click(function () {
                    $('.startDt').trigger("click");
                });
                $(".btncl2").click(function () {
                    $('.endDt').trigger("click");
                });
                $('.btnaddpm').attr('disabled', true);
                $(".addenableu").change(function () {
                    $('.btnuppm').attr('disabled', false);
                });
                $("#businessUnitum").change(function () {
                    $('.btnuppm').attr('disabled', false);
                });
                $("#businessUnitut").change(function () {
                    $('.btnuppm').attr('disabled', false);
                });
                
                $("#channeluw").change(function () {
                    $('.btnuppm').attr('disabled', false);
                });
                $("#channelus").change(function () {
                    $('.btnuppm').attr('disabled', false);
                });
                getLast();
            });
            function isValidRange() {
                if (fia > ffa) {
                    return false;
                } else {
                    return true;
                }
            }
            function isaffected(data) {
                var obj = jQuery.parseJSON(data);
                if (obj.response === 0) {
                    return false;
                } else {
                    return true;
                }
            }
            var selectedBusinessUnit;
            function getValueUsingClass(unit) {
                var chkArray = [];
                $(unit).each(function () {
                    chkArray.push($(this).val());
                });
                selectedBusinessUnit = chkArray.join(',');
            }
            var selectedChannel;
            function getChannelsClass(unit) {
                var chkArray = [];
                $(unit).each(function () {
                    chkArray.push($(this).val());
                });
                selectedChannel = chkArray.join(',');
            }
            function add() {
                getValueUsingClass(".businessUnita:checked");
                getChannelsClass(".channela:checked")
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    data: {
                        subject: $('#pmsubject').val(),
                        content: $('textarea#pmtx').val(),
                        startDt: fia,
                        endDt: ffa,
                        status: $('.addenable').is(':checked'),
                        businessUnit: selectedBusinessUnit,
                        channel: selectedChannel
                    },
                    url: '/support/tools/pullMessage/add',
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    if (isaffected(data)) {
                        showSweet('<%=Languages.getString("jsp.tool.notification.pull.message.lbl.l8", SessionData.getLanguage())%>', true);
                        getLast();
                        $('input:checkbox').attr('checked', false);
                    } else {
                        showSweet('<%=Languages.getString("jsp.tool.notification.pull.message.lbl.l7", SessionData.getLanguage())%>', false);
                    }
                });
                $('textarea#pmtx').val('');
                $('#pmsubject').val('');
            }
            function update() {
                getValueUsingClass(".businessUnitu:checked");
                getChannelsClass(".channelu:checked");
                if (isValidRange()) {
                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'POST',
                        data: {
                            id: cuid,
                            subject: $('#pmsubjectu').val(),
                            content: $('textarea#pmtxu').val(),
                            startDt: fia,
                            endDt: ffa,
                            status: $('.addenableu').is(':checked'),
                            businessUnit: selectedBusinessUnit,
                            channel: selectedChannel
                        },
                        url: '/support/tools/pullMessage/update',
                        error: function (jqXHR, exception) {
                            $(".ajax-loading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        if (isaffected(data)) {
                            showSweet('<%=Languages.getString("jsp.tool.notification.pull.message.lbl.l10", SessionData.getLanguage())%>', true);
                            getLast();
                        } else {
                            showSweet('<%=Languages.getString("jsp.tool.notification.pull.message.lbl.l7", SessionData.getLanguage())%>', false);
                        }
                    });
                    $('textarea#pmtx').val('');
                    $('#pmsubject').val('');
                }
            }
            function remove(id) {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    data: {
                        id: id
                    },
                    url: '/support/tools/pullMessage/remove',
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    if (isaffected(data)) {
                        showSweet('<%=Languages.getString("jsp.tool.notification.pull.message.lbl.l9", SessionData.getLanguage())%>', true);
                        getLast();
                    } else {
                        showSweet('<%=Languages.getString("jsp.tool.notification.pull.message.lbl.l7", SessionData.getLanguage())%>', false);
                    }
                });
            }
            function showSweet(content, success) {
                if (success) {
                    $.sweetModal({
                        title: '<%=Languages.getString("jsp.tool.notification.pull.message", SessionData.getLanguage())%>',
                        content: content,
                        icon: $.sweetModal.ICON_SUCCESS,
                        theme: $.sweetModal.THEME_MIXED,
                        type: $.sweetModal.TYPE_MODAL,
                        showCloseButton: false,
                        timeout: 2000,
                        width: '30%'
                    });
                } else {
                    $.sweetModal({
                        title: '<%=Languages.getString("jsp.tool.notification.pull.message", SessionData.getLanguage())%>',
                        content: content,
                        icon: $.sweetModal.ICON_ERROR,
                        theme: $.sweetModal.THEME_MIXED,
                        type: $.sweetModal.TYPE_MODAL,
                        showCloseButton: false,
                        timeout: 2000,
                        width: '30%'
                    });
                }
            }
            function getLast() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/tools/pullMessage/getLast',
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    buildTbl(data);
                });
            }
            function getByRange() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    data: {
                        startDt: fi,
                        endDt: ff
                    },
                    url: '/support/tools/pullMessage/getByRange',
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    buildTbl(data);
                });
            }
            function showModalUpdate() {
                $('.btnuppm').attr('disabled', true);
                isadd = false;
                $('#pm-updatemodal').modal('show');
            }
            function  buildTbl(data) {
                $('#tblpullmsg').dataTable().fnDestroy();
                $('#tblpullmsg').DataTable({
                    aaData: data,
                    bLengthChange: false,
                    bDestroy: true,
                    info: true,
                    paging: true,
                    searching: true,
                    iDisplayLength: 13,
                    aoColumns: [
                        {mRender: function (data, type, row) {
                                return moment(row.createDt, "x").format("MM-DD-YYYY hh:mm:ss");
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "50px"
                        },
                        {"mData": 'subject', sDefaultContent: "--", sClass: "alignCenter"},
                        {mRender: function (data, type, row) {
                                if (row.status === true) {
                                    return "<span class='label label-success'><%= Languages.getString("jsp.tool.notification.pull.message.lbl.l1", SessionData.getLanguage())%></span>";
                                } else {
                                    return "<span class='label label-danger'><%= Languages.getString("jsp.tool.notification.pull.message.lbl.l2", SessionData.getLanguage())%></span>";
                                }
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "50px"
                        },
                        {mRender: function (data, type, row) {
                                return moment(row.startDt, "x").format("MM-DD-YYYY 00:00:00");
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "50px"
                        },
                        {mRender: function (data, type, row) {
                                return moment(row.endDt, "x").format("MM-DD-YYYY 23:59:59");
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "50px"
                        },
                        {mRender: function (data, type, row) {
                                return '<span style="cursor: pointer" data-toggle="modal" onclick="showModalUpdate()" class="glyphicon glyphicon-edit" aria-hidden="true"></span>';
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "20px"
                        }
                    ],
                    order: [[0, 'desc']],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '<%= Languages.getString("jsp.report.consolidate.monthly.sales.search.empty", SessionData.getLanguage())%>',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    },
                    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).on('click', function () {
                            cuid = aData.id;
                            $('#pmsubjectu').val(aData.subject);
                            $('textarea#pmtxu').val(aData.content);
                            $('.startDta').val(moment(aData.startDt, "x").format("MM/DD/YYYY"));
                            $('.endDta').val(moment(aData.endDt, "x").format("MM/DD/YYYY"));
                            fia = moment(aData.startDt, "x").format('YYYY-MM-DD');
                            ffa = moment(aData.endDt, "x").format('YYYY-MM-DD');
                            if (aData.status) {
                                $('.addenableu').prop('checked', true);
                            } else {
                                $('.addenableu').prop('checked', false);
                            }

                            if (aData.businessUnit.includes("MOBILMEX")) {
                                $('#businessUnitum').prop('checked', true);
                            } else {
                                $('#businessUnitum').prop('checked', false);
                            }
                            if (aData.businessUnit.includes("TERRECARGAMOS")) {
                                $('#businessUnitut').prop('checked', true);
                            } else {
                                $('#businessUnitut').prop('checked', false);
                            }
                            
                            if (aData.channel.includes("WEB")) {
                                $('#channeluw').prop('checked', true);
                            } else {
                                $('#channeluw').prop('checked', false);
                            }
                            if (aData.channel.includes("APP")) {
                                $('#channelus').prop('checked', true);
                            } else {
                                $('#channelus').prop('checked', false);
                            }
                        });
                    }
                });
            }
        </script>
    </head>

    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>

    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px;">
        <tbody>
            <tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.tool.notification.pull.message", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>                                                                                                                                                                                                                                                                   
                                                                    <div class="container-fluid">                                                                                                                                                
                                                                        <div class="row" style="background-color: #ffffff">
                                                                            <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                                <li role="presentation" class="lstemplate active "><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.tool.notification.pull.message", SessionData.getLanguage())%></a></li>                                                                                                                                                
                                                                            </ul>  
                                                                            <br />   
                                                                            <!--date control-->
                                                                            <div class="col-md-2 card">                                                                                                                                    
                                                                                <div class="form-group">                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c4", SessionData.getLanguage())%></h5>
                                                                                    <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                        <span class="input-group-addon btncl" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                        <input type="text" class="form-control startDt" name="startDt" />                                                                                                                                                                                                                      
                                                                                    </div>                                                                                                                                                                                               
                                                                                    <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startdt.lbl1", SessionData.getLanguage())%></small>
                                                                                </div>

                                                                                <div class="form-group">                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c5", SessionData.getLanguage())%></h5>
                                                                                    <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                        <span class="input-group-addon btncl2" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                        <input type="text" class="form-control endDt" name="endDt" />                                                                                                                                                                                                                      
                                                                                    </div>                                                                                                                                                                                               
                                                                                    <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startdt.lbl2", SessionData.getLanguage())%></small>
                                                                                </div>                                                                                 
                                                                                <br />
                                                                                <br />
                                                                            </div>                                                                                    
                                                                            <!--batchs table-->
                                                                            <div class="col-md-10">                                                                                    
                                                                                <a style="width: 150px" onclick="$('.btnaddpm').attr('disabled', true);
                                                                                        isadd = true" href="#" class="list-group-item btn" data-toggle="modal" data-target="#pm-addmodal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <%= Languages.getString("jsp.tool.notification.pull.message.lbl.l4", SessionData.getLanguage())%></a>                                                                                                                                                                                                                                                                        
                                                                                <br />
                                                                                <div class="tblpullmsg card">                                                                                                                                                                        
                                                                                    <table id="tblpullmsg" class="table table-hover text-muted">                                                                                                                                                                        
                                                                                        <thead>
                                                                                            <tr>                                                                                                                                                                        
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c1", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c2", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c3", SessionData.getLanguage())%></th>                                                                                                             
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c4", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c5", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tbl2.col4", SessionData.getLanguage())%></th>                                                                                                                                                                                               
                                                                                            </tr>
                                                                                        </thead>                                                                                                    
                                                                                    </table>   
                                                                                    <br />
                                                                                </div> 
                                                                            </div>                                                                                                                                                              
                                                                        </div>                                                                                                                                                                                                                                                                                    
                                                                        <!--add modal-->
                                                                        <div class="modal fade" id="pm-addmodal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper">
                                                                                <div class="modal-dialog vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.tool.notification.pull.message.lbl.l4", SessionData.getLanguage())%></h4>
                                                                                        </div>
                                                                                        <div class="modal-body">                                                                                                                                                                                                    
                                                                                            <!--subject-->
                                                                                            <div class="form-group">                                                                                                
                                                                                                <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c2", SessionData.getLanguage())%></label>                                                                                        
                                                                                                <input type="text" maxlength="40" minlength="10" class="form-control" id="pmsubject" aria-describedby="emailHelp" placeholder="<%= Languages.getString("jsp.tool.notification.pull.message.ph1", SessionData.getLanguage())%>">                                                                                                
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt1", SessionData.getLanguage())%></small>
                                                                                            </div>
                                                                                            <!--content-->
                                                                                            <div class="form-group">
                                                                                                <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.lbl.l13", SessionData.getLanguage())%></label>
                                                                                                <textarea placeholder="_" class="form-control" id="pmtx" maxlength="1024" style="resize: none; font-size: 16px; height: 150px; width: 100%" class="text"></textarea>                                                                                                                                                                 
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt2", SessionData.getLanguage())%></small>
                                                                                            </div>    

                                                                                            <div class="row" style="background-color: #ffffff">

                                                                                                <div class="col-md-6">
                                                                                                    <!--start date-->
                                                                                                    <div class="form-group">                                                                                                                                                                                          
                                                                                                        <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c4", SessionData.getLanguage())%></label>                                                                                        
                                                                                                        <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                                            <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                                            <input type="text" class="form-control startDta" name="startDta" />                                                                                                                                                                                                                      
                                                                                                        </div>
                                                                                                        <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt3", SessionData.getLanguage())%></small>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <!--end date-->
                                                                                                    <div class="form-group">                                                                                                
                                                                                                        <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c5", SessionData.getLanguage())%></label>                                                                                        
                                                                                                        <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                                            <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                                            <input type="text" class="form-control endDta" name="endDta" />                                                                                                                                                                                                                      
                                                                                                        </div>
                                                                                                        <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt4", SessionData.getLanguage())%></small>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>                                                                                                                                                                                        

                                                                                            <!--enabled-->
                                                                                            <div class="form-group">                                                                                                                                                                                                                                                                                                
                                                                                                <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.lbl.l14", SessionData.getLanguage())%></label>                                                                                        
                                                                                                <div class="input-group input-group-sm">
                                                                                                    <input class="addenable" name="addenable" id="addenable" type="checkbox">
                                                                                                </div>
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt5", SessionData.getLanguage())%></small>
                                                                                            </div>                                                                                                                                                                                           

                                                                                            <div class="row" style="background-color: #ffffff">

                                                                                                <!-- business unit -->
                                                                                                <div class="col-md-6">
                                                                                                    <div class="form-group">                                                                                                                                                                                                                                                                                                
                                                                                                        <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.unitbusines", SessionData.getLanguage())%></label>                                                                                                                                                                                        
                                                                                                        <div class="checkbox">
                                                                                                            <label class="text-muted"><input type="checkbox" class="businessUnita" value="MOBILMEX">Mobilmex</label>
                                                                                                        </div>                                                                                                    
                                                                                                        <div class="checkbox">
                                                                                                            <label class="text-muted"><input type="checkbox" class="businessUnita" value="TERRECARGAMOS">TeRecargamos</label>
                                                                                                        </div>
                                                                                                        <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.unitbusines.help", SessionData.getLanguage())%></small>
                                                                                                    </div>    
                                                                                                </div>

                                                                                                <!-- channels -->
                                                                                                <div class="col-md-6">
                                                                                                    <div class="form-group">                                                                                                                                                                                                                                                                                                
                                                                                                        <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.channel.title", SessionData.getLanguage())%></label>                                                                                                                                                                                        
                                                                                                        <div class="checkbox">
                                                                                                            <label class="text-muted"><input type="checkbox" class="channela" value="WEB">Web</label>
                                                                                                        </div>                                                                                                    
                                                                                                        <div class="checkbox">
                                                                                                            <label class="text-muted"><input type="checkbox" class="channela" value="APP">SmartPhone</label>
                                                                                                        </div>

                                                                                                        <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.channel.help", SessionData.getLanguage())%></small>
                                                                                                    </div>  
                                                                                                </div>

                                                                                            </div>                                                                                                                                                                                                                                                                                                                                                                                 

                                                                                        </div>
                                                                                        <div class="modal-footer">                                                                                                        
                                                                                            <button type="button" class="btn btn-default" onclick="$('textarea#pmtx').val('');" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                            <button type="button" class="btn btn-success btnaddpm" onclick="add();" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div>      
                                                                        <!--update modal-->
                                                                        <div class="modal fade" id="pm-updatemodal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper">
                                                                                <div class="modal-dialog vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.tool.notification.pull.message.lbl.l11", SessionData.getLanguage())%></h4>
                                                                                        </div>
                                                                                        <div class="modal-body">                                                                                                                                                                                                    
                                                                                            <!--subject-->
                                                                                            <div class="form-group">                                                                                                
                                                                                                <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c2", SessionData.getLanguage())%></label>                                                                                        
                                                                                                <input type="text" maxlength="40" minlength="10" class="form-control" id="pmsubjectu" aria-describedby="emailHelp" placeholder="<%= Languages.getString("jsp.tool.notification.pull.message.ph1", SessionData.getLanguage())%>">                                                                                                
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt1", SessionData.getLanguage())%></small>
                                                                                            </div>
                                                                                            <!--content-->
                                                                                            <div class="form-group">
                                                                                                <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.lbl.l13", SessionData.getLanguage())%></label>
                                                                                                <textarea placeholder="_" class="form-control" id="pmtxu" maxlength="1024" style="resize: none; font-size: 16px; height: 150px; width: 100%" class="text"></textarea>                                                                                                                                                                 
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt2", SessionData.getLanguage())%></small>
                                                                                            </div>    

                                                                                            <div class="row" style="background-color: #ffffff">

                                                                                                <div class="col-md-6">
                                                                                                    <!--start date-->
                                                                                                    <div class="form-group">                                                                                                                                                                                          
                                                                                                        <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c4", SessionData.getLanguage())%></label>                                                                                        
                                                                                                        <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                                            <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                                            <input type="text" class="form-control startDta" name="startDta" />                                                                                                                                                                                                                      
                                                                                                        </div>
                                                                                                        <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt3", SessionData.getLanguage())%></small>
                                                                                                    </div>                                                                                                    
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <!--end date-->
                                                                                                    <div class="form-group">                                                                                                
                                                                                                        <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tbl.c5", SessionData.getLanguage())%></label>                                                                                        
                                                                                                        <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                                            <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                                            <input type="text" class="form-control endDta" name="endDta" />                                                                                                                                                                                                                      
                                                                                                        </div>
                                                                                                        <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt4", SessionData.getLanguage())%></small>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>


                                                                                            <!--enabled-->
                                                                                            <div class="form-group">                                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.lbl.l14", SessionData.getLanguage())%></label>                                                                                        
                                                                                                <div class="input-group input-group-sm">                                                                                                     
                                                                                                    <input type="checkbox" id="addenableu" class="addenableu" />                                                                                                    
                                                                                                </div>                                                                                   
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.message.tt5", SessionData.getLanguage())%></small>
                                                                                            </div>    

                                                                                            <div class="row" style="background-color: #ffffff">

                                                                                                <div class="col-md-6">
                                                                                                    <!-- business unit -->
                                                                                                    <div class="form-group">                                                                                                                                                                                                                                                                                                
                                                                                                        <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.unitbusines", SessionData.getLanguage())%></label>                                                                                                                                                                                        
                                                                                                        <div class="checkbox">
                                                                                                            <label class="text-muted"><input type="checkbox" id="businessUnitum" class="businessUnitu" value="MOBILMEX">Mobilmex</label>
                                                                                                        </div>                                                                                                    
                                                                                                        <div class="checkbox">
                                                                                                            <label class="text-muted"><input type="checkbox" id="businessUnitut" class="businessUnitu" value="TERRECARGAMOS">TeRecargamos</label>
                                                                                                        </div>

                                                                                                        <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.unitbusines.help", SessionData.getLanguage())%></small>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <!-- channels -->
                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">                                                                                                                                                                                                                                                                                                
                                                                                                            <label class="text-muted"><%= Languages.getString("jsp.tool.notification.pull.channel.title", SessionData.getLanguage())%></label>                                                                                                                                                                                        
                                                                                                            <div class="checkbox">
                                                                                                                <label class="text-muted"><input type="checkbox" id="channeluw" class="channelu" value="WEB">Web</label>
                                                                                                            </div>                                                                                                    
                                                                                                            <div class="checkbox">
                                                                                                                <label class="text-muted"><input type="checkbox" id="channelus" class="channelu" value="APP">SmartPhone</label>
                                                                                                            </div>

                                                                                                            <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.tool.notification.pull.channel.help", SessionData.getLanguage())%></small>
                                                                                                        </div>  
                                                                                                    </div>                                                                                                    
                                                                                                </div>

                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="modal-footer">                                                                                                        
                                                                                            <button type="button" class="btn btn-default" onclick="$('textarea#pmtx').val('');" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                            <button type="button" class="btn btn-success btnuppm" onclick="update()" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div> 

                                                                    </div>                                                                      
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>     
        </tbody>
    </table>

</body>
<style type="text/css">
    .alignCenter { text-align: center; }
    .alignRight { text-align: right; }            
    .alignLeft { text-align: left; }

    .dataTables_wrapper .dataTables_length {
        float: left;
    }
    .dataTables_wrapper .dataTables_filter {
        float: left;
        text-align: left;                
    }
    .horizontal-scroll {
        overflow-x:scroll ; 
        overflow-y: scroll;
    }
    .dataTables_wrapper .dt-buttons {
        float: right;
    }            

    .modal {
    }
    .vertical-alignment-helper {
        display:table;
        height: 100%;
        width: 100%;
    }
    .vertical-align-center {                
        display: table-cell;
        vertical-align: middle;
    }
    .modal-content {                
        width:inherit;
        height:inherit;               
        margin: 0 auto;
    }
    .modal.fade .modal-dialog {
        -webkit-transform: scale(0.1);
        -moz-transform: scale(0.1);
        -ms-transform: scale(0.1);
        transform: scale(0.1);
        top: 300px;
        opacity: 0;
        -webkit-transition: all 0.1s;
        -moz-transition: all 0.1s;
        transition: all 0.1s;
    }

    .modal.fade.in .modal-dialog {
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1);
        -webkit-transform: translate3d(0, -300px, 0);
        transform: translate3d(0, -300px, 0);
        opacity: 1;
    }
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;        
        border-radius: 5px;        
    }

    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
</style>
</html>
