<%-- 
    Document   : achStatement
    Created on : Jun 13, 2019, 9:42:09 AM
    Author     : janez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%

    int section = 9;
    int section_page = 47;

    /*
    int section = 4;
    int section_page = 1;                
     */
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<!DOCTYPE html>
<html>
    <head>             
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <title><%=Languages.getString("promotions.main.title", SessionData.getLanguage())%></title>                        

        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>                                                            
        <script src="<%=path%>/js/themes/vue.js" type="text/javascript"></script>                                          

        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/themes/vue_upload_style.css" rel="stylesheet" type="text/css"/>                
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css" />               

        <script src="<%=path%>/js/sweetalert.min.js" type="text/javascript"></script>                          
        <script type="text/javascript" src="<%=path%>/includes/_lib/tinymce/tinymce.min.js"></script>

        <link rel="stylesheet" href="<%=path%>/css/element_ui.css"/>             
        <script src="<%=path%>/js/vue/element_ui_en.js" type="text/javascript"></script>    

        <script>
            $(document).ready(function () {
                $('textarea').keypress(function (event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                    }
                });
            });

            function getTinyContent() {
                return tinyMCE.activeEditor.getContent();
            }
            function setContent(content) {
                tinyMCE.activeEditor.setContent(content);
            }

            function getLang() {
                rlang = '${SessionData.getLanguage()}';
                if (rlang === 'english') {
                    return 'en';
                } else {
                    return 'es';
                }
            }
            var voptions = {
                selector: "#contemplate",
                height: 500,
                theme: 'modern',
                language: getLang(),
                plugins: [
                    'advlist autolink lists link charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image print preview media | forecolor backcolor',
                image_advtab: true,
                plugin_preview_width: "1200",
                plugin_preview_height: "800",
                templates: [
                    {
                        title: '${Languages.getString("jsp.tools.notice.contracts.template.title")}',
                        /*description: '${Languages.getString("jsp.tools.notice.contracts.template.description")}',*/
                        url: window.location.protocol + "//" + window.location.hostname + ':' + window.location.port + '/support/admin/templates/temp_mexico.jsp'
                    }
                ]
            };

            tinyMCE.init(voptions);
        </script>
    </head>

    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>

    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px;">
        <tbody>
            <tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="1800px"><%=Languages.getString("ach.statement.main.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>                                                                                                                                                                                                                                                                   
                                                                    <div class="container-fluid text text-muted" id="ach-statements">                                                                                   

                                                                        <div class="row text" style="background-color: #fff !important">
                                                                            <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                                <li role="presentation" class="lstemplate active "><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("ach.statement.main.title", SessionData.getLanguage())%></a></li>                                                                                                                                                
                                                                            </ul>  
                                                                            <br />                                                                                                                                                                                                                                                                                                            
                                                                        </div>   

                                                                        <div v-show="view === 'init'" class="row text text-muted" style="background-color: #fff !important">                                                                        
                                                                            <div class="col-md-12 text-muted">     


                                                                                <div v-if="list.length <= 0">
                                                                                    <el-card class="box-card" style="margin-top: 1%; padding: 1%; width: 30% !important">                                                                                                                                                         

                                                                                        <center>
                                                                                            <span class="glyphicon glyphicon-plus" style="font-size:200px;" aria-hidden="true"></span>
                                                                                            <hr />                                                                                       
                                                                                        </center>

                                                                                        <div class="pull-right">
                                                                                            <el-button @click="showAddForm" type="default" round><%=Languages.getString("jsp.admin.genericLabel.add", SessionData.getLanguage())%> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></el-button>                                                                                                                                                                          
                                                                                        </div>
                                                                                    </el-card> 
                                                                                </div>

                                                                                <div>
                                                                                    <el-card class="box-card" v-for="item in list" style="margin-top: 1%; padding: 1%; width: 30% !important">                                                                                                                                                         

                                                                                        <center>
                                                                                            <span class="glyphicon glyphicon-cog" style="font-size:200px;" aria-hidden="true"></span>
                                                                                            <hr />                                                                                       
                                                                                        </center>

                                                                                        <div class="pull-right">
                                                                                            <el-button @click="handleEdit(item)" type="default" round><%=Languages.getString("jsp.admin.genericLabel.edit", SessionData.getLanguage())%> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></el-button>                                                                                                                                                                          
                                                                                        </div>
                                                                                    </el-card> 
                                                                                </div>                                                                                                                                                             

                                                                            </div>    

                                                                        </div>


                                                                        <div v-show="view === 'add'">
                                                                            <el-card class="box-card">

                                                                                <button @click="view= 'init';" type="button " class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>                                                                                  
                                                                                <h3><%=Languages.getString("ach.statement.main.title", SessionData.getLanguage())%></h3>                                                                            
                                                                                <hr />

                                                                                <div style="margin-top: 2%;">
                                                                                    <div>                                                                                
                                                                                        <el-form ref="form" v-bind:rules="basic_rules" :model="model" label-width="0px">

                                                                                            <div class="row text text-muted" style="background-color: #fff">
                                                                                                <div class="col-md-6" style="padding-left: 3%">
                                                                                                    <el-form-item>
                                                                                                        <h4><%=Languages.getString("ach.statement.main.lbl.model.image.header.1", SessionData.getLanguage())%></h4>
                                                                                                        <el-upload                                                                                                                 
                                                                                                            drag
                                                                                                            ref="uploaderhead"
                                                                                                            :limit="1"
                                                                                                            accept="image/jpeg,image/gif,image/png"
                                                                                                            :multiple="false"                                                                                                    
                                                                                                            action="/support/tools/ach-statement/loadImage"
                                                                                                            list-type="picture-card"                                                                                                    
                                                                                                            :on-preview="handlePictureCardPreview">                                                                                                              
                                                                                                            <img v-if="imageUrl" :src="imageUrl" class="avatar">
                                                                                                            <i v-else class="el-icon-plus avatar-uploader-icon"></i>                                                                                                                
                                                                                                        </el-upload>   

                                                                                                        <el-dialog show-close="false" :visible.sync="dialogVisible">
                                                                                                            <img width="50%" :src="dialogImageUrl" alt="">
                                                                                                        </el-dialog>                                                                                                     
                                                                                                        <br />                                                                                                         
                                                                                                    </el-form-item>

                                                                                                    <el-form-item prop="title">
                                                                                                        <h4><%=Languages.getString("ach.statement.main.lbl.model.txt.header.1", SessionData.getLanguage())%></h4>
                                                                                                        <el-input
                                                                                                            style="height: 50px !important; width: 360px !important"
                                                                                                            v-model="model.title"
                                                                                                            :maxlength="32"
                                                                                                            type="text"                                                                                                            
                                                                                                            placeholder="<%=Languages.getString("ach.statement.main.lbl.model.txt.header.1", SessionData.getLanguage())%>">
                                                                                                        </el-input>
                                                                                                    </el-form-item>

                                                                                                </div>

                                                                                                <div class="col-md-6">
                                                                                                    <el-form-item>
                                                                                                        <h4><%=Languages.getString("ach.statement.main.lbl.model.image.footer.1", SessionData.getLanguage())%></h4>
                                                                                                        <el-upload  
                                                                                                            drag
                                                                                                            ref="uploaderfoot"
                                                                                                            :limit="1"
                                                                                                            accept="image/jpeg,image/gif,image/png"
                                                                                                            :multiple="false"                                                                                                    
                                                                                                            action="/support/tools/ach-statement/loadImage"
                                                                                                            list-type="picture-card"                                                                                                    
                                                                                                            :on-preview="handlePictureCardPreview">
                                                                                                            <img v-if="imageUrl" :src="imageUrl" class="avatar">
                                                                                                            <i v-else class="el-icon-plus avatar-uploader-icon"></i>                                                                                                                
                                                                                                        </el-upload>                                                                                                
                                                                                                        <el-dialog show-close="false" :visible.sync="dialogVisible">
                                                                                                            <img width="50%" :src="dialogImageUrl" alt="">
                                                                                                        </el-dialog>
                                                                                                        <br />
                                                                                                    </el-form-item>

                                                                                                    <el-form-item prop="footer_line1">
                                                                                                        <h4><%=Languages.getString("ach.statement.main.lbl.model.txt.footer.1", SessionData.getLanguage())%></h4>
                                                                                                        <el-input
                                                                                                            v-model="model.footer_line1"
                                                                                                            :maxlength="110"
                                                                                                            type="textarea"
                                                                                                            :rows="2"
                                                                                                            placeholder="<%=Languages.getString("ach.statement.main.lbl.model.txt.footer.1", SessionData.getLanguage())%>">
                                                                                                        </el-input>
                                                                                                    </el-form-item>


                                                                                                    <el-form-item prop="footer_line2">
                                                                                                        <h4><%=Languages.getString("ach.statement.main.lbl.model.txt.footer.2", SessionData.getLanguage())%></h4>
                                                                                                        <el-input
                                                                                                            v-model="model.footer_line2"
                                                                                                            :maxlength="110"
                                                                                                            type="textarea"
                                                                                                            :rows="2"
                                                                                                            placeholder="<%=Languages.getString("ach.statement.main.lbl.model.txt.footer.2", SessionData.getLanguage())%>">
                                                                                                        </el-input>
                                                                                                    </el-form-item>

                                                                                                    <el-form-item prop="image_line">
                                                                                                        <h4><%=Languages.getString("ach.statement.main.lbl.model.txt.footer.3", SessionData.getLanguage())%></h4>
                                                                                                        <el-input
                                                                                                            v-model="model.image_line"
                                                                                                            :maxlength="110"
                                                                                                            type="textarea"
                                                                                                            :rows="2"
                                                                                                            placeholder="<%=Languages.getString("ach.statement.main.lbl.model.txt.footer.3", SessionData.getLanguage())%>">
                                                                                                        </el-input>
                                                                                                    </el-form-item>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="row text text-muted" style="background-color: #fff; margin-left: 1%; margin-right: 1%">
                                                                                                <hr />
                                                                                                <h3>Notification Setting</h3>        
                                                                                                <br />                                                                                                


                                                                                                <div class="col-md-8">                                                                                                    
                                                                                                    <h4><%=Languages.getString("ach.statement.main.lbl.model.txt.email.body", SessionData.getLanguage())%></h4>
                                                                                                    <textarea v-model="model.email_body" id="contemplate"></textarea>
                                                                                                </div>

                                                                                                <div class="col-md-4">                                                                                                                    
                                                                                                    <el-form-item prop="email_from" label="">
                                                                                                        <h4><%=Languages.getString("ach.statement.main.lbl.model.txt.email.from", SessionData.getLanguage())%></h4>
                                                                                                        <el-input                                                                                                            
                                                                                                            v-model="model.email_from"
                                                                                                            :maxlength="32"
                                                                                                            type="text"                                                                                                            
                                                                                                            placeholder="<%=Languages.getString("ach.statement.main.lbl.model.txt.email.from", SessionData.getLanguage())%>">
                                                                                                        </el-input>
                                                                                                    </el-form-item>


                                                                                                    <el-form-item prop="email_subject" label="">
                                                                                                        <h4><%=Languages.getString("ach.statement.main.lbl.model.txt.email.subject", SessionData.getLanguage())%></h4>
                                                                                                        <el-input                                                                                                            
                                                                                                            v-model="model.email_subject"
                                                                                                            :maxlength="32"
                                                                                                            type="text"                                                                                                            
                                                                                                            placeholder="<%=Languages.getString("ach.statement.main.lbl.model.txt.email.subject", SessionData.getLanguage())%>">
                                                                                                        </el-input>
                                                                                                    </el-form-item>
                                                                                                </div>

                                                                                            </div>

                                                                                        </el-form>                                                                            

                                                                                    </div>                                                                         

                                                                                </div>


                                                                                <hr style="margin-top: 2%" />
                                                                                <el-row class="pull-right" style="margin-bottom: 2%">
                                                                                    <el-button @click="cancelAction()" plain><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></el-button>                                                                                    
                                                                                    <el-button @click="save()" type="success" plain><span class="glyphicon glyphicon-save" aria-hidden="true"></span> <%=Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></el-button>                                                                                
                                                                                </el-row>

                                                                                <el-row v-if="requestType === 'update'" class="pull-left" style="margin-bottom: 2%">
                                                                                    <el-button @click="handleRemove" type="danger" plain><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <%=Languages.getString("jsp.admin.genericLabel.Delete", SessionData.getLanguage())%></el-button>                                                                                                                                                                                                                                                                                                  
                                                                                    <br />                                                                                    
                                                                                </el-row>                                                                                                                                                                

                                                                            </el-card>
                                                                        </div>

                                                                    </div>                                                                      
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>     
        </tbody>                    
    </table>
</body>
<script src="<%=path%>/js/pages/tools/achStatement/achStatement.js" type="text/javascript"></script>        

<style type="text/css">

    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.005s;        
        border-radius: 5px;     
    }
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
    .el-table .danger-row {
        background: #fff9f9;
    }

    .el-upload--picture-card {
        background-color: #fbfdff;
        border: 1px  #c0ccda !important; 
        border-radius: 10px !important;
        box-sizing: border-box;
        width: 146px !important;
        height: 146px !important;
        line-height: 146px !important;
        vertical-align: top;
    }  

    textarea {
        resize: none !important;
    }

</style>
</html>