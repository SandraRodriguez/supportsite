<%-- 
    Document   : pinManagementStatus
    Created on : 25-ene-2018, 15:08:21
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>JSP Page</title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>                              

        <link href="<%=path%>/css/multi-select.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.multi-select.js"></script>   
        <script type="text/javascript" src="<%=path%>/js/jquery.quicksearch.js"></script> 

        <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
        <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
        <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>        

        <link href="<%=path%>/css/jquery.sweet-modal.min.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="<%=path%>/js/jquery.sweet-modal.min.js"></script> 

        <script>
            var fromDateEx = null;
            var vbulkOperType = -1;
            var vpin = null;
            var voldPinStatusId = null;
            var vobject = null;
            var toDateEx = null;
            var topsi = null;

            var intPager = 500;
            var pf = 0;
            var pt = 0;
            var evninc = false;
            var evndec = false;

            var pft2 = 0;
            var ptt2 = 0;
            var evninct2 = false;
            var evndect2 = false;

            function initct1() {
                pf = 0;
                pt = intPager;
                evninc = false;
                evndec = false;
            }
            function initct2() {
                pft2 = 0;
                ptt2 = intPager;
                evninct2 = false;
                evndect2 = false;
            }

            $(document).ready(function () {
                $(".divErrMessage").hide();
                $(".tbllstbundlingdiv").hide();
                $(".tblpinsdiv").hide();
                $(".diveditstatuspin").hide();
                $(".divedit").hide();
                $('.btnsearchanipn').hide();
                $('.divoptions').hide();

                initct1();
                initct2();

                $(".btncl").click(function () {
                    $('.startDt').trigger("click");
                });

                $(".btncl2").click(function () {
                    $('.endDt').trigger("click");
                });

                $('.decrebtn').prop("disabled", true);

                $('input[name="startDt"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "autoApply": true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    endDate: moment(),
                    maxDate: moment()
                }, function (start, end, label) {
                    fromDateEx = start.format('YYYY-MM-DD');
                    if (fromDateEx === null || fromDateEx === '' || toDateEx === null || toDateEx === '') {
                        $('.btnsearchanipn').hide();
                    } else {
                        var reportBackwardDateLimit = 5;

                        var duration = moment.duration(start.diff(toDateEx));
                        var diffDays = parseInt(duration.asMonths());
                        if (diffDays > reportBackwardDateLimit || diffDays < 0) {
                            $.sweetModal({
                                title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                                icon: $.sweetModal.ICON_WARNING,
                                content: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.sweet.message.1", SessionData.getLanguage())%>',
                                theme: $.sweetModal.THEME_MIXED,
                                type: $.sweetModal.TYPE_MODAL,
                                showCloseButton: true,
                                blocking: true
                            });
                            $('.btnsearchanipn').hide();
                        } else {
                            $('.btnsearchanipn').show();
                        }
                    }
                });

                $('input[name="endDt"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "autoApply": true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    endDate: moment(),
                    maxDate: moment()
                }, function (start, end, label) {
                    toDateEx = end.format('YYYY-MM-DD');
                    initct1();
                    initct2();
                    if (fromDateEx === null || fromDateEx === '' || toDateEx === null || toDateEx === '') {
                        $('.btnsearchanipn').hide();
                    } else {
                        var reportBackwardDateLimit = 5;

                        var duration = moment.duration(end.diff(fromDateEx));
                        var diffDays = parseInt(duration.asMonths());
                        if (diffDays > reportBackwardDateLimit) {
                            $.sweetModal({
                                title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                                icon: $.sweetModal.ICON_WARNING,
                                content: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.sweet.message.1", SessionData.getLanguage())%>',
                                theme: $.sweetModal.THEME_MIXED,
                                type: $.sweetModal.TYPE_MODAL,
                                showCloseButton: true
                            });
                            $('.btnsearchanipn').hide();
                        } else {
                            $('.btnsearchanipn').show();
                        }
                    }
                });

                $('.btnsearchanipn').click(function () {
                    findBatchesByInsertDate();
                });

                $('#auditTextArea1').keyup(function () {
                    var c = $('textarea#auditTextArea1').val();
                    if (c.length <= 10) {
                        $('.btnupdatesku').attr('disabled', true);
                    } else {
                        $('.btnupdatesku').attr('disabled', false);
                    }
                });

                $('#auditTextArea2').keyup(function () {
                    var c = $('textarea#auditTextArea2').val();
                    if (c.length <= 10) {
                        $('.btnrmp').attr('disabled', true);
                    } else {
                        $('.btnrmp').attr('disabled', false);
                    }
                });
                $('#auditTextArea3').keyup(function () {
                    var c = $('textarea#auditTextArea3').val();
                    if (c.length <= 10) {
                        $('.btnbk').attr('disabled', true);
                    } else {
                        $('.btnbk').attr('disabled', false);
                    }
                });
            });
            function incrementbtl1() {
                evninc = true;
                evndec = false;
                $('.decrebtn').prop("disabled", false);
                pf = pt + 1;
                pt = pt + intPager;
                findBatchesByInsertDate();
            }
            function decreasetbl1() {
                evninc = false;
                evndec = true;
                $('.increbtn').prop("disabled", false);
                pt = pf - 1;
                pf = pf - intPager;
                findBatchesByInsertDate();
            }
            function incrementbtl2() {
                evninct2 = true;
                evndect2 = false;
                $('.decrebtnt2').prop("disabled", false);
                pft2 = ptt2 + 1;
                ptt2 = ptt2 + intPager;
                onFindPinsByBatchId(vobject);
            }
            function decreasetbl2() {
                evninct2 = false;
                evndect2 = true;
                $('.increbtnt2').prop("disabled", false);
                ptt2 = pft2 - 1;
                pft2 = pft2 - intPager;
                onFindPinsByBatchId(vobject);
            }

            function findBatchesByInsertDate() {
                $(".diveditstatuspin").hide();
                if (fromDateEx !== null && fromDateEx !== '' && toDateEx !== null && toDateEx !== '') {
                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'GET',
                        data: {
                            startDt: fromDateEx,
                            endDt: toDateEx,
                            pageFrom: pf,
                            pageTo: pt
                        },
                        url: '/support/tools/pinManagementStatus/findBatchesByInsertDate',
                        async: true,
                        error: function (jqXHR, exception) {
                            $(".ajax-loading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        if (data.length === 0) {
                            $.sweetModal({
                                title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                                icon: $.sweetModal.ICON_WARNING,
                                content: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.sweet.message.2", SessionData.getLanguage())%>',
                                theme: $.sweetModal.THEME_MIXED,
                                type: $.sweetModal.TYPE_MODAL,
                                showCloseButton: true
                            });
                        }
                        if (data.length < intPager) {
                            if (evninc) {
                                $('.increbtn').prop("disabled", true);
                            } else {
                                $('.increbtn').prop("disabled", true);
                            }
                        } else {
                            $('.increbtn').prop("disabled", false);
                        }
                        if ((pf === 0 && pt === intPager) || (pf === 1 && pt === intPager)) {
                            $('.decrebtn').prop("disabled", true);
                        }
                        onSuccessResponse(data);
                    });
                    $(".divErrMessage").hide();
                } else {
                    $(".tbllstbundlingdiv").hide();
                }
            }

            function onSuccessResponse(data) {
                if (data.length === 0) {
                    $(".tbllstbundlingdiv").hide();
                    $(".btnReportDownloader").hide();
                    $('#tbllstbundling').dataTable().fnClearTable();
                    $(".divErrMessage").show();
                    $('#errMessage').text('<%= Languages.getString("jsp.includes.menu.xBorderExchangeRateAuditReport.err.empty", SessionData.getLanguage())%>');
                    $(".alertmaxs").hide(150);
                    $('#cadd').text(' (0)');
                    $('#cedit').text(' (0)');
                    $('#cremove').text(' (0)');
                } else {
                    if (fromDateEx !== null && fromDateEx !== '' && toDateEx !== null && toDateEx !== '') {
                        $(".btnReportDownloader").show();
                    } else {
                        $(".btnReportDownloader").hide();
                    }
                    $(".tbllstbundlingdiv").show();
                    $(".filterbyc").show();
                    $(".divErrMessage").hide();
                    $('#tbllstbundling').dataTable().fnDestroy();
                    buildTable(data);
                }
            }

            function onFindPinsByBatchId(data) {
                $(".diveditstatuspin").hide();
                $(".tblpinsdiv").hide();
                $(".divoptions").hide();

                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    data: {
                        startDt: fromDateEx,
                        endDt: toDateEx,
                        ani: data.productId,
                        pageFrom: pft2,
                        pageTo: ptt2
                    },
                    url: '/support/tools/pinManagementStatus/findPinsByProductId',
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    if (data.length < intPager) {
                        if (evninct2) {
                            $('.increbtnt2').prop("disabled", true);
                        } else {
                            $('.increbtnt2').prop("disabled", true);
                        }
                    } else {
                        $('.increbtnt2').prop("disabled", false);
                    }
                    if ((pft2 === 0 && ptt2 === intPager) || (pft2 === 1 && ptt2 === intPager)) {
                        $('.decrebtnt2').prop("disabled", true);
                    }
                    if (data.length === 0) {
                        $(".tblpinsdiv").hide();
                    } else {
                        $(".tblpinsdiv").show();
                        $(".divoptions").show();
                        $('.hospy').show();
                        buildTablePins(data);
                    }
                });
            }
            function findPinByMatch() {
                var pin = $('.spin').val();
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    data: {
                        pin: pin
                    },
                    url: '/support/tools/pinManagementStatus/findPinByMatch',
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                        $('.spin').val('');
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                    $('.spin').val('');
                });
                result.success(function (data, status, headers, config) {
                    buildTablePins(data);
                    $('.spin').val('');
                    $('.hospy').hide();
                });
            }
            function setpin(pin, oldPinStatusId) {
                vpin = pin;
                voldPinStatusId = oldPinStatusId;
                $('.btnupdatesku').attr('disabled', true);
                $('.btnrmp').attr('disabled', true);
            }
            function onEditPin() {
                var pinStatusId = $("#cmseldc :selected").val();
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    data: {
                        pin: $.trim(vpin),
                        oldPinStatusId: voldPinStatusId,
                        pinStatusId: pinStatusId,
                        reason: $('textarea#auditTextArea1').val()
                    },
                    url: '/support/tools/pinManagementStatus/updatePin',
                    async: true,
                    error: function (jqXHR, exception) {
                        $.sweetModal({
                            title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                            icon: $.sweetModal.ICON_ERROR,
                            theme: $.sweetModal.THEME_MIXED,
                            type: $.sweetModal.TYPE_MODAL,
                            showCloseButton: false,
                            timeout: 2000,
                            width: '30%'
                        });
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    $.sweetModal({
                        title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                        icon: $.sweetModal.ICON_SUCCESS,
                        theme: $.sweetModal.THEME_MIXED,
                        type: $.sweetModal.TYPE_MODAL,
                        showCloseButton: false,
                        timeout: 2000,
                        width: '30%'
                    });
                    onFindPinsByBatchId(vobject);
                });
                $('textarea#auditTextArea2').val('');
                $('textarea#auditTextArea1').val('');
            }
            function onRemovePin() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    data: {
                        pin: vpin,
                        oldPinStatusId: voldPinStatusId,
                        reason: $('textarea#auditTextArea2').val()
                    },
                    url: '/support/tools/pinManagementStatus/removePin',
                    async: true,
                    error: function (jqXHR, exception) {
                        $.sweetModal({
                            title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                            icon: $.sweetModal.ICON_ERROR,
                            theme: $.sweetModal.THEME_MIXED,
                            type: $.sweetModal.TYPE_MODAL,
                            showCloseButton: false,
                            timeout: 2000,
                            width: '30%'
                        });
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onFindPinsByBatchId(vobject);
                    $.sweetModal({
                        title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                        icon: $.sweetModal.ICON_SUCCESS,
                        theme: $.sweetModal.THEME_MIXED,
                        type: $.sweetModal.TYPE_MODAL,
                        showCloseButton: false,
                        timeout: 2000,
                        width: '30%'
                    });
                });
                $('textarea#auditTextArea2').val('');
                $('textarea#auditTextArea1').val('');
            }
            function bulkOperType() {
                if (vbulkOperType === 1 || vbulkOperType === 2) {
                    onUpdateBulkPin();
                } else if (vbulkOperType === 3) {
                    onRemoveBulkPin();
                }
            }
            function onRemoveBulkPin() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    data: {
                        startDt: fromDateEx,
                        endDt: toDateEx,
                        product_id: vobject.productId,
                        reason: $('textarea#auditTextArea3').val()
                    },
                    url: '/support/tools/pinManagementStatus/removeBulkPin',
                    async: true,
                    error: function (jqXHR, exception) {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                    $.sweetModal({
                        title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                        icon: $.sweetModal.ICON_ERROR,
                        theme: $.sweetModal.THEME_MIXED,
                        type: $.sweetModal.TYPE_MODAL,
                        showCloseButton: false,
                        timeout: 2000,
                        width: '30%'
                    });
                });
                result.success(function (data, status, headers, config) {
                    onFindPinsByBatchId(vobject);
                    $.sweetModal({
                        title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                        icon: $.sweetModal.ICON_SUCCESS,
                        theme: $.sweetModal.THEME_MIXED,
                        type: $.sweetModal.TYPE_MODAL,
                        showCloseButton: false,
                        timeout: 2000,
                        width: '30%'
                    });
                });
                $('textarea#auditTextArea3').val('');
            }
            function onUpdateBulkPin() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'POST',
                    data: {
                        startDt: fromDateEx,
                        endDt: toDateEx,
                        product_id: vobject.productId,
                        pinStatusId: topsi,
                        reason: $('textarea#auditTextArea3').val()
                    },
                    url: '/support/tools/pinManagementStatus/updateBulkPin',
                    async: true,
                    error: function (jqXHR, exception) {
                        $.sweetModal({
                            title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                            icon: $.sweetModal.ICON_ERROR,
                            theme: $.sweetModal.THEME_MIXED,
                            type: $.sweetModal.TYPE_MODAL,
                            showCloseButton: false,
                            timeout: 2000,
                            width: '30%'
                        });
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onFindPinsByBatchId(vobject);
                    $.sweetModal({
                        title: '<%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>',
                        icon: $.sweetModal.ICON_SUCCESS,
                        theme: $.sweetModal.THEME_MIXED,
                        type: $.sweetModal.TYPE_MODAL,
                        showCloseButton: false,
                        timeout: 2000,
                        width: '30%'
                    });
                });
                $('textarea#auditTextArea3').val('');
            }
            function buildTable(data) {
                $(".tblpinsdiv").hide();
                $(".divoptions").hide();
                $('#tbllstbundling').DataTable({
                    aaData: data,
                    bLengthChange: false,
                    bDestroy: true,
                    bSortable: false,
                    info: false,
                    paging: false,
                    searching: true,
                    deferRender: true,
                    scrollY: 400,
                    scrollCollapse: true,
                    scroller: {
                        loadingIndicator: true
                    },
                    aoColumns: [
                        {"mData": 'productId', sDefaultContent: "--", sClass: "alignCenter"},
                        {mRender: function (data, type, row) {
                                return '<span class="badge">' + row.pinCount + '</span> ';
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "50px"
                        }
                    ],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '<%= Languages.getString("jsp.report.consolidate.monthly.sales.search.empty", SessionData.getLanguage())%>',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    },
                    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).on('click', function () {
                            onFindPinsByBatchId(aData);
                            initct2();
                            vobject = aData;
                        });
                    }
                });
            }
            function  buildTablePins(data) {
                $('#tblpins').dataTable().fnDestroy();
                $('#tblpins').DataTable({
                    aaData: data,
                    bLengthChange: false,
                    bDestroy: true,
                    bSortable: false,
                    info: false,
                    paging: false,
                    searching: true,
                    deferRender: true,
                    scrollY: 400,
                    scrollCollapse: true,
                    scroller: {
                        loadingIndicator: true
                    },
                    aoColumns: [
                        {"mData": 'pin', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'productId', sDefaultContent: "--", sClass: "alignCenter"},
                        {mRender: function (data, type, row) {
                                if (row.pinStatus === 'AVAILABLE') {
                                    return '<span class="label label-success">' + row.pinStatus + '</span>';
                                } else {
                                    return '<span class="label label-danger">' + row.pinStatus + '</span>';
                                }
                            }, sDefaultContent: "--", sClass: "alignCenter"
                        },
                        {mRender: function (data, type, row) {
                                if (row.pinStatus === 'AVAILABLE' || row.pinStatus === 'QUARANTINED') {
                                    return '<span style="cursor: pointer" onclick=\'setpin("' + row.pin + '","' + row.pinStatusId + '")\' data-toggle="modal" data-target="#cm-editModal" class="glyphicon glyphicon-edit" aria-hidden="true"></span>';
                                } else {
                                    return '--';
                                }
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "50px"
                        },
                        {mRender: function (data, type, row) {
                                if (row.activationDt === null && row.transactionCount === 0 && (row.pinStatus === 'AVAILABLE' || row.pinStatus === 'QUARANTINED')) {
                                    return '<span style="cursor: pointer" onclick=\'setpin("' + row.pin + '","' + row.pinStatusId + '")\' data-toggle="modal" data-target="#cm-rmodal" class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                                } else {
                                    return '--';
                                }
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "50px"
                        }
                    ],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '<%= Languages.getString("jsp.report.consolidate.monthly.sales.search.empty", SessionData.getLanguage())%>',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    },
                    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).on('click', function () {
                            $(".diveditstatuspin").show();
                        });
                    }
                });
            }

        </script>
        <style type="text/css">
            .alignCenter { text-align: center; }
            .alignRight { text-align: right; }            
            .alignLeft { text-align: left; }

            .dataTables_wrapper .dataTables_length {
                float: left;
            }
            .dataTables_wrapper .dataTables_filter {
                float: left;
                text-align: left;                
            }
            .horizontal-scroll {
                overflow-x:scroll ; 
                overflow-y: scroll;
            }
            .dataTables_wrapper .dt-buttons {
                float: right;
            }
        </style>

    </head>
    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>    

    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px;">
        <tbody>
            <tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>
                                                                    <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                        <li role="presentation" class="lstemplate active "><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%></a></li>                                                                                                                                                
                                                                    </ul>  
                                                                    <br />                                                                                                                                                                                                  
                                                                    <div class="container-fluid">
                                                                        <div class="row" style="background-color: #ffffff">
                                                                            <!--date control-->
                                                                            <div class="col-md-2">                                                                                                                                    
                                                                                <div class="form-group">                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startDt", SessionData.getLanguage())%></h5>
                                                                                    <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                        <span class="input-group-addon btncl" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                        <input type="text" class="form-control startDt" name="startDt" />                                                                                                                                                                                                                      
                                                                                    </div>                                                                                                                                                                                               
                                                                                    <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startdt.lbl1", SessionData.getLanguage())%></small>
                                                                                </div>

                                                                                <div class="form-group">                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.endDt", SessionData.getLanguage())%></h5>
                                                                                    <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                        <span class="input-group-addon btncl2" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                        <input type="text" class="form-control endDt" name="endDt" />                                                                                                                                                                                                                      
                                                                                    </div>                                                                                                                                                                                               
                                                                                    <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startdt.lbl2", SessionData.getLanguage())%></small>
                                                                                </div>                                                                                                                                                                                                                                     
                                                                                <button type="button" class="btn btn-default btnsearchanipn"><%= Languages.getString("jsp.admin.genericLabel.search", SessionData.getLanguage())%></button>                                                                                                                                                                
                                                                            </div>
                                                                            <!--batchs table-->
                                                                            <div class="col-md-3">
                                                                                <div class="tbllstbundlingdiv horizontal-scroll">                                                                                                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.batchs", SessionData.getLanguage())%></h5>                                                                                    
                                                                                    <table id="tbllstbundling" class="table table-hover text-muted">                                                                                                                                                                        
                                                                                        <thead>
                                                                                            <tr>                                                                                                                                                                        
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tbl1.col1", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tbl1.col2", SessionData.getLanguage())%></th>                                                                                                
                                                                                            </tr>
                                                                                        </thead>                                                                                                    
                                                                                    </table>
                                                                                    <br />
                                                                                    <div align="right">
                                                                                        <button type="button" onclick="decreasetbl1()" class="btn btn-default btn-sm decrebtn"><%= Languages.getString("jsp.admin.previous", SessionData.getLanguage())%></button>
                                                                                        <button type="button" onclick="incrementbtl1()" class="btn btn-default btn-sm increbtn"><%= Languages.getString("jsp.admin.next", SessionData.getLanguage())%></button>
                                                                                    </div>                                                                                    
                                                                                </div> 
                                                                            </div>                                                                                  
                                                                            <!--pins table-->          
                                                                            <div class="col-md-5">
                                                                                <div class="tblpinsdiv horizontal-scroll">                                                                                                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.pintbl", SessionData.getLanguage())%></h5>
                                                                                    <table id="tblpins" class="table table-hover text-muted">                                                                                                                                                                        
                                                                                        <thead>
                                                                                            <tr>                                                                                                                                                                        
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tbl2.col1", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tbl2.col2", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tbl2.col3", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tbl2.col4", SessionData.getLanguage())%></th>                                                                                                
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tbl2.col5", SessionData.getLanguage())%></th>                                                                                                
                                                                                            </tr>
                                                                                        </thead>                                                                                                    
                                                                                    </table>                                                                                    
                                                                                    <br />
                                                                                    <div align="right">                                                                                                                                                                                
                                                                                        <button type="button" onclick="decreasetbl2()" class="btn btn-default btn-sm decrebtnt2"><%= Languages.getString("jsp.admin.previous", SessionData.getLanguage())%></button>
                                                                                        <button type="button" onclick="incrementbtl2()" class="btn btn-default btn-sm increbtnt2"><%= Languages.getString("jsp.admin.next", SessionData.getLanguage())%></button>
                                                                                    </div>  

                                                                                </div> 
                                                                            </div>   
                                                                            <div class="col-md-2">
                                                                                <div class="divoptions horizontal-scroll">                                                                                                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.optins.title", SessionData.getLanguage())%></h5>

                                                                                    <div class="list-group">                                                                                           
                                                                                        <a href="#" class="list-group-item disabled"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt.m3", SessionData.getLanguage())%></a>
                                                                                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#cm-search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt4", SessionData.getLanguage())%></a>                                                                                        
                                                                                        <div class="hospy">
                                                                                            <a href="#" class="list-group-item disabled"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt.m1", SessionData.getLanguage())%></a>
                                                                                            <a class="list-group-item" style="cursor: pointer" onclick="$('.btnbk').attr('disabled', true);
                                                                                                    topsi = 5;
                                                                                                    vbulkOperType = 1;
                                                                                                    $('.bmt').text('<%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt1", SessionData.getLanguage())%>')" data-toggle="modal" data-target="#cm-bulkmodal"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt1", SessionData.getLanguage())%></a>
                                                                                            <a class="list-group-item" style="cursor: pointer" onclick="$('.btnbk').attr('disabled', true);
                                                                                                    topsi = 1;
                                                                                                    vbulkOperType = 2;
                                                                                                    $('.bmt').text('<%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt2", SessionData.getLanguage())%>')" data-toggle="modal" data-target="#cm-bulkmodal"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt2", SessionData.getLanguage())%></a>
                                                                                            <a class="list-group-item disabled"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt.m2", SessionData.getLanguage())%></a>
                                                                                            <a class="list-group-item" style="cursor: pointer" onclick="$('.btnbk').attr('disabled', true);
                                                                                                    vbulkOperType = 3;
                                                                                                    $('.bmt').text('<%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt3", SessionData.getLanguage())%>')" data-toggle="modal" data-target="#cm-bulkmodal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt3", SessionData.getLanguage())%></a>
                                                                                        </div>                                                                                       
                                                                                    </div>
                                                                                </div>                                                                                                                                                                                                                                                    
                                                                            </div>
                                                                        </div>                                                                                                                                                                                                                                                                                    
                                                                        <!--edit modal-->
                                                                        <div class="modal fade" id="cm-editModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper">
                                                                                <div class="modal-dialog vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.win.edit.title", SessionData.getLanguage())%></h4>
                                                                                        </div>
                                                                                        <div class="modal-body">                                                                                                        
                                                                                            <div class="alert alert-warning" role="alert">
                                                                                                <blockquote class="init-cm-msg">
                                                                                                    <p>
                                                                                                        <%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                    </p> <small><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tips4", SessionData.getLanguage())%></small>
                                                                                                </blockquote>
                                                                                            </div>
                                                                                            <div class="form-group">                                                                                                
                                                                                                <label><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.win.edit.lbl1", SessionData.getLanguage())%></label>                                                                                        
                                                                                                <select class="form-control cmseldc" id="cmseldc">                                                                                                                
                                                                                                    <option value="1"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.win.edit.s11", SessionData.getLanguage())%></option>
                                                                                                    <option value="5"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.win.edit.s12", SessionData.getLanguage())%></option>
                                                                                                </select>                                                                                                
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tips2", SessionData.getLanguage())%></small>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.win.edit.lbl2", SessionData.getLanguage())%></label>
                                                                                                <textarea placeholder="_" class="form-control" id="auditTextArea1" maxlength="256" style="resize: none; font-size: 16px; height: 150px; width: 100%" class="text"></textarea>                                                                                                                                                                 
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tips3", SessionData.getLanguage())%></small>
                                                                                            </div>                                                                                                                                                                                                                                                                                                                                                                
                                                                                        </div>
                                                                                        <div class="modal-footer">                                                                                                        
                                                                                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('textarea#auditTextArea2').val('');
                                                                                                    $('textarea#auditTextArea1').val('');"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                            <button type="button" class="btn btn-success btnupdatesku" onclick="onEditPin()" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div>
                                                                        <!--remove modal-->
                                                                        <div class="modal fade" id="cm-rmodal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper">
                                                                                <div class="modal-dialog vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.win.rem.title", SessionData.getLanguage())%></h4>
                                                                                        </div>
                                                                                        <div class="modal-body"> 
                                                                                            <div class="alert alert-warning" role="alert">
                                                                                                <blockquote class="init-cm-msg">
                                                                                                    <p>
                                                                                                        <%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                    </p> <small><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tips4", SessionData.getLanguage())%></small>
                                                                                                </blockquote>
                                                                                            </div>                                                                                                                                                                                    
                                                                                            <div class="form-group">
                                                                                                <label><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.win.edit.lbl2", SessionData.getLanguage())%></label>
                                                                                                <textarea placeholder="_" class="form-control" maxlength="256" id="auditTextArea2" style="resize: none; font-size: 16px; height: 150px; width: 100%" class="text"></textarea>                                                                                                                                                                 
                                                                                                <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tips3", SessionData.getLanguage())%></small>
                                                                                            </div>                                                                                                                                                                                                                                                                                                                                                                
                                                                                        </div>
                                                                                        <div class="modal-footer">                                                                                                        
                                                                                            <button type="button" class="btn btn-default" onclick="$('textarea#auditTextArea2').val('');
                                                                                                    $('textarea#auditTextArea1').val('');" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                            <button type="button" class="btn btn-danger btnrmp" onclick="onRemovePin()"  data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.Delete", SessionData.getLanguage())%></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div>
                                                                        <!--search a specify pin-->
                                                                        <div class="modal fade" id="cm-search" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper">
                                                                                <div class="modal-dialog modal-sm vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.opt4", SessionData.getLanguage())%></h4>
                                                                                        </div>
                                                                                        <div class="modal-body">                                                                                                                                                                                                                                                                                 
                                                                                            <div class="form-group">
                                                                                                <label>PIN</label>
                                                                                                <input type="text" class="form-control spin" placeholder="<%= Languages.getString("jsp.includes.menu.tools.pinmanagement.search.msg1", SessionData.getLanguage())%>">                                                                                                
                                                                                            </div>                                                                                                                                                                                                                                                                                                                                                                
                                                                                        </div>                                                                                        
                                                                                        <br />
                                                                                        <div class="modal-footer">                                                                                                                                                                                                    
                                                                                            <button type="button" onclick="findPinByMatch()" class="btn btn-default"  data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.search", SessionData.getLanguage())%></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div>
                                                                        <!--oper-multiple-->
                                                                        <div class="modal fade" id="cm-bulkmodal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper">
                                                                                <div class="modal-dialog vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title bmt" id="myModalLabel"></h4>
                                                                                        </div>
                                                                                        <div class="modal-body"> 
                                                                                            <div class="alert alert-warning" role="alert">
                                                                                                <blockquote class="init-cm-msg">
                                                                                                    <p>
                                                                                                        <%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.title", SessionData.getLanguage())%>                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                    </p> <small><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.tips4", SessionData.getLanguage())%></small>
                                                                                                </blockquote>
                                                                                            </div>                                                                                                                                                                                    
                                                                                            <div class="form-group">
                                                                                                <label><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.win.edit.lbl2", SessionData.getLanguage())%></label>
                                                                                                <textarea placeholder="_" class="form-control" maxlength="256" id="auditTextArea3" style="resize: none; font-size: 16px; height: 150px; width: 100%" class="text"></textarea>                                                                                                                                                                                                                                                                 
                                                                                            </div>                                                                                                                                                                                                                                                                                                                                                                
                                                                                        </div>
                                                                                        <div class="modal-footer">                                                                                                        
                                                                                            <button type="button" class="btn btn-default" onclick="$('textarea#auditTextArea3').val('');" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                            <button type="button" class="btn btn-success btnbk" onclick="bulkOperType()"  data-dismiss="modal"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.accept.title", SessionData.getLanguage())%></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div>
                                                                    </div>                                                                      
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>     
        </tbody>
    </table>
    <style type="text/css">
        .alignCenter { text-align: center; }
        .alignRight { text-align: right; }            
        .alignLeft { text-align: left; }


        .modal {
        }
        .vertical-alignment-helper {
            display:table;
            height: 100%;
            width: 100%;
        }
        .vertical-align-center {                
            display: table-cell;
            vertical-align: middle;
        }
        .modal-content {                
            width:inherit;
            height:inherit;               
            margin: 0 auto;
        }

        .modal.fade .modal-dialog {
            -webkit-transform: scale(0.1);
            -moz-transform: scale(0.1);
            -ms-transform: scale(0.1);
            transform: scale(0.1);
            top: 300px;
            opacity: 0;
            -webkit-transition: all 0.1s;
            -moz-transition: all 0.1s;
            transition: all 0.1s;
        }

        .modal.fade.in .modal-dialog {
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
            -webkit-transform: translate3d(0, -300px, 0);
            transform: translate3d(0, -300px, 0);
            opacity: 1;
        }
    </style>
</body>
<%@ include file="/includes/footer.jsp" %>
</html>
