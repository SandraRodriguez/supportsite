<%-- 
    Document   : sortableProducts
    Created on : Jul 23, 2018, 11:32:20 AM
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title><%=Languages.getString("sort.products.title", SessionData.getLanguage())%></title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>                 
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>                              

        <script src="<%=path%>/js/themes/vue.js" type="text/javascript" charset="utf-8"></script>        

        <script src="<%=path%>/js/vuelibs/Sortable.min.js" type="text/javascript" charset="utf-8"></script>        
        <script src="<%=path%>/js/vuelibs/vuedraggable.min.js" type="text/javascript" charset="utf-8"></script>        

        <link href="<%=path%>/css/jquery.sweet-modal.min.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="<%=path%>/js/jquery.sweet-modal.min.js"></script>        
    </head>

    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>

    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px;">
        <tbody>
            <tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("sort.products.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>                                                                                                                                                                                                                                                                   
                                                                    <div class="container-fluid" id="sort-products">                                                                                                                                                
                                                                        <div class="row" style="background-color: #ffffff">
                                                                            <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                                <li role="presentation" class="lstemplate active "><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("sort.products.title", SessionData.getLanguage())%></a></li>                                                                                                                                                
                                                                            </ul>  
                                                                            <br />                                                                                                                                                            
                                                                            <div class="text text-muted" style="margin-left: 1%">
                                                                                <strong>                                                                                                                                                                    
                                                                                    <small>{{breadcrumb}}</small>
                                                                                </strong>
                                                                            </div>

                                                                            <!--categories table-->
                                                                            <div class="col-md-2 text-muted">         
                                                                                <center>
                                                                                    <h4><%=Languages.getString("sort.products.categories.title", SessionData.getLanguage())%></h4>
                                                                                </center>
                                                                                <input v-model="searchCategory" type="text" class="form-control" aria-describedby="emailHelp" placeholder="<%=Languages.getString("sort.products.categories.placeholder", SessionData.getLanguage())%>"><br />                                                                                
                                                                                <div class="list-group card sbar">                                                                                                                                                         
                                                                                    <a style="cursor: pointer" v-for="item in categoriesComputed" @click="getCarriers(item)" class="list-group-item">{{item.description}}</a>                                                                                                                                                                                                                               
                                                                                </div>                                                                                                                                                                    
                                                                            </div>    

                                                                            <!--carriers table-->
                                                                            <div class="col-md-2 text-muted">         
                                                                                <div v-if="showCarriers">
                                                                                    <center>
                                                                                        <h4><%=Languages.getString("sort.products.carriers.title", SessionData.getLanguage())%></h4>
                                                                                    </center>
                                                                                    <input v-model="searchCarrier" type="text" class="form-control" aria-describedby="emailHelp" placeholder="<%=Languages.getString("sort.products.carriers.placeholder", SessionData.getLanguage())%>"><br />                                                                                
                                                                                    <div class="list-group card sbar">                                                                                        
                                                                                        <a style="cursor: pointer" v-for="item in carriersComputed" @click="getProducts(item)" class="list-group-item">{{item.name}}</a>                                                                                                                                                                                                                               
                                                                                    </div>
                                                                                </div>                                                                                
                                                                            </div> 

                                                                            <!-- products component-->
                                                                            <div class="col-md-4 text-muted">                                                                                
                                                                                <div v-if="showProducts">                                                                                    
                                                                                    <center>
                                                                                        <h4><%=Languages.getString("sort.products.products.title", SessionData.getLanguage())%></h4>
                                                                                    </center>                                                                                  
                                                                                    <blockquote class="init-cm-msg">
                                                                                        <p>
                                                                                            <%=Languages.getString("sort.products.products.lbl.l7", SessionData.getLanguage())%>
                                                                                        </p><small><%=Languages.getString("sort.products.products.lbl.l10", SessionData.getLanguage())%></small>
                                                                                    </blockquote>
                                                                                    <small>{{products.length}} <%=Languages.getString("sort.products.products.lbl.l9", SessionData.getLanguage())%></small>
                                                                                    <div class="list-group card sbar" style="height: 440px !important">                                                                                                                                                                                
                                                                                        <draggable v-model="searchProduct" :move="onMove" :options="dragOptions" :list="products">                                                                                            
                                                                                            <a style="cursor: pointer" v-for="(item, index) in products" :key="item.id" class="list-group-item"><span class="badge">{{item.productOrder}}</span> {{item.id}} - {{item.description}} </a>
                                                                                        </draggable>

                                                                                    </div>                                                                                                                                                                         
                                                                                </div>                                                                                       
                                                                            </div>

                                                                            <!-- options -->
                                                                            <div class="col-md-2 text-muted">                                                                                      
                                                                                <div v-if="products.length > 1">                                                                                              
                                                                                    <center>
                                                                                        <h4><%=Languages.getString("sort.products.options.title", SessionData.getLanguage())%></h4>
                                                                                    </center>                                                                                                                                                                        

                                                                                    <div class="form-group">                                                                                                
                                                                                        <label><%=Languages.getString("sort.products.products.lbl.l11", SessionData.getLanguage())%></label>                                                                                        
                                                                                        <select class="form-control" v-model="orderby" @change="getProductsOrderedBy()">                                                                                                                
                                                                                            <option>--</option>
                                                                                            <option value="amount"><%=Languages.getString("sort.products.products.lbl.l3", SessionData.getLanguage())%></option>
                                                                                            <option value="name"><%=Languages.getString("sort.products.products.lbl.l4", SessionData.getLanguage())%></option>
                                                                                            <option value="sku"><%=Languages.getString("sort.products.products.lbl.l8", SessionData.getLanguage())%></option>
                                                                                        </select>                                                                                                
                                                                                        <small class="form-text text-muted"><%=Languages.getString("sort.products.products.lbl.l12", SessionData.getLanguage())%></small>
                                                                                    </div>

                                                                                </div>

                                                                                <div v-if="showProducts && ismoved">   
                                                                                    <hr />
                                                                                    <button style="width: 100% !important" @click="updateProductPosition()"type="button" class="btn btn-success"><%=Languages.getString("sort.products.products.lbl.l5", SessionData.getLanguage())%> <span class="glyphicon glyphicon-save pull-left" aria-hidden="true"></span></button>
                                                                                    <br /><br />                                                                                     
                                                                                    <button style="width: 100% !important" @click="resetDragAction()"type="button" class="btn btn-default"><%=Languages.getString("sort.products.products.lbl.l6", SessionData.getLanguage())%> <span class="glyphicon glyphicon-chevron-left pull-left" aria-hidden="true"></span></button>                                                                                    
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                                                                                                                                                                                                                                                                                                                            
                                                                    </div>                                                                      
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>     
        </tbody>
    </table>
</body>
<script src="<%=path%>/js/sortableProducts.js" type="text/javascript"></script>        

<style type="text/css">
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;        
        border-radius: 5px;        
    }
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
    .sbar {        
        width: 100%;
        overflow-y: auto;
        height: 500px
    }
</style>
</html>