
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title><%=Languages.getString("sort.products.title", SessionData.getLanguage())%></title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>                 
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>                              

        <script src="<%=path%>/js/themes/vue.js" type="text/javascript" charset="utf-8"></script>        
        <script src="<%=path%>/js/themes/vue.js" type="text/javascript"></script>                                          
        <link rel="stylesheet" href="<%=path%>/css/element_ui.css"/>   
        <link href="<%=path%>/css/themes/element_ui_theme.css" rel="stylesheet" type="text/css"/>                
        <!--script src="<%=path%>/js/vuelibs/Sortable.min.js" type="text/javascript" charset="utf-8"></script>        
        <script src="<%=path%>/js/vuelibs/vuedraggable.min.js" type="text/javascript" charset="utf-8"></script-->        

        
        <script src="<%=path%>/js/themes/element_ui_en.js" type="text/javascript"></script>             
        <link href="<%=path%>/css/jquery.sweet-modal.min.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="<%=path%>/js/jquery.sweet-modal.min.js"></script>        
    </head>

    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>
<%
    String columnProvider = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.columnProvider", SessionData.getLanguage());
    String columnStatus = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.columnStatus", SessionData.getLanguage());
    String columnRegion = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.columnRegion", SessionData.getLanguage());
    String columnUser = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.columnUser", SessionData.getLanguage());
    String columnPass = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.columnPassword", SessionData.getLanguage());
    String btnNewCredential = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.addNew", SessionData.getLanguage());
    
    String btnDelete = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.delete", SessionData.getLanguage());
    String btnConfirm = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.confirm", SessionData.getLanguage());
    String btnEdit = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.edit", SessionData.getLanguage());

%>
    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px;">
        <tbody>
            <tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="center">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td align="center">                                                                                                                                                                                                                                                                   
                                                                    <div class="container-fluid" id="credentials-providers">                                                                                                                                                
                                                                        
    <div class="row" style="background-color: #ffffff">
        <ul class="nav nav-tabs">                                                                                                                                                                                                                 
            <li role="presentation" class="lstemplate active ">
                <a style="cursor: pointer" class="lstemplate active">
                    <img src="images/analysis_cube.png" border="0" width="22" height="22">  
                    <%=Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials", SessionData.getLanguage())%></a></li>                                                                                                                                                
        </ul>  
        <br />                                                                                                                                                            
        
        <template>
        <div >                      
          <el-table :data="providers" style="width: 100%" row-key="id" border default-expand-all v-if="viewCredentialsTable">            
            <el-table-column label="<%=columnProvider%>" width="250" >
                <template slot-scope="scope" v-if="scope.row.region == null">
                    <span style="float: right; color: #8492a6; font-size: 15px">{{scope.row.name}} ({{scope.row.providerId}})</span> 
                    <el-switch v-model="scope.row.status" @change="updateStatusProvider(scope.row)"
                        active-color="#13ce66" inactive-color="#ff4949"></el-switch>                    
                </template>
            </el-table-column>            
                          
            <el-table-column prop="region" label="<%=columnRegion%>" width="180"></el-table-column>
            <el-table-column prop="user" label="<%=columnUser%>" width="180"></el-table-column>
            <el-table-column prop="password" label="<%=columnPass%>" width="180"></el-table-column>
            <el-table-column align="center" width="180">
                <template slot-scope="scope" v-if="scope.row.region != null">
                    <el-button size="mini" @click="handleEdit(scope.$index, scope.row)"><%=btnEdit%></el-button>                    
                </template> 
            </el-table-column>           
            
            <el-table-column align="center" label="<%=columnStatus%>" width="180" >
                <template slot-scope="scope" v-if="scope.row.region != null">                                                                
                    <el-switch v-model="scope.row.statusDetail" @change="updateStatusDetailCredential(scope.row)"
                        active-color="#13ce66" inactive-color="#ff4949"></el-switch>
                </template>
            </el-table-column>
                       
            <el-pagination layout="prev, pager, next" :total="10"></el-pagination>
          </el-table>
            
        </div>
        <br />  
        <el-button type="success" @click="openNew"><%=btnNewCredential%></el-button>
            
            <el-dialog title="" :visible.sync="dialogVisible" width="30%" >
                
                <el-form label-width="90px" :model="formCredentials" v-bind:rules="rules" ref="credentialForm">
                    <el-form-item label="<%=columnProvider%>" prop="providerId">            
                        <!--span v-if="disabledProvidersControl" style="color: #8492a6; font-size: 15px">{{ formCredentials.providerName }}</span-->                        
                        <el-select v-model="formCredentials.providerId" placeholder="" filterable :disabled="disabledProvidersControl">
                            <el-option v-for="item in allProviders" :key="item.providerId" :label="item.name" :value="item.providerId">
                                <span style="float: left">{{ item.name }}</span>                                
                            </el-option>
                        </el-select>                        
                    </el-form-item>                    
                    <el-form-item label="<%=columnUser%>" prop="user">
                      <el-input v-model="formCredentials.user"></el-input>
                    </el-form-item>
                    <el-form-item label="<%=columnPass%>" prop="password">
                      <el-input v-model="formCredentials.password"></el-input>
                    </el-form-item>                    
                </el-form>
                
                <span slot="footer" class="dialog-footer">
                    <!--el-button @click="deleteCredentialDetail"><%=btnDelete%></el-button-->
                    <el-button type="primary" @click="upateInsertCredentialDetail"><%=btnConfirm%></el-button>
                </span>
            </el-dialog>
            
        </template> 
        
    </div>                                                                                                                                                                                                                                                                                                                                                            
                                                                    </div>                                                                      
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>     
        </tbody>
    </table>
</body>
<script src="<%=path%>/js/credentialsByProvider.js" type="text/javascript"></script>        

<style type="text/css">
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;        
        border-radius: 5px;        
    }
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
    .sbar {        
        width: 100%;
        overflow-y: auto;
        height: 500px
    }
</style>
</html>