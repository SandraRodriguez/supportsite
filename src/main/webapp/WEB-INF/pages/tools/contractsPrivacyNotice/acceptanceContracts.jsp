<%-- 
    Document   : acceptanceContracts
    Created on : 06-sep-2017, 9:32:02
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 16;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<html>
    <head>        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>        
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                        
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>     

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.5.2/dom-to-image.min.js"></script>        
        <script type="text/javascript" src="<%=path%>/js/html2canvas.min.js"></script>                                                

        <script>

            function dateToYMD(date) {
                var d = date.getDate();
                var m = date.getMonth() + 1;
                var y = date.getFullYear();
                return (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + '' + y;
            }
            var fromDate = null;
            var toDate = null;
            var status = null;
            var id = '';
            $(document).ready(function () {
                $(".logtx").hide();
                $(".btnsearch").prop('disabled', true);
                $('#tbllstcontracts').DataTable();
                $(".divErrMessage").hide();
                $(".divSuccessMessage").hide();
                $(".div-edit-status").hide();
                $(".dinamyContract").hide();

                fromDate = dateToYMD(new Date());
                toDate = dateToYMD(new Date());

                findLastAcceptanceContractsByQueryRangue();
                $(".tbllstcontracts").hide();
                $(".btnReportDownloader").hide();
                $('input[name="daterange"]').daterangepicker({
                    "showDropdowns": true,
                    "showWeekNumbers": true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>',
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oc", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    endDate: moment(),
                    maxDate: moment()
                }, function (start, end, label) {
                    fromDate = start.format('DD/MM/YYYY');
                    toDate = end.format('DD/MM/YYYY');
                    if ((fromDate === null && toDate === null) || status == null || status == 'null') {
                        $(".btnsearch").prop('disabled', false);
                    }
                });
                $(".btnsearch").click(function () {
                    if (fromDate == 'null' || toDate == 'null') {
                        $('.lblrd').css('background-color', 'rgba(219, 126, 126, 0.2)');
                        setTimeout(function () {
                            $('.lblrd').css('background-color', '');
                        }, 500);
                    } else {
                        findAcceptanceContractsByQueryRangue();                        
                        $(".alrt-dd").hide();
                    }
                });
                $(".btnReportDownloader").click(function () {
//                    window.location.href = '/support/tools/bundlingActivation/downloadReport?startDt=' + fromDate + '&endDt=' + toDate + '&statusCode=' + status;
                    var url_download = '/support/tools/bundlingActivation/downloadReport?startDt=' + fromDate + '&endDt=' + toDate + '&statusCode=' + status;

                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'GET',
                        timeout: 10000,
                        url: url_download,
                        async: true
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        var csvContent = data;
                        var pom = document.createElement('a');
                        var blob = new Blob([csvContent], {type: 'text/csv;charset=UTF-8;'});
                        var url = URL.createObjectURL(blob);
                        pom.href = url;
                        pom.setAttribute('download', 'bundlingActivation.csv');
                        document.body.appendChild(pom);
                        pom.click();
                        setTimeout(function () {
                            document.body.removeChild(pom);
                            window.URL.revokeObjectURL(url);
                        }, 100);
                    });
                });
                $(".btnOkUpdate").click(function () {
                    onEditHoldBackRequest();
                });
                
                $(".btnDownloadPdf").click(function () {
                    window.open('/support/reports/acceptanceContracts/pdf?id=' + id, 'blank', 'location = yes, height = 900, width = 1200, scrollbars = yes, status = yes');
                });                                

                $(".pre-main").click(function () {
                    $(".div-main").show();
                    $(".tbllstcontracts").show();

                    $(".dinamyContract").hide();
                });

                $(".btnCancelUpdate").click(function () {
                    $(".div-main").show();
                    $(".tbllstcontracts").show();
                    $(".div-edit-status").hide();
                });
                $("#activlink").click(function (event) {
                    event.preventDefault();
                });
                $('input[type=radio][name=radioStatus]').change(function () {
                    var myRadio = $('input[name=radioStatus]');
                    status = myRadio.filter(':checked').val();
                    if (fromDate !== "" & toDate !== "" && status !== '' && status !== 'undefined' && status !== 'null') {
                        $(".btnsearch").prop('disabled', false);
                    }
                });
            });
            function findLastAcceptanceContractsByQueryRangue() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    timeout: 10000,
                    url: '/support/reports/acceptanceContracts/queryLast',
                    async: true
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function findAcceptanceContractsByQueryRangue() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    timeout: 10000,
                    data: {
                        startDt: fromDate,
                        endDt: toDate
                    },
                    url: '/support/reports/acceptanceContracts/queryRangue',
                    async: true
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function findAcceptanceContractsById() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    timeout: 10000,
                    data: {
                        id: id
                    },
                    url: '/support/reports/acceptanceContracts/queryById',
                    async: true
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    document.getElementById("dinamyContract").innerHTML = data.content;                                        
                });
            }

            function onSuccessResponse(data) {
                if (data.length === 0) {
                    $(".tbllstcontracts").hide();
                    $(".btnReportDownloader").hide();
                    $('#tbllstcontracts').dataTable().fnClearTable();
                    $(".divErrMessage").show();
                    $(".ui-datepicker-trigger").css("vertical-align", "top");
                } else {
                    $(".btnReportDownloader").show();
                    $(".tbllstcontracts").show();
                    $(".divErrMessage").hide();
                    $('#tbllstcontracts').dataTable().fnDestroy();
                    buildTable(data);
                }
            }

            function  buildTable(data) {
                $('#tbllstcontracts').DataTable({
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
//                    "bInfo": false,
//                    "bDestroy": true,
                    "paging": true,
                    "iDisplayLength": 10,
                    searching: true,
                    aoColumns: [
                        {"mData": 'entityId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "80px"},
                        {"mData": 'legal_businessname', sDefaultContent: "--", sClass: "alignCenter", sWidth: "140px"},
                        {mRender: function (data, type, row) {
                                return "<span class='glyphicon glyphicon-calendar' aria-hidden='true'></span> " + row.acceptanceDt;
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "120px"
                        },
                        {mRender: function (data, type, row) {
                                return "<span class='glyphicon glyphicon-user' aria-hidden='true'></span> " + row.logonId;
                            }, sDefaultContent: "--", sClass: "alignCenter"
                        },
                        {mRender: function (data, type, row) {
                                return "<span class='glyphicon glyphicon-search' aria-hidden='true'></span> <a href='javascript:;'>" + "Pre-View" + "</a>";
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "60px"
                        }
                    ],
                    "aaSorting": [[1, "desc"]],
//                    "sPaginationType": "full_numbers",
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("noticeAcceptanceContracts.sZeroRecords")}',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    },
                    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).on('click', function () {
                            id = aData.id;
                            previewContract(aData);
                        });
                    }
                });
            }

            function showLogTransaction(data) {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    data: {
                        holdBackTrxId: data.holdBackTrxId
                    },
                    url: '/support/tools/bundlingActivation/getHistoryTransaction',
                    async: true,
                    complete: function () {
                        $('.ajax-loading').hide(1);
                    }
                });
                result.success(function (data, status, headers, config) {
                    $("textarea#txtarealog").val(data.transText);
                    $('.txtProviderResponse').text(data.provider_response);
                });
            }

            function previewContract(object) {
                $(".div-main").hide();
                $(".tbllstcontracts").hide();

                $(".dinamyContract").show();

                findAcceptanceContractsById();
            }

            function onEditHoldBackRequest() {
                var stUpdate = $('input[name=radioStatusUpdate]');
                var vstatus = stUpdate.filter(':checked').val();
                $(".ajax-loading").show();
                $.ajax({
                    type: 'POST',
                    data: {
                        id: id,
                        statusCode: vstatus
                    },
                    url: '/support/tools/bundlingActivation/updateHoldBackRequestStatus',
                    async: true,
                    complete: function () {
                        $('.ajax-loading').hide(1);
                        $('.divSuccessMessage').show(1);
                        $(".div-edit-status").hide();
                        setTimeout(function () {
                            $('.divSuccessMessage').hide(1);
                            findAcceptanceContractsByQueryRangue();
                            $(".div-main").show();
                            $(".tbllstcontracts").show();
                        }, 1500);
                    }
                });
            }
        </script>        
        <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
        <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
        <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>             
    </head>
    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>        
    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" width="1200" style="margin-top: 1%; margin-left: 1%">
        <tbody><tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("noticeAcceptanceContracts.label", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td bgcolor="#7B9EBD" width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table class="table-hover" cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table table-hover">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>
                                                                    <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                        <li role="presentation" class="lstemplate active"><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("noticeAcceptanceContracts.label", SessionData.getLanguage())%></a></li>                                                                                                                                                
                                                                    </ul>                                                                       
                                                                    <br />
                                                                    <div class="div-main">                                                                                                                                            
                                                                        <table width="20%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="lblrd">
                                                                                        <div class="form-group">                                                                                                  
                                                                                            <label class="text-muted"><%= Languages.getString("jsp.tools.bundling.date.range", SessionData.getLanguage())%></label>                                                                                        
                                                                                            <div class="input-group input-group-sm">                                                                        
                                                                                                <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                                                                                <input type="text" class="form-control" name="daterange" />                                                                    
                                                                                            </div>                                                                                                                                                                                           
                                                                                        </div>
                                                                                    </td>                                                                                                                                                                                                                                                     
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>

                                                                        <ul class="pager">
                                                                            <li class="previous btnsearch" id="btnsearch" style="cursor:pointer"><a><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.search", SessionData.getLanguage())%></a></li>                                                                                                                                                        
                                                                        </ul>
                                                                        <hr />                                                                        
                                                                        <div class="alert alert-warning alert-dismissible alrt-dd" role="alert">
                                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> <%= Languages.getString("jsp.tools.bundling.temporal.information", SessionData.getLanguage())%>
                                                                        </div>
                                                                    </div>

                                                                    <div class="div-edit-status">                                                                                                                                            
                                                                        <table width="50%">
                                                                            <tbody>
                                                                                <tr>                                                                                                                                                                   
                                                                                    <td>
                                                                                        <div class="form-group">
                                                                                            <label class="text-muted"><%= Languages.getString("jsp.tools.bundling.select.status", SessionData.getLanguage())%></label><br />                                                                                                                     
                                                                                            <input type="radio" class="updateComplete" name="radioStatusUpdate" value="COMPLETED" /> <span class="label label-success"><%= Languages.getString("jsp.tools.bundling.status.label.1", SessionData.getLanguage())%></span>                                                                        
                                                                                            <input type="radio" class="updatePending" name="radioStatusUpdate" value="PENDING" /> <span class="label label-warning"><%= Languages.getString("jsp.tools.bundling.status.label.2", SessionData.getLanguage())%></span>                                                                        
                                                                                            <input type="radio" class="updateUnavailable" name="radioStatusUpdate" value="UNAVAILABLE" /> <span class="label label-danger"><%= Languages.getString("jsp.tools.bundling.status.label.3", SessionData.getLanguage())%></span>                                                                        
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>                                                                                
                                                                            </tbody>
                                                                        </table>
                                                                        <hr />
                                                                        <button type="button" class="btn btn-default btnOkUpdate"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <%= Languages.getString("jsp.tools.bundling.update.status", SessionData.getLanguage())%></button>                                                                    
                                                                        <button type="button" class="btn btn-danger btnCancelUpdate"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.cancel", SessionData.getLanguage())%></button>                                                                                                                                            
                                                                        <div class="logtx">                                                                                                                                                    
                                                                            <br />
                                                                            <b><p class="txtProviderResponse" style="font: bold"></p></b>
                                                                            <textarea id="txtarealog" class="txtarealog" style="background-color: #ececec; color: #010101; border: none; resize: none; height: 400px; width: 100%; font-family: 'Courier New'" class="text" disabled="true">                                                                            
                                                                            </textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="divErrMessage">
                                                                        <div class="alert alert-danger" role="alert">
                                                                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                                            <p id="errMessage" style="display:inline"><%= Languages.getString("noticeAcceptanceContracts.sZeroRecords", SessionData.getLanguage())%></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="divSuccessMessage">                                                                        
                                                                        <div class="alert alert-success" role="alert">
                                                                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>                                                                                
                                                                            <p id="errMessage" style="display:inline"><%= Languages.getString("jsp.tools.bundling.success.message", SessionData.getLanguage())%></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tbllstcontracts">
                                                                        <table id="tbllstcontracts" class="table table-hover">                                                                            
                                                                            <thead>
                                                                                <tr>                                                                                    
                                                                                    <th><%= Languages.getString("noticeAcceptanceContracts.tbl.col1", SessionData.getLanguage())%></th>
                                                                                    <th><%= Languages.getString("noticeAcceptanceContracts.tbl.col4", SessionData.getLanguage())%></th>
                                                                                    <th><%= Languages.getString("noticeAcceptanceContracts.tbl.col2", SessionData.getLanguage())%></th>
                                                                                    <th><%= Languages.getString("noticeAcceptanceContracts.tbl.col3", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th><%= Languages.getString("noticeAcceptanceContracts.tbl.col5", SessionData.getLanguage())%></th>                                                                                    
                                                                                </tr>
                                                                            </thead>                                                                                                    
                                                                        </table>
                                                                    </div>   

                                                                    <!--contract content-->
                                                                    <div class="dinamyContract">
                                                                        <ul class="pager">                                                                            
                                                                            <li class="previous pre-main" id="" style="cursor:pointer"><a><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.search", SessionData.getLanguage())%></a></li>                                                                            
                                                                            <li class="next btnDownloadPdf" style="cursor:pointer"><a><span class="glyphicon glyphicon-save" aria-hidden="true"></span> <%= Languages.getString("noticeAcceptanceContracts.downloadPdf", SessionData.getLanguage())%></a></li>                                                                            
                                                                        </ul>
                                                                        <hr />
                                                                        <div id="dinamyContract" data-html2canvas-ignore="true">

                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody></table>
                </td>
            </tr>     
        </tbody>
    </table>
</body>      
<%@ include file="/includes/footer.jsp" %>
</html>