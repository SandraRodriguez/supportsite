<%-- 
    Document   : contractsPrivacyNotice
    Created on : 17-abr-2017, 17:26:03
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 16;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>
<html>
    <head>          
        <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        <link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>        
        <!--<link href="<%=path%>/css/summernote.css" rel="stylesheet" type="text/css"/>-->
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>

        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                

        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>                         
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>
        <!--<script type="text/javascript" src="<%=path%>/includes/_lib/summernote.js"></script>-->                                 
        <script type="text/javascript" src="<%=path%>/includes/_lib/moment.js"></script>        
        <script type="text/javascript" src="<%=path%>/includes/_lib/tinymce/tinymce.min.js"></script>                
    </head>
    <body>        
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>

    <div class="page" style="margin-top: 2%">
        <div class="page-content container-fluid">                                      
            <table style="background-color: #fefefe">       
                <tr>                                                    
                <table width="150px" style="background-color: #fefefe">       
                    <tr>
                        <td>
                            <div class="list-group">                    
                                <a class="list-group-item disabled"><%=Languages.getString("jsp.admin.tools.smsInventory.action", SessionData.getLanguage())%></a>                                                   
                                <a href="#" class="list-group-item add-confg"><%=Languages.getString("jsp.admin.pincache.newtemplate", SessionData.getLanguage())%> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>                                                                      
                                <a href="#" class="list-group-item onEdit previewbtn"><%=Languages.getString("jsp.tools.notice.contracts.preview", SessionData.getLanguage())%> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></a>                                                                      
                                <a href="#" class="list-group-item cancelpreviewbtn"><%=Languages.getString("jsp.tools.notice.contracts.cancelpreview", SessionData.getLanguage())%> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>                                                                      
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="onEdit">
                                <div class="list-group">                    
                                    <a class="list-group-item disabled"><%=Languages.getString("jsp.tools.notice.contracts.objmapper", SessionData.getLanguage())%></a>                                      
                                    <select id="objtemplate" class="form-control objtemplate"></select>                         
                                </div>   
                            </div> 
                        </td>
                    </tr>
                </table>                                    
                </tr>
                <tr>
                <table width="1600px" style="background-color: #fefefe; margin-top: 1%">       
                    <tr>
                        <td>                                        
                            <div class="page-header lstAllTemplate">
                                <h3><%=Languages.getString("jsp.tools.notice.contracts.link", SessionData.getLanguage())%> <small><%=Languages.getString("jsp.tools.notice.contracts.add", SessionData.getLanguage())%></small></h3>
                            </div>
                            <!--on list all templates-->
                            <div class="lstAllTemplate">
                                <ul class="nav nav-tabs">                                                                 
                                    <li role="presentation" class="lstemplate"><a style="cursor: pointer" class="lstemplate" ><%=Languages.getString("jsp.tools.notice.contracts.lstemplates", SessionData.getLanguage())%> <span class="badge"></span></a></li>
<li role="presentation" class="lshelp"><a style="cursor: help" class="lshelp"><%=Languages.getString("jsp.includes.menu.help", SessionData.getLanguage())%> <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></li>                                    
                                </ul>
                                <br />
                                <jsp:include page="/WEB-INF/pages/tools/contractsPrivacyNotice/listTemplatesContracts.jsp"></jsp:include>                                                                              
                                </div>                                                                   
                                <!--on add new tempplate-->
                                <div id="onAddNewConfig" class="text-muted onAddNewConfig">                            
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><%=Languages.getString("jsp.admin.pincache.createtemplate", SessionData.getLanguage())%></div>
                                    <div class="panel-body">                                    
                                        <div class="form-group">
                                            <label class="control-label col-md-2"><%=Languages.getString("jsp.tools.notice.contracts.templateName", SessionData.getLanguage())%></label>                                                
                                            <div class="col-md-4">
                                                <input minlength="5" class="form-control configName" id="configName">
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="form-group">
                                            <label class="control-label col-md-2"><%=Languages.getString("jsp.tools.notice.contracts.description", SessionData.getLanguage())%></label>                                                
                                            <div class="col-md-10">          
                                                <textarea id="txtaddDescription" class="form-control description" id="description" style="height: 150px"></textarea>
                                            </div>
                                        </div>                                                      
                                    </div>
                                    <div class="panel-footer">
                                        <button type="button" class="btn btn-default btAddConf" id="btAddConf" onclick="onAddNewConfg()"><%=Languages.getString("jsp.tools.notice.contracts.save", SessionData.getLanguage())%> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
                                        <button type="button" class="btn btn-danger" onclick="doInit()"><%=Languages.getString("jsp.tools.notice.contracts.cancel", SessionData.getLanguage())%> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    </div>
                                </div>            
                            </div>  
                            <!--on edit template-->
                            <div class="onEdit">                                                                
                                <div class="form-group">
                                    <label class="text-muted"><%=Languages.getString("jsp.tools.notice.contracts.templateName", SessionData.getLanguage())%></label>
                                    <input type="text" class="form-control configNameUpdate"/>                                                                                                                       
                                </div>                

                                <div class="form-group">
                                    <label class="text-muted"><%=Languages.getString("jsp.tools.notice.contracts.status", SessionData.getLanguage())%> - <span class="label label-default spstatus"></span></label>                                                                                
                                    <input type="checkbox" class="checkbox checkbox-inline changestatus"/>
                                </div> 

                                <label class="text-muted"><%=Languages.getString("jsp.tools.notice.contracts.content.summer", SessionData.getLanguage())%></label>                                                                                            
                                <textarea id="contemplate"></textarea>
                                <div class="form-group">
                                    <label class="text-muted"><%=Languages.getString("jsp.tools.notice.contracts.description", SessionData.getLanguage())%></label>
                                    <textarea id="descriptionUpdate" class="form-control descriptionUpdate" style="height: 100px"></textarea>
                                </div>   

                                <div class="onEdit">
                                    <hr />
                                    <button type="button" class="btn btn-default btEditConf" id="btEditConf" onclick="onUpdateTemplate()"><%=Languages.getString("jsp.tools.notice.contracts.update", SessionData.getLanguage())%> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
                                    <button type="button" class="btn btn-danger" onclick="doInit()"><%=Languages.getString("jsp.tools.notice.contracts.cancel", SessionData.getLanguage())%> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                </div>  
                            </div>

                            <div class="onPreview">

                            </div>

                        </td>                                                                                

                    </tr>             
                </table>
                </tr>                    
            </table>
        </div>
    </div>
    <script>
        function getEditTxt() {
            return '${Languages.getString("jsp.admin.tools.edit")}';
        }
        function getStateEnableTxt() {
            return '${Languages.getString("jsp.tools.notice.contracts.enable")}';
        }
        function getStateDisableTxt() {
            return '${Languages.getString("jsp.tools.notice.contracts.disable")}';
        }
        function getVLang() {
            return {
                "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                        " _START_ - _END_ " +
                        '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                        " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                        '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                "sInfoFiltered": "",
                "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                "sZeroRecords": '${Languages.getString("jsp.tools.notice.contracts.notemplate")}',
                "oPaginate": {
                    "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                    "sNext": '${Languages.getString("jsp.admin.next")}',
                    "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                    "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                }
            };
        }
        function getVTinyContent() {
            return tinyMCE.activeEditor.getContent();
        }

        function innerContent(content) {
            tinyMCE.activeEditor.execCommand('mceInsertContent', false, content);
        }
        function doPreview() {
            tinyMCE.activeEditor.execCommand('mcePreview');
        }
        function setVContent(content) {
            tinyMCE.activeEditor.setContent(content);
        }

        function getSLang() {
            rlang = '${SessionData.getLanguage()}';
            if (rlang === 'english') {
                return 'en';
            } else {
                return 'es';
            }
        }

        var voptions = {
            selector: "#contemplate",
            height: 500,
            theme: 'modern',
            language: getSLang(),
            plugins: [
                'advlist autolink lists link charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image print preview media | forecolor backcolor',
            image_advtab: true,
            plugin_preview_width: "1200",
            plugin_preview_height: "800",
            templates: [
                {
                    title: '${Languages.getString("jsp.tools.notice.contracts.template.title")}',
                    description: '${Languages.getString("jsp.tools.notice.contracts.template.description")}',
                    url: window.location.protocol + "//" + window.location.hostname + ':' + window.location.port + '/support/admin/templates/temp_mexico.jsp'
                }
            ]
        };

        tinyMCE.init(voptions);
    </script>
    <script type="text/javascript" src="<%=path%>/js/contractPrivacyNotice.js"></script>
    <hr />
</body>
<%@ include file="/includes/footer.jsp" %>
</html>
