<%-- 
    Document   : listTemplatesContracts
    Created on : 19-abr-2017, 17:03:30
    Author     : janez
--%>
<%@page import="com.debisys.languages.Languages"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<!--templates page-->
<div class="divtemplates">
    <div style="background-color: #fefefe; margin-left: 1%; margin-right: 1%">
        <table id="tblTemplates" class="table table-hover tblTemplates" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><%=Languages.getString("jsp.tools.notice.contracts.templateName", SessionData.getLanguage())%></th>  
                    <th><%=Languages.getString("jsp.tools.notice.contracts.createDt", SessionData.getLanguage())%></th>
                    <th><%=Languages.getString("jsp.tools.notice.contracts.lastUpdate", SessionData.getLanguage())%></th>       
                    <th><%=Languages.getString("jsp.tools.notice.contracts.status", SessionData.getLanguage())%></th>
                    <th><%=Languages.getString("jsp.tools.notice.contracts.description", SessionData.getLanguage())%></th>                                                                                                          
                </tr>
            </thead>
        </table>  
    </div>                             
</div>
<!--help page-->
<div class="divhelp">    
    <h4 class="text-success"><%=Languages.getString("jsp.tools.notice.help.p1", SessionData.getLanguage())%></h4>
    <ol>
        <li>
            <p><%=Languages.getString("jsp.tools.notice.help.p2", SessionData.getLanguage())%></p>
        </li>
        <li>
            <p><%=Languages.getString("jsp.tools.notice.help.p3", SessionData.getLanguage())%></p>
        </li>
        <li>
            <p><%=Languages.getString("jsp.tools.notice.help.p4", SessionData.getLanguage())%></p>
        </li>
        <li>
            <p><%=Languages.getString("jsp.tools.notice.help.p5", SessionData.getLanguage())%></p>
        </li>                   
        <li>
            <p><%=Languages.getString("jsp.tools.notice.help.p6", SessionData.getLanguage())%></p>
            <ol style="list-style-type: lower-alpha;">
                <li>
		<p><%=Languages.getString("jsp.tools.notice.help.p7", SessionData.getLanguage())%></p>
                </li>
                <li>
                    <p><%=Languages.getString("jsp.tools.notice.help.p8", SessionData.getLanguage())%></p>                    
                </li>
            </ol>                        
            <p><%=Languages.getString("jsp.tools.notice.help.p9", SessionData.getLanguage())%></p>
        </li>
    </ol>  
</div>