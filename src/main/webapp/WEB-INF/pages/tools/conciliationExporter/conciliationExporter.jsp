<%-- 
    Document   : conciliationExporter
    Created on : May 22, 2018, 4:05:26 PM
    Author     : fernandob
--%>

<%@page import="com.debisys.languages.Languages"%>
<%@ include file="/WEB-INF/jspf/jstlIncludes.jspf" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
    int section = 9;
    int section_page = 37;
    String path = request.getContextPath();
%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<c:import url="/WEB-INF/pages/header.jsp" />

<%@ include file="/WEB-INF/jspf/admin/tools/conciliationExporter/deleteRecordConfimForm.jspf" %>                                                                                            
<%@ include file="/WEB-INF/jspf/admin/tools/conciliationExporter/errorInfoForm.jspf" %>
<%@ include file="/WEB-INF/jspf/admin/tools/conciliationExporter/exportTaskForm.jspf" %>
<%@ include file="/WEB-INF/jspf/admin/tools/conciliationExporter/retrieverForm.jspf" %>
<%@ include file="/WEB-INF/jspf/admin/tools/conciliationExporter/fileExporterForm.jspf" %>
<!-- Modal to update file transfer -->
<%@ include file="/WEB-INF/jspf/admin/tools/conciliationExporter/fileTransferModal.jspf" %>

<link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
<link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>  
<link rel="stylesheet" type="text/css" href="<%=path%>/css/dataTables/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/dataTables/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/admin/transactions/transactions.css">
<link href="<%=path%>/css/admin/tools/conciliationExporter/conciliationExporter.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/toastr/toastr.css" />
<%@ include file="/WEB-INF/jspf/admin/tools/conciliationExporter/conciliationExporterJavaScript.jspf" %>

<%
    // Mthod defined in conciliationExporterJavaScript.jspf
    setJavaScriptGlobals(out, SessionData.getLanguage());
%>

<center>
    <div class="ajax-loading">
        <div></div>
    </div>
</center> 

<table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" width="1200" style="margin-top: 1%; margin-left: 1%">
    <tbody>
        <tr>
            <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
            <td class="formAreaTitle" width="4000">${util:langString(Languages, "jsp.tools.conciliationExporter.toolTitle")}</td>
            <td height="20"><img src="images/top_right_blue.gif"></td>
        </tr>
        <tr>
            <td colspan="3">
                <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                    <tbody>                  
                        <tr>
                            <td bgcolor="#7B9EBD" width="1"><img src="images/trans.gif" width="1"></td>
                            <td bgcolor="#ffffff" valign="top" align="center">
                                <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                    <tbody><tr>
                                            <td>
                                                <table class="table">
                                                    <tbody>                                                     
                                                        <tr>                                                           
                                                            <td>
                                                                <div class='container-fluid'>
                                                                    <div class ="row row-content">
                                                                        <div id="conciliationExporterColContainer" class="col-12" style="color:#000;background-color: #FFF">
                                                                            <ul class="nav nav-tabs" id="exporterTabs" role="tablist">
                                                                                <li role="presentation" class="active">
                                                                                    <a id="exportTasksTab" data-toggle="tab" href="#exportTasksTabContent" role="tab" aria-controls="exportTasks">${util:langString(Languages, "jsp.tools.conciliationExporter.exportTask.tabTitle")}</a>
                                                                                </li>
                                                                                <li role="presentation">
                                                                                    <a id="retrieversTab" data-toggle="tab" href="#retrieversTabContent" role="tab" aria-controls="retrievers">${util:langString(Languages, "jsp.tools.conciliationExporter.retriever.tabTitle")}</a>
                                                                                </li>
                                                                                <li role="presentation">
                                                                                    <a id="fileExportersTab" data-toggle="tab" href="#fileExportersTabContent" role="tab" aria-controls="fileExporters">${util:langString(Languages, "jsp.tools.conciliationExporter.fileExporter.tabTitle")}</a>
                                                                                </li>
                                                                                <li role="presentation">
                                                                                    <a  id="fileTransferTasksTab" data-toggle="tab" href="#fileTransferTasksTabContent" role="tab" aria-controls="fileTransferTasks">${util:langString(Languages, "jsp.tools.conciliationExporter.fileTransferTask.tabTitle")}</a>
                                                                                </li>
                                                                            </ul>
                                                                            <div class="tab-content" id="exporterTabContent">
                                                                                <div class="tab-pane active display cell-border select" id="exportTasksTabContent" role="tabpanel">
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading">
                                                                                            <div class="btn-group" role="group" aria-label="...">
                                                                                                <button id="btnAddExportTask" type="button" class="btn btn-default">${util:langString(Languages, "jsp.tools.conciliationExporter.exportTask.addExportTaskBtnLabel")}</button>
                                                                                            </div>                                                                                            
                                                                                        </div>                                                                                        
                                                                                        <div class="panel-body">
                                                                                            <table id="exportTasksTable" class="display">
                                                                                            </table>              
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tab-pane" id="retrieversTabContent" role="tabpanel">
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading">
                                                                                            <div class="btn-group" role="group" aria-label="...">
                                                                                                <button id="btnAddRetriever" type="button" class="btn btn-default">${util:langString(Languages, "jsp.tools.conciliationExporter.retriever.addRetrieverBtnLabel")}</button>
                                                                                            </div>                                                                                            
                                                                                        </div>                                                                                        
                                                                                        <div class="panel-body">
                                                                                            <table id="retrieversTable" class="display">
                                                                                            </table>                                                                                
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tab-pane" id="fileExportersTabContent" role="tabpanel" aria-labelledby="fileExportersTab">
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading">
                                                                                            <div class="btn-group" role="group" aria-label="...">
                                                                                                <button id="btnAddExporter" type="button" class="btn btn-default">${util:langString(Languages, "jsp.tools.conciliationExporter.fileExporter.addFileExporterBtnLabel")}</button>
                                                                                            </div>                                                                                            
                                                                                        </div>
                                                                                        <div class="panel-body">
                                                                                            <table id="fileExportersTable" class="table table-striped table-bordered table-hover table-condensed">
                                                                                            </table>                                                                                
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tab-pane table-wrapper" id="fileTransferTasksTabContent" role="tabpanel" aria-labelledby="fileTransferTasksTab">
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading">
                                                                                            <div class="btn-group" role="group" aria-label="...">
                                                                                                <button id="btnUpdateFileTransfer" type="button" class="btn btn-default" onclick="fileTransferTasks.showModal()">${util:langString(Languages, "jsp.tools.conciliationExporter.fileTransferTask.updateFileTransferBtnLabel")}</button>
                                                                                            </div>                                                                                            
                                                                                        </div>
                                                                                        <div class="panel-body">
                                                                                            <table id="fileTransferTasksTable" class="display">
                                                                                            </table>                                                                                
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>                                                                    
                                                                </div>                                                                                                                                          
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                            <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                        </tr>
                        <tr>
                            <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>     
    </tbody>
</table>

<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/jquery.validate.min.js"></script>
<script type="text/javascript" charset="utf8" src="<%=path%>/includes/toastr/toastr.js"></script>
<script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="js/regexpValidationFunctions.js" type="text/javascript"></script>
<script src="js/admin/tools/conciliationExporter/conciliationExporterHeader.js" type="text/javascript"></script>
<script src="js/admin/tools/conciliationExporter/FileTransferTasks.js" type="text/javascript"></script>
<script src="js/admin/tools/conciliationExporter/FileExporters.js" type="text/javascript"></script>
<script src="js/admin/tools/conciliationExporter/Retrievers.js" type="text/javascript"></script>
<script src="js/admin/tools/conciliationExporter/FileExportTasks.js" type="text/javascript"></script>
<script src="js/admin/tools/conciliationExporter/conciliationExporterFooter.js" type="text/javascript"></script>


