<%-- 
    Document   : switchAccount
    Created on : 08/05/2019, 14:21:40 PM
    Author     : emida
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 4;
    int section_page = 67;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@ include file="/WEB-INF/jspf/pages/reports/switchAccount/switchAccountConstants.jspf" %>

<%
    String path = request.getContextPath();
    setJavaScriptGlobals(out, SessionData.getLanguage());
    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title><%=Languages.getString("jsp.reports.internalReport.switchAccount.title", SessionData.getLanguage())%></title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/themes/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/loading.css" />      
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/dataTables/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/dataTables/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<%=path%>/glyphicons/glyphicons.min.css" /> 
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<%=path%>/css/pages/reports/switchAccount/switchAccount.css" /> 
    </head>
    <body> 
        <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px">
            <tbody>
                <tr>
                    <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                    <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.reports.internalReport.switchAccount.title", SessionData.getLanguage())%></td>
                    <td height="20"><img src="images/top_right_blue.gif"></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                            <tbody>                  
                                <tr>
                                    <td width="1"><img src="images/trans.gif" width="1"></td>
                                    <td bgcolor="#ffffff" valign="top" align="center">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="main" align="left">
                                                        <table class="table">
                                                            <tbody>                                                     
                                                                <tr>                                                           
                                                                    <td>
                                                                        <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                            <li role="presentation" class="lstemplate active cm-tab1"><a style="cursor: pointer" class="lstemplate active"><img src="images/report-icon.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.reports.internalReport.switchAccount.title", SessionData.getLanguage())%></a></li>                                                                                                                                                                                                                                                                                                             
                                                                        </ul>  
                                                                        <br/>                                                                                                                                                                                                  
                                                                        <div class="container-fluid">
                                                                            <div class="content-cm-1">
                                                                                <form id="fmSwitchAccount" name="fmSwitchAccount" class="form-horizontal" autocomplete="off" >
                                                                                    <div class="row" style="background-color: #ffffff">                                                                                
                                                                                        <div class="col-md-2">                                                                                        
                                                                                            <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startDt", SessionData.getLanguage())%></h5>
                                                                                            <div class="input-group input-group-sm">
                                                                                                <span class="input-group-addon btncls" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                                                                                <input type="text" class="form-control" id="startDate" name="startDate" />
                                                                                            </div>
                                                                                            <br/><br/>
                                                                                            <button type="submit" id="btnSearchTerminals" class="btn btn-default btnsearch"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.genericLabel.search", SessionData.getLanguage())%></button>                                                                                    
                                                                                        </div>
                                                                                        <div class="col-md-2">                                                                                        
                                                                                            <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.endDt", SessionData.getLanguage())%></h5>
                                                                                            <div class="input-group input-group-sm">  
                                                                                                <span class="input-group-addon btncle" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                                <input type="text" class="form-control" id="endDate" name="endDate" />
                                                                                            </div>                                                                                                                                                                                               
                                                                                        </div>
                                                                                        <div class="col-md-2">                                                                                        
                                                                                            <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.label.terminalId", SessionData.getLanguage())%></h5>
                                                                                            <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                                <input type="text" class="form-control" id="terminalId" name="terminalId" />
                                                                                            </div>                                                                                                                                                                                               
                                                                                        </div>
                                                                                        <div class="col-md-2">                                                                                        
                                                                                            <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.label.merchantId", SessionData.getLanguage())%></h5>
                                                                                            <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                                <input type="text" class="form-control" id="merchantId" name="merchantId" />
                                                                                            </div>                                                                                                                                                                                               
                                                                                        </div>
                                                                                    </div>   
                                                                                </form>
                                                                                <br><br>
                                                                                <div id="divTableSwitchAccount" class="row" style="background-color: #ffffff; display:none;">
                                                                                    <div id="tbllstbundlingdiv" class="tbllstbundlingdiv text-muted card">
                                                                                        <table id="tableSwitchAccount" class="table table-hover table-responsive cell-border">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerDba", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerMerchantId", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerTerminalId", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerUserName", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerTerminalType", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerTerminalLabel", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerTerminalDate", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerTerminalBranding", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.ownerTerminalVersion", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedDate", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedDba", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedMerchantId", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedTerminalId", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedTerminalType", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedTerminalLabel", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedTerminalBranding", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedTerminalVersion", SessionData.getLanguage())%></th>
                                                                                                    <th><%= Languages.getString("jsp.reports.internalReport.switchAccount.form.table.column.title.associatedTerminalDate", SessionData.getLanguage())%></th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>   
                                                                        </div>   
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/jquery-1.12.4.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/jquery/jquery-ui.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/1.5.6/dataTables.buttons.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/1.5.6/buttons.flash.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/1.5.6/buttons.html5.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/dataTables/1.5.6/buttons.print.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/includes/jquery.validate.min.js"></script>
        <script type="text/javascript" charset="utf8" src="<%=path%>/js/pages/reports/switchAccount/SwitchAccount.js"></script>
    </body>
    <%@ include file="/includes/footer.jsp" %>
</html>
