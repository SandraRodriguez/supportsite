<%-- 
    Document   : balanceHistoryReportResult
    Created on : Jan 20, 2016, 12:32:26 AM
    Author     : g.martinez
--%>

<%--
  Author : Franky Villadiego
  Date: 2015-06-25
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
    
    .tools_link{
        text-decoration: underline !important;
        color: #2C0973 !important;   
    }    
/*    .pui-datatable-tablewrapper table{
        table-layout:auto;
    }*/

</style>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <spt:message code="jsp.admin.reports.balancehistory.balanceHistoryByRepsReport" />
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                
                <tr>
                    <td>
                        <form method="get" action="/support/reports/balancehistory_byreps/historical/download" >
                            <input type="hidden" name="startDate" value="${startDate}">
                            <input type="hidden" name="endDate" value="${endDate}">
                            <input type="hidden" name="reps" value="${reps}"> 
                            <input type="hidden" name="totalRecords" value="${totalRecords}">
                            
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                        <br/>
                        <input id="btnBack" class="btnDimensions" type="submit" value="${util:langString(Languages,"jsp.admin.reports.back")}" />
                        <br/>
                        <input id="btnDownload" type="submit" value="${util:langString(Languages, "jsp.admin.reports.downloadToCsv")}" />
                        </form>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <br/>
                        <p style="margin: 3px;">
                            ${totalRecords} ${util:langString(Languages,"jsp.admin.results_found")} ${util:langString(Languages,"jsp.admin.from")} ${startDate} ${util:langString(Languages,"jsp.admin.to")} ${endDate}
                                <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        
                        <p style="margin: 3px;">
                            - ${util:langString(Languages,"jsp.admin.reports.balancehistory.repsSharedBalanceInfo1")}
                        </p>
                        <p style="margin: 3px;">
                            - ${util:langString(Languages,"jsp.admin.reports.balancehistory.repsSharedBalanceInfo2")}
                        </p>
                        
                        <p style="margin: 3px; color: #425f07">${util:langString(Languages,"jsp.admin.reports.totals")}:</p>
                            
                            <p style="margin: 3px;  color: #425f07">- ${util:langString(Languages,"jsp.admin.reports.balancehistory.field.openBalance")} : ${sumOpenBalance}</p>
                            <p style="margin: 3px; color: #425f07">- ${util:langString(Languages,"jsp.admin.reports.balancehistory.field.transactionsAdjustments")} : ${sumTransactions}</p>
                            <p style="margin: 3px; color: #425f07">- ${util:langString(Languages,"jsp.admin.reports.balancehistory.field.deposits")} : ${sumDeposits}</p>
                            <p style="margin: 3px; color: #425f07">- ${util:langString(Languages,"jsp.admin.reports.balancehistory.field.closeBalance")} : ${sumCloseBalance}</p>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
    function generateAuditLink(entityId){
        sdate= encodeURIComponent($( "form input[name='startDate']").val());
        edate= encodeURIComponent($( "form input[name='endDate']").val());
        return '<a class="tools_link" target="_BLANK" href="/support/reports/balanceaudit/historical?startDate='+sdate+'&endDate='+edate+'&fType=ENTITY_ID&fValue='+entityId+'">'+"${util:langString(Languages,"jsp.admin.reports.balancehistory.field.auditLink")}"+'</a>';       
    }


    function removeNullData(data){
        if(data=='null') return "";
        return data;
    }
    
    function formatTimestamp(date){
        return date;
    }
    

        

    $(function() {

	$('#btnBack').click(function(){
		parent.history.back();
		return false;
	});

        $('#btnDownload').prop('disabled',function(){
            if("${totalRecords}" == "0"){
                return true;
            }
            return false;
        });

        $('#tblResult').puidatatable({
            lazy:true,
            paginator: {
                rows: 25,
                pageLinks:10,
                totalRecords:${totalRecords}
            },
            columns: [
                {field:'rwn', headerText: '#', headerStyle:'width:3%'},  
                {field:'entityId', headerStyle:'width:12%', headerText: '${util:langString(Languages, "jsp.admin.reports.balancehistory.field.entityId")}', sortable:true, content: function(row){return row.entityId+' '+row.entityName;}},               
                {field:'openBalance', headerStyle:'width:13%', headerText: '${util:langString(Languages, "jsp.admin.reports.balancehistory.field.openBalance")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.openBalanceFormat;}},
                {field:'transactions', headerStyle:'width:20%', headerText: '${util:langString(Languages, "jsp.admin.reports.balancehistory.field.transactionsAdjustments")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.transactionsFormat;}},
                {field:'deposits', headerStyle:'width:20%', headerText: '${util:langString(Languages, "jsp.admin.reports.balancehistory.field.deposits")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.depositsFormat;}}, 
                {field:'closeBalance', headerStyle:'width:15%', headerText: '${util:langString(Languages, "jsp.admin.reports.balancehistory.field.closeBalance")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.closeBalanceFormat;}},
                {field:'links', headerStyle:'width:11%', headerText: '${util:langString(Languages, "jsp.admin.reports.balancehistory.field.tools")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return generateAuditLink(row.entityId);}}
               
            ],
            sortField:'entityId',
            sortOrder:'1',
            datasource: function(callback, ui){
                $('#spinner').addClass("fa fa-refresh fa-spin");
                console.log('Calling..');
                var uri = '/support/reports/balancehistory_byreps/historical/grid';
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder == 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var data = {
                    "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows":rows, "totalRecords":"${totalRecords}",
                    "startDate":"${startDate}", "endDate":"${endDate}", "reps":"${reps}"};
                $('#sortField').val(sortField);
                $('#sortOrder').val(sortOrder);
                $.ajax({
                    type:"POST",
                    data:data,
                    url:uri,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        /*$('.pui-datatable-tablewrapper table').css("table-layout", "auto");*/
                        callback.call(this, response);
                    },
                    error:function(err){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        console.log(err);
                    }
                });
            }
        });



    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />