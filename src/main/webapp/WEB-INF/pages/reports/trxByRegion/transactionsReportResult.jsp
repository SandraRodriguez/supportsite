<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
    .pui-datatable table{
        width: auto;
    }
    .textRed{
        color:red;
    }

</style>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E; text-transform: uppercase;" class="formAreaTitle">
            ${util:langString(Languages,"jsp.admin.reports.transactions.transactions.title")}
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="font-size: 1.1em;">
                <tr>
                    <td>
                        <form method="get" action="/support/admin/transactions/download" >
                            <input type="hidden" name="startDate" value="${startDate}">
                            <input type="hidden" name="endDate" value="${endDate}">                            
                            <input type="hidden" name="totalRecords" value="${totalRecords}">
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                        <br/>
                        <input id="btnDownload" type="submit" value="${util:langString(Languages,"jsp.admin.reports.downloadToCsv")}" />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <p style="margin: 3px; font-size: 1.1em;">
                            ${totalRecords} ${util:langString(Languages,"jsp.admin.results_found")} ${util:langString(Languages,"jsp.admin.from")} ${startDate} ${util:langString(Languages,"jsp.admin.to")} ${endDate}
                                <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function removeNullData(data){
        if(data=='null') return "";
        if(typeof data === 'number'){
            if(data == 0) return "";
        }
        if(typeof data === 'string'){
            if(data.trim() === '0') return "";
        }
        return data;
    }
    function showClerkInfo(data){
        var clerkName = removeNullData(data.clerkName);
        if(clerkName) return clerkName;
        var clerk = removeNullData(data.clerk);
        if(clerk && clerk === 'RCH') return "(Recharge)"
        return "N/A";
    }
    function formatCurr(amount, currency){
        if(amount === null) return "";
        return currency + amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    }

    function showMerchantAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strShowCommDetail = ('${showCommissionDetails}' === 'true');
        if(strShowCommDetail && (strAccessLevel==='5' || strAccessLevel==='4' || strAccessLevel==='3' || strAccessLevel==='2' || strAccessLevel==='1')){
            return true;
        }
        return false;
    }

    function showRepAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strShowCommDetail = ('${showCommissionDetails}' == 'true');
        if(strShowCommDetail && (strAccessLevel=='4' || strAccessLevel=='3' || strAccessLevel=='2' || strAccessLevel=='1')){
            return true;
        }
        return false;
    }

    function showSubAgentAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strDistChainType = '${dist_chain_type}';
        var strShowCommDetail = ('${showCommissionDetails}' == 'true');
        if(strShowCommDetail && (strAccessLevel=='3' || strAccessLevel=='2' || (strAccessLevel=='1' && strDistChainType=='2'))){
            return true;
        }
        return false;
    }

    function showAgentAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strDistChainType = '${dist_chain_type}';
        var strShowCommDetail = ('${showCommissionDetails}' == 'true');
        if(strShowCommDetail && (strAccessLevel=='2' || (strAccessLevel=='1' && strDistChainType=='2'))){
            return true;
        }
        return false;
    }

    function showIsoAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strShowCommDetail = ('${showCommissionDetails}' == 'true');
        if(strShowCommDetail && strAccessLevel=='1'){
            return true;
        }
        return false;
    }
    
    function showBonusAmountColumn(){
        var international = ('${isInternational}' == 'true');
        if(international){
            return true;
        }
        return false;
    }

    function showTotalRechargeColumn(){
        return showBonusAmountColumn();
    }

    function showTaxCalcColumns(){
        var showTaxCalc = ('${showTaxCalc}' == 'true');
        var international = ('${isInternational}' == 'true');
        if(international && showTaxCalc){
            return true;
        }
        return false;
    }

    function showPinColumn(){
        var viewPin = ('${viewPin}' == 'true');
        var domestic = ('${isDomestic}' == 'true');
        var strAccessLevel = '${access_level}';
        if(viewPin && domestic && strAccessLevel=='1'){
            return true;
        }
        return false;
    }

    function showPinColumnValue(row){
        var viewPin = ('${viewPin}' === 'true');
        var prdTransType = row.productTransType;
        var amount = row.amount;        
        if(viewPin){
            if(prdTransType === 2 && amount > 0)
                return row.pin;
        }
        return "N/A";
    }

    function printControlValue(row){
        var viewPin = ('${viewPin}' == 'true');
        var prdTransType = row.productTransType;
        var amount = row.amount;
        if(viewPin){
            if(prdTransType == 2 && amount > 0)
                return "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + row.recId +
                    "');\"><span class=\"showLink\"><span class=\"showLink\">" + row.controlNo + "</span></span></a>";
        }
        return row.controlNo;
    }

    function showAchDateColumn(){
        var viewAchDate = ('${viewAchDate}' == 'true');
        var domestic = ('${isDomestic}' == 'true');
        var strAccessLevel = '${access_level}';
        if(viewAchDate && domestic && strAccessLevel=='1'){
            return true;
        }
        return false;
    }

    function showInvoiceColumn(){
        var invoiceNumberEnabled = ('${invoiceNumberEnabled}' == 'true');
        var international = ('${isInternational}' == 'true');
        if(invoiceNumberEnabled && international){
            return true;
        }
        return false;
    }

    function showRefCardsColumn(){
        var viewRefCards = ('${viewRefCards}' == 'true');
        var international = ('${isInternational}' == 'true');
        if(viewRefCards && international){
            return true;
        }
        return false;
    }

    function showColumn(perm){
        var boolPerm = perm == 'true';
        if(boolPerm) return true;
        return false;
    }
    
    function showExternalMerchantIdValue(){
        var showExternalMerchantId = ('${showExternalMerchantId}' === 'true');
        var international = ('${isInternational}' === 'true');
        if(international && showExternalMerchantId){
            return true;
        }
        return false;
    }

    
    
    
    
    function isPermPromoData() {
        var isPromoData = ((('${ isPermPromoData }') === 'true') ? true : false);
        return isPromoData;
    }
    
    function getHeaderAdditionalData () {
        return (isPermPromoData() ? '${util:langString(Languages,"jsp.reports.reports.transactions.form.table.column.name.promoData")}' : '${util:langString(Languages,"jsp.admin.reports.additionalData")}');
    }   

    //showAdditionalDataCol
    function printAdditionalData(row){
        var showAdditionalData = ('${showAdditionalData}' === 'true');
        var ad = row.additionalData;
        if(showAdditionalData)
            if(ad) return ad;
            else return '${util:langString(Languages,"jsp.admin.reports.additionalData.default")}';
        return '';
    }
    //

    function styleRow(row){
        if(row.transactionType === 'Return' || row.transactionType === 'Adjustment' || row.transactionType === 'Credit' || row.transactionType === 'Write Off'){
            return 'color: red';
        }
        return '';
    }

    function styleRow2(row, value){
        if(row.transactionType === 'Return' || row.transactionType === 'Adjustment' || row.transactionType === 'Credit' || row.transactionType === 'Write Off'){
            return '<span class="colorized">' + value + '</span>';
        }
        return value;
    }

    function js_yyyy_mm_dd_hh_mm_ss(recordDate) {
        var localDate = new Date(recordDate);
        year = "" + localDate.getFullYear();
        month = "" + (localDate.getMonth() + 1);
        if (month.length === 1) {
            month = "0" + month;
        }
        day = "" + localDate.getDate();
        if (day.length === 1) {
            day = "0" + day;
        }
        hour = "" + localDate.getHours();
        if (hour.length === 1) {
            hour = "0" + hour;
        }
        minute = "" + localDate.getMinutes();
        if (minute.length === 1) {
            minute = "0" + minute;
        }
        second = "" + localDate.getSeconds();
        if (second.length === 1) {
            second = "0" + second;
        }
        return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }


    function openViewPIN(sTransId) {
        var sURL = "/support/admin/transactions/view_pin.jsp?trans_id=" + sTransId;
        var sOptions = "left=" + (screen.width - (screen.width * 0.4))/2 + ",top=" + (screen.height - (screen.height * 0.3))/2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.3) + ",location=no,menubar=no,resizable=yes";
        var w = window.open(sURL, "ViewPIN", sOptions, true);
        w.focus();
    }
    
    function openViewReceipt(data){
        debugger;        
        var sURL = "/support/admin/transactions/view_receipt.jsp?data=" + data;
        var sOptions = "left=" + (screen.width - (screen.width * 0.4)) / 2 + ",top=" + (screen.height - (screen.height * 0.3)) / 2 + ",width=" + (screen.width * 0.4) + ",height=" + (screen.height * 0.5) + ",location=no,menubar=no,resizable=yes";
        var w = window.open(sURL, "ViewData", sOptions, true);
        w.focus();
    }
    
    function printReceiptTrxId(row){        
        if (row.receipt !== null){
            return "<a href=\"javascript:void(0);\" onclick=\"openViewReceipt('" + row.receipt +
                    "');\"><span class=\"showLink\"><span class=\"showLink\">" + row.recId + "</span></span></a>";
        } else{
            return "<span >" + row.recId + "</span>";
        }
    }

    $(function() {

        $('#tblResult').puidatatable({
            lazy:true,
            paginator: {
                rows: 25,
                pageLinks:10,
                totalRecords:${totalRecords}
            },
            columns: [
                {field:'rowNum', headerText: '#', headerStyle:'width:15px', content: function(row){return styleRow2(row, row.rowNum);}},
                {field:'recId', headerText: '${util:langString(Languages,"jsp.admin.reports.tran_no")}', sortable:true, headerClass:'header-tbl', 
                    content: function(row){ return styleRow2(row, printReceiptTrxId(row));}},
                {field:'millenniumNo', headerText: '${util:langString(Languages,"jsp.admin.reports.term_no")}', sortable:true, content: function(row){ return styleRow2(row, row.millenniumNo);}},
                {field:'dba', headerText: '${util:langString(Languages,"jsp.admin.reports.dba")}', sortable:true, content: function(row){ return styleRow2(row, row.dba);}},
                {field:'merchantId', headerText: '${util:langString(Languages,"jsp.admin.reports.merchant_id")}', sortable:true, content: function(row){ return styleRow2(row, row.merchantId);}},
                {field:'datetime', headerText: '${util:langString(Languages,"jsp.admin.reports.date")}', sortable:true, content: function(row){return styleRow2(row, removeNullData(row.datetime));}},
                {field:'region', headerText: '${util:langString(Languages,"jsp.admin.reports.region")}', sortable:true, content: function(row){ return styleRow2(row, row.region);}},
                {field:'providerName', headerText: '${util:langString(Languages,"jsp.admin.reports.providerName")}', sortable:true, content: function(row){ return styleRow2(row, row.providerName);}},
                {field:'providerId', headerText: '${util:langString(Languages,"jsp.admin.reports.providerId")}', sortable:true, content: function(row){ return styleRow2(row, row.providerId);}},
                //{field:'physCity', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.city")}', sortable:true, content: function(row){return styleRow2(row, row.physCity);}},
                //{field:'physCounty', headerText: '${util:langString(Languages,"jsp.admin.reports.county")}', sortable:true, content: function(row){return styleRow2(row, row.physCounty);}},
                //{field:'clerk', headerText: '${util:langString(Languages,"jsp.admin.reports.clerk")}', sortable:true,content: function(row){return styleRow2(row, showClerkInfo(row));}},
                {field:'ani', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.reference")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.ani));}},
                //{field:'password', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.ref_no")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.password));}},
                {field:'amount', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.recharge")}', sortable:true, content: function(row){return styleRow2(row,formatCurr(row.amount, "$"));}, bodyStyle:'text-align:right'},
                {field:'isoCommission', headerText: '${util:langString(Languages,"jsp.admin.iso_percent")}', sortable:true, bodyStyle:'text-align:right',
                    headerClass:function(){ return showIsoAmountColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showIsoAmountColumn() ? '' : 'hidden-column'; },
                    content: function(row){return styleRow2(row,formatCurr(row.isoCommission, "$"));}
                },
                {field:'agentCommission', headerText: '${util:langString(Languages,"jsp.admin.agent_percent")}', sortable:true, bodyStyle:'text-align:right',
                    headerClass:function(){ return showAgentAmountColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showAgentAmountColumn() ? '' : 'hidden-column'; },
                    content: function(row){return styleRow2(row,formatCurr(row.agentCommission, "$"));}
                },
                {field:'subAgentCommission', headerText: '${util:langString(Languages,"jsp.admin.subagent_percent")}', sortable:true, bodyStyle:'text-align:right',
                    headerClass:function(){ return showSubAgentAmountColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showSubAgentAmountColumn() ? '' : 'hidden-column'; },
                    content: function(row){return styleRow2(row,formatCurr(row.subAgentCommission, "$"));}
                },
                {field:'repCommission', headerText: '${util:langString(Languages,"jsp.admin.rep_percent")}', sortable:true, bodyStyle:'text-align:right',
                    headerClass:function(){ return showRepAmountColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showRepAmountColumn() ? '' : 'hidden-column'; },
                    content: function(row){return styleRow2(row,formatCurr(row.repCommission, "$"));}
                },
                {field:'merchantCommission', headerText: '${util:langString(Languages,"jsp.admin.merchant_percent")}', sortable:true, bodyStyle:'text-align:right',
                    headerClass:function(){ return showMerchantAmountColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showMerchantAmountColumn() ? '' : 'hidden-column'; },
                    content: function(row){return styleRow2(row,formatCurr(row.merchantCommission, "$"));}
                },
                {field:'bonusAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.bonus")}', sortable:true, content: function(row){return styleRow2(row,formatCurr(row.bonusAmount, "$"));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showBonusAmountColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showBonusAmountColumn() ? '' : 'hidden-column'; }
                },
                {field:'totalRecharge', headerText: '${util:langString(Languages,"jsp.admin.reports.total_recharge")}', sortable:true, content: function(row){return styleRow2(row,formatCurr(row.totalRecharge, "$"));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showTotalRechargeColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showTotalRechargeColumn() ? '' : 'hidden-column'; }
                },
                {field:'netAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.netAmount")}', sortable:true, content: function(row){return styleRow2(row,formatCurr(row.netAmount, "$"));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showTaxCalcColumns() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showTaxCalcColumns() ? '' : 'hidden-column'; }
                },/*
                {field:'taxAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.taxAmount")}', sortable:true, content: function(row){return styleRow2(row,formatCurr(row.taxAmount, "$"));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showTaxCalcColumns() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showTaxCalcColumns() ? '' : 'hidden-column'; }
                },
                {field:'taxPercentage', headerText: '${util:langString(Languages,"jsp.admin.reports.taxpercentage")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.taxPercentage));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showTaxCalcColumns() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showTaxCalcColumns() ? '' : 'hidden-column'; }
                },
                {field:'taxType', headerText: '${util:langString(Languages,"jsp.admin.reports.taxtype")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.taxType));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showTaxCalcColumns() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showTaxCalcColumns() ? '' : 'hidden-column'; }
                },
                {field:'balance', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.balance")}', sortable:true, content: function(row){return styleRow2(row,formatCurr(row.balance, "$"));}, bodyStyle:'text-align:right'},*/
                {field:'id', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.product")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.id));}, bodyStyle:'text-align:right'},
                {field:'description', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.description")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.description));}},
                //{field:'controlNo', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.control_no")}', sortable:true,content: function(row){return styleRow2(row, printControlValue(row));}, bodyStyle:'text-align:right'},
                {field:'otherInfo', headerText: '${util:langString(Languages,"jsp.admin.reports.authorization_carrier_number")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.otherInfo));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showColumn('${viewCarrierNumber}') ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showColumn('${viewCarrierNumber}') ? '' : 'hidden-column'; }
                },/*
                {field:'additionalData', sortable:true, content: function(row){ return styleRow2(row,printAdditionalData(row)); }, bodyStyle:'text-align:right',
                    headerClass:function(){ return showColumn('${showAdditionalData}') ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showColumn('${showAdditionalData}') ? '' : 'hidden-column'; },
                    headerText:function(){ return showColumn('${showAdditionalData}') ? getHeaderAdditionalData() : '${util:langString(Languages,"jsp.admin.reports.transactions.terminals_summary.terminal_type")}'; }
                },*/
                {field:'trmDescription', headerText: '${util:langString(Languages,"jsp.admin.reports.transactions.terminals_summary.terminal_type")}', sortable:true, content: function(row){ return styleRow2(row,removeNullData(row.trmDescription)); }},                
                {field:'pin', headerText: '${util:langString(Languages,"jsp.admin.reports.pin_number")}', sortable:true, content: function(row){return styleRow2(row,showPinColumnValue(row));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showPinColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showPinColumn() ? '' : 'hidden-column'; }
                },/*
                {field:'achDatetime', headerText: '${util:langString(Languages,"jsp.admin.reports.ach_date")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.achDatetime));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showAchDateColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showAchDateColumn() ? '' : 'hidden-column'; }
                },
                {field:'transactionType', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.transaction_type")}', 
                  sortable:true, content: function(row){return styleRow2(row,removeNullData(row.transactionType));}},
                {field:'invoiceNo', headerText: '${util:langString(Languages,"jsp.admin.reports.invoiceno")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.invoiceNo));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showInvoiceColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showInvoiceColumn() ? '' : 'hidden-column'; }
                },*/
                {field:'info', headerText: '${util:langString(Languages,"jsp.admin.report.transactions.DTU1204")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.info));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showRefCardsColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showRefCardsColumn() ? '' : 'hidden-column'; }
                },/*
                {field:'qrCode', headerText: '${util:langString(Languages,"jsp.admin.report.transactions.reference")}', sortable:true, content: function(row){return styleRow2(row,removeNullData(row.qrCode));}, bodyStyle:'text-align:right',
                    headerClass:function(){ return showColumn('${showQrCode}') ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showColumn('${showQrCode}') ? '' : 'hidden-column'; }
                },
                {field:'externalMerchantId', headerText: '${util:langString(Languages,"jsp.admin.report.transactions.externalMerchantId")}', sortable:true, bodyStyle:'text-align:right',
                    headerClass:function(){ return showExternalMerchantIdValue() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showExternalMerchantIdValue() ? '' : 'hidden-column'; }
                }*/
            ],
            sortField:'recId',
            sortOrder:'1',
            datasource: function(callback, ui){
                $('#spinner').addClass("fa fa-refresh fa-spin");
                console.log('Calling..');
                var uri = '/support/reports/trx/region';
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder === 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var data = {
                    "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows":rows, "totalRecords":"${totalRecords}",
                    "startDate":"${startDate}", "endDate":"${endDate}", "pinNumber":"${pinNumber}", "invoiceId":"${invoiceId}",
                    "transactionId":"${transactionId}", "allMerchants":"${allMerchants}", "merchantIds":"${merchantIds}",
                    "isoIds":"${isoIds}"};
                $('#sortField').val(sortField);
                $('#sortOrder').val(sortOrder);                
                $.ajax({
                    type:"GET",
                    data:data,
                    url:uri,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        callback.call(this, response);
                        debugger;
                        $(".colorized").parent("td").parent("tr").addClass('textRed');
                    },
                    error:function(err){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        console.log(err);
                    }
                });
            }
        });

    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />