<%-- 
    Document   : paymentProcessorInfoView
    Created on : 26-feb-2018, 17:55:38
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 4;
    int section_page = 17;
    //int section_page = 66;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<html>
    <head>        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>                                     

        <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
        <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
        <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>                

        <script>
            var vstartDt = '';
            var vendDt = '';
            var currentObj = null;
            var selectDt = '';
            $(document).ready(function () {
                $('.dv-detail').hide();
                $('.btnsearch').prop('disabled', true);
                $('.btndownload').hide();
                $('.divErrMessage').hide();
                $('#tbllstbundling').DataTable();
                $(".tbllstbundlingdiv").hide();
                $(".btncls").click(function () {
                    $('.startDt').trigger("click");
                });
                $(".btncle").click(function () {
                    $('.endDt').trigger("click");
                });
                $(".btnsearch").click(function () {
                    findByCreateDt();
                    $('.tbllstbundlingdiv').removeClass('col-md-8');
                    $('.dv-detail').hide();
                });
                $(".btndownload").click(function () {
                    window.location.href = "/support/reports/paymentProcessorInformation/csv?startDt=" + vstartDt + "&endDt=" + vendDt;
                });
                $(".vlt").click(function () {
                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'GET',
                        timeout: 600000,
                        data: {
                            merchant_id: currentObj.merchantId,
                            authorizationNumber: currentObj.authorizationNumber
                        },
                        url: '/support/reports/paymentProcessorInformation/findLastTransactionByMerchantId',
                        error: function () {
                            $(".ajax-loading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        buildTablelt(data);
                    });
                });

                $('input[name="startDt"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "autoApply": true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    endDate: moment(),
                    maxDate: moment()
                }, function (start, end, label) {
                    $('.endDt').trigger("click");
                    vstartDt = start.format('YYYY-MM-DD');
                });

                $('input[name="endDt"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    "autoApply": true,
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    endDate: moment(),
                    maxDate: moment()
                }, function (start, end, label) {
                    vendDt = end.format('YYYY-MM-DD');
                    if (vstartDt !== null || vstartDt !== '' || vendDt !== null || vendDt !== '') {
                        var duration = moment.duration(end.diff(vstartDt));
                        var diffDays = parseInt(duration.asDays());
                        if (diffDays > 182) {
                            $('.errMessage').text('<%= Languages.getString("jsp.includes.menu.report.ppi.err1", SessionData.getLanguage())%>');
                            $(".divErrMessage").show();
                            $(".alertmaxs").hide(150);
                            $('.btnsearch').prop('disabled', true);
                        } else {
                            $('.btnsearch').prop('disabled', false);
                        }
                    }
                });
            });

            function findByCreateDt() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    timeout: 600000,
                    data: {
                        startDt: vstartDt,
                        endDt: vendDt
                    },
                    url: '/support/reports/paymentProcessorInformation/findByCreateDt',
                    error: function () {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function onSuccessResponse(data) {
                if (data.length === 0) {
                    $(".tbllstbundlingdiv").hide();
                    $(".btnReportDownloader").hide();
                    $('#tbllstbundling').dataTable().fnClearTable();
                    $('.errMessage').text('<%= Languages.getString("jsp.report.consolidate.monthly.sales.search.empty", SessionData.getLanguage())%>');
                    $(".divErrMessage").show();
                    $(".alertmaxs").hide(150);
                    $('.btndownload').hide();
                } else {
                    $(".btnReportDownloader").show();
                    $(".tbllstbundlingdiv").show();
                    $(".divErrMessage").hide();
                    $('#tbllstbundling').dataTable().fnDestroy();
                    $('.btndownload').show();
                    buildTable(data);
                }
            }

            function buildTablelt(data) {
                $('.tblasttx').DataTable({
                    "aaData": data,
                    "paging": false,
                    "ordering": true,
                    "info": false,
                    "bDestroy": true,
                    "searching": false,
                    aoColumns: [
                        {"mData": 'datetime', sWidth: "0px"},
                        {mRender: function (data, type, row) {
                                return moment(row.datetime, "x").format("MM-DD-YYYY hh:mm:ss");
                            }, sDefaultContent: "--", sClass: "alignCenter"
                        },
                        {"mData": 'millennium_no', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'merchant_id', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'rec_id', sDefaultContent: "--", sClass: "alignCenter"},
                        {mRender: function (data, type, row) {
                                var finalb = parseFloat(row.amount);
                                return '$' + finalb.toFixed(2);
                            }, sDefaultContent: "--", sClass: "alignRight"
                        }
                    ],
                    "aoColumnDefs": [
                        { "bVisible": false, "aTargets": [0] }
                    ],
                    "aaSorting": [[0, "desc"]],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.report.consolidate.monthly.sales.search.empty")}',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": ""
                    }
                });
            }

            function buildTable(data) {
                $('#tbllstbundling').DataTable({
                    "autoWidth": false,
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bDestroy": true,
                    "paging": true,
                    "iDisplayLength": 12,
                    searching: true,                            
                    aoColumns: [
                        {"mData": 'createDt', sWidth: "0px"},
                        {mRender: function (data, type, row) {
                                return moment(row.createDt, "x").format("MM-DD-YYYY hh:mm:ss");
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "70px"
                        },
                        {"mData": 'merchantId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "70px"},
                        {"mData": 'legal_businessname', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'authorizationNumber', sDefaultContent: "--", sClass: "alignCenter", sWidth: "70px"},
                        {"mData": 'transactionId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "60px"},
                        {mRender: function (data, type, row) {
                                var finalb = parseFloat(row.paymentAmount);
                                return '$' + finalb.toFixed(2);
                            }, sDefaultContent: "--", sClass: "alignRight", sWidth: "15px"
                        },
                        {mRender: function (data, type, row) {
                                var finalb = parseFloat(row.fee);
                                return '$' + finalb.toFixed(2);
                            }, sDefaultContent: "--", sClass: "alignRight", sWidth: "15px"
                        },
                        {mRender: function (data, type, row) {
                                var finalb = parseFloat(row.totalAmount);
                                return '$' + finalb.toFixed(2);
                            }, sDefaultContent: "--", sClass: "alignRight", sWidth: "15px"
                        },
                        {mRender: function (data, type, row) {
                                if (row.state === 'approved') {
                                    return '<span style="cursor: pointer" onclick="viewdetails()" class="glyphicon glyphicon-time" aria-hidden="true"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span>';
                                } else if (row.state === 'completed' || row.state === 'captured') {
                                    return '<span style="cursor: pointer" onclick="viewdetails()" class="glyphicon glyphicon-ok" aria-hidden="true"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span>';
                                } else {
                                    return '<span style="cursor: pointer" onclick="viewdetails()" class="glyphicon glyphicon-remove" aria-hidden="true"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span>';
                                }
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "50px"
                        }
                    ],
                    "aoColumnDefs": [
                        { "bVisible": false, "aTargets": [0] }
                    ],
                    "aaSorting": [[0, "desc"]],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.report.consolidate.monthly.sales.search.empty")}',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    },
                    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).on('click', function () {
                            $('.tdc1').text(aData.merchantId);
                            $('.tdc2').text(aData.legal_businessname);
                            $('.tdc3').text(moment(aData.createDt, "x").format("MM-DD-YYYY hh:mm:ss"));

                            $('.tdc5').text(aData.host);
                            $('.tdc6').text(currencyForm(aData.paymentAmount));
                            $('.tdc7').text(currencyForm(aData.fee));
                            $('.tdc8').text(currencyForm(aData.totalAmount));

                            $('.tdc9').text(aData.authorizationNumber);
                            $('.tdc10').text(aData.transactionId);
                            if (aData.transactionId === null) {
                                $('.aditionaldata').hide();
                            } else {
                                $('.aditionaldata').show();
                            }
                            $('.tdc4').text(aData.email);
                            currentObj = aData;
                            selectDt = moment(aData.updateDt, "x").format("YYYY-MM-DD");
                        });
                    }
                });
            }
            function viewdetails(aData) {
                $('#tbllstbundlingdiv').removeClass('col-md-12');
                $('#tbllstbundlingdiv').addClass('col-md-8');

                var td = $('#tbllstbundling').DataTable();
                td.columns([5, 6, 7]).visible(false, false);

                $('.dv-detail').show();
            }
            function currencyForm(value) {
                var finalb = parseFloat(value);
                return '$' + finalb.toFixed(2);
            }
            function resertview() {
                $('#tbllstbundlingdiv').removeClass('col-md-8');
                $('#tbllstbundlingdiv').addClass('col-md-12');
                ;

                var td = $('#tbllstbundling').DataTable();
                td.columns([5, 6, 7]).visible(true, true);
                $('.dv-detail').hide();
            }
        </script>      
    </head>
    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>   


    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" style="margin-top: 5px; margin-left: 5px">
        <tbody><tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.includes.menu.report.ppi.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>
                                                                    <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                        <li role="presentation" class="lstemplate active cm-tab1"><a style="cursor: pointer" class="lstemplate active"><img src="images/report-icon.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.includes.menu.report.ppi.title", SessionData.getLanguage())%></a></li>                                                                                                                                                                                                                                                                                                             
                                                                    </ul>  
                                                                    <br />                                                                                                                                                                                                  
                                                                    <div class="container-fluid">
                                                                        <div class="content-cm-1">
                                                                            <div class="row" style="background-color: #ffffff">                                                                                
                                                                                <div class="col-md-2">                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startDt", SessionData.getLanguage())%></h5>
                                                                                    <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                        <span class="input-group-addon btncls" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                        <input type="text" class="form-control startDt" name="startDt" />                                                                                                                                                                                                                      
                                                                                    </div>                                                                                                                                                                                               
                                                                                    <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startdt.lbl1", SessionData.getLanguage())%></small>                                                                                    
                                                                                    <br /><br />
                                                                                    <button type="button" class="btn btn-default btnsearch"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.genericLabel.search", SessionData.getLanguage())%></button>                                                                                    

                                                                                </div>

                                                                                <div class="col-md-2">                                                                                        
                                                                                    <h5 style="font-weight: bold" class="text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.endDt", SessionData.getLanguage())%></h5>
                                                                                    <div class="input-group input-group-sm">                                                                                                                                                                
                                                                                        <span class="input-group-addon btncle" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>                                                                                                                                                                                                                                                                        
                                                                                        <input type="text" class="form-control endDt" name="endDt" />                                                                                                                                                                                                                      
                                                                                    </div>                                                                                                                                                                                               
                                                                                    <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.includes.menu.tools.pinmanagement.status.startdt.lbl2", SessionData.getLanguage())%></small>                                                                                                                                                                        
                                                                                </div>                                                                                

                                                                                <div class="col-lg-8">
                                                                                    <br />
                                                                                    <div class="alertmaxs">                                                                        
                                                                                        <div class="alert alert-warning">                                                                                                                         
                                                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                            <span class="glyphicon glyphicon-eye-open"></span> 
                                                                                            <strong>
                                                                                                <%= Languages.getString("jsp.includes.menu.report.ppi.wan1", SessionData.getLanguage())%>                                                                                
                                                                                            </strong>                                                                            
                                                                                        </div>                                              
                                                                                    </div> 
                                                                                    <div class="divErrMessage">
                                                                                        <div class="alert alert-danger" role="alert">
                                                                                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                                                            <p class="errMessage" style="display:inline"></p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <button type="button" class="btn btn-default btndownload pull-right"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.genericLabel.download", SessionData.getLanguage())%></button>                                                                                    
                                                                                </div>
                                                                            </div> 
                                                                            <hr />                                                                                                                                                                                                                                                                                                                       
                                                                            <div class="row" style="background-color: #ffffff"> 
                                                                                <div id="tbllstbundlingdiv" class="tbllstbundlingdiv text-muted card">
                                                                                    <table id="tbllstbundling" class="table table-hover table-responsive">                                                                            
                                                                                        <thead style="background-color: #f1f1f1">
                                                                                            <tr>
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c1", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c1", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c2", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c3", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c4", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c5", SessionData.getLanguage())%></th>                                                                                                                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c8", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c9", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c10", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c11", SessionData.getLanguage())%></th>                                                                                    
                                                                                            </tr>
                                                                                        </thead>                                                                                                    
                                                                                    </table>
                                                                                    <br />                                                                                    
                                                                                </div>
                                                                                <div class="col-md-4 dv-detail text-muted">     
                                                                                    <div class="alert" role="alert">
                                                                                        <button style="margin-right: 2%; margin-top: 2%" type="button" class="close" onclick="resertview()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                        <div class="card">                                                                                                                                                                                
                                                                                            <div style="margin-left: 2%">
                                                                                                <br />
                                                                                                <h3><strong><b><%= Languages.getString("jsp.includes.menu.report.ppi.title.1", SessionData.getLanguage())%></b> <em class="vlt" data-toggle="modal" data-target="#cm-rmodal"><small style="color: #52a4ff; cursor: pointer"><%= Languages.getString("jsp.includes.menu.report.ppi.modal1", SessionData.getLanguage())%></small></em></strong></h3>
                                                                                                <br />
                                                                                                <table cellspacing="10" style="font-size: 14px; width: 95%" >
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c2", SessionData.getLanguage())%></p></td>                                                                                                        
                                                                                                            <td><p class="tdc1 alignRight"></p></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c3", SessionData.getLanguage())%></p></td>                                                                                                        
                                                                                                            <td><p class="tdc2 alignRight"></p></td>
                                                                                                        </tr>                                                                                                                                                                                                       
                                                                                                        <tr>
                                                                                                            <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c1", SessionData.getLanguage())%></p></td>                                                                                                        
                                                                                                            <td><p class="tdc3 alignRight"></p></td>
                                                                                                        </tr>                                                                                                                                                                                                            
                                                                                                        <tr>
                                                                                                            <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c8", SessionData.getLanguage())%></p></td>                                                                                                        
                                                                                                            <td><p class="tdc6 alignRight"></p></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c9", SessionData.getLanguage())%></p></td>                                                                                                        
                                                                                                            <td><p class="tdc7 alignRight"></p></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><strong><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c10", SessionData.getLanguage())%></p></strong></td>                                                                                                        
                                                                                                            <td><strong><p class="tdc8 alignRight"></p></strong></td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>    
                                                                                                <div class="aditionaldata">
                                                                                                    <h3><strong><%= Languages.getString("jsp.includes.menu.report.ppi.title.2", SessionData.getLanguage())%></strong></h3>
                                                                                                    <table cellspacing="10" style="font-size: 14px; width: 95%" >
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c7", SessionData.getLanguage())%></p></td>                                                                                                        
                                                                                                                <td><p class="tdc5 alignRight"></p></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c4", SessionData.getLanguage())%></p></td>
                                                                                                                <td><p class="tdc9 alignRight"></p></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c5", SessionData.getLanguage())%></p></td>
                                                                                                                <td><p class="tdc10 alignRight"></p></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td><p><%= Languages.getString("jsp.includes.menu.report.ppi.tbl1.c6", SessionData.getLanguage())%></p></td>
                                                                                                                <td><p class="tdc4 alignRight"></p></td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>                                                                                                
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>    
                                                                        <div class="modal fade" id="cm-rmodal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                            <div class="vertical-alignment-helper text-muted">
                                                                                <div class="modal-dialog modal-lg vertical-align-center">                                                                                    
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                            <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.includes.menu.report.ppi.modal1", SessionData.getLanguage())%></h4>
                                                                                        </div>                                                                                        
                                                                                        <table id="tblasttx" class="table table-hover tblasttx" style="width: 100% !important">                                                                            
                                                                                            <thead style="background-color: #f1f1f1">
                                                                                                <tr>
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl2.c1", SessionData.getLanguage())%></th>                                                                                    
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl2.c1", SessionData.getLanguage())%></th>                                                                                    
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl2.c2", SessionData.getLanguage())%></th>                                                                                    
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl2.c3", SessionData.getLanguage())%></th>                                                                                    
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl2.c4", SessionData.getLanguage())%></th>                                                                                    
                                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.includes.menu.report.ppi.tbl2.c5", SessionData.getLanguage())%></th>                                                                                    
                                                                                                </tr>
                                                                                            </thead>                                                                                                    
                                                                                        </table>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                    </div>
                                                                                </div>                                                                                            
                                                                            </div>                                                                            
                                                                        </div>                                                                        
                                                                    </div>   
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody></table>
                </td>
            </tr>     
        </tbody>
    </table>
    <style type="text/css">
        .alignCenter { text-align: center; }
        .alignRight { text-align: right; }            
        .alignLeft { text-align: left; }
               
        .modal {
        }
        .vertical-alignment-helper {
            display:table;
            height: 100%;
            width: 100%;
        }
        .vertical-align-center {                
            display: table-cell;
            vertical-align: middle;
        }
        .modal-content {                
            width:inherit;
            height:inherit;               
            margin: 0 auto;
        }

        .modal.fade .modal-dialog {
            -webkit-transform: scale(0.1);
            -moz-transform: scale(0.1);
            -ms-transform: scale(0.1);
            transform: scale(0.1);
            top: 300px;
            opacity: 0;
            -webkit-transition: all 0.1s;
            -moz-transition: all 0.1s;
            transition: all 0.1s;
        }

        .modal.fade.in .modal-dialog {
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
            -webkit-transform: translate3d(0, -300px, 0);
            transform: translate3d(0, -300px, 0);
            opacity: 1;
        }
        .card {            
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;            
            border-radius: 5px;
        }
        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }
        .dataTables_wrapper .dataTables_length {
            float: left;
        }
        .dataTables_wrapper .dataTables_filter {
            float: left;
            text-align: left;                
        }
    </style>
</body>      
<%@ include file="/includes/footer.jsp" %>
</html>