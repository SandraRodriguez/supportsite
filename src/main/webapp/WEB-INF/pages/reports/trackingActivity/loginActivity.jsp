<%-- 
    Document   : loginActivity
    Created on : 20-nov-2017, 14:26:32
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 4;
    int section_page = 67;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<html>
    <head>        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                       
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>       

        <script>
            $(document).ready(function () {
                $('#tbllstTrackerTR').DataTable();
                $(".tbllstTrackerTRdiv").hide();
                $(".showDetails").hide();

                $('.btnback').click(function () {
                    $('.tbllstTrackerTRdiv').show(200);
                    $('.showDetails').hide(200);
                });

                findAllTerminalActivityLogs();
            });

            function findAllTerminalActivityLogs() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/reports/loginActivity/all'
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function onSuccessResponse(data) {
                if (data.length === 0) {
                    $(".tbllstTrackerTRdiv").hide(200);
                    $('#tbllstTrackerTR').dataTable().fnClearTable();
                    $(".alertmaxs").hide(150);
                } else {
                    $(".tbllstTrackerTRdiv").show();
                    $('#tbllstTrackerTR').dataTable().fnDestroy();
                    buildTable(data);
                }
            }

            function  buildTable(data) {
                $('#totaltr').text(data.length);
                $('#tbllstTrackerTR').DataTable({
                    "aaData": data,
                    "deferLoading": 57,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bDestroy": true,
                    "paging": true,
                    "iDisplayLength": 12,
                    searching: true,
                    aoColumns: [
                        {"mData": 'recId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "80px"},
                        {"mData": 'activityDate', sDefaultContent: "--", sClass: "alignCenter", sWidth: "80px"},
                        {"mData": 'customConfigId', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'merchantId', sDefaultContent: "--", sClass: "alignLeft", sWidth: "60px"},
                        {"mData": 'millenniumNo', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'registrationCode', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'userId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {"mData": 'userIp', sDefaultContent: "--", sClass: "alignCenter"},
                        {"mData": 'serverIp', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {"mData": 'description', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'errorCode', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"}
                    ],
                    "aaSorting": [[1, "desc"]],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.report.consolidate.monthly.sales.search.empty")}',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    }, fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).on('click', function () {
                            previewContent(aData);
                        });
                    }
                });
            }
            function previewContent(content) {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/reports/loginActivity/findOne',
                    data: {
                        recId: content.recId
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    renderInformation(data);
                    $(".showDetails").show(200);
                    $(".tbllstTrackerTRdiv").hide(200);
                });
            }
            function renderInformation(content) {
                $('.id_recId').text(content.recId);
                $('.id_activityDate').text(content.activityDate);
                $('.id_customConfigId').text(content.customConfigId);
                $('.id_merchantId').text(content.merchantId);
                $('.id_millenniumNo').text(content.millenniumNo);
                $('.id_registrationCode').text(content.registrationCode);
                $('.id_userId').text(content.userId);
                $('.id_userIp').text(content.userIp);
                $('.id_serverIp').text(content.serverIp);
                $('.id_urlAddress').text(content.urlAddress);
                $('.id_classs').text(content.classs);
                $('.id_funtion').text(content.funtion);
                $('.id_description').text(content.description);
                $('.id_errorCode').text(content.errorCode);
                $('.id_errorMessage').text(content.errorMessage);
                $('.id_clientUserAgent').text(content.clientUserAgent);
                $('.id_clientVersion').text(content.clientVersion);
                $('.id_serverVersion').text(content.serverVersion);
            }
        </script>      
        <style type="text/css">
            .alignCenter { text-align: center; }
            .alignRight { text-align: right; }            
            .alignLeft { text-align: left; }

            .dataTables_wrapper .dataTables_length {
                float: left;
            }
            .dataTables_wrapper .dataTables_filter {
                float: left;
                text-align: left;                
            }
        </style>
    </head>
    <body>
    <center>
        <div class="ajax-loading">
            <div></div>            
        </div>
    </center>        
    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" width="1800px" style="margin-top: 5px; margin-left: 5px">
        <tbody><tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.report.login.activity.main.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>   
                                                            <tr>         
                                                                <td>
                                                                    <ul class="nav nav-tabs">
                                                                        <li role="presentation" class="lstemplate active "><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.report.login.activity.main.title", SessionData.getLanguage())%> <span class="badge" id="totaltr"></span></a></li>  
                                                                    </ul>  
                                                                    <br />          

                                                                    <div class="tbllstTrackerTRdiv">
                                                                        <div class="alertmaxs">                      
                                                                            <div class="alert alert-warning">    
                                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                <span class="glyphicon glyphicon-eye-open"></span> 
                                                                                <strong>
                                                                                    <%= Languages.getString("jsp.report.login.activity.main.war.message", SessionData.getLanguage())%>     
                                                                                </strong> 
                                                                            </div>                     
                                                                        </div>  

                                                                        <table id="tbllstTrackerTR" class="table table-hover"> 
                                                                            <thead>
                                                                                <tr> 
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.1", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.2", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.3", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.4", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.5", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.6", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.7", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.8", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.9", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.10", SessionData.getLanguage())%></th>                                                                                    
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.login.activity.main.table.col.11", SessionData.getLanguage())%></th>                                                                                                                                                                        
                                                                                </tr>
                                                                            </thead>        
                                                                        </table>
                                                                    </div>   

                                                                    <!--show all details-->
                                                                    <div class="showDetails">
                                                                        <ul class="pager">
                                                                            <li class="previous btnback" id="btnsearch" style="cursor:pointer"><a><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.genericLabel.back", SessionData.getLanguage())%></a></li>       
                                                                        </ul>

                                                                        <div class="container-fluid">          
                                                                            <!--SITE INFO (view)-->
                                                                            <div class="panel panel-default">     
                                                                                <div class="panel-heading"><strong><%= Languages.getString("jsp.report.terminal.registration.title.table1", SessionData.getLanguage())%></strong></div>
                                                                                <table class="table table-hover">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.1", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 120px" class="text text-left"><strong><p class="id_recId"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.2", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_activityDate"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.3", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_customConfigId"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.4", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_merchantId"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.5", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 120px" class="text text-left"><strong><p class="id_millenniumNo"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.6", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_registrationCode"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.7", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_userId"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.8", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_userIp"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.9", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 120px" class="text text-left"><strong><p class="id_serverIp"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.10", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_urlAddress"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.11", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_classs"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.12", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_funtion"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.13", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 120px" class="text text-left"><strong><p class="id_description"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.14", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_errorCode"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.15", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_errorMessage"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.16", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_clientUserAgent"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.17", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_clientVersion"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 120px" class="text text-muted"><%= Languages.getString("jsp.report.login.activity.table1.row.18", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-left"><strong><p class="id_serverVersion"></p></strong></td>                                                                                            
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>  
                                                                            </div>   
                                                                        </div> 
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody></table>
                </td>
            </tr>     
        </tbody>
    </table>      
</body>      
<%@ include file="/includes/footer.jsp" %>
</html>