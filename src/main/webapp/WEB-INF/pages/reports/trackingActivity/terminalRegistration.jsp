<%-- 
    Document   : terminalRegistration
    Created on : 14-nov-2017, 15:43:38
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 4;
    int section_page = 67;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<html>
    <head>        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                       
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>       

        <script>
            $(document).ready(function () {
                $(".tbllstTrackerTRdiv").hide();
                $(".showDetails").hide();
                $(".btnallrecords").hide();

                $('.btnback').click(function () {
                    $('.tbllstTrackerTRdiv').show(200);
                    $('.showDetails').hide(200);
                });

                $('.btnnotok').click(function () {
                    notOkTerminalRegistration();
                    $(".btnallrecords").show();
                    $(".btnnotok").hide();
                });
                $('.btnallrecords').click(function () {
                    AllTerminalRegistration();
                    $(".btnnotok").show();
                    $(".btnallrecords").hide();
                });
                
                AllTerminalRegistration();
            });

            function AllTerminalRegistration() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/reports/AllTerminalRegistration/all'
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function notOkTerminalRegistration() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/reports/AllTerminalRegistration/allNotOk'
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function onSuccessResponse(data) {
                if (data.length === 0) {
                    $(".tbllstTrackerTRdiv").hide(200);
                    $('#tbllstTrackerTR').dataTable().fnClearTable();
                    $(".alertmaxs").hide(150);
                } else {
                    $(".tbllstTrackerTRdiv").show();
                    $('#tbllstTrackerTR').dataTable().fnDestroy();
                    buildTable(data);
                }
            }

            function  buildTable(data) {
                $('#totaltr').text(data.length);
                $('#tbllstTrackerTR').DataTable({
                    "aaData": data,
                    "deferLoading": 57,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bDestroy": true,
                    "paging": true,
                    "iDisplayLength": 12,
                    searching: true,
                    aoColumns: [
                        {mRender: function (data, type, row) {
                                var href = '--';
                                if (row.registrationAttemptNum > 3) {
                                    href = 'NO';
                                } else if (row.registrationAttemptNum === 1) {
                                    href = 'YES';
                                }
                                return href;
                            }, sDefaultContent: "--", sClass: "alignCenter"
                        },
                        {"mData": 'millennium_no', sDefaultContent: "--", sClass: "alignCenter", sWidth: "80px"},
                        {"mData": 'registrationInd', sDefaultContent: "--", sClass: "alignCenter", sWidth: "40px"},
                        {"mData": 'registrationDate', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'registrationIp', sDefaultContent: "--", sClass: "alignLeft", sWidth: "60px"},
                        {"mData": 'registrationAttemptNum', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'lastLoginAttemptNum', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'userId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {"mData": 'registrationCode', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'registrationIgnoreInd', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {"mData": 'restrictTolpAddr', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {"mData": 'ipAddrIgnoreInd', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {"mData": 'customConfigId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"}
                    ],
                    "aaSorting": [[1, "desc"]],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.report.consolidate.monthly.sales.search.empty")}',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    }, fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).on('click', function () {
                            previewContent(aData);
                        });
                    }
                });
            }
            function previewContent(content) {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/reports/AllTerminalRegistration/findOne',
                    data: {
                        userId: content.userId,
                        registrationCode: content.registrationCode,
                        millenniumNo: content.millennium_no
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    renderInformation(data);
                    $(".showDetails").show(200);
                    $(".tbllstTrackerTRdiv").hide(200);
                });
            }
            function renderInformation(content) {
                $('.id_millenium').text(content.terminalRegistrationDto.millennium_no);
                $('.id_pcterminal_model_type').text(content.terminalTypesDto.terminal_type + ' : ' + content.terminalResponseTypeDto.terminalResponseDescription);
                $('.id_terminal_response_type').text(content.terminalResponseTypeDto.terminalResponseType + ' : ' + content.terminalResponseTypeDto.terminalResponseDescription);
                $('.id_products_count').text(content.terminalRegistrationDto.products);
                $('.id_CustomConfigId').text(content.terminalRegistrationDto.customConfigId);
                $('.id_user_id').text(content.terminalRegistrationDto.userId);
                $('.id_user_password').text(content.terminalRegistrationDto.password);
                $('.id_lastLoginAttemptNum').text(content.terminalRegistrationDto.lastLoginAttemptNum);
                if (content.terminalRegistrationDto.disabledInd === '1') {
                    $('.id_disabledInd').text('DISABLED');
                } else {
                    $('.id_disabledInd').text('ACTIVE');
                }
                $('.id_lastLoginAttemptDate').text(content.terminalRegistrationDto.lastLoginAttemptDate);
                $('.id_lastLoginSuccessDate').text(content.terminalRegistrationDto.lastLoginSuccessDate);
                $('.id_lastLoginAttemptIpAddr').text(content.terminalRegistrationDto.lastLoginAttemptIpAddr);
                $('.id_lastLoginSuccessIpAddr').text(content.terminalRegistrationDto.lastLoginSuccessIpAddr);
                if (content.terminalRegistrationDto.registrationIgnoreInd === -1) {
                    $('.id_RegistrationIgnoreInd').text('YES - REG CODE REQUIRED');
                    $('.id_RegistrationIgnoreInd').addClass("text text-success");
                    $('.id_registrationAttemptNum_ok').text(content.terminalRegistrationDto.registrationIgnoreInd + ' : ' + 'NOT YET REGISTERED');
                } else {
                    $('.id_RegistrationIgnoreInd').text('NOT - REG CODE NOT REQUIRED');
                    $('.id_registrationAttemptNum_ok').text(content.terminalRegistrationDto.registrationIgnoreInd + ' : ' + 'REGISTERED - OK');
                }
                $('.id_registrationCode').text(content.terminalRegistrationDto.registrationCode);
                $('.id_registrationAttemptNum').text(content.terminalRegistrationDto.registrationAttemptNum);
                $('.id_registrationDate').text(content.terminalRegistrationDto.registrationDate);
                $('.id_registrationIp').text(content.terminalRegistrationDto.registrationIp);
                if (content.terminalRegistrationDto.ipAddrIgnoreInd === -1) {
                    $('.id_ipAddrIgnoreInd').text('YES - IP ADDRESS RESTRICTION');
                } else {
                    $('.id_ipAddrIgnoreInd').text('NO - IP ADDRESS IS IGNORED');
                }
                $('.id_restrictTolpAddr').text(content.terminalRegistrationDto.restrictTolpAddr);
                $('.id_restrictToSubnetMask').text(content.terminalRegistrationDto.restrictToSubnetMask);
                if (content.terminalRegistrationDto.certificateIgnoreInd === -1) {
                    $('.id_certificateIgnoreInd').text('YES - CERTIFICATE REQUIRED');
                } else {
                    $('.id_certificateIgnoreInd').text('NO - CERTIFICATE NOT REQUIRED');
                }
                if (content.terminalRegistrationDto.certificateInstallAlowedInd === -1) {
                    $('.id_certificateInstallAlowedInd').text('YES - CERT INSTALL ALLOWED');
                } else {
                    $('.id_certificateInstallAlowedInd').text('NOT - CERT INSTALL NOT ALLOWED');
                }
                $('.id_certificateInstallRemainNum').text(content.terminalRegistrationDto.certificateInstallRemainNum);

                $('.id_certificateInstallCompleteNum').text(content.terminalRegistrationDto.certificateInstallCompleteNum);
                $('.id_certificateRequestDate').text(content.terminalRegistrationDto.certificateRequestDate);
                $('.id_certificateStatusCode').text(content.terminalRegistrationDto.certificateStatusCode);
                $('.id_certificateStatusDate').text(content.terminalRegistrationDto.certificateStatusDate);
            }
        </script>      
        <style type="text/css">
            .alignCenter { text-align: center; }
            .alignRight { text-align: right; }            
            .alignLeft { text-align: left; }

            .dataTables_wrapper .dataTables_length {
                float: left;
            }
            .dataTables_wrapper .dataTables_filter {
                float: left;
                text-align: left;                
            }
        </style>
    </head>
    <body>
    <center>
        <div class="ajax-loading">
            <div></div>            
        </div>
    </center>
    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" width="1800px" style="margin-top: 5px; margin-left: 5px">
        <tbody><tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("terminal.registration.records.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>   
                                                            <tr>         
                                                                <td>
                                                                    <ul class="nav nav-tabs">
                                                                        <li role="presentation" class="lstemplate active "><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("terminal.registration.records.title", SessionData.getLanguage())%> <span class="badge" id="totaltr"></span></a></li>                                                                         
                                                                    </ul>  
                                                                    <br />          

                                                                    <div class="tbllstTrackerTRdiv">
                                                                        <div class="alertmaxs">                      
                                                                            <div class="alert alert-warning">    
                                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                <span class="glyphicon glyphicon-eye-open"></span> 
                                                                                <strong>
                                                                                    <%= Languages.getString("terminal.registration.records.message.1", SessionData.getLanguage())%>     
                                                                                </strong>                                                                                
                                                                            </div>                     
                                                                        </div>                                                                                                                                                                                                                             
                                                                        <table id="tbllstTrackerTR" class="table table-hover"> 
                                                                            <thead>
                                                                                <tr>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c0", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c1", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c2", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c3", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c4", SessionData.getLanguage())%></th>                                                                                                                                                                          
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c5", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c6", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c7", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c8", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c9", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c10", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c11", SessionData.getLanguage())%></th>
                                                                                    <th style="text-align: center !important"><%= Languages.getString("jsp.report.terminal.registration.title.main.table.c12", SessionData.getLanguage())%></th>
                                                                                </tr>
                                                                            </thead>        
                                                                        </table>
                                                                        <button type="button" class="btn btn-default btnnotok">View NOT OK Records</button>
                                                                        <button type="button" class="btn btn-default btnallrecords">View All Records</button>
                                                                    </div>   

                                                                    <!--show all details-->
                                                                    <div class="showDetails">
                                                                        <ul class="pager">
                                                                            <li class="previous btnback" id="btnsearch" style="cursor:pointer"><a><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.genericLabel.back", SessionData.getLanguage())%></a></li>       
                                                                        </ul>

                                                                        <div class="container-fluid">          
                                                                            <!--SITE INFO (view)-->
                                                                            <div class="panel panel-default">     
                                                                                <div class="panel-heading"><strong><%= Languages.getString("jsp.report.terminal.registration.title.table1", SessionData.getLanguage())%></strong></div>
                                                                                <table class="table table-hover">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table1.r1", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_millenium"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table1.r1", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table1.r2", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-center"><strong><p class="id_pcterminal_model_type"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table1.r2", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table1.r3", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-center"><strong><p class="id_terminal_response_type"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table1.r3", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table1.r4", SessionData.getLanguage())%></td>
                                                                                            <td class="text text-center"><strong><p class="id_products_count"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table1.r4", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>  
                                                                            </div>   
                                                                            <!--USER LOGIN (view)-->
                                                                            <div class="panel panel-default">     
                                                                                <div class="panel-heading"><strong><%= Languages.getString("jsp.report.terminal.registration.title.table2", SessionData.getLanguage())%></strong></div>
                                                                                <table class="table table-hover">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r1", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_CustomConfigId"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r1", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r2", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_user_id"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r2", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r3", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_user_password"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r2", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r4", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_lastLoginAttemptNum"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r4", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r5", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_disabledInd"></p></strong></td>                 
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r5", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r6", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_lastLoginAttemptDate"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r6", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r7", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_lastLoginSuccessDate"></p></strong></td>                 
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r7", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r8", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_lastLoginAttemptIpAddr"></p></strong></td>                 
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r8", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table2.r9", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_lastLoginSuccessIpAddr"></p></strong></td>         
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table2.r9", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>  
                                                                            </div>  
                                                                            <!--CLIENT SECURITY via REGISTRATION CODE (view)-->
                                                                            <div class="panel panel-default">     
                                                                                <div class="panel-heading"><strong><%= Languages.getString("jsp.report.terminal.registration.title.table3", SessionData.getLanguage())%></strong></div>
                                                                                <table class="table table-hover">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table3.r1", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_RegistrationIgnoreInd"></p></strong></td>                 
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table3.r1", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table3.r2", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_registrationCode"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table3.r2", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table3.r3", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_registrationAttemptNum"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table3.r3", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table3.r4", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_registrationAttemptNum_ok"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table3.r4", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table3.r5", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_registrationDate"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table3.r5", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table3.r6", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_registrationIp"></p></strong></td>                                                                                            
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table3.r6", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>  
                                                                            </div>  
                                                                            <!--CLIENT SECURITY via IP RESTRICTION (view)-->
                                                                            <div class="panel panel-default">     
                                                                                <div class="panel-heading"><strong><%= Languages.getString("jsp.report.terminal.registration.title.table4", SessionData.getLanguage())%></strong></div>
                                                                                <table class="table table-hover">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table4.r1", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_ipAddrIgnoreInd"></p></strong></td>                                                                                                                                                                                        
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table4.r1", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table4.r2", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_restrictTolpAddr"></p></strong></td>                                                                                                                                                                                        
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table4.r2", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table4.r3", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_restrictToSubnetMask"></p></strong></td>                                                                                                                                                                                                                                                                                    
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table4.r3", SessionData.getLanguage())%></td>
                                                                                        </tr>         
                                                                                    </tbody>
                                                                                </table>  
                                                                            </div> 
                                                                            <!--CLIENT SECURITY via CERTIFICATES (view)-->
                                                                            <div class="panel panel-default">     
                                                                                <div class="panel-heading"><strong><%= Languages.getString("jsp.report.terminal.registration.title.table5", SessionData.getLanguage())%></strong></div>
                                                                                <table class="table table-hover">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table5.r1", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_certificateIgnoreInd"></p></strong></td>                                                                                                                                                                                                                                                                                                                                                                                
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table5.r1", SessionData.getLanguage())%></td>
                                                                                        </tr>        
                                                                                    </tbody>
                                                                                </table>  
                                                                            </div> 
                                                                            <!--CERTIFICATE INSTALLATION ADMIN (view)-->
                                                                            <div class="panel panel-default">     
                                                                                <div class="panel-heading"><strong><%= Languages.getString("jsp.report.terminal.registration.title.table6", SessionData.getLanguage())%></strong></div>
                                                                                <table class="table table-hover">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table6.r1", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_certificateInstallAlowedInd"></p></strong></td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table6.r1", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table6.r2", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_certificateInstallRemainNum"></p></strong></td>
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table6.r2", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table6.r3", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_certificateInstallCompleteNum"></p></strong></td>                                                                                            
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table6.r3", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table6.r4", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_certificateRequestDate"></p></strong></td>                                                                                            
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table6.r4", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table6.r5", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_certificateStatusCode"></p></strong></td>                                                                                            
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table6.r5", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 220px" class="text text-muted"><%= Languages.getString("jsp.report.terminal.registration.label.table6.r6", SessionData.getLanguage())%></td>
                                                                                            <td style="width: 220px" class="text text-center"><strong><p class="id_certificateStatusDate"></p></strong></td>                                                                                            
                                                                                            <td class="text text-info"><%= Languages.getString("jsp.report.terminal.registration.inf.table6.r6", SessionData.getLanguage())%></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>  
                                                                            </div>  
                                                                        </div> 
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody></table>
                </td>
            </tr>     
        </tbody>
    </table>      



</body>      
<%@ include file="/includes/footer.jsp" %>
</html>