<%-- 
    Document   : emidaBalanceOnProviderForm
    Created on : 04-ago-2017, 14:49:13
    Author     : fernandob
--%>


<%@page import="com.debisys.languages.Languages"%>
<%@ include file="/WEB-INF/jspf/jstlIncludes.jspf" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%    
    int section = 4;
    int section_page = 65;
    String path = request.getContextPath();
%>


<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<c:import url="/WEB-INF/pages/header.jsp" />

<link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
<link href="<%=path%>/css/multi-select.css" rel="stylesheet" type="text/css"/>        
<link href="<%=path%>/css/jquery.dataTables.css" type="text/css"/>
<link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>            
<link href="<%=path%>/css/admin/reports/balance/emidaBalanceOnProvider/emidaBalanceOnProvider.css" type="text/css"/>

<script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script> 
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                
<script src="js/admin/reports/balance/emidaBalanceOnProvider/EmidaBalanceOnProvider.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.multi-select.js"></script>   
<script type="text/javascript" src="<%=path%>/js/jquery.quicksearch.js"></script>   

<%@ include file="/WEB-INF/jspf/admin/reports/balance/emidaBalanceOnProvider/emidaBalanceOnProvider.jspf" %>

<%
    setJsInvoicingGlobals(out, SessionData.getLanguage());
%>

  
<center>
    <div class="ajax-loading">
        <div></div>
    </div>
</center> 

<table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" width="1200" style="margin-top: 1%; margin-left: 1%">
    <tbody><tr>
            <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
            <td class="formAreaTitle" width="4000">${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.title")}</td>
            <td height="20"><img src="images/top_right_blue.gif"></td>
        </tr>
        <tr>
            <td colspan="3">
                <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                    <tbody>                  
                        <tr>
                            <td bgcolor="#7B9EBD" width="1"><img src="images/trans.gif" width="1"></td>
                            <td bgcolor="#ffffff" valign="top" align="center">
                                <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                    <tbody><tr>
                                            <td class="main" align="left">
                                                <table class="table">
                                                    <tbody>                                                     
                                                        <tr>                                                           
                                                            <td>
                                                                <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                    <li role="presentation" class="lstemplate active"><a style="cursor: pointer" class="lstemplate active"><img src="images/analysis_cube.png" border="0" width="22" height="22">  ${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.title")}</a></li>                                                                                                                                                
                                                                </ul>                                                                       
                                                                <br />
                                                                <div class="div-main">                                                                                                                                            
                                                                    <table width="50%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="lblrd">
                                                                                    <div class="form-group componentLeftMargin" >                                                                                                  
                                                                                        <label class="text-muted">${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.dateRange")}</label>                                                                                        
                                                                                        <div class="input-group input-group-sm">                                                                        
                                                                                            <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                                                                            <input type="text" class="form-control" name="daterange" />                                                                    
                                                                                        </div>                                                                                                                                                                                                                                             
                                                                                    </div>
                                                                                </td>  
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group componentLeftMargin">            
                                                                                        <label class="text-muted">${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.providerSelect")} :</label>

                                                                                        <select id="providerListBox" name="providerListBox[]" multiple="multiple">
                                                                                            <c:if test="${not empty providerList}">
                                                                                                <c:forEach items="${providerList}" var="currentProvider" varStatus="loopStatus">
                                                                                                    <option value="${currentProvider.providerId}">${currentProvider.name}</option>
                                                                                                </c:forEach>
                                                                                            </c:if>
                                                                                        </select>                                                                                            
                                                                                    </div>
                                                                                </td>                                                                                    
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="error" style="display:none" id="errorMessageIdDiv">
                                                                                        <span id="errorMessageIdSpan"></span>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                    <ul class="pager">
                                                                        <li class="previous btnsearch" id="btnsearch" style="cursor:pointer"><a><span class="glyphicon glyphicon-search" aria-hidden="true"></span> ${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.search")}</a></li>                                                                            
                                                                        <li class="next btnReportDownloader" style="cursor:pointer"><a><span class="glyphicon glyphicon-save" aria-hidden="true"></span> ${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.dowload")}</a></li>                                                                            
                                                                    </ul>
                                                                    <hr />
                                                                </div>
                                                                <div class="divErrMessage">
                                                                    <div class="alert alert-danger" role="alert">
                                                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>                                                                        
                                                                        <p id="errMessage" style="display:inline">${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.noData")}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="tblEmidaBalance">
                                                                    <table id="tblEmidaBalanceId" class="table table-hover">                                                                            
                                                                        <thead>
                                                                            <tr>                                                                                    
                                                                                <th>${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.balanceQueryDate")}</th>
                                                                                <th>${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.providerId")}</th>
                                                                                <th>${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.providerName")}</th>
                                                                                <th>${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.operatorName")}</th>
                                                                                <th>${util:langString(Languages, "jsp.report.balance.emidaBalanceOnProvider.balance")}</th>
                                                                            </tr>
                                                                        </thead>                                                                                                    
                                                                    </table>
                                                                </div>                                                                                                                                            
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                            <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                        </tr>
                        <tr>
                            <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                        </tr>
                    </tbody></table>
            </td>
        </tr>     
    </tbody>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />              
</table>



<c:import url="/WEB-INF/pages/footer.jsp" />