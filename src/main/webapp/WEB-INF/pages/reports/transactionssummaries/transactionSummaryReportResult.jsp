<%--
  Author : Franky Villadiego
  Date: 2015-06-25
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
    .pui-datatable table{
        width: auto;
    }
    .textRed{
        color:red;
    }

</style>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px; width: 100%">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E; text-transform: uppercase;" class="formAreaTitle">
            ${util:langString(Languages,"jsp.admin.reports.title4")}
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="font-size: 1.1em;">
                <tr style="font-size: 1.1em;">
                    <td>
                        ${timeZoneLabel}
                        <br/>
                        <span style="color: #ff0000;">${util:langString(Languages,"jsp.admin.reports.test_trans")}</span>

                    </td>
                </tr>
                <tr>
                    <td>
                        <form method="get" action="/support/admin/reports/transactions/merchants/download" >
                            <input type="hidden" name="startDate" value="${startDate}">
                            <input type="hidden" name="endDate" value="${endDate}">
                            <input type="hidden" name="merchantIds" value="${merchantIds}">
                            <input type="hidden" name="saleRepsIds" value="${saleRepsIds}">
                            <input type="hidden" name="totalRecords" value="${totalRecords}">
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                        <br/>
                        <input id="btnDownload" type="submit" value="${util:langString(Languages,"jsp.admin.reports.downloadToCsv")}" disabled />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <p style="font-size: 1.1em;">${util:langString(Languages,"jsp.admin.reports.transaction.merchant_summary")}</p>
                        <p id="txtCount" style="margin: 3px; font-size: 1.1em;"></p>
                        <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; font-size: 1.1em;">
                        ${util:langString(Languages,"jsp.admin.index.company_name")} : ${company_name}
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function removeNullData(data){
        if(data=='null') return "";
        if(typeof data == 'number'){
            if(data == 0) return "";
        }
        if(typeof data == 'string'){
            if(data.trim() == '0') return "";
        }
        return data;
    }
    function formatQty(quantity){
        if(quantity == null) return "";
        return quantity.toLocaleString('en');
    }
    function formatCurr(amount, currency){
        if(amount == null) return "";
        return currency + amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    }
    function showAddressColumn(){
        var strAccessLevel = '${access_level}';
        var domestic = ('${isDomestic}' == 'true');
        if( (strAccessLevel=='1' && domestic) || strAccessLevel=='9') return true;
        return false;
    }
    function showS2000ToolsColumn(){
        var strAccessLevel = '${access_level}';
        var s2000Tools = ('${s2000Tools}' == 'true');
        if( strAccessLevel=='1' && s2000Tools) return true;
        return false;
    }
    function showInvoiceTypeColumn(){
        var strAccessLevel = '${access_level}';
        if( strAccessLevel=='1') return true;
        return false;
    }
    function showRoutesColumn(){
        var international = ('${isInternational}' == 'true');
        var mexico = ('${isMexico}' == 'true');
        if(international || mexico) return true;
        return false;
    }
    function showBonusColumn(){
        var international = ('${isInternational}' == 'true');
        if(international) return true;
        return false;
    }
    function showTaxColumn(){
        if( showMexTaxColumns() || showIntTaxColumns() ) return true;
        return false;
    }
    function showIntTaxColumns(){
        var taxesInt = ('${taxesInt}' == 'true');
        var international = ('${isInternational}' == 'true');
        if(international && taxesInt) return true;
        return false;
    }
    function showMexTaxColumns(){
        var taxesMex = ('${taxesMex}' == 'true');
        var mexico = ('${isMexico}' == 'true');
        if(mexico && taxesMex) return true;
        return false;
    }
    function showIsoCarrierColumn(){
        var strAccessLevel = '${access_level}';
        if(strAccessLevel=='1' || strAccessLevel=='9') return true;
        return false;
    }
    function showIsoCarrierTaxColumn(){
        if(showIsoCarrierColumn() && (showIntTaxColumns() || showMexTaxColumns())) return true;
        return false;
    }
    function showIsoCarrierNETColumn(){
        if(showIsoCarrierColumn() && showIntTaxColumns()) return true;
        return false;
    }
    function showAgentColumn(){
        var strAccessLevel = '${access_level}';
        if(showIsoCarrierColumn() || strAccessLevel=='2') return true;
        return false;
    }
    function showAgentTaxColumn(){
        if((showIsoCarrierColumn() || showAgentColumn()) && (showIntTaxColumns() || showMexTaxColumns())) return true;
        return false;
    }
    function showAgentNETColumn(){
        if((showIsoCarrierColumn() || showAgentColumn()) && showIntTaxColumns()) return true;
        return false;
    }
    function showSubAgentColumn(){
        var strAccessLevel = '${access_level}';
        if(showAgentColumn() || strAccessLevel=='3') return true;
        return false;
    }
    function showSubAgentTaxColumn(){
        if((showAgentColumn() || showSubAgentColumn()) && (showIntTaxColumns() || showMexTaxColumns())) return true;
        return false;
    }
    function showSubAgentNETColumn(){
        if((showAgentColumn() || showSubAgentColumn()) && showIntTaxColumns()) return true;
        return false;
    }
    function showMerchantTaxColumn(){
        if((showIntTaxColumns() || showMexTaxColumns())) return true;
        return false;
    }
    function showMerchantNETColumn(){
        if(showIntTaxColumns()) return true;
        return false;
    }
    function showRepColumn(){
        var strAccessLevel = '${access_level}';
        if(showSubAgentColumn() || strAccessLevel=='4') return true;
        return false;
    }
    function showRepTaxColumn(){
        if((showSubAgentColumn() || showRepColumn()) && (showIntTaxColumns() || showMexTaxColumns())) return true;
        return false;
    }
    function showRepNETColumn(){
        if((showSubAgentColumn() || showRepColumn()) && showIntTaxColumns()) return true;
        return false;
    }
    function isIntlAndHasDataPromoPermission() {
        var response = ('${isIntlAndHasDataPromoPermission}' == 'true');
        console.log(response);
        return (response);
    }
    function taxTotalSalesHeaderText() {
        if(showTaxColumn())
            return '${util:langString(Languages,"jsp.admin.reports.total")}' + '${util:langString(Languages,"jsp.admin.reports.minusvat2")}';
        return '${util:langString(Languages,"jsp.admin.reports.netAmount")}';
    }
    function isoTaxCommissionHeaderText() {
        if(showIsoCarrierColumn() && showIsoCarrierNETColumn())
            return '${util:langString(Languages,"jsp.admin.net_iso_percent")}';
        return '${util:langString(Languages,"jsp.admin.iso_percent")}' + '${util:langString(Languages,"jsp.admin.reports.minusvat2")}';
    }
    function agentTaxCommissionHeaderText() {
        if(showAgentColumn() && showAgentNETColumn())
            return '${util:langString(Languages,"jsp.admin.net_agent_percent")}';
        return '${util:langString(Languages,"jsp.admin.agent_percent")}' + '${util:langString(Languages,"jsp.admin.reports.minusvat2")}';
    }
    function subAgentTaxCommissionHeaderText() {
        if(showSubAgentColumn() && showSubAgentNETColumn())
            return '${util:langString(Languages,"jsp.admin.net_subagent_percent")}';
        return '${util:langString(Languages,"jsp.admin.subagent_percent")}' + '${util:langString(Languages,"jsp.admin.reports.minusvat2")}';
    }
    function repTaxCommissionHeaderText() {
        if(showRepColumn() && showRepNETColumn())
            return '${util:langString(Languages,"jsp.admin.net_rep_percent")}';
        return '${util:langString(Languages,"jsp.admin.rep_percent")}' + '${util:langString(Languages,"jsp.admin.reports.minusvat2")}';
    }
    function merchantTaxCommissionHeaderText() {
        if(showMerchantNETColumn())
            return '${util:langString(Languages,"jsp.admin.net_merchant_percent")}';
        return '${util:langString(Languages,"jsp.admin.merchant_percent")}' + '${util:langString(Languages,"jsp.admin.reports.minusvat2")}';
    }



    function getTotalsColSpan() {
        var initial = 3;
        if(showAddressColumn()) initial +=4;
        if(showS2000ToolsColumn()) initial +=3;
        if(showInvoiceTypeColumn()) initial +=1;
        if(showRoutesColumn()) initial +=2;
        return initial;
    }
    function fillTotalsRow(colArray, totals) {

        if(showBonusColumn()){
            colArray.push({footerText : formatCurr(totals.totalBonus, "$"), bodyStyle:'text-align:right'});
            colArray.push({footerText : formatCurr(totals.totalRecharge, "$"), bodyStyle:'text-align:right'});
        }

        if(showTaxColumn()) colArray.push({footerText : formatCurr(totals.taxTotalSales, "$"), bodyStyle:'text-align:right'});
        if(showIntTaxColumns()) colArray.push({footerText : formatCurr(totals.totalTax, "$"), bodyStyle:'text-align:right'});

        //iso
        if(showIsoCarrierColumn()) colArray.push({footerText : formatCurr(totals.isoCommission, "$")});
        if(showIsoCarrierTaxColumn()) colArray.push({footerText : formatCurr(totals.taxIsoCommission, "$")});

        //agent
        if(showAgentColumn()) colArray.push({footerText : formatCurr(totals.agentCommission, "$")});
        if(showAgentTaxColumn()) colArray.push({footerText : formatCurr(totals.taxAgentCommission, "$")});

        //subagent
        if(showSubAgentColumn()) colArray.push({footerText : formatCurr(totals.subagentCommission, "$")});
        if(showIsoCarrierTaxColumn()) colArray.push({footerText : formatCurr(totals.taxSubagentCommission, "$")});

        //rep
        if(showRepColumn()) colArray.push({footerText : formatCurr(totals.repCommission, "$")});
        if(showRepTaxColumn()) colArray.push({footerText : formatCurr(totals.taxRepCommission, "$")});

        //merchant
        colArray.push({footerText : formatCurr(totals.merchantCommission, "$")});
        if(showMerchantTaxColumn()) colArray.push({footerText : formatCurr(totals.taxMerchantCommission, "$")});

        //Adj
        if(showIsoCarrierColumn()) colArray.push({footerText : formatCurr(totals.adjAmount, "$")});

        //Fill row remains
        colArray.push({footerText : ''});
        colArray.push({footerText : ''});
        colArray.push({footerText : ''});

    }


    function js_yyyy_mm_dd_hh_mm_ss(recordDate) {
        var localDate = new Date(recordDate);
        year = "" + localDate.getFullYear();
        month = "" + (localDate.getMonth() + 1);
        if (month.length === 1) {
            month = "0" + month;
        }
        day = "" + localDate.getDate();
        if (day.length === 1) {
            day = "0" + day;
        }
        hour = "" + localDate.getHours();
        if (hour.length === 1) {
            hour = "0" + hour;
        }
        minute = "" + localDate.getMinutes();
        if (minute.length === 1) {
            minute = "0" + minute;
        }
        second = "" + localDate.getSeconds();
        if (second.length === 1) {
            second = "0" + second;
        }
        return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }


    function viewTrx(row) {
        var urlText = formatCurr(row.totalSales, "$");
        var encodedStartDate = '${encodedStartDate}';
        var encodedEndDate = '${encodedEndDate}';
        var chkUseTaxValue = ('${useTaxValue}' == 'true') ? 'on' : '';
        var params =  "startDate=" + encodedStartDate  + "&endDate=" + encodedEndDate + "&search=y&report=y&merchantId="
            + row.merchantId + "&chkUseTaxValue=" + chkUseTaxValue + '&s2kRatePlans='  + '${ratePlansIds}';
        var sURL = "/support/admin/transactions/merchants_transactions.jsp?" + params;
        return '<a href="' + sURL + '" target="_blank" style="text-decoration: underline;" >' + urlText + '</a>';
    }



    $(function() {

        $('#tblResult').puidatatable({
            footerRows: [
                {
                    columns: [
                        {footerText: '${util:langString(Languages,"jsp.admin.reports.totals")}', colspan: 0},
                        {footerText: ''},
                        {footerText: ''}
                    ]
                }
            ],
            columns: [
                {field:'rowNum', headerText: '#', headerStyle:'width:15px'},
                {field:'dba', headerText: '${util:langString(Languages,"jsp.admin.reports.dba")}', sortable:true,
                    content: function(row){ return removeNullData(row.dba);}
                },
                {field:'physAddress', headerText: '${util:langString(Languages,"jsp.admin.customers.merchants_info.address")}', sortable:true,
                    headerClass:function(){ return showAddressColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showAddressColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.physAddress);}
                },
                {field:'physCity', headerText: '${util:langString(Languages,"jsp.admin.customers.merchants_edit.city")}', sortable:true,
                    headerClass:function(){ return showAddressColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showAddressColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.physCity);}
                },
                {field:'physState', headerText: '${util:langString(Languages,"jsp.admin.customers.merchants_edit.state_domestic")}', sortable:true,
                    headerClass:function(){ return showAddressColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showAddressColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.physState);}
                },
                {field:'physZip', headerText: '${util:langString(Languages,"jsp.admin.customers.merchants_edit.zip_domestic")}', sortable:true,
                    headerClass:function(){ return showAddressColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showAddressColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.physZip);}
                },
                {field:'merchantId', headerText: '${util:langString(Languages,"jsp.admin.reports.id")}', sortable:true,
                    content: function(row){ return row.merchantId}
                },
                {field:'salesmanId', headerText: '${util:langString(Languages,"jsp.admin.reports.SalesRepId")}', sortable:true,
                    headerClass:function(){ return showS2000ToolsColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showS2000ToolsColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.salesmanId);}
                },
                {field:'salesmanName', headerText: '${util:langString(Languages,"jsp.admin.reports.SalesRepName")}', sortable:true,
                    headerClass:function(){ return showS2000ToolsColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showS2000ToolsColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.salesmanName);}
                },
                {field:'accountNo', headerText: '${util:langString(Languages,"jsp.admin.reports.AccountNo")}', sortable:true,
                    headerClass:function(){ return showS2000ToolsColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showS2000ToolsColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.accountNo);}
                },
                {field:'invoiceType', headerText: '${util:langString(Languages,"jsp.admin.reports.transactions.commerce.invoice_type")}', sortable:true,
                    headerClass:function(){ return showInvoiceTypeColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showInvoiceTypeColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.invoiceType);}
                },
                {field:'routeName', headerText: '${util:langString(Languages,"jsp.admin.customers.merchants_edit.route")}', sortable:true,
                    headerClass:function(){ return showRoutesColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showRoutesColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.routeName);}
                },
                {field:'physCity', headerText: '${util:langString(Languages,"jsp.admin.customers.merchants_edit.city")}', sortable:true,
                    headerClass:function(){ return showRoutesColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showRoutesColumn() ? '' : 'hidden-column'; },
                    content: function(row){return removeNullData(row.physCity);}
                },
                {field:'qty', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.qty")}', bodyStyle:'text-align:right', sortable:true,
                    content: function(row){return row.qty;}
                },
                {field:'totalSales', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.recharge")}', sortable:true,
                    bodyStyle:'text-align:right',
                    content: function(row){return viewTrx(row);}
                },
                {field:'totalBonus', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.bonus")}', sortable:true,
                    headerClass:function(){ return showBonusColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showBonusColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.totalBonus, "$");}, bodyStyle:'text-align:right'
                },
                {field:'totalRecharge', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.total_recharge")}', sortable:true,
                    headerClass:function(){ return showBonusColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showBonusColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.totalRecharge, "$");}, bodyStyle:'text-align:right'
                },
                {field:'taxTotalSales', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.netAmount")}', sortable:true,
                    headerClass:function(){ return showTaxColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showTaxColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.taxTotalSales, "$");}, bodyStyle:'text-align:right'
                },
                {field:'taxAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.taxAmount")}', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showIntTaxColumns() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showIntTaxColumns() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.taxAmount, "$");}
                },
                {field:'isoCommission', bodyStyle:'text-align:right', headerText: '${util:langString(Languages,"jsp.admin.iso_percent")}', sortable:true,
                    headerClass:function(){ return showIsoCarrierColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showIsoCarrierColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.isoCommission, "$");}
                },
                {field:'taxIsoCommission', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showIsoCarrierTaxColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showIsoCarrierTaxColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.taxIsoCommission, "$");},
                    headerText: isoTaxCommissionHeaderText
                },
                {field:'agentCommission', headerText: '${util:langString(Languages,"jsp.admin.agent_percent")}', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showAgentColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showAgentColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.agentCommission, "$");}
                },
                {field:'taxAgentCommission', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showAgentTaxColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showAgentTaxColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.taxAgentCommission, "$");},
                    headerText: agentTaxCommissionHeaderText
                },
                {field:'subAgentCommission', headerText: '${util:langString(Languages,"jsp.admin.subagent_percent")}', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showSubAgentColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showSubAgentColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.subAgentCommission, "$");}
                },
                {field:'taxSubAgentCommission', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showSubAgentTaxColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showSubAgentTaxColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.taxSubAgentCommission, "$");},
                    headerText: subAgentTaxCommissionHeaderText
                },
                {field:'repCommission', headerText: '${util:langString(Languages,"jsp.admin.rep_percent")}', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showRepColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showRepColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.repCommission, "$");}
                },
                {field:'taxRepCommission', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showRepTaxColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showRepTaxColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.taxRepCommission, "$");},
                    headerText: repTaxCommissionHeaderText
                },
                {field:'merchantCommission', headerText: '${util:langString(Languages,"jsp.admin.merchant_percent")}', bodyStyle:'text-align:right', sortable:true,
                    content: function(row){return formatCurr(row.merchantCommission, "$");}
                },
                {field:'taxMerchantCommission', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showMerchantTaxColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showMerchantTaxColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.taxMerchantCommission, "$");},
                    headerText: merchantTaxCommissionHeaderText
                },
                {field:'adjAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.adjustment")}', bodyStyle:'text-align:right', sortable:true,
                    headerClass:function(){ return showIsoCarrierColumn() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return showIsoCarrierColumn() ? '' : 'hidden-column'; },
                    content: function(row){return formatCurr(row.adjAmount, "$");}
                },
                {field:'runningLiability', headerText: '${util:langString(Languages,"jsp.admin.reports.balance")}', bodyStyle:'text-align:right', sortable:true,
                    content: function(row){return formatCurr(row.runningLiability, "$");}
                },
                {field:'liabilityLimit', headerText: '${util:langString(Languages,"jsp.admin.reports.limit")}', bodyStyle:'text-align:right', sortable:true,
                    content: function(row){return formatCurr(row.liabilityLimit, "$");}
                },
                {field:'availableCredit', headerText: '${util:langString(Languages,"jsp.admin.reports.available")}', bodyStyle:'text-align:right', sortable:true,
                    content: function(row){return formatCurr(row.availableCredit, "$");}
                },
                {field:'promoData', headerText: '${util:langString(Languages,"jsp.reports.reports.transactions.form.table.column.name.promoData")}', sortable:true,
                    headerClass:function(){ return isIntlAndHasDataPromoPermission() ? '' : 'hidden-column'; },
                    bodyClass:function(){ return isIntlAndHasDataPromoPermission() ? '' : 'hidden-column'; },
                    content: function(row){ return removeNullData(row.additionalData);}
                }
            ],
            sortField:'dba',
            sortOrder:'1',
            datasource: function(callback){
                $('#spinner').addClass("fa fa-refresh fa-spin");
                console.log('Calling..');
                var uri = '/support/admin/reports/transactions/merchants/grid';
                var data = {"startDate":"${startDate}", "endDate":"${endDate}", "merchantIds":"${merchantIds}",
                    "saleRepsIds":"${saleRepsIds}"};
                $.ajax({
                    type:"GET",
                    data:data,
                    url:uri,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        var txtCounText = response.totalRecords + ' ' + '${util:langString(Languages,"jsp.admin.results_found")}' + ' ' + '${util:langString(Languages,"jsp.admin.from")}' + ' ' +
                            '${startDate}' + ' ' + '${util:langString(Languages,"jsp.admin.to")}' + ' ' + '${endDate}';
                        $('#txtCount').html(txtCounText);
                        var totalsColSpan = getTotalsColSpan();
                        this.options.footerRows[0].columns[0].colspan = totalsColSpan;
                        this.options.footerRows[0].columns[1].footerText = formatQty(response.totals.qty);
                        this.options.footerRows[0].columns[1].bodyStyle = 'text-align:right';
                        this.options.footerRows[0].columns[2].footerText = formatCurr(response.totals.totalSales, "$");
                        this.options.footerRows[0].columns[2].bodyStyle = 'text-align:right';
                        fillTotalsRow(this.options.footerRows[0].columns, response.totals);
                        callback.call(this, response.body);
                        $('#btnDownload').prop("disabled", false);
                    },
                    error:function(err){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        debugger;
                        console.log(err);
                    }
                });
            }
        });



        /*$("div.pui-paginator").clone().prependTo("#tblResult");*/
        /*$("div.pui-paginator").css("text-align", "right");*/


    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />