<%--
  Author : Franky Villadiego
  Date: 2018-10-03
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
</style>

<table border="0" cellpadding="0" cellspacing="0" width="750" style="margin: 5px">

    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E; text-transform: uppercase;" class="formAreaTitle">
            <sp:message code="jsp.admin.reports.title4" />
        </td>
    </tr>

    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <spf:form method="post" modelAttribute="reportModel">
                <input type="hidden" name="useTaxValue" value="${useTaxValue}">
                <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="font-size: 1.2em;">
                    <tr>
                        <td>
                            <spf:errors path="*" element="div" cssClass="errors"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${timeZoneLabel}
                            <br/>
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">${dateLabel}</td>
                                </tr>
                                <tr>
                                    <td width="24%"><spf:label path="startDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.start_date")} :</spf:label> </td>
                                    <td>
                                        <input id="fromDate" name="startDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${startDate}" />" type="text" size="12" style="height: 16px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td><spf:label path="endDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.end_date")} :</spf:label></td>
                                    <td><input id="toDate" name="endDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${endDate}" />" type="text" size="12" style="height: 16px;"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <c:if test="${showMerchants}">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="24%"></td>
                                    <td>
                                        <spf:checkbox path="allMerchants" id="chkMerchants" />
                                        ${util:langString(Languages, "jsp.tools.searchmerchant.selectAll")}
                                    </td>
                                </tr>
                                <tr>
                                    <td>${util:langString(Languages, "jsp.admin.reports.transactions.merchants.option")} :</td>
                                    <td>
                                        <input id="selMids" name="merchantIds" type="text" autocomplete="off"/>
                                        <select style="visibility: hidden; height: 30px;" name="merchantIds" id="merchantIds" multiple></select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </c:if>
                    <c:if test="${showSalesRep}">
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="24%">${util:langString(Languages, "jsp.admin.reports.payments.index.select_saleReps")} :</td>
                                        <td>
                                            <select name="salesRepIds" size="10" multiple style="width: 50%">
                                                <option value="" selected>${util:langString(Languages, "jsp.admin.reports.all")}</option>
                                                <c:forEach items="${salesRepList}" var="item">
                                                    <option value="${util:getObject(item,0)}">${util:getObject(item,1)}</option>
                                                </c:forEach>
                                            </select>
                                            <br/><br/>
                                            * ${util:langString(Languages, "jsp.admin.reports.salesRep.instructions")}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </c:if>
                    <c:if test="${showS2KRatePlans}">
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="24%">${util:langString(Languages, "jsp.admin.tools.s2k.rateplan")} :</td>
                                        <td>
                                            <select name="ratePlansIds" id="ratePlansIds" size="10" multiple style="width: 50%">
                                                <option value="" selected>${util:langString(Languages, "jsp.admin.reports.all")}</option>
                                                <c:forEach items="${ratePlanList}" var="item">
                                                    <option value="${item.ratePlanId}">${item.name}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </c:if>
                    <tr>
                        <td>
                            <br/>
                            <input id="btnSubmit" type="submit" class="main" value="${util:langString(Languages, "jsp.admin.reports.show_report")}"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br/>
                            * ${util:langString(Languages, "jsp.admin.reports.general")}
                        </td>
                    </tr>
                </table>
            </spf:form>
        </td>
    </tr>
    <tr>
        <td style="overflow: scroll; width:100%;">
            <div id="tblResult" style="margin-top:5px; width: 100%"></div>
        </td>
    </tr>
</table>

<script>

    function getRatePlansForMerchants(){
        var merchantsIds = $('#merchantIds').val();
        debugger;
        if(merchantsIds != null && merchantsIds.length != 0) {
            $.post("/support/admin/reports/transactions/merchants/rate-plans", {mIds: merchantsIds.toString()},
                function (data, status) {
                    debugger;
                    var array_data = String($.trim(data)).split("|");
                    fillRatePlans(array_data);
                }
            ).done(function() { console.log("Request successful"); })
            .fail(function(jqxhr, settings, ex) { console.log("Request fail"); });
        }
    }

    function fillRatePlans(array_data){
        debugger;
        var allLabel = '${util:langString(Languages,"jsp.admin.reports.all")}';
        $('#ratePlansIds').empty();
        $('#ratePlansIds').append('<option value="" selected>' + allLabel + '</option>');
        if (array_data[0] !== 'NOMATCH') {
            for (var i = 0; i < array_data.length; i++) {
                if (array_data[i].toString().length > 0) {
                    var rateplanData = String($.trim(array_data[i])).split("#");
                    $('#ratePlansIds').append('<option value="' + rateplanData[0] + '">' + rateplanData[1] + '</option>');
                }
            }
        }
    }

    function getRatePlansForAllMerchants(){
        debugger;
        $.get("/support/admin/reports/transactions/merchants/rate-plan-list",
            function (data, status) {
                debugger;
                var allLabel = '${util:langString(Languages,"jsp.admin.reports.all")}';
                $('#ratePlansIds').empty();
                $('#ratePlansIds').append('<option value="" selected>' + allLabel + '</option>');
                data.forEach(function(item, index){
                    $('#ratePlansIds').append('<option value="' + item.ratePlanId + '">' + item.name + '</option>');
                });
            }
        ).done(function() { console.log("Request successful"); })
        .fail(function(jqxhr, settings, ex) { console.log("Request fail"); });

    }


    $(function(){

        $("#fromDate").datepicker({
            showOn: "button",
            buttonImage: "admin/calendar/calbtn.gif",
            buttonImageOnly: true,
            dateFormat:"mm/dd/yy",
            maxDate:$("#toDate").val(),
            onClose: function( selectedDate ) {
                $( "#toDate" ).datepicker( "option", "minDate", selectedDate );
                $(".ui-datepicker-trigger").css("vertical-align","top");
            }
        });

        $("#toDate").datepicker({
            showOn: "button",
            buttonImage: "admin/calendar/calbtn.gif",
            buttonImageOnly: true,
            dateFormat:"mm/dd/yy",
            minDate:$("#fromDate").val(),
            onClose: function( selectedDate ) {
                $( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
                $(".ui-datepicker-trigger").css("vertical-align","top");
            }
        });

        $(".ui-datepicker-trigger").css("vertical-align","top");

        $("#chkMerchants").prop("checked", true);

        $("#chkMerchants").change(function() {
            if(this.checked) {
                $("ul.pui-autocomplete-multiple li.pui-autocomplete-token").remove();
                $("#merchantIds option").remove();
            }
        });


        $('#selMids').puiautocomplete({
            effect: 'fade',
            effectSpeed: 'fast',
            multiple: true,
            select: function(event, item) {
                debugger;
                var itemValue = item.data('value');
                var option = new Option(item.data('label'), itemValue);
                if(itemValue.indexOf('*') == 0){
                    $("#selMids").prop( "disabled", true);
                }
                $('#merchantIds').append($(option));
                $("#merchantIds option[value='" + option.value + "']").prop('selected', true);
                $("#chkMerchants").prop("checked", false);
                getRatePlansForMerchants();
            },
            unselect: function(event, item) {
                debugger;
                var itemValue = item.data('value');
                if(itemValue.indexOf('*') == 0){
                    $("#selMids").prop( "disabled", false);
                }
                $("#merchantIds option[value='" + itemValue + "']").remove();
                var opts = $("#merchantIds option").length;
                if(opts == 0){
                    $("#chkMerchants").prop("checked", true);
                    getRatePlansForAllMerchants();
                }else{
                    getRatePlansForMerchants();
                }
            },
            completeSource: function(request, response) {
                //in a real application, make a call to a remote url by passing the request.query as parameter
                var data = {"query": request.query};
                debugger;
                $.ajax({
                    type: "GET",
                    data: data,
                    url: '/support/admin/reports/transactions/merchants/search',
                    dataType: "json",
                    context: this,
                    success: function(data) {
                        console.log('Data :', data);
                        response.call(this, data);
                    }
                });
            }
        });

        $("span.pui-autocomplete").css("border", "1px solid #a9a9a9");
        $("ul.pui-autocomplete-multiple").removeClass("ui-state-default");
        $("#selMids").removeClass("ui-state-default");

        $("#selMids").focus(function() {
            $("ul.pui-autocomplete-multiple").removeClass("ui-state-focus");
            $("#selMids").removeClass("ui-state-focus");
            $("#selMids").removeClass("ui-state-hover");
        });
        $("#selMids").hover(function() {
            $("ul.pui-autocomplete-multiple").removeClass("ui-state-focus");
            $("#selMids").removeClass("ui-state-focus");
            $("#selMids").removeClass("ui-state-hover");
        });


    });

</script>

<c:import url="/WEB-INF/pages/footer.jsp" />