<%--
  Author : Franky Villadiego
  Date: 2015-06-25
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
</style>

<table border="0" cellpadding="0" cellspacing="0" width="750" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <spt:message code="jsp.admin.reports.AFBR.title" />
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <spf:form method="post" commandName="availableBalanceReportForm">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>

                <tr>
                    <td>
                        <spf:errors path="*" element="div" cssClass="errors"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td width="20%"><spf:label path="repId" cssErrorClass="new-error" ><spt:message code="jsp.admin.reports.AFBR.repId" /> :</spf:label></td>
                                <td><spf:input path="repId"  size="15" /></td>
                            </tr>
                            <tr>
                                <td><spt:message code="jsp.admin.reports.AFBR.reps" /> :</td>
                                <td>
                                    <select id="reps" name="reps" size="12" multiple>
                                        <c:if test="${util:getSize(repList) > 0}">
                                            <option value="-1" selected><spt:message code="jsp.admin.reports.all" /></option>
                                        </c:if>
                                        <c:forEach items="${repList}" var="rep">
                                            <option value="${util:getValue(rep,"repId")}">${util:getValue(rep,"repName")}</option>
                                        </c:forEach>
                                    </select>
                                    <br/><br/>
                                    * ${util:langString(Languages, "jsp.admin.reports.AFBR.reps.instructions")}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <br/>
                        <input id="btnSubmit" type="submit" class="main" value="${util:langString(Languages, "jsp.admin.reports.show_report")}"/>
                    </td>
                </tr>

            </table>
            </spf:form>
        </td>
    </tr>
</table>

<script>
    $(function(){

        var options = $("select#reps option").length;
        if(options==0){
            $("#btnSubmit").attr("disabled", "disabled");
        }


    });

</script>

<c:import url="/WEB-INF/pages/footer.jsp" />