<%--
  Author : Franky Villadiego
  Date: 2015-06-25
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
    .pui-datatable table{
        width: auto;
    }
    .textRed{
        color:red;
    }

</style>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            ${util:langString(Languages,"jsp.admin.reports.spiffReports.spiffDetailReport")}
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form method="get" action="/support/reports/spiff/detail/download" >
                            <input type="hidden" name="startDate" value="${startDate}">
                            <input type="hidden" name="endDate" value="${endDate}">
                            <input type="hidden" name="mids" value="${mids}">
                            <input type="hidden" name="pnumber" value="${spiffDetailReportForm.pnumber}">
                            <input type="hidden" name="sim" value="${spiffDetailReportForm.sim}">
                            <input type="hidden" name="pids" value="${pids}">
                            <input type="hidden" name="totalRecords" value="${totalRecords}">
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                        <br/>
                        <input id="btnDownload" type="submit" value="${util:langString(Languages,"jsp.admin.reports.downloadToCsv")}" />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <p style="margin: 3px;">
                            ${totalRecords} ${util:langString(Languages,"jsp.admin.results_found")} ${util:langString(Languages,"jsp.admin.from")} ${startDate} ${util:langString(Languages,"jsp.admin.to")} ${endDate}
                                <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function removeNullData(data){
        if(data=='null') return "";
        if(typeof data == 'number'){
            if(data == 0) return "";
        }
        if(typeof data == 'string'){
            if(data.trim() == '0') return "";
        }
        return data;
    }
    function formatCurr(amount, currency){
        return currency + amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    }

    function showRepAmountColumn(){
        var strAccessLevel = '${access_level}';
        if(strAccessLevel=='4' || strAccessLevel=='3' || strAccessLevel=='2' || strAccessLevel=='1'){
            return true;
        }
        return false;
    }

    function showSubAgentAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strDistChainType = '${dist_chain_type}';
        if(strAccessLevel=='3' || strAccessLevel=='3' || (strAccessLevel=='1' && strDistChainType=='2')){
            return true;
        }
        return false;
    }

    function showAgentAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strDistChainType = '${dist_chain_type}';
        if(strAccessLevel=='2' || (strAccessLevel=='1' && strDistChainType=='2')){
            return true;
        }
        return false;
    }

    function showIsoAmountColumn(){
        var strAccessLevel = '${access_level}';
        if(strAccessLevel=='1'){
            return true;
        }
        return false;
    }

    function showRepNameColumn(){
        var strAccessLevel = '${access_level}';
        if(strAccessLevel=='3' || strAccessLevel=='2' || strAccessLevel=='1'){
            return true;
        }
        return false;
    }

    function showSubAgentNameColumn(){
        return showAgentAmountColumn();
    }

    function showAgentNameColumn(){
        var strAccessLevel = '${access_level}';
        var strDistChainType = '${dist_chain_type}';
        if(strAccessLevel=='1' && strDistChainType=='2'){
            return true;
        }
        return false;
    }

    function styleRow(row){
        if(row.merchantCommissionAmount < 0){
            return '<span class="colorized">' + row.rowNum + '</span>' ;
        }
        return row.rowNum;
    }

    $(function() {

        $('#tblResult').puidatatable({
            lazy:true,
            paginator: {
                rows: 25,
                pageLinks:10,
                totalRecords:${totalRecords}
            },
            columns: [
                {field:'rowNum', headerText: '#', headerStyle:'width:15px', content: function(row){return styleRow(row);}},
                {field:'recId', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.recId")}', sortable:true, headerClass:'header-tbl'},
                {field:'activationTrxId', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.activationId")}', sortable:true, content: function(row){return removeNullData(row.activationTrxId);}},
                {field:'merchantId', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.merchantId")}', sortable:true},
                {field:'merchantDba', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.merchantDba")}', sortable:true},
                {field:'productName', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.productName")}', sortable:true},
                {field:'spiffType', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.spiffType")}', sortable:true},
                {field:'paymentDate', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.commissionPaymentDate")}', sortable:true, content: function(row){return removeNullData(row.paymentDate);}},
                {field:'activationDate', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.activationDate")}', sortable:true, content: function(row){return removeNullData(row.activationDate);}},
                {field:'activationProduct', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.activationProduct")}', sortable:true, content: function(row){return removeNullData(row.activationProduct);}},
                {field:'pin', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.pin")}', sortable:true, content: function(row){return removeNullData(row.pin);}},
                {field:'esn', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.esn")}', sortable:true, content: function(row){return removeNullData(row.esn);}},
                {field:'sim', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.sim")}', sortable:true, content: function(row){return removeNullData(row.sim);}},
                {field:'commissionType', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.commissionType")}', sortable:true},
                {field:'merchantCommissionAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.mCommissionAmount")}', sortable:true, content: function(row){return formatCurr(row.merchantCommissionAmount, "$");},
                    bodyStyle:'text-align:right'
                },
                {field:'repCommissionAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.rCommissionAmount")}', sortable:true, content: function(row){return formatCurr(row.repCommissionAmount, "$");},
                    bodyStyle:'text-align:right',
                    headerClass:function(){
                        return showRepAmountColumn() ? '' : 'hidden-column';
                    },
                    bodyClass:function(){
                        return showRepAmountColumn() ? '' : 'hidden-column';
                    }
                },
                {field:'subAgentCommissionAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.saCommissionAmount")}', sortable:true, content: function(row){return formatCurr(row.subAgentCommissionAmount, "$");},
                    bodyStyle:'text-align:right',
                    headerClass:function(){
                        return showSubAgentAmountColumn() ? '' : 'hidden-column';
                    },
                    bodyClass:function(){
                        return showSubAgentAmountColumn() ? '' : 'hidden-column';
                    }
                },
                {field:'agentCommissionAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.aCommissionAmount")}', sortable:true, content: function(row){return formatCurr(row.agentCommissionAmount, "$");},
                    bodyStyle:'text-align:right',
                    headerClass:function(){
                        return showAgentAmountColumn() ? '' : 'hidden-column';
                    },
                    bodyClass:function(){
                        return showAgentAmountColumn() ? '' : 'hidden-column';
                    }
                },
                {field:'isoCommissionAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.iCommissionAmount")}', sortable:true, content: function(row){return formatCurr(row.isoCommissionAmount, "$");},
                    bodyStyle:'text-align:right',
                    headerClass:function(){
                        return showIsoAmountColumn() ? '' : 'hidden-column';
                    },
                    bodyClass:function(){
                        return showIsoAmountColumn() ? '' : 'hidden-column';
                    }
                },
                {field:'paymentType', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.paymentType")}', sortable:false, content: function(row){return removeNullData(row.paymentType);}},
                {field:'paymentReference', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.paymentReference")}', sortable:true, content: function(row){return removeNullData(row.paymentReference);}},
                {field:'repName', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.repName")}', sortable:true,
                    headerClass:function(){
                        return showRepNameColumn() ? '' : 'hidden-column';
                    },
                    bodyClass:function(){
                        return showRepNameColumn() ? '' : 'hidden-column';
                    }
                },
                {field:'subAgentName', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.subAgentName")}', sortable:true,
                    headerClass:function(){
                        return showSubAgentNameColumn() ? '' : 'hidden-column';
                    },
                    bodyClass:function(){
                        return showSubAgentNameColumn() ? '' : 'hidden-column';
                    }
                },
                {field:'agentName', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.agentName")}', sortable:true,
                    headerClass:function(){
                        return showAgentNameColumn() ? '' : 'hidden-column';
                    },
                    bodyClass:function(){
                        return showAgentNameColumn() ? '' : 'hidden-column';
                    }
                },
                {field:'providerName', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffReports.providerName")}', sortable:true, content: function(row){return removeNullData(row.providerName);}},

            ],
            sortField:'recId',
            sortOrder:'1',
            datasource: function(callback, ui){
                $('#spinner').addClass("fa fa-refresh fa-spin");
                console.log('Calling..');
                var uri = '/support/reports/spiff/detail/grid';
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder === 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var data = {
                    "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows":rows, "totalRecords":"${totalRecords}",
                    "startDate":"${startDate}", "endDate":"${endDate}",
                    "pnumber":"${spiffDetailReportForm.pnumber}", "sim":"${spiffDetailReportForm.sim}", "mids":"${mids}", "pids":"${pids}"};
                $('#sortField').val(sortField);
                $('#sortOrder').val(sortOrder);                
                $.ajax({
                    type:"GET",
                    data:data,
                    url:uri,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        callback.call(this, response);
                        $(".colorized").parent("td").parent("tr").addClass('textRed');
                    },
                    error:function(err){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        console.log(err);
                    }
                });
            }
        });



    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />