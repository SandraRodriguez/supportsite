<%--
  Author : Franky Villadiego
  Date: 2015-06-25
--%>
<%@page import="com.debisys.utils.DebisysConstants"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
    .pui-datatable table{
        width: auto;
    }
    .textRed{
        color:red;
    }

</style>

<%
boolean hasPermission = SessionData.checkPermission(DebisysConstants.PERM_ALLOW_FREE_UP_ACTIVATIONS);    
String sAccessLevel = SessionData.getProperty("access_level");
%>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            ${util:langString(Languages,"jsp.admin.reports.spiffReports.activationDetailReport")}
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form method="get" action="/support/tools/activationVerizon/download" >
                            <input type="hidden" name="startDate" value="${startDate}">
                            <input type="hidden" name="endDate" value="${endDate}">
                            <input type="hidden" name="selectStatus" value="${selectStatus}">
                            <input type="hidden" name="merchants" value="${merchants}">
                            <input id="sortOrder" type="hidden" name="sortOrder"  >
                            <input id="sortField" type="hidden" name="sortField"  >
                             <input type="hidden" name="totalRecords" value="${totalRecords}">
                        <br/>
                        <input id="btnDownload" type="submit" value="${util:langString(Languages,"jsp.admin.reports.downloadToCsv")}" />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <p style="margin: 3px;">
                            ${totalRecords} ${util:langString(Languages,"jsp.admin.results_found")} ${util:langString(Languages,"jsp.admin.from")} ${startDate} ${util:langString(Languages,"jsp.admin.to")} ${endDate}
                                <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function removeNullData(data){
        if(data=='null') return "";
        if(typeof data == 'number'){
            if(data == 0) return "";
        }
        if(typeof data == 'string'){
            if(data.trim() == '0') return "";
        }
        return data;
    }
    function formatCurr(amount, currency){
        return currency + amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    }

    function showRepAmountColumn(){
        var strAccessLevel = '${access_level}';
        if(strAccessLevel=='4' || strAccessLevel=='3' || strAccessLevel=='2' || strAccessLevel=='1'){
            return true;
        }
        return false;
    }

    function showSubAgentAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strDistChainType = '${dist_chain_type}';
        if(strAccessLevel=='3' || strAccessLevel=='3' || (strAccessLevel=='1' && strDistChainType=='2')){
            return true;
        }
        return false;
    }

    function showAgentAmountColumn(){
        var strAccessLevel = '${access_level}';
        var strDistChainType = '${dist_chain_type}';
        if(strAccessLevel=='2' || (strAccessLevel=='1' && strDistChainType=='2')){
            return true;
        }
        return false;
    }

    function showIsoAmountColumn(){
        var strAccessLevel = '${access_level}';
        if(strAccessLevel=='1'){
            return true;
        }
        return false;
    }

    function showRepNameColumn(){
        var strAccessLevel = '${access_level}';
        if(strAccessLevel=='3' || strAccessLevel=='2' || strAccessLevel=='1'){
            return true;
        }
        return false;
    }

    function showSubAgentNameColumn(){
        return showAgentAmountColumn();
    }

    function showAgentNameColumn(){
        var strAccessLevel = '${access_level}';
        var strDistChainType = '${dist_chain_type}';
        if(strAccessLevel=='1' && strDistChainType=='2'){
            return true;
        }
        return false;
    }

    function styleRow(row){
        if(row.merchantCommissionAmount < 0){
            return '<span class="colorized">' + row.rowNum + '</span>' ;
        }
        return row.rowNum;
    }

    function edit(data){
    	 //alert( "'Transaction ID: "+ data);
    	 window.location = "/support/admin/tools/activations/verizon/editActivation.jsp?trxId="+data;
    }

   
    
    

    $(function() {
        

    	 $('#tblResult').puidatatable({
             lazy:true,
             paginator: {
                 rows: 25,
                 pageLinks:10,
                 totalRecords:${totalRecords}
             },
             columns: [                    
                 {field:'rowNum', headerText: 'Row Number', headerStyle:'width:15px', content: function(row){return styleRow(row);}},
                 {field:'transactionId', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.transactionId")}', sortable:true, content: function(row){return removeNullData(row.transactionId);}},
                 {field:'terminalNumber', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.terminalNumber")}', sortable:true, content: function(row){return removeNullData(row.terminalNumber);}},
                 {field:'dba', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.DBA")}', sortable:true, content: function(row){return removeNullData(row.dba);}},      
                 {field:'merchantId', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.merchantId")}', sortable:true, content: function(row){return removeNullData(row.merchantId);}},                 
				 {field:'transactionDate', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.transactionDate")}', sortable:true, content: function(row){return removeNullData(row.transactionDate);}},                 
                 {field:'merchatCity', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.merchantCity")}', sortable:true, content: function(row){return removeNullData(row.merchatCity);}},
                 {field:'merchantState', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.merchantState")}', sortable:true, content: function(row){return removeNullData(row.merchantState);}},                 
                 {field:'merchantEmail', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.merchantEmail")}', sortable:true, content: function(row){return removeNullData(row.merchantEmail);}},                	
                 {field:'merchantPhone', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.merchantPhone")}', sortable:true, content: function(row){return removeNullData(row.merchantPhone);}},
                 {field:'productId', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.productId")}', sortable:true, content: function(row){return removeNullData(row.productId);}},
                 {field:'productDescription', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.productDescription")}', sortable:true, content: function(row){return removeNullData(row.productDescription);}},
                 {field:'numersLines', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.numberLines")}', sortable:true, content: function(row){return removeNullData(row.numersLines);}},
                 {field:'subcriberFirstName',headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.subscriberFirstName")}', sortable:true, content: function(row){return removeNullData(row.subcriberFirstName);}},
				 {field:'subcriberLastName', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.subscriberLastName")}', sortable:true, content: function(row){return removeNullData(row.subcriberLastName);}},
                 {field:'streetNumber', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.streetNumber")}', sortable:true, content: function(row){return removeNullData(row.streetNumber);}},
                 {field:'streetName', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.streetName")}', sortable:true, content: function(row){return removeNullData(row.streetName);}},
                 {field:'city', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.city")}', sortable:true, content: function(row){return removeNullData(row.city);}},
                 {field:'stateName', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.state")}', sortable:true, content: function(row){return removeNullData(row.stateName);}},
                 {field:'zipCode', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.zipCode")}', sortable:true, content: function(row){return removeNullData(row.zipCode);}},
                 {field:'languange', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.preferredLanguage")}', sortable:true, content: function(row){return removeNullData(row.languange);}},
                 {field:'desiredAreaCode', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.desiredAreaCode")}', sortable:true, content: function(row){return removeNullData(row.desiredAreaCode);}},
                 {field:'contactPhoneNumber', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.contactPhone")}', sortable:true, content: function(row){return removeNullData(row.contactPhoneNumber);}},
                 {field:'subscriberPIN', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.SubscriberPIN")}', sortable:true, content: function(row){return removeNullData(row.subscriberPIN);}},
                 {field:'sim', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.SIM")}', sortable:true, content: function(row){return removeNullData(row.sim);}},
                 {field:'esnImei', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.ESNIMEI")}', sortable:true, content: function(row){return removeNullData(row.esnImei);}},
                 {field:'requestStatusDescription', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.RequestStatus")}', sortable:true, content: function(row){return removeNullData(row.requestStatusDescription);}},
                 {field:'activationPhoneNumber', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.activationPhoneNumber")}', sortable:true, content: function(row){return removeNullData(row.activationPhoneNumber);}},
                <%if(hasPermission && sAccessLevel.equals(DebisysConstants.ISO)){%> 
                    {field:'activationPhoneNumber', headerStyle:'width:150px', headerText: '${util:langString(Languages,"jsp.admin.reports.activation.verizon.editButton")}', sortable:true, 
                        content: function(row){return "<button  onclick=edit("+row.transactionId+")>"+'${util:langString(Languages,"jsp.admin.genericLabel.edit")}'+"</button>";}}
                <%}%>
             ],
             sortField:'transactionId',
             sortOrder:'1',
             datasource: function(callback, ui){
                 $('#spinner').addClass("fa fa-refresh fa-spin");
                 console.log('Calling..');
                 var uri = '/support/tools/activation/verizon/detail/grid';	
                 var start = ui.first + 1;
                 var sortField = ui.sortField;
                 var sortOrder = ui.sortOrder == 1 ? 'ASC' : 'DESC';
                 var rows = this.options.paginator.rows;
                 var data = {
                         "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows":rows, "totalRecords":"${totalRecords}",
                         "startDate":"${startDate}", "endDate":"${endDate}", "selectStatus":"${selectStatus}" ,"merchants":"${merchants}"};
                 $('#sortField').val(sortField);
                 $('#sortOrder').val(sortOrder);
                 $.ajax({
                     type:"POST",
                     data:data,
                     url:uri,
                     dataType:"json",
                     context:this,
                     success:function(response){
                         $('#spinner').removeClass("fa fa-refresh fa-spin");
                         callback.call(this, response);
                         $(".colorized").parent("td").parent("tr").addClass('textRed');
                     },
                     error:function(err){
                         $('#spinner').removeClass("fa fa-refresh fa-spin");
                         console.log(err);
                     }
                 });
             }
         });



    	 $(document).ready(function() {
    		 $('#tblResult tbody').on( 'click', 'button', function () {
    		        var data = table.row( $(this).parents('tr') ).data();
    		        //alert( "'Transaction ID: "+ data[ 0 ] );
    		    } );
    	 } );


    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />