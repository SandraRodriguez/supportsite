<%--
  Author : Fernando Briceño based on Franky Villadiego work
  Date: 2015-10-22
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
    .pui-datatable table{
        width: auto;
    }
    .textRed{
        color:red;
    }
    
    .btnDimensions{
        width: 9em;
    }


</style>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            ${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.reportTitle")}
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form method="get" action="/support/reports/spiff/processHistory/download" >
                            <input type="hidden" name="pin" value="${pin}">
                            <input type="hidden" name="totalTrxRecords" value="${totalTrxRecords}">
                            <input type="hidden" name="totalRecRecords" value="${totalRecRecords}">
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                            <br/>
                            <input id="btnDownload" class="btnDimensions" type="submit" value="${util:langString(Languages,"jsp.admin.reports.downloadToCsv")}" />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form method="get" action="/support/reports/spiff/processHistory" >
                            <input type="hidden" name="totalTrxRecords" value="0">
                            <input type="hidden" name="totalRecRecords" value="0">
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                            <br/>
                            <input id="btnBack" class="btnDimensions" type="submit" value="${util:langString(Languages,"jsp.admin.reports.back")}" />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <br/>
                        ${util:langString(Languages, "jsp.admin.reports.spiffProcessHistoryReport.requestPin")} : ${pin}
                    </td>
                </tr>                
                <tr>
                    <td>
                        <br/>
                        <p style="margin: 3px;">
                            ${totalTrxRecords} ${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.trxResultsFound")} ${util:langString(Languages,"jsp.admin.from")} ${startDate} ${util:langString(Languages,"jsp.admin.to")} ${endDate}
                            <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblTransactionsResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <p style="margin: 3px;">
                            ${totalRecRecords} ${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.recResultsFound")} ${util:langString(Languages,"jsp.admin.from")} ${startDate} ${util:langString(Languages,"jsp.admin.to")} ${endDate}
                            <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblHistoryResult" style="margin-top:5px; width: 100%"></div>
                    </td>

                </tr>

            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function removeNullData(data) {
        if (data === 'null')
                return "";
                if (typeof data === 'number') {
        if (data === 0)
                return "";
        }
        if (typeof data === 'string') {
        if (data.trim() === '0')
                return "";
        }
        return data;
    }
    function formatCurr(amount, currency) {
        return currency + amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    }

    function js_yyyy_mm_dd_hh_mm_ss(recordDate) {
        var localDate = new Date(recordDate);
                year = "" + localDate.getFullYear();
                month = "" + (localDate.getMonth() + 1);
                if (month.length === 1) {
        month = "0" + month;
        }
        day = "" + localDate.getDate();
                if (day.length === 1) {
        day = "0" + day;
        }
        hour = "" + localDate.getHours();
                if (hour.length === 1) {
        hour = "0" + hour;
        }
        minute = "" + localDate.getMinutes();
                if (minute.length === 1) {
        minute = "0" + minute;
        }
        second = "" + localDate.getSeconds();
                if (second.length === 1) {
        second = "0" + second;
        }
        return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }

    function getPaidByString(paidByCode)
    {
        if ("PAID_BY_BILLNG" === paidByCode) {
            return '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.paidBy.billing")}';
        } else if ("PAID_BY_JOB" === paidByCode) {
            return '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.paidBy.commissionsJob")}';
        }
        else 
        {
            return '';
        }
    }


    $(function () {

    $('#tblTransactionsResult').puidatatable({
    lazy: true,
            paginator: {
            rows: 25,
                    pageLinks: 10,
                    totalRecords:${totalTrxRecords}
            },
            columns: [
            {field: 'rowNum', headerText: '#', headerStyle: 'width:15px'},
            {field: 'trxDate', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.trxDate")}', sortable: true, content: function (row) {
                return js_yyyy_mm_dd_hh_mm_ss(row.trxDate);
            },
                    bodyStyle: 'text-align:left'
            },
            {field: 'productId', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.productId")}', sortable: false,
                    bodyStyle: 'text-align:right'
            },
            {field: 'productTransType', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.productTrxType")}', sortable: false},
            {field: 'billingTransactionType', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.billingTrxType")}', sortable: false},
            {field: 'trxId', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.TrxId")}', sortable: true,
                    bodyStyle: 'text-align:right'
            },
            {field: 'relatedTrxId', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.relTrxId")}', sortable: false,
                    bodyStyle: 'text-align:right'
            },
            {field: 'controlNo', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.controlNo")}', sortable: false,
                    bodyStyle: 'text-align:right'
            },
            {field: 'amount', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.trxAmount")}', sortable: false, content: function (row) {
                return formatCurr(row.amount, "$");
            },
                    bodyStyle: 'text-align:right'
            },
            {field: 'rep', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.repId")}', sortable: false,
                    bodyStyle: 'text-align:right'
            },
            {field: 'merchant', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.merchantId")}', sortable: false,
                    bodyStyle: 'text-align:right'
            },
            {field: 'terminal', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.terminal")}', sortable: false,
                    bodyStyle: 'text-align:right'
            },
            {field: 'paidBy', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.paidBy")}', sortable: false, content: function(row){
                    return getPaidByString(row.paidBy);
                },
                    bodyStyle: 'text-align:left'
            }
            ],
            sortField: 'trxDate',
            sortOrder: '1',
            datasource: function (callback, ui) {
            $('#spinner').addClass("fa fa-refresh fa-spin");
                    console.log('Calling..');
                    var uri = '/support/reports/spiff/processHistory/grid';
                    var start = ui.first + 1;
                    var sortField = ui.sortField;
                    var sortOrder = ui.sortOrder === 1 ? 'ASC' : 'DESC';
                    var rows = this.options.paginator.rows;
                    var data = {
                    "start": start, "sortField": sortField, "sortOrder": sortOrder, "rows": rows, "totalRecords": "${totalTrxRecords}",
                            "pin": "${pin}"};
                    $('#sortField').val(sortField);
                    $('#sortOrder').val(sortOrder);
                    $.ajax({
                    type: "GET",
                            data: data,
                            url: uri,
                            dataType: "json",
                            context: this,
                            success: function (response) {
                            $('#spinner').removeClass("fa fa-refresh fa-spin");
                                    callback.call(this, response.trxList);
                                    $('#tblHistoryResult').puidatatable({
                            lazy: true,
                                    paginator: {
                                    rows: 25,
                                            pageLinks: 10,
                                            totalRecords:${totalRecRecords}
                                    },
                                    columns: [
                                    {field: 'rowNum', headerText: '#', headerStyle: 'width:15px'},
                                    {field: 'emidaTrxId', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.TrxId")}', sortable: false,
                                            bodyStyle: 'text-align:right'
                                    },
                                    {field: 'datetime', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.loadDate")}', sortable: false, content: function (row) {
                                    return js_yyyy_mm_dd_hh_mm_ss(row.datetime);
                                    },
                                            bodyStyle: 'text-align:left'
                                    },
                                    {field: 'fileDate', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.fileDate")}', sortable: false, content: function (row) {
                                    return js_yyyy_mm_dd_hh_mm_ss(row.fileDate);
                                    },
                                            bodyStyle: 'text-align:left'
                                    },
                                    {field: 'processDate', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.processDate")}', sortable: false, content: function (row) {
                                    return js_yyyy_mm_dd_hh_mm_ss(row.processDate);
                                    },
                                            bodyStyle: 'text-align:left'
                                    },
                                    {field: 'processResult', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.processResult")}', sortable: false},
                                    {field: 'tracfoneRejectReason', headerText: '${util:langString(Languages,"jsp.admin.reports.spiffProcessHistoryReport.rejectReason")}', sortable: false}
                                    ],
                                    sortField: 'datetime',
                                    sortOrder: '1',
                                    datasource: response.recList
                            });
                            },
                            error: function (err) {
                            $('#spinner').removeClass("fa fa-refresh fa-spin");
                                    console.log(err);
                            }
                    });
            }
    });
    });

</script>

<c:import url="/WEB-INF/pages/footer.jsp" />