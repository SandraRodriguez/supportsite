<%-- 
    Document   : paymentOptions
    Created on : 24/01/2017, 01:38:35 PM
    Author     : janez
--%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@page import="com.debisys.languages.Languages"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />
<%
    String path = request.getContextPath();
%>

<style type="text/css">
    @import "<%=path%>/css/jquery.dataTables.min.css";
    .alignCenter { text-align: center; }
</style>

<script type="text/javascript" charset="utf8" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                        

<script>
    $(document).ready(function () {

        findHistorySuggest();

        $("#tblagent").hide();
        $("#toBack").hide();
        $("#divgralInfo").hide();

        $("#asearch").click(function () {
            $("#tblResult").show();
            findHistory();
        });

        $("#doDownload").click(function (event) {
            event.preventDefault();
            var vl = document.getElementById('doDownload').value;
            alert(vl);
        });

        $(function () {

            $("#fromDate").datepicker({
                showOn: "button",
                buttonImage: "admin/calendar/calbtn.gif",
                buttonImageOnly: true,
                dateFormat: "dd/mm/yy",
//                maxDate: $("#toDate").val(),
                onClose: function (selectedDate) {
                    $("#toDate").datepicker("option", "minDate", selectedDate);
                    $(".ui-datepicker-trigger").css("vertical-align", "top");
                }
            });
            var today = new Date();
            $("#toDate").datepicker({
                showOn: "button",
                buttonImage: "admin/calendar/calbtn.gif",
                buttonImageOnly: true,
                dateFormat: "dd/mm/yy",
                minDate: $("#fromDate").val(),
                maxDate: today,
                onClose: function (selectedDate) {
                    $("#fromDate").datepicker("option", "maxDate", selectedDate);
                    $(".ui-datepicker-trigger").css("vertical-align", "top");
                }
            });


            $("#filterValue2").change(function () {
                $("#filterValue").val($("#filterValue2").val());
            });

            $(".ui-datepicker-trigger").css("vertical-align", "top");
        });

        $("#evnsch").click(function (event) {
            event.preventDefault();
            onScheduleHistoryBalance();
        });
    });

    function onScheduleHistoryBalance() {
        if ($("#fromDate").val().length <= 0 || $("#fromDate").val().length < 10) {
            alert('<%=Languages.getString("jsp.report.history.balance.emptydata", SessionData.getLanguage()).toUpperCase()%>');
        } else {            
            var result = $.ajax({
                type: 'POST',
                timeout: 10000,
                data: {
                    startDt: $("#fromDate").val(),
                    endDt: $("#fromDate").val()
                },
                url: '/support/reports/onScheduleHistoryBalance',
                async: true
            });
            result.success(function (data, status, headers, config) {
                if (data.length !== 0) {
                    $("p#tblagentmessage").text('<%=Languages.getString("jsp.report.history.balance.scheduledmessage", SessionData.getLanguage())%>');
                    $("#evnsch").hide();
                    window.location.href = '/support/reports/scheduleHistorialBalanceController';
                }
            });
            result.error(function (x, t, m) {
                if (t === "timeout") {
                } else {
                }
            });
        }
    }

    function findHistory() {
        var result = $.ajax({
            type: 'GET',
            timeout: 10000,
            data: {
                startDt: $("#fromDate").val(),
                endDt: $("#toDate").val()
            },
            url: '/support/reports/findHistoryBalance',
            async: true
        });
        result.success(function (data, status, headers, config) {
            if (data.length === 0) {
                $('#tblagendreport').dataTable().fnClearTable();
                $("p#totalRecords").text(data.length);
                $("p#totalRecords").attr('style', "color: #cd0a0a; display:inline");
                $("p#pfrom").text($("#fromDate").val());
                $("p#pto").text($("#toDate").val());

                $("#tblagent").show();
                $("p#tblagentmessage").text('<%=Languages.getString("jsp.report.history.balance.agend", SessionData.getLanguage()).toUpperCase()%>');
                $("#evnsch").show();
                $("#toBack").show();

                $("#fromDate").datepicker('option', {
                    maxDate: new Date()
                });

                $("#idenddatetitle").hide();
                $("#idenddate").hide();
                $("p#startDtTitle").text('<%=Languages.getString("jsp.report.history.balance.select.date", SessionData.getLanguage()).toUpperCase()%>');

                $("#divtblagend").hide();
            } else {
                $("#divtblagend").show();
                $("#divgralInfo").show();
                $('#tblagendreport').DataTable({
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "bDestroy": true,
                    "paging": false,
                    "iDisplayLength": 15,
                    bFilter: false,
                    ordering: false,
                    searching: true,
                    aoColumns: [
                        {mRender: function (data, type, row) {
                                var href = '/support/reports/doDownloadHistoryBalance?fileName=';
                                if (row.status === 'COMPLETED') {
                                    href = "<a href='" + href + row.fileName + "'>" + row.fileName + "</a>";
                                } else {
                                    href = row.fileName;
                                }
                                return href;
                            }, sDefaultContent: "n/a", sClass: "alignCenter"
                        },
                        {"mData": 'balanceType', sDefaultContent: "n/a", sClass: "alignCenter"},
                        {"mData": 'status', sDefaultContent: "n/a", sClass: "alignCenter"},
                        {"mData": 'records', sDefaultContent: "n/a", sClass: "alignCenter"},
                        {"mData": 'startDt', sDefaultContent: "n/a", sClass: "alignCenter"}
                    ],           
                    "sPaginationType": "full_numbers",
                    language: {
                        "search": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "paginate": {
                            "next": '${Languages.getString("jsp.admin.next")}',
                            "previous": '${Languages.getString("jsp.admin.previous")}'
                        }
                    }
                });
                $('#tblagendreport').dataTable().fnClearTable();
                $('#tblagendreport').dataTable().fnAddData(data);

                $("p#totalRecords").text(data.length);
                $("p#pfrom").text($("#fromDate").val());
                $("p#pto").text($("#toDate").val());

                $("#tblagent").hide();
                $("#toBack").show();
            }

        });
    }

    function formatedDate(today) {
        var day = today.getDate();
        var month = today.getMonth() + 1;
        var year = today.getFullYear();

        var formatted =
                (day < 10 ? '0' : '') + day + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                year;
        return formatted;
    }

    function findHistorySuggest() {
        var today = new Date();
        var bac = new Date();
        var day = bac.getDate();
        var mont = bac.getMonth();
        var year = bac.getFullYear();

        bac.setFullYear(year, mont, (day - 15));

        var result = $.ajax({
            type: 'GET',
            timeout: 10000,
            data: {
                startDt: '' + formatedDate(bac) + '',
                endDt: '' + formatedDate(today) + ''
            },
            url: '/support/reports/findHistoryBalance',
            async: true
        });
        result.success(function (data, status, headers, config) {
            if (data.length === 0) {
                $("p#totalRecords").text(data.length);
                $("p#pfrom").text(formatedDate(bac));
                $("p#pto").text(formatedDate(today));

                $("#tblagent").show();
                $("p#tblagentmessage").text('<%=Languages.getString("jsp.report.history.balanceNotExist.data", SessionData.getLanguage()).toUpperCase()%>');
                $("#evnsch").hide();

                $("#tblResult").hide();
                $("#divtblagend").hide();
            } else {
                $("p#totalRecords").text(data.length);
                $("p#pfrom").text(formatedDate(bac));
                $("p#pto").text(formatedDate(today));

                $("#tblagent").hide();
                $("#toBack").hide();
                $("#divgralInfo").show();

                $('#tblagendreport').DataTable({
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "bDestroy": true,
                    "paging": false,
                    "iDisplayLength": 15,
                    bFilter: false,
                    ordering: false,
                    searching: true,
                    aoColumns: [
                        {mRender: function (data, type, row) {
                                var href = '/support/reports/doDownloadHistoryBalance?fileName=';
                                if (row.status === 'COMPLETED') {
                                    href = "<a href='" + href + row.fileName + "'>" + row.fileName + "</a>";
                                } else {
                                    href = row.fileName;
                                }
                                return href;
                            }, sDefaultContent: "n/a", sClass: "alignCenter"
                        },
                        {"mData": 'balanceType', sDefaultContent: "n/a", sClass: "alignCenter"},
                        {"mData": 'status', sDefaultContent: "n/a", sClass: "alignCenter"},
                        {"mData": 'records', sDefaultContent: "n/a", sClass: "alignCenter"},
                        {"mData": 'startDt', sDefaultContent: "n/a", sClass: "alignCenter"}
                    ],
                    "sPaginationType": "full_numbers",
                    language: {
                        "search": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "oPaginate": {
                            "next": '${Languages.getString("jsp.admin.next")}',
                            "previous": '${Languages.getString("jsp.admin.previous")}'
                        }
                    }
                });
                $('#tblagendreport').dataTable().fnClearTable();
                $('#tblagendreport').dataTable().fnAddData(data);
            }
        });
    }
</script>


<table border="0" cellpadding="0" cellspacing="0" width="1200px" background="images/top_blue.gif" style="margin-bottom: 1%">
    <tr>        
        <td class="formAreaTitle" align="left" width="100%"><%=Languages.getString("jsp.admin.reports.balancehistory.agendBalanceHistoryReport", SessionData.getLanguage()).toUpperCase()%></td>        
    </tr>    
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="right">                                
                <tr>
                    <td>                                                                                                  
                        <table border="0" width="100%" cellpadding="0" cellspacing="0" align="right">                                                                
                            <tr>                                     
                                <td class="formArea2">                                                                                                                    
                                    <br />                                                                                               
                            <center><a href="/support/admin/tools/paymentOption/addPaymentProcessor?message=">                                            
                                </a></center>                                       <br />
                            <table width="100%" border="0" style="margin-bottom: 1%">                                          
                                <tr>
                                    <td width="150"  align="center">
                                        <p id="startDtTitle" style="display:inline">
                                            <%=Languages.getString("jsp.admin.start_date", SessionData.getLanguage()).toUpperCase()%>
                                        </p>                                            
                                    </td>                                        
                                    <td>                                                                                                  
                                        <input id="fromDate" name="startDate" value="" type="text" size="12" style="height: 16px;">                                       
                                    </td>
                                </tr>                                    
                                <tr>                                    
                                    <td width="150" align="center">
                                        <div id="idenddatetitle">
                                            <%=Languages.getString("jsp.admin.end_date", SessionData.getLanguage()).toUpperCase()%>
                                        </div>                                        
                                    </td>                                        
                                    <td>     
                                        <div id="idenddate">
                                            <input id="toDate" name="endDate" value="" type="text" size="12" style="height: 16px;">                                            
                                            <input value="<%=Languages.getString("jsp.admin.search", SessionData.getLanguage()).toUpperCase()%>" id="asearch" type="button" size="12" style="margin-left: 2%;">
                                        </div>                                                                                        
                                    </td>                                                                                    
                                </tr>                                    
                                <tr>
                                    <td></td>                                    
                                    <td>
                                        <!--<div id="tblResult" class="pui-datatable ui-widget" style="margin-top:1%">-->                                                
                                        <div>   
                                            <div id="divgralInfo">
                                                <br />
                                                <strong>
                                                    <%=Languages.getString("jsp.report.totalrecords", SessionData.getLanguage())%>
                                                    <p id="totalRecords" style="display:inline"></p>
                                                    <%=Languages.getString("jsp.admin.from", SessionData.getLanguage())%>
                                                    <p id="pfrom" style="display:inline"></p>                                                    
                                                    <%=Languages.getString("jsp.report.totalrecords.to", SessionData.getLanguage())%>
                                                    <p id="pto" style="display:inline"></p>
                                                </strong>
                                            </div>
                                            <div id="divtblagend">
                                                <table id="tblagendreport" class="table display" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th><%=Languages.getString("jsp.report.history.balance.filename", SessionData.getLanguage()).toUpperCase()%></th>
                                                            <th><%=Languages.getString("jsp.report.history.balance.balancetype", SessionData.getLanguage()).toUpperCase()%></th>
                                                            <th><%=Languages.getString("jsp.report.history.balance.status", SessionData.getLanguage()).toUpperCase()%></th>
                                                            <th><%=Languages.getString("jsp.report.history.balance.totalrecord", SessionData.getLanguage()).toUpperCase()%></th>
                                                            <th><%=Languages.getString("jsp.admin.reports.user.date", SessionData.getLanguage()).toUpperCase()%></th>           
                                                        </tr>
                                                    </thead>
                                                </table>  
                                            </div>                                                                                         
                                        </div>    

                                        <div id="tblagent" style="margin-top:1%">
                                            <p id="tblagentmessage" style="display:inline; color: #cd0a0a"></p>                                                
                                            <a href="javascript:void(0)" id="evnsch">
                                                <%=Languages.getString("jsp.report.history.balance.click", SessionData.getLanguage()).toUpperCase()%>
                                            </a> 
                                            <div id="toBack">
                                                <br />
                                                <a href="/support/reports/scheduleHistorialBalanceController" id="evnsch"><<
                                                    <%=Languages.getString("jsp.admin.genericLabel.back", SessionData.getLanguage()).toUpperCase()%>
                                                </a>
                                            </div>                                                
                                        </div>
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>   
            </table>                                                      
        </td> 
    </tr>
</table> 
</table>

<c:import url="/WEB-INF/pages/footer.jsp" />