<%--
  Author : Franky Villadiego
  Date: 2015-06-25
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
</style>

<table border="0" cellpadding="0" cellspacing="0" width="750" style="margin: 5px">

    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E; text-transform: uppercase;" class="formAreaTitle">
            <sp:message code="jsp.admin.reports.transactions.transactions.title" />
        </td>
    </tr>

    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <spf:form method="post" modelAttribute="reportModel">
                <spf:hidden id="report" path="report" />
                <spf:hidden id="repName" path="repName" />
                <spf:hidden id="repId" path="repId" />
                <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="font-size: 1.2em;">
                    <tr>
                        <td>
                            <spf:errors path="*" element="div" cssClass="errors"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${timeZoneLabel}
                            <br/>
                            <br/>
                        </td>
                    </tr>
                    <c:if test="${showIsoName}">
                        <tr>
                            <td>
                                ${util:langString(Languages, "jsp.admin.iso_name")} : &nbsp; ${isoName}
                            </td>
                        </tr>
                    </c:if>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">${dateLabel}</td>
                                </tr>
                                <tr>
                                    <td width="22%"><spf:label path="startDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.start_date")} :</spf:label> </td>
                                    <td>
                                        <input id="fromDate" name="startDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${reportModel.startDate}" />" type="text" size="12" style="height: 16px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td><spf:label path="endDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.end_date")} :</spf:label></td>
                                    <td><input id="toDate" name="endDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${reportModel.endDate}" />" type="text" size="12" style="height: 16px;"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="22%"><spf:label path="transactionId" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.reports.transaction_id")} :</spf:label></td>
                                    <td><spf:input path="transactionId" size="12" /></td>
                                </tr>
                                <c:if test="${showInvoiceId}">
                                    <tr>
                                        <td><spf:label path="invoiceId" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.reports.invoiceno")} :</spf:label></td>
                                        <td><spf:input path="invoiceId"  size="12" /></td>
                                    </tr>
                                </c:if>
                                <c:if test="${showPinNumber}">
                                    <tr>
                                        <td><spf:label path="pinNumber" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.reports.pinreturn_search.pin")} :</spf:label></td>
                                        <td><spf:input path="pinNumber"  size="12" /></td>
                                    </tr>
                                </c:if>
                                <c:if test="${showIsos}">
                                    <tr>
                                        <td>${util:langString(Languages, "jsp.productapproval.selectISO")} :</td>
                                        <td>
                                            <select name="isoIds" size="10" multiple style="width: 50%">
                                                <option value="" selected>${util:langString(Languages, "jsp.admin.reports.all")}</option>
                                                <c:forEach items="${isoList}" var="iso">
                                                    <option value="${util:getObject(iso,1)}">${util:getObject(iso,0)}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                </c:if>
                                <c:if test="${showMerchants}">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <spf:checkbox path="allMerchants" id="chkMerchants" />
                                            ${util:langString(Languages, "jsp.tools.searchmerchant.selectAll")}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>${util:langString(Languages, "jsp.admin.reports.transactions.merchants.option")} :</td>
                                        <td>
                                            <input id="selMids" name="merchantIds" type="text" autocomplete="off"/>
                                            <select style="visibility: hidden; height: 30px;" name="merchantIds" id="merchantIds" multiple></select>

                                        </td>
                                    </tr>
                                </c:if>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br/>
                            <input id="btnSubmit" type="submit" class="main" value="${util:langString(Languages, "jsp.admin.reports.show_report")}"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br/>
                            * ${util:langString(Languages, "jsp.admin.reports.general")}
                        </td>
                    </tr>
                </table>
            </spf:form>
        </td>
    </tr>
</table>

<script>
    $(function(){

        $("#fromDate").datepicker({
            showOn: "button",
            buttonImage: "admin/calendar/calbtn.gif",
            buttonImageOnly: true,
            dateFormat:"mm/dd/yy",
            maxDate:$("#toDate").val(),
            onClose: function( selectedDate ) {
                $( "#toDate" ).datepicker( "option", "minDate", selectedDate );
                $(".ui-datepicker-trigger").css("vertical-align","top");
            }
        });

        $("#toDate").datepicker({
            showOn: "button",
            buttonImage: "admin/calendar/calbtn.gif",
            buttonImageOnly: true,
            dateFormat:"mm/dd/yy",
            minDate:$("#fromDate").val(),
            onClose: function( selectedDate ) {
                $( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
                $(".ui-datepicker-trigger").css("vertical-align","top");
            }
        });

        $(".ui-datepicker-trigger").css("vertical-align","top");

        $("#chkMerchants").prop("checked", true);

        $("#chkMerchants").change(function() {
            if(this.checked) {
                $("ul.pui-autocomplete-multiple li.pui-autocomplete-token").remove();
                $("#merchantIds option").remove();
            }
        });

        $('#selMids').puiautocomplete({
            effect: 'fade',
            effectSpeed: 'fast',
            multiple: true,
            select: function(event, item) {
                debugger;
                var itemValue = item.data('value');
                var option = new Option(item.data('label'), itemValue);
                if(itemValue.indexOf('*') == 0){
                    $("#selMids").prop( "disabled", true);
                }
                $('#merchantIds').append($(option));
                $("#merchantIds option[value='" + option.value + "']").prop('selected', true);
                $("#chkMerchants").prop("checked", false);
            },
            unselect: function(event, item) {
                debugger;
                var itemValue = item.data('value');
                if(itemValue.indexOf('*') == 0){
                    $("#selMids").prop( "disabled", false);
                }
                $("#merchantIds option[value='" + itemValue + "']").remove();
                var opts = $("#merchantIds option").length;
                if(opts == 0){
                    $("#chkMerchants").prop("checked", true);
                }
            },
            completeSource: function(request, response) {
                //in a real application, make a call to a remote url by passing the request.query as parameter
                var data = {"query": request.query, "repName": "${repName}", "repId": "${repId}"};
                debugger;
                $.ajax({
                    type: "GET",
                    data: data,
                    url: '/support/admin/transactions/merchants',
                    dataType: "json",
                    context: this,
                    success: function(data) {
                        console.log('Data :', data);
                        response.call(this, data);
                    }
                });
            }
        });

        $("span.pui-autocomplete").css("border", "1px solid #a9a9a9");
        $("ul.pui-autocomplete-multiple").removeClass("ui-state-default");
        $("#selMids").removeClass("ui-state-default");

        $("#selMids").focus(function() {
            $("ul.pui-autocomplete-multiple").removeClass("ui-state-focus");
            $("#selMids").removeClass("ui-state-focus");
            $("#selMids").removeClass("ui-state-hover");
        });
        $("#selMids").hover(function() {
            $("ul.pui-autocomplete-multiple").removeClass("ui-state-focus");
            $("#selMids").removeClass("ui-state-focus");
            $("#selMids").removeClass("ui-state-hover");
        });



    });

</script>

<c:import url="/WEB-INF/pages/footer.jsp" />