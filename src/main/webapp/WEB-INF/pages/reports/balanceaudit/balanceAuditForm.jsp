<%-- 
    Document   : balanceAuditReportForm
    Created on : Jan 19, 2016, 8:49:43 AM
    Author     : g.martinez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
    
    .input-error{
        color: #cd0a0a;
        margin-left: 1em;
        font-weight: bold;
    }
</style>

<script type="text/javascript">
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    
    var ini = function initializeFields(filterType,filterValue) {
            flag=false;
            if($.inArray( filterType, [ "USERNAME", "ENTITY_ID", "CHANGE_TYPE" ] )>-1){
                $("#filterType [value='"+filterType+"']").attr("selected","selected");
                flag=true;
                /*if(filterType=="CHANGE_TYPE"){
                    $("#filterValue").hide();
                    $("#filter_types_list").show();
                    $("#filterValue").val($("#filterValue2").val());
                }  */  
            
            }
            $("#filterValue").val(filterValue);
            if(flag==true){
               $('#balanceAuditForm').submit();    
            }
 
        };        
</script>

<table border="0" cellpadding="0" cellspacing="0" width="750" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
        ${util:langString(Languages, "jsp.admin.reports.balanceAudit.balanceAuditReport")}        
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <spf:form method="post" commandName="balanceAuditForm">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <spf:errors path="*" element="div" cssClass="errors"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td width="22%"><spf:label path="startDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.start_date")} :</spf:label> </td>
                                <td>
                                    <input id="fromDate" name="startDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${startDate}" />" type="text" size="12" style="height: 16px;">
                                </td>
                            </tr>
                            <tr>
                                <td><spf:label path="endDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.end_date")} :</spf:label></td>
                                <td><input id="toDate" name="endDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${endDate}" />" type="text" size="12" style="height: 16px;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td width="22%">${util:langString(Languages, "jsp.admin.reports.balanceAudit.filter.option")} :</td>
                                <td>
                                    <select id="filterType" name="filterType" path="filterType" style="width: 30%">  
                                        <option value="-1" >${util:langString(Languages, "jsp.admin.reports.balanceAudit.filterTypeSelectOption")}</option>
                                        <c:forEach items="${filterTypes}" var="prov">
                                            <option value="${util:getObject(prov,0)}">${util:getObject(prov,1)}</option>
                                        </c:forEach>
                                    </select>
                                    <br/><br/>
                                </td>
                            </tr>
                            <tr>
                                <td width="22%" id="fieldTextValue"><spf:label path="filterValue" id="labelFilterValue" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.reports.balanceAudit.filterValue")} :</spf:label></td>
                                <td id="textFilterTd"><spf:input path="filterValue" size="12" /><span id="filterValue_error" class="input-error" style="display:none">${util:langString(Languages, "jsp.admin.reports.balanceAudit.filterMustBeNumeric")}</span>
                                    <span id="filter_types_list" style="display:none;">
                                        <select id="filterValue2" style="width: 30%;"> 
                                            <c:forEach items="${changeTypesList}" var="type">
                                                <option value="${util:getObject(type,0)}">${util:getObject(type,1)}</option>
                                            </c:forEach>                                                                                           
                                        </select> 
                                    </span>      
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <input id="btnSubmit" type="submit" class="main" value="${util:langString(Languages, "jsp.admin.reports.show_report")}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        * ${util:langString(Languages, "jsp.admin.reports.general")}
                    </td>
                </tr>
            </table>
            </spf:form>
        </td>
    </tr>
</table>

<script>
$(document).ready(function(){
    
         $("#filterValue").hide();
         $("#fieldTextValue").hide();
         
        ini(getUrlParameter("fType"), getUrlParameter("fValue"));
        

        function setInputField(){
            //$("#textFilterTd").html(""); 
            $("#filterValue").hide();
        $("#filter_types_list").hide();  
        $("#filterValue").val("");
        $("#fieldTextValue").show();
        $("#filterValue").show();
        $("#labelFilterValue").removeClass("new-error"); 
        $("#filterValue_error").hide();
        
        }
        
        function setSelectList(){
            //$("#textFilterTd").html("");
            $("#filterValue2").show();
            $("#filterValue").hide();
            $("#fieldTextValue").show();
            $("#filter_types_list").show();
            $("#filterValue").val($("#filterValue2").val());
            $("#labelFilterValue").removeClass("new-error");
            $("#filterValue_error").hide();
                  
        }  
        //ajax petition to load list
    
    $(function(){

        $("#fromDate").datepicker({
            showOn: "button",
            buttonImage: "admin/calendar/calbtn.gif",
            buttonImageOnly: true,
            dateFormat:"mm/dd/yy",
            maxDate:$("#toDate").val(),
            onClose: function( selectedDate ) {
                $( "#toDate" ).datepicker( "option", "minDate", selectedDate );
                $(".ui-datepicker-trigger").css("vertical-align","top");
            }
        });

        $("#filterType").change(function(){
          var str = $("#filterType option:selected").val();
          switch(str){
              case "-1":
                 $("#filterValue").hide();
                 $("#fieldTextValue").hide();
                 $("#filterValue2").hide();
                 break;
              case "USERNAME":
                 setInputField();
                 break;
              case "ENTITY_ID":
                 setInputField();
                 break; 
              case "CHANGE_TYPE":
                 setSelectList();
                 break; 
              /*case "OLD_VALUE":
                 setInputField();
                 break; 
              case "NEW_VALUE":
                 setInputField();
                 break;*/
              default:
                 setInputField();
                 break;             
          }  
        });
        
        $('#filterValue').on('input', function() {
            var input=$(this);
            var re = /^[0-9]*$/;
            if($("#filterType option:selected").val()=="ENTITY_ID"||$("#filterType option:selected").val()=="OLD_VALUE"||$("#filterType option:selected").val()=="NEW_VALUE"){
                var is_numeric=re.test(input.val());
                if(is_numeric){$("#labelFilterValue").removeClass("new-error");$("#filterValue_error").hide();}
                else{$("#labelFilterValue").addClass("new-error");$("#filterValue_error").show();}
            }

        });  
        
        $("#filterValue2").change(function(){
             $("#filterValue").val($("#filterValue2").val());
        });    
        

        

        $("#toDate").datepicker({
            showOn: "button",
            buttonImage: "admin/calendar/calbtn.gif",
            buttonImageOnly: true,
            dateFormat:"mm/dd/yy",
            minDate:$("#fromDate").val(),
            onClose: function( selectedDate ) {
                $( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
                $(".ui-datepicker-trigger").css("vertical-align","top");
            }
        });

        $(".ui-datepicker-trigger").css("vertical-align","top");
    }); 
});
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />