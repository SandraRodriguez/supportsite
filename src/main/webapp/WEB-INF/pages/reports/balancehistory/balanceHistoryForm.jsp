<%-- 
    Document   : balanceHistoryReportForm
    Created on : Jan 19, 2016, 8:49:43 AM
    Author     : g.martinez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
    
    .input-error{
        color: #cd0a0a;
        margin-left: 1em;
        font-weight: bold;
    }
</style>

<table border="0" cellpadding="0" cellspacing="0" width="750" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle"> 
            ${util:langString(Languages, "jsp.admin.reports.balancehistory.balanceHistoryReport")}  
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <spf:form method="post" commandName="balanceHistoryForm" onsubmit="return validateMerchants();">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <spf:errors path="*" element="div" cssClass="errors"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td width="22%"><spf:label path="startDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.start_date")} :</spf:label> </td>
                                <td>
                                    <input id="fromDate" name="startDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${startDate}" />" type="text" size="12" style="height: 16px;">
                                </td>
                            </tr>
                            <tr>
                                <td><spf:label path="endDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.end_date")} :</spf:label></td>
                                <td><input id="toDate" name="endDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${endDate}" />" type="text" size="12" style="height: 16px;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td colspan="3">
                                    <jsp:include page="/admin/customers/merchants/entity_combo_selector.jsp" >
                                        <jsp:param value="${strRefId}" name="strRefId" />
                                        <jsp:param value="${strAccessLevel}" name="strAccessLevel" />
                                        <jsp:param value="${strDistChainType}" name="strDistChainType" />                                            
                                    </jsp:include>                                                
                                </td>
                            </tr>
                            <tr>
                                <td><spf:input type="hidden" path="merchants" size="12" /></td>   
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <input id="btnSubmit" type="submit" class="main" value="${util:langString(Languages, "jsp.admin.reports.show_report")}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        * ${util:langString(Languages, "jsp.admin.reports.general")}
                    </td>
                </tr>
            </table>
            </spf:form>
        </td>
    </tr>
</table>

<script>
    
    function validateMerchants(){
        $("#merchants").val($("#currentSelectedIds").val());
        if($("#merchants").val() === ''){
            alert('Select Merchants!');
            return false;
        }
        return true;
    }
    
$(document).ready(function(){
       
    function addMerchantList(){
        $("#merchants").val($("#currentSelectedIds").val());
    }
        
    $(function(){

        $("#fromDate").datepicker({
            showOn: "button",
            buttonImage: "admin/calendar/calbtn.gif",
            buttonImageOnly: true,
            dateFormat:"mm/dd/yy",
            maxDate:$("#toDate").val(),
            onClose: function( selectedDate ) {
                $( "#toDate" ).datepicker( "option", "minDate", selectedDate );
                $(".ui-datepicker-trigger").css("vertical-align","top");
            }
        });  
        
        $("#options_5").change(function(){
             addMerchantList();
        });
        
        
        $("#toDate").datepicker({
            showOn: "button",
            buttonImage: "admin/calendar/calbtn.gif",
            buttonImageOnly: true,
            dateFormat:"mm/dd/yy",
            minDate:$("#fromDate").val(),
            onClose: function( selectedDate ) {
                $( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
                $(".ui-datepicker-trigger").css("vertical-align","top");
            }
        });

        $(".ui-datepicker-trigger").css("vertical-align","top");
    });
});
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />