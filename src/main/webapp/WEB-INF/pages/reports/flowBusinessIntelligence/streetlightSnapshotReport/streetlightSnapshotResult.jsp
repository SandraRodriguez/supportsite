<%-- 
    Document   : StreetlightSnapshotReport
    Created on : Apr 20, 2017, 9:48:04 AM
    Author     : janez
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />
<%@ page isELIgnored="false" %>

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
    
    .tools_link{
        text-decoration: underline !important;
        color: #2C0973 !important;   
    }
/*    .pui-datatable-tablewrapper table{
        table-layout:auto;
    }*/

</style>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <spt:message code="jsp.admin.reports.flow.streetlightSnapshot.moreDetail" />
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                
                <tr>
                    <td>
                        <form method="get" action="/support/reports/streetlightSnapshot/historical/download" >
                            <input type="hidden" name="light" value="${light}">
                            <input type="hidden" name="totalRecords" value="${totalRecords}">
                            <input type="hidden" name="startDate" value="${startDate}">
                            <input type="hidden" name="endDate" value="${endDate}">
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                        <br/>
                        <input id="btnBack" class="btnDimensions" type="submit" value="${util:langString(Languages,"jsp.admin.reports.back")}" />
                        <br/>
                        <input id="btnDownload" type="submit" value="${util:langString(Languages, "jsp.admin.reports.downloadToCsv")}" />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
    
    function removeNullData(data){
        if(data=='null') return "";
        return data;
    }
    
    function formatTimestamp(date){
        return date;
    }
    

        

    $(function() {

	$('#btnBack').click(function(){
		parent.history.back();
		return false;
	});

        $('#btnDownload').prop('disabled',function(){
            if("${totalRecords}" == "0"){
                return true;
            }
            return false;
        });

        $('#tblResult').puidatatable({
            lazy:true,
            paginator: {
                rows: 25,
                pageLinks:10,
                totalRecords:${totalRecords}
            },
            columns: [
                {field:'rwn', headerText: '#', headerStyle:'width:3%'},  
                {field:'agentName', headerStyle:'width:12%', headerText: '${util:langString(Languages, "jsp.admin.reports.flow.streetlightSnapshot.agentName")}', sortable:true, content: function(row){return row.agentName;}},               
                {field:'subAgentName', headerStyle:'width:13%', headerText: '${util:langString(Languages, "jsp.admin.reports.flow.streetlightSnapshot.subAgentName")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.subAgentName;}},
                {field:'repName', headerStyle:'width:20%', headerText: '${util:langString(Languages, "jsp.admin.reports.flow.streetlightSnapshot.repName")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.repName;}},
                {field:'merchantName', headerStyle:'width:20%', headerText: '${util:langString(Languages, "jsp.admin.reports.flow.streetlightSnapshot.merchantName")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.merchantName;}}, 
                {field:'merchant_id', headerStyle:'width:15%', headerText: '${util:langString(Languages, "jsp.admin.reports.flow.streetlightSnapshot.merchantId")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.merchant_id;}},
            ],
            sortField:'merchantName',
            sortOrder:'1',
            datasource: function(callback, ui){
                $('#spinner').addClass("fa fa-refresh fa-spin");
                console.log('Calling..');
                var uri = 'reports/streetlightSnapshot/historical/grid';
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder == 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var data = {
                    "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows":rows, "totalRecords":"${totalRecords}",
                    "light":"${light}", "startDate":"${startDate}", "endDate":"${endDate}"};
                $('#sortField').val(sortField);
                $('#sortOrder').val(sortOrder);
                $.ajax({
                    type:"POST",
                    data:data,
                    url:uri,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        /*$('.pui-datatable-tablewrapper table').css("table-layout", "auto");*/
                        callback.call(this, response);
                    },
                    error:function(err){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        console.log(err);
                    }
                });
            }
        });



    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />