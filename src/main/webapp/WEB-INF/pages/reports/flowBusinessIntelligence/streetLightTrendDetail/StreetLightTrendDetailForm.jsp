<%-- 
    Document   : streetLightTrendDetailForm
    Created on : May 3, 2017, 10:49:27 AM
    Author     : janez
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />
<%@ page isELIgnored="false" %>

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }

    .input-error{
        color: #cd0a0a;
        margin-left: 1em;
        font-weight: bold;
    }
</style>

<table border="0" cellpadding="0" cellspacing="0" width="750" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle"> 
            ${util:langString(Languages, "jsp.admin.reports.flow.streetlightTrendDetail.title")}  
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
                <spf:form method="post" commandName="streetLightTrendDetailForm" onsubmit="return validateMerchants();" id="mainform" name="mainform">
                    <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                        <tr>
                            <td>
                                <spf:errors path="*" element="div" cssClass="errors"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="22%"><spf:label path="startDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.start_date")} :</spf:label> </td>
                                            <td>
                                                <input id="startDate" name="startDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${startDate}" />" type="text" size="12" style="height: 16px;">
                                                <a href="javascript:void(0)" onclick="if (self.gfPop)gfPop.fStartPop(document.mainform.startDate, document.mainform.endDate);return false;" HIDEFOCUS>
                                                <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><spf:label path="endDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.end_date")} :</spf:label></td>
                                        <td><input id="endDate" name="endDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${endDate}" />" type="text" size="12" style="height: 16px;">
                                            <a href="javascript:void(0)" onclick="if (self.gfPop)gfPop.fEndPop(document.mainform.startDate, document.mainform.endDate);return false;" HIDEFOCUS>
                                            <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td colspan="3">
                                            <jsp:include page="/admin/customers/merchants/entity_combo_selector.jsp" >
                                                <jsp:param value="${strRefId}" name="strRefId" />
                                                <jsp:param value="${strAccessLevel}" name="strAccessLevel" />
                                                <jsp:param value="${strDistChainType}" name="strDistChainType" />                                            
                                            </jsp:include>                                                
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                                <input type="hidden" name="filterBy" id="filterBy" value="">
                                <input type="hidden" name="filterValues" id="filterValues" value="">
                                <input id="btnSubmit" type="submit" class="main" value="${util:langString(Languages, "jsp.admin.reports.show_report")}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                                * ${util:langString(Languages, "jsp.admin.reports.general")}
                            </td>
                        </tr>
                    </table>
            </spf:form>
        </td>
    </tr>
</table>

<script>

    $(document).ready(function(){

    $(function(){

    $("#options_5").change(function(){
    addMerchantList();
    });


    $(".ui-datepicker-trigger").css("vertical-align","top");
    });
});

 function validateMerchants() {

    var filterBy = "iso";
    var filterValues = "";
    var selectedMerchants = $('#options_5').val();

     if(selectedMerchants != null){
        filterBy = "merchants";
        filterValues = selectedMerchants;
     }
     else{
         // REPS
         var selectedReps = $('#options_4').val();
         if(selectedReps != null){
         filterBy = "reps";
                 filterValues = selectedReps;
         }
         else{
             // SUBAGENTS
             var selectedSubAgents = $('#options_3').val();
                 if(selectedSubAgents != null){
             filterBy = "subAgents";
                     filterValues = selectedSubAgents;
             }
             else{
                 // AGENTS
                 var selectedAgents = $('#options_2').val();
                         if(selectedAgents != null){
                 filterBy = "agents";
                         filterValues = selectedAgents;
                 }
             }
         }
     }

    $("#filterBy").val(filterBy);
    $("#filterValues").val(filterValues);

    return true;
 }
    


</script>


<iframe width=132 height=142 name="gToday:contrast:agenda.js"
        id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm"
        scrolling="no" frameborder="0"
        style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;">
</iframe>


<c:import url="/WEB-INF/pages/footer.jsp" />
