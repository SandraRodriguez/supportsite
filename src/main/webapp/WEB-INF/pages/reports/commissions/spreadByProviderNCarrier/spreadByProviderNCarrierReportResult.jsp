<%--
  Author : Fernando Briceño based on Franky Villadiego
  Date: 2015-10-22
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
    .pui-datatable table{
        width: auto;
    }
    .textRed{
        color:red;
    }

    .btnDimensions{
        width: 9em;
    }
</style>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            ${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.reportTitle")}
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <form method="get" action="/support/reports/commissions/spreadByProviderNCarrier/download" >
                            <input type="hidden" name="startDate" value="${startDate}">
                            <input type="hidden" name="endDate" value="${endDate}">
                            <input type="hidden" name="providerIds" value="${providerIds}">
                            <input type="hidden" name="carrierIds" value="${carrierIds}">

                            <input type="hidden" name="totalRecords" value="${totalRecords}">
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                            <br/>
                            <input id="btnDownload" class="btnDimensions" type="submit" value="${util:langString(Languages,"jsp.admin.reports.downloadToCsv")}" />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form method="get" action="/support/reports/commissions/spreadByProviderNCarrier" >
                            <br/>
                            <input id="btnBack" class="btnDimensions" type="submit" value="${util:langString(Languages,"jsp.admin.reports.back")}" />
                        </form>
                    </td>                    
                </tr>
                   
                <tr>
                    <td>
                        <br/>
                        <p style="margin: 3px;">
                            ${totalRecords} ${util:langString(Languages,"jsp.admin.results_found")} ${util:langString(Languages,"jsp.admin.from")} ${startDate} ${util:langString(Languages,"jsp.admin.to")} ${endDate}
                            <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function removeNullData(data) {
        if (data === 'null')
            return "";
        if (typeof data === 'number') {
            if (data === 0)
                return "";
        }
        if (typeof data === 'string') {
            if (data.trim() === '0')
                return "";
        }
        return data;
    }
    function formatCurr(amount, currency) {
        return currency + amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    }



    $(function () {

        $('#tblResult').puidatatable({
            lazy: true,
            paginator: {
                rows: 25,
                pageLinks: 10,
                totalRecords:${totalRecords}
            },
            columns: [
                {field: 'rowNum', headerText: '#', headerStyle: 'width:15px'},
                {field: 'providerName', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.providerName")}', sortable: true},
                {field: 'carrierName', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.carrierName")}', sortable: true},
                {field: 'trxCount', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.trxCount")}', sortable: true,
                    bodyStyle: 'text-align:right'
                },
                {field: 'trxAmount', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.trxAmount")}', sortable: true, content: function (row) {
                        return formatCurr(row.trxAmount, "$");
                    },
                    bodyStyle: 'text-align:right'
                },
                {field: 'isoCommission', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.isoCommission")}', sortable: true, content: function (row) {
                        return formatCurr(row.isoCommission, "$");
                    },
                    bodyStyle: 'text-align:right'
                },
                {field: 'agentCommission', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.agentCommission")}', sortable: true, content: function (row) {
                        return formatCurr(row.agentCommission, "$");
                    },
                    bodyStyle: 'text-align:right'
                },
                {field: 'subagentCommission', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.subagentCommission")}', sortable: true, content: function (row) {
                        return formatCurr(row.subagentCommission, "$");
                    },
                    bodyStyle: 'text-align:right'
                },
                {field: 'repCommission', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.repCommission")}', sortable: true, content: function (row) {
                        return formatCurr(row.repCommission, "$");
                    },
                    bodyStyle: 'text-align:right'
                },
                {field: 'merchantCommission', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.merchantCommission")}', sortable: true, content: function (row) {
                        return formatCurr(row.merchantCommission, "$");
                    },
                    bodyStyle: 'text-align:right'
                },
                {field: 'totalChannelCommission', headerText: '${util:langString(Languages,"jsp.admin.reports.commissionSpreadByProvCarReport.totalChannelCommission")}', sortable: true, content: function (row) {
                        return formatCurr(row.totalChannelCommission, "$");
                    },
                    bodyStyle: 'text-align:right'
                }
            ],
            sortField: 'providerName',
            sortOrder: '1',
            datasource: function (callback, ui) {
                $('#spinner').addClass("fa fa-refresh fa-spin");
                console.log('Calling..');
                var uri = '/support/reports/commissions/spreadByProviderNCarrier/grid';
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder === 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var data = {
                    "start": start, "sortField": sortField, "sortOrder": sortOrder, "rows": rows, "totalRecords": "${totalRecords}",
                    "startDate": "${startDate}", "endDate": "${endDate}",
                    "providerIds": "${providerIds}", "carrierIds": "${carrierIds}"};
                $('#sortField').val(sortField);
                $('#sortOrder').val(sortOrder);
                $.ajax({
                    type: "GET",
                    data: data,
                    url: uri,
                    dataType: "json",
                    context: this,
                    success: function (response) {
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        callback.call(this, response);                        
                    },
                    error: function (err) {
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        console.log(err);
                    }
                });
            }
        });

    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />