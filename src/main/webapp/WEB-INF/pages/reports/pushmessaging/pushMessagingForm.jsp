<%-- 
    Document   : pushMessaging
    Created on : Mar 17, 2016, 10:50:05 AM
    Author     : dgarzon
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }

    .input-error{
        color: #cd0a0a;
        margin-left: 1em;
        font-weight: bold;
    }
</style>

<table border="0" cellpadding="0" cellspacing="0" width="1150" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle"> 
            ${util:langString(Languages, "jsp.admin.tools.pushMessaging.reportTitle")}        
        </td>
        
        
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">

            <form name=insertPushMessaging" action="admin/tools/pushMessaging/pushMessagingEdit.jsp" method="post">
                <input type="submit" name="submit" value="${util:langString(Languages, "jsp.admin.tools.pushMessaging.add")}">
                <input type=hidden name=type value=INSERT />
            </form>
        </td>
    </tr>
    <tr>
        
            
        
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <spf:form method="post" commandName="pushMessagingForm">
                <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                    <tr>
                        <td>
                            <spf:errors path="*" element="div" cssClass="errors"/>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="22%"><spf:label path="startDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.start_date")} :</spf:label> </td>
                                        <td>
                                            <input id="fromDate" name="startDate" value="${startDate}"type="text" size="12" style="height: 16px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td><spf:label path="endDate" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.end_date")} :</spf:label></td>
                                    <td><input id="toDate" name="endDate" value="${endDate}" type="text" size="12" style="height: 16px;"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin: 3px;">
                                <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                            
                            <table width="100%">
                                <tr>
                                    <td width="22%" id="fieldTextValue"><spf:label path="title" id="labelTitle" cssErrorClass="new-error">${util:langString(Languages, "jsp.admin.tools.pushMessaging.title")} :</spf:label></td>
                                    <td id="textFilterTd"><spf:input path="title" size="12" /><span id="title_error" class="input-error" style="display:none"></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br/>
                            <input id="btnSubmit" type="submit" class="main" value="${util:langString(Languages, "jsp.admin.tools.pushMessaging.search")}"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="overflow: scroll; width:100%;">
                            <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                        </td>
                    </tr>

                </table>
            </spf:form>
        </td>
    </tr>
</table>

<script>

    function editPushMessaging(pushMessagingId) {
        var url = goPath()+"pushMessagingEdit.jsp?pushMessagingId=" + pushMessagingId;
        //$(location).attr("href", url);
        return '<a class="tools_link" target="_self" href="'+url+'">'+"${util:langString(Languages,"jsp.admin.tools.pushMessaging.edit")}"+'</a>';      

    }
    
    function goPath() {
        if (isInternetExplorer()) {
            return "admin/tools/pushMessaging/";
        }
        else {
            return "admin/tools/pushMessaging/";
        }
    }
    
    function isInternetExplorer() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {      // If Internet Explorer, return version number
            return true;
        }
        else {                 // If another browser, return 0
            return false;
        }
    }
    
    function createLinkRemove(pushMessagingId){
        return '<a onclick="removePushMessaging(\''+pushMessagingId+'\')" href="javascript:void(0);">'+"${util:langString(Languages,"jsp.admin.tools.pushMessaging.delete")}"+'</a>';
    }
    
    function createCheckBox(pushMessagingId, isActive){
        var checked = "";
        if(isActive){
            checked = "checked"
        }
        return '<input type="checkbox" id="checkSMS" name="checkSMS" '+checked+' onclick="enableDisable(\''+pushMessagingId+'\')"/>';   
    }
    
    function enableDisable(pushMessagingId) {
        $.ajax({
            type: "POST",
            async: false,
            data: {pushMessagingId: pushMessagingId, action: 'enableDisablePushMessaging'},
            url: goPath()+"pushMessagingVerifyDB.jsp",
            success: function (data) {
            }
        });
        location.reload();
    }

    function removePushMessaging(pushMessagingId) {
        if (confirm('${util:langString(Languages, "jsp.admin.tools.pushMessaging.deleteConfirm")}')) {

            $.ajax({
                type: "POST",
                async: false,
                data: {pushMessagingId: pushMessagingId, action: 'deletePushMessaging'},
                url: goPath()+"pushMessagingVerifyDB.jsp",
                success: function (data) {
                }
            });
        }
        location.reload();
    }
    
        $(function () {

            $("#fromDate").datepicker({
                showOn: "button",
                buttonImage: "admin/calendar/calbtn.gif",
                buttonImageOnly: true,
                dateFormat: "mm/dd/yy",
                maxDate: $("#toDate").val(),
                onClose: function (selectedDate) {
                    $("#toDate").datepicker("option", "minDate", selectedDate);
                    $(".ui-datepicker-trigger").css("vertical-align", "top");
                }
            });

            $("#toDate").datepicker({
                showOn: "button",
                buttonImage: "admin/calendar/calbtn.gif",
                buttonImageOnly: true,
                dateFormat: "mm/dd/yy",
                minDate: $("#fromDate").val(),
                onClose: function (selectedDate) {
                    $("#fromDate").datepicker("option", "maxDate", selectedDate);
                    $(".ui-datepicker-trigger").css("vertical-align", "top");
                }
            });

            $(".ui-datepicker-trigger").css("vertical-align", "top");
        
        
        $('#tblResult').puidatatable({
            lazy:true,
            paginator: {
                rows: 25,
                pageLinks:10
            },
            columns: [
                {field:'rwn', headerText: '#', headerStyle:'width:3%'},
                {field:'title', headerStyle:'width:19%', headerText: '${util:langString(Languages, "jsp.admin.tools.pushMessaging.title")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.title;}},
                {field:'notificationDate', headerStyle:'width:15%', headerText: '${util:langString(Languages, "jsp.admin.tools.pushMessaging.notificationDate")}', sortable:true, content: function(row){return row.notificationDate;}},
                {field:'active', headerStyle:'width:10%', headerText: '${util:langString(Languages, "jsp.admin.tools.pushMessaging.active")}', content: function(row){return createCheckBox(row.id, row.active);}},
                {field:'edit', headerStyle:'width:12%', headerText: '${util:langString(Languages, "jsp.admin.tools.pushMessaging.edit")}', bodyStyle:'text-align:right', content: function(row){return editPushMessaging(row.id);}},
                {field:'delete', headerStyle:'width:12%', headerText: '${util:langString(Languages, "jsp.admin.tools.pushMessaging.delete")}', bodyStyle:'text-align:right', content: function(row){return createLinkRemove(row.id);}}
        
            ],
            sortField:'title',
            sortOrder:'1',
            datasource: function(callback, ui){
                
                $('#spinner').addClass("fa fa-refresh fa-spin");
                console.log('Calling..');
                var uri = 'reports/pushmessaging/historical/grid';
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder == 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var data = {
                    "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows":rows, "totalRecords":"${totalRecords}",
                    "startDate":"${startDate}", "endDate":"${endDate}", "title":"${title}"
                };
                $('#sortField').val(sortField);
                $('#sortOrder').val(sortOrder);
                
                $.ajax({
                    type:"POST",
                    data:data,
                    url:uri,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        callback.call(this, response);
                    },
                    error:function(request, err){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        console.log(err);
                    }
                });
            }
        });
        
        });
        
        

   
    
    
    
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />