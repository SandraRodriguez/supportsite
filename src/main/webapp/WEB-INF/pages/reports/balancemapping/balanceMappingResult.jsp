<%-- 
    Document   : balanceAuditReportResult
    Created on : Jan 20, 2016, 12:32:26 AM
    Author     : g.martinez
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="util" uri="http://emida.net/jsp/utils" %>

<c:import url="/WEB-INF/pages/header.jsp" />

<style type="text/css">


    .header-tbl{
        white-space: nowrap;
    }
    .hidden-column{
        display: none;
    }
/*    .pui-datatable-tablewrapper table{
        table-layout:auto;
    }*/

</style>


<table border="0" cellpadding="0" cellspacing="0" style="margin: 5px;">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <spt:message code="jsp.admin.reports.balanceMapping.merchantRemainingBalanceReport" />
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">
            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                
                <tr>
                    <td>
                        <form method="get" action="/support/reports/balancemapping/historical/download" >
                            <input type="hidden" name="totalRecords" value="${totalRecords}">       
                            <input id="sortOrder" type="hidden" name="sortOrder" >
                            <input id="sortField" type="hidden" name="sortField" >
                            <br/>
                            <input id="btnBack" class="btnDimensions" type="submit" value="${util:langString(Languages,"jsp.admin.reports.back")}" />                            
                        <br/>
                        <input id="btnDownload" type="submit" value="${util:langString(Languages, "jsp.admin.reports.downloadToCsv")}" />
                        </form>
                    </td>
                </tr>                
                <tr>
                    <td>
                        <br/>
                        <p style="margin: 3px;">
                            ${totalRecords} ${util:langString(Languages,"jsp.admin.results_found")}: ${activeRecords} ${util:langString(Languages,"jsp.admin.reports.balanceMapping.merchantRemaining.results.active")} / ${disabledRecords} ${util:langString(Languages,"jsp.admin.reports.balanceMapping.merchantRemaining.results.disabled")} / ${cancelledRecords} ${util:langString(Languages,"jsp.admin.reports.balanceMapping.merchantRemaining.results.cancelled")}
                                <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                        </p>
                        <span></span>
                    </td>
                </tr>
                <tr>
                    <td style="overflow: scroll; width:100%;">
                        <div id="tblReview" style="margin-top:5px; width: 100%"></div>
                        <div id="tblResult" style="margin-top:5px; width: 100%"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
      
    function removeNullData(data){
        if(data=='null') return "";
        return data;
    }
    
    function formatTimestamp(date){
        return date;
    }

    $(function() {

        $('#btnDownload').prop('disabled',function(){
            if("${totalRecords}" == "0"){
                return true;
            }
            return false;
        });
        

	$('#btnBack').click(function(){
		parent.history.back();
		return false;
	});
      
        $('#tblResult').puidatatable({
            lazy:true,
            paginator: {
                rows: 25,
                pageLinks:10,
                totalRecords:${totalRecords}
            },
            columns: [
                {field:'rwn', headerText: '#', headerStyle:'width:3%'},
                {field:'merchantId', headerStyle:'width:13%', headerText: '${util:langString(Languages, "jsp.admin.reports.balanceMapping.merchantRemaining.field.merchantId")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.merchantId;}},             
                {field:'merchant', headerStyle:'width:7%', headerText: '${util:langString(Languages, "jsp.admin.reports.balanceMapping.merchantRemaining.field.merchant")}', sortable:true, content: function(row){return row.merchant;}},             
                {field:'contact', headerStyle:'width:20%', headerText: '${util:langString(Languages, "jsp.admin.reports.balanceMapping.merchantRemaining.field.contact")}', sortable:true, content: function(row){return row.contact;}},
                {field:'phone', headerStyle:'width:12%', headerText: '${util:langString(Languages, "jsp.admin.reports.balanceMapping.merchantRemaining.field.phone")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.phone;}},
                {field:'status', headerStyle:'width:12%', headerText: '${util:langString(Languages, "jsp.admin.reports.balanceMapping.merchantRemaining.field.status")}', bodyStyle:'text-align:right', sortable:true, content: function(row){return row.status;}}
            ],
            sortField:'merchant',
            sortOrder:'1',
            datasource: function(callback, ui){
                $('#spinner').addClass("fa fa-refresh fa-spin");
                console.log('Calling..');
                var uri = '/support/reports/balancemapping/historical/grid';
                var start = ui.first + 1;
                var sortField = ui.sortField;
                var sortOrder = ui.sortOrder == 1 ? 'ASC' : 'DESC';
                var rows = this.options.paginator.rows;
                var data = {
                    "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows":rows, "totalRecords":"${totalRecords}",
                };
                $('#sortField').val(sortField);
                $('#sortOrder').val(sortOrder);
                $.ajax({
                    type:"GET",
                    data:data,
                    url:uri,
                    dataType:"json",
                    context:this,
                    success:function(response){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        callback.call(this, response);
                    },
                    error:function(err){
                        $('#spinner').removeClass("fa fa-refresh fa-spin");
                        console.log(err);
                    }
                });
            }
        });



    });
</script>

<c:import url="/WEB-INF/pages/footer.jsp" />