<%-- 
    Document   : consolidateMonthlySales
    Created on : 10-ago-2017, 15:43:38
    Author     : janez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.util.*,com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
    String path = request.getContextPath();
%>

<html>
    <head>        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/loading.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.js"></script>                                                
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script>         
        <link href="<%=path%>/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>        
        <script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.min.js"></script>                
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>                              

        <link href="<%=path%>/css/multi-select.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="<%=path%>/js/jquery.multi-select.js"></script>   
        <script type="text/javascript" src="<%=path%>/js/jquery.quicksearch.js"></script>   

        <script>
            var vgid = "";
            var vgcarrier = "";
            function loadCarriers(isinit) {
                if (isinit) {
                    $('.init-cm-msg').show();
                } else {
                    $('.init-cm-msg').hide();
                }
                $('.tbl-product-mapping').hide();
                $('.carrs-mappers').html('');
                $('.carrs-mappers').append("<a class='list-group-item disabled'>" + '<%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl2", SessionData.getLanguage())%>' + "</a>");
                $('.carrs-mappers').append("<a class='list-group-item' href='javascript:void(0)' onclick='onfinddiffcarriers()' data-toggle='modal' data-target='#cmdescription-addModal'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span> " + '<%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl14", SessionData.getLanguage())%>' + "</a>");
                $('.carrs-mappers').append("<a class='list-group-item disabled'>" + '<%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl3", SessionData.getLanguage())%>' + "</a>");

                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/reports/consolidateMonthlySales/findACarrierMappingInf',
                    error: function () {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    data.forEach(function (element) {
                        var item = element.carrier_description;
                        $('.carrs-mappers').append("<a onclick=\"whoIts('" + item + "')\" class='list-group-item' href='javascript:void(0)'><span class='glyphicon glyphicon-cog' aria-hidden='true'></span> " + item + " <span class='badge'>" + element.product + "</span></a>");
                    });
                });
            }
            function whoIts(name) {
                vgcarrier = name;
                $('.cmctitle').text(vgcarrier);
                $('.alert1').hide();
                $('.cmaddpro').hide();
                $('.init-cm-msg').hide();
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/reports/consolidateMonthlySales/findProductsByCarrier',
                    data: {
                        carrierDescription: name
                    },
                    error: function () {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    buildTableAdmSku(data);
                });
            }
            function ondeletesku(id) {
                $(".ajax-loading").show();
                $.ajax({
                    type: 'POST',
                    timeout: 60000,
                    data: {
                        id: id
                    },
                    url: '/support/reports/consolidateMonthlySales/deleteProductStatus',
                    error: function () {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                    whoIts(vgcarrier);
                    loadCarriers(false);
                });
            }
            ;
            function onfindproducts() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    data: {
                        description: vgcarrier
                    },
                    url: '/support/reports/consolidateMonthlySales/findProductsByCarrierDescription'
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    $("select").remove(".cmproductsl");
                    $("div").remove(".ms-cmproductsl");
                    $("div").remove(".ms-container");
                    $(".inner").append('<select class="cmproductsl" id="cmproductsl" multiple>');
                    data.forEach(function (element, index) {
                        $("#cmproductsl").append($('<option>', {
                            value: element.id,
                            text: element.description
                        }));
                    });
                    $("#cmproductsl").multiSelect();
                });
            }
            function onfinddiffcarriers() {
                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    url: '/support/reports/consolidateMonthlySales/findDiffCarriers'
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    $('.cmseldc').empty();
                    data.forEach(function (element, index) {
                        $(".cmseldc").append($('<option>', {
                            value: element.carrier_description,
                            text: element.carrier_description
                        }));
                    });
                });
            }
            function onaddProductMapping() {
                $(".ajax-loading").show();
                $.ajax({
                    type: 'POST',
                    timeout: 60000,
                    data: {
                        carrier_description: vgcarrier,
                        product_id: getProducts().toString()
                    },
                    url: '/support/reports/consolidateMonthlySales/addProductMapping',
                    error: function () {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                    whoIts(vgcarrier);
                    loadCarriers(false);
                });
            }

            function getProducts() {
                var selectedProducts = [];
                if ($('#cmproductsl').val() !== null) {
                    $("#cmproductsl :selected").each(function () {
                        selectedProducts.push($(this).val());
                    });
                }
                return selectedProducts;
            }

            $(document).ready(function () {
                $('.alert1').hide();
                $(".logtx").hide();
                $(".cmaddpro").hide();
                $('.cmaddpro').hide();
                $('.content-cm-2').hide();

                $(".btnsearch").prop('disabled', true);
                $('#tbllstbundling').DataTable();
                $(".divErrMessage").hide();

                $(".tbllstbundlingdiv").hide();
                $(".btnReportDownloader").hide();

                $(".btnsearch").click(function () {
                    findConsolidateMonthlySales();
                });
                $('.btnupdatesku').click(function () {
                    var vst = $('#skuisenabled').is(":checked");
                    $(".ajax-loading").show();
                    $.ajax({
                        type: 'POST',
                        timeout: 60000,
                        data: {
                            enabled: (vst ? 1 : 0),
                            id: vgid
                        },
                        url: '/support/reports/consolidateMonthlySales/updateProductStatus',
                        error: function () {
                            $(".ajax-loading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                        whoIts(vgcarrier);
                    });
                });
                $('.btnaddcarrier').click(function () {
                    var cardes = $('.cmseldc').find(":selected").text();
                    if (cardes.length !== 0) {
                        $(".ajax-loading").show();
                        $.ajax({
                            type: 'POST',
                            timeout: 60000,
                            data: {
                                carrier_description: cardes
                            },
                            url: '/support/reports/consolidateMonthlySales/addCarrier',
                            error: function () {
                                $(".ajax-loading").hide();
                            }
                        }).done(function (msg) {
                            $(".ajax-loading").hide();
                            loadCarriers(false);
                            vgcarrier = cardes;
                            $('.cmaddpro').show();
                            $('.tbl-product-mapping').hide();
                            onfindproducts();
                        });
                    }
                });
                $('.btndeletecarrier').click(function () {
                    if (vgcarrier.length !== 0) {
                        $(".ajax-loading").show();
                        $.ajax({
                            type: 'POST',
                            timeout: 60000,
                            data: {
                                carrier_description: vgcarrier
                            },
                            url: '/support/reports/consolidateMonthlySales/deleteCarrier',
                            error: function () {
                                $(".ajax-loading").hide();
                                $('.alert1').hide();
                            }
                        }).done(function (msg) {
                            $(".ajax-loading").hide();
                            $('.alert1').hide();
                            loadCarriers(true);
                        });
                    }
                });
                $('.btnupdatecarrier').click(function () {
                    $(".ajax-loading").show();
                    $.ajax({
                        type: 'POST',
                        timeout: 60000,
                        data: {
                            carrier_description_old: vgcarrier,
                            carrier_description_new: $('.namecarrierdes').val().toString()
                        },
                        url: '/support/reports/consolidateMonthlySales/updateCarrierDescription',
                        error: function () {
                            $(".ajax-loading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                        loadCarriers(false);
                    });
                });
                $('.cm-tab1').click(function () {
                    $(".content-cm-2").hide();
                    $(".content-cm-1").show();
                    $('.cm-tab1').addClass("active");
                    $('.cm-tab2').removeClass("active");
                });
                $('.cm-tab2').click(function () {
                    $(".content-cm-1").hide();
                    $(".cmaddpro").hide();
                    $(".alert1").hide();
                    $(".content-cm-2").show();
                    $('.cm-tab2').addClass("active");
                    $('.cm-tab1').removeClass("active");
                    loadCarriers(true);
                });
                $(".btnReportDownloader").click(function () {
                    var month = parseInt($("#idmonth :selected").val());
                    var selectDt = $("#idyear :selected").text() + '-' + (month < 10 ? '0' + month : month) + '-01';

                    var entityType = -1;
                    if ($('#reportType1').is(":checked")) {
                        entityType = 8;
                    } else if ($('#reportType2').is(":checked")) {
                        entityType = 1;
                    }
                    var url_download = '/support/reports/consolidateMonthlySales/csv?periodDt=' + selectDt + '&entityType=' + entityType + '&operation=2';
                    $(".ajax-loading").show();
                    var result = $.ajax({
                        type: 'GET',
                        timeout: 600000,
                        url: url_download,
                        async: true,
                        error: function () {
                            $(".ajaxloading").hide();
                        }
                    }).done(function (msg) {
                        $(".ajax-loading").hide();
                    });
                    result.success(function (data, status, headers, config) {
                        var fileName = (entityType === 8 ? "creditoIndividual" : "creditoCompartido");
                        var csvContent = data;
                        var pom = document.createElement('a');
                        var blob = new Blob([csvContent], {
                            type: 'text/csv;charset=UTF-8;'
                        });
                        var url = URL.createObjectURL(blob);
                        pom.href = url;
                        pom.setAttribute('download', fileName + '_' + selectDt + '.csv');
                        document.body.appendChild(pom);
                        pom.click();
                        setTimeout(function () {
                            document.body.removeChild(pom);
                            window.URL.revokeObjectURL(url);
                        }, 150);
                    });
                });

                var cyear = parseInt((new Date()).getFullYear());
                for (var i = 1; i <= 2; i++) {
                    if (i === 2) {
                        $('#idyear').append($('<option> selected', {
                            value: i,
                            text: (cyear - 1)
                        }));
                    } else {
                        $('#idyear').append($('<option>', {
                            value: i,
                            text: cyear
                        }));
                    }
                }

                $("#idyear").change(function () {
                    var select = $("#idyear :selected").val();
                    $('option', '#idmonth').remove();
                    if (select === '2') {
                        loadMonthsPass();
                    } else {
                        loadMonths();
                    }
                });
                loadMonths();

                $("#reportType1").attr('checked', 'checked');
            });

            function isInternetExplorer() {
                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE");
                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {      // If Internet Explorer, return version number
                    return true;
                } else {
                    return false;
                }
            }

            function getMonthName(index) {
                switch (index) {
                    case 1:
                        return '<%= Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>';
                    case 2:
                        return '<%= Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>';
                    case 3:
                        return '<%= Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>';
                    case 4:
                        return '<%= Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>';
                    case 5:
                        return '<%= Languages.getString("reports.monthName.May", SessionData.getLanguage())%>';
                    case 6:
                        return '<%= Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>';
                    case 7:
                        return '<%= Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>';
                    case 8:
                        return '<%= Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>';
                    case 9:
                        return '<%= Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>';
                    case 10:
                        return '<%= Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>';
                    case 11:
                        return '<%= Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>';
                    case 12:
                        return '<%= Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>';
                }
            }

            function loadMonthsPass() {
                var cmes = parseInt(new Date().getMonth()) + 1;
                for (var indej = cmes; indej <= 12; indej++) {
                    $('#idmonth').append($('<option>', {
                        value: indej,
                        text: getMonthName(indej)
                    }));
                }
            }

            function loadMonths() {
                var cmes = parseInt(new Date().getMonth()) + 1;
                for (var index = 1; index <= cmes; index++) {
                    $('#idmonth').append($('<option>', {
                        value: index,
                        text: getMonthName(index)
                    }));
                }
            }

            function findConsolidateMonthlySales() {
                var month = parseInt($("#idmonth :selected").val());
                var selectDt = $("#idyear :selected").text() + '-' + (month < 10 ? '0' + month : month) + '-01';
                var entityType = -1;
                if ($('#reportType1').is(":checked")) {
                    entityType = 8;
                } else if ($('#reportType2').is(":checked")) {
                    entityType = 1;
                }

                $(".ajax-loading").show();
                var result = $.ajax({
                    type: 'GET',
                    timeout: 600000,
                    data: {
                        periodDt: selectDt,
                        entityType: entityType,
                        operation: 1
                    },
                    url: '/support/reports/consolidateMonthlySales/consolidate',
                    error: function () {
                        $(".ajax-loading").hide();
                    }
                }).done(function (msg) {
                    $(".ajax-loading").hide();
                });
                result.success(function (data, status, headers, config) {
                    onSuccessResponse(data);
                });
            }

            function onSuccessResponse(data) {
                if (data.length === 0) {
                    $(".tbllstbundlingdiv").hide();
                    $(".btnReportDownloader").hide();
                    $('#tbllstbundling').dataTable().fnClearTable();
                    $(".divErrMessage").show();
                    $(".alertmaxs").hide(150);
                } else {
                    $(".btnReportDownloader").show();
                    $(".tbllstbundlingdiv").show();
                    $(".divErrMessage").hide();
                    $('#tbllstbundling').dataTable().fnDestroy();
                    buildTable(data);
                }
            }

            function buildTable(data) {
                $('#tbllstbundling').DataTable({
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bDestroy": true,
                    "paging": true,
                    "iDisplayLength": 10,
                    searching: true,
                    aoColumns: [
                        {"mData": 'merchantData.merchantId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "80px"},
                        {"mData": 'merchantData.isoId', sDefaultContent: "--", sClass: "alignCenter", sWidth: "40px"},
                        {"mData": 'merchantData.merchantName', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'merchantData.taxId', sDefaultContent: "--", sClass: "alignLeft", sWidth: "60px"},
                        {"mData": 'merchantData.address', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'merchantData.city', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'merchantData.delegation', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {"mData": 'merchantData.state', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {"mData": 'merchantData.country', sDefaultContent: "--", sClass: "alignLeft"},
                        {"mData": 'merchantData.zip', sDefaultContent: "--", sClass: "alignCenter", sWidth: "15px"},
                        {mRender: function (data, type, row) {
                                var initb = parseFloat(row.merchantData.iniBalance);
                                return initb.toFixed(2);
                            }, sDefaultContent: "--", sClass: "alignRight"
                        },
                        {mRender: function (data, type, row) {
                                var finalb = parseFloat(row.merchantData.finalBalance);
                                return finalb.toFixed(2);
                            }, sDefaultContent: "--", sClass: "alignRight"
                        }
                    ],
                    "aaSorting": [[0, "desc"]],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.report.consolidate.monthly.sales.search.empty")}',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    }
                });
            }

            function buildTableAdmSku(data) {
                $('.tbl-product-mapping').show();
                $('#tbl-product-mapping').DataTable({
                    "aaData": data,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bDestroy": true,
                    "paging": true,
                    "iDisplayLength": 10,
                    searching: true,
                    aoColumns: [
                        {"mData": 'product', sDefaultContent: "--", sClass: "alignCenter", sWidth: "80px"},
                        {"mData": 'carrier_description', sDefaultContent: "--", sClass: "alignLeft"},
                        {mRender: function (data, type, row) {
                                if (row.enabled) {
                                    return '<span class="label label-success">Si</span>';
                                } else {
                                    return '<span class="label label-danger">No</span>';
                                }
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "120px"
                        },
                        {mRender: function (data, type, row) {
                                return '<span style="cursor: pointer" onclick=onEditsku("' + row.enabled + '","' + row.id + '") data-toggle="modal" data-target="#cm-editModal" class="glyphicon glyphicon-edit" aria-hidden="true"></span>';
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "60px"
                        },
                        {mRender: function (data, type, row) {
                                return '<span style="cursor: pointer" onclick=ondeletesku("' + row.id + '") class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                            }, sDefaultContent: "--", sClass: "alignCenter", sWidth: "60px"
                        }
                    ],
                    "aaSorting": [[1, "desc"]],
                    "oLanguage": {
                        "sSearch": '${Languages.getString("jsp.admin.genericLabel.search")}',
                        "sZeroRecords": '${Languages.getString("jsp.report.consolidate.monthly.sales.search.empty")}',
                        "sInfo": '${Languages.getString("jsp.tools.notice.contracts.showing")}' +
                                " _START_ - _END_ " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' +
                                " _TOTAL_ " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoEmpty": '${Languages.getString("jsp.tools.notice.contracts.showing")}' + " 0 - 0 " +
                                '${Languages.getString("jsp.tools.notice.contracts.of")}' + " 0 " + '${Languages.getString("jsp.tools.notice.contracts.register")}',
                        "sInfoFiltered": "",
                        "oPaginate": {
                            "sFirst": '${Languages.getString("jsp.tools.datatable.first")}',
                            "sNext": '${Languages.getString("jsp.admin.next")}',
                            "sPrevious": '${Languages.getString("jsp.admin.previous")}',
                            "sLast": '${Languages.getString("jsp.tools.datatable.last")}'
                        }
                    }
                });
            }
            function onEditsku(enabled, id) {
                vgid = id;
                if (enabled === 'true') {
                    $("#skuisenabled").prop('checked', true);
                } else {
                    $("#skuisenabled").prop('checked', false);
                }
            }
        </script>      
        <style type="text/css">
            .alignCenter { text-align: center; }
            .alignRight { text-align: right; }            
            .alignLeft { text-align: left; }
        </style>
    </head>
    <body>
    <center>
        <div class="ajax-loading">
            <div></div>
        </div>
    </center>        
    <table cellspacing="0" cellpadding="0" border="0" background="images/top_blue.gif" width="1800px" style="margin-top: 5px; margin-left: 5px">
        <tbody><tr>
                <td width="23" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td class="formAreaTitle" width="4000"><%=Languages.getString("jsp.report.consolidate.monthly.sales.title", SessionData.getLanguage())%></td>
                <td height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="0" border="0" bgcolor="#7B9EBD" width="100%">
                        <tbody>                  
                            <tr>
                                <td width="1"><img src="images/trans.gif" width="1"></td>
                                <td bgcolor="#ffffff" valign="top" align="center">
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%" align="center">
                                        <tbody><tr>
                                                <td class="main" align="left">
                                                    <table class="table">
                                                        <tbody>                                                     
                                                            <tr>                                                           
                                                                <td>
                                                                    <ul class="nav nav-tabs">                                                                                                                                                                                                                 
                                                                        <li role="presentation" class="lstemplate active cm-tab1"><a style="cursor: pointer" class="lstemplate active"><img src="images/report-icon.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.report.consolidate.monthly.sales.title", SessionData.getLanguage())%></a></li>                                                                                                                                                                                                                                     
                                                                        <li role="presentation" class="cm-tab2"><a style="cursor: pointer"><img src="images/analysis_cube.png" border="0" width="22" height="22">  <%=Languages.getString("jsp.conf.consolidate.monthly.sales.title", SessionData.getLanguage())%> <span class="label label-success"><%=Languages.getString("jsp.admin.genericLabel.new", SessionData.getLanguage())%></span></a></li>                                                                                                                                                
                                                                    </ul>  
                                                                    <br />                                                                                                                                                                                                  
                                                                    <div class="container-fluid">
                                                                        <div class="content-cm-1">
                                                                            <div class="row" style="background-color: #ffffff">
                                                                                <div class="col-md-1">
                                                                                    <label class="text-muted"><%= Languages.getString("jsp.report.consolidate.monthly.sales.month", SessionData.getLanguage())%></label>                                                                                                                                                                                                                                                                       
                                                                                </div>  

                                                                                <div class="col-sm-2">
                                                                                    <select id='idmonth' class="form-control">                                                                                                                                                                                            
                                                                                    </select> 
                                                                                </div>
                                                                                <div class="col-md-1">
                                                                                    <select id='idyear' class="form-control">                                                                                                                                                                                            
                                                                                    </select>                                               
                                                                                </div>                                                                                             
                                                                            </div>  
                                                                            <br />
                                                                            <div class="row" style="background-color: #ffffff">
                                                                                <div class="col-md-1">
                                                                                    <label class="text-muted"><%= Languages.getString("jsp.report.consolidate.monthly.sales.label", SessionData.getLanguage())%></label>
                                                                                </div>                                     
                                                                                <div class="col-md-10">
                                                                                    <input type="radio" id="reportType1" name="reportType" value="8"> <label class="text-muted"><%= Languages.getString("jsp.report.consolidate.monthly.sales.option1", SessionData.getLanguage())%></label><br />
                                                                                    <input type="radio" id="reportType2" name="reportType" value="1"> <label class="text-muted"><%= Languages.getString("jsp.report.consolidate.monthly.sales.option2", SessionData.getLanguage())%></label>
                                                                                </div>                                           
                                                                            </div>
                                                                            <hr />
                                                                            <div class="alertmaxs">                                                                        
                                                                                <div class="alert alert-warning">                                                                                                                         
                                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                    <span class="glyphicon glyphicon-eye-open"></span> 
                                                                                    <strong>
                                                                                        <%= Languages.getString("jsp.report.consolidate.monthly.sales.search.rule1", SessionData.getLanguage())%>                                                                                
                                                                                    </strong>                                                                            
                                                                                </div>                                              
                                                                            </div>                                                                                    
                                                                            <ul class="pager">
                                                                                <li class="previous btnsearch" id="btnsearch" style="cursor:pointer"><a><span class="glyphicon glyphicon-search" aria-hidden="true"></span> <%= Languages.getString("jsp.admin.search", SessionData.getLanguage())%></a></li>                                                                            
                                                                                <li class="next btnReportDownloader" style="cursor:pointer"><a><span class="glyphicon glyphicon-save" aria-hidden="true"></span> <%= Languages.getString("jsp.tools.bundling.update.download", SessionData.getLanguage())%></a></li>                                                                            
                                                                            </ul>                                                                    
                                                                            <div class="divErrMessage">
                                                                                <div class="alert alert-danger" role="alert">
                                                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                                                    <p id="errMessage" style="display:inline"><%= Languages.getString("jsp.report.consolidate.monthly.sales.search.empty", SessionData.getLanguage())%></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tbllstbundlingdiv">
                                                                                <table id="tbllstbundling" class="table table-hover">                                                                            
                                                                                    <thead>
                                                                                        <tr>                                                                                                                                                                        
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col1", SessionData.getLanguage())%></th>                                                                                    
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col2", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col3", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col4", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col5", SessionData.getLanguage())%></th>                                                                                    
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col7", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col13", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col8", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col9", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col10", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col11", SessionData.getLanguage())%></th>
                                                                                            <th style="text-align: center !important"><%= Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col12", SessionData.getLanguage())%></th>
                                                                                        </tr>
                                                                                    </thead>                                                                                                    
                                                                                </table>
                                                                            </div>                                       
                                                                        </div>  
                                                                        <!--content tab 2-->
                                                                        <div class="content-cm-2">
                                                                            <div class="col-md-2">
                                                                                <div class="list-group carrs-mappers">                                                                                    
                                                                                </div>                
                                                                            </div>

                                                                            <div class="col-md-10">
                                                                                <div class="alert1">
                                                                                    <div class="alert alert-warning" role="alert">                                                                                        
                                                                                        <h4><span class="glyphicon glyphicon-alert" aria-hidden="true"></span> <%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl19", SessionData.getLanguage())%></h4>                       
                                                                                        <br />                                                                                        
                                                                                        <%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl20", SessionData.getLanguage())%>
                                                                                        <br />
                                                                                        <br />
                                                                                        <br />
                                                                                        <br />
                                                                                        <hr />                                                                                        
                                                                                        <%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl21", SessionData.getLanguage())%>
                                                                                        <br />
                                                                                        <br />                                                                                                                                                                                
                                                                                        <button type="button" class="btn btn-default" onclick="$('.alert1').hide();
                                                                                                $('.tbl-product-mapping').show()"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                        <button type="button" class="btn btn-danger btndeletecarrier"><%= Languages.getString("jsp.admin.genericLabel.Delete", SessionData.getLanguage())%></button>                                                                                        
                                                                                    </div>
                                                                                </div>

                                                                                <blockquote class="init-cm-msg">
                                                                                    <p>
                                                                                        <%= Languages.getString("jsp.conf.consolidate.monthly.sales.title", SessionData.getLanguage())%>                                                                                                                                                                                                                                                                        
                                                                                    </p> <small><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl22", SessionData.getLanguage())%>                                                                                        </small>
                                                                                </blockquote>                                                                                                                                                                

                                                                                <div class="tbl-product-mapping">                                                                                    
                                                                                    <h1 class="cmctitle text-muted text-capitalize"></h1>
                                                                                    <div class="btn-group">
                                                                                        <button type="button" class="btn btn-default"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl4", SessionData.getLanguage())%></button>
                                                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                            <span class="caret"></span>                                                                                            
                                                                                        </button>
                                                                                        <ul class="dropdown-menu">
                                                                                            <li><a href='javascript:void(0)' onclick="$('.cmaddpro').show();
                                                                                                    $('.tbl-product-mapping').hide();
                                                                                                    onfindproducts()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl5", SessionData.getLanguage())%></a></li>                                                                                            
                                                                                            <li role="separator" class="divider"></li>                                                                                                                                                                                        
                                                                                            <li><a href='javascript:void(0)' onclick="$('.lblcarrierdes').text('<%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl1", SessionData.getLanguage())%>');
                                                                                                    $('.namecarrierdes').val(vgcarrier)" data-toggle="modal" data-target="#cmdescription-editModal"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl6", SessionData.getLanguage())%></a></li>                                                                                            
                                                                                            <li><a href='javascript:void(0)' onclick="$('.alert1').show();
                                                                                                    $('.tbl-product-mapping').hide();"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl7", SessionData.getLanguage())%></a></li>                                                                                            
                                                                                        </ul>
                                                                                    </div>                                                                                                                                                                        
                                                                                    <table id="tbl-product-mapping" class="table table-hover">                                                                            
                                                                                        <thead>
                                                                                            <tr>                                                                                                                                                                        
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.tbl.col1", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.tbl.col2", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.tbl.col3", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.tbl.col4", SessionData.getLanguage())%></th>                                                                                    
                                                                                                <th style="text-align: center !important"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.tbl.col5", SessionData.getLanguage())%></th>                                                                                    
                                                                                            </tr>
                                                                                        </thead>                                                                                                    
                                                                                    </table>
                                                                                </div>  
                                                                                <!--edit product-->
                                                                                <div>                                                                                    
                                                                                    <div class="modal fade" id="cm-editModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                                        <div class="vertical-alignment-helper">
                                                                                            <div class="modal-dialog modal-sm vertical-align-center">
                                                                                                <!--<div class="modal-dialog modal-sm">-->
                                                                                                <div class="modal-content">
                                                                                                    <div class="modal-header">
                                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                        <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl8", SessionData.getLanguage())%></h4>
                                                                                                    </div>
                                                                                                    <div class="modal-body">                                                                                                        
                                                                                                        <h4><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl9", SessionData.getLanguage())%></h4>
                                                                                                        <input type="checkbox" id="skuisenabled"> 
                                                                                                    </div>
                                                                                                    <div class="modal-footer">                                                                                                        
                                                                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                                        <button type="button" class="btn btn-success btnupdatesku" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>                                                                                            
                                                                                        </div>
                                                                                        <!--</div>-->
                                                                                    </div>
                                                                                </div>

                                                                                <!--edit carrier description-->
                                                                                <div>
                                                                                    <div class="modal fade" id="cmdescription-editModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                                        <div class="vertical-alignment-helper">
                                                                                            <div class="modal-dialog modal-sm vertical-align-center">
                                                                                                <!--<div class="modal-dialog">-->
                                                                                                <div class="modal-content">
                                                                                                    <div class="modal-header">
                                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                        <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl17", SessionData.getLanguage())%></h4>
                                                                                                    </div>
                                                                                                    <div class="modal-body">                                                                                                    
                                                                                                        <div class="form-group">
                                                                                                            <label for="inputdefault"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl15", SessionData.getLanguage())%></label>
                                                                                                            <input class="form-control namecarrierdes" id="inputdefault" type="text">
                                                                                                            <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl18", SessionData.getLanguage())%></small>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="modal-footer">
                                                                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                                        <button type="button" class="btn btn-success btnupdatecarrier" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></button>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--</div>-->
                                                                                            </div></div>
                                                                                    </div>                                               
                                                                                </div>
                                                                                <!--add carrier description-->                                                                               
                                                                                <div>
                                                                                    <div class="modal fade" id="cmdescription-addModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
                                                                                        <div class="vertical-alignment-helper">
                                                                                            <div class="modal-dialog modal-sm vertical-align-center">
                                                                                                <div class="modal-content">
                                                                                                    <div class="modal-header">
                                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                        <h4 class="modal-title" id="myModalLabel"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl14", SessionData.getLanguage())%></h4>
                                                                                                    </div>
                                                                                                    <div class="modal-body">                                                                                                    
                                                                                                        <div class="form-group">                                                                                                               
                                                                                                            <label for="inputdefault" class="lblcarrierdes"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl15", SessionData.getLanguage())%></label>                                              
                                                                                                            <br/>
                                                                                                            <!--newnamecarrierdes-->
                                                                                                            <select class="form-control cmseldc">                                                                                                                
                                                                                                            </select>                                                                                                        
                                                                                                            <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl16", SessionData.getLanguage())%></small>
                                                                                                        </div>                                                                                                    
                                                                                                    </div>
                                                                                                    <div class="modal-footer">
                                                                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>
                                                                                                        <button type="button" class="btn btn-success btnaddcarrier" data-dismiss="modal"><%= Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>                                               
                                                                                </div>
                                                                                <!--add product-->
                                                                                <div class="cmaddpro text-muted">                                                                                                                                                                                                                                                        
                                                                                    <h3><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl10", SessionData.getLanguage())%></h3>                                                                                    
                                                                                    <small id="emailHelp" class="form-text text-muted"><%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl11", SessionData.getLanguage())%></small>
                                                                                    <br />
                                                                                    <div class="inner">                                                                                        
                                                                                    </div>                                                                                    
                                                                                    </select>                                                                                      
                                                                                    <a href='javascript:void(0)' onclick="$('#cmproductsl').multiSelect('select_all')"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> <%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl12", SessionData.getLanguage())%></a>
                                                                                    <br />                                                                                    
                                                                                    <a href='javascript:void(0)' onclick="$('#cmproductsl').multiSelect('deselect_all')"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> <%= Languages.getString("jsp.conf.consolidate.monthly.sales.lbl13", SessionData.getLanguage())%></a>                                                                                                                                                                                    
                                                                                    <br />
                                                                                    <hr />
                                                                                    <button type="button" onclick="$('.cmaddpro').hide();
                                                                                            $('.tbl-product-mapping').show()" class="btn btn-default"><%= Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%></button>                                                                                                                                                                                                                                                           
                                                                                    <button type="button" onclick="onaddProductMapping()" class="btn btn-success"><%= Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%></button>                                                                                                                                                                                                                                                           
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>   
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                                <td bgcolor="#003082" width="1"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#003082" height="1"><img src="images/trans.gif" height="1"></td>
                            </tr>
                        </tbody></table>
                </td>
            </tr>     
        </tbody>
    </table>
    <style type="text/css">
        .alignCenter { text-align: center; }
        .alignRight { text-align: right; }            
        .alignLeft { text-align: left; }


        .modal {
        }
        .vertical-alignment-helper {
            display:table;
            height: 100%;
            width: 100%;
        }
        .vertical-align-center {                
            display: table-cell;
            vertical-align: middle;
        }
        .modal-content {                
            width:inherit;
            height:inherit;               
            margin: 0 auto;
        }

        .modal.fade .modal-dialog {
            -webkit-transform: scale(0.1);
            -moz-transform: scale(0.1);
            -ms-transform: scale(0.1);
            transform: scale(0.1);
            top: 300px;
            opacity: 0;
            -webkit-transition: all 0.1s;
            -moz-transition: all 0.1s;
            transition: all 0.1s;
        }

        .modal.fade.in .modal-dialog {
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
            -webkit-transform: translate3d(0, -300px, 0);
            transform: translate3d(0, -300px, 0);
            opacity: 1;
        }
    </style>
</body>      
<%@ include file="/includes/footer.jsp" %>
</html>