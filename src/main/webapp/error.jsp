<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ page import="com.debisys.utils.DebisysConfigListener,com.debisys.utils.ConfigHelper"%>
<%
	ConfigHelper ch = new ConfigHelper(request);
	String serverName = ch.getServerName();
	String baseHref = ch.getBaseHref();
	String domainName = ch.getDomainName();
	String serverPort = ch.getServerPort();
	String requestUrl = ch.getRequestUrl();
	String imageName = ch.getImageName();
	String headerBackground = ch.getHeaderBackground();
	String strBrandedCompanyName = ch.getBrandedCompanyName();
//requestUrl comes either as     http://    or     https://
	String strProtocol = requestUrl.substring(0, requestUrl.indexOf(':')).toLowerCase();
	String deploymentType = DebisysConfigListener.getDeploymentType(application);
	String customConfigType = DebisysConfigListener.getCustomConfigType(application);
	boolean isNG = request.getParameter("site")!=null && request.getParameter("site").equals("NG");
%>
<html>
<head>
	<base href="<%=strProtocol%>://<%=baseHref%>/support/">
	<link href="default.css" type="text/css" rel="stylesheet">
	<link href="css/themes/emida-green/jquery-ui.css" type="text/css" rel="stylesheet">
	<title><%=strBrandedCompanyName%></title>
	<script language="JavaScript" src="/support/includes/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script language="JavaScript" src="/support/includes/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="/support/includes/SSWSCheckboxes.js"></script>
</head>
<body>
<%
	if (isNG) {
%>
<div class="error">
	<p>This section is not enabled</p>
</div>
<%
} else {
%>
<div id="divLanguage" class="main"></div>
<div id="divPrintBanner" style="display: inline">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/banner.gif" bgcolor="#ffffff" class="fondogris" background="images/banner.gif">
					<tr>
						<td width="100%" height="58">
							<table valign="middle" align="left" border="0" cellpadding="0" cellspacing="0" >
								<tr>
									<td colspan="2" align="left"><a href="https://support.debisys.com/support/admin/index.jsp"><img src="images/localhost.png" alt="Inicio" border="0"></a></td>
								</tr>
								<tr>
									<td style="font-size: 8pt;" class="float">&nbsp;&nbsp;Idioma:
										<input type="hidden" value="Espa�ol" id="language" name="language"/>
										<!-- <a href="/support/servlet/Languages?lan=spanish&page=%2Fsupport%2F%2Flogin.jsp%3Fnull" id="Espa�ol">Espa�ol</a> -->
										<a href="javascript:" id="Espa�ol" name="Espa�ol" onclick="changeLanguage('spanish');">Espa�ol</a>
										|
										<input type="hidden" value="Ingl�s" id="language" name="language"/>
										<!-- <a href="/support/servlet/Languages?lan=english&page=%2Fsupport%2F%2Flogin.jsp%3Fnull" id="Ingl�s">Ingl�s</a> -->
										<a href="javascript:" id="Ingl�s" name="Ingl�s" onclick="changeLanguage('english');">Ingl�s</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<div>
	<div class="error">
		<p>We're sorry. An error has occurred!</p><p> Please click <a href="/support/">here</a> to continue or contact your administrator.</p>
	</div>
</div>

<%
	}
%>
</body>
</html>
