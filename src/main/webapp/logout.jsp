<%
	com.debisys.users.SessionData sessionSupportSite = (com.debisys.users.SessionData)session.getAttribute("SessionData");
	
	String sendRedirect="message.jsp?message=3&l="+sessionSupportSite.getLanguage();
	if (sessionSupportSite!=null && sessionSupportSite.getUser()!=null && sessionSupportSite.getUser().isIntranetUser())
	{
                HttpSession localSession = request.getSession();
                if(localSession != null)
                {
                    ServletContext localServletContext = localSession.getServletContext();
                    if(localServletContext != null)
                    {
                        String intranetSite = (String)localServletContext.getAttribute("intranet-site");
                        String intranetPortSite = (String)localServletContext.getAttribute("intranet-port-site");
                        sendRedirect="http://"+intranetSite+":"+intranetPortSite+"/logout.asp";
                    }
                }
	}
	session.invalidate();
	response.sendRedirect(sendRedirect);
%>