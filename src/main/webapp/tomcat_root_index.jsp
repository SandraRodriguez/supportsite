<%
  // To have this jsp take effect in Tomcat 5.5, remover the servlet and
  // servlet-mapping for index.jsp in the ROOT/web.xml file
 
  String serverName = request.getServerName();
  String serverPort = Integer.toString(request.getServerPort());
  String baseHref = serverName;
  String domainName = "";
  
  if (serverPort != null && !serverPort.equals(""))
  {
    baseHref += ":" + serverPort;
  }
  if ((serverName != null) && (!serverName.equals("")) && !serverName.equalsIgnoreCase("support.debisys.com"))
  {

  int index = serverName.indexOf('.');
  if (index == -1) 
  {
    domainName = serverName;
  }
  else 
  {
    int index2 = serverName.indexOf('.', index + 1);
    if (index2 == -1)
    {
      domainName = serverName;
    }
    else
    {
      domainName = serverName.substring(index + 1);
    }
    
  }
  }
  else
  {
    serverName = "support.debisys.com";
    domainName = "debisys.com";

  }

  //defaults
  String strProtocol = "https";


    if (domainName.equalsIgnoreCase("debisys.com"))
    {
      if(serverName.toLowerCase().indexOf("qualitycalling") != -1)
      {
        strProtocol = "http";
      }
      else if(serverName.toLowerCase().indexOf("giromex") != -1)
      {
        strProtocol = "http";
      }
      // only "support.debisys.com" has the ssl cert for https operation
      else if(serverName.toLowerCase().indexOf("support") == -1)
      {
        strProtocol = "http";
      }
    }
    else
    {
      strProtocol = "http";
      //force them to debisys
      //serverName = "support.debisys.com";
      //9-27-05 - Jay - taken out because it was causing problems during our ip address change

     }


response.sendRedirect(strProtocol + "://" + baseHref + "/support/index.jsp");
%>
