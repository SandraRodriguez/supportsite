Instructions to Deploy.

	
1) Edit web.xml and change paths and configuration values

2) Edit log4j.properties and change from DEBUG to WARN and
   use ConsoleAppender A1 for rootLogger

3) Change database to live db in torque.properties

4) Access the tomcat manager webapp at http://fhdev01:9006/manager/html. 
   Login as tc-manager with password t0mcat.
   jar:file:/Volumes/Share1/jaychi/Temp/debisys.war!/

fhdev01
jar:file:/Users/gandalf/Deployments/debisys.war!/


fhdev02
jar:file:/Users/gandalf/support_site/debisys.war!/

irapp02
jar:file:/Users/gandalf/Deployments/debisys.war!/

irapp01
jar:file:/Users/gandalf/support_site/debisys.war!/

5) for new tomcat installations, copy the tomcat_root_index.jsp
   to webapps/ROOT/index.jsp.  This sets up the re-direct to the 
   support site so as to avoid the tomcat WELCOME page.